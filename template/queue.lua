local ENQUEUE_CAPACITY = 100

local function l_exists(rec, bin)
  if aerospike:exists(rec)
    and rec[bin] ~= nil
      and type(rec) == "userdata" then
    return true
  end
  return false
end

local function l_update(rec)
  if aerospike:exists(rec) then
    aerospike:update(rec)
  else
    aerospike:create(rec)
  end
end

local function l_enqueue(q, value)
  if q.rear == nil then
    q.rear = list(ENQUEUE_CAPACITY)
  end
  list.append(q.rear, value)
  q.size = q.size + 1
end

local function l_dequeue(q)
  if q.front == nil then
    q.front = q.rear
    q.pos = 1
    map.remove(q, "rear")
  end

  local item = nil
  local last = list.size(q.front)
  if last == 1 then
    item = q.front[1]
    map.remove(q, "front")
    map.remove(q, "pos")
  else
    if q.pos < last then
      item = q.front[q.pos]
      q.front[q.pos] = q.front[last]
      q.pos = q.pos + 1
    else
      item = q.front[last]
    end
    list.remove(q.front, last)
  end
  q.size = q.size - 1
  return item
end

function enqueue(rec, bin, value)
  local q = rec[bin]
  if q == nil then
    q = map { size = 0 }
  end
  l_enqueue(q, value)
  rec[bin] = q
  l_update(rec)
  return q.size
end

function dequeue(rec, bin)
  if not l_exists(rec, bin) then
    return nil
  end

  local q = rec[bin]
  if q.size == 0 then
    return nil
  end

  local item = l_dequeue(q)
  rec[bin] = q
  l_update(rec)
  return item
end
