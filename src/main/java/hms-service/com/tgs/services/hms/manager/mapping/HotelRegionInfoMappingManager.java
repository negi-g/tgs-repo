package com.tgs.services.hms.manager.mapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import com.tgs.services.hms.datamodel.HotelRegionInfoMapping;
import com.tgs.services.hms.datamodel.HotelRegionMappingQuery;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfoMapping;
import com.tgs.services.hms.dbmodel.DbHotelSupplierRegionInfo;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import com.tgs.services.hms.restmodel.HotelCityInfoMappingResponse;
import com.tgs.services.hms.restmodel.HotelCityMappingRequest;
import com.tgs.services.hms.restmodel.HotelDeleteMappingRequest;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.restmodel.HotelRegionInfoRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelRegionInfoMappingManager {

	@Autowired
	HotelRegionInfoService regionInfoService;

	@Autowired
	HotelSupplierRegionInfoManager supplierRegionInfoManager;

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	public void process(HotelRegionInfoRequest hotelCityInfos) throws Exception {
		saveRegionInfoMappingList(hotelCityInfos.getRegionInfoQuery(), true, false);
	}

	public void saveRegionInfoMappingList(List<HotelRegionInfoQuery> hotelRegionInfos, Boolean enableMasterRegion,
			Boolean isFullRegionNameExists) {
		for (HotelRegionInfoQuery hotelRegionInfo : hotelRegionInfos) {
			HotelRegionInfo regionInfo = processRegionInfo(hotelRegionInfo);
			DbHotelRegionInfo masterRegionInfo = null;
			DbHotelSupplierRegionInfo supplierRegionInfo = null;
			if ((masterRegionInfo =
					regionInfoService.isMasterRegionExists(regionInfo, isFullRegionNameExists)) == null) {
				log.debug("Unable to find master region for region name {}, country name {} and region type {}",
						regionInfo.getRegionName(), regionInfo.getCountryName(), regionInfo.getRegionType());
				continue;
			}
			if ((supplierRegionInfo =
					regionInfoService.isSupplierRegionExists(hotelRegionInfo, isFullRegionNameExists)) == null) {
				log.debug(
						"Unable to find supplier region for region name {}, state name {}, country name {}, supplier name {} and region type {}",
						hotelRegionInfo.getRegionName(), hotelRegionInfo.getStateName(),
						hotelRegionInfo.getCountryName(), hotelRegionInfo.getSupplierName(),
						hotelRegionInfo.getRegionType());
				continue;
			}
			HotelRegionInfoMapping regionInfoMapping =
					processRegionInfoMapping(hotelRegionInfo, masterRegionInfo.getId(), supplierRegionInfo.getId());
			saveRegionMapping(regionInfoMapping);
			supplierRegionInfo.setSupplierRegionName(hotelRegionInfo.getRegionName());
			supplierRegionInfoManager.saveSupplierRegionInfo(Arrays.asList(supplierRegionInfo.toDomain()));
			if (BooleanUtils.isTrue(enableMasterRegion)) {
				regionInfoService.enableMasterRegion(masterRegionInfo.getId());
			}
		}
	}

	public void saveRegionMapping(HotelRegionInfoMapping regionInfoMapping) {
		DbHotelRegionInfoMapping dbRegionInfoMapping = new DbHotelRegionInfoMapping().from(regionInfoMapping);
		try {
			dbRegionInfoMapping = regionInfoService.saveSupplierMapping(dbRegionInfoMapping);
			log.debug("Updated region info mapping with region id {} and supplierName{} ",
					dbRegionInfoMapping.getRegionId(), dbRegionInfoMapping.getSupplierName());
		} catch (Exception e) {
			DbHotelRegionInfoMapping oldRegionInfoMapping = regionInfoService.findByRegionIdAndSupplierName(
					dbRegionInfoMapping.getRegionId(), dbRegionInfoMapping.getSupplierName());
			Long id = oldRegionInfoMapping.getId();
			oldRegionInfoMapping =
					new GsonMapper<>(dbRegionInfoMapping, oldRegionInfoMapping, DbHotelRegionInfoMapping.class)
							.convert();
			oldRegionInfoMapping.setId(id);
			log.debug("Updated region info mapping with region id {} and supplierName{} ",
					oldRegionInfoMapping.getRegionId(), oldRegionInfoMapping.getSupplierName());
			regionInfoService.saveSupplierMapping(oldRegionInfoMapping);
		}
	}

	public static HotelRegionInfoMapping processRegionInfoMapping(HotelRegionInfoQuery regionInfoQuery,
			Long masterRegionId, Long supplierRegionId) {

		HotelRegionInfoMapping regionInfoMapping =
				HotelRegionInfoMapping.builder().supplierName(regionInfoQuery.getSupplierName())
						.regionId(masterRegionId).supplierRegionId(supplierRegionId).build();
		return regionInfoMapping;
	}

	public static HotelRegionInfo processRegionInfo(HotelRegionInfoQuery regionInfoQuery) {

		String regionName = ObjectUtils.firstNonNull(regionInfoQuery.getRegionName(), regionInfoQuery.getStateName());
		HotelRegionInfo regionInfo = HotelRegionInfo.builder().regionName(regionName)
				.countryName(regionInfoQuery.getCountryName()).regionType(regionInfoQuery.getRegionType())
				.fullRegionName(ObjectUtils.firstNonNull(regionInfoQuery.getFullRegionName(), "")).build();
		return regionInfo;
	}

	public void saveHotelRegionMappingRequest(HotelCityMappingRequest unmappedCityRequest) {

		try {
			List<HotelRegionMappingQuery> cityMappingQueryList = unmappedCityRequest.getRegionMappingQuery();
			for (HotelRegionMappingQuery cityMappingQuery : cityMappingQueryList) {
				processAndSaveRegionMappingRequest(cityMappingQuery);
			}
		} catch (Exception e) {
			log.error("Unable to save region mapping for request {}", GsonUtils.getGson().toJson(unmappedCityRequest),
					e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}

	private void saveSupplierRegionInfoCache(HotelSupplierRegionInfo supplierRegionInfo, Long regionId) {
		Map<String, Object> supplierRegionInfoBin = new HashMap<>();
		supplierRegionInfoBin.put(supplierRegionInfo.getSupplierName(), GsonUtils.getGson().toJson(supplierRegionInfo));
		CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.REGION_MAPPING.getName()).key(String.valueOf(regionId)).expiration(-1)
				.binValues(supplierRegionInfoBin).build();
		cacheService.store(metaInfoToStore);
	}


	public void saveSupplierRegionMappingIntoCache(Long regionId) {
		List<DbHotelRegionInfoMapping> dbRegionInfoMappings = regionInfoService.findByRegionId(regionId);
		if (CollectionUtils.isNotEmpty(dbRegionInfoMappings)) {
			for (DbHotelRegionInfoMapping dbRegionInfoMapping : dbRegionInfoMappings) {
				DbHotelSupplierRegionInfo dbSupplierRegionInfo =
						regionInfoService.findSupplierRegionInfoById(dbRegionInfoMapping.getSupplierRegionId());
				saveSupplierRegionInfoCache(dbSupplierRegionInfo.toDomain(), regionId);
			}
		}
	}


	private HotelRegionInfoMapping processAndSaveRegionMappingRequest(HotelRegionMappingQuery regionMappingQuery) {
		String regionName = "";
		String countryName = "";
		String fullRegionName = "";

		if (StringUtils.isBlank(regionName = regionMappingQuery.getRegionName())) {
			throw new CustomGeneralException(SystemError.INVALID_REGION_NAME);
		}

		if (StringUtils.isBlank(countryName = regionMappingQuery.getCountryName())) {
			throw new CustomGeneralException(SystemError.INVALID_COUNTRY_NAME);
		}

		if (StringUtils.isBlank(fullRegionName = regionMappingQuery.getFullRegionName())) {
			throw new CustomGeneralException(SystemError.INVALID_FULL_REGION_NAME);
		}

		DbHotelRegionInfo dbHotelRegionInfo =
				regionInfoService.findByRegionNameAndCountryNameAndRegionTypeAndFullRegionName(regionName,
						countryName, regionMappingQuery.getRegionType(), fullRegionName);

		if (BooleanUtils.isNotTrue(dbHotelRegionInfo.getEnabled())) {
			throw new CustomGeneralException(SystemError.REGION_INFO_DISABLED);
		}

		if (Objects.isNull(dbHotelRegionInfo))
			throw new CustomGeneralException(SystemError.HOTEL_REGION_NOT_EXISTS);

		DbHotelSupplierRegionInfo dbSupplierRegionInfo =
				regionInfoService.findSupplierRegionInfoById(regionMappingQuery.getSupplierRegionInfoId());
		if (Objects.isNull(dbHotelRegionInfo))
			throw new CustomGeneralException(SystemError.HOTEL_SUPPLIER_REGION_NOT_EXISTS);

		HotelRegionInfoMapping regionInfoMapping =
				HotelRegionInfoMapping.builder().supplierName(dbSupplierRegionInfo.getSupplierName())
						.regionId(dbHotelRegionInfo.getId()).supplierRegionId(dbSupplierRegionInfo.getId()).build();
		saveRegionMapping(regionInfoMapping);
		dbSupplierRegionInfo.setSupplierRegionName(regionMappingQuery.getSupplierRegionName());
		supplierRegionInfoManager.saveSupplierRegionInfo(Arrays.asList(dbSupplierRegionInfo.toDomain()));
		saveSupplierRegionInfoCache(dbSupplierRegionInfo.toDomain(), regionInfoMapping.getRegionId());
		return regionInfoMapping;
	}

	public List<HotelRegionInfoMapping> getHotelRegionMappingForSupplier(String supplierName) {

		List<DbHotelRegionInfoMapping> dbRegionInfoMapping = regionInfoService.findBySupplierName(supplierName);
		return DbHotelRegionInfoMapping.toDomainList(dbRegionInfoMapping);
	}

	public HotelCityInfoMappingResponse getHotelRegionMappingForRegionId(String regionId) {
		HotelCityInfoMappingResponse cityMappingResponse = new HotelCityInfoMappingResponse();
		try {
			List<DbHotelRegionInfoMapping> dbRegionMappings =
					regionInfoService.findByRegionId(Long.parseLong(regionId));
			cityMappingResponse.setRegionInfos(DbHotelRegionInfoMapping.toDomainList(dbRegionMappings));
		} catch (Exception e) {
			throw new CustomGeneralException(SystemError.INVALID_CITY);
		}
		return cityMappingResponse;
	}

	public List<HotelSupplierRegionInfo> findBySupplierName(String supplierName) {
		List<HotelSupplierRegionInfo> supplierRegionInfos = new ArrayList<>();
		for (int i = 0; i < 500; i++) {
			Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
			List<HotelSupplierRegionInfo> regionInfoMappingChunk =
					DbHotelSupplierRegionInfo.toDomainList(regionInfoService.findBySupplierName(supplierName, page));
			if (CollectionUtils.isEmpty(regionInfoMappingChunk))
				break;
			supplierRegionInfos.addAll(regionInfoMappingChunk);
			log.debug("Fetched hotel region mapping info from database, mapping list size is {}",
					supplierRegionInfos.size());
		}

		return supplierRegionInfos;

	}

	public void deleteHotelRegionMapping(HotelDeleteMappingRequest deleteMappingRequest) {

		try {
			List<HotelRegionInfoMapping> regionMappingQueryList = deleteMappingRequest.getRegionInfoMappingQuery();
			for (HotelRegionInfoMapping regionMappingQuery : regionMappingQueryList) {

				if (Objects.isNull(regionMappingQuery.getRegionId())) {
					throw new CustomGeneralException(SystemError.INVALID_HOTEL_REGION_ID);
				}

				if (StringUtils.isBlank(regionMappingQuery.getSupplierName())) {
					throw new CustomGeneralException(SystemError.INVALID_SUPPLIER_NAME);
				}

				DbHotelRegionInfoMapping oldRegionInfoMapping = regionInfoService.findByRegionIdAndSupplierName(
						regionMappingQuery.getRegionId(), regionMappingQuery.getSupplierName());

				if (Objects.isNull(oldRegionInfoMapping))
					throw new CustomGeneralException(SystemError.HOTEL_SUPPLIER_REGION_NOT_EXISTS);

				DbHotelSupplierRegionInfo supplierRegionInfo =
						regionInfoService.findSupplierRegionInfoById(oldRegionInfoMapping.getSupplierRegionId());
				supplierRegionInfo.setSupplierRegionName("");
				regionInfoService.saveSupplierRegionInfo(supplierRegionInfo);
				saveSupplierRegionInfoCache(supplierRegionInfo.toDomain(), oldRegionInfoMapping.getRegionId());
			}
		} catch (Exception e) {
			log.error("Unable to delete region mapping for request {}",
					GsonUtils.getGson().toJson(deleteMappingRequest), e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}

}
