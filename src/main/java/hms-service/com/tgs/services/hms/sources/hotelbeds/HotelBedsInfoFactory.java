package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelBedsInfoFactory extends AbstractHotelInfoFactory {

	@Autowired
	protected HotelCacheHandler cacheHandler;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public HotelBedsInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {

		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException, InterruptedException {
		if (StringUtils.isNotBlank(searchQuery.getSearchPreferences().getHotelId())
				&& Objects.isNull(searchQuery.getMiscInfo().getHotelWiseSearchInfo())) {
			log.info("Not able to search for searchid {} and supplier {} as hotel wise search info is not present",
					searchQuery.getSearchId(), supplierConf.getBasicInfo().getSupplierName());
			return;
		}
		Set<String> propertyIds = getPropertyIdsOfCity(HotelSourceType.HOTELBEDS.name());
		searchResult = getResultOfAllPropertyIds(propertyIds);
	}

	@Override
	protected HotelSearchResult doSearchForSupplier(Set<String> propertyIds, int threadCount)
			throws IOException, JAXBException {
		List<Integer> propertyIdList = propertyIds.stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
		log.info("Doing search for searchid {} with propertyids {}", searchQuery.getSearchId(), propertyIdList.size());
		HotelBedsSearchService searchService = HotelBedsSearchService.builder().sourceConfig(sourceConfigOutput)
				.supplierConf(this.getSupplierConf()).propertyIds(propertyIdList).searchQuery(searchQuery)
				.cacheHandler(cacheHandler).moneyExchnageComm(moneyExchnageComm).build();
		searchService.doSearch(threadCount);
		HotelSearchResult searchResult = (HotelSearchResult) searchService.getSearchResult();
		return searchResult;
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException, JAXBException {
		Set<String> propertyIds = new HashSet<>();
		propertyIds.add(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		List<Integer> propertyIdList = propertyIds.stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
		HotelBedsSearchService searchService = HotelBedsSearchService.builder().sourceConfig(sourceConfigOutput)
				.supplierConf(this.getSupplierConf()).propertyIds(propertyIdList).searchQuery(searchQuery)
				.cacheHandler(cacheHandler).moneyExchnageComm(moneyExchnageComm).build();
		searchService.doDetailSearch();
		searchResult = (HotelSearchResult) searchService.getSearchResult();
		hInfo.setOptions(searchResult.getHotelInfos().get(0).getOptions());

	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {

		boolean isCancellationPolicyAlreadySet = false;
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option cachedOption = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		for (Option op : hotel.getOptions()) {
			if (op.getId().equals(optionId) && !ObjectUtils.isEmpty(cachedOption.getCancellationPolicy())) {
				op.setCancellationPolicy(cachedOption.getCancellationPolicy());
				isCancellationPolicyAlreadySet = true;
			}
		}
		if (!isCancellationPolicyAlreadySet) {
			HotelBedsCancellationPolicyService cancellationPolicyService =
					HotelBedsCancellationPolicyService.builder().sourceConfig(sourceConfigOutput).hInfo(hotel)
							.supplierConf(this.getSupplierConf()).searchQuery(searchQuery).build();
			cancellationPolicyService.searchCancellationPolicy(logKey);
		}

	}
}
