package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CleartripHotelInfoFactory extends AbstractHotelInfoFactory {

	public CleartripHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	public void searchAvailableHotels() throws IOException {
		if (StringUtils.isNotBlank(searchQuery.getSearchPreferences().getHotelId())
				&& Objects.isNull(searchQuery.getMiscInfo().getHotelWiseSearchInfo())) {
			log.info("Not able to search for searchid {} and supplier {} as hotel wise search info is not present",
					searchQuery.getSearchId(), supplierConf.getBasicInfo().getSupplierName());
			return;
		}

		if (Objects.isNull(supplierRegionInfo)) {
			log.info("Not able to search for searchid {} and supplier {} as region mapping is not present",
					searchQuery.getSearchId(), supplierConf.getBasicInfo().getSupplierName());
			return;
		}

		if (Objects.isNull(searchQuery.getMiscInfo().getHotelWiseSearchInfo())) {
			CleartripSearchService searchService = CleartripSearchService.builder()
					.supplierConfig(this.getSupplierConf()).searchQuery(this.getSearchQuery())
					.sourceConfig(sourceConfigOutput).supplierRegionInfo(supplierRegionInfo).build();
			searchService.init();
			searchService.doSearch();
			searchResult = searchService.getSearchResult();
		} else {
			HotelInfo hInfo = HotelInfo.builder().build();
			searchHotel(hInfo, searchQuery);
			searchResult = HotelSearchResult.builder().build();
			searchResult.getHotelInfos().add(hInfo);
		}
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException {
		supplierRegionInfo = cacheHandler.getRegionInfoMappingFromRegionId(
				searchQuery.getSearchCriteria().getRegionId(), CleartripConstants.CLEARTRIP_SOURCE_NAME);
		CleartripSearchService searchService = CleartripSearchService.builder().sourceConfig(sourceConfigOutput)
				.searchQuery(this.getSearchQuery()).supplierConfig(this.getSupplierConf())
				.supplierRegionInfo(supplierRegionInfo).hotelInfo(hInfo).build();
		searchService.init();
		searchService.doDetailSearch();
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		boolean isCancellationPolicyAlreadySet = false;
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option cachedOption = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		for (Option op : hotel.getOptions()) {
			if (op.getId().equals(optionId) && !ObjectUtils.isEmpty(cachedOption.getCancellationPolicy())) {
				op.setCancellationPolicy(cachedOption.getCancellationPolicy());
				isCancellationPolicyAlreadySet = true;
			}
		}
		if (!isCancellationPolicyAlreadySet) {
			CleartripCancellationPolicyService cancellationPolicyService =
					CleartripCancellationPolicyService.builder().hInfo(hotel).searchQuery(searchQuery)
							.supplierConf(this.getSupplierConf()).sourceConfigOutput(sourceConfigOutput).build();
			cancellationPolicyService.searchCancellationPolicy(logKey);
		}
	}
}

