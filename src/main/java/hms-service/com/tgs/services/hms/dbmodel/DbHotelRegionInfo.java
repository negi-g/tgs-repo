package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.CreationTimestamp;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Getter
@Setter
@Table(name = "hotelregioninfo", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"regionName", "regionType", "countryName", "fullRegionName"})})
public class DbHotelRegionInfo extends BaseModel<DbHotelRegionInfo, HotelRegionInfo> {

	@SerializedName("name")
	private String regionName;
	@SerializedName("type")
	private String regionType;
	@SerializedName("fullName")
	private String fullRegionName;
	private String countryName;
	@CreationTimestamp
	private LocalDateTime createdOn;
	@CreationTimestamp
	private LocalDateTime processedOn;
	private Boolean enabled;
	private Long priority;
	private String iataCode;

	@Override
	public HotelRegionInfo toDomain() {
		return new GsonMapper<>(this, HotelRegionInfo.class).convert();
	}

	@Override
	public DbHotelRegionInfo from(HotelRegionInfo dataModel) {

		dataModel.setRegionName(dataModel.getRegionName().toUpperCase());
		dataModel.setCountryName(dataModel.getCountryName().toUpperCase());
		if (StringUtils.isBlank(dataModel.getFullRegionName()))
			dataModel.setFullRegionName("");
		dataModel.setFullRegionName(dataModel.getFullRegionName().toUpperCase());
		return new GsonMapper<>(dataModel, this, DbHotelRegionInfo.class).convert();
	}

}
