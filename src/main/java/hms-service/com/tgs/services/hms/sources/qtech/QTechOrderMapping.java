package com.tgs.services.hms.sources.qtech;

import lombok.Getter;

@Getter
public enum QTechOrderMapping {

	VOUCHERED("SUCCESS");
	
	public String getStatus() {
		return this.getCode();
	}

	private String code;

	QTechOrderMapping(String code) {
		this.code = code;
	}
	
}
