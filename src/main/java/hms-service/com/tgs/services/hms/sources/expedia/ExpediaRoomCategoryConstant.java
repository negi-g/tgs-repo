package com.tgs.services.hms.sources.expedia;

import lombok.Getter;

@Getter
public enum ExpediaRoomCategoryConstant {

	SINGLE("Single Bed"),
	DOUBLE("Double Bed"),
	TRIPLE("Triple Bed"),
	QUADRUPLE("Quadruple Bed"),
	QUINTUPLE("Quintuple Bed");
	
	private String roomType;
	
	private ExpediaRoomCategoryConstant(String roomType) {
		this.roomType = roomType;
	}
	
}
