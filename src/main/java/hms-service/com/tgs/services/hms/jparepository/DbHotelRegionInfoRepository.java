package com.tgs.services.hms.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfo;

@Repository
public interface DbHotelRegionInfoRepository
		extends JpaRepository<DbHotelRegionInfo, Long>, JpaSpecificationExecutor<DbHotelRegionInfo> {

	public DbHotelRegionInfo findById(Long id);

	public DbHotelRegionInfo findByRegionNameAndCountryNameAndRegionTypeAndFullRegionName(String regionName,
			String countryName, String regionType, String fullRegionName);
	
	public DbHotelRegionInfo findByRegionNameAndCountryNameAndRegionType(String regionName, String countryName,
			String regionType);

	@Query(value = "Select * from hotelregioninfo where id > :cursor and enabled = true order by id asc limit :limit",
			nativeQuery = true)
	public List<DbHotelRegionInfo> findByIdGreaterThanOrderByIdAscLimit(@Param("cursor") Long cursor,
			@Param("limit") Long limit);
	
}
