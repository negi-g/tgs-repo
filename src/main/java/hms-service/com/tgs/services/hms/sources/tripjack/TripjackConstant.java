package com.tgs.services.hms.sources.tripjack;

public enum TripjackConstant {

	STATIC_CITY_SUFFIX("/hms/v1/static-cities/"),
	STATIC_HOTEL_SUFFIX("/hms/v1/static-hotels/"),
	SEARCH_LIST("/hms/v1/hotel-searchquery-list"),
	SEARCH("/hms/v1/hotel-search"),
	DETAIL_SEARCH("/hms/v1/hotelDetail-search"),
	REVIEW("/hms/v1/hotel-review"),
	BOOKING("/oms/v1/hotel/book"),
	BOOKING_DETAIL("/oms/v1/hotel/booking-details"),
	CANCELLATION_POLICY("/hms/v1/hotel-cancellation-policy"),
	BOOKING_CANCELLATION("/oms/v1/hotel/cancel-booking/");

	public String value;

	TripjackConstant(String val) {
		this.value = val;
	}
}
