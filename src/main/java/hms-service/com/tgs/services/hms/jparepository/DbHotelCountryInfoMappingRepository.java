package com.tgs.services.hms.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.tgs.services.hms.dbmodel.DbHotelCountryInfoMapping;

@Repository
public interface DbHotelCountryInfoMappingRepository
		extends JpaRepository<DbHotelCountryInfoMapping, Long>, JpaSpecificationExecutor<DbHotelCountryInfoMapping> {

	public DbHotelCountryInfoMapping findByCountryIdAndSupplierName(String countryId, String supplierName);

	public DbHotelCountryInfoMapping findBySupplierCountryId(String supplierCountryId);
	
	public List<DbHotelCountryInfoMapping> findBySupplierName(String supplierName);
	
	public List<DbHotelCountryInfoMapping> findByCountryId(String countryId);

}

	
	

