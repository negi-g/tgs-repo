package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.cxf.common.util.CollectionUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelInfo.HotelInfoBuilder;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchCriteria;
import com.tgs.services.hms.datamodel.HotelSearchPreferences;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.Amenities;
import com.tgs.services.hms.datamodel.qtech.HotelAmenities;
import com.tgs.services.hms.datamodel.qtech.HotelDetailRequest;
import com.tgs.services.hms.datamodel.qtech.HotelDetailResponse;
import com.tgs.services.hms.datamodel.qtech.HotelImages;
import com.tgs.services.hms.datamodel.qtech.HotelProperty;
import com.tgs.services.hms.datamodel.qtech.HotelSearchRequest;
import com.tgs.services.hms.datamodel.qtech.HotelSearchResponse;
import com.tgs.services.hms.datamodel.qtech.QTechHotelInfo;
import com.tgs.services.hms.datamodel.qtech.RoomRates;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
public class QtechSearchService {

	private HotelSupplierRegionInfo supplierRegionInfo;

	private HotelSupplierConfiguration supplierConf;
	private HotelSearchQuery searchQuery;
	private HotelSourceConfigOutput sourceConfigOutput;

	private HotelSearchResponse searchResponse;
	private HotelSearchResult searchResult;
	private HotelDetailResponse detailResponse;
	private LocalDateTime currentTime;
	private Set<String> hotelids;

	private HotelInfo hInfo;

	protected RestAPIListener listener;

	private static final DateTimeFormatter dateTimeFormatter_DD_MM_YYYY = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public void doSearch(int threadCount) throws IOException {
		HttpUtils httpUtils = null;
		try {
			listener = new RestAPIListener("");
			searchResult = HotelSearchResult.builder().build();
			if (isValidRequest()) {
				HotelSearchRequest request = createSearchRequest();
				httpUtils = QTechUtil.getResponseURL(request, supplierConf, sourceConfigOutput);
				httpUtils.setPrintResponseLog(false);
				searchResponse = httpUtils.getResponse(HotelSearchResponse.class).orElse(null);
				currentTime = LocalDateTime.now();
				searchResult = createSearchResponse(searchResponse);
				log.info("Total number of hotels found after creating search response {} are {}",
						supplierConf.getHotelSupplierCredentials().getUserName() + ", "
								+ supplierConf.getBasicInfo().getSupplierName(),
						searchResult.getNoOfHotelOptions());
			} else {
				log.info("Unable to find region mapping for searchId {}, supplier {} and region {}",
						searchQuery.getSearchId(), this.supplierConf.getBasicInfo().getSupplierName(),
						this.searchQuery.getSearchCriteria().getCityName());
			}
		} finally {

			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.SEARCH.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(searchResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder()
						.supplierName(supplierConf.getHotelSupplierCredentials().getUserName().toUpperCase())
						.requestType(BaseHotelConstants.SEARCH).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId()).threadCount(threadCount)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private boolean isValidRequest() {
		return StringUtils.isNotBlank(supplierRegionInfo.getRegionId());
	}

	public void doDetailSearch() throws IOException {

		String qTechSearchId = hInfo.getOptions().get(0).getMiscInfo().getSupplierSearchId();
		HotelDetailRequest request = createDetailRequest(qTechSearchId, hInfo);
		HttpUtils httpUtils = null;
		try {
			listener = new RestAPIListener("");
			httpUtils = QTechUtil.getResponseURL(request, supplierConf, sourceConfigOutput);
			detailResponse = httpUtils.getResponse(HotelDetailResponse.class).orElse(null);
			createDetailResponse(detailResponse, hInfo);
		} finally {
			if (Objects.nonNull(httpUtils)) {
				SystemContextHolder.getContextData().addCheckPoint(
						CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
								.subType(HotelFlowType.DETAIL.name()).time(System.currentTimeMillis()).build());
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.DETAIL.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(detailResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder()
						.supplierName(supplierConf.getHotelSupplierCredentials().getUserName().toUpperCase())
						.requestType(BaseHotelConstants.DETAILSEARCH).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

			}
		}
	}

	private HotelSearchResult createSearchResponse(HotelSearchResponse hotelSearchResponse)
			throws JsonProcessingException {

		if (hotelSearchResponse == null)
			return searchResult;
		if (StringUtils.isNotEmpty(hotelSearchResponse.getSuccess())
				&& hotelSearchResponse.getSuccess().equals("fail")) {
			SystemContextHolder.getContextData().getErrorMessages().add(hotelSearchResponse.getMessage());
			log.error("Unable to get search result from QTech due to {} for supplier {} with username {}",
					hotelSearchResponse.getMessage(), this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName());
			return searchResult;
		}

		if (StringUtils.isNotEmpty(hotelSearchResponse.getMessage())
				&& hotelSearchResponse.getMessage().equals("fail")) {
			SystemContextHolder.getContextData().getErrorMessages().add(hotelSearchResponse.getMessageInfo());
			log.error("Unable to get search result from QTech due to {} for supplier {} with username {}",
					hotelSearchResponse.getMessageInfo(), this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName());
			return searchResult;
		}

		log.info("Total number of hotels found before creating search response for {} are {}",
				supplierConf.getBasicInfo().getSupplierName() + ", "
						+ supplierConf.getHotelSupplierCredentials().getUserName(),
				hotelSearchResponse.getTotalCount());

		String supplierSearchId = hotelSearchResponse.getSearchUniqueId();
		List<HotelInfo> hotels = new ArrayList<>();

		for (QTechHotelInfo qTechHotelInfo : hotelSearchResponse.getHotelList()) {
			try {
				// Populate missing hotelInfo from database / cache into HotelInfo object
				HotelInfoBuilder builder = HotelInfo.builder();

				HotelInfo propertyInfo = builder
						.address(Address.builder().addressLine1(qTechHotelInfo.getAddress()).build())
						.description(qTechHotelInfo.getShortDescription())
						.geolocation(GeoLocation.builder().latitude(qTechHotelInfo.getLatitude())
								.longitude(qTechHotelInfo.getLongitude()).build())
						.miscInfo(HotelMiscInfo.builder()
								.searchKeyExpiryTime(
										currentTime.plusMinutes(sourceConfigOutput.getSearchKeyExpirationTime()))
								.searchId(searchQuery.getSearchId())
								.supplierStaticHotelId(qTechHotelInfo.getLocalHotelId()).build())
						.unicaId(qTechHotelInfo.getMappingProviderCode()).name(qTechHotelInfo.getHotelName())
						.rating(NumberUtils.isParsable(qTechHotelInfo.getPropertyRating())
								? (int) Double.parseDouble(qTechHotelInfo.getPropertyRating())
								: null)
						.images(Arrays
								.asList(Image.builder()
								.thumbnail(qTechHotelInfo.getThumbNailUrl()).build()))
						.build();
				if (StringUtils.isBlank(qTechHotelInfo.getMappingProviderCode())) {
					log.debug("Unica id is missing for search id {}, hotelid {}, hotelName {}",
							searchQuery.getSearchId(), qTechHotelInfo.getLocalHotelId(), qTechHotelInfo.getHotelName());
				}
				List<Option> optionList = populateRoomInfo(qTechHotelInfo, supplierSearchId);
				if (!CollectionUtils.isEmpty(optionList)) {
					propertyInfo.setOptions(optionList);
					hotels.add(propertyInfo);
				}
			} catch (Exception e) {
				log.error("Unable to parse hotelInfo for hotel {} for search id {}", qTechHotelInfo,
						searchQuery.getSearchId(), e);
			}
		}
		searchResult.setHotelInfos(hotels);
		return searchResult;
	}

	private List<Option> populateRoomInfo(QTechHotelInfo hotelList, String supplierSearchId) {
		List<Option> options = new ArrayList<>();
		for (HotelProperty hotelProperty : hotelList.getHotelProperty()) {

			if (Objects.nonNull(hotelProperty.getRoomRates())) {
				Option option = getOption(hotelProperty.getRoomRates(), hotelProperty.getSectionUniqueId(),
						hotelList.getHotelId());
				if (Objects.nonNull(option)) {
					option.getMiscInfo()
							.setSecondarySupplier(StringUtils.isEmpty(hotelProperty.getSupplierName())
									? supplierConf.getBasicInfo().getSupplierName()
									: hotelProperty.getSupplierName());
					option.getMiscInfo().setSupplierSearchId(supplierSearchId);
					if (BooleanUtils.isTrue(sourceConfigOutput.getPopulateOptionOnDetail())) {
						option.getMiscInfo().setIsNotRequiredOnDetail(true);
					}
					options.add(option);
				}
			}
		}
		return options;
	}

	private Option getOption(List<RoomRates> roomRatesList, String sectionUniqueId, String supplierHotelId) {
		List<RoomInfo> roomInfos = new ArrayList<>();
		String currency = HotelBaseSupplierUtils.getSupplierCurrency(supplierConf);
		RoomInfo roomInfo = null;
		Option option = Option.builder().build();
		boolean isOptionOnRequest = false;
		for (RoomRates roomRates : roomRatesList) {
			int numberOfRooms = roomRates.getNumberOfRooms();
			roomInfo = new RoomInfo();
			roomInfo.setNumberOfAdults(roomRates.getNumberOfAdults());
			roomInfo.setNumberOfChild(roomRates.getNumberOfChild());
			roomInfo.setRoomCategory(roomRates.getRoomCategory());
			roomInfo.setRoomType(roomRates.getRoomType());
			roomInfo.setRoomAmenities(roomRates.getAmenities());
			roomInfo.setMealBasis(roomRates.getMealBasis().trim());
			roomInfo.setMiscInfo(RoomMiscInfo.builder().roomBookingId(roomRates.getClassUniqueId())
					.secondarySupplier(supplierConf.getBasicInfo().getSupplierId()).build());
			roomInfo.setId(QTechUtil.getRoomId(roomRates.getRoomType(), roomRates.getNumberOfAdults(),
					roomRates.getNumberOfChild()));
			if (!isOptionOnRequest && roomRates.getAvailable() == 0) {

				if (sourceConfigOutput.getIsOnRequestAllowed() != null && !sourceConfigOutput.getIsOnRequestAllowed())
					break;
				Integer hours = sourceConfigOutput.getOnRequestHours();
				if (hours != null
						&& Duration.between(LocalDateTime.now(), searchQuery.getCheckinDate().atTime(LocalTime.now()))
								.toHours() < hours)
					break;
				isOptionOnRequest = true;
			}
			QTechUtil.populatePriceInRoomInfo(roomInfo, roomRates.getRateBreakup(), currency);
			roomInfos.add(roomInfo);
			if (numberOfRooms > 1) {
				for (int r = 1; r < numberOfRooms; r++) {
					RoomInfo copyRoomInfo =
							GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(roomInfo), RoomInfo.class);
					roomInfos.add(copyRoomInfo);
				}
			}
		}
		if (roomInfos.size() != searchQuery.getRoomInfo().size()) {
			return null;
		}

		option.setMiscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierId())
				.hotelOptionId(sectionUniqueId).supplierHotelId(supplierHotelId)
				.sourceId(supplierConf.getBasicInfo().getSourceId()).build());
		HotelUtils.updateRoomIds(roomInfos);
		option.setRoomInfos(roomInfos);
		option.setIsOptionOnRequest(isOptionOnRequest);
		option.setId(sectionUniqueId);
		return option;
	}

	private HotelSearchRequest createSearchRequest() {
		HotelSearchRequest hotelSearchRequest = new HotelSearchRequest();
		hotelSearchRequest.setAction(QTechConstants.ACTION_S.getValue());
		HotelSearchCriteria hotelSearchCriteria = searchQuery.getSearchCriteria();
		HotelSearchPreferences hotelSearchPreferences = searchQuery.getSearchPreferences();
		List<RoomSearchInfo> hotelRoomInfo = searchQuery.getRoomInfo();
		Gson gson = new Gson();
		hotelSearchRequest.setCheckin_date(searchQuery.getCheckinDate().format(dateTimeFormatter_DD_MM_YYYY));
		hotelSearchRequest.setCheckout_date(searchQuery.getCheckoutDate().format(dateTimeFormatter_DD_MM_YYYY));
		hotelSearchRequest.setNumber_of_rooms(String.valueOf(hotelRoomInfo.size()));
		hotelSearchRequest.setTimeout(String.valueOf(sourceConfigOutput.getSearchRequestTimeOut()));

		if (hotelSearchPreferences.getRatings() != null) {
			StringBuilder hotelSearchRatingStr = new StringBuilder();
			List<Integer> hotelSearchRatingList = hotelSearchPreferences.getRatings();
			for (Integer hotelSearchRating : hotelSearchRatingList) {
				hotelSearchRatingStr.append(String.valueOf(hotelSearchRating) + ".0" + ",");
			}
			hotelSearchRequest.setChk_ratings(hotelSearchRatingStr.substring(0, hotelSearchRatingStr.length() - 1));
		} else
			hotelSearchRequest.setChk_ratings("1.0,2.0,3.0,4.0,5.0");
		if (hotelSearchPreferences.getLimitHotelRoomType() != null)
			hotelSearchRequest.setLimit_hotel_room_type(String.valueOf(hotelSearchPreferences.getLimitHotelRoomType()));
		else
			hotelSearchRequest.setLimit_hotel_room_type(String.valueOf(sourceConfigOutput.getRoomTypeListLimit()));
		if (hotelSearchPreferences.getCurrency() != null) {
			String currency = HotelBaseSupplierUtils.getSupplierCurrency(supplierConf);
			hotelSearchRequest.setSel_currency(currency);
		}
		if (hotelSearchPreferences.getAvailableonly() != null)
			hotelSearchRequest.setAvailableonly(hotelSearchPreferences.getAvailableonly() ? "1" : "0");
		else
			hotelSearchRequest.setAvailableonly(sourceConfigOutput.isAvailableOnly() ? "1" : "0");

		if (supplierRegionInfo != null && supplierRegionInfo.getRegionId() != null
				&& supplierRegionInfo.getCountryId() != null) {

			hotelSearchRequest.setSel_city(supplierRegionInfo.getRegionId());
			hotelSearchRequest.setSel_country(supplierRegionInfo.getCountryId());
			hotelSearchRequest.setCountry_of_residence(supplierRegionInfo.getCountryId());

		} else {
			if (hotelSearchCriteria.getRegionId() != null)
				hotelSearchRequest.setSel_city(hotelSearchCriteria.getRegionId());

			if (hotelSearchCriteria.getCountryId() != null) {
				hotelSearchRequest.setCountry_of_residence(hotelSearchCriteria.getCountryId());
				hotelSearchRequest.setSel_country(hotelSearchCriteria.getCountryId());
			}
		}
		if (Objects.nonNull(supplierConf.getHotelSupplierCredentials().getIsHotelidSearchEnabled())
				&& supplierConf.getHotelSupplierCredentials().getIsHotelidSearchEnabled() && Objects.nonNull(hotelids)
				&& !hotelids.isEmpty()) {
			String suphotelids = String.join(",", hotelids);
			hotelSearchRequest.setHotel_ids(suphotelids);
		}
		/*
		 * if (hotelSearchCriteria.getCity() != null) hotelSearchRequest.setSel_city(hotelSearchCriteria.getCity());
		 * 
		 * if (hotelSearchCriteria.getCountry() != null) {
		 * hotelSearchRequest.setCountry_of_residence(hotelSearchCriteria.getCountry());
		 * hotelSearchRequest.setSel_country(hotelSearchCriteria.getCountry()); }
		 * 
		 */

		if (hotelSearchCriteria.getNationality() != null) {
			hotelSearchRequest.setSel_nationality(hotelSearchCriteria.getNationality());
		}

		if (hotelSearchCriteria.getHotelName() != null)
			hotelSearchRequest.setSel_hotel(hotelSearchCriteria.getHotelName());
		else {
			hotelSearchRequest.setSel_hotel("");
		}
		hotelSearchRequest.setStatic_data(sourceConfigOutput.isStaticDataAllowed() ? "1" : "0");
		String hotelSearch = gson.toJson(searchQuery.getRoomInfo());

		/*
		 * To convert child age list into child string. For e.g. [1,2,3] into "1,2,3"
		 */
		hotelSearch = convertListIntoString(hotelSearch, "childAge");
		hotelSearchRequest.setRoomDetails(hotelSearch);
		return hotelSearchRequest;
	}

	private String convertListIntoString(String hotelSearch, String key) {
		JSONArray updatedArray = new JSONArray();
		JSONArray jsonArray = new JSONArray(hotelSearch);
		String childAge = "";
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			if (jsonObject.has("numberOfChild") && jsonObject.getInt("numberOfChild") != 0) {
				if (jsonObject.has(key)) {
					JSONArray childAgeArray = jsonObject.getJSONArray(key);
					StringBuilder childAgeStr = new StringBuilder();
					for (int j = 0; j < childAgeArray.length(); j++) {
						childAgeStr.append(childAgeArray.getInt(j) + ",");
					}
					childAge = childAgeStr.substring(0, childAgeStr.length() - 1);
					jsonObject.remove("childAge");
					jsonObject.put("ChildAge", childAge);
				}
			}
			updatedArray.put(jsonObject);
		}
		return updatedArray.toString();
	}

	private HotelDetailRequest createDetailRequest(String qTechSearchId, HotelInfo hInfo) {
		HotelDetailRequest request = new HotelDetailRequest();
		request.setAction(QTechConstants.ACTION_D.getValue());
		request.setHotel_id(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		request.setUnique_id(qTechSearchId);
		return request;
	}

	private void createDetailResponse(HotelDetailResponse detailResponse, HotelInfo hInfo) {
		if (StringUtils.isNotEmpty(detailResponse.getSuccess()) && detailResponse.getSuccess().equals("fail")) {
			SystemContextHolder.getContextData().getErrorMessages().add(detailResponse.getMessage());
			log.info(
					"Unable to get detail result from QTech due to {} for supplier {} with username {} for search id {}",
					detailResponse.getMessage(), this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName(), searchQuery.getSearchId());
		}
		List<String> hotelAmenitiesList = new ArrayList<>();
		List<Image> imageList = new ArrayList<>();
		List<Option> options = new ArrayList<>();
		Amenities amenities = detailResponse.getAmenities();
		if (amenities != null) {

			for (HotelAmenities hotelAmenities : amenities.getHotelAmenities()) {
				if (StringUtils.isNotBlank(hotelAmenities.getAmenityName())) {
					hotelAmenitiesList.add(hotelAmenities.getAmenityName());
				}
			}
		}
		if (!CollectionUtils.isEmpty(detailResponse.getHotelImages())) {
			for (HotelImages qTechImage : detailResponse.getHotelImages()) {
				if (StringUtils.isNotBlank(qTechImage.getBigUrl())
						|| StringUtils.isNotBlank(qTechImage.getThumbnailUrl())) {
					Image image = Image.builder().thumbnail(qTechImage.getThumbnailUrl()).bigURL(qTechImage.getBigUrl())
							.build();
					imageList.add(image);
				}
				if (imageList.size() > sourceConfigOutput.getImageListSize()) {
					break;
				}
			}
		}

		/*
		 * Temporary Logs
		 */
		if (!CollectionUtils.isEmpty(detailResponse.getSectionSelection())) {
			for (HotelProperty property : detailResponse.getSectionSelection()) {
				if (CollectionUtils.isEmpty(property.getRoomRates())) {
					log.info(
							"Unable to find rates in detail api for search id {}, supplier {} and supplier hotel id {}",
							searchQuery.getSearchId(), supplierConf.getHotelSupplierCredentials().getUserName(),
							hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
					break;
				} else {
					if (BooleanUtils.isTrue(sourceConfigOutput.getPopulateOptionOnDetail())) {
						Option option = getOption(property.getRoomRates(), property.getSectionUniqueId(),
								detailResponse.getHotelId());
						option.getMiscInfo()
								.setSecondarySupplier(StringUtils.isEmpty(property.getSupplierName())
										? supplierConf.getBasicInfo().getSupplierName()
										: property.getSupplierName());
						option.getMiscInfo()
								.setSupplierSearchId(hInfo.getOptions().get(0).getMiscInfo().getSupplierSearchId());
						options.add(option);
						hInfo.setOptions(options);
					}
				}
			}
		} else {
			log.info("Unable to find rates in detail api for search id {}, supplier {} and supplier hotel id {}",
					searchQuery.getSearchId(), supplierConf.getHotelSupplierCredentials().getUserName(),
					hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		}

		Contact con = Contact.builder().email(detailResponse.getEmail()).build();
		hInfo.getMiscInfo().setSupplierStaticHotelId(detailResponse.getHotelId());
		hInfo.setFacilities(hotelAmenitiesList).setImages(imageList).setDescription(detailResponse.getDescription())
				.setContact(con).getMiscInfo();
	}
}

