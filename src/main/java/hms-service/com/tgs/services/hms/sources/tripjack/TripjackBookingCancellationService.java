package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.tripjack.TJBookingDetailResponse;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Service
public class TripjackBookingCancellationService extends TripjackBaseService {

	private Order order;
	private HotelInfo hInfo;
	private BaseResponse cancellationResponse;
	private TJBookingDetailResponse bookingDetailResponse;

	public boolean cancelBooking() throws IOException {
		HttpUtils httpUtils = null;
		String requestUrl = StringUtils.join(endpoint, TripjackConstant.BOOKING_CANCELLATION.value);
		String supplierBookingId = hInfo.getMiscInfo().getSupplierBookingReference();
		try {
			listener = new RestAPIListener("");
			httpUtils = getHttpUtils(null, requestUrl + supplierBookingId);
			cancellationResponse = httpUtils.getResponse(BaseResponse.class).orElse(null);
			if (cancellationResponse.getStatus().getHttpStatus() == 200) {
				return getBookingCancellationStatus();
			}
		} finally {
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(cancellationResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRIPJACK.name())
					.requestType(BaseHotelConstants.BOOKING_CANCELLATION).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
		return false;
	}

	private boolean getBookingCancellationStatus() throws IOException {
		String supplierBookingId = hInfo.getMiscInfo().getSupplierBookingReference();
		HttpUtils httpUtils = null;
		try {
			String bookingDetailRequestUrl = StringUtils.join(endpoint, TripjackConstant.BOOKING_DETAIL.value);
			BookingDetailRequest bookingDetailRequest =
					BookingDetailRequest.builder().bookingId(supplierBookingId).build();
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(bookingDetailRequest), bookingDetailRequestUrl);
			bookingDetailResponse = httpUtils.getResponse(TJBookingDetailResponse.class).orElse(null);
			if (bookingDetailResponse.getOrder().getStatus().equals("CANCELLED")) {
				return true;
			}
		} finally {
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(cancellationResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRIPJACK.name())
					.requestType(BaseHotelConstants.BOOKING_CANCELLATION).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId()).prefix("Detail")
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
		return false;

	}
}

