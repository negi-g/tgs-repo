package com.tgs.services.hms.servicehandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.gson.FieldExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelDetailResult;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMissingInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.helper.analytics.HotelAnalyticsHelper;
import com.tgs.services.hms.manager.HotelDetailManager;
import com.tgs.services.hms.manager.HotelSearchManager;
import com.tgs.services.hms.manager.HotelSearchResultProcessingManager;
import com.tgs.services.hms.mapper.HotelMethodExecutionInfoMapper;
import com.tgs.services.hms.restmodel.HotelDetailRequest;
import com.tgs.services.hms.restmodel.HotelDetailResponse;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelDetailHandler extends ServiceHandler<HotelDetailRequest, HotelDetailResponse> {

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	HotelDetailManager detailManager;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelSearchManager searchManager;

	@Autowired
	HotelSearchResultProcessingManager processingManager;

	@Autowired
	GeneralServiceCommunicator gmsComm;

	@Autowired
	HotelAnalyticsHelper analyticsHelper;

	private void initContextData() {

		contextData.setMethodExecutionInfo(null);
		if (BooleanUtils.isFalse(request.getHitSupplier())) {
			contextData.setValue(BaseHotelConstants.METHOD_SUB_EXECUTION_TYPE, BaseHotelConstants.NO_SUPPLIER_HIT);
		} else {
			contextData.setValue(BaseHotelConstants.METHOD_SUB_EXECUTION_TYPE, BaseHotelConstants.SUPPLIER_HIT);
		}
		contextData.setValue(BaseHotelConstants.METHOD_EXECUTION_TYPE, BaseHotelConstants.MULTIPLE_SUPPLIER);
	}

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		HotelSearchQuery searchQuery = null;
		HotelInfo hInfo = null;
		String searchId = null;
		try {
			searchId = HotelUtils.getSearchId(request.getId());
			HotelSearchQuery oldSearchQuery = searchManager.getSearchQuery(searchId);

			boolean isSearchCompleted = cacheHandler.isSearchCompleted(searchId);
			if (isSearchCompleted) {
				oldSearchQuery.setSearchCompleted(true);
			}

			searchQuery = request.getSearchQuery();
			boolean isToFetchCompleteHotelInfo =
					BooleanUtils.isNotTrue(SystemContextHolder.getChannelType().equals(ChannelType.API))
							&& BooleanUtils.isTrue(request.getHitSupplier());
			if (searchQuery != null && !Objects.equals(oldSearchQuery, searchQuery)) {

				/*
				 * Modify Search Flow
				 */

				hInfo = cacheHandler.getHotelDetailsFromCacheWithRetry(request.getId(), searchQuery, false);
				if (ObjectUtils.isEmpty(hInfo)) {
					log.info("Hotel details not found in cache for hotel id {}", request.getId());
					throw new CustomGeneralException(SystemError.HOTEL_NOT_FOUND);
				}
				log.info("Doing search for same supplier for search id {} and hotel id {}", searchQuery.getSourceId(),
						hInfo.getId());
				HotelSearchManager.setSearchId(searchQuery);
				HotelUtils.populateMissingParametersInHotelSearchQuery(searchQuery);
				cacheHandler.storeSearchQueryInCache(searchQuery);
				hInfo = searchManager.doSearchAgainWithSameSuppliers(searchQuery, hInfo, contextData);
				searchQuery.setSearchCompleted(true);
			} else {
				searchQuery = oldSearchQuery;
				hInfo = cacheHandler.getHotelDetailsFromCacheWithRetry(request.getId(), searchQuery,
						isToFetchCompleteHotelInfo);
				if (ObjectUtils.isEmpty(hInfo)) {
					log.info("Hotel details not found in cache for hotel id {}", request.getId());
					throw new CustomGeneralException(SystemError.HOTEL_NOT_FOUND);
				}

				/**
				 * Search Expired Flow
				 * 
				 * 
				 * Keeping result for additional time to decrease response time. In future we can move 2 hour source
				 * type.
				 */
				if (hInfo != null && (!HotelUtils.allOptionDetailHit(hInfo) && hInfo.isExpired())
						|| (!ObjectUtils.isEmpty(hInfo.getMiscInfo().getSearchKeyExpiryTime()) && hInfo.getMiscInfo()
								.getSearchKeyExpiryTime().plusHours(2).isBefore(LocalDateTime.now()))) {
					log.info(
							"Doing new search for suppliers {} because of expiration of key for search id {} and hotel id {}",
							HotelUtils.getSupplierIdsFromHotelInfo(hInfo), searchQuery.getSearchId(), hInfo.getId());
					hInfo = searchManager.doSearchAgainWithSameSuppliers(searchQuery, hInfo, contextData);
				}
			}

			List<String> supplierIds = hInfo.getOptions().stream().map(option -> option.getMiscInfo().getSupplierId())
					.distinct().collect(Collectors.toList());
			searchQuery.setMiscInfo(HotelSearchQueryMiscInfo.builder().supplierIds(supplierIds).build());
			initContextData();

			if (!HotelUtils.allOptionDetailHit(hInfo)) {
				processAndFetchHotelDetails(searchQuery, hInfo, contextData);
			}

			SystemContextHolder.getContextData()
					.setExclusionStrategys(new ArrayList<>(Arrays.asList(new FieldExclusionStrategy(null,
							new ArrayList<>(Arrays.asList("rmi", "omi", "catid", "hwsi", "hoc"))))));
			updateForRetry(hInfo, searchQuery);
			response.setId(hInfo.getId());
			response.setSearchQuery(searchQuery);
			response.setHotel(hInfo);

		} catch (CustomGeneralException e) {
			log.info("Unable to fetch hotel detail for searchQuery {}, hInfo {} ", searchQuery, hInfo, e);
		} catch (Exception e) {
			log.error("Unable to fetch hotel detail for searchQuery {}, hInfo {} ", searchQuery, hInfo, e);
		} finally {
			if (response.getHotel() == null) {
				throw new CustomGeneralException(SystemError.HOTEL_NOT_FOUND);
			}
			SystemContextHolder.getContextData().getReqIds().add(searchId);
		}
	}

	private void processAndFetchHotelDetails(HotelSearchQuery searchQuery, HotelInfo hInfo, ContextData contextData)
			throws Exception {

		final Map<String, List<Option>> supplierOptionMap =
				hInfo.getOptions().stream().collect(Collectors.groupingBy(o -> o.getMiscInfo().getSupplierId()));
		log.info("Fetching hotel details from supplier {} for search id {} and hotel id {}", supplierOptionMap.keySet(),
				searchQuery.getSearchId(), hInfo.getId());
		Map<String, Option> optionIdOptionMap = HotelUtils.getOptionIdToOptionMap(hInfo);
		if (SystemContextHolder.getChannelType().equals(ChannelType.API)) {
			fetchAllOptionByHittingSuppliers(hInfo, searchQuery, supplierOptionMap, optionIdOptionMap, contextData);
			if (CollectionUtils.isNotEmpty(hInfo.getOptions())) {
				updateRoomMapping(searchQuery, hInfo);
				saveIntoCache(hInfo, searchQuery);
			}
		} else if (!searchQuery.isSearchCompleted() || BooleanUtils.isFalse(request.getHitSupplier())) {
			HotelInfo copyHInfo = new GsonMapper<>(hInfo, HotelInfo.class).convert();
			HotelSearchQuery copyQuery = new GsonMapper<>(searchQuery, HotelSearchQuery.class).convert();
			copyQuery.setSupplierIds(new ArrayList<>(supplierOptionMap.keySet()));
			ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
				ContextData fetchAllOptionContextData = contextData.deepCopy();
				fetchAllOptionContextData.setMethodExecutionInfo(null);
				fetchAllOptionContextData.setValue(BaseHotelConstants.METHOD_SUB_EXECUTION_TYPE,
						BaseHotelConstants.SUPPLIER_HIT);
				fetchAllOptionContextData.setValue(BaseHotelConstants.METHOD_EXECUTION_TYPE,
						BaseHotelConstants.MULTIPLE_SUPPLIER);
				SystemContextHolder.setContextData(fetchAllOptionContextData);
				final long allSupplierStartTime = System.currentTimeMillis();
				try {
					Boolean isAnyResultFetchedFromSupplier = fetchAllOptionByHittingSuppliers(copyHInfo, searchQuery,
							supplierOptionMap, optionIdOptionMap, fetchAllOptionContextData);
					if (CollectionUtils.isNotEmpty(hInfo.getOptions())) {
						HotelInfo finalHotelInfo =
								BooleanUtils.isTrue(isAnyResultFetchedFromSupplier) ? copyHInfo : hInfo;
						updateRoomMapping(searchQuery, finalHotelInfo);
						saveIntoCache(finalHotelInfo, searchQuery);
					}
				} catch (Exception e) {
					log.info("Unable to fetch and persist hotel detail info for searchid {} and hotelid {}",
							searchQuery.getSearchId(), hInfo.getId(), e);
				} finally {
					BaseHotelUtils.setMethodExecutionInfo("processAndFetchHotelDetails", searchQuery,
							Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
							System.currentTimeMillis() - allSupplierStartTime);
					sendMethodExecutionDataToAnalytics(hInfo, searchQuery, fetchAllOptionContextData);
				}
			});
			processHotelInfoWithoutHittingSuppliers(hInfo, searchQuery, supplierOptionMap);
		}
		log.info("Fetched hotel details from supplier for search id {} and hotel id {}", searchQuery.getSearchId(),
				hInfo.getId());
	}

	private void saveIntoCache(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		final long startTime = System.currentTimeMillis();
		try {
			for (Option allOption : hInfo.getOptions()) {
				allOption.getMiscInfo().setIsDetailHit(true);
			}
			cacheHandler.persistHotelInCache(hInfo, searchQuery);
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
					System.currentTimeMillis() - startTime);
		}
	}

	private void processHotelInfoWithoutHittingSuppliers(HotelInfo hInfo, HotelSearchQuery searchQuery,
			Map<String, List<Option>> supplierIdToAllOptionsMap) {

		long startTime = System.currentTimeMillis();
		try {
			log.info("Processing hotel details for search id {} and hotel id {} and suppliers {}",
					searchQuery.getSearchId(), hInfo.getId(), supplierIdToAllOptionsMap.keySet());
			doInternalProcessingWithoutHittingSupplier(hInfo, searchQuery, supplierIdToAllOptionsMap);
			HotelGeneralPurposeOutput configuratorInfo =
					HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
			if (BooleanUtils.isNotFalse(configuratorInfo.getRoomMappingInFirstDetailCall())) {
				updateRoomMapping(searchQuery, hInfo);
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
					System.currentTimeMillis() - startTime);
			sendMethodExecutionDataToAnalytics(hInfo, searchQuery, SystemContextHolder.getContextData());
		}
	}

	private void doInternalProcessingWithoutHittingSupplier(HotelInfo hInfo, HotelSearchQuery searchQuery,
			Map<String, List<Option>> supplierIdToAllOptionsMap) {
		long startTime = System.currentTimeMillis();
		try {
			for (String supplier : supplierIdToAllOptionsMap.keySet()) {
				HotelInfo copyHInfo = new GsonMapper<>(hInfo, HotelInfo.class).convert();
				copyHInfo.setOptions(supplierIdToAllOptionsMap.get(supplier));
				HotelSearchQuery copyQuery = HotelUtils.getHotelSearchQuery(searchQuery, copyHInfo);
				processingManager.updateRoomStaticData(copyHInfo, copyQuery);
			}
			processingManager.updateStaticData(hInfo, HotelUtils.getHotelSearchQuery(searchQuery, hInfo),
					OperationType.DETAIL_SEARCH);
			processingManager.processDetailResult(hInfo, searchQuery);
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
					System.currentTimeMillis() - startTime);
		}
	}

	private boolean fetchAllOptionByHittingSuppliers(HotelInfo hInfo, HotelSearchQuery searchQuery,
			Map<String, List<Option>> supplierOptionMap, Map<String, Option> optionIdToOptionMap,
			ContextData allSupplierContextData) throws Exception {
		List<HotelDetailResult> hotelDetailResults =
				fetchOptionByHittingAllSuppliers(hInfo, searchQuery, supplierOptionMap, optionIdToOptionMap);
		boolean isAnyResultFetchedFromSupplier = doInternalProcessingAfterHittingSupplier(hInfo, searchQuery,
				hotelDetailResults, optionIdToOptionMap, allSupplierContextData);
		return isAnyResultFetchedFromSupplier;
	}

	private boolean doInternalProcessingAfterHittingSupplier(HotelInfo hInfo, HotelSearchQuery searchQuery,
			List<HotelDetailResult> hotelDetailResults, Map<String, Option> optionIdToOptionMap,
			ContextData allSupplierContextData) {

		final long startTime = System.currentTimeMillis();
		boolean isAnyResultFetchedFromSupplier = false;
		try {

			for (HotelDetailResult hotelDetailResult : hotelDetailResults) {
				if (Objects.nonNull(hotelDetailResult)
						&& CollectionUtils.isNotEmpty(hotelDetailResult.getHotel().getOptions())
						&& BooleanUtils.isNotTrue(hotelDetailResult.getHotel().getOptions().get(0).getMiscInfo()
								.getIsNotRequiredOnDetail())) {
					HotelInfo copyHInfo = hotelDetailResult.getHotel();
					HotelSearchQuery copyQuery = hotelDetailResult.getSearchQuery();
					isAnyResultFetchedFromSupplier = true;
					for (Option option : copyHInfo.getOptions()) {
						option.setIsPassportMandatory(HotelUtils.isPassportMandatory(option, searchQuery));
						option.setIsPanRequired(HotelUtils.isPanRequired(hInfo, option, searchQuery));
					}
					updateMealMapping(copyQuery, copyHInfo);
					processingManager.updateRoomStaticData(copyHInfo, copyQuery);
					mergeOptions(hInfo, copyHInfo, copyQuery, optionIdToOptionMap);
				}
			}

			if (CollectionUtils.isNotEmpty(hInfo.getOptions())) {
				processingManager.updateStaticData(hInfo, HotelUtils.getHotelSearchQuery(searchQuery, hInfo),
						OperationType.DETAIL_SEARCH);
				processingManager.processDetailResult(hInfo, searchQuery);
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
					System.currentTimeMillis() - startTime);
		}
		return isAnyResultFetchedFromSupplier;
	}

	private List<HotelDetailResult> fetchOptionByHittingAllSuppliers(HotelInfo hInfo, HotelSearchQuery searchQuery,
			Map<String, List<Option>> supplierOptionMap, Map<String, Option> optionIdToOptionMap) {
		final long startTime = System.currentTimeMillis();
		List<HotelDetailResult> hotelDetailResults = new ArrayList<>();
		try {
			List<Future<?>> hotelDetailResultFutures = new ArrayList<>();
			for (String supplier : supplierOptionMap.keySet()) {
				Future<?> hotelDetailResultFuture = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
					HotelDetailResult hotelDetailResult = fetchOptionByHittingSingleSupplier(hInfo, searchQuery,
							supplierOptionMap, optionIdToOptionMap, supplier);
					return hotelDetailResult;
				});
				hotelDetailResultFutures.add(hotelDetailResultFuture);
			}
			for (Future<?> hotelDetailResultFuture : hotelDetailResultFutures) {
				try {
					HotelDetailResult hotelDetailResult = (HotelDetailResult) hotelDetailResultFuture.get();
					hotelDetailResults.add(hotelDetailResult);
				} catch (Exception e) {
					log.error("Error while fetching hotel detail for searchId {} , supplier {}",
							searchQuery.getSearchId(), hInfo.getOptions().get(0).getMiscInfo().getSupplierId(), e);
				}
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
					System.currentTimeMillis() - startTime);
		}
		return hotelDetailResults;
	}

	private HotelDetailResult fetchOptionByHittingSingleSupplier(HotelInfo hInfo, HotelSearchQuery searchQuery,
			Map<String, List<Option>> supplierOptionMap, Map<String, Option> optionIdToOptionMap, String supplier) {

		HotelInfo copyHInfo = null;
		HotelSearchQuery copyQuery = null;
		try {
			copyHInfo = new GsonMapper<>(hInfo, HotelInfo.class).convert();
			copyHInfo.setOptions(supplierOptionMap.get(supplier));
			copyQuery = HotelUtils.getHotelSearchQuery(searchQuery, copyHInfo);
			Option firstOption = copyHInfo.getOptions().get(0);
			copyHInfo.setOptions(new ArrayList<>(Arrays.asList(Option.builder().roomInfos(new ArrayList<>())
					.miscInfo(OptionMiscInfo.builder().supplierSearchId(firstOption.getMiscInfo().getSupplierSearchId())
							.supplierId(firstOption.getMiscInfo().getSupplierId())
							.supplierHotelId(firstOption.getMiscInfo().getSupplierHotelId())
							.resultIndex(firstOption.getMiscInfo().getResultIndex()).isNotRequiredOnDetail(true)
							.build())
					.build())));
			detailManager.fetchHotelDetails(searchQuery, copyHInfo, contextData);
		} catch (Exception e) {
			log.error("Error while fetching hotel detail for searchId {} , supplier {}", searchQuery.getSearchId(),
					hInfo.getOptions().get(0).getMiscInfo().getSupplierId(), e);
		}
		return HotelDetailResult.builder().hotel(copyHInfo).searchQuery(copyQuery).build();
	}

	public void updateMealMapping(HotelSearchQuery searchQuery, HotelInfo hInfo) {
		Set<String> supplierMealInfoIdList = new HashSet<>();
		if (StringUtils.isNotBlank(hInfo.getMiscInfo().getSupplierStaticHotelId())) {
			hInfo.getOptions().forEach(option -> {
				option.getRoomInfos().forEach(roomInfo -> {
					if (StringUtils.isNotBlank(roomInfo.getMealBasis())) {
						supplierMealInfoIdList.add(roomInfo.getMealBasis());
					}
				});
			});
		}
		Map<String, String> hotelStaticMealInfoMap =
				processingManager.fetchMealInfoData(Arrays.asList(hInfo), searchQuery, supplierMealInfoIdList);
		processingManager.mapHotelMealBasis(hotelStaticMealInfoMap, hInfo, searchQuery, new HotelMissingInfo());
	}

	private void mergeOptions(HotelInfo hInfo, HotelInfo copyHInfo, HotelSearchQuery searchQuery,
			Map<String, Option> map) {

		synchronized (this) {
			removeExtraOptions(hInfo, map, searchQuery);
			removeExtraOptions(copyHInfo, map, searchQuery);
		}

		if (!CollectionUtils.isEmpty(copyHInfo.getImages())) {
			if (CollectionUtils.isEmpty(hInfo.getImages()) || (!CollectionUtils.isEmpty(hInfo.getImages())
					&& copyHInfo.getImages().size() > hInfo.getImages().size())) {
				hInfo.setImages(copyHInfo.getImages());
			}
		}

		if (!CollectionUtils.isEmpty(copyHInfo.getFacilities())) {
			if (CollectionUtils.isEmpty(hInfo.getFacilities()) || (!CollectionUtils.isEmpty(hInfo.getFacilities())
					&& copyHInfo.getFacilities().size() > hInfo.getFacilities().size())) {
				hInfo.setFacilities(copyHInfo.getFacilities());
			}
		}

		if (!StringUtils.isEmpty(copyHInfo.getDescription())) {
			hInfo.setDescription(copyHInfo.getDescription());
		}

		if (CollectionUtils.isNotEmpty(copyHInfo.getOptions())) {
			for (Option option : copyHInfo.getOptions()) {
				if (CollectionUtils.isNotEmpty(option.getRoomInfos())) {
					hInfo.getOptions().add(option);
				}
			}
		}
	}

	public void removeExtraOptions(HotelInfo hInfo, Map<String, Option> map, HotelSearchQuery searchQuery) {

		for (Iterator<Option> optionIterator = hInfo.getOptions().iterator(); optionIterator.hasNext();) {
			Option option = optionIterator.next();
			String optionSupplierId = option.getMiscInfo().getSupplierId();
			if (BooleanUtils.isTrue(option.getMiscInfo().getIsNotRequiredOnDetail())
					&& optionSupplierId.equals(searchQuery.getMiscInfo().getSupplierId())) {
				optionIterator.remove();
				map.remove(option.getId());
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {
		SystemContextHolder.getContextData().setExclusionStrategys(new ArrayList<>(Arrays.asList(
				new FieldExclusionStrategy(null, Arrays.asList(/* "cnp", */ "links", "catid", "hwsi", "hoc")))));
	}

	private void updateRoomMapping(HotelSearchQuery searchQuery, HotelInfo hInfo) {
		long startTime = System.currentTimeMillis();
		try {
			ClientGeneralInfo clientInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
			if (BooleanUtils.isTrue(clientInfo.getIsRoomMappingEnabled())) {
				HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();
				Map<String, String> roomMappingInfo = staticDataService.getRoomMappingInfo(searchQuery, hInfo);
				if (MapUtils.isNotEmpty(roomMappingInfo)) {
					for (Option option : hInfo.getOptions()) {
						String key = "";
						for (RoomInfo roomInfo : option.getRoomInfos()) {
							String standardRoomName = roomMappingInfo.get(roomInfo.getId());
							if (StringUtils.isNotBlank(standardRoomName)) {
								roomInfo.setStandardRoomName(standardRoomName);
								key = StringUtils.join(key, HotelUtils.getKey(standardRoomName));
							}
						}
						if (StringUtils.isNotBlank(key)) {
							option.setOptionMappingId(key);
						}
					}
				}
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
					System.currentTimeMillis() - startTime);
		}
	}

	private void updateForRetry(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		LocalDateTime searchStartAt = cacheHandler.getSearchStartAt(searchQuery.getSearchId());
		LocalDateTime currentTime = LocalDateTime.now();
		if ((!searchQuery.isSearchCompleted() && searchStartAt.plusSeconds(30).isAfter(currentTime))
				|| BooleanUtils.isFalse(request.getHitSupplier())) {
			response.setRetryInSecond(1);
		}
	}

	private void sendMethodExecutionDataToAnalytics(HotelInfo hotelInfo, HotelSearchQuery searchQuery,
			ContextData contextData) {

		if (Objects.nonNull(hotelInfo) && MapUtils.isNotEmpty(contextData.getMethodExecutionInfo())) {
			ContextData methodContextData = BaseHotelUtils.getNewContextDataForMethodExecutionInfo(contextData);
			HotelMethodExecutionInfoMapper methodExecutionMapper = HotelMethodExecutionInfoMapper.builder()
					.contextData(methodContextData).user(methodContextData.getUser()).searchQuery(searchQuery)
					.flowType(HotelFlowType.DETAIL).build();
			analyticsHelper.storeMethodData(methodExecutionMapper);
			log.info("Method execution info for search id {} and hotel id {} is {}", searchQuery.getSearchId(),
					request.getId(), GsonUtils.getGson().toJson(methodContextData.getMethodExecutionInfo()));
		} else {
			log.info(
					"Unable to store method execution info as not result found for for search id {} and supplier name {}",
					searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId());
		}
	}
}
