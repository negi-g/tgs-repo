package com.tgs.services.hms.sources.tbo;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.mapping.CityInfoRequest;
import com.tgs.services.hms.datamodel.tbo.mapping.CityInfoResponse;
import com.tgs.services.hms.datamodel.tbo.mapping.Country;
import com.tgs.services.hms.datamodel.tbo.mapping.CountryInfoRequest;
import com.tgs.services.hms.datamodel.tbo.mapping.CountryInfoResponse;
import com.tgs.services.hms.datamodel.tbo.mapping.Destination;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@SuperBuilder
public class TravelBoutiqueCitySaveService extends TravelBoutiqueBaseService {

	private HotelStaticDataRequest staticDataRequest;

	private HotelSupplierConfiguration supplierConf;

	private HotelSupplierRegionInfoManager supplierRegionInfoManager;

	private HotelRegionInfoMappingManager regionInfoMappingManager;

	public List<HotelRegionInfoQuery> getTravelBoutiqueRegions() {

		supplierConf = HotelSupplierConfigurationHelper.getSupplierConfiguration("TBO");
		HttpUtils httpUtils = null;
		List<HotelRegionInfoQuery> regionInfos = new ArrayList<>();
		try {
			String tokenId = getCachedToken();
			CountryInfoRequest searchRequest = createSearchRequest(tokenId);
			httpUtils = TravelBoutiqueUtil.getRequest(searchRequest, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.COUNTRY_INFO));
			CountryInfoResponse searchResponse = httpUtils.getResponse(CountryInfoResponse.class).orElse(null);
			String xmlResponse = searchResponse.getCountryList();
			List<Country> countryList = TravelBoutiqueUtil.getCountryListFromXML(xmlResponse);

			for (Country country : countryList) {

				CityInfoRequest cityInfoRequest = createCityInfoSearchRequest(tokenId, country.getCode());
				httpUtils = TravelBoutiqueUtil.getRequest(cityInfoRequest, supplierConf,
						supplierConf.getHotelAPIUrl(HotelUrlConstants.CITY_INFO));
				CityInfoResponse response = httpUtils.getResponse(CityInfoResponse.class).orElse(null);
				if (CollectionUtils.isEmpty(response.getDestinations())) {

					log.error("No Region Found For Country {} , name {}", country.getCode(), country.getName());
					continue;
				}
				for (Destination region : response.getDestinations()) {

					HotelRegionInfoQuery regionInfoQuery = new HotelRegionInfoQuery();
					regionInfoQuery.setRegionId(String.valueOf(region.getDestinationId()));
					regionInfoQuery.setStateName(region.getStateProvince());
					regionInfoQuery.setRegionName(region.getCityName());
					regionInfoQuery.setRegionType("CITY");
					regionInfoQuery.setCountryName(region.getCountryName());
					regionInfoQuery.setCountryId(region.getCountryCode());
					regionInfoQuery.setSupplierName(HotelSourceType.TBO.name());
					regionInfos.add(regionInfoQuery);
				}
			}
		} catch (Exception e) {
			log.error("error while fetching regions {}", e);
		} finally {
			log.info("total number of region info fetched are {} ", regionInfos.size());
		}
		return regionInfos;
	}


	public CountryInfoRequest createSearchRequest(String tokenId) throws Exception {

		CountryInfoRequest request = new CountryInfoRequest();
		request.setClientId(supplierConf.getHotelSupplierCredentials().getClientId());
		request.setEndUserIp(TravelBoutiqueUtil.IP);
		request.setTokenId(tokenId);
		return request;
	}

	public CityInfoRequest createCityInfoSearchRequest(String tokenId, String countryCode) throws Exception {

		CityInfoRequest request = new CityInfoRequest();
		request.setClientId(supplierConf.getHotelSupplierCredentials().getClientId());
		request.setEndUserIp(TravelBoutiqueUtil.IP);
		request.setTokenId(tokenId);
		request.setCountryCode(countryCode);
		return request;
	}
}
