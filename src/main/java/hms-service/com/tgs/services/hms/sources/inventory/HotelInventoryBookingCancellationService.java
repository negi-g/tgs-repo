package com.tgs.services.hms.sources.inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import com.tgs.filters.HotelRatePlanFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryAllocationInfo;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.manager.HotelRatePlanManager;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
public class HotelInventoryBookingCancellationService {
	
	private HotelInfo hInfo;
	private HotelRatePlanManager ratePlanManager;
	
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public boolean cancelBooking() {
		
		Option option = hInfo.getOptions().get(0);
		List<Long> ratePlanIdList = getRatePlanIdListFromOption(option);
		HotelRatePlanFilter ratePlanFilter = HotelRatePlanFilter.builder().ids(ratePlanIdList).build();
		List<HotelRatePlan> ratePlanList = ratePlanManager.fetchHotelRatePlan(ratePlanFilter);
		
		if(CollectionUtils.isEmpty(ratePlanList)) 
			throw new CustomGeneralException(SystemError.RATE_PLAN_NOT_FOUND);
		
		Map<Long, HotelRatePlan> ratePlanIdAsKeyRatePlanValue = ratePlanList.stream().collect(Collectors
				.toMap(HotelRatePlan :: getId, Function.identity(), (a1, a2) -> a1));
		Map<Long,Long> ratePlanIdAsKeyCountAsValue = ratePlanIdList.stream().collect(Collectors
				.groupingBy(Function.identity(), Collectors.counting()));
		
		List<HotelRatePlan> updatedRatePlanList = new ArrayList<>();
		
		for(Long key : ratePlanIdAsKeyRatePlanValue.keySet()) {
			HotelRatePlan ratePlan = ratePlanIdAsKeyRatePlanValue.get(key);
			Integer inventoryToBeCanceled = ratePlanIdAsKeyCountAsValue.get(key).intValue();
			HotelInventoryAllocationInfo inventoryInfo = ratePlan.getInventoryAllocationInfo();
			if(ObjectUtils.isEmpty(inventoryInfo)) 
				throw new CustomGeneralException(SystemError.INVENTORY_INFO_NOT_FOUND);
			Integer soldInventory = inventoryInfo.getSoldInventory();
			soldInventory -= inventoryToBeCanceled;
			ratePlan.getInventoryAllocationInfo().setSoldInventory(soldInventory);
			updatedRatePlanList.add(ratePlan);
		}
		
		ratePlanManager.storeRatePlanList(updatedRatePlanList);
		return true;
		
	}

	private List<Long> getRatePlanIdListFromOption(Option option) {
		
		List<Long> ratePlanIdList = new ArrayList<>();
		option.getRoomInfos().forEach((room -> {
			ratePlanIdList.addAll(room.getMiscInfo().getRatePlanIdList());
		}));
		return ratePlanIdList;
	}

}
