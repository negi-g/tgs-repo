package com.tgs.services.hms.sources.fitruums;

import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;

@Service
public class FitruumsPriceValidationFactory extends AbstractHotelPriceValidationFactory {

	public FitruumsPriceValidationFactory(HotelSearchQuery query, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
		super(query, hotel, hotelSupplierConf, bookingId);
		
	}

	@Override
	public Option getOptionWithUpdatedPrice() throws Exception {

		FitruumsPriceValidationService priceValidationService = FitruumsPriceValidationService.builder()
				.searchQuery(this.searchQuery).bookingId(bookingId).sourceConfig(sourceConfigOutput)
				.supplierConf(this.getSupplierConf()).build();
		priceValidationService.validate(this.getHInfo());
		return priceValidationService.getUpdatedOption();
		
	}

}
