package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelBedsStaticDataFactory extends AbstractStaticDataInfoFactory {

	@Autowired
	private HotelRegionInfoMappingManager regionInfoMappingManager;

	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	private GeneralServiceCommunicator generalServiceCommunicator;
	
	@Autowired
	private HotelSupplierRegionInfoManager supplierRegionInfoManager;
	
	public HotelBedsStaticDataFactory(HotelSupplierConfiguration supplierConf,
			HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getHotelStaticData() throws IOException {

		HotelBedsHotelStaticDataService hotelBedStaticDataService = HotelBedsHotelStaticDataService.builder()
				.hotelInfoSaveManager(hotelInfoSaveManager).generalServiceCommunicator(generalServiceCommunicator)
				.supplierConf(supplierConf).sourceConfig(sourceConfigOutput).build();
		hotelBedStaticDataService.init();
		hotelBedStaticDataService.process();
	}

	@Override
	protected void getCityMappingData() throws IOException {

		HotelBedsCityStaticDataService staticCityService = HotelBedsCityStaticDataService.builder()
				.sourceConfig(sourceConfigOutput)
				.supplierConf(supplierConf).regionInfoMappingManager(regionInfoMappingManager)
				.supplierRegionInfoManager(supplierRegionInfoManager)
				.staticDataRequest(staticDataRequest).build();
		staticCityService.saveHotelBedsCityList();

	}

	@Override
	protected Map<String, String> getRoomMappingData(HotelSearchQuery searchQuery, HotelInfo hotelInfo) {

		return null;
	}

	@Override
	protected void getNationalityMappingData() throws IOException {
		// TODO Auto-generated method stub

	}
}
