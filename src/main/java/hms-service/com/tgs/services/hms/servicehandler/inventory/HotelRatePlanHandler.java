package com.tgs.services.hms.servicehandler.inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.HotelRatePlanFilter;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryAllocationInfo;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlanInfo;
import com.tgs.services.hms.dbmodel.inventory.DbHotelRatePlan;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.jparepository.inventory.HotelRatePlanService;
import com.tgs.services.hms.manager.HotelRatePlanManager;
import com.tgs.services.hms.restmodel.inventory.HotelRatePlanRequest;
import com.tgs.services.hms.restmodel.inventory.HotelRatePlanResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelRatePlanHandler extends ServiceHandler<HotelRatePlanRequest, HotelRatePlanResponse> {

	@Autowired
	private HotelRatePlanService ratePlanService;

	@Autowired
	private HotelCacheHandler cacheHandler;
	
	@Autowired
	private HotelRatePlanManager ratePlanManager;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		List<DbHotelRatePlan> dbRatePlans = new ArrayList<>();
		for (HotelRatePlan ratePlan : request.getRatePlans()) {

			ratePlan.cleanData();
			if (Objects.nonNull(ratePlan.getId())) {
				DbHotelRatePlan oldDBRatePlan = ratePlanService.findById(ratePlan.getId());
				if (Objects.isNull(oldDBRatePlan)) {
					log.info("Unable to update rate plan for {} as the id passed is invalid", ratePlan.getId());
					response.addError(
							new ErrorDetail(SystemError.RESOURCE_NOT_FOUND, "Id missing is " + ratePlan.getId()));
					return;
				} else {
					DbHotelRatePlan newDBRatePlan = new DbHotelRatePlan().from(ratePlan);
					updateHotelRatePlanInfo(newDBRatePlan, oldDBRatePlan);
					dbRatePlans.add(oldDBRatePlan);
				}
			} else {
				DbHotelRatePlan newDBRatePlan = new DbHotelRatePlan().from(ratePlan);
				DbHotelRatePlan oldDBRatePlan = ratePlanService.findUniqueRatePlan(ratePlan);
				if (Objects.nonNull(oldDBRatePlan)) {
					updateHotelRatePlanInfo(newDBRatePlan, oldDBRatePlan);
					dbRatePlans.add(oldDBRatePlan);
				} else {
					dbRatePlans.add(newDBRatePlan);
				}
			}
		}
		List<HotelRatePlan> ratePlanList = ratePlanService.saveRatePlans(dbRatePlans);
		cacheHandler.updateInventoryRatePlans(ratePlanList);
		response.getRatePlans().addAll(ratePlanList);
	}

	public BaseResponse deleteRatePlan(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbHotelRatePlan dbRatePlan = ratePlanService.findById(id);
		if (Objects.nonNull(dbRatePlan)) {
			dbRatePlan.setDeleted(true);
			ratePlanService.save(dbRatePlan);
			cacheHandler.deleteRatePlanFromCache(dbRatePlan.toDomain());
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	private static void updateHotelRatePlanInfo(DbHotelRatePlan newRatePlan, DbHotelRatePlan oldRatePlan) {

		oldRatePlan = new GsonMapper<>(newRatePlan, oldRatePlan, DbHotelRatePlan.class).convert();
		HotelInventoryAllocationInfo newInventoryAllocationInfo = newRatePlan.getInventoryAllocationInfo();
		HotelInventoryAllocationInfo oldInventoryAllocationInfo = oldRatePlan.getInventoryAllocationInfo();

		if (Objects.nonNull(newInventoryAllocationInfo) && Objects.nonNull(oldInventoryAllocationInfo)) {

			if (Objects.isNull(newInventoryAllocationInfo.getSoldInventory())
					&& Objects.nonNull(oldInventoryAllocationInfo.getSoldInventory())) {
				oldInventoryAllocationInfo.setSoldInventory(null);
			}

			if (Objects.isNull(newInventoryAllocationInfo.getTotalInventory())
					&& Objects.nonNull(oldInventoryAllocationInfo.getTotalInventory())) {
				oldInventoryAllocationInfo.setTotalInventory(null);
			}
		}

		HotelRatePlanInfo oldRatePlanInfo = oldRatePlan.getRatePlanInfo();
		HotelRatePlanInfo newRatePlanInfo = newRatePlan.getRatePlanInfo();

		if (Objects.nonNull(oldRatePlanInfo) && Objects.nonNull(newRatePlanInfo)) {

			if (Objects.isNull(newRatePlanInfo.getMinStay()) && Objects.nonNull(oldRatePlanInfo.getMinStay())) {
				oldRatePlanInfo.setMinStay(null);
			}
			if (Objects.isNull(newRatePlanInfo.getReleasePeriod())
					&& Objects.nonNull(oldRatePlanInfo.getReleasePeriod())) {
				oldRatePlanInfo.setReleasePeriod(null);
			}
			if (Objects.isNull(newRatePlanInfo.getExcludeNationality())
					&& Objects.nonNull(oldRatePlanInfo.getReleasePeriod())) {
				oldRatePlanInfo.setExcludeNationality(null);
			}
			if (Objects.isNull(newRatePlanInfo.getExtraBedRates())
					&& Objects.nonNull(oldRatePlanInfo.getExtraBedRates())) {
				oldRatePlanInfo.setExtraBedRates(null);
			}
			if (Objects.isNull(newRatePlanInfo.getMarketProfile())
					&& Objects.nonNull(oldRatePlanInfo.getMarketProfile())) {
				oldRatePlanInfo.setMarketProfile(null);
			}
		}
	}

	public HotelRatePlanResponse fetchHotelRatePlan(HotelRatePlanFilter ratePlanFilter) {
		
		HotelRatePlanResponse ratePlanResponse = new HotelRatePlanResponse();
		List<HotelRatePlan> ratePlans = ratePlanManager.fetchHotelRatePlan(ratePlanFilter);
		ratePlanResponse.setRatePlans(ratePlans);
		return ratePlanResponse;
		
	}

	@Override
	public void afterProcess() throws Exception {

	}
}
