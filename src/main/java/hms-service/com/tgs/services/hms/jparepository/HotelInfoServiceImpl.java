package com.tgs.services.hms.jparepository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.dbmodel.DbHotelMealBasis;
import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;
import com.tgs.services.hms.dbmodel.DbSupplierHotelIdMapping;

@Service
public class HotelInfoServiceImpl implements HotelInfoService {

	@Autowired
	DbHotelInfoRepository hotelRepository;

	@Autowired
	DbHotelSupplierMappingRepository hotelSupplierMappingRepository;

	@Autowired
	DbHotelMealMappingRepository hotelMealMappingRepository;

	@Autowired
	DbHotelSupplierRepository supplierRepository;

	@Autowired
	DbSupplierHotelIdMappingRepository hotelIdMappingRepository;


	@Autowired
	HotelCriteriaNoCountDAO hotelCriteriaNoCountDAO;

	@Override
	public DbHotelInfo save(DbHotelInfo hotel) {
		return hotelRepository.save(hotel);
	}

	@Override
	public DbSupplierHotelIdMapping save(DbSupplierHotelIdMapping hotelstaticData) {
		return hotelIdMappingRepository.save(hotelstaticData);
	}

	@Override
	public DbHotelSupplierMapping save(DbHotelSupplierMapping supplier) {
		return supplierRepository.save(supplier);
	}

	@Override
	public DbHotelInfo findBySupplierHotelIdAndSupplierNameAndUnicaId(String supplierHotelId, String supplierName,
			String unicaId) {
		return hotelRepository.findBySupplierHotelIdAndSupplierNameAndUnicaId(supplierHotelId, supplierName, unicaId);
	}

	@Override
	public DbHotelInfo findByNameAndRatingAndCityNameAndCountryName(String name, String rating, String cityName,
			String countryName) {
		return hotelRepository.findByNameAndRatingAndCityNameAndCountryName(name.toUpperCase(), rating,
				cityName.toUpperCase(), countryName.toUpperCase());
	}

	@Override
	public List<DbHotelInfo> findAllHotels(Pageable pageable) {
		List<DbHotelInfo> hotelInfoList = new ArrayList<>();
		hotelCriteriaNoCountDAO.findAll(pageable, DbHotelInfo.class).forEach(hotelInfo -> hotelInfoList.add(hotelInfo));
		return hotelInfoList;
	}

	@Override
	public List<DbHotelSupplierMapping> findAllHotelSupplierMapping(Pageable pageable) {
		List<DbHotelSupplierMapping> mappingList = new ArrayList<>();
		hotelCriteriaNoCountDAO.findAll(pageable, DbHotelSupplierMapping.class)
				.forEach(mapping -> mappingList.add(mapping));
		return mappingList;
	}

	@Override
	public List<DbHotelInfo> findAllByOrderByIdAsc() {
		return hotelRepository.findAllByOrderByIdAsc();
	}

	@Override
	public DbHotelSupplierMapping findFirstBySupplierNameOrderByHotelIdDesc(String supplierName) {
		return hotelSupplierMappingRepository.findFirstBySupplierNameOrderByHotelIdDesc(supplierName);
	}

	@Override
	public List<DbHotelInfo> findByIdGreaterThanOrderByIdAsc(Long id) {
		return hotelRepository.findByIdGreaterThanOrderByIdAsc(id);
	}

	@Override

	public List<DbHotelInfo> findUnmappedhotels(String supplierName) {
		return hotelRepository.findHotelsRightJoinHotelSupplierMapping(supplierName);
	}

	@Override
	public DbHotelInfo findById(Long id) {
		return hotelRepository.findById(id);
	}

	@Override
	public DbHotelSupplierMapping findMapping(String supplierName, Long hotelId) {
		return hotelSupplierMappingRepository.findBySupplierNameAndHotelId(supplierName, hotelId);
	}

	@Override
	public DbHotelMealBasis findBySMealBasisAndSupplier(String smealBasis, String supplier) {
		return hotelMealMappingRepository.findBySMealBasisAndSupplier(smealBasis, supplier);
	}

	@Override
	public DbHotelMealBasis save(DbHotelMealBasis mealBasis) {
		return hotelMealMappingRepository.save(mealBasis);
	}

	@Override
	public List<DbHotelMealBasis> findAllHotelMeals(Pageable pageable) {
		List<DbHotelMealBasis> hotelMealBasisList = new ArrayList<>();
		hotelMealMappingRepository.findAll(pageable).forEach(hotelMealBasis -> hotelMealBasisList.add(hotelMealBasis));
		return hotelMealBasisList;
	}

	@Override
	public List<DbHotelInfo> findByIdGreaterThanOrderByIdAscLimit(Long limit, Long cursor) {
		return hotelRepository.findByIdGreaterThanOrderByIdAscLimit(cursor, limit);
	}

	@Override
	public List<DbHotelInfo> findByIdBetween(Long from, Long to) {
		return hotelRepository.findByIdBetween(from, to);
	}

	@Override
	public Object[] findMinAndMaxIdHotelInfo() {
		return hotelRepository.findMinAndMaxId();
	}

	@Override
	public List<Object[]> findHotelNameIdAddress(Pageable pageable) {
		return hotelRepository.findHotelNameIdAddress(pageable);
	}

	@Override
	public List<DbHotelSupplierMapping> findAllHotelSupplierMappingBySource(Pageable pageable, String sourceName) {
		return hotelSupplierMappingRepository.findAllBySourceName(pageable, sourceName);
	}

	public List<DbHotelSupplierMapping> findAllBySupplierNameAndSourceName(String supplierName, String sourceName) {
		return hotelSupplierMappingRepository.findAllBySupplierNameAndSourceName(supplierName, sourceName);
	}

	@Override
	public List<Object[]> findHotelBasicInfoByIdIn(List<Long> idList) {
		return hotelRepository.findNameRatingAddressByIdIn(idList);
	}

	@Override
	public DbSupplierHotelIdMapping findByHotelId(String hotelid) {
		return hotelIdMappingRepository.findByHotelId(hotelid);

	}

	@Override
	public List<DbSupplierHotelIdMapping> findAll(Pageable pageable) {
		Page<DbSupplierHotelIdMapping> page = hotelIdMappingRepository.findAll(pageable);
		return page.getContent();
	}

	@Override
	public List<DbSupplierHotelIdMapping> findByhotelIdBetween(Long from, Long to) {
		return hotelIdMappingRepository.findByIdBetween(from, to);
	}


}
