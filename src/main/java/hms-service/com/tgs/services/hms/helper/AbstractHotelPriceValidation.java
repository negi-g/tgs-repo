package com.tgs.services.hms.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.datamodel.FareChangeAlert;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.manager.HotelSearchResultProcessingManager;
import com.tgs.services.hms.manager.HotelUserFeeManager;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.AmountType;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AbstractHotelPriceValidation {

	@Autowired
	HotelSearchResultProcessingManager searchResultProcessingManager;

	@Autowired
	HotelUserFeeManager userFeeManager;

	@Autowired
	private CommercialCommunicator commericialCommunicator;

	public FareChangeAlert updatePriceIfChanged(HotelSearchQuery searchQuery, HotelInfo hInfo,
			boolean isToSendFareDifference, String bookingId) throws Exception {
		FareChangeAlert fareAlert = null;
		final long startTime = System.currentTimeMillis();
		try {
			Option oldOption = hInfo.getOptions().get(0);
			double oldAmount = oldOption.getTotalPrice();
			AbstractHotelPriceValidationFactory priceValidationFactory =
					HotelSourceType.getHotelPriceValidationFactoryInstance(searchQuery, hInfo, bookingId);
			Option updatedOption = priceValidationFactory.getUpdatedOption();
			User user = SystemContextHolder.getContextData().getUser();
			if (ObjectUtils.isEmpty(updatedOption))
				throw new CustomGeneralException(SystemError.UNABLE_TO_VALIDATE_PRICE);

			HotelInfo updatedHInfo = getUpdateHotelInfo(hInfo, searchQuery, updatedOption);
			applyCommission(updatedHInfo, user, searchQuery);
			searchResultProcessingManager.applyAdditonalTax(new ArrayList<>(Arrays.asList(updatedHInfo)), searchQuery,
					false);

			boolean isOptionsEqual =
					HotelUtils.compareIfTwoOptionsEqual(oldOption.getRoomInfos(), updatedOption.getRoomInfos());
			double totalMarkup = 0.0;
			if (!isOptionsEqual) {
				totalMarkup = HotelUtils.getTotalMarkupOfOption(oldOption);
				logIfRoomsWithDuplicateUniqueId(updatedOption.getRoomInfos(), searchQuery);
				updateRoomInfoWithUpdatedPriceAndCancellationPolicy(oldOption.getRoomInfos(),
						updatedOption.getRoomInfos());
				updateNewCancellationPolicy(oldOption, updatedOption);
				searchResultProcessingManager.processHotelFareComponents(hInfo, searchQuery, user, null);
				if (oldOption.getMiscInfo().isMarkupUpdatedByUser()) {
					userFeeManager.updateUserFeeForHotelList(hInfo, totalMarkup, AmountType.FIXED.name());
				}
				searchResultProcessingManager.updateClientMarkup(hInfo, searchQuery, user, false);
				searchResultProcessingManager.updateManagementFees(hInfo, searchQuery);
				searchResultProcessingManager.buildProcessOptions(hInfo, searchQuery);
				BaseHotelUtils.updateTotalFareComponents(hInfo, false);
			} else if (isToSendFareDifference) {
				oldAmount = oldAmount - oldOption.getRoomInfos().get(0).getTotalAddlFareComponents()
						.get(HotelFareComponent.TAF).getOrDefault(HotelFareComponent.MU, 0.0);
				fareAlert = FareChangeAlert.builder().newFare(oldAmount + oldAmount * 0.01).oldFare(oldAmount)
						.type(AlertType.FAREALERT.name()).build();
			}

			checkIfExpectedBookingAmountSame(searchQuery, hInfo);

			if (!isOptionsEqual) {
				double newAmount = hInfo.getOptions().get(0).getTotalPrice();
				fareAlert = FareChangeAlert.builder().newFare(newAmount - totalMarkup).oldFare(oldAmount - totalMarkup)
						.type(AlertType.FAREALERT.name()).build();
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
					System.currentTimeMillis() - startTime);
		}
		return fareAlert;
	}

	private HotelInfo getUpdateHotelInfo(HotelInfo hInfo, HotelSearchQuery searchQuery, Option updatedOption) {
		HotelInfo updatedHInfo = new GsonMapper<>(hInfo, HotelInfo.class).convert();
		BaseHotelUtils.updateTotalFareComponents(updatedOption, null);
		updatedHInfo.setOptions(Arrays.asList(updatedOption));
		if (ObjectUtils.isEmpty(updatedHInfo.getOptions().get(0).getMiscInfo())) {
			updatedHInfo.getOptions().get(0).setMiscInfo(OptionMiscInfo.builder()
					.supplierId(hInfo.getOptions().get(0).getMiscInfo().getSupplierId()).build());
		}
		return updatedHInfo;
	}

	private void updateNewCancellationPolicy(Option oldOption, Option updatedOption) {
		if ((!ObjectUtils.isEmpty(updatedOption.getCancellationPolicy())
				&& CollectionUtils.isNotEmpty(updatedOption.getCancellationPolicy().getPenalyDetails()))) {
			oldOption.setCancellationPolicy(updatedOption.getCancellationPolicy());
			oldOption.setCancellationRestrictedDateTime(updatedOption.getCancellationRestrictedDateTime());
			oldOption.setDeadlineDateTime(updatedOption.getDeadlineDateTime());
		}
	}

	private void applyCommission(HotelInfo hInfo, User user, HotelSearchQuery searchQuery) {
		hInfo.getOptions().get(0).getMiscInfo().setIsCommissionApplied(false);
		commericialCommunicator.processUserCommissionInHotel(hInfo, user, searchQuery);
	}

	private void checkIfExpectedBookingAmountSame(HotelSearchQuery searchQuery, HotelInfo hInfo) {

		OptionMiscInfo optionMiscInfo = hInfo.getOptions().get(0).getMiscInfo();
		if (optionMiscInfo != null && optionMiscInfo.getExpectedOptionBookingPrice() != null) {
			BigDecimal expectedSupplierAmount = new BigDecimal(optionMiscInfo.getExpectedOptionBookingPrice());
			BigDecimal totalAmount = BaseHotelUtils.getTotalSupplierBookingAmount(hInfo.getOptions().get(0));
			double clientMarkup = BaseHotelUtils.getFareComponentPriceAtOptionLevel(hInfo.getOptions().get(0),
					HotelFareComponent.CMU);
			if (Math.abs(totalAmount.doubleValue() - clientMarkup - expectedSupplierAmount.doubleValue()) >= 1) {
				log.error(
						"Expected Supplier Booking Amount {} Does Not Match With Amount To Be Paid {} For SearchId {}",
						expectedSupplierAmount, totalAmount, searchQuery.getSearchId());
				throw new CustomGeneralException(SystemError.PRICE_MISMATCH_ERROR,
						"Price Mismatch Between Expected Price & Amount to Be Paid");
			}
		}
	}

	private void updateRoomInfoWithUpdatedPriceAndCancellationPolicy(List<RoomInfo> oldRoomInfoList,
			List<RoomInfo> newRoomInfoList) {


		Map<String, RoomInfo> newRoomPriceInfoMap =
				newRoomInfoList.stream().collect(Collectors.toMap(RoomInfo::getId, Function.identity()));

		for (RoomInfo roomInfo : oldRoomInfoList) {
			RoomInfo updatedRoom = newRoomPriceInfoMap.get(roomInfo.getId());
			List<PriceInfo> priceInfo = updatedRoom.getPerNightPriceInfos();
			if (CollectionUtils.isEmpty(priceInfo)) {
				throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
			}
			roomInfo.setPerNightPriceInfos(priceInfo);
			roomInfo.setCancellationPolicy(updatedRoom.getCancellationPolicy());
		}
	}

	private void logIfRoomsWithDuplicateUniqueId(List<RoomInfo> roomInfos, HotelSearchQuery searchQuery) {
		Set<String> uniqueRoomIds = new HashSet<>();
		if (roomInfos.size() > 0) {
			for (RoomInfo roomInfo : roomInfos) {
				uniqueRoomIds.add(roomInfo.getId());
			}
		}
		if (uniqueRoomIds.size() != roomInfos.size()) {
			log.info("Duplicate room ids after price change for search id {}", searchQuery.getSearchId());
		}
	}
}
