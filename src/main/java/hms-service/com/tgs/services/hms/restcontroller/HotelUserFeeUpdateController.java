package com.tgs.services.hms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.restmodel.HotelUserFeeUpdateRequest;
import com.tgs.services.hms.servicehandler.HotelUserFeeUpdateHandler;




@RestController
@RequestMapping("/hms/v1")
public class HotelUserFeeUpdateController {
	
	
	@Autowired
	private HotelUserFeeUpdateHandler userFeeUpdateHandler;

	@RequestMapping(value = "/update-user-fee", method = RequestMethod.POST)
	protected BaseResponse updateUserFee(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelUserFeeUpdateRequest userFeeUpdateRequest) throws Exception {
		if (BooleanUtils
				.isTrue(SystemContextHolder.getContextData().getUser().getAdditionalInfo().getDisableMarkup())) {
			throw new CustomGeneralException(SystemError.PERMISSION_DENIED);
		}
		userFeeUpdateHandler.initData(userFeeUpdateRequest, new BaseResponse());
		return userFeeUpdateHandler.getResponse();
	}
	

}
