package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo.HotelImportedBookingInfoBuilder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingDetailsRequestV2;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingDetailsResponseV2;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingDetailsResponseV2.Bookings.Booking;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Getter
@Setter
@SuperBuilder
public class AgodaRetrieveBookingService extends AgodaBaseService {

	private HotelImportBookingParams importBookingParams;
	private HotelImportedBookingInfo bookingInfo;
	protected MoneyExchangeCommunicator moneyExchnageComm;
	BookingDetailsResponseV2 bookingDetailResponse;
	protected String hotelReferenceNumber;
	protected String bookNowPayLaterStatus;

	public void retrieveBooking() throws IOException, JAXBException {
		HttpUtils httpUtils = null;
		listener = new RestAPIListener("");
		String supplierBookingReference = importBookingParams.getSupplierBookingId();

		BookingDetailsRequestV2 bookindDetailRequest = new BookingDetailsRequestV2();
		bookindDetailRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		bookindDetailRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		bookindDetailRequest.setBookingID(Arrays.asList(supplierBookingReference));
		bookingDetailResponse = getAgodaBookingDetailResponse(bookindDetailRequest);
		if (bookingDetailResponse.getStatus().equals("200")) {
			bookingInfo = createBookingDetailResponse();
		}
	}

	private HotelImportedBookingInfo createBookingDetailResponse() {

		HotelImportedBookingInfoBuilder importBookingInfo = HotelImportedBookingInfo.builder();
		if (bookingDetailResponse == null || CollectionUtils.isEmpty(bookingDetailResponse.getBookings().getBooking()))
			throw new CustomGeneralException(SystemError.EMPTY_SUPPLIER_RESPONSE);
		Booking response = bookingDetailResponse.getBookings().getBooking().get(0);
		HotelInfo hotelInfo = getHotelDetails(response);
		searchQuery = getHotelSearchQuery(response);

		Option option = getOption(response, searchQuery);
		hotelInfo.setOptions(Arrays.asList(option));
		String orderStatus = AgodaConstants.valueOf(response.getStatus().toUpperCase()).getValue();
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		String currency = response.getPayment().get(0).getPaymentRateInclusive().get(0).getCurrency();

		importBookingInfo.hInfo(hotelInfo).searchQuery(searchQuery).orderStatus(orderStatus).deliveryInfo(deliveryInfo)
				.bookingCurrencyCode(currency).bookingDate(null).build();

		return importBookingInfo.build();
	}

	private HotelSearchQuery getHotelSearchQuery(Booking response) {

		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
		searchQueryBuilder.checkinDate(LocalDate.parse(response.getCheckInDate()));
		searchQueryBuilder.checkoutDate(LocalDate.parse(response.getCheckOutDate()));
		searchQueryBuilder.sourceId(8);
		searchQueryBuilder
				.miscInfo(HotelSearchQueryMiscInfo.builder().supplierId(HotelSourceType.AGODA.name()).build());
		return searchQueryBuilder.build();
	}

	private HotelInfo getHotelDetails(Booking response) {

		HotelInfo hotelInfo = HotelInfo.builder().build();
		HotelMiscInfo miscInfo = HotelMiscInfo.builder().build();
		String supplierBookingRef = response.getBookingID() + "";
		String hotelConfirmationNnumber =
				!response.getSupplierReference().equalsIgnoreCase("AWAITING") ? response.getSupplierReference() : null;
		miscInfo.setSupplierBookingReference(supplierBookingRef);
		miscInfo.setSupplierBookingId(supplierBookingRef);
		miscInfo.setSupplierBookingUrl(response.getSelfServiceURL());
		miscInfo.setHotelBookingReference(hotelConfirmationNnumber);
		hotelInfo.setName(response.getHotel());
		hotelInfo.setAddress(
				Address.builder().cityName(response.getCity()).countryName(response.getCountry())
						.city(City.builder().name(response.getCity()).build())
				.country(Country.builder().name(response.getCountry()).build()).build());
		hotelInfo.setMiscInfo(miscInfo);
		return hotelInfo;
	}

	protected double getAmountBasedOnCurrency(double amount, String fromCurrency) {

		String toCurrency = "INR";
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(HotelSourceType.AGODA.name()).build();
		return moneyExchnageComm.getExchangeValue(amount, filter, true);
	}

	private BookingDetailsResponseV2 getAgodaBookingDetailResponse(BookingDetailsRequestV2 bookindDetailRequest)
			throws IOException, JAXBException {
		HttpUtils httpUtils = null;
		BookingDetailsResponseV2 response = null;
		try {

			String xmlRequest = AgodaMarshaller.marshallXml(bookindDetailRequest);
			httpUtils = AgodaUtil.getHttpUtils(xmlRequest, HotelUrlConstants.BOOKING_DETAIL, null, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			log.info("Response for booking detail for request {} is {}", xmlRequest, xmlResponse);
			response = AgodaMarshaller.unmarshallBookingDetailResponse(xmlResponse);

		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.AGODA.name())
					.headerParams(httpUtils.getHeaderParams()).requestType(BaseHotelConstants.RETRIEVE_BOOKING)
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.prefix(importBookingParams.getFlowType().getName()).postData(httpUtils.getPostData())
					.logKey(importBookingParams.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

		}
		return response;
	}

	private Option getOption(Booking response, HotelSearchQuery searchQuery) {

		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		List<RoomInfo> roomList = new ArrayList<>();
		Option option = Option.builder().build();
		double totalPrice = response.getPayment().get(0).getPaymentRateInclusive().get(0).getValue().doubleValue();

		int roomCount = (int) response.getRoom().getRoomsBooked();

		double perNightPrice = totalPrice / (numberOfNights * roomCount);

		for (int room = 0; room < response.getRoom().getRoomsBooked(); room++) {
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setRoomType(response.getRoom().getRoomType());
			roomInfo.setCheckInDate(LocalDate.parse(response.getCheckInDate()));
			roomInfo.setCheckOutDate(LocalDate.parse(response.getCheckOutDate()));
			List<PriceInfo> priceInfoList = new ArrayList<>();

			for (int day = 0; day < numberOfNights; day++) {
				PriceInfo priceInfo = new PriceInfo();
				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				fareComponents.put(HotelFareComponent.BF, perNightPrice);
				priceInfo.setFareComponents(fareComponents);
				priceInfo.setDay(day + 1);

				priceInfoList.add(priceInfo);
			}
			roomInfo.setPerNightPriceInfos(priceInfoList);
			roomList.add(roomInfo);
		}
		option.setMiscInfo(OptionMiscInfo.builder().secondarySupplier(supplierConf.getBasicInfo().getSupplierId())
				.sourceId(supplierConf.getBasicInfo().getSourceId()).supplierId(importBookingParams.getSupplierId())
				.build());
		updatePriceWithClientsCommission(roomList);
		option.setRoomInfos(roomList);
		return option;
	}

	public void updatePriceWithClientsCommission(List<RoomInfo> rInfoList) {

		double supplierMarkup =
				sourceConfigOutput.getSupplierMarkup() == null ? 0.0 : sourceConfigOutput.getSupplierMarkup();

		for (RoomInfo roomInfo : rInfoList) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				double supplierGrossPrice = priceInfo.getFareComponents().get(HotelFareComponent.BF);
				priceInfo.getFareComponents().put(HotelFareComponent.CMU, (supplierGrossPrice * supplierMarkup) / 100);


			}
		}
	}

	public void getHotelConfirmationNumber() throws IOException, JAXBException {

		String supplierBookingId = importBookingParams.getSupplierBookingId();
		BookingDetailsRequestV2 bookindDetailRequest = new BookingDetailsRequestV2();
		bookindDetailRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		bookindDetailRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		bookindDetailRequest.setBookingID(Arrays.asList(supplierBookingId));
		BookingDetailsResponseV2 bookingDetailResponse = getAgodaBookingDetailResponse(bookindDetailRequest);
		updateHotelReferenceNumber(bookingDetailResponse);

	}

	public void updateHotelReferenceNumber(BookingDetailsResponseV2 bookingDetailResponse) {
		if (!bookingDetailResponse.getStatus().equals("200"))
			return;
		String supplierReference = bookingDetailResponse.getBookings().getBooking().get(0).getSupplierReference();
		if (!supplierReference.equalsIgnoreCase("Awaiting") && !supplierReference.equalsIgnoreCase("Acknowledged")) {
			hotelReferenceNumber = supplierReference;
		}
	}

	public void getBookNowPayLaterStatus() throws IOException, JAXBException {

		String supplierBookingId = importBookingParams.getSupplierBookingId();
		BookingDetailsRequestV2 bookindDetailRequest = new BookingDetailsRequestV2();
		bookindDetailRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		bookindDetailRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		bookindDetailRequest.setBookingID(Arrays.asList(supplierBookingId));
		BookingDetailsResponseV2 bookingDetailResponse = getAgodaBookingDetailResponse(bookindDetailRequest);
		updateBookNowPayLaterStatus(bookingDetailResponse);

	}

	public void updateBookNowPayLaterStatus(BookingDetailsResponseV2 bookingDetailResponse) {
		if (!bookingDetailResponse.getStatus().equals("200"))
			return;
		bookNowPayLaterStatus = bookingDetailResponse.getBookings().getBooking().get(0).getStatus();
	}

}
