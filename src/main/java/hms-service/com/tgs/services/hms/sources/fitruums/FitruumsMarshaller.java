package com.tgs.services.hms.sources.fitruums;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import com.tgs.services.hms.datamodel.fitruums.CancellationPolicies;
import com.tgs.services.hms.datamodel.fitruums.CancellationPolicy;
import com.tgs.services.hms.datamodel.fitruums.Destination;
import com.tgs.services.hms.datamodel.fitruums.DestinationList;
import com.tgs.services.hms.datamodel.fitruums.Destinations;
import com.tgs.services.hms.datamodel.fitruums.FitruumsSearchRequest;
import com.tgs.services.hms.datamodel.fitruums.GetStaticHotelsAndRoomsResult;
import com.tgs.services.hms.datamodel.fitruums.Hotel;
import com.tgs.services.hms.datamodel.fitruums.Hotels;
import com.tgs.services.hms.datamodel.fitruums.Meal;
import com.tgs.services.hms.datamodel.fitruums.Meals;
import com.tgs.services.hms.datamodel.fitruums.Note;
import com.tgs.services.hms.datamodel.fitruums.Notes;
import com.tgs.services.hms.datamodel.fitruums.PaymentMethod;
import com.tgs.services.hms.datamodel.fitruums.PaymentMethods;
import com.tgs.services.hms.datamodel.fitruums.Price;
import com.tgs.services.hms.datamodel.fitruums.Prices;
import com.tgs.services.hms.datamodel.fitruums.Room;
import com.tgs.services.hms.datamodel.fitruums.Rooms;
import com.tgs.services.hms.datamodel.fitruums.Roomtype;
import com.tgs.services.hms.datamodel.fitruums.Roomtypes;
import com.tgs.services.hms.datamodel.fitruums.Searchresult;
import com.tgs.services.hms.datamodel.fitruums.book.BookResult;
import com.tgs.services.hms.datamodel.fitruums.book.BookingCancellationResult;
import com.tgs.services.hms.datamodel.fitruums.book.Error;
import com.tgs.services.hms.datamodel.fitruums.book.GetBookingInformationResult;
import com.tgs.services.hms.datamodel.fitruums.book.PreBookResult;

public class FitruumsMarshaller {
	private Jaxb2Marshaller marshaller2;

	public FitruumsMarshaller() {
		marshaller2 = new Jaxb2Marshaller();
		marshaller2.setClassesToBeBound(new Class[] {DestinationList.class, FitruumsSearchRequest.class,
				Destination.class, Destinations.class, Searchresult.class, CancellationPolicies.class,
				CancellationPolicy.class, Hotel.class, Hotels.class, Meal.class, Meals.class, PreBookResult.class,
				Note.class, Notes.class, PaymentMethod.class, PaymentMethods.class, Price.class, Prices.class,
				BookResult.class, BookingCancellationResult.class, GetBookingInformationResult.class, Room.class,
				Error.class, Rooms.class, Roomtype.class, Roomtypes.class, GetStaticHotelsAndRoomsResult.class});
		marshaller2.setMarshallerProperties(new HashMap<String, Object>() {
			{
				put(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
			}
		});
	}

	public <T> String marshallXml(final T obj) throws JAXBException {
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		marshaller2.marshal(obj, result);
		return sw.toString();
	}

	@SuppressWarnings("unchecked")
	public <T> T unmarshallXML(String data, Class<T> T) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (T) marshaller2.unmarshal(new StreamSource(targetStream));

	}

}
