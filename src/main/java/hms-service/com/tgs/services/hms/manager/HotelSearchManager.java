package com.tgs.services.hms.manager;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.MethodExecutionInfo;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.HotelMissingInfoLoggingUtil;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMissingInfo;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfoMapping;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.helper.HotelSearch;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.hms.helper.analytics.HotelAnalyticsHelper;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import com.tgs.services.hms.mapper.HotelMethodExecutionInfoMapper;
import com.tgs.services.hms.restmodel.HotelSearchResponse;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelSearchManager {

	@Autowired
	HotelSearch hotelSearch;

	@Autowired
	HotelInfoService hotelInfoService;

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelRegionInfoService cityInfoService;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;

	@Autowired
	HotelAnalyticsHelper analyticsHelper;

	@Autowired
	GeneralServiceCommunicator gmsComm;

	@Autowired
	HotelSearchResultProcessingManager searchResultProcessingManager;

	public void search(HotelSearchQuery searchQuery) {

		try {
			ContextData contextData = SystemContextHolder.getContextData();
			if (CollectionUtils.isNotEmpty(searchQuery.getSupplierIds())) {
				/*
				 * Search for specific suppliers mentioned in Search Query
				 */
				Set<String> supplierIds = new HashSet<>(searchQuery.getSupplierIds());
				List<HotelSupplierInfo> hotelSupplierInfoList =
						HotelSupplierConfigurationHelper.getSupplierListFromSupplierIds(supplierIds);
				doSearch(searchQuery, contextData, hotelSupplierInfoList,
						new AtomicInteger(hotelSupplierInfoList.size()));
				return;
			}

			/*
			 * Search for each source in parallel
			 */
			searchQuery.setSourceIds(HotelSourceType.getAllSourceIds());
			List<HotelSupplierInfo> supplierList = HotelUtils.getHotelSupplierInfo(searchQuery, contextData);

			if (CollectionUtils.isEmpty(supplierList)) {
				log.info("No supplier is enabled for search id {}", searchQuery.getSearchId());
				return;
			}

			if (!isValidRegionForSearch(searchQuery, supplierList)) {
				log.info("Unable to validate region info for search query {}", searchQuery);
				return;
			}

			log.info("Started hotel search with search id {} for suppliers {}", searchQuery.getSearchId(),
					supplierList.stream().map(HotelSupplierInfo::getName).collect(Collectors.toList()));

			AtomicInteger searchCount = new AtomicInteger(supplierList.size());
			cacheHandler.storeSearchSpiltCount(searchQuery.getSearchId(), searchCount.get());
			doSearch(searchQuery, contextData, supplierList, searchCount);
		} finally {
			cacheHandler.cacheSearchResultCompleteAt(searchQuery);
			LogUtils.clearLogList();
		}
	}

	public HotelSearchResponse getSearchResult(String searchId) throws Exception {

		HotelSearchResult searchResult = null;
		boolean isSearchCompleted = false;
		HotelSearchResponse searchResponse = new HotelSearchResponse();
		int attemptCount = 0;
		do {
			isSearchCompleted = cacheHandler.isSearchCompleted(searchId);
			if (isSearchCompleted) {
				searchResult = cacheHandler.getSearchResult(searchId);
				if (searchResult != null) {
					// filterHotelsBasedOnLimit(searchQuery, searchResult);
					searchResponse.setSearchResult(searchResult);
					return searchResponse;
				} else {
					SystemContextHolder.getContextData().getErrorMessages().add("Cross sell search result is empty");
					log.info("Cross sell search result is empty for search query {}", searchId);
					throw new CustomGeneralException(SystemError.EMPTY_SEARCH_RESULT);
				}
			}
			attemptCount++;
			Thread.sleep(3 * 1000);
		} while (!isSearchCompleted && attemptCount < 20);
		return null;
	}

	public boolean isSearchRequestValidForSource(HotelSearchQuery searchQuery, HotelSourceType sourceType,
			ClientGeneralInfo clientInfo) {

		if (StringUtils.isBlank(searchQuery.getSearchPreferences().getHotelId())
				&& StringUtils.isBlank(clientInfo.getPolygonInfoSupplierName())) {
			String sourceName = sourceType.name();
			if (HotelUtils.isCrossSell(searchQuery.getSearchId())) {
				return true;
			}
			boolean isValid = true;

			HotelSupplierRegionInfo supplierRegionInfo = cacheHandler
					.getRegionInfoMappingFromRegionId(searchQuery.getSearchCriteria().getRegionId(), sourceName);
			if (supplierRegionInfo == null && !sourceType.getisValidSourceIfRegionMappingNull()) {
				isValid = false;
			}
			return isValid;
		}
		return true;
	}

	public HotelSearchResult doSearch(HotelSearchQuery searchQuery, ContextData contextData,
			List<HotelSupplierInfo> supplierList, AtomicInteger searchCount) {

		ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				List<Future<?>> suppliersResultToProcess = new ArrayList<Future<?>>();
				supplierList.forEach(supplierInfo -> {
					ContextData copyContextData = contextData.deepCopy();
					Future<?> supplierWiseSearchResult = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
						HotelSearchResponse searchResponse = new HotelSearchResponse();
						HotelSearchResult searchResult = null;
						HotelSearchQuery copyQuery = null;
						try {
							log.info("Search started with search id {} for supplier {}", searchQuery.getSearchId(),
									supplierInfo.getName());
							copyQuery = new GsonMapper<>(searchQuery, HotelSearchQuery.class).convert();
							copyQuery.setMiscInfo(
									HotelSearchQueryMiscInfo.builder().supplierId(supplierInfo.getSupplierId())
											.sourceId(supplierInfo.getSourceId().toString()).build());
							copyQuery.setSourceId(supplierInfo.getSourceId());
							SystemContextHolder.setContextData(copyContextData);
							copyContextData.setValue(BaseHotelConstants.METHOD_EXECUTION_TYPE,
									BaseHotelConstants.SINGLE_SUPPLIER);
							copyContextData.setValue(BaseHotelConstants.METHOD_SUB_EXECUTION_TYPE,
									BaseHotelConstants.BEFORE_SEARCH);
							searchResult = hotelSearch.search(copyQuery, copyContextData);
							processAndPopulateCheckpointInfo(contextData);
						} catch (Exception e) {
							log.error("Error while searching for searchId {} , supplier {}", copyQuery.getSearchId(),
									copyQuery.getMiscInfo().getSupplierId(), e);
						} finally {
							log.info("Search completed with search id {} for supplier {}", searchQuery.getSearchId(),
									supplierInfo.getName());
							searchResponse.setMethodExecutionInfo(copyContextData.getMethodExecutionInfo());
							searchResponse.setSearchQuery(copyQuery);
							searchResponse.setSearchResult(searchResult);
						}
						return searchResponse;
					});
					suppliersResultToProcess.add(supplierWiseSearchResult);
				});

				processSupplierResult(suppliersResultToProcess, searchQuery, supplierList, searchCount);
				log.info("Finished hotel search with search id {} for suppliers {}", searchQuery.getSearchId(),
						supplierList.stream().map(HotelSupplierInfo::getName).collect(Collectors.toList()));
			} catch (ExecutionException | InterruptedException e) {
				log.error("Error while searching for hotels due to {} for searchId {} ", e.getMessage(),
						searchQuery.getSearchId(), e);
			} catch (Exception e) {
				log.error("Error while searching for hotels for searchId {} ", searchQuery.getSearchId(), e);
			}
		});
		return null;
	}

	private void processSupplierResult(List<Future<?>> suppliersResultToProcess, HotelSearchQuery searchQuery,
			List<HotelSupplierInfo> supplierList, AtomicInteger searchCount) throws Exception {

		ContextData contextData = SystemContextHolder.getContextData();
		contextData.setValue(BaseHotelConstants.METHOD_EXECUTION_TYPE, BaseHotelConstants.MULTIPLE_SUPPLIER);
		contextData.setValue(BaseHotelConstants.METHOD_SUB_EXECUTION_TYPE, BaseHotelConstants.BEFORE_SEARCH);
		Set<String> processedTasks = new HashSet<>(); // To make sure no supplier will get reprocessed.
		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
		int searchRetryCount = ObjectUtils.isEmpty(configuratorInfo.getSearchRetryCount()) ? 120
				: configuratorInfo.getSearchRetryCount();
		List<Map<String, Map<String, MethodExecutionInfo>>> methodExecutionInfos = new ArrayList<>();
		do {
			AtomicInteger supplierResponseFetchedCount = new AtomicInteger();
			try {
				List<HotelSearchResponse> searchResultFromMultipleSuppliers = fetchTasksIfNotProcessedYet(
						suppliersResultToProcess, processedTasks, supplierResponseFetchedCount);
				if (CollectionUtils.isNotEmpty(searchResultFromMultipleSuppliers)) {
					processHotelSearchResponses(searchResultFromMultipleSuppliers, methodExecutionInfos);
					searchCount.set(searchCount.get() - supplierResponseFetchedCount.get());
					cacheHandler.storeSearchSpiltCount(searchQuery.getSearchId(), searchCount.get());
				} else {
					Thread.sleep(500);
				}
				log.info(
						"Search result from multiple suppliers for search id {}, processed tasks {} and size {}, fetch count {}, search count {}, supplier list size {}, searchRetryCount {}",
						searchQuery.getSearchId(), processedTasks, processedTasks.size(),
						supplierResponseFetchedCount.get(), searchCount.get(), supplierList.size(), searchRetryCount);
			} catch (Exception e) {
				searchCount.set(0);
				cacheHandler.storeSearchSpiltCount(searchQuery.getSearchId(), searchCount.get());
				log.error("Search failed while searching for hotels for searchId {} ", searchQuery.getSearchId(), e);
				throw e;
			}
			if (--searchRetryCount < 0 || processedTasks.size() == supplierList.size()) {
				searchCount.set(0);
				cacheHandler.storeSearchSpiltCount(searchQuery.getSearchId(), searchCount.get());
				addMethodExecutionInfo(methodExecutionInfos, contextData.getMethodExecutionInfo());
				log.info("Method execution info for search id {} is {}", searchQuery.getSearchId(),
						GsonUtils.getGson().toJson(methodExecutionInfos));
				log.info(
						"Either search processing retry threshold reached or all suppliers are processed for searchId {}. {}",
						searchQuery.getSearchId(), processedTasks.size() == supplierList.size());
				return;
			}
		} while (true);
	}

	private void sendMethodExecutionDataToAnalytics(HotelSearchResult searchResult, HotelSearchQuery searchQuery,
			ContextData contextData) {

		if (Objects.nonNull(searchResult) && searchResult.getNoOfHotelOptions() > 0
				&& MapUtils.isNotEmpty(contextData.getMethodExecutionInfo())) {
			ContextData methodContextData = BaseHotelUtils.getNewContextDataForMethodExecutionInfo(contextData);
			HotelMethodExecutionInfoMapper methodExecutionMapper =
					HotelMethodExecutionInfoMapper.builder().contextData(methodContextData)
							.user(methodContextData.getUser())
							.searchQuery(searchQuery).flowType(HotelFlowType.SEARCH).build();
			analyticsHelper.storeMethodData(methodExecutionMapper);
		} else {
			log.info(
					"Unable to store method execution info as no result found for for search id {} and supplier name {}",
					searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId());
		}
	}

	private void addMethodExecutionInfo(List<Map<String, Map<String, MethodExecutionInfo>>> methodsExecutionInfos,
			Map<String, Map<String, MethodExecutionInfo>> methodsExecutionInfo) {
		if (MapUtils.isNotEmpty(methodsExecutionInfo)) {
			methodsExecutionInfos.add(methodsExecutionInfo);
		}
	}

	private List<HotelSearchResponse> fetchTasksIfNotProcessedYet(List<Future<?>> suppliersResultToProcess,
			Set<String> processedTasks, AtomicInteger supplierResponseFetchedCount)
			throws ExecutionException, InterruptedException {

		List<HotelSearchResponse> searchResultFromMultipleSuppliers = new ArrayList<>();
		for (Future<?> singleSupplierResult : suppliersResultToProcess) {
			if (singleSupplierResult.isDone()) {
				Object searchResult = singleSupplierResult.get();
				HotelSearchResponse searchResponse = (HotelSearchResponse) searchResult;
				if (!processedTasks.contains(searchResponse.getSearchQuery().getMiscInfo().getSupplierId())) {
					searchResultFromMultipleSuppliers.add(searchResponse);
					supplierResponseFetchedCount.getAndIncrement();
					processedTasks.add(searchResponse.getSearchQuery().getMiscInfo().getSupplierId());
				}
			}
		}
		return searchResultFromMultipleSuppliers;
	}

	private void processHotelSearchResponses(List<HotelSearchResponse> searchResponses,
			List<Map<String, Map<String, MethodExecutionInfo>>> methodExecutionInfos) {

		ClientGeneralInfo generalInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		SystemContextHolder.getContextData().setValue(ConfiguratorRuleType.CLIENTINFO.name(), generalInfo);

		HotelSearchQuery searchQuery = searchResponses.get(0).getSearchQuery();
		List<String> supplierIds = new ArrayList<>();

		/*
		 * Created single hotel search result from multiple search result
		 */
		HotelSearchResult searchResult = HotelSearchResult.builder().build();
		for (HotelSearchResponse searchResponse : searchResponses) {
			if (Objects.nonNull(searchResponse.getSearchResult())
					&& searchResponse.getSearchResult().getNoOfHotelOptions() > 0) {
				cacheHandler.processAndStore(searchResponse.getSearchResult(), searchResponse.getSearchQuery());
				searchResult.getHotelInfos().addAll(searchResponse.getSearchResult().getHotelInfos());
				addMethodExecutionInfo(methodExecutionInfos, searchResponse.getMethodExecutionInfo());
				supplierIds.add(searchResponse.getSearchQuery().getMiscInfo().getSupplierId());
			} else {
				log.info("No result found for search id {} from supplier {}", searchQuery.getSearchId(),
						searchResponse.getSearchQuery().getMiscInfo().getSupplierId());
			}
		}

		if (CollectionUtils.isNotEmpty(searchResult.getHotelInfos())) {
			searchQuery.getMiscInfo().setSupplierIds(supplierIds); // This is required for better logging
			log.info("Processing of merged hotels for search id {} and suppliers {} are {}", searchQuery.getSearchId(),
					supplierIds, searchResult.getNoOfHotelOptions());
			searchResultProcessingManager.combineAndProcessPreviousResult(searchResult, searchQuery);
			sendMethodExecutionDataToAnalytics(searchResult, searchQuery, SystemContextHolder.getContextData());
			log.info("Processing completed for merged hotels for search id {} and suppliers {} are {}",
					searchQuery.getSearchId(), supplierIds, searchResult.getNoOfHotelOptions());
		}
	}

	private void processAndPopulateCheckpointInfo(ContextData contextData) {

		ContextData copyContextData = SystemContextHolder.getContextData();
		Map<String, List<CheckPointData>> copyCheckPointInfoMap =
				copyContextData.getCheckPointInfo().entrySet().stream()
						.filter(map -> !(map.getKey().equals(SystemCheckPoint.REQUEST_STARTED.name())
								|| map.getKey().equals(SystemCheckPoint.REQUEST_FINISHED.name())))
						.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		copyCheckPointInfoMap.entrySet().forEach(copyCheckPointInfo -> {
			copyCheckPointInfo.getValue().stream().forEach(checkpoint -> {
				checkpoint.setParallel(true);
				checkpoint.setSubType(HotelFlowType.SEARCH.name());
			});
			contextData.addCheckPoints(copyCheckPointInfo.getValue());
		});
	}

	public void fetchHotelDetails(HotelSearchQuery searchQuery, HotelInfo hInfo) {

		hotelSearch.fetchDetails(searchQuery, hInfo);

	}

	public static void setSearchId(HotelSearchQuery searchQuery) {
		String sourceIdStr = RandomStringUtils.random(10, false, true);
		String searchId = String.join("", "hsid", sourceIdStr);
		searchQuery.setSearchId(searchId);
	}

	public void populateRegionInfo(HotelSearchQuery searchQuery) {
		HotelRegionInfo regionInfo =
				cacheHandler.getRegionInfoFromRegionId(searchQuery.getSearchCriteria().getRegionId());
		if (!ObjectUtils.isEmpty(regionInfo)) {
			searchQuery.getSearchCriteria().setCityName(regionInfo.getRegionName());
			searchQuery.getSearchCriteria().setCountryName(regionInfo.getCountryName());
		}
		logMissingCityInfo(searchQuery);
	}

	public void populateRegionInfoFromIATA(HotelSearchQuery searchQuery) {
		HotelRegionInfo regionInfo = cacheHandler
				.getRegionInfoFromIATACode(searchQuery.getSearchPreferences().getCrossSellParameter().getIataCode());
		if (!ObjectUtils.isEmpty(regionInfo)) {
			searchQuery.getSearchCriteria().setCityName(regionInfo.getRegionName());
			searchQuery.getSearchCriteria().setRegionId(String.valueOf(regionInfo.getId()));
		}
	}

	public HotelInfo doSearchAgainWithSameSuppliers(HotelSearchQuery searchQuery, HotelInfo hInfo,
			ContextData contextData) throws Exception {

		Set<String> supplierIds = HotelUtils.getSupplierIdsFromHotelInfo(hInfo);
		List<HotelSupplierInfo> hotelSupplierInfoList =
				HotelSupplierConfigurationHelper.getSupplierListFromSupplierIds(supplierIds);
		AtomicInteger searchCount = new AtomicInteger(hotelSupplierInfoList.size());
		cacheHandler.storeSearchSpiltCount(searchQuery.getSearchId(), searchCount.get());
		doSearch(searchQuery, contextData, hotelSupplierInfoList, searchCount);
		HotelSearchResult searchResult = null;
		boolean isSearchCompleted = false;
		String hotelName = hInfo.getName();
		int attemptCount = 0;
		do {
			isSearchCompleted = cacheHandler.isSearchCompleted(searchQuery.getSearchId());
			if (isSearchCompleted) {
				searchResult = cacheHandler.getSearchResult(searchQuery.getSearchId());
				if (searchResult != null) {
					hInfo = HotelUtils.filterHotel(searchResult, hInfo);
					if (hInfo == null) {
						contextData.getErrorMessages().add("Hotel Not available in new search result for searchId");
						log.info("Hotel {} Not available in new search result for new searchId {} ", hotelName,
								searchQuery.getSearchId());
						throw new CustomGeneralException(SystemError.HOTEL_NOT_FOUND);
					}
				} else {
					contextData.getErrorMessages().add("Search Result Empty while we do search again on review page");
					log.info(
							"Search result empty while we do search again on review page for hotel {} , new searchId {}",
							hotelName, searchQuery.getSearchId());
					throw new CustomGeneralException(SystemError.EMPTY_SEARCH_RESULT);
				}
				break;
			}
			attemptCount++;
			Thread.sleep(3 * 1000);
		} while (!isSearchCompleted && attemptCount < 20);
		hInfo = cacheHandler.getHotelDetailsFromCacheWithRetry(hInfo.getId(), searchQuery, false);

		return hInfo;
	}

	public HotelSearchQuery getSearchQuery(String id) {
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(id);
		if (ObjectUtils.isEmpty(searchQuery)) {
			if (HotelUtils.isCrossSell(id)) {
				String cleanedId = HotelUtils.removeCrossSellFromSearchId(id);
				searchQuery = HotelUtils.getHotelSearchQuery(cleanedId);
				if (ObjectUtils.isEmpty(searchQuery)) {
					throw new CustomGeneralException(SystemError.INVALID_SEARCH_ID);
				}
				HotelUtils.populateMissingParametersInHotelSearchQuery(searchQuery);
			} else {
				throw new CustomGeneralException(SystemError.SEARCH_ID_NOT_FOUND);
			}
		}
		return searchQuery;
	}

	public HotelSearchResponse searchCrossSellHotels(HotelSearchQuery searchQuery) {
		HotelSearchResponse searchResponse = new HotelSearchResponse();
		HotelSearchResult searchResult = cacheHandler.getSearchResult(searchQuery.getSearchId());
		if (ObjectUtils.isEmpty(searchResult)) {
			search(searchQuery);
		} else {
			HotelUtils.filterCrossSellHotels(searchQuery, searchResult);
			HotelUtils.filterHotelsBasedOnLimit(searchQuery, searchResult);
		}
		searchResponse.setSearchResult(searchResult);
		searchResponse.setSearchQuery(searchQuery);
		return searchResponse;
	}

	@SuppressWarnings("unchecked")
	private void logMissingCityInfo(HotelSearchQuery searchQuery) {
		if (StringUtils.isNotBlank(searchQuery.getSearchCriteria().getRegionId())) {
			ExecutorService executor = ExecutorUtils.getHotelSearchThreadPool();
			executor.submit(() -> {
				try {
					List<String> supplierNames = Arrays.asList(HotelSourceType.values()).stream()
							.map(x -> x.name().toLowerCase()).collect(Collectors.toList());
					supplierNames.remove("expediacrosssell");
					List<DbHotelRegionInfoMapping> regionInfoMapping = cityInfoService.findByRegionIdAndSupplierList(
							Long.parseLong(searchQuery.getSearchCriteria().getRegionId()), supplierNames);
					List<String> supplierNames2 = regionInfoMapping.stream().map(x -> x.getSupplierName().toLowerCase())
							.collect(Collectors.toList());
					Set<String> missingCityInfoSuppliers =
							new HashSet<>(CollectionUtils.disjunction(supplierNames, supplierNames2));

					HotelMissingInfo missingInfo = new HotelMissingInfo();
					missingInfo.setCountryName(searchQuery.getSearchCriteria().getCountryName());
					missingInfo.setCityName(searchQuery.getSearchCriteria().getCityName());
					missingInfo.setSupplierNames(missingCityInfoSuppliers);
					missingInfo.setOperationType(OperationType.CITY_MAPPING);
					HotelMissingInfoLoggingUtil.addToLogList(missingInfo, searchQuery);
				} catch (Exception e) {
					log.info("Unable to generate search query for bookingId {}", e);
				} finally {
					HotelMissingInfoLoggingUtil.clearLog(LogTypes.MISSING_INFO);
				}
			});
		}
	}

	private boolean isValidRegionForSearch(HotelSearchQuery searchQuery, List<HotelSupplierInfo> supplierList) {

		ClientGeneralInfo clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		String polygonInfoSupplierName = clientInfo.getPolygonInfoSupplierName();
		if (StringUtils.isNotBlank(polygonInfoSupplierName)
				&& StringUtils.isBlank(searchQuery.getSearchPreferences().getHotelId())) {
			HotelRegionInfo regionInfo =
					cacheHandler.getRegionInfoFromRegionId(searchQuery.getSearchCriteria().getRegionId());
			if (Objects.isNull(regionInfo) || BooleanUtils.isNotTrue(regionInfo.getEnabled())) {
				log.info("Region {} is not enabled/present for search id {}", searchQuery.getSearchId());
				return false;
			}
			log.info("Polygon based supplier configured for search id {} is {}", searchQuery.getSearchId(),
					polygonInfoSupplierName);
		}

		for (Iterator<HotelSupplierInfo> supplierInfoIterator = supplierList.iterator(); supplierInfoIterator
				.hasNext();) {
			HotelSupplierInfo supplierInfo = supplierInfoIterator.next();
			if (!isSearchRequestValidForSource(searchQuery,
					HotelSourceType.getHotelSourceType(supplierInfo.getSourceId()), clientInfo)) {
				supplierInfoIterator.remove();
			}
		}

		if (CollectionUtils.isEmpty(supplierList)) {
			log.info("No region mapping is configured for any supplier for search id {}", searchQuery.getSearchId());
			return false;
		}
		return true;
	}

	public void updateSearchQueryForNewSearch(HotelSearchQuery searchQuery) {

		HotelUtils.populateMissingParametersInHotelSearchQuery(searchQuery);
		populateRegionInfo(searchQuery);

	}

	public void doFullSearchAgain(HotelSearchQuery searchQuery, ContextData contextData) {
		search(searchQuery);
	}

}
