package com.tgs.services.hms.sources.fitruums;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.fitruums.FitruumsStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.utils.common.HttpUtils;

public class FitruumsUtil {
	private final static int TIMEOUT = 200 * 1000;

	public static HttpUtils getHttpUtils(String postData, HotelUrlConstants url,
			FitruumsStaticDataRequest staticRequest, HotelSupplierConfiguration supplierConf) {

		Map<String, String> queryParams = null;
		if (staticRequest != null) {
			staticRequest.setUserName(supplierConf.getHotelSupplierCredentials().getUserName());
			staticRequest.setPassword(supplierConf.getHotelSupplierCredentials().getPassword());
			staticRequest.setLanguage("en");
			staticRequest.setSortBy("");
			staticRequest.setSortOrder("");
			staticRequest.setExactDestinationMatch("");
			queryParams = convertToMap(staticRequest);
		}
		String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
		String urlString = StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(url));

		HttpUtils httpUtils =
				HttpUtils.builder().urlString(urlString).postData(postData).headerParams(getHeaderParams(postData))
						.queryParams(queryParams).requestMethod(HttpUtils.REQ_METHOD_POST).timeout(TIMEOUT).build();
		return httpUtils;
	}

	private static Map<String, String> getHeaderParams(String postData) {
		Map<String, String> headerParams = new HashMap<>();
		if (postData != null) {
			int postDataLength = postData.length();
			headerParams.put("content-type", "application/x-www-form-urlencoded");
			headerParams.put("charset", "utf-8");
			headerParams.put("Content-Length", String.valueOf(postDataLength));
			headerParams.put("Accept-Encoding", "gzip,deflate");
		}

		else {
			headerParams.put("content-type", "text/xml");
			headerParams.put("Accept-Encoding", "gzip,deflate");
		}

		return headerParams;

	}

	public static void setMealBasis(List<HotelInfo> hotelInfos, HotelSearchQuery searchQuery) {
		mapMealidToMealName(hotelInfos);
		HotelBaseSupplierUtils.populateMealInfo(hotelInfos, searchQuery, false);
	}


	private static void mapMealidToMealName(List<HotelInfo> hotelInfos) {
		hotelInfos.forEach(hotelInfo -> {
			hotelInfo.getOptions().forEach(option -> {
				option.getRoomInfos().forEach(roomInfo -> {
					roomInfo.getMiscInfo().setAmenities(mapMeal(roomInfo.getMiscInfo().getAmenities()));
				});
			});
		});
	}

	private static List<String> mapMeal(List<String> amenities) {
		List<String> mealName = new LinkedList<>();
		for (String mealId : amenities) {
			switch (mealId) {
				case "1":
					mealName.add("No meals");
					break;
				case "3":
					mealName.add("Breakfast");
					break;
				case "4":
					mealName.add("Half board");
					break;
				case "5":
					mealName.add("Full board");
					break;
				case "6":
					mealName.add("All inclusive");
					break;
				default:
					break;
			}
		}
		return mealName;
	}

	private static <T> Map<String, String> convertToMap(T obj) {
		return new Gson().fromJson(
				GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(obj),
				new TypeToken<HashMap<String, String>>() {}.getType());
	}

}
