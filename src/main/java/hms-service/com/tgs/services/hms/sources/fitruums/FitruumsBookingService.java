package com.tgs.services.hms.sources.fitruums;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.CountryInfoType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.fitruums.book.BookResult;
import com.tgs.services.hms.datamodel.fitruums.book.GetBookingInformationResult;
import com.tgs.services.hms.datamodel.fitruums.book.GetBookingInformationResult.Bookings.Booking.HotelNotes;
import com.tgs.services.hms.datamodel.fitruums.book.GetBookingInformationResult.Bookings.Booking.HotelNotes.HotelNote;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
@Getter
@Setter
public class FitruumsBookingService extends FitruumsBaseService {
	private Order order;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelInfo hInfo;
	private HotelOrderItemCommunicator itemComm;
	private GeneralServiceCommunicator gnComm;

	public void init() {
		gnComm = (GeneralServiceCommunicator) SpringContext.getApplicationContext()
				.getBean("generalServiceCommunicatorImpl");
	}

	public boolean book() throws IOException, InterruptedException {
		HttpUtils httpUtils = null;
		HashMap<String, String> bookRequestMap = getBookRequestMap(hInfo.getOptions().get(0));
		FitruumsMarshaller fitruumsMarshaller = new FitruumsMarshaller();
		String xmlResponse = null;
		BookResult bookingResult = null;
		try {
			listener = new RestAPIListener("");
			String bookingRequest = getKeyValuePair(bookRequestMap);
			httpUtils = FitruumsUtil.getHttpUtils(bookingRequest, HotelUrlConstants.BOOK_URL, null, supplierConf);
			xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (StringUtils.isNotEmpty(xmlResponse)) {
				bookingResult = fitruumsMarshaller.unmarshallXML(xmlResponse, BookResult.class);
				if (Objects.nonNull((bookingResult)) && Objects.nonNull(bookingResult.getBooking())
						&& StringUtils.isNotEmpty(bookingResult.getBooking().getBookingnumber())) {
					return updateBookingStatus(bookingResult.getBooking().getBookingnumber());
				}
			} else {
				/*
				 * As mentioned in the documentation, we have to wait for 90 sec before calling their retrieve booking
				 * API
				 */
				ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
					try {
						Thread.sleep(90000);
						if (getstatus(order.getBookingId())) {
							itemComm.updateOrderItem(hInfo, order, HotelItemStatus.SUCCESS);
						}
					} catch (Exception e) {
						log.info("Unable to retrive booking for booking id {}", order.getBookingId());
					}
				});
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.BOOK.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(xmlResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.FITRUUMS.name())
						.requestType(BaseHotelConstants.BOOKING).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(order.getBookingId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
		return false;
	}

	private boolean updateBookingStatus(String supplierbookingId) throws IOException {
		hInfo.getMiscInfo().setSupplierBookingId(supplierbookingId);
		hInfo.getMiscInfo().setSupplierBookingReference(supplierbookingId);
		return true;
	}

	private boolean getstatus(String bookingId) throws IOException {
		HashMap<String, String> bookRetrieveMap = getBookRetrieveMap(bookingId);
		FitruumsMarshaller fitruumsMarshaller = new FitruumsMarshaller();
		String xmlResponse = null;
		GetBookingInformationResult retrieveBookingResult = null;
		HttpUtils httpUtils = null;
		try {
			listener = new RestAPIListener("");
			String retrievebookingRequest = getKeyValuePair(bookRetrieveMap);
			httpUtils = FitruumsUtil.getHttpUtils(retrievebookingRequest, HotelUrlConstants.CONFIRM_BOOKING_URL, null,
					supplierConf);
			xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			log.info("Retrieve booking response for booking id {} is {}", order.getBookingId(), xmlResponse);
			if (StringUtils.isNotEmpty(xmlResponse)) {
				retrieveBookingResult =
						fitruumsMarshaller.unmarshallXML(xmlResponse, GetBookingInformationResult.class);
				if (Objects.nonNull(retrieveBookingResult.getBookings())
						&& Objects.nonNull(retrieveBookingResult.getBookings().getBooking()) && StringUtils
								.isNotEmpty(retrieveBookingResult.getBookings().getBooking().getBookingnumber())) {
					setHotelNotes(hInfo.getOptions().get(0),
							retrieveBookingResult.getBookings().getBooking().getHotelNotes());
					return updateBookingStatus(retrieveBookingResult.getBookings().getBooking().getBookingnumber());
				}
			}
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(retrieveBookingResult)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.FITRUUMS.name())
					.requestType(BaseHotelConstants.RETRIEVE_BOOKING).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

		}
		return false;
	}

	private void setHotelNotes(Option option, HotelNotes hotelNotes) {
		List<Instruction> instructionList = new ArrayList<>();
		if (Objects.nonNull(hotelNotes) && hotelNotes.getHotelNote().size() < 1) {
			for (HotelNote note : hotelNotes.getHotelNote()) {
				Instruction instruction =
						Instruction.builder().type(InstructionType.BOOKING_NOTES).msg(note.getText()).build();
				instructionList.add(instruction);
			}
		}
		option.setInstructions(instructionList);
	}


	private HashMap<String, String> getBookRequestMap(Option option) {
		HashMap<String, String> searchRequestmap = new HashMap<>();
		List<RoomInfo> roomList = option.getRoomInfos();
		HashMap<String, String> adultChildMap = setAdultChild();
		List<RoomInfo> roomInfoList = hInfo.getOptions().get(0).getRoomInfos();
		ClientGeneralInfo cInfo = gnComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		searchRequestmap = setBasicRequirement(searchRequestmap);
		searchRequestmap.put("checkInDate", getDate(searchQuery.getCheckinDate()));
		searchRequestmap.put("checkOutDate", getDate(searchQuery.getCheckoutDate()));
		searchRequestmap.put("currency", FitruumsConstant.CURRENCY.getValue());
		searchRequestmap.put("email", cInfo.getAltEmailIds().get(0));
		searchRequestmap.put("roomId", String.valueOf(roomList.get(0).getMiscInfo().getRoomIndex()));
		searchRequestmap.put("rooms", String.valueOf(searchQuery.getRoomInfo().size()));
		searchRequestmap.put("adults", adultChildMap.get("adults"));
		searchRequestmap.put("children", adultChildMap.get("children"));
		searchRequestmap.put("infant", adultChildMap.get("infant"));
		searchRequestmap.put("yourRef", order.getBookingId());
		searchRequestmap.put("specialrequest", "");
		searchRequestmap.put("mealId", roomList.get(0).getMiscInfo().getRatePlanCode());
		searchRequestmap.put("adultGuest1FirstName", "");
		searchRequestmap.put("adultGuest1LastName", "");
		searchRequestmap.put("adultGuest2FirstName", "");
		searchRequestmap.put("adultGuest2LastName", "");
		searchRequestmap.put("adultGuest3FirstName", "");
		searchRequestmap.put("adultGuest3LastName", "");
		searchRequestmap.put("adultGuest4FirstName", "");
		searchRequestmap.put("adultGuest4LastName", "");
		searchRequestmap.put("adultGuest5FirstName", "");
		searchRequestmap.put("adultGuest5LastName", "");
		searchRequestmap.put("adultGuest6FirstName", "");
		searchRequestmap.put("adultGuest6LastName", "");
		searchRequestmap.put("adultGuest7FirstName", "");
		searchRequestmap.put("adultGuest7LastName", "");
		searchRequestmap.put("adultGuest8FirstName", "");
		searchRequestmap.put("adultGuest8LastName", "");
		searchRequestmap.put("adultGuest9FirstName", "");
		searchRequestmap.put("adultGuest9LastName", "");
		searchRequestmap.put("childrenGuest1FirstName", "");
		searchRequestmap.put("childrenGuest1LastName", "");
		searchRequestmap.put("childrenGuestAge1", "");
		searchRequestmap.put("childrenGuest2FirstName", "");
		searchRequestmap.put("childrenGuest2LastName", "");
		searchRequestmap.put("childrenGuestAge2", "");
		searchRequestmap.put("childrenGuest3FirstName", "");
		searchRequestmap.put("childrenGuest3LastName", "");
		searchRequestmap.put("childrenGuestAge3", "");
		searchRequestmap.put("childrenGuest4FirstName", "");
		searchRequestmap.put("childrenGuest4LastName", "");
		searchRequestmap.put("childrenGuestAge4", "");
		searchRequestmap.put("childrenGuest5FirstName", "");
		searchRequestmap.put("childrenGuest5LastName", "");
		searchRequestmap.put("childrenGuestAge5", "");
		searchRequestmap.put("childrenGuest6FirstName", "");
		searchRequestmap.put("childrenGuest6LastName", "");
		searchRequestmap.put("childrenGuestAge6", "");
		searchRequestmap.put("childrenGuest7FirstName", "");
		searchRequestmap.put("childrenGuest7LastName", "");
		searchRequestmap.put("childrenGuestAge7", "");
		searchRequestmap.put("childrenGuest8FirstName", "");
		searchRequestmap.put("childrenGuest8LastName", "");
		searchRequestmap.put("childrenGuestAge8", "");
		searchRequestmap.put("childrenGuest9FirstName", "");
		searchRequestmap.put("childrenGuest9LastName", "");
		searchRequestmap.put("childrenGuestAge9", "");
		searchRequestmap.put("paymentMethodId", FitruumsConstant.PAYMENTMETHODID.getValue());
		searchRequestmap.put("creditCardType", "");
		searchRequestmap.put("creditCardNumber", "");
		searchRequestmap.put("creditCardHolder", "");
		searchRequestmap.put("creditCardCVV2", "");
		searchRequestmap.put("creditCardExpYear", "");
		searchRequestmap.put("creditCardExpMonth", "");
		searchRequestmap.put("customerEmail", "");
		searchRequestmap.put("invoiceRef", "");
		searchRequestmap.put("commissionAmountInHotelCurrency", "");
		searchRequestmap.put("customerCountry", BaseHotelUtils
				.getSpecificFieldFromId(searchQuery.getSearchCriteria().getNationality(), CountryInfoType.CODE.name()));
		searchRequestmap.put("b2c", FitruumsConstant.B2C.getValue());
		searchRequestmap.put("preBookCode", option.getMiscInfo().getBookingCode());
		setGuestsName(roomInfoList, searchRequestmap);
		return searchRequestmap;
	}

	private void setGuestsName(List<RoomInfo> roomInfoList, HashMap<String, String> searchRequestmap) {
		int childIndex = 1;
		int adultIndex = 1;
		for (RoomInfo room : roomInfoList) {
			List<TravellerInfo> travellerInfoList = room.getTravellerInfo();
			for (TravellerInfo traveller : travellerInfoList) {
				if (traveller.getTitle().toUpperCase().equals("MASTER")) {
					if (traveller.getAge() >= 2) {
						String fn = "childrenGuest" + childIndex + "FirstName";
						String ln = "childrenGuest" + childIndex + "LastName";
						String age = "childrenGuestAge" + childIndex;
						searchRequestmap.put(fn, traveller.getFirstName());
						searchRequestmap.put(ln, traveller.getLastName());
						searchRequestmap.put(age, String.valueOf(traveller.getAge()));
						childIndex++;
					} else
						continue;
				} else {
					String adfn = "adultGuest" + adultIndex + "FirstName";
					String adln = "adultGuest" + adultIndex + "LastName";
					searchRequestmap.put(adfn, traveller.getFirstName());
					searchRequestmap.put(adln, traveller.getLastName());
					adultIndex++;
				}
			}
		}
	}
}
