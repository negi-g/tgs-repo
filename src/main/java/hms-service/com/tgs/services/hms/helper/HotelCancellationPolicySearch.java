package com.tgs.services.hms.helper;

import java.util.Arrays;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelCancellationPolicySearch extends AbstractHotelCancellationPolicy {

	@Autowired
	HotelCacheHandler cacheHandler;

	@Override
	public void getCancellationPolicy(HotelSearchQuery searchQuery, HotelInfo hInfo, String optionId, String logKey) {
		if (Objects.isNull(hInfo)) {
			log.info("Option cannot be found for requested optionid {} as hotelinfo is null", optionId);
			return;
		}
		Option option = HotelUtils.filterOptionById(hInfo, optionId);
		if (Objects.isNull(option)) {
			log.info("Option not found for requested optionid {} in hotelid {}", optionId, hInfo.getId());
			return;
		}
		searchQuery.setSourceId(option.getMiscInfo().getSourceId());
		if (Objects.isNull(option.getCancellationPolicy())) {
			HotelInfo copyHInfo = new GsonMapper<>(hInfo, HotelInfo.class).convert();
			option.getRoomInfos().get(0).setCheckInDate(searchQuery.getCheckinDate());
			copyHInfo.setOptions(Arrays.asList(option));
			super.getCancellationPolicy(searchQuery, copyHInfo, optionId, logKey);
			for (int i = 0; i < hInfo.getOptions().size(); i++) {
				Option opt = hInfo.getOptions().get(i);
				if (opt.getId().equals(option.getId())) {
					hInfo.getOptions().set(i, copyHInfo.getOptions().get(0));
				}
			}
			cacheHandler.persistHotelInCache(hInfo, searchQuery);
		}
	}
}
