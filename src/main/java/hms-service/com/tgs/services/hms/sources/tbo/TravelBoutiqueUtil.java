package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PenaltyDetails.PenaltyDetailsBuilder;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomBedType;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomPrice;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.authentication.HotelAuthenticationRequest;
import com.tgs.services.hms.datamodel.tbo.authentication.HotelAuthenticationResponse;
import com.tgs.services.hms.datamodel.tbo.booking.BaseBookingResponse;
import com.tgs.services.hms.datamodel.tbo.mapping.Cities;
import com.tgs.services.hms.datamodel.tbo.mapping.City;
import com.tgs.services.hms.datamodel.tbo.mapping.Countries;
import com.tgs.services.hms.datamodel.tbo.mapping.Country;
import com.tgs.services.hms.datamodel.tbo.search.ArrayOfBasicPropertyInfo.BasicPropertyInfo;
import com.tgs.services.hms.datamodel.tbo.search.BedType;
import com.tgs.services.hms.datamodel.tbo.search.CancellationPolicy;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomDetail;
import com.tgs.services.hms.datamodel.tbo.search.Price;
import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;
import com.tgs.services.hms.datamodel.tbo.staticData.VendorMessages.VendorMessage;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TravelBoutiqueUtil {

	protected static final String IP = "::1";
	protected static final String CURRENCY = "INR";
	protected static final String NATIONALITY = "IN";
	protected static final String CACHE_KEY = "TBO";
	protected static final String TBO_SUPPLIER_ID = "TBO";
	protected static final int AMOUNT = 1;
	protected static final int PERCENTAGE = 2;
	protected static final int NIGHT = 3;

	public static HttpUtils getRequest(TBOBaseRequest request, HotelSupplierConfiguration sConf, String url)
			throws UnknownHostException {

		HttpUtils httpUtils = HttpUtils.builder().urlString(url).postData(GsonUtils.getGson().toJson(request))
				.headerParams(getHeaderParams()).proxy(HotelUtils.getProxyFromConfigurator()).build();
		return httpUtils;
	}


	public static Map<String, String> getHeaderParams() {

		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("content-type", "application/json");
		headerParams.put("Accept-Encoding", "gzip, deflate");
		return headerParams;

	}

	public static String fetchNewToken(HotelSupplierConfiguration supplierConf) throws IOException {

		HotelAuthenticationResponse authResponse = null;
		try {
			HotelAuthenticationRequest authRequest = createAuthenticationRequest(supplierConf);
			log.info("Auth Request is {}", GsonUtils.getGson().toJson(authRequest));
			HttpUtils httpUtils = TravelBoutiqueUtil.getRequest(authRequest, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_AUTH_URL));
			log.info("Auth Response is {}", GsonUtils.getGson().toJson(httpUtils.getResponseString()));
			authResponse = httpUtils.getResponse(HotelAuthenticationResponse.class).orElse(null);
			if (authResponse == null || StringUtils.isBlank(authResponse.getTokenId())) {
				throw new CustomGeneralException(SystemError.AUTHENTICATION_FAILED,
						authResponse.getError().getErrorMessage());
			}
		} catch (Exception e) {
			log.error("Exception During Authentication for supplier {}",
					supplierConf.getHotelSupplierCredentials().getUserName());
			throw e;
		}
		return authResponse.getTokenId();
	}


	private static HotelAuthenticationRequest createAuthenticationRequest(HotelSupplierConfiguration sConf)
			throws UnknownHostException {

		HotelAuthenticationRequest authRequest = new HotelAuthenticationRequest();
		authRequest.setPassword(sConf.getHotelSupplierCredentials().getPassword());
		authRequest.setUserName(sConf.getHotelSupplierCredentials().getUserName());
		authRequest.setClientId(sConf.getHotelSupplierCredentials().getClientId());
		authRequest.setEndUserIp(TravelBoutiqueUtil.IP);
		return authRequest;

	}


	public static HotelCancellationPolicy getCancellationPolicy(HotelRoomDetail roomDetail, RoomInfo roomInfo) {

		List<PenaltyDetails> penaltyDetails = new ArrayList<>();
		for (CancellationPolicy cancellationPolicy : roomDetail.getCancellationPolicies()) {
			PenaltyDetailsBuilder pd = PenaltyDetails.builder();
			if (cancellationPolicy.getChargeType() == AMOUNT) {
				pd.penaltyAmount(cancellationPolicy.getCharge());
			} else if (cancellationPolicy.getChargeType() == PERCENTAGE) {
				pd.penaltyPercent(cancellationPolicy.getCharge());
				double totalSupplierPrice = roomInfo.getPerNightPriceInfos().stream()
						.mapToDouble(perNightPrice -> perNightPrice.getFareComponents().get(HotelFareComponent.BF))
						.sum();
				pd.penaltyAmount((cancellationPolicy.getCharge() * totalSupplierPrice) / 100);
			} else if (cancellationPolicy.getChargeType() == NIGHT) {
				pd.penaltyRoomNights(cancellationPolicy.getCharge().intValue());
				pd.penaltyAmount(roomInfo.getPerNightPriceInfos().get(0).getFareComponents().get(HotelFareComponent.BF)
						* cancellationPolicy.getCharge().intValue());
			}
			PenaltyDetails penaltyDetail =
					pd.fromDate(cancellationPolicy.getFromDate()).toDate(cancellationPolicy.getToDate()).build();
			penaltyDetails.add(penaltyDetail);
		}
		HotelCancellationPolicy hotelCancellationPolicy = HotelCancellationPolicy.builder()
				.miscInfo(CancellationMiscInfo.builder().isCancellationPolicyBelongToRoom(true).build())
				.id(roomInfo.getId()).penalyDetails(penaltyDetails)
				.cancellationPolicy(roomDetail.getCancellationPolicy()).build();
		return hotelCancellationPolicy;
	}

	public static void updateHotelInfo(HotelInfo hInfo, BaseBookingResponse baseResponse,
			HotelSourceConfigOutput sourceConfigOutput) {

		Map<String, RoomInfo> roomInfoMap = new HashMap<>();
		List<RoomInfo> roomInfos = hInfo.getOptions().get(0).getRoomInfos();
		for (RoomInfo roomInfo : roomInfos) {
			roomInfoMap.put(roomInfo.getMiscInfo().getRoomIndex().toString(), roomInfo);
		}

		if (baseResponse.isCancellationPolicyChanged()) {
			updateCancellationPolicyInRooms(baseResponse, roomInfoMap);
			hInfo.getOptions().get(0).setDeadlineDateTime(getFullRefundCancellationDeadlineDatetime(roomInfos));
		}
		if (baseResponse.isPriceChanged()) {
			updateHotelPrice(hInfo, baseResponse, roomInfoMap);
			updatePriceWithMarkup(hInfo.getOptions().get(0).getRoomInfos(), sourceConfigOutput);
		}
	}


	public static LocalDateTime getFullRefundCancellationDeadlineDatetime(List<RoomInfo> roomInfos) {

		LocalDateTime lastCancellationDateTime = roomInfos.get(0).getDeadlineDateTime();
		for (int i = 1; i < roomInfos.size(); i++) {
			RoomInfo roomInfo = roomInfos.get(i);
			if (lastCancellationDateTime.isBefore(roomInfo.getDeadlineDateTime())) {
				lastCancellationDateTime = roomInfo.getDeadlineDateTime();
			}
		}
		return lastCancellationDateTime;

	}

	private static void updateCancellationPolicyInRooms(BaseBookingResponse priceValidationResponse,
			Map<String, RoomInfo> roomInfoMap) {

		for (HotelRoomDetail roomDetail : priceValidationResponse.getHotelRoomDetails()) {

			RoomInfo roomInfo = roomInfoMap.get(roomDetail.getRoomIndex().toString());
			roomInfo.setCancellationPolicy(getCancellationPolicy(roomDetail, roomInfo));
			roomInfo.getCancellationPolicy().setId(roomInfo.getId());
			roomInfo.setDeadlineDateTime(roomDetail.getLastCancellationDate());
		}
	}

	public static List<PriceInfo> getPriceInfos(HotelRoomDetail roomDetail, int numberOfNights) {

		List<PriceInfo> priceInfos = new ArrayList<>();
		Price supplierPriceInfo = roomDetail.getPrice();
		Double totalTaxesAndFees = supplierPriceInfo.getPublishedPriceRoundedOff() - supplierPriceInfo.getRoomPrice();
		Double perNightAgentCommission = supplierPriceInfo.getAgentCommission() / numberOfNights;
		Double perRoomNightTaxAndFees = totalTaxesAndFees / numberOfNights;
		Double perNightTotalRoomPrice = supplierPriceInfo.getPublishedPrice() / numberOfNights;
		// for (DayRate dayRate : roomDetail.getDayRates())
		for (int i = 1; i <= numberOfNights; i++) {

			PriceInfo priceInfo = new PriceInfo();
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, perNightTotalRoomPrice);
			fareComponents.put(HotelFareComponent.TAF, perRoomNightTaxAndFees);
			fareComponents.put(HotelFareComponent.SAC, perNightAgentCommission);

			priceInfo.setFareComponents(fareComponents);
			priceInfo.setDay(i);
			priceInfo.setTotalPrice(perNightTotalRoomPrice);
			priceInfos.add(priceInfo);
		}
		return priceInfos;
	}


	private static void updateHotelPrice(HotelInfo hInfo, BaseBookingResponse priceValidationResponse,
			Map<String, RoomInfo> roomInfoMap) {

		for (HotelRoomDetail roomDetail : priceValidationResponse.getHotelRoomDetails()) {
			RoomInfo roomInfo = roomInfoMap.get(roomDetail.getRoomIndex().toString());
			int numberOfNights = (int) ChronoUnit.DAYS.between(roomInfo.getCheckInDate(), roomInfo.getCheckOutDate());

			roomInfo.setPerNightPriceInfos(getPriceInfos(roomDetail, numberOfNights));
			roomInfo.setTotalPrice(roomDetail.getPrice().getPublishedPrice());
		}

	}

	public static List<Country> getCountryListFromXML(String xml) throws Exception {

		JAXBContext jaxbContext = JAXBContext.newInstance(Countries.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		Countries countries = (Countries) jaxbUnmarshaller.unmarshal(reader);
		return countries.getCountry();

	}


	public static List<City> getCityListFromXML(String xml) throws Exception {

		JAXBContext jaxbContext = JAXBContext.newInstance(Cities.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		Cities cities = (Cities) jaxbUnmarshaller.unmarshal(reader);
		return cities.getCity();

	}

	public static RoomPrice getRoomPrice(Price price) {

		return new GsonMapper<>(price, RoomPrice.class).convert();
	}

	public static Price getSupplierPrice(RoomPrice price) {

		return new GsonMapper<>(price, Price.class).convert();

	}

	public static List<RoomBedType> getBedTypes(List<BedType> bedTypes) {

		List<RoomBedType> roomBedTypes = new ArrayList<>();
		for (BedType bedType : bedTypes) {
			RoomBedType roomBedType = new GsonMapper<>(bedType, RoomBedType.class).convert();
			roomBedTypes.add(roomBedType);
		}
		return roomBedTypes;

	}

	public static void updatePriceWithMarkup(List<RoomInfo> rInfoList, HotelSourceConfigOutput sourceConfigOutput) {

		double supplierMarkup =
				sourceConfigOutput.getSupplierMarkup() == null ? 0.0 : sourceConfigOutput.getSupplierMarkup();

		for (RoomInfo roomInfo : rInfoList) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				double supplierGrossPrice = priceInfo.getFareComponents().get(HotelFareComponent.BF);
				priceInfo.getFareComponents().put(HotelFareComponent.CMU, (supplierGrossPrice * supplierMarkup) / 100);
				priceInfo.getFareComponents().put(HotelFareComponent.SGP, supplierGrossPrice);
				priceInfo.getFareComponents().put(HotelFareComponent.SNP, supplierGrossPrice);
				priceInfo.getFareComponents().put(HotelFareComponent.BF, supplierGrossPrice);

			}
		}
	}
	
	public static VendorMessage getVendorMessageFromProperty(BasicPropertyInfo basicPropertyInfo, String key) {
		List<VendorMessage> vendorMessages = basicPropertyInfo.getVendorMessages().getVendorMessage();
		return vendorMessages.stream()
				.filter(vendorMessage -> vendorMessage.getTitle().equalsIgnoreCase(key))
				.findFirst().orElse(null);
	}
}
