package com.tgs.services.hms.servicehandler;

import com.tgs.services.hms.restmodel.HotelPriceValidationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;

@Service
public class HotelPriceValidateHandler extends ServiceHandler<HotelPriceValidationRequest, HotelReviewResponse>  {

	@Autowired
	HotelCacheHandler cacheHandler;
	
	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws Exception {
		
		HotelReviewResponse reviewResponse = cacheHandler.getHotelReviewFromCache(request.getBookingId());
		AbstractHotelPriceValidationFactory priceValidationFactory = HotelSourceType
				.getHotelPriceValidationFactoryInstance(reviewResponse.getQuery() , reviewResponse.getHInfo(), reviewResponse.getBookingId());
		priceValidationFactory.getUpdatedOption();
		
		
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
