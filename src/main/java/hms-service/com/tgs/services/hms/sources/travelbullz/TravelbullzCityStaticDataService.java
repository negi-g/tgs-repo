package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.travelbullz.City;
import com.tgs.services.hms.datamodel.travelbullz.Country;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@SuperBuilder
@Slf4j
@Service
public class TravelbullzCityStaticDataService extends TravelbullzBaseService {

	private Country[] countryResponseList;
	private TravelbullzRequest staticRequest;
	private City[] cityResponse;

	public List<HotelRegionInfoQuery> getTravelbullzCityList() throws IOException {
		HttpUtilsV2 httpUtils = null;
		String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
		String urlString = StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.COUNTRY_INFO));
		staticRequest = getCountryRequest();
		httpUtils = getHttpUtils(GsonUtils.getGson().toJson(staticRequest), urlString);
		countryResponseList = httpUtils.getResponse(Country[].class).orElse(null);

		List<HotelRegionInfoQuery> cityList = getTravelbullzCityListByCountry(countryResponseList);

		return cityList;
	}

	public List<HotelRegionInfoQuery> getTravelbullzCityListByCountry(Country[] countryResponseList2) {
		List<HotelRegionInfoQuery> regionInfoQueries = new ArrayList<>();
		HttpUtilsV2 httpUtils = null;
		String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
		String urlString = StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.CITY_INFO));
		for (Country country : countryResponseList2) {
			staticRequest = getCityRequest(country.getCountryId());
			try {
				httpUtils = getHttpUtils(GsonUtils.getGson().toJson(staticRequest), urlString);
				cityResponse = httpUtils.getResponse(City[].class).orElse(null);

			} catch (IOException e) {
				log.info("Error in fetching cities for country {}", country.getName());
			}
			log.info("Total cities in country : {} is {}", country.getName(), cityResponse.length);
			for (City city : cityResponse) {
				HotelRegionInfoQuery regionInfoQuery = new HotelRegionInfoQuery();
				regionInfoQuery.setRegionId(city.getCityId() + "");
				regionInfoQuery.setRegionName(city.getName());
				regionInfoQuery.setRegionType("CITY");
				regionInfoQuery.setCountryId(country.getCountryId());
				regionInfoQuery.setCountryName(country.getName());
				regionInfoQuery.setSupplierName(HotelSourceType.TRAVELBULLZ.name());
				regionInfoQueries.add(regionInfoQuery);
			}
		}
		log.info("Total cities of TRAVLEBULLZ are :" + regionInfoQueries.size());
		return regionInfoQueries;
	}
}
