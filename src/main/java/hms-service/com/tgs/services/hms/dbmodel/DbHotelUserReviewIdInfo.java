package com.tgs.services.hms.dbmodel;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.HotelUserReviewIdInfo;
import com.tgs.services.hms.datamodel.UserReviewStaticData;
import com.tgs.services.hms.datamodel.tripadvisor.ReviewData;
import com.tgs.services.hms.dbmodel.tripadvisor.ReviewDataType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Getter
@Setter
@TypeDefs({@TypeDef(name = "StaticDataType", typeClass = StaticDataType.class),
	@TypeDef(name = "ReviewDataType", typeClass = ReviewDataType.class)})
@Table(name = "userreviewidinfo", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "key","supplierId" }) })
public class DbHotelUserReviewIdInfo extends BaseModel<DbHotelUserReviewIdInfo, HotelUserReviewIdInfo>{

	private String key;
	private String reviewId;
	private String hotelId;
	private String supplierId;
	private Integer rating;
	private String city;
	
	@Type(type = "ReviewDataType")
	private List<ReviewData> reviewDataList;
	@Type(type = "StaticDataType")
	private UserReviewStaticData staticData;
	
	@Override
	public HotelUserReviewIdInfo toDomain() {
		return new GsonMapper<>(this, HotelUserReviewIdInfo.class).convert();
	}

	@Override
	public DbHotelUserReviewIdInfo from(HotelUserReviewIdInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbHotelUserReviewIdInfo.class).convert();
	}
	
}
