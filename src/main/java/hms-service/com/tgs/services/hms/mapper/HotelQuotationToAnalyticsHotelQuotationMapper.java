package com.tgs.services.hms.mapper;

import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.hms.analytics.AnalyticsHotelQuery;
import com.tgs.services.hms.datamodel.HotelDetailResult;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierBasicInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelQuotationToAnalyticsHotelQuotationMapper extends Mapper<AnalyticsHotelQuery> {

	private HotelInfo hInfo;
	private HotelSearchQuery searchQuery;
	private User user;
	private ContextData contextData;
	private String quotationName;
	private String optionId;
	private boolean isRoomFacilityAvailable;
	@Builder.Default
	private Set<String> mealBasis = new HashSet<>();
	@Builder.Default
	private Set<String> roomCategory = new HashSet<>();

	@Override
	protected void execute() throws CustomGeneralException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		output = output != null ? output : AnalyticsHotelQuery.builder().build();
		HotelSupplierBasicInfo basicInfo = null;
		if (hInfo != null) {
			basicInfo = HotelSupplierBasicInfo.builder().sourceId(hInfo.getOptions().get(0).getMiscInfo().getSourceId())
					.supplierName(hInfo.getOptions().get(0).getMiscInfo().getSupplierId()).build();
		}
		output = HotelDetailToAnalyticsHotelDetailMapper.builder()
				.detailResult(HotelDetailResult.builder().hotel(hInfo).searchQuery(searchQuery).build()).user(user)
				.contextData(contextData).basicInfo(basicInfo).build().setOutput(output).convert();
		output.setAnalyticstype(HotelFlowType.QUOTATION.name());
		output.setQuotationname(quotationName);
		if (hInfo != null && hInfo.getOptions() != null) {
			List<Option> options = hInfo.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(options)) {
				Option option = options.get(0);
				output.setIsonrequest(option.getIsOptionOnRequest());
				if (!ObjectUtils.isEmpty(option.getCancellationPolicy())) {
					output.setIszerocancellationallowed(option.getCancellationPolicy().getIsFullRefundAllowed());
				}
				if (!ObjectUtils.isEmpty(option.getDeadlineDateTime()))
					output.setExpirationdate(option.getDeadlineDateTime().format(formatter));
				populateOptionSpecificInfo(option);
			}
			output.setMealbasis(mealBasis.toString());
			output.setRoomcategory(roomCategory.toString());
		}
		output.setRoomfacilityavail(BooleanUtils.isTrue(isRoomFacilityAvailable));
		if (CollectionUtils.isNotEmpty(contextData.getErrorMessages())) {
			output.setErrormsg(contextData.getErrorMessages().toString());
		}
		filterQuotationOutput();
	}

	private void filterQuotationOutput() {
		output.setSearchresultcount(null);
		output.setNumberofadults(null);
		output.setNumberofchild(null);
		output.setOptionscount(null);
	}

	private void populateOptionSpecificInfo(Option option) {
		if (!ObjectUtils.isEmpty(option)) {
			option.getRoomInfos().stream().forEach(roomInfo -> {
				if (CollectionUtils.isNotEmpty(roomInfo.getRoomAmenities())) {
					isRoomFacilityAvailable = true;
				}
				roomCategory.add(roomInfo.getRoomCategory());
				mealBasis.add(roomInfo.getMealBasis());
			});
		}
	}
}
