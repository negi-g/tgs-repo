package com.tgs.services.hms.dbmodel;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.qtech.AdditionalHotelIdMappingInfo;
import com.tgs.services.hms.datamodel.qtech.SupplierHotelIdMapping;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Table(name = "supplierhotelidmapping")
@TypeDefs(@TypeDef(name = "AdditionalHotelIdMappingInfoType", typeClass = AdditionalHotelIdMappingInfoType.class))
public class DbSupplierHotelIdMapping extends BaseModel<DbSupplierHotelIdMapping, SupplierHotelIdMapping> {

	@SerializedName("hid")
	private String hotelId;

	@SerializedName("shid")
	private String supplierHotelId;

	@SerializedName("sn")
	private String supplierName;

	@Type(type = "AdditionalHotelIdMappingInfoType")
	@SerializedName("addinfo")
	private AdditionalHotelIdMappingInfo additionalInfo;

	public SupplierHotelIdMapping toDomain() {

		return new GsonMapper<>(this, SupplierHotelIdMapping.class).convert();
	}

	public DbSupplierHotelIdMapping from(SupplierHotelIdMapping dataModel) {
		return new GsonMapper<>(dataModel, this, DbSupplierHotelIdMapping.class).convert();
	}

}
