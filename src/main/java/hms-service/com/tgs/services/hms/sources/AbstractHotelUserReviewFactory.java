package com.tgs.services.hms.sources;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUserReview;
import com.tgs.services.hms.datamodel.HotelUserReviewIdInfo;
import com.tgs.services.hms.dbmodel.DbHotelUserReviewIdInfo;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.jparepository.HotelUserReviewService;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractHotelUserReviewFactory {
	
	
	@Autowired
	HotelCacheHandler cacheHandler;
	
	@Autowired
	HotelUserReviewService userReviewService;
	
	protected abstract void searchHotelReview(HotelUserReview hReview);
	protected abstract void searchIdFromNameLatLong(HotelInfo hInfo ,
			HotelUserReviewIdInfo hUserReviewSupplierIdInfo) throws Exception;
	
	public HotelUserReview getHotelReview(HotelInfo hInfo) {
		HotelUserReview hReview = cacheHandler.getHotelUserReviewById(hInfo.getUserReviewSupplierId());
		if(hReview != null) {
			hReview.setHotelId(hInfo.getId());
			return hReview;
		}
		hReview = new HotelUserReview();
		hReview.setId(hInfo.getUserReviewSupplierId());
		hReview.setHotelId(hInfo.getId());
		this.searchHotelReview(hReview);
		cacheHandler.cacheHotelUserReview(hReview);
		return hReview;
	}
	
	public void getUserReviewIdFromNameLatLong(HotelInfo hInfo) {
		
		HotelUserReviewIdInfo hUserReviewSupplierIdInfo = new HotelUserReviewIdInfo();
		String key = HotelUtils.getKeyForUserReview(hInfo);
		String blacklistedHotel = cacheHandler.getBlacklistedUserReviewHotelDetail(key);
		try {
			if(blacklistedHotel == null) {
				this.searchIdFromNameLatLong(hInfo, hUserReviewSupplierIdInfo);
				DbHotelUserReviewIdInfo userReviewIdInfo = HotelUtils.getDbHotelUserReviewInfo(hInfo);
				userReviewIdInfo.setReviewDataList(hUserReviewSupplierIdInfo.getReviewDataList());
				userReviewIdInfo.setStaticData(hUserReviewSupplierIdInfo.getStaticData());
				if(StringUtils.isBlank(hUserReviewSupplierIdInfo.getReviewId())) {
					userReviewIdInfo.setReviewId(null);
					cacheHandler.storeBlacklistedUserReviewHotelDetail(key);
					userReviewService.save(userReviewIdInfo);
					log.info("Stored Blacklisted Hotel For Key {}", key);
				}else {
					cacheHandler.storeUserReviewIdWithKey(key, hUserReviewSupplierIdInfo.getReviewId());
					userReviewService.save(userReviewIdInfo);
				}
			}
		}catch(Exception e) {
			log.error("Error While Searching For User Review Id For Key {}", key, e);
		}
	}

}
