package com.tgs.services.hms.sources.qtech;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.math.NumberUtils;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.AdditionalHotelIdMappingInfo;
import com.tgs.services.hms.datamodel.qtech.SupplierHotelIdMapping;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.dbmodel.DbSupplierHotelIdMapping;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.SupplierHotelIdMappingSaveManager;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Setter
@Getter
public class QtechStaticDataService {
	private HotelSourceConfigOutput sourceConfig;
	private HotelSupplierConfiguration supplierConf;
	private HotelStaticDataRequest staticDataRequest;
	private HotelStaticDataService staticDataService;
	private SupplierHotelIdMappingSaveManager hotelidMappingSaveManager;
	private int errorRecords = 0;
	private int updatedRecords = 0;
	private int savedRecords = 0;

	public void init() {
		staticDataService = (HotelStaticDataService) HotelUtils.getStaticDataService();
		hotelidMappingSaveManager = (SupplierHotelIdMappingSaveManager) SpringContext.getApplicationContext()
				.getBean("supplierHotelIdMappingSaveManager");

	}

	public void savehotelIdMapping() throws Exception {
		try {
			String filePath = getFilePath();
			saveDataFromCSV(filePath);
		} finally {
			log.info("Saved hotel id mapping for request {}", GsonUtils.getGson().toJson(staticDataRequest));
		}
	}

	private String getFilePath() {
		return sourceConfig.getFilePath();
	}

	private void saveDataFromCSV(String filePath) throws IOException {
		int duplicateRecords = 0;
		int emptyOrInvalidRecords = 0;
		int cityAndCountryIdMissingRecords = 0;
		Map<String, String> hotelidAsKeySupplierHotelIdasValue = new HashMap<>();
		log.info("FilePath is {}", filePath);
		File file = new File(filePath);
		log.info("FileObject name is {}, size is {}", file.getName(), file.getTotalSpace());
		LineIterator it = FileUtils.lineIterator(file);
		try {
			while (it.hasNext()) {
				List<String> csvEntryList = HotelUtils.splitByCommasNotInQuotes(it.nextLine());
				if (Objects.nonNull(csvEntryList) && csvEntryList.size() > 1 && csvEntryList.get(0).startsWith("QT")
						&& NumberUtils.isCreatable(csvEntryList.get(1))) {
					if (!hotelidAsKeySupplierHotelIdasValue.containsKey(csvEntryList.get(1))) {
						try {
							AdditionalHotelIdMappingInfo addInfo = AdditionalHotelIdMappingInfo.builder()
									.supplierCountry(getStringValue(csvEntryList.get(5)))
									.supplierCity(getStringValue(csvEntryList.get(3))).build();
							SupplierHotelIdMapping supplierHotelIdMapping = SupplierHotelIdMapping.builder()
									.hotelId(getStringValue(csvEntryList.get(1))).additionalInfo(addInfo)
									.supplierHotelId(getStringValue(csvEntryList.get(0)))
									.supplierName(HotelSourceType.QTECH.name()).build();
							hotelidAsKeySupplierHotelIdasValue.put(getStringValue(csvEntryList.get(1)),
									getStringValue(csvEntryList.get(0)));
							saveOrUpdateHotelIdMapping(supplierHotelIdMapping);
						} catch (Exception e) {
							cityAndCountryIdMissingRecords++;
							log.info("Exception in accessing csv record due to missing fields. Record is {}",
									csvEntryList);
						}
					} else {
						duplicateRecords++;
					}
				} else {
					emptyOrInvalidRecords++;
				}
			}
		} finally {
			log.info(
					"Finished saving data for request {}. Total skipped record due to empty/invalid records are {}, due to duplicate records are {}, records failed to save due to missing cityid {},records failed to save due to dbexception are {}, updated records are {}, saved records are {}",
					GsonUtils.getGson().toJson(staticDataRequest), emptyOrInvalidRecords, duplicateRecords,
					cityAndCountryIdMissingRecords, errorRecords, updatedRecords, savedRecords);
		}
	}

	public String getStringValue(Object obj) {
		return Objects.isNull(obj) ? null : obj.toString();
	}

	private void saveOrUpdateHotelIdMapping(SupplierHotelIdMapping supplierHotelIdMapping) {
		boolean isRecordSaved = false;
		try {
			DbSupplierHotelIdMapping dbHotelIdMapping = new DbSupplierHotelIdMapping().from(supplierHotelIdMapping);
			isRecordSaved = hotelidMappingSaveManager.saveHotelIdMapping(dbHotelIdMapping, staticDataService);
			if (isRecordSaved) {
				savedRecords++;
			} else {
				updatedRecords++;
			}
		} catch (Exception e) {
			errorRecords++;
			log.info("Error while saving/updating hotelid mapping {}",
					GsonUtils.getGson().toJson(supplierHotelIdMapping), e);
		}

	}
}
