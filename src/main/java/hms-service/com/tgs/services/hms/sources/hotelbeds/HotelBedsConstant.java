package com.tgs.services.hms.sources.hotelbeds;

public enum HotelBedsConstant {

	FACILITY_URL("/hotel-content-api/1.0/types/facilities"),

	IMAGEBASE_URL("https://photos.hotelbeds.com/giata/"),
	
	LANGUAGE("ENG"),
	
	FIELDS("all");
	
	public String value;

	HotelBedsConstant(String val) {
		this.value = val;
	}
}
