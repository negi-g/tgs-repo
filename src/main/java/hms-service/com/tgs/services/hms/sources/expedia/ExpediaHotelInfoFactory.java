package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.aerospike.client.query.Filter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelWiseSearchInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.expedia.ExpediaAdditionalSearchParams;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Setter
@Slf4j
public class ExpediaHotelInfoFactory extends AbstractHotelInfoFactory {

	private Set<String> propertyIds;

	public ExpediaHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException {

		if (StringUtils.isNotBlank(searchQuery.getSearchPreferences().getHotelId())
				&& Objects.isNull(searchQuery.getMiscInfo().getHotelWiseSearchInfo())) {
			log.info("Not able to search for searchid {} and supplier {} as hotel wise search info is not present",
					searchQuery.getSearchId(), supplierConf.getBasicInfo().getSupplierName());
			return;
		}

		String cityName = StringUtils.isNotBlank(supplierRegionInfo.getSupplierRegionName())
				? supplierRegionInfo.getSupplierRegionName()
				: searchQuery.getSearchCriteria().getCityName();
		searchResult = HotelSearchResult.builder().build();
		propertyIds = getPropertyIdsOfCity(cityName);
		Future<?> hotelOnlySearchResultTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				HotelSearchResult hotelOnlySearchResult = getResultOfAllPropertyIds(propertyIds, null);
				return hotelOnlySearchResult;
			} catch (IOException e) {
				log.error("IOException while fetching hotel only search result for search query {} ", this.searchQuery,
						e);
				throw new CustomGeneralException("Unable to fetch hotel only search result due to " + e.getMessage());
			}
		});

		Future<?> packageOnlySearchResultTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				HotelSearchResult packageOnlySearchResult = searchPackageOnlyAvailableHotels();
				return packageOnlySearchResult;
			} catch (IOException e) {
				log.error("IOException while fetching package only search result for search query {} ",
						this.searchQuery, e);
				throw new CustomGeneralException("Unable to fetch package only search result due to " + e.getMessage());
			}

		});
		try {
			Object hotelOnlySearchResultObj = hotelOnlySearchResultTask.get();
			Object packageOnlySearchResultObj = packageOnlySearchResultTask.get();
			if (Objects.nonNull(hotelOnlySearchResultObj) && Objects.nonNull(packageOnlySearchResultObj)) {
				HotelSearchResult hotelOnlySearchResult = (HotelSearchResult) hotelOnlySearchResultObj;
				HotelSearchResult packageOnlySearchResult = (HotelSearchResult) packageOnlySearchResultObj;
				searchResult = mergePackageAndNonPackageResult(hotelOnlySearchResult, packageOnlySearchResult);
				log.debug("Total number of hotels after merging for {} are {}",
						supplierConf.getBasicInfo().getSupplierName(), searchResult.getNoOfHotelOptions());
			}
		} catch (InterruptedException | ExecutionException e) {
			log.error("Interrupted exception while fetching search result {} ", this.searchQuery, e);
			return;
		}
	}

	public HotelSearchResult searchPackageOnlyAvailableHotels() throws IOException {
		propertyIds = CollectionUtils.isEmpty(propertyIds)
				? getPropertyIdsOfCity(StringUtils.isNotBlank(supplierRegionInfo.getSupplierRegionName())
						? supplierRegionInfo.getSupplierRegionName()
						: searchQuery.getSearchCriteria().getCityName())
				: propertyIds;
		return getResultOfAllPropertyIds(propertyIds, true);
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String bookingId) throws IOException {
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option option = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		hotel.getOptions().forEach(op -> {
			if (op.getId().equals(optionId)) {
				op.setCancellationPolicy(option.getCancellationPolicy());
			}
		});
	}

	private HotelSearchResult getResultOfAllPropertyIds(Set<String> propertyIds, Boolean isPackageOnly)
			throws IOException {
		List<Set<String>> listOfPartitionedIdsSet = HotelUtils.partitionSet(propertyIds, 250);
		Integer maxLimit = Objects.isNull(sourceConfigOutput.getPartitionHotelIdsLimit()) ? 30
				: sourceConfigOutput.getPartitionHotelIdsLimit();
		if (listOfPartitionedIdsSet.size() > maxLimit) {
			throw new CustomGeneralException(SystemError.MAXIMUM_PARTITIONSET_LIMIT_EXCEEDED);
		}
		AtomicInteger threadCount = new AtomicInteger(0);
		HotelSearchResult finalSearchResult = HotelSearchResult.builder().build();
		List<Future<?>> hotelSearchFutureTaskList = new ArrayList<Future<?>>();
		listOfPartitionedIdsSet.forEach(partitionedIdsSet -> {
			Future<?> hotelSearchFutureTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
				try {
					ExpediaAdditionalSearchParams searchParams = ExpediaAdditionalSearchParams.builder()
							.country_code(HotelUtils.getCountryCode()).language(HotelUtils.getLanguage())
							.property_id(partitionedIdsSet).isPackageRate(isPackageOnly).build();

					ExpediaSearchService searchService = ExpediaSearchService.builder()
							.supplierConf(this.getSupplierConf()).searchQuery(this.getSearchQuery())
							.additionalSearchParams(searchParams).sourceConfigOutput(sourceConfigOutput).build();
					searchService.doSearch(threadCount.incrementAndGet());
					return searchService.getSearchResult();
				} catch (IOException e) {
					log.error("IOException while fetching search result for search query {} ", this.searchQuery, e);
					throw new CustomGeneralException("Unable to fetch search result due to " + e.getMessage());
				} finally {
					LogUtils.clearLogList();
				}
			});
			hotelSearchFutureTaskList.add(hotelSearchFutureTask);
		});

		for (Future<?> future : hotelSearchFutureTaskList) {
			try {
				Object result = future.get();
				if (Objects.nonNull(result)) {
					HotelSearchResult searchResult = (HotelSearchResult) result;
					finalSearchResult.getHotelInfos().addAll(searchResult.getHotelInfos());
				}
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while fetching search result {} ", this.searchQuery, e);
				return null;
			}
		}
		return finalSearchResult;
	}

	protected Set<String> getPropertyIdsOfCity(String cityName) {

		HotelWiseSearchInfo hotelWiseSearchInfo = searchQuery.getMiscInfo().getHotelWiseSearchInfo();
		if (Objects.nonNull(hotelWiseSearchInfo)) {
			return new HashSet<>(Arrays.asList(hotelWiseSearchInfo.getHotelId()));
		}
		log.info("Fetching property id from cache for city {} and searchid {}", cityName, searchQuery.getSearchId());
		Set<String> propertyIds = new HashSet<>();
		try {
			HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();
			String polygonInfo = getPolygonInfo();
			String supplierName = HotelSourceType.EXPEDIA.name();
			if (StringUtils.isNotBlank(polygonInfo)) {
				propertyIds = fetchPropertyIdsByPolygonInfo(supplierName, polygonInfo, staticDataService);
				return propertyIds;
			} else if (!cityName.contains("@")) {
				cityName = staticDataService.getCityName(supplierName, cityName.toLowerCase());
				CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder()
						.namespace(CacheNameSpace.HOTEL.getName())
						.set(staticDataService.getSupplierSetName(supplierName))
						.bins(new String[] {BinName.CITY.name(), BinName.RATING.name()}).plainData(true).build();
				Map<String, Map<String, String>> supplierhotelIdMap = cachingCommunicator.getResultSet(
						supplierMappingMetaInfo, String.class, Filter.equal(BinName.CITY.getName(), cityName));
				for (Map.Entry<String, Map<String, String>> supplierHotel : supplierhotelIdMap.entrySet()) {
					Map<String, String> supplierInfo = supplierHotel.getValue();
					if (StringUtils.isNotBlank(supplierInfo.get(BinName.RATING.name()))
							&& searchQuery.getSearchPreferences().getRatings()
									.contains(Integer.parseInt(supplierInfo.get(BinName.RATING.name())))) {
						propertyIds.add(supplierHotel.getKey());
					}
				}
				if (StringUtils.isNotBlank(staticDataService.getSeparator())) {
					return HotelUtils.removePrefix(propertyIds, supplierName + staticDataService.getSeparator());
				}
				return propertyIds;
			} else {
				propertyIds =
						fetchPropertyIdsBySupplierCityName(HotelSourceType.EXPEDIA.name(), cityName, staticDataService);
				return propertyIds;
			}
		} catch (Exception e) {
			log.info("Unable to fetch property ids of city {}", cityName, e);
			return null;
		} finally {
			log.info("Fetched property id from cache for city {} and searchid {}. Size is {}", cityName,
					searchQuery.getSearchId(), propertyIds.size());
		}
	}

	private HotelSearchResult mergePackageAndNonPackageResult(HotelSearchResult hotelOnlySearchResult,
			HotelSearchResult packageOnlySearchResult) {
		HotelSearchResult mergedHotelSearchResult = HotelSearchResult.builder().build();
		List<HotelInfo> mergedHotels = new ArrayList<>();

		Map<String, HotelInfo> groupByPackageOnlyResult =
				hotelOnlySearchResult.getHotelInfos().stream().collect(Collectors.toMap(
						hotelInfo -> hotelInfo.getMiscInfo().getSupplierStaticHotelId(), hotelInfo -> hotelInfo));
		Map<String, HotelInfo> groupByHotelOnlyResult =
				packageOnlySearchResult.getHotelInfos().stream().collect(Collectors.toMap(
						hotelInfo -> hotelInfo.getMiscInfo().getSupplierStaticHotelId(), hotelInfo -> hotelInfo));

		Set<String> hotelOnlyCombinedHotels = new HashSet<>();
		groupByPackageOnlyResult.forEach((key, hInfo) -> {
			HotelInfo hotelOnlyResult = groupByHotelOnlyResult.get(key);
			if (Objects.nonNull(hotelOnlyResult)) {
				hInfo.getOptions().addAll(hotelOnlyResult.getOptions());
				hotelOnlyCombinedHotels.add(key);
			}
		});
		mergedHotels.addAll(groupByPackageOnlyResult.values());

		groupByHotelOnlyResult.forEach((key, hInfo) -> {
			if (!hotelOnlyCombinedHotels.contains(key)) {
				mergedHotels.add(hInfo);
			}
		});

		mergedHotelSearchResult.getHotelInfos().addAll(mergedHotels);
		return mergedHotelSearchResult;
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException {

		Set<String> propertyIds = new HashSet<>();
		propertyIds.add(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());

		Future<?> hotelOnlySearchResultTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				ExpediaAdditionalSearchParams searchParams =
						ExpediaAdditionalSearchParams.builder().country_code(HotelUtils.getCountryCode())
								.language(HotelUtils.getLanguage()).property_id(propertyIds).build();

				ExpediaSearchService searchService = ExpediaSearchService.builder().supplierConf(this.getSupplierConf())
						.searchQuery(this.getSearchQuery()).additionalSearchParams(searchParams)
						.sourceConfigOutput(sourceConfigOutput).build();
				searchService.doDetailSearch();
				return searchService.getSearchResult();
			} catch (IOException e) {
				log.error("IOException while fetching detail hotel info for search query {} ", this.searchQuery, e);
				throw new CustomGeneralException("Unable to fetch detail hotel info due to " + e.getMessage());
			} finally {
				LogUtils.clearLogList();
			}
		});

		Future<?> packageOnlySearchResultTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				ExpediaAdditionalSearchParams searchParams = ExpediaAdditionalSearchParams.builder()
						.country_code(HotelUtils.getCountryCode()).language(HotelUtils.getLanguage())
						.property_id(propertyIds).isPackageRate(true).build();

				ExpediaSearchService searchService = ExpediaSearchService.builder().supplierConf(this.getSupplierConf())
						.searchQuery(this.getSearchQuery()).additionalSearchParams(searchParams)
						.sourceConfigOutput(sourceConfigOutput).build();
				searchService.doDetailSearch();
				return searchService.getSearchResult();
			} catch (IOException e) {
				log.error("IOException while fetching package only search result for search query {} ",
						this.searchQuery, e);
				throw new CustomGeneralException("Unable to fetch package only search result due to " + e.getMessage());
			}
		});

		try {
			Object hotelOnlySearchResultObj = hotelOnlySearchResultTask.get();
			Object packageOnlySearchResultObj = packageOnlySearchResultTask.get();
			if (Objects.nonNull(hotelOnlySearchResultObj) && Objects.nonNull(packageOnlySearchResultObj)) {
				HotelSearchResult hotelOnlySearchResult = (HotelSearchResult) hotelOnlySearchResultObj;
				HotelSearchResult packageOnlySearchResult = (HotelSearchResult) packageOnlySearchResultObj;
				searchResult = mergePackageAndNonPackageResult(hotelOnlySearchResult, packageOnlySearchResult);
				log.debug("Total number of hotels after merging for {} are {}",
						supplierConf.getBasicInfo().getSupplierName(), searchResult.getNoOfHotelOptions());
				hInfo.setOptions(searchResult.getHotelInfos().get(0).getOptions());
			}
		} catch (InterruptedException | ExecutionException e) {
			log.error("Interrupted exception while fetching detail hotel info {} ", this.searchQuery, e);
			return;
		}
	}
}
