package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.travelbullz.AdvancedOptions;
import com.tgs.services.hms.datamodel.travelbullz.CancellationPolicy;
import com.tgs.services.hms.datamodel.travelbullz.CancellationPolicyResponse;
import com.tgs.services.hms.datamodel.travelbullz.HotelBookOption;
import com.tgs.services.hms.datamodel.travelbullz.HotelRoom;
import com.tgs.services.hms.datamodel.travelbullz.Request;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class TravelbullzCancellationPolicyService extends TravelbullzBaseService {
	private HotelInfo hInfo;
	private CancellationPolicyResponse cancellationPolicyResponse;
	private String searchkey;

	public void searchCancellationPolicy(String logKey) throws IOException {
		searchkey = hInfo.getOptions().get(0).getMiscInfo().getSearchKey();
		listener = new RestAPIListener("");
		HttpUtilsV2 httpUtils = null;
		try {
			for (Option hoteloption : hInfo.getOptions()) {
				TravelbullzRequest cancellationPolicyRequest = getCancellationPolicyRequest(hoteloption);
				String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
				String urlString =
						StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.CANCELLATION_POLICY));
				try {
					httpUtils = getHttpUtils(GsonUtils.getGson().toJson(cancellationPolicyRequest), urlString);
					cancellationPolicyResponse = httpUtils.getResponse(CancellationPolicyResponse.class).orElse(null);
				} catch (IOException e) {
					log.info("Unable to get cancellation policy response for searchid {} due to IO Exception",
							searchkey);
				}
				if (Objects.nonNull(cancellationPolicyResponse)
						&& CollectionUtils.isNotEmpty(cancellationPolicyResponse.getCancellationRS())) {
					populateCancellationPolicy(cancellationPolicyResponse, hoteloption);
				} else if (CollectionUtils.isNotEmpty(cancellationPolicyResponse.getError())) {
					log.info("Unable to get cancellation policy for searchid {} of Supplier {} due to error {} ",
							searchkey, supplierConf.getBasicInfo().getSupplierId(),
							cancellationPolicyResponse.getError().get(0).getDescription());
				}
			}
			searchQuery.setSourceId(HotelSourceType.TRAVELBULLZ.getSourceId());
			HotelUtils.setOptionCancellationPolicyFromRooms(hInfo.getOptions());
			HotelUtils.setBufferTimeinCnp(hInfo.getOptions(), searchQuery);

		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.CANCELLATION.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(cancellationPolicyResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRAVELBULLZ.name())
						.requestType(BaseHotelConstants.CANCELLATIONPOLICY).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(logKey)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private void populateCancellationPolicy(CancellationPolicyResponse cancellationResponse, Option hotelOption) {

		Map<String, List<CancellationPolicy>> roomBlockIdasKeyCnpAsValue = cancellationResponse.getCancellationRS()
				.stream().collect(Collectors.toMap(HotelRoom::getRoomToken, HotelRoom::getCancellationPolicy));
		hotelOption.getRoomInfos().forEach(room -> {
			setCancellationPolicy(room,
					roomBlockIdasKeyCnpAsValue.getOrDefault(room.getMiscInfo().getRoomBlockId(), null));
		});
	}

	private TravelbullzRequest getCancellationPolicyRequest(Option option) {

		HotelBookOption hotelOption = HotelBookOption.builder().HotelOptionId(option.getMiscInfo().getHotelOptionId())
				.HotelRooms(gethotelroomslist(option)).build();
		Request request = Request.builder().SearchKey(searchkey).HotelOption(hotelOption).build();
		AdvancedOptions advancedOptions =
				AdvancedOptions.builder().Currency(TravelbullzConstant.CURRENCY.value).build();
		TravelbullzRequest searchRequest =
				TravelbullzRequest.builder().Token(supplierConf.getHotelSupplierCredentials().getPassword())
						.Request(request).AdvancedOptions(advancedOptions).build();

		return searchRequest;
	}
}
