package com.tgs.services.hms.dbmodel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.HotelRegionAdditionalInfo;

public class HotelRegionAdditionalInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return HotelRegionAdditionalInfo.class;
	}
}
