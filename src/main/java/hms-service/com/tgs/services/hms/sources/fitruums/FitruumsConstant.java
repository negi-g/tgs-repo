package com.tgs.services.hms.sources.fitruums;

import lombok.Getter;

@Getter
public enum FitruumsConstant {
	CURRENCY("INR"),
	PAYMENTMETHODID("1"),
	SUCCESSBOOKINGCANCELLATIONCODE("1"),
	B2C("0");
	private String value;

	FitruumsConstant(String val) {
		this.value = val;
	}
}
