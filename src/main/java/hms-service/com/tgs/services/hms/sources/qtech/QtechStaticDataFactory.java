package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import java.util.Map;
import javax.xml.bind.JAXBException;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class QtechStaticDataFactory extends AbstractStaticDataInfoFactory {

	public QtechStaticDataFactory(HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getHotelStaticData() throws IOException, JAXBException {
		QtechStaticDataService staticDataService = QtechStaticDataService.builder().sourceConfig(sourceConfigOutput)
				.supplierConf(supplierConf).staticDataRequest(staticDataRequest).build();
		staticDataService.init();
		try {
			staticDataService.savehotelIdMapping();
		} catch (Exception e) {
			log.info("Unable to save hotelid mapping due to exception {}", e);
			throw new CustomGeneralException("Unable to save hotelid mapping due to {} " + e.getMessage());
		}

	}

	@Override
	protected void getCityMappingData() throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	protected void getNationalityMappingData() throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	protected Map<String, String> getRoomMappingData(HotelSearchQuery searchQuery, HotelInfo hotelInfo)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
