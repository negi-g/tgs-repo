package com.tgs.services.hms.sources.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.HotelRatePlanManager;
import com.tgs.services.hms.manager.mapping.HotelRoomInfoSaveManager;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;
import com.tgs.services.hms.utils.HotelUtils;
@Service
public class HotelInventoryPriceValidationFactory extends AbstractHotelPriceValidationFactory {
	
	@Autowired
	HotelRatePlanManager ratePlanManager;
	
	@Autowired
	HotelRoomInfoSaveManager roomInfoManager;

	public HotelInventoryPriceValidationFactory(HotelSearchQuery query, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
		super(query, hotel, hotelSupplierConf, bookingId);
	}

	@Override
	public Option getOptionWithUpdatedPrice() throws Exception {
		
		HotelInventoryPriceValidationService priceValidationService = HotelInventoryPriceValidationService.builder()
				.searchQuery(this.searchQuery).bookingId(bookingId)
				.supplierConf(this.getSupplierConf()).ratePlanManager(ratePlanManager)
				.roomOccupancyRoomTypeInfoMap(HotelUtils.getRoomOccupancyRoomTypeInfoMap()).build();
		priceValidationService.validate(this.getHInfo());
		return priceValidationService.getUpdatedOption();
	}

}
