package com.tgs.services.hms.restcontroller.inventory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.HotelRatePlanFilter;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.HotelRatePlanProcessor;
import com.tgs.services.hms.restmodel.inventory.HotelRatePlanRequest;
import com.tgs.services.hms.restmodel.inventory.HotelRatePlanResponse;
import com.tgs.services.hms.servicehandler.inventory.HotelRatePlanHandler;

@RestController
@RequestMapping("/hms/v1/rateplan")
public class HotelRatePlanController {

	@Autowired
	private HotelRatePlanRequestValidator ratePlanValidator;

	@Autowired
	private HotelRatePlanHandler ratePlanHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(ratePlanValidator);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {HotelRatePlanProcessor.class})
	protected HotelRatePlanResponse save(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody HotelRatePlanRequest ratePlanRequest) throws Exception {
		ratePlanHandler.initData(ratePlanRequest, new HotelRatePlanResponse());
		return ratePlanHandler.getResponse();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	protected BaseResponse delete(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Long id)
			throws Exception {
		return ratePlanHandler.deleteRatePlan(id);
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {HotelRatePlanProcessor.class})
	protected HotelRatePlanResponse search(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid HotelRatePlanFilter ratePlanFilter) throws Exception {
		ratePlanFilter.setValidOnGreaterThan(ratePlanFilter.getValidOnGreaterThan().minusDays(1));
		ratePlanFilter.setValidOnLessThan(ratePlanFilter.getValidOnLessThan().plusDays(1));
		return ratePlanHandler.fetchHotelRatePlan(ratePlanFilter);
	}
}
