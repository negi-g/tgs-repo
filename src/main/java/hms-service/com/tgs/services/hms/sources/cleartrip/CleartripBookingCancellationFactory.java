package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.oms.datamodel.Order;


@Service
public class CleartripBookingCancellationFactory extends AbstractHotelBookingCancellationFactory {

	public CleartripBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo,
			Order order) {
		super(supplierConf, hInfo, order);
	}

	@Override
	public boolean cancelHotel() throws IOException {
		CleartripBookingCancellationService cancellationService = CleartripBookingCancellationService.builder()
				.supplierConfig(supplierConf).sourceConfig(sourceConfigOutput).hInfo(hInfo).order(order).build();
		return cancellationService.cancelBooking();
	}

	@Override
	public boolean getCancelHotelStatus() {
		return false;
	}
}
