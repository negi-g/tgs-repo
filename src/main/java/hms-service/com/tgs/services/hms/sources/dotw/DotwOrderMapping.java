package com.tgs.services.hms.sources.dotw;

import lombok.Getter;

@Getter
public enum DotwOrderMapping {

	CONFIRMED("SUCCESS"), DENIED("PENDING"), CANCELED("CANCELLED");

	public String getStatus() {
		return this.getCode();
	}

	private String code;

	DotwOrderMapping(String code) {
		this.code = code;
	}
}
