package com.tgs.services.hms.restcontroller.source;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.servicehandler.source.ExpediaStaticDataHandler;


@RestController
@RequestMapping("/hms/v1/expedia/job")
public class ExpediaStaticDataController {
	
	@Autowired
	ExpediaStaticDataHandler expediaHandler;
	
	@RequestMapping(value = "/sync-property-catalog", method = RequestMethod.POST)
	protected BaseResponse syncPropertyCatalog(@RequestBody HotelStaticDataRequest staticDataRequest, HttpServletRequest request, HttpServletResponse response) throws Exception {
		expediaHandler.initData(staticDataRequest, new BaseResponse());
		return expediaHandler.getResponse();
	}
	
	@RequestMapping(value = "/sync-cities", method = RequestMethod.POST)
	protected BaseResponse syncCities(@RequestBody HotelStaticDataRequest staticDataRequest, HttpServletRequest request, HttpServletResponse response) throws Exception {
		expediaHandler.syncCities(staticDataRequest);
		return new BaseResponse();
	}
}
