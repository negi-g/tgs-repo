package com.tgs.services.hms.dbmodel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.HotelAdditionalInfo;


public class AdditionalInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return HotelAdditionalInfo.class;
	}

}