package com.tgs.services.hms.restcontroller.mapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoSaveManager;
import com.tgs.services.hms.restmodel.FetchHotelCityInfoResponse;
import com.tgs.services.hms.restmodel.FetchHotelRegionInfoResponse;
import com.tgs.services.hms.restmodel.HotelRegionInfoRequest;
import com.tgs.services.hms.restmodel.HotelRegionInfoResponse;
import com.tgs.services.hms.restmodel.HotelRegionInfoUpdateRequest;
import com.tgs.services.hms.restmodel.HotelRegionPriorityUpdateRequest;
import com.tgs.services.hms.servicehandler.HotelRegionInfoFetchHandler;

@RestController
@RequestMapping("/hms/v1")
public class HotelRegionInfoController {

	@Autowired
	HotelRegionInfoSaveManager regionInfoSaveManager;
	
	@Autowired
	HotelRegionInfoFetchHandler regionInfoFetchHandler;

	@RequestMapping(value = "/region/{id}", method = RequestMethod.GET)
	protected HotelRegionInfoResponse getRegionInfoById(HttpServletRequest request,
			HttpServletResponse response, @PathVariable String id) throws Exception {
		return regionInfoFetchHandler.getRegionInfoById(id);
	}

	@RequestMapping(value = "/region/save", method = RequestMethod.POST)
	protected BaseResponse saveRegionInfoIntoDB(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelRegionInfoRequest regionInfoRequest) throws Exception {
		regionInfoSaveManager.process(regionInfoRequest.getRegionInfoQuery(), true, false);
		return new BaseResponse();
	}

	@RequestMapping(value = "/region/update", method = RequestMethod.POST)
	protected BaseResponse saveIataInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelRegionInfoUpdateRequest regionInfoUpdateRequest) throws Exception {
		regionInfoSaveManager.updateRegionInfo(regionInfoUpdateRequest.getRegionInfos());
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/region/bulkupload", method = RequestMethod.POST)
	protected BulkUploadResponse saveSupplierHotelMealBasis(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelRegionPriorityUpdateRequest regionPriorityUdpateRequest) throws Exception {
		return regionInfoSaveManager.updateRegionInfoPriority(regionPriorityUdpateRequest);
	}

	/*
	 * Used in API Out
	 */

	@RequestMapping(value = {"/static-cities", "/static-cities/{next}"}, method = RequestMethod.GET)
	protected FetchHotelCityInfoResponse getCityInfo(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(required = false) String next) throws Exception {
		regionInfoFetchHandler.initData(next, new FetchHotelRegionInfoResponse());
		return regionInfoFetchHandler.convertRegionInfosIntoCityInfos(regionInfoFetchHandler.getResponse());
	}
}
