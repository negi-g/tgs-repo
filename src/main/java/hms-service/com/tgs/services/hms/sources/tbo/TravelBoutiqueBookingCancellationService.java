package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.bookingcancellation.HotelBookingCancellationRequest;
import com.tgs.services.hms.datamodel.tbo.bookingcancellation.HotelBookingCancellationResponse;
import com.tgs.services.hms.datamodel.tbo.bookingcancellation.HotelBookingCancellationStatusRequest;
import com.tgs.services.hms.datamodel.tbo.bookingcancellation.HotelBookingCancellationStatusResponse;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Service
public class TravelBoutiqueBookingCancellationService extends TravelBoutiqueBaseService {

	HotelSupplierConfiguration supplierConf;
	Order order;
	HotelInfo hInfo;
	protected RestAPIListener listener;

	public boolean cancelBooking() throws IOException {

		HttpUtils httpUtils = null;
		HotelBookingCancellationRequest cancellationRequest = null;
		try {
			listener = new RestAPIListener("");
			cancellationRequest = getHotelBookingCancellationRequest();
			httpUtils = TravelBoutiqueUtil.getRequest(cancellationRequest, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.CANCEL_BOOKING_URL));
			HotelBookingCancellationResponse cancellationResponse =
					httpUtils.getResponse(HotelBookingCancellationResponse.class).orElse(null);
			log.info("TBO cancel booking response is :{}", cancellationResponse);
			return processChangeRequest(cancellationResponse, hInfo);

		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TBO.name())
					.postData(GsonUtils.getGson().toJson(cancellationRequest))
					.requestType(BaseHotelConstants.BOOKING_CANCELLATION).responseString(httpUtils.getResponseString())
					.urlString(httpUtils.getUrlString()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
	}

	private boolean processChangeRequest(HotelBookingCancellationResponse cancellationResponse, HotelInfo hInfo) {

		Integer changeRequestStatus = cancellationResponse.getHotelChangeRequestResult().getChangeRequestStatus();
		if (changeRequestStatus == 3 && cancellationResponse.getHotelChangeRequestResult().getResponseStatus() == 1) {
			log.info("Booking Cancelled");
			hInfo.getMiscInfo().setHotelBookingCancellationReference(
					cancellationResponse.getHotelChangeRequestResult().getChangeRequestId());
			return true;
		} else if (changeRequestStatus == 2 || changeRequestStatus == 3) {
			log.info("Booking Cancellation In Progress/Pending");
			hInfo.getMiscInfo().setHotelBookingCancellationReference(
					cancellationResponse.getHotelChangeRequestResult().getChangeRequestId());
		} else if (changeRequestStatus == 4) {
			log.info("Booking Cancellation Rejected");
		}
		return false;
	}

	private HotelBookingCancellationRequest getHotelBookingCancellationRequest() throws IOException {

		HotelBookingCancellationRequest request = new HotelBookingCancellationRequest();
		String token = getCachedToken();
		request.setEndUserIp(TravelBoutiqueUtil.IP);
		request.setTokenId(token);
		request.setRequestType(4);
		request.setRemarks("Cancel");
		request.setBookingId(hInfo.getMiscInfo().getSupplierBookingId());
		return request;

	}


	private HotelBookingCancellationStatusRequest getBookingCancellationStatusRequest() throws IOException {

		String token = getCachedToken();
		HotelBookingCancellationStatusRequest request = HotelBookingCancellationStatusRequest.builder().EndUserIp("::1")
				.ChangeRequestId(hInfo.getMiscInfo().getHotelBookingCancellationReference()).TokenId(token).build();
		return request;

	}


	public boolean getCancelBookingStatus() throws IOException {

		HttpUtils httpUtils = null;
		HotelBookingCancellationStatusRequest request = null;
		try {
			listener = new RestAPIListener("");
			request = getBookingCancellationStatusRequest();
			httpUtils = TravelBoutiqueUtil.getRequest(request, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.CANCEL_BOOKING_STATUS_URL));
			HotelBookingCancellationStatusResponse response =
					httpUtils.getResponse(HotelBookingCancellationStatusResponse.class).orElse(null);
			if (response != null && response.getHotelChangeRequestStatusResult() != null
					&& response.getHotelChangeRequestStatusResult().getChangeRequestStatus() == 3) {
				return true;
			}
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TBO.name())
					.postData(GsonUtils.getGson().toJson(request)).prefix("Status")
					.requestType(BaseHotelConstants.BOOKING_CANCELLATION).responseString(httpUtils.getResponseString())
					.urlString(httpUtils.getUrlString()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
		return false;
	}

}
