package com.tgs.services.hms.sources.tbo;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class TravelBoutiqueHotelBookingCancellationFactory extends AbstractHotelBookingCancellationFactory{

	@Autowired
	private HMSCachingServiceCommunicator cacheService;
	
	public TravelBoutiqueHotelBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo, Order order) {
		super(supplierConf, hInfo, order);
		
	}

	@Override
	public boolean cancelHotel() throws IOException {
		
		TravelBoutiqueBookingCancellationService cancellationService = TravelBoutiqueBookingCancellationService.builder().supplierConf(supplierConf).hInfo(hInfo).order(order)
				.cacheService(cacheService).build();
		return cancellationService.cancelBooking();
	}
	
	@Override
	public boolean getCancelHotelStatus() throws IOException{
		
		TravelBoutiqueBookingCancellationService cancellationStatusService = TravelBoutiqueBookingCancellationService.builder()
				.supplierConf(supplierConf).hInfo(hInfo).order(order).build();
		return cancellationStatusService.getCancelBookingStatus();
	}
	
	

}
