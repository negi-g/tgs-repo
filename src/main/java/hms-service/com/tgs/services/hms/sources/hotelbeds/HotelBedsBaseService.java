package com.tgs.services.hms.sources.hotelbeds;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomAdditionalInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelBeds.CancellationPolicy;
import com.tgs.services.hms.datamodel.hotelBeds.Hotel;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedPaxType;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsRateCheckRequest;
import com.tgs.services.hms.datamodel.hotelBeds.Paxes;
import com.tgs.services.hms.datamodel.hotelBeds.Rate;
import com.tgs.services.hms.datamodel.hotelBeds.RateCheckRoomRequest;
import com.tgs.services.hms.datamodel.hotelBeds.RoomResponse;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Getter
@Setter
@Slf4j
public class HotelBedsBaseService {

	protected HotelSupplierConfiguration supplierConf;
	protected HotelSearchQuery searchQuery;
	protected HotelSourceConfigOutput sourceConfig;
	protected RestAPIListener listener;
	protected MoneyExchangeCommunicator moneyExchnageComm;
	protected HotelCacheHandler cacheHandler;
	protected String currency;

	protected LocalDateTime getFormattedDate(String supplierDate) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX");
		return LocalDateTime.parse(supplierDate, formatter);
	}

	protected HotelCancellationPolicy getRoomCancellationPolicy(Rate rate, HotelSearchQuery searchQuery,
			RoomInfo rInfo) {

		LocalDateTime checkInDate = searchQuery.getCheckinDate().atTime(12, 00);
		List<PenaltyDetails> pds = new ArrayList<>();
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime deadlineDateTime = now;
		boolean isFullRefundAllowed = false;
		if (CollectionUtils.isEmpty(rate.getCancellationPolicies())) {
			return null;
		}
		for (CancellationPolicy cnp : rate.getCancellationPolicies()) {
			if (pds.size() < 1) {
				LocalDateTime fromDate = getFormattedDate(cnp.getFrom());
				if (fromDate.isBefore(now)) {
					PenaltyDetails penaltyDetails2 = PenaltyDetails.builder().fromDate(now).toDate(checkInDate)
							.penaltyAmount(cnp.getAmount()).build();
					pds.add(penaltyDetails2);
				} else {
					PenaltyDetails penaltyDetails1 =
							PenaltyDetails.builder().fromDate(now).toDate(fromDate).penaltyAmount(0.0).build();
					isFullRefundAllowed = true;
					pds.add(penaltyDetails1);
					deadlineDateTime = fromDate;
					PenaltyDetails penaltyDetails2 = PenaltyDetails.builder().fromDate(fromDate).toDate(checkInDate)
							.penaltyAmount(cnp.getAmount()).build();
					pds.add(penaltyDetails2);
				}
			} else {
				LocalDateTime fromDate = getFormattedDate(cnp.getFrom());
				pds.get(pds.size() - 1).setToDate(fromDate);
				PenaltyDetails penaltyDetails = PenaltyDetails.builder().fromDate(fromDate).toDate(checkInDate)
						.penaltyAmount(cnp.getAmount()).build();
				pds.add(penaltyDetails);
			}
		}
		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder().id(rInfo.getId())
				.isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds).miscInfo(CancellationMiscInfo.builder()
						.isBookingAllowed(true).isSoldOut(false).isCancellationPolicyBelongToRoom(true).build())
				.build();
		rInfo.setDeadlineDateTime(deadlineDateTime);
		return cancellationPolicy;
	}

	public void setCancellationDeadlineFromRooms(Option option) {

		LocalDateTime earliestDeadlineDateTime = null;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			if (earliestDeadlineDateTime == null)
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
			else if (earliestDeadlineDateTime.isAfter(roomInfo.getDeadlineDateTime()))
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
		}
		option.setDeadlineDateTime(earliestDeadlineDateTime);
	}

	protected void populateRoomPerNightPrice(RoomInfo rInfo, Rate rate) {

		long numberOfNights = Duration
				.between(searchQuery.getCheckinDate().atStartOfDay(), searchQuery.getCheckoutDate().atStartOfDay())
				.toDays();
		double netPrice = rate.getNet();
		double perNightRoomPrice = (netPrice / numberOfNights);
		List<PriceInfo> priceInfoList = new ArrayList<>();
		for (int i = 0; i < numberOfNights; i++) {
			PriceInfo priceInfo = new PriceInfo();
			priceInfo.setDay(i);
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, perNightRoomPrice);
			fareComponents.put(HotelFareComponent.SBP, perNightRoomPrice);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
		}
		rInfo.setPerNightPriceInfos(priceInfoList);
		rInfo.setTotalPrice((rate.getNet()));
	}


	protected List<Option> createOptionFromHotel(Hotel hotelResponse, boolean isDetail) {

		List<Option> optionList = new ArrayList<>();
		currency = hotelResponse.getCurrency();
		Map<String, List<RoomInfo>> roomOccupancyKeyRoomInfosValues =
				getroomInfoMap(hotelResponse.getRooms(), isDetail);
		roomOccupancyKeyRoomInfosValues.entrySet().forEach(entry -> {
			entry.getValue().stream().sorted((p1, p2) -> p1.getTotalPrice().compareTo(p2.getTotalPrice()))
					.collect(Collectors.toList());
		});

		while (!roomOccupancyKeyRoomInfosValues.isEmpty()) {
			List<RoomInfo> optionRoomInfos = new ArrayList<>();
			for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
				String key = getKeyFromSearchRoom(room);
				List<RoomInfo> roomList = roomOccupancyKeyRoomInfosValues.get(key);
				if (CollectionUtils.isNotEmpty(roomList) && roomList.size() > 0) {
					optionRoomInfos.add(roomList.get(0));
					roomOccupancyKeyRoomInfosValues.get(key).remove(0);
				}
			}
			if (optionRoomInfos.size() == searchQuery.getRoomInfo().size()) {
				updatePriceWithMarkup(optionRoomInfos);
				updateRoomIds(optionRoomInfos);
				Option option = Option.builder().roomInfos(optionRoomInfos)
						.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
								.supplierHotelId(hotelResponse.getCode() + "")
								.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
								.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
						.id(RandomStringUtils.random(20, true, true)).build();
				if (StringUtils.isNotBlank(optionRoomInfos.get(0).getMiscInfo().getNotes()) && isDetail) {
					List<String> notes = optionRoomInfos.stream().map(roomInfo -> roomInfo.getMiscInfo().getNotes())
							.collect(Collectors.toList());
					List<Instruction> instructions = new ArrayList<>();
					instructions.add(Instruction.builder().type(InstructionType.BOOKING_NOTES)
							.msg(notes.stream().collect(Collectors.joining("\n")).toString()).build());
					option.setInstructions(instructions);
				}
				if (!isDetail) {
					option.getMiscInfo().setIsNotRequiredOnDetail(true);
				}
				optionList.add(option);
			} else
				break;
		}

		searchQuery.setSourceId(HotelSourceType.HOTELBEDS.getSourceId());
		HotelUtils.setBufferTimeinCnp(optionList, searchQuery);
		HotelUtils.setOptionCancellationPolicyFromRooms(optionList);
		return optionList;
	}

	protected void updateRoomIds(List<RoomInfo> optionRoomInfos) {
		int index = 0;
		for (RoomInfo room : optionRoomInfos) {
			room.setId(room.getId() + "_" + index++);
		}
	}

	private String getKeyFromSearchRoom(RoomSearchInfo room) {

		StringBuilder key = new StringBuilder();
		key.append(room.getNumberOfAdults());
		key.append(room.getNumberOfChild() != null ? room.getNumberOfChild() : 0);
		if (room.getChildAge() != null && CollectionUtils.isNotEmpty(room.getChildAge())) {
			String childAge = room.getChildAge().stream().map(String::valueOf).collect(Collectors.joining(","));
			key.append(childAge);
		}
		return key.toString();
	}

	private Map<String, List<RoomInfo>> getroomInfoMap(List<RoomResponse> rooms, boolean isDetail) {

		Map<String, List<RoomInfo>> roomOccupancyKeyRoomInfosValues = new ConcurrentHashMap<>();
		for (RoomResponse hotelBedsRoom : rooms) {
			RoomInfo room = new RoomInfo();
			room.setId(hotelBedsRoom.getCode());
			room.setRoomCategory(hotelBedsRoom.getName());
			room.setCheckInDate(searchQuery.getCheckinDate());
			room.setCheckOutDate(searchQuery.getCheckoutDate());
			room.setRoomType(hotelBedsRoom.getName());
			hotelBedsRoom.getRates().forEach(rate -> {
				if (rate.getPaymentType().equals("AT_WEB")) {
					StringBuilder key = new StringBuilder();
					key.append(rate.getAdults());
					key.append(rate.getChildren());
					if (rate.getChildrenAges() != null) {
						key.append(rate.getChildrenAges());
					}
					RoomInfo rInfo = new GsonMapper<>(room, RoomInfo.class).convert();
					populateRoomPerNightPrice(rInfo, rate);
					populateRoomPaxInfoFromRate(rInfo, rate);
					rInfo.setMiscInfo(RoomMiscInfo.builder().amenities(Arrays.asList(rate.getBoardName())).build());
					rInfo.getMiscInfo().setRatePlanCode(rate.getRateKey());
					if (isDetail) {
						rInfo.getMiscInfo().setRateType(rate.getRateType());
						rInfo.getMiscInfo().setRateChannel(rate.getRateClass());
						if (StringUtils.isNotBlank(rate.getRateComments())) {
							rInfo.getMiscInfo().setNotes(rate.getRateComments());
						}
						if (CollectionUtils.isNotEmpty(hotelBedsRoom.getPaxes())) {
							rInfo.setTravellerInfo(getTravellerList(hotelBedsRoom));
						}
					}
					rInfo.setCancellationPolicy(getRoomCancellationPolicy(rate, searchQuery, rInfo));
					rInfo.getMiscInfo().setRoomTypeCode(rInfo.getId());
					rInfo.setRoomAdditionalInfo(RoomAdditionalInfo.builder().roomId(hotelBedsRoom.getCode()).build());
					roomOccupancyKeyRoomInfosValues.computeIfAbsent(key.toString(), (x) -> new LinkedList<>())
							.add(rInfo);
				}
			});
		}
		return roomOccupancyKeyRoomInfosValues;
	}

	public void populateRoomPaxInfoFromRate(RoomInfo rInfo, Rate rate) {
		rInfo.setNumberOfAdults(rate.getAdults());
		rInfo.setNumberOfChild(rate.getChildren());
	}

	public void updatePriceWithMarkup(List<RoomInfo> rInfoList) {

		double supplierMarkup = (sourceConfig == null || sourceConfig.getSupplierMarkup() == null) ? 0.0
				: sourceConfig.getSupplierMarkup();

		for (RoomInfo roomInfo : rInfoList) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				double supplierGrossPrice = priceInfo.getFareComponents().get(HotelFareComponent.BF);
				priceInfo.getFareComponents().put(HotelFareComponent.CMU, (supplierGrossPrice * supplierMarkup) / 100);
				priceInfo.getFareComponents().put(HotelFareComponent.SGP, supplierGrossPrice);
				priceInfo.getFareComponents().put(HotelFareComponent.SNP, supplierGrossPrice);
				priceInfo.getFareComponents().put(HotelFareComponent.BF, supplierGrossPrice);

			}
		}
	}

	protected List<TravellerInfo> getTravellerList(RoomResponse room) {

		List<TravellerInfo> travellerInfoList = new ArrayList<>();
		for (Paxes pax : room.getPaxes()) {
			TravellerInfo ti = new TravellerInfo();
			ti.setFirstName(pax.getName());
			ti.setLastName(pax.getSurname());
			ti.setPaxType(pax.getType().equals(HotelBedPaxType.ADULT.getCode()) ? PaxType.ADULT : PaxType.CHILD);
			ti.setTitle(ti.getPaxType().equals(PaxType.ADULT) ? "Mr" : "Master");
			travellerInfoList.add(ti);
		}
		return travellerInfoList;
	}


	protected HotelBedsRateCheckRequest getRateCheckRequest(Option selectedOption) {

		List<RateCheckRoomRequest> rateKeys = new ArrayList<>();
		selectedOption.getRoomInfos().forEach(room -> {
			rateKeys.add(RateCheckRoomRequest.builder().rateKey(room.getMiscInfo().getRatePlanCode()).build());
		});
		HotelBedsRateCheckRequest rateCheckrequest = HotelBedsRateCheckRequest.builder().rooms(rateKeys).build();
		return rateCheckrequest;
	}

}
