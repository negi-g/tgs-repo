package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchPreferences;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwBookingDetailRequest;
import com.tgs.services.hms.datamodel.dotw.DotwBookingDetailResponse;
import com.tgs.services.hms.datamodel.dotw.DotwPassenger;
import com.tgs.services.hms.datamodel.dotw.DotwRoomBookingDetailResponse;
import com.tgs.services.hms.datamodel.dotw.RateBasis;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
@Getter
@Setter
@SuperBuilder
public class DotwRetrieveBookingService extends DotwBaseService {

	private HotelImportBookingParams importBookingParams;
	private HotelImportedBookingInfo bookingInfo;

	public void retrieveBooking() throws IOException, JAXBException {

		HttpUtils httpUtils = null;
		try {
			listener = new RestAPIListener("");
			Customer customer = createRetrieveBookingRequest();
			String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
			bookingInfo = createBookingDetailResponse();
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.DOTW.name())
					.requestType(BaseHotelConstants.RETRIEVE_BOOKING).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).prefix(importBookingParams.getFlowType().getName())
					.logKey(importBookingParams.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			httpUtils.getCheckPoints().stream()
					.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.BOOKING_DETAIL.name()));
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (!ObjectUtils.isEmpty(result.getRequest()) && !ObjectUtils.isEmpty(result.getRequest().getError())
					&& result.getRequest().getError().getDetails() != null) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(result.getRequest().getError().getDetails());
			}
		}

	}


	private HotelImportedBookingInfo createBookingDetailResponse() {

		if (result == null || result.getBookingDetailResponse() == null)
			throw new CustomGeneralException(SystemError.EMPTY_SUPPLIER_RESPONSE);
		DotwBookingDetailResponse response = result.getBookingDetailResponse();
		HotelInfo hInfo = getHotelDetails(response);
		HotelSearchQuery searchQuery = getHotelSearchQuery(response);
		List<RoomInfo> roomInfos = new ArrayList<>();
		String currencyCode = null;
		String orderStatus = null;
		for (DotwRoomBookingDetailResponse roomResponse : response.getRoomDetails()) {
			currencyCode = roomResponse.getCurrency();
			RoomInfo roomInfo = new RoomInfo();
			RoomMiscInfo roomMiscInfo = RoomMiscInfo.builder().roomBookingId(roomResponse.getRoomBookingId())
					.roomTypeCode(roomResponse.getRoomTypeCode()).build();
			roomInfo.setMiscInfo(roomMiscInfo);
			roomInfo.setNumberOfAdults(roomResponse.getAdults());
			roomInfo.setRoomCategory(roomResponse.getRoomName());
			roomInfo.setRoomType(roomResponse.getRoomName());
			roomInfo.setMealBasis(getMealBasis(roomResponse.getRateBasis()));
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
			roomInfo.setTotalPrice(roomResponse.getServicePrice().getServicePrice());
			if (roomResponse.getChildren() != null)
				roomInfo.setNumberOfChild(roomResponse.getChildren().getNo());
			RateBasis rateBasis = new RateBasis();
			rateBasis.setCancellationRules(roomResponse.getCancellationRules());
			rateBasis.setTotal(HotelBaseSupplierUtils.getAmountBasedOnCurrency(
					roomResponse.getServicePrice().getServicePrice(), roomResponse.getCurrency(),
					HotelSourceType.DOTW.name(), BaseHotelConstants.DEFAULT_CURRENCY));
			HotelCancellationPolicy cp = getRoomCancellationPolicy(rateBasis, searchQuery, roomInfo);
			roomInfo.setCancellationPolicy(cp);


			List<TravellerInfo> travellerInfoList = getTravellerList(roomResponse.getPassengerList());
			roomInfo.setTravellerInfo(travellerInfoList);

			List<PriceInfo> priceInfoList = getDailyPriceInfo(roomResponse.getDate());
			roomInfo.setPerNightPriceInfos(priceInfoList);
			roomInfos.add(roomInfo);
			orderStatus = getOrderStatus(roomResponse.getStatus());// DotwOrderMapping.valueOf(response.getStatus().toUpperCase()).getCode();

		}
		Double totalPrice = getTotalSupplierPrice(roomInfos);
		Option option = Option.builder().totalPrice(totalPrice)
				.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
						.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
						.supplierHotelId(hInfo.getMiscInfo().getSupplierStaticHotelId())
						.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
				.roomInfos(roomInfos).id(RandomStringUtils.random(20, true, true)).build();
		List<Option> options = new ArrayList<>();
		updatePriceWithMarkup(option.getRoomInfos());
		options.add(option);
		searchQuery.setSourceId(HotelSourceType.DOTW.getSourceId());
		HotelUtils.setOptionCancellationPolicyFromRooms(options);

		HotelUtils.setBufferTimeinCnp(options, searchQuery);
		hInfo.setOptions(options);
		DeliveryInfo deliveryInfo = new DeliveryInfo();

		HotelImportedBookingInfo importedBookingInfo =
				HotelImportedBookingInfo.builder().hInfo(hInfo).searchQuery(searchQuery).orderStatus(orderStatus)
						.deliveryInfo(deliveryInfo).bookingCurrencyCode(currencyCode).build();
		return importedBookingInfo;
	}

	private String getOrderStatus(String status) {

		if (status.equals("1666"))
			return OrderStatus.SUCCESS.name();
		if (status.equals("1667"))
			return OrderStatus.CANCELLED.name();
		if (status.equals("1707"))
			return OrderStatus.PENDING.name();
		return OrderStatus.PENDING.name();
	}


	private List<TravellerInfo> getTravellerList(List<DotwPassenger> passengers) {

		List<TravellerInfo> travellerInfoList = new ArrayList<>();
		for (DotwPassenger passenger : passengers) {
			TravellerInfo ti = new TravellerInfo();
			ti.setFirstName(passenger.getFirstName());
			ti.setLastName(passenger.getLastName());
			ti.setTitle(getTitleFromSaluationCode(passenger.getSalutation()));
			travellerInfoList.add(ti);
		}
		return travellerInfoList;
	}

	private HotelSearchQuery getHotelSearchQuery(DotwBookingDetailResponse response) {

		List<DotwRoomBookingDetailResponse> roomResponseList = response.getRoomDetails();
		DotwRoomBookingDetailResponse roomResponse = roomResponseList.get(0);
		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
		searchQueryBuilder.checkinDate(LocalDate.parse(roomResponse.getFrom()));
		searchQueryBuilder.checkoutDate(LocalDate.parse(roomResponse.getTo()));
		searchQueryBuilder.miscInfo(HotelSearchQueryMiscInfo.builder().supplierId(HotelSourceType.DOTW.name()).build());
		HotelSearchPreferences searchPreferences = new HotelSearchPreferences();
		searchPreferences.setCurrency("INR");
		searchQueryBuilder.searchPreferences(searchPreferences);
		searchQueryBuilder.sourceId(4);
		return searchQueryBuilder.build();
	}

	private HotelInfo getHotelDetails(DotwBookingDetailResponse response) {

		List<DotwRoomBookingDetailResponse> roomResponseList = response.getRoomDetails();
		DotwRoomBookingDetailResponse roomResponse = roomResponseList.get(0);
		String hotelId = roomResponse.getHotelId();

		HotelMiscInfo hMiscInfo = HotelMiscInfo.builder().supplierStaticHotelId(hotelId)
				.supplierBookingConfirmationNo(response.getSupplierConfirmation())
				.supplierBookingId(response.getBookingId()).supplierBookingReference(response.getBookingId()).build();
		HotelInfo hInfo = HotelInfo.builder().miscInfo(hMiscInfo).name(roomResponse.getHotelName()).build();
		return hInfo;

	}


	private Customer createRetrieveBookingRequest() {

		Customer customer = getCustomer(supplierConf);
		Request request = new Request();
		request.setCommand("getbookingdetails");
		DotwBookingDetailRequest bookingDetailRequest = new DotwBookingDetailRequest();
		bookingDetailRequest.setBookingCode(importBookingParams.getSupplierBookingId());
		bookingDetailRequest.setBookingType(1);
		request.setBookingDetails(bookingDetailRequest);
		customer.setRequest(request);
		return customer;
	}

}
