package com.tgs.services.hms.helper;

import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUserReview;
import com.tgs.services.hms.sources.AbstractHotelUserReviewFactory;

@Service
public abstract class AbstractHotelUserReviewSearch {

	public HotelUserReview reviewSearch(int sourceId , HotelInfo hInfo) {
		
		UserReviewSourceType sourceType = UserReviewSourceType.getReviewSourceType(sourceId);
		AbstractHotelUserReviewFactory factory = sourceType.getReviewFactoryInstance();
		return factory.getHotelReview(hInfo);

	}
	
	public void userReviewIdSearch(int sourceId , HotelInfo hInfo) {
		
		UserReviewSourceType sourceType = UserReviewSourceType.getReviewSourceType(sourceId);
		AbstractHotelUserReviewFactory factory = sourceType.getReviewFactoryInstance();
		factory.getUserReviewIdFromNameLatLong(hInfo);
		
	}
	
}
