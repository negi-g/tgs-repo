package com.tgs.services.hms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.HotelConfiguratorInfoFilter;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.dbmodel.DbHotelConfiguratorRule;
import com.tgs.services.hms.restmodel.HotelConfigRuleTypeResponse;
import com.tgs.services.hms.servicehandler.HotelConfigHandler;


@RestController
@RequestMapping("hms/v1/config")
public class HotelConfiguratorController {

	@Autowired
	AuditsHandler auditHandler;
	
	@Autowired
	private HotelConfigHandler configHandler;
	
	@Autowired
	private HotelConfigRequestValidator configRequestValidator;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(configRequestValidator);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected HotelConfigRuleTypeResponse saveOrUpdateHotelConfigRule(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody HotelConfiguratorInfo hotelConfiguratorInfo)
			throws Exception {
		configHandler.initData(hotelConfiguratorInfo, new HotelConfigRuleTypeResponse());
		return configHandler.getResponse();
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected HotelConfigRuleTypeResponse getHotelConfigRuleType(HttpServletRequest request,
			HttpServletResponse httpResoponse, @RequestBody HotelConfiguratorInfoFilter configuratorFilter)
			throws Exception {
		return configHandler.getConfiguratorInfo(configuratorFilter);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteConfigRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return configHandler.deleteConfiguratorInfo(id);
	}
	
	@RequestMapping(value="/status/{id}/{status}", method= RequestMethod.GET)
	protected BaseResponse updateConfigStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {		
		return configHandler.updateConfigStatus(id,status);
	}

	@RequestMapping(value = "/hotelconfig-audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbHotelConfiguratorRule.class, ""));
		return auditResponse;
	}
}
