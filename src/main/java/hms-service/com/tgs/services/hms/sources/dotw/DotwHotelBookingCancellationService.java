package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwCancellationCriteria;
import com.tgs.services.hms.datamodel.dotw.DotwServiceRequest;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.datamodel.dotw.ServiceType;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class DotwHotelBookingCancellationService extends DotwBaseService {

	private Order order;

	public boolean cancelBooking() throws IOException, JAXBException {

		Customer customer = getCustomer(supplierConf);
		Request request = getBaseRequest();
		customer.setRequest(request);
		return cancelBookingItineary(customer);
	}

	private Request getBaseRequest() {

		Request request = new Request();
		request.setCommand("deleteitinerary");
		DotwCancellationCriteria cancellationCriteria = new DotwCancellationCriteria();
		cancellationCriteria.setBookingType("1");
		request.setBookingCancellationRequest(cancellationCriteria);
		return request;

	}

	private boolean cancelBookingItineary(Customer customer) throws JAXBException, IOException {
		HttpUtils httpUtils = null;
		listener = new RestAPIListener("");
		updateRequest(customer);
		try {
			String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			Results result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
			if (result.getSuccessful().equalsIgnoreCase("TRUE")) {
				return confirmBookingCancellation(result, customer);
			}
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.DOTW.name())
					.requestType(BaseHotelConstants.BOOKING_CANCELLATION).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId()).prefix("Check")
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
		return false;
	}

	private boolean confirmBookingCancellation(Results result, Customer customer) throws IOException, JAXBException {
		HttpUtils httpUtils = null;
		try {
			updateRequestForCancel(customer, result);
			String xmlRequest1 = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest1, supplierConf);
			String xmlResponse1 = (String) httpUtils.getResponse(null).orElse(null);
			Results result1 = DotwMarshallerWrapper.unmarshallXML(xmlResponse1);
			if (result1.getSuccessful().equalsIgnoreCase("TRUE")) {
				log.info("Booking {} Supplier {} cancelled Successfully", order.getBookingId(), "DOTW");
				return true;
			}

			log.info("Booking Cancellation Failed For BookingId {}, Supplier {}, NumberOfProductsLeft {}",
					order.getBookingId(), "DOTW", result1.getProductsLeftOnItinerary());
			return false;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.DOTW.name())
					.requestType(BaseHotelConstants.BOOKING_CANCELLATION).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId()).prefix("Confirm")
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

		}
	}

	private void updateRequestForCancel(Customer customer, Results result) throws IOException, JAXBException {
		List<DotwServiceRequest> services = new ArrayList<>();

		for (ServiceType service : result.getServices()) {
			DotwServiceRequest serviceRequest = new DotwServiceRequest();

			serviceRequest.setPenaltyApplied(service.getCancellationPenalty().get(0).getCharge().getFormatted());
			serviceRequest.setReferencenumber(service.getCode().toString());
			services.add(serviceRequest);
		}
		customer.getRequest().getBookingCancellationRequest().setConfirm("yes");

		customer.getRequest().getBookingCancellationRequest().setService(services);
	}

	private void updateRequest(Customer customer) {

		customer.getRequest().getBookingCancellationRequest()
				.setBookingCode(hInfo.getMiscInfo().getSupplierBookingId());
		customer.getRequest().getBookingCancellationRequest().setConfirm("no");

	}

}
