package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class DotwRetrieveBookingFactory extends AbstractRetrieveHotelBookingFactory {

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public DotwRetrieveBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		super(supplierConf, importBookingInfo);
	}

	@Override
	public void retrieveBooking() throws IOException, JAXBException {
		DotwRetrieveBookingService bookingService =
				DotwRetrieveBookingService.builder().supplierConf(supplierConf).sourceConfigOutput(sourceConfigOutput)
						.importBookingParams(importBookingParams).build();
		bookingService.retrieveBooking();
		bookingDetailResponse = bookingService.getBookingInfo();

	}

}
