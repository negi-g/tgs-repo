package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class AgodaRetrieveBookingFactory extends AbstractRetrieveHotelBookingFactory {

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public AgodaRetrieveBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		super(supplierConf, importBookingInfo);
	}

	@Override
	public void retrieveBooking() throws IOException, JAXBException {
		AgodaRetrieveBookingService bookingService = AgodaRetrieveBookingService.builder().supplierConf(supplierConf)
				.moneyExchnageComm(moneyExchnageComm).sourceConfigOutput(sourceConfigOutput)
				.importBookingParams(importBookingParams).build();
		bookingService.retrieveBooking();
		bookingDetailResponse = bookingService.getBookingInfo();
	}

	@Override
	public String getHotelConfirmationNumber() throws IOException, JAXBException {
		AgodaRetrieveBookingService bookingService =
				AgodaRetrieveBookingService.builder().supplierConf(supplierConf).sourceConfigOutput(sourceConfigOutput)
						.importBookingParams(importBookingParams).build();
		 bookingService.getHotelConfirmationNumber();
		 return bookingService.hotelReferenceNumber;
	}
	
	@Override
	public String getBookNowPayLaterStatus() throws IOException, JAXBException {
		AgodaRetrieveBookingService bookingService =
				AgodaRetrieveBookingService.builder().supplierConf(supplierConf).sourceConfigOutput(sourceConfigOutput)
						.importBookingParams(importBookingParams).build();
		 bookingService.getBookNowPayLaterStatus();
		 return bookingService.bookNowPayLaterStatus;
	}
}
