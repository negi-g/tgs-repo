package com.tgs.services.hms.sources.vervotech;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierCredential;
import com.tgs.services.hms.datamodel.vervotech.VervotechBaseRequest;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VervotechUtil {

	public static HttpUtilsV2 getResponseURLWithRequestBody(VervotechBaseRequest request,
			HotelSupplierConfiguration supplierConf, String serviceType) {
		HotelSupplierCredential supplierCredential = supplierConf.getHotelSupplierCredentials();
		setCredentials(request, supplierConf, serviceType);
		String postData = GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(request);
		HttpUtilsV2 httpUtils = HttpUtilsV2.builder().urlString(supplierCredential.getUrl() + request.getSuffixOfURL())
				.proxy(HotelUtils.getProxyFromConfigurator()).postData(postData).headerParams(request.getHeaderParams())
				.build();

		log.debug("URL generated is {} with request body {}", httpUtils.getUrlString(), postData);
		return httpUtils;
	}

	public static void setCredentials(VervotechBaseRequest request, HotelSupplierConfiguration supplierConf,
			String serviceType) {

		HotelSupplierCredential supplierCredential = supplierConf.getHotelSupplierCredentials();
		if (serviceType.equals(VervotechConstant.HOTEL_SERVICE_TYPE)) {
			request.getHeaderParams().put("accountId", supplierCredential.getAccountingCode());
			request.getHeaderParams().put("apikey", supplierCredential.getApiKey());
		} else if (serviceType.equals(VervotechConstant.ROOM_SERVICE_TYPE)) {
			request.getHeaderParams().put("accountId", supplierCredential.getUserName());
			request.getHeaderParams().put("token", supplierCredential.getToken());
		}
	}

	@SuppressWarnings("unchecked")
	public static HttpUtilsV2 getResponseURL(VervotechBaseRequest request, HotelSupplierConfiguration supplierConf,
			String serviceType) {

		HotelSupplierCredential supplierCredential = supplierConf.getHotelSupplierCredentials();
		setCredentials(request, supplierConf, serviceType);
		Map<String, String> queryParams = convertToMap(request);
		MultiMap recurringQueryParams = new MultiValueMap();
		recurringQueryParams.putAll(queryParams);
		HttpUtilsV2 httpUtils = HttpUtilsV2.builder().urlString(supplierCredential.getUrl() + request.getSuffixOfURL())
				.proxy(HotelUtils.getProxyFromConfigurator()).recurringQueryParams(recurringQueryParams)
				.headerParams(request.getHeaderParams()).build();
		return httpUtils;
	}

	private static <T> Map<String, String> convertToMap(T obj) {

		return new Gson().fromJson(
				GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(obj),
				new TypeToken<HashMap<String, String>>() {}.getType());
	}
}
