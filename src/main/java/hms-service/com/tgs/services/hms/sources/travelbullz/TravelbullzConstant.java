package com.tgs.services.hms.sources.travelbullz;


public enum TravelbullzConstant {

	/*
	 * CancelAll - 1 Define cancelling of whole booking
	 */
	CANCEL_ALL("1"),
	/*
	 * Id-0 defines all rooms have to be cancelled
	 */
	BOOKING_DETAIL_ID("0"),
	SUCCESS_BOOKING_CANCEL("1"),
	CURRENCY("USD");

	public String value;

	TravelbullzConstant(String val) {
		this.value = val;
	}

}
