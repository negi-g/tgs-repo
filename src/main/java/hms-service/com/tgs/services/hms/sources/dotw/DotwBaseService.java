package com.tgs.services.hms.sources.dotw;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomAdditionalInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwCancellationRule;
import com.tgs.services.hms.datamodel.dotw.DotwChild;
import com.tgs.services.hms.datamodel.dotw.DotwChildren;
import com.tgs.services.hms.datamodel.dotw.DotwHotel;
import com.tgs.services.hms.datamodel.dotw.DotwPerDayRoomPrice;
import com.tgs.services.hms.datamodel.dotw.DotwRoomList;
import com.tgs.services.hms.datamodel.dotw.DotwRoomRequest;
import com.tgs.services.hms.datamodel.dotw.DotwRoomResponse;
import com.tgs.services.hms.datamodel.dotw.DotwRoomType;
import com.tgs.services.hms.datamodel.dotw.DotwSearchCriteria;
import com.tgs.services.hms.datamodel.dotw.RateBasis;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierCredential;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class DotwBaseService {


	protected HotelInfo hInfo;
	protected HotelSearchQuery searchQuery;
	protected Results result;
	protected HotelSupplierConfiguration supplierConf;
	protected RestAPIListener listener;
	protected HotelCacheHandler cacheHandler;

	protected HotelSourceConfigOutput sourceConfigOutput;

	protected Map<String, List<RoomInfo>> getRoomInfoMap(DotwHotel hotel, boolean isDetail) {

		Map<String, List<RoomInfo>> roomOccupancyKeyRoomInfosValues = new ConcurrentHashMap<>();
		int index = 0;
		for (DotwRoomResponse room : hotel.getRooms()) {
			int numberOfAdults = Integer.valueOf(room.getAdults());
			int numberOfChilds = Integer.valueOf(room.getChild());
			for (DotwRoomType roomType : room.getRoomType()) {
				RoomInfo roomInfo = new RoomInfo();
				roomInfo.setNumberOfAdults(numberOfAdults);
				roomInfo.setNumberOfChild(numberOfChilds);
				RoomMiscInfo roomMiscInfo = RoomMiscInfo.builder().roomTypeCode(roomType.getRoomtypecode()).build();
				RoomAdditionalInfo roomAdditionalInfo =
						RoomAdditionalInfo.builder().roomId(roomType.getRoomtypecode()).build();
				roomInfo.setRoomAdditionalInfo(roomAdditionalInfo);
				roomInfo.setMiscInfo(roomMiscInfo);
				roomInfo.setRoomType(roomType.getName());
				roomInfo.setRoomCategory(roomType.getName());
				roomInfo.setCheckInDate(searchQuery.getCheckinDate());
				roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());

				StringBuilder key = new StringBuilder();
				key.append(room.getAdults());
				key.append(room.getChild());
				if (room.getChildrenages() != null) {
					key.append(room.getChildrenages());
				}
				key.append(index);
				if (!(roomType.getRateBases() != null
						&& CollectionUtils.isNotEmpty(roomType.getRateBases().getRateBasis())))
					continue;

				for (RateBasis rateBasis : roomType.getRateBases().getRateBasis()) {
					String roomId = rateBasis.getId() + key + roomType.getRoomtypecode();
					RoomInfo rInfo = new GsonMapper<>(roomInfo, RoomInfo.class).convert();
					rInfo.getMiscInfo().setRatePlanCode(rateBasis.getId());
					rInfo.setMealBasis(getMealBasis(rateBasis.getId()));
					HotelCancellationPolicy cancellationPolicy =
							getRoomCancellationPolicy(rateBasis, searchQuery, rInfo);
					rInfo.setCancellationPolicy(cancellationPolicy);

					rInfo.getCancellationPolicy().setId(roomId);

					if (rateBasis.getOnRequest() != null && rateBasis.getOnRequest().equals("1")) {
						rInfo.setIsOptionOnRequest(true);
					}
					rInfo.getMiscInfo().setAllocationDetails(rateBasis.getAllocationDetails());
					rInfo.getMiscInfo().setNotes(rateBasis.getTariffNotes());

					if (StringUtils.isNotBlank(rateBasis.getPassengerNamesRequiredForBooking())) {
						rInfo.getMiscInfo().setNumberOfPassengerNameRequiredForbooking(
								Integer.parseInt(rateBasis.getPassengerNamesRequiredForBooking()));
					}
					if (StringUtils.isNotBlank(rateBasis.getStatus())) {
						if (rateBasis.getStatus().equalsIgnoreCase("checked"))
							rInfo.getMiscInfo().setIsRoomBlocked(true);
						else
							rInfo.getMiscInfo().setIsRoomBlocked(false);
					}

					rInfo.setId(roomId);
					setPriceInRoom(rInfo, rateBasis);
					roomOccupancyKeyRoomInfosValues.computeIfAbsent(key.toString(), (x) -> new LinkedList<>())
							.add(rInfo);
				}
			}
			index++;
		}

		return roomOccupancyKeyRoomInfosValues;
	}


	public void setPriceInRoom(RoomInfo rInfo, RateBasis rateBasis) {

		Double price = getTotalRoomPrice(rateBasis);
		rInfo.setTotalPrice(price);
		List<PriceInfo> priceInfoList = null;
		if (CollectionUtils.isNotEmpty(rateBasis.getDate())) {
			priceInfoList = getDailyPriceInfo(rateBasis.getDate());
		} else {
			priceInfoList = new ArrayList<>();
			long numberOfNights = Duration
					.between(searchQuery.getCheckinDate().atStartOfDay(), searchQuery.getCheckoutDate().atStartOfDay())
					.toDays();
			numberOfNights = Math.max(numberOfNights, 0);
			double perNightPrice = (rInfo.getTotalPrice()) / numberOfNights;
			for (int j = 1; j <= numberOfNights; j++) {
				PriceInfo priceInfo = new PriceInfo();
				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				fareComponents.put(HotelFareComponent.BF, perNightPrice);
				fareComponents.put(HotelFareComponent.SBP, perNightPrice);
				priceInfo.setDay(j);
				priceInfo.setFareComponents(fareComponents);
				priceInfoList.add(priceInfo);
			}
		}
		rInfo.setPerNightPriceInfos(priceInfoList);

	}

	protected List<Option> getOptionList(DotwHotel hotel, boolean isDetail) {
		List<Option> optionList = new ArrayList<>();
		Map<String, List<RoomInfo>> roomOccupancyKeyRoomInfosValues = getRoomInfoMap(hotel, isDetail);

		roomOccupancyKeyRoomInfosValues.entrySet().stream().forEach(entry -> {
			entry.getValue().stream().sorted((p1, p2) -> p1.getTotalPrice().compareTo(p2.getTotalPrice()))
					.collect(Collectors.toList());
		});

		while (!roomOccupancyKeyRoomInfosValues.isEmpty()) {
			List<RoomInfo> optionRoomInfos = new ArrayList<>();
			int index = 0;
			for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
				String key = getKeyFromSearchRoom(room, index);
				List<RoomInfo> roomList = roomOccupancyKeyRoomInfosValues.get(key);
				if (roomList != null && roomList.size() > 0) {
					optionRoomInfos.add(roomList.get(0));
					roomOccupancyKeyRoomInfosValues.get(key).remove(0);
				}
				index++;
			}
			if (optionRoomInfos.size() == searchQuery.getRoomInfo().size()) {
				boolean isOptionOnRequest = checkIfOptionOnRequest(optionRoomInfos);
				String supplierHotelId = StringUtils.isBlank(hotel.getHotelid()) ? hotel.getId() : hotel.getHotelid();
				Option option = Option.builder()
						.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
								.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
								.supplierHotelId(supplierHotelId).sourceId(supplierConf.getBasicInfo().getSourceId())
								.build())
						.roomInfos(optionRoomInfos).id(RandomStringUtils.random(20, true, true)).build();
				String optionId = option.getId();
				option.setInstructions(
						new ArrayList<>(Arrays.asList(Instruction.builder().type(InstructionType.BOOKING_NOTES)
								.msg(optionRoomInfos.get(0).getMiscInfo().getNotes()).build())));
				option.getCancellationPolicy().setId(optionId);
				updatePriceWithMarkup(option.getRoomInfos());
				if (isOptionOnRequest)
					option.setIsOptionOnRequest(true);
				optionList.add(option);
			} else
				break;
		}

		searchQuery.setSourceId(HotelSourceType.DOTW.getSourceId());
		HotelUtils.setOptionCancellationPolicyFromRooms(optionList);
		HotelUtils.setBufferTimeinCnp(optionList, searchQuery);
		return optionList;
	}

	public void setCancellationDeadlineFromRooms(List<RoomInfo> roomInfoList, Option option) {

		LocalDateTime earliestDeadlineDateTime = null;
		LocalDateTime earliestCancellationRestrictedDateTime = null;

		for (RoomInfo roomInfo : roomInfoList) {

			if (earliestDeadlineDateTime == null)
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
			else if (earliestDeadlineDateTime.isAfter(roomInfo.getDeadlineDateTime()))
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();

			if (earliestCancellationRestrictedDateTime == null)
				earliestCancellationRestrictedDateTime = roomInfo.getCancellationRestrictedDateTime();
			else if (earliestCancellationRestrictedDateTime.isAfter(roomInfo.getCancellationRestrictedDateTime()))
				earliestCancellationRestrictedDateTime = roomInfo.getCancellationRestrictedDateTime();

		}

		option.setDeadlineDateTime(earliestDeadlineDateTime);
		if (earliestCancellationRestrictedDateTime != null) {
			option.setCancellationRestrictedDateTime(earliestCancellationRestrictedDateTime);
		}

	}


	protected boolean checkIfOptionOnRequest(List<RoomInfo> roomInfoList) {

		boolean isOptionOnRequest =
				roomInfoList.stream().map(ri -> ri.getIsOptionOnRequest()).reduce(false, (a, b) -> a | b);
		return isOptionOnRequest;

	}


	protected Double getTotalSupplierPrice(List<RoomInfo> roomInfoList) {

		Double totalSupplierOptionPrice =
				roomInfoList.stream().map(ri -> ri.getTotalPrice()).reduce(0.0, (a, b) -> a + b);
		return totalSupplierOptionPrice;
	}


	protected String getMealBasis(String rateBasisId) {

		if (rateBasisId.equals("0"))
			return BaseHotelConstants.ROOM_ONLY;
		else if (rateBasisId.equals("1331"))
			return "Breakfast";
		else if (rateBasisId.equals("1334"))
			return "Half Board";
		else if (rateBasisId.equals("1335"))
			return "Full Board";
		else if (rateBasisId.equals("1336"))
			return "All Inclusive";
		return "Default";
	}

	protected String getTitleFromSaluationCode(String code) {

		if (code.equals("14632"))
			return "Child";
		else if (code.equals("558"))
			return "Dr.";
		else if (code.equals("1671"))
			return "Madam";
		else if (code.equals("9234"))
			return "Messrs.";
		else if (code.equals("15134"))
			return "Miss";
		else if (code.equals("149"))
			return "Mrs.";
		else if (code.equals("148"))
			return "Ms.";
		else if (code.equals("1328"))
			return "Sir";
		else if (code.equals("3801"))
			return "Sir/Madam";
		return "Mr.";


	}

	protected LocalDateTime getFormattedDate(String localDateTime) {

		String input = localDateTime.replace(" ", "T");
		return LocalDateTime.parse(input);

	}

	protected HotelCancellationPolicy getRoomCancellationPolicy(RateBasis rateBasis, HotelSearchQuery searchQuery,
			RoomInfo rInfo) {
		String fromCurrency = HotelBaseSupplierUtils.getSupplierCurrency(supplierConf);

		LocalDateTime checkInDate = searchQuery.getCheckinDate().atTime(12, 00);
		List<PenaltyDetails> pds = new ArrayList<>();
		List<DotwCancellationRule> dcps = rateBasis.getCancellationRules();
		if (dcps == null) {
			return null;
		}
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime deadlineDateTime = now;
		LocalDateTime cancellationRestrictedDatetime = null;
		boolean isFullRefundAllowed = false;
		for (DotwCancellationRule dcp : dcps) {

			if (dcp.getCancelCharge() != null && dcp.getCancelCharge() == 0) {
				isFullRefundAllowed = true;
				deadlineDateTime = getFormattedDate(dcp.getToDate());
				PenaltyDetails penaltyDetails1 =
						PenaltyDetails.builder().fromDate(now).toDate(deadlineDateTime).penaltyAmount(0.0).build();
				pds.add(penaltyDetails1);
			} else if (dcp.getCancelRestricted() != null && dcp.getCancelRestricted()) {
				if (pds.size() > 0)
					cancellationRestrictedDatetime = pds.get(pds.size() - 1).getToDate();
				else
					cancellationRestrictedDatetime = LocalDateTime.now();
				PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(cancellationRestrictedDatetime)
						.toDate(checkInDate).isCancellationRestricted(true).build();
				pds.add(penaltyDetails1);
				break;

			} else if (dcp.getAmendRestricted() != null && dcp.getAmendRestricted()
					|| dcp.getNoShowPolicy() != null && dcp.getNoShowPolicy())
				continue;
			else {
				LocalDateTime fromDate = null;
				LocalDateTime toDate = checkInDate;
				if (StringUtils.isNotBlank(dcp.getToDate())) {
					toDate = getFormattedDate(dcp.getToDate());
				}
				if (pds.size() > 0)
					fromDate = pds.get(pds.size() - 1).getToDate();
				else
					fromDate = LocalDateTime.now();
				// Double totalBookingAmount = getTotalRoomPrice(rateBasis);
				PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(fromDate).toDate(toDate)
						.penaltyAmount(HotelBaseSupplierUtils.getAmountBasedOnCurrency(dcp.getCancelCharge(),
								fromCurrency, HotelSourceType.DOTW.name(), BaseHotelConstants.DEFAULT_CURRENCY))
						.build();
				pds.add(penaltyDetails1);
			}
		}

		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder()
				.isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds).miscInfo(CancellationMiscInfo.builder()
						.isBookingAllowed(true).isSoldOut(false).isCancellationPolicyBelongToRoom(true).build())
				.build();
		rInfo.setDeadlineDateTime(deadlineDateTime);
		rInfo.setCancellationRestrictedDateTime(cancellationRestrictedDatetime);
		return cancellationPolicy;
	}

	protected Double getTotalRoomPrice(RateBasis rateBasis) {

		Double amount = rateBasis.getTotalInRequestedCurrency() != null ? rateBasis.getTotalInRequestedCurrency()
				: rateBasis.getTotal();
		String fromCurrency = HotelBaseSupplierUtils.getSupplierCurrency(supplierConf);

		return HotelBaseSupplierUtils.getAmountBasedOnCurrency(amount, fromCurrency, HotelSourceType.DOTW.name(),
				BaseHotelConstants.DEFAULT_CURRENCY);

	}


	protected Customer getCustomer(HotelSupplierConfiguration conf) {

		Customer customer = new Customer();
		HotelSupplierCredential supplierCredentials = conf.getHotelSupplierCredentials();
		customer.setUsername(supplierCredentials.getUserName());
		customer.setPassword(supplierCredentials.getPassword());
		customer.setId(supplierCredentials.getClientId());
		customer.setSource("1");
		return customer;
	}

	protected Map<String, String> getHeaderParams() {

		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("content-type", "text/xml");
		headerParams.put("Accept-Encoding", "gzip");
		headerParams.put("Connection", "close");
		return headerParams;

	}

	protected HttpUtils getHttpRequest(String xmlRequest, HotelSupplierConfiguration supplierConf) {

		HttpUtils httpUtils = HttpUtils.builder().urlString(supplierConf.getHotelSupplierCredentials().getUrl())
				.postData(xmlRequest).headerParams(getHeaderParams()).requestMethod(HttpUtils.REQ_METHOD_POST)
				.timeout(400 * 1000).build();
		return httpUtils;
	}


	protected DotwSearchCriteria getSearchCriteriaFromSearchQuery(HotelSearchQuery searchQuery) {

		String countryOfResidence = HotelBaseSupplierUtils.getSupplierCountryCode(
				searchQuery.getSearchCriteria().getCountryOfResidence(), HotelSourceType.DOTW.name());
		String nationalityCode = HotelBaseSupplierUtils
				.getSupplierCountryCode(searchQuery.getSearchCriteria().getNationality(), HotelSourceType.DOTW.name());
		List<DotwRoomRequest> rooms = new ArrayList<>();
		DotwRoomList roomList = new DotwRoomList();
		roomList.setNo(searchQuery.getRoomInfo().size());
		DotwSearchCriteria searchCriteria = new DotwSearchCriteria();
		String formattedFromDate = searchQuery.getCheckinDate().format(DateTimeFormatter.ISO_DATE);
		String formattedToDate = searchQuery.getCheckoutDate().format(DateTimeFormatter.ISO_DATE);
		searchCriteria.setFromDate(String.valueOf(formattedFromDate));
		searchCriteria.setToDate(String.valueOf(formattedToDate));
		searchCriteria.setCurrency(DotwConstants.CURRENCY.getValue());
		int index = 0;
		for (RoomSearchInfo roomSearchInfo : searchQuery.getRoomInfo()) {
			DotwRoomRequest room = new DotwRoomRequest();
			room.setNumber(String.valueOf(index));
			room.setAdultsCode(roomSearchInfo.getNumberOfAdults());
			room.setPassengerCountryOfResidence(countryOfResidence);
			room.setPassengerNationality(nationalityCode);
			DotwChildren children = getRoomChildrenRequest(roomSearchInfo);
			room.setChildren(children);
			room.setRateBasis(-1);
			rooms.add(room);
			index++;
		}
		roomList.setRoom(rooms);
		searchCriteria.setRooms(roomList);
		return searchCriteria;
	}


	public DotwChildren getRoomChildrenRequest(RoomSearchInfo roomSearchInfo) {

		DotwChildren children = new DotwChildren();
		children.setNo(0);
		if (roomSearchInfo.getNumberOfChild() != null && roomSearchInfo.getNumberOfChild() > 0) {
			children.setNo(roomSearchInfo.getNumberOfChild());
			List<DotwChild> dotwChild = new ArrayList<>();
			int index = 0;
			for (int childAge : roomSearchInfo.getChildAge()) {
				DotwChild child = new DotwChild();
				child.setRunno(index++);
				child.setChild(childAge);
				dotwChild.add(child);
			}
			children.setChild(dotwChild);
		}
		return children;
	}

	public List<PriceInfo> getDailyPriceInfo(List<DotwPerDayRoomPrice> dates) {
		String fromCurrency = HotelBaseSupplierUtils.getSupplierCurrency(supplierConf);
		List<PriceInfo> priceInfoList = new ArrayList<>();
		int day = 1;
		for (DotwPerDayRoomPrice perDayPriceInfo : dates) {

			PriceInfo priceInfo = new PriceInfo();
			priceInfo.setDay(day);
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			Double perDayPrice = perDayPriceInfo.getPriceInRequestedCurrency() != null
					? perDayPriceInfo.getPriceInRequestedCurrency().getPriceInRequestedCurrency()
					: perDayPriceInfo.getPrice().getPrice();
			perDayPrice = HotelBaseSupplierUtils.getAmountBasedOnCurrency(perDayPrice, fromCurrency,
					HotelSourceType.DOTW.name(), BaseHotelConstants.DEFAULT_CURRENCY);
			fareComponents.put(HotelFareComponent.BF, perDayPrice);
			fareComponents.put(HotelFareComponent.SBP, perDayPrice);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
			day++;

		}
		return priceInfoList;

	}

	public void updatePriceWithMarkup(List<RoomInfo> rInfoList) {

		double supplierMarkup = (sourceConfigOutput == null || sourceConfigOutput.getSupplierMarkup() == null) ? 0.0
				: sourceConfigOutput.getSupplierMarkup();

		for (RoomInfo roomInfo : rInfoList) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				double supplierGrossPrice = priceInfo.getFareComponents().get(HotelFareComponent.BF);
				priceInfo.getFareComponents().put(HotelFareComponent.CMU, (supplierGrossPrice * supplierMarkup) / 100);
				priceInfo.getFareComponents().put(HotelFareComponent.SGP, supplierGrossPrice);
				priceInfo.getFareComponents().put(HotelFareComponent.SNP, supplierGrossPrice);
				priceInfo.getFareComponents().put(HotelFareComponent.BF, supplierGrossPrice);

			}
		}

	}

	private String getKeyFromSearchRoom(RoomSearchInfo room, int index) {

		StringBuilder key = new StringBuilder();
		key.append(room.getNumberOfAdults());
		key.append(room.getNumberOfChild() != null ? room.getNumberOfChild() : 0);
		if (room.getChildAge() != null && CollectionUtils.isNotEmpty(room.getChildAge())) {
			String childAge = room.getChildAge().stream().map(String::valueOf).collect(Collectors.joining(","));
			key.append(childAge);
		}
		key.append(index);
		return key.toString();
	}

}
