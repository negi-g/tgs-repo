package com.tgs.services.hms.restcontroller.mapping;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelStaticDataMappingRequest;
import com.tgs.services.hms.datamodel.inventory.MappedHotelInfo;
import com.tgs.services.hms.manager.mapping.HotelInfoMappingManager;
import com.tgs.services.hms.restmodel.HotelInfoStaticDataRequest;
import com.tgs.services.hms.restmodel.HotelMealInfoResponse;
import com.tgs.services.hms.restmodel.HotelSupplierMappedListResponse;


@RestController
@RequestMapping("/hms/v1")
public class HotelInfoMappingController {

	@Autowired
	HotelInfoMappingManager mappingManager;
	
	@RequestMapping(value = "/hotel-mapping", method = RequestMethod.POST)
	protected BaseResponse saveSupplierHotelMapping(HttpServletRequest request , HttpServletResponse response
			, @RequestBody HotelInfoStaticDataRequest staticDataRequest) throws Exception {
		mappingManager.processHotelInfoMapping(staticDataRequest.getMappingRequest());
		return new BaseResponse();
	}
	
	/*
	 * Mapped Hotels For a supplier will be fetched only in case of Inventory.
	 * This is why this API don't have any pagination enabled.
	 * So Need to make changes before using this API for normal suppliers.
	 */
	@RequestMapping(value = "/hotel-mapping", method = RequestMethod.GET)
	protected HotelSupplierMappedListResponse fetchSupplierHotelMapping(HttpServletRequest request , HttpServletResponse response
			, @RequestBody HotelInfoStaticDataRequest staticDataRequest) throws Exception {
		
		HotelStaticDataMappingRequest mappingRequest = staticDataRequest.getMappingRequest();
		List<MappedHotelInfo> mappedHotels = mappingManager.fetchHotelInfoMapping(mappingRequest);
		HotelSupplierMappedListResponse mappingResponse = new HotelSupplierMappedListResponse();
		mappingResponse.setHInfoList(mappedHotels);
		mappingResponse.setSourceId(mappingRequest.getSourceId());
		mappingResponse.setSupplierId(mappingRequest.getSupplierId());
		return mappingResponse;
	}
	
	@RequestMapping(value = "/meal-mapping", method = RequestMethod.GET)
	protected HotelMealInfoResponse listSourceInfo(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return mappingManager.getMealInfo();
	}
}

