package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.restmodel.FetchHotelCityInfoResponse;
import com.tgs.services.hms.restmodel.FetchHotelRegionInfoResponse;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class TripjackRegionInfoStaticDataService extends TripjackBaseService {

	protected FetchHotelRegionInfoResponse staticDataRequest;
	private HotelRegionInfoMappingManager regionInfoMappingManager;

	public void saveTripjackCityList() throws IOException {
		String endPoint = supplierConf.getHotelSupplierCredentials().getUrl();
		String next = "";
		do {
			List<HotelRegionInfoQuery> cityList = new ArrayList<>();
			String requestUrl = StringUtils.join(endPoint, TripjackConstant.STATIC_CITY_SUFFIX.value, next);
			HttpUtils httpUtils = HttpUtils.builder().urlString(requestUrl).headerParams(getHeaderParams())
					.requestMethod("GET").timeout(90 * 1000).build();
			FetchHotelCityInfoResponse regionInfoResponse =
					httpUtils.getResponse(FetchHotelCityInfoResponse.class)
					.orElse(null);
			regionInfoResponse.getResponse().getCityInfoList().forEach(city -> {
				HotelRegionInfoQuery regionInfoQuery = new HotelRegionInfoQuery();
				regionInfoQuery.setRegionId(String.valueOf(city.getId()));
				regionInfoQuery.setRegionName(city.getCityName());
				regionInfoQuery.setCountryName(city.getCountryName());
				regionInfoQuery.setSupplierName(HotelSourceType.TRIPJACK.name());
				cityList.add(regionInfoQuery);
			});
			regionInfoMappingManager.saveRegionInfoMappingList(cityList, true, false);
			next = regionInfoResponse.getResponse().getNext();
		} while (next != null);
	}
}
