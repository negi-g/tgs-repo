package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.google.common.collect.Lists;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.Address.AddressBuilder;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.City.CityBuilder;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.Country.CountryBuilder;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelInfo.HotelInfoBuilder;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo.HotelMiscInfoBuilder;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchPreferences;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.cleartrip.CleartripBaseRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripCancellationPolicySuccessResponse;
import com.tgs.services.hms.datamodel.cleartrip.CleartripOrderMapping;
import com.tgs.services.hms.datamodel.cleartrip.CleartripRetrieveBookingResponse;
import com.tgs.services.hms.datamodel.cleartrip.CleartripRetrieveBookingSuccessResponse;
import com.tgs.services.hms.datamodel.cleartrip.ContactDetail;
import com.tgs.services.hms.datamodel.cleartrip.HotelDetail;
import com.tgs.services.hms.datamodel.cleartrip.Pricing;
import com.tgs.services.hms.datamodel.cleartrip.Room;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
public class CleartripRetrieveBookingService {

	private HotelImportBookingParams importBookingParams;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConf;
	private HotelImportedBookingInfo bookingInfo;
	protected RestAPIListener listener;
	private HotelSupplierRegionInfoManager supplierRegionInfoManager;

	private static final DateTimeFormatter dateTimeFormatter_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final String RETRIEVE_BOOKING_SUFFIX = "/hotels/api/v2/trips/";

	public void init() {
		supplierRegionInfoManager = (HotelSupplierRegionInfoManager) SpringContext.getApplicationContext()
				.getBean("hotelSupplierRegionInfoManager");
	}

	public void importBookingDetails() throws IOException {

		CleartripRetrieveBookingResponse bookingResponse = retrieveBookingDetails();
		bookingInfo = createBookingResponse(bookingResponse);
	}

	public CleartripRetrieveBookingResponse retrieveBookingDetails() throws IOException {

		HttpUtilsV2 httpUtils = null;
		CleartripRetrieveBookingResponse response = null;
		RestAPIListener listener = null;
		try {
			listener = new RestAPIListener("");
			CleartripBaseRequest retrieveBookingRequest =
					createRetrieveBookingRequest(importBookingParams.getSupplierBookingId());
			httpUtils = CleartripUtils.getResponseURL(retrieveBookingRequest, supplierConf);
			response = httpUtils.getResponse(CleartripRetrieveBookingResponse.class).orElseGet(null);
			log.info("Cleartrip Retrieve Booking Response is {}", GsonUtils.getGson().toJson(response));
			return response;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.CLEARTRIP.name())
					.requestType(BaseHotelConstants.RETRIEVE_BOOKING).responseString(httpUtils.getResponseString())
					.urlString(httpUtils.getUrlString()).postData(httpUtils.getPostData())
					.logKey(importBookingParams.getBookingId()).prefix(importBookingParams.getFlowType().getName())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

		}
	}

	private HotelImportedBookingInfo createBookingResponse(CleartripRetrieveBookingResponse bookingResponse) {

		if (!ObjectUtils.isEmpty(bookingResponse.getError())) {
			log.error("Unable to get booking result due to {}", bookingResponse.getError());
			throw new CustomGeneralException(SystemError.INVALID_SUPPLIER_BOOKING_ID);
		}

		CleartripRetrieveBookingSuccessResponse bookingInfo = bookingResponse.getSuccess();
		Pricing pricing = bookingInfo.getPricing();
		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
		searchQueryBuilder.checkinDate(
				LocalDate.parse(bookingInfo.getHotelDetail().getCheckInDate(), dateTimeFormatter_YYYY_MM_DD));
		searchQueryBuilder.checkoutDate(
				LocalDate.parse(bookingInfo.getHotelDetail().getCheckOutDate(), dateTimeFormatter_YYYY_MM_DD));
		searchQueryBuilder.sourceId(supplierConf.getBasicInfo().getSourceId());
		HotelSearchPreferences searchPreferences = new HotelSearchPreferences();
		String currency = pricing.getCurrency();
		searchPreferences.setCurrency(currency);
		searchQueryBuilder.searchPreferences(searchPreferences);
		searchQueryBuilder.miscInfo(
				HotelSearchQueryMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName()).build());

		HotelInfo hotelInfo = setHotelInfo(bookingInfo, searchQueryBuilder.build());
		ContactDetail contactInfo = bookingInfo.getContactDetail();
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		deliveryInfo.setContacts(
				StringUtils.isEmpty(contactInfo.getMobile()) ? null : Lists.newArrayList(contactInfo.getMobile()));
		deliveryInfo.setEmails(
				StringUtils.isEmpty(contactInfo.getEmail()) ? null : Lists.newArrayList(contactInfo.getEmail()));
		deliveryInfo.cleanData();
		String orderStatus = "";
		try {
			orderStatus = CleartripOrderMapping.valueOf(bookingInfo.getPaymentDetail().getStatus()).getCode();
		} catch (IllegalArgumentException e) {
			log.error("Unable to get correct order status for bookingid {}", importBookingParams.getBookingId());
		}
		return HotelImportedBookingInfo.builder().hInfo(hotelInfo).searchQuery(searchQueryBuilder.build())
				.deliveryInfo(deliveryInfo).orderStatus(orderStatus).bookingCurrencyCode(currency).build();

	}

	private HotelInfo setHotelInfo(CleartripRetrieveBookingSuccessResponse bookingResponse,
			HotelSearchQuery searchQuery) {

		HotelInfoBuilder hotelInfoBuilder = HotelInfo.builder();
		AddressBuilder addressBuilder = Address.builder();
		HotelDetail hotelDetail = bookingResponse.getHotelDetail();
		addressBuilder.addressLine1(hotelDetail.getAddress());
		CityBuilder cityBuilder = City.builder();
		cityBuilder.name(hotelDetail.getCity());
		addressBuilder.city(cityBuilder.build());
		addressBuilder.cityName(hotelDetail.getCity());

		HotelSupplierRegionInfo supplierRegionInfo =
				supplierRegionInfoManager.findByCityNameAndSupplierName(hotelDetail.getCity(),
						HotelSourceType.CLEARTRIP);

		CountryBuilder countryBuilder = Country.builder();
		countryBuilder.name(supplierRegionInfo.getCountryName());
		addressBuilder.country(countryBuilder.build());
		addressBuilder.countryName(supplierRegionInfo.getCountryName());
		hotelInfoBuilder.address(addressBuilder.build());

		HotelMiscInfoBuilder hotelMiscInfoBuilder = HotelMiscInfo.builder();
		hotelMiscInfoBuilder.supplierBookingId(bookingResponse.getTripRef());
		hotelMiscInfoBuilder.supplierStaticHotelId(bookingResponse.getHotelDetail().getHotelId());
		hotelMiscInfoBuilder.supplierBookingReference(bookingResponse.getTripRef());
		hotelMiscInfoBuilder.supplierBookingConfirmationNo(bookingResponse.getBookingInfo().getVoucherNumber());
		hotelInfoBuilder.miscInfo(hotelMiscInfoBuilder.build());

		List<Option> optionList = new ArrayList<>();
		optionList.add(populateOptionInfo(bookingResponse, searchQuery));
		hotelInfoBuilder.options(optionList);
		return hotelInfoBuilder.build();
	}

	public Option populateOptionInfo(CleartripRetrieveBookingSuccessResponse bookingResponse,
			HotelSearchQuery searchQuery) {

		Option option = Option.builder()
				.id(bookingResponse.getTripRef() + "_" + bookingResponse.getHotelDetail().getHotelId()).build();

		List<RoomInfo> roomInfoList = populateRoomInfo(bookingResponse, searchQuery);
		option.setRoomInfos(roomInfoList);
		option.setMiscInfo(OptionMiscInfo.builder().secondarySupplier(supplierConf.getBasicInfo().getSupplierId())
				.supplierHotelId(bookingResponse.getHotelDetail().getHotelId())
				.sourceId(supplierConf.getBasicInfo().getSourceId()).supplierId(importBookingParams.getSupplierId())
				.build());

		CleartripCancellationPolicySuccessResponse cancellationResponse =
				createCancellationPolicyResponse(bookingResponse.getCancellationPolicy());
		CleartripUtils.setCancellationPolicyInOptionNew(option, cancellationResponse, sourceConf, searchQuery);
		CleartripUtils.updatePriceWithClientCommissionComponents(option.getRoomInfos(), sourceConf);
		return option;
	}


	private List<RoomInfo> populateRoomInfo(CleartripRetrieveBookingSuccessResponse bookingResponse,
			HotelSearchQuery searchQuery) {

		List<RoomInfo> roomInfos = new ArrayList<>();
		RoomInfo roomInfo = new RoomInfo();
		roomInfo.setCheckInDate(searchQuery.getCheckinDate());
		roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
		long daysDuration = Duration
				.between(searchQuery.getCheckoutDate().atStartOfDay(), searchQuery.getCheckoutDate().atStartOfDay())
				.toDays() + 1;
		populatePriceInRoomInfo(roomInfo, bookingResponse.getPricing(),
				Integer.parseInt(bookingResponse.getRoomCount()), daysDuration);
		roomInfos.add(roomInfo);
		CleartripUtils.updatePriceWithClientCommissionComponents(roomInfos, sourceConf);
		roomInfos = createMultipleRoomsOfSameRoomType(roomInfo, bookingResponse);
		return roomInfos;
	}

	public void populatePriceInRoomInfo(RoomInfo roomInfo, Pricing pricing, int noOfRooms, long daysDuration) {

		Double total = 0.0;
		List<PriceInfo> priceInfoList = new ArrayList<>();
		for (int i = 1; i <= daysDuration; i++) {

			PriceInfo priceInfo = new PriceInfo();
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, pricing.getRoomRate() / noOfRooms / daysDuration);
			total += fareComponents.get(HotelFareComponent.BF);

			fareComponents.put(HotelFareComponent.TSF, pricing.getHotelTaxes() / noOfRooms / daysDuration);
			total += fareComponents.get(HotelFareComponent.TSF);

			fareComponents.put(HotelFareComponent.SDS, pricing.getDiscount() / noOfRooms / daysDuration);
			total -= fareComponents.get(HotelFareComponent.SDS);

			priceInfo.setDay(i);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
			i++;
		}

		roomInfo.setMiscInfo(RoomMiscInfo.builder().totalBaseAmount(BigDecimal.valueOf(total)).build());
		roomInfo.setPerNightPriceInfos(priceInfoList);
	}

	private List<RoomInfo> createMultipleRoomsOfSameRoomType(RoomInfo roomInfo,
			CleartripRetrieveBookingSuccessResponse bookingResponse) {

		List<RoomInfo> roomInfos = new ArrayList<>();
		List<Room> rooms = bookingResponse.getRooms();
		for (Room room : rooms) {
			int noOfAdults = room.getGuests().getAdults();
			int noOfChildren = CollectionUtils.isNotEmpty(room.getGuests().getChildrenAge())
					? room.getGuests().getChildrenAge().size()
					: 0;

			RoomInfo copyRoomInfo = new GsonMapper<>(roomInfo, RoomInfo.class).convert();
			copyRoomInfo.setNumberOfAdults(noOfAdults);
			copyRoomInfo.setNumberOfChild(noOfChildren);
			copyRoomInfo.setRoomCategory(room.getRoomName());
			copyRoomInfo.setRoomType(room.getTypeName());
			roomInfos.add(copyRoomInfo);
		}
		return roomInfos;
	}

	private CleartripCancellationPolicySuccessResponse createCancellationPolicyResponse(String cancellationPolicy) {

		String refundable = "NON_REFUNDABLE";
		if (cancellationPolicy.contains("Fully refundable")) {

			if (cancellationPolicy.contains("not be refunded")) {
				refundable = "PARTIALLY_REFUNDABLE";
			} else {
				refundable = "REFUNDABLE";
			}
		}
		return CleartripCancellationPolicySuccessResponse.builder().cancelPolicy(cancellationPolicy)
				.refundable(refundable).build();
	}


	public CleartripBaseRequest createRetrieveBookingRequest(String bookingId) {

		return CleartripBaseRequest.builder().suffixOfURL(RETRIEVE_BOOKING_SUFFIX + bookingId).build();
	}
}
