package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.aerospike.client.query.Filter;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.qtech.SupplierHotelIdMapping;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class QTechHotelInfoFactory extends AbstractHotelInfoFactory {


	public QTechHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	public void searchAvailableHotels() throws IOException, InterruptedException {
		if (StringUtils.isBlank(searchQuery.getSearchCriteria().getRegionId())
				&& StringUtils.isBlank(searchQuery.getSearchPreferences().getHotelId())) {
			log.info("Not able to search for searchid {} and supplier {} as hotel wise search info is not present",
					searchQuery.getSearchId(), supplierConf.getBasicInfo().getSupplierName());
			return;
		}
		if (Objects.nonNull(supplierConf.getHotelSupplierCredentials().getIsHotelidSearchEnabled())
				&& supplierConf.getHotelSupplierCredentials().getIsHotelidSearchEnabled()) {
			Set<String> hotelids;
			Set<SupplierHotelIdMapping> supplierHotelMappingInfos;
			if (StringUtils.isNotBlank(searchQuery.getSearchPreferences().getHotelId())) {
				log.info("Starting hotel wise search for hotelid {}, supplier {}",
						searchQuery.getSearchPreferences().getHotelId(), supplierConf.getBasicInfo().getSupplierName());
				hotelids = new HashSet<>(Arrays.asList(searchQuery.getSearchPreferences().getHotelId()));
				supplierHotelMappingInfos = getSupplierHotelInfofromHotelids(hotelids);
				if (!CollectionUtils.isNullOrEmpty(supplierHotelMappingInfos)) {
					setSupplierCityAndCountry(supplierHotelMappingInfos);
				} else {
					log.info("No mapping found for hotelid {} for supplier {}",
							searchQuery.getSearchPreferences().getHotelId(),
							supplierConf.getBasicInfo().getSupplierName());
				}
			} else {
				hotelids = getHotelIdsOfDirectSupplierBasedOnCity();
				supplierHotelMappingInfos = getSupplierHotelInfofromHotelids(hotelids);
			}
			searchResult = getResultOfAllPropertyIds(getSupplierHotelIdsFromHotelInfo(supplierHotelMappingInfos));
			return;
		} else {
			QtechSearchService searchService =
					QtechSearchService.builder().supplierConf(this.getSupplierConf()).searchQuery(this.getSearchQuery())
							.supplierRegionInfo(supplierRegionInfo).sourceConfigOutput(sourceConfigOutput).build();
			searchService.doSearch(0);
			searchResult = searchService.getSearchResult();
		}
	}

	private void setSupplierCityAndCountry(Set<SupplierHotelIdMapping> supplierHotelIdMappingInfos) {

		this.supplierRegionInfo = HotelSupplierRegionInfo.builder()
				.regionId(supplierHotelIdMappingInfos.iterator().next().getAdditionalInfo().getSupplierCity())
				.countryId(supplierHotelIdMappingInfos.iterator().next().getAdditionalInfo().getSupplierCountry())
				.build();
	}

	private Set<String> getSupplierHotelIdsFromHotelInfo(Set<SupplierHotelIdMapping> supplierHotelIdMappingInfos) {
		Set<String> supplierHotelIds = new HashSet<>();
		supplierHotelIdMappingInfos.forEach(hotelIdMappingInfo -> {
			supplierHotelIds.add(hotelIdMappingInfo.getSupplierHotelId());
		});
		return supplierHotelIds;
	}

	@Override
	protected HotelSearchResult doSearchForSupplier(Set<String> hotelids, int threadCount)
			throws IOException, JAXBException {
		log.info("Doing search for searchid {} and supplier {} with hotelids {}", searchQuery.getSearchId(),
				supplierConf.getBasicInfo().getSupplierName(), hotelids.size());
		QtechSearchService searchService = QtechSearchService.builder().supplierConf(this.getSupplierConf())
				.searchQuery(this.getSearchQuery()).hotelids(hotelids).supplierRegionInfo(supplierRegionInfo)
				.sourceConfigOutput(sourceConfigOutput).build();
		searchService.doSearch(threadCount);
		return searchService.getSearchResult();
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException {
		QtechSearchService searchService = QtechSearchService.builder().sourceConfigOutput(sourceConfigOutput)
				.searchQuery(searchQuery).supplierConf(this.getSupplierConf()).hInfo(hInfo).build();
		searchService.doDetailSearch();
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		QTechCancellationPolicyService cancellationPolicyService = QTechCancellationPolicyService.builder().hInfo(hotel)
				.supplierConf(this.getSupplierConf()).sourceConfigOutput(sourceConfigOutput).build();
		cancellationPolicyService.searchCancellationPolicy(logKey);

	}

	private Set<SupplierHotelIdMapping> getSupplierHotelInfofromHotelids(Set<String> hotelIds) {
		Set<SupplierHotelIdMapping> supplierHotelIdMappingInfos = new HashSet<>();
		log.debug("Fetching supplier hotelMappingInfo from cache for total hotelids {} and city {} and searchid {}",
				hotelIds.size(), getCityName(), searchQuery.getSearchId());
		// Max batch request limit of aerospike is 5000
		int partitionSize = 4000;
		List<Set<String>> partitionedSupplierHotelIdKeys = HotelUtils.partitionSet(hotelIds, partitionSize);
		try {
			partitionedSupplierHotelIdKeys.forEach(hotelIdKeys -> {
				String[] hotelIdsArray = hotelIdKeys.stream().toArray(String[]::new);
				log.debug("Hotelids set converted to keysArray. Size is {}", hotelIdsArray.length);
				Map<String, Map<String, String>> supplierhotelIdMappingMap = cachingCommunicator.get(CacheMetaInfo
						.builder().set(CacheSetName.SUPPLIER_HOTEL_ID_MAPPING.getName()).keys(hotelIdsArray)
						.bins(new String[] {BinName.HOTELID.name(), BinName.SUPHOTELINFO.name()})
						.namespace(CacheNameSpace.HOTEL.getName()).build(), String.class);
				for (Map.Entry<String, Map<String, String>> hotelId : supplierhotelIdMappingMap.entrySet()) {
					Map<String, String> supplierHotelIdMappingInfo = hotelId.getValue();
					SupplierHotelIdMapping hotelIdMappingInfo = new Gson().fromJson(
							supplierHotelIdMappingInfo.get(BinName.SUPHOTELINFO.name()), SupplierHotelIdMapping.class);
					supplierHotelIdMappingInfos.add(hotelIdMappingInfo);
				}
			});
		} finally {
			log.info("Retrieved supplier hotelIdMappingInfos for searchid {} and supplier {}. Size is {}",
					searchQuery.getSearchId(), supplierConf.getBasicInfo().getSupplierName(),
					supplierHotelIdMappingInfos.size());
		}

		return supplierHotelIdMappingInfos;
	}

	private Set<String> getHotelIdsOfDirectSupplierBasedOnCity() {
		String supplierName = getDirectSupplierName();
		if (Objects.nonNull(supplierName)) {
			return getHotelIdsOfCity(supplierName);
		}
		return null;
	}

	protected Set<String> getHotelIdsOfCity(String supplierName) {
		String cityName = getCityName();
		Set<String> hotelIds = new HashSet<>();
		log.info("Fetching hotel ids from cache for city {} and searchid {}", cityName, searchQuery.getSearchId());
		try {
			HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();
			String polygonInfo = getPolygonInfo();
			if (StringUtils.isNotBlank(polygonInfo)) {
				hotelIds = fetchPropertyIdsByPolygonInfo(supplierName, polygonInfo, staticDataService);
				return hotelIds;
			} else if (!cityName.contains("@")) {
				cityName = staticDataService.getCityName(supplierName, cityName.toLowerCase());
				CacheMetaInfo supplierMappingMetaInfo =
						CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
								.set(staticDataService.getSupplierSetName(supplierName))
								.bins(new String[] {BinName.CITY.name(), BinName.RATING.name(), BinName.HOTELID.name()})
								.plainData(true).build();
				Map<String, Map<String, String>> supplierhotelIdMap = cachingCommunicator.getResultSet(
						supplierMappingMetaInfo, String.class, Filter.equal(BinName.CITY.getName(), cityName));
				for (Map.Entry<String, Map<String, String>> supplierHotel : supplierhotelIdMap.entrySet()) {
					Map<String, String> supplierInfo = supplierHotel.getValue();
					if (StringUtils.isNotBlank(supplierInfo.get(BinName.RATING.name()))
							&& searchQuery.getSearchPreferences().getRatings()
									.contains(Integer.parseInt(supplierInfo.get(BinName.RATING.name())))) {
						hotelIds.add(supplierInfo.get(BinName.HOTELID.name()));
					}
				}
			} else {
				hotelIds = fetchHotelIdsBySupplierCityName(supplierName, cityName, staticDataService);
				return hotelIds;
			}
		} catch (Exception e) {
			log.info("Unable to fetch hotel ids of city {}", cityName, e);
			return null;
		} finally {
			log.info("Fetched hotelids from cache for city {} and searchid {}. Size is {}", cityName,
					searchQuery.getSearchId(), hotelIds.size());
		}
		return hotelIds;
	}

	public Set<String> fetchHotelIdsBySupplierCityName(String supplierName, String regionKey,
			HotelStaticDataService staticDataService) {

		String bin = "";
		String searchRegion = "";
		Set<String> hotelIds = new HashSet<>();
		String[] region = regionKey.split("@");
		String city = region[0];
		String state = region[1];
		String country = region[2];
		if (!city.equals("NA")) {
			bin = BinName.CITY.name();
			searchRegion = city;
		} else if (!state.equals("NA")) {
			bin = BinName.STATE.name();
			searchRegion = state;
		} else if (!country.equals("NA")) {
			bin = BinName.COUNTRY.name();
			searchRegion = country;
		}
		city = staticDataService.getCityName(supplierName, city);
		state = staticDataService.getStateName(supplierName, state);
		country = staticDataService.getCountryName(supplierName, country);
		// HotelUtils.fetchBinAndRegionFromKey(bin, searchRegion, regionKey);
		CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(staticDataService.getSupplierSetName(supplierName)).bins(new String[] {BinName.CITY.name(),
						BinName.STATE.name(), BinName.COUNTRY.name(), BinName.RATING.name(), BinName.HOTELID.name()})
				.plainData(true).build();
		searchRegion = String.join("_", supplierName, searchRegion.toLowerCase());
		log.info(
				"Fetching hotel id from cache for region {} and searchid {}. City {}, State {}, Country {} and Rating {}",
				searchRegion, searchQuery.getSearchId(), city, state, country,
				searchQuery.getSearchPreferences().getRatings());
		try {
			Map<String, Map<String, String>> supplierhotelIdMap = cachingCommunicator
					.getResultSet(supplierMappingMetaInfo, String.class, Filter.equal(bin, searchRegion));
			for (Map.Entry<String, Map<String, String>> supplierHotel : supplierhotelIdMap.entrySet()) {

				Map<String, String> supplierInfo = supplierHotel.getValue();
				if (StringUtils.isNotBlank(supplierInfo.get(BinName.RATING.name()))
						&& searchQuery.getSearchPreferences().getRatings()
								.contains(Integer.parseInt(supplierInfo.get(BinName.RATING.name())))) {
					if (StringUtils.isNotBlank(supplierInfo.get(BinName.STATE.name()))
							&& !state.equalsIgnoreCase(staticDataService.getStateName(supplierName, "NA"))) {
						if (state.equalsIgnoreCase(supplierInfo.get(BinName.STATE.name()))) {
							hotelIds.add(supplierInfo.get(BinName.HOTELID.name()));
						}
					}
					if (StringUtils.isNotBlank(supplierInfo.get(BinName.COUNTRY.name()))
							&& !country.equals(staticDataService.getCountryName(supplierName, "NA"))) {
						if (country.equalsIgnoreCase(supplierInfo.get(BinName.COUNTRY.name()))) {
							hotelIds.add(supplierInfo.get(BinName.HOTELID.name()));
						}
					}
				}
			}
		} finally {
			log.info("Fetched hotel id from cache for region {} and searchid {}. Size is {}", searchRegion,
					searchQuery.getSearchId(), hotelIds.size());
		}
		return hotelIds;
	}

	public Set<String> fetchPropertyIdsByPolygonInfo(String supplierName, String polygonInfo,
			HotelStaticDataService staticDataService) {

		CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.bins(new String[] {BinName.RATING.name(), BinName.HOTELID.name()})
				.set(staticDataService.getSupplierSetName(supplierName))
				.build();
		log.info("Fetching property ids from cache for search id {}, supplier {} and polygon {}, cacheMetaInfo {}",
				searchQuery.getSearchId(), supplierName, polygonInfo, supplierMappingMetaInfo);
		Set<String> hotelIds = new HashSet<>();
		try {
			Map<String, Map<String, String>> supplierhotelIdMap =
					cachingCommunicator.getResultSet(supplierMappingMetaInfo, String.class,
							Filter.geoWithinRegion(BinName.GEOLOCATION.name(), polygonInfo));
			for (Map.Entry<String, Map<String, String>> supplierHotel : supplierhotelIdMap.entrySet()) {
				Map<String, String> supplierInfo = supplierHotel.getValue();
				if (StringUtils.isNotBlank(supplierInfo.get(BinName.RATING.name()))
						&& searchQuery.getSearchPreferences().getRatings()
								.contains(Integer.parseInt(supplierInfo.get(BinName.RATING.name())))) {
					hotelIds.add(supplierInfo.get(BinName.HOTELID.name()));
				}
			}
			return hotelIds;
		} catch (Exception e) {
			log.info("Unable to fetch property ids from cache for search id {}, supplier {} and polygon {}",
					searchQuery.getSearchId(), supplierName, polygonInfo);
			return null;
		} finally {
			log.info("Fetched property ids from cache for search id {}, supplier {} and polygon {} are {}",
					searchQuery.getSearchId(), supplierName, polygonInfo, hotelIds.size());
		}
	}


	private String getDirectSupplierName() {
		Map<String, String> supplierMapping = sourceConfigOutput.getSupplierMapping();
		if (Objects.nonNull(supplierMapping)) {
			return supplierMapping.get(supplierConf.getBasicInfo().getSupplierName());
		}
		return null;
	}

	private String getCityName() {
		HotelSupplierRegionInfo supplierRegionInfo = HotelSupplierRegionInfo.builder().build();

		if (StringUtils.isNotBlank(searchQuery.getSearchCriteria().getRegionId())) {
			supplierRegionInfo =
					cacheHandler.getRegionInfoMappingFromRegionId(searchQuery.getSearchCriteria().getRegionId(),
							HotelSourceType.getHotelSourceType(supplierConf.getBasicInfo().getSourceId()).name());
		}
		if (Objects.nonNull(supplierRegionInfo)) {
			return HotelUtils.firstNonEmpty(supplierRegionInfo.getRegionName(),
					searchQuery.getSearchCriteria().getCityName());
		}
		return searchQuery.getSearchCriteria().getCityName();
	}

}
