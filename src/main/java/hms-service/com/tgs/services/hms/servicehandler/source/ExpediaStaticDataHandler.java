package com.tgs.services.hms.servicehandler.source;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoSaveManager;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ExpediaStaticDataHandler extends ServiceHandler<HotelStaticDataRequest, BaseResponse> {

	@Autowired
	HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	HotelRegionInfoSaveManager regionInfoSaveManager;

	@Autowired
	HotelRegionInfoMappingManager regionInfoMappingManager;

	@Override
	public void beforeProcess() throws Exception {
		request.setLanguage(HotelUtils.getLanguage());
	}

	@Override
	public void process() throws Exception {
		try {
			log.info("Starting to fetch hotel static data for request {}", GsonUtils.getGson().toJson(request));
			String supplierName = StringUtils.isNotBlank(request.getSupplierId()) ? request.getSupplierId() :  HotelSourceType.EXPEDIA.name();
			AbstractStaticDataInfoFactory staticDataInfoFactory =
					HotelSourceType.getStaticDataFactoryInstance(supplierName, request);
			staticDataInfoFactory.getStaticHotelInfo();
			log.info("Finished fetching hotel static data for request {}", GsonUtils.getGson().toJson(request));
		} catch (Exception e) {
			log.error("Unable to handle static info", e);
		}
	}

	public void syncCities(HotelStaticDataRequest staticDataRequest) {
		try {
			staticDataRequest.setLanguage(HotelUtils.getLanguage());
			String supplierName = StringUtils.isNotBlank(staticDataRequest.getSupplierId()) ? staticDataRequest.getSupplierId() :  HotelSourceType.EXPEDIA.name();
			AbstractStaticDataInfoFactory staticDataInfoFactory = HotelSourceType
					.getStaticDataFactoryInstance(supplierName, staticDataRequest);
			staticDataInfoFactory.getCityMappingInfo();
		} catch (Exception e) {
			log.error("Unable to handle city info", e);
		}
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}
}
