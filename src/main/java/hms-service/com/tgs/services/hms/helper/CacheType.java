package com.tgs.services.hms.helper;

import lombok.Getter;

@Getter
public enum CacheType {

	HOTELSEARCHCACHING(2 * 60 * 60),
	HOTELRATINGCACHING(48 * 60 * 60),
	BULKUPLOADCACHING(24 * 60 * 60),
	SEARCHQUERYBINCACHING(2 * 60 * 60);

	private int ttl;

	private CacheType(int ttl) {
		this.ttl = ttl;
	}

}
