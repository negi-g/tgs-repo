package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import com.tgs.services.base.runtime.database.CustomTypes.StringArrayUserType;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelAdditionalInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.Option;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@TypeDefs({ @TypeDef(name = "GeoLocationType", typeClass = GeoLocationType.class),
		@TypeDef(name = "AddressType", typeClass = AddressType.class),
		@TypeDef(name = "StringArrayUserType", typeClass = StringArrayUserType.class),
		@TypeDef(name = "ImageType", typeClass = ImageType.class),
		@TypeDef(name = "OptionType", typeClass = OptionType.class),
		@TypeDef(name = "InstructionType", typeClass = InstructionType.class),
		@TypeDef(name = "AdditionalInfoType", typeClass = AdditionalInfoType.class),
		@TypeDef(name = "ContactType", typeClass = ContactType.class) })
@Table(name = "hotelinfo")
public class DbHotelInfo extends BaseModel<DbHotelInfo, HotelInfo> {

	private String name;

	@SerializedName("des")
	private String description;

	@Type(type = "ImageType")
	@CustomSerializedName(key = FieldName.HOTEL_IMAGES)
	private List<Image> images;

	@Type(type = "OptionType")
	@SerializedName("ops")
	private List<Option> options;

	@SerializedName("rt")
	private String rating;

	@Type(type = "GeoLocationType")
	@SerializedName("gl")
	private GeoLocation geolocation;

	@Type(type = "AddressType")
	@SerializedName("ad")
	private Address address;

	@Type(type = "StringArrayUserType")
	@SerializedName("fl")
	private String[] facility = {};

	@SerializedName("ldes")
	private String longDescription;

	@SerializedName("wb")
	private String website;

	private String type;

	@CreationTimestamp
	@SerializedName("co")
	private LocalDateTime createdOn;

	@SerializedName("po")
	private LocalDateTime processedOn;

	@SerializedName("gId")
	private String giataId;

	@SerializedName("pt")
	private String propertyType;

	@Type(type = "InstructionType")
	@SerializedName("inst")
	private List<Instruction> instructions;

	@Type(type = "ContactType")
	@SerializedName("cnt")
	private Contact contact;

	@SerializedName("taid")
	private String tripadvisorid;

	private String cityName;

	private String countryName;

	@SerializedName("shid")
	private String supplierHotelId;

	@SerializedName("sn")
	private String supplierName;

	@SerializedName("uid")
	private String unicaId;

	@Type(type = "AdditionalInfoType")
	@SerializedName("hai")
	private HotelAdditionalInfo additionalInfo;

	@Override
	public HotelInfo toDomain() {

		return new GsonMapper<>(this, HotelInfo.class).convert();
	}

	@Override
	public DbHotelInfo from(HotelInfo dataModel) {

		dataModel.setUnicaId(Objects.isNull(dataModel.getUnicaId()) ? "" : dataModel.getUnicaId());
		return new GsonMapper<>(dataModel, this, DbHotelInfo.class).convert();
	}

	public HotelInfo toBasicHotelInfo() {

		HotelInfo basicHotelInfo = HotelInfo.builder().id(String.valueOf(getId())).address(getAddress()).name(getName())
				.rating(Objects.nonNull(getRating()) ? Integer.parseInt(rating) : null).giataId(getGiataId())
				.unicaId(getUnicaId()).geolocation(getGeolocation()).additionalInfo(getAdditionalInfo())
				.supplierName(getSupplierName()).supplierHotelId(getSupplierHotelId()).build();
		if (CollectionUtils.isNotEmpty(getImages())) {
			Image firstImage = getImages().get(0);
			basicHotelInfo.setImages(new ArrayList<>(Arrays.asList(
					Image.builder().bigURL(firstImage.getBigURL()).thumbnail(firstImage.getThumbnail()).build())));
		}
		if (Objects.nonNull(basicHotelInfo.getAdditionalInfo())) {
			basicHotelInfo.getAdditionalInfo().setPointOfInterests(null);
		}
		return basicHotelInfo;
	}

	public HotelInfo toDetailHotelInfo() {

		HotelInfo detailHotelInfo = HotelInfo.builder().id(String.valueOf(getId())).description(getDescription())
				.longDescription(getLongDescription()).instructions(getInstructions())
				.facilities(Objects.nonNull(facility) ? Arrays.asList(facility) : null).images(getImages())
				.options(getOptions()).propertyType(getPropertyType()).contact(getContact()).build();
		return detailHotelInfo;
	}
}
