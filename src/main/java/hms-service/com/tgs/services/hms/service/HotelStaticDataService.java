package com.tgs.services.hms.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelWiseSearchInfo;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.qtech.SupplierHotelIdMapping;

public interface HotelStaticDataService {

	public String getSeparator();

	public String getKeyToMergeHotel(HotelInfo hotelInfo);

	public String getHotelInfoUniqueKey(HotelInfo hotelInfo);

	public String getCityName(String supplierName, String cityName);

	public String getStateName(String supplierName, String stateName);

	public String getCountryName(String supplierName, String countryName);

	public String getSupplierSetName(String supplierName);

	public String getSupplierStaticHotelId(HotelInfo hotelInfo);

	public Map<String, HotelInfo> processAndFetchHotelInfo(String supplierName, Set<String> supplierHotelIds,
			HotelSearchQuery searchQuery, OperationType operationType);

	public HotelInfo getHotelInfoFromDB(HotelInfo hotelInfo) throws Exception;

	public SupplierHotelIdMapping getHotelIdMappingFromDB(String hotelid);

	public boolean validateHotelInfo(HotelInfo updatedHotelInfo, String supplierName, String bookingId)
			throws IOException;

	public Map<String, String> getRoomMappingInfo(HotelSearchQuery searchQuery, HotelInfo hotelInfo);

	public List<HotelWiseSearchInfo> getHotelWiseSearchInfo(String hotelId);
}
