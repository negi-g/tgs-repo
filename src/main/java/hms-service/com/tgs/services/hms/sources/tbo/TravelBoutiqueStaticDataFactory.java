package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;

@Service
public class TravelBoutiqueStaticDataFactory extends AbstractStaticDataInfoFactory {

	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	protected HotelCacheHandler hotelCacheHandler;

	@Autowired
	private HotelRegionInfoMappingManager regionInfoMappingManager;

	@Autowired
	private HotelSupplierRegionInfoManager supplierRegionInfoManager;

	public TravelBoutiqueStaticDataFactory(HotelSupplierConfiguration supplierConf,
			HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getHotelStaticData() throws IOException {
		TravelBoutiqueHotelStaticDataService staticDataService = TravelBoutiqueHotelStaticDataService.builder()
				.regionInfoMappingManager(regionInfoMappingManager).hotelInfoSaveManager(hotelInfoSaveManager)
				.supplierConf(supplierConf).sourceConfigOutput(sourceConfigOutput).staticDataRequest(staticDataRequest)
				.build();
		staticDataService.process();
	}

	@Override
	protected void getCityMappingData() throws IOException {
		TravelBoutiqueCitySaveService staticCityService  = TravelBoutiqueCitySaveService.builder()
				.staticDataRequest(staticDataRequest).regionInfoMappingManager(regionInfoMappingManager)
				.supplierRegionInfoManager(supplierRegionInfoManager).supplierConf(supplierConf).build();
		List<HotelRegionInfoQuery> travelBoutiqueRegions = staticCityService.getTravelBoutiqueRegions();
		HotelBaseSupplierUtils.saveOrUpdateRegionInfo(travelBoutiqueRegions, staticDataRequest.getIsMasterData(),
				false);
	}

	@Override
	protected Map<String, String> getRoomMappingData(HotelSearchQuery searchQuery, HotelInfo hotelInfo) {

		return null;
	}

	@Override
	protected void getNationalityMappingData() throws IOException {
		// TODO Auto-generated method stub

	}
}
