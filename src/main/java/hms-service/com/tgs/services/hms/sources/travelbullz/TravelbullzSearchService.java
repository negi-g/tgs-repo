package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.enums.CountryInfoType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomAdditionalInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.travelbullz.AdvancedOptions;
import com.tgs.services.hms.datamodel.travelbullz.Filters;
import com.tgs.services.hms.datamodel.travelbullz.HotelOption;
import com.tgs.services.hms.datamodel.travelbullz.HotelResult;
import com.tgs.services.hms.datamodel.travelbullz.HotelRoom;
import com.tgs.services.hms.datamodel.travelbullz.Request;
import com.tgs.services.hms.datamodel.travelbullz.Room;
import com.tgs.services.hms.datamodel.travelbullz.StarRating;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzRequest;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzSearchResponse;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Service
@SuperBuilder
@Slf4j
public class TravelbullzSearchService extends TravelbullzBaseService {
	private HotelSearchResult searchResult;
	private List<String> hotelIds;
	private TravelbullzSearchResponse searchResponse;
	protected HotelSupplierRegionInfo supplierRegionInfo;

	public void doSearch() throws IOException {

		searchResult = HotelSearchResult.builder().build();
		HttpUtilsV2 httpUtils = null;
		listener = new RestAPIListener("");
		try {
			TravelbullzRequest searchRequest = getSearchRequest();
			String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
			String urlString =
					StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_SEARCH_URL));
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(searchRequest), urlString);
			searchResponse = httpUtils.getResponse(TravelbullzSearchResponse.class).orElse(null);
			if (Objects.nonNull(searchResponse)) {
				if (CollectionUtils.isNotEmpty(searchResponse.getError())) {
					log.info("Empty response for searchid {} of Supplier {} due to error {} ",
							searchQuery.getSearchId(), supplierConf.getBasicInfo().getSupplierId(),
							searchResponse.getError().get(0).getDescription());
				} else if (CollectionUtils.isEmpty(searchResponse.getAvailabilityRS().getHotelResult())) {
					log.info("Empty response from supplier {} for searchid {}",
							supplierConf.getBasicInfo().getSupplierId(), searchQuery.getSearchId());
				} else {
					searchResult.setHotelInfos(getHotelListFromTravelbullzResult(searchResponse, false));
					log.info("Total hotelcount for searchid {} is {} ", searchQuery.getSearchId(),
							searchResult.getHotelInfos().size());
				}
			}
		} finally {
			List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
			checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
					.time(System.currentTimeMillis()).build());
			httpUtils.setCheckPoints(checkPointsMap);
			httpUtils.getCheckPoints().stream()
					.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.SEARCH.name()));
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (Objects.isNull(searchResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString());
			}

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRAVELBULLZ.name())
					.requestType(BaseHotelConstants.SEARCH).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
	}

	private List<HotelInfo> getHotelListFromTravelbullzResult(TravelbullzSearchResponse response, Boolean isDetail) {
		List<HotelInfo> hInfos = new ArrayList<>();

		searchQuery.setSourceId(HotelSourceType.TRAVELBULLZ.getSourceId());
		response.getAvailabilityRS().getHotelResult().forEach(hotel -> {
			List<Option> optionList = getOptionForHotel(hotel.getHotelOption(), hotel,
					response.getAvailabilityRS().getSearchKey(), isDetail);
			HotelInfo hotelInfo = HotelInfo.builder().id(RandomStringUtils.random(20, true, true))
					.miscInfo(HotelMiscInfo.builder().supplierStaticHotelId(String.valueOf(hotel.getHotelId()))
							.searchId(searchQuery.getSearchId()).build())
					.options(optionList).build();
			hInfos.add(hotelInfo);

		});
		return hInfos;
	}

	private List<Option> getOptionForHotel(List<HotelOption> list, HotelResult hotel, String searchKey,
			Boolean isDetail) {
		List<Option> optionList = new ArrayList<>();
		list.forEach(option -> {
			for (int i = 0; i < option.getHotelRooms().size(); i++) {
				List<RoomInfo> roomInfos = getRoomInfo(option.getHotelRooms().get(i));
				Option opt = Option.builder().id(RandomStringUtils.random(20, true, true))
						.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
								.supplierHotelId(String.valueOf(hotel.getHotelId()) + "").searchKey(searchKey)
								.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
								.sourceId(supplierConf.getBasicInfo().getSourceId())
								.hotelOptionId(option.getHotelOptionId()).build())
						.roomInfos(roomInfos).build();
				if (!isDetail) {
					opt.getMiscInfo().setIsNotRequiredOnDetail(true);
				}
				// setOptionCancellationPolicy(opt, option.getHotelRooms().get(i).get(0).getCancellationPolicy());
				optionList.add(opt);
			}
		});
		return optionList;
	}


	public TravelbullzRequest getSearchRequest() {
		String hotelidsList = String.join(", ", hotelIds);
		Filters filters =
				Filters.builder().HotelIds(hotelidsList).IsOnlyAvailable("1").IsRecommendedOnly("0").IsShowRooms("1")
						.StarRating(StarRating.builder()
								.Max(Collections.max(searchQuery.getSearchPreferences().getRatings()))
								.Min(Collections.min(searchQuery.getSearchPreferences().getRatings())).build())
						.build();
		Request request = Request.builder().Rooms(getRoom(searchQuery.getRoomInfo()))
				.CheckInDate(getDate(searchQuery.getCheckinDate())).CheckOutDate(getDate(searchQuery.getCheckoutDate()))
				.Nationality(BaseHotelUtils.getSpecificFieldFromId(searchQuery.getSearchCriteria().getNationality(),
						CountryInfoType.NAME.name()))
				.NoofNights(getNoOfNights(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate()))
				.CityID(supplierRegionInfo.getRegionId()).Filters(filters).build();

		AdvancedOptions advancedOptions =
				AdvancedOptions.builder().Currency(TravelbullzConstant.CURRENCY.value).build();
		TravelbullzRequest searchRequest =
				TravelbullzRequest.builder().Token(supplierConf.getHotelSupplierCredentials().getPassword())
						.Request(request).AdvancedOptions(advancedOptions).build();

		return searchRequest;
	}

	private List<Room> getRoom(List<RoomSearchInfo> roomInfoList) {
		List<Room> roomList = new ArrayList<Room>();
		int count = 1;
		for (RoomSearchInfo roomInfo : roomInfoList) {
			Room room = Room.builder().NoofAdults(roomInfo.getNumberOfAdults()).NoOfChild(roomInfo.getNumberOfChild())
					.ChildAge(ObjectUtils.isEmpty(roomInfo.getChildAge()) ? new ArrayList<>() : roomInfo.getChildAge())
					.RoomNo(count++).build();
			roomList.add(room);
		}
		return roomList;
	}

	private List<RoomInfo> getRoomInfo(List<HotelRoom> list) {
		List<RoomInfo> roominfolist = new ArrayList<>();
		list.forEach(room -> {
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setRoomType(room.getRoomTypeName());
			roomInfo.setId(getRoomId(room));
			roomInfo.setRoomCategory(room.getRoomTypeName());
			roomInfo.setTotalPrice(room.getPrice());
			setMealBasis(roomInfo, room.getMealName());
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
			roomInfo.setRoomAdditionalInfo(RoomAdditionalInfo.builder().roomId(roomInfo.getId()).build());
			roomInfo.setMiscInfo(RoomMiscInfo.builder().roomBlockId(room.getRoomToken()).status(room.getBookingStatus())
					.roomIndex(Integer.parseInt(room.getRoomNo())).inclusive(room.getPrice()).build());
			roomInfo.setPerNightPriceInfos(setPerNightPrice(room.getPrice()));
			roomInfo.setNumberOfAdults(
					searchQuery.getRoomInfo().get(Integer.parseInt(room.getRoomNo()) - 1).getNumberOfAdults());
			roomInfo.setNumberOfChild(
					searchQuery.getRoomInfo().get(Integer.parseInt(room.getRoomNo()) - 1).getNumberOfChild());
			roominfolist.add(roomInfo);


		});
		updatePriceWithMarkup(roominfolist);
		return roominfolist;
	}


	private String getDate(LocalDate date) {
		String formattedDate = date.format(DateTimeFormatter.ofPattern("MM-dd-uuuu"));
		return formattedDate;
	}

	private String getNoOfNights(LocalDate date1, LocalDate date2) {
		Long noofnightsLong = ChronoUnit.DAYS.between(date1, date2);
		return String.valueOf(noofnightsLong);
	}

	public void doDetailSearch() throws IOException {
		searchResult = HotelSearchResult.builder().build();
		listener = new RestAPIListener("");
		HttpUtilsV2 httpUtils = null;
		try {
			TravelbullzRequest detailSearchRequest = getSearchRequest();
			String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
			String urlString =
					StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_SEARCH_URL));
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(detailSearchRequest), urlString);
			searchResponse = httpUtils.getResponse(TravelbullzSearchResponse.class).orElse(null);
			if (Objects.nonNull(searchResponse)) {
				if (CollectionUtils.isNotEmpty(searchResponse.getError())) {
					log.info("Empty response for searchid {} of Supplier {} due to error {} ",
							searchQuery.getSearchId(), supplierConf.getBasicInfo().getSupplierId(),
							searchResponse.getError().get(0).getDescription());
				} else if (CollectionUtils.isEmpty(searchResponse.getAvailabilityRS().getHotelResult())) {
					log.info("Empty response from supplier {} for searchid {}",
							supplierConf.getBasicInfo().getSupplierId(), searchQuery.getSearchId());
				} else {
					searchResult.setHotelInfos(getHotelListFromTravelbullzResult(searchResponse, true));
					log.info("Total hotelcount for searchid {} is {} ", searchQuery.getSearchId(),
							searchResult.getHotelInfos().size());
				}
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.DETAIL.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(searchResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRAVELBULLZ.name())
						.requestType(BaseHotelConstants.DETAILSEARCH).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}

	}
}
