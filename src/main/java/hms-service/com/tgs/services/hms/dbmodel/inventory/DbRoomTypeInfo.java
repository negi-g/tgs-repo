package com.tgs.services.hms.dbmodel.inventory;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "roomType", uniqueConstraints = {@UniqueConstraint(columnNames = {"roomTypeName"})})
public class DbRoomTypeInfo extends BaseModel<DbRoomTypeInfo, HotelRoomTypeInfo> {
	
	private String roomTypeName;
	private Integer maxOccupancy;
	
	@Override
	public HotelRoomTypeInfo toDomain() {
		return new GsonMapper<>(this, HotelRoomTypeInfo.class).convert();
	}

	@Override
	public DbRoomTypeInfo from(HotelRoomTypeInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbRoomTypeInfo.class).convert();
	}

}
