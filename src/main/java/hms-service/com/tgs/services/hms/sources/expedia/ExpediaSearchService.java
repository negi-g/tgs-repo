package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.CrossSellParameter;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionLinkType;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomAdditionalInfo;
import com.tgs.services.hms.datamodel.RoomBenefitType;
import com.tgs.services.hms.datamodel.RoomExtraBenefit;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.expedia.Amenities;
import com.tgs.services.hms.datamodel.expedia.BedGroups;
import com.tgs.services.hms.datamodel.expedia.ExpediaAdditionalSearchParams;
import com.tgs.services.hms.datamodel.expedia.ExpediaCancellationDeadline;
import com.tgs.services.hms.datamodel.expedia.Promotions;
import com.tgs.services.hms.datamodel.expedia.Room;
import com.tgs.services.hms.datamodel.expedia.RoomRates;
import com.tgs.services.hms.datamodel.expedia.ShoppingAPIRequest;
import com.tgs.services.hms.datamodel.expedia.ShoppingAPIRequest.ShoppingAPIRequestBuilder;
import com.tgs.services.hms.datamodel.expedia.ShoppingAPIResponse;
import com.tgs.services.hms.datamodel.expedia.ValueAdds;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
@Builder
public class ExpediaSearchService {

	private HotelSourceConfigOutput sourceConfigOutput;
	private ShoppingAPIResponse shoppingAPIResponse;
	private HotelSearchQuery searchQuery;
	private ExpediaAdditionalSearchParams additionalSearchParams;
	private HotelSearchResult searchResult;
	private HotelSupplierConfiguration supplierConf;
	private CrossSellParameter crossSellParameter;

	protected RestAPIListener listener;

	private static final DateTimeFormatter dateTimeFormatter_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final String SHOPPING_API_SUFFIX = "/properties/availability";
	private static final String PACKAGE_DEAL = "Package Deal";

	@SuppressWarnings({"unchecked"})
	public void doSearch(int threadCount) throws IOException {
		try {
			MultiMap occupancies = ExpediaUtils.getOccupancies(searchQuery);
			ShoppingAPIRequest shoppingApiRequest = createShoppingAPIRequest(true);
			List<ShoppingAPIResponse> shoppingAPIResponse =
					doShopping(shoppingApiRequest, occupancies, "Search", threadCount);
			searchResult =
					createSearchResponse(shoppingAPIResponse, (ArrayList<String>) occupancies.get("occupancy"), false);
			if (!CollectionUtils.isEmpty(searchResult.getHotelInfos()))
				log.info("Total number of hotels found for {} are {}", supplierConf.getBasicInfo().getSupplierName(),
						searchResult.getHotelInfos().size());
			searchQuery.getSearchPreferences().setFetchSpecialCategory(true);
		} finally {
			if (Objects.nonNull(searchResult)) {
				SystemContextHolder.getContextData().addCheckPoint(
						CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
								.subType(HotelFlowType.SEARCH.name()).time(System.currentTimeMillis()).build());
			}
		}
	}

	@SuppressWarnings({"unchecked", "serial"})
	private List<ShoppingAPIResponse> doShopping(ShoppingAPIRequest shoppingApiRequest, MultiMap occupancies,
			String requestType, int threadCount) throws IOException {

		HttpUtilsV2 httpUtils = null;
		String supplierName =
				BooleanUtils.isTrue(additionalSearchParams.getIsPackageRate()) ? "Expedia_Package" : "Expedia";
		List<ShoppingAPIResponse> shoppingAPIResponse = null;
		try {
			listener = new RestAPIListener("");
			httpUtils = ExpediaUtils.getResponseURL(shoppingApiRequest, sourceConfigOutput, supplierConf);
			httpUtils.getRecurringQueryParams().putAll(occupancies);
			if (ObjectUtils.isEmpty(crossSellParameter)) {
				MultiMap propertyIdMap = getPropertyIds(additionalSearchParams.getProperty_id());
				httpUtils.getRecurringQueryParams().putAll(propertyIdMap);
			}
			httpUtils.setPrintResponseLog(false);
			httpUtils.getResponseFromType(new TypeToken<ArrayList<ShoppingAPIResponse>>() {}.getType());
			String responseString = httpUtils.getResponseString();
			try {
				shoppingAPIResponse = GsonUtils.getGson().fromJson(responseString,
						new TypeToken<ArrayList<ShoppingAPIResponse>>() {}.getType());
				return shoppingAPIResponse;
			} catch (Exception e) {
				ShoppingAPIResponse errorInPropertyCatalog =
						GsonUtils.getGson().fromJson(responseString, ShoppingAPIResponse.class);
				log.info("Unable to fetch hotel details due to {} and {}", errorInPropertyCatalog.getType(),
						errorInPropertyCatalog.getMessage(), e);
				return null;
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().stream()
						.forEach(checkpoint -> checkpoint
								.setSubType(StringUtils.equals(requestType, "Detail") ? HotelFlowType.DETAIL.name()
										: HotelFlowType.SEARCH.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(supplierName.toUpperCase())
						.requestType(StringUtils.equals(requestType, "Detail") ? BaseHotelConstants.DETAILSEARCH
								: BaseHotelConstants.SEARCH)
						.headerParams(httpUtils.getHeaderParams()).threadCount(threadCount)
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private MultiMap getPropertyIds(Set<String> propertyIds) {
		MultiMap propertyIdMap = new MultiValueMap();
		for (String propertyId : propertyIds) {
			propertyIdMap.put("property_id", propertyId);
		}
		return propertyIdMap;
	}

	private ShoppingAPIRequest createShoppingAPIRequest(boolean isSearch) {

		String salesChannel = StringUtils.isBlank(sourceConfigOutput.getSalesChannel()) ? getSalesChannel()
				: sourceConfigOutput.getSalesChannel();
		ShoppingAPIRequestBuilder apiRequestBuilder = ShoppingAPIRequest.builder().suffixOfURL(SHOPPING_API_SUFFIX)
				.checkin(searchQuery.getCheckinDate().format(dateTimeFormatter_YYYY_MM_DD))
				.checkout(searchQuery.getCheckoutDate().format(dateTimeFormatter_YYYY_MM_DD))
				.currency(searchQuery.getSearchPreferences().getCurrency())
				.language(additionalSearchParams.getLanguage()).country_code(additionalSearchParams.getCountry_code())
				.sales_channel(salesChannel)
				.sales_environment(
						BooleanUtils.isTrue(additionalSearchParams.getIsPackageRate()) ? "hotel_package" : "hotel_only")
				.sort_type(sourceConfigOutput.getSortType())
				.rate_plan_count(isSearch ? sourceConfigOutput.getSearchRatePlanCount()
						: sourceConfigOutput.getDetailRatePlanCount())
				.partner_point_of_sale(sourceConfigOutput.getPartnerPointOfSale())
				.billing_terms(sourceConfigOutput.getBillingTerms()).payment_terms(sourceConfigOutput.getPaymentTerms())
				.platform_name(sourceConfigOutput.getPlatformName()).filter(sourceConfigOutput.getAdditionalFilter());

		populateCrossSellRequestParameters(apiRequestBuilder, crossSellParameter);
		return apiRequestBuilder.build();
	}

	private String getSalesChannel() {

		ChannelType channelType = SystemContextHolder.getContextData().getHttpHeaders().getChannelType();
		if (ObjectUtils.isEmpty(channelType))
			return "website";

		if (channelType.equals(ChannelType.MOBILE) || channelType.equals(ChannelType.ANDROID)
				|| channelType.equals(ChannelType.IPHONE)) {
			return "mobile_app";
		} else {
			return "website";
		}
	}

	private static void populateCrossSellRequestParameters(ShoppingAPIRequestBuilder apiRequestBuilder,
			CrossSellParameter crossSellParameter) {
		if (ObjectUtils.isEmpty(crossSellParameter))
			return;
		apiRequestBuilder.cabin_class("economy");
		apiRequestBuilder.destination_iata_airport_code(crossSellParameter.getIataCode());
		apiRequestBuilder.rate_option("");
	}

	private HotelSearchResult createSearchResponse(List<ShoppingAPIResponse> shoppingAPIResponses,
			List<String> occupancy, boolean isDetail) {
		HotelSearchResult searchResult = HotelSearchResult.builder().build();
		List<HotelInfo> hotelInfoList = new ArrayList<>();
		Map<String, Integer> occupancyMap = ExpediaUtils.getOccupancyMap(occupancy);
		Set<String> amenities = new HashSet<>();
		for (ShoppingAPIResponse shoppingAPIResponse : shoppingAPIResponses) {
			HotelInfo hotelInfo = HotelInfo.builder()
					.miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
							.supplierStaticHotelId(shoppingAPIResponse.getProperty_id()).isMealAlreadyMapped(true)
							.build())
					.build();
			List<Option> listOfOption =
					getOptionInfo(hotelInfo, shoppingAPIResponse.getRooms(), occupancyMap, amenities, isDetail);
			if (CollectionUtils.isNotEmpty(listOfOption)) {
				hotelInfo.setOptions(listOfOption);
				hotelInfoList.add(hotelInfo);
			}
		}
		ExpediaUtils.setMealBasis(hotelInfoList, amenities, searchQuery);
		searchResult.setHotelInfos(hotelInfoList);
		return searchResult;
	}

	private List<Option> getOptionInfo(HotelInfo hInfo, List<Room> roomList, Map<String, Integer> occupancyMap,
			Set<String> allAmenties, boolean isDetail) {

		List<Option> options = new ArrayList<>();
		for (Room room : roomList) {
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setRoomCategory(room.getRoom_name());
			if (room.getRoom_name() == null) {
				log.info("Unable to parse property catalog {}", GsonUtils.getGson().toJson(room));
				continue;
			}
			roomInfo.setRoomType(room.getRoom_name());
			List<RoomRates> roomRates = room.getRates();
			for (RoomRates roomRate : roomRates) {

				if (isRoomRateApplicable(roomRate, searchQuery)) {
					Option option = Option.builder().build();
					List<String> amenities = new ArrayList<>();
					List<String> amenitiesId = new ArrayList<>();
					if (!ObjectUtils.isEmpty(roomRate.getAmenities())) {
						for (Map.Entry<String, Amenities> roomAmenities : roomRate.getAmenities().entrySet()) {
							amenities.add(roomAmenities.getValue().getName());
							amenitiesId.add(String.valueOf(roomAmenities.getValue().getId()));
						}
					}
					allAmenties.addAll(amenitiesId);
					ExpediaUtils.setRoomPricing(option, roomInfo, roomRate.getOccupancy_pricing(), occupancyMap);
					option.setMiscInfo(
							OptionMiscInfo.builder().secondarySupplier(supplierConf.getBasicInfo().getSupplierId())
									.supplierId(supplierConf.getBasicInfo().getSupplierId())
									.supplierHotelId(hInfo.getMiscInfo().getSupplierStaticHotelId())
									.sourceId(supplierConf.getBasicInfo().getSourceId()).build());

					ExpediaUtils.updatePriceWithClientCommissionComponents(option, sourceConfigOutput);
					if (!isDetail) {
						option.getMiscInfo().setIsNotRequiredOnDetail(true);
					}

					ExpediaCancellationDeadline cancellationDeadline = new ExpediaCancellationDeadline();
					option.setCancellationPolicy(ExpediaUtils.getCancellationPolicy(searchQuery,
							roomRate.getRefundable(), roomRate.getCancel_penalties(), getTotalPrice(option),
							option.getRoomInfos().get(0).getPerNightPriceInfos().size(), cancellationDeadline,
							option.getRoomInfos(), sourceConfigOutput.getCancellationPolicyBuffer()));

					option.setDeadlineDateTime(cancellationDeadline.getDeadlineDateTime());
					option.setCancellationPolicyBuffer(sourceConfigOutput.getCancellationPolicyBuffer());

					for (Map.Entry<String, BedGroups> bedGroups : roomRate.getBed_groups().entrySet()) {
						Option copyOptionInfo = new GsonMapper<>(option, Option.class).convert();
						BedGroups bedGroup = bedGroups.getValue();
						String roomId = ExpediaUtils.getRoomId(room.getId(), roomRate.getId(), bedGroup.getId());
						copyOptionInfo.setId(roomId + (BooleanUtils.isTrue(additionalSearchParams.getIsPackageRate())
								? "--" + bedGroup.getId().hashCode()
								: ""));
						copyOptionInfo.getCancellationPolicy().setId(copyOptionInfo.getId());
						copyOptionInfo.setIsPackageRate(additionalSearchParams.getIsPackageRate());
						AtomicInteger roomCount = new AtomicInteger(1);
						copyOptionInfo.getRoomInfos().stream().forEach(ri -> {
							ri.setRoomAmenities(amenities);
							ri.setId(String.join("--", roomId, String.valueOf(roomCount.getAndAdd(1))));
							ri.setRoomType(String.join("-", ri.getRoomType(), bedGroup.getDescription()));
							ri.setRoomType(BooleanUtils.isTrue(additionalSearchParams.getIsPackageRate())
									? ri.getRoomType() + " - " + PACKAGE_DEAL
									: ri.getRoomType());
							ri.setMiscInfo(RoomMiscInfo.builder().amenities(amenitiesId).build());
							ri.setRoomAdditionalInfo(RoomAdditionalInfo.builder().roomId(room.getId()).build());
							setPromotions(ri, roomRate);
						});
						copyOptionInfo.getMiscInfo().getLinks().put(OptionLinkType.PRICE_CHECK,
								bedGroup.getLinks().getPrice_check().getHref());
						options.add(copyOptionInfo);
					}
				}
			}
		}
		return options;
	}

	private void setPromotions(RoomInfo room, RoomRates roomRate) {
		Promotions promotions = roomRate.getPromotions();
		Map<RoomBenefitType, List<RoomExtraBenefit>> list = new HashMap<RoomBenefitType, List<RoomExtraBenefit>>();
		if (!ObjectUtils.isEmpty(promotions) && !ObjectUtils.isEmpty(promotions.getValue_adds())) {
			List<RoomExtraBenefit> roomExtraBenefits = new ArrayList<RoomExtraBenefit>();
			for (Map.Entry<String, ValueAdds> valueAdds : promotions.getValue_adds().entrySet()) {
				RoomExtraBenefit benefit = new RoomExtraBenefit();
				ValueAdds valueAdd = valueAdds.getValue();
				benefit.setType(valueAdd.getCategory());
				Set<String> values = new HashSet<String>();
				values.add(valueAdd.getDescription());
				benefit.setValues(values);
				roomExtraBenefits.add(benefit);
			}
			list.put(RoomBenefitType.PROMOTION, roomExtraBenefits);

		}

		if (!ObjectUtils.isEmpty(roomRate.getAmenities())) {
			List<RoomExtraBenefit> roomExtraBenefits = new ArrayList<RoomExtraBenefit>();
			RoomExtraBenefit benefit = new RoomExtraBenefit();
			Set<String> values = new HashSet<String>();
			for (Map.Entry<String, Amenities> roomAmenities : roomRate.getAmenities().entrySet()) {
				values.add(roomAmenities.getValue().getName());
			}
			benefit.setValues(values);
			roomExtraBenefits.add(benefit);
			list.put(RoomBenefitType.BENEFIT, roomExtraBenefits);
		}

		if (list.size() != 0) {
			room.setRoomExtraBenefits(list);
		}


	}

	@SuppressWarnings({"unchecked"})
	public void doDetailSearch() throws IOException {
		ShoppingAPIRequest shoppingApiRequest = null;
		try {
			MultiMap occupancies = ExpediaUtils.getOccupancies(searchQuery);
			shoppingApiRequest = createShoppingAPIRequest(false);
			List<ShoppingAPIResponse> shoppingAPIResponse = doShopping(shoppingApiRequest, occupancies, "Detail", 0);

			searchResult =
					createSearchResponse(shoppingAPIResponse, (ArrayList<String>) occupancies.get("occupancy"), true);
			searchQuery.getSearchPreferences().setFetchSpecialCategory(true);
		} finally {
			if (Objects.nonNull(shoppingApiRequest)) {
				SystemContextHolder.getContextData().addCheckPoint(
						CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
								.subType(HotelFlowType.SEARCH.name()).time(System.currentTimeMillis()).build());
			}
		}
	}

	private static void removeRedundantFareComponents(Option option) {

		for (RoomInfo roomInfo : option.getRoomInfos()) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {

				priceInfo.getFareComponents().remove(HotelFareComponent.SRC);
				priceInfo.getFareComponents().remove(HotelFareComponent.SAC);
				priceInfo.getFareComponents().remove(HotelFareComponent.TMF);
				priceInfo.getFareComponents().remove(HotelFareComponent.ST);
				priceInfo.getFareComponents().remove(HotelFareComponent.EPF);
				priceInfo.getFareComponents().remove(HotelFareComponent.PRF);
				priceInfo.getFareComponents().remove(HotelFareComponent.MUP);
				priceInfo.getFareComponents().remove(HotelFareComponent.TSF);
			}
		}
	}

	private boolean isRoomRateApplicable(RoomRates roomRate, HotelSearchQuery searchQuery) {

		if (roomRate.getAvailable_rooms() >= searchQuery.getRoomInfo().size()) {
			return true;
		}
		return false;
	}

	private double getTotalPrice(Option option) {

		return option.getRoomInfos().stream().mapToDouble(ri -> {
			return ri.getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
				return priceInfo.getFareComponents().get(HotelFareComponent.BF);
			}).sum();
		}).sum();
	}
}
