package com.tgs.services.hms.sources.inventory;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.HotelRatePlanManager;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class HotelInventoryBookingCancellationFactory extends AbstractHotelBookingCancellationFactory {

	@Autowired
	HotelRatePlanManager ratePlanManager;
	
	public HotelInventoryBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo,
			Order order) {
		super(supplierConf, hInfo, order);
	}

	@Override
	public boolean cancelHotel() throws IOException, JAXBException {
		HotelInventoryBookingCancellationService cancellationService = HotelInventoryBookingCancellationService.builder()
				.hInfo(hInfo).ratePlanManager(ratePlanManager).build();
		return cancellationService.cancelBooking();
	}

	@Override
	public boolean getCancelHotelStatus() throws IOException {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
