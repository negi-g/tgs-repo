package com.tgs.services.hms.sources.expedia;

public class ExpediaConstants {

	public static final String CUSTOMER_IP = "10.10.18.165";
	public static final Integer TIMEOUT = 60000;
	public static final String RETREIVE_BOOKING_SUFFIX = "/itineraries";
}
