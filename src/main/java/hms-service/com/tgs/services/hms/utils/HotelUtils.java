package com.tgs.services.hms.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.aerospike.client.Value;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.CountryInfoType;
import com.tgs.services.base.enums.HotelSearchType;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelKeyInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.HotelTag;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelPanConfigOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelPassportConfigOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSupplierConfigOutput;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.dbmodel.DbHotelUserReviewIdInfo;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.helper.HotelRoomTypeHelper;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HotelUtils {

	public static final String MASTER_HOTEL_ID_PREPEND = "M";

	private static GeneralServiceCommunicator gmsComm;
	final static private Pattern splitSearchPattern = Pattern.compile("[\",]");

	public static void init(GeneralServiceCommunicator gmsComm) {
		HotelUtils.gmsComm = gmsComm;
	}

	private static <T> Map<String, String> convertPojoToMap(T request) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> responseMap = mapper.convertValue(request, new TypeReference<Map<String, String>>() {});
		return responseMap;
	}

	public static <T> HttpUtils getReviewURL(T request, String baseUrl) {
		Map<String, String> queryParams = HotelUtils.convertPojoToMap(request);
		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("Accept-Encoding", "gzip");
		HttpUtils httpUtils =
				HttpUtils.builder().urlString(baseUrl).headerParams(headerParams).queryParams(queryParams).build();
		return httpUtils;
	}

	public static void populateMissingParametersInHotelSearchQuery(HotelSearchQuery searchQuery) {

		if (searchQuery.getSearchPreferences().getRatings() == null) {
			searchQuery.getSearchPreferences().setRatings(new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5)));
		}
		if (searchQuery.getSearchCriteria().getNationality() == null) {
			searchQuery.getSearchCriteria().setNationality("106");
		}
		if (searchQuery.getSearchPreferences().getCurrency() == null) {
			searchQuery.getSearchPreferences().setCurrency("INR");
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T getUniqueHotelKey(T... combinationOfValues) {
		return (T) StringUtils.join(combinationOfValues, "-");
	}

	/**
	 * Intially id consist of datbaseId , this function will generate new Id based on search Id and set new Id.
	 * Otherwise it will generate hashcode and set hashcode.
	 * 
	 * @param hotel
	 * @param searchQuery
	 * @return
	 */
	public static String generateKey(HotelInfo dynamicHotelInfo, HotelInfo staticHotelInfo,
			HotelSearchQuery searchQuery, HotelStaticDataService staticDataService) {

		String key = searchQuery.getSearchId();
		String uniqueId = null;
		if (Objects.nonNull(staticHotelInfo)) {
			uniqueId = HotelUtils.firstNonEmpty(staticDataService.getKeyToMergeHotel(staticHotelInfo),
					staticHotelInfo.getId());
		} else {
			uniqueId = HotelUtils.firstNonEmpty(staticDataService.getKeyToMergeHotel(dynamicHotelInfo),
					TgsStringUtils.generateRandomNumber(10));
		}
		return String.join("-", key, uniqueId);
	}

	public static String getSearchId(String id) {
		return id.split("-")[0];
	}

	public static String getHotelId(String id) {
		return id.split("-")[1];
	}

	public static String getSourceNameFromSourceId(Integer sourceId) {
		HotelSourceType hotelSourceType = HotelSourceType.getHotelSourceType(sourceId);
		return hotelSourceType.name();
	}

	public static Option filterOptionById(HotelInfo hotel, String id) {
		if (Objects.isNull(hotel)) {
			return null;
		}
		return hotel.getOptions().stream().filter(opts -> {
			return opts.getId().equals(id);
		}).findFirst().orElse(null);
	}

	public static Option filterOptionByPriceAndRoomCategory(HotelInfo hotel, Option oldOption) {

		if (Objects.nonNull(hotel)) {
			for (Option option : hotel.getOptions()) {
				if (Math.abs(option.getTotalPrice() - oldOption.getTotalPrice()) < 1) {
					if (checkIfRoomTypeSame(option, oldOption))
						return option;
				}
			}
		}
		return null;
	}

	public static boolean checkIfRoomTypeSame(Option option, Option oldOption) {

		for (int i = 0; i < option.getRoomInfos().size(); i++) {
			RoomInfo newRoomInfo = option.getRoomInfos().get(i);
			RoomInfo oldRoomInfo = oldOption.getRoomInfos().get(i);
			if (!(newRoomInfo.getRoomCategory().equalsIgnoreCase(oldRoomInfo.getRoomCategory()))
					|| !(newRoomInfo.getMealBasis().equalsIgnoreCase(oldRoomInfo.getMealBasis()))) {
				return false;
			}
		}
		return true;
	}

	public static HotelInfo filterHotel(HotelSearchResult result, HotelInfo oldHInfo) {
		if (result == null || CollectionUtils.isEmpty(result.getHotelInfos())) {
			return null;
		}

		Optional<HotelInfo> hotelInfo = result.getHotelInfos().stream().filter(hInfo -> {
			return hInfo.getGiataId() != null ? hInfo.getGiataId().equals(oldHInfo.getGiataId()) : false;
		}).findFirst();

		hotelInfo = result.getHotelInfos().stream().filter(hInfo -> {
			return StringUtils.isNotBlank(hInfo.getUnicaId()) ? hInfo.getUnicaId().equals(oldHInfo.getUnicaId())
					: false;
		}).findFirst();

		if (!hotelInfo.isPresent()) {
			hotelInfo = result.getHotelInfos().stream().filter(hInfo -> {
				return hInfo.getName().equalsIgnoreCase((oldHInfo.getName()));
			}).findFirst();
		}

		return hotelInfo.orElse(null);
	}

	public static Proxy getProxyFromConfigurator() {

		Proxy proxy = null;
		String proxyAddress = null;
		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
		if (configuratorInfo != null) {
			proxyAddress = configuratorInfo.getProxyAddress();
			if (StringUtils.isNotBlank(proxyAddress)) {
				proxy = new java.net.Proxy(java.net.Proxy.Type.HTTP,
						new InetSocketAddress(proxyAddress.split(":")[0], Integer.valueOf(proxyAddress.split(":")[1])));
			}
		}
		return proxy;
	}

	public static String getHotelTypeCode(boolean isDomestic) {
		return isDomestic ? HotelSearchType.DOMESTIC.getCode() : HotelSearchType.INTERNATIONAL.getCode();
	}

	public static String getSupplierBinName(String sourceName) {
		/**
		 * Due to the limitation of aerospike binName
		 */

		if (sourceName.length() > 12) {
			sourceName = sourceName.substring(0, 9);
		}
		return "S_" + sourceName + "_ID";
	}

	public static String getSupplierSetName(String supplierName) {
		return "S_" + supplierName;
	}

	public static String getSearchTypeKey(HotelSearchQuery hotelSearchQuery) {
		return "DOM";
	}

	public static HotelSourceType getSourceType(HotelSearchQuery searchQuery) {
		return HotelSourceType.getHotelSourceType(searchQuery.getSourceId());
	}

	public static List<Map<?, ?>> readObjectsFromCsv(File file) throws IOException {
		try {
			CsvSchema bootstrap = CsvSchema.emptySchema().withEscapeChar('\\').withHeader();
			CsvMapper csvMapper = new CsvMapper();
			MappingIterator<Map<?, ?>> mappingIterator = csvMapper.reader(Map.class).with(bootstrap).readValues(file);
			List<Map<?, ?>> csvEntries = mappingIterator.readAll();
			return csvEntries;
		} finally {
			log.info("Completed reading csv object file {} with space {}", file.getName(), file.getTotalSpace());
		}
	}

	public static List<Map<?, ?>> getCSVListFromFile(String filePath) throws Exception {
		File inputFile = null;
		try {
			inputFile = new File(filePath);
			log.info("FileObject name is {}, size is {}", inputFile.getName(), inputFile.getTotalSpace());
			return HotelUtils.readObjectsFromCsv(inputFile);
		} finally {
			log.info("Completed reading csv list file {} with space {}", inputFile.getName(),
					inputFile.getTotalSpace());
		}
	}

	public static List<String> splitByCommasNotInQuotes(String s) {
		if (s == null) {
			return Collections.emptyList();
		}
		List<String> list = new ArrayList<>();
		Matcher m = splitSearchPattern.matcher(s);
		int pos = 0;
		boolean quoteMode = false;
		while (m.find()) {
			String sep = m.group();
			if ("\"".equals(sep)) {
				quoteMode = !quoteMode;
			} else if (!quoteMode && ",".equals(sep)) {
				int toPos = m.start();
				list.add(s.substring(pos, toPos));
				pos = m.end();
			}
		}
		if (pos < s.length()) {
			list.add(s.substring(pos));
		}
		return list;
	}

	public static List<HotelSupplierInfo> getHotelSupplierInfo(HotelSearchQuery searchQuery, ContextData contextData) {

		List<HotelSupplierInfo> supplierInfos = new ArrayList<>();
		contextData = contextData == null ? SystemContextHolder.getContextData() : contextData;
		String userId = null;
		if (contextData.getUser() != null) {
			User user = contextData.getUser();
			userId = user.getPartnerId().equals(UserUtils.DEFAULT_PARTNERID) ? user.getUserId() : user.getPartnerId();
		}

		HotelBasicFact basicFact =
				HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery).generateFactFromUserId(userId);

		List<HotelConfiguratorInfo> hotelconfigrules =
				HotelConfiguratorHelper.getHotelConfiguratorRules(HotelConfiguratorRuleType.SUPPLIERCONFIG);

		/*
		 * Set all enabled suppliers of requested source id(s) if no SUPPLIERCONFIG rule type is present.
		 */
		HotelConfiguratorInfo configuratorInfo = null;
		if (!CollectionUtils.isEmpty(hotelconfigrules)) {

			configuratorInfo = HotelConfiguratorHelper.getRule(basicFact, hotelconfigrules);
			HotelSupplierConfigOutput configuratorRuleOutput =
					Objects.nonNull(configuratorInfo) ? (HotelSupplierConfigOutput) configuratorInfo.getOutput() : null;

			/*
			 * If Condition - Set only those enabled suppliers which are configured in the output of the satisfied rule.
			 * Else Condition - Set all enabled suppliers of requested source id(s) if no rule is satisfied.
			 */

			if (Objects.nonNull(configuratorRuleOutput)) {
				supplierInfos =
						CollectionUtils.isNotEmpty(configuratorRuleOutput.getAllowedSuppliers())
								? HotelSupplierConfigurationHelper
										.getSupplierListFromSupplierIds(configuratorRuleOutput.getAllowedSuppliers())
								: new ArrayList<>();
			} else {
				List<HotelSupplierInfo> currSupplierInfos =
						HotelSupplierConfigurationHelper.getSupplierListFromSourceIds(searchQuery.getSourceIds());
				supplierInfos = CollectionUtils.isNotEmpty(currSupplierInfos) ? currSupplierInfos : new ArrayList<>();
			}
		} else {
			List<HotelSupplierInfo> currSupplierInfos =
					HotelSupplierConfigurationHelper.getSupplierListFromSourceIds(searchQuery.getSourceIds());
			supplierInfos = CollectionUtils.isNotEmpty(currSupplierInfos) ? currSupplierInfos : new ArrayList<>();
		}

		if (Objects.isNull(configuratorInfo)) {
			log.info("No rule is applied for search id {} ", searchQuery.getSearchId());
		} else {
			log.info("Rule applied for search id {} is {}", searchQuery.getSearchId(), configuratorInfo.getId());
		}
		return supplierInfos;
	}

	public static String getKey(String name) {
		if (StringUtils.isBlank(name))
			return null;
		return String.valueOf(name.toLowerCase().replaceAll("\\s", "").hashCode());
	}

	public static String getKey(HotelKeyInfo keyInfo) {
		return StringUtils.join(getKey(
				keyInfo.getHotelName() + (Objects.isNull(keyInfo.getHotelRating()) ? "" : keyInfo.getHotelRating())
						+ keyInfo.getCityName() + keyInfo.getCountryName()));
	}

	public static <T> List<Set<T>> partitionSet(Set<T> originalSet, int partitionSize) {
		List<Set<T>> partitionedSetList = new ArrayList<>();
		Iterator<T> iterator = originalSet.iterator();
		while (iterator.hasNext()) {
			Set<T> newSet = new HashSet<>();
			for (int j = 0; j < partitionSize && iterator.hasNext(); j++) {
				T s = iterator.next();
				newSet.add(s);
			}
			partitionedSetList.add(newSet);
		}
		return partitionedSetList;
	}

	public static void updateTotalFareComponents(HotelInfo hotelInfo) {
		hotelInfo.getOptions().forEach(option -> {
			option.getRoomInfos().forEach(roomInfo -> {
				BaseHotelUtils.updateRoomTotalFareComponents(roomInfo);
			});
		});
	}

	public static Set<String> getSupplierIdsFromHotelInfo(HotelInfo hInfo) {

		Set<String> supplierIds = new HashSet<>();
		for (Option option : hInfo.getOptions()) {
			supplierIds.add(option.getMiscInfo().getSupplierId());
		}
		return supplierIds;
	}

	public static HotelSourceConfigOutput getHotelSourceConfigOutput(HotelSearchQuery searchQuery) {

		ContextData contextData = SystemContextHolder.getContextData();
		HotelBasicFact hotelFact =
				HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery).generateFactFromUserId(
						Objects.nonNull(contextData.getUser()) ? contextData.getUser().getUserId() : null);
		List<HotelConfiguratorInfo> hotelconfigrules =
				HotelConfiguratorHelper.getHotelConfiguratorRules(HotelConfiguratorRuleType.SOURCECONFIG);

		if (!CollectionUtils.isEmpty(hotelconfigrules)) {
			return HotelConfiguratorHelper.getRuleOutPut(hotelFact, hotelconfigrules);
		}
		return null;
	}

	public static double getTotalMarkupOfOption(Option option) {
		double totalMarkup = 0.0;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			totalMarkup += roomInfo.getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
				if (MapUtils.isNotEmpty(priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF))) {
					Map<HotelFareComponent, Double> fareComponent =
							priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF);
					return fareComponent.getOrDefault(HotelFareComponent.MU, 0.0);
				}
				return 0.0;
			}).sum();
		}
		return totalMarkup;
	}

	public static boolean compareIfTwoOptionsEqual(List<RoomInfo> oldRoomInfo, List<RoomInfo> updatedRoomInfo) {
		double oldOptionPrice = 0.0;
		double updatedOptionPrice = 0.0;
		for (int i = 0; i < oldRoomInfo.size(); i++) {
			updatedOptionPrice += (updatedRoomInfo.get(i).getPerNightPriceInfos().stream()
					.mapToDouble(priceInfo -> priceInfo.getFareComponents().get(HotelFareComponent.BF)).sum()
					- (updatedRoomInfo.get(i).getPerNightPriceInfos().stream().mapToDouble(
							priceInfo -> priceInfo.getFareComponents().getOrDefault(HotelFareComponent.CMU, 0.0)))
									.sum());
			oldOptionPrice += (oldRoomInfo.get(i).getPerNightPriceInfos().stream()
					.mapToDouble(priceInfo -> priceInfo.getFareComponents().get(HotelFareComponent.BF)).sum()
					- (oldRoomInfo.get(i).getPerNightPriceInfos().stream().mapToDouble(
							priceInfo -> priceInfo.getFareComponents().getOrDefault(HotelFareComponent.CMU, 0.0)))
									.sum());
		}
		if ((Math.abs(oldOptionPrice - updatedOptionPrice)) < 5) {
			return true;
		}
		return false;
	}

	public static boolean allOptionDetailHit(HotelInfo hInfo) {

		boolean allOptionDetailHit = true;
		for (Option option : hInfo.getOptions()) {
			if (!option.getMiscInfo().getIsDetailHit()) {
				allOptionDetailHit = false;
				break;
			}
		}
		return allOptionDetailHit;
	}

	public static String getLanguage() {
		return "en-US";
	}

	// origin of booking
	public static String getCountryCode() {
		return "IN";
	}

	public static BigDecimal getTotalSupplierBookingAmount(HotelInfo hInfo) {

		BigDecimal amount = BigDecimal.ZERO;
		List<RoomInfo> roomInfoList = hInfo.getOptions().get(0).getRoomInfos();
		for (RoomInfo roomInfo : roomInfoList) {
			BigDecimal roomAmount = BigDecimal.valueOf(roomInfo.getTotalFareComponents().get(HotelFareComponent.BF));
			amount = amount.add(roomAmount);
		}
		amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		return amount;
	}


	public static String getKeyForUserReview(HotelInfo hInfo) {

		GeoLocation geoLocation = hInfo.getGeolocation();
		if (geoLocation != null) {
			String key = String.join("-", hInfo.getName(), geoLocation.getLatitude(), geoLocation.getLongitude());
			return key;
		} else
			return hInfo.getName();

	}

	public static void setUniqueOptionId(List<HotelInfo> hotelInfoList) {
		hotelInfoList.forEach(hotelInfo -> {
			if (CollectionUtils.isNotEmpty(hotelInfo.getOptions())) {
				String supplierId = hotelInfo.getOptions().get(0).getMiscInfo().getSupplierId();
				HotelSourceType.setOptionId(supplierId, hotelInfo);
			}
		});
	}

	public static DbHotelUserReviewIdInfo getDbHotelUserReviewInfo(HotelInfo hInfo) {

		DbHotelUserReviewIdInfo userReviewIdInfo = new DbHotelUserReviewIdInfo();
		String key = HotelUtils.getKeyForUserReview(hInfo);

		String hotelId = HotelUtils.getHotelId(hInfo.getId());
		String masterId =
				StringUtils.isNotBlank(hotelId) && hotelId.substring(0, 1).equals(HotelUtils.MASTER_HOTEL_ID_PREPEND)
						? hotelId.substring(1)
						: null;
		String city = StringUtils.isBlank(hInfo.getAddress().getCity().getName()) ? hInfo.getAddress().getCityName()
				: hInfo.getAddress().getCity().getName();
		userReviewIdInfo.setKey(key);
		userReviewIdInfo.setReviewId(hInfo.getUserReviewSupplierId());
		userReviewIdInfo.setSupplierId("TRIP_ADVISOR");
		userReviewIdInfo.setHotelId(masterId);
		userReviewIdInfo.setRating(hInfo.getRating());
		userReviewIdInfo.setCity(city);
		return userReviewIdInfo;
	}

	public static void filterCrossSellHotels(HotelSearchQuery searchQuery, HotelSearchResult searchResult) {

		if (Objects.isNull(searchQuery) || Objects.isNull(searchQuery.getSearchPreferences())
				|| Objects.isNull(searchQuery.getSearchPreferences().getCrossSellParameter()) || BooleanUtils
						.isNotTrue(searchQuery.getSearchPreferences().getCrossSellParameter().getCrossSellOnly())) {
			return;
		}
		List<HotelInfo> hotelInfos = searchResult.getHotelInfos().stream()
				.filter(hotel -> BooleanUtils.isTrue(hotel.getTags().contains(HotelTag.CROSS_SELL)))
				.collect(Collectors.toList());
		searchResult.setHotelInfos(hotelInfos);
	}

	public static void filterHotelsBasedOnLimit(HotelSearchQuery searchQuery, HotelSearchResult searchResult) {
		if (Objects.isNull(searchQuery) || Objects.isNull(searchQuery.getSearchPreferences())
				|| Objects.isNull(searchQuery.getSearchPreferences().getLimitHotels())) {
			return;
		}
		int limit = searchQuery.getSearchPreferences().getLimitHotels();
		if (Objects.nonNull(limit)) {
			int searchResultSize = searchResult.getHotelInfos().size();
			if (limit > searchResultSize) {
				limit = searchResultSize;
			}
			searchResult.setHotelInfos(searchResult.getHotelInfos().subList(0, limit));
		}
	}

	public static HotelSearchQuery getHotelSearchQuery(String bookingId) {
		ProductMetaInfo metaInfo = Product.getProductMetaInfoFromId(bookingId);
		if (metaInfo.getProduct() == Product.AIR) {
			AirOrderItemCommunicator airOrderCommunicator =
					SpringContext.getApplicationContext().getBean(AirOrderItemCommunicator.class);
			HotelSearchQuery searchQuery = airOrderCommunicator.getHotelSearchQuery(bookingId);
			return searchQuery;
		}
		return null;
	}

	public static void populateOldSearchQueryWithNewSearchQueryParam(HotelSearchQuery oldSearchQuery,
			HotelSearchQuery newSearchQuery) {
		Integer hotelLimits = null;
		Boolean crossSellOnly = null;
		if (Objects.nonNull(newSearchQuery) && Objects.nonNull(newSearchQuery.getSearchPreferences())) {
			hotelLimits = newSearchQuery.getSearchPreferences().getLimitHotels();
		}

		if (Objects.nonNull(newSearchQuery) && Objects.nonNull(newSearchQuery.getSearchPreferences())
				&& Objects.nonNull(newSearchQuery.getSearchPreferences().getCrossSellParameter())) {
			crossSellOnly = newSearchQuery.getSearchPreferences().getCrossSellParameter().getCrossSellOnly();
		}
		oldSearchQuery.getSearchPreferences().setLimitHotels(hotelLimits);
		oldSearchQuery.getSearchPreferences().getCrossSellParameter().setCrossSellOnly(crossSellOnly);
	}

	public static String removeCrossSellFromSearchId(String id) {
		return id.lastIndexOf('_') == -1 ? id : id.substring(0, id.lastIndexOf('_'));
	}

	public static boolean isCrossSell(String id) {

		return StringUtils.isBlank(id) ? false
				: id.lastIndexOf("_") == -1 ? false : id.substring(id.lastIndexOf("_") + 1).equals("CS");
	}

	public static String getCityNameFromAddress(Address adr) {

		if (!Objects.isNull(adr) && !Objects.isNull(adr.getCity()))
			return adr.getCity().getName();
		return null;

	}


	public static String getCountryNameFromAddress(Address adr) {

		if (!Objects.isNull(adr) && !Objects.isNull(adr.getCountry()))
			return adr.getCountry().getName();
		return null;

	}

	public static Map<Integer, HotelRoomTypeInfo> getRoomOccupancyRoomTypeInfoMap() {

		Map<Long, HotelRoomTypeInfo> roomTypeIdValueMap = HotelRoomTypeHelper.getRoomTypeIdRoomTypeInfoMap();
		List<HotelRoomTypeInfo> roomTypeList =
				roomTypeIdValueMap.entrySet().stream().map(entry -> entry.getValue()).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(roomTypeList))
			return new HashMap<>();
		Map<Integer, HotelRoomTypeInfo> roomOccupancyRoomTypeMap = roomTypeList.stream()
				.collect(Collectors.toMap(HotelRoomTypeInfo::getMaxOccupancy, Function.identity()));
		return roomOccupancyRoomTypeMap;
	}

	public static Map<String, List<HotelRatePlan>> getSupplierHotelIdRatePlanListValue(
			List<HotelRatePlan> ratePlanList) {
		if (CollectionUtils.isEmpty(ratePlanList))
			return new HashMap<>();
		return ratePlanList.stream().collect(Collectors.groupingBy(ratePlan -> getRatePlanCachingKey(ratePlan)));
	}

	public static String getRatePlanCachingKey(HotelRatePlan ratePlan) {
		return ratePlan.getSupplierHotelId() + String.valueOf(ratePlan.getValidOn()) + ratePlan.getSupplierId();
	}

	public static String toCamelCase(String s) {
		String[] parts = s.split(" ");
		String camelCaseString = "";
		for (String part : parts) {
			camelCaseString = camelCaseString + toProperCase(part) + " ";
		}
		return camelCaseString.trim();
	}

	private static String toProperCase(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}

	private static HotelKeyInfo getKeyMetaInfo(HotelInfo hInfo) {

		Address address = hInfo.getAddress();
		return HotelKeyInfo.builder().hotelName(hInfo.getName()).hotelRating(hInfo.getRating())
				.cityName(address.getCity().getName()).countryName(address.getCountry().getName()).build();
	}

	public static Map<String, Object> getHotelBasisInfo(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		Map<String, Object> basicInfoToLog = new HashMap<>();
		basicInfoToLog.put("Number of hotels", searchResult.getHotelInfos().size());

		int numberOfOptions = getNumberOfOptionsInSearchResult(searchResult);
		basicInfoToLog.put("Number of options", numberOfOptions);

		int noOfRooms = searchQuery.getRoomInfo().size();
		basicInfoToLog.put("Rooms Searched", noOfRooms);

		long noOfNights = Duration
				.between(searchQuery.getCheckinDate().atStartOfDay(), searchQuery.getCheckoutDate().atStartOfDay())
				.toDays();
		basicInfoToLog.put("Number of nights", noOfNights);

		long totalRoomNights = numberOfOptions * noOfRooms * noOfNights;
		basicInfoToLog.put("Total Room Nights", totalRoomNights);
		return basicInfoToLog;
	}

	public static int getNumberOfOptionsInSearchResult(HotelSearchResult searchResult) {

		if (CollectionUtils.isNotEmpty(searchResult.getHotelInfos())) {
			return searchResult.getHotelInfos().stream().mapToInt(hotelInfo -> {
				return hotelInfo.getOptions().size();
			}).sum();
		}
		return 0;
	}

	public static List<String> getHotelSupplierIds(HotelSearchQuery searchQuery) {

		List<String> supplierIds = new ArrayList<>();
		if (Objects.nonNull(searchQuery.getMiscInfo())) {
			supplierIds = CollectionUtils.isEmpty(searchQuery.getMiscInfo().getSupplierIds())
					? Arrays.asList(searchQuery.getMiscInfo().getSupplierId())
					: searchQuery.getMiscInfo().getSupplierIds();
		}
		return supplierIds;
	}

	public static void fetchBinAndRegionFromKey(String bin, String searchRegion, String regionKey) {

		String[] region = regionKey.split("@");
		String city = region[0];
		String state = region[1];
		String country = region[2];
		if (!city.equals("NA")) {
			bin = BinName.CITY.name();
			searchRegion = city;
		} else if (!state.equals("NA")) {
			bin = BinName.STATE.name();
			searchRegion = state;
		} else if (!country.equals("NA")) {
			bin = BinName.COUNTRY.name();
			searchRegion = country;
		}
	}

	public static String firstNonEmpty(String... values) {
		if (values != null) {
			for (String val : values) {
				if (!StringUtils.isBlank(val)) {
					return val;
				}
			}
		}
		return null;
	}

	public static void setBufferTimeinCnp(List<Option> optionList, HotelSearchQuery searchQuery) {

		HotelSourceConfigOutput sourceConfigOutput = HotelUtils.getHotelSourceConfigOutput(searchQuery);
		Integer cancellationBuffer =
				sourceConfigOutput == null || Objects.isNull(sourceConfigOutput.getCancellationPolicyBuffer()) ? 24
						: sourceConfigOutput.getCancellationPolicyBuffer();
		optionList.forEach(option -> {
			if (Objects.isNull(option.getCancellationPolicy())
					|| !isRoomBasedCnp(option) && Objects.isNull(option.getCancellationPolicy().getPenalyDetails())) {
				return;
			}

			if (isRoomBasedCnp(option)) {
				option.getRoomInfos().forEach(roomInfo -> {
					double totalRoomPrice = getTotalRoomPrice(roomInfo);
					if (CollectionUtils.isEmpty(roomInfo.getCancellationPolicy().getPenalyDetails()))
						return;
					roomInfo.setCancellationPolicy(getCnpWithBuffer(searchQuery, roomInfo.getCancellationPolicy(),
							cancellationBuffer, totalRoomPrice));
					roomInfo.getCancellationPolicy().getMiscInfo().setIsCancellationPolicyBelongToRoom(true);
					roomInfo.setDeadlineDateTime(getDeadlineDateTimeFromPd(roomInfo.getCancellationPolicy()));
					// roomInfo.setCancellationRestrictedDateTime(
					// getCancellationRestrictedDateTime(roomInfo, cancellationBuffer));
				});
				setCancellationDeadlineFromRooms(option);
			} else {
				double totalOptionPrice = getTotalOptionPrice(option);
				option.setCancellationPolicy(getCnpWithBuffer(searchQuery, option.getCancellationPolicy(),
						cancellationBuffer, totalOptionPrice));
				option.setDeadlineDateTime(getDeadlineDateTimeFromPd(option.getCancellationPolicy()));
				option.setCancellationRestrictedDateTime(getCancellationRestrictedDateTime(option, cancellationBuffer));
			}
			option.setCancellationPolicyBuffer(cancellationBuffer);
		});
	}

	private static LocalDateTime getCancellationRestrictedDateTime(Option option, int cancellationBuffer) {

		LocalDateTime now = LocalDateTime.now();
		LocalDateTime cancellationRestrictedDateTime = option.getCancellationRestrictedDateTime() != null
				? option.getCancellationRestrictedDateTime().minus(cancellationBuffer, ChronoUnit.HOURS)
				: null;
		if (cancellationRestrictedDateTime != null && cancellationRestrictedDateTime.isBefore(now))
			return now;
		return cancellationRestrictedDateTime;
	}

	public static LocalDateTime getDeadlineDateTimeFromPd(HotelCancellationPolicy cancellationPolicy) {
		LocalDateTime deadlineDateTime = LocalDateTime.now();
		for (PenaltyDetails pd : cancellationPolicy.getPenalyDetails()) {
			if (pd.getPenaltyAmount() != null && pd.getPenaltyAmount() == 0)
				deadlineDateTime = pd.getToDate();
		}
		return deadlineDateTime;
	}

	public static void setCancellationDeadlineFromRooms(Option option) {

		LocalDateTime earliestDeadlineDateTime = null;
		LocalDateTime earliestCancellationRestrictedDateTime = null;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			if (earliestDeadlineDateTime == null)
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
			else if (earliestDeadlineDateTime.isAfter(roomInfo.getDeadlineDateTime()))
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
			if (BooleanUtils.isTrue(option.getRoomInfos().get(0).getCancellationPolicy().getPenalyDetails().get(0)
					.getIsCancellationRestricted())) {
				if (earliestCancellationRestrictedDateTime == null)
					earliestCancellationRestrictedDateTime = roomInfo.getCancellationRestrictedDateTime();
				else if (earliestCancellationRestrictedDateTime.isAfter(roomInfo.getCancellationRestrictedDateTime()))
					earliestCancellationRestrictedDateTime = roomInfo.getCancellationRestrictedDateTime();
			}
		}
		option.setDeadlineDateTime(earliestDeadlineDateTime);
		for (RoomInfo room : option.getRoomInfos()) {
			room.setDeadlineDateTime(earliestDeadlineDateTime);
		}
		if (earliestCancellationRestrictedDateTime != null) {
			option.setCancellationRestrictedDateTime(earliestCancellationRestrictedDateTime);
		}
	}

	public static HotelCancellationPolicy getCnpWithBuffer(HotelSearchQuery searchQuery,
			HotelCancellationPolicy cancellationPolicy, Integer cancellationBuffer, double totalPrice) {
		LocalDateTime now = LocalDateTime.now();
		boolean isFullRefundAllowed = false;
		List<PenaltyDetails> pds = cancellationPolicy.getPenalyDetails();
		if (Objects.nonNull(pds) && CollectionUtils.isNotEmpty(pds)) {
			pds = pds.stream().sorted(Comparator.comparingDouble(PenaltyDetails::getPenaltyAmount))
					.collect(Collectors.toList());
			for (PenaltyDetails pd : pds) {
				LocalDateTime fDate = pd.getFromDate().minus(cancellationBuffer, ChronoUnit.HOURS);
				LocalDateTime tDate = pd.getToDate().minus(cancellationBuffer, ChronoUnit.HOURS);
				pd.setFromDate(fDate);
				pd.setToDate(tDate);
			}
			Iterator<PenaltyDetails> itr = pds.iterator();
			PenaltyDetails pd = null;
			while (itr.hasNext()) {
				pd = (PenaltyDetails) itr.next();
				LocalDateTime tDate = pd.getToDate();
				if (tDate.isBefore(now)) {
					itr.remove();
				}
			}
			if (CollectionUtils.isEmpty(pds)) {
				PenaltyDetails penaltyDetail = PenaltyDetails.builder().fromDate(now)
						.toDate(searchQuery.getCheckinDate().atStartOfDay()).penaltyAmount(totalPrice).build();
				pds.add(penaltyDetail);
			} else {
				pds.get(0).setFromDate(now);
				pds.get(pds.size() - 1).setToDate(searchQuery.getCheckinDate().atStartOfDay());
			}

			isFullRefundAllowed =
					pds.stream().anyMatch(p -> p.getPenaltyAmount() != null && p.getPenaltyAmount() < 0.1);

			HotelCancellationPolicy hotelCancellationPolicy = HotelCancellationPolicy.builder()
					.id(cancellationPolicy.getId()).isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds)
					.isNoRefundAllowed(cancellationPolicy.getIsNoRefundAllowed())
					.cancellationPolicy(cancellationPolicy.getCancellationPolicy())
					.miscInfo(CancellationMiscInfo.builder()
							.isBookingAllowed(cancellationPolicy.getMiscInfo().getIsBookingAllowed())
							.isSoldOut(cancellationPolicy.getMiscInfo().getIsSoldOut()).build())
					.build();
			return hotelCancellationPolicy;
		}
		return null;
	}

	private static double getTotalOptionPrice(Option option) {

		return option.getRoomInfos().stream().mapToDouble(ri -> {
			return ri.getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
				return priceInfo.getFareComponents().get(HotelFareComponent.BF);
			}).sum();
		}).sum();
	}

	private static double getTotalRoomPrice(RoomInfo room) {
		return room.getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
			return priceInfo.getFareComponents().get(HotelFareComponent.BF);
		}).sum();

	}

	public static boolean isRoomBasedCnp(Option option) {
		return option.getCancellationPolicy().getRoomCancellationPolicyList() != null
				&& option.getCancellationPolicy().getRoomCancellationPolicyList().size() > 0;
	}

	public static Set<String> getStaticDataKeys(String supplierName, Set<String> supplierHotelIds) {

		Set<String> staticDataKeys = new HashSet<>();
		for (String supplierHotelId : supplierHotelIds) {
			staticDataKeys.add(String.join("_", supplierName, supplierHotelId));
		}
		return staticDataKeys;
	}

	public static String getStaticDataKey(String supplierName, String supplierHotelId) {

		return String.join("_", supplierName, supplierHotelId);
	}

	public static String getProbeLogsIdentifier(String supplierName, String logType) {

		return String.join("_", supplierName, logType);
	}

	public static Set<String> removePrefix(Set<String> set, String prefix) {

		Set<String> cleanSet = new HashSet<>();
		for (String ele : set) {
			cleanSet.add(StringUtils.removeStart(ele, prefix));
		}
		return cleanSet;
	}

	public static String[] getUncommonWordsOfFirstParam(String firstName, String secondName) {
		List<String> ansList = new LinkedList<>();
		Set<String> firstNameList = new LinkedHashSet<>(Arrays.asList(firstName.split(" ")));
		List<String> secondNameList = Arrays.asList(secondName.split(" "));
		for (String word : firstNameList) {
			if (!secondNameList.contains(word)) {
				ansList.add(word);
			}
		}
		return ansList.toArray(new String[ansList.size()]);
	}

	public static String removeUnwantedWordsAndChars(String name, List<String> excludedWords,
			List<String> excludedChars) {

		name = StringUtils.stripAccents(name);
		name = StringEscapeUtils.unescapeHtml(name);

		if (CollectionUtils.isNotEmpty(excludedChars)) {
			for (String chars : excludedChars) {
				name = name.replaceAll(chars, "");
			}
		}
		List<String> nameList = new ArrayList<>(Arrays.asList(name.split(" ")));
		if (CollectionUtils.isNotEmpty(excludedWords)) {
			nameList.removeAll(excludedWords);
		}
		return StringUtils.join(nameList, " ").trim().replaceAll(" +", " ");
	}

	public static HotelStaticDataService getStaticDataService() {

		ClientGeneralInfo clientInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		if (StringUtils.isNotBlank(clientInfo.getHotelStaticDataBean())) {
			HotelStaticDataService staticDataService = (HotelStaticDataService) SpringContext.getApplicationContext()
					.getBean(clientInfo.getHotelStaticDataBean());
			return staticDataService;
		}
		return (HotelStaticDataService) SpringContext.getApplicationContext().getBean("defaultStaticDataService");
	}

	public static List<HotelSupplierRegionInfo> getSupplierRegionInfo(List<HotelRegionInfoQuery> regionInfoQueries,
			String supplierName) {

		List<HotelSupplierRegionInfo> supplierRegionInfos = new ArrayList<>();
		regionInfoQueries.forEach(regionInfoQuery -> {

			HotelSupplierRegionInfo supplierRegionInfo = HotelSupplierRegionInfo.builder()
					.regionId(regionInfoQuery.getRegionId()).regionName(regionInfoQuery.getRegionName())
					.stateId(regionInfoQuery.getStateId()).stateName(regionInfoQuery.getStateName())
					.countryId(regionInfoQuery.getCountryId()).countryName(regionInfoQuery.getCountryName())
					.supplierName(supplierName).regionType(regionInfoQuery.getRegionType())
					.additionalInfo(regionInfoQuery.getAdditionalInfo())
					.fullRegionName(ObjectUtils.firstNonNull(regionInfoQuery.getFullRegionName(), "")).build();
			supplierRegionInfos.add(supplierRegionInfo);
		});
		return supplierRegionInfos;
	}

	public static void updateRoomIds(List<RoomInfo> roomInfos) {
		Set<String> set = roomInfos.stream().map(room -> room.getId()).collect(Collectors.toSet());
		if (set.size() != roomInfos.size()) {
			for (int i = 0; i < roomInfos.size(); i++) {
				roomInfos.get(i).setId(roomInfos.get(i).getId() + "_" + i);
			}
		}
	}

	public static void setOptionCancellationPolicyFromRooms(List<Option> options) {

		options.forEach(option -> {
			if (option.getCancellationPolicy() != null
					&& CollectionUtils.isNotEmpty(option.getCancellationPolicy().getRoomCancellationPolicyList())) {
				List<RoomInfo> roomInfoList = option.getRoomInfos();
				LocalDateTime earliestDeadlineDateTime = null;
				LocalDateTime earliestCancellationRestrictedDateTime = null;
				boolean isFullRefundAllowed = true;
				double totalOptionPrice = getTotalOptionPrice(option);
				List<HotelCancellationPolicy> roomCnpList =
						option.getCancellationPolicy().getRoomCancellationPolicyList();

				for (RoomInfo roomInfo : roomInfoList) {

					if (earliestDeadlineDateTime == null) {
						earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
					} else if (Objects.nonNull(roomInfo.getDeadlineDateTime())
							&& earliestDeadlineDateTime.isAfter(roomInfo.getDeadlineDateTime())) {
						earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
					}
					if (earliestCancellationRestrictedDateTime == null) {
						earliestCancellationRestrictedDateTime = roomInfo.getCancellationRestrictedDateTime();
					} else if (Objects.nonNull(roomInfo.getCancellationRestrictedDateTime())
							&& earliestCancellationRestrictedDateTime
									.isAfter(roomInfo.getCancellationRestrictedDateTime())) {
						earliestCancellationRestrictedDateTime = roomInfo.getCancellationRestrictedDateTime();
					}


					if (!BooleanUtils.isTrue(roomInfo.getCancellationPolicy().getIsFullRefundAllowed()))
						isFullRefundAllowed = false;
					roomInfo.setCancellationPolicy(null);
					roomInfo.setDeadlineDateTime(null);
					roomInfo.setCancellationRestrictedDateTime(null);

				}

				Set<LocalDateTime> date = new HashSet<>();
				LocalDateTime now = LocalDateTime.now();
				for (HotelCancellationPolicy rcnp : roomCnpList) {
					rcnp.getPenalyDetails().get(0).setFromDate(now);
					for (PenaltyDetails pd : rcnp.getPenalyDetails()) {
						date.add(pd.getFromDate());
						date.add(pd.getToDate());
					}
				}
				List<LocalDateTime> dateList = new ArrayList<LocalDateTime>(date);
				Collections.sort(dateList);

				List<PenaltyDetails> pds = new ArrayList<>();
				if (dateList.size() > 1) {
					for (int i = 1; i < dateList.size(); i++) {
						PenaltyDetails pd =
								PenaltyDetails.builder().fromDate(dateList.get(i - 1)).toDate(dateList.get(i)).build();
						double penalty = getPenaltyAmountFromRoomCancellationPolicy(option.getCancellationPolicy(),
								pd.getToDate());
						pd.setPenaltyAmount(penalty < 0 ? totalOptionPrice : penalty);
						pds.add(pd);
					}
					mergeCommonAmountPds(pds);
					option.setDeadlineDateTime(earliestDeadlineDateTime);
					option.setCancellationRestrictedDateTime(earliestCancellationRestrictedDateTime);

					if (earliestCancellationRestrictedDateTime != null)
						for (PenaltyDetails penalty : pds) {
							if (penalty.getFromDate().equals(earliestCancellationRestrictedDateTime)
									|| penalty.getFromDate().isAfter(earliestCancellationRestrictedDateTime)) {
								penalty.setIsCancellationRestricted(true);
							}
						}
					HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder().id(option.getId())
							.isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds)
							.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build())
							.build();
					option.setCancellationPolicy(cancellationPolicy);
				}
			}
		});
	}

	// for cases where while merging cnp two penalty detail is having same penalty amounts
	public static void mergeCommonAmountPds(List<PenaltyDetails> pds) {
		Map<Double, List<PenaltyDetails>> pdsWithCommonPrice =
				pds.stream().collect(Collectors.groupingBy(PenaltyDetails::getPenaltyAmount, Collectors.toList()));
		if (pds.size() == pdsWithCommonPrice.size())
			return;
		pds.clear();
		for (Entry<Double, List<PenaltyDetails>> entry : pdsWithCommonPrice.entrySet()) {
			if (entry.getValue().size() > 1) {
				mergePenaltyDetails(entry.getValue(), entry);
			}
			pds.addAll(entry.getValue());
		}

		// Sorting the Penalty details based on FromDate
		pds.sort((firstPenalty, secondPenalty) -> firstPenalty.getFromDate().compareTo(secondPenalty.getFromDate()));

	}

	private static void mergePenaltyDetails(List<PenaltyDetails> pds, Entry<Double, List<PenaltyDetails>> entry) {
		LocalDateTime fromDate = pds.get(0).getFromDate();
		LocalDateTime toDate = pds.get(0).getToDate();
		for (int pd = 1; pd < pds.size(); pd++) {
			if (pds.get(pd).getFromDate().isBefore(fromDate))
				fromDate = pds.get(pd).getFromDate();
			if (pds.get(pd).getToDate().isAfter(toDate))
				toDate = pds.get(pd).getToDate();

		}
		PenaltyDetails penaltyDetail =
				PenaltyDetails.builder().fromDate(fromDate).toDate(toDate).penaltyAmount(entry.getKey()).build();
		pds.clear();
		entry.setValue(Arrays.asList(penaltyDetail));
	}

	private static double getPenaltyChargeFromPenaltyDetail(PenaltyDetails pd, LocalDateTime date) {

		LocalDateTime currentTime = date;
		if (pd.getFromDate().isBefore(currentTime)
				&& (pd.getToDate().isAfter(currentTime) || pd.getToDate().equals(currentTime))) {
			return pd.getPenaltyAmount() != null ? pd.getPenaltyAmount() : -1;
		}
		return 0;
	}

	private static double getPenaltyAmountFromRoomCancellationPolicy(HotelCancellationPolicy cp, LocalDateTime date) {

		double penaltyAmount = 0;
		List<HotelCancellationPolicy> cpList = cp.getRoomCancellationPolicyList();
		for (HotelCancellationPolicy roomCancellationPolicy : cpList) {
			for (PenaltyDetails pd : roomCancellationPolicy.getPenalyDetails()) {
				double pdCharge = getPenaltyChargeFromPenaltyDetail(pd, date);
				if (pdCharge < 0)
					return -1;
				penaltyAmount += pdCharge;
			}
		}

		return penaltyAmount;
	}

	public static double getTotalPrice(Option option) {

		return option.getRoomInfos().stream().mapToDouble(ri -> {
			return ri.getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
				return priceInfo.getFareComponents().get(HotelFareComponent.BF);
			}).sum();
		}).sum();
	}

	public static Value getGeoJSONValue(String type, Object coordinates) {

		return Value.getAsGeoJSON(
				String.format(Locale.ENGLISH, "{ \"type\": \"Point\", \"coordinates\":" + coordinates + " }"));
	}

	public static Boolean isNationalityIndia(HotelSearchQuery searchQuery) {
		String nationality = searchQuery.getSearchCriteria().getNationality();
		String countryCode = BaseHotelUtils.getSpecificFieldFromId(nationality, CountryInfoType.CODE.name());
		return countryCode.equalsIgnoreCase("IN");
	}

	public static Set<String> getCancellableRoomsFromHInfo(HotelInfo hInfo) {
		Set<String> roomKeys = new HashSet<>();
		hInfo.getOptions().get(0).getRoomInfos().forEach(roomInfo -> {
			if (roomInfo.getMiscInfo() != null
					&& BooleanUtils.isTrue(roomInfo.getMiscInfo().getIsRoomToBeCancelled())) {
				roomKeys.add(roomInfo.getId());
			}
		});
		return roomKeys;
	}

	public static boolean isPanRequired(HotelInfo hInfo, Option option, HotelSearchQuery searchQuery) {
		HotelGeneralPurposeOutput configuratorInfo = null;
		if (MapUtils.getObject(SystemContextHolder.getContextData().getValueMap(),
				HotelConfiguratorRuleType.GNPURPOSE.name()) == null) {
			configuratorInfo =
					HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
			SystemContextHolder.getContextData().setValue(HotelConfiguratorRuleType.GNPURPOSE.name(), configuratorInfo);
		} else {
			configuratorInfo = (HotelGeneralPurposeOutput) SystemContextHolder.getContextData().getValueMap()
					.get(HotelConfiguratorRuleType.GNPURPOSE.name());
		}

		if (Objects.nonNull(option.getIsPanRequired())) {
			return option.getIsPanRequired();
		}
		if (BooleanUtils.isTrue(configuratorInfo.getIsPanOrPassConfigEnabled())) {
			return setPanRequirement(hInfo, option, searchQuery);
		}
		return isNationalityIndia(searchQuery) && isPanOrPassSupplierRequired(option);
	}

	public static Boolean isPassportMandatory(Option option, HotelSearchQuery searchQuery) {

		HotelGeneralPurposeOutput configuratorInfo = null;
		if (MapUtils.getObject(SystemContextHolder.getContextData().getValueMap(),
				HotelConfiguratorRuleType.GNPURPOSE.name()) == null) {
			configuratorInfo =
					HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
			SystemContextHolder.getContextData().setValue(HotelConfiguratorRuleType.GNPURPOSE.name(), configuratorInfo);
		} else {
			configuratorInfo = (HotelGeneralPurposeOutput) SystemContextHolder.getContextData().getValueMap()
					.get(HotelConfiguratorRuleType.GNPURPOSE.name());
		}

		if (BooleanUtils.isTrue(configuratorInfo.getIsPanOrPassConfigEnabled())) {
			return setPassportRequirement(option, searchQuery);
		}
		return !isNationalityIndia(searchQuery) && isPanOrPassSupplierRequired(option);
	}

	public static Boolean isPanOrPassSupplierRequired(Option option) {
		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
		if (Objects.nonNull(configuratorInfo) && Objects.nonNull(configuratorInfo.getSuppliersPanCardNotReqd())) {
			return !configuratorInfo.getSuppliersPanCardNotReqd().contains(option.getMiscInfo().getSupplierId());
		}
		return true;
	}

	public static Boolean setPanRequirement(HotelInfo hInfo, Option option, HotelSearchQuery searchQuery) {
		HotelBasicFact basicFact =
				HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery).generateFactFromHotelInfo(hInfo);
		basicFact.setSupplierId(option.getMiscInfo().getSupplierId());
		HotelPanConfigOutput panConfigOutput =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(basicFact, HotelConfiguratorRuleType.PANCONFIG);
		if (Objects.nonNull(panConfigOutput)) {
			option.getMiscInfo().setIsPersonalPanRequired(panConfigOutput.getIsPersonalPanRequired());
			option.getMiscInfo().setIsOnePanRequiredPerBooking(panConfigOutput.getIsOnePanRequiredPerBooking());
			return true;
		}
		return false;
	}

	public static Boolean setPassportRequirement(Option option, HotelSearchQuery searchQuery) {
		HotelBasicFact basicFact = HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery);
		basicFact.setSupplierId(option.getMiscInfo().getSupplierId());
		HotelPassportConfigOutput passportConfigOutput =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(basicFact, HotelConfiguratorRuleType.PASSPORTCONFIG);
		if (Objects.nonNull(passportConfigOutput)) {
			return true;
		}
		return false;
	}

	public static void storeLogs(HotelProbeLogsMetaInfo log) {
		RestAPIListener listener = new RestAPIListener("");
		listener.addLog(LogData.builder().key(log.getLogKey()).logData(getRequestLogData(log))
				.type(getProbeLogsIdentifier(log, "Req")).generationTime(Instant
						.ofEpochMilli(log.getRequestGenerationTime()).atZone(ZoneId.systemDefault()).toLocalDateTime())
				.build());

		listener.addLog(LogData.builder().key(log.getLogKey()).logData(log.getResponseString())
				.type(getProbeLogsIdentifier(log, "Res")).generationTime(Instant
						.ofEpochMilli(log.getResponseGenerationTime()).atZone(ZoneId.systemDefault()).toLocalDateTime())
				.build());

	}

	private static String getProbeLogsIdentifier(HotelProbeLogsMetaInfo log, String type) {
		if (Objects.nonNull(log.getPrefix())
				&& !StringUtils.containsIgnoreCase(log.getRequestType(), log.getPrefix())) {
			log.setRequestType(log.getPrefix() + "-" + log.getRequestType());
		}
		StringBuilder identifier =
				new StringBuilder(String.join("_", log.getSupplierName(), log.getRequestType() + "-" + type));
		if (Objects.nonNull(log.getThreadCount()) && log.getThreadCount() > 0) {
			identifier.append("_");
			identifier.append(log.getThreadCount());
		}
		if (StringUtils.isNotEmpty(log.getAdditionalinfo())) {
			identifier.append("_");
			identifier.append(log.getAdditionalinfo());
		}
		return identifier.toString();
	}

	private static String getRequestLogData(HotelProbeLogsMetaInfo log) {
		StringBuilder logData = new StringBuilder();
		logData.append(log.getUrlString());
		if (Objects.nonNull(log.getHeaderParams())) {
			logData.append(" ");
			logData.append(log.getHeaderParams());
		}
		if (Objects.nonNull(log.getPostData())) {
			logData.append(" ");
			logData.append(log.getPostData());
		}
		return logData.toString();
	}

	public static String getLeadPanNumber(Option option) {
		RoomInfo roomInfo = option.getRoomInfos().get(0);
		return roomInfo.getTravellerInfo().get(0).getPanNumber();
	}

	public static String getCompleteAddress(Address address) {
		return Stream
				.of(address.getAddressLine1(), address.getAddressLine2(), address.getCity().getName(),
						address.getCountry().getName())
				.filter(s -> StringUtils.isNotBlank(s)).collect(Collectors.joining(","));

	}

	public static Map<String, Option> getOptionIdToOptionMap(HotelInfo hInfo) {

		Map<String, Option> map = new HashMap<>();
		for (Option option : hInfo.getOptions()) {
			map.put(option.getId(), option);
		}
		return map;
	}

	public static HotelSearchQuery getHotelSearchQuery(HotelSearchQuery searchQuery, HotelInfo hInfo) {

		HotelSearchQuery copyQuery = new GsonMapper<>(searchQuery, HotelSearchQuery.class).convert();
		copyQuery.setSourceId(hInfo.getOptions().get(0).getMiscInfo().getSourceId());
		copyQuery.setMiscInfo(HotelSearchQueryMiscInfo.builder()
				.supplierId(hInfo.getOptions().get(0).getMiscInfo().getSupplierId()).build());
		return copyQuery;
	}


}
