
package com.tgs.services.hms.sources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.aerospike.client.query.Filter;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.HotelWiseSearchInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelIdSearchOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.HotelSearchResponse;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public abstract class AbstractHotelInfoFactory {

	protected HotelSearchResult searchResult;
	protected HotelSupplierConfiguration supplierConf;
	protected HotelSearchQuery searchQuery;
	protected HotelSourceConfigOutput sourceConfigOutput;
	protected HotelSupplierRegionInfo supplierRegionInfo;

	@Autowired
	protected HotelCacheHandler cacheHandler;

	@Autowired
	protected GeneralServiceCommunicator gsCommunicator;

	@Autowired
	protected GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public AbstractHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		this.searchQuery = searchQuery;
		this.supplierConf = supplierConf;
		this.sourceConfigOutput = HotelUtils.getHotelSourceConfigOutput(searchQuery);
	}

	private void init() {

		if (StringUtils.isNotBlank(searchQuery.getSearchCriteria().getRegionId())) {
			supplierRegionInfo =
					cacheHandler.getRegionInfoMappingFromRegionId(searchQuery.getSearchCriteria().getRegionId(),
							HotelSourceType.getHotelSourceType(supplierConf.getBasicInfo().getSourceId()).name());
		}
		supplierRegionInfo =
				Objects.nonNull(supplierRegionInfo) ? supplierRegionInfo : HotelSupplierRegionInfo.builder().build();
	}

	protected abstract void searchAvailableHotels() throws IOException, JAXBException, InterruptedException;

	protected abstract void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery)
			throws IOException, JAXBException;

	protected abstract void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException;

	public HotelSearchResult getAvailableHotels() {
		long startTime = System.currentTimeMillis();
		try {
			LogUtils.log(LogTypes.HOTEL_SUPPLIER_SEARCH_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			init();
			setHotelWiseSearchInfo();
			searchResult = this.getHotelSearchDetailsFromCache();
			if (Objects.isNull(searchResult)) {
				log.info("Doing search with search id {} for supplier {}", searchQuery.getSearchId(),
						supplierConf.getBasicInfo().getSupplierName());
				searchResult = HotelSearchResult.builder().build();
				if (Objects.isNull(sourceConfigOutput.getSupplierSearchCriteria())
						|| sourceConfigOutput.getSupplierSearchCriteria().validateSearchCriteria(searchQuery)) {
					this.searchAvailableHotels();
					HotelUtils.setUniqueOptionId(searchResult.getHotelInfos());
					cacheHandler.cacheHotelSearchResults(searchResult, searchQuery, supplierConf);
					LogUtils.log(LogTypes.HOTEL_SUPPLIER_SEARCH_END,
							LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
									.searchId(searchQuery.getSearchId()).trips(searchResult.getNoOfHotelOptions())
									.supplierId(supplierConf.getBasicInfo().getSupplierName()).build(),
							LogTypes.HOTEL_SUPPLIER_SEARCH_START);
				} else {
					throw new CustomGeneralException(SystemError.INVALID_SEARCH_REQUST);
				}
			}
		} catch (IOException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to get search result due to I/O exception");
			log.info("Available hotel search failed because of I/O exception for searchQuery {}", searchQuery, e);

		} catch (CustomGeneralException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to get search result due to invalid search query for supplier");
			log.info("Available hotel search failed because of invalid searchQuery {} for supplier {}", searchQuery,
					supplierConf.getBasicInfo().getSupplierId(), e);
		} catch (Exception e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to get search result due to " + e.getCause());
			log.error("Available hotel search failed for searchQuery {}", searchQuery, e);
		} finally {
			LogUtils.clearLogList();
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, searchResult.getHotelInfos(), System.currentTimeMillis() - startTime);
		}
		return searchResult;
	}

	public void fetchHotelDetails(HotelInfo hInfo) {
		try {
			init();
			String searchId = hInfo.getMiscInfo().getSearchId();
			setHotelWiseSearchInfo();
			HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
			this.searchHotel(hInfo, searchQuery);
			HotelUtils.setUniqueOptionId(
					Objects.isNull(searchResult) ? Arrays.asList(hInfo) : searchResult.getHotelInfos());
		} catch (IOException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to get search hotel details to I/O exception");
			log.info("Available hotel search failed because of I/O exception for searchQuery {}", searchQuery, e);
		} catch (Exception e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to get hotel details due to " + e.getCause());
			log.error("Fetch hotel Details failed for hInfo {}", hInfo, e);
		} finally {
			LogUtils.clearLogList();
		}
	}

	private HotelSearchResult getHotelSearchDetailsFromCache() {
		return cacheHandler.getCachedSearchResult(searchQuery, supplierConf);
	}

	public void getCancellationPolicy(HotelSearchQuery query, HotelInfo hInfo, String logKey) {
		try {
			this.searchCancellationPolicy(hInfo, logKey);
		} catch (IOException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to fetch cancellation policy due to I/O exception");
			log.info("Unable to fetch cancellation policy due to I/O exception for searchQuery {}", searchQuery, e);
		} catch (Exception e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to fetch cancellation policy due to " + e.getCause());
			log.error("Unable to fetch cancellation policy for searchQuery {}, hInfo {}", query, hInfo, e);
		} finally {
			LogUtils.clearLogList();
		}
	}


	private Set<String> getPropertyIdsFromConfigRule() {
		HotelBasicFact hotelFact = HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery);
		HotelIdSearchOutput hotelIdSearchOutput =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(hotelFact, HotelConfiguratorRuleType.HOTELIDSEARCH);
		return Objects.isNull(hotelIdSearchOutput) ? null : hotelIdSearchOutput.getHotelIds();
	}

	protected Set<String> getPropertyIdsOfCity(String supplierName) {
		HotelWiseSearchInfo hotelWiseSearchInfo = searchQuery.getMiscInfo().getHotelWiseSearchInfo();
		if (Objects.nonNull(hotelWiseSearchInfo)) {
			return new HashSet<>(Arrays.asList(hotelWiseSearchInfo.getHotelId()));
		}

		Set<String> propertyIds = getPropertyIdsFromConfigRule();
		if (CollectionUtils.isNotEmpty(propertyIds)) {
			return propertyIds;
		}

		String cityName = getCityName();
		propertyIds = new HashSet<>();
		log.info("Fetching property id from cache for city {} and searchid {}", cityName, searchQuery.getSearchId());
		try {
			HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();
			String polygonInfo = getPolygonInfo();
			if (StringUtils.isNotBlank(polygonInfo)) {
				propertyIds = fetchPropertyIdsByPolygonInfo(supplierName, polygonInfo, staticDataService);
				return propertyIds;
			} else if (!cityName.contains("@")) {
				cityName = staticDataService.getCityName(supplierName, cityName.toLowerCase());
				CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder()
						.namespace(CacheNameSpace.HOTEL.getName())
						.set(staticDataService.getSupplierSetName(supplierName))
						.bins(new String[] {BinName.CITY.name(), BinName.RATING.name()}).plainData(true).build();
				Map<String, Map<String, String>> supplierhotelIdMap = cachingCommunicator.getResultSet(
						supplierMappingMetaInfo, String.class, Filter.equal(BinName.CITY.getName(), cityName));
				for (Map.Entry<String, Map<String, String>> supplierHotel : supplierhotelIdMap.entrySet()) {
					Map<String, String> supplierInfo = supplierHotel.getValue();
					if (StringUtils.isNotBlank(supplierInfo.get(BinName.RATING.name()))
							&& searchQuery.getSearchPreferences().getRatings()
									.contains(Integer.parseInt(supplierInfo.get(BinName.RATING.name())))) {
						propertyIds.add(supplierHotel.getKey());
					}
				}
				if (StringUtils.isNotBlank(staticDataService.getSeparator())) {
					return HotelUtils.removePrefix(propertyIds, supplierName + staticDataService.getSeparator());
				}
				return propertyIds;
			} else {
				propertyIds = fetchPropertyIdsBySupplierCityName(supplierName, cityName, staticDataService);
				return propertyIds;
			}
		} catch (Exception e) {
			log.info("Unable to fetch property ids of city {}", cityName, e);
			return null;
		} finally {
			log.info("Fetched property id from cache for city {} and searchid {}. Size is {}", cityName,
					searchQuery.getSearchId(), propertyIds.size());
		}
	}

	public Set<String> fetchPropertyIdsBySupplierCityName(String supplierName, String regionKey,
			HotelStaticDataService staticDataService) {

		String bin = "";
		String searchRegion = "";
		Set<String> propertyIds = new HashSet<>();
		String[] region = regionKey.split("@");
		String city = region[0];
		String state = region[1];
		String country = region[2];
		if (!city.equals("NA")) {
			bin = BinName.CITY.name();
			searchRegion = city;
		} else if (!state.equals("NA")) {
			bin = BinName.STATE.name();
			searchRegion = state;
		} else if (!country.equals("NA")) {
			bin = BinName.COUNTRY.name();
			searchRegion = country;
		}
		city = staticDataService.getCityName(supplierName, city);
		state = staticDataService.getStateName(supplierName, state);
		country = staticDataService.getCountryName(supplierName, country);
		// HotelUtils.fetchBinAndRegionFromKey(bin, searchRegion, regionKey);
		CacheMetaInfo supplierMappingMetaInfo =
				CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
						.set(staticDataService.getSupplierSetName(supplierName)).bins(new String[] {BinName.CITY.name(),
								BinName.STATE.name(), BinName.COUNTRY.name(), BinName.RATING.name()})
						.plainData(true).build();
		searchRegion = String.join("_", supplierName, searchRegion.toLowerCase());
		log.info(
				"Fetching property id from cache for region {} and searchid {}. City {}, State {}, Country {} and Rating {}",
				searchRegion, searchQuery.getSearchId(), city, state, country,
				searchQuery.getSearchPreferences().getRatings());
		try {
			Map<String, Map<String, String>> supplierhotelIdMap = cachingCommunicator
					.getResultSet(supplierMappingMetaInfo, String.class, Filter.equal(bin, searchRegion));
			for (Map.Entry<String, Map<String, String>> supplierHotel : supplierhotelIdMap.entrySet()) {

				Map<String, String> supplierInfo = supplierHotel.getValue();
				if (StringUtils.isNotBlank(supplierInfo.get(BinName.RATING.name()))
						&& searchQuery.getSearchPreferences().getRatings()
								.contains(Integer.parseInt(supplierInfo.get(BinName.RATING.name())))) {
					if (StringUtils.isNotBlank(supplierInfo.get(BinName.STATE.name()))
							&& !state.equalsIgnoreCase(staticDataService.getStateName(supplierName, "NA"))) {
						if (state.equalsIgnoreCase(supplierInfo.get(BinName.STATE.name()))) {
							propertyIds.add(supplierHotel.getKey());
						}
					}
					if (StringUtils.isNotBlank(supplierInfo.get(BinName.COUNTRY.name()))
							&& !country.equals(staticDataService.getCountryName(supplierName, "NA"))) {
						if (country.equalsIgnoreCase(supplierInfo.get(BinName.COUNTRY.name()))) {
							propertyIds.add(supplierHotel.getKey());
						}
					}
				}
			}
			int propertyIdSizeLimit = getPropertyIdSizeLimit();
			if (propertyIds.size() > propertyIdSizeLimit)
				throw new CustomGeneralException(SystemError.MAXIMUM_PROPERTYIDS_LIMIT_EXCEEDED);

			if (StringUtils.isNotBlank(staticDataService.getSeparator())) {
				return HotelUtils.removePrefix(propertyIds, supplierName + staticDataService.getSeparator());
			}
		} finally {
			log.info("Fetched property id from cache for region {} and searchid {}. Size is {}", searchRegion,
					searchQuery.getSearchId(), propertyIds.size());
		}
		return propertyIds;
	}

	public HotelSearchResult getResultOfAllPropertyIds(Set<String> propertyIds)
			throws IOException, InterruptedException {
		/*
		 * If we have to nullify this new code, simply make sequentialcalls=1, and maxthreadAllowed= list.size()
		 */
		HotelSearchResult finalSearchResult = HotelSearchResult.builder().build();
		List<Map<String, List<CheckPointData>>> checkPointInfoList = new ArrayList<>();
		Set<String> errorMessages = new HashSet<>();
		if (CollectionUtils.isEmpty(propertyIds)) {
			return finalSearchResult;
		}
		try {
			/*
			 * If we have to nullify this new code, simply make sequentialcalls=1, and maxthreadAllowed= list.size()
			 */
			List<Set<String>> listOfPartitionedIdsSet = getPropertyIdsSupportedPerSearch(propertyIds);

			final int maxThreadAllowed = getMaxParallelRequestAllowed();
			final int sequentialCalls = getTotalSequentialCalls(maxThreadAllowed, listOfPartitionedIdsSet.size());

			List<Future<?>> hotelSearchFutureTaskList = null;
			AtomicInteger threadCount = new AtomicInteger(0);
			for (int sequentialCallsIndex = 0; sequentialCallsIndex < sequentialCalls; sequentialCallsIndex++) {
				hotelSearchFutureTaskList = new ArrayList<Future<?>>();
				for (int maxThreadAllowedIndex = 0; maxThreadAllowedIndex < maxThreadAllowed; maxThreadAllowedIndex++) {
					int listIndex = (sequentialCallsIndex * maxThreadAllowed) + maxThreadAllowedIndex;
					if (listIndex < listOfPartitionedIdsSet.size()) {
						Future<?> hotelSearchFutureTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
							HotelSearchResponse searchResponse = new HotelSearchResponse();
							try {
								HotelSearchResult searchResult = doSearchForSupplier(
										listOfPartitionedIdsSet.get(listIndex), threadCount.incrementAndGet());
								searchResponse.setSearchResult(
										ObjectUtils.firstNonNull(searchResult, HotelSearchResult.builder().build()));

							} catch (IOException e) {
								log.info("IOException while fetching search result for search query {} ",
										this.searchQuery, e);
								throw new CustomGeneralException(
										"Unable to fetch search result due to " + e.getMessage());
							} finally {
								searchResponse
										.setCheckPointInfoMap(SystemContextHolder.getContextData().getCheckPointInfo());
								searchResponse
										.setErrorMessages(SystemContextHolder.getContextData().getErrorMessages());
								LogUtils.clearLogList();
								SystemContextHolder.getContextData().setCheckPointInfo(null);
							}
							return searchResponse;
						});
						hotelSearchFutureTaskList.add(hotelSearchFutureTask);
					}
				}
				for (Future<?> future : hotelSearchFutureTaskList) {
					Object result;
					try {
						result = future.get();
						if (Objects.nonNull(result)) {
							HotelSearchResponse searchResponse = (HotelSearchResponse) result;
							HotelSearchResult searchResult = searchResponse.getSearchResult();
							checkPointInfoList.add(searchResponse.getCheckPointInfoMap());
							errorMessages.addAll(searchResponse.getErrorMessages());
							finalSearchResult.getHotelInfos().addAll(searchResult.getHotelInfos());
						}
					} catch (InterruptedException | ExecutionException e) {
						log.info("Interrupted exception while fetching search result for search query {} ",
								this.searchQuery, e);
					}
				}
			}
		} finally {
			try {
				SystemContextHolder.getContextData().addCheckPoints(getCheckPoints(checkPointInfoList));
				SystemContextHolder.getContextData().setErrorMessages(new ArrayList<>(errorMessages));
			} catch (Exception e) {
				log.info("Checkpoints can not be added to system context for searchId {} due to exception {}",
						searchQuery.getSearchId(), e);
			}
		}
		setBookPayLaterDate(finalSearchResult);
		return finalSearchResult;
	}

	public Set<String> fetchPropertyIdsByPolygonInfo(String supplierName, String polygonInfo,
			HotelStaticDataService staticDataService) {


		log.info("Fetching property ids from cache for search id {}, supplier {} and polygon {}",
				searchQuery.getSearchId(), supplierName, polygonInfo);
		Set<String> propertyIds = new HashSet<>();
		try {

			CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
					.bins(new String[] {BinName.RATING.name()}).set(staticDataService.getSupplierSetName(supplierName))
					.build();

			Map<String, Map<String, String>> supplierhotelIdMap =
					cachingCommunicator.getResultSet(supplierMappingMetaInfo, String.class,
							Filter.geoWithinRegion(BinName.GEOLOCATION.name(), polygonInfo));
			for (Map.Entry<String, Map<String, String>> supplierHotel : supplierhotelIdMap.entrySet()) {
				Map<String, String> supplierInfo = supplierHotel.getValue();
				String ratingBinValue = supplierInfo.get(BinName.RATING.name());
				if (StringUtils.isNotBlank(ratingBinValue)
						&& searchQuery.getSearchPreferences().getRatings().contains(Integer.parseInt(ratingBinValue))) {
					propertyIds.add(supplierHotel.getKey());
				}
			}
			if (StringUtils.isNotBlank(staticDataService.getSeparator())) {
				return HotelUtils.removePrefix(propertyIds, supplierName + staticDataService.getSeparator());
			}
			return propertyIds;
		} catch (Exception e) {
			log.info("Unable to fetch property ids from cache for search id {}, supplier {} and polygon {}",
					searchQuery.getSearchId(), supplierName, polygonInfo);
			return null;
		} finally {
			log.info("Fetched property ids from cache for search id {}, supplier {} and polygon {}",
					searchQuery.getSearchId(), supplierName, polygonInfo);
		}
	}

	private List<CheckPointData> getCheckPoints(List<Map<String, List<CheckPointData>>> checkPointInfoList) {
		if (CollectionUtils.isEmpty(checkPointInfoList)) {
			return null;
		}
		Map<String, List<CheckPointData>> checkPointInfoParallelCallMap = new HashMap<>();
		List<Long> externalAPIParsingTimeDiff = new ArrayList<>();
		Set<CheckPointData> checkPointsList = new HashSet<>();
		for (Map<String, List<CheckPointData>> checkPointInfoMap : checkPointInfoList) {
			externalAPIParsingTimeDiff.add(getParsingTimeDiff(checkPointInfoMap));
			for (Entry<String, List<CheckPointData>> checkPointData : checkPointInfoMap.entrySet()) {
				List<CheckPointData> checkPointDataList =
						checkPointInfoParallelCallMap.getOrDefault(checkPointData.getKey(), new ArrayList<>());

				checkPointDataList.addAll(checkPointData.getValue());
				checkPointInfoParallelCallMap.put(checkPointData.getKey(), checkPointDataList);
			}
		}
		Long externalApiFinishedTime = null;
		List<CheckPointData> checkPointDataList = null;

		checkPointDataList = checkPointInfoParallelCallMap.get("EXTERNAL_API_STARTED");
		if (Objects.nonNull(checkPointDataList)) {
			checkPointsList.add(checkPointDataList.stream()
					.collect(Collectors.minBy(Comparator.comparingLong(CheckPointData::getTime))).get());
		}
		checkPointDataList = checkPointInfoParallelCallMap.get("EXTERNAL_API_FINISHED");
		if (Objects.nonNull(checkPointDataList)) {
			CheckPointData checkPointData = checkPointDataList.stream()
					.collect(Collectors.maxBy(Comparator.comparingLong(CheckPointData::getTime))).get();
			externalApiFinishedTime = checkPointData.getTime();
			checkPointsList.add(checkPointData);
		}
		checkPointDataList = checkPointInfoParallelCallMap.get("EXTERNAL_API_PARSING_FINISHED");
		if (Objects.nonNull(checkPointDataList) && Objects.nonNull(externalApiFinishedTime)) {
			Long externalApiParsingTime = externalApiFinishedTime
					+ externalAPIParsingTimeDiff.stream().max(Comparator.comparing(Long::valueOf)).get();
			checkPointsList.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
					.time(externalApiParsingTime).build());
		}
		checkPointDataList = checkPointInfoParallelCallMap.get("REQUEST_STARTED");
		if (Objects.nonNull(checkPointDataList)) {
			checkPointsList.add(checkPointDataList.stream()
					.collect(Collectors.minBy(Comparator.comparingLong(CheckPointData::getTime))).get());
		}
		return new ArrayList<>(checkPointsList);
	}

	private Long getParsingTimeDiff(Map<String, List<CheckPointData>> checkPointInfoMap) {
		if (CollectionUtils.isNotEmpty(checkPointInfoMap.get("EXTERNAL_API_PARSING_FINISHED"))
				&& CollectionUtils.isNotEmpty(checkPointInfoMap.get("EXTERNAL_API_FINISHED"))) {
			Long externalAPIParsingFinished = checkPointInfoMap.get("EXTERNAL_API_PARSING_FINISHED").get(0).getTime();
			Long externalAPIFinished = checkPointInfoMap.get("EXTERNAL_API_FINISHED").get(0).getTime();
			return (externalAPIParsingFinished - externalAPIFinished);
		}
		return 0L;
	}

	private int getMaxParallelRequestAllowed() {
		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
		return Objects.isNull(configuratorInfo.getMaxThreadAllowed()) ? 10 : configuratorInfo.getMaxThreadAllowed();
	}

	private int getTotalSequentialCalls(int maxThreadAllowed, int hotelIdListSize) {
		return (hotelIdListSize % maxThreadAllowed) == 0 ? (hotelIdListSize / maxThreadAllowed)
				: (hotelIdListSize / maxThreadAllowed) + 1;
	}

	private List<Set<String>> getPropertyIdsSupportedPerSearch(Set<String> propertyIds) {
		final int maxBatchSize =
				Objects.isNull(sourceConfigOutput.getHotelBatchSize()) ? 50 : sourceConfigOutput.getHotelBatchSize();
		final int hotelIdsPerRequest =
				Objects.isNull(sourceConfigOutput.getHotelHitCount()) ? 100 : sourceConfigOutput.getHotelHitCount();
		List<Set<String>> listOfPartitionedIdsSet = HotelUtils.partitionSet(propertyIds, hotelIdsPerRequest);
		if (listOfPartitionedIdsSet.size() > maxBatchSize) {
			log.info("For searchId {} limiting hotelids  from {} to {}", searchQuery.getSearchId(),
					listOfPartitionedIdsSet.size() * hotelIdsPerRequest,
					listOfPartitionedIdsSet.subList(0, maxBatchSize).size() * hotelIdsPerRequest);
			return listOfPartitionedIdsSet.subList(0, maxBatchSize);
		}
		return listOfPartitionedIdsSet;
	}

	private void setBookPayLaterDate(HotelSearchResult searchResult) {
		if (BooleanUtils.isNotTrue(sourceConfigOutput.getAllowBookNowPayLater())) {
			List<HotelInfo> hotelInfos = searchResult.getHotelInfos();
			hotelInfos.forEach(hInfo -> {
				List<Option> options = hInfo.getOptions();
				options.forEach(option -> {
					option.getRoomInfos().forEach(roomInfo -> {
						roomInfo.setBookNowPayLaterDate(null);
					});
				});
			});
		}
	}

	protected HotelSearchResult doSearchForSupplier(Set<String> set, int threadCount)
			throws IOException, JAXBException {
		return null;
	}

	private String getCityName() {

		if (supplierRegionInfo != null) {
			return HotelUtils.firstNonEmpty(supplierRegionInfo.getSupplierRegionName(),
					searchQuery.getSearchCriteria().getCityName());
		}
		return searchQuery.getSearchCriteria().getCityName();
	}

	private Integer getPropertyIdSizeLimit() {
		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
		return configuratorInfo.getPropertyIdsSizeLimit() != null ? configuratorInfo.getPropertyIdsSizeLimit() : 7500;

	}

	private void setHotelWiseSearchInfo() {

		if (StringUtils.isNotBlank(searchQuery.getSearchPreferences().getHotelId())) {
			List<HotelWiseSearchInfo> hotelWiseSearchInfos = HotelUtils.getStaticDataService()
					.getHotelWiseSearchInfo(searchQuery.getSearchPreferences().getHotelId());
			searchQuery.getSearchCriteria().setCountryName(hotelWiseSearchInfos.get(0).getCountryName());
			hotelWiseSearchInfos = hotelWiseSearchInfos.stream()
					.filter(hotelWiseSearchInfo -> hotelWiseSearchInfo.getSupplierName()
							.equals(supplierConf.getBasicInfo().getSupplierName()))
					.sorted(Comparator.comparing(HotelWiseSearchInfo::getLastModifiedDateTime,
							Comparator.nullsLast(Comparator.naturalOrder())))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(hotelWiseSearchInfos)) {
				HotelWiseSearchInfo hotelWiseSearchInfo = hotelWiseSearchInfos.get(0);
				searchQuery.getSearchCriteria().setCityName(hotelWiseSearchInfo.getCityName());
				searchQuery.getSearchCriteria().setCountryName(hotelWiseSearchInfo.getCountryName());
				searchQuery.getMiscInfo().setHotelWiseSearchInfo(hotelWiseSearchInfo);
			}
		}
	}

	protected String getPolygonInfo() {

		ClientGeneralInfo clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		String supplierName = clientInfo.getPolygonInfoSupplierName();
		if (StringUtils.isNotBlank(supplierName)) {
			HotelSupplierRegionInfo supplierRegionInfo =
					cacheHandler.getRegionInfoMappingFromRegionId(searchQuery.getSearchCriteria().getRegionId(),
							HotelSourceType.getHotelSourceTypeFromSourceName(supplierName).name());
			if (Objects.nonNull(supplierRegionInfo.getAdditionalInfo())
					&& StringUtils.isNotBlank(supplierRegionInfo.getAdditionalInfo().getPolygonInfo()))
				return supplierRegionInfo.getAdditionalInfo().getPolygonInfo();
		}
		return null;
	}
}
