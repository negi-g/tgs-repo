package com.tgs.services.hms.sources;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.tgs.services.base.datamodel.VerifyMappingRequest;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Service
@Slf4j
public abstract class AbstractVerifyHotelMappingFactory {
	protected HotelSupplierConfiguration supplierConf;
	protected HotelSourceConfigOutput sourceConfigOutput;
	protected VerifyMappingRequest verifyMappingData;

	public AbstractVerifyHotelMappingFactory(HotelSupplierConfiguration supplierConf,
			VerifyMappingRequest verifyMappingData) {
		this.supplierConf = supplierConf;
		this.verifyMappingData = verifyMappingData;
		this.sourceConfigOutput = HotelUtils.getHotelSourceConfigOutput(
				HotelSearchQuery.builder().sourceId(supplierConf.getBasicInfo().getSourceId()).build());
	}

	public abstract void uploadVerifyHotelMapping() throws SftpException, JSchException, IOException;

	public void verifyHotelMappings() {
		try {
			this.uploadVerifyHotelMapping();
		} catch (SftpException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to uplaod verifyMapping due to SftpException");
			log.info("Upload verify mapping failed due to SftpException for data of size {}",
					verifyMappingData.getVerifyMappinglist().size(), e);
		} catch (JSchException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to uplaod verifyMapping due to JSchException");
			log.info("Upload verify mapping failed due to JSchException for data of size {}",
					verifyMappingData.getVerifyMappinglist().size(), e);
		} catch (IOException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to uplaod verifyMapping due to IOException");
			log.info("Upload verify mapping failed due to IOException for data of size {}",
					verifyMappingData.getVerifyMappinglist().size(), e);
		} catch (Exception e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to uplaod verifyMapping due to Exception");
			log.info("Upload verify mapping failed due to service exception for data of size {}",
					verifyMappingData.getVerifyMappinglist().size(), e);
		}
	}
}
