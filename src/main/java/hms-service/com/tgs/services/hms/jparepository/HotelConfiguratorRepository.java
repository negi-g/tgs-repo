package com.tgs.services.hms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.hms.dbmodel.DbHotelConfiguratorRule;

@Repository
public interface HotelConfiguratorRepository
		extends JpaRepository<DbHotelConfiguratorRule, Long>, JpaSpecificationExecutor<DbHotelConfiguratorRule> {

}
