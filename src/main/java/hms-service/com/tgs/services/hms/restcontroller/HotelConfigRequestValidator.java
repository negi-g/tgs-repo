package com.tgs.services.hms.restcontroller;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.SearchExclusionOutput;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;

@Component
public class HotelConfigRequestValidator extends TgsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof HotelConfiguratorInfo) {
			HotelConfiguratorInfo configuratorInfo = (HotelConfiguratorInfo) target;

			if (configuratorInfo.getRuleType() == null)
				rejectValue("ruleType", SystemError.INVALID_HOTEL_CONFIGURATOR_RULE_TYPE, errors);

			if (!ObjectUtils.isEmpty(configuratorInfo.getInclusionCriteria())) {
				registerErrors(errors, "inclusionCriteria",
						(HotelBasicRuleCriteria) configuratorInfo.getInclusionCriteria());
			}

			if (!ObjectUtils.isEmpty(configuratorInfo.getExclusionCriteria())) {
				registerErrors(errors, "exclusionCriteria",
						(HotelBasicRuleCriteria) configuratorInfo.getExclusionCriteria());
			}


			if (configuratorInfo.getRuleType() != null
					&& HotelConfiguratorRuleType.SEARCHEXCLUSION.name().equals(configuratorInfo.getRuleType().name())) {

				HotelGeneralPurposeOutput generalConfiguratorInfo =
						HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);

				int unicaIdsLimit = ObjectUtils.isEmpty(generalConfiguratorInfo.getMaxUnicaAllowedInConfig()) ? 10000
						: generalConfiguratorInfo.getMaxUnicaAllowedInConfig();

				SearchExclusionOutput output = (SearchExclusionOutput) configuratorInfo.getOutput();

				if (CollectionUtils.isNotEmpty(output.getUnicaIds()) && unicaIdsLimit < output.getUnicaIds().size()) {
					rejectValue(errors, "output", SystemError.MAXIMUM_UNICAID_LIMIT_EXCEEDED, unicaIdsLimit);
				}

			}

		}
	}

	private static void rejectValue(String field, SystemError error, Errors errors) {
		errors.rejectValue("ruleType", error.errorCode(), error.getMessage());
	}

	private static void rejectValue(Errors errors, String field, SystemError error, Object... args) {
		errors.rejectValue(field, error.errorCode(), error.getMessage(args));
	}

}
