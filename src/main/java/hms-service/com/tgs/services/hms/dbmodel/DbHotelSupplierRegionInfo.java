package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.HotelRegionAdditionalInfo;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Getter
@Setter
@TypeDefs({@TypeDef(name = "HotelRegionAdditionalInfoType", typeClass = HotelRegionAdditionalInfoType.class)})
@Table(name = "hotelsupplierregioninfo", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"regionName", "stateName", "countryName", "supplierName", "regionType",
				"fullRegionName"})})
public class DbHotelSupplierRegionInfo extends BaseModel<DbHotelSupplierRegionInfo, HotelSupplierRegionInfo> {

	private String regionId;
	private String regionName;
	private String stateId;
	private String stateName;
	private String countryId;
	private String countryName;
	private String regionType;
	private String supplierName;
	private String supplierRegionName;
	private String fullRegionName;

	@Type(type = "HotelRegionAdditionalInfoType")
	private HotelRegionAdditionalInfo additionalInfo;

	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;


	@Override
	public HotelSupplierRegionInfo toDomain() {
		return new GsonMapper<>(this, HotelSupplierRegionInfo.class).convert();
	}

	@Override
	public DbHotelSupplierRegionInfo from(HotelSupplierRegionInfo dataModel) {
		if (StringUtils.isBlank(dataModel.getRegionId()))
			dataModel.setRegionId("");
		if (StringUtils.isBlank(dataModel.getRegionName()))
			dataModel.setRegionName("");
		if (StringUtils.isBlank(dataModel.getStateId()))
			dataModel.setStateId("");
		if (StringUtils.isBlank(dataModel.getStateName()))
			dataModel.setStateName("");
		if (StringUtils.isBlank(dataModel.getCountryName()))
			dataModel.setCountryId("");
		if (StringUtils.isBlank(dataModel.getRegionType()))
			dataModel.setRegionType("CITY");
		if (StringUtils.isBlank(dataModel.getFullRegionName()))
			dataModel.setFullRegionName("");
		return new GsonMapper<>(dataModel, this, DbHotelSupplierRegionInfo.class).convert();
	}
}
