package com.tgs.services.hms.sources.inventory;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.tgs.filters.HotelRatePlanFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.manager.HotelRatePlanManager;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class HotelInventoryPriceValidationService extends HotelInventoryBaseService {
	
	private String bookingId;
	private Option updatedOption;
	private HotelRatePlanManager ratePlanManager;
	
	
	public void validate(HotelInfo hInfo) throws Exception {

		if(!isSupplierEnabled(hInfo.getOptions().get(0).getRoomInfos().get(0).getMiscInfo().getSecondarySupplier())) 
			throw new CustomGeneralException(SystemError.SUPPLIER_NOT_FOUND);
		
		List<Long> ratePlanIds = getRatePlanIdsToBeBooked(hInfo);
		HotelRatePlanFilter ratePlanFilter = HotelRatePlanFilter.builder()
				.ids(ratePlanIds).build();
		List<HotelRatePlan> ratePlanList = ratePlanManager.fetchHotelRatePlan(ratePlanFilter);
		
		int ratePlanListSize=ratePlanList.size();
		filterRatePlanList(ratePlanList);
		if(ratePlanList.size()!=ratePlanListSize) {
			throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
		}
		List<Option> optionList = fetchOptionListForHotel(ratePlanList);
		if(CollectionUtils.isEmpty(optionList))
			throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
		
		Option oldOption = hInfo.getOptions().get(0);
		List<Option> filteredOptions = optionList.stream().filter(option -> option.getId().equals(oldOption.getId()))
 				.collect(Collectors.toList());
		if(filteredOptions.size() > 1)
			return;
		updatedOption = filteredOptions.get(0);
		
	}


	private List<Long> getRatePlanIdsToBeBooked(HotelInfo hInfo) {
		
		List<Long> ratePlanIdList  = hInfo.getOptions().get(0).getRoomInfos().stream()
			.map(roomInfo -> roomInfo.getMiscInfo().getRatePlanIdList()).flatMap(list -> list.stream())
			.collect(Collectors.toList());
		
		return ratePlanIdList;
	}
	

}
