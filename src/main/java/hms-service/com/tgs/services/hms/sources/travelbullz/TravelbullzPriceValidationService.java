package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.travelbullz.AdvancedOptions;
import com.tgs.services.hms.datamodel.travelbullz.HotelBookOption;
import com.tgs.services.hms.datamodel.travelbullz.HotelRoom;
import com.tgs.services.hms.datamodel.travelbullz.Request;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzPreBookResponse;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@SuperBuilder
@Service
@Slf4j
public class TravelbullzPriceValidationService extends TravelbullzBaseService {
	private Option updatedOption;
	private String bookingId;
	private TravelbullzPreBookResponse preBookResponse;
	private String searchkey;

	public void validate(HotelInfo hInfo) throws IOException {
		HttpUtilsV2 httpUtils = null;
		listener = new RestAPIListener("");
		try {
			searchkey = hInfo.getOptions().get(0).getMiscInfo().getSearchKey();
			TravelbullzRequest searchRequest = getSearchRequest(hInfo.getOptions().get(0));
			String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
			String urlString =
					StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.PRICECHECK_REVIEW));
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(searchRequest), urlString);
			preBookResponse = httpUtils.getResponse(TravelbullzPreBookResponse.class).orElse(null);
			if (!ObjectUtils.isEmpty(preBookResponse.getError())) {
				log.info("price validation failed for booking id {} due to {}", bookingId,
						preBookResponse.getError().get(0).getDescription());
				throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
			}
			createOptionWithNewPrice(preBookResponse.getPreBookRS().getHotelOption(), hInfo);
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.REVIEW.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(preBookResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRAVELBULLZ.name())
						.requestType(BaseHotelConstants.PRICE_CHECK).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(bookingId)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private void createOptionWithNewPrice(HotelBookOption hotelBookOption, HotelInfo hInfo) {
		Option oldOption = hInfo.getOptions().get(0);

		if (!hotelBookOption.getStatus().equalsIgnoreCase("bookable")) {
			throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
		}
		OptionMiscInfo optionMiscInfo = OptionMiscInfo.builder()
				.supplierId(supplierConf.getBasicInfo().getSupplierName())
				.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
				.sourceId(supplierConf.getBasicInfo().getSourceId()).hotelOptionId(hotelBookOption.getHotelOptionId())
				.bookingToken(hotelBookOption.getBookingToken()).status(hotelBookOption.getStatus()).build();

		List<RoomInfo> roomInfos = new ArrayList<>();
		List<HotelRoom> responseRoom = hotelBookOption.getHotelRooms();

		responseRoom.forEach(room -> {
			RoomMiscInfo roomMiscInfo = RoomMiscInfo.builder().roomBookingId(room.getUniqueId() + "")
					.inclusive(room.getPrice()).roomIndex(Integer.parseInt(room.getRoomNo())).build();
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setRoomType(room.getRoomTypeName());
			roomInfo.setMealBasis(room.getMappedMealName());
			roomInfo.setMiscInfo(roomMiscInfo);
			roomInfo.setPerNightPriceInfos(setPerNightPrice(room.getPrice()));
			roomInfos.add(roomInfo);
		});
		updatePriceWithMarkup(roomInfos);

		Option option = Option.builder().roomInfos(roomInfos).miscInfo(optionMiscInfo)
				.id(hInfo.getOptions().get(0).getId()).build();

		oldOption.getMiscInfo().setBookingToken(hotelBookOption.getBookingToken());
		for (RoomInfo room : oldOption.getRoomInfos()) {
			for (RoomInfo newroom : option.getRoomInfos()) {
				if (room.getMiscInfo().getRoomIndex().equals(newroom.getMiscInfo().getRoomIndex())) {
					room.getMiscInfo().setRoomBookingId(newroom.getMiscInfo().getRoomBookingId());
					room.getMiscInfo().setInclusive(newroom.getMiscInfo().getInclusive());
					newroom.setId(room.getId());
				}
			}
		}
		updatedOption = option;

	}

	private TravelbullzRequest getSearchRequest(Option option) {
		HotelBookOption hotelOption = HotelBookOption.builder().HotelOptionId(option.getMiscInfo().getHotelOptionId())
				.HotelRooms(gethotelroomslist(option)).build();
		Request request = Request.builder().SearchKey(searchkey).HotelOption(hotelOption).build();
		AdvancedOptions advancedOptions =
				AdvancedOptions.builder().Currency(TravelbullzConstant.CURRENCY.value).build();
		TravelbullzRequest searchRequest =
				TravelbullzRequest.builder().Token(supplierConf.getHotelSupplierCredentials().getPassword())
						.Request(request).AdvancedOptions(advancedOptions).build();
		return searchRequest;
	}
}
