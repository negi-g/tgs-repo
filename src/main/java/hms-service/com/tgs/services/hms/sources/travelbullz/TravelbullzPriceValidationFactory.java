package com.tgs.services.hms.sources.travelbullz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;

@Service

public class TravelbullzPriceValidationFactory extends AbstractHotelPriceValidationFactory{
	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public TravelbullzPriceValidationFactory(HotelSearchQuery query, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
		super(query, hotel, hotelSupplierConf, bookingId);
		
	}

	@Override
	public Option getOptionWithUpdatedPrice() throws Exception {
		TravelbullzPriceValidationService priceValidationService=TravelbullzPriceValidationService.builder()
		.searchQuery(this.searchQuery).bookingId(bookingId).moneyExchnageComm(moneyExchnageComm)
		.cacheHandler(cacheHandler).supplierConf(this.getSupplierConf()).sourceConfig(sourceConfigOutput)
		.build();
		priceValidationService.validate(this.getHInfo());
		return priceValidationService.getUpdatedOption();
	}

}
