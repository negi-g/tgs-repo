package com.tgs.services.hms.servicehandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.HibernateUtils;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import com.tgs.services.hms.datamodel.HotelStaticRegionInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfo;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import com.tgs.services.hms.restmodel.FetchHotelCityInfoResponse;
import com.tgs.services.hms.restmodel.FetchHotelRegionInfoResponse;
import com.tgs.services.hms.restmodel.HotelRegionInfoResponse;

@Service
public class HotelRegionInfoFetchHandler extends ServiceHandler<String, FetchHotelRegionInfoResponse> {
	
	private final static long LIMIT = 1000;
	
	@Autowired
	HotelRegionInfoService regionInfoService;
	
	@Override
	public void beforeProcess() throws Exception {
		if(StringUtils.isBlank(request))
			updateForFirstRequest();
		
	}

	private void updateForFirstRequest() {
		request = HibernateUtils.getNextDataSetCursorInfo(LIMIT, 0l);
	}
	
	
	@Override
	public void process() throws Exception {
		
		
		String next = null;
		long[] data = HibernateUtils.getDataFromRequest(request);
		List<DbHotelRegionInfo> dbRegions = regionInfoService.findByIdGreaterThanOrderByIdAscLimit(data[0], data[1]);
		List<HotelRegionInfo> regions = DbHotelRegionInfo.toDomainList(dbRegions);
		if (regions.size() == LIMIT) {
			Long lastAccessedId = regions.get(regions.size() - 1).getId();
			next = HibernateUtils.getNextDataSetCursorInfo(LIMIT, lastAccessedId);
		}
		response.setRegionInfoList(regions);
		response.setNext(next);
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public HotelRegionInfoResponse getRegionInfoById(String regionId) {
		HotelRegionInfoResponse regionInfoResponse = new HotelRegionInfoResponse();
		DbHotelRegionInfo regionInfo = regionInfoService.findById(Long.valueOf(regionId));
		if (Objects.isNull(regionInfo)) {
			throw new CustomGeneralException(SystemError.INVALID_HOTEL_REGION_ID);
		}
		regionInfoResponse.getRegionInfos().add(regionInfo.toDomain());
		return regionInfoResponse;
	}

	public FetchHotelCityInfoResponse convertRegionInfosIntoCityInfos(
			FetchHotelRegionInfoResponse fetchHotelRegionInfoResponse) {
		FetchHotelCityInfoResponse cityInfoResponse = new FetchHotelCityInfoResponse();
		List<HotelRegionInfo> regionInfos = fetchHotelRegionInfoResponse.getRegionInfoList();
		fetchHotelRegionInfoResponse.setRegionInfoList(null);
		List<CityInfo> cityInfos = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(regionInfos)) {

			for (HotelRegionInfo regionInfo : regionInfos) {
				CityInfo cityInfo = CityInfo.builder().id(regionInfo.getId()).cityName(regionInfo.getRegionName())
						.countryName(regionInfo.getCountryName()).type(regionInfo.getRegionType()).build();
				cityInfos.add(cityInfo);
			}
		}
		HotelStaticRegionInfo staticRegionInfo = HotelStaticRegionInfo.builder().cityInfoList(cityInfos)
				.next(fetchHotelRegionInfoResponse.getNext()).build();
		cityInfoResponse.setResponse(staticRegionInfo);
		return cityInfoResponse;
	}
}
