package com.tgs.services.hms.sources.tripadvisor;

import lombok.Getter;

@Getter
public enum TripAdvisorConstants {

	LOCATION_URL("http://api.tripadvisor.com/api/partner/2.0/location"),
	LOCATION_MAPPER_URL("http://api.tripadvisor.com/api/partner/2.0/location_mapper"),
	KEY("45530c19e2344d29bdfbbcfe9d32460d"),
	CATEGORY("hotels"),
	NAME("TRIP_ADVISOR"),
	SOURCE("1");
	
	TripAdvisorConstants(String value){
		this.value = value;
	}

	private String value;
}
