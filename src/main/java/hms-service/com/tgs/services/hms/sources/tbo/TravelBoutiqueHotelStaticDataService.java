package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.search.ArrayOfBasicPropertyInfo;
import com.tgs.services.hms.datamodel.tbo.search.ArrayOfBasicPropertyInfo.BasicPropertyInfo;
import com.tgs.services.hms.datamodel.tbo.search.HotelStaticDetailResponse;
import com.tgs.services.hms.datamodel.tbo.staticData.TBOStaticDataRequest;
import com.tgs.services.hms.datamodel.tbo.staticData.VendorMessages.VendorMessage;
import com.tgs.services.hms.datamodel.tbo.staticData.VendorMessages.VendorMessage.SubSection;
import com.tgs.services.hms.datamodel.tbo.staticData.VendorMessages.VendorMessage.SubSection.Paragraph;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
public class TravelBoutiqueHotelStaticDataService extends TravelBoutiqueBaseService {

	protected HotelInfoSaveManager hotelInfoSaveManager;
	private HotelStaticDataRequest staticDataRequest;
	protected HotelSupplierConfiguration supplierConf;
	private HotelRegionInfoMappingManager regionInfoMappingManager;
	private HotelSourceConfigOutput sourceConfigOutput;

	public void process() throws IOException {

		HotelStaticDataService staticDataService = (HotelStaticDataService) HotelUtils.getStaticDataService();
		int totalSupplierFetchedHotelCount = 0, totalPersistedHotelCount = 0;
		List<String> cityIds = new ArrayList<>();
		try {
			if (CollectionUtils.isNotEmpty(sourceConfigOutput.getCityList())) {
				cityIds = sourceConfigOutput.getCityList();
			} else {
				cityIds = getTBOCityIdList();
			}
			for (String city : cityIds) {
				int savedHotelCount = 0;
				List<HotelInfo> hInfoList = new ArrayList<>();
				try {
					hInfoList = getMasterHotelForCity(city);
					totalSupplierFetchedHotelCount += hInfoList.size();
					savedHotelCount = saveHotelInfoList(hInfoList, staticDataService);
					totalPersistedHotelCount += savedHotelCount;
				} catch (Exception e) {
					log.error("Error While Fetching Static Hotels For supplier TBO  City {}", city);
				} finally {
					log.info("Number Of Results For City {} from TBO is {}. Total Fetched Hotels {}", city,
							hInfoList.size(), totalSupplierFetchedHotelCount);
					log.info("Number Of Hotels saved for City {} is {}. Total Hotel Saved {}", city, savedHotelCount,
							totalPersistedHotelCount);
					hInfoList = null;
				}
			}
		} catch (Exception e) {
			log.error("Error While Fetching Static Hotels For supplier TBO");
		} finally {
			log.info("Total no of hotels fetched from Supplier are {} and persisted are {}. Total City Count {}",
					totalSupplierFetchedHotelCount, totalPersistedHotelCount, cityIds.size());
		}
	}

	private int saveHotelInfoList(List<HotelInfo> hInfoList, HotelStaticDataService staticDataService) {

		int hotelCount = 0;
		for (HotelInfo hInfo : hInfoList) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hInfo);
				dbHotelInfo.setId(null);
				dbHotelInfo.setSupplierName(HotelSourceType.TBO.name());
				dbHotelInfo.setSupplierHotelId(hInfo.getId());
				if (BooleanUtils.isTrue(staticDataRequest.getCacheStaticData())) {
					hotelInfoSaveManager.saveAndCacheHotelInfo(dbHotelInfo, staticDataService);
				} else {
					hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, staticDataService);
				}
				hotelCount++;
			} catch (Exception e) {
				log.error("Error While Storing Master Hotel With Name {} Rating {} {}", hInfo.getName(),
						hInfo.getRating(), e.getMessage());
			}
		}
		return hotelCount;
	}

	private List<HotelInfo> getMasterHotelForCity(String city) throws IOException {
		HotelSupplierRegionInfo supplierRegionInfo = HotelSupplierRegionInfo.builder().regionId(city).build();
		return fetchHotelFeedFromCityId(supplierRegionInfo);
	}

	private List<HotelInfo> fetchHotelFeedFromCityId(HotelSupplierRegionInfo supplierRegionInfo) throws IOException {

		TBOStaticDataRequest staticRequest = TBOStaticDataRequest.builder().CityId(supplierRegionInfo.getRegionId())
				.EndUserIp(TravelBoutiqueUtil.IP).ClientId(supplierConf.getHotelSupplierCredentials().getClientId())
				.TokenId(getCachedToken()).build();
		HttpUtils httpUtils = null;
		HotelStaticDetailResponse hotelDetailResponse = null;
		List<HotelInfo> list = new ArrayList<HotelInfo>();
		try {
			httpUtils = TravelBoutiqueUtil.getRequest(staticRequest, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_STATIC_DETAIL_DATA));
			httpUtils.setPrintResponseLog(false);
			hotelDetailResponse = httpUtils.getResponse(HotelStaticDetailResponse.class).orElse(null);
			list = getHotelDetailsReponse(hotelDetailResponse);
		} catch (Exception e) {
			log.error("Exception While fetching TBO Feed From City Id {}", supplierRegionInfo.getRegionId(), e);
		}
		return list;
	}

	private List<HotelInfo> getHotelDetailsReponse(HotelStaticDetailResponse hotelDetailResponse) {
		List<HotelInfo> hInfos = new ArrayList<HotelInfo>();
		if (hotelDetailResponse != null && hotelDetailResponse.getHotelData() != null) {
			String hotelData = hotelDetailResponse.getHotelData();
			StringReader reader = new StringReader(hotelData);
			JAXBContext jaxbContext;
			try {
				jaxbContext = JAXBContext.newInstance(ArrayOfBasicPropertyInfo.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				ArrayOfBasicPropertyInfo baseResponse = (ArrayOfBasicPropertyInfo) jaxbUnmarshaller.unmarshal(reader);
				if (baseResponse != null) {
					List<BasicPropertyInfo> basicPropertyInfoList = baseResponse.getBasicPropertyInfo();
					for (BasicPropertyInfo basicPropertyInfo : basicPropertyInfoList) {
						HotelInfo hInfo = convertToHInfo(basicPropertyInfo);
						if (hInfo != null) {
							hInfos.add(hInfo);
						}
					}
					log.info("Number Of Resultsis {}", basicPropertyInfoList.size());
				}
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return hInfos;
	}

	private HotelInfo convertToHInfo(BasicPropertyInfo basicPropertyInfo) {
		try {
			String id = Long.toString(basicPropertyInfo.getTboHotelCode());
			Address address = getHotelAddress(basicPropertyInfo);
			HotelInfo hotelInfo = HotelInfo.builder().id(id).name(basicPropertyInfo.getHotelName())
					.description(getHotelDescription(basicPropertyInfo)).images(getHotelImages(basicPropertyInfo))
					.rating(getHotelRating(basicPropertyInfo)).facilities(getHotelFacilities(basicPropertyInfo))
					.geolocation(getHotelLocation(basicPropertyInfo)).address(address)
					.countryName(StringUtils.upperCase(address.getCountry().getName()))
					.cityName(StringUtils.upperCase(address.getCity().getName())).build();
			return hotelInfo;
		} catch (Exception e) {
			log.error("Exception While converting {} to HInfo", basicPropertyInfo.getHotelName(), e);
		}
		return null;
	}

	private GeoLocation getHotelLocation(BasicPropertyInfo basicPropertyInfo) {
		GeoLocation geolocation = new GeoLocation();
		BigDecimal latitude = basicPropertyInfo.getPosition().getLatitude();
		BigDecimal longitude = basicPropertyInfo.getPosition().getLongitude();
		if (latitude != null) {
			geolocation.setLatitude(latitude.toString());
		}
		if (longitude != null) {
			geolocation.setLongitude(longitude.toString());
		}
		return geolocation;
	}

	private Integer getHotelRating(BasicPropertyInfo basicPropertyInfo) {
		short brandCode = basicPropertyInfo.getBrandCode();
		Short rating = new Short(brandCode);
		return rating.intValue();
	}

	private List<String> getHotelFacilities(BasicPropertyInfo basicPropertyInfo) {
		List<String> facilities = null;
		VendorMessage facilityVendorMessage =
				TravelBoutiqueUtil.getVendorMessageFromProperty(basicPropertyInfo, "Facilities");
		if (facilityVendorMessage != null) {
			facilities = facilityVendorMessage.getSubSection().stream()
					.map(subSection -> (String) subSection.getParagraph().get(0).getText().getContent().get(0))
					.collect(Collectors.toList());
		}
		return facilities;
	}

	private String getHotelDescription(BasicPropertyInfo basicPropertyInfo) {
		VendorMessage descriptionVendorMessage =
				TravelBoutiqueUtil.getVendorMessageFromProperty(basicPropertyInfo, "Hotel Description");
		String description = null;
		if (descriptionVendorMessage != null) {
			description = (String) descriptionVendorMessage.getSubSection().get(0).getParagraph().get(0).getText()
					.getContent().get(0);
		}
		return description;
	}

	private Address getHotelAddress(BasicPropertyInfo basicPropertyInfo) {
		return Address.builder().address(basicPropertyInfo.getAddress().getAddressLine().get(0))
				.addressLine1(basicPropertyInfo.getAddress().getAddressLine().get(0))
				.addressLine2(basicPropertyInfo.getAddress().getAddressLine().get(1))
				.cityName(basicPropertyInfo.getAddress().getCityName())
				.countryName(basicPropertyInfo.getAddress().getCountryName().getValue())
				.city(City.builder().name(basicPropertyInfo.getAddress().getCityName()).build())
				.country(Country.builder().name(basicPropertyInfo.getAddress().getCountryName().getValue()).build())
				.postalCode(Long.toString((basicPropertyInfo.getAddress().getPostalCode()))).build();
	}

	private List<Image> getHotelImages(BasicPropertyInfo basicPropertyInfo) {
		VendorMessage imagesVendorMessage =
				TravelBoutiqueUtil.getVendorMessageFromProperty(basicPropertyInfo, "Hotel Pictures");
		List<Image> hotelImages = new ArrayList<>();
		if (imagesVendorMessage != null) {
			List<SubSection> subSections = imagesVendorMessage.getSubSection();

			subSections.stream().forEach(section -> {
				Paragraph paragraphFull = section.getParagraph().stream()
						.filter(predicate -> predicate.getType().equalsIgnoreCase("FullImage")).findFirst()
						.orElse(null);
				Paragraph paragraphThumbail = section.getParagraph().stream()
						.filter(predicate -> predicate.getType().equalsIgnoreCase("Thumbnail")).findFirst()
						.orElse(null);
				String bigImage = null;
				String thumbnail = null;
				if (paragraphFull != null) {
					bigImage = paragraphFull.getUrl();
				}
				if (paragraphThumbail != null) {
					thumbnail = paragraphThumbail.getUrl();
				}

				Image hotelImage = Image.builder().bigURL(bigImage).thumbnail(thumbnail).build();
				hotelImages.add(hotelImage);
			});
		}
		return hotelImages;
	}

	private List<String> getTBOCityIdList() {
		if (CollectionUtils.isEmpty(staticDataRequest.getCityIds())) {
			List<HotelSupplierRegionInfo> supplierRegionInfos =
					regionInfoMappingManager.findBySupplierName(HotelSourceType.TBO.name());
			return supplierRegionInfos.stream().map(HotelSupplierRegionInfo::getRegionId).collect(Collectors.toList());
		} else {
			return staticDataRequest.getCityIds();
		}
	}
}
