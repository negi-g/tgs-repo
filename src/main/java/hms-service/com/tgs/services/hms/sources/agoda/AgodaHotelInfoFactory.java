package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.amazonaws.util.CollectionUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AgodaHotelInfoFactory extends AbstractHotelInfoFactory {

	public AgodaHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException, InterruptedException {
		if (StringUtils.isNotBlank(searchQuery.getSearchPreferences().getHotelId())
				&& Objects.isNull(searchQuery.getMiscInfo().getHotelWiseSearchInfo())) {
			log.info("Not able to search for searchid {} and supplier {} as hotel wise search info is not present",
					searchQuery.getSearchId(), supplierConf.getBasicInfo().getSupplierName());
			return;
		}
		Set<String> propertyIds = getPropertyIdsOfCity(HotelSourceType.AGODA.name());
		searchResult = getResultOfAllPropertyIds(propertyIds);
	}

	@Override
	protected HotelSearchResult doSearchForSupplier(Set<String> set, int threadCount)
			throws IOException, JAXBException {
		AgodaSearchService searchService = AgodaSearchService.builder().supplierConf(this.getSupplierConf())
				.searchQuery(this.getSearchQuery()).sourceConfigOutput(sourceConfigOutput).hotelIds(set).build();
		searchService.doSearch(threadCount);
		HotelSearchResult searchResult = (HotelSearchResult) searchService.getSearchResult();
		return searchResult;
	}


	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option option = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		hotel.getOptions().forEach(op -> {
			if (op.getId().equals(optionId)) {
				op.setCancellationPolicy(option.getCancellationPolicy());
			}
		});

	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException, JAXBException {

		Set<String> propertyIds = new HashSet<>();
		propertyIds.add(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		AgodaSearchService searchService =
				AgodaSearchService.builder().supplierConf(this.getSupplierConf()).searchQuery(this.getSearchQuery())
						.sourceConfigOutput(sourceConfigOutput).hotelIds(propertyIds).build();
		searchService.doDetailSearch();
		searchResult = (HotelSearchResult) searchService.getSearchResult();
		hInfo.setOptions(CollectionUtils.isNullOrEmpty(searchResult.getHotelInfos()) ? null
				: searchResult.getHotelInfos().get(0).getOptions());

	}

}
