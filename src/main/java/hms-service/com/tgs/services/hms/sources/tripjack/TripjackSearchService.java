package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchCriteria;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.HotelDetailRequest;
import com.tgs.services.hms.restmodel.HotelDetailResponse;
import com.tgs.services.hms.restmodel.HotelSearchQueryListResponse;
import com.tgs.services.hms.restmodel.HotelSearchRequest;
import com.tgs.services.hms.restmodel.HotelSearchResponse;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Service
@SuperBuilder
public class TripjackSearchService extends TripjackBaseService {

	private HotelSearchResult searchResult;
	private List<Integer> propertyIds;
	private HotelSearchResponse searchResponse;
	private HotelSearchQueryListResponse searchListResponse;
	private final static int MAX_SEARCH_HIT = 30;

	public void doSearch() throws IOException {

		searchResult = HotelSearchResult.builder().build();
		HttpUtils httpUtils = null;
		String requestUrl = StringUtils.join(endpoint, TripjackConstant.SEARCH_LIST.value);
		listener = new RestAPIListener("");
		HotelSearchRequest searchRequest = getTripjackSearchRequest();
		httpUtils = getHttpUtils(GsonUtils.getGson().toJson(searchRequest), requestUrl);
		searchListResponse = httpUtils.getResponse(HotelSearchQueryListResponse.class).orElse(null);
		try {
			if (searchListResponse.getStatus().getHttpStatus().equals(HttpURLConnection.HTTP_OK)
					&& searchListResponse.getSearchIds() != null) {
				doSearchFromSearchList();
			} else {
				log.error("unable to fetch search id from tripjack");
			}
		} finally {
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(searchListResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRIPJACK.name())
					.requestType(BaseHotelConstants.SEARCH).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId()).prefix("Search-List")
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

		}
	}

	private void doSearchFromSearchList() throws IOException {
		HotelSearchRequest search = new HotelSearchRequest();
		HttpUtils httpUtils = null;
		search.setSearchId(searchListResponse.getSearchIds().get(0));
		try {
			String searchRequestUrl = StringUtils.join(endpoint, TripjackConstant.SEARCH.value);
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(search), searchRequestUrl);
			searchResponse = httpUtils.getResponse(HotelSearchResponse.class).orElse(null);
			int index = 0;
			if (searchResponse.getStatus().getHttpStatus().equals(200)) {
				while (searchResponse.getRetryInSecond() != null && index < MAX_SEARCH_HIT) {
					try {
						Thread.sleep(2 * 1000);
					} catch (InterruptedException e) {
						log.error("error while searching hotel list {}", e);
					}
					httpUtils = getHttpUtils(GsonUtils.getGson().toJson(search), searchRequestUrl);
					searchResponse = httpUtils.getResponse(HotelSearchResponse.class).orElse(null);
					index++;
				}
				if (searchResponse.getSearchResult() != null
						&& CollectionUtils.isNotEmpty(searchResponse.getSearchResult().getHotelInfos())) {
					searchResult.setHotelInfos(
							getHotelListFromTripjackResult(searchResponse.getSearchResult().getHotelInfos()));
				}
			} else {
				log.error("unable to fetch search list response from tripjack");
			}
		} finally {
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(searchResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRIPJACK.name())
					.requestType(BaseHotelConstants.SEARCH).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}

	}

	private HotelSearchRequest getTripjackSearchRequest() {

		HotelSearchRequest searchRequest = new HotelSearchRequest();
		List<RoomSearchInfo> roomSearchInfoList = new ArrayList<>();
		searchQuery.getRoomInfo().forEach(roomInfo -> {
			RoomSearchInfo roomSearchInfo = RoomSearchInfo.builder().numberOfAdults(roomInfo.getNumberOfAdults())
					.numberOfChild(roomInfo.getNumberOfChild()).build();
			if (roomInfo.getNumberOfChild() != null && roomInfo.getNumberOfChild() > 0) {
				roomSearchInfo.setChildAge(roomInfo.getChildAge());
			}
			roomSearchInfoList.add(roomSearchInfo);
		});
		HotelSearchCriteria searchCriteria = new HotelSearchCriteria();
		searchCriteria.setRegionId(supplierRegionInfo.getRegionId());
		searchCriteria.setCountryId(supplierRegionInfo.getCountryId());
		searchCriteria.setNationality(searchQuery.getSearchCriteria().getNationality());
		searchCriteria.setCountryOfResidence(searchQuery.getSearchCriteria().getCountryOfResidence());
		HotelSearchQuery searchReqQuery = HotelSearchQuery.builder().checkinDate(searchQuery.getCheckinDate())
				.checkoutDate(searchQuery.getCheckoutDate()).searchPreferences(searchQuery.getSearchPreferences())
				.roomInfo(roomSearchInfoList).searchCriteria(searchCriteria).build();
		searchRequest.setSearchQuery(searchReqQuery);
		searchRequest.setSync(false);
		return searchRequest;
	}

	public void doDetailSearch(HotelInfo hInfo) throws IOException {
		HttpUtils httpUtils = null;
		HotelDetailRequest searchDetailRequest = new HotelDetailRequest();
		searchDetailRequest.setId(hInfo.getMiscInfo().getCorrelationId());
		String detailSearchUrl = StringUtils.join(endpoint, TripjackConstant.DETAIL_SEARCH.value);
		listener = new RestAPIListener("");
		try {
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(searchDetailRequest), detailSearchUrl);
			HotelDetailResponse searchDetailRespone = httpUtils.getResponse(HotelDetailResponse.class).orElse(null);
			createDetailSearchResult(searchDetailRespone, hInfo);
		} finally {
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(searchResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRIPJACK.name())
					.requestType(BaseHotelConstants.DETAILSEARCH).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(hInfo.getMiscInfo().getSearchId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
	}

	private void createDetailSearchResult(HotelDetailResponse searchDetailRespone, HotelInfo hInfo) {

		if (searchDetailRespone == null)
			return;
		if (searchDetailRespone.getStatus().getHttpStatus().equals(HttpURLConnection.HTTP_OK)
				&& searchDetailRespone.getHotel() != null) {
			HotelInfo hotel = searchDetailRespone.getHotel();
			hInfo.setImages(hotel.getImages());
			hInfo.setDescription(hotel.getDescription());
			hInfo.setFacilities(hotel.getFacilities());
			List<Option> optionList = createOptionFromHotel(searchDetailRespone.getHotel());
			hInfo.setOptions(optionList);
		} else {
			log.error("unable to get detail search response from tripjack, status code {}  due to {}",
					searchDetailRespone.getStatus().getHttpStatus(), searchDetailRespone.getErrorMessage());
		}

	}
}
