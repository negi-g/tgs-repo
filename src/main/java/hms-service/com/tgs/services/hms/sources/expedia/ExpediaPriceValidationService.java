package com.tgs.services.hms.sources.expedia;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionLinkType;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.expedia.PriceCheckResponse;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ExpediaPriceValidationService {

	private HotelSearchQuery searchQuery;
	private Option option;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConfigOutput;
	private String bookingId;
	private Option updatedOption;
	private RestAPIListener listener;
	private LocalDateTime currentTime;

	public void fetchPriceChanges() throws Exception {
		HttpUtilsV2 httpUtils = null;
		PriceCheckResponse priceCheckResponse = null;
		try {
			listener = new RestAPIListener("");
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Authorization", ExpediaUtils.getAuthorizationSignature(supplierConf));
			headerParam.put("Accept", "application/json");
			httpUtils = HttpUtilsV2.builder().headerParams(headerParam)
					.urlString(supplierConf.getHotelSupplierCredentials().getUrl()
							+ option.getMiscInfo().getLinks().get(OptionLinkType.PRICE_CHECK))
					.build();
			httpUtils.setPrintResponseLog(true);
			httpUtils.getResponse(PriceCheckResponse.class);
			priceCheckResponse =
					GsonUtils.getGson().fromJson(httpUtils.getResponseString(), PriceCheckResponse.class);
			if (CollectionUtils.isNotEmpty(priceCheckResponse.getFields())) {
				throw new CustomGeneralException("Unable to retreive info from price check api for booking id "
						+ bookingId + "due to " + GsonUtils.getGson().toJson(priceCheckResponse));
			} else {
				updatedOption = createUpdatedOption(priceCheckResponse);
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.REVIEW.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(priceCheckResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.EXPEDIA.name())
						.requestType(BaseHotelConstants.PRICE_CHECK).responseString(httpUtils.getResponseString())
						.urlString(httpUtils.getUrlString()).postData(httpUtils.getPostData()).logKey(bookingId)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Option createUpdatedOption(PriceCheckResponse priceCheckResponse) {
		if (priceCheckResponse.getStatus().equals("sold_out")) {
			return null;
		}
		/*
		 * In available status, price are getting changed too
		 * 
		 * else if (priceCheckResponse.getStatus().equals("available")) {
		 * option.getMiscInfo().getLinks().put(OptionLinkType.ITINERARY,
		 * priceCheckResponse.getLinks().get("book").getHref()); return option; }
		 */
		else {
			MultiMap occupancies = ExpediaUtils.getOccupancies(searchQuery);
			Map<String, Integer> occupancyMap =
					ExpediaUtils.getOccupancyMap((ArrayList<String>) occupancies.get("occupancy"));
			Option newOption = Option.builder().build();
			newOption.setId(option.getId());

			ExpediaUtils.setRoomPricing(newOption, null, priceCheckResponse.getOccupancy_pricing(), occupancyMap);
			ExpediaUtils.updatePriceWithClientCommissionComponents(newOption, sourceConfigOutput);
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				String occupancyPattern = roomInfo.getOccupancyPattern();
				for (RoomInfo updatedRoomInfo : newOption.getRoomInfos()) {
					if (StringUtils.isBlank(updatedRoomInfo.getId())
							&& occupancyPattern.equals(updatedRoomInfo.getOccupancyPattern())) {
						updatedRoomInfo.setId(roomInfo.getId());
						updatedRoomInfo.setMealBasis(roomInfo.getMealBasis());
						break;
					}
				}
			}
			option.getMiscInfo().getLinks().put(OptionLinkType.ITINERARY,
					priceCheckResponse.getLinks().get("book").getHref());
			return newOption;
		}
	}
}
