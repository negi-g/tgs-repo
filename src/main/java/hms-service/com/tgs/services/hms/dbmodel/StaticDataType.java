package com.tgs.services.hms.dbmodel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.UserReviewStaticData;

public class StaticDataType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return UserReviewStaticData.class;
	}

}
