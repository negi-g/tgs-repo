package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.travelbullz.AdvancedOptions;
import com.tgs.services.hms.datamodel.travelbullz.BookRQ;
import com.tgs.services.hms.datamodel.travelbullz.BookingDetailRQ;
import com.tgs.services.hms.datamodel.travelbullz.BookingDetailResponse;
import com.tgs.services.hms.datamodel.travelbullz.HotelRoom;
import com.tgs.services.hms.datamodel.travelbullz.Request;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzBookResponse;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@SuperBuilder
@Service
@Slf4j
public class TravelbullzBookingService extends TravelbullzBaseService {
	private Order order;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelInfo hInfo;
	private HotelOrderItemCommunicator itemComm;
	private TravelbullzBookResponse bookingResponse;
	private BookingDetailResponse bookingDetailResponse;

	public boolean book() throws IOException {
		listener = new RestAPIListener("");
		HttpUtilsV2 httpUtils = null;
		boolean isBookingSuccessful = false;
		try {
			TravelbullzRequest searchRequest = getBookingRequest();
			String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
			String urlString = StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.BOOK_URL));
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(searchRequest), urlString);
			bookingResponse = httpUtils.getResponse(TravelbullzBookResponse.class).orElse(null);
			if (Objects.nonNull(bookingResponse.getBookRS().getBookingId())) {
				isBookingSuccessful = updateBookingStatus(bookingResponse);
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.BOOK.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(bookingResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRAVELBULLZ.name())
						.requestType(BaseHotelConstants.BOOKING).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(order.getBookingId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
		return isBookingSuccessful;
	}

	private boolean updateBookingStatus(TravelbullzBookResponse bookingResponse) throws IOException {

		hInfo.getMiscInfo().setSupplierBookingReference(bookingResponse.getBookRS().getReferenceNo());
		hInfo.getMiscInfo().setSupplierBookingId(String.valueOf(bookingResponse.getBookRS().getBookingId()));

		return checkStatus();
	}

	private boolean checkStatus() throws IOException {
		HttpUtilsV2 httpUtils = null;
		try {
			TravelbullzRequest searchRequest = getConfirmBookingRequest();
			String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
			String urlString =
					StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.CONFIRM_BOOKING_URL));
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(searchRequest), urlString);
			bookingDetailResponse = httpUtils.getResponse(BookingDetailResponse.class).orElse(null);
			if (Objects.nonNull(bookingDetailResponse) && ObjectUtils.isEmpty(bookingDetailResponse.getError())) {
				List<HotelRoom> hotelResponselist =
						bookingDetailResponse.BookingDetailRS.getHotelOption().getHotelRooms();
				for (HotelRoom room : hotelResponselist) {
					if (!room.getBookingStatus().equalsIgnoreCase("Confirmed")
							&& !(room.getBookingStatus().equalsIgnoreCase("Vouchered")))
						return false;
				}
				return true;
			}
			return false;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(bookingDetailResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRAVELBULLZ.name())
					.requestType(BaseHotelConstants.RETRIEVE_BOOKING).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
	}

	private TravelbullzRequest getConfirmBookingRequest() {
		BookingDetailRQ bookingDetailRQ =
				BookingDetailRQ.builder().BookingId(String.valueOf(bookingResponse.getBookRS().getBookingId()))
						.ReferenceNo(bookingResponse.getBookRS().getReferenceNo())
						.InternalReference(order.getBookingId()).build();
		Request request = Request.builder().BookingDetailRQ(bookingDetailRQ).build();
		AdvancedOptions advancedOptions =
				AdvancedOptions.builder().Currency(TravelbullzConstant.CURRENCY.value).build();
		TravelbullzRequest searchRequest =
				TravelbullzRequest.builder().Token(supplierConf.getHotelSupplierCredentials().getPassword())
						.Request(request).AdvancedOptions(advancedOptions).build();

		return searchRequest;
	}

	private TravelbullzRequest getBookingRequest() {
		List<HotelRoom> rooms = getRooms(hInfo.getOptions().get(0).getRoomInfos());
		double totalBookingPrice = hInfo.getOptions().get(0).getRoomInfos().stream()
				.mapToDouble(room -> room.getMiscInfo().getInclusive()).sum();
		BookRQ bookRQ = BookRQ.builder().BookingToken(hInfo.getOptions().get(0).getMiscInfo().getBookingToken())
				.TotalPrice(totalBookingPrice).InternalReference(order.getBookingId()).HotelRooms(rooms).build();
		Request request = Request.builder().SearchKey(hInfo.getOptions().get(0).getMiscInfo().getSearchKey())
				.BookRQ(bookRQ).build();
		AdvancedOptions advancedOptions =
				AdvancedOptions.builder().Currency(TravelbullzConstant.CURRENCY.value).build();
		TravelbullzRequest searchRequest =
				TravelbullzRequest.builder().Token(supplierConf.getHotelSupplierCredentials().getPassword())
						.Request(request).AdvancedOptions(advancedOptions).build();

		return searchRequest;

	}

	private List<HotelRoom> getRooms(List<RoomInfo> list) {
		List<HotelRoom> hotelRoomsList = new ArrayList<>();
		int index = 0;
		for (RoomInfo roomInfo : list) {
			List<TravellerInfo> travellerInfos = roomInfo.getTravellerInfo();
			for (TravellerInfo traveller : travellerInfos) {
				HotelRoom room = HotelRoom.builder()
						.UniqueId(Integer.parseInt(roomInfo.getMiscInfo().getRoomBookingId()))
						.IsLead(index == 0 ? 1 + "" : 0 + "")
						.RoomNo(String.valueOf(roomInfo.getMiscInfo().getRoomIndex()))
						.Prefix(traveller.getTitle().equalsIgnoreCase("master") ? "Mr."
								: TravelbullzPrefixMapping.valueOf(traveller.getTitle().toUpperCase()).getTitle())
						.PaxType(traveller.getPaxType().name()).FirstName(traveller.getFirstName())
						.LastName(traveller.getLastName()).ChildAge(getAge(traveller)).build();
				index++;
				hotelRoomsList.add(room);
			}
		}
		return hotelRoomsList;
	}

	private String getAge(TravellerInfo traveller) {
		PaxType paxType = traveller.getPaxType();
		if (paxType.getCode().equals("A"))
			return "0";
		return String.valueOf(traveller.getAge());
	}
}
