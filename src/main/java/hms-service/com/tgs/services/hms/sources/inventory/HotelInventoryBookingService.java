package com.tgs.services.hms.sources.inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import com.tgs.filters.HotelRatePlanFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryAllocationInfo;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.manager.HotelRatePlanManager;
import com.tgs.services.oms.datamodel.Order;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class HotelInventoryBookingService extends HotelInventoryBaseService{
	
	private Order order;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelInfo hInfo;
	private HotelRatePlanManager ratePlanManager;
	
	
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public boolean book() {
		
		Option option = hInfo.getOptions().get(0);
		if(!isSupplierEnabled(option.getRoomInfos().get(0).getMiscInfo().getSecondarySupplier())) 
			throw new CustomGeneralException(SystemError.SUPPLIER_NOT_FOUND);
		
		List<Long> ratePlanIdList = getRatePlanIdListFromOption(option);
		HotelRatePlanFilter ratePlanFilter = HotelRatePlanFilter.builder().ids(ratePlanIdList).build();
		List<HotelRatePlan> ratePlanList = ratePlanManager.fetchHotelRatePlan(ratePlanFilter);
		
		validate(ratePlanList);
		
		Map<Long,Long> ratePlanIdAsKeyCountAsValue = ratePlanIdList.stream().collect(Collectors
				.groupingBy(Function.identity(), Collectors.counting()));
		
		Map<Long, HotelRatePlan> ratePlanIdAsKeyRatePlanValue = ratePlanList.stream().collect(Collectors
				.toMap(HotelRatePlan :: getId, Function.identity(), (a1, a2) -> a1));
		
		List<HotelRatePlan> updatedRatePlanList = new ArrayList<>();
		
		for(Long key : ratePlanIdAsKeyRatePlanValue.keySet()) {
			HotelRatePlan ratePlan = ratePlanIdAsKeyRatePlanValue.get(key);
			Integer inventoryToBeBooked = ratePlanIdAsKeyCountAsValue.get(key).intValue();
			HotelInventoryAllocationInfo inventoryInfo = ratePlan.getInventoryAllocationInfo();
			
			if(ObjectUtils.isEmpty(inventoryInfo)) 
				throw new CustomGeneralException(SystemError.INVENTORY_INFO_NOT_FOUND);
			
			Integer totalInventory = inventoryInfo.getTotalInventory();
			Integer soldInventory = inventoryInfo.getSoldInventory();
			
			if(totalInventory-(soldInventory + inventoryToBeBooked) < 0) 
				throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
			
			soldInventory += inventoryToBeBooked;
			ratePlan.getInventoryAllocationInfo().setSoldInventory(soldInventory);
			updatedRatePlanList.add(ratePlan);
		}
		
		hInfo.getMiscInfo().setSupplierBookingId(order.getBookingId());
		hInfo.getMiscInfo().setSupplierBookingReference(order.getBookingId());
		ratePlanManager.storeRatePlanList(updatedRatePlanList);
		return true;
	}

	private void validate(List<HotelRatePlan> ratePlanList) {
		
		if(CollectionUtils.isEmpty(ratePlanList)) 
			throw new CustomGeneralException(SystemError.RATE_PLAN_NOT_FOUND);
		
		int ratePlanListSize = ratePlanList.size();
		filterRatePlanList(ratePlanList);
		
		if(ratePlanList.size()!=ratePlanListSize) 
			throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
		
		List<Option> optionList = fetchOptionListForHotel(ratePlanList);
		
		Option reviewedOption = hInfo.getOptions().get(0);
		Option updatedOption = optionList.get(0);
		checkIfOptionInfoSame(reviewedOption, updatedOption);
		
	}

	private void checkIfOptionInfoSame(Option option, Option updatedOption) {
		
		Double oldPrice = option.getRoomInfos().stream().map(ri -> ri.getTotalFareComponents().get(HotelFareComponent.BF))
				.reduce(Double :: sum).orElseGet(null);
		Double updatedPrice = updatedOption.getRoomInfos().stream().map(ri -> ri.getTotalFareComponents().get(HotelFareComponent.BF))
				.reduce(Double :: sum).orElseGet(null);
		
		if(oldPrice == null || updatedPrice == null) {
			log.error("OldOption or UpdatedOption Price is null {} {} for bookingId {}", oldPrice, updatedPrice, order.getBookingId());
			throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
		}
		
		if(oldPrice.compareTo(updatedPrice) != 0)
			throw new CustomGeneralException(SystemError.PRICE_MISMATCH_ERROR);
		
		/*
		 * Further checks can be added 
		 */
	}

	private List<Long> getRatePlanIdListFromOption(Option option) {
		
		List<Long> ratePlanIdList = new ArrayList<>();
		option.getRoomInfos().forEach((room -> {
			ratePlanIdList.addAll(room.getMiscInfo().getRatePlanIdList());
		}));
		return ratePlanIdList;
	}
	
}
