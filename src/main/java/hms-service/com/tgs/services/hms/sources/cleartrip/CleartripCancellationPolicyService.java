package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.cleartrip.CleartripCancellationPolicyRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripCancellationPolicyResponse;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Getter
public class CleartripCancellationPolicyService {

	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelSupplierConfiguration supplierConf;
	private HotelSearchQuery searchQuery;
	private HotelInfo hInfo;
	private String searchId;
	private LocalDateTime deadlineDateTime;
	private HotelCancellationPolicy cancellationPolicy;

	private static final DateTimeFormatter dateTimeFormatter_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final String HOTEL_CANCELLATION_PREFIX = "/hotels/api/v2/get-policy";

	public void searchCancellationPolicy(String logKey) throws IOException {
		HttpUtilsV2 httpUtils = null;
		RestAPIListener listener = new RestAPIListener("");
		CleartripCancellationPolicyRequest cancellationPolicyRequest = null;
		CleartripCancellationPolicyResponse cancellationPolicyResponse = null;
		try {
			cancellationPolicyRequest = createCancellationPolicyRequest();
			httpUtils = CleartripUtils.getResponseURLWithRequestBody(cancellationPolicyRequest, supplierConf);
			cancellationPolicyResponse = httpUtils.getResponse(CleartripCancellationPolicyResponse.class).orElse(null);

			createCancellationPolicyResponse(cancellationPolicyResponse);
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.CANCELLATION.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(cancellationPolicyResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.CLEARTRIP.name())
						.requestType(BaseHotelConstants.CANCELLATIONPOLICY)
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(logKey)
						.additionalinfo(hInfo.getOptions().get(0).getId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private void createCancellationPolicyResponse(CleartripCancellationPolicyResponse response) {

		boolean isError = false;
		if (response == null) {
			log.error("Cancellation policy is empty for searchid {} and hotelid {}", searchQuery.getSearchId(),
					hInfo.getId());
			SystemContextHolder.getContextData().getErrorMessages().add("Cancellation policy is empty");
			isError = true;
		}

		if (!ObjectUtils.isEmpty(response.getError())) {
			log.error("Unable to fetch cancellation policy for search id {} and hotelid {} due to {}",
					searchQuery.getSearchId(), hInfo.getId(), response.getError().getErrorMessage());
			SystemContextHolder.getContextData().getErrorMessages().add(response.getError().getErrorMessage());
			isError = true;
		}

		if (!ObjectUtils.isEmpty(response.getSuccess())
				&& !CleartripUtils.isValidCancellationPolicyType(response.getSuccess().getRefundable())) {

			log.error("Unknown cancellation policy type for search id {}, optionid {}, hotelid {}",
					searchQuery.getSearchId(), hInfo.getOptions().get(0).getId(), hInfo.getId(), response.getSuccess());
			SystemContextHolder.getContextData().getErrorMessages().add("Unknown cancellation policy type");
			isError = true;
		}

		if (isError) {
			hInfo.getOptions().get(0).setCancellationPolicy(null);
			return;
		}
		CleartripUtils.setCancellationPolicyInOptionNew(hInfo.getOptions().get(0), response.getSuccess(),
				sourceConfigOutput, searchQuery);
	}

	private CleartripCancellationPolicyRequest createCancellationPolicyRequest() {

		Option option = hInfo.getOptions().get(0);
		return CleartripCancellationPolicyRequest.builder()
				.checkInDate(searchQuery.getCheckinDate().format(dateTimeFormatter_YYYY_MM_DD))
				.checkOutDate(searchQuery.getCheckoutDate().format(dateTimeFormatter_YYYY_MM_DD))
				.roomTypeCode(option.getMiscInfo().getRoomTypeCode()).hotelId(option.getMiscInfo().getSupplierHotelId())
				.bookingCode(option.getMiscInfo().getBookingCode()).nri("false").suffixOfURL(HOTEL_CANCELLATION_PREFIX)
				.occupancy(CleartripUtils.createOccupancy(searchQuery)).build();
	}
}
