package com.tgs.services.hms.jparepository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tgs.services.hms.dbmodel.DbHotelSupplierRegionInfo;

public interface DbHotelSupplierRegionInfoRepository
		extends JpaRepository<DbHotelSupplierRegionInfo, Long>, JpaSpecificationExecutor<DbHotelSupplierRegionInfo> {

	DbHotelSupplierRegionInfo findById(Long id);

	DbHotelSupplierRegionInfo findByRegionNameAndStateNameAndCountryNameAndSupplierNameAndRegionTypeAndFullRegionName(
			String regionName, String stateName, String countryName, String supplierName, String regionType,
			String fullRegionName);

	DbHotelSupplierRegionInfo findByRegionNameAndStateNameAndCountryNameAndSupplierNameAndRegionType(String regionName,
			String stateName, String countryName, String supplierName, String regionType);

	DbHotelSupplierRegionInfo findByRegionNameAndCountryNameAndSupplierName(String regionName, String countryName,
			String supplierName);

	DbHotelSupplierRegionInfo findByStateNameAndCountryNameAndSupplierName(String regionName, String countryName,
			String supplierName);

	List<DbHotelSupplierRegionInfo> findByRegionNameAndSupplierName(String regionName, String supplierName);

	List<DbHotelSupplierRegionInfo> findByRegionNameContainingIgnoreCaseAndSupplierName(String regionName,
			String supplierName);

	List<DbHotelSupplierRegionInfo> findByRegionIdAndSupplierName(String regionId, String supplierName);

	List<DbHotelSupplierRegionInfo> findByStateIdAndSupplierName(String stateId, String supplierName);

	List<DbHotelSupplierRegionInfo> findByStateNameAndSupplierName(String stateName, String supplierName);

	List<DbHotelSupplierRegionInfo> findByStateNameContainingIgnoreCaseAndSupplierName(String stateName,
			String supplierName);

	List<DbHotelSupplierRegionInfo> findByCountryNameAndSupplierName(String countryName, String supplierName);

	List<DbHotelSupplierRegionInfo> findByCountryNameContainingIgnoreCaseAndSupplierName(String countryName,
			String supplierName);

	List<DbHotelSupplierRegionInfo> findByCountryIdAndSupplierName(String countryId, String supplierNName);

	Page<DbHotelSupplierRegionInfo> findBySupplierName(String supplierName, Pageable pageable);
}
