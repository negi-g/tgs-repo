package com.tgs.services.hms.servicehandler;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.gson.FieldExclusionStrategy;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSearch;
import com.tgs.services.hms.manager.HotelSearchManager;
import com.tgs.services.hms.restmodel.HotelSearchRequest;
import com.tgs.services.hms.restmodel.HotelSearchResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelSearchHandler extends ServiceHandler<HotelSearchRequest, HotelSearchResponse> {

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	HotelSearchManager searchManager;

	@Autowired
	HotelSearch hotelSearch;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	GeneralServiceCommunicator gmsComm;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void process() throws Exception {

		HotelSearchResult searchResult = HotelSearchResult.builder().build();
		int hitCount = 0;
		boolean isSearchCompleted = false;
		SystemContextHolder.getContextData().getReqIds().add(request.getSearchId());
		if (SystemContextHolder.getChannelType().equals(ChannelType.API)) {
			do {
				searchResult = search();
				hitCount++;
				isSearchCompleted = cacheHandler.isSearchCompleted(request.getSearchId());
				if (!isSearchCompleted)
					Thread.sleep(2000);
				else
					response.setRetryInSecond(null);
			} while (!isSearchCompleted && hitCount < 30);
		} else {
			searchResult = search();
		}
		if (!ObjectUtils.isEmpty(searchResult) && CollectionUtils.isEmpty(searchResult.getHotelInfos())) {
			log.info("empty hotel list for search id {}", searchResult.getSearchId());
		}
		response.setSearchResult(searchResult);
	}

	private boolean updateRetryInResponse() {

		ClientGeneralInfo clientInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		Integer searchRequestTimeout =
				ObjectUtils.isEmpty(clientInfo.getSearchTimeoutInSecond()) ? 60 : clientInfo.getSearchTimeoutInSecond();
		Integer searchRetryInSecond =
				ObjectUtils.isEmpty(clientInfo.getSearchRetryInSecond()) ? 2 : clientInfo.getSearchRetryInSecond();

		LocalDateTime searchStartAt = cacheHandler.getSearchStartAt(request.getSearchId());
		long timeInSec = ChronoUnit.SECONDS.between(searchStartAt, LocalDateTime.now());
		if (timeInSec < searchRequestTimeout) {
			response.setRetryInSecond(searchRetryInSecond);
			return true;
		}
		return false;
	}

	private HotelSearchResult search() {
		HotelSearchResult searchResult = HotelSearchResult.builder().build();

		try {
			int searchCountReturned = cacheHandler.getSearchCountReturned(request.getSearchId());
			boolean isSearchCompleted = cacheHandler.isSearchCompleted(request.getSearchId());
			searchResult = cacheHandler.getSearchResult(request.getSearchId());
			if (searchResult != null) {
				cacheHandler.storeSearchCountReturned(request.getSearchId(), searchResult.getNoOfHotelOptions());
			}
			if (!isSearchCompleted) {
				boolean isUpdated = updateRetryInResponse();
				if (isUpdated && searchResult != null && searchCountReturned == searchResult.getNoOfHotelOptions()) {
					log.info("Retry response updated for search id {} and search count is {}", request.getSearchId(),
							searchCountReturned);
					/**
					 * This is to ensure that we don't return same hotel result again.
					 */
					searchResult = null;
				}
			} else {
				log.info("Search completed for search id {} and search count {}", request.getSearchId(),
						searchCountReturned);
			}
			SystemContextHolder.getContextData()
					.setExclusionStrategys(new ArrayList<>(Arrays.asList(new FieldExclusionStrategy(null,
							new ArrayList<>(Arrays.asList("ops", "mi", "des", "hwsi", "hoc"))))));
		} catch (Exception cause) {
			log.debug("Unable to get hotel from cache {}", cause);
		}
		return searchResult;
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
