package com.tgs.services.hms.sources.agoda;

import java.util.HashMap;
import java.util.Map;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class AgodaNamespacePrefixMapper extends NamespacePrefixMapper{

	private Map<String, String> namespaceMap = new HashMap<>();
	
	public AgodaNamespacePrefixMapper() {
		//namespaceMap.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");
	}
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		return namespaceMap.getOrDefault(namespaceUri, suggestion);
	}
	
	
	

}
