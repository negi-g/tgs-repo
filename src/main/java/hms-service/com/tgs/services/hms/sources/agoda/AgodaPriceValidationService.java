package com.tgs.services.hms.sources.agoda;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.agoda.precheck.HotelRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.PrecheckDetailsRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.PrecheckRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.PrecheckResponse;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Getter
@Setter
@Slf4j
@SuperBuilder
public class AgodaPriceValidationService extends AgodaBaseService {

	private String bookingId;

	public void validate(HotelInfo hInfo) throws Exception {
		HttpUtils httpUtils = null;
		PrecheckResponse result = null;
		try {
			listener = new RestAPIListener("");
			PrecheckRequest preCheckRequest = createPreCheckRequest(hInfo);
			String xmlRequest = AgodaMarshaller.marshallXml(preCheckRequest);

			httpUtils = AgodaUtil.getHttpUtils(xmlRequest, HotelUrlConstants.BLOCK_ROOM_URL, null, supplierConf);
			log.info("Price Validation Request is {}", httpUtils.getPostData());
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (xmlResponse == null) {
				log.error("Unable to get response {}", preCheckRequest, xmlResponse);
			}
			result = AgodaMarshaller.unmarshallPreCheckResponse(xmlResponse);
			if (!(result.getStatus().equals(200))) {
				log.error(result.getMessage());
				throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.REVIEW.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(result)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.AGODA.name())
						.headerParams(httpUtils.getHeaderParams()).requestType(BaseHotelConstants.PRICE_CHECK)
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(bookingId)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}

	}

	private PrecheckRequest createPreCheckRequest(HotelInfo hInfo) {

		PrecheckRequest precheckRequest = new PrecheckRequest();
		HotelRequest hotel = new HotelRequest();
		String supplierHotelId = hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId();
		if (StringUtils.isBlank(supplierHotelId)) {
			log.info("agoda supplier hotel is blank for booking{}", bookingId);
		}
		hotel.setId(supplierHotelId);
		setRooms(hotel, hInfo);
		precheckRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		precheckRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		PrecheckDetailsRequest precheckDetailRequest = new PrecheckDetailsRequest();
		precheckDetailRequest.setSearchid(hInfo.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		precheckDetailRequest.setTag(bookingId);
		precheckDetailRequest.setAllowDuplication(true);
		precheckDetailRequest.setUserCountry("IN");
		precheckDetailRequest.setCheckIn(searchQuery.getCheckinDate().toString());
		precheckDetailRequest.setCheckOut(searchQuery.getCheckoutDate().toString());
		precheckDetailRequest.setHotel(hotel);
		precheckRequest.setPrecheckDetails(precheckDetailRequest);
		return precheckRequest;
	}


}
