package com.tgs.services.hms.sources.vervotech;

import com.tgs.services.hms.helper.HotelSourceType;
import lombok.Getter;


@Getter
public enum VervotechSupplierMapping {

	EXPEDIA {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.EXPEDIA;
		}

		@Override
		public String getVervotechSupplierName() {
			return "EAN";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJEXPEDIA";
		}
	},
	AGODA {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.AGODA;
		}

		@Override
		public String getVervotechSupplierName() {
			return "Agoda";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJAGODA";
		}
	},
	CLEARTRIP {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.CLEARTRIP;
		}

		@Override
		public String getVervotechSupplierName() {
			return "CleartripAPI";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJCLERTRIP";
		}

	},
	HOTELBEDS {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.HOTELBEDS;
		}

		@Override
		public String getVervotechSupplierName() {
			return "HotelBeds";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJHOTELBEDS";
		}
	},
	FITRUUMS {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.FITRUUMS;
		}

		@Override
		public String getVervotechSupplierName() {
			return "Fitruums";
		}

	},
	TRAVELBULLZ {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.TRAVELBULLZ;
		}

		@Override
		public String getVervotechSupplierName() {
			return "TravelBullzInternal";
		}

	},
	DOTW {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.DOTW;
		}

		@Override
		public String getVervotechSupplierName() {
			return "DOTW";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJDOTW";
		}
	},
	DESIYA {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.DESIYA;
		}

		@Override
		public String getVervotechSupplierName() {
			return "TravelGuru";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJDESIYA";
		}
	},
	TOTALSTAY {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.TOTALSTAY;
		}

		@Override
		public String getVervotechSupplierName() {
			return "TotalStay";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJTOTALSTAY";
		}
	},
	RTS {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.RTS;
		}

		@Override
		public String getVervotechSupplierName() {
			return "RTS";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJRTS";
		}
	},
	ROOMS24X7 {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.ROOMS24X7;
		}

		@Override
		public String getVervotechSupplierName() {
			return "24x7Rooms";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJ24X7";
		}
	},
	W2M {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.W2M;
		}

		@Override
		public String getVervotechSupplierName() {
			return "W2M";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJW2M";
		}
	},
	OLYMPIA {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.OLYMPIA;
		}

		@Override
		public String getVervotechSupplierName() {
			return "OlympiaEurope";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJOLYMPIA";
		}
	},
	TJQUANTUM {
		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.QUANTUM;
		}

		@Override
		public String getVervotechSupplierName() {
			return "QuantumReservations";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJQUANTUM";
		}
	},
	HOTELSPRO {
		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.HOTELSPRO;
		}

		@Override
		public String getVervotechSupplierName() {
			return "HotelsPro";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJHOTELSPRO";
		}
	},
	DARINA {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.DARINA;
		}

		@Override
		public String getVervotechSupplierName() {
			return "Darina";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJDARINA";
		}
	},
	TEAMUSA {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.TEAMUSA;
		}

		@Override
		public String getVervotechSupplierName() {
			return "TeamAmerica";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJTEAMUSA";
		}
	},
	MIKI {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.MIKI;
		}

		@Override
		public String getVervotechSupplierName() {
			return "Miki";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJMIKI";
		}
	},
	TAMTOUR {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.TAMTOUR;
		}

		@Override
		public String getVervotechSupplierName() {
			return "TamTour";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJTAMTOUR";
		}
	},
	TRAVCO {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.TRAVCO;
		}

		@Override
		public String getVervotechSupplierName() {
			return "Travco";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJTRAVCO";
		}
	},
	RESTEL {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.RESTEL;
		}

		@Override
		public String getVervotechSupplierName() {
			return "Restel";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJRESTEL";
		}
	},
	FASTPAY {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.FASTPAY;
		}

		@Override
		public String getVervotechSupplierName() {
			return "FastPay";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJFASTPAY";
		}
	},
	TOURICO {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.TOURICO;
		}

		@Override
		public String getVervotechSupplierName() {
			return "Tourico";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJTOURICO";
		}
	},
	OYO {

		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.OYO;
		}

		@Override
		public String getVervotechSupplierName() {
			return "OyoB2B";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJOYO";
		}
	},
	REDAPPLE {
		@Override
		public HotelSourceType getHotelSourceType() {
			return HotelSourceType.REDAPPLE;
		}

		@Override
		public String getVervotechSupplierName() {
			return "RedApple";
		}

		@Override
		public String getQtechSupplierName() {
			return "TJREDAPPLE";
		}
	};

	// TJLOCAL {
	//
	// @Override
	// public HotelSourceType getHotelSourceType() {
	// return HotelSourceType.QTECH;
	// }
	//
	// @Override
	// public String getVervotechSupplierName() {
	// return "AtlasExtranet";
	// }
	//
	// @Override
	// public boolean isProviderHotelIdPresent() {
	// return false;
	// }
	// },
	// TJONEABOVE {
	//
	// @Override
	// public HotelSourceType getHotelSourceType() {
	// return HotelSourceType.QTECH;
	// }
	//
	// @Override
	// public String getVervotechSupplierName() {
	// return "AtlasExtranet";
	// }
	//
	// @Override
	// public boolean isProviderHotelIdPresent() {
	// return false;
	// }
	// };

	public static VervotechSupplierMapping getVervotechSupplierTypeFromHotelSourceName(HotelSourceType sourceType) {
		for (VervotechSupplierMapping supplierMapping : VervotechSupplierMapping.values()) {
			if (supplierMapping.getHotelSourceType().equals(sourceType)) {
				return supplierMapping;
			}
		}
		return null;
	}

	public static VervotechSupplierMapping getVervotechSupplierTypeFromVervotechSourceName(String vervotechSourceName) {
		for (VervotechSupplierMapping supplierMapping : VervotechSupplierMapping.values()) {
			if (supplierMapping.getVervotechSupplierName().equalsIgnoreCase(vervotechSourceName)) {
				return supplierMapping;
			}
		}
		return null;
	}

	public static VervotechSupplierMapping getVervotechSupplierMapping(String name) {
		for (VervotechSupplierMapping supplierMapping : VervotechSupplierMapping.values()) {
			if (supplierMapping.name().equalsIgnoreCase(name)) {
				return supplierMapping;
			}
		}
		return null;
	}

	public static VervotechSupplierMapping getVervotechSupplierMappingFromQtechSupplierName(String name) {
		for (VervotechSupplierMapping supplierMapping : VervotechSupplierMapping.values()) {
			if (supplierMapping.getQtechSupplierName().equalsIgnoreCase(name)) {
				return supplierMapping;
			}
		}
		return null;
	}

	public String getStatus() {
		return this.getCode();
	}

	private String code;


	public abstract HotelSourceType getHotelSourceType();

	public abstract String getVervotechSupplierName();

	public String getQtechSupplierName() {
		return "";
	}
}
