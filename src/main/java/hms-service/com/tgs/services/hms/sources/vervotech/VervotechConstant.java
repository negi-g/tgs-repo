package com.tgs.services.hms.sources.vervotech;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class VervotechConstant {

	public static final String HOTEL_SERVICE_TYPE = "HOTEL_MAPPING";
	public static final String ROOM_SERVICE_TYPE = "ROOM_MAPPING";
}
