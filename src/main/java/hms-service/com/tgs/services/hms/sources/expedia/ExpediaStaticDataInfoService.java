package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.ObjectUtils;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.gms.datamodel.CountryInfo;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelRegionAdditionalInfo;
import com.tgs.services.hms.datamodel.HotelRegionAncestorType;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.State;
import com.tgs.services.hms.datamodel.expedia.Descriptions;
import com.tgs.services.hms.datamodel.expedia.PolygonCoordinate;
import com.tgs.services.hms.datamodel.expedia.PreparePropertyCatalogRequest;
import com.tgs.services.hms.datamodel.expedia.PropertyCatalog;
import com.tgs.services.hms.datamodel.expedia.RegionWiseDataRequest;
import com.tgs.services.hms.datamodel.expedia.RegionWiseDataResponse;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoSaveManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Getter
public class ExpediaStaticDataInfoService {

	private HotelInfoSaveManager hotelInfoSaveManager;
	private HotelRegionInfoSaveManager regionInfoSaveManager;
	private HotelRegionInfoMappingManager regionInfoMappingManager;
	private GeneralServiceCommunicator gsCommunicator;
	private HotelSourceConfigOutput sourceConfig;
	private HotelStaticDataRequest staticDataRequest;
	private HotelSupplierConfiguration supplierConf;
	private HotelStaticDataService staticDataService;

	private static final String PROPERTY_CATALOG_SUFFIX = "/properties/content";
	private static final String REGION_WISE_CITY_MAPPING_SUFFIX = "/regions";

	public void init() {

		hotelInfoSaveManager =
				(HotelInfoSaveManager) SpringContext.getApplicationContext().getBean("hotelInfoSaveManager");

		regionInfoSaveManager = (HotelRegionInfoSaveManager) SpringContext.getApplicationContext()
				.getBean("hotelRegionInfoSaveManager");

		regionInfoMappingManager = (HotelRegionInfoMappingManager) SpringContext.getApplicationContext()
				.getBean("hotelRegionInfoMappingManager");

		gsCommunicator = (GeneralServiceCommunicator) SpringContext.getApplicationContext()
				.getBean("generalServiceCommunicatorImpl");
		staticDataService = (HotelStaticDataService) HotelUtils.getStaticDataService();

	}

	@SuppressWarnings({"unchecked", "serial"})
	public void getRegionWiseCityMapping() throws IOException {
		RegionWiseDataRequest regionWiseDataRequest = createRequestToFetchRegionWiseCityMapping();
		HttpUtilsV2 httpUtils = ExpediaUtils.getResponseURL(regionWiseDataRequest, sourceConfig, supplierConf);
		httpUtils.setPrintResponseLog(false);
		httpUtils.getResponse(Object.class);
		List<HotelRegionInfoQuery> regionInfoQueries =
				createRegionMappingResponse(createRegionWiseDataResponse(httpUtils.getResponseString()));
		HotelBaseSupplierUtils.saveOrUpdateRegionInfo(regionInfoQueries, staticDataRequest.getIsMasterData(), true);
		handleRestRegionWiseCityMapping(httpUtils.getResponseHeaderParams());
	}

	private List<RegionWiseDataResponse> createRegionWiseDataResponse(String regionWiseDataString) {

		JSONArray regions = new JSONArray(regionWiseDataString);
		List<RegionWiseDataResponse> regionWiseDataResponses = new ArrayList<>();
		for (int i = 0; i < regions.length(); i++) {
			JSONObject region = regions.getJSONObject(i);
			RegionWiseDataResponse regionWiseDataResponse = new RegionWiseDataResponse();
			regionWiseDataResponse.setId(String.valueOf(region.get("id")));
			regionWiseDataResponse.setType(String.valueOf(region.get("type")));
			regionWiseDataResponse.setName(String.valueOf(region.get("name")));
			if (region.has("name_full")) {
				regionWiseDataResponse.setName_full(String.valueOf(region.get("name_full")));
			} else {
				regionWiseDataResponse.setName_full("");
			}

			if (region.has("country_code")) {
				regionWiseDataResponse.setCountry_code(String.valueOf(region.get("country_code")));
			} else {
				regionWiseDataResponse.setCountry_code("");
			}
			PolygonCoordinate polygonCoordinates = new PolygonCoordinate();
			if (region.has("ancestors")) {
				JSONArray ancestorsArr = region.getJSONArray("ancestors");
				List<HotelRegionAncestorType> regionAncestors = new ArrayList<>();
				for (int j = 0; j < ancestorsArr.length(); j++) {
					JSONObject ancestors = ancestorsArr.getJSONObject(j);
					HotelRegionAncestorType regionAncestorType = new HotelRegionAncestorType();
					regionAncestorType.setId(ancestors.getString("id"));
					regionAncestorType.setType(ancestors.getString("type"));
					regionAncestors.add(regionAncestorType);
				}
				regionWiseDataResponse.setAncestors(regionAncestors);
			}

			if (region.has("coordinates")) {
				JSONObject coordinate = region.getJSONObject("coordinates");
				polygonCoordinates.setCenter_latitude(coordinate.getDouble("center_latitude"));
				polygonCoordinates.setCenter_longitude(coordinate.getDouble("center_longitude"));
				if (coordinate.has("bounding_polygon")) {
					polygonCoordinates.setBounding_polygon(String.valueOf(coordinate.get("bounding_polygon")));
				}
				regionWiseDataResponse.setCoordinates(polygonCoordinates);
			}
			regionWiseDataResponses.add(regionWiseDataResponse);
		}
		return regionWiseDataResponses;
	}

	@SuppressWarnings({"serial"})
	private void handleRestRegionWiseCityMapping(Map<String, List<String>> headerParams) throws IOException {
		int i = 0;
		int retryCount = 0;
		HttpUtilsV2 httpUtils = null;
		int cityCount = 0;
		try {
			while (ObjectUtils.isEmpty(sourceConfig.getCityHitCount())
					|| (!ObjectUtils.isEmpty(sourceConfig.getCityHitCount()) && i < sourceConfig.getCityHitCount())) {
				try {
					List<String> listOfLink = headerParams.get("Link");
					if (CollectionUtils.isEmpty(listOfLink))
						break;
					String link = listOfLink.get(0);
					link = link.substring(1, link.indexOf(">"));
					Map<String, String> headerParam = new HashMap<>();
					headerParam.put("Authorization", ExpediaUtils.getAuthorizationSignature(supplierConf));
					headerParam.put("Accept", "application/json");
					httpUtils = HttpUtilsV2.builder().headerParams(headerParam).urlString(link).build();
					httpUtils.setPrintResponseLog(false);
					httpUtils.getResponse(Object.class);
					try {
						List<HotelRegionInfoQuery> regionInfoQueries =
								createRegionMappingResponse(
										createRegionWiseDataResponse(httpUtils.getResponseString()));
						HotelBaseSupplierUtils.saveOrUpdateRegionInfo(regionInfoQueries,
								staticDataRequest.getIsMasterData(), true);
						headerParams = httpUtils.getResponseHeaderParams();
						cityCount += regionInfoQueries.size();
						/*
						 * Temporary Logs
						 */
						log.info("No of cities fetched are {}", cityCount);
						retryCount = 0;
						i++;
					} catch (Exception e) {
						RegionWiseDataResponse errorInPropertyCatalog =
								GsonUtils.getGson().fromJson(httpUtils.getResponseString(),
										RegionWiseDataResponse.class);
						if (errorInPropertyCatalog.getType().equals("unknown_internal_error")) {
							retryCount++;
						}
						log.info("Unable to fetch complete city static dump due to type {} and message {}",
								errorInPropertyCatalog.getType(), errorInPropertyCatalog.getMessage(), e);
						if (retryCount > 5)
							break;
					}
				} catch (IOException e) {
					log.error("Unable to fetch hotel city mapping for url {}, headers {}, link {}",
							httpUtils.getUrlString(), httpUtils.getHeaderParams(), headerParams, e);
				}
			}
		} finally {
			log.info("Total no of city fetched are {}, final response {}, headers {}", cityCount,
					httpUtils.getResponseString(), httpUtils.getResponseHeaderParams());
		}
	}

	@SuppressWarnings({"unchecked", "serial"})
	public void handleHotelStaticContent() throws IOException {
		PreparePropertyCatalogRequest preparePropertyCatalogRequest = createRequestToPreparePropertyCatalogURL();
		HttpUtilsV2 httpUtils = ExpediaUtils.getResponseURL(preparePropertyCatalogRequest, sourceConfig, supplierConf);

		if (CollectionUtils.isNotEmpty(staticDataRequest.getAddlParams())) {
			MultiMap propertyIdMap = getCountryCodes(staticDataRequest.getAddlParams());
			httpUtils.getRecurringQueryParams().putAll(propertyIdMap);
		}
		httpUtils.setPrintResponseLog(false);
		httpUtils.getResponseFromType(new TypeToken<HashMap<String, PropertyCatalog>>() {}.getType());
		String responseString = httpUtils.getResponseString();
		try {
			Map<String, PropertyCatalog> propertyCatalogs = GsonUtils.getGson().fromJson(responseString,
					new TypeToken<HashMap<String, PropertyCatalog>>() {}.getType());
			List<HotelInfo> hotelInfos = convertPropertyCatalogsIntoHotelInfos(propertyCatalogs);
			saveOrUpdateHotelInfos(hotelInfos);
			handleRestHotelStaticContent(httpUtils.getResponseHeaderParams());
		} catch (Exception e) {
			PropertyCatalog errorInPropertyCatalog =
					GsonUtils.getGson().fromJson(responseString, PropertyCatalog.class);
			log.info("Unable to fetch complete hotel static dump due to type {} and message {}",
					errorInPropertyCatalog.getType(), errorInPropertyCatalog.getMessage(), e);
		}
	}

	@SuppressWarnings({"unchecked", "serial"})
	private void handleRestHotelStaticContent(Map<String, List<String>> headerParams) throws IOException {
		int i = 1;
		String link = "";
		HttpUtilsV2 httpUtils = null;
		int retryCount = 0;
		try {
			while (ObjectUtils.isEmpty(sourceConfig.getHotelHitCount())
					|| (!ObjectUtils.isEmpty(sourceConfig.getHotelHitCount()) && i < sourceConfig.getHotelHitCount())) {
				try {

					List<String> listOfLink = headerParams.get("Link");
					if (CollectionUtils.isEmpty(listOfLink))
						break;
					link = listOfLink.get(0);
					link = link.substring(1, link.indexOf(">"));
					Map<String, String> headerParam = new HashMap<>();
					headerParam.put("Authorization", ExpediaUtils.getAuthorizationSignature(supplierConf));
					headerParam.put("Accept", "application/json");
					httpUtils = HttpUtilsV2.builder().headerParams(headerParam).urlString(link).build();
					httpUtils.setPrintResponseLog(false);
					httpUtils.getResponseFromType(new TypeToken<HashMap<String, PropertyCatalog>>() {}.getType());
					String responseString = httpUtils.getResponseString();
					try {
						Map<String, PropertyCatalog> propertyCatalogs = GsonUtils.getGson().fromJson(responseString,
								new TypeToken<HashMap<String, PropertyCatalog>>() {}.getType());
						List<HotelInfo> hotelInfos = convertPropertyCatalogsIntoHotelInfos(propertyCatalogs);
						saveOrUpdateHotelInfos(hotelInfos);
						headerParams = httpUtils.getResponseHeaderParams();
						/*
						 * Temporary Logs
						 */
						log.info("No of hotels fetched are {}", i * 250);
						retryCount = 0;
						i++;
					} catch (Exception e) {
						PropertyCatalog errorInPropertyCatalog =
								GsonUtils.getGson().fromJson(responseString, PropertyCatalog.class);
						if (errorInPropertyCatalog.getType().equals("unknown_internal_error")) {
							retryCount++;
						}
						log.info("Unable to fetch complete hotel static dump due to type {} and message {}",
								errorInPropertyCatalog.getType(), errorInPropertyCatalog.getMessage(), e);

						if (retryCount > 3)
							break;
					}
				} catch (IOException e) {
					log.error("Unable to fetch hotel mapping for url {}, headers {}, link {}", httpUtils.getUrlString(),
							httpUtils.getHeaderParams(), headerParams, e);
				}
			}
		} finally {
			log.info("Total no of hotels fetched are {}, final response {}, headers {}", i * 250,
					httpUtils.getResponseString(), httpUtils.getResponseHeaderParams());
		}
	}

	private List<HotelRegionInfoQuery> createRegionMappingResponse(
			List<RegionWiseDataResponse> regionWiseDataResponse) {

		List<HotelRegionInfoQuery> regionInfoQueries = new ArrayList<>();
		for (RegionWiseDataResponse region : regionWiseDataResponse) {
			CountryInfo countryInfo = getCountryInfo(region.getCountry_code());
			HotelRegionInfoQuery regionInfoQuery = new HotelRegionInfoQuery();
			if (Objects.nonNull(countryInfo)) {
				regionInfoQuery.setCountryName(countryInfo.getCountry());
			} else {
				continue;
			}
			regionInfoQuery.setRegionType(region.getType().toUpperCase());
			regionInfoQuery.setSupplierName(HotelSourceType.EXPEDIA.name());
			if (region.getType().equals("province_state")) {
				regionInfoQuery.setStateId(region.getId());
				regionInfoQuery.setStateName(region.getName());
			} else if (region.getType().equals("city")) {
				regionInfoQuery.setRegionId(region.getId());
				regionInfoQuery.setRegionName(region.getName());
			} else {
				regionInfoQuery.setRegionId(region.getId());
				regionInfoQuery.setRegionName(region.getName());
			}
			regionInfoQuery.setFullRegionName(region.getName_full());
			regionInfoQuery
					.setAdditionalInfo(HotelRegionAdditionalInfo.builder().ancestors(region.getAncestors()).build());
			if (Objects.nonNull(region.getCoordinates())
					&& Objects.nonNull(region.getCoordinates().getBounding_polygon())) {
				HotelRegionAdditionalInfo regionAdditionalInfo =
						Objects.nonNull(regionInfoQuery.getAdditionalInfo()) ? regionInfoQuery.getAdditionalInfo()
								: HotelRegionAdditionalInfo.builder().build();
				regionAdditionalInfo.setPolygonInfo(region.getCoordinates().getBounding_polygon());
				regionInfoQuery.setAdditionalInfo(regionAdditionalInfo);
			}
			regionInfoQueries.add(regionInfoQuery);
		}
		return regionInfoQueries;
	}

	private List<HotelInfo> convertPropertyCatalogsIntoHotelInfos(Map<String, PropertyCatalog> propertyContent) {
		List<HotelInfo> hotelInfos = new ArrayList<>();

		for (Map.Entry<String, PropertyCatalog> propertyContentMap : propertyContent.entrySet()) {
			try {
				PropertyCatalog propertyCatalog = propertyContentMap.getValue();
				CountryInfo countryInfo = getCountryInfo(propertyCatalog.getAddress().getCountry_code());
				String countryName = !ObjectUtils.isEmpty(countryInfo) ? countryInfo.getName()
						: propertyCatalog.getAddress().getCountry_code() + "--Unknown";
				Address address = Address.builder().addressLine1(propertyCatalog.getAddress().getLine_1())
						.addressLine2(propertyCatalog.getAddress().getLine_2())
						.cityName(propertyCatalog.getAddress().getCity())
						.stateName(propertyCatalog.getAddress().getState_province_name())
						.countryName(countryName)
						.city(City.builder().name(propertyCatalog.getAddress().getCity()).build())
						.state(State.builder().name(propertyCatalog.getAddress().getState_province_name()).build())
						.country(Country.builder().name(countryName)
								.code(propertyCatalog.getAddress().getCountry_code()).build())
						.postalCode(propertyCatalog.getAddress().getPostal_code()).build();
				GeoLocation geolocation = GeoLocation.builder()
						.latitude(String.valueOf(propertyCatalog.getLocation().getCoordinates().getLatitude()))
						.longitude(String.valueOf(propertyCatalog.getLocation().getCoordinates().getLongitude()))
						.build();


				List<Image> images = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(propertyCatalog.getImages())) {
					propertyCatalog.getImages().forEach(image -> {
						Image img = Image.builder().build();

						if (image.getLinks().get("350px") != null) {
							img.setBigURL(image.getLinks().get("350px").getHref());
						}

						if (image.getLinks().get("70px") != null) {
							img.setThumbnail(image.getLinks().get("70px").getHref());
						}
						images.add(img);
					});
				}

				// temp_logs, star rating should be in string rather than integer
				HotelInfo hotelInfo = HotelInfo.builder().id(propertyCatalog.getProperty_id())
						.name(propertyCatalog.getName()).propertyType(propertyCatalog.getCategory().getName())
						.address(address).images(images).geolocation(geolocation).build();

				if (MapUtils.isNotEmpty(propertyCatalog.getAmenities())) {
					List<String> facilities = new ArrayList<>();
					propertyCatalog.getAmenities().forEach((id, amenity) -> {
						facilities.add(amenity.getName());
					});
					hotelInfo.setFacilities(facilities);
				}

				if (!ObjectUtils.isEmpty(propertyCatalog.getRatings())
						&& !ObjectUtils.isEmpty(propertyCatalog.getRatings().getProperty())) {
					hotelInfo.setRating(
							(int) Double.parseDouble(propertyCatalog.getRatings().getProperty().getRating()));
				} else {
					hotelInfo.setRating(null);
				}

				if (!ObjectUtils.isEmpty(propertyCatalog.getCheckin())) {
					populateCheckinInstructions(propertyCatalog, hotelInfo);
				}

				if (!ObjectUtils.isEmpty(propertyCatalog.getDescriptions())) {
					hotelInfo.setDescription(createDescription(propertyCatalog.getDescriptions()));
				}

				hotelInfos.add(hotelInfo);
			} catch (Exception e) {
				log.info("Unable to convert property catalog into hotel info {}",
						GsonUtils.getGson().toJson(propertyContentMap.getValue()), e);

			}
		}

		return hotelInfos;
	}

	private void populateCheckinInstructions(PropertyCatalog propertyCatalog, HotelInfo hotelInfo) {

		List<Instruction> instructions = new ArrayList<>();

		if (StringUtils.isNotBlank(propertyCatalog.getCheckin().getInstructions())) {
			instructions.add(Instruction.builder().type(InstructionType.BOOKING_NOTES)
					.msg(propertyCatalog.getCheckin().getInstructions().replaceAll("\\<.*?\\>", "")).build());
		}

		if (StringUtils.isNotBlank(propertyCatalog.getCheckin().getSpecial_instructions())) {
			instructions.add(Instruction.builder().type(InstructionType.SPECIAL_INSTRUCTIONS)
					.msg(propertyCatalog.getCheckin().getSpecial_instructions().replaceAll("\\<.*?\\>", "")).build());
		}

		if (!ObjectUtils.isEmpty(propertyCatalog.getFees())) {
			if (StringUtils.isNotBlank(propertyCatalog.getFees().getMandatory())) {
				instructions.add(Instruction.builder().type(InstructionType.MANDATORY_FEES)
						.msg(propertyCatalog.getFees().getMandatory().replaceAll("\\<.*?\\>", "")).build());
			}

			if (StringUtils.isNotBlank(propertyCatalog.getFees().getOptional())) {
				instructions.add(Instruction.builder().type(InstructionType.OPTIONAL_FEES)
						.msg(propertyCatalog.getFees().getOptional().replaceAll("\\<.*?\\>", "")).build());
			}
		}

		if (!ObjectUtils.isEmpty(propertyCatalog.getPolicies())
				&& StringUtils.isNotBlank(propertyCatalog.getPolicies().getKnow_before_you_go())) {
			instructions.add(Instruction.builder().type(InstructionType.KNOW_BEFORE_YOU_GO)
					.msg(propertyCatalog.getPolicies().getKnow_before_you_go().replaceAll("\\<.*?\\>", "")).build());
		}

		hotelInfo.setInstructions(instructions);
	}


	private String createDescription(Descriptions descriptions) {

		StringBuilder builder = new StringBuilder();
		if (!ObjectUtils.isEmpty(descriptions.getHeadline())) {
			builder.append(descriptions.getHeadline());
		}
		if (!ObjectUtils.isEmpty(descriptions.getLocation())) {
			builder.append(descriptions.getLocation());
		}
		if (!ObjectUtils.isEmpty(descriptions.getAmenities())) {
			builder.append(descriptions.getAmenities());
		}
		if (!ObjectUtils.isEmpty(descriptions.getAttractions())) {
			builder.append(descriptions.getAttractions());
		}
		return builder.toString().replaceAll("\\<.*?\\>", "");
	}

	private void saveOrUpdateHotelInfos(List<HotelInfo> hotelInfos) {
		for (HotelInfo hotelInfo : hotelInfos) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hotelInfo);
				dbHotelInfo.setId(null);
				String supplierName = HotelSourceType.EXPEDIA.name();
				dbHotelInfo.setSupplierName(supplierName);
				dbHotelInfo.setSupplierHotelId(hotelInfo.getId());
				hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, staticDataService);
			} catch (Exception e) {
				log.info("Error while processing hotel {} due to {} ", hotelInfo.getId(), e.getMessage());
			}
		}
	}

	private RegionWiseDataRequest createRequestToFetchRegionWiseCityMapping() {
		return RegionWiseDataRequest.builder().suffixOfURL(REGION_WISE_CITY_MAPPING_SUFFIX)
				.include(sourceConfig.getCityInfoGranularity()).language(staticDataRequest.getLanguage()).build();
	}

	private PreparePropertyCatalogRequest createRequestToPreparePropertyCatalogURL() {
		return PreparePropertyCatalogRequest.builder().suffixOfURL(PROPERTY_CATALOG_SUFFIX)
				.language(staticDataRequest.getLanguage()).build();
	}

	private MultiMap getCountryCodes(List<String> countryIds) {

		MultiMap countryIdMap = new MultiValueMap();
		for (String countryId : countryIds) {
			countryIdMap.put("country_code", countryId);
		}
		return countryIdMap;
	}

	private CountryInfo getCountryInfo(String countryCode) {
		CountryInfo country = gsCommunicator.getCountryInfo(countryCode);
		return country;
	}
}
