package com.tgs.services.hms.mapper;

import java.util.stream.Collectors;

import org.springframework.util.ObjectUtils;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.quotation.HotelQuotation;
import com.tgs.services.hms.datamodel.quotation.QuotationRoomInfo;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class HotelInfoToHotelQuotationMapper extends Mapper<HotelQuotation> {

	private HotelInfo hotelInfo;
	private HotelSearchQuery searchQuery;
	private String optionId;

	@Override
	protected void execute() throws CustomGeneralException {
		// TODO Auto-generated method stub
		Double totalOptionPrice = 0.0;
		Double totalOptionDiscount = 0.0;

		output = output != null ? output : HotelQuotation.builder().build();
		Option option = hotelInfo.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);

		if (option.getCancellationPolicy() != null) {
			output.getCancellationPolicy().add(option.getCancellationPolicy());
		}

		for (RoomInfo roomInfo : option.getRoomInfos()) {
			QuotationRoomInfo quotationRoomInfo = new QuotationRoomInfo();
			quotationRoomInfo.setMealBasis(roomInfo.getMealBasis());
			quotationRoomInfo.setRoomCategory(roomInfo.getRoomCategory());
			quotationRoomInfo.setRoomType(roomInfo.getRoomType());
			quotationRoomInfo.setRoomId(roomInfo.getId());
			if (roomInfo.getCancellationPolicy() != null) {
				quotationRoomInfo.getCancellationPolicy().add(roomInfo.getCancellationPolicy());
			}
			output.getQuotationRoomInfoList().add(quotationRoomInfo);
			totalOptionPrice += roomInfo.getTotalPrice();
			totalOptionDiscount+= roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF).getOrDefault(HotelFareComponent.DS, 0.0);
		}

		if (!ObjectUtils.isEmpty(hotelInfo.getImages()))
			output.getImages().add(hotelInfo.getImages().get(0));
		output.setAddress(hotelInfo.getAddress());
		output.setCheckinDate(searchQuery.getCheckinDate());
		output.setCheckoutDate(searchQuery.getCheckoutDate());
		output.setFacilities(hotelInfo.getFacilities());
		output.setHotelName(hotelInfo.getName());
		output.setIsAvailable(true); // Need to clarify
		output.setOptionId(optionId);
		output.setPrice(totalOptionPrice);
		output.setDiscount(totalOptionDiscount);
		output.setSearchQuery(searchQuery);
		output.setStarRating(hotelInfo.getRating());
	}
}
