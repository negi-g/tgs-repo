package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.bookingcancellation.RoomGuest;
import com.tgs.services.hms.datamodel.tbo.search.ArrayOfBasicPropertyInfo;
import com.tgs.services.hms.datamodel.tbo.search.ArrayOfBasicPropertyInfo.BasicPropertyInfo;
import com.tgs.services.hms.datamodel.tbo.search.HotelDetail;
import com.tgs.services.hms.datamodel.tbo.search.HotelDetailRequest;
import com.tgs.services.hms.datamodel.tbo.search.HotelDetailResponse;
import com.tgs.services.hms.datamodel.tbo.search.HotelResult;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomDetail;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomResponse;
import com.tgs.services.hms.datamodel.tbo.search.HotelSearchRequest;
import com.tgs.services.hms.datamodel.tbo.search.HotelSearchResponse;
import com.tgs.services.hms.datamodel.tbo.search.HotelStaticDetailRequest;
import com.tgs.services.hms.datamodel.tbo.search.HotelStaticDetailResponse;
import com.tgs.services.hms.datamodel.tbo.search.RoomCombination;
import com.tgs.services.hms.datamodel.tbo.search.RoomCombinations;
import com.tgs.services.hms.datamodel.tbo.staticData.VendorMessages.VendorMessage;
import com.tgs.services.hms.datamodel.tbo.staticData.VendorMessages.VendorMessage.SubSection;
import com.tgs.services.hms.datamodel.tbo.staticData.VendorMessages.VendorMessage.SubSection.Paragraph;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@SuperBuilder
public class TravelBoutiqueSearchService extends TravelBoutiqueBaseService {

	private HotelSupplierRegionInfo supplierRegionInfo;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelSupplierConfiguration supplierConf;
	private HotelSearchQuery searchQuery;
	private HotelInfo hInfo;
	private HotelSearchResult searchResult;
	protected RestAPIListener listener;
	public HotelCacheHandler cacheHandler;

	private static final Integer maxOptionSize = 15;


	public void doSearch() throws IOException {

		HttpUtils httpUtils = null;
		HotelSearchRequest searchRequest = null;
		try {
			listener = new RestAPIListener("");
			searchRequest = createSearchRequest();
			httpUtils = TravelBoutiqueUtil.getRequest(searchRequest, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_SEARCH_URL));
			httpUtils.setPrintResponseLog(false);
			listener.addLog(LogData.builder().key(searchQuery.getSearchId())
					.logData(GsonUtils.getGson().toJson(searchRequest)).type("TBO-HotelSearch-Req").build());

			HotelSearchResponse searchResponse = httpUtils.getResponse(HotelSearchResponse.class).orElse(null);
			if (searchResponse.isSessionExpired()) {
				log.error("TBO Session Expired SearchId {}", searchQuery.getSearchId());
				storeToken();
				searchRequest.setTokenId(getCachedToken());
				log.info("SearchRequest is {}", GsonUtils.getGson().toJson(searchRequest));
				httpUtils = TravelBoutiqueUtil.getRequest(searchRequest, supplierConf,
						supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_SEARCH_URL));
				searchResponse = httpUtils.getResponse(HotelSearchResponse.class).orElse(null);
			}
			searchResult = createSearchResult(searchResponse);
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TBO.name())
					.postData(GsonUtils.getGson().toJson(searchRequest)).requestType(BaseHotelConstants.SEARCH)
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.logKey(searchQuery.getSearchId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
	}


	private HotelSearchRequest createSearchRequest() throws IOException {

		String token = getCachedToken();
		HotelSearchRequest searchRequest = new HotelSearchRequest();
		searchRequest.setCheckInDate(DateTimeFormatter.ofPattern("dd/MM/yyyy").format(searchQuery.getCheckinDate()));
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		searchRequest.setNoOfNights(numberOfNights);

		searchRequest.setCountryCode(supplierRegionInfo.getCountryId());
		searchRequest.setCityId(Integer.parseInt(supplierRegionInfo.getRegionId()));

		/*
		 * Nationality for Qtech coming as its own code i.e. 106 . Need nationality mapping same as city mapping
		 */
		// String nationalityCode =
		// cacheHandler.fetchSupplierNationalityMapping("TBO", searchQuery.getSearchCriteria().getNationality());
		//
		searchRequest.setGuestNationality(TravelBoutiqueUtil.NATIONALITY);
		searchRequest.setPreferredCurrency(TravelBoutiqueUtil.CURRENCY);

		searchRequest.setNoOfRooms(searchQuery.getRoomInfo().size());
		List<RoomGuest> roomGuests = new ArrayList<>();
		for (RoomSearchInfo roomInfo : searchQuery.getRoomInfo()) {
			Integer numberOfChild = roomInfo.getNumberOfChild() != null ? roomInfo.getNumberOfChild() : 0;
			RoomGuest roomGuest = RoomGuest.builder().NoOfAdults(roomInfo.getNumberOfAdults()).NoOfChild(numberOfChild)
					.ChildAge(roomInfo.getChildAge()).build();
			roomGuests.add(roomGuest);
		}
		searchRequest.setRoomGuests(roomGuests);

		searchRequest.setTokenId(token);
		searchRequest.setEndUserIp(TravelBoutiqueUtil.IP);

		List<Integer> ratings = searchQuery.getSearchPreferences().getRatings();
		int minRating = 0, maxRating = 5;
		if (CollectionUtils.isNotEmpty(ratings)) {
			Collections.sort(ratings);
			minRating = ratings.get(0);
			maxRating = ratings.get(ratings.size() - 1);
		}

		searchRequest.setIsTBOMapped(BooleanUtils.isTrue(sourceConfigOutput.getAllowMappedRequest()));

		searchRequest.setMinRating(minRating);
		searchRequest.setMaxRating(maxRating);

		return searchRequest;
	}

	private HotelSearchResult createSearchResult(HotelSearchResponse response) {

		searchResult = HotelSearchResult.builder().build();
		if (response == null || response.getHotelSearchResult() == null
				|| CollectionUtils.isEmpty(response.getHotelSearchResult().getHotelResults())) {
			log.error("Empty Response From Supplier {} for searchId {}", supplierConf.getBasicInfo().getSupplierId(),
					searchQuery.getSearchId());
			return null;
		}


		List<HotelResult> hotelResults = response.getHotelSearchResult().getHotelResults();
		List<HotelInfo> hotelInfos = new ArrayList<>();
		for (HotelResult hotelResult : hotelResults) {
			Address address = Address.builder().addressLine1(hotelResult.getHotelAddress()).build();
			GeoLocation geoLocation = null;
			if (StringUtils.isNotBlank(hotelResult.getLatitude())
					&& StringUtils.isNotBlank(hotelResult.getLongitude())) {
				geoLocation = GeoLocation.builder().latitude(hotelResult.getLatitude())
						.longitude(hotelResult.getLongitude()).build();
			}

			Image image = Image.builder().thumbnail(hotelResult.getHotelPicture()).bigURL(hotelResult.getHotelPicture())
					.build();
			HotelMiscInfo miscInfo =
					HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
							.searchKeyExpiryTime(LocalDateTime.now().plusMinutes(
									ObjectUtils.firstNonNull(sourceConfigOutput.getSearchKeyExpirationTime(), 20l)))
							.build();

			/**
			 * Dummy Option to store supplier & miscInfo
			 */

			Option option = Option.builder().totalPrice(hotelResult.getPrice().getRoomPrice())
					.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
							.resultIndex(hotelResult.getResultIndex())
							.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
							.supplierSearchId(response.getHotelSearchResult().getTraceId())
							.supplierHotelId(hotelResult.getHotelCode())
							.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
					.roomInfos(getRoomInfo(hotelResult)).id(RandomStringUtils.random(20, true, true)).build();
			option.getMiscInfo().setIsNotRequiredOnDetail(true);

			/**
			 * We need to pick from static Hotel data if IsTBOMapped is true, else form HInfo from search response
			 */
			if (BooleanUtils.isTrue(hotelResult.getIsTBOMapped())) {
				HotelInfo hInfo = HotelInfo.builder()
						.miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId()).isSupplierMapped(true)
								.supplierStaticHotelId(hotelResult.getHotelCode()).build())
						.options(new ArrayList<>(Arrays.asList(option))).build();
				hotelInfos.add(hInfo);
			} else {
				HotelInfo hInfo = HotelInfo.builder().name(hotelResult.getHotelName())
						.rating(hotelResult.getStarRating()).description(hotelResult.getHotelDescription())
						.address(address).geolocation(geoLocation).images(Arrays.asList(image)).miscInfo(miscInfo)
						.options(new ArrayList<>(Arrays.asList(option))).build();
				hotelInfos.add(hInfo);
			}
		}
		searchResult.setHotelInfos(hotelInfos);

		return searchResult;
	}


	private List<RoomInfo> getRoomInfo(HotelResult hotelResult) {

		Double totalOptionPrice = hotelResult.getPrice().getPublishedPrice();
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		int numberOfRoom = searchQuery.getRoomInfo().size();
		Double perRoomPerNightPrice = totalOptionPrice / (numberOfNights * numberOfRoom);
		List<RoomInfo> roomInfos = new ArrayList<>(searchQuery.getRoomInfo().size());
		for (int i = 0; i < searchQuery.getRoomInfo().size(); i++) {
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setNumberOfAdults(searchQuery.getRoomInfo().get(i).getNumberOfAdults());
			roomInfo.setNumberOfChild(searchQuery.getRoomInfo().get(i).getNumberOfChild());
			roomInfo.setRoomCategory("Dummy Room Category");
			roomInfo.setRoomType("Dummy Room Type");
			roomInfo.setMealBasis(BaseHotelConstants.ROOM_ONLY);
			Double totalRoomPrice = 0.0;
			List<PriceInfo> priceInfoList = new ArrayList<>();
			for (int j = 1; j <= numberOfNights; j++) {
				PriceInfo priceInfo = new PriceInfo();
				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				fareComponents.put(HotelFareComponent.BF, perRoomPerNightPrice);
				fareComponents.put(HotelFareComponent.SBP, perRoomPerNightPrice);
				priceInfo.setDay(i);
				priceInfo.setFareComponents(fareComponents);
				totalRoomPrice += perRoomPerNightPrice;
				priceInfoList.add(priceInfo);
			}
			roomInfo.setTotalPrice(totalRoomPrice);
			roomInfo.setPerNightPriceInfos(priceInfoList);
			roomInfos.add(roomInfo);
		}
		TravelBoutiqueUtil.updatePriceWithMarkup(roomInfos, sourceConfigOutput);
		return roomInfos;
	}

	public void doDetailSearch(HotelInfo hInfo) throws IOException {
		listener = new RestAPIListener("");
		HotelDetailRequest detailRequest = createDetailRequest(hInfo);
		if (CollectionUtils.isEmpty(hInfo.getFacilities())
				&& BooleanUtils.isTrue(hInfo.getMiscInfo().getIsSupplierMapped())) {
			doNewStaticDataSearch(hInfo, detailRequest);
		}
		if (CollectionUtils.isEmpty(hInfo.getFacilities())) {
			doStaticDataSearch(hInfo, detailRequest);
		}
		doRoomInfoSearch(hInfo, detailRequest);
	}

	private HotelDetailRequest createDetailRequest(HotelInfo hInfo) throws IOException {

		HotelDetailRequest detailRequest = new HotelDetailRequest();
		detailRequest.setEndUserIp(TravelBoutiqueUtil.IP);
		detailRequest.setHotelCode(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		detailRequest.setResultIndex(hInfo.getOptions().get(0).getMiscInfo().getResultIndex().toString());
		detailRequest.setTraceId(hInfo.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		detailRequest.setTokenId(getCachedToken());
		return detailRequest;

	}

	private void doStaticDataSearch(HotelInfo hInfo, HotelDetailRequest detailRequest) throws IOException {

		HttpUtils httpUtils = null;
		HotelDetailResponse detailResponse = null;
		try {
			httpUtils = TravelBoutiqueUtil.getRequest(detailRequest, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_STATIC_DATA));
			detailResponse = httpUtils.getResponse(HotelDetailResponse.class).orElse(null);
			createDetailResponse(detailResponse, hInfo);
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TBO.name())
					.postData(GsonUtils.getGson().toJson(detailRequest)).prefix("Static")
					.requestType(BaseHotelConstants.DETAILSEARCH).responseString(httpUtils.getResponseString())
					.urlString(httpUtils.getUrlString()).logKey(searchQuery.getSearchId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
	}

	private void doNewStaticDataSearch(HotelInfo hInfo, HotelDetailRequest detailRequest) throws IOException {
		HttpUtils httpUtils = null;
		HotelStaticDetailResponse hotelDetailResponse = null;
		HotelStaticDetailRequest hotelStaticDetailRequest = null;
		if (StringUtils.isNotBlank(supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_STATIC_DETAIL_DATA))) {
			try {
				hotelStaticDetailRequest = getNewDetailRequest(detailRequest);
				httpUtils = TravelBoutiqueUtil.getRequest(hotelStaticDetailRequest, supplierConf,
						supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_STATIC_DETAIL_DATA));
				hotelDetailResponse = httpUtils.getResponse(HotelStaticDetailResponse.class).orElse(null);
				createNewDetailResponse(hotelDetailResponse, hInfo);
			} finally {
				listener.addLog(LogData.builder().key(searchQuery.getSearchId())
						.logData(GsonUtils.getGson().toJson(hotelStaticDetailRequest))
						.type("TBO-HotelDetail-new-Static-Req").build());
				listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(httpUtils.getResponseString())
						.type("TBO-HotelDetail-new-Static-Res").build());
			}
		}
	}

	private void createNewDetailResponse(HotelStaticDetailResponse hotelDetailResponse, HotelInfo hInfo) {
		if (hotelDetailResponse != null) {
			String hotelData = hotelDetailResponse.getHotelData();
			StringReader reader = new StringReader(hotelData);
			JAXBContext jaxbContext;
			try {
				jaxbContext = JAXBContext.newInstance(ArrayOfBasicPropertyInfo.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				ArrayOfBasicPropertyInfo baseResponse = (ArrayOfBasicPropertyInfo) jaxbUnmarshaller.unmarshal(reader);
				if (baseResponse != null && baseResponse.getBasicPropertyInfo() != null) {
					BasicPropertyInfo basicPropertyInfo = baseResponse.getBasicPropertyInfo().get(0);
					VendorMessage descriptionVendorMessage =
							TravelBoutiqueUtil.getVendorMessageFromProperty(basicPropertyInfo, "Hotel Description");
					VendorMessage facilityVendorMessage =
							TravelBoutiqueUtil.getVendorMessageFromProperty(basicPropertyInfo, "Facilities");
					VendorMessage imagesVendorMessage =
							TravelBoutiqueUtil.getVendorMessageFromProperty(basicPropertyInfo, "Hotel Pictures");

					String description = null;
					List<String> facilities = null;
					List<String> fullImages = null;
					if (descriptionVendorMessage != null) {
						description = (String) descriptionVendorMessage.getSubSection().get(0).getParagraph().get(0)
								.getText().getContent().get(0);
					}
					if (facilityVendorMessage != null) {
						facilities = facilityVendorMessage.getSubSection().stream().map(
								subSection -> (String) subSection.getParagraph().get(0).getText().getContent().get(0))
								.collect(Collectors.toList());
					}
					if (imagesVendorMessage != null) {
						List<SubSection> subSections = imagesVendorMessage.getSubSection();
						fullImages = subSections.stream().map(section -> {
							Paragraph paragraph = section.getParagraph().stream()
									.filter(predicate -> predicate.getType().equalsIgnoreCase("FullImage")).findFirst()
									.orElse(null);
							if (paragraph != null) {
								return paragraph.getUrl();
							}
							return null;
						}).filter(image -> image != null).collect(Collectors.toList());
					}

					hInfo.setDescription(description);
					hInfo.setFacilities(facilities);
					if (!CollectionUtils.isEmpty(fullImages)) {
						List<Image> hotelImages = new ArrayList<>();
						for (String image : fullImages) {
							Image hotelImage = Image.builder().bigURL(image).build();
							hotelImages.add(hotelImage);
						}
						hInfo.setImages(hotelImages);
					}
				}

			} catch (JAXBException e) {
				log.error("Error in parsing new Hotel Static Response for hotelid {}", hInfo.getId());
			}
		}
	}

	private HotelStaticDetailRequest getNewDetailRequest(HotelDetailRequest detailRequest) {
		HotelStaticDetailRequest hotelStaticDetailRequest =
				HotelStaticDetailRequest.builder().CityId(supplierRegionInfo.getRegionId())
						.ClientId(supplierConf.getHotelSupplierCredentials().getClientId())
						.HotelId(detailRequest.getHotelCode()).EndUserIp(detailRequest.getEndUserIp())
						.TokenId(detailRequest.getTokenId()).build();
		return hotelStaticDetailRequest;
	}

	private void doRoomInfoSearch(HotelInfo hInfo, HotelDetailRequest detailRequest) throws IOException {

		HttpUtils httpUtils = null;
		HotelRoomResponse roomResponse = null;
		try {
			httpUtils = TravelBoutiqueUtil.getRequest(detailRequest, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_ROOM_URL));
			listener.addLog(LogData.builder().key(searchQuery.getSearchId())
					.logData(GsonUtils.getGson().toJson(detailRequest)).type("TBO-HotelDetail-Room-Req").build());

			roomResponse = httpUtils.getResponse(HotelRoomResponse.class).orElse(null);
			if (roomResponse.isSessionExpired()) {
				log.error("TBO Session Expired SearchId {}", searchQuery.getSearchId());
				return;
			}
			createRoomDetailResponse(roomResponse, hInfo);
		} finally {
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(httpUtils.getResponseString())
					.type("TBO-HotelDetail-Room-Res").build());
		}
	}

	private void createDetailResponse(HotelDetailResponse detailResponse, HotelInfo hInfo) {

		HotelDetail hotelDetail = detailResponse.getHotelInfoResult().getHotelDetails();
		hInfo.setDescription(hotelDetail.getDescription());
		hInfo.setFacilities(hotelDetail.getHotelFacilities());
		if (!CollectionUtils.isEmpty(hotelDetail.getImages())) {
			List<Image> hotelImages = new ArrayList<>();
			for (String image : hotelDetail.getImages()) {
				Image hotelImage = Image.builder().bigURL(image).build();
				hotelImages.add(hotelImage);
			}
			hInfo.setImages(hotelImages);
		}
	}

	private void createRoomDetailResponse(HotelRoomResponse roomResponse, HotelInfo hInfo) {
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());

		List<Option> options = new ArrayList<>();
		Map<String, RoomInfo> roomInfoMap = new HashMap<>();

		if (roomResponse == null || roomResponse.getGetHotelRoomResult() == null) {
			log.info("Error in getting Room Response for searchId {} , hotel {}", searchQuery.getSearchId(),
					hInfo.getName());
			return;
		}

		boolean isUnderCancellationAllowedForAgent = roomResponse.isUnderCancellationAllowedForAgent();
		for (HotelRoomDetail roomDetail : roomResponse.getGetHotelRoomResult().getHotelRoomsDetails()) {

			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setId(roomDetail.getRoomTypeCode());
			roomInfo.setRoomType(roomDetail.getRoomTypeName());
			roomInfo.setRoomCategory(roomDetail.getRoomTypeName());
			roomInfo.setPerNightPriceInfos(TravelBoutiqueUtil.getPriceInfos(roomDetail, numberOfNights));
			roomInfo.setTotalPrice(roomDetail.getPrice().getPublishedPrice());
			roomInfo.setRoomAmenities(roomDetail.getAmenity());
			roomInfo.setDeadlineDateTime(roomDetail.getLastCancellationDate());
			roomInfo.setCancellationPolicy(TravelBoutiqueUtil.getCancellationPolicy(roomDetail, roomInfo));
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
			setMealBasis(roomInfo, roomDetail.getAmenities(), searchQuery);
			RoomMiscInfo roomMiscInfo = RoomMiscInfo.builder()
					.price(TravelBoutiqueUtil.getRoomPrice(roomDetail.getPrice())).roomIndex(roomDetail.getRoomIndex())
					.ratePlanCode(roomDetail.getRatePlanCode()).roomTypeCode(roomDetail.getRoomTypeCode())
					.bedTypes(TravelBoutiqueUtil.getBedTypes(roomDetail.getBedTypes()))
					.roomTypeName(roomDetail.getRoomTypeName())
					.isUnderCancellationAllowedForAgent(isUnderCancellationAllowedForAgent)
					.categoryId(roomDetail.getCategoryId()).build();
			roomInfo.setMiscInfo(roomMiscInfo);

			roomInfoMap.put(roomDetail.getCategoryId() + "##" + roomDetail.getRoomIndex().toString(), roomInfo);

		}
		options.addAll(getOptionList(roomInfoMap, roomResponse));
		searchQuery.setSourceId(HotelSourceType.TBO.getSourceId());

		HotelUtils.setBufferTimeinCnp(options, searchQuery);
		hInfo.setOptions(options);

		hInfo.getOptions().get(0).getMiscInfo().setIsDetailHit(true);
	}

	private List<Option> getOptionList(Map<String, RoomInfo> roomInfoMap, HotelRoomResponse roomResponse) {

		List<RoomCombinations> comboList = roomResponse.getGetHotelRoomResult().getRoomCombinationsArray();

		if (comboList == null) {
			RoomCombinations combo = roomResponse.getGetHotelRoomResult().getRoomCombinations();
			comboList = new ArrayList<RoomCombinations>();
			comboList.add(combo);
		}

		List<List<RoomInfo>> roomInfosList = new ArrayList<>();
		List<Option> options = new ArrayList<>();

		for (RoomCombinations combo : comboList) {
			if (combo.getInfoSource().equals("FixedCombination")) {

				List<RoomCombination> roomCombos = combo.getRoomCombination();
				for (RoomCombination roomCombo : roomCombos) {
					List<RoomInfo> roomInfos = new ArrayList<>();
					List<String> indexes = roomCombo.getRoomIndex();

					for (String index : indexes) {
						roomInfos.add(roomInfoMap.get(combo.getCategoryId() + "##" + index));
					}
					roomInfosList.add(roomInfos);
				}
			} else if (combo.getInfoSource().equals("OpenCombination")) {
				/*
				 * Getting Custom Room Combinations
				 */
				roomInfosList.addAll(getRoomCombosForOpenCombination(combo, roomResponse, roomInfoMap));
			}
		}


		for (List<RoomInfo> roomInfos : roomInfosList) {
			LocalDateTime fullRefundCancellationDeadlineDatetime =
					TravelBoutiqueUtil.getFullRefundCancellationDeadlineDatetime(roomInfos);
			OptionMiscInfo dummyOptionMiscInfo = hInfo.getOptions().get(0).getMiscInfo();

			OptionMiscInfo miscInfo = OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierId())
					.secondarySupplier(supplierConf.getBasicInfo().getSupplierId())
					.sourceId(supplierConf.getBasicInfo().getSourceId())
					.resultIndex(dummyOptionMiscInfo.getResultIndex())
					.supplierHotelId(dummyOptionMiscInfo.getSupplierHotelId())
					.supplierSearchId(dummyOptionMiscInfo.getSupplierSearchId()).isDetailHit(true)
					.categoryId(roomInfos.get(0).getMiscInfo().getCategoryId()).build();

			String optionId = RandomStringUtils.random(20, true, true);
			TravelBoutiqueUtil.updatePriceWithMarkup(roomInfos, sourceConfigOutput);
			Option option = Option.builder().roomInfos(roomInfos).id(optionId).miscInfo(miscInfo)
					.deadlineDateTime(fullRefundCancellationDeadlineDatetime).build();

			options.add(option);
		}

		return options;
	}

	public List<List<RoomInfo>> getRoomCombosForOpenCombination(RoomCombinations roomCombos,
			HotelRoomResponse roomResponse, Map<String, RoomInfo> roomInfoMap) {

		List<List<RoomInfo>> roomInfosList = new ArrayList<>();
		List<RoomCombination> roomComboList = roomCombos.getRoomCombination();
		Map<Integer, List<String>> map = new HashMap<>();
		/*
		 * 2 elements in array 1) Index of roomIndex in kth list 2) Index of list
		 */

		PriorityQueue<int[]> pq = new PriorityQueue<>((a1, a2) -> {
			RoomInfo r1 = roomInfoMap.get(roomCombos.getCategoryId() + "##" + map.get(a1[1]).get(a1[0]));
			RoomInfo r2 = roomInfoMap.get(roomCombos.getCategoryId() + "##" + map.get(a2[1]).get(a1[0]));

			if (r1.getTotalPrice() < r2.getTotalPrice())
				return -1;
			else if (r1.getTotalPrice() == r2.getTotalPrice())
				return 0;
			return 1;
		});

		for (int i = 0; i < roomComboList.size(); i++) {
			map.put(i, roomComboList.get(i).getRoomIndex());
			pq.offer(new int[] {0, i});
		}

		int optionSize = 0;
		while (!pq.isEmpty() && optionSize < maxOptionSize) {

			List<RoomInfo> rInfoList = pq.stream().map(a -> {
				return roomInfoMap.get(roomCombos.getCategoryId() + "##" + map.get(a[1]).get(a[0]));
			}).collect(Collectors.toList());

			roomInfosList.add(rInfoList);
			int[] polledData = pq.poll();
			if (polledData[0] + 1 == map.get(polledData[1]).size())
				break;
			else
				pq.offer(new int[] {polledData[0] + 1, polledData[1]});
			optionSize++;
		}

		return roomInfosList;

	}

	/*
	 * private void getCombos(RoomCombinations roomCombos , List<RoomCombination> list , List<String> indexList , int
	 * size){
	 * 
	 * if(indexList.size() == size) { RoomCombination roomCombo =
	 * RoomCombination.builder().RoomIndex(indexList).build(); list.add(roomCombo); return; } List<String> kIndexList =
	 * roomCombos.getRoomCombination().get(indexList.size()).getRoomIndex(); for(int i = 0 ; i < kIndexList.size() ;
	 * i++) {
	 * 
	 * indexList.add(kIndexList.get(i)); getCombos(roomCombos , list , indexList , size);
	 * indexList.remove(indexList.size()-1); } }
	 */


	public void setMealBasis(RoomInfo roomInfo, List<String> facilities, HotelSearchQuery searchQuery) {
		if (CollectionUtils.isEmpty(facilities)) {
			roomInfo.setMealBasis("Room Only");
			return;
		}

		if (facilities.get(0).contains(",")) {
			facilities = Arrays.asList(facilities.get(0).split(","));
		}
		Map<String, String> mealBasis = cacheHandler.fetchMealInfoFromAerospike(searchQuery, new HashSet<>(facilities));
		for (String amenity : facilities) {
			if (!org.springframework.util.ObjectUtils.isEmpty(roomInfo.getMealBasis())
					&& !roomInfo.getMealBasis().equals("Room Only")) {
				return;
			}
			roomInfo.setMealBasis(mealBasis.getOrDefault(amenity.toUpperCase(), "Room Only"));
		}
	}

}
