package com.tgs.services.hms.sources.tbo;

import java.io.IOException;

import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.dbmodel.DbHotelSupplierInfo;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.hms.jparepository.HotelSupplierInfoService;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class TravelBoutiqueBaseService {
	
	protected HMSCachingServiceCommunicator cacheService;
	protected HotelSupplierInfoService supplierService;

	public String getCachedToken() throws IOException {
		
		HotelSupplierConfiguration supplierConf = HotelSupplierConfigurationHelper
				.getSupplierConfiguration(TravelBoutiqueUtil.TBO_SUPPLIER_ID);
		return supplierConf.getHotelSupplierCredentials().getToken();
		
	}
	
	public void storeToken() throws IOException {
		
		HotelSupplierInfo supplierInfo = HotelSupplierConfigurationHelper
				.getSupplierInfo(TravelBoutiqueUtil.TBO_SUPPLIER_ID);
		
		HotelSupplierConfiguration supplierConf = HotelSupplierConfigurationHelper
				.getSupplierConfiguration(TravelBoutiqueUtil.TBO_SUPPLIER_ID);
		
		String tokenId = TravelBoutiqueUtil.fetchNewToken(supplierConf);
		supplierInfo.getCredentialInfo().setToken(tokenId);
		supplierService.save(new DbHotelSupplierInfo().from(supplierInfo));
		
	}
	

}
