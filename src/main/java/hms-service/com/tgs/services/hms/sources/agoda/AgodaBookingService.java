package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cms.datamodel.creditcard.CreditCardFilter;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSurcharge;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingDetailsRequestV2;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingDetailsResponseV2;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingRequestV3;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingRequestV3.BookingDetails;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingRequestV3.BookingDetails.Hotel.Rooms.Room.GuestDetails;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingRequestV3.BookingDetails.Hotel.Rooms.Room.GuestDetails.GuestDetail;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingRequestV3.CustomerDetail;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingRequestV3.CustomerDetail.Phone;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingRequestV3.PaymentDetails;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingRequestV3.PaymentDetails.CreditCardInfo;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingResponseV3;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingResponseV3.BookingDetails.Booking;
import com.tgs.services.hms.datamodel.agoda.precheck.HotelRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.RateRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.RoomRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.SurchargeRequest;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.helper.HotelAlertType;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class AgodaBookingService extends AgodaBaseService {

	private Order order;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelInfo hInfo;
	private BookingResponseV3 result;
	private HotelOrderItemCommunicator itemComm;
	private CommercialCommunicator cmsComm;
	private GeneralServiceCommunicator gmsCommunicator;

	private static final int maxAttempts = 12;
	private static final int sleepTime = 1000 * 10;

	public boolean book() throws IOException, JAXBException, InterruptedException {
		HttpUtils httpUtils = null;
		try {

			listener = new RestAPIListener("");
			BookingRequestV3 bookingRequest = getBookingRequest();
			String xmlRequest = AgodaMarshaller.marshallXml(bookingRequest);
			httpUtils = AgodaUtil.getHttpUtils(xmlRequest, HotelUrlConstants.BOOK_URL, null, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			log.info("Response for booking {} for request {} is {}", order.getBookingId(), xmlRequest, xmlResponse);

			if (xmlResponse == null) {
				log.info("Unable to get response {}", bookingRequest, xmlResponse);
			}
			result = AgodaMarshaller.unmarshallBookingResponse(xmlResponse);
			log.info("Unmarshalled response for booking {} is {}", order.getBookingId(),
					GsonUtils.getGson().toJson(result));
			return updateBookingStatus(result);
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.BOOK.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(result)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.AGODA.name())
						.headerParams(httpUtils.getHeaderParams()).requestType(BaseHotelConstants.BOOKING)
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(order.getBookingId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private boolean updateBookingStatus(BookingResponseV3 result)
			throws JAXBException, IOException, InterruptedException {

		if (Objects.nonNull(result.getErrorMessages())
				&& CollectionUtils.isNotEmpty(result.getErrorMessages().getErrorMessage())
				&& Objects.nonNull(result.getErrorMessages().getErrorMessage().get(0))) {

			String value = AgodaConstants.PRE_AUTH_ERROR.getValue();
			String errorSubId = result.getErrorMessages().getErrorMessage().get(0).getSubid();
			log.info("Error response for booking {} is {}. Value {}, error sub id {}", order.getBookingId(),
					GsonUtils.getGson().toJson(result.getErrorMessages()), value, errorSubId);
			if (!StringUtils.isBlank(errorSubId) && errorSubId.equals(value)) {
				hInfo.getMiscInfo().setAlertType(HotelAlertType.INSUFFICIENT_BALANCE_ALERT.getAlert());
				hInfo.getMiscInfo()
						.setSupplierBookingFailedReason(result.getErrorMessages().getErrorMessage().get(0).getValue());
				return false;
			}
		}
		if (Objects.isNull(result.getErrorMessages()) && result.getStatus().equals("200")) {
			if (result.getBookingDetails() != null
					&& CollectionUtils.isNotEmpty(result.getBookingDetails().getBooking())) {
				Booking booking = result.getBookingDetails().getBooking().get(0);
				hInfo.getMiscInfo().setSupplierBookingId(booking.getId());
				hInfo.getMiscInfo().setSupplierBookingReference(booking.getId());
				hInfo.getMiscInfo().setSupplierBookingUrl(booking.getSelfservice());
				// setting agoda booking id in correlation field such that in case of pending booking so that we can
				// store agoda
				// booking id in our system for future job of updating booking status
				hInfo.getMiscInfo().setTempSupplierBookingId(booking.getId());
				/*
				 * Adding supplier Booking URL to note is temporary measure Until UI makes supplierBookingId a hyperlink
				 * on manage-carts
				 */
				gmsCommunicator.addNote(Note.builder().noteType(NoteType.SUPPLIER_MESSAGE)
						.noteMessage(booking.getSelfservice()).bookingId(order.getBookingId()).build());

				return BookingDetailResponse(result);
			}
		}
		return false;
	}

	private boolean BookingDetailResponse(BookingResponseV3 result)
			throws JAXBException, IOException, InterruptedException {


		BookingDetailsRequestV2 bookingRequest = getBookingDetailRequest(result);
		HttpUtils httpUtils = null;
		try {
			String xmlRequest = AgodaMarshaller.marshallXml(bookingRequest);
			log.info("Booking Detail Request For BookingId {} is {}", order.getBookingId(), bookingRequest);

			int numberOfAttempts = 0;
			while (numberOfAttempts < maxAttempts) {
				httpUtils = AgodaUtil.getHttpUtils(xmlRequest, HotelUrlConstants.BOOKING_DETAIL, null, supplierConf);
				String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
				BookingDetailsResponseV2 bookingDetailResponse =
						AgodaMarshaller.unmarshallBookingDetailResponse(xmlResponse);
				log.info("Response of Agoda {} ", bookingDetailResponse);
				if (bookingDetailResponse.getStatus().equals("200")) {
					String status = bookingDetailResponse.getBookings().getBooking().get(0).getStatus();

					if (BooleanUtils.isTrue(hInfo.getMiscInfo().getIsBnplBooking())) {
						hInfo.getMiscInfo().setSupplierBnplStatus(status);
					}

					if (status.equalsIgnoreCase("BookingCharged") || status.equalsIgnoreCase("BookingConfirmed")) {
						updateHotelReferenceNumber(bookingDetailResponse);
						hInfo.getMiscInfo().setTempSupplierBookingId(null);
						/*
						 * It means booking is successful so returning true Also updating order status to success.
						 */

						itemComm.updateOrderItem(hInfo, order, HotelItemStatus.SUCCESS);
						return true;
					}
				} else
					break;
				Thread.sleep(sleepTime);
				numberOfAttempts++;
			}
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.AGODA.name())
					.headerParams(httpUtils.getHeaderParams()).requestType(BaseHotelConstants.RETRIEVE_BOOKING)
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}

		return false;
	}


	private BookingDetailsRequestV2 getBookingDetailRequest(BookingResponseV3 bookResponse) {

		BookingDetailsRequestV2 bookindDetailRequest = new BookingDetailsRequestV2();
		bookindDetailRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		bookindDetailRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		bookindDetailRequest.setBookingID(Arrays.asList(bookResponse.getBookingDetails().getBooking().get(0).getId()));
		return bookindDetailRequest;
	}


	private BookingRequestV3 getBookingRequest() {

		BookingRequestV3 bookingRequest = new BookingRequestV3();
		try {
			bookingRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
			bookingRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
			bookingRequest.setCustomerDetail(getCustomerDetails());
			bookingRequest.setBookingDetails(getBookingDetails());
			bookingRequest.setPaymentDetails(getPaymentDetails());
		} catch (Exception ex) {
			if (searchQuery == null)
				log.info("Search query is null for booking id {}", order.getBookingId());
			log.info("Exception while creating booking request for  booking id {}, hotel info {}, search query {}",
					order.getBookingId(), GsonUtils.getGson().toJson(hInfo), searchQuery, ex);
			throw ex;
		}
		return bookingRequest;
	}


	private PaymentDetails getPaymentDetails() {

		PaymentDetails paymentDetail = new PaymentDetails();

		CreditCardFilter cardFilter = CreditCardFilter.builder().supplierId(HotelSourceType.AGODA.name())
				.product(Product.HOTEL.getCode()).build();
		List<com.tgs.services.cms.datamodel.creditcard.CreditCardInfo> tgsCreditCardInfoList =
				cmsComm.getCreditCardInfoList(cardFilter);
		if (CollectionUtils.isEmpty(tgsCreditCardInfoList)) {
			log.info("Payment Details IS Empty for supplier AGODA");
			return null;
		}
		com.tgs.services.cms.datamodel.creditcard.CreditCardInfo tgsCreditCardInfo = tgsCreditCardInfoList.get(0);
		paymentDetail.setCreditCardInfo(null);
		CreditCardInfo creditCardInfo = new CreditCardInfo();
		String cardType = tgsCreditCardInfo.getCardType().name();
		cardType = AgodaConstants.valueOf(cardType.toUpperCase()).getValue();
		creditCardInfo.setCardtype(cardType);
		creditCardInfo.setCountryOfIssue("IN");
		creditCardInfo.setCvc(tgsCreditCardInfo.getCvv());
		/*
		 * In Db Expiry Date is in the form : 12/2021 While Agoda expects it as 122021
		 */
		String expiryDate = Arrays.stream(tgsCreditCardInfo.getExpiry().split("/")).collect(Collectors.joining(""));
		creditCardInfo.setExpiryDate(expiryDate);
		creditCardInfo.setHolderName(tgsCreditCardInfo.getHolderName());
		creditCardInfo.setIssuingBank(tgsCreditCardInfo.getBankName());
		creditCardInfo.setNumber(tgsCreditCardInfo.getCardNumber());
		paymentDetail.setCreditCardInfo(creditCardInfo);
		hInfo.getMiscInfo().setCreditCardAppliedId(tgsCreditCardInfo.getId());

		return paymentDetail;
	}


	private BookingDetails getBookingDetails() {

		BookingDetails bookingDetails = new BookingDetails();
		bookingDetails.setSearchid(hInfo.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		bookingDetails.setTag(order.getBookingId());
		bookingDetails.setCheckIn(searchQuery.getCheckinDate().toString());
		bookingDetails.setCheckOut(searchQuery.getCheckoutDate().toString());
		bookingDetails.setHotel(getHotel());
		bookingDetails.setUserCountry("IN");
		bookingDetails.setAllowDuplication(true);
		return bookingDetails;
	}


	private HotelRequest getHotel() {

		HotelRequest agodaHotel = new HotelRequest();
		agodaHotel.setId(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		agodaHotel.getRooms();
		setRooms(agodaHotel, hInfo);
		RoomInfo roomInfo = hInfo.getOptions().get(0).getRoomInfos().get(0);
		RoomRequest room = agodaHotel.getRooms().getRoom().get(0);
		room.setGuestDetails(getGuestDetails());

		if (!Objects.isNull(roomInfo.getMiscInfo().getSurcharges())
				&& roomInfo.getMiscInfo().getSurcharges().size() > 0) {
			SurchargeRequest surchargeRequest = new SurchargeRequest();
			List<SurchargeRequest.Surcharge> surchargeList = new ArrayList<>();
			for (RoomSurcharge roomSurcharge : roomInfo.getMiscInfo().getSurcharges()) {
				SurchargeRequest.Surcharge surcharge = new SurchargeRequest.Surcharge();
				surcharge.setId(roomSurcharge.getId());

				RateRequest rate = new RateRequest();
				rate.setExclusive(roomSurcharge.getRate().getExclusive());
				rate.setInclusive(roomSurcharge.getRate().getInclusive());
				rate.setTax(roomSurcharge.getRate().getTax());
				rate.setFees(roomSurcharge.getRate().getFees());
				surcharge.setRate(rate);
				surchargeList.add(surcharge);

			}
			surchargeRequest.setSurcharge(surchargeList);
			room.setSurcharges(surchargeRequest);
		}

		return agodaHotel;

	}


	private GuestDetails getGuestDetails() {

		GuestDetails guestDetails = new GuestDetails();

		List<GuestDetail> guestDetailList = new ArrayList<>();
		for (RoomInfo roomInfo : hInfo.getOptions().get(0).getRoomInfos()) {
			for (TravellerInfo travellerInfo : roomInfo.getTravellerInfo()) {
				Boolean isChild = travellerInfo.getPaxType().equals(PaxType.CHILD) ? true : false;
				if (!isChild) {
					GuestDetail guestDetail = new GuestDetail();
					guestDetail.setGender(getGender(travellerInfo.getTitle()));
					guestDetail.setFirstName(travellerInfo.getFirstName());
					guestDetail.setLastName(travellerInfo.getLastName());
					guestDetail.setTitle(travellerInfo.getTitle() + ".");
					guestDetail.setCountryOfPassport("IN");
					// Boolean isChild = travellerInfo.getPaxType().equals(PaxType.CHILD) ? true : false;
					// if(!isChild) {
					guestDetail.setTitle(travellerInfo.getTitle() + ".");
					// }else guestDetail.setTitle("");
					guestDetail.setIsChild(isChild);
					guestDetailList.add(guestDetail);
				}
			}
		}
		guestDetails.setGuestDetail(guestDetailList);
		return guestDetails;
	}


	private CustomerDetail getCustomerDetails() {

		CustomerDetail customerDetails = new CustomerDetail();

		customerDetails.setEmail(order.getDeliveryInfo().getEmails().get(0));

		Phone phone = new Phone();

		phone.setCountryCode("91");
		phone.setNumber(order.getDeliveryInfo().getContacts().get(0));

		customerDetails.setPhone(phone);
		TravellerInfo travellerInfo = hInfo.getOptions().get(0).getRoomInfos().get(0).getTravellerInfo().get(0);
		customerDetails.setFirstName(travellerInfo.getFirstName());
		customerDetails.setLastName(travellerInfo.getLastName());
		customerDetails.setTitle(travellerInfo.getTitle() + ".");
		customerDetails.setNewsletter(false);
		customerDetails.setLanguage("en-us");
		return customerDetails;
	}

	private String getGender(String title) {
		if (title.equalsIgnoreCase("Mrs") || title.equalsIgnoreCase("Ms"))
			return "Female";
		else
			return "Male";
	}

	public void updateOrderStatus() {

		String supplierBookingId = hInfo.getMiscInfo().getTempSupplierBookingId();
		if (supplierBookingId == null) {
			log.error("no supplier id present in system for booking id {}", order.getBookingId());
			return;
		}
		BookingDetailsRequestV2 bookindDetailRequest = new BookingDetailsRequestV2();
		bookindDetailRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		bookindDetailRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		bookindDetailRequest.setBookingID(Arrays.asList(supplierBookingId));
		BookingDetailsResponseV2 bookingDetailResponse = getAgodaBookingDetailResponse(bookindDetailRequest);
		if (bookingDetailResponse.getStatus().equals("200")) {
			String status = bookingDetailResponse.getBookings().getBooking().get(0).getStatus();
			if (status.equalsIgnoreCase("BookingCharged") || status.equalsIgnoreCase("BookingConfirmed")) {
				/*
				 * It means booking is successful so updating order status to success.
				 */
				hInfo.getMiscInfo().setSupplierBookingId(
						bookingDetailResponse.getBookings().getBooking().get(0).getBookingID() + "");
				hInfo.getMiscInfo().setSupplierBookingReference(
						bookingDetailResponse.getBookings().getBooking().get(0).getBookingID() + "");
				hInfo.getMiscInfo().setTempSupplierBookingId(null);

				updateHotelReferenceNumber(bookingDetailResponse);

				log.info("booking status updated for booking id {}", order.getBookingId());
				itemComm.updateOrderItem(hInfo, order, HotelItemStatus.SUCCESS);
				return;
			} else {
				if (!(status.equalsIgnoreCase("BookingConfirmed"))) {
					hInfo.getMiscInfo().setIsFailedFromSupplier(true);
					itemComm.updateOrderItem(hInfo, order, HotelItemStatus.PENDING);
				}
			}
		}
	}

	private BookingDetailsResponseV2 getAgodaBookingDetailResponse(BookingDetailsRequestV2 bookindDetailRequest) {
		HttpUtils httpUtils = null;
		BookingDetailsResponseV2 response = null;
		try {
			String xmlRequest = AgodaMarshaller.marshallXml(bookindDetailRequest);
			httpUtils = AgodaUtil.getHttpUtils(xmlRequest, HotelUrlConstants.BOOKING_DETAIL, null, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			response = AgodaMarshaller.unmarshallBookingDetailResponse(xmlResponse);
		} catch (Exception e) {
			log.error("error while fetching booking detail ", e);
		}
		return response;
	}

	public void updateHotelReferenceNumber(BookingDetailsResponseV2 bookingDetailResponse) {

		if (!bookingDetailResponse.getStatus().equals("200"))
			return;
		String supplierReference = bookingDetailResponse.getBookings().getBooking().get(0).getSupplierReference();
		if (!supplierReference.equalsIgnoreCase("Awaiting") && !supplierReference.equalsIgnoreCase("Acknowledged")) {
			hInfo.getMiscInfo().setHotelBookingReference(supplierReference);
		}
	}

}
