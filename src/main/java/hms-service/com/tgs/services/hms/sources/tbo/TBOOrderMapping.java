package com.tgs.services.hms.sources.tbo;

import lombok.Getter;

@Getter
public enum TBOOrderMapping {

	CONFIRMED("SUCCESS"),
	BOOKFAILED("PENDING"),
	CANCELLED("CANCELLED");
	public String getStatus() {
		return this.getCode();
	}

	private String code;

	TBOOrderMapping(String code) {
		this.code = code;
	}
}
