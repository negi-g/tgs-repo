package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomAdditionalInfo;
import com.tgs.services.hms.datamodel.RoomBenefitType;
import com.tgs.services.hms.datamodel.RoomExtraBenefit;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.State;
import com.tgs.services.hms.datamodel.cleartrip.CleartripHotelDetailRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripHotelInfo;
import com.tgs.services.hms.datamodel.cleartrip.CleartripRoomRate;
import com.tgs.services.hms.datamodel.cleartrip.CleartripRoomType;
import com.tgs.services.hms.datamodel.cleartrip.CleartripSearchRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripSearchResponse;
import com.tgs.services.hms.datamodel.cleartrip.HotelBasicInfo;
import com.tgs.services.hms.datamodel.cleartrip.Locality;
import com.tgs.services.hms.datamodel.cleartrip.PricingElement;
import com.tgs.services.hms.datamodel.cleartrip.RateBreakDown;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Getter
public class CleartripSearchService {

	protected RestAPIListener listener;
	private GeneralServiceCommunicator gsCommunicator;
	private HotelSearchQuery searchQuery;
	private HotelSourceConfigOutput sourceConfig;
	private HotelSupplierConfiguration supplierConfig;
	private CleartripSearchResponse searchResponse;
	private HotelSearchResult searchResult;
	private HotelInfo hotelInfo;
	private HotelSupplierRegionInfo supplierRegionInfo;

	private static final DateTimeFormatter dateTimeFormatter_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final String HOTEL_SEARCH_PREFIX = "/hotels/api/v2/search";
	private static final String HOTEL_DETAIL_PREFIX = "/hotels/api/v2/detail";

	// Possible values are basic, detail and no
	private static String HOTEL_SEARCH_GRANULARITY;
	private static String STATIC_DATA_BASE_URL;

	public void init() {

		gsCommunicator = (GeneralServiceCommunicator) SpringContext.getApplicationContext()
				.getBean("generalServiceCommunicatorImpl");
		STATIC_DATA_BASE_URL =
				supplierConfig.getHotelSupplierCredentials().getSupplierUrl().get(HotelUrlConstants.HOTEL_STATIC_DATA);
		HOTEL_SEARCH_GRANULARITY = sourceConfig.getHotelInfoGranularity();
	}

	@SuppressWarnings({"unchecked"})
	public void doSearch() throws IOException {
		HttpUtilsV2 httpUtils = null;
		try {
			CleartripSearchRequest searchRequest = createSearchRequest();
			httpUtils = CleartripUtils.getResponseURL(searchRequest, supplierConfig);
			httpUtils.setPrintResponseLog(false);
			searchResponse = httpUtils.getResponse(CleartripSearchResponse.class).orElse(null);
			searchResult = createSearchResult(searchResponse);
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.SEARCH.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(searchResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.CLEARTRIP.name())
						.requestType(BaseHotelConstants.SEARCH).responseString(httpUtils.getResponseString())
						.urlString(httpUtils.getUrlString()).postData(httpUtils.getPostData())
						.logKey(searchQuery.getSearchId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	@SuppressWarnings({"unchecked"})
	public void doDetailSearch() throws IOException {

		HttpUtilsV2 httpUtils = null;
		try {
			CleartripSearchRequest searchRequest = createDetailRequest();
			httpUtils = CleartripUtils.getResponseURL(searchRequest, supplierConfig);
			httpUtils.setPrintResponseLog(false);
			searchResponse = httpUtils.getResponse(CleartripSearchResponse.class).orElseGet(null);
			createDetailResult(searchResponse);
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.DETAIL.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(searchResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.CLEARTRIP.name())
						.requestType(BaseHotelConstants.DETAILSEARCH).responseString(httpUtils.getResponseString())
						.urlString(httpUtils.getUrlString()).postData(httpUtils.getPostData())
						.logKey(searchQuery.getSearchId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private CleartripSearchRequest createSearchRequest() {

		String countryCode = getCountryCode();
		String cityName = getCityName();

		return CleartripSearchRequest.builder().country(countryCode).hotelInfo(HOTEL_SEARCH_GRANULARITY)
				.checkInDate(searchQuery.getCheckinDate().format(dateTimeFormatter_YYYY_MM_DD))
				.checkOutDate(searchQuery.getCheckoutDate().format(dateTimeFormatter_YYYY_MM_DD)).city(cityName)
				.suffixOfURL(HOTEL_SEARCH_PREFIX).occupancy(CleartripUtils.getOccupancies(searchQuery)).build();
	}

	private String getCityName() {

		String cityName = searchQuery.getSearchCriteria().getCityName();
		if (Objects.nonNull(supplierRegionInfo) && StringUtils.isNotEmpty(supplierRegionInfo.getSupplierRegionName())) {
			if (supplierRegionInfo.getSupplierRegionName().contains("@")) {
				for (String searchRegion : supplierRegionInfo.getSupplierRegionName().split("@")) {
					if (!searchRegion.equals("NA")) {
						cityName = searchRegion;
						break;
					}
				}
			}
		}
		return cityName;
	}

	private String getCountryCode() {

		String countryName = null;
		if (Objects.isNull(searchQuery.getMiscInfo().getHotelWiseSearchInfo())) {
			if (Objects.nonNull(supplierRegionInfo)
					&& StringUtils.isNotEmpty(supplierRegionInfo.getSupplierRegionName())) {
				String searchRegion = supplierRegionInfo.getSupplierRegionName();
				String regions[] = searchRegion.split("@");
				int count = 0;

				for (String region : regions) {
					if (!region.equals("NA") && count == 2) {
						countryName = region;
						break;
					}
					count++;
				}
			}
			countryName = org.apache.commons.lang3.ObjectUtils.firstNonNull(countryName,
					searchQuery.getSearchCriteria().getCountryName());
		} else {
			countryName = searchQuery.getMiscInfo().getHotelWiseSearchInfo().getCountryName();
		}

		return gsCommunicator.getCountryCode(HotelUtils.toCamelCase(countryName));
	}

	private CleartripHotelDetailRequest createDetailRequest() {

		String countryCode = getCountryCode();
		return CleartripHotelDetailRequest.builder().country(countryCode).sellingCountry(countryCode)
				.sellingCurrency(searchQuery.getSearchPreferences().getCurrency()).hotelInfo(HOTEL_SEARCH_GRANULARITY)
				.checkInDate(searchQuery.getCheckinDate().format(dateTimeFormatter_YYYY_MM_DD))
				.checkOutDate(searchQuery.getCheckoutDate().format(dateTimeFormatter_YYYY_MM_DD))
				.suffixOfURL(HOTEL_DETAIL_PREFIX)
				.hotelId(Objects.nonNull(searchQuery.getMiscInfo().getHotelWiseSearchInfo())
						? searchQuery.getMiscInfo().getHotelWiseSearchInfo().getHotelId()
						: hotelInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId())
				.occupancy(CleartripUtils.getOccupancies(searchQuery)).build();
	}

	private void createDetailResult(CleartripSearchResponse searchResponse) {
		searchResult = HotelSearchResult.builder().build();
		if (searchResponse == null)
			return;

		if (!ObjectUtils.isEmpty(searchResponse.getError())) {
			SystemContextHolder.getContextData().getErrorMessages().add(searchResponse.getError().getErrorMessage());
			log.error("Unable to get search result from Cleartrip due to {} for supplier {} with username {}",
					searchResponse.getError().getErrorMessage(), supplierConfig.getBasicInfo().getSupplierName(),
					supplierConfig.getHotelSupplierCredentials().getUserName());
			return;
		}

		CleartripHotelInfo cHotelInfo = null;
		Set<String> amenities = new HashSet<>();
		try {
			cHotelInfo = searchResponse.getSuccess().getHotels().get(0);
			HotelBasicInfo basicInfo = cHotelInfo.getBasicInfo();
			Integer rating = null;
			if (Objects.nonNull(basicInfo)) {
				Locality locality = basicInfo.getLocality();
				Address address = Address.builder().addressLine1(locality.getAddress()).postalCode(locality.getZip())
						.cityName(locality.getCity()).countryName(locality.getCountry()).stateName(locality.getState())
						.city(City.builder().name(locality.getCity()).build())
						.country(Country.builder().name(locality.getCountry()).build())
						.state(StringUtils.isNotBlank(locality.getState())
								? State.builder().name(locality.getState()).build()
								: null)
						.build();

				GeoLocation geolocation = GeoLocation.builder().latitude(locality.getLocalityLatitude())
						.longitude(locality.getLocalityLongitude()).build();

				hotelInfo.setAddress(address).setGeolocation(geolocation).setDescription(basicInfo.getOverview())
						.setName(basicInfo.getHotelName())
						.setImages(Arrays
								.asList(Image.builder().thumbnail(STATIC_DATA_BASE_URL + basicInfo.getThumbNailImage())
										.build()));
				rating = NumberUtils.isParsable(basicInfo.getStarRating())
						? (int) Double.parseDouble(basicInfo.getStarRating())
						: null;

			}
			hotelInfo.setRating(rating);
			if (Objects.nonNull(searchQuery.getMiscInfo().getHotelWiseSearchInfo())) {
				hotelInfo.setMiscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
						.supplierStaticHotelId(cHotelInfo.getHotelId())
						.searchKeyExpiryTime(LocalDateTime.now().plusMinutes(sourceConfig.getSearchKeyExpirationTime()))
						.build());
			}
			List<Option> optionList = populateRoomInfo(cHotelInfo, amenities, true);
			for (Option option : optionList) {
				option.getMiscInfo().setSecondarySupplier(supplierConfig.getBasicInfo().getSupplierId());
				option.getMiscInfo().setIsDetailHit(true);
			}
			hotelInfo.setOptions(optionList);
			CleartripUtils.setMealBasis(Arrays.asList(hotelInfo), amenities, searchQuery);
		} catch (Exception e) {
			log.info("Unable to parse hotel detail info for hotel {} for search id {}",
					GsonUtils.getGson().toJson(cHotelInfo), searchQuery.getSearchId(), e);
		}
	}

	private HotelSearchResult createSearchResult(CleartripSearchResponse searchResponse) {
		searchResult = HotelSearchResult.builder().build();;
		if (searchResponse == null)
			return searchResult;

		if (!ObjectUtils.isEmpty(searchResponse.getError())) {
			SystemContextHolder.getContextData().getErrorMessages().add(searchResponse.getError().getErrorMessage());
			log.error("Unable to get search result from Cleartrip due to {} for supplier {} with username {}",
					searchResponse.getError().getErrorMessage(), supplierConfig.getBasicInfo().getSupplierName(),
					supplierConfig.getHotelSupplierCredentials().getUserName());
			return searchResult;
		}

		List<HotelInfo> hotels = new ArrayList<>();
		Set<String> amenities = new HashSet<>();
		for (CleartripHotelInfo cHotelInfo : searchResponse.getSuccess().getHotels()) {
			try {
				HotelBasicInfo basicInfo = cHotelInfo.getHotelBasicInfo();
				HotelInfo.HotelInfoBuilder hInfoBuilder = HotelInfo.builder();
				Integer rating = null;
				if (Objects.nonNull(basicInfo)) {
					Locality locality = basicInfo.getLocality();
					Address address = Address.builder().addressLine1(locality.getName()).postalCode(locality.getZip())
							.city(City.builder().name(locality.getCity()).build())
							.country(Country.builder().name(locality.getCountry()).build())
							.state(State.builder().name(locality.getState()).build()).build();

					GeoLocation geolocation = GeoLocation.builder().latitude(locality.getLatitude())
							.longitude(locality.getLongitude()).build();

					hInfoBuilder.address(address).geolocation(geolocation).name(basicInfo.getHotelName())
							.images(Arrays.asList(
									Image.builder().thumbnail(STATIC_DATA_BASE_URL + basicInfo.getThumbNailImage())
											.bigURL(STATIC_DATA_BASE_URL + basicInfo.getThumbNailImage()).build()))
							.build();
					rating = NumberUtils.isParsable(basicInfo.getStarRating())
							? (int) Double.parseDouble(basicInfo.getStarRating())
							: null;

				}
				HotelInfo hInfo = hInfoBuilder.build();
				hInfo.setRating(rating);
				hInfo.setMiscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
						.supplierStaticHotelId(cHotelInfo.getHotelId())
						.searchKeyExpiryTime(LocalDateTime.now().plusMinutes(sourceConfig.getSearchKeyExpirationTime()))
						.build());
				List<Option> optionList = populateRoomInfo(cHotelInfo, amenities, false);
				for (Option option : optionList) {
					option.getMiscInfo().setIsNotRequiredOnDetail(true);
				}
				hInfo.setOptions(optionList);
				hotels.add(hInfo);
			} catch (Exception e) {
				log.info("Unable to parse hotel search info for hotel {} for search id {}",
						GsonUtils.getGson().toJson(cHotelInfo), searchQuery.getSearchId(), e);
			}
		}
		CleartripUtils.setMealBasis(hotels, amenities, searchQuery);
		searchResult.setHotelInfos(hotels);
		return searchResult;
	}

	private List<Option> populateRoomInfo(CleartripHotelInfo cHotelInfo, Set<String> amenities, boolean isDetail) {
		List<CleartripRoomRate> cRoomRates = cHotelInfo.getRoomRates();
		List<Option> options = new ArrayList<>();

		for (CleartripRoomRate roomRate : cRoomRates) {
			amenities.addAll(roomRate.getInclusions());
			Option option = Option.builder().build();
			List<RoomInfo> roomInfos = new ArrayList<>();
			RoomInfo roomInfo = new RoomInfo();
			CleartripRoomType roomType = roomRate.getRoomType();
			roomInfo.setRoomCategory(roomType.getRoomTypeName());
			roomInfo.setRoomType(roomType.getRoomTypeName());
			roomInfo.setRoomAmenities(roomRate.getInclusions());
			roomInfo.setId(roomType.getRoomTypeCode());
			populatePriceInRoomInfo(roomInfo, roomRate.getRateBreakdown(), searchQuery.getRoomInfo().size());
			roomInfo.getMiscInfo().setAmenities(roomRate.getInclusions());
			RoomAdditionalInfo roomAdditionalInfo =
					RoomAdditionalInfo.builder().roomId(roomType.getRoomTypeCode()).build();
			roomInfo.setRoomAdditionalInfo(roomAdditionalInfo);

			if (!CollectionUtils.isEmpty(roomRate.getInclusions())) {
				List<RoomExtraBenefit> roomExtraBenefits = new ArrayList<RoomExtraBenefit>();
				RoomExtraBenefit benefit = new RoomExtraBenefit();
				benefit.setValues(new HashSet<String>(roomRate.getInclusions()));
				roomExtraBenefits.add(benefit);
				Map<RoomBenefitType, List<RoomExtraBenefit>> list =
						new HashMap<RoomBenefitType, List<RoomExtraBenefit>>();
				list.put(RoomBenefitType.PROMOTION, roomExtraBenefits);
				roomInfo.setRoomExtraBenefits(list);
			}

			roomInfos.add(roomInfo);
			CleartripUtils.updatePriceWithClientCommissionComponents(roomInfos, sourceConfig);
			roomInfos = CleartripUtils.createMultipleRoomsOfSameRoomType(roomInfo, searchQuery);
			option.setMiscInfo(OptionMiscInfo.builder().supplierId(supplierConfig.getBasicInfo().getSupplierId())
					.supplierHotelId(cHotelInfo.getHotelId()).sourceId(supplierConfig.getBasicInfo().getSourceId())
					.build());

			option.setRoomInfos(roomInfos);
			if (CollectionUtils.isNotEmpty(roomRate.getInclusions())) {
				option.setInstructions(new ArrayList<>(Arrays.asList(Instruction.builder()
						.type(InstructionType.INCLUSIONS).msg(String.join("--", roomRate.getInclusions())).build())));
			}

			option.getMiscInfo().setBookingCode(roomRate.getBookingCode());
			option.getMiscInfo().setRoomTypeCode(roomType.getRoomTypeCode());
			option.setId(roomType.getRoomTypeCode().replaceAll(":", "_"));
			options.add(option);
		}
		return options;
	}

	public void populatePriceInRoomInfo(RoomInfo roomInfo, List<RateBreakDown> rateBreakDowns, int noOfRooms) {
		int i = 1;
		Double total = 0.0;
		List<PriceInfo> priceInfoList = new ArrayList<>();
		for (RateBreakDown rateBreakDown : rateBreakDowns) {
			PriceInfo priceInfo = new PriceInfo();
			List<PricingElement> pricingElements = rateBreakDown.getPricingElements();
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			for (PricingElement pricingElement : pricingElements) {
				if (pricingElement.getCategory().equals("BF")) {
					fareComponents.put(HotelFareComponent.BF, pricingElement.getAmount() / noOfRooms);
					total += fareComponents.get(HotelFareComponent.BF);
					fareComponents.put(HotelFareComponent.SBP, fareComponents.get(HotelFareComponent.BF));
				}

				if (pricingElement.getCategory().equals("TAX")) {
					fareComponents.put(HotelFareComponent.TSF, pricingElement.getAmount() / noOfRooms);
					total += fareComponents.get(HotelFareComponent.TSF);
					fareComponents.put(HotelFareComponent.TTSF, fareComponents.get(HotelFareComponent.TSF));
				}

				if (pricingElement.getCategory().equals("DIS")) {
					fareComponents.put(HotelFareComponent.SDS, (pricingElement.getAmount() / noOfRooms) * -1);
					total -= fareComponents.get(HotelFareComponent.SDS);
				}
			}

			priceInfo.setDay(i);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
			i++;
		}

		roomInfo.setMiscInfo(RoomMiscInfo.builder().totalBaseAmount(BigDecimal.valueOf(total)).build());
		roomInfo.setPerNightPriceInfos(priceInfoList);
	}
}
