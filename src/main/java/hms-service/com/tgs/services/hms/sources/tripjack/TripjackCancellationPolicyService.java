package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.HotelCancellationPolicyRequest;
import com.tgs.services.hms.restmodel.HotelCancellationPolicyResponse;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class TripjackCancellationPolicyService extends TripjackBaseService {

	private HotelCancellationPolicyResponse cancellationPolicyResponse;
	private HotelInfo hInfo;

	public void searchCancellationPolicy(String logKey, String endpoint) throws IOException {
		HttpUtils httpUtils = null;
		String requestUrl = StringUtils.join(endpoint, TripjackConstant.CANCELLATION_POLICY.value);
		listener = new RestAPIListener("");
		try {
			HotelCancellationPolicyRequest cancellationPolicyRequest = new HotelCancellationPolicyRequest();
			cancellationPolicyRequest.setId(hInfo.getMiscInfo().getCorrelationId());
			cancellationPolicyRequest.setOptionId(hInfo.getOptions().get(0).getId());
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(cancellationPolicyRequest), requestUrl);
			cancellationPolicyResponse = httpUtils.getResponse(HotelCancellationPolicyResponse.class).orElse(null);
			int RoomCount = hInfo.getOptions().get(0).getRoomInfos().size();
			tripjckManagementFee =
					hInfo.getOptions().get(0).getRoomInfos().get(0).getMiscInfo().getFees() * (RoomCount);
			curiocityManagementFee = hInfo.getOptions().get(0).getRoomInfos().get(0).getTotalAddlFareComponents()
					.get(HotelFareComponent.TAF).getOrDefault(HotelFareComponent.MF, 0.0) * (RoomCount);

			if (!ObjectUtils.isEmpty(cancellationPolicyResponse)) {
				setCancellationPolicy(hInfo.getOptions().get(0), cancellationPolicyResponse.getCancellationPolicy());
			}
		} finally {
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(cancellationPolicyResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRIPJACK.name())
					.requestType(BaseHotelConstants.CANCELLATIONPOLICY).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(logKey)
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

		}
	}
}
