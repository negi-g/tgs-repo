package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.time.LocalDateTime;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class DotwHotelBookingCancellationFactory extends AbstractHotelBookingCancellationFactory {

	public DotwHotelBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo, Order order) {
		super(supplierConf, hInfo, order);
	}

	@Override
	public boolean cancelHotel() throws IOException, JAXBException {

		checkIfSupplierCancellationAllowed();
		DotwHotelBookingCancellationService cancellationService = DotwHotelBookingCancellationService.builder()
				.supplierConf(supplierConf).hInfo(hInfo).order(order)
				.build();
		return cancellationService.cancelBooking();
	}

	private void checkIfSupplierCancellationAllowed() {

		HotelCancellationPolicy cp = hInfo.getOptions().get(0).getCancellationPolicy();
		if (cp == null)
			return;
		LocalDateTime currentTime = LocalDateTime.now();
		if (!CollectionUtils.isEmpty(cp.getPenalyDetails())) {
				for (PenaltyDetails pd : cp.getPenalyDetails()) {
					if (pd.getFromDate().isBefore(currentTime) && pd.getToDate() != null
							&& pd.getToDate().isAfter(currentTime)) {
						if (pd.getIsCancellationRestricted() != null && pd.getIsCancellationRestricted()) {
							throw new CustomGeneralException(SystemError.CANCELLATION_RESTRICTED);
						}
				}
		}

	}
	}

	@Override
	public boolean getCancelHotelStatus() throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

}
