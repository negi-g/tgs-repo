package com.tgs.services.hms.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.hms.datamodel.qtech.AdditionalHotelIdMappingInfo;
import com.tgs.services.hms.datamodel.qtech.SupplierHotelIdMapping;
import com.tgs.services.hms.dbmodel.DbSupplierHotelIdMapping;
import com.tgs.services.hms.jparepository.HotelInfoService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SupplierHotelIdMappingHelper extends InMemoryInitializer {

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelInfoService hotelInfoService;
	private Map<String, SupplierHotelIdMapping> hotelIdGlobalMap;

	public SupplierHotelIdMappingHelper() {
		super(null);
	}

	@Override
	public void process() {
		final long limit = 10000;
		Long from = 0L;
		Long to = limit;

		hotelIdGlobalMap = new HashMap<>();
		try {
			for (int offset = 0; offset < 400; offset++) {
				// Pageable page = new PageRequest(offset, pageSize);
				// List<DbSupplierHotelIdMapping> dbHotelIdMappingList = hotelInfoService.findAll(page);
				log.info("Started fetching hotelid mapping from database. Total mapping count {}, From {} - To {}",
						hotelIdGlobalMap.size(), from, to);
				List<DbSupplierHotelIdMapping> dbHotelIdMappingList = hotelInfoService.findByhotelIdBetween(from, to);
				log.info("Finished fetching hotelid mapping from database. Current mapping count {}, From {} - To {}",
						dbHotelIdMappingList.size(), from, to);
				from = to + 1;
				to = to + limit;
				if (CollectionUtils.isNotEmpty(dbHotelIdMappingList)) {
					saveIntoCache(dbHotelIdMappingList);
				}
			}
		} finally {
			log.info("Finished storing data in cache. Total mapping stored in aerospike is {}",
					hotelIdGlobalMap.keySet().size());
		}
	}

	private void saveIntoCache(List<DbSupplierHotelIdMapping> dbHotelIdMappingList) {
		Map<String, SupplierHotelIdMapping> hotelIdAsKeyAndSupplierHotelIdAsValue = getMap(dbHotelIdMappingList);
		hotelIdGlobalMap.putAll(hotelIdAsKeyAndSupplierHotelIdAsValue);
		log.debug("Local Map size after mapping DbSupplierhotelid list to cache map is {}, global map size is {}",
				hotelIdAsKeyAndSupplierHotelIdAsValue.keySet().size(), hotelIdGlobalMap.keySet().size());
		hotelIdAsKeyAndSupplierHotelIdAsValue.forEach((key, value) -> {
			try {
				cacheHandler.storeHotelIdMapping(key, value);
			} catch (Exception e) {
				log.info(
						"Unable to save hotelid mapping data with hotelid {}, supplier hotelid {} into aerospike due to exception {}",
						key, value, e);
			}
		});

	}

	private Map<String, SupplierHotelIdMapping> getMap(List<DbSupplierHotelIdMapping> dbHotelIdMappingList) {
		Map<String, SupplierHotelIdMapping> map = new HashMap<>();
		dbHotelIdMappingList.forEach(dbHotelIdMapping -> {
			AdditionalHotelIdMappingInfo addInfo = AdditionalHotelIdMappingInfo.builder()
					.supplierCity(dbHotelIdMapping.getAdditionalInfo().getSupplierCity())
					.supplierCountry(dbHotelIdMapping.getAdditionalInfo().getSupplierCountry()).build();
			SupplierHotelIdMapping hotelIdMappingInfo = SupplierHotelIdMapping.builder()
					.supplierHotelId(dbHotelIdMapping.getSupplierHotelId()).additionalInfo(addInfo).build();
			map.put(dbHotelIdMapping.getHotelId(), hotelIdMappingInfo);
		});
		return map;
	}

	@Override
	public void deleteExistingInitializer() {

	}

}
