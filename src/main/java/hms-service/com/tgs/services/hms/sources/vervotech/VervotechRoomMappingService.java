package com.tgs.services.hms.sources.vervotech;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import com.google.gson.Gson;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.vervotech.RoomMappingRequest;
import com.tgs.services.hms.datamodel.vervotech.RoomMappingResponse;
import com.tgs.services.hms.datamodel.vervotech.VervotechRoomMappingRequest;
import com.tgs.services.hms.datamodel.vervotech.VervotechRoomMappingResponse;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import io.jsonwebtoken.lang.Collections;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Setter
@Getter
public class VervotechRoomMappingService {

	private HotelSourceConfigOutput sourceConfig;
	private HotelSupplierConfiguration supplierConf;
	private HotelInfo hInfo;
	protected RestAPIListener listener;
	private HotelSearchQuery searchQuery;
	private static final String GET_ROOM_MAPPING_REQUEST = "/api/mapping/rooms";
	private Map<String, String> standardRoomMapping;
	private Gson SINGLETON_GSON;
	private final Map<String, List<RoomInfo>> UNIQUE_ROOM_INFO_MAP = new HashMap<>();
	private final Map<Integer, String> ROOM_INDEX_TO_ROOM_INFO_MAP = new HashMap<>();

	private Gson getSingletonGson() {

		if (SINGLETON_GSON == null) {
			SINGLETON_GSON = GsonUtils.getGson();
		}
		return SINGLETON_GSON;
	}

	public void getRoomMappingInfo() throws IOException {
		HttpUtilsV2 httpUtils = null;
		String postData = null;
		Map<String, String> headerParams = null;
		VervotechRoomMappingResponse roomMappingResponse = null;
		try {
			listener = new RestAPIListener("");
			VervotechRoomMappingRequest roomMappingRequest = createRequestToFetchRoomMappings();
			if (CollectionUtils.isEmpty(roomMappingRequest.getRoomMappingRequest())) {
				log.info(
						"Unable to fetch vervotech room mapping for search id {} and suppliers {} as the request is empty.",
						searchQuery.getSearchId(), hInfo.getOptions().stream()
								.map(option -> option.getMiscInfo().getSupplierId()).collect(Collectors.toSet()));
				return;
			}
			log.info("Started fetching vervotech room mapping for search id {} and suppliers {}. Request is {}",
					searchQuery.getSearchId(), hInfo.getOptions().stream()
							.map(option -> option.getMiscInfo().getSupplierId()).collect(Collectors.toSet()),
					getSingletonGson().toJson(roomMappingRequest));
			VervotechUtil.setCredentials(roomMappingRequest, supplierConf, VervotechConstant.ROOM_SERVICE_TYPE);
			postData = GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null)
					.toJson(roomMappingRequest.getRoomMappingRequest());
			headerParams = roomMappingRequest.getHeaderParams();
			headerParams.put("unicaId", hInfo.getUnicaId());
			httpUtils = HttpUtilsV2.builder()
					.urlString(supplierConf.getHotelSupplierCredentials().getSupplierUrl()
							.get(HotelUrlConstants.HOTEL_ROOM_URL) + roomMappingRequest.getSuffixOfURL())
					.proxy(HotelUtils.getProxyFromConfigurator()).postData(postData).headerParams(headerParams).build();
			httpUtils.setPrintResponseLog(false);
			roomMappingResponse = httpUtils.getResponse(VervotechRoomMappingResponse.class).orElse(null);
			standardRoomMapping = getStandardRoomMapping(roomMappingRequest, roomMappingResponse);
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.DETAIL.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(roomMappingResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.VERVOTECH.name())
						.headerParams(headerParams).requestType(BaseHotelConstants.ROOM_MAPPING)
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private VervotechRoomMappingRequest createRequestToFetchRoomMappings() {

		List<RoomMappingRequest> roomMappingList = new ArrayList<>();
		AtomicInteger indexCount = new AtomicInteger(1);
		for (Option option : hInfo.getOptions()) {
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				VervotechSupplierMapping vervotechSupplierMapping = null;
				boolean isQtechSupplier = option.getMiscInfo().getSourceId() == 1;
				vervotechSupplierMapping = isQtechSupplier
						? VervotechSupplierMapping
								.getVervotechSupplierMappingFromQtechSupplierName(option.getMiscInfo().getSupplierId())
						: VervotechSupplierMapping.getVervotechSupplierMapping(option.getMiscInfo().getSupplierId());
				if (Objects.isNull(vervotechSupplierMapping)) {
					break;
				}

				RoomMappingRequest roomMappingRequest =
						RoomMappingRequest.builder().RoomName(roomInfo.getRoomCategory())
								.providerHotelId(isQtechSupplier ? null : option.getMiscInfo().getSupplierHotelId())
								.Provider(vervotechSupplierMapping.getVervotechSupplierName()).build();
				String uniqueKeyPerRoom = getUniqueRoomInfoKey(roomMappingRequest);
				if (UNIQUE_ROOM_INFO_MAP.containsKey(uniqueKeyPerRoom)) {
					UNIQUE_ROOM_INFO_MAP.get(uniqueKeyPerRoom).add(roomInfo);
				} else {
					roomMappingRequest.setIndex(indexCount.getAndIncrement());
					roomMappingList.add(roomMappingRequest);
					ROOM_INDEX_TO_ROOM_INFO_MAP.put(roomMappingRequest.getIndex(), uniqueKeyPerRoom);
					UNIQUE_ROOM_INFO_MAP.put(uniqueKeyPerRoom, new ArrayList<>(Arrays.asList(roomInfo)));
				}
			}
		}
		return VervotechRoomMappingRequest.builder().suffixOfURL(GET_ROOM_MAPPING_REQUEST)
				.roomMappingRequest(roomMappingList).build();
	}

	private String getUniqueRoomInfoKey(RoomMappingRequest roomMappingRequest) {

		return String.join("_", roomMappingRequest.getRoomName(), roomMappingRequest.getProvider(),
				ObjectUtils.firstNonNull(roomMappingRequest.getProviderHotelId(), ""));
	}

	private Map<String, String> getStandardRoomMapping(VervotechRoomMappingRequest vervotechRoomMappingRequest,
			VervotechRoomMappingResponse roomMappingResponse) {
		Map<String, String> localRoomIdAsKeyAndStandardRoomNameAsValue = new HashMap<>();
		List<String> suppliers = hInfo.getOptions().stream().map(option -> option.getMiscInfo().getSupplierId())
				.distinct().collect(Collectors.toList());

		if (Objects.isNull(roomMappingResponse) || Collections.isEmpty(roomMappingResponse.getResult())) {
			log.info("Unable to fetch vervotech room mapping for search id {} and suppliers {}",
					searchQuery.getSearchId(), suppliers);
			return null;
		}

		log.info("Finished fetching vervotech room mapping for search id {} and suppliers {}. Response is {}",
				searchQuery.getSearchId(), suppliers, getSingletonGson().toJson(roomMappingResponse));


		for (RoomMappingResponse roomMappingResult : roomMappingResponse.getResult()) {
			List<Integer> roomIndices = roomMappingResult.getRoomIndexes();
			for (Integer roomIndex : roomIndices) {
				String uniqueKeyPerRoom = ROOM_INDEX_TO_ROOM_INFO_MAP.get(roomIndex);
				List<RoomInfo> roomInfos = UNIQUE_ROOM_INFO_MAP.get(uniqueKeyPerRoom);
				for (RoomInfo roomInfo : roomInfos) {
					localRoomIdAsKeyAndStandardRoomNameAsValue.put(roomInfo.getId(),
							roomMappingResult.getStandardName());
				}
			}
		}
		return localRoomIdAsKeyAndStandardRoomNameAsValue;
	}
}
