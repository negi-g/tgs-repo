package com.tgs.services.hms.sources.desiya;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;

@Service

public class DesiyaPriceValidationFactory extends AbstractHotelPriceValidationFactory {
	
	@Autowired
	HotelCacheHandler cacheHandler;
	
	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public DesiyaPriceValidationFactory(HotelSearchQuery query, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
		super(query, hotel, hotelSupplierConf, bookingId);
	}

	@Override
	public Option getOptionWithUpdatedPrice() throws Exception {
		return this.getHInfo().getOptions().get(0);
	}
	
	

}
