package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.tripjack.TJBookingDetailResponse;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingRequest;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class TripjackBookingService extends TripjackBaseService {

	private Order order;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelInfo hInfo;
	private HotelOrderItemCommunicator itemComm;
	private BookingResponse bookingResponse;
	private TJBookingDetailResponse bookingDetailResponse;

	public boolean book() throws IOException, JAXBException, InterruptedException {
		String requestUrl = StringUtils.join(endpoint, TripjackConstant.BOOKING.value);
		HttpUtils httpUtils = null;
		try {
			listener = new RestAPIListener("");
			BookingRequest bookingRequest = getBookingRequest(hInfo.getOptions().get(0));
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(bookingRequest), requestUrl);
			bookingResponse = httpUtils.getResponse(BookingResponse.class).orElse(null);
			if (bookingResponse.getStatus().getHttpStatus() == 200) {
				return getBookingStatus();
			}
		} finally {
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(bookingResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRIPJACK.name())
					.requestType(BaseHotelConstants.BOOKING).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
		return false;
	}

	private boolean getBookingStatus() throws IOException, InterruptedException {
		HttpUtils httpUtils = null;
		try {
			String bookingDetailUrl = StringUtils.join(endpoint, TripjackConstant.BOOKING_DETAIL.value);
			BookingDetailRequest bookingDetailRequest =
					BookingDetailRequest.builder().bookingId(hInfo.getMiscInfo().getSupplierBookingReference()).build();
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(bookingDetailRequest), bookingDetailUrl);
			bookingDetailResponse = httpUtils.getResponse(TJBookingDetailResponse.class).orElse(null);
			while (bookingDetailResponse.getOrder().getStatus().equals("PAYMENT_SUCCESS")
					|| bookingDetailResponse.getOrder().getStatus().equals("IN_PROGRESS")) {
				Thread.sleep(10000);
				httpUtils = getHttpUtils(GsonUtils.getGson().toJson(bookingDetailRequest), bookingDetailUrl);
				bookingDetailResponse = httpUtils.getResponse(TJBookingDetailResponse.class).orElse(null);
			}
			if (bookingDetailResponse.getOrder().getStatus().equals("SUCCESS")) {
				if (bookingDetailResponse.getItemInfos().get("HOTEL").getHInfo().getMiscInfo() != null) {
					hInfo.getMiscInfo().setHotelBookingReference(bookingDetailResponse.getItemInfos().get("HOTEL")
							.getHInfo().getMiscInfo().getHotelBookingReference());
				}
				return true;
			}
		} finally {
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(bookingDetailResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRIPJACK.name())
					.requestType(BaseHotelConstants.RETRIEVE_BOOKING).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
		return false;
	}

	private BookingRequest getBookingRequest(Option option) {

		HotelBookingRequest bookingRequest = new HotelBookingRequest();
		bookingRequest.setBookingId(hInfo.getMiscInfo().getSupplierBookingReference());

		BigDecimal paymentamount = BigDecimal.valueOf(option.getRoomInfos().stream()
				.mapToDouble(room -> room.getTotalFareComponents().get(HotelFareComponent.BF)).sum());
		PaymentRequest paymentInfo = PaymentRequest.builder().amount(paymentamount).build();
		bookingRequest.setPaymentInfos(Arrays.asList(paymentInfo));
		bookingRequest.setDeliveryInfo(order.getDeliveryInfo());
		bookingRequest.setType(order.getOrderType());

		List<RoomTravellerInfo> roomTravellerInfoList = new ArrayList<>();
		option.getRoomInfos().forEach(room -> {
			RoomTravellerInfo roomTravelleinfo = new RoomTravellerInfo();
			roomTravelleinfo.setTravellerInfo(room.getTravellerInfo());
			roomTravellerInfoList.add(roomTravelleinfo);
		});
		bookingRequest.setRoomTravellerInfo(roomTravellerInfoList);
		return bookingRequest;
	}

}

