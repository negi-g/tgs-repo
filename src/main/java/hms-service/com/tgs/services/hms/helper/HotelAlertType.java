package com.tgs.services.hms.helper;

import lombok.Getter;

@Getter
public enum HotelAlertType {

	INSUFFICIENT_BALANCE_ALERT("Insufficient Balance Alert");

	String alert;

	HotelAlertType(String alert) {
		this.alert = alert;
	}
}
