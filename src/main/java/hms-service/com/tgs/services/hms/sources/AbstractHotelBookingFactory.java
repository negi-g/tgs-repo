package com.tgs.services.hms.sources;

import java.io.IOException;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelPanConfigOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelBookingQuery;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.pms.datamodel.PaymentStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Service
@Slf4j
public abstract class AbstractHotelBookingFactory {

	protected HotelSupplierConfiguration supplierConf;
	protected HotelInfo hotel;
	protected Order order;
	protected HotelBookingQuery bookingQuery;
	@Value("${env}")
	private String env;
	@Value("${pbuid}")
	private String prodBookingUserId;
	private boolean isHold, isRequest;
	protected boolean isPanRequired;
	protected HotelSourceConfigOutput sourceConfigOutput;

	@Autowired
	HotelOrderItemCommunicator itemComm;

	@Autowired
	protected GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;
	
	@Autowired
	HotelCacheHandler cacheHandler;


	public AbstractHotelBookingFactory(HotelSupplierConfiguration supplierConf, HotelInfo hotel, Order order) {
		this.supplierConf = supplierConf;
		this.hotel = hotel;
		this.order = order;
		isRequest = isRequestBooking(hotel);
		isHold = isHoldBooking(hotel, order);

		RoomInfo firstRoomInfo = hotel.getOptions().get(0).getRoomInfos().get(0);
		this.sourceConfigOutput = HotelUtils.getHotelSourceConfigOutput(HotelSearchQuery.builder()
				.checkinDate(firstRoomInfo.getCheckInDate()).checkoutDate(firstRoomInfo.getCheckOutDate())
				.sourceId(supplierConf.getBasicInfo().getSourceId()).build());

	}

	private boolean isPanRequiredForSupplier(HotelInfo hotel) {
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(hotel.getMiscInfo().getSearchId());
		HotelBasicFact basicFact = HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery).generateFactFromHotelInfo(hotel);
		basicFact.setSupplierId(hotel.getOptions().get(0).getMiscInfo().getSupplierId());
		HotelPanConfigOutput panConfigOutput =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(basicFact, HotelConfiguratorRuleType.PANCONFIG);
		return Objects.nonNull(panConfigOutput);
	}

	private boolean isHoldBooking(HotelInfo hInfo, Order order) {
		boolean isHoldBook = false;
		if (order.getAdditionalInfo() == null || order.getAdditionalInfo().getPaymentStatus() == null) {
			if (!isRequest)
				isHoldBook = true;
		} else if (order.getAdditionalInfo() != null && order.getAdditionalInfo().getPaymentStatus() != null
				&& !order.getAdditionalInfo().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
			throw new CustomGeneralException("Booking Not Allowed, Due To Payment is Not Done!");
		}
		return isHoldBook;
	}

	private boolean isRequestBooking(HotelInfo hInfo) {
		if (hInfo.getOptions().get(0).getIsOptionOnRequest()) {
			return true;
		}
		return false;
	}

	public abstract boolean bookHotel() throws IOException, InterruptedException, JAXBException;

	public abstract boolean confirmHotel() throws IOException;

	public boolean doBooking() {
		boolean isBooked = false;
		try {
			if (isBookingAllowed()) {
				try {
					log.info("Started booking process for bookingid {}", order.getBookingId());
					isPanRequired=isPanRequiredForSupplier(hotel);
					isBooked = this.bookHotel();
					return isBooked;
				} catch (IOException e) {
					SystemContextHolder.getContextData().getErrorMessages()
							.add("Unable to book hotel due to I/O exception");
					log.info("Unable to book hotel for {} due to I/O exception", order.getBookingId(), e);
				}
			} else {
				log.info("Booking not allowed at supplier end for bookingid {}, thus creating test booking",
						order.getBookingId());
				hotel.getMiscInfo().setSupplierBookingId("HTESTREF");
				hotel.getMiscInfo().setSupplierBookingReference("HTESTREF");
				isBooked = true;
			}
		} catch (CustomGeneralException e) {
			log.error("Hotel Booking Failed From Supplier For BookingId {}", order.getBookingId(), e);
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to book hotel due to " + e.getMessage());
		} catch (Exception e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to book hotel from supplier due to " + e.getCause());
			log.error("Hotel Booking failed from supplier for bookingId {}", order.getBookingId(), e);
		} finally {
			if (!isBooked) {
				String supplierErrorMessage = "";
				if (CollectionUtils.isNotEmpty(SystemContextHolder.getContextData().getErrorMessages())) {
					supplierErrorMessage = String.join(",", SystemContextHolder.getContextData().getErrorMessages());
				}

				if (StringUtils.isBlank(supplierErrorMessage)) {
					supplierErrorMessage =
							"Booking Either Failed From Supplier Or " + "Not Yet Confirmed From Supplier";
				}
				String note = Stream
						.of(supplierErrorMessage, hotel.getMiscInfo().getSupplierBookingReference(),
								hotel.getMiscInfo().getSupplierBookingId())
						.filter(s -> s != null && !s.isEmpty()).collect(Collectors.joining(" "));

				gmsCommunicator.addNote(Note.builder().noteType(NoteType.BOOKING_FAILED).noteMessage(note)
						.bookingId(order.getBookingId()).build());

				hotel.getMiscInfo().setSupplierBookingReference(null);
				hotel.getMiscInfo().setSupplierBookingId(null);
				itemComm.updateOrderItem(hotel, order, HotelItemStatus.PENDING);
				log.info("Finished booking process for bookingid {} with order status {}", order.getBookingId(),
						HotelItemStatus.PENDING);
			} else {
				if (isHold) {
					itemComm.updateOrderItem(hotel, order, HotelItemStatus.ON_HOLD);
					log.info("Finished booking process for bookingid {} with order status {}", order.getBookingId(),
							HotelItemStatus.ON_HOLD);
				} else if (isRequest) {
					itemComm.updateOrderItem(hotel, order, HotelItemStatus.PENDING);
					log.info("Finished booking process for bookingid {} with order status {}", order.getBookingId(),
							HotelItemStatus.PENDING);
				} else {
					itemComm.updateOrderItem(hotel, order, HotelItemStatus.SUCCESS);
					log.info("Finished booking process for bookingid {} with order status {}", order.getBookingId(),
							HotelItemStatus.SUCCESS);
				}
			}

			LogUtils.clearLogList();
		}
		return isBooked;
	}


	public boolean doConfirmBooking() {

		boolean isConfirmed = false;
		try {
			// Condition to check if booking can be cancelled even when Supplier is Disabled.
			if (!(isSupplierDisabled() && !allowOnSupplierDisabled())) {
				isConfirmed = this.confirmHotel();
			} else {
				log.info("Booking can’t be cancelled as supplier is disabled for booking id {}", order.getBookingId());
			}
		} catch (Exception e) {
			log.error("Error while Confirming booking for bookingId {}", order.getBookingId(), e);
		} finally {
			log.info("Confirming booking for bookingId {} with isConfirmed {}", order.getBookingId(), isConfirmed);
			if (isConfirmed)
				itemComm.updateOrderItem(hotel, order, HotelItemStatus.SUCCESS);
			else
				itemComm.updateOrderItem(hotel, order, HotelItemStatus.PENDING);
			LogUtils.clearLogList();
		}
		return isConfirmed;
	}

	public boolean isBookingAllowed() {
		return ((BooleanUtils.isTrue(this.getSupplierConf().getHotelSupplierCredentials().getIsTestCredential())
				|| "prod".equalsIgnoreCase(env) || order.getBookingUserId().equals(prodBookingUserId)));
	}

	public void updateBookingStatus() {

	}

	public boolean isSupplierDisabled() {
		return BooleanUtils.isNotTrue(supplierConf.getEnabled());
	}

	public boolean allowOnSupplierDisabled() {
		return true;
	}

}
