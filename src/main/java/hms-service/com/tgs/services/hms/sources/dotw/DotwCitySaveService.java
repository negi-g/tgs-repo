package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwCity;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class DotwCitySaveService extends DotwBaseService {

	public List<HotelRegionInfoQuery> getRegionMapppings() throws IOException {

		List<HotelRegionInfoQuery> dotwAllRegions = new ArrayList<>();
		try {
			List<DotwCity> cityList = getCityList();
			Map<String, List<DotwCity>> countryCityListMap =
					cityList.stream().collect(Collectors.groupingBy(DotwCity::getCountryName));
			for (String country : countryCityListMap.keySet()) {
				List<DotwCity> cities = countryCityListMap.get(country);
				if (CollectionUtils.isNotEmpty(cities)) {
					List<HotelRegionInfoQuery> dotwRegions = getRegionsFromDotwCities(cities, country);
					dotwAllRegions.addAll(dotwRegions);
				}
			}
		} catch (Exception e) {
			log.error("Error while mapping regions for DOTW", e);
		}
		return dotwAllRegions;
	}

	private List<DotwCity> getCityList() throws JAXBException, IOException {

		Customer customer = getRequestForStaticDataCity();
		String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
		HttpUtils httpUtils = getHttpRequest(xmlRequest, supplierConf);
		String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
		Results result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
		return result.getCities();
	}

	private List<HotelRegionInfoQuery> getRegionsFromDotwCities(List<DotwCity> cityResult, String countryName) {

		List<HotelRegionInfoQuery> regions = new ArrayList<>();
		cityResult.forEach((dotwCity) -> {
			HotelRegionInfoQuery regionInfoQuery = new HotelRegionInfoQuery();
			regionInfoQuery.setRegionId(String.valueOf(dotwCity.getCode()));
			regionInfoQuery.setRegionName(dotwCity.getName());
			regionInfoQuery.setRegionType("CITY");
			regionInfoQuery.setCountryName(dotwCity.getCountryName());
			regionInfoQuery.setCountryId(String.valueOf(dotwCity.getCountryCode()));
			regionInfoQuery.setSupplierName("DOTW");
			regions.add(regionInfoQuery);
		});
		return regions;
	}


	private Customer getRequestForStaticDataCity() {

		Customer customer = getCustomer(supplierConf);
		Request request = new Request();
		request.setCommand("getallcities");
		request.getRet().getFields().setFields(new ArrayList<>(Arrays.asList("countryName", "countryCode")));
		customer.setRequest(request);
		return customer;

	}

}
