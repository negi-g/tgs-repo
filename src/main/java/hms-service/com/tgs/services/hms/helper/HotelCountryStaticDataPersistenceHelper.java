
package com.tgs.services.hms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.HotelCountryInfoMapping;
import com.tgs.services.hms.dbmodel.DbHotelCountryInfoMapping;
import com.tgs.services.hms.jparepository.HotelCountryInfoMappingService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@InitializerGroup(group = InitializerGroup.Group.HOTEL)
public class HotelCountryStaticDataPersistenceHelper extends InMemoryInitializer {

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	HotelCountryInfoMappingService countryInfoService;


	public HotelCountryStaticDataPersistenceHelper() {
		super(null);
	}

	@Override
	public void process() {
		List<HotelCountryInfoMapping> countryMappingList = new ArrayList<>();
		Runnable fetchCityInfoTask = () -> {
			log.info("Fetching hotel country info mapping from database");
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
				List<HotelCountryInfoMapping> countryInfoListChunk =
						DbHotelCountryInfoMapping.toDomainList(countryInfoService.findAll(page));
				if (CollectionUtils.isEmpty(countryInfoListChunk))
					break;
				countryMappingList.addAll(countryInfoListChunk);
				log.debug("Fetched hotel country mapping info from database, info list size is {}",
						countryMappingList.size());
			}

			Map<String, List<HotelCountryInfoMapping>> countryMap =
					countryMappingList.stream().collect(Collectors.groupingBy(HotelCountryInfoMapping::getCountryId));
			for (Entry<String, List<HotelCountryInfoMapping>> entry : countryMap.entrySet()) {
				try {
					List<HotelCountryInfoMapping> countryInfoMappingList =
							entry.getValue();
					Map<String, Object> countryInfoBinMap = new HashMap<>();
					if (CollectionUtils.isNotEmpty(countryInfoMappingList)) {
						for (HotelCountryInfoMapping supplierCountryMapping : countryInfoMappingList) {
							countryInfoBinMap.put(supplierCountryMapping.getSupplierName(),
									GsonUtils.getGson().toJson(supplierCountryMapping));
						}
						CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder()
								.namespace(CacheNameSpace.HOTEL.getName()).set(CacheSetName.NATIONALITY.getName())
								.key(entry.getKey()).expiration(-1)
								.binValues(countryInfoBinMap).build();
						cacheService.store(metaInfoToStore);
					}
				} catch (Exception e) {
					log.info("Error while storing country mapping in aerospike for country {} , {}",
							entry.getKey(), entry.getValue().get(0).getSupplierCountryName(), e);
				}
			}
		};

		Thread fetchCityInfoThread = new Thread(fetchCityInfoTask);
		fetchCityInfoThread.start();

	}

	@Override
	public void deleteExistingInitializer() {}

}
