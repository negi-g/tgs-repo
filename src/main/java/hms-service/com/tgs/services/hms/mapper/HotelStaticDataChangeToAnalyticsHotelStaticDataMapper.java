package com.tgs.services.hms.mapper;

import java.time.LocalDateTime;
import java.util.Objects;
import org.apache.commons.lang3.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.hms.analytics.AnalyticsHotelQuery;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class HotelStaticDataChangeToAnalyticsHotelStaticDataMapper extends Mapper<AnalyticsHotelQuery> {

	private String bookingId;
	private String searchId;
	private HotelInfo oldHotel;
	private HotelInfo newHotel;

	@Override
	protected void execute() throws CustomGeneralException {
		output = Objects.nonNull(output) ? output : AnalyticsHotelQuery.builder().build();
		output.setBookingid(bookingId);
		output.setSearchid(searchId);
		output.setUnicaid(oldHotel.getUnicaId());
		output.setGenerationtime(LocalDateTime.now());
		output.setAnalyticstype(HotelFlowType.STATIC_CHANGE.name());
		OptionMiscInfo oldOptionMiscInfo = oldHotel.getOptions().get(0).getMiscInfo();
		OptionMiscInfo newOptionMiscInfo = newHotel.getOptions().get(0).getMiscInfo();
		output.setSourcename(
				getOldValueNewValueString(HotelSourceType.getHotelSourceType(oldOptionMiscInfo.getSourceId()).name(),
						HotelSourceType.getHotelSourceType(newOptionMiscInfo.getSourceId()).name()));
		output.setSuppliername(
				getOldValueNewValueString(oldOptionMiscInfo.getSupplierId(), newOptionMiscInfo.getSupplierId()));
		output.setSupplierhotelid(
				getOldValueNewValueString(oldHotel.getMiscInfo().getSupplierStaticHotelId(), ObjectUtils.firstNonNull(
						newHotel.getSupplierHotelId(), newHotel.getMiscInfo().getSupplierStaticHotelId())));
		output.setAddress(getOldValueNewValueString(HotelUtils.getCompleteAddress(oldHotel.getAddress()),
				HotelUtils.getCompleteAddress(newHotel.getAddress())));
		output.setHotelname(getOldValueNewValueString(oldHotel.getName(), newHotel.getName()));
	}

	private String getOldValueNewValueString(String oldName, String newName) {
		return String.join("@@@", oldName, newName);
	}

}
