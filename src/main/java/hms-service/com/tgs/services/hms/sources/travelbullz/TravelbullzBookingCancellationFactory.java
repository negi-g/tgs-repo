package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import javax.xml.bind.JAXBException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class TravelbullzBookingCancellationFactory extends AbstractHotelBookingCancellationFactory {

	public TravelbullzBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo,
			Order order) {
		super(supplierConf, hInfo, order);

	}

	@Override
	public boolean cancelHotel() throws IOException, JAXBException {
		TravelbullzBookingCancellationService service = TravelbullzBookingCancellationService.builder()
				.supplierConf(supplierConf).hInfo(hInfo).order(order).build();
		service.init();
		return service.cancelBooking();
	}

	@Override
	public boolean getCancelHotelStatus() throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

}
