package com.tgs.services.hms.helper;

import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;

@Service
public class HotelBookingCancellation {

	public boolean cancelBooking(Order order, HotelInfo hInfo) {
		Set<String> roomKeys = HotelUtils.getCancellableRoomsFromHInfo(hInfo);
		if ((CollectionUtils.isNotEmpty(roomKeys) && hInfo.getOptions().get(0).getRoomInfos().size() != roomKeys.size())
				&& !isPartialCancellationAllowed(order, hInfo)) {
			return false;
		}
		AbstractHotelBookingCancellationFactory factory =
				HotelSourceType.getHotelBookingCancellationFactoryInstance(hInfo, order);
		return factory.cancelBooking();
	}

	public boolean isPartialCancellationAllowed(Order order, HotelInfo hInfo) {

		AbstractHotelBookingCancellationFactory factory =
				HotelSourceType.getHotelBookingCancellationFactoryInstance(hInfo, order);
		return factory.isPartialCancellationAllowed();
	}

}
