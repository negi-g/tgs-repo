package com.tgs.services.hms.jparepository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.dbmodel.DbHotelCountryInfoMapping;


@Service
public class HotelCountryInfoMappingService {
	@Autowired
	DbHotelCountryInfoMappingRepository repository;

	public DbHotelCountryInfoMapping save(DbHotelCountryInfoMapping mapping) {
		return repository.save(mapping);
	}

	public List<DbHotelCountryInfoMapping> findAll(Pageable pageable) {
		List<DbHotelCountryInfoMapping> nationalityInfoList = new ArrayList<>();
		repository.findAll(pageable).forEach(nationalityInfo -> nationalityInfoList.add(nationalityInfo));
		return nationalityInfoList;
	}

	public List<DbHotelCountryInfoMapping> findByCountryId(String countryId) {
		return repository.findByCountryId(countryId);
	}

	public List<DbHotelCountryInfoMapping> findBySupplierName(String supplierName) {
		return repository.findBySupplierName(supplierName);
	}

	public DbHotelCountryInfoMapping findByCountryIdAndSupplierName(String countryId, String supplierName) {
		return repository.findByCountryIdAndSupplierName(countryId, supplierName);
	}

	public DbHotelCountryInfoMapping findBySupplierCountryId(String supplierCountryId) {
		return repository.findBySupplierCountryId(supplierCountryId);
	}
}
