package com.tgs.services.hms.sources.tripjack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.HotelReviewRequest;
import com.tgs.services.hms.restmodel.tripjack.TJPriceValidationResponse;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@SuperBuilder
public class TripjackPriceValidationService extends TripjackBaseService {

	private String bookingId;
	private TJPriceValidationResponse priceValidationResponse;

	public void validate(HotelInfo hInfo) throws Exception {
		HttpUtils httpUtils = null;
		listener = new RestAPIListener("");
		String requestUrl = StringUtils.join(endpoint, TripjackConstant.REVIEW.value);
		try {
			HotelReviewRequest priceValidationRequest =
					HotelReviewRequest.builder().hotelId(hInfo.getMiscInfo().getCorrelationId())
							.optionId(hInfo.getOptions().get(0).getId()).build();
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(priceValidationRequest), requestUrl);
			priceValidationResponse = httpUtils.getResponse(TJPriceValidationResponse.class).orElse(null);
			if (ObjectUtils.isEmpty(priceValidationResponse.getHInfo()))
				throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);

			if (priceValidationResponse.getHInfo() != null) {
				updateOptionWithNewPrice(priceValidationResponse, hInfo);
			}
		} finally {
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(priceValidationResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRIPJACK.name())
					.requestType(BaseHotelConstants.PRICE_CHECK).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(bookingId)
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
	}

	private void updateOptionWithNewPrice(TJPriceValidationResponse priceValidationResponse, HotelInfo hInfo) {

		Option respOption = priceValidationResponse.getHInfo().getOptions().get(0);
		Option option = hInfo.getOptions().get(0);

		double prevPrice = option.getRoomInfos().stream()
				.mapToDouble(room -> room.getTotalFareComponents().get(HotelFareComponent.BF)).sum();
		if (Math.abs(prevPrice - respOption.getTotalPrice()) > 5) {
			tripjckManagementFee =
					option.getRoomInfos().get(0).getMiscInfo().getFees() * (option.getRoomInfos().size());

			Map<String, RoomInfo> map =
					option.getRoomInfos().stream().collect(Collectors.toMap(RoomInfo::getId, Function.identity()));
			setCancellationPolicy(option, respOption.getCancellationPolicy());
			respOption.getRoomInfos().forEach(roomResponse -> {
				RoomInfo room = map.get(roomResponse.getId());
				setRoomPriceInfo(room, roomResponse);
			});
		} else if (option.getCancellationPolicy() == null) {
			setCancellationPolicy(option, respOption.getCancellationPolicy());
		}
		List<Instruction> instructions = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(respOption.getInstructions())) {
			respOption.getInstructions().forEach(inst -> {
				if (inst.getMsg() != null && !StringUtils.isEmpty(inst.getMsg())) {
					instructions.add(inst);
				}
			});
		}
		List<Instruction> hotelInstructions = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(priceValidationResponse.getHInfo().getInstructions())) {
			priceValidationResponse.getHInfo().getInstructions().forEach(inst -> {
				if (inst.getMsg() != null && !StringUtils.isEmpty(inst.getMsg())) {
					hotelInstructions.add(inst);
				}
			});
		}
		if (instructions.size() > 0)
			option.setInstructions(instructions);
		if (hotelInstructions.size() > 0)
			hInfo.setInstructions(hotelInstructions);
		hInfo.getMiscInfo().setSupplierBookingReference(priceValidationResponse.getBookingId());
		hInfo.getMiscInfo().setSupplierBookingId(priceValidationResponse.getBookingId());
	}
}
