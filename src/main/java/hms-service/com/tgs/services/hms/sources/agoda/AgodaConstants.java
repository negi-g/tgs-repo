package com.tgs.services.hms.sources.agoda;

public enum AgodaConstants {
	NAME("AGODA"),
	CURRENCY("INR"),
	LANGUAGECODE("en-us"),
	SHORTSEARCH("6"),
	LONGSEARCH("4"),

	//order status
	BOOKINGCONFIRMED("SUCCESS"),
	BOOKINGCHARGED("SUCCESS"),
	BOOKINGCANCELLEDBYCUSTOMER("CANCELLED"),
	BOOKINGCANCELLED("CANCELLED"),
	DEPARTED("PENDING"),
	//card type
    VISA("Visa"),
    MASTERCARD("MasterCard"),
	AMERICAN_EXPRESS("AmericanExpress"),
	PRE_AUTH_ERROR("419");
	
	private String value;

	private AgodaConstants(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
