package com.tgs.services.hms.manager;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.HotelMissingInfoLoggingUtil;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelMissingInfo;
import com.tgs.services.hms.datamodel.HotelPromotion;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelTag;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.OpinionatedContent;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.ProcessedOption;
import com.tgs.services.hms.datamodel.RoomAdditionalInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelClientMarkupOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelPromotionOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelPropertyCategoryOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelStaticDataConfigOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.PropertyCriteria;
import com.tgs.services.hms.datamodel.hotelconfigurator.PropertyType;
import com.tgs.services.hms.datamodel.hotelconfigurator.SearchExclusionOutput;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelSearchResultProcessingManager {

	@Autowired
	private HotelUserFeeManager userFeeManager;

	@Autowired
	GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	private CommercialCommunicator commericialCommunicator;

	@Autowired
	GeneralServiceCommunicator gmsComm;

	private static final String IS_QUARANTINE = "QUARANTINE";

	public void processSearchResult(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {
		long startTime = System.currentTimeMillis();
		try {
			ContextData contextData = SystemContextHolder.getContextData();
			HotelRegionInfo regionInfo =
					cacheHandler.getRegionInfoFromRegionId(searchQuery.getSearchCriteria().getRegionId());
			ClientGeneralInfo clientInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
			searchResult.getHotelInfos().forEach(hotelInfo -> {
				populateCityAndCountry(hotelInfo, regionInfo, searchQuery);
				filterOption(hotelInfo, searchQuery, clientInfo);
				processHotelFareComponents(hotelInfo, searchQuery, contextData.getUser(), true);
			});
			process(searchResult, searchQuery, contextData);
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, searchResult.getHotelInfos(), System.currentTimeMillis() - startTime);
		}
	}

	public void processDetailResult(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		long startTime = System.currentTimeMillis();
		try {
			if (CollectionUtils.isEmpty(hInfo.getOptions())
					|| CollectionUtils.isEmpty(hInfo.getOptions().get(0).getRoomInfos())) {
				return;
			}
			User user = SystemContextHolder.getContextData().getUser();
			ClientGeneralInfo clientInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
			filterOption(hInfo, searchQuery, clientInfo);
			processHotelFareComponents(hInfo, searchQuery, user, null);
			aggregateHotels(HotelSearchResult.builder().hotelInfos(new ArrayList<>(Arrays.asList(hInfo))).build(),
					searchQuery);
			BaseHotelUtils.updateManagementFee(hInfo, searchQuery, false);
			updateClientMarkup(hInfo, searchQuery, user, null);
			commericialCommunicator.processUserCommissionInHotel(hInfo, user, searchQuery);
			applyAdditonalTax(new ArrayList<>(Arrays.asList(hInfo)), searchQuery, false);
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
					System.currentTimeMillis() - startTime);
		}
	}

	private void populateCityAndCountry(HotelInfo hInfo, HotelRegionInfo regionInfo, HotelSearchQuery searchQuery) {

		City city = null;
		Country country = null;
		if (regionInfo != null || searchQuery.getSearchCriteria().getCityName() != null) {
			city = City.builder()
					.name(regionInfo != null ? regionInfo.getRegionName()
							: searchQuery.getSearchCriteria().getCityName())
					.build();
			country = Country.builder().name(
					regionInfo != null ? regionInfo.getCountryName() : searchQuery.getSearchCriteria().getCountryName())
					.build();
			if (Objects.isNull(hInfo.getAddress())) {
				hInfo.setAddress(Address.builder().build());
			}
			hInfo.getAddress().setCity(city);
			hInfo.getAddress().setCityName(city.getName());
			hInfo.getAddress().setCountry(country);
			hInfo.getAddress().setCountryName(country.getName());
		}
	}

	private void filterOption(HotelInfo hotelInfo, HotelSearchQuery searchQuery, ClientGeneralInfo clientInfo) {

		if (Objects.nonNull(hotelInfo)) {
			for (Iterator<Option> optionIterator = hotelInfo.getOptions().iterator(); optionIterator.hasNext();) {
				Option option = optionIterator.next();
				if (!isValidOption(hotelInfo, option, searchQuery, clientInfo))
					optionIterator.remove();
			}
		}
	}

	private boolean isValidOption(HotelInfo hotelInfo, Option option, HotelSearchQuery searchQuery,
			ClientGeneralInfo clientInfo) {


		Integer roomCountInSearchQuery = searchQuery.getRoomInfo().size();
		if (CollectionUtils.isEmpty(option.getRoomInfos()) || option.getRoomInfos().size() != roomCountInSearchQuery) {
			log.debug(
					"Removed Option for hotel {} due to either no room available or roomCount in option doesn't match "
							+ "with user criteria",
					hotelInfo.getName());
			return false;
		} else if (option.getIsOptionOnRequest()) {
			if (clientInfo.getIsOnRequestAllowed() != null && !clientInfo.getIsOnRequestAllowed())
				return false;
			if (clientInfo.getOnRequestHours() != null) {
				Integer requiredHours = clientInfo.getOnRequestHours();
				Long actualHours = Duration
						.between(LocalDateTime.now(), searchQuery.getCheckinDate().atTime(LocalTime.now())).toHours();
				if (requiredHours != null && actualHours < requiredHours) {
					log.debug(
							"Removed Option for hotel {} due to On Request Not Allowed"
									+ " for targetHours {}, actualHours {}",
							hotelInfo.getName(), requiredHours, actualHours);
					return false;
				}
			}
		} else if (Objects.nonNull(searchQuery.getSearchPreferences())
				&& BooleanUtils.isTrue(searchQuery.getSearchPreferences().getIsQuarantinePackage())) {

			return !option.getRoomInfos().stream()
					.anyMatch(roomInfo -> !roomInfo.getRoomType().toUpperCase().contains(IS_QUARANTINE));
		}
		return true;
	}

	private void process(HotelSearchResult searchResult, HotelSearchQuery searchQuery, ContextData contextData) {
		long startTime = System.currentTimeMillis();
		try {
			LogUtils.log(LogTypes.HOTEL_SINGLE_SUPPLIER_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();
			Set<String> supplierHotelIdList = new HashSet<>();
			Set<String> supplierMealInfoIdList = new HashSet<>();
			populateHotelAndMealIds(searchResult, supplierHotelIdList, supplierMealInfoIdList);
			Map<String, HotelInfo> hotelStaticInfoMap = fetchHotelStaticInfoMap(searchResult.getHotelInfos(),
					searchQuery, supplierHotelIdList, OperationType.LIST_SEARCH);
			Map<String, String> hotelStaticMealInfoMap =
					fetchMealInfoData(searchResult.getHotelInfos(), searchQuery, supplierMealInfoIdList);
			for (Iterator<HotelInfo> hotelIterator = searchResult.getHotelInfos().iterator(); hotelIterator
					.hasNext();) {
				HotelInfo hInfo = hotelIterator.next();
				HotelMissingInfo missingInfo = new HotelMissingInfo(OperationType.LIST_SEARCH);
				populateMissingHotelInfo(hotelStaticInfoMap, hInfo, missingInfo, searchQuery, staticDataService);
				if (!isValidHotelInfo(hInfo, searchQuery)) {
					log.debug(
							"Unable to validate hotel info for search id {}, hotelname {},hotel code {} and hotel rating {}",
							searchQuery.getSearchId(), hInfo.getName(), hInfo.getLocalHotelCode(), hInfo.getRating());
					hotelIterator.remove();
				} else {
					try {
						processOptions(hInfo, searchQuery);
						sortOptions(hInfo);
						mapHotelMealBasis(hotelStaticMealInfoMap, hInfo, searchQuery, missingInfo);
						BaseHotelUtils.updateManagementFee(hInfo, searchQuery, true);
						updateClientMarkup(hInfo, searchQuery, contextData.getUser(), true);
						applyPropertyType(hInfo, searchQuery, contextData);
						applyPromotionIfApplicable(hInfo, searchQuery);
					} finally {
						HotelMissingInfoLoggingUtil.addToLogList(missingInfo, hInfo, searchQuery,
								hInfo.getMiscInfo().getSupplierStaticHotelId());
					}
				}
			}
			LogUtils.log(LogTypes.HOTEL_SINGLE_SUPPLIER_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.trips(searchResult == null ? 0 : searchResult.getNoOfHotelOptions())
							.supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
					LogTypes.HOTEL_SINGLE_SUPPLIER_PROCESS_START);
		} finally {
			HotelMissingInfoLoggingUtil.clearLog(LogTypes.MISSING_INFO);
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, searchResult.getHotelInfos(), System.currentTimeMillis() - startTime);
		}
	}

	public void aggregateHotels(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {
		long startTime = System.currentTimeMillis();
		List<String> supplierIds = HotelUtils.getHotelSupplierIds(searchQuery);
		try {

			log.info("Inside aggregate hotels for search id {} and supplier {}", searchQuery.getSearchId(),
					supplierIds);
			LogUtils.log(LogTypes.HOTEL_AGGREGATE_HOTEL_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			List<HotelInfo> mappedHotels = aggregateByUniqueId(searchResult.getHotelInfos(), searchQuery);
			searchResult.setHotelInfos(mappedHotels);
		} finally {
			LogUtils.log(LogTypes.HOTEL_AGGREGATE_HOTEL_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.trips(searchResult.getNoOfHotelOptions()).supplierId(supplierIds.toString()).build(),
					LogTypes.HOTEL_AGGREGATE_HOTEL_PROCESS_START);
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, searchResult.getHotelInfos(), System.currentTimeMillis() - startTime);
		}
	}

	public List<HotelInfo> aggregateByUniqueId(List<HotelInfo> hotelList, HotelSearchQuery searchQuery) {

		LogUtils.log(LogTypes.HOTEL_AGGREGATE_BY_UNIQUE_KEY_HOTEL_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();

		HotelBasicFact hotelFact = HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery);
		List<HotelConfiguratorInfo> hotelconfigrules =
				HotelConfiguratorHelper.getHotelConfiguratorRules(HotelConfiguratorRuleType.STATICDATACONFIG);
		Map<String, HotelInfo> hotelInfoMap = new HashMap<>();
		List<HotelInfo> aggregatedHotelInfos = new ArrayList<>();
		for (HotelInfo hInfo : hotelList) {
			String key = staticDataService.getKeyToMergeHotel(hInfo);
			if (StringUtils.isBlank(key)) {
				aggregatedHotelInfos.add(hInfo);
			} else {
				hotelFact = hotelFact.generateFactFromHotelInfo(hInfo);
				HotelStaticDataConfigOutput staticDataConfig =
						HotelConfiguratorHelper.getRuleOutPut(hotelFact, hotelconfigrules);

				if (hotelInfoMap.isEmpty() || !hotelInfoMap.containsKey(key)) {
					mergeOpinionatedContent(hInfo, staticDataConfig);
					hotelInfoMap.put(key, hInfo);
				} else {
					int staticDataPriority = getStaticDataPriority(hotelInfoMap.get(key), hInfo, staticDataConfig);
					if (staticDataPriority >= 0) {
						hotelInfoMap.get(key).getOptions().addAll(hInfo.getOptions());
					} else {
						HotelInfo oldHotelInfo = hotelInfoMap.remove(key);
						hInfo.getOptions().addAll(oldHotelInfo.getOptions());
						mergeOpinionatedContent(hInfo, staticDataConfig);
						hotelInfoMap.put(key, hInfo);
					}
				}
			}
			mergeOptions(hInfo, searchQuery);
		}
		Iterator<Map.Entry<String, HotelInfo>> hotelInfoIterator = hotelInfoMap.entrySet().iterator();
		while (hotelInfoIterator.hasNext()) {
			if (!isValidHotelInfo(hotelInfoIterator.next().getValue(), searchQuery)) {
				hotelInfoIterator.remove();
			}
		}
		aggregatedHotelInfos.addAll(new ArrayList<>(hotelInfoMap.values()));
		LogUtils.log(LogTypes.HOTEL_AGGREGATE_BY_UNIQUE_KEY_HOTEL_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.supplierId(HotelUtils.getHotelSupplierIds(searchQuery).toString()).build(),
				LogTypes.HOTEL_AGGREGATE_BY_UNIQUE_KEY_HOTEL_PROCESS_START);
		return aggregatedHotelInfos;
	}

	public void limitOptionsPerHotel(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		for (HotelInfo hotelInfo : searchResult.getHotelInfos()) {
			Map<String, Option> limitedOptionMap = new HashMap<>();
			for (Option option : hotelInfo.getOptions()) {
				if (!limitedOptionMap.containsKey(option.getMiscInfo().getSupplierId())) {
					limitedOptionMap.put(option.getMiscInfo().getSupplierId(), option);
				}
			}
			hotelInfo.cleanDataForSearch();
			for (Option option : limitedOptionMap.values()) {
				for (RoomInfo roomInfo : option.getRoomInfos()) {
					roomInfo.cleanDataForSearch();
				}
				option.cleanDataForSearch();
			}
			hotelInfo.setOptions(new ArrayList<>(limitedOptionMap.values()));
			sortOptions(hotelInfo);
		}
	}

	public void mergeOpinionatedContent(HotelInfo hotelInfo, HotelStaticDataConfigOutput staticDataConfig) {

		if (Objects.nonNull(staticDataConfig) && BooleanUtils.isTrue(staticDataConfig.getIsToPreferOpinionatedContent())
				&& Objects.nonNull(hotelInfo.getAdditionalInfo())) {
			OpinionatedContent opinionatedContent = hotelInfo.getAdditionalInfo().getOpinionatedContent();
			if (Objects.nonNull(opinionatedContent)) {

				hotelInfo.setName(ObjectUtils.firstNonNull(opinionatedContent.getName(), hotelInfo.getName()));
				hotelInfo.setGeolocation(
						ObjectUtils.firstNonNull(opinionatedContent.getGeolocation(), hotelInfo.getGeolocation()));
				hotelInfo.setAddress(ObjectUtils.firstNonNull(opinionatedContent.getAddress(), hotelInfo.getAddress()));
				hotelInfo.setRating(StringUtils.isNotBlank(opinionatedContent.getRating())
						? (int) Double.parseDouble(opinionatedContent.getRating())
						: hotelInfo.getRating());
				hotelInfo.setPropertyType(Strings.toUpperCase(
						ObjectUtils.firstNonNull(opinionatedContent.getPropertyType(), hotelInfo.getPropertyType())));
				hotelInfo.getMiscInfo().setIsOpinionatedContentRequired(true);
			}
		}
		hotelInfo.setAdditionalInfo(null);
	}

	/**
	 * 0: if priority of both prevHotelInfo and newHotelInfo hotel are same. 1: if priority of prevHotelInfo is greater
	 * than the priority of newHotelInfo. -1: if priority of newHotelInfo is greater than the priority of oldHotelInfo.
	 */
	private int getStaticDataPriority(HotelInfo prevHotelInfo, HotelInfo newHotelInfo,
			HotelStaticDataConfigOutput staticDataConfig) {

		if (Objects.nonNull(staticDataConfig)) {
			List<String> supplierInfo = staticDataConfig.getSuppliers();
			Map<String, Integer> supplierInfoMap = IntStream.range(0, supplierInfo.size()).boxed()
					.collect(Collectors.toMap(supplierInfo::get, i -> i));
			return supplierInfoMap.getOrDefault(newHotelInfo.getSupplierName(), Integer.MAX_VALUE)
					.compareTo(supplierInfoMap.getOrDefault(prevHotelInfo.getSupplierName(), Integer.MAX_VALUE));
		}
		return 0;
	}

	public void updateStaticData(HotelInfo hotelInfo, HotelSearchQuery searchQuery, OperationType operationType) {

		HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();
		HotelMissingInfo missingInfo = new HotelMissingInfo(operationType);
		String supplierHotelId = hotelInfo.getMiscInfo().getSupplierStaticHotelId();
		try {
			hotelInfo.setSupplierName(HotelUtils.firstNonEmpty(hotelInfo.getSupplierName(),
					HotelUtils.getSourceNameFromSourceId(searchQuery.getSourceId())));

			Set<String> supplierHotelIdList = new HashSet<>();
			supplierHotelIdList.add(staticDataService.getSupplierStaticHotelId(hotelInfo));

			Map<String, HotelInfo> hotelStaticInfoMap =
					fetchHotelStaticInfoMap(Arrays.asList(hotelInfo), searchQuery, supplierHotelIdList, operationType);
			if (MapUtils.isNotEmpty(hotelStaticInfoMap)) {
				populateMissingHotelBasicInfo(
						hotelStaticInfoMap.get(staticDataService.getSupplierStaticHotelId(hotelInfo)), hotelInfo,
						searchQuery);
				populateMissingHotelDetailInfo(
						hotelStaticInfoMap.get(staticDataService.getSupplierStaticHotelId(hotelInfo)), hotelInfo,
						searchQuery);
				if (BooleanUtils.isTrue(hotelInfo.getMiscInfo().getIsOpinionatedContentRequired())) {
					HotelStaticDataConfigOutput staticDataConfigOutput = new HotelStaticDataConfigOutput();
					staticDataConfigOutput.setIsToPreferOpinionatedContent(true);
					mergeOpinionatedContent(hotelInfo, staticDataConfigOutput);
				}
				logMissingHotelDetails(hotelInfo, missingInfo);
			}
		} finally {
			HotelMissingInfoLoggingUtil.addToLogList(missingInfo, hotelInfo, searchQuery, supplierHotelId);
		}
	}

	public void updateRoomStaticData(HotelInfo hotelInfo, HotelSearchQuery searchQuery) {

		final long startTime = System.currentTimeMillis();
		try {
			Map<String, List<Option>> supplierIdToAllOptionsMap = hotelInfo.getOptions().stream()
					.collect(Collectors.groupingBy(o -> o.getMiscInfo().getSupplierId()));
			HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();
			Set<String> supplierHotelIdList = new HashSet<>();
			for (String supplier : supplierIdToAllOptionsMap.keySet()) {
				Option firstOption = supplierIdToAllOptionsMap.get(supplier).get(0);
				HotelInfo hotelInfoFromFirstOption =
						HotelInfo.builder().supplierName(firstOption.getMiscInfo().getSupplierId())
								.miscInfo(HotelMiscInfo.builder()
										.supplierStaticHotelId(firstOption.getMiscInfo().getSupplierHotelId()).build())
								.build();
				supplierHotelIdList.add(staticDataService.getSupplierStaticHotelId(hotelInfoFromFirstOption));
			}

			Map<String, HotelInfo> hotelStaticInfoMap = fetchHotelStaticInfoMap(Arrays.asList(hotelInfo), searchQuery,
					supplierHotelIdList, OperationType.DETAIL_SEARCH);
			for (String supplier : supplierIdToAllOptionsMap.keySet()) {
				HotelInfo copyHInfo = new GsonMapper<>(hotelInfo, HotelInfo.class).convert();
				copyHInfo.setOptions(supplierIdToAllOptionsMap.get(supplier));
				copyHInfo.setSupplierName(copyHInfo.getOptions().get(0).getMiscInfo().getSupplierId());
				copyHInfo.getMiscInfo()
						.setSupplierStaticHotelId(copyHInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
				HotelSearchQuery copyQuery = HotelUtils.getHotelSearchQuery(searchQuery, copyHInfo);
				if (MapUtils.isNotEmpty(hotelStaticInfoMap) && Objects
						.nonNull(hotelStaticInfoMap.get(staticDataService.getSupplierStaticHotelId(copyHInfo)))) {
					populateMissingHotelRoomInfo(
							hotelStaticInfoMap.get(staticDataService.getSupplierStaticHotelId(copyHInfo)), copyHInfo,
							copyQuery);
				}
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hotelInfo) ? null : Arrays.asList(hotelInfo),
					System.currentTimeMillis() - startTime);
		}
	}

	public HotelInfo mergeSameHotelOptions(List<HotelInfo> hInfoList) {

		HotelInfo hInfo = hInfoList.get(0);
		for (int i = 1; i < hInfoList.size(); i++) {
			hInfo.getOptions().addAll(hInfoList.get(i).getOptions());
		}
		return hInfo;

	}

	public void mergeSortedOptions(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		Map<String, Option> optionMap = new HashMap<>();
		List<Option> updatedOptionList = new ArrayList<>();
		for (Option option : hInfo.getOptions()) {
			if (BooleanUtils.isNotTrue(option.getMiscInfo().getIsNotRequiredOnDetail())) {
				String key = "";
				for (RoomInfo room : option.getRoomInfos()) {
					key = StringUtils.join(key, HotelUtils.getKey(room.getRoomCategory()),
							HotelUtils.getKey(room.getMealBasis()));
				}
				if (optionMap.isEmpty() || !optionMap.containsKey(key)) {
					optionMap.put(key, option);
					updatedOptionList.add(option);
				}
			} else {
				updatedOptionList.add(option);
			}
		}
		hInfo.setOptions(updatedOptionList);
	}

	public HotelSearchResult combineAndProcessPreviousResult(HotelSearchResult hotelSearchResult,
			HotelSearchQuery searchQuery) {
		long startTime = System.currentTimeMillis();
		try {
			List<String> supplierIds = HotelUtils.getHotelSupplierIds(searchQuery);
			log.info("Inside combine previous result for search id {} and supplier {}", searchQuery.getSearchId(),
					supplierIds);

			if (hotelSearchResult != null) {
				combinePreviousResult(hotelSearchResult, searchQuery);
				aggregateHotels(hotelSearchResult, searchQuery);
				sortSearchResult(hotelSearchResult, searchQuery);
				applyCommercial(hotelSearchResult, searchQuery);
				limitOptionsPerHotel(hotelSearchResult, searchQuery);
				storeSearchResult(hotelSearchResult, searchQuery);
			}
			return hotelSearchResult;
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, hotelSearchResult.getHotelInfos(), System.currentTimeMillis() - startTime);
		}
	}

	public void applyAdditonalTax(List<HotelInfo> hotelInfos, HotelSearchQuery searchQuery, Boolean isSearch) {

		ClientGeneralInfo clientInfo =
				gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
						GeneralBasicFact.builder().isPackage(Objects.nonNull(searchQuery.getSearchPreferences())
								&& BooleanUtils.isTrue(searchQuery.getSearchPreferences().getIsQuarantinePackage()))
								.build());
		if (Objects.nonNull(clientInfo) && MapUtils.isNotEmpty(clientInfo.getExtraInfo())) {
			for (HotelInfo hotelInfo : hotelInfos) {
				for (Option option : hotelInfo.getOptions()) {
					for (RoomInfo roomInfo : option.getRoomInfos()) {
						for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {

							Map<String, Object> extraInfo = clientInfo.getExtraInfo();
							double gstInPercent = (double) extraInfo.getOrDefault(HotelFareComponent.GST.name(), 0.0d);
							double tcsInPercent = (double) extraInfo.getOrDefault(HotelFareComponent.TCS.name(), 0.0d);

							if (Objects.nonNull(clientInfo) && MapUtils.isNotEmpty(clientInfo.getExtraInfo())) {
								double baseFare = priceInfo.getFareComponents().get(HotelFareComponent.BF);
								double gstAmount = (gstInPercent * baseFare) / 100;
								priceInfo.getFareComponents().put(HotelFareComponent.GST, gstAmount);
								priceInfo.getFareComponents().put(HotelFareComponent.TCS,
										(tcsInPercent * (baseFare + gstAmount)) / 100);
							}
						}
					}
				}
				BaseHotelUtils.updateTotalFareComponents(hotelInfo, isSearch);
			}
		}
	}

	private void combinePreviousResult(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {
		long startTime = System.currentTimeMillis();
		try {
			HotelSearchResult oldHotelResults = cacheHandler.getSearchResult(searchQuery.getSearchId());
			if (oldHotelResults != null) {
				searchResult.getHotelInfos().addAll(oldHotelResults.getHotelInfos());
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, searchResult.getHotelInfos(), System.currentTimeMillis() - startTime);
		}
	}

	public void applyCommercial(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {
		long startTime = System.currentTimeMillis();
		List<String> supplierIds = HotelUtils.getHotelSupplierIds(searchQuery);
		try {
			log.info("Inside apply commercial for search id {} and supplier {}", searchQuery.getSearchId(),
					supplierIds);
			LogUtils.log(LogTypes.HOTEL_APPLY_COMMERCIAL_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			User user = SystemContextHolder.getContextData().getUser();
			commericialCommunicator.processUserCommissionInHotels(searchResult.getHotelInfos(), user, searchQuery);
			applyAdditonalTax(searchResult.getHotelInfos(), searchQuery, true);
			postApplyCommercial(searchResult, searchQuery);
		} finally {
			LogUtils.log(LogTypes.HOTEL_APPLY_COMMERCIAL_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.supplierId(supplierIds.toString()).trips(searchResult.getNoOfHotelOptions()).build(),
					LogTypes.HOTEL_APPLY_COMMERCIAL_PROCESS_START);
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, searchResult.getHotelInfos(), System.currentTimeMillis() - startTime);
		}
	}

	private void postApplyCommercial(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		log.info("Inside post apply commercial for search id {} and supplier {}", searchQuery.getSearchId(),
				searchQuery.getMiscInfo().getSupplierId());
		LogUtils.log(LogTypes.HOTEL_POST_APPLY_COMMERCIAL_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		for (Iterator<HotelInfo> hotelIterator = searchResult.getHotelInfos().iterator(); hotelIterator.hasNext();) {
			HotelInfo hInfo = hotelIterator.next();
			if (CollectionUtils.isEmpty(hInfo.getOptions())) {
				hotelIterator.remove();
			} else {
				buildProcessOptions(hInfo, searchQuery);
			}
		}
		LogUtils.log(LogTypes.HOTEL_POST_APPLY_COMMERCIAL_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.supplierId(searchQuery.getMiscInfo().getSupplierId()).trips(searchResult.getNoOfHotelOptions())
						.build(),
				LogTypes.HOTEL_POST_APPLY_COMMERCIAL_PROCESS_START);
	}

	private void storeSearchResult(HotelSearchResult hotelSearchResult, HotelSearchQuery searchQuery) {
		List<String> supplierIds = HotelUtils.getHotelSupplierIds(searchQuery);
		long startTime = System.currentTimeMillis();
		try {
			log.info("Inside store search result for search id {} and supplier {}", searchQuery.getSearchId(),
					supplierIds);
			LogUtils.log(LogTypes.HOTEL_STORE_SEARCH_RESULT_IN_CACHE_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			cacheHandler.storeSearchResult(hotelSearchResult, searchQuery);
		} finally {
			LogUtils.log(LogTypes.HOTEL_STORE_SEARCH_RESULT_IN_CACHE_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.supplierId(supplierIds.toString()).trips(hotelSearchResult.getNoOfHotelOptions()).build(),
					LogTypes.HOTEL_STORE_SEARCH_RESULT_IN_CACHE_PROCESS_START);
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, hotelSearchResult.getHotelInfos(), System.currentTimeMillis() - startTime);
		}
	}

	public void mergeOptions(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		sortOptions(hInfo);
		// mergeSortedOptions(hInfo, searchQuery);
		buildProcessOptions(hInfo, searchQuery);
	}

	public void applyPropertyType(HotelInfo hotelInfo, HotelSearchQuery searchQuery, ContextData contextData) {
		long startTime = System.currentTimeMillis();
		HotelPropertyCategoryOutput configuratorOutput = null;
		try {

			if (StringUtils.isBlank(hotelInfo.getPropertyType())) {
				if (MapUtils.getObject(SystemContextHolder.getContextData().getValueMap(),
						HotelConfiguratorRuleType.PROPERTY) == null) {
					List<HotelConfiguratorInfo> hotelpropertyrules =
							HotelConfiguratorHelper.getHotelConfiguratorRules(HotelConfiguratorRuleType.PROPERTY);
					if (CollectionUtils.isNotEmpty(hotelpropertyrules)) {
						configuratorOutput = HotelConfiguratorHelper.getRuleOutPut(null, hotelpropertyrules);
						contextData.setValue(HotelConfiguratorRuleType.PROPERTY.name() + BaseHotelConstants.RULE_OUTPUT,
								configuratorOutput);
					}
				} else {
					configuratorOutput = (HotelPropertyCategoryOutput) contextData.getValueMap()
							.get(HotelConfiguratorRuleType.PROPERTY.name() + BaseHotelConstants.RULE_OUTPUT);
				}

				if (Objects.nonNull(configuratorOutput)) {
					for (Map.Entry<PropertyType, PropertyCriteria> entry : configuratorOutput.getPropertyCategory()
							.entrySet()) {
						PropertyCriteria propertyCriteria = entry.getValue();
						if (propertyCriteria.isPropertyCriteriaMatches(hotelInfo)) {
							hotelInfo.setPropertyType(entry.getKey().name());
							break;
						}
					}
					if (StringUtils.isEmpty(hotelInfo.getPropertyType())) {
						hotelInfo.setPropertyType(PropertyType.HOTEL.name());
					}
				}
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Arrays.asList(hotelInfo), System.currentTimeMillis() - startTime);
		}
	}

	public void mapHotelMealBasis(Map<String, String> hotelStaticMealInfoMap, HotelInfo hInfo,
			HotelSearchQuery hotelSearchQuery, HotelMissingInfo missingInfo) {
		if (MapUtils.isNotEmpty(hotelStaticMealInfoMap)) {
			hInfo.getOptions().forEach(option -> {
				option.getRoomInfos().forEach(roomInfo -> {
					Set<String> amenities = new HashSet<>();
					String meal = roomInfo.getMealBasis();
					amenities.addAll(StringUtils.isNotBlank(meal) ? Arrays.asList(meal)
							: (Objects.nonNull(roomInfo.getMiscInfo())
									? ObjectUtils.firstNonNull(roomInfo.getMiscInfo().getAmenities(), Arrays.asList())
									: Arrays.asList()));
					mapHotelMealBasis(roomInfo, hotelStaticMealInfoMap, hotelSearchQuery, missingInfo, amenities);
				});
			});
		} else {
			hInfo.getOptions().forEach(option -> {
				option.getRoomInfos().forEach(roomInfo -> {
					RoomMiscInfo miscInfo = roomInfo.getMiscInfo();
					List<String> amenities = null;
					amenities = Objects.nonNull(miscInfo) ? miscInfo.getAmenities() : new ArrayList<>();
					roomInfo.setMealBasis(BaseHotelConstants.ROOM_ONLY);
					if (CollectionUtils.isNotEmpty(amenities)) {
						missingInfo.getMealBasis().addAll(amenities);
						if (Objects.nonNull(miscInfo)) {
							roomInfo.getMiscInfo().setAmenities(null);
						}
					}
				});
			});
		}
	}

	private static void mapHotelMealBasis(RoomInfo roomInfo, Map<String, String> hotelStaticMealInfoMap,
			HotelSearchQuery hotelSearchQuery, HotelMissingInfo missingInfo, Set<String> amenities) {
		if (!CollectionUtils.isEmpty(amenities)) {
			String mealBasis = "";
			for (String amenity : amenities) {
				mealBasis = hotelStaticMealInfoMap.getOrDefault(amenity.trim().toUpperCase(), null);
				if (StringUtils.isNotBlank(mealBasis)) {
					roomInfo.setMealBasis(mealBasis);
					break;
				}
			}
			if (StringUtils.isBlank(mealBasis)) {
				roomInfo.setMealBasis(BaseHotelConstants.ROOM_ONLY);
				missingInfo.getMealBasis().addAll(amenities);
			}
		}
		if (Objects.nonNull(roomInfo.getMiscInfo())) {
			roomInfo.getMiscInfo().setAmenities(null);
		}
		roomInfo.setMealBasis(
				StringUtils.isEmpty(roomInfo.getMealBasis()) ? BaseHotelConstants.ROOM_ONLY : roomInfo.getMealBasis());
	}


	public Map<String, String> fetchMealInfoData(List<HotelInfo> hotelInfos, HotelSearchQuery searchQuery,
			Set<String> supplierMealInfoIdList) {
		long startTime = System.currentTimeMillis();
		try {
			LogUtils.log(LogTypes.HOTEL_FETCH_MEAL_SUPPLIER_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			Map<String, String> hotelStaticMealInfoMap =
					cacheHandler.fetchMealInfoFromAerospike(searchQuery, supplierMealInfoIdList);

			LogUtils.log(LogTypes.HOTEL_FETCH_MEAL_SUPPLIER_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.trips(supplierMealInfoIdList.size()).supplierId(searchQuery.getMiscInfo().getSupplierId())
							.build(),
					LogTypes.HOTEL_FETCH_MEAL_SUPPLIER_PROCESS_START);
			return hotelStaticMealInfoMap;
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, hotelInfos, System.currentTimeMillis() - startTime);
		}
	}

	public Map<String, HotelInfo> fetchHotelStaticInfoMap(List<HotelInfo> hotelInfos, HotelSearchQuery searchQuery,
			Set<String> supplierHotelIdList, OperationType operationType) {
		long startTime = System.currentTimeMillis();
		try {
			LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			log.info("Number of static info is getting fetched for search id {} and supplier {} are {}",
					searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(), supplierHotelIdList.size());
			if (CollectionUtils.isEmpty(supplierHotelIdList)) {
				return new HashMap<>();
			}

			Map<String, HotelInfo> hotelStaticDataMap = fetchAndPopulateHotelInfo(
					Objects.isNull(searchQuery.getSourceId()) ? ""
							: HotelUtils.getSourceNameFromSourceId(searchQuery.getSourceId()),
					supplierHotelIdList, searchQuery, operationType);

			log.info("Number of static is fetched for search id {} and supplier {} are {}", searchQuery.getSearchId(),
					searchQuery.getMiscInfo().getSupplierId(), supplierHotelIdList.size());

			LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.trips(supplierHotelIdList.size()).supplierId(searchQuery.getMiscInfo().getSupplierId())
							.build(),
					LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_PROCESS_START);
			return hotelStaticDataMap;
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, hotelInfos, System.currentTimeMillis() - startTime);
		}
	}

	private void populateHotelAndMealIds(HotelSearchResult searchResult, Set<String> supplierHotelIdList,
			Set<String> supplierMealInfoIdList) {

		searchResult.getHotelInfos().stream().forEach(hotelInfo -> {
			if (StringUtils.isNotBlank(hotelInfo.getMiscInfo().getSupplierStaticHotelId()))
				supplierHotelIdList.add(hotelInfo.getMiscInfo().getSupplierStaticHotelId());
			hotelInfo.getOptions().forEach(option -> {
				option.getRoomInfos().forEach(roomInfo -> {
					if (StringUtils.isNotBlank(roomInfo.getMealBasis())) {
						supplierMealInfoIdList.add(roomInfo.getMealBasis());
					}
				});
			});
		});
	}

	public void populateMissingHotelInfo(Map<String, HotelInfo> hotelStaticDataMap, HotelInfo hInfo,
			HotelMissingInfo missingInfo, HotelSearchQuery searchQuery, HotelStaticDataService staticDataService) {
		String supplierHotelId = hInfo.getMiscInfo().getSupplierStaticHotelId();
		hInfo.setSupplierName(HotelUtils.firstNonEmpty(hInfo.getSupplierName(),
				HotelUtils.getSourceNameFromSourceId(searchQuery.getSourceId())));
		hInfo.getMiscInfo().setIsQuarantinePackage(Objects.isNull(searchQuery.getSearchPreferences()) ? null
				: searchQuery.getSearchPreferences().getIsQuarantinePackage());
		if (hInfo.getMiscInfo().getSearchKeyExpiryTime() == null) {
			hInfo.getMiscInfo().setSearchKeyExpiryTime(LocalDateTime.now().plusMinutes(20));
		}
		HotelInfo staticHotel = hotelStaticDataMap.get(staticDataService.getSupplierStaticHotelId(hInfo));
		if (StringUtils.isBlank(supplierHotelId)) {
			hInfo.setId(HotelUtils.generateKey(hInfo, staticHotel, searchQuery, staticDataService));
			return;
		}
		if (Objects.nonNull(staticHotel)) {
			populateMissingHotelBasicInfo(staticHotel, hInfo, searchQuery);
			if (Objects.nonNull(hInfo.getAddress())) {
				Address address = hInfo.getAddress();
				address.setCity(null);
				address.setState(null);
				address.setCountry(null);
			}
		}

		if (hInfo.getName() == null)
			return;

		hInfo.setId(HotelUtils.generateKey(hInfo, staticHotel, searchQuery, staticDataService));
		logMissingHotelDetails(hInfo, missingInfo);
	}

	public void populateMissingHotelBasicInfo(HotelInfo staticHotel, HotelInfo hInfo, HotelSearchQuery searchQuery) {

		if (staticHotel.isAddressPresent()) {
			populateMissingAddressDetails(hInfo, staticHotel);
		}

		if (CollectionUtils.isNotEmpty(staticHotel.getImages())) {
			hInfo.setImages(staticHotel.getImages());
		}

		if (Objects.nonNull(staticHotel.getRating())) {
			hInfo.setRating(staticHotel.getRating());
		}

		if (StringUtils.isNotBlank(staticHotel.getName())) {
			hInfo.setName(staticHotel.getName());
		}

		if (Objects.nonNull(staticHotel.getGeolocation())) {
			hInfo.setGeolocation(staticHotel.getGeolocation());
		}

		if (Objects.nonNull(staticHotel.getGiataId())) {
			hInfo.setGiataId(staticHotel.getGiataId());
		}

		if (Objects.nonNull(staticHotel.getUnicaId())) {
			hInfo.setUnicaId(staticHotel.getUnicaId());
		}

		if (Objects.nonNull(staticHotel.getPropertyType())) {
			hInfo.setUnicaId(Strings.toUpperCase(staticHotel.getPropertyType()));
		}

		if (Objects.nonNull(staticHotel.getUserReviewSupplierId())) {
			hInfo.setUserReviewSupplierId(staticHotel.getUserReviewSupplierId());
		}
		hInfo.setSupplierName(staticHotel.getSupplierName());
		hInfo.setAdditionalInfo(staticHotel.getAdditionalInfo());
		hInfo.setLocalHotelCode(staticHotel.getId());
	}

	public void populateMissingHotelDetailInfo(HotelInfo staticHotel, HotelInfo hInfo, HotelSearchQuery searchQuery) {

		if (StringUtils.isNotBlank(staticHotel.getSupplierHotelId())) {
			hInfo.setSupplierHotelId(staticHotel.getSupplierHotelId());
		}

		if (StringUtils.isNotBlank(staticHotel.getDescription())) {
			hInfo.setDescription(staticHotel.getDescription());
		}

		if (StringUtils.isNotBlank(staticHotel.getLongDescription())) {
			hInfo.setDescription(staticHotel.getLongDescription());
		}

		if (Objects.nonNull(staticHotel.getInstructions())) {
			hInfo.setInstructions(staticHotel.getInstructions());
		}

		if (CollectionUtils.isNotEmpty(staticHotel.getFacilities())) {
			hInfo.setFacilities(staticHotel.getFacilities());
		}

		if (Objects.nonNull(staticHotel.getContact())) {
			hInfo.setContact(Contact.builder().phone(staticHotel.getContact().getPhone())
					.email(staticHotel.getContact().getEmail()).website(staticHotel.getContact().getWebsite()).build());
		}
	}

	public void populateMissingHotelRoomInfo(HotelInfo staticHotel, HotelInfo hInfo, HotelSearchQuery searchQuery) {

		if (CollectionUtils.isNotEmpty(staticHotel.getOptions())) {
			List<Option> options = hInfo.getOptions();
			List<Option> staticHotelOptions = staticHotel.getOptions();

			Map<String, List<RoomInfo>> groupByRoomId = staticHotelOptions.get(0).getRoomInfos().stream()
					.filter(room -> StringUtils.isNotBlank(room.getId()))
					.collect(Collectors.groupingBy(roomInfo -> roomInfo.getId()));

			options.forEach(option -> option.getRoomInfos().forEach(roomInfo -> {
				if (Objects.nonNull(roomInfo.getRoomAdditionalInfo())) {
					List<RoomInfo> staticRoomInfos = groupByRoomId.get(roomInfo.getRoomAdditionalInfo().getRoomId());
					if (CollectionUtils.isNotEmpty(staticRoomInfos)) {
						RoomInfo staticRoomInfo = staticRoomInfos.get(0);
						roomInfo.setRoomCategory(
								HotelUtils.firstNonEmpty(staticRoomInfo.getRoomType(), roomInfo.getRoomType()));
						roomInfo.setRoomType(
								HotelUtils.firstNonEmpty(staticRoomInfo.getRoomType(), roomInfo.getRoomType()));
						roomInfo.setDescription(
								HotelUtils.firstNonEmpty(staticRoomInfo.getDescription(), roomInfo.getDescription()));
						roomInfo.setRoomAmenities(ObjectUtils.firstNonNull(staticRoomInfo.getRoomAmenities(),
								roomInfo.getRoomAmenities()));
						RoomAdditionalInfo roomAdditionalInfo = RoomAdditionalInfo.builder().build();
						roomAdditionalInfo.setRoomId(staticRoomInfo.getId());
						roomAdditionalInfo.setArea(staticRoomInfo.getRoomAdditionalInfo().getArea());
						roomAdditionalInfo.setViews(staticRoomInfo.getRoomAdditionalInfo().getViews());
						roomAdditionalInfo.setBeds(staticRoomInfo.getRoomAdditionalInfo().getBeds());
						roomAdditionalInfo
								.setMaxGuestAllowed(staticRoomInfo.getRoomAdditionalInfo().getMaxGuestAllowed());
						roomAdditionalInfo
								.setMaxAdultAllowed(staticRoomInfo.getRoomAdditionalInfo().getMaxAdultAllowed());
						roomAdditionalInfo
								.setMaxChildrenAllowed(staticRoomInfo.getRoomAdditionalInfo().getMaxChildrenAllowed());
						roomInfo.setRoomAdditionalInfo(roomAdditionalInfo);
					}
				}
			}));
		}
	}

	public void populateMissingAddressDetails(HotelInfo hInfo, HotelInfo staticHotel) {

		if (staticHotel.isAddressPresent()) {
			Address staticHotelAddress = staticHotel.getAddress();
			hInfo.setAddress(hInfo.isAddressPresent() ? hInfo.getAddress() : Address.builder().build());
			if (staticHotelAddress.isAddressLine1Present()) {
				hInfo.getAddress().setAddressLine1(staticHotel.getAddress().getAddressLine1());
			}
			if (staticHotelAddress.isAddressLine2Present()) {
				hInfo.getAddress().setAddressLine2(staticHotel.getAddress().getAddressLine2());
			}
			if (staticHotelAddress.isCityPresent()) {
				hInfo.getAddress().setCity(staticHotel.getAddress().getCity());
				hInfo.getAddress().setCityName(staticHotel.getAddress().getCityName());
			}
			if (staticHotelAddress.isCountryPresent()) {
				hInfo.getAddress().setCountry(staticHotel.getAddress().getCountry());
				hInfo.getAddress().setCountryName(staticHotelAddress.getCountryName());
			}
			if (staticHotelAddress.isStatePresent()) {
				hInfo.getAddress().setState(staticHotelAddress.getState());
				hInfo.getAddress().setStateName(staticHotelAddress.getStateName());
			}
			if (Objects.nonNull(staticHotelAddress.getPostalCode())) {
				hInfo.getAddress().setPostalCode(staticHotelAddress.getPostalCode());
			}
		}
	}


	public void logMissingHotelDetails(HotelInfo hInfo, HotelMissingInfo missingInfo) {
		if (Objects.isNull(hInfo.getAddress())) {
			missingInfo.setAddress(true);
		}

		if (Objects.isNull(hInfo.getImages())) {
			missingInfo.setImages(true);
		}

		if (Objects.isNull(hInfo.getGeolocation())) {
			missingInfo.setGeolocation(true);
		}
		if (Objects.isNull(hInfo.getRating())) {
			missingInfo.setRating(true);
		}

		if (missingInfo.getOperationType().equals(OperationType.DETAIL_SEARCH)) {
			if (Objects.isNull(hInfo.getFacilities())) {
				missingInfo.setHotelFacilities(true);
			}

			missingInfo.setRoomFacilities(true);
			for (Option option : hInfo.getOptions()) {
				if (!CollectionUtils.isEmpty(option.getRoomInfos().get(0).getRoomAmenities())) {
					missingInfo.setRoomFacilities(false);
					break;
				}
			}
		}
	}

	public void sortSearchResult(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		long startTime = System.currentTimeMillis();
		try {
			List<HotelInfo> hotelInfoList =
					searchResult.getHotelInfos().stream().filter(hInfo -> hInfo.getOptions().size() > 0)
							.sorted(Comparator.comparing(hInfo -> hInfo.getOptions().get(0).getTotalPrice()))
							.collect(Collectors.toCollection(ArrayList::new));
			searchResult.setHotelInfos(hotelInfoList);
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, searchResult.getHotelInfos(), System.currentTimeMillis() - startTime);
		}
	}

	public void sortOptions(HotelInfo hInfo) {
		List<Option> options = hInfo.getOptions().stream().sorted(Comparator.comparing(op -> op.getTotalPrice()))
				.collect(Collectors.toCollection(ArrayList::new));
		hInfo.setOptions(options);
	}

	public boolean isValidHotelInfo(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		/**
		 * Checking Star Rating as per search criteria, In case Star rating is Blank then system won't filter those
		 * results
		 */

		if (hInfo.getName() == null)
			return false;

		if ((BooleanUtils.isTrue(searchQuery.getSearchPreferences().getFetchSpecialCategory())
				&& hInfo.getRating() == null))
			return true;

		if (CollectionUtils.isEmpty(hInfo.getOptions())
				|| !searchQuery.getSearchPreferences().getRatings().contains(hInfo.getRating()))
			return false;

		if (!isValidAgainstExternalRules(hInfo, searchQuery))
			return false;

		return true;
	}

	public boolean isValidAgainstExternalRules(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		User user = SystemContextHolder.getContextData().getUser();
		HotelBasicFact hotelFact = HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery)
				.generateFactFromHotelInfo(hInfo).generateFactFromUserId(user.getUserId());
		List<HotelConfiguratorInfo> hotelconfigrules =
				HotelConfiguratorHelper.getHotelConfigRulesFromValueMap(HotelConfiguratorRuleType.SEARCHEXCLUSION);

		if (CollectionUtils.isNotEmpty(hotelconfigrules)) {
			SearchExclusionOutput searchExclusionConfig =
					HotelConfiguratorHelper.getRuleOutPut(hotelFact, hotelconfigrules);
			return Objects.isNull(searchExclusionConfig) || CollectionUtils.isEmpty(searchExclusionConfig.getUnicaIds())
					|| !searchExclusionConfig.getUnicaIds().contains(hInfo.getUnicaId());
		}
		return true;
	}

	public void updateManagementFees(HotelInfo hotelInfo, HotelSearchQuery searchQuery) {
		long startTime = System.currentTimeMillis();
		try {
			for (Option option : hotelInfo.getOptions()) {
				for (RoomInfo roomInfo : option.getRoomInfos()) {
					BaseHotelUtils.updateManagementFee(roomInfo, searchQuery);
				}
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Arrays.asList(hotelInfo), System.currentTimeMillis() - startTime);
		}
	}

	public void updateClientMarkup(HotelInfo hInfo, HotelSearchQuery searchQuery, User user, Boolean isSearch) {

		HotelBasicFact hotelFact = HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery)
				.generateFactFromHotelInfo(hInfo).generateFactFromUserId(user.getUserId());
		List<HotelConfiguratorInfo> hotelconfigrules =
				HotelConfiguratorHelper.getHotelConfiguratorRules(HotelConfiguratorRuleType.CLIENTMARKUP);

		if (!CollectionUtils.isEmpty(hotelconfigrules)) {
			hotelFact = hotelFact.generateFactFromHotelInfo(hInfo);
			HotelClientMarkupOutput clientMarkupOutput =
					HotelConfiguratorHelper.getRuleOutPut(hotelFact, hotelconfigrules);
			if (Objects.nonNull(clientMarkupOutput) && clientMarkupOutput.getHotelClientMarkUp() > 0) {
				for (Option option : hInfo.getOptions()) {
					for (RoomInfo roomInfo : option.getRoomInfos()) {
						roomInfo.getPerNightPriceInfos().forEach(priceInfo -> {
							priceInfo.getFareComponents().put(HotelFareComponent.CMU,
									clientMarkupOutput.getHotelClientMarkUp());
						});
					}
				}
				BaseHotelUtils.updateTotalFareComponents(hInfo, isSearch);
			}
		}
	}

	public void buildProcessOptions(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		List<ProcessedOption> processOptions = new ArrayList<>();
		Map<String, ProcessedOption> mealWiseProcessOptionMap = new HashMap<>();
		for (Option option : hInfo.getOptions()) {
			double totalPrice = 0.0;
			double discountedPrice = 0.0;
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				totalPrice += roomInfo.getTotalPrice();
				double priceLevelDiscount = 0.0d;
				for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {

					double priceInfoDiscountedPrice =
							priceInfo.getFareComponents().getOrDefault(HotelFareComponent.DS, 0.0d);
					if (priceLevelDiscount <= 0) {
						if (MapUtils.isNotEmpty(priceInfo.getAddlFareComponents())) {
							priceInfoDiscountedPrice = priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF)
									.getOrDefault(HotelFareComponent.DS, 0.0);
						}
					}
					priceLevelDiscount += priceInfoDiscountedPrice;
				}
				discountedPrice += priceLevelDiscount;
			}

			String mealBasis = option.getRoomInfos().get(0).getMealBasis();
			if (!mealWiseProcessOptionMap.isEmpty() && mealWiseProcessOptionMap.containsKey(mealBasis)) {
				if (totalPrice < mealWiseProcessOptionMap.get(mealBasis).getTotalPrice()) {
					mealWiseProcessOptionMap.put(mealBasis,
							ProcessedOption.builder().totalPrice(totalPrice).discountedPrice(discountedPrice)
									.supplierName(option.getMiscInfo().getSupplierId()).build());
				}
			} else {
				mealWiseProcessOptionMap.put(mealBasis, ProcessedOption.builder().totalPrice(totalPrice)
						.discountedPrice(discountedPrice).supplierName(option.getMiscInfo().getSupplierId()).build());
			}
			if (option.getMiscInfo() != null)
				hInfo.getSuppliers().add(option.getMiscInfo().getSupplierId());
			option.setTotalPrice(totalPrice);
		}
		for (String mealBasis : mealWiseProcessOptionMap.keySet()) {
			ProcessedOption po =
					ProcessedOption.builder().supplierName(mealWiseProcessOptionMap.get(mealBasis).getSupplierName())
							.facilities(Arrays.asList(mealBasis)).build();
			if (mealWiseProcessOptionMap.get(mealBasis).getDiscountedPrice() > 0) {
				po.setTotalPrice(mealWiseProcessOptionMap.get(mealBasis).getTotalPrice()
						+ mealWiseProcessOptionMap.get(mealBasis).getDiscountedPrice());
				po.setDiscountedPrice(mealWiseProcessOptionMap.get(mealBasis).getDiscountedPrice());
			} else {
				po.setTotalPrice(mealWiseProcessOptionMap.get(mealBasis).getTotalPrice());
			}
			processOptions.add(po);
		}
		processOptions = processOptions.stream().sorted(Comparator.comparing(po -> po.getTotalPrice()))
				.collect(Collectors.toCollection(ArrayList::new));
		hInfo.setProcessedOptions(processOptions);
	}

	public void processOptions(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		for (Iterator<Option> optionIterator = hInfo.getOptions().iterator(); optionIterator.hasNext();) {
			Option option = optionIterator.next();
			if (!arrangeRoomAccordingToSearchCriteria(option, searchQuery)) {
				log.debug("Removing optionId {} from hotel {} due to mismatch of search criteria", option.getId(),
						hInfo.getName());
				optionIterator.remove();
			}
		}
	}

	private boolean arrangeRoomAccordingToSearchCriteria(Option option, HotelSearchQuery searchQuery) {
		List<RoomInfo> roomInfoList = option.getRoomInfos();
		List<RoomSearchInfo> sRoomInfo = searchQuery.getRoomInfo();
		int index = 0;
		for (RoomInfo room : roomInfoList) {
			RoomSearchInfo sRoom = sRoomInfo.get(index);
			int i = search(roomInfoList, sRoom, index);
			if (i == -1) {
				return false;
			}
			Collections.swap(roomInfoList, i, index);
			index++;
		}
		return true;
	}

	private int search(List<RoomInfo> roomInfoList, RoomSearchInfo sRoom, int startIndex) {
		for (int i = startIndex; i < roomInfoList.size(); i++) {
			RoomInfo roomInfo = roomInfoList.get(i);
			if (roomInfo.getNumberOfAdults() == sRoom.getNumberOfAdults()
					&& (sRoom.getNumberOfChild() == null || Objects.isNull(roomInfo.getNumberOfChild())
							|| roomInfo.getNumberOfChild() == sRoom.getNumberOfChild())) {
				return i;
			}
		}
		return -1;
	}

	public void applyPromotionIfApplicable(HotelInfo hotelInfo, HotelSearchQuery searchQuery) {
		long startTime = System.currentTimeMillis();
		try {
			HotelBasicFact hotelFact = HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery);
			List<HotelConfiguratorInfo> hotelconfigrules =
					HotelConfiguratorHelper.getHotelConfigRulesFromValueMap(HotelConfiguratorRuleType.PROMOTION);

			if (!CollectionUtils.isEmpty(hotelconfigrules)) {
				hotelFact = hotelFact.generateFactFromHotelInfo(hotelInfo);
				HotelPromotionOutput promotionDetails =
						HotelConfiguratorHelper.getRuleOutPut(hotelFact, hotelconfigrules);
				if (promotionDetails != null) {
					HotelPromotion promotion = HotelPromotion.builder()
							.isFlexibleTimingAllowed(promotionDetails.getFlexibleTiming())
							.isLowestPriceAllowed(promotionDetails.getLowestPrice()).msg(promotionDetails.getMsg())
							.preferredHotel(promotionDetails.getPreferredHotel()).build();
					hotelInfo.getTags().add(HotelTag.PREFERRED);
					hotelInfo.setPromotionInfo(promotion);
				}
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Arrays.asList(hotelInfo), System.currentTimeMillis() - startTime);
		}
	}

	public void processHotelFareComponents(HotelInfo hotelInfo, HotelSearchQuery searchQuery, User user,
			Boolean isSearch) {
		long startTime = System.currentTimeMillis();
		try {
			if (!(user != null && user.getAdditionalInfo() != null
					&& BooleanUtils.isTrue(user.getAdditionalInfo().getDisableMarkup()))) {
				userFeeManager.processUserFee(user, hotelInfo, UserFeeType.MARKUP, HotelFareComponent.MU);
				BaseHotelUtils.updateTotalFareComponents(hotelInfo, isSearch);
			}
		} catch (Exception e) {
			log.error("Exception occured while processing fare components for hotel {} , query {} ",
					hotelInfo.getName(), searchQuery, e);
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Arrays.asList(hotelInfo), System.currentTimeMillis() - startTime);
		}
	}

	private Map<String, HotelInfo> fetchAndPopulateHotelInfo(String supplierName, Set<String> supplierHotelIds,
			HotelSearchQuery searchQuery, OperationType operationType) {

		HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();
		return staticDataService.processAndFetchHotelInfo(supplierName, supplierHotelIds, searchQuery, operationType);
	}

}
