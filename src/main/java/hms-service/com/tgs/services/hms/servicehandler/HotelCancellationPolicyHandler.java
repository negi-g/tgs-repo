package com.tgs.services.hms.servicehandler;

import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomSoldOut;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelCancellationPolicySearch;
import com.tgs.services.hms.restmodel.HotelCancellationPolicyRequest;
import com.tgs.services.hms.restmodel.HotelCancellationPolicyResponse;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelCancellationPolicyHandler
		extends ServiceHandler<HotelCancellationPolicyRequest, HotelCancellationPolicyResponse> {

	@Autowired
	HotelCancellationPolicySearch search;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() throws Exception {

		HotelInfo hInfo = null;
		HotelSearchQuery searchQuery = null;
		String searchId = null;
		try {
			hInfo = cacheHandler.getCachedHotelById(request.getId());
			if (Objects.isNull(hInfo)) {
				log.error("Requested hotel {} is not available now ", hInfo);
				contextData.getErrorMessages().add("Hotel not found in cache while doing cnp search");
				throw new CustomGeneralException(SystemError.HOTEL_NOT_FOUND,
						StringUtils.join(" You can use hotel Id ", request.getId(), " for reference"));
			}
			searchId = HotelUtils.getSearchId(request.getId());
			searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
			search.getCancellationPolicy(searchQuery, hInfo, request.getOptionId(), searchId);
			Option option = HotelUtils.filterOptionById(hInfo, request.getOptionId());
			HotelCancellationPolicy cp = option.getCancellationPolicy();
			response.setCancellationPolicy(cp);
			response.setId(request.getId());

		} catch (CustomGeneralException e) {
			String message = StringUtils.join("Failed to fetch cancellation policy for hotelId", request.getId(),
					", optionId ", request.getOptionId());
			RoomSoldOut hotelNotAvailable =
					RoomSoldOut.builder().type(AlertType.HOTEL_NOT_AVAILABLE.name()).message(message).build();
			response.addAlert(hotelNotAvailable);
			response.setStatus(new Status(HttpStatusCode.HTTP_200));
			log.error("Failed to fetch cancellation policy for hotelId {}, optionid {}, error msg is {}",
					request.getId(), request.getOptionId(), contextData.getErrorMessages(), e);

			if (Objects.nonNull(e.getError()) && e.getError().equals(SystemError.HOTEL_NOT_FOUND)) {
				contextData.getErrorMessages().add(SystemError.HOTEL_NOT_FOUND.getMessage());
			} else {
				contextData.getErrorMessages()
						.add("Failed to fetch cancellation policy for hotel, as hotelinfo not found in cache.");
			}
		} catch (Exception e) {
			log.error("Error while searching for cancellation policy for query {}", searchQuery, e);
		} finally {
			SystemContextHolder.getContextData().getReqIds().add(searchId);
		}
	}

	@Override
	public void afterProcess() throws Exception {}

}
