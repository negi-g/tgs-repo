package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.booking.BookingDetailResponse;
import com.tgs.services.hms.datamodel.tbo.booking.GetBookingDetailResult;
import com.tgs.services.hms.datamodel.tbo.booking.HotelBookingResponse;
import com.tgs.services.hms.datamodel.tbo.booking.HotelConfirmBookingResponse;
import com.tgs.services.hms.datamodel.tbo.booking.TravelBoutiqueBookingDetailRequest;
import com.tgs.services.hms.datamodel.tbo.booking.Travellers;
import com.tgs.services.hms.datamodel.tbo.search.DayRate;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomDetail;
import com.tgs.services.hms.datamodel.tbo.search.Price;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
@Getter
@Setter
@SuperBuilder
public class TravelBoutiqueRetrieveBookingService extends TravelBoutiqueBaseService {

	private HotelImportBookingParams importBookingParams;
	private HotelSupplierConfiguration supplierConf;
	private HotelImportedBookingInfo bookingInfo;
	protected HotelSourceConfigOutput sourceConfigOutput;
	HotelBookingResponse bookingResponse;
	HotelConfirmBookingResponse confirmBookingResponse;
	protected RestAPIListener listener;
	public HotelCacheHandler cacheHandler;


	public void retrieveBooking() throws IOException {

		HttpUtils httpUtils = null;
		TravelBoutiqueBookingDetailRequest bookingDetailRequest = null;
		try {
			listener = new RestAPIListener("");
			bookingDetailRequest = getBookingDetailRequest(importBookingParams.getSupplierBookingId());
			httpUtils = HttpUtils.builder().urlString(supplierConf.getHotelAPIUrl(HotelUrlConstants.BOOKING_DETAIL))
					.postData(GsonUtils.getGson().toJson(bookingDetailRequest))
					.headerParams(TravelBoutiqueUtil.getHeaderParams()).build();
			BookingDetailResponse bookingDetailResponse =
					httpUtils.getResponse(BookingDetailResponse.class).orElse(null);
			createBookingDetailResponse(bookingDetailResponse);
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TBO.name())
					.postData(GsonUtils.getGson().toJson(bookingDetailRequest))
					.requestType(BaseHotelConstants.RETRIEVE_BOOKING).responseString(httpUtils.getResponseString())
					.urlString(httpUtils.getUrlString()).logKey(importBookingParams.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
	}

	private TravelBoutiqueBookingDetailRequest getBookingDetailRequest(String bookingId) throws IOException {

		TravelBoutiqueBookingDetailRequest bookingDetailRequest = new TravelBoutiqueBookingDetailRequest();
		bookingDetailRequest.setEndUserIp(TravelBoutiqueUtil.IP);
		String token = getCachedToken();
		bookingDetailRequest.setTokenId(token);
		bookingDetailRequest.setBookingId(bookingId);
		return bookingDetailRequest;

	}


	private HotelImportedBookingInfo createBookingDetailResponse(BookingDetailResponse bookingDetailResponse) {

		HotelImportedBookingInfo importBooking = null;
		if (bookingDetailResponse == null || bookingDetailResponse.getGetBookingDetailResult() == null)
			throw new CustomGeneralException("Error While Trying To Fetch Booking Details");
		try {
			GetBookingDetailResult response = bookingDetailResponse.getGetBookingDetailResult();
			HotelInfo hInfo = getHotelDetails(response);
			HotelSearchQuery searchQuery = getHotelSearchQuery(response);
			List<RoomInfo> roomInfos = new ArrayList<>();
			String curr = response.getHotelRoomsDetails().get(0).getPrice().getCurrencyCode();
			for (HotelRoomDetail roomDetail : response.getHotelRoomsDetails()) {
				RoomInfo roomInfo = new RoomInfo();
				roomInfo.setId(roomDetail.getRoomTypeCode());
				roomInfo.setRoomType(roomDetail.getRoomTypeName());
				roomInfo.setRoomCategory(roomDetail.getRoomTypeName());
				roomInfo.setPerNightPriceInfos(getPriceInfos(roomDetail));
				roomInfo.setTotalPrice(roomDetail.getPrice().getPublishedPrice());
				roomInfo.setRoomAmenities(roomDetail.getAmenity());
				roomInfo.setDeadlineDateTime(roomDetail.getLastCancellationDate());
				roomInfo.setCancellationPolicy(TravelBoutiqueUtil.getCancellationPolicy(roomDetail, roomInfo));
				roomInfo.setCheckInDate(searchQuery.getCheckinDate());
				roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
				setMealBasis(roomInfo, roomDetail.getAmenities(), searchQuery);
				RoomMiscInfo roomMiscInfo =
						RoomMiscInfo.builder().price(TravelBoutiqueUtil.getRoomPrice(roomDetail.getPrice()))
								.roomIndex(roomDetail.getRoomIndex()).ratePlanCode(roomDetail.getRatePlanCode())
								.roomTypeCode(roomDetail.getRoomTypeCode())
								.bedTypes(TravelBoutiqueUtil.getBedTypes(roomDetail.getBedTypes()))
								.roomTypeName(roomDetail.getRoomTypeName()).build();
				roomInfo.setMiscInfo(roomMiscInfo);
				List<TravellerInfo> travellerInfoList = getTravellerList(roomDetail.getHotelPassenger());
				roomInfo.setTravellerInfo(travellerInfoList);

				roomInfos.add(roomInfo);
			}
			double totalPrice = roomInfos.stream().mapToDouble(RoomInfo::getTotalPrice).sum();
			Option option = Option.builder().totalPrice(totalPrice)
					.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
							.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
							.supplierHotelId(hInfo.getMiscInfo().getSupplierStaticHotelId())
							.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
					.roomInfos(roomInfos).id(RandomStringUtils.random(20, true, true)).build();
			TravelBoutiqueUtil.updatePriceWithMarkup(option.getRoomInfos(), sourceConfigOutput);
			List<Option> options = new ArrayList<>();
			options.add(option);
			HotelUtils.setBufferTimeinCnp(hInfo.getOptions(), searchQuery);

			hInfo.setOptions(options);
			String orderStatus = TBOOrderMapping.valueOf(response.getHotelBookingStatus().toUpperCase()).getCode();

			DeliveryInfo deliveryInfo = setDeliveryInfo(response);
			importBooking = HotelImportedBookingInfo.builder().hInfo(hInfo).searchQuery(searchQuery)
					.orderStatus(orderStatus).deliveryInfo(deliveryInfo).bookingCurrencyCode(curr)
					.bookingDate(LocalDate.parse(response.getBookingDate())).build();

		} catch (Exception ex) {
			log.info("exception while parsing tbo retrieve response for booking id {}",
					importBookingParams.getBookingId(), ex);
			throw ex;
		}
		return importBooking;
	}

	private DeliveryInfo setDeliveryInfo(GetBookingDetailResult roomDetails) {

		DeliveryInfo deliveryInfo = new DeliveryInfo();
		roomDetails.getHotelRoomsDetails().forEach(roomDetail -> {
			roomDetail.getHotelPassenger().forEach(passenger -> {
				if (passenger.getLeadPassenger()) {
					deliveryInfo.setContacts(Arrays.asList(passenger.getPhoneno()));
					deliveryInfo.setEmails(Arrays.asList(passenger.getEmail()));
				}
			});
		});
		return deliveryInfo;
	}

	private List<TravellerInfo> getTravellerList(List<Travellers> travellerInfos) {

		List<TravellerInfo> travellerInfoList = new ArrayList<>();
		for (Travellers traveller : travellerInfos) {
			if (traveller.getPaxType() == 2) {
				TravellerInfo travellerInfo = new TravellerInfo();
				travellerInfo.setAge(traveller.getAge());
				travellerInfo.setFirstName(traveller.getFirstName());
				travellerInfo.setLastName(traveller.getPAN());
				travellerInfo.setTitle(traveller.getTitle());
				travellerInfo.setLastName(traveller.getLastName());
				travellerInfo.setPaxType(PaxType.CHILD);
				travellerInfoList.add(travellerInfo);
			} else {
				TravellerInfo travellerInfo = new TravellerInfo();
				travellerInfo.setAge(traveller.getAge());
				travellerInfo.setFirstName(traveller.getFirstName());
				travellerInfo.setLastName(traveller.getPAN());
				travellerInfo.setLastName(traveller.getLastName());
				travellerInfo.setTitle(traveller.getTitle());
				travellerInfo.setPaxType(PaxType.ADULT);
				travellerInfoList.add(travellerInfo);
			}
		}

		return travellerInfoList;
	}

	private HotelSearchQuery getHotelSearchQuery(GetBookingDetailResult response) {

		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
		searchQueryBuilder.checkinDate(LocalDate.parse(response.getCheckInDate()));
		searchQueryBuilder.checkoutDate(LocalDate.parse(response.getCheckOutDate()));
		searchQueryBuilder.miscInfo(HotelSearchQueryMiscInfo.builder().supplierId(HotelSourceType.TBO.name()).build());
		searchQueryBuilder.sourceId(HotelSourceType.TBO.getSourceId());

		return searchQueryBuilder.build();
	}

	private HotelInfo getHotelDetails(GetBookingDetailResult response) {

		Address address = Address.builder().addressLine1(response.getAddressLine1()).build();

		GeoLocation geoLocation = null;
		if (StringUtils.isNotBlank(response.getLatitude()) && StringUtils.isNotBlank(response.getLongitude())) {
			geoLocation =
					GeoLocation.builder().latitude(response.getLatitude()).longitude(response.getLongitude()).build();
		}

		HotelMiscInfo miscInfo = HotelMiscInfo.builder().supplierBookingId(response.getBookingId() + "")
				.supplierBookingConfirmationNo(response.getConfirmationNo())
				.hotelBookingReference(response.getBookingRefNo() + "")
				.supplierBookingReference(response.getBookingRefNo()).build();


		HotelInfo hInfo = HotelInfo.builder().name(response.getHotelName()).rating(response.getStarRating())
				.address(address).geolocation(geoLocation).miscInfo(miscInfo).build();

		return hInfo;
	}

	public static List<PriceInfo> getPriceInfos(HotelRoomDetail roomDetail) {

		int i = 1;
		List<PriceInfo> priceInfos = new ArrayList<>();
		Price supplierPriceInfo = roomDetail.getPrice();
		Double totalTaxesAndFees = supplierPriceInfo.getPublishedPriceRoundedOff() - supplierPriceInfo.getRoomPrice();
		Double perRoomNightTaxAndFees = totalTaxesAndFees / roomDetail.getDayRates().size();
		for (DayRate dayRate : roomDetail.getDayRates()) {
			PriceInfo priceInfo = new PriceInfo();
			Double totalPerRoomNightPrice = dayRate.getAmount() + perRoomNightTaxAndFees;
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, totalPerRoomNightPrice);
			fareComponents.put(HotelFareComponent.TAF, perRoomNightTaxAndFees);
			priceInfo.setFareComponents(fareComponents);
			priceInfo.setDay(i);
			priceInfo.setTotalPrice(totalPerRoomNightPrice);
			priceInfos.add(priceInfo);
			i++;
		}
		return priceInfos;
	}

	public void setMealBasis(RoomInfo roomInfo, List<String> facilities, HotelSearchQuery searchQuery) {
		if (CollectionUtils.isEmpty(facilities)) {
			roomInfo.setMealBasis("Room Only");
			return;
		}

		if (facilities.get(0).contains(",")) {
			facilities = Arrays.asList(facilities.get(0).split(","));
		}
		Map<String, String> mealBasis = cacheHandler.fetchMealInfoFromAerospike(searchQuery, new HashSet<>(facilities));
		for (String amenity : facilities) {
			if (!org.springframework.util.ObjectUtils.isEmpty(roomInfo.getMealBasis())
					&& !roomInfo.getMealBasis().equals("Room Only")) {
				return;
			}
			roomInfo.setMealBasis(mealBasis.getOrDefault(amenity.toLowerCase(), "Room Only"));
		}
	}

}
