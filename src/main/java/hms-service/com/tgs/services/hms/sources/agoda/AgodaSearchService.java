package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.agoda.search.AgodaSearchRequest;
import com.tgs.services.hms.datamodel.agoda.search.AgodaSearchResponse;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room;
import com.tgs.services.hms.datamodel.agoda.search.ChildrenAges;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
@SuperBuilder
public class AgodaSearchService extends AgodaBaseService {

	private HotelSearchResult searchResult;
	private Set<String> hotelIds;

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public void doSearch(int threadCount) throws IOException, JAXBException {
		HttpUtils httpUtils = null;
		searchResult = HotelSearchResult.builder().build();
		listener = new RestAPIListener("");
		AgodaSearchRequest searchRequest = createSearchRequest();
		try {
			String xmlRequest = AgodaMarshaller.marshallXml(searchRequest);
			httpUtils = AgodaUtil.getHttpUtils(xmlRequest, HotelUrlConstants.HOTEL_SEARCH_URL, null, supplierConf);
			httpUtils.setPrintResponseLog(false);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (Objects.isNull(xmlResponse)) {
				log.error("Unable to get response {}", searchQuery, xmlResponse);
				return;
			}
			AgodaSearchResponse Result = AgodaMarshaller.unmarshallLongSearchResponse(xmlResponse);
			searchResult.setHotelInfos(getHotelListFromAgodaResult(Result, false));
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.SEARCH.name()));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.AGODA.name())
						.headerParams(httpUtils.getHeaderParams()).requestType(BaseHotelConstants.SEARCH)
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
						.threadCount(threadCount).build());
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(searchResult)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
			}
		}
	}

	private AgodaSearchRequest createSearchRequest() {

		AgodaSearchRequest searchRequest = new AgodaSearchRequest();
		searchRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		searchRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		setOccupancy(searchRequest);
		String hotelIdList = String.join(",", hotelIds);
		searchRequest.setId(hotelIdList);
		searchRequest.setCheckIn(searchQuery.getCheckinDate().format(dateTimeFormatter));
		searchRequest.setCheckOut(searchQuery.getCheckoutDate().format(dateTimeFormatter));
		searchRequest.setCurrency(AgodaConstants.CURRENCY.getValue());
		searchRequest.setRooms(searchQuery.getRoomInfo().size() + "");
		searchRequest.setType("6");
		searchRequest.setLanguage(AgodaConstants.LANGUAGECODE.getValue());
		searchRequest.setUserCountry("IN");
		return searchRequest;
	}


	private void setOccupancy(AgodaSearchRequest searchRequest) {

		int totalNumberOfAdults = 0;
		int totalNumberOfchildren = 0;
		List<Integer> age = new ArrayList<Integer>();
		ChildrenAges childrenAges = new ChildrenAges();
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			totalNumberOfAdults += room.getNumberOfAdults();
			if (CollectionUtils.isNotEmpty(room.getChildAge()) && !Objects.isNull(room.getNumberOfChild())
					&& room.getNumberOfChild() > 0) {
				totalNumberOfchildren += room.getNumberOfChild();
				for (Integer childAge : room.getChildAge()) {
					age.add(childAge);
				}
			}
		}
		searchRequest.setAdults(totalNumberOfAdults + "");
		searchRequest.setChildren(totalNumberOfchildren + "");
		if (age.size() > 0) {
			childrenAges.setAge(age);
			searchRequest.setChildrenAges(childrenAges);
		}

	}

	protected List<HotelInfo> getHotelListFromAgodaResult(AgodaSearchResponse result, boolean isDetail) {

		List<HotelInfo> hInfos = new ArrayList<>();
		if (result == null || CollectionUtils.isEmpty(result.getHotels())) {
			log.info("Empty Response From Supplier {} for searchId {}", supplierConf.getBasicInfo().getSupplierId(),
					searchQuery.getSearchId());
			return null;
		}
		for (Hotel hotel : result.getHotels()) {
			List<Option> optionList = new ArrayList<>();
			for (Room agodaRoom : hotel.getRooms().getRoom()) {
				if (validRoomOcuupancy(agodaRoom)) {
					List<RoomInfo> roomList = getRooms(agodaRoom, isDetail);
					Option option = Option.builder().roomInfos(roomList)
							.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
									.supplierSearchId(result.getSearchid()).supplierHotelId(hotel.getId())
									.sourceId(supplierConf.getBasicInfo().getSourceId())
									.secondarySupplier(supplierConf.getBasicInfo().getSupplierId()).build())
							.id(RandomStringUtils.random(20, true, true)).build();
					if (!isDetail) {
						option.getMiscInfo().setIsNotRequiredOnDetail(true);
					}
					setOptionCancellationPolicy(agodaRoom, option);
					setOptionSurcharge(option, roomList.get(0));
					setOptionBenefits(option, agodaRoom);
					optionList.add(option);
				}
			}
			HotelInfo hInfo = HotelInfo.builder().miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
					.supplierStaticHotelId(hotel.getId()).build()).options(optionList).build();

			hInfos.add(hInfo);
		}
		return hInfos;

	}


	public void doDetailSearch() throws JAXBException, IOException {
		HttpUtils httpUtils = null;
		listener = new RestAPIListener("");
		AgodaSearchRequest searchRequest = createSearchRequest();
		AgodaSearchResponse Result = null;
		try {

			String xmlRequest = AgodaMarshaller.marshallXml(searchRequest);
			httpUtils = AgodaUtil.getHttpUtils(xmlRequest, HotelUrlConstants.HOTEL_SEARCH_URL, null, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (xmlResponse == null) {
				log.error("Unable to get response {}", searchQuery, xmlResponse);
			}

			Result = AgodaMarshaller.unmarshallLongSearchResponse(xmlResponse);
			List<HotelInfo> hotelList = getHotelListFromAgodaResult(Result, true);
			searchResult = HotelSearchResult.builder().build();
			searchResult.setHotelInfos(hotelList);

		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.SEARCH.name()));
				if (Objects.isNull(Result)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.AGODA.name())
						.headerParams(httpUtils.getHeaderParams()).requestType(BaseHotelConstants.DETAILSEARCH)
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

}

