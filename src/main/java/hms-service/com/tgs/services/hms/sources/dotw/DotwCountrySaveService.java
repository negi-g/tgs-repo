package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBException;
import com.tgs.services.hms.datamodel.HotelCountryInfoMapping;
import com.tgs.services.hms.datamodel.dotw.Country;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class DotwCountrySaveService extends DotwBaseService {

	public List<HotelCountryInfoMapping> getCountryMapppings() {

		List<HotelCountryInfoMapping> dotwCountryMappingList = new ArrayList<>();
		try {
			List<Country> countryList = getCountryList();
			for (Country country : countryList) {
				HotelCountryInfoMapping dotwCountryMapping = HotelCountryInfoMapping.builder()
						.supplierCountryId(country.getCode()).supplierName(HotelSourceType.DOTW.name())
						.supplierCountryName(country.getName()).build();
				dotwCountryMappingList.add(dotwCountryMapping);

			}
		} catch (Exception e) {
			log.info("Error while mapping cities for DOTW", e);
		}
		return dotwCountryMappingList;
	}

	private List<Country> getCountryList() throws JAXBException, IOException {

		Customer customer = getRequestForStaticDataCountry();
		String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
		HttpUtils httpUtils = getHttpRequest(xmlRequest, supplierConf);
		String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
		Results result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
		return result.getCountries();
	}

	private Customer getRequestForStaticDataCountry() {

		Customer customer = getCustomer(supplierConf);
		Request request = new Request();
		request.setCommand("getallcountries");
		customer.setRequest(request);
		return customer;

	}

}
