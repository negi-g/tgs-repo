package com.tgs.services.hms.dbmodel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.qtech.AdditionalHotelIdMappingInfo;

public class AdditionalHotelIdMappingInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return AdditionalHotelIdMappingInfo.class;
	}
}
