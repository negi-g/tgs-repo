package com.tgs.services.hms.jparepository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfoMapping;

public interface DbHotelRegionInfoMappingRepository
		extends JpaRepository<DbHotelRegionInfoMapping, Long>,
		JpaSpecificationExecutor<DbHotelRegionInfoMapping> {

	public DbHotelRegionInfoMapping findByRegionIdAndSupplierName(Long regionId, String supplierName);
	
	public Page<DbHotelRegionInfoMapping> findBySupplierName(String supplierName, Pageable pageable);

	@Query(value = "SELECT * FROM hotelregioninfomapping h WHERE h.regionid = :regionid and lower(h.suppliername) in :supplierNames",
			nativeQuery = true)
	public List<DbHotelRegionInfoMapping> findByRegionIdAndSupplierList(@Param("regionid") Long regionId,
			@Param("supplierNames") List<String> supplierNames);
	
	public List<DbHotelRegionInfoMapping> findBySupplierName(String supplierName);
	
	public List<DbHotelRegionInfoMapping> findByRegionId(Long regionId);
}
