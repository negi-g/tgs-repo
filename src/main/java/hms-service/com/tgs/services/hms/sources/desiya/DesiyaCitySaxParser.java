package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DesiyaCitySaxParser extends DefaultHandler {

	List<HotelRegionInfoQuery> regions;
	InputStream cityXmlFileName;
	String regionName;
	HotelRegionInfoQuery regionInfoQuery;

	public DesiyaCitySaxParser(InputStream cityXmlFileName, HotelRegionInfoMappingManager regionInfoMappingManager,
			HotelStaticDataRequest staticDataRequest, HotelSupplierRegionInfoManager supplierRegionInfoManager) {

		this.cityXmlFileName = cityXmlFileName;
		regions = new ArrayList<>();
		parseDocument();
		log.info("desiya region size : " + regions.size());
		HotelBaseSupplierUtils.saveOrUpdateRegionInfo(regions, staticDataRequest.getIsMasterData(), false);
	}

	public void parseDocument() {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(cityXmlFileName, this);
		} catch (ParserConfigurationException e) {
			log.error("ParserConfig error");
		} catch (SAXException e) {
			log.error("SAXException : xml not well formed");
		} catch (IOException e) {
			log.error("IO error");
		}
	}

	@Override
	public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {

		if (elementName.equalsIgnoreCase("City")) {
			regionInfoQuery = new HotelRegionInfoQuery();
		}
	}

	@Override
	public void endElement(String s, String s1, String element) throws SAXException {

		if (element.equals("City")) {
			regionInfoQuery.setRegionId(regionName);
			regionInfoQuery.setRegionName(regionName);
			regionInfoQuery.setRegionType("CITY");
			regionInfoQuery.setCountryId("INDIA");
			regionInfoQuery.setCountryName("INDIA");
			regionInfoQuery.setSupplierName(HotelSourceType.DESIYA.name());
			regions.add(regionInfoQuery);
		}
	}

	@Override
	public void characters(char[] ac, int i, int j) throws SAXException {
		regionName = new String(ac, i, j);
	}

}
