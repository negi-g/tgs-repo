package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class TripjackRetrieveBookingFactory extends AbstractRetrieveHotelBookingFactory {

	@Autowired
	HotelCacheHandler cacheHandler;
	@Autowired
	protected HMSCommunicator hmsComm;


	public TripjackRetrieveBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		super(supplierConf, importBookingInfo);
	}

	@Override
	public void retrieveBooking() throws IOException {
		TripjackRetrieveBookingService bookingService =
				TripjackRetrieveBookingService.builder().hmsComm(hmsComm).sourceConfig(sourceConfigOutput)
						.supplierConf(supplierConf).importBookingParams(importBookingParams).build();
		bookingService.retrieveBooking();
		bookingDetailResponse = bookingService.getBookingInfo();

	}
}
