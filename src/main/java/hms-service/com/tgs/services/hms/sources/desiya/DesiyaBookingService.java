package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.rmi.RemoteException;
import javax.xml.bind.JAXBException;
import org.apache.axiom.om.OMException;
import org.apache.axis2.AxisFault;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.GuaranteeType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.HotelReservation_type0;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.HotelReservationsType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.OTA_HotelResRQ;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.OTA_HotelResRS;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.POS_Type;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.ResGlobalInfoType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.SourceType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.UniqueID_Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class DesiyaBookingService extends DesiyaBaseService {

	private Order order;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelInfo hInfo;
	private SoapRequestResponseListner listener;

	public boolean book() throws IOException, JAXBException {

		OTA_HotelResRS bookResponse = null;
		boolean isbookingSuccessful = false;
		TGBookingServiceEndPointImplServiceStub stub = DesiyaBindingService.getBookingStub(supplierConf);
		listener.setType("Desiya Booking-Service");
		stub._getServiceClient().getAxisService().addMessageContextListener(listener);
		OTA_HotelResRQ bookingRequest = createBookingrequest();
		try {
			try {
				SystemContextHolder.getContextData()
						.addCheckPoint(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_STARTED.name())
								.subType(HotelFlowType.BOOK.name()).time(System.currentTimeMillis()).build());
				bookResponse = stub.createBooking(bookingRequest);
			} finally {
				SystemContextHolder.getContextData()
						.addCheckPoint(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_FINISHED.name())
								.subType(HotelFlowType.BOOK.name()).time(System.currentTimeMillis()).build());
			}
			if (bookResponse != null) {
				if (bookResponse.getErrors() == null)
					isbookingSuccessful = createBookResponse(bookResponse);
			}
		} catch (OMException om) {
			throw new NoSeatAvailableException(om.getMessage());
		} catch (AxisFault e) {
			throw new NoSearchResultException(e.getMessage());
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} catch (Exception e) {
			throw e;
		} finally {
			SystemContextHolder.getContextData()
					.addCheckPoint(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
							.subType(HotelFlowType.BOOK.name()).time(System.currentTimeMillis()).build());
			stub._getServiceClient().getAxisService().removeMessageContextListener(listener);

		}
		return isbookingSuccessful;

	}


	private boolean createBookResponse(OTA_HotelResRS bookResponse) {

		boolean isBooked = false;
		hInfo.getMiscInfo().setSupplierBookingReference(
				bookResponse.getHotelReservations().getHotelReservation()[0].getUniqueID()[0].getID());
		hInfo.getMiscInfo().setSupplierBookingId(
				bookResponse.getHotelReservations().getHotelReservation()[0].getUniqueID()[0].getID());
		isBooked = true;
		return isBooked;
	}

	private OTA_HotelResRQ createBookingrequest() {

		OTA_HotelResRQ req = new OTA_HotelResRQ();
		HotelReservationsType hotelRes = new HotelReservationsType();

		HotelReservation_type0[] hotelResList = new HotelReservation_type0[1];
		HotelReservation_type0 hotelReservation = new HotelReservation_type0();

		req.setCorrelationID(order.getBookingId());
		req.setTransactionIdentifier(order.getBookingId());
		setSource(req);

		UniqueID_Type[] uniqueIDs = new UniqueID_Type[1];
		UniqueID_Type uniqueId = new UniqueID_Type();
		uniqueId.setType("23");
		uniqueId.setID(hInfo.getMiscInfo().getSupplierBookingId());
		uniqueIDs[0] = uniqueId;
		req.setUniqueID(uniqueIDs);

		ResGlobalInfoType globalInfo = new ResGlobalInfoType();

		GuaranteeType guaranteeType = new GuaranteeType();
		guaranteeType.setGuaranteeType("PrePay");
		globalInfo.setGuarantee(guaranteeType);
		hotelReservation.setResGlobalInfo(globalInfo);
		hotelResList[0] = hotelReservation;
		hotelRes.setHotelReservation(hotelResList);
		req.setHotelReservations(hotelRes);

		return req;
	}


	private void setSource(OTA_HotelResRQ reviewrequest) {
		POS_Type posType = new POS_Type();
		SourceType[] sources = new SourceType[1];
		SourceType source = new SourceType();
		source.setRequestorID(getRequestId());
		sources[0] = source;
		posType.setSource(sources);
		reviewrequest.setPOS(posType);

	}

}

