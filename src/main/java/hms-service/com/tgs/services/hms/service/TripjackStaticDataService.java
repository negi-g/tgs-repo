package com.tgs.services.hms.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.es.datamodel.ESSearchRequest;
import com.tgs.services.es.restmodel.ESAutoSuggestionResponse;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelWiseSearchInfo;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.SupplierHotelIdMapping;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.vervotech.MasterFacilitiesResponse;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.dbmodel.DbSupplierHotelIdMapping;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import com.tgs.services.hms.sources.vervotech.VervotechStaticDataService;
import com.tgs.services.hms.sources.vervotech.VervotechSupplierMapping;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TripjackStaticDataService implements HotelStaticDataService {

	@Autowired
	GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	HotelInfoService infoService;

	@Autowired
	ElasticSearchCommunicator esCommunicator;

	@Override
	public String getSeparator() {
		return "_";
	}

	@Override
	public String getKeyToMergeHotel(HotelInfo hotelInfo) {
		return hotelInfo.getUnicaId();
	}

	@Override
	public String getHotelInfoUniqueKey(HotelInfo hotelInfo) {

		return String.join(getSeparator(), hotelInfo.getSupplierName(), hotelInfo.getSupplierHotelId());
	}

	@Override
	public String getCityName(String supplierName, String cityName) {

		return String.join(getSeparator(), supplierName, cityName);
	}

	@Override
	public String getStateName(String supplierName, String stateName) {

		return String.join(getSeparator(), supplierName, stateName);
	}

	@Override
	public String getCountryName(String supplierName, String countryName) {

		return String.join(getSeparator(), supplierName, countryName);
	}

	@Override
	public String getSupplierSetName(String supplierName) {

		return BaseHotelUtils.getSupplierSetName(supplierName);
	}

	@Override
	public String getSupplierStaticHotelId(HotelInfo hotelInfo) {

		return String.join("_", hotelInfo.getSupplierName(), hotelInfo.getMiscInfo().getSupplierStaticHotelId());
	}

	@Override
	public HotelInfo getHotelInfoFromDB(HotelInfo hotelInfo) throws IOException {
		Option option = hotelInfo.getOptions().get(0);
		DbHotelInfo dbHotelInfo =
				infoService.findBySupplierHotelIdAndSupplierNameAndUnicaId(option.getMiscInfo().getSupplierHotelId(),
						option.getMiscInfo().getSupplierId(), hotelInfo.getUnicaId());
		if (Objects.nonNull(dbHotelInfo))
			return dbHotelInfo.toDomain();
		return HotelInfo.builder().build();
	}

	@Override
	public boolean validateHotelInfo(HotelInfo updatedHotelInfo, String supplierName, String bookingId)
			throws IOException {
		try {
			HotelSupplierConfiguration supplierConf =
					HotelSupplierConfigurationHelper.getSupplierConfiguration(HotelSourceType.VERVOTECH.name());
			HotelSourceConfigOutput sourceConfigOutput = HotelUtils.getHotelSourceConfigOutput(
					HotelSearchQuery.builder().sourceId(supplierConf.getBasicInfo().getSourceId()).build());

			VervotechStaticDataService staticDataService = VervotechStaticDataService.builder()
					.sourceConfig(sourceConfigOutput).supplierConf(supplierConf).build();

			Map<Integer, String> masterFacilities = staticDataService.getMasterFacilities().stream()
					.collect(Collectors.toMap(MasterFacilitiesResponse::getId, MasterFacilitiesResponse::getName));
			String vervotechSupplierName = VervotechSupplierMapping
					.getVervotechSupplierTypeFromHotelSourceName(HotelSourceType.valueOf(supplierName))
					.getVervotechSupplierName();
			List<HotelInfo> vervotechHotelInfo =
					staticDataService.fetchHotelContent(Arrays.asList(Long.valueOf(updatedHotelInfo.getUnicaId())),
							masterFacilities, vervotechSupplierName, 1, 1, bookingId);
			if (!validateHotelInfo(vervotechHotelInfo.get(0), updatedHotelInfo)) {
				throw new CustomGeneralException(SystemError.INVALID_HOTEL_STATIC_DATA);
			}
			return true;
		} finally {
			LogUtils.clearLogList();
		}
	}

	private boolean validateHotelInfo(HotelInfo vervotechHotelInfo, HotelInfo dbHotelInfo) {

		if ((Objects.nonNull(vervotechHotelInfo.getRating()) && Objects.isNull(dbHotelInfo.getRating()))
				|| (Objects.isNull(vervotechHotelInfo.getRating()) && Objects.nonNull(dbHotelInfo.getRating()))) {
			return false;
		} else if (Objects.nonNull(vervotechHotelInfo.getRating()) && Objects.nonNull(dbHotelInfo.getRating())
				&& Integer.compare(vervotechHotelInfo.getRating(), dbHotelInfo.getRating()) != 0) {
			return false;
		}

		if ((Objects.nonNull(vervotechHotelInfo.getName()) && Objects.isNull(dbHotelInfo.getName()))
				|| (Objects.isNull(vervotechHotelInfo.getName()) && Objects.nonNull(dbHotelInfo.getName()))) {
			return false;
		} else if (Objects.nonNull(vervotechHotelInfo.getName()) && Objects.nonNull(dbHotelInfo.getName())
				&& !vervotechHotelInfo.getName().equals(dbHotelInfo.getName())) {
			return false;
		}

		if ((Objects.nonNull(vervotechHotelInfo.getDescription()) && Objects.isNull(dbHotelInfo.getDescription()))
				|| (Objects.isNull(vervotechHotelInfo.getDescription())
						&& Objects.nonNull(dbHotelInfo.getDescription()))) {
			return false;
		} else if (Objects.nonNull(vervotechHotelInfo.getDescription()) && Objects.nonNull(dbHotelInfo.getDescription())
				&& !vervotechHotelInfo.getDescription().equals(dbHotelInfo.getDescription())) {
			return false;
		}

		if (Objects.nonNull(vervotechHotelInfo.getLongDescription())
				&& Objects.nonNull(dbHotelInfo.getLongDescription())
				&& !vervotechHotelInfo.getLongDescription().equals(dbHotelInfo.getLongDescription())) {
			return false;
		} else if (Objects.nonNull(vervotechHotelInfo.getLongDescription())
				|| Objects.nonNull(dbHotelInfo.getLongDescription())) {
			return false;
		}

		if ((Objects.nonNull(vervotechHotelInfo.getPropertyType()) && Objects.isNull(dbHotelInfo.getPropertyType()))
				|| (Objects.isNull(vervotechHotelInfo.getPropertyType())
						&& Objects.nonNull(dbHotelInfo.getPropertyType()))) {
			return false;
		} else if (Objects.nonNull(vervotechHotelInfo.getPropertyType())
				&& Objects.nonNull(dbHotelInfo.getPropertyType())
				&& !vervotechHotelInfo.getPropertyType().equals(dbHotelInfo.getPropertyType())) {
			return false;
		}

		if ((Objects.nonNull(vervotechHotelInfo.getInstructions()) && Objects.isNull(dbHotelInfo.getInstructions()))
				|| (Objects.isNull(vervotechHotelInfo.getInstructions())
						&& Objects.nonNull(dbHotelInfo.getInstructions()))) {
			return false;
		} else if (Objects.nonNull(vervotechHotelInfo.getInstructions())
				&& Objects.nonNull(dbHotelInfo.getInstructions())) {
			List<Instruction> vervotechInstructions = vervotechHotelInfo.getInstructions();
			List<Instruction> dbHotelInfoInstructions = dbHotelInfo.getInstructions();

			Map<InstructionType, String> instructionTypeMap =
					vervotechInstructions.stream().collect(Collectors.toMap(Instruction::getType, Instruction::getMsg));
			for (Instruction dbHotelInfoInstruction : dbHotelInfoInstructions) {
				String vervotechInstructionMsg = instructionTypeMap.get(dbHotelInfoInstruction.getType());
				if (!dbHotelInfoInstruction.getMsg().equals(vervotechInstructionMsg)) {
					return false;
				}
			}
		}

		boolean isAddressInfoValid = validateAddressInfo(vervotechHotelInfo, dbHotelInfo);
		if (!isAddressInfoValid)
			return false;

		Contact vervotechHotelContact = vervotechHotelInfo.getContact();
		Contact dbHotelContact = dbHotelInfo.getContact();

		if ((Objects.nonNull(vervotechHotelContact.getPhone()) && Objects.isNull(dbHotelContact.getPhone()))
				|| (Objects.isNull(vervotechHotelContact.getPhone()) && Objects.nonNull(dbHotelContact.getPhone()))) {
			return false;
		} else if (Objects.nonNull(vervotechHotelContact.getPhone()) && Objects.nonNull(dbHotelContact.getPhone())) {

			if (!vervotechHotelContact.getPhone().equals(dbHotelContact.getPhone()))
				return false;
		}

		if ((Objects.nonNull(vervotechHotelContact.getEmail()) && Objects.isNull(dbHotelContact.getEmail()))
				|| (Objects.isNull(vervotechHotelContact.getEmail()) && Objects.nonNull(dbHotelContact.getEmail()))) {
			return false;
		} else if (Objects.nonNull(vervotechHotelContact.getEmail()) && Objects.nonNull(dbHotelContact.getEmail())) {

			if (!vervotechHotelContact.getEmail().equals(dbHotelContact.getEmail()))
				return false;
		}
		return true;
	}

	private boolean validateAddressInfo(HotelInfo vervotechHotelInfo, HotelInfo dbHotelInfo) {

		Address vervotechAddressInfo = vervotechHotelInfo.getAddress();
		Address hotelAddressInfo = dbHotelInfo.getAddress();

		if ((Objects.nonNull(vervotechAddressInfo.getAddressLine1())
				&& Objects.isNull(hotelAddressInfo.getAddressLine1()))
				|| (Objects.isNull(vervotechAddressInfo.getAddressLine1())
						&& Objects.nonNull(hotelAddressInfo.getAddressLine1()))) {
			return false;
		} else if (Objects.nonNull(vervotechAddressInfo.getAddressLine1())
				&& Objects.nonNull(hotelAddressInfo.getAddressLine1())) {

			if (!vervotechAddressInfo.getAddressLine1().equals(hotelAddressInfo.getAddressLine1()))
				return false;
		}

		if ((Objects.nonNull(vervotechAddressInfo.getAddressLine2())
				&& Objects.isNull(hotelAddressInfo.getAddressLine2()))
				|| (Objects.isNull(vervotechAddressInfo.getAddressLine2())
						&& Objects.nonNull(hotelAddressInfo.getAddressLine2()))) {
			return false;
		} else if (Objects.nonNull(vervotechAddressInfo.getAddressLine2())
				&& Objects.nonNull(hotelAddressInfo.getAddressLine2())) {

			if (!vervotechAddressInfo.getAddressLine2().equals(hotelAddressInfo.getAddressLine2()))
				return false;
		}

		if ((Objects.nonNull(vervotechAddressInfo.getCity().getName())
				&& Objects.isNull(hotelAddressInfo.getCity().getName()))
				|| (Objects.isNull(vervotechAddressInfo.getCity().getName())
						&& Objects.nonNull(hotelAddressInfo.getCity().getName()))) {
			return false;
		} else if (Objects.nonNull(vervotechAddressInfo.getCity().getName())
				&& Objects.nonNull(hotelAddressInfo.getCity().getName())) {

			if (!vervotechAddressInfo.getCity().getName().equals(hotelAddressInfo.getCity().getName()))
				return false;
		}

		if ((Objects.nonNull(vervotechAddressInfo.getState().getName())
				&& Objects.isNull(hotelAddressInfo.getState().getName()))
				|| (Objects.isNull(vervotechAddressInfo.getState().getName())
						&& Objects.nonNull(hotelAddressInfo.getState().getName()))) {
			return false;
		} else if (Objects.nonNull(vervotechAddressInfo.getState().getName())
				&& Objects.nonNull(hotelAddressInfo.getState().getName())) {

			if (!vervotechAddressInfo.getState().getName().equals(hotelAddressInfo.getState().getName()))
				return false;
		}

		if ((Objects.nonNull(vervotechAddressInfo.getCountry().getName())
				&& Objects.isNull(hotelAddressInfo.getCountry().getName()))
				|| (Objects.isNull(vervotechAddressInfo.getCountry().getName())
						&& Objects.nonNull(hotelAddressInfo.getCountry().getName()))) {
			return false;
		} else if (Objects.nonNull(vervotechAddressInfo.getCountry().getName())
				&& Objects.nonNull(hotelAddressInfo.getCountry().getName())) {

			if (!vervotechAddressInfo.getCountry().getName().equals(hotelAddressInfo.getCountry().getName()))
				return false;
		}

		if ((Objects.nonNull(vervotechHotelInfo.getFacilities()) && Objects.isNull(dbHotelInfo.getFacilities()))
				|| (Objects.isNull(vervotechHotelInfo.getFacilities())
						&& Objects.nonNull(dbHotelInfo.getFacilities()))) {
			return false;
		} else if (Objects.nonNull(vervotechHotelInfo.getFacilities()) && Objects.nonNull(dbHotelInfo.getFacilities())
				&& !BaseHotelUtils.equalLists(vervotechHotelInfo.getFacilities(), dbHotelInfo.getFacilities())) {
			return false;
		}
		return true;
	}

	@Override
	public Map<String, HotelInfo> processAndFetchHotelInfo(String supplierName, Set<String> supplierHotelIds,
			HotelSearchQuery searchQuery, OperationType operationType) {

		log.info("Processing static info for search id {} and supplier {} are {}", searchQuery.getSearchId(),
				searchQuery.getMiscInfo().getSupplierId(), supplierHotelIds.size());
		Map<String, HotelInfo> hotelInfoList = new HashMap<>();
		if (operationType.equals(OperationType.LIST_SEARCH)) {
			Set<String> supplierHotelIdsAsKey = getStaticDataKeys(supplierName, supplierHotelIds);
			Map<String, Map<String, HotelInfo>> hotelIdWithHotelInfoMap = fetchHotelIdToHotelStaticData(
					supplierHotelIdsAsKey, searchQuery, new String[] {BinName.HOTEL_INFO.name()});
			log.info(
					"Processing static info after fetching hotel id to hotel static data for search id {} and supplier {} are {}",
					searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(),
					hotelIdWithHotelInfoMap.size());

			for (String supplierHotelIdAsKey : supplierHotelIdsAsKey) {
				try {
					Map<String, HotelInfo> hotelInfo = hotelIdWithHotelInfoMap.get(supplierHotelIdAsKey);
					if (!ObjectUtils.isEmpty(hotelInfo)) {
						hotelInfoList.put(supplierHotelIdAsKey, hotelInfo.get(BinName.HOTEL_INFO.name()));
					}
				} catch (Exception e) {
					log.debug("Unable to find mapping for supplierhotelId {}", supplierHotelIdAsKey, e);
				}
			}
			log.info(
					"Processed static info after fetching hotel id to hotel static data for search id {} and supplier {} are {}",
					searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(),
					hotelIdWithHotelInfoMap.size());
		} else {
			Map<String, Map<String, HotelInfo>> hotelIdWithHotelInfoMap =
					fetchHotelIdToHotelStaticData(supplierHotelIds, searchQuery,
							new String[] {BinName.HOTEL_INFO.name(), BinName.HOTEL_DETAIL.name()});
			for (String supplierHotelIdAsKey : supplierHotelIds) {
				try {
					Map<String, HotelInfo> hotelInfo = hotelIdWithHotelInfoMap.get(supplierHotelIdAsKey);
					if (!ObjectUtils.isEmpty(hotelInfo)) {
						HotelInfo basicInfo = hotelInfo.get(BinName.HOTEL_INFO.name());
						if (Objects.nonNull(hotelInfo.get(BinName.HOTEL_DETAIL.name()))) {
							HotelInfo detailInfo = hotelInfo.get(BinName.HOTEL_DETAIL.name());
							hotelInfoList.put(supplierHotelIdAsKey,
									new GsonMapper<>(detailInfo, basicInfo, HotelInfo.class).convert());
						} else {
							hotelInfoList.put(supplierHotelIdAsKey, basicInfo);
						}
					}
				} catch (Exception e) {
					log.debug("Unable to find mapping for hotelId {} and supplierId {}", supplierHotelIdAsKey);
				}
			}
		}
		log.info("Processed static info for search id {} and supplier {} are {}", searchQuery.getSearchId(),
				searchQuery.getMiscInfo().getSupplierId(), supplierHotelIds.size());
		return hotelInfoList;
	}

	@Override
	public Map<String, String> getRoomMappingInfo(HotelSearchQuery searchQuery, HotelInfo hotelInfo) {
		AbstractStaticDataInfoFactory factory =
				HotelSourceType.getStaticDataFactoryInstance(HotelSourceType.VERVOTECH.name(), null);
		return factory.getRoomMappingInfo(searchQuery, hotelInfo);
	}

	private Map<String, Map<String, HotelInfo>> fetchHotelIdToHotelStaticData(Set<String> supplierHotelIdsAsKey,
			HotelSearchQuery searchQuery, String[] bins) {

		log.info("Fetching hotel id to hotel static data for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(), supplierHotelIdsAsKey.size());

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		CacheMetaInfo hotelMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_STATIC_INFO.getName()).bins(bins).kyroCompress(true)
				.keys(supplierHotelIdsAsKey.toArray(new String[0])).build();
		Map<String, Map<String, HotelInfo>> hotelIdWithHotelInfoMap =
				cachingCommunicator.get(hotelMappingMetaInfo, HotelInfo.class);

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(supplierHotelIdsAsKey.size()).supplierId(searchQuery.getMiscInfo().getSupplierId())
						.build(),
				LogTypes.HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_START);
		log.info("Fetched hotel id to hotel static data for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(), supplierHotelIdsAsKey.size());
		return hotelIdWithHotelInfoMap;
	}


	public Set<String> getStaticDataKeys(String supplierName, Set<String> supplierHotelIds) {

		Set<String> staticDataKeys = new HashSet<>();
		for (String supplierHotelId : supplierHotelIds) {
			staticDataKeys.add(String.join(getSeparator(), supplierName, supplierHotelId));
		}
		return staticDataKeys;
	}

	@Override
	@SuppressWarnings("serial")
	public List<HotelWiseSearchInfo> getHotelWiseSearchInfo(String hotelId) {

		Map<String, String> keyValue = new HashMap<>();
		keyValue.put("unicaid", hotelId);
		ESSearchRequest request = ESSearchRequest.builder().source(GsonUtils.getGson().toJson(keyValue))
				.metaInfo(ESMetaInfo.HOTEL_MAPPING.getType()).build();
		ESAutoSuggestionResponse response = esCommunicator.getSuggestions(request);
		String jsonString = GsonUtils.getGson().toJson(response.getSuggestions());
		log.info("Fetch hotel info from es {}", jsonString);
		List<HotelWiseSearchInfo> hotelWiseSearchInfo =
				GsonUtils.getGson().fromJson(jsonString, new TypeToken<ArrayList<HotelWiseSearchInfo>>() {}.getType());
		return hotelWiseSearchInfo;
	}

	@Override
	public SupplierHotelIdMapping getHotelIdMappingFromDB(String hotelid) {
		DbSupplierHotelIdMapping dbHotelIdMapping = infoService.findByHotelId(hotelid);
		return Objects.isNull(dbHotelIdMapping) ? null : dbHotelIdMapping.toDomain();
	}

}
