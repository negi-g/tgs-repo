package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelCountryInfoMapping;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.mapping.HotelCountryInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DotwStaticDataFactory extends AbstractStaticDataInfoFactory {

	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	private HotelRegionInfoMappingManager regionInfoMappingManager;
	
	@Autowired
	private HotelCountryInfoMappingManager supplierCountryInfoManager;

	public DotwStaticDataFactory(HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getCityMappingData() throws IOException {
		DotwCitySaveService citySaveService =
				DotwCitySaveService.builder().supplierConf(this.getSupplierConf()).build();
		List<HotelRegionInfoQuery> regionInfoQueries = citySaveService.getRegionMapppings();
		log.info("dotw region size : " + regionInfoQueries.size());
		HotelBaseSupplierUtils.saveOrUpdateRegionInfo(regionInfoQueries, staticDataRequest.getIsMasterData(), false);
	}

	@Override
	protected void getHotelStaticData() throws IOException, JAXBException {

		DotwHotelSaveService hotelSaveService = DotwHotelSaveService.builder().supplierConf(this.getSupplierConf())
				.regionInfoMappingManager(regionInfoMappingManager).hotelInfoSaveManager(hotelInfoSaveManager).build();
		hotelSaveService.process();

	}

	@Override
	protected Map<String, String> getRoomMappingData(HotelSearchQuery searchQuery, HotelInfo hotelInfo) {

		return null;
	}

	@Override
	protected void getNationalityMappingData() throws IOException {
		DotwCountrySaveService countrySaveService =
				DotwCountrySaveService.builder().supplierConf(this.getSupplierConf()).build();
		List<HotelCountryInfoMapping> countryData = countrySaveService.getCountryMapppings();
		log.info("dotw country size : {}" + countryData.size());
		supplierCountryInfoManager.saveHotelNationalityMappingInfoList(countryData);
	}
}
