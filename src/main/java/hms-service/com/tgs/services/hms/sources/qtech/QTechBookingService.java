
package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.Gson;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.HotelBookingRequest;
import com.tgs.services.hms.datamodel.qtech.HotelBookingResponse;
import com.tgs.services.hms.datamodel.qtech.RoomDetails;
import com.tgs.services.hms.datamodel.qtech.Travellers;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
public class QTechBookingService {

	private HotelInfo hotel;
	private Order order;
	protected boolean isPanRequired;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelBookingResponse response;
	protected RestAPIListener listener;

	public boolean book() throws IOException {
		HttpUtils httpUtils = null;
		HotelBookingRequest request = null;
		try {
			listener = new RestAPIListener("");
			request = createBookingRequest();
			httpUtils = QTechUtil.getResponseURL(request, supplierConf, sourceConfigOutput);
			response = httpUtils.getResponse(HotelBookingResponse.class).orElse(null);
			log.info("Qtech booking response for bookingid {} is {}", order.getBookingId(), response);
			boolean bookingStatus = updateBookingStatus();
			return bookingStatus;
		} finally {

			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.BOOK.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(response)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder()
						.supplierName(supplierConf.getHotelSupplierCredentials().getUserName().toUpperCase())
						.requestType(BaseHotelConstants.BOOKING).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(order.getBookingId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	public boolean updateBookingStatus() throws IOException {

		boolean isBooked = false;
		if (Objects.nonNull(response) && Objects.nonNull(response.getBookingDetail())
				&& StringUtils.isNotBlank(response.getBookingDetail().getCurrentStatus())
				&& StringUtils.isNotBlank(response.getMessage())) {

			if (response.getMessage().equalsIgnoreCase("Success")
					&& response.getBookingDetail().getCurrentStatus().equalsIgnoreCase("vouchered")) {
				updateSupplierBookingReferenceId();
				isBooked = true;
			} else {
				SystemContextHolder.getContextData().getErrorMessages()
						.add("Booking is failed due to " + response.getMessageInfo());
				log.error("Hotel Booking failed from supplier {} for bookingId {} due to {}",
						this.supplierConf.getBasicInfo().getSupplierName(), order.getBookingId(),
						response.getMessageInfo());
			}
		}
		return isBooked;
	}

	public void updateSupplierBookingReferenceId() {

		hotel.getMiscInfo().setSupplierBookingId(response.getBookingDetail().getId());
		hotel.getMiscInfo().setSupplierBookingReference(response.getBookingDetail().getBookingReference());

	}

	private HotelBookingRequest createBookingRequest() {
		String totalBookingAmount =
				hotel.getOptions().get(0).getMiscInfo().getExpectedOptionBookingPriceInSupplierCurrency();
		HotelBookingRequest request = new HotelBookingRequest();
		request.setAction(QTechConstants.ACTION_B.getValue());
		request.setUnique_id(hotel.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		request.setHotel_id(hotel.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		request.setAgent_ref_no(order.getBookingId());
		request.setSection_unique_id(hotel.getOptions().get(0).getMiscInfo().getHotelOptionId());
		request.setGzip("no");
		request.setExpected_price(totalBookingAmount);
		request.setRoomDetails(String.valueOf(new Gson().toJson(createRoomDetails())));
		if (isPanRequired) {
			request.setLead_pax_registration_number(HotelUtils.getLeadPanNumber(hotel.getOptions().get(0)));
		}
		return request;

	}

	private List<RoomDetails> createRoomDetails() {

		List<RoomDetails> roomDetailList = new ArrayList<>();
		for (RoomInfo roomInfo : hotel.getOptions().get(0).getRoomInfos()) {
			List<Travellers> travellerList = new ArrayList<>();
			for (TravellerInfo travellerInfo : roomInfo.getTravellerInfo()) {
				Travellers traveller = Travellers.builder().salutation(travellerInfo.getTitle())
						.first_name(travellerInfo.getFirstName()).last_name(travellerInfo.getLastName())
						.age(travellerInfo.getAge()).build();
				if (travellerInfo.getPaxType().equals(PaxType.CHILD)) {
					traveller.setSalutation("Child");
				}
				travellerList.add(traveller);
			}
			String roomClassId = roomInfo.getMiscInfo().getRoomBookingId();
			RoomDetails roomDetail = RoomDetails.builder().numberOfChilds(roomInfo.getNumberOfChild().toString())
					.passangers(travellerList).roomClassId(roomClassId).build();
			roomDetailList.add(roomDetail);
		}
		return roomDetailList;
	}
}
