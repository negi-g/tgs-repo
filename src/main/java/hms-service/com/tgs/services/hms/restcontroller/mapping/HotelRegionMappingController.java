package com.tgs.services.hms.restcontroller.mapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.restmodel.HotelCityMappingRequest;
import com.tgs.services.hms.restmodel.HotelDeleteMappingRequest;
import com.tgs.services.hms.restmodel.HotelRegionInfoRequest;

@RestController
@RequestMapping("/hms/v1/region/mapping")
public class HotelRegionMappingController {

	@Autowired
	HotelRegionInfoMappingManager mappingManager;


	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected BaseResponse saveSupplierCityMapping(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelRegionInfoRequest regionInfoRequest) throws Exception {
		mappingManager.process(regionInfoRequest);
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	protected BaseResponse uploadSupplierCityMapping(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelCityMappingRequest cityInfoRequest) throws Exception {
		mappingManager.saveHotelRegionMappingRequest(cityInfoRequest);
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/fetch-mapping/{id}", method = RequestMethod.POST)
	protected BaseResponse uploadSupplierCityMapping(HttpServletRequest request,
			HttpServletResponse response,
			@PathVariable("id") String regionId) throws Exception {
		return mappingManager.getHotelRegionMappingForRegionId(regionId);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	protected BaseResponse deleteSupplierCityMapping(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelDeleteMappingRequest cityInfoRequest) throws Exception {
		mappingManager.deleteHotelRegionMapping(cityInfoRequest);
		return new BaseResponse();
	}

}
