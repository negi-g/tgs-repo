package com.tgs.services.hms.jparepository.inventory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.dbmodel.inventory.DbRoomCategory;
import com.tgs.services.hms.dbmodel.inventory.DbRoomTypeInfo;

@Service
public class HotelRoomInfoService {
	
	@Autowired
	DbRoomCategoryRepository repository;
	
	@Autowired
	DbRoomTypeRepository roomTypeRepo;
	
	public void storeRoomCategory(DbRoomCategory roomCategory) {
		repository.save(roomCategory);
	}
	
	public void storeRoomTypeInfo(DbRoomTypeInfo roomType) {
		roomTypeRepo.save(roomType);
	}
	
	public List<DbRoomCategory> findAll(){
		return repository.findAll();
	}
	
	public List<DbRoomTypeInfo> findAllRoomTypeInfo(){
		return roomTypeRepo.findAll();
	}
	
	public DbRoomCategory findById(String id) {
		return repository.findById(Long.valueOf(id));
	}
	
	public DbRoomTypeInfo findRoomTypeInfoById(String id) {
		return roomTypeRepo.findById(Long.valueOf(id));
	}
	
	public List<DbRoomCategory> findAllByIds(List<Long> ids){
		return repository.findAll();
	}
	
	public List<DbRoomTypeInfo> findAllRoomTypeInfoByIds(List<Long> ids){
		return roomTypeRepo.findAll();
	}

}
