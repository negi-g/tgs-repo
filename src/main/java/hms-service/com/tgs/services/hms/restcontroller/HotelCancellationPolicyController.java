package com.tgs.services.hms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.hms.restmodel.HotelCancellationPolicyRequest;
import com.tgs.services.hms.restmodel.HotelCancellationPolicyResponse;
import com.tgs.services.hms.servicehandler.HotelCancellationPolicyHandler;

@RestController
@RequestMapping("/hms/v1")
public class HotelCancellationPolicyController {
	
	@Autowired
	HotelCancellationPolicyHandler cancellationPolicyHandler;
	
	
	@RequestMapping(value = "/hotel-cancellation-policy", method = RequestMethod.POST)
	protected HotelCancellationPolicyResponse getCancellationPolicy(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelCancellationPolicyRequest cancellationPolicyRequest) throws Exception {
		
		cancellationPolicyHandler.initData(cancellationPolicyRequest, new HotelCancellationPolicyResponse());
		return cancellationPolicyHandler.getResponse();
		
	}

}
