package com.tgs.services.hms.mapper;

import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.hms.analytics.AnalyticsHotelQuery;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierBasicInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Builder
@Slf4j
public class HotelSearchToAnalyticsHotelSearchMapper extends Mapper<AnalyticsHotelQuery> {

	private HotelSearchQuery searchQuery;
	private HotelSupplierBasicInfo basicInfo;
	private Integer searchResultCount;
	private User user;
	private ContextData contextData;

	@Override
	protected void execute() throws CustomGeneralException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		output = output != null ? output : AnalyticsHotelQuery.builder().build();
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		if (output.getExternalapiparsingtimems() > 5000 || output.getExternalapitimems() > 40000
				|| output.getResponsetimems() < 0) {
			// Temporary log, will remove once the system is stable.
			log.info("API time needs to cross check for search id {} and checkpoint info {}", searchQuery.getSearchId(),
					GsonUtils.getGson().toJson(contextData.getCheckPointInfo()));
		}
		output.setAnalyticstype(HotelFlowType.SEARCH.name());
		if (searchQuery.getCheckinDate() != null && searchQuery.getCheckoutDate() != null) {
			output.setCheckindate(searchQuery.getCheckinDate().format(formatter));
			output.setCheckoutdate(searchQuery.getCheckoutDate().format(formatter));
			output.setNoofroomnights(
					(int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate()));
		}

		if (!ObjectUtils.isEmpty(searchQuery.getSearchCriteria())) {
			output.setCity(searchQuery.getSearchCriteria().getCityName());
			output.setCountry(searchQuery.getSearchCriteria().getCountryName());
			output.setLocality(searchQuery.getSearchCriteria().getLocality());
			output.setNationality(searchQuery.getSearchCriteria().getNationality());
		}
		if (!ObjectUtils.isEmpty(searchQuery.getSearchPreferences())) {
			output.setRatings(searchQuery.getSearchPreferences().getRatings());
		}
		output.setSearchid(searchQuery.getSearchId());
		output.setSourcename(getSourceName());
		output.setSuppliername(getSupplierName());
		if (CollectionUtils.isNotEmpty(searchQuery.getRoomInfo())) {
			output.setNoofroom(searchQuery.getRoomInfo().size());
		}
		output.setSearchresultcount(searchResultCount);
		if (CollectionUtils.isNotEmpty(contextData.getErrorMessages())) {
			output.setErrormsg(contextData.getErrorMessages().toString());
		}
		output.setResponsetimems(contextData.calculateCheckPointTimeDiff(SystemCheckPoint.REQUEST_STARTED.name(),
				SystemCheckPoint.REQUEST_FINISHED.name())
				- contextData.calculateCheckPointTimeDiff(SystemCheckPoint.EXTERNAL_API_STARTED.name(),
						SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name()));

		evaluateAdultChildCombination();
	}

	private String getSourceName() {
		if (basicInfo != null) {
			return HotelSourceType.getHotelSourceType(basicInfo.getSourceId()).name();
		}
		return null;
	}

	private String getSupplierName() {
		if (basicInfo != null) {
			return basicInfo.getSupplierName();
		}
		return null;
	}

	private void evaluateAdultChildCombination() {
		StringBuilder adultAndChildCombination = new StringBuilder();
		if (!ObjectUtils.isEmpty(searchQuery.getRoomInfo())) {
			List<RoomSearchInfo> roomInfos = searchQuery.getRoomInfo();
			for (RoomSearchInfo roomInfo : roomInfos) {
				adultAndChildCombination.append(roomInfo.getNumberOfAdults());
				if (!ObjectUtils.isEmpty(roomInfo.getNumberOfChild())) {
					adultAndChildCombination.append("-" + roomInfo.getNumberOfChild());
				}
				if (CollectionUtils.isNotEmpty(roomInfo.getChildAge())) {
					adultAndChildCombination.append("-" + roomInfo.getChildAge());
				}
				adultAndChildCombination.append(";");
			}
			output.setAdultchildcombination(adultAndChildCombination.toString());
		}
	}

}
