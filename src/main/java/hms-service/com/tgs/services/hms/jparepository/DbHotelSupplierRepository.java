package com.tgs.services.hms.jparepository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;

@Repository
public interface DbHotelSupplierRepository extends JpaRepository<DbHotelSupplierMapping, Long> {

	//@Query(value = "select v.hotelId as id, v.supplierHotelId as supplierHotelId , v.supplierName as supplierName from HotelInfoMapping v group by v.hotelId")
	//public List<?> findHotelMapping();
	
}
