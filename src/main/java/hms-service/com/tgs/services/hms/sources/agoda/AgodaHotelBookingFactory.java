package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class AgodaHotelBookingFactory extends AbstractHotelBookingFactory {

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelOrderItemCommunicator itemComm;

	@Autowired
	CommercialCommunicator cmsComm;

	public AgodaHotelBookingFactory(HotelSupplierConfiguration supplierConf, HotelInfo hotel, Order order) {
		super(supplierConf, hotel, order);
	}

	@Override
	public boolean bookHotel() throws IOException, InterruptedException, JAXBException {

		HotelInfo hInfo = this.getHotel();
		this.setBookNowPayLater(hInfo);
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(hInfo.getMiscInfo().getSearchId());
		AgodaBookingService bookingService = AgodaBookingService.builder().hInfo(this.getHotel()).order(order)
				.supplierConf(supplierConf).sourceConfigOutput(sourceConfigOutput).searchQuery(searchQuery)
				.itemComm(itemComm).gmsCommunicator(gmsCommunicator).cmsComm(cmsComm).build();
		return bookingService.book();
	}
	
	public void setBookNowPayLater(HotelInfo hInfo) {
		if(BooleanUtils.isNotTrue(sourceConfigOutput.getAllowBookNowPayLater())) {
			return;
		}
		
		List<RoomInfo> roomInfos = hInfo.getOptions().get(0).getRoomInfos();
		LocalDate firstDate = roomInfos.get(0).getBookNowPayLaterDate();
		boolean isBnplBooking = roomInfos.stream().allMatch(x -> (x.getBookNowPayLaterDate() != null && x.getBookNowPayLaterDate().equals(firstDate)));
		if(isBnplBooking && firstDate!= null) {
			hInfo.getMiscInfo().setIsBnplBooking(isBnplBooking);
		}
	}

	@Override
	public boolean confirmHotel() throws IOException {
		return true;
	}

	@Override
	public void updateBookingStatus() {
		AgodaBookingService bookingService = AgodaBookingService.builder().hInfo(this.getHotel()).order(order)
				.supplierConf(supplierConf).sourceConfigOutput(sourceConfigOutput).itemComm(itemComm).build();
		bookingService.updateOrderStatus();
		
	}

}
