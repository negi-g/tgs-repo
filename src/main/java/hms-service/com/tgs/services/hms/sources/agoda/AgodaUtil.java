package com.tgs.services.hms.sources.agoda;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.utils.common.HttpUtils;

public class AgodaUtil {


	public static HttpUtils getHttpUtils(String postData, HotelUrlConstants url, AgodaStaticDataRequest staticRequest, HotelSupplierConfiguration supplierConf) {
	
		Map<String, String> queryParams = null;
		if(staticRequest != null) {
			staticRequest.setToken(supplierConf.getHotelSupplierCredentials().getToken());
			staticRequest.setSite_id(supplierConf.getHotelSupplierCredentials().getClientId());
			queryParams = convertToMap(staticRequest);
		}
		
		HttpUtils httpUtils = HttpUtils.builder().urlString(supplierConf.getHotelAPIUrl(url)).postData(postData)
				.headerParams(getHeaderParams(supplierConf)).queryParams(queryParams).requestMethod(HttpUtils.REQ_METHOD_POST)
						.timeout(200 * 1000).build();
		return httpUtils;
	}


	private static Map<String, String> getHeaderParams(HotelSupplierConfiguration supplierConf) {

		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("content-type", "text/xml");
		headerParams.put("Accept-Encoding", "gzip");
		headerParams.put("Connection", "close");
		String authCode = supplierConf.getHotelSupplierCredentials().getClientId() 
				+ ":" + supplierConf.getHotelSupplierCredentials().getApiKey();
		headerParams.put("Authorization", authCode);
		return headerParams;

	}
	
	private static <T> Map<String, String> convertToMap(T obj) {
		return new Gson().fromJson(
				GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(obj),
				new TypeToken<HashMap<String, String>>() {}.getType());
	}
	
	
}
