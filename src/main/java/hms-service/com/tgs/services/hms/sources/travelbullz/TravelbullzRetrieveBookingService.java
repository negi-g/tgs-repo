package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.travelbullz.AdvancedOptions;
import com.tgs.services.hms.datamodel.travelbullz.BookingDetailRQ;
import com.tgs.services.hms.datamodel.travelbullz.BookingDetailRS;
import com.tgs.services.hms.datamodel.travelbullz.BookingDetailResponse;
import com.tgs.services.hms.datamodel.travelbullz.HotelBookOption;
import com.tgs.services.hms.datamodel.travelbullz.HotelRoom;
import com.tgs.services.hms.datamodel.travelbullz.Request;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Getter
@Setter
@SuperBuilder
public class TravelbullzRetrieveBookingService extends TravelbullzBaseService {
	private HotelImportBookingParams importBookingParams;
	private HotelImportedBookingInfo bookingInfo;
	BookingDetailResponse bookingDetailResponse;

	public void retrieveBooking() throws IOException {
		listener = new RestAPIListener("");
		HttpUtilsV2 httpUtils = null;
		try {
			TravelbullzRequest bookingDetailRequest = getSearchRequest();
			String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
			String urlString =
					StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.CONFIRM_BOOKING_URL));
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(bookingDetailRequest), urlString);
			bookingDetailResponse = httpUtils.getResponse(BookingDetailResponse.class).orElse(null);

			if (Objects.nonNull(bookingDetailResponse) && CollectionUtils.isEmpty(bookingDetailResponse.getError())) {
				bookingInfo = createBookingDetailResponse(bookingDetailResponse.getBookingDetailRS());
			} else if (Objects.nonNull(bookingDetailResponse.getError().get(0))) {
				log.info("Unable to retirve booking for bookingid {} of Supplier {} due to error {} ",
						importBookingParams.getBookingId(), supplierConf.getBasicInfo().getSupplierId(),
						bookingDetailResponse.getError().get(0).getDescription());
			}

		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(bookingDetailResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRAVELBULLZ.name())
					.requestType(BaseHotelConstants.RETRIEVE_BOOKING).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.prefix(importBookingParams.getFlowType().getName()).postData(httpUtils.getPostData())
					.logKey(importBookingParams.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
	}

	private HotelImportedBookingInfo createBookingDetailResponse(BookingDetailRS response) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-uuuu");
		List<HotelRoom> roomlist = response.getHotelOption().getHotelRooms();
		HotelInfo hInfo = getHotelDetails(response);
		searchQuery = getHotelSearchQuery(response);
		String orderStatus =
				TravelbullzOrderMapping.valueOf(roomlist.get(0).getBookingStatus().toUpperCase()).getCode();
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		HotelImportedBookingInfo hBookingInfo = HotelImportedBookingInfo.builder().hInfo(hInfo).searchQuery(searchQuery)
				.bookingDate(LocalDate.parse(response.getHotelOption().getBookingDate(), formatter))
				.deliveryInfo(deliveryInfo).bookingCurrencyCode(TravelbullzConstant.CURRENCY.value)
				.orderStatus(orderStatus).build();
		HotelUtils.setOptionCancellationPolicyFromRooms(hInfo.getOptions());
		HotelUtils.setBufferTimeinCnp(hInfo.getOptions(), searchQuery);
		return hBookingInfo;
	}


	private HotelSearchQuery getHotelSearchQuery(BookingDetailRS response) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-uuuu");
		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
		searchQueryBuilder.checkinDate(LocalDate.parse(response.getHotelOption().getCheckInDate(), formatter));
		searchQueryBuilder.checkoutDate(LocalDate.parse(response.getHotelOption().getCheckOutDate(), formatter));
		searchQueryBuilder.sourceId(HotelSourceType.TRAVELBULLZ.getSourceId());
		searchQueryBuilder
				.miscInfo(HotelSearchQueryMiscInfo.builder().supplierId(HotelSourceType.TRAVELBULLZ.name()).build());
		return searchQueryBuilder.build();
	}

	private HotelInfo getHotelDetails(BookingDetailRS response) {
		List<Option> optionList = getHotelOption(response.getHotelOption());
		String hotelId = response.getHotelOption().getHotelId();
		HotelMiscInfo hMiscInfo = HotelMiscInfo.builder().supplierStaticHotelId(hotelId)
				.supplierBookingReference(response.getHotelOption().getReferenceNo()).build();
		HotelInfo hInfo = HotelInfo.builder().options(optionList).miscInfo(hMiscInfo)
				.name(response.getHotelOption().getHotelName()).build();
		return hInfo;
	}

	private List<Option> getHotelOption(HotelBookOption hotelOption) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-uuuu");
		List<RoomInfo> roomInfos = new ArrayList<>();
		List<Option> optionList = new ArrayList<>();
		hotelOption.getHotelRooms().forEach(hotelResponseRoom -> {
			RoomInfo room = new RoomInfo();
			room.setId(getRoomId(hotelResponseRoom));
			room.setRoomCategory(hotelResponseRoom.getRoomTypeName());
			room.setCheckInDate(LocalDate.parse(hotelOption.getCheckInDate(), formatter));
			room.setCheckOutDate(LocalDate.parse(hotelOption.getCheckOutDate(), formatter));
			room.setRoomType(hotelResponseRoom.getRoomTypeName().split("-")[0]);
			setMealBasis(room, hotelResponseRoom.getMealName());
			room.setPerNightPriceInfos(
					setPerNightPrice(hotelResponseRoom.getPrice(), room.getCheckInDate(), room.getCheckOutDate()));
			List<TravellerInfo> travellerInfoList = getTravellerList(hotelResponseRoom);
			room.setTravellerInfo(travellerInfoList);
			room.setMiscInfo(RoomMiscInfo.builder().status(hotelResponseRoom.getBookingStatus())
					.roomIndex(Integer.parseInt(hotelResponseRoom.getRoomNo())).inclusive(hotelResponseRoom.getPrice())
					.build());
			setCancellationPolicy(room, hotelResponseRoom.getCancellationPolicy());
			setNoOfAdultsAndChild(room, room.getTravellerInfo());
			roomInfos.add(room);

		});
		updatePriceWithMarkup(roomInfos);
		Option option = Option.builder().totalPrice(hotelOption.getTotalPrice())
				.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
						.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
						.supplierHotelId(hotelOption.getHotelId()).sourceId(supplierConf.getBasicInfo().getSourceId())
						.build())
				.roomInfos(roomInfos).id(RandomStringUtils.random(20, true, true)).build();

		optionList.add(option);
		return optionList;
	}

	private void setNoOfAdultsAndChild(RoomInfo room, List<TravellerInfo> travellerInfo) {
		Integer adult = 0;
		Integer child = 0;
		for (TravellerInfo traveller : travellerInfo) {
			if (traveller.getTitle().equals("Master"))
				child++;
			adult++;
		}
		room.setNumberOfAdults(adult);
		room.setNumberOfChild(child);

	}

	protected List<PriceInfo> setPerNightPrice(Double price, LocalDate localDate, LocalDate localDate2) {
		List<PriceInfo> priceInfoList = new ArrayList<>();
		int numberOfNights = (int) ChronoUnit.DAYS.between(localDate, localDate2);
		double perNightBasePrice = getAmountBasedOnCurrency(price / numberOfNights, TravelbullzConstant.CURRENCY.value);
		for (int j = 1; j <= numberOfNights; j++) {
			PriceInfo priceInfo = new PriceInfo();
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, perNightBasePrice);
			priceInfo.setDay(j);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
		}
		return priceInfoList;
	}


	private TravelbullzRequest getSearchRequest() {
		BookingDetailRQ bookingDetailRQ =
				BookingDetailRQ.builder().ReferenceNo(importBookingParams.getSupplierBookingId())
						.InternalReference(importBookingParams.getBookingId()).build();
		Request request = Request.builder().BookingDetailRQ(bookingDetailRQ).build();
		AdvancedOptions advancedOptions =
				AdvancedOptions.builder().Currency(TravelbullzConstant.CURRENCY.value).build();
		TravelbullzRequest searchRequest =
				TravelbullzRequest.builder().Token(supplierConf.getHotelSupplierCredentials().getPassword())
						.Request(request).AdvancedOptions(advancedOptions).build();

		return searchRequest;
	}
}
