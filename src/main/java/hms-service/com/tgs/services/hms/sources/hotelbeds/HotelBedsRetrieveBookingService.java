package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.hotelBeds.Booking;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsBookResponse;
import com.tgs.services.hms.datamodel.hotelBeds.RoomResponse;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
@Getter
@Setter
@SuperBuilder
public class HotelBedsRetrieveBookingService extends HotelBedsBaseService {

	private HotelImportBookingParams importBookingParams;
	private HotelImportedBookingInfo bookingInfo;
	HotelBedsBookResponse bookingDetailResponse;

	public void retrieveBooking() throws IOException {
		HttpUtilsV2 httpUtils = null;
		try {
			listener = new RestAPIListener("");
			String supplierBookingReference = importBookingParams.getSupplierBookingId();
			httpUtils = HotelBedsUtils.getPostBookingHttpUtils(supplierBookingReference, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.BOOK_URL), HttpUtils.REQ_METHOD_GET);
			bookingDetailResponse = httpUtils.getResponse(HotelBedsBookResponse.class).orElse(null);
			log.info("Response for booking for supplier booking reference {} is {}", supplierBookingReference,
					GsonUtils.getGson().toJson(bookingDetailResponse));

			bookingInfo = createBookingDetailResponse();
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(bookingDetailResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.HOTELBEDS.name())
					.requestType(BaseHotelConstants.RETRIEVE_BOOKING).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.prefix(importBookingParams.getFlowType().getName()).postData(httpUtils.getPostData())
					.logKey(importBookingParams.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
	}

	private HotelImportedBookingInfo createBookingDetailResponse() {

		try {
			if (bookingDetailResponse == null || bookingDetailResponse.getBooking() == null)
				throw new CustomGeneralException("Error While Trying To Fetch Booking Details");
			Booking response = bookingDetailResponse.getBooking();
			HotelInfo hInfo = getHotelDetails(response);
			searchQuery = getHotelSearchQuery(response);
			List<RoomInfo> roomInfos = new ArrayList<>();
			currency = response.getCurrency();
			for (RoomResponse roomResponse : response.getHotel().getRooms()) {
				RoomInfo room = new RoomInfo();
				room.setId(roomResponse.getCode());
				room.setRoomCategory(roomResponse.getName());
				room.setCheckInDate(response.getHotel().getCheckIn());
				room.setCheckOutDate(response.getHotel().getCheckOut());
				room.setRoomType(roomResponse.getName());
				roomResponse.getRates().forEach(rate -> {
					populateRoomPaxInfoFromRate(room, rate);
					populateRoomPerNightPrice(room, rate);
					room.setCancellationPolicy(getRoomCancellationPolicy(rate, searchQuery, room));
					room.setMiscInfo(RoomMiscInfo.builder().ratePlanCode(rate.getRateKey()).rateType(rate.getRateType())
							.rateChannel(rate.getRateClass()).amenities(Arrays.asList(rate.getBoardName())).build());
					room.getMiscInfo().setRoomTypeCode(room.getId());
				});
				List<TravellerInfo> travellerInfoList = getTravellerList(roomResponse);
				room.setTravellerInfo(travellerInfoList);
				roomInfos.add(room);
			}
			updatePriceWithMarkup(roomInfos);
			double totalPrice = roomInfos.stream().mapToDouble(RoomInfo::getTotalPrice).sum();
			updateRoomIds(roomInfos);
			Option option = Option.builder().totalPrice(totalPrice)
					.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
							.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
							.supplierHotelId(hInfo.getMiscInfo().getSupplierStaticHotelId())
							.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
					.roomInfos(roomInfos).id(RandomStringUtils.random(20, true, true)).build();

			List<Option> options = new ArrayList<>();
			options.add(option);
			searchQuery.setSourceId(HotelSourceType.HOTELBEDS.getSourceId());
			HotelUtils.setOptionCancellationPolicyFromRooms(options);
			HotelUtils.setBufferTimeinCnp(options, searchQuery);

			hInfo.setOptions(options);
			HotelBaseSupplierUtils.populateMealInfo(new ArrayList<>(Arrays.asList(hInfo)), searchQuery);
			String orderStatus = getOrderStatus(response.getHotel().getRooms().get(0).getStatus());
			DeliveryInfo deliveryInfo = new DeliveryInfo();

			return HotelImportedBookingInfo.builder().hInfo(hInfo).searchQuery(searchQuery).orderStatus(orderStatus)
					.deliveryInfo(deliveryInfo).bookingCurrencyCode(currency).bookingDate(response.getCreationDate())
					.build();
		} catch (Exception ex) {
			log.info("Exception in retrieve booking for booking id {}", importBookingParams.getBookingId(), ex);
			throw ex;

		}
	}

	private String getOrderStatus(String status) {

		if (status.equals("CONFIRMED"))
			return OrderStatus.SUCCESS.name();
		if (status.equals("CANCELLED"))
			return OrderStatus.CANCELLED.name();
		if (status.equals("PRECONFIRMED"))
			return OrderStatus.PENDING.name();
		return OrderStatus.PENDING.name();

	}

	private HotelSearchQuery getHotelSearchQuery(Booking response) {

		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
		searchQueryBuilder.checkinDate(response.getHotel().getCheckIn());
		searchQueryBuilder.checkoutDate(response.getHotel().getCheckOut());
		searchQueryBuilder.sourceId(HotelSourceType.HOTELBEDS.getSourceId());
		searchQueryBuilder
				.miscInfo(HotelSearchQueryMiscInfo.builder().supplierId(HotelSourceType.HOTELBEDS.name()).build());
		return searchQueryBuilder.build();
	}

	private HotelInfo getHotelDetails(Booking response) {

		String hotelId = response.getHotel().getCode().toString();
		HotelMiscInfo hMiscInfo = HotelMiscInfo.builder().supplierStaticHotelId(hotelId)
				.supplierBookingReference(response.getReference()).build();
		HotelInfo hInfo = HotelInfo.builder().miscInfo(hMiscInfo).name(response.getHotel().getName()).build();
		return hInfo;
	}
}
