package com.tgs.services.hms.sources.desiya;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class DesiyaHotelBookingFactory extends AbstractHotelBookingFactory {
	@Autowired
	HotelCacheHandler cacheHandler;
	protected SoapRequestResponseListner listener = null;

	public DesiyaHotelBookingFactory(HotelSupplierConfiguration supplierConf, HotelInfo hotel, Order order) {
		super(supplierConf, hotel, order);
	}

	@Override
	public boolean bookHotel() throws IOException, InterruptedException, JAXBException {

		try {
			if (listener == null) {
				listener = new SoapRequestResponseListner(order.getBookingId(), null,
						supplierConf.getBasicInfo().getSupplierName());
			}
			HotelInfo hInfo = this.getHotel();
			HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(hInfo.getMiscInfo().getSearchId());
			DesiyaPreBookingService preBookingService = DesiyaPreBookingService.builder().hInfo(hInfo).order(order)
					.listener(listener).searchQuery(searchQuery).supplierConf(supplierConf)
					.sourceConfigOutput(sourceConfigOutput).build();

			if(preBookingService.book()) {
				DesiyaBookingService bookingService = DesiyaBookingService.builder().hInfo(this.getHotel()).order(order)
						.supplierConf(supplierConf).sourceConfigOutput(sourceConfigOutput).listener(listener).build();
				return bookingService.book();
			}
		} catch (IOException | JAXBException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean confirmHotel() throws IOException {
		return true;
	}


}
