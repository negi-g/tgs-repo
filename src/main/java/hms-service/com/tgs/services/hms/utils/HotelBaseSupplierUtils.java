package com.tgs.services.hms.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.utils.HotelMissingInfoLoggingUtil;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMissingInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HotelBaseSupplierUtils {

	public static Map<String, String> hotelStaticMealInfoMap;

	public static HotelCacheHandler cacheHandler;

	public static MoneyExchangeCommunicator moneyExchnageComm;

	private static HotelRegionInfoSaveManager masterRegionInfoSaveManager;
	private static HotelSupplierRegionInfoManager supplierRegionInfoSaveManager;
	private static HotelRegionInfoMappingManager regionInfoMappingManager;


	public static void init(HotelCacheHandler cacheHandler, MoneyExchangeCommunicator moneyExchnageComm,
			HotelRegionInfoSaveManager masterRegionInfoSaveManager,
			HotelSupplierRegionInfoManager supplierRegionInfoSaveManager,
			HotelRegionInfoMappingManager regionInfoMappingManager) {
		HotelBaseSupplierUtils.cacheHandler = cacheHandler;
		HotelBaseSupplierUtils.moneyExchnageComm = moneyExchnageComm;
		HotelBaseSupplierUtils.masterRegionInfoSaveManager = masterRegionInfoSaveManager;
		HotelBaseSupplierUtils.supplierRegionInfoSaveManager = supplierRegionInfoSaveManager;
		HotelBaseSupplierUtils.regionInfoMappingManager = regionInfoMappingManager;
	}

	private static void mapHotelMealBasis(HotelInfo hInfo, HotelSearchQuery hotelSearchQuery,
			HotelMissingInfo missingInfo) {

		if (MapUtils.isNotEmpty(hotelStaticMealInfoMap)) {
			hInfo.getOptions().forEach(option -> {
				option.getRoomInfos().forEach(roomInfo -> {
					List<String> amenities = new ArrayList<>();
					String mealBasis = "";
					if (!CollectionUtils.isEmpty(roomInfo.getMiscInfo().getAmenities())) {

						for (String amenity : roomInfo.getMiscInfo().getAmenities()) {
							amenities.add(amenity);
							mealBasis = hotelStaticMealInfoMap.getOrDefault(amenity.trim().toUpperCase(), null);
							if (!StringUtils.isBlank(mealBasis)) {
								roomInfo.setMealBasis(mealBasis);
								break;
							}
						}
					}
					if (StringUtils.isBlank(mealBasis)) {
						roomInfo.setMealBasis(BaseHotelConstants.ROOM_ONLY);
						missingInfo.getMealBasis().addAll(amenities);
					}
				});
			});

		} else {
			hInfo.getOptions().forEach(option -> {
				option.getRoomInfos().forEach(roomInfo -> {
					List<String> amenities =
							roomInfo.getMiscInfo().getAmenities() != null ? roomInfo.getMiscInfo().getAmenities()
									: new ArrayList<>();
					roomInfo.setMealBasis(BaseHotelConstants.ROOM_ONLY);
					missingInfo.getMealBasis().addAll(amenities);

				});

			});
		}
	}

	public static void populateMealInfo(List<HotelInfo> hInfoList, HotelSearchQuery searchQuery,
			Boolean isLoggingRequired) {
		Set<String> supplierMealIds = new HashSet<>();
		populateSupplierMealIdsFromHotels(hInfoList, supplierMealIds);
		hotelStaticMealInfoMap = cacheHandler.fetchMealInfoFromAerospike(searchQuery, supplierMealIds);
		try {
			hInfoList.forEach(hInfo -> {
				HotelMissingInfo missingInfo = new HotelMissingInfo(OperationType.LIST_SEARCH);
				mapHotelMealBasis(hInfo, searchQuery, missingInfo);
				hInfo.getMiscInfo().setIsMealAlreadyMapped(true);
				if (isLoggingRequired && CollectionUtils.isNotEmpty(missingInfo.getMealBasis())) {
					HotelMissingInfoLoggingUtil.addToLogList(missingInfo, hInfo, searchQuery,
							hInfo.getMiscInfo().getSupplierStaticHotelId());
				}
			});
			hInfoList.forEach(hInfo -> hInfo.getOptions().forEach(option -> {
				option.getRoomInfos().forEach(roomInfo -> {
					roomInfo.getMiscInfo().setAmenities(null);
				});
			}));
		} finally {
			HotelMissingInfoLoggingUtil.clearLog(LogTypes.MISSING_INFO);
		}
	}

	public static void populateMealInfo(List<HotelInfo> hInfoList, HotelSearchQuery searchQuery) {

		populateMealInfo(hInfoList, searchQuery, true);
	}

	public static void populateSupplierMealIdsFromHotels(List<HotelInfo> hotelList, Set<String> supplierMealIds) {
		hotelList.forEach(hotelInfo -> {
			hotelInfo.getOptions().forEach(option -> {
				option.getRoomInfos().forEach(roomInfo -> {
					if (Objects.nonNull(roomInfo.getMiscInfo())
							&& CollectionUtils.isNotEmpty(roomInfo.getMiscInfo().getAmenities())) {
						supplierMealIds.addAll(roomInfo.getMiscInfo().getAmenities());
					}
				});
			});
		});
	}

	public static String getSupplierCountryCode(String masterCountryCode, String supplierName) {

		return !ObjectUtils.isEmpty(cacheHandler.getSupplierCountryInfoFromCountryId(masterCountryCode, supplierName))
				? cacheHandler.getSupplierCountryInfoFromCountryId(masterCountryCode, supplierName)
						.getSupplierCountryId()
				: "20";
	}

	public static double getAmountBasedOnCurrency(double amount, String fromCurrency, String type, String toCurrency) {

		if (fromCurrency.equalsIgnoreCase(toCurrency))
			return amount;
		MoneyExchangeInfoFilter filter =
				MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency).toCurrency(toCurrency).type(type).build();
		double exchangeAmount = moneyExchnageComm.getExchangeValue(amount, filter, true);
		if (exchangeAmount == 0) {
			filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency).toCurrency(toCurrency).type("Default")
					.build();
			exchangeAmount = moneyExchnageComm.getExchangeValue(amount, filter, true);
		}
		return exchangeAmount;

	}

	public static String getSupplierCurrency(HotelSupplierConfiguration supplierConf) {

		return StringUtils.isEmpty(supplierConf.getHotelSupplierCredentials().getCurrency())
				? BaseHotelConstants.DEFAULT_CURRENCY
				: supplierConf.getHotelSupplierCredentials().getCurrency();
	}

	public static void saveOrUpdateRegionInfo(List<HotelRegionInfoQuery> regionInfoQueries, Boolean isMasterData,
			Boolean isFullRegionNameExists) {

		if(CollectionUtils.isEmpty(regionInfoQueries))
			return;
		
		String supplierName = regionInfoQueries.get(0).getSupplierName();
		try {
			if (BooleanUtils.isTrue(isMasterData)) {
				HotelBaseSupplierUtils.masterRegionInfoSaveManager.process(regionInfoQueries, false,
						isFullRegionNameExists);
			} else {
				HotelBaseSupplierUtils.supplierRegionInfoSaveManager
						.saveSupplierRegionInfo(HotelUtils.getSupplierRegionInfo(regionInfoQueries, supplierName));
				HotelBaseSupplierUtils.regionInfoMappingManager.saveRegionInfoMappingList(regionInfoQueries, true,
						isFullRegionNameExists);
			}
		} catch (Exception e) {
			log.info("Error while processing regions for supplier {} due to ", supplierName, e);
		}
	}
}
