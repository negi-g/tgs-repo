package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;

@Service
public class TravelbullzStaticDataFactory extends AbstractStaticDataInfoFactory {

	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	public TravelbullzStaticDataFactory(HotelSupplierConfiguration supplierConf,
			HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);

	}

	@Override
	protected void getHotelStaticData() throws IOException, JAXBException {
		TravelbullzHotelStaticDataService hotelStaticDataService = TravelbullzHotelStaticDataService.builder()
				.supplierConf(supplierConf).staticDataRequest(staticDataRequest)
				.hotelInfoSaveManager(hotelInfoSaveManager).build();
		hotelStaticDataService.init();
		hotelStaticDataService.process();
	}

	@Override
	protected void getCityMappingData() throws IOException {
		TravelbullzCityStaticDataService cityStaticDataService =
				TravelbullzCityStaticDataService.builder().supplierConf(supplierConf).build();
		List<HotelRegionInfoQuery> cityList = cityStaticDataService.getTravelbullzCityList();
		HotelBaseSupplierUtils.saveOrUpdateRegionInfo(cityList, staticDataRequest.getIsMasterData(), false);
	}

	@Override
	protected Map<String, String> getRoomMappingData(HotelSearchQuery searchQuery, HotelInfo hotelInfo)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void getNationalityMappingData() throws IOException {
		// TODO Auto-generated method stub

	}

}
