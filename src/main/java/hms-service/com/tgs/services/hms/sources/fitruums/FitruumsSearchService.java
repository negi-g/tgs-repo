package com.tgs.services.hms.sources.fitruums;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.util.CollectionUtils;
import com.amazonaws.util.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.enums.CountryInfoType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.fitruums.CancellationPolicy;
import com.tgs.services.hms.datamodel.fitruums.Meal;
import com.tgs.services.hms.datamodel.fitruums.Note;
import com.tgs.services.hms.datamodel.fitruums.Room;
import com.tgs.services.hms.datamodel.fitruums.Roomtype;
import com.tgs.services.hms.datamodel.fitruums.Searchresult;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Getter
@Setter
public class FitruumsSearchService extends FitruumsBaseService {
	private HotelSearchResult searchResult;
	private String hotelIds;

	public void doSearch(int threadCount) throws IOException {
		HttpUtils httpUtils = null;
		searchResult = HotelSearchResult.builder().build();
		HashMap<String, String> searchRequestMap = getSearchRequestMap();
		FitruumsMarshaller fitruumsMarshaller = new FitruumsMarshaller();
		String xmlResponse = null;
		Searchresult fitruumsSearchResult = null;
		try {
			listener = new RestAPIListener("");
			String searchRequest = getKeyValuePair(searchRequestMap);
			httpUtils =
					FitruumsUtil.getHttpUtils(searchRequest, HotelUrlConstants.HOTEL_SEARCH_URL, null, supplierConf);
			xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (!StringUtils.isNullOrEmpty(xmlResponse)) {
				fitruumsSearchResult = fitruumsMarshaller.unmarshallXML(xmlResponse, Searchresult.class);
				List<HotelInfo> hotelList = getHotelListFromFitruumsResult(fitruumsSearchResult, false);
				searchResult.setHotelInfos(hotelList);
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().stream()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.SEARCH.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(fitruumsSearchResult)) {
					SystemContextHolder.getContextData().getErrorMessages()
							.add(httpUtils.getResponseString() + httpUtils.getPostData());
				}
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.FITRUUMS.name())
						.requestType(BaseHotelConstants.SEARCH).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId()).threadCount(threadCount)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	public void doDetailSearch() throws IOException {
		HttpUtils httpUtils = null;
		HashMap<String, String> searchRequestMap = getSearchRequestMap();
		FitruumsMarshaller fitruumsMarshaller = new FitruumsMarshaller();
		String xmlResponse = null;
		Searchresult fitruumsSearchResult = null;
		listener = new RestAPIListener("");
		try {
			String searchRequest = getKeyValuePair(searchRequestMap);
			httpUtils =
					FitruumsUtil.getHttpUtils(searchRequest, HotelUrlConstants.HOTEL_SEARCH_URL, null, supplierConf);
			xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (!StringUtils.isNullOrEmpty(xmlResponse)) {
				fitruumsSearchResult = fitruumsMarshaller.unmarshallXML(xmlResponse, Searchresult.class);
				List<HotelInfo> hotelList = getHotelListFromFitruumsResult(fitruumsSearchResult, true);
				searchResult = HotelSearchResult.builder().build();
				searchResult.setHotelInfos(hotelList);
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().stream()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.DETAIL.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(fitruumsSearchResult)) {
					SystemContextHolder.getContextData().getErrorMessages()
							.add(httpUtils.getResponseString() + httpUtils.getPostData());
				}
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.FITRUUMS.name())
						.requestType(BaseHotelConstants.DETAILSEARCH).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}

		}
	}

	private HashMap<String, String> getSearchRequestMap() {
		HashMap<String, String> searchRequestmap = new HashMap<>();
		HashMap<String, String> adultChildMap = setAdultChild();
		searchRequestmap = setBasicRequirement(searchRequestmap);
		searchRequestmap.put("checkInDate", getDate(searchQuery.getCheckinDate()));
		searchRequestmap.put("checkOutDate", getDate(searchQuery.getCheckoutDate()));
		searchRequestmap.put("currencies", FitruumsConstant.CURRENCY.getValue());
		searchRequestmap.put("numberOfRooms", String.valueOf(searchQuery.getRoomInfo().size()));
		searchRequestmap.put("numberOfAdults", adultChildMap.get("adults"));
		searchRequestmap.put("numberOfChildren", adultChildMap.get("children"));
		searchRequestmap.put("childrenAges", adultChildMap.get("childrenAges"));
		searchRequestmap.put("infant", adultChildMap.get("infant"));
		searchRequestmap.put("destinationID", "");
		searchRequestmap.put("hotelIDs", hotelIds);
		searchRequestmap.put("destination", "");
		searchRequestmap.put("resortIDs", "");
		searchRequestmap.put("accommodationTypes", "");
		searchRequestmap.put("sortBy", "");
		searchRequestmap.put("sortOrder", "");
		searchRequestmap.put("exactDestinationMatch", "");
		searchRequestmap.put("blockSuperdeal", "");
		searchRequestmap.put("mealIds", "");
		searchRequestmap.put("showCoordinates", "");
		searchRequestmap.put("showReviews", "");
		searchRequestmap.put("referencePointLatitude", "");
		searchRequestmap.put("referencePointLongitude", "");
		searchRequestmap.put("maxDistanceFromReferencePoint", "");
		searchRequestmap.put("minStarRating", "");
		searchRequestmap.put("maxStarRating", "");
		searchRequestmap.put("featureIds", "");
		searchRequestmap.put("minPrice", "");
		searchRequestmap.put("maxPrice", "");
		searchRequestmap.put("themeIds", "");
		searchRequestmap.put("excludeSharedRooms", "");
		searchRequestmap.put("excludeSharedFacilities", "");
		searchRequestmap.put("prioritizedHotelIds", "");
		searchRequestmap.put("totalRoomsInBatch", "");
		searchRequestmap.put("paymentMethodId", FitruumsConstant.PAYMENTMETHODID.getValue());
		searchRequestmap.put("customerCountry", BaseHotelUtils
				.getSpecificFieldFromId(searchQuery.getSearchCriteria().getNationality(), CountryInfoType.CODE.name()));
		searchRequestmap.put("b2c", FitruumsConstant.B2C.getValue());

		return searchRequestmap;
	}

	private List<HotelInfo> getHotelListFromFitruumsResult(Searchresult searchResult, Boolean isDetail) {
		List<HotelInfo> hInfos = new ArrayList<>();
		if (Objects.isNull(searchResult) || Objects.isNull(searchResult.getHotels())) {
			log.info("Empty Response From Supplier {} for searchId {}", supplierConf.getBasicInfo().getSupplierId(),
					searchQuery.getSearchId());
			return null;
		}
		searchResult.getHotels().getHotel().forEach(hotelResponse -> {
			List<Option> optionList = new ArrayList<>();
			List<Instruction> hotelInstructionList = getinstructionList(hotelResponse.getNotes().getNote());

			for (Roomtype optionResponse : hotelResponse.getRoomtypes().getRoomtype()) {
				List<CancellationPolicy> policyList =
						optionResponse.getRooms().getRoom().get(0).getCancellationPolicies().getCancellationPolicy();
				/**
				 * diff option for each meal, as prices according to meal
				 */
				for (Meal meal : optionResponse.getRooms().getRoom().get(0).getMeals().getMeal()) {
					int fitruumsPrice = meal.getPrices().getPrice().getValue();
					String roomTypeId = String.valueOf(optionResponse.getRoomtypeID());
					List<RoomInfo> roomList = getRooms(optionResponse.getRooms().getRoom().get(0), roomTypeId, meal);
					Option option = Option.builder().roomInfos(roomList)
							.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
									.supplierHotelId(String.valueOf(hotelResponse.getHotelId()))
									.sourceId(supplierConf.getBasicInfo().getSourceId())
									.secondarySupplier(supplierConf.getBasicInfo().getSupplierId()).build())
							.id(RandomStringUtils.random(20, true, true)).build();
					List<Instruction> roomInstructionList = getInstructions(optionResponse);
					option.setInstructions(roomInstructionList);
					setOptionCancellationPolicy(policyList, option, fitruumsPrice);
					if (!isDetail) {
						option.getMiscInfo().setIsNotRequiredOnDetail(true);
					}
					optionList.add(option);
				}
			}
			HotelInfo hInfo = HotelInfo.builder().instructions(hotelInstructionList)
					.miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
							.supplierStaticHotelId(String.valueOf(hotelResponse.getHotelId())).build())
					.options(optionList).build();
			HotelUtils.setBufferTimeinCnp(hInfo.getOptions(), searchQuery);
			hInfos.add(hInfo);
		});
		FitruumsUtil.setMealBasis(hInfos, searchQuery);
		return hInfos;
	}

	private List<Instruction> getInstructions(Roomtype optionResponse) {
		List<Instruction> instructionList = new ArrayList<>();
		for (Room room : optionResponse.getRooms().getRoom()) {
			if (Objects.nonNull(room.getNotes())) {
				List<Instruction> roomInstructionList = getinstructionList(room.getNotes().getNote());
				instructionList.addAll(roomInstructionList);
			}
		}
		return instructionList;
	}

	private List<Instruction> getinstructionList(List<Note> notesList) {
		List<Instruction> instructionList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(instructionList) && notesList.size() >= 1) {
			for (Note note : notesList) {
				Instruction instruction =
						Instruction.builder().type(InstructionType.BOOKING_NOTES).msg(note.getText()).build();
				instructionList.add(instruction);
			}
		}
		return instructionList;
	}

}
