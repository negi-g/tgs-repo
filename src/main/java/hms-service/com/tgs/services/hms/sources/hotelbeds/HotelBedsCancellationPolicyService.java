package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsRateCheckRequest;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsRateCheckResponse;
import com.tgs.services.hms.datamodel.hotelBeds.Rate;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class HotelBedsCancellationPolicyService extends HotelBedsBaseService {

	private HotelInfo hInfo;
	HotelBedsRateCheckResponse priceValidationResponse;

	public void searchCancellationPolicy(String logKey) throws IOException {
		listener = new RestAPIListener("");
		HttpUtilsV2 httpUtils = null;
		try {
			HotelBedsRateCheckRequest priceValidationRequest = getRateCheckRequest(hInfo.getOptions().get(0));
			httpUtils = HotelBedsUtils.getHttpUtils(priceValidationRequest, null, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.BLOCK_ROOM_URL));
			priceValidationResponse = httpUtils.getResponse(HotelBedsRateCheckResponse.class).orElse(null);
			updateOptionWithCancellationPolicy(priceValidationResponse, hInfo.getOptions().get(0));
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.CANCELLATION.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(priceValidationResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.HOTELBEDS.name())
						.requestType(BaseHotelConstants.CANCELLATIONPOLICY).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(logKey)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}

	}

	private void updateOptionWithCancellationPolicy(HotelBedsRateCheckResponse priceValidationResponse, Option option) {

		Map<String, Rate> rateKeyRateVal = new HashMap<>();
		searchQuery.setSourceId(HotelSourceType.HOTELBEDS.getSourceId());
		if (!ObjectUtils.isEmpty(priceValidationResponse.getHotel())) {
			priceValidationResponse.getHotel().getRooms().forEach(room -> {
				room.getRates().forEach(rate -> {
					rateKeyRateVal.put(rate.getRateKey(), rate);
				});
			});
			option.getRoomInfos().forEach(room -> {
				room.setCancellationPolicy(getRoomCancellationPolicy(
						rateKeyRateVal.get(room.getMiscInfo().getRatePlanCode()), searchQuery, room));
			});
			HotelUtils.setOptionCancellationPolicyFromRooms(new ArrayList<>(Arrays.asList(option)));
			HotelUtils.setBufferTimeinCnp(new ArrayList<>(Arrays.asList(option)), searchQuery);
		}
	}
}
