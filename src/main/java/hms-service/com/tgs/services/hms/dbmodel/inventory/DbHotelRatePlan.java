package com.tgs.services.hms.dbmodel.inventory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.HotelMealBasisType;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryAllocationInfo;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlanInfo;
import lombok.Getter;
import lombok.Setter;

@Audited
@Getter
@Setter
@Entity
@Table(name="hotelrateplan")
@TypeDefs({
	@TypeDef(name = "HotelRatePlanInfoType", typeClass = HotelRatePlanInfoType.class),
	@TypeDef(name = "HotelInventoryAllocationInfoType", typeClass = HotelInventoryAllocationInfoType.class)
})
public class DbHotelRatePlan extends BaseModel<DbHotelRatePlan, HotelRatePlan> {
	
	@Column
	private String name;

	@Column
	@SerializedName("rcid")
	private String roomCategoryId;
	
	@Column
	@SerializedName("mbid")
	private String mealBasisId;
	
	@Column
	@SerializedName("sid")
	private String supplierId;
	
	@Column
	@SerializedName("shid") 
	private String supplierHotelId;
	
	@Column
	@SerializedName("rtid") 
	private String roomTypeId;
	
	@Column
	@SerializedName("sp")
	private String sellPolicy;
	
	@Column
	@Type(type = "HotelRatePlanInfoType")
	@SerializedName("rpi")
	private HotelRatePlanInfo ratePlanInfo;
	
	@Column
	@SerializedName("iai")
	@Type(type = "HotelInventoryAllocationInfoType")
	private HotelInventoryAllocationInfo inventoryAllocationInfo;
	
	@Column
    @CreationTimestamp
	private LocalDateTime createdOn;
	
	@Column
	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private LocalDate validOn;
	
	@Column
	private boolean enabled;
	
	@Column
	private boolean isDeleted;
	
	@Override
    public HotelRatePlan toDomain() {
		
        HotelRatePlan ratePlan = new GsonMapper<>(this, HotelRatePlan.class).convert();
        ratePlan.setMealBasisName(HotelMealBasisType.getMealBasisType(ratePlan.getMealBasisId()).name());
        return ratePlan;
    }

    @Override
    public DbHotelRatePlan from(HotelRatePlan dataModel) {
        return new GsonMapper<>(dataModel, this, DbHotelRatePlan.class).convert();
    }
}
