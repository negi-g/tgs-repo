package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.base.runtime.database.CustomTypes.HotelBasicRuleCriteriaType;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;

import lombok.Getter;
import lombok.Setter;

@Audited
@Getter
@Setter
@Entity
@Table(name="hotelconfiguratorrule")
@TypeDefs({ @TypeDef(name = "HotelBasicRuleCriteriaType", typeClass = HotelBasicRuleCriteriaType.class) })
public class DbHotelConfiguratorRule extends BaseModel<DbHotelConfiguratorRule, HotelConfiguratorInfo> {

    @Column
    @CreationTimestamp
    private LocalDateTime createdOn;
    
    @CreationTimestamp
	private LocalDateTime processedOn;

    @Column
    private boolean enabled;

    @Column
    @Nonnull
    private String ruleType;

    @Column
    @Type(type = "HotelBasicRuleCriteriaType")
    private HotelBasicRuleCriteria inclusionCriteria;

    @Column
    @Type(type = "HotelBasicRuleCriteriaType")
    private HotelBasicRuleCriteria exclusionCriteria;

    @Column
    @Nonnull
    @JsonAdapter(JsonStringSerializer.class)
    private String output;

    @Column
    private double priority;

    @Column
    private Boolean exitOnMatch;
    
    @Column
    private boolean isDeleted;
    
    @Column
    private String description;
    
    @Override
    public HotelConfiguratorInfo toDomain() {
    	return new GsonMapper<>(this, HotelConfiguratorInfo.class).convert();
    }
    
    @Override
    public DbHotelConfiguratorRule from(HotelConfiguratorInfo dataModel) {
    	return new GsonMapper<>(dataModel, this, DbHotelConfiguratorRule.class).convert();
    }

}