package com.tgs.services.hms.sources.tripjack;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;

@Service
public class TripjackPriceValidationFactory extends AbstractHotelPriceValidationFactory {

	@Autowired
	HotelCacheHandler cacheHandler;
	
	public TripjackPriceValidationFactory(HotelSearchQuery query, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
		super(query, hotel, hotelSupplierConf, bookingId);
	}

	@Override
	public Option getOptionWithUpdatedPrice() throws Exception {
		String endpoint = supplierConf.getHotelSupplierCredentials().getUrl();

		TripjackPriceValidationService priceValidationService = TripjackPriceValidationService.builder()
				.searchQuery(this.searchQuery).bookingId(bookingId).endpoint(endpoint).sourceConfig(sourceConfigOutput)
				.cacheHandler(cacheHandler).supplierConf(this.getSupplierConf()).build();
		priceValidationService.validate(this.getHInfo());
		return this.getHInfo().getOptions().get(0);
	}
}
