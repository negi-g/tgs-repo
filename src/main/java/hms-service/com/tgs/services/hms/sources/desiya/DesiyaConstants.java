package com.tgs.services.hms.sources.desiya;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DesiyaConstants {
	
	public static String CURRENCY="INR";
	public static String SORT_ORDER="TG_RANKING";
	public static String SEARCH_SUFFIX="/TGServiceEndPoint";
	public static String BOOKING_SUFFIX="/TGBookingServiceEndPoint";
	public static List<String> MEALS = new ArrayList<>(Arrays.asList("BREAKFAST","DINNER", "BREAKFAST AND DINNER","ALL MEALS"));
}
