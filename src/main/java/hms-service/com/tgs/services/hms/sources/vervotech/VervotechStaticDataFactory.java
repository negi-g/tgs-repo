package com.tgs.services.hms.sources.vervotech;

import java.io.IOException;
import java.util.Map;
import javax.xml.bind.JAXBException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;

@Service
public class VervotechStaticDataFactory extends AbstractStaticDataInfoFactory {

	public VervotechStaticDataFactory(HotelSupplierConfiguration supplierConf,
			HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getHotelStaticData() throws IOException, JAXBException {

		VervotechStaticDataService staticDataService =
				VervotechStaticDataService.builder().sourceConfig(sourceConfigOutput).supplierConf(supplierConf)
						.staticDataRequest(staticDataRequest).build();
		staticDataService.init();
		for (String fetchType : staticDataRequest.getFetchTypes()) {

			if (fetchType.equals("new")) {
				staticDataService.setIsNew(true);
				staticDataService.handleHotelStaticContent();
			} else if (fetchType.equals("updated")) {
				staticDataService.setIsNew(false);
				staticDataService.handleHotelStaticContent();
			} else if (fetchType.equals("deleted")) {
				staticDataService.deleteHotelStaticContent();
			}
		}
	}

	@Override
	protected void getCityMappingData() throws IOException {

	}

	@Override
	protected Map<String, String> getRoomMappingData(HotelSearchQuery searchQuery, HotelInfo hotelInfo)
			throws IOException {

		VervotechRoomMappingService roomMappingService =
				VervotechRoomMappingService.builder().sourceConfig(sourceConfigOutput).supplierConf(supplierConf)
					.searchQuery(searchQuery).hInfo(hotelInfo).build();
		roomMappingService.getRoomMappingInfo();
		return roomMappingService.getStandardRoomMapping();
	}

	@Override
	protected void getNationalityMappingData() throws IOException {
		// TODO Auto-generated method stub

	}
}
