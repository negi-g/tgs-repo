package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DesiyaStaticDataFactory extends AbstractStaticDataInfoFactory {

	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	private HotelRegionInfoMappingManager regionInfoMappingManager;

	@Autowired
	private HotelSupplierRegionInfoManager supplierRegionInfoManager;

	public DesiyaStaticDataFactory(HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getCityMappingData() throws IOException {
		DesiyaCitySaveService citySaveService =
				DesiyaCitySaveService.builder().supplierConf(supplierConf)
						.regionInfoMappingManager(regionInfoMappingManager)
						.staticDataRequest(staticDataRequest).supplierRegionInfoManager(supplierRegionInfoManager)
						.build();
		citySaveService.saveDesiyaCities();
	}

	@Override
	protected void getHotelStaticData() throws IOException {

		DesiyaHotelStaticDataSaveService hotelSaveService = DesiyaHotelStaticDataSaveService.builder()
				.hotelInfoSaveManager(hotelInfoSaveManager).supplierConf(supplierConf).build();
		try {
			hotelSaveService.process();
		} catch (Exception e) {
			log.error("Error While fetching static data", e);
		}


	}

	@Override
	protected Map<String, String> getRoomMappingData(HotelSearchQuery searchQuery, HotelInfo hotelInfo) {

		return null;
	}

	@Override
	protected void getNationalityMappingData() throws IOException {
		// TODO Auto-generated method stub

	}
}
