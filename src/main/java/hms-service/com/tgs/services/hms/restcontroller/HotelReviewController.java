package com.tgs.services.hms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.hms.restmodel.HotelPriceValidationRequest;
import com.tgs.services.hms.restmodel.HotelReviewRequest;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.hms.servicehandler.HotelPriceValidateHandler;
import com.tgs.services.hms.servicehandler.HotelReviewHandler;
import com.tgs.services.hms.validators.HotelReviewValidator;

@RestController
@RequestMapping("/hms/v1")
public class HotelReviewController {

	@Autowired
	HotelReviewValidator reviewValidator;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(reviewValidator);
	}
	
	@Autowired
	HotelReviewHandler reviewHandler;
	
	@Autowired
	HotelPriceValidateHandler priceValidateHandler;

	@RequestMapping(value = "/hotel-review", method = RequestMethod.POST)
	protected HotelReviewResponse searchHotelDetail(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid HotelReviewRequest reviewRequest) throws Exception {
		reviewHandler.initData(reviewRequest, new HotelReviewResponse());
		return reviewHandler.getResponse();
	}
	
	
	/*
	 * Front End won't use  this for now . We will validate price internally
	 */
	@RequestMapping(value="/validate-price" , method = RequestMethod.POST)
	protected HotelReviewResponse validatePrice(HttpServletRequest request, HttpServletResponse response
			, @RequestBody HotelPriceValidationRequest priceValidationRequest) throws Exception {
		
		priceValidateHandler.initData(priceValidationRequest, new HotelReviewResponse());
		return priceValidateHandler.getResponse();
		
	}
	
}
