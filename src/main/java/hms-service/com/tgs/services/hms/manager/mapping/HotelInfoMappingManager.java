package com.tgs.services.hms.manager.mapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMealBasisType;
import com.tgs.services.hms.datamodel.HotelStaticDataMappingInfo;
import com.tgs.services.hms.datamodel.HotelStaticDataMappingRequest;
import com.tgs.services.hms.datamodel.inventory.MappedHotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelMealBasis;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierMappingInfo;
import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.restmodel.HotelMealInfoResponse;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelInfoMappingManager {

	@Autowired
	HotelInfoService infoService;
	
	@Autowired
	HotelInfoSaveManager hotelInfoManager;
	
	@Autowired
	HotelCacheHandler cacheHandler;
	
	public void processHotelInfo(String filePath) throws Exception {
		List<Map<?, ?>> csvList = HotelUtils.getCSVListFromFile(filePath);
		saveHotelInfoMappingList(csvList);
	}

	private void saveHotelInfoMappingList(List<Map<?, ?>> csvList) {
		ExecutorService executor = Executors.newFixedThreadPool(10);
		for (Map<?, ?> csvEntry : csvList) {
			executor.submit(() -> {
				HotelSupplierMappingInfo supplierInfo = null;
				try {
					supplierInfo = processHotelMappingInfo(csvEntry);
					DbHotelSupplierMapping dbSupplierMapping = new DbHotelSupplierMapping().from(supplierInfo);
					try {
						dbSupplierMapping = infoService.save(dbSupplierMapping);
						log.debug("New Hotel Info Mapping object saved is {}", dbSupplierMapping);
					} catch (Exception e) {
						DbHotelSupplierMapping oldDbObject = infoService.findMapping(supplierInfo.getSupplierName(),
								supplierInfo.getHotelId());
						oldDbObject = new GsonMapper<>(dbSupplierMapping, oldDbObject, DbHotelSupplierMapping.class)
								.convert();
						log.info("Updated Hotel Info Mapping Object for supplier {} ,hotel id {}",
								oldDbObject.getSourceName(), oldDbObject.getHotelId());
						infoService.save(oldDbObject);
					}
				} catch (Exception e) {
					log.error("Error while processing Hotel Info Mapping for supplier name {} , sourceName {}, hotel id {} ",
							supplierInfo.getSupplierName(), supplierInfo.getSourceName(), supplierInfo.getHotelId(), e);
				}
			});
		}
	}

	public HotelSupplierMappingInfo processHotelMappingInfo(Map<?, ?> csvEntry) {
		HotelSupplierMappingInfo supplierInfo = HotelSupplierMappingInfo.builder().hotelId(Long.valueOf(csvEntry.get("hotelid").toString()))
				.supplierHotelId(csvEntry.get("supplierhotelId").toString())
				.supplierName(csvEntry.get("suppliername").toString())
				.sourceName(csvEntry.get("suppliername").toString()).build();
		return supplierInfo;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public void processHotelInfoMapping(HotelStaticDataMappingRequest mappingRequest) {
		
		HotelSupplierInfo supplierInfo = HotelSupplierConfigurationHelper.getSupplierInfo(mappingRequest.getSupplierId());
		String sourceName = HotelSourceType.getHotelSourceType(supplierInfo.getSourceId()).name();
		
		if(Objects.isNull(supplierInfo)) {
			throw new CustomGeneralException(SystemError.NO_SUPPLIER_FOUND);
		}
		
		
		for(HotelStaticDataMappingInfo info : mappingRequest.getMappingInfoList()) {
			
			String supplierName = supplierInfo.getName();
			String supplierHotelId = ObjectUtils.firstNonNull(info.getSupplierhotelId()
					,String.join("-",  supplierName, info.getHotelId()));
			
 			HotelSupplierMappingInfo mappingInfo = HotelSupplierMappingInfo.builder().hotelId(Long.valueOf(info.getHotelId()))
					.sourceName(sourceName).supplierHotelId(supplierHotelId)
					.supplierName(supplierName).build();
			DbHotelSupplierMapping dbMappingInfo = new DbHotelSupplierMapping().from(mappingInfo);
			try {
				dbMappingInfo = infoService.save(dbMappingInfo);
				cacheHandler.storeSupplierMappingInCache(dbMappingInfo.toDomain());
			}catch(ConstraintViolationException e) {
				log.error("Error while creating mapping for supplier {} , hotelId {}"
						, mappingRequest.getSupplierId(), info.getHotelId());
				throw new CustomGeneralException(SystemError.MAPPING_ALREADY_EXISTS);
			}
			
		}
		
	}
	
	public List<MappedHotelInfo> fetchHotelInfoMapping(HotelStaticDataMappingRequest mappingRequest) {
		
		HotelSupplierInfo supplierInfo = HotelSupplierConfigurationHelper.getSupplierInfo(mappingRequest.getSupplierId());
		if(Objects.isNull(supplierInfo)) throw new CustomGeneralException(SystemError.NO_SUPPLIER_FOUND);
		String sourceName = HotelSourceType.getHotelSourceType(supplierInfo.getSourceId()).name();
		String supplierName = supplierInfo.getName();
		
		List<DbHotelSupplierMapping> dbMappingList = infoService.findAllBySupplierNameAndSourceName(supplierName, sourceName);
		List<HotelSupplierMappingInfo> mappingList = DbHotelSupplierMapping.toDomainList(dbMappingList);
		
		List<MappedHotelInfo> mappedHotelList = new ArrayList<>();
		
		if(CollectionUtils.isEmpty(mappingList)) return mappedHotelList;
		Map<Long, HotelSupplierMappingInfo>  mappingMap =  mappingList.stream()
				.collect(Collectors.toMap(HotelSupplierMappingInfo :: getHotelId , Function.identity()));

		
		List<Long> idList = new ArrayList<>();
		mappingList.forEach(mappingInfo -> idList.add(mappingInfo.getHotelId()));
		List<HotelInfo> hInfoList =  hotelInfoManager.fetchHotelWithBasicInfoFromMasterHotelIdList(idList);
		Map<String, HotelInfo> masterIdAsKeyHotelAsValue = hInfoList.stream()
				.collect(Collectors.toMap(HotelInfo :: getId, Function.identity()));
		masterIdAsKeyHotelAsValue.keySet().forEach(key -> {
			HotelInfo hInfo = masterIdAsKeyHotelAsValue.get(key);
			HotelSupplierMappingInfo mappingInfo = mappingMap.get(Long.valueOf(hInfo.getId()));
			MappedHotelInfo mappedHotelInfo = MappedHotelInfo.builder().hInfo(hInfo)
					.supplierHotelId(mappingInfo.getSupplierHotelId()).build();
			mappedHotelList.add(mappedHotelInfo);
			
		});
		
		return mappedHotelList;
		
	}
	
	public HotelMealInfoResponse getMealInfo() {
		HotelMealInfoResponse mealInfoResponse = new HotelMealInfoResponse();
		for (HotelMealBasisType mealType : HotelMealBasisType.values()) {
			mealInfoResponse.getMealInfos()
					.add(HotelMealBasis.builder().code(mealType.getCode()).name(mealType.getName()).build());
		}
		return mealInfoResponse;
	}

}

