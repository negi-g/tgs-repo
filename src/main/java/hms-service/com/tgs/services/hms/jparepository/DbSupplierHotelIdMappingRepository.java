package com.tgs.services.hms.jparepository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.tgs.services.hms.dbmodel.DbSupplierHotelIdMapping;

@Repository
public interface DbSupplierHotelIdMappingRepository extends JpaRepository<DbSupplierHotelIdMapping, Long> {

	public List<DbSupplierHotelIdMapping> findAll();

	public DbSupplierHotelIdMapping findByHotelId(String hotelid);

	public Page<DbSupplierHotelIdMapping> findAll(Pageable pageable);

	public List<DbSupplierHotelIdMapping> findByIdBetween(Long from, Long to);


}
