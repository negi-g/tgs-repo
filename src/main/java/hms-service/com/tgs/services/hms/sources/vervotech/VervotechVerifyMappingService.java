package com.tgs.services.hms.sources.vervotech;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import com.amazonaws.util.StringUtils;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.tgs.services.base.datamodel.VerifyHotelMappingQuery;
import com.tgs.services.base.datamodel.VerifyMappingRequest;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelCredentialsType;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.utils.file.TgsCsvUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Setter
@Getter
public class VervotechVerifyMappingService {
	private HotelSourceConfigOutput sourceConfig;
	private HotelSupplierConfiguration supplierConf;
	private VerifyMappingRequest verifyMappingData;
	private static String currentTimestamp = LocalDate.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd"));

	public void uploadHotelMapping() throws SftpException, JSchException, IOException {
		File file = new File(getFilename());
		log.info("Total number of rows in file {} are {}", file.getName(),
				verifyMappingData.getVerifyMappinglist().size());
		TgsCsvUtils.writeToCSV(getContent(verifyMappingData.getVerifyMappinglist()), file, getOutputFields());
		Boolean status = uploadSftpToRemoteServer(file);
		log.info("Upload status of file {} on server on date {} is {}", file.getName(), currentTimestamp, status);
	}

	public List<String> getOutputFields() {
		return Arrays.asList("UnicaId", "BookingReferenceId", "BookingClientName", "ProviderName", "ProviderHotelId",
				"CheckInDate", "CheckOutDate");
	}

	private List<Object[]> getContent(List<VerifyHotelMappingQuery> verifyMappinglist) {
		List<Object[]> objectList = new ArrayList<>();
		for (VerifyHotelMappingQuery verifyMappingRequestObject : verifyMappinglist) {
			VervotechSupplierMapping vervotechProviderName =
					VervotechSupplierMapping.getVervotechSupplierMapping(verifyMappingRequestObject.getProviderName());
			if (StringUtils.isNullOrEmpty(verifyMappingRequestObject.getUnicaId())
					|| Objects.isNull(vervotechProviderName)
					|| vervotechProviderName.getHotelSourceType().name().equalsIgnoreCase("QTECH")) {
				continue;
			} else {
				objectList.add(new Object[] {verifyMappingRequestObject.getUnicaId(),
						verifyMappingRequestObject.getBookingReferenceId(),
						verifyMappingRequestObject.getBookingClientName(),
						vervotechProviderName.getVervotechSupplierName(),
						verifyMappingRequestObject.getProviderHotelId(), verifyMappingRequestObject.getCheckInDate(),
						verifyMappingRequestObject.getCheckOutDate()});
			}
		}
		return objectList;
	}

	public ChannelSftp setupJsch() throws JSchException {
		Map<String, String> credentialsMap =
				supplierConf.getHotelSupplierCredentials().getAdditionalCredentials().get(HotelCredentialsType.FTP);
		if (Objects.isNull(credentialsMap)) {
			log.info("Credentials not found for supplier {}", supplierConf.getBasicInfo().getSupplierName());
			return null;
		}
		String username = credentialsMap.get("username");
		String password = credentialsMap.get("password");
		String host = credentialsMap.get("host");
		JSch jsch = new JSch();
		log.info("Creating session with username {} on host {} on {}", username, host, currentTimestamp);
		Session jschSession = jsch.getSession(username, host);
		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		jschSession.setConfig(config);
		jschSession.setPassword(password);
		jschSession.connect();
		return (ChannelSftp) jschSession.openChannel("sftp");
	}

	public boolean uploadSftpToRemoteServer(File sftpFile) throws SftpException, JSchException, IOException {
		ChannelSftp channelSftp = null;
		channelSftp = setupJsch();
		if (Objects.isNull(channelSftp)) {
			log.info("Unable to setup channel for supplier {} on date {}",
					supplierConf.getBasicInfo().getSupplierName(), currentTimestamp);
			return false;
		}
		log.info("Attempting to connect to channel on {} to upload file {}", currentTimestamp, sftpFile.getName());
		channelSftp.connect();
		log.info("Connection to ftp channel successfull on {}", currentTimestamp);
		createDirectoryIfDoesNotExists(channelSftp);
		channelSftp.put(new FileInputStream(sftpFile), sftpFile.getName());
		channelSftp.exit();
		return true;
	}

	private void createDirectoryIfDoesNotExists(ChannelSftp channelSftp) throws SftpException {
		String currentDirectory = channelSftp.pwd();
		String directoryPath = getDirectoryPath();
		String[] directoriesList = directoryPath.split("/");
		for (String directory : directoriesList) {
			SftpATTRS attrs = null;
			try {
				attrs = channelSftp.stat(currentDirectory + directory);
			} catch (Exception e) {
				log.info("Unable to find directory with name {} on date {}", directory, currentTimestamp);
			}
			if (Objects.isNull(attrs)) {
				log.info("Creating new directory with name {} on date {}", directory, currentTimestamp);
				channelSftp.cd(currentDirectory);
				channelSftp.mkdir(directory);
			}
			currentDirectory = currentDirectory + directory + "/";
		}
		channelSftp.cd("/" + directoryPath + "/");
	}

	public String getFilename() {
		DateFormat fulldateFormat = new SimpleDateFormat("ddMMYYYY");
		Date date = Calendar.getInstance().getTime();
		String filename = fulldateFormat.format(date).concat(".csv");
		return filename;
	}

	public String getDirectoryPath() {
		DateFormat halfdateFormat = new SimpleDateFormat("MMYYYY");
		Date date = Calendar.getInstance().getTime();
		String directoryName = sourceConfig.getVerifyMappingDirectory();
		String remoteDirfileName = StringUtils.isNullOrEmpty(directoryName) ? "VerifyMappings"
				: directoryName.concat("/").concat(halfdateFormat.format(date));
		return remoteDirfileName;
	}
}
