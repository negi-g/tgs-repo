package com.tgs.services.hms.servicehandler;

import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class HotelInfoStaticDataHandler extends ServiceHandler<HotelStaticDataRequest, BaseResponse>{

	@Override
	public void beforeProcess() throws Exception {
		
	}

	@Override
	public void process() throws Exception {

		log.info("Starting to fetch hotel static data for request {}", GsonUtils.getGson().toJson(request));
		AbstractStaticDataInfoFactory factory = HotelSourceType.getStaticDataFactoryInstance(request.getSupplierId() , request);
		factory.getStaticHotelInfo();
		log.info("Finished fetching hotel static data for request {}", GsonUtils.getGson().toJson(request));
	}

	@Override
	public void afterProcess() throws Exception {
		
	}
}
