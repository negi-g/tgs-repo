package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.travelbullz.AdvancedOptions;
import com.tgs.services.hms.datamodel.travelbullz.BookingCancellationResponse;
import com.tgs.services.hms.datamodel.travelbullz.CancelRQ;
import com.tgs.services.hms.datamodel.travelbullz.CancellationChargesResponse;
import com.tgs.services.hms.datamodel.travelbullz.CheckHotelCancellationChargesRQ;
import com.tgs.services.hms.datamodel.travelbullz.HotelCancellationOption;
import com.tgs.services.hms.datamodel.travelbullz.HotelRoom;
import com.tgs.services.hms.datamodel.travelbullz.Request;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
@Service
public class TravelbullzBookingCancellationService extends TravelbullzBaseService {

	private GeneralServiceCommunicator gnComm;
	private Order order;
	private HotelInfo hInfo;
	private CancellationChargesResponse cancellationChargesResponse;
	private BookingCancellationResponse bookingCancellationResponse;

	public void init() {
		gnComm = (GeneralServiceCommunicator) SpringContext.getApplicationContext()
				.getBean("generalServiceCommunicatorImpl");
	}

	public boolean cancelBooking() throws IOException {
		listener = new RestAPIListener("");
		HttpUtilsV2 httpUtils = null;
		try {
			TravelbullzRequest searchRequest = getCancellationChargesRequest();
			String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
			String cancellationChargeUrl =
					StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.CANCELLATION_CHARGES));
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(searchRequest), cancellationChargeUrl);
			cancellationChargesResponse = httpUtils.getResponse(CancellationChargesResponse.class).orElse(null);
			if (Objects.nonNull(cancellationChargesResponse)
					&& CollectionUtils.isEmpty(cancellationChargesResponse.getError())) {
				return getBookingCancellationStatus(cancellationChargesResponse);
			}
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(cancellationChargesResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRAVELBULLZ.name())
					.requestType(BaseHotelConstants.BOOKING_CANCELLATION).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId()).prefix("CancellationCharges")
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

		}
		log.info("Unable to cancel booking for booking id {} for supplier {} ", order.getBookingId(),
				supplierConf.getBasicInfo().getSupplierName());
		return false;
	}

	private boolean getBookingCancellationStatus(CancellationChargesResponse cancellationChargesResponse)
			throws IOException {
		HttpUtilsV2 httpUtils = null;
		try {
			String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
			HotelCancellationOption hotelCancellationOption =
					cancellationChargesResponse.getCheckHotelCancellationChargesRS().getHotelOption();
			TravelbullzRequest cancelbookingRequest = getCancelBookingRequest(hotelCancellationOption);
			String cancelBookingurl =
					StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.CANCEL_BOOKING_URL));
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(cancelbookingRequest), cancelBookingurl);
			bookingCancellationResponse = httpUtils.getResponse(BookingCancellationResponse.class).orElse(null);
			if (Objects.nonNull(bookingCancellationResponse)
					&& CollectionUtils.isEmpty(bookingCancellationResponse.getError())) {
				return checkCancellationStatus(bookingCancellationResponse.getCancelRS().getHotelOption());
			}
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(bookingCancellationResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}

			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.TRAVELBULLZ.name())
					.requestType(BaseHotelConstants.BOOKING_CANCELLATION).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
		}
		return false;
	}

	private boolean checkCancellationStatus(HotelCancellationOption hotelCancellationOption) {
		for (HotelRoom room : hotelCancellationOption.getHotelRooms()) {
			if (!(String.valueOf(room.getCancelStatus()).equals(TravelbullzConstant.SUCCESS_BOOKING_CANCEL.value)))
				return false;
		}
		log.info("Booking Successfully cancelled for bookingid {} for supplier {} ", order.getBookingId(),
				supplierConf.getBasicInfo().getSupplierName());
		return true;
	}

	private TravelbullzRequest getCancelBookingRequest(HotelCancellationOption hotelCancellationOption) {
		CancelRQ cancelRQ = CancelRQ.builder()
				.BookingDetailId(Integer.parseInt(TravelbullzConstant.BOOKING_DETAIL_ID.value))
				.BookingId(hotelCancellationOption.getBookingId())
				.CancelAll(Integer.parseInt(TravelbullzConstant.CANCEL_ALL.value))
				.CancelCode(hotelCancellationOption.getCancelCode()).Reason(getReason(order.getBookingId())).build();
		Request request = Request.builder().CancelRQ(cancelRQ).build();
		AdvancedOptions advancedOptions =
				AdvancedOptions.builder().Currency(TravelbullzConstant.CURRENCY.value).build();
		TravelbullzRequest searchRequest =
				TravelbullzRequest.builder().Token(supplierConf.getHotelSupplierCredentials().getPassword())
						.Request(request).AdvancedOptions(advancedOptions).build();

		return searchRequest;
	}

	private String getReason(String bookingId) {
		NoteFilter filter = NoteFilter.builder().bookingId(bookingId).build();
		List<Note> reasonList = gnComm.getNotes(filter);
		List<String> listmsg = new ArrayList<>();
		for (Note note : reasonList) {
			listmsg.add(note.getNoteMessage());
		}
		return CollectionUtils.isEmpty(listmsg) ? "Reason not Specified" : String.join(",", listmsg);
	}

	private TravelbullzRequest getCancellationChargesRequest() {
		CheckHotelCancellationChargesRQ chargesRQ = CheckHotelCancellationChargesRQ.builder()
				.BookingId(Integer.parseInt(hInfo.getMiscInfo().getSupplierBookingId()))
				.InternalReference(order.getBookingId()).ReferenceNo(hInfo.getMiscInfo().getSupplierBookingReference())
				.build();
		Request request = Request.builder().CheckHotelCancellationChargesRQ(chargesRQ).build();
		AdvancedOptions advancedOptions =
				AdvancedOptions.builder().Currency(TravelbullzConstant.CURRENCY.value).build();
		TravelbullzRequest searchRequest =
				TravelbullzRequest.builder().Token(supplierConf.getHotelSupplierCredentials().getPassword())
						.Request(request).AdvancedOptions(advancedOptions).build();

		return searchRequest;
	}
}
