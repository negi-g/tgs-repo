package com.tgs.services.hms.restcontroller.mapping;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.restmodel.HotelMappedSupplierCityRequest;
import com.tgs.services.hms.restmodel.HotelMappedSupplierCityResponse;
import com.tgs.services.hms.restmodel.HotelSupplierRegionRequest;
import com.tgs.services.hms.restmodel.HotelSupplierRegionResponse;

@RestController
@RequestMapping("/hms/v1/supplier/region")
public class HotelSupplierRegionInfoController {

	@Autowired
	HotelSupplierRegionInfoManager supplierRegionInfoManager;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected BaseResponse saveSupplierRegionInfoIntoDB(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelStaticDataRequest staticDataRequest) throws Exception {
		supplierRegionInfoManager.fetchAndSaveSupplierRegionInfo(staticDataRequest);
		return new BaseResponse();
	}

	@RequestMapping(value = "/savedump", method = RequestMethod.POST)
	protected BaseResponse saveSupplierRegionInfoFromDumpIntoDB(HttpServletRequest request,
			HttpServletResponse response,
			@RequestBody List<HotelSupplierRegionInfo> cityInfos) throws Exception {
		supplierRegionInfoManager.saveSupplierRegionInfo(cityInfos);
		return new BaseResponse();
	}

	@RequestMapping(value = "/fetch", method = RequestMethod.POST)
	protected HotelSupplierRegionResponse getSupplierRegionInfo(HttpServletRequest request,
			HttpServletResponse response,
			@RequestBody HotelSupplierRegionRequest supplierCityRequest) throws Exception {
		return supplierRegionInfoManager.getSupplierRegionInfo(supplierCityRequest.getSupplierRegionQuery());
	}

	@RequestMapping(value = "/fetch-mapped", method = RequestMethod.POST)
	protected HotelMappedSupplierCityResponse getMappedSupplierRegionInfo(HttpServletRequest request,
			HttpServletResponse response, @RequestBody HotelMappedSupplierCityRequest supplierCityRequest)
			throws Exception {
		return supplierRegionInfoManager.getMappedSupplierRegionInfo(supplierCityRequest.getSupplierRegionQuery());
	}
}
