package com.tgs.services.hms.servicehandler;

import java.time.LocalDateTime;

import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.manager.HotelSearchManager;
import com.tgs.services.hms.restmodel.HotelSearchRequest;


@Service
public class HotelSearchResultValidateHandler extends ServiceHandler<HotelSearchRequest, BaseResponse> {

	@Autowired
	HotelCacheHandler cacheHandler;
	
	@Autowired
	HotelSearchManager searchManager;
	
	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void process() throws Exception {
		
		String searchId = request.getSearchId();
		LocalDateTime expiryTime = cacheHandler.getSearchResultCompleteAt(searchId).plusMinutes(20);
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
		LocalDateTime currentTime = LocalDateTime.now();
		if(currentTime.plusMinutes(3).isAfter(expiryTime)) {
			/*
			 * Doing Search For All Sources
			 */
			searchManager.search(searchQuery);
			response.setRefresh(true);
		}
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
