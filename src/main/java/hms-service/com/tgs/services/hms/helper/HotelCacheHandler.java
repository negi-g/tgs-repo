package com.tgs.services.hms.helper;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.query.Filter;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelCountryInfoMapping;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.HotelUserReview;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryCacheData;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.datamodel.inventory.HotelRoomCategory;
import com.tgs.services.hms.datamodel.qtech.SupplierHotelIdMapping;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierMappingInfo;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.restmodel.HotelCacheSetRequest;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class HotelCacheHandler {

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	HotelInfoService hotelInfoService;

	private Gson SINGLETON_GSON;

	public void cacheHotelSearchResults(HotelSearchResult searchResult, HotelSearchQuery searchQuery,
			HotelSupplierConfiguration supplierConf) {
		if (false && searchResult != null && CollectionUtils.isNotEmpty(searchResult.getHotelInfos())) {
			try {
				Map<String, Object> binMap = new HashMap<>();
				binMap.put(BinName.SEARCHQUERYBIN.getName(), searchResult);

				CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL_SEARCH_RESULT.getName())
						.set(CacheSetName.HOTEL_SEARCH_RESULT.getName()).key(generateKey(searchQuery, supplierConf))
						.expiration(CacheType.HOTELSEARCHCACHING.getTtl()).binValues(binMap).kyroCompress(true).build();
				cacheService.store(metaInfo);

			} catch (Exception e) {
				log.error("Unable to store result in cache", e);
			}
		} else {
			log.info("Hotel search result is empty, hence can't be cached.");
		}
	}

	public HotelSearchResult getCachedSearchResult(HotelSearchQuery searchQuery,
			HotelSupplierConfiguration supplierConf) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL_SEARCH_RESULT.name())
				.set(CacheSetName.HOTEL_SEARCH_RESULT.getName()).key(generateKey(searchQuery, supplierConf))
				.kyroCompress(true).bins(new String[] {BinName.SEARCHQUERYBIN.getName()}).build();
		Optional<HotelSearchResult> searchResult =
				Optional.ofNullable(cacheService.fetchValue(HotelSearchResult.class, metaInfo));
		return searchResult.orElse(null);
	}

	private String generateKey(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {

		String key = "";
		key = StringUtils.join(key, searchQuery.getRoomInfo().size(), "S");
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			key = StringUtils.join(key, room.getNumberOfAdults(), "A", room.getNumberOfChild(), "C");
		}
		key = StringUtils.join(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate(), key,
				searchQuery.getSearchCriteria().getRegionId() != null ? searchQuery.getSearchCriteria().getRegionId()
						: searchQuery.getSearchCriteria().getCityName(),
				"-",
				searchQuery.getSearchCriteria().getCountryId() != null ? searchQuery.getSearchCriteria().getCountryId()
						: searchQuery.getSearchCriteria().getCountryName(),
				"-", searchQuery.getSearchPreferences().getCurrency(), "-",
				searchQuery.getSearchPreferences().getAvailableonly());
		if (searchQuery.getSearchPreferences().getRatings() != null) {
			for (Integer rating : searchQuery.getSearchPreferences().getRatings()) {
				key = StringUtils.join(key, rating);
			}
		}
		return key;
	}

	public HotelInfo getFullyCachedHotel(String id) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).keys(new String[] {id}).kyroCompress(true).build();

		Map<String, Map<String, HotelInfo>> supplierhotelIdMap = cachingCommunicator.get(metaInfo, HotelInfo.class);
		if (MapUtils.isNotEmpty(supplierhotelIdMap)) {
			Map<String, HotelInfo> binWiseHotelInfos = supplierhotelIdMap.get(id);
			if (Objects.nonNull(binWiseHotelInfos.get(BinName.HOTELDETAILS.name()))) {
				HotelInfo hotelInfo = binWiseHotelInfos.get(BinName.HOTELDETAILS.name());
				hotelInfo.populatedRepeatedInfoInHotel();
				return hotelInfo;
			}
		}
		return null;
	}

	public List<HotelInfo> getCachedHotelsById(List<String> hotelIdList) {

		List<HotelInfo> hInfoList = new ArrayList<>();
		for (String hotelId : hotelIdList) {
			HotelInfo hotelInfo = getCachedHotelById(hotelId);
			if (Objects.nonNull(hotelInfo)) {
				hInfoList.add(hotelInfo);
			}
		}
		return hInfoList;
	}

	public HotelInfo getCachedHotelById(String id) {

		HotelInfo hotelInfo = null;
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).keys(new String[] {id}).kyroCompress(true).build();

		Map<String, Map<String, HotelInfo>> supplierhotelIdMap = cachingCommunicator.get(metaInfo, HotelInfo.class);
		if (MapUtils.isNotEmpty(supplierhotelIdMap)) {
			Map<String, HotelInfo> binWiseHotelInfos = supplierhotelIdMap.get(id);

			if (Objects.nonNull(binWiseHotelInfos.get(BinName.HOTELDETAILS.name()))) {
				return binWiseHotelInfos.get(BinName.HOTELDETAILS.name());
			}

			for (Map.Entry<String, HotelInfo> binWiseHotelInfo : binWiseHotelInfos.entrySet()) {
				if (Objects.isNull(hotelInfo)) {
					hotelInfo = binWiseHotelInfo.getValue();
					hotelInfo.populatedRepeatedInfoInHotel();
				} else {
					hotelInfo.getOptions().addAll(binWiseHotelInfo.getValue().getOptions());
					hotelInfo.populatedRepeatedInfoInHotel();
				}
			}
		}
		return hotelInfo;
	}

	public HotelInfo getHotelDetailsFromCacheWithRetry(String hotelId, HotelSearchQuery searchQuery,
			Boolean isToFetchCompleteHotelInfo) throws InterruptedException {
		HotelInfo hInfo = BooleanUtils.isTrue(isToFetchCompleteHotelInfo) ? getFullyCachedHotel(hotelId)
				: getCachedHotelById(hotelId);
		if (Objects.isNull(hInfo)) {
			HotelGeneralPurposeOutput configuratorInfo =
					HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
			int attemptCount =
					Objects.isNull(configuratorInfo.getDetailRetryCount()) ? 10
							: configuratorInfo.getDetailRetryCount();
			while (Objects.isNull(hInfo) && attemptCount-- > 0) {
				Thread.sleep(1 * 1000);
				hInfo = BooleanUtils.isTrue(isToFetchCompleteHotelInfo) ? getFullyCachedHotel(hotelId)
						: getCachedHotelById(hotelId);
				log.debug(
						"Retrying to fetch hotel details from cache for hotel id {}. Attempts remaining : {}, Found : {}",
						hotelId, attemptCount, Objects.nonNull(hInfo));
			}
		}
		return hInfo;
	}

	public void persistHotelInCache(HotelInfo hotel, HotelSearchQuery searchQuery) {
		if (hotel == null) {
			log.info("Hotel Can't be cached as it is empty for searchQuery {}", searchQuery);
			return;
		}
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().expiration(CacheType.HOTELSEARCHCACHING.getTtl())
				.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_API_CACHE_SET.getName())
				.key(hotel.getId()).binValues(ImmutableMap.of(BinName.HOTELDETAILS.getName(), hotel)).kyroCompress(true)
				.build();
		cacheService.store(metaInfo);
	}

	public HotelSearchQuery getHotelSearchQueryFromCache(String key) {

		if (StringUtils.isBlank(key)) {
			return null;
		}
		CacheMetaInfo metaInfo =
				CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
						.key(key).bins(new String[] {BinName.SEARCHQUERYBIN.getName()}).compress(true).build();
		HotelSearchQuery searchQuery = cacheService.fetchValue(HotelSearchQuery.class, metaInfo);
		return searchQuery;
	}

	public void processAndStore(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		HotelSearchResult copySearchResult = new GsonMapper<>(searchResult, HotelSearchResult.class).convert();
		ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			String supplierId = searchQuery.getMiscInfo().getSupplierId();
			log.info("Inside process and store for search id {} and supplier {}", searchQuery.getSearchId(),
					supplierId);
			LogUtils.log(LogTypes.HOTEL_PROCESS_AND_STORE_IN_CACHE_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

			for (HotelInfo hotelInfo : copySearchResult.getHotelInfos()) {
				for (Option option : hotelInfo.getOptions()) {
					option.setIsPassportMandatory(HotelUtils.isPassportMandatory(option, searchQuery));
					option.setIsPanRequired(HotelUtils.isPanRequired(hotelInfo, option, searchQuery));
				}
				BaseHotelUtils.updateTotalFareComponents(hotelInfo, null);
				CacheMetaInfo metaInfo = CacheMetaInfo.builder().expiration(CacheType.HOTELSEARCHCACHING.getTtl())
						.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_API_CACHE_SET.getName())
						.key(hotelInfo.getId())
						.binValues(ImmutableMap.of(searchQuery.getMiscInfo().getSupplierId(), hotelInfo))
						.kyroCompress(true).build();
				cacheService.store(metaInfo);
			}
			LogUtils.log(LogTypes.HOTEL_PROCESS_AND_STORE_IN_CACHE_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.supplierId(supplierId).trips(searchResult.getNoOfHotelOptions()).build(),
					LogTypes.HOTEL_PROCESS_AND_STORE_IN_CACHE_PROCESS_START);
		});
	}

	public void storeSearchResult(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {
		log.debug("No of results found for supplier {} are {}", searchQuery.getMiscInfo().getSupplierId(),
				searchResult.getHotelInfos().size());
		if (searchResult.getHotelInfos().size() == 0) {
			return;
		}

		try {
			CacheMetaInfo metaInfoToStore =
					CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL_SEARCH_RESULT.getName())
							.set(CacheSetName.HOTEL_SEARCH_RESULT.getName()).key(searchQuery.getSearchId())
							.kyroCompress(true).expiration(CacheType.HOTELSEARCHCACHING.getTtl())
							.binValues(ImmutableMap.of(BinName.HOTELRESULTS.getName(), searchResult)).build();

			int n = getHotelInfoSizeLimit();
			if (searchResult.getHotelInfos().size() > n) {
				HotelSearchResult newSearchResult = HotelSearchResult.builder().build();
				ExecutorService storeSearchResultExecutor = Executors.newFixedThreadPool(4);
				try {
					List<List<HotelInfo>> smallerLists = TgsCollectionUtils.chunkList(searchResult.getHotelInfos(), n);
					metaInfoToStore.setBinValues(ImmutableMap.of(BinName.HOTELRESULTS.getName(), newSearchResult));
					cacheService.store(metaInfoToStore);
					AtomicInteger searchIdSuffix = new AtomicInteger(0);
					List<Future<?>> storeSearchResultTasks = new ArrayList<Future<?>>();
					for (List<HotelInfo> hInfos : smallerLists) {
						Future<?> storeSearchResultTask = storeSearchResultExecutor.submit(() -> {
							HotelSearchResult searchResultInChunk =
									new GsonMapper<>(newSearchResult, HotelSearchResult.class).convert();
							searchResultInChunk.setHotelInfos(hInfos);
							metaInfoToStore.setKey(searchQuery.getSearchId() + searchIdSuffix.get())
									.setBinValues(ImmutableMap.of(BinName.HOTELRESULTS.getName(), searchResultInChunk));
							searchIdSuffix.getAndIncrement();
							cacheService.store(metaInfoToStore);
						});
						storeSearchResultTasks.add(storeSearchResultTask);
					}

					try {
						for (Future<?> storeSearchResultTask : storeSearchResultTasks)
							storeSearchResultTask.get();
					} catch (InterruptedException | ExecutionException e) {
						log.error("Interrupted exception while persisting search result in cache for search query {}"
								+ GsonUtils.getGson().toJson(searchQuery), e);
					}
				} catch (AerospikeException e) {
					Map<String, Object> basicInfoToLog = HotelUtils.getHotelBasisInfo(newSearchResult, searchQuery);
					log.error("Unable to store search result for search query {} and hotel info {}",
							GsonUtils.getGson().toJson(searchQuery), basicInfoToLog, e);
				}
			} else {
				cacheService.store(metaInfoToStore);
			}
		} catch (AerospikeException e) {
			Map<String, Object> basicInfoToLog = HotelUtils.getHotelBasisInfo(searchResult, searchQuery);
			log.error("Unable to store search result for search query {} and hotel info {} in cache",
					GsonUtils.getGson().toJson(searchQuery), basicInfoToLog, e);
		}
	}

	private Integer getHotelInfoSizeLimit() {

		int sizeLimit = 1000;
		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
		if (configuratorInfo != null) {
			sizeLimit = configuratorInfo.getHotelListSizeLimit();
		}
		return sizeLimit;
	}

	public HotelSearchResult getSearchResult(String searchId) {

		log.info("Fetching search result from cache for search id {}, search result size is {}", searchId);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL_SEARCH_RESULT.name())
				.set(CacheSetName.HOTEL_SEARCH_RESULT.getName()).key(searchId).kyroCompress(true)
				.bins(new String[] {BinName.HOTELRESULTS.getName()}).build();

		HotelSearchResult searchResult = cacheService.fetchValue(HotelSearchResult.class, metaInfo);

		if (searchResult != null && searchResult.getHotelInfos().isEmpty()) {

			List<String> keys = new ArrayList<>();
			for (int i = 0; i < 10; i++) {
				keys.add(searchId + i);
			}
			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL_SEARCH_RESULT.name())
					.set(CacheSetName.HOTEL_SEARCH_RESULT.getName()).keys(keys.toArray(new String[0]))
					.kyroCompress(true).bins(new String[] {BinName.HOTELRESULTS.getName()}).build();

			Map<String, Map<String, HotelSearchResult>> hotelSearchResultMap =
					cachingCommunicator.get(metaInfo, HotelSearchResult.class);

			for (String key : keys) {
				Map<String, HotelSearchResult> hotelSearchResult = hotelSearchResultMap.get(key);
				if (hotelSearchResult == null) {
					break;
				} else {
					searchResult.getHotelInfos()
							.addAll(hotelSearchResult.get(BinName.HOTELRESULTS.getName()).getHotelInfos());
					searchResult.populatedRepeatedInfoInSearchResult();
				}
			}
		}
		log.info("Fetched search result from cache for search id {}, search result size {}", searchId,
				searchResult == null ? "no result" : searchResult.getNoOfHotelOptions());
		return searchResult;
	}


	public HotelUserReview getHotelUserReviewById(String locationId) {

		if (StringUtils.isBlank(locationId)) {
			return null;
		}
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(locationId)
				.bins(new String[] {BinName.HOTELRATING.getName()}).kyroCompress(true).build();
		HotelUserReview hReview = cacheService.fetchValue(HotelUserReview.class, metaInfo);
		return hReview;
	}

	public void cacheHotelUserReview(HotelUserReview hReview) {

		if (hReview == null) {
			return;
		}

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(hReview.getId())
				.expiration(CacheType.HOTELRATINGCACHING.getTtl())
				.binValues(ImmutableMap.of(BinName.HOTELRATING.getName(), hReview)).kyroCompress(true).build();
		cacheService.store(metaInfo);

	}

	public void storeSearchSpiltCount(String key, Integer count) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(key).expiration(300)
				.binValues(ImmutableMap.of(BinName.SPLITCOUNT.getName(), count)).build();
		cacheService.store(metaInfo);
	}

	public int getSearchSplitCount(String key) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(key)
				.bins(new String[] {BinName.SPLITCOUNT.getName()}).build();
		Integer searchcount = cacheService.fetchValue(Integer.class, metaInfo);
		return ObjectUtils.firstNonNull(searchcount, 0);
	}

	public void storeSearchCountReturned(String key, Integer count) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(key).expiration(300)
				.binValues(ImmutableMap.of(BinName.SEARCHCOUNT.getName(), count)).build();
		cacheService.store(metaInfo);
	}

	public int getSearchCountReturned(String key) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(key)
				.bins(new String[] {BinName.SEARCHCOUNT.getName()}).build();
		Integer searchcount = cacheService.fetchValue(Integer.class, metaInfo);
		return ObjectUtils.firstNonNull(searchcount, 0);
	}

	public String getLocationId(String id, String binName) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_USER_REVIEW.getName()).key(id).bins(new String[] {binName}).kyroCompress(true)
				.build();
		String reviewId = cacheService.fetchValue(String.class, metaInfo);
		return reviewId;
	}

	public HotelSupplierRegionInfo getRegionInfoMappingFromRegionId(String regionId, String sourceName) {
		if (regionId == null)
			return null;

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.REGION_MAPPING.getName()).key(regionId).bins(new String[] {sourceName}).build();
		String cachedRegionMappingJSON = cacheService.fetchValue(String.class, metaInfo);
		HotelSupplierRegionInfo supplierRegionInfo =
				GsonUtils.getGson().fromJson(cachedRegionMappingJSON, HotelSupplierRegionInfo.class);
		return supplierRegionInfo;
	}

	public HotelRegionInfo getRegionInfoFromRegionId(String tgsCityId) {

		if (StringUtils.isBlank(tgsCityId))
			return null;
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.REGION_MAPPING.getName()).key(tgsCityId)
				.bins(new String[] {BinName.REGION_INFO.name()}).build();
		String regionInfoJSON = cacheService.fetchValue(String.class, metaInfo);
		HotelRegionInfo regionInfo = GsonUtils.getGson().fromJson(regionInfoJSON, HotelRegionInfo.class);
		return regionInfo;
	}

	public void storeSearchQueryInCache(HotelSearchQuery query) {

		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(BinName.SEARCHQUERYBIN.getName(), query);
		binMap.put(BinName.STOREAT.getName(), LocalDateTime.now().toString());
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().expiration(CacheType.SEARCHQUERYBINCACHING.getTtl())
				.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
				.key(query.getSearchId()).binValues(binMap).compress(true).build();
		cacheService.store(metaInfo);

	}

	public void cacheSearchResultCompleteAt(HotelSearchQuery searchQuery) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.expiration(CacheType.HOTELSEARCHCACHING.getTtl()).set(CacheSetName.HOTEL_SEARCH.getName())
				.key(searchQuery.getSearchId())
				.binValues(ImmutableMap.of(BinName.COMPLETEAT.getName(), LocalDateTime.now().toString())).compress(true)
				.build();
		cacheService.store(metaInfo);
	}

	public LocalDateTime getSearchResultCompleteAt(String searchId) {

		CacheMetaInfo metaInfo =
				CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
						.key(searchId).bins(new String[] {BinName.COMPLETEAT.getName()}).compress(true).build();

		String completeAt = cacheService.fetchValue(String.class, metaInfo);
		return LocalDateTime.parse(completeAt);
	}

	public boolean isSearchCompleted(String searchId) {
		int splitCount = getSearchSplitCount(searchId);
		if (splitCount > 0) {
			return false;
		}
		return true;

	}

	public void removeSearchResult(HotelSearchQuery searchQuery) {

		CacheMetaInfo metaInfoToDelete = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(searchQuery.getSearchId()).build();
		cacheService.delete(metaInfoToDelete);

	}

	public void cacheHotelReviewAgainstBookingId(HotelReviewResponse reviewResponse) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).key(reviewResponse.getBookingId())
				.binValues(ImmutableMap.of(BinName.HOTELINFO.getName(), reviewResponse)).compress(false).build();
		cacheService.store(metaInfo);
	}

	public HotelReviewResponse getHotelReviewFromCache(String bookingId) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).bins(new String[] {BinName.HOTELINFO.getName()})
				.typeOfT(HotelReviewResponse.class).key(bookingId).compress(false).build();
		return cacheService.fetchValue(HotelReviewResponse.class, metaInfo);

	}

	public void storeCrossSellSearchQueryInCache(HotelSearchQuery searchQuery) {
		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(BinName.SEARCHQUERYBIN.getName(), searchQuery);
		binMap.put(BinName.STOREAT.getName(), LocalDateTime.now().toString());
		int expirySeconds =
				(int) Duration.between(LocalDateTime.now(), searchQuery.getCheckoutDate().atStartOfDay()).getSeconds();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().expiration(expirySeconds)
				.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
				.key(searchQuery.getSearchId()).binValues(binMap).compress(true).build();
		cacheService.store(metaInfo);
	}

	public void storeSearchQueryAndStartAt(HotelSearchQuery query) {

		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(BinName.SEARCHQUERYBIN.getName(), query);
		binMap.put(BinName.STOREAT.getName(), LocalDateTime.now().toString());
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().expiration(CacheType.HOTELSEARCHCACHING.getTtl())
				.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
				.key(query.getSearchId()).binValues(binMap).compress(true).build();
		cacheService.store(metaInfo);

	}


	public LocalDateTime getSearchStartAt(String searchId) {

		CacheMetaInfo metaInfo =
				CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
						.key(searchId).bins(new String[] {BinName.STOREAT.getName()}).compress(true).build();

		String startAt = cacheService.fetchValue(String.class, metaInfo);
		log.info("StartAt is {}", startAt);
		return LocalDateTime.parse(startAt);

	}


	public void storeUserReviewIdWithKey(String key, String userReviewId) {

		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(BinName.USERREVIEWID.getName(), userReviewId);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_USER_REVIEW.getName()).key(key).binValues(binMap).kyroCompress(true).build();
		cacheService.store(metaInfo);

	}

	public Map<String, String> getKeyHotelUserReviewSupplierIdMap(List<String> keyList) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_USER_REVIEW.getName()).keys(keyList.toArray(new String[0]))
				.bins(new String[] {BinName.USERREVIEWID.getName()}).kyroCompress(true).build();
		Map<String, String> tempMap = new HashMap<>();
		Map<String, Map<String, String>> userReviewSupplierIdMap = cachingCommunicator.get(metaInfo, String.class);
		for (String key : userReviewSupplierIdMap.keySet()) {
			Map<String, String> map = userReviewSupplierIdMap.get(key);
			String userReviewSupplierId = map.get("USERREVIEWID");
			tempMap.put(key, userReviewSupplierId);
		}
		return tempMap;
	}


	public void storeBlacklistedUserReviewHotelDetail(String key) {

		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(BinName.UNMAPPEDUR.getName(), key);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_USER_REVIEW.getName()).key(key).binValues(binMap).kyroCompress(true).build();
		cacheService.store(metaInfo);

	}

	public HotelRegionInfo getRegionInfoFromIATACode(String iataCode) {
		if (StringUtils.isBlank(iataCode))
			return null;
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.REGION_MAPPING.getName()).bins(new String[] {BinName.REGION_INFO.name()}).build();

		Map<String, Map<String, String>> regionIataMappings = cachingCommunicator.getResultSet(metaInfo, String.class,
				Filter.equal(BinName.IATA.getName(), "\"" + iataCode + "\""));
		for (Map.Entry<String, Map<String, String>> regionIataMapping : regionIataMappings.entrySet()) {
			Map<String, String> regionInfoMap = regionIataMapping.getValue();
			HotelRegionInfo regionInfo =
					GsonUtils.getGson().fromJson(regionInfoMap.get(BinName.REGION_INFO.name()), HotelRegionInfo.class);
			return regionInfo;
		}
		return null;
	}

	public String getBlacklistedUserReviewHotelDetail(String key) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_USER_REVIEW.getName()).key(key)
				.bins(new String[] {BinName.UNMAPPEDUR.getName()}).kyroCompress(true).build();

		String value = cacheService.fetchValue(String.class, metaInfo);
		return value;
	}

	public Map<String, String> fetchMealInfoFromAerospike(HotelSearchQuery searchQuery,
			Set<String> supplierMealInfoIds) {
		Map<String, String> mealInfos = new HashMap<>();
		if (CollectionUtils.isEmpty(supplierMealInfoIds)) {
			return mealInfos;
		}
		// Max batch request limit of aerospike is 5000
		int partitionSize = 4000;
		String binName = BinName.MEALBASIS.getName();
		Set<String> supplierMealKeys =
				supplierMealInfoIds.stream().map(String::toUpperCase).collect(Collectors.toSet());
		try {
			mealInfos = fetchAggregatedChunkedMealFetch(supplierMealKeys, binName, partitionSize);
		} catch (Exception e) {
			log.debug("Unable to find meal mapping for searchQuery {}", searchQuery, e);
		}
		return mealInfos;
	}

	private Map<String, String> fetchAggregatedChunkedMealFetch(Set<String> supplierMealKeys, String binName,
			int partitionSize) {
		Map<String, String> mealInfos = new HashMap<>();
		List<Set<String>> partitionedSupplierMealKeys = HotelUtils.partitionSet(supplierMealKeys, partitionSize);
		partitionedSupplierMealKeys.stream().forEach(mealKeys -> {
			CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
					.set(CacheSetName.HOTEL_MEAL_BASIS.getName()).keys(supplierMealKeys.toArray(new String[0]))
					.bins(new String[] {binName}).build();
			Map<String, Map<String, String>> supplierhotelIdMap =
					cachingCommunicator.get(supplierMappingMetaInfo, String.class);
			for (Map.Entry<String, Map<String, String>> supplierhotelIdMapEntry : supplierhotelIdMap.entrySet()) {
				mealInfos.put(supplierhotelIdMapEntry.getKey(), supplierhotelIdMapEntry.getValue().get(binName));
			}
		});
		return mealInfos;
	}


	public void updateInventoryRatePlans(List<HotelRatePlan> ratePlanList) {

		Map<String, List<HotelRatePlan>> supplierHotelIdDateSupplierIdKeyRatePlanListValue =
				HotelUtils.getSupplierHotelIdRatePlanListValue(ratePlanList);
		List<String> keys = new ArrayList<>(supplierHotelIdDateSupplierIdKeyRatePlanListValue.keySet());
		List<HotelInventoryCacheData> cachedInventoryData = getHotelInventoryCachedDatafromKeys(keys);

		if (CollectionUtils.isNotEmpty(cachedInventoryData)) {
			List<HotelRatePlan> cachedRatePlanList = new ArrayList<>();
			cachedInventoryData.forEach(invData -> cachedRatePlanList.addAll(invData.getRatePlanList()));
			Map<String, List<HotelRatePlan>> cachedRatePlanMap =
					cachedRatePlanList.stream().collect(Collectors.groupingBy(ratePlan -> ratePlan.getSupplierHotelId()
							+ String.valueOf(ratePlan.getValidOn()) + ratePlan.getSupplierId()));

			supplierHotelIdDateSupplierIdKeyRatePlanListValue.keySet().forEach((key -> {

				List<HotelRatePlan> cachedListForKey = cachedRatePlanMap.get(key);
				if (CollectionUtils.isEmpty(cachedListForKey)) {
					storeInventoryRatePlansForKey(key, supplierHotelIdDateSupplierIdKeyRatePlanListValue.get(key));
				} else {

					Map<Long, HotelRatePlan> cachedMap = cachedListForKey.stream()
							.collect(Collectors.toMap(HotelRatePlan::getId, Function.identity()));
					List<HotelRatePlan> ratePlans = supplierHotelIdDateSupplierIdKeyRatePlanListValue.get(key);
					ratePlans.forEach(ratePlan -> {
						cachedMap.put(ratePlan.getId(), ratePlan);
					});
					List<HotelRatePlan> finalRatePlanList = new ArrayList<>();
					cachedMap.keySet().forEach(mapKey -> {
						finalRatePlanList.add(cachedMap.get(mapKey));
					});
					storeInventoryRatePlansForKey(key, finalRatePlanList);
				}
			}));

		} else {
			supplierHotelIdDateSupplierIdKeyRatePlanListValue.keySet().forEach(key -> {
				storeInventoryRatePlansForKey(key, supplierHotelIdDateSupplierIdKeyRatePlanListValue.get(key));
			});
		}
	}

	public void deleteRatePlanFromCache(HotelRatePlan ratePlan) {

		String key = HotelUtils.getRatePlanCachingKey(ratePlan);
		List<HotelInventoryCacheData> cachedInventoryData =
				getHotelInventoryCachedDatafromKeys(new ArrayList<>(Arrays.asList(key)));
		List<HotelRatePlan> cachedRatePlanList = new ArrayList<>();
		cachedInventoryData.forEach(invData -> cachedRatePlanList.addAll(invData.getRatePlanList()));
		List<HotelRatePlan> updatedRatePlanList =
				cachedRatePlanList.stream().filter(rp -> rp.getId() != ratePlan.getId()).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(updatedRatePlanList)) {
			deleteKeyFromCache(key);
			return;
		}
		storeInventoryRatePlansForKey(key, updatedRatePlanList);

	}

	private void deleteKeyFromCache(String key) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_INVENTORY.getName()).key(key).build();
		cacheService.delete(metaInfo);

	}

	public void storeInventoryRatePlansForKey(String key, List<HotelRatePlan> ratePlanList) {

		String supplierHotelId = ratePlanList.get(0).getSupplierHotelId();
		LocalDate validOn = ratePlanList.get(0).getValidOn();
		String supplierId = ratePlanList.get(0).getSupplierId();
		Map<String, Object> binValues = new HashMap<>();
		HotelInventoryCacheData data = HotelInventoryCacheData.builder().ratePlanList(ratePlanList)
				.supplierHotelId(supplierHotelId).validOn(validOn).supplierId(supplierId).build();
		binValues.put(BinName.H_RATEPLAN.name(), GsonUtils.getGson().toJson(data));
		CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_INVENTORY.getName()).key(key).expiration(-1).binValues(binValues).build();
		cacheService.store(metaInfoToStore);

	}

	public List<HotelInventoryCacheData> getHotelInventoryCachedDatafromKeys(List<String> keys) {

		List<HotelInventoryCacheData> hotelInventoryCacheDataList = new ArrayList<>();

		CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_INVENTORY.getName()).keys(keys.toArray(new String[0]))
				.bins(new String[] {BinName.H_RATEPLAN.name()}).build();
		Map<String, Map<String, String>> hotelInventoryCacheMap =
				cachingCommunicator.get(supplierMappingMetaInfo, String.class);

		for (Map.Entry<String, Map<String, String>> hotelInventoryCacheMapEntry : hotelInventoryCacheMap.entrySet()) {
			hotelInventoryCacheDataList.add(
					GsonUtils.getGson().fromJson(hotelInventoryCacheMapEntry.getValue().get(BinName.H_RATEPLAN.name()),
							HotelInventoryCacheData.class));
		}
		return hotelInventoryCacheDataList;
	}

	public void storeRoomCategoryList(List<HotelRoomCategory> rcList) {

		rcList.forEach(rcInfo -> {
			Long key = rcInfo.getId();
			Map<String, Object> binValues = new HashMap<>();
			binValues.put(BinName.ROOMCATEGORY.name(), rcInfo.getRoomCategory());
			CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
					.set(CacheSetName.HOTEL_INVENTORY.getName()).key(String.valueOf(key)).expiration(-1)
					.binValues(binValues).build();
			cacheService.store(metaInfoToStore);
		});
	}

	public void storeSupplierMappingInCache(HotelSupplierMappingInfo supplierMapping) {

		DbHotelInfo hotelInfo = hotelInfoService.findById(supplierMapping.getHotelId());
		storeSupplierMappingInCache(supplierMapping, hotelInfo);
	}

	public void storeSupplierMappingInCache(HotelSupplierMappingInfo supplierMapping, DbHotelInfo hotelInfo) {

		try {
			String cityName = "";
			String countryName = "";
			String starRating = "";
			String stateName = "";
			if (!Objects.isNull(hotelInfo)) {
				cityName = hotelInfo.getAddress().getCity().getName().toLowerCase();
				if (!Objects.isNull(hotelInfo.getAddress().getState())
						&& StringUtils.isNotBlank(hotelInfo.getAddress().getState().getName()))
					stateName = hotelInfo.getAddress().getState().getName().toLowerCase();

				if (!Objects.isNull(hotelInfo.getAddress().getCountry()))
					countryName = hotelInfo.getAddress().getCountry().getName().toLowerCase();
				starRating = hotelInfo.getRating();
			}

			String binName = HotelUtils.getSupplierBinName(supplierMapping.getSourceName());
			String setName = HotelUtils.getSupplierSetName(supplierMapping.getSourceName());
			Map<String, Object> binValues = new HashMap<>();
			binValues.put(binName, supplierMapping.getHotelId());
			binValues.put(BinName.CITY.name(), cityName);
			binValues.put(BinName.COUNTRY.name(), countryName);
			binValues.put(BinName.RATING.name(), starRating);
			if (!Objects.isNull(hotelInfo)) {
				binValues.put(BinName.HOTELNAME.name(), hotelInfo.getName());
			}
			binValues.put(BinName.STATE.name(), stateName);
			CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
					.set(setName).key(String.valueOf(supplierMapping.getSupplierHotelId())).expiration(-1)
					.binValues(binValues).build();
			cacheService.store(metaInfoToStore);
		} catch (Exception e) {
			log.error("Unable to save hotel static data mapping into aerospike", e);
		}
	}

	public void storeMasterHotel(HotelInfo basicHotelInfo, HotelInfo detailHotelInfo,
			HotelStaticDataService staticDataService) {
		//
		// if (Objects.nonNull(hotelInfo.getAdditionalInfo())
		// && BooleanUtils.isTrue(hotelInfo.getAdditionalInfo().getIsDisabled())) {
		// log.debug("Unable to persist hotel info into cache as the hotel is deleted. Id {}", hotelInfo.getId());
		// return;
		// }
		storeSearchInfoOfHotel(basicHotelInfo, staticDataService);
		storeStaticInfoOfHotel(basicHotelInfo, detailHotelInfo, staticDataService);
	}

	private void storeStaticInfoOfHotel(HotelInfo basicHotelInfo, HotelInfo detailHotelInfo,
			HotelStaticDataService staticDataService) {

		CacheMetaInfo metaInfoToStoreDetail = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_STATIC_INFO.getName())
				.key(staticDataService.getHotelInfoUniqueKey(basicHotelInfo))
				.expiration(-1).binValues(ImmutableMap.of(BinName.HOTEL_INFO.getName(), basicHotelInfo,
						BinName.HOTEL_DETAIL.getName(), detailHotelInfo))
				.kyroCompress(true).build();
		cacheService.store(metaInfoToStoreDetail);
	}

	private void storeSearchInfoOfHotel(HotelInfo hotelInfo, HotelStaticDataService staticDataService) {
		// if (Objects.nonNull(hotelInfo.getAdditionalInfo())
		// && BooleanUtils.isTrue(hotelInfo.getAdditionalInfo().getIsDisabled())) {
		// log.debug("Unable to persist hotel info into cache as the hotel is deleted. Id {}", hotelInfo.getId());
		// return;
		// }

		String cityName = hotelInfo.getAddress().getCity().getName().toLowerCase();
		String supplierName = hotelInfo.getSupplierName();
		Map<String, Object> searchHotelBinValues = new HashMap<>();
		searchHotelBinValues.put(BinName.CITY.name(), staticDataService.getCityName(supplierName, cityName));
		if (Objects.nonNull(hotelInfo.getRating())) {
			searchHotelBinValues.put(BinName.RATING.name(), String.valueOf(hotelInfo.getRating()));
		}

		if (Objects.nonNull(hotelInfo.getAdditionalInfo())
				&& Objects.nonNull(hotelInfo.getAdditionalInfo().getOpinionatedContent())
				&& Objects.nonNull(hotelInfo.getAdditionalInfo().getOpinionatedContent().getGeolocation())) {
			GeoLocation geoLocation = hotelInfo.getAdditionalInfo().getOpinionatedContent().getGeolocation();
			searchHotelBinValues.put(BinName.GEOLOCATION.name(), HotelUtils.getGeoJSONValue("point",
					Arrays.asList(geoLocation.getLongitude(), geoLocation.getLatitude())));
		}

		if (Objects.nonNull(hotelInfo.getAddress().getCountry())
				&& StringUtils.isNotBlank(hotelInfo.getAddress().getCountry().getName())) {
			String countryName = hotelInfo.getAddress().getCountry().getName().toLowerCase();
			searchHotelBinValues.put(BinName.COUNTRY.name(),
					staticDataService.getCountryName(supplierName, countryName));
		}

		if (Objects.nonNull(hotelInfo.getAddress().getState())
				&& StringUtils.isNotBlank(hotelInfo.getAddress().getState().getName())) {
			String stateName = hotelInfo.getAddress().getState().getName().toLowerCase();
			searchHotelBinValues.put(BinName.STATE.name(), staticDataService.getStateName(supplierName, stateName));
		}

		if (Objects.nonNull(hotelInfo.getUnicaId()) && StringUtils.isNotBlank(hotelInfo.getUnicaId())) {
			searchHotelBinValues.put(BinName.HOTELID.name(), hotelInfo.getUnicaId());
		}

		CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(staticDataService.getSupplierSetName(supplierName))
				.key(staticDataService.getHotelInfoUniqueKey(hotelInfo)).expiration(-1).binValues(searchHotelBinValues)
				.plainData(true).build();
		cacheService.store(metaInfoToStore);
	}

	public boolean deleteMasterHotel(HotelInfo hotelInfo, String supplierName,
			HotelStaticDataService staticDataService) {

		try {
			cacheService.delete(CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
					.set(CacheSetName.HOTEL_STATIC_INFO.getName())
					.key(staticDataService.getHotelInfoUniqueKey(hotelInfo)).build());

			cacheService.delete(CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
					.set(staticDataService.getSupplierSetName(supplierName))
					.key(staticDataService.getHotelInfoUniqueKey(hotelInfo)).build());
		} catch (Exception e) {
			log.info("Unable to delete record from cache for key {}",
					staticDataService.getHotelInfoUniqueKey(hotelInfo), e.getMessage());
			return false;
		}
		return true;
	}

	private Gson getSingletonGson() {

		if (SINGLETON_GSON == null) {
			SINGLETON_GSON = GsonUtils.getGson();
		}
		return SINGLETON_GSON;
	}

	public void updateRegionInfo(HotelRegionInfo regionInfo) {
		Map<String, Object> regionInfoBinMap = new HashMap<>();
		regionInfoBinMap.put(BinName.REGION_INFO.name(), GsonUtils.getGson().toJson(regionInfo));
		CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.REGION_MAPPING.getName()).key(String.valueOf(regionInfo.getId())).expiration(-1)
				.binValues(regionInfoBinMap).build();
		cacheService.store(metaInfoToStore);
	}

	public HotelCountryInfoMapping getSupplierCountryInfoFromCountryId(String tgsCountryId, String sourceName) {
		if (StringUtils.isBlank(tgsCountryId))
			return null;

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.NATIONALITY.getName()).key(tgsCountryId).bins(new String[] {sourceName}).build();
		String supplierCountryMappingJson = cacheService.fetchValue(String.class, metaInfo);
		HotelCountryInfoMapping supplierCountryMapping =
				GsonUtils.getGson().fromJson(supplierCountryMappingJson, HotelCountryInfoMapping.class);
		return supplierCountryMapping;
	}

	public void storeHotelIdMapping(String hotelId, SupplierHotelIdMapping hotelIdMappingInfo) {
		Map<String, Object> binValues = new HashMap<>();
		binValues.put(BinName.SUPHOTELINFO.name(), getSingletonGson().toJson(hotelIdMappingInfo));
		CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.SUPPLIER_HOTEL_ID_MAPPING.name()).key(hotelId).expiration(-1).binValues(binValues)
				.build();
		cacheService.store(metaInfoToStore);
	}

	public List getUncompressedDataSet(HotelCacheSetRequest hotelCacheSetRequest) {
		List cacheDataList = new ArrayList<>();
		Class<?> classType = null;
		if (Objects.nonNull(hotelCacheSetRequest.getClassType())) {
			try {
				classType = Class.forName(hotelCacheSetRequest.getClassType());
			} catch (ClassNotFoundException e) {
				log.info("Invalid class name for request {}", new Gson().toJson(hotelCacheSetRequest));
				return null;
			}
		} else {
			classType = Object.class;
		}
		Object dataSet = null;
		for (String key : hotelCacheSetRequest.getKeys()) {
			Map<String, Object> dataMap = new HashMap<>();
			dataMap.put("PK", key);
			for (String bin : hotelCacheSetRequest.getBins()) {
				dataSet = cacheService.fetchValue(classType,
						CacheMetaInfo.builder().key(key).namespace(hotelCacheSetRequest.getNamespace())
								.compress(hotelCacheSetRequest.getCompress())
								.kyroCompress(hotelCacheSetRequest.getKyroCompress()).set(hotelCacheSetRequest.getSet())
								.bins(new String[] {bin}).build());
				dataMap.put(bin, dataSet);
			}
			CollectionUtils.addIgnoreNull(cacheDataList, dataMap);
		}

		return cacheDataList;
	}
}
