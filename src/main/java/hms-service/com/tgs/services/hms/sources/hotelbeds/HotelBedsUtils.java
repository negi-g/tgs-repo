package com.tgs.services.hms.sources.hotelbeds;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsBaseRequest;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HotelBedsUtils {

	@SuppressWarnings("unchecked")
	public static <T> HttpUtilsV2 getHttpUtils(HotelBedsBaseRequest request, HotelBedsStaticDataRequest staticRequest,
			HotelSupplierConfiguration supplierConf, String url) {

		String endPoint = supplierConf.getHotelSupplierCredentials().getUrl();
		String requestUrl = StringUtils.join(endPoint, url);
		Map<String, String> queryParams = null;
		if (staticRequest != null) {
			queryParams = convertToMap(staticRequest);
			MultiMap recurringQueryParams = new MultiValueMap();
			recurringQueryParams.putAll(queryParams);
			HttpUtilsV2 httpUtils = HttpUtilsV2.builder().urlString(requestUrl)
					.headerParams(getHeaderParams(supplierConf)).recurringQueryParams(recurringQueryParams)
					.requestMethod(HttpUtilsV2.REQ_METHOD_GET).timeout(90 * 1000).build();
			return httpUtils;
		}
		HttpUtilsV2 httpUtils = HttpUtilsV2.builder().urlString(requestUrl).headerParams(getHeaderParams(supplierConf))
				.postData(GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(request))
				.requestMethod(HttpUtilsV2.REQ_METHOD_POST).timeout(90 * 1000).build();
		return httpUtils;
	}

	public static Map<String, String> getHeaderParams(HotelSupplierConfiguration supplierConf) {

		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("content-type", "application/json");
		headerParams.put("Accept-Encoding", "gzip, deflate");
		headerParams.put("Api-key", supplierConf.getHotelSupplierCredentials().getClientId());
		String apiKey = supplierConf.getHotelSupplierCredentials().getClientId();
		String secretKey = supplierConf.getHotelSupplierCredentials().getPassword();
		long timestamp = System.currentTimeMillis() / 1000;
		headerParams.put("X-Signature", getSignature(apiKey, secretKey, timestamp));
		return headerParams;
	}

	private static String getSignature(String apiKey, String secretKey, long timestamp) {

		String signature = StringUtils.join(apiKey, secretKey, timestamp);
		return hash256(signature);
	}

	public static String hash256(String data) {

		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(data.getBytes());
		} catch (Exception e) {
			log.error("Error while creating instance of SHA-256");
		}
		return bytesToHex(md.digest());
	}

	public static String bytesToHex(byte[] bytes) {

		StringBuffer result = new StringBuffer();
		for (byte byt : bytes)
			result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
		return result.toString();
	}

	private static <T> Map<String, String> convertToMap(T obj) {
		return new Gson().fromJson(
				GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(obj),
				new TypeToken<HashMap<String, String>>() {}.getType());
	}

	public static HttpUtilsV2 getPostBookingHttpUtils(String supplierBookingReference,
			HotelSupplierConfiguration supplierConf, String url, String requestMethod) {

		String endPoint = supplierConf.getHotelSupplierCredentials().getUrl();
		String requestUrl = StringUtils.join(endPoint, url, "/", supplierBookingReference);
		HttpUtilsV2 httpUtils = HttpUtilsV2.builder().urlString(requestUrl).headerParams(getHeaderParams(supplierConf))
				.requestMethod(requestMethod).timeout(90 * 1000).build();
		return httpUtils;
	}

}
