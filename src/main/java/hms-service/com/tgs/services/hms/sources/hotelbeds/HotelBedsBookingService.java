package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelBeds.BookRequestRoom;
import com.tgs.services.hms.datamodel.hotelBeds.Holder;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedPaxType;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsBookRequest;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsBookResponse;
import com.tgs.services.hms.datamodel.hotelBeds.Paxes;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class HotelBedsBookingService extends HotelBedsBaseService {

	private Order order;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelInfo hInfo;
	private HotelOrderItemCommunicator itemComm;
	HotelBedsBookResponse bookingResponse;

	public boolean book() throws IOException, JAXBException, InterruptedException {
		HttpUtilsV2 httpUtils = null;
		boolean isBookingSuccessful = false;
		try {
			listener = new RestAPIListener("");
			HotelBedsBookRequest bookingRequest = getBookingRequest(hInfo.getOptions().get(0));
			httpUtils = HotelBedsUtils.getHttpUtils(bookingRequest, null, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.BOOK_URL));
			bookingResponse = httpUtils.getResponse(HotelBedsBookResponse.class).orElse(null);
			log.info("Response for booking for request {} is {}", GsonUtils.getGson().toJson(bookingRequest),
					GsonUtils.getGson().toJson(bookingResponse));

			if (bookingResponse.getBooking() != null && ObjectUtils.isEmpty(bookingResponse.getError())) {
				isBookingSuccessful = updateBookingStatus(bookingResponse);
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.BOOK.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(bookingResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.HOTELBEDS.name())
						.requestType(BaseHotelConstants.BOOKING).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(order.getBookingId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
		return isBookingSuccessful;
	}

	private boolean updateBookingStatus(HotelBedsBookResponse bookingResponse) {

		hInfo.getMiscInfo().setSupplierBookingReference(bookingResponse.getBooking().getReference());
		hInfo.getMiscInfo().setSupplierBookingId(bookingResponse.getBooking().getReference());
		return bookingResponse.getBooking().getStatus().equals("CONFIRMED");
	}

	private HotelBedsBookRequest getBookingRequest(Option option) {

		List<BookRequestRoom> rooms = new ArrayList<>();
		double tolerance = sourceConfigOutput.getTolerance();
		int roomIndex = 1;
		option.getRoomInfos().forEach(rInfo -> {
			BookRequestRoom room = new BookRequestRoom();
			room.setRateKey(rInfo.getMiscInfo().getRatePlanCode());
			List<Paxes> paxes = new ArrayList<>();
			for (TravellerInfo traveller : rInfo.getTravellerInfo()) {
				Paxes pax = Paxes.builder().name(traveller.getFirstName()).surname(traveller.getLastName())
						.type(traveller.getPaxType().getCode().equals(PaxType.ADULT.getCode())
								? HotelBedPaxType.ADULT.getCode()
								: HotelBedPaxType.CHILD.getCode())
						.roomId(roomIndex).build();
				paxes.add(pax);
			}
			room.setPaxes(paxes);
			rooms.add(room);
		});
		TravellerInfo traveller = option.getRoomInfos().get(0).getTravellerInfo().get(0);
		Holder holder = Holder.builder().name(traveller.getFirstName()).surname(traveller.getLastName()).build();

		HotelBedsBookRequest bookingRequest = HotelBedsBookRequest.builder().clientReference(order.getBookingId())
				.holder(holder).remark("").rooms(rooms).tolerance(tolerance).build();
		return bookingRequest;
	}

}

