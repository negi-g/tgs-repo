package com.tgs.services.hms.sources.desiya;

import org.apache.axis2.client.Stub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.commons.lang3.StringUtils;

import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Setter
@Getter
@Slf4j
public class DesiyaBindingService {


	public static TGServiceEndPointImplServiceStub getSearchStub(HotelSupplierConfiguration configuration) {
		TGServiceEndPointImplServiceStub searchStub = null;
		try {
			if (searchStub == null) {
				searchStub = new TGServiceEndPointImplServiceStub(
						configuration.getHotelSupplierCredentials().getUrl() + "" + DesiyaConstants.SEARCH_SUFFIX);
				updateProxy(searchStub);
			}
		} catch (Exception e) {
			log.error("FullFillment Stub Binding Initilaized failed", e);
		}
		return searchStub;
	}

	public static TGServiceEndPointImplServiceStub getDetailStub(HotelSupplierConfiguration configuration) {
		TGServiceEndPointImplServiceStub detailstub = null;
		try {
			if (detailstub == null) {
				detailstub = new TGServiceEndPointImplServiceStub(
						configuration.getHotelSupplierCredentials().getUrl() + "" + DesiyaConstants.SEARCH_SUFFIX);
				updateProxy(detailstub);
			}
		} catch (Exception e) {
			log.error("FullFillment Stub Binding Initilaized failed", e);
		}
		return detailstub;
	}

	public static TGBookingServiceEndPointImplServiceStub getBookingStub(HotelSupplierConfiguration configuration) {
		TGBookingServiceEndPointImplServiceStub bookStub = null;
		try {
			if (bookStub == null) {
				bookStub = new TGBookingServiceEndPointImplServiceStub(
						configuration.getHotelSupplierCredentials().getUrl() + "" + DesiyaConstants.BOOKING_SUFFIX);
				updateProxy(bookStub);
			}
		} catch (Exception e) {
			log.error("FullFillment Stub Binding Initilaized failed", e);
		}
		return bookStub;
	}

	public static TGBookingServiceEndPointImplServiceStub getCancellationStub(
			HotelSupplierConfiguration configuration) {
		TGBookingServiceEndPointImplServiceStub cancelStub = null;
		try {
			if (cancelStub == null) {
				cancelStub = new TGBookingServiceEndPointImplServiceStub(
						configuration.getHotelSupplierCredentials().getUrl() + "" + DesiyaConstants.BOOKING_SUFFIX);
				updateProxy(cancelStub);
			}
		} catch (Exception e) {
			log.error("FullFillment Stub Binding Initilaized failed", e);
		}
		return cancelStub;
	}
	

	protected static void updateProxy(Stub stub) {
		
		String proxyAddress = null;
		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
		if (configuratorInfo != null) {
			proxyAddress = configuratorInfo.getProxyAddress();
			if(StringUtils.isNotBlank(proxyAddress)) {
				HttpTransportProperties.ProxyProperties proxyProperties = new HttpTransportProperties.ProxyProperties();
		        proxyProperties.setProxyName(proxyAddress.split(":")[0]);
		        proxyProperties.setProxyPort(Integer.valueOf(proxyAddress.split(":")[1]));
		        stub._getServiceClient().getOptions().setProperty(HTTPConstants.PROXY, proxyProperties);
			}
		}
	}

}
