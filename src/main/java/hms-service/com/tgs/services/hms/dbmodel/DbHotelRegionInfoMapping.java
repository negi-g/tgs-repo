package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.HotelRegionInfoMapping;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "hotelregioninfomapping", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"regionId", "supplierName"})})
public class DbHotelRegionInfoMapping extends BaseModel<DbHotelRegionInfoMapping, HotelRegionInfoMapping> {

	private Long regionId;
	private Long supplierRegionId;
	private String supplierName;

	@CreationTimestamp
	private LocalDateTime createdOn;

	@Override
	public HotelRegionInfoMapping toDomain() {
		return new GsonMapper<>(this, HotelRegionInfoMapping.class).convert();
	}

	@Override
	public DbHotelRegionInfoMapping from(HotelRegionInfoMapping dataModel) {
		return new GsonMapper<>(dataModel, this, DbHotelRegionInfoMapping.class).convert();
	}
}
