package com.tgs.services.hms.restcontroller.mapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.hms.restmodel.FetchHotelRegionInfoResponse;
import com.tgs.services.hms.servicehandler.HotelRegionInfoFetchHandler;

@RestController
@RequestMapping("/hms/v2/region")
public class HotelRegionInfoControllerV2 {

	@Autowired
	HotelRegionInfoFetchHandler regionInfoHandler;

	/*
	 * Used in API Out
	 */

	@RequestMapping(value = {"/static-info", "/static-info/{next}"},
			method = RequestMethod.GET)
	protected FetchHotelRegionInfoResponse getRegionInfo(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(required = false) String next) throws Exception {
		regionInfoHandler.initData(next, new FetchHotelRegionInfoResponse());
		return regionInfoHandler.getResponse();
	}
}
