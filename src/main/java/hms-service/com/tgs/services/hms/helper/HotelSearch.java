package com.tgs.services.hms.helper;

import org.springframework.stereotype.Service;

import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;

@Service
public class HotelSearch extends AbstractHotelSearch {

	@Override
	public HotelSearchResult search(HotelSearchQuery searchQuery, ContextData contextData) {
		return super.search(searchQuery, contextData);
	}

	@Override
	public void fetchDetails(HotelSearchQuery searchQuery, HotelInfo hInfo) {
		super.fetchDetails(searchQuery, hInfo);
	}

}
