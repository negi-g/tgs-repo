package com.tgs.services.hms.servicehandler;

import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class HotelCityStaticDataHandler extends ServiceHandler<HotelStaticDataRequest, BaseResponse> {
	
	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws Exception {
		AbstractStaticDataInfoFactory factory = HotelSourceType.getStaticDataFactoryInstance(request.getSupplierId() , request);
		factory.getCityMappingInfo();
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
