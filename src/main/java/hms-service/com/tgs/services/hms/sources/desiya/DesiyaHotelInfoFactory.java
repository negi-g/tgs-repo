package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class DesiyaHotelInfoFactory extends AbstractHotelInfoFactory {

	@Autowired
	protected GeneralCachingCommunicator cachingComm;

	protected SoapRequestResponseListner listener = null;

	@Autowired
	protected HotelCacheHandler cacheHandler;

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	protected TGServiceEndPointImplServiceStub stub;

	protected DesiyaBindingService bindingService;

	public DesiyaHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException, InterruptedException {
		if (StringUtils.isNotBlank(searchQuery.getSearchPreferences().getHotelId())
				&& Objects.isNull(searchQuery.getMiscInfo().getHotelWiseSearchInfo())) {
			log.info("Not able to search for searchid {} and supplier {} as hotel wise search info is not present",
					searchQuery.getSearchId(), supplierConf.getBasicInfo().getSupplierName());
			return;
		}
		Set<String> propertyIds = getPropertyIdsOfCity(HotelSourceType.DESIYA.name());
		searchResult = getResultOfAllPropertyIds(propertyIds);
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException, JAXBException {
		listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
				supplierConf.getBasicInfo().getSupplierName());
		if (!HotelUtils.allOptionDetailHit(hInfo)) {
			DesiyaSearchService searchService = DesiyaSearchService.builder().sourceConfig(sourceConfigOutput)
					.supplierConf(this.getSupplierConf()).hInfo(hInfo).searchQuery(searchQuery)
					.cacheHandler(cacheHandler).listener(listener).build();
			try {
				searchService.doDetailSearch(hInfo);
				for (Option option : hInfo.getOptions()) {
					option.getMiscInfo().setIsDetailHit(true);
				}
			} finally {
			}
		}
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) {

		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option option = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		hotel.getOptions().forEach(op -> {
			if (op.getId().equals(optionId)) {
				op.setCancellationPolicy(option.getCancellationPolicy());
			}
		});
	}

	@Override
	protected HotelSearchResult doSearchForSupplier(Set<String> set, int threadCount)
			throws IOException, JAXBException {
		listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
				supplierConf.getBasicInfo().getSupplierName());

		DesiyaSearchService searchService = DesiyaSearchService.builder().sourceConfig(sourceConfigOutput)
				.supplierConf(this.getSupplierConf()).propertyIds(set).searchQuery(searchQuery).listener(listener)
				.cacheHandler(cacheHandler).build();
		searchService.doPropertyIdBasedSearch();
		HotelSearchResult searchResult = (HotelSearchResult) searchService.getSearchResult();
		return searchResult;
	}

}
