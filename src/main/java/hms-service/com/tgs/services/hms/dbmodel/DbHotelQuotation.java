package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.quotation.HotelQuotation;
import com.tgs.services.hms.datamodel.quotation.HotelQuotationAddResult;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@TypeDefs({ @TypeDef(name = "QuotationType", typeClass = QuotationType.class)})
@Table(name = "hotelquotation", uniqueConstraints = { @UniqueConstraint(columnNames = { "id", "userId" }) })
public class DbHotelQuotation extends BaseModel<DbHotelQuotation, HotelQuotationAddResult> {

	private String name;
	private String userId;
	@CreationTimestamp
	@Column(updatable=false)
	private LocalDateTime createdOn;
	
	@Type(type = "QuotationType")
	private List<HotelQuotation> quotationInfo;
	
	@Override
	public HotelQuotationAddResult toDomain() {
		return new GsonMapper<>(this, HotelQuotationAddResult.class).convert();
	}

	@Override
	public DbHotelQuotation from(HotelQuotationAddResult dataModel) {
		return new GsonMapper<>(dataModel, this, DbHotelQuotation.class).convert();
	}
}
