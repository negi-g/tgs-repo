package com.tgs.services.hms.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelKeyInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelWiseSearchInfo;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.qtech.SupplierHotelIdMapping;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.dbmodel.DbSupplierHotelIdMapping;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DefaultStaticDataService implements HotelStaticDataService {

	@Autowired
	GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	HotelInfoService infoService;

	@Override
	public String getSeparator() {
		return null;
	}

	@Override
	public String getKeyToMergeHotel(HotelInfo hotelInfo) {
		Address address = hotelInfo.getAddress();
		String key = HotelUtils
				.getKey(HotelKeyInfo.builder().hotelName(hotelInfo.getName()).hotelRating(hotelInfo.getRating())
						.cityName(address.getCity().getName()).countryName(address.getCountry().getName()).build());
		return key;
	}

	@Override
	public String getHotelInfoUniqueKey(HotelInfo hotelInfo) {
		return hotelInfo.getId();
	}

	@Override
	public String getCityName(String supplierName, String cityName) {
		return cityName;
	}

	@Override
	public String getStateName(String supplierName, String stateName) {
		return stateName;
	}

	@Override
	public String getCountryName(String supplierName, String countryName) {
		return countryName;
	}

	@Override
	public String getSupplierSetName(String supplierName) {

		return BaseHotelUtils.getSupplierSetName(supplierName);
	}

	@Override
	public String getSupplierStaticHotelId(HotelInfo hotelInfo) {

		return hotelInfo.getMiscInfo().getSupplierStaticHotelId();
	}

	@Override
	public HotelInfo getHotelInfoFromDB(HotelInfo hotelInfo) throws Exception {
		Address address = hotelInfo.getAddress();
		DbHotelInfo dbHotelInfo = infoService.findByNameAndRatingAndCityNameAndCountryName(hotelInfo.getName(),
				Objects.nonNull(hotelInfo.getRating()) ? String.valueOf(hotelInfo.getRating()) : null,
				address.getCity().getName(), address.getCountry().getName());
		if (Objects.nonNull(dbHotelInfo))
			return dbHotelInfo.toDomain();
		return HotelInfo.builder().build();
	}

	@Override
	public boolean validateHotelInfo(HotelInfo updatedHotelInfo, String supplierName, String bookingId)
			throws IOException {

		return true;
	}

	@Override
	public Map<String, HotelInfo> processAndFetchHotelInfo(String supplierName, Set<String> supplierHotelIds,
			HotelSearchQuery searchQuery, OperationType operationType) {

		String supplierMappingBinName = BaseHotelUtils.getSupplierBinName(supplierName);
		String supplierMappingSetName = BaseHotelUtils.getSupplierSetName(supplierName);

		Map<String, Map<String, String>> supplierhotelIdMap = fetchSupplierIdToHotelIdMapping(supplierMappingSetName,
				supplierMappingBinName, supplierHotelIds, searchQuery);

		Map<String, String> hotelIdAsKeySupplierIdAsValueMap = getHotelIdAsKeySupplierIdAsValue(supplierhotelIdMap);
		return processAndGenerateHotelInfo(hotelIdAsKeySupplierIdAsValueMap, searchQuery, operationType);
	}

	@Override
	public Map<String, String> getRoomMappingInfo(HotelSearchQuery searchQuery, HotelInfo hotelInfo) {

		return null;
	}

	private Map<String, Map<String, String>> fetchSupplierIdToHotelIdMapping(String supplierMappingSetName,
			String supplierMappingBinName, Set<String> supplierHotelIds, HotelSearchQuery searchQuery) {

		log.info("Fetching supplier id to hotel id mapping for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(), supplierHotelIds.size());

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_MAPPING_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(supplierMappingSetName).keys(supplierHotelIds.toArray(new String[0]))
				.bins(new String[] {supplierMappingBinName}).build();
		Map<String, Map<String, String>> supplierhotelIdMap =
				cachingCommunicator.get(supplierMappingMetaInfo, String.class);

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_MAPPING_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(supplierHotelIds.size()).supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
				LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_MAPPING_PROCESS_START);

		log.info("Fetched supplier id to hotel id mapping for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(), supplierHotelIds.size());
		return supplierhotelIdMap;
	}

	private Map<String, HotelInfo> processAndGenerateHotelInfo(Map<String, String> hotelIdAsKeySupplierIdAsValueMap,
			HotelSearchQuery searchQuery, OperationType operationType) {
		log.info("Processing static info for search id {} and supplier {} are {}", searchQuery.getSearchId(),
				searchQuery.getMiscInfo().getSupplierId(), hotelIdAsKeySupplierIdAsValueMap.size());
		Map<String, HotelInfo> hotelInfoList = new HashMap<>();
		if (operationType.equals(OperationType.LIST_SEARCH)) {
			Map<String, Map<String, HotelInfo>> hotelIdWithHotelInfoMap = fetchHotelIdToHotelStaticData(
					hotelIdAsKeySupplierIdAsValueMap, searchQuery, new String[] {BinName.HOTEL_INFO.name()});
			log.info(
					"Processing static info after fetching hotel id to hotel static data for search id {} and supplier {} are {}",
					searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(),
					hotelIdWithHotelInfoMap.size());

			for (Map.Entry<String, String> hotelIdAsKeySupplierIdAsValueEntry : hotelIdAsKeySupplierIdAsValueMap
					.entrySet()) {
				try {
					Map<String, HotelInfo> hotelInfo =
							hotelIdWithHotelInfoMap.get(hotelIdAsKeySupplierIdAsValueEntry.getKey());
					if (Objects.nonNull(hotelInfo)) {
						hotelInfoList.put(hotelIdAsKeySupplierIdAsValueEntry.getValue(),
								hotelInfo.get(BinName.HOTEL_INFO.name()));
					}
				} catch (Exception e) {
					log.debug("Unable to find mapping for hotelId {} and supplierId {}",
							hotelIdAsKeySupplierIdAsValueEntry.getKey(), hotelIdAsKeySupplierIdAsValueEntry.getValue(),
							e);
				}
			}
			log.info(
					"Processed static info after fetching hotel id to hotel static data for search id {} and supplier {} are {}",
					searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(),
					hotelIdWithHotelInfoMap.size());
		} else {
			Map<String, Map<String, HotelInfo>> hotelIdWithHotelInfoMap =
					fetchHotelIdToHotelStaticData(hotelIdAsKeySupplierIdAsValueMap, searchQuery,
							new String[] {BinName.HOTEL_INFO.name(), BinName.HOTEL_DETAIL.name()});
			for (Map.Entry<String, String> hotelIdAsKeySupplierIdAsValueEntry : hotelIdAsKeySupplierIdAsValueMap
					.entrySet()) {
				try {
					Map<String, HotelInfo> hotelInfo =
							hotelIdWithHotelInfoMap.get(hotelIdAsKeySupplierIdAsValueEntry.getKey());
					if (Objects.nonNull(hotelInfo)) {
						HotelInfo basicInfo = hotelInfo.get(BinName.HOTEL_INFO.name());
						if (Objects.nonNull(hotelInfo.get(BinName.HOTEL_DETAIL.name()))) {
							HotelInfo detailInfo = hotelInfo.get(BinName.HOTEL_DETAIL.name());
							hotelInfoList.put(hotelIdAsKeySupplierIdAsValueEntry.getValue(),
									new GsonMapper<>(detailInfo, basicInfo, HotelInfo.class).convert());
						} else {
							hotelInfoList.put(hotelIdAsKeySupplierIdAsValueEntry.getValue(), basicInfo);
						}
					}
				} catch (Exception e) {
					log.debug("Unable to find mapping for hotelId {} and supplierId {}",
							hotelIdAsKeySupplierIdAsValueEntry.getKey(), hotelIdAsKeySupplierIdAsValueEntry.getValue(),
							e);
				}
			}
		}
		log.info("Processed static info for search id {} and supplier {} are {}", searchQuery.getSearchId(),
				searchQuery.getMiscInfo().getSupplierId(), hotelIdAsKeySupplierIdAsValueMap.size());
		return hotelInfoList;
	}

	private Map<String, Map<String, HotelInfo>> fetchHotelIdToHotelStaticData(
			Map<String, String> hotelIdAsKeySupplierIdAsValueMap, HotelSearchQuery searchQuery, String[] bins) {

		log.info("Fetching hotel id to hotel static data for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(),
				hotelIdAsKeySupplierIdAsValueMap.size());

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		CacheMetaInfo hotelMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_STATIC_INFO.getName()).bins(bins)
				.kyroCompress(true).keys(hotelIdAsKeySupplierIdAsValueMap.keySet().toArray(new String[0])).build();
		Map<String, Map<String, HotelInfo>> hotelIdWithHotelInfoMap =
				cachingCommunicator.get(hotelMappingMetaInfo, HotelInfo.class);

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(hotelIdAsKeySupplierIdAsValueMap.size())
						.supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
				LogTypes.HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_START);
		log.info("Fetched hotel id to hotel static data for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(),
				hotelIdAsKeySupplierIdAsValueMap.size());
		return hotelIdWithHotelInfoMap;
	}

	private Map<String, String> getHotelIdAsKeySupplierIdAsValue(Map<String, Map<String, String>> supplierIdMap) {

		Map<String, String> supplierIdHotelIdMap = new HashMap<>();
		for (Map.Entry<String, Map<String, String>> supplierIdEntryMap : supplierIdMap.entrySet()) {
			Iterator<Map.Entry<String, String>> supplierIdIterator =
					supplierIdEntryMap.getValue().entrySet().iterator();
			if (supplierIdIterator.hasNext()) {
				Map.Entry<String, String> supplierIdEntry = supplierIdIterator.next();
				supplierIdHotelIdMap.put(supplierIdEntry.getValue(), supplierIdEntryMap.getKey());
			}
		}
		return supplierIdHotelIdMap;
	}

	@Override
	public List<HotelWiseSearchInfo> getHotelWiseSearchInfo(String hotelId) {

		return null;
	}

	@Override
	public SupplierHotelIdMapping getHotelIdMappingFromDB(String hotelid) {
		DbSupplierHotelIdMapping dbHotelIdMapping = infoService.findByHotelId(hotelid);
		return Objects.isNull(dbHotelIdMapping) ? null : dbHotelIdMapping.toDomain();
	}


}
