package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.travelbullz.HotelStaticResponse;
import com.tgs.services.hms.datamodel.travelbullz.StaticHotels;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzRequest;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfoMapping;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
@Service
public class TravelbullzHotelStaticDataService extends TravelbullzBaseService {

	protected HotelInfoSaveManager hotelInfoSaveManager;
	private TravelbullzRequest staticRequest;
	private HotelStaticDataRequest staticDataRequest;
	private HotelRegionInfoService regionInfoService;
	private HotelStaticResponse hotelStaticResponse;
	private HotelStaticDataService staticDataService;

	public void init() {

		hotelInfoSaveManager =
				(HotelInfoSaveManager) SpringContext.getApplicationContext().getBean("hotelInfoSaveManager");

		regionInfoService =
				(HotelRegionInfoService) SpringContext.getApplicationContext().getBean("hotelRegionInfoService");

		staticDataService = (HotelStaticDataService) HotelUtils.getStaticDataService();
	}

	public void process() {
		HttpUtilsV2 httpUtils = null;
		List<String> cityList = getTravelbullzCityIdList();
		log.info("Total cities for TRAVELBULLZ hotel search are {}", cityList.size());
		// for (String city : cityList) {
		staticRequest = getHotelRequest(cityList.get(0));
		String baseurl = supplierConf.getHotelSupplierCredentials().getUrl();
		String urlString = StringUtils.join(baseurl, supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_STATIC_DATA));
		try {
			httpUtils = getHttpUtils(GsonUtils.getGson().toJson(staticRequest), urlString);
			hotelStaticResponse = httpUtils.getResponse(HotelStaticResponse.class).orElse(null);

		} catch (IOException e) {
			log.error("Error while fetching static hotel info ", e);
		}
		List<HotelInfo> hotelList = getHotelListFromResponse(hotelStaticResponse);
		saveHotelInfoList(hotelList);
		log.info("Total hotels for cityID : {} are {}", staticRequest.getRequest().getCityID(), hotelList.size());
		// }

	}

	private List<HotelInfo> getHotelListFromResponse(HotelStaticResponse response) {

		List<HotelInfo> hotelList = new ArrayList<>();
		if (response.getHotelList() != null && response.getHotelList().size() > 0) {
			response.getHotelList().forEach(hotel -> {
				String description = hotel.getDescription() != null ? hotel.getDescription() : "";
				HotelInfo hInfo = HotelInfo.builder().name(hotel.getHotelName().toUpperCase()).description(description)
						.geolocation(GetGeoLocation(hotel)).address(getaddress(hotel)).contact(getContact(hotel))
						.images(getImages(hotel)).supplierName(HotelSourceType.TRAVELBULLZ.name())
						.rating(Integer.parseInt(hotel.getRating())).cityName(hotel.getCityName().toUpperCase())
						.countryName(hotel.getCountryName().toUpperCase()).id(String.valueOf(hotel.getHotelId()))
						.build();
				hotelList.add(hInfo);
			});
		}
		return hotelList;
	}

	private List<Image> getImages(StaticHotels hotel) {
		List<Image> images = new ArrayList<>();
		Image image = Image.builder()
				.bigURL(ObjectUtils.isEmpty(hotel.getURL()) ? hotel.getHotelFrontImage() : hotel.getURL())
				.thumbnail(hotel.getHotelFrontImage()).build();
		images.add(image);
		return images;

	}

	private Contact getContact(StaticHotels hotel) {

		String phoneNumbers = hotel.getHotelContactNo();
		Contact contact = Contact.builder().email(hotel.getReservationEmail()).phone(phoneNumbers).build();
		return contact;
	}

	private GeoLocation GetGeoLocation(StaticHotels hotel) {

		GeoLocation geoLocation = null;
		if (hotel.getLatitude() != null || hotel.getLongitude() != null) {
			geoLocation = GeoLocation.builder().latitude(hotel.getLatitude()).longitude(hotel.getLongitude()).build();
		}
		return geoLocation;
	}

	private Address getaddress(StaticHotels hotel) {

		City city =
				City.builder().name(hotel.getCityName().toUpperCase()).code(String.valueOf(hotel.getCityId())).build();
		String countryName = (hotel.getCountryName().toUpperCase()) != null ? hotel.getCountryName() : "";
		Country country = Country.builder().name(countryName).code(String.valueOf(hotel.getCountryId())).build();
		Address address = Address.builder().addressLine1(hotel.getAddress()).city(city).country(country).build();
		return address;
	}

	private void saveHotelInfoList(List<HotelInfo> hInfoList) {
		staticDataService = (HotelStaticDataService) HotelUtils.getStaticDataService();
		for (HotelInfo hInfo : hInfoList) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hInfo);
				dbHotelInfo.setSupplierHotelId(hInfo.getId());
				dbHotelInfo.setId(null);
				dbHotelInfo.setSupplierName(HotelSourceType.TRAVELBULLZ.name());
				hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, staticDataService);
			} catch (Exception e) {
				log.error("Error While Storing Master Hotel With Name {} Rating {}", hInfo.getName(), hInfo.getRating(),
						e);
			}
		}
	}

	private List<String> getTravelbullzCityIdList() {
		if (CollectionUtils.isEmpty(staticDataRequest.getCityIds())) {
			List<DbHotelRegionInfoMapping> supplierRegionInfos =
					regionInfoService.findBySupplierName(HotelSourceType.CLEARTRIP.name());
			return supplierRegionInfos.stream().map(regionInfo -> regionInfo.getId().toString())
					.collect(Collectors.toList());
		} else {
			return staticDataRequest.getCityIds();
		}
	}

}
