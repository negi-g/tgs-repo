package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.xml.bind.JAXBException;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.hms.sources.qtech.QTechBookingCancellationService;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class DesiyaHotelCancellationFactory  extends AbstractHotelBookingCancellationFactory {
	protected SoapRequestResponseListner listener;
	public DesiyaHotelCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo, Order order) {
		super(supplierConf, hInfo, order);
	}


	@Override
	public boolean cancelHotel() throws IOException {
		listener = new SoapRequestResponseListner(order.getBookingId(), null,
				supplierConf.getBasicInfo().getSupplierName());
		DesiyaBookingCancellationService cancellationService = DesiyaBookingCancellationService.builder().supplierConf(supplierConf)
				.sourceConfigOutput(sourceConfigOutput).hInfo(hInfo).order(order).listener(listener).build();
		return cancellationService.cancelBooking();
	}

	@Override
	public boolean getCancelHotelStatus() {
		return false;
	}

}
