package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.springframework.util.ObjectUtils;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.travelbullz.CancellationPolicy;
import com.tgs.services.hms.datamodel.travelbullz.HotelRoom;
import com.tgs.services.hms.datamodel.travelbullz.Passenger;
import com.tgs.services.hms.datamodel.travelbullz.Request;
import com.tgs.services.hms.datamodel.travelbullz.TravelbullzRequest;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Getter
@Setter
@Slf4j
public class TravelbullzBaseService {
	protected MoneyExchangeCommunicator moneyExchnageComm;
	protected HotelSupplierConfiguration supplierConf;
	protected HotelSearchQuery searchQuery;
	protected HotelSourceConfigOutput sourceConfig;
	protected HotelCacheHandler cacheHandler;
	protected RestAPIListener listener;

	protected HttpUtilsV2 getHttpUtils(String request, String url) throws IOException {
		HttpUtilsV2 httpUtils =
				HttpUtilsV2.builder().urlString(url).postData(request).proxy(HotelUtils.getProxyFromConfigurator())
						.requestMethod(HttpUtils.REQ_METHOD_POST).headerParams(getHeaderParams()).build();
		httpUtils.setPrintResponseLog(false);
		return httpUtils;

	}

	public Map<String, String> getHeaderParams() {

		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("content-type", "application/json");
		headerParams.put("Accept", "application/json");
		headerParams.put("Accept-Encoding", "gzip, deflate, br");

		return headerParams;
	}

	protected TravelbullzRequest getCountryRequest() {
		TravelbullzRequest request =
				TravelbullzRequest.builder().Token(supplierConf.getHotelSupplierCredentials().getPassword()).build();
		return request;
	}

	protected TravelbullzRequest getCityRequest(String countryId) {
		Request request = Request.builder().CountryId(countryId).build();
		TravelbullzRequest cityrequest = TravelbullzRequest.builder()
				.Token(supplierConf.getHotelSupplierCredentials().getPassword()).Request(request).build();
		return cityrequest;
	}

	protected TravelbullzRequest getHotelRequest(String cityId) {
		Request request = Request.builder().CityID(cityId).build();
		TravelbullzRequest hotelrequest = TravelbullzRequest.builder()
				.Token(supplierConf.getHotelSupplierCredentials().getPassword()).Request(request).build();
		return hotelrequest;
	}

	protected List<HotelRoom> gethotelroomslist(Option option) {
		List<HotelRoom> roomList = new ArrayList<>();
		for (RoomInfo room : option.getRoomInfos()) {
			HotelRoom hotelroom = HotelRoom.builder().RoomNo(String.valueOf(room.getMiscInfo().getRoomIndex()))
					.RoomToken(room.getMiscInfo().getRoomBlockId()).build();
			roomList.add(hotelroom);
		}
		return roomList;
	}

	protected String getRoomId(HotelRoom room) {
		String idString = room.getRoomNo() + "_" + room.getMealName() + ":" + room.getRoomTypeName().split("\\s")[0];
		return idString;
	}

	protected List<PriceInfo> setPerNightPrice(Double price) {
		List<PriceInfo> priceInfoList = new ArrayList<>();
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		double perNightBasePrice = getAmountBasedOnCurrency(price / numberOfNights, TravelbullzConstant.CURRENCY.value);
		for (int j = 1; j <= numberOfNights; j++) {
			PriceInfo priceInfo = new PriceInfo();
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, perNightBasePrice);
			fareComponents.put(HotelFareComponent.SBP, perNightBasePrice);
			priceInfo.setDay(j);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
		}
		return priceInfoList;
	}

	public void updatePriceWithMarkup(List<RoomInfo> rInfoList) {

		double supplierMarkup = (sourceConfig == null || sourceConfig.getSupplierMarkup() == null) ? 0.0
				: sourceConfig.getSupplierMarkup();

		for (RoomInfo roomInfo : rInfoList) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				double supplierGrossPrice = priceInfo.getFareComponents().get(HotelFareComponent.BF);
				priceInfo.getFareComponents().put(HotelFareComponent.CMU, (supplierGrossPrice * supplierMarkup) / 100);
				priceInfo.getFareComponents().put(HotelFareComponent.SGP, supplierGrossPrice);
				priceInfo.getFareComponents().put(HotelFareComponent.SNP, supplierGrossPrice);
				priceInfo.getFareComponents().put(HotelFareComponent.BF, supplierGrossPrice);

			}
		}

	}

	protected HotelCancellationPolicy setCancellationPolicy(RoomInfo roomInfo,
			List<CancellationPolicy> travelBullzcnpResponse) {
		List<PenaltyDetails> penaltyList = new ArrayList<>();

		travelBullzcnpResponse.forEach(policy -> {
			PenaltyDetails penalty = PenaltyDetails.builder().fromDate(formatDate(policy.getFromDate()))
					.toDate(formatDate(policy.getToDate()))
					.penaltyAmount(
							getAmountBasedOnCurrency(policy.getCancellationPrice(), TravelbullzConstant.CURRENCY.value))
					.build();
			penaltyList.add(penalty);
		});
		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder().penalyDetails(penaltyList)
				.id(roomInfo.getId())
				.miscInfo(CancellationMiscInfo.builder().isCancellationPolicyBelongToRoom(true).build()).build();

		roomInfo.setCancellationPolicy(cancellationPolicy);
		roomInfo.setDeadlineDateTime(HotelUtils.getDeadlineDateTimeFromPd(cancellationPolicy));
		return cancellationPolicy;

	}

	public void setOptionCancellationPolicy(Option opt, List<CancellationPolicy> cancellationPolicyResponse) {
		List<PenaltyDetails> penaltyList = new ArrayList<>();

		cancellationPolicyResponse.forEach(policy -> {
			PenaltyDetails penalty = PenaltyDetails.builder().fromDate(formatDate(policy.getFromDate()))
					.toDate(formatDate(policy.getToDate()))
					.penaltyAmount(
							getAmountBasedOnCurrency(policy.getCancellationPrice(), TravelbullzConstant.CURRENCY.value))
					.build();
			penaltyList.add(penalty);
		});
		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder().penalyDetails(penaltyList)
				.id(opt.getId()).miscInfo(CancellationMiscInfo.builder().isCancellationPolicyBelongToRoom(true).build())
				.build();

		opt.setCancellationPolicy(cancellationPolicy);
		opt.setDeadlineDateTime(HotelUtils.getDeadlineDateTimeFromPd(cancellationPolicy));

	}

	protected LocalDateTime formatDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
		LocalDate localDate = LocalDate.parse(date, formatter);
		return localDate.atStartOfDay();
	}

	public double getAmountBasedOnCurrency(double amount, String fromCurrency) {

		String toCurrency = "INR";
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(HotelSourceType.TRAVELBULLZ.name()).build();
		double exchangeAmount = moneyExchnageComm.getExchangeValue(amount, filter, true);
		if (exchangeAmount == 0) {
			filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency).toCurrency(toCurrency).type("Default")
					.build();
			exchangeAmount = moneyExchnageComm.getExchangeValue(amount, filter, true);
		}
		return exchangeAmount;

	}

	public void setMealBasis(RoomInfo roomInfo, String suppliermealbasis) {

		if (ObjectUtils.isEmpty(suppliermealbasis)) {
			roomInfo.setMealBasis(BaseHotelConstants.ROOM_ONLY);
			return;
		}
		Map<String, String> mealBasis =
				cacheHandler.fetchMealInfoFromAerospike(searchQuery, new HashSet<>(Arrays.asList(suppliermealbasis)));
		if (!ObjectUtils.isEmpty(roomInfo.getMealBasis())
				&& !roomInfo.getMealBasis().equals(BaseHotelConstants.ROOM_ONLY)) {
			return;
		}
		roomInfo.setMealBasis(mealBasis.getOrDefault(suppliermealbasis.toUpperCase(), BaseHotelConstants.ROOM_ONLY));
	}

	protected List<TravellerInfo> getTravellerList(HotelRoom room) {

		List<TravellerInfo> travellerInfoList = new ArrayList<>();
		for (Passenger pax : room.getPassenger()) {
			TravellerInfo ti = new TravellerInfo();
			ti.setFirstName(pax.getName());
			ti.setPaxType(pax.getPaxType().equals("ADULT") ? PaxType.ADULT : PaxType.CHILD);
			ti.setTitle(ti.getPaxType().equals(PaxType.ADULT) ? "Mr" : "Master");
			travellerInfoList.add(ti);
		}

		return travellerInfoList;
	}

}
