package com.tgs.services.hms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.tgs.services.base.helper.InitializerGroup;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import com.tgs.services.hms.datamodel.HotelRegionInfoMapping;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfoMapping;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@InitializerGroup(group = InitializerGroup.Group.HOTEL)
public class HotelRegionStaticDataPersistenceHelper extends InMemoryInitializer {

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	HotelRegionInfoService regionInfoService;


	public HotelRegionStaticDataPersistenceHelper() {
		super(null);
	}

	@Override
	public void process() {
		List<HotelRegionInfo> regionInfos = new ArrayList<>();
		List<HotelRegionInfoMapping> regionMappingList = new ArrayList<>();
		Runnable reloadRegionInfoSet = () -> {

			log.info("Fetching hotel region info from database");
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
				List<HotelRegionInfo> regionInfoListChunk = DbHotelRegionInfo
						.toDomainList(regionInfoService.findAll(page));
				if (CollectionUtils.isNotEmpty(regionInfoListChunk)) {
					regionInfos.addAll(regionInfoListChunk);
				}
				log.debug("Fetched hotel region info from database, info list size is {}", regionInfos.size());
			}

			if (CollectionUtils.isEmpty(regionInfos)) {

				log.info("Unable to persist master region in cache as the region is not present/enabled");
				return;
			}

			log.info("Fetching hotel region info mapping info from database");
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
				List<HotelRegionInfoMapping> regionInfoMappingChunk =
						DbHotelRegionInfoMapping.toDomainList(regionInfoService.findAllMapping(page));
				if (CollectionUtils.isEmpty(regionInfoMappingChunk))
					break;
				regionMappingList.addAll(regionInfoMappingChunk);
				log.debug("Fetched hotel region mapping info mapping from database, mapping list size is {}",
						regionMappingList.size());
			}

			Map<Long, List<HotelRegionInfoMapping>> cityMap =
					regionMappingList.stream().collect(Collectors.groupingBy(HotelRegionInfoMapping::getRegionId));
			for (int i = 0; i < regionInfos.size(); i++) {
				try {
					List<HotelRegionInfoMapping> regionInfoMappingList = cityMap.get(regionInfos.get(i).getId());
					Map<String, Object> regionInfoBinMap = new HashMap<>();
					regionInfoBinMap.put(BinName.REGION_INFO.name(), GsonUtils.getGson().toJson(regionInfos.get(i)));
					regionInfoBinMap.put(BinName.IATA.name(),
							regionInfos.get(i).getIataCode() == null ? "" : regionInfos.get(i).getIataCode());
					if (CollectionUtils.isNotEmpty(regionInfoMappingList)) {
						for (HotelRegionInfoMapping regionInfoMapping : regionInfoMappingList) {
							HotelSupplierRegionInfo supplierRegionInfo = regionInfoService
									.findSupplierRegionInfoById(regionInfoMapping.getSupplierRegionId()).toDomain();
							regionInfoBinMap.put(regionInfoMapping.getSupplierName(),
									GsonUtils.getGson().toJson(supplierRegionInfo));
						}
						CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder()
								.namespace(CacheNameSpace.HOTEL.getName()).set(CacheSetName.REGION_MAPPING.getName())
								.key(String.valueOf(regionInfos.get(i).getId())).expiration(-1)
								.binValues(regionInfoBinMap).build();
						cacheService.store(metaInfoToStore);
					}
				} catch (Exception e) {
					log.error("Error while storing city mapping in aerospike for region {} , {}",
							regionInfos.get(i).getId(), regionInfos.get(i).getRegionName(), e);
				}
			}
			log.info("Fetched hotel region info mapping info from database");
		};

		Thread reloadRegionInfoSetThread = new Thread(reloadRegionInfoSet);
		reloadRegionInfoSetThread.start();

	}

	@Override
	public void deleteExistingInitializer() {}

}
