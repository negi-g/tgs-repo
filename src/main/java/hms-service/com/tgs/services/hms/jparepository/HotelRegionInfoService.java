package com.tgs.services.hms.jparepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfoMapping;
import com.tgs.services.hms.dbmodel.DbHotelSupplierRegionInfo;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelRegionInfoService {

	@Autowired
	DbHotelRegionInfoRepository regionInfoRepository;

	@Autowired
	DbHotelRegionInfoMappingRepository regionMappingRepository;

	@Autowired
	DbHotelSupplierRegionInfoRepository supplierRegionInfoRepository;

	public DbHotelRegionInfo saveRegionInfoIntoDB(DbHotelRegionInfo cityInfo) {

		cityInfo.setRegionName(cityInfo.getRegionName().toUpperCase());
		cityInfo.setCountryName(cityInfo.getCountryName().toUpperCase());
		return regionInfoRepository.save(cityInfo);
	}

	public List<DbHotelRegionInfo> findAll(Pageable pageable) {
		List<DbHotelRegionInfo> regionInfos = new ArrayList<>();
		Page<DbHotelRegionInfo> dbRegionInfos = regionInfoRepository.findAll(pageable);
		for (DbHotelRegionInfo dbRegionInfo : dbRegionInfos) {
			if (BooleanUtils.isTrue(dbRegionInfo.getEnabled())) {
				regionInfos.add(dbRegionInfo);
			}
		}
		return regionInfos;
	}

	public DbHotelRegionInfo findById(Long id) {
		return regionInfoRepository.findById(id);
	}

	public DbHotelRegionInfoMapping saveSupplierMapping(DbHotelRegionInfoMapping mappingObj) {
		return regionMappingRepository.save(mappingObj);
	}

	public DbHotelRegionInfoMapping findByRegionIdAndSupplierName(Long regionId, String supplierName) {
		return regionMappingRepository.findByRegionIdAndSupplierName(regionId, supplierName);
	}

	public List<DbHotelRegionInfoMapping> findByRegionId(Long regionId) {
		return regionMappingRepository.findByRegionId(regionId);
	}

	public List<DbHotelSupplierRegionInfo> findBySupplierName(String supplierName, Pageable pageable) {
		List<DbHotelSupplierRegionInfo> supplierRegionInfos = new ArrayList<>();
		supplierRegionInfoRepository.findBySupplierName(supplierName, pageable)
				.forEach(supplierRegionInfo -> supplierRegionInfos.add(supplierRegionInfo));
		return supplierRegionInfos;
	}

	public List<DbHotelRegionInfoMapping> findAllMapping(Pageable pageable) {
		List<DbHotelRegionInfoMapping> cityInfoMappingList = new ArrayList<>();
		regionMappingRepository.findAll(pageable).forEach(hotelInfo -> cityInfoMappingList.add(hotelInfo));
		return cityInfoMappingList;
	}

	public DbHotelRegionInfo findByRegionNameAndCountryNameAndRegionTypeAndFullRegionName(String regionName,
			String countryName, String regionType, String fullRegionName) {
		return regionInfoRepository.findByRegionNameAndCountryNameAndRegionTypeAndFullRegionName(
				regionName.toUpperCase(), countryName.toUpperCase(), regionType.toUpperCase(),
				fullRegionName.toUpperCase());
	}

	public DbHotelRegionInfo findByRegionNameAndCountryNameAndRegionType(String regionName, String countryName,
			String regionType) {
		return regionInfoRepository.findByRegionNameAndCountryNameAndRegionType(regionName.toUpperCase(),
				countryName.toUpperCase(), regionType.toUpperCase());
	}

	public List<DbHotelRegionInfo> findByIdGreaterThanOrderByIdAscLimit(Long limit, Long cursor) {
		return regionInfoRepository.findByIdGreaterThanOrderByIdAscLimit(cursor, limit);
	}

	public List<DbHotelRegionInfoMapping> findByRegionIdAndSupplierList(Long regionId, List<String> supplierNames) {
		return regionMappingRepository.findByRegionIdAndSupplierList(regionId, supplierNames);
	}

	public List<DbHotelRegionInfoMapping> findBySupplierName(String supplierName) {
		return regionMappingRepository.findBySupplierName(supplierName);
	}

	public DbHotelSupplierRegionInfo findByRegionNameAndCountryNameAndSupplierName(String regionName,
			String countryName, String supplierName) {
		return supplierRegionInfoRepository.findByRegionNameAndCountryNameAndSupplierName(regionName, countryName,
				supplierName);
	}

	public DbHotelSupplierRegionInfo findByRegionNameAndStateNameAndCountryNameAndSupplierNameAndRegionTypeAndFullRegionName(
			String regionName, String stateName, String countryName, String supplierName, String regionType,
			String fullRegionName) {
		return supplierRegionInfoRepository
				.findByRegionNameAndStateNameAndCountryNameAndSupplierNameAndRegionTypeAndFullRegionName(regionName,
						stateName, countryName, supplierName, regionType, fullRegionName);
	}

	public DbHotelSupplierRegionInfo findByRegionNameAndStateNameAndCountryNameAndSupplierNameAndRegionType(
			String regionName, String stateName, String countryName, String supplierName, String regionType) {
		return supplierRegionInfoRepository.findByRegionNameAndStateNameAndCountryNameAndSupplierNameAndRegionType(
				regionName, stateName, countryName, supplierName, regionType);
	}

	public DbHotelSupplierRegionInfo findByStateNameAndCountryNameAndSupplierName(String regionName, String countryName,
			String supplierName) {
		return supplierRegionInfoRepository.findByStateNameAndCountryNameAndSupplierName(regionName, countryName,
				supplierName);
	}

	public DbHotelSupplierRegionInfo findSupplierRegionInfoById(Long id) {
		return supplierRegionInfoRepository.findById(id);
	}

	public DbHotelSupplierRegionInfo saveSupplierRegionInfo(DbHotelSupplierRegionInfo regionInfo) {
		return supplierRegionInfoRepository.save(regionInfo);
	}

	public List<DbHotelSupplierRegionInfo> findByRegionNameAndSupplierName(String regionName, String supplierName) {
		return supplierRegionInfoRepository.findByRegionNameAndSupplierName(regionName, supplierName);
	}

	public List<DbHotelSupplierRegionInfo> findByRegionNameContainingIgnoreCaseAndSupplierName(String regionName,
			String supplierName) {
		return supplierRegionInfoRepository.findByRegionNameContainingIgnoreCaseAndSupplierName(regionName,
				supplierName);
	}

	public List<DbHotelSupplierRegionInfo> findByRegionIdAndSupplierName(String regionId, String supplierName) {
		return supplierRegionInfoRepository.findByRegionIdAndSupplierName(regionId, supplierName);
	}

	public List<DbHotelSupplierRegionInfo> findByStateIdAndSupplierName(String stateId, String supplierName) {
		return supplierRegionInfoRepository.findByStateIdAndSupplierName(stateId, supplierName);
	}

	public List<DbHotelSupplierRegionInfo> findByStateNameAndSupplierName(String stateName, String supplierName) {
		return supplierRegionInfoRepository.findByStateNameAndSupplierName(stateName, supplierName);
	}

	public List<DbHotelSupplierRegionInfo> findByStateNameContainingIgnoreCaseAndSupplierName(String stateName,
			String supplierName) {
		return supplierRegionInfoRepository.findByStateNameContainingIgnoreCaseAndSupplierName(stateName, supplierName);
	}

	public List<DbHotelSupplierRegionInfo> findByCountryIdAndSupplierName(String countryId, String supplierName) {
		return supplierRegionInfoRepository.findByCountryIdAndSupplierName(countryId, supplierName);
	}

	public List<DbHotelSupplierRegionInfo> findByCountryNameAndSupplierName(String countryName, String supplierName) {
		return supplierRegionInfoRepository.findByCountryNameAndSupplierName(countryName, supplierName);
	}

	public List<DbHotelSupplierRegionInfo> findByCountryNameContainingIgnoreCaseAndSupplierName(String countryName,
			String supplierName) {
		return supplierRegionInfoRepository.findByCountryNameContainingIgnoreCaseAndSupplierName(countryName,
				supplierName);
	}

	public DbHotelRegionInfo isMasterRegionExists(HotelRegionInfo regionInfo, Boolean isFullRegionNameExists) {
		DbHotelRegionInfo dbRegionInfo = null;
		try {
			if (BooleanUtils.isTrue(isFullRegionNameExists)) {
				dbRegionInfo = findByRegionNameAndCountryNameAndRegionTypeAndFullRegionName(regionInfo.getRegionName(),
						regionInfo.getCountryName(), regionInfo.getRegionType(), regionInfo.getFullRegionName());
			} else {
				dbRegionInfo = findByRegionNameAndCountryNameAndRegionType(regionInfo.getRegionName(),
						regionInfo.getCountryName(), regionInfo.getRegionType());
			}
		} catch (Exception e) {
			log.info(
					"Error while fetching master region name {}, country name {}, region type {} and full region name {} due to {}",
					regionInfo.getRegionName(), regionInfo.getCountryName(), regionInfo.getRegionType(),
					regionInfo.getFullRegionName(), e.getMessage());
		}
		return dbRegionInfo;
	}

	public DbHotelSupplierRegionInfo isSupplierRegionExists(HotelRegionInfoQuery regionInfoQuery,
			Boolean isFullRegionNameExists) {

		String regionName = ObjectUtils.firstNonNull(regionInfoQuery.getRegionName(), "");
		String stateName = ObjectUtils.firstNonNull(regionInfoQuery.getStateName(), "");
		DbHotelSupplierRegionInfo dbSupplierRegionInfo = null;
		try {
			if (BooleanUtils.isTrue(isFullRegionNameExists)) {
				dbSupplierRegionInfo =
						findByRegionNameAndStateNameAndCountryNameAndSupplierNameAndRegionTypeAndFullRegionName(
								regionName, stateName, regionInfoQuery.getCountryName(),
								regionInfoQuery.getSupplierName(), regionInfoQuery.getRegionType(),
								regionInfoQuery.getFullRegionName());
			} else {
				dbSupplierRegionInfo = findByRegionNameAndStateNameAndCountryNameAndSupplierNameAndRegionType(
						regionName, stateName, regionInfoQuery.getCountryName(), regionInfoQuery.getSupplierName(),
						regionInfoQuery.getRegionType());
			}
		} catch (Exception e) {
			log.info(
					"Error while fetching supplier region name {}, state name {}, country name {}, supplier name {}, region type {} and full region name {} due to {}",
					regionInfoQuery.getRegionName(), regionInfoQuery.getStateName(), regionInfoQuery.getCountryName(),
					regionInfoQuery.getSupplierName(), regionInfoQuery.getRegionType(),
					regionInfoQuery.getFullRegionName(), e.getMessage());
		}
		return dbSupplierRegionInfo;
	}

	public boolean enableMasterRegion(Long regionId) {
		DbHotelRegionInfo regionInfo = regionInfoRepository.findById(regionId);
		if (Objects.isNull(regionInfo)) {
			log.debug("Unable to enable master region for region id {} as it does not exist.", regionId);
			return false;
		}
		if (BooleanUtils.isTrue(regionInfo.getEnabled())) {
			return true;
		}
		regionInfo.setEnabled(true);
		regionInfoRepository.save(regionInfo);
		return true;
	}
}
