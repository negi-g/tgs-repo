package com.tgs.services.hms.sources.fitruums;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import com.amazonaws.util.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.enums.CountryInfoType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.fitruums.book.CancellationPolicy;
import com.tgs.services.hms.datamodel.fitruums.book.PreBookResult;
import com.tgs.services.hms.datamodel.fitruums.book.PreBookResult.Notes;
import com.tgs.services.hms.datamodel.fitruums.book.PreBookResult.Notes.Note;
import com.tgs.services.hms.datamodel.fitruums.book.PreBookResult.Price;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@SuperBuilder
public class FitruumsPriceValidationService extends FitruumsBaseService {
	private String bookingId;
	private Option updatedOption;

	public void validate(HotelInfo hInfo) throws IOException {

		HashMap<String, String> priceValidationRequestMap = getPreBookMap(hInfo.getOptions().get(0));
		FitruumsMarshaller fitruumsMarshaller = new FitruumsMarshaller();
		String xmlResponse = null;
		PreBookResult fitruumspreBookResult = null;
		HttpUtils httpUtils = null;
		try {
			listener = new RestAPIListener("");
			String priceValidationRequest = getKeyValuePair(priceValidationRequestMap);

			httpUtils = FitruumsUtil.getHttpUtils(priceValidationRequest, HotelUrlConstants.BLOCK_ROOM_URL, null,
					supplierConf);

			xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (!StringUtils.isNullOrEmpty(xmlResponse)) {
				fitruumspreBookResult = fitruumsMarshaller.unmarshallXML(xmlResponse, PreBookResult.class);
				if (Objects.nonNull(fitruumspreBookResult.getCancellationPolicies())) {
					createOptionWithNewPrice(fitruumspreBookResult, hInfo);
				} else {
					throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
				}
			}
		} finally {

			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().stream()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.REVIEW.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(xmlResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.FITRUUMS.name())
						.requestType(BaseHotelConstants.PRICE_CHECK).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(bookingId)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private void createOptionWithNewPrice(PreBookResult fitruumsSearchResult, HotelInfo hInfo) {
		Option oldOption = hInfo.getOptions().get(0);
		List<Option> newOptionList = new ArrayList<>();
		List<RoomInfo> roomList = getRooms(oldOption.getRoomInfos(), fitruumsSearchResult.getPrice());
		OptionMiscInfo optionMiscInfo =
				OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
						.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
						.sourceId(supplierConf.getBasicInfo().getSourceId())
						.bookingCode(fitruumsSearchResult.getPreBookCode()).build();
		oldOption.getMiscInfo().setBookingCode(fitruumsSearchResult.getPreBookCode());
		oldOption.setInstructions(getInstructions(fitruumsSearchResult.getNotes()));

		Option option = Option.builder().miscInfo(optionMiscInfo).id(hInfo.getOptions().get(0).getId())
				.roomInfos(roomList).instructions(getInstructions(fitruumsSearchResult.getNotes())).build();
		setOptionCancellationPolicyPreBook(fitruumsSearchResult.getCancellationPolicies().getCancellationPolicy(),
				option, fitruumsSearchResult.getPrice().getValue());
		newOptionList.add(option);
		HotelUtils.setBufferTimeinCnp(newOptionList, searchQuery);
		updatedOption = option;
	}

	private List<Instruction> getInstructions(Notes notes) {
		List<Instruction> instructionList = new ArrayList<>();
		if (Objects.nonNull(notes) && Objects.nonNull(notes.getNote()) && notes.getNote().size() >= 1) {
			for (Note note : notes.getNote()) {
				Instruction instruction =
						Instruction.builder().type(InstructionType.BOOKING_NOTES).msg(note.getText()).build();
				instructionList.add(instruction);
			}
		}
		return instructionList;
	}

	protected void setOptionCancellationPolicyPreBook(List<CancellationPolicy> list, Option option, int fitruumsPrice) {
		LocalDateTime checkInDate = searchQuery.getCheckinDate().atTime(12, 00);
		boolean isFullRefundAllowed = true;
		LocalDateTime currentDate = LocalDateTime.now();
		LocalDateTime fromDate = null;
		List<PenaltyDetails> pds = new ArrayList<>();
		Collections.reverse(list);
		for (CancellationPolicy policy : list) {
			if (Objects.isNull(policy.getDeadline())) {
				fromDate = LocalDateTime.now();
				isFullRefundAllowed = false;
			} else {
				Long deadline = (long) policy.getDeadline();
				fromDate = checkInDate.minusHours(deadline);
			}
			PenaltyDetails penaltyDetail = PenaltyDetails.builder().fromDate(fromDate).toDate(checkInDate)
					.penaltyAmount((double) fitruumsPrice * policy.getPercentage() / 100)
					.penaltyPercent((double) policy.getPercentage()).build();
			pds.add(penaltyDetail);
			checkInDate = fromDate;
		}
		if (isFullRefundAllowed) {
			PenaltyDetails penaltyDetail = PenaltyDetails.builder().fromDate(currentDate).toDate(checkInDate)
					.penaltyAmount(0.0).penaltyPercent(0.0).build();
			pds.add(0, penaltyDetail);
		}
		pds = pds.stream().sorted(Comparator.comparingDouble(PenaltyDetails::getPenaltyAmount))
				.collect(Collectors.toList());
		HotelCancellationPolicy cancellationPolicy =
				HotelCancellationPolicy.builder().id(option.getId()).isFullRefundAllowed(isFullRefundAllowed)
						.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build())
						.penalyDetails(pds).build();
		option.setDeadlineDateTime(HotelUtils.getDeadlineDateTimeFromPd(cancellationPolicy));
		option.setCancellationPolicy(cancellationPolicy);
	}

	private List<RoomInfo> getRooms(List<RoomInfo> roomInfos, Price price) {
		List<RoomInfo> roomList = new ArrayList<RoomInfo>();
		for (RoomInfo room : roomInfos) {
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setNumberOfAdults(room.getNumberOfAdults());
			roomInfo.setNumberOfChild(room.getNumberOfChild());
			roomInfo.setRoomType(room.getRoomType());
			roomInfo.setRoomCategory(room.getRoomType());
			roomInfo.setMealBasis(room.getMealBasis());
			populateRoomInfo(roomInfo, room, price);
			roomInfo.setId(room.getId());
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
			roomList.add(roomInfo);
		}
		updatePriceWithClientCommission(roomList);
		return roomList;
	}

	private void populateRoomInfo(RoomInfo roomInfo, RoomInfo oldroom, Price price) {
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		int noOfRooms = searchQuery.getRoomInfo().size();
		setPerNightRoomPrice(roomInfo, price.getValue(), numberOfNights, noOfRooms);
		roomInfo.setIsextraBedIncluded(oldroom.getIsextraBedIncluded());
		RoomMiscInfo roomMiscInfo = RoomMiscInfo.builder().roomIndex(oldroom.getMiscInfo().getRoomIndex())
				.inclusive((double) price.getValue()).amenities(oldroom.getMiscInfo().getAmenities()).build();
		roomInfo.setMiscInfo(roomMiscInfo);
	}

	private HashMap<String, String> getPreBookMap(Option option) {
		List<RoomInfo> roomList = option.getRoomInfos();
		HashMap<String, String> searchRequestmap = new HashMap<>();
		HashMap<String, String> adultChildMap = setAdultChild();
		searchRequestmap = setBasicRequirement(searchRequestmap);
		searchRequestmap.put("checkInDate", getDate(searchQuery.getCheckinDate()));
		searchRequestmap.put("checkOutDate", getDate(searchQuery.getCheckoutDate()));
		searchRequestmap.put("currency", FitruumsConstant.CURRENCY.getValue());
		searchRequestmap.put("rooms", String.valueOf(searchQuery.getRoomInfo().size()));
		searchRequestmap.put("checkInDate", getDate(searchQuery.getCheckinDate()));
		searchRequestmap.put("checkOutDate", getDate(searchQuery.getCheckoutDate()));
		searchRequestmap.put("roomId", String.valueOf(roomList.get(0).getMiscInfo().getRoomIndex()));
		searchRequestmap.put("mealId", roomList.get(0).getMiscInfo().getRatePlanCode());
		searchRequestmap.put("customerCountry", BaseHotelUtils
				.getSpecificFieldFromId(searchQuery.getSearchCriteria().getNationality(), CountryInfoType.CODE.name()));
		searchRequestmap.put("b2c", FitruumsConstant.B2C.getValue());
		searchRequestmap.put("searchPrice",
				String.valueOf((int) Math.round(roomList.get(0).getMiscInfo().getInclusive())));
		searchRequestmap.put("adults", adultChildMap.get("adults"));
		searchRequestmap.put("children", adultChildMap.get("children"));
		searchRequestmap.put("childrenAges", adultChildMap.get("childrenAges"));
		searchRequestmap.put("infant", adultChildMap.get("infant"));
		searchRequestmap.put("hotelId", "");
		searchRequestmap.put("roomtypeId", "");
		searchRequestmap.put("blockSuperDeal", "");
		searchRequestmap.put("showPriceBreakdown", "");
		return searchRequestmap;
	}
}
