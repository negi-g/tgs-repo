package com.tgs.services.hms.jparepository.inventory;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.filters.HotelRatePlanFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.datamodel.inventory.HotelSellPolicy;
import com.tgs.services.hms.dbmodel.inventory.DbHotelRatePlan;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelRatePlanService extends SearchService<DbHotelRatePlan> {

	@Autowired
	private HotelRatePlanRepository ratePlanRepo;

	public DbHotelRatePlan save(DbHotelRatePlan plan) {
		plan = ratePlanRepo.save(plan);
		return plan;
	}

	public DbHotelRatePlan findById(Long id) {
		return ratePlanRepo.findOne(id);
	}

	public DbHotelRatePlan findUniqueRatePlan(HotelRatePlan ratePlan) {
		return ratePlanRepo
				.findBySupplierIdAndSupplierHotelIdAndRoomCategoryIdAndMealBasisIdAndRoomTypeIdAndValidOnAndIsDeleted(
						ratePlan.getSupplierId(), ratePlan.getSupplierHotelId(), ratePlan.getRoomCategoryId(),
						ratePlan.getMealBasisId(), ratePlan.getRoomTypeId(), ratePlan.getValidOn(), true);
	}

	public List<DbHotelRatePlan> findAll(HotelRatePlanFilter filter) {
		return super.search(filter, ratePlanRepo);
	}

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public List<HotelRatePlan> saveRatePlans(List<DbHotelRatePlan> dbRatePlans) {
		List<HotelRatePlan> ratePlans = new ArrayList<>();
		for (DbHotelRatePlan dbRatePlan : dbRatePlans) {
			try {
				dbRatePlan = save(dbRatePlan);
				HotelRatePlan ratePlan = dbRatePlan.toDomain();
				ratePlan.setSellPolicy(HotelSellPolicy.getSellPolicy(dbRatePlan.getSellPolicy()));
				ratePlans.add(ratePlan);
			} catch (DataIntegrityViolationException e) {
				log.info("Rate plan already exists for {}", GsonUtils.getGson().toJson(dbRatePlan));
				throw new CustomGeneralException(SystemError.DUPLICATE_RATEPLAN,
						SystemError.DUPLICATE_RATEPLAN.getErrorDetail(dbRatePlan.getName()).getMessage());
			}
		}
		return ratePlans;
	}
}
