package com.tgs.services.hms.jparepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.dbmodel.DbHotelUserReviewIdInfo;

@Service
public class HotelUserReviewServiceImpl implements HotelUserReviewService{

	@Autowired
	DbHotelUserReviewRepository userReviewRepository;
	

	@Override
	public void save(DbHotelUserReviewIdInfo hotelUserReviewIdInfo) {
		userReviewRepository.save(hotelUserReviewIdInfo);
	}


	@Override
	public List<DbHotelUserReviewIdInfo> findAll(Pageable pageable) {
		List<DbHotelUserReviewIdInfo> list = new ArrayList<>();
		userReviewRepository.findAll(pageable).forEach(userReview -> list.add(userReview));
		return list;
	}
}