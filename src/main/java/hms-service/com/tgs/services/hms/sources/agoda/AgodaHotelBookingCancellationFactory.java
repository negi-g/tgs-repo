package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import javax.xml.bind.JAXBException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.oms.datamodel.Order;


@Service
public class AgodaHotelBookingCancellationFactory extends AbstractHotelBookingCancellationFactory {

	public AgodaHotelBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo, Order order) {
		super(supplierConf, hInfo, order);
	}

	@Override
	public boolean cancelHotel() throws IOException, JAXBException {

		AgodaHotelBookingCancellationService cancellationService = AgodaHotelBookingCancellationService.builder()
				.supplierConf(supplierConf).hInfo(hInfo).order(order).build();
		return cancellationService.cancelBooking();
	}

	@Override
	public boolean getCancelHotelStatus() throws IOException {

		return false;
	}

}
