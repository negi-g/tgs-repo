package com.tgs.services.hms.sources.vervotech;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.Area;
import com.tgs.services.hms.datamodel.Bed;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelAdditionalInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.OpinionatedContent;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PointOfInterest;
import com.tgs.services.hms.datamodel.RoomAdditionalInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.State;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.vervotech.Checkin;
import com.tgs.services.hms.datamodel.vervotech.Description;
import com.tgs.services.hms.datamodel.vervotech.Fee;
import com.tgs.services.hms.datamodel.vervotech.GeoCode;
import com.tgs.services.hms.datamodel.vervotech.Hotel;
import com.tgs.services.hms.datamodel.vervotech.HotelContent;
import com.tgs.services.hms.datamodel.vervotech.HotelContentRequest;
import com.tgs.services.hms.datamodel.vervotech.HotelContentResponse;
import com.tgs.services.hms.datamodel.vervotech.HotelIdentifierRequest;
import com.tgs.services.hms.datamodel.vervotech.HotelIdentifierRequest.HotelIdentifierRequestBuilder;
import com.tgs.services.hms.datamodel.vervotech.HotelIdentifierResponse;
import com.tgs.services.hms.datamodel.vervotech.Mapping;
import com.tgs.services.hms.datamodel.vervotech.MasterFacilitiesResponse;
import com.tgs.services.hms.datamodel.vervotech.OpinionatedHotel;
import com.tgs.services.hms.datamodel.vervotech.Policy;
import com.tgs.services.hms.datamodel.vervotech.ProviderHotel;
import com.tgs.services.hms.datamodel.vervotech.Room;
import com.tgs.services.hms.datamodel.vervotech.VervotechBaseRequest;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Setter
@Getter
public class VervotechStaticDataService {

	private HotelSourceConfigOutput sourceConfig;
	private HotelSupplierConfiguration supplierConf;
	private HotelStaticDataRequest staticDataRequest;
	private HotelInfoSaveManager hotelInfoSaveManager;
	private HotelStaticDataService staticDataService;

	private static ExecutorService fetchHotelContentPool = Executors.newFixedThreadPool(10);
	private Boolean isNew;
	private static final String HOTEL_IDENTIFIER_REQUEST_NEW = "/api/hotels/new";
	private static final String HOTEL_IDENTIFIER_REQUEST_DELETED = "/api/hotels/deleted";
	private static final String HOTEL_IDENTIFIER_REQUEST_UPDATED = "/api/hotels/updated";
	private static final String HOTEL_MASTER_FACILITIES_REQUEST = "/api/content/facilitygroups";
	private static final String HOTEL_STATIC_CONTENT_REQUEST = "/api/hotels/getHotelContent";
	private static final String DEFAULT_LAST_UPDATED_TIME = "1900-01-01T12:00:00Z";

	@Builder.Default
	private final Gson GSON = GsonUtils.getGson();


	public void init() {

		hotelInfoSaveManager =
				(HotelInfoSaveManager) SpringContext.getApplicationContext().getBean("hotelInfoSaveManager");
		validateRequest();
		staticDataService = (HotelStaticDataService) HotelUtils.getStaticDataService();
	}

	private void validateRequest() {

		Set<String> fetchTypes = staticDataRequest.getFetchTypes();
		if (CollectionUtils.isEmpty(fetchTypes))
			throw new CustomGeneralException(SystemError.UNKNOWN_FETCH_TYPE);

		for (String fetchType : fetchTypes) {
			if (!(fetchType.equals("new") || fetchType.equals("updated") || fetchType.equals("deleted"))) {
				throw new CustomGeneralException(SystemError.UNKNOWN_FETCH_TYPE);
			}
		}
	}

	public void handleHotelStaticContent() throws IOException {

		log.info("Started fetching hotel static content to save/update for request {}", GSON.toJson(staticDataRequest));
		List<MasterFacilitiesResponse> masterFacilityResponse = getMasterFacilities();
		if (CollectionUtils.isNotEmpty(masterFacilityResponse)) {
			Map<Integer, String> masterFacilities = masterFacilityResponse.stream()
					.collect(Collectors.toMap(MasterFacilitiesResponse::getId, MasterFacilitiesResponse::getName));
			Set<String> supplierNames = staticDataRequest.getProviders();
			if (CollectionUtils.isNotEmpty(supplierNames)) {
				for (String supplierName : supplierNames) {
					log.info("Started fetching hotel static content to save/update for supplier {} and request {}",
							supplierName, GSON.toJson(staticDataRequest));
					String vervotechSupplierName = VervotechSupplierMapping
							.getVervotechSupplierTypeFromHotelSourceName(HotelSourceType.valueOf(supplierName))
							.getVervotechSupplierName();
					handleHotelStaticContent(masterFacilities, vervotechSupplierName);
					log.info("Finished fetching hotel static content to save/update for supplier {} and request {}",
							supplierName, GSON.toJson(staticDataRequest));
				}
			} else {
				handleHotelStaticContent(masterFacilities, null);
			}
		} else {
			log.info("Unable to fetch master facilities to save/update hotel static content for request {}",
					GSON.toJson(staticDataRequest));
		}
	}

	public void handleHotelStaticContent(Map<Integer, String> masterFacilities, String supplierName)
			throws IOException {

		int offset = Objects.isNull(staticDataRequest.getOffset()) ? 1 : staticDataRequest.getOffset();
		int unicaIdCount = 0;
		HttpUtilsV2 httpUtils = null;
		HotelIdentifierRequest hotelRequest = null;
		int saveOrUpdatedHotelCount = 0;
		int totalHotelCount = 0;
		/*
		 * Currently we can fetch 100 Unica Ids from Vervotech in a single api call. Therefore, we're expecting that we
		 * will be getting only 25L UnicaIds at max. If tomorrow it increases with new supplier integration then we will
		 * make it configurable.
		 */

		try {
			outer: for (int i = 0; i < 25000; i++) {
				int retryCount = 0;
				for (retryCount = 0; retryCount < 10; retryCount++) {
					int limit = Objects.nonNull(sourceConfig) && Objects.nonNull(sourceConfig.getSize())
							? Integer.parseInt(sourceConfig.getSize())
							: 100;
					try {
						hotelRequest = createRequestToFetchHotelIdentifier(offset, limit,
								isNew ? HOTEL_IDENTIFIER_REQUEST_NEW : HOTEL_IDENTIFIER_REQUEST_UPDATED, supplierName);
						httpUtils = VervotechUtil.getResponseURL(hotelRequest, supplierConf,
								VervotechConstant.HOTEL_SERVICE_TYPE);
						HotelIdentifierResponse hotelIdentifierResponse =
								httpUtils.getResponse(HotelIdentifierResponse.class).orElse(null);
						List<Mapping> mappings = hotelIdentifierResponse.getMappings();
						if (CollectionUtils.isNotEmpty(mappings)) {
							unicaIdCount += mappings.size();
							List<Long> unicaIds =
									mappings.stream().map(Mapping::getUnicaId).collect(Collectors.toList());
							List<HotelInfo> hotelInfos =
									fetchAndSaveHotelContent(unicaIds, masterFacilities, offset, limit, supplierName);
							saveOrUpdateHotelInfos(hotelInfos);
							totalHotelCount += getTotalHotelMapping(mappings);
							offset += limit;
							log.info(
									"Unica id fetched are {}. No. of hotels fetched {}. Unica Ids Count {}, Offset {}, Limit {}",
									unicaIds, totalHotelCount, unicaIdCount, offset, limit);
						} else {
							break outer;
						}
						break;
					} catch (Exception e) {
						try {
							Thread.sleep(80000);
						} catch (InterruptedException ie) {
							log.info("Unable to fetch unica hotel ids for url {}, headers {}. Offset {} and limit {}",
									httpUtils.getUrlString(), httpUtils.getHeaderParams(), offset, limit, ie);
							return;
						} finally {
							log.info("Unable to fetch unica hotel ids for url {}, headers {}. Offset {} and limit {}",
									httpUtils.getUrlString(), httpUtils.getHeaderParams(), offset, limit, e);
						}
					}
				}
				if (retryCount >= 3)
					break outer;
			}
		} finally {
			log.info(
					"Finished fetching hotel static content to save/update for request {}. Total no of unica ids fetched are {}, final response {}, headers {}. Final offset {} and limit {}",
					GSON.toJson(staticDataRequest), unicaIdCount, httpUtils.getResponseString(),
					httpUtils.getResponseHeaderParams(), hotelRequest.getOffset(), hotelRequest.getLimit());
		}
	}

	@SuppressWarnings("unchecked")
	public List<HotelInfo> fetchAndSaveHotelContent(List<Long> unicaIds, Map<Integer, String> masterFacilities,
			int offset, int limit, String supplierName) {

		List<List<Long>> unicaIdsInChunks = TgsCollectionUtils.chunkList(unicaIds, 10);
		List<HotelInfo> hotelInfos = new ArrayList<>();
		try {
			List<Future<?>> fetchHotelContentTasks = new ArrayList<Future<?>>();
			for (List<Long> unicaIdsChunk : unicaIdsInChunks) {
				Future<List<HotelInfo>> fetchHotelContentTask = fetchHotelContentPool.submit(() -> {
					return fetchHotelContent(unicaIdsChunk, masterFacilities, supplierName, offset, limit, null);
				});
				fetchHotelContentTasks.add(fetchHotelContentTask);
			}

			try {
				for (Future<?> fetchHotelContentTask : fetchHotelContentTasks) {
					Object hotelInfosInChunk = fetchHotelContentTask.get();
					if (Objects.nonNull(hotelInfosInChunk)) {
						hotelInfos.addAll((List<HotelInfo>) hotelInfosInChunk);
					}
				}
			} catch (InterruptedException | ExecutionException e) {
				log.info("Interrupted exception while fetching hotel content for unica ids {}. Offset {} and limit {}",
						unicaIds, offset, limit, e);
			}
		} finally {
			log.info("Finished fetching hotel content for {}", unicaIdsInChunks);
		}
		return hotelInfos;
	}

	public List<HotelInfo> fetchHotelContent(List<Long> unicaIds, Map<Integer, String> masterFacilities,
			String supplierName, int offset, int limit, String logId) {

		HttpUtilsV2 httpUtils = null;
		try {
			int retryCount = 0;
			for (retryCount = 0; retryCount < 5; retryCount++) {
				try {
					HotelContentRequest contentRequest =
							createRequestToFetchHotelStaticContent(new HashSet<>(unicaIds), supplierName);
					httpUtils = VervotechUtil.getResponseURLWithRequestBody(contentRequest, supplierConf,
							VervotechConstant.HOTEL_SERVICE_TYPE);
					httpUtils.setPrintResponseLog(false);
					HotelContentResponse hotelContentResponse =
							httpUtils.getResponse(HotelContentResponse.class).orElse(null);
					List<HotelInfo> hotelInfosInChunk =
							convertHotelContentToHotelInfo(hotelContentResponse, masterFacilities);
					log.info("No of hotel content fetched for unica ids {} are {} for offset {} and limit {}", unicaIds,
							hotelInfosInChunk.size(), offset, limit);
					return hotelInfosInChunk;
				} catch (Exception e) {
					try {
						Thread.sleep(60000);
					} catch (InterruptedException ie) {
						log.info("Unable to fetch unica hotel content for url {}, headers {}. Offset {} and limit {}",
								httpUtils.getUrlString(), httpUtils.getHeaderParams(), offset, limit, ie);
						return null;
					} finally {
						log.info("Unable to fetch unica hotel content for url {}, headers {}. Offset {} and limit {}",
								httpUtils.getUrlString(), httpUtils.getHeaderParams(), offset, limit, e);
					}
				}
			}
		} finally {
			if (StringUtils.isNotBlank(logId)) {
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(supplierName)
						.headerParams(httpUtils.getHeaderParams()).requestType(BaseHotelConstants.STATIC_DATA)
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString()).logKey(logId)
						.prefix("Vervotech")
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	private HotelIdentifierRequest createRequestToFetchHotelIdentifier(long offset, int limit, String suffix,
			String supplierName) {

		HotelIdentifierRequestBuilder identifierRequestBuilder = HotelIdentifierRequest.builder().suffixOfURL(suffix)
				.offset(String.valueOf(offset)).limit(String.valueOf(limit)).lastUpdateDateTime(
						StringUtils.isBlank(staticDataRequest.getLastUpdatedTime()) ? DEFAULT_LAST_UPDATED_TIME
								: staticDataRequest.getLastUpdatedTime());
		if (StringUtils.isNotBlank(supplierName)) {
			identifierRequestBuilder.providerFamily(supplierName);
		}
		return identifierRequestBuilder.build();
	}

	private HotelContentRequest createRequestToFetchHotelStaticContent(Set<Long> unicaIds, String supplierName) {

		HotelContentRequest contentRequest = HotelContentRequest.builder().suffixOfURL(HOTEL_STATIC_CONTENT_REQUEST)
				.opinionatedContent(true).unicaIds(unicaIds).build();
		if (StringUtils.isBlank(supplierName)) {
			contentRequest.setAllProvidersContent(true);
		} else {
			contentRequest.setProviderPreferences(new HashSet<>(Arrays.asList(supplierName)));
		}
		contentRequest.setReturnAllProviderHotelsForUnicaId(true);
		return contentRequest;
	}

	private List<HotelInfo> convertHotelContentToHotelInfo(HotelContentResponse hotelContentResponse,
			Map<Integer, String> masterFacilities) {

		List<HotelInfo> hotelInfos = new ArrayList<>();
		for (HotelContent hotelContent : hotelContentResponse.getHotels()) {
			List<ProviderHotel> providerHotels = hotelContent.getProviderHotels();
			OpinionatedContent opinionatedContent = getOpinionatedContent(hotelContent);
			if (Objects.nonNull(opinionatedContent)) {
				for (ProviderHotel providerHotel : providerHotels) {
					try {
						com.tgs.services.hms.datamodel.vervotech.Contact vervotechContact = providerHotel.getContact();
						com.tgs.services.hms.datamodel.vervotech.Address vervotechAddress =
								vervotechContact.getAddress();
						Address address = Address.builder().addressLine1(vervotechAddress.getLine1())
								.addressLine2(vervotechAddress.getLine2())
								.cityName(vervotechAddress.getCity())
								.stateName(vervotechAddress.getState())
								.city(City.builder().name(vervotechAddress.getCity()).code(vervotechAddress.getCode())
										.build())
								.state(State.builder().name(vervotechAddress.getState())
										.code(vervotechAddress.getStateCode()).build())
								.postalCode(vervotechAddress.getPostalCode()).build();

						if (StringUtils.isBlank(vervotechAddress.getCountry())) {
							address.setCountry(
									Country.builder().name(opinionatedContent.getAddress().getCountry().getName())
											.code(opinionatedContent.getAddress().getCountry().getCode()).build());
							address.setCountryName(opinionatedContent.getAddress().getCountry().getName());

						} else {
							address.setCountry(Country.builder().name(vervotechAddress.getCountry())
									.code(vervotechAddress.getCountryCode()).build());
							address.setCountryName(vervotechAddress.getCountry());
						}

						GeoLocation geolocation = null;
						if (Objects.nonNull(providerHotel.getGeoCode())) {

							geolocation =
									GeoLocation.builder().latitude(String.valueOf(providerHotel.getGeoCode().getLat()))
											.longitude(String.valueOf(providerHotel.getGeoCode().getLong())).build();
						}

						Contact contact = Contact.builder()
								.phone(CollectionUtils.isNotEmpty(vervotechContact.getPhones())
										? StringUtils.join(vervotechContact.getPhones(), ',')
										: null)
								.fax(vervotechContact.getFax())
								.email(CollectionUtils.isNotEmpty(vervotechContact.getEmails())
										? StringUtils.join(vervotechContact.getEmails())
										: null)
								.website(vervotechContact.getWeb()).build();

						List<Image> images = new ArrayList<>();
						if (CollectionUtils.isNotEmpty(providerHotel.getImages())) {
							providerHotel.getImages().forEach(image -> {
								image.getLinks().forEach(link -> {
									if (BooleanUtils.isNotTrue(link.getDisabled())) {
										images.add(Image.builder().bigURL(link.getProviderHref()).size(link.getSize())
												.roomIds(CollectionUtils.isNotEmpty(image.getRoomId())
														? image.getRoomId()
														: null)
												.build());
									}
								});
							});
						}

						HotelInfo hotelInfo = HotelInfo.builder().name(providerHotel.getName())
								.propertyType(providerHotel.getMasterPropertyType()).address(address).images(images)
								.geolocation(geolocation).contact(contact)
								.miscInfo(HotelMiscInfo.builder()
										.supplierStaticHotelId(providerHotel.getProviderHotelId()).build())
								.additionalInfo(HotelAdditionalInfo.builder().opinionatedContent(opinionatedContent)
										.isDisabled(providerHotel.getDisabled())
										.lastModifiedDateTime(
												LocalDateTime.parse(providerHotel.getLastModifiedDateTime()))
										.build())
								.supplierHotelId(providerHotel.getProviderHotelId())
								.supplierName(VervotechSupplierMapping.getVervotechSupplierTypeFromVervotechSourceName(
										providerHotel.getProviderFamily()).name())
								.unicaId(providerHotel.getUnicaId().toString()).build();

						hotelInfo.setProcessedOn(LocalDateTime.now());
						setOptions(providerHotel, hotelInfo, masterFacilities);

						if (CollectionUtils.isNotEmpty(providerHotel.getFacilities())) {
							List<String> facilities = new ArrayList<>();
							providerHotel.getFacilities().forEach(facility -> {
								if (Objects.isNull(facility.getGroupId())) {
									facilities.add(facility.getName());
								} else {
									facilities.add(masterFacilities.get(facility.getGroupId()));
								}
							});
							hotelInfo.setFacilities(facilities);
						}

						if (StringUtils.isNotBlank(providerHotel.getRating())) {
							hotelInfo.setRating((int) Double.parseDouble(providerHotel.getRating()));
						}

						if (Objects.nonNull(providerHotel.getPolicies())) {
							populateInstructions(providerHotel, hotelInfo);
						}

						if (CollectionUtils.isNotEmpty(providerHotel.getDescriptions())) {
							hotelInfo.setDescription(createDescription(providerHotel.getDescriptions()));
						}

						hotelInfo.setSupplierHotelId(providerHotel.getProviderHotelId());
						hotelInfo.setSupplierName(VervotechSupplierMapping
								.getVervotechSupplierTypeFromVervotechSourceName(providerHotel.getProviderFamily())
								.getHotelSourceType().name());
						hotelInfo.setUnicaId(providerHotel.getUnicaId().toString());
						if (CollectionUtils.isNotEmpty(providerHotel.getPointOfInterests())) {
							hotelInfo.getAdditionalInfo()
									.setPointOfInterests(getPointOfInterests(providerHotel.getPointOfInterests()));
						}
						hotelInfos.add(hotelInfo);
					} catch (Exception e) {
						log.info("Unable to convert provider content into hotel info {}", GSON.toJson(providerHotel),
								e);

					}
				}
			}
		}
		return hotelInfos;
	}

	private void setOptions(ProviderHotel providerHotel, HotelInfo hInfo, Map<Integer, String> masterFacilities) {
		List<Room> rooms = providerHotel.getRooms();
		List<Option> options = new ArrayList<>();
		Option option = Option.builder().miscInfo(OptionMiscInfo.builder().supplierId(hInfo.getSupplierName())
				.supplierHotelId(hInfo.getSupplierHotelId()).build()).build();
		List<RoomInfo> roomInfos = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(rooms)) {
			for (Room room : rooms) {

				RoomInfo roomInfo = new RoomInfo();
				roomInfo.setRoomType(room.getType());
				if (providerHotel.getProviderFamily().equalsIgnoreCase(HotelSourceType.FITRUUMS.name())
						&& room.getRoomId().contains(".")) {
					roomInfo.setId(room.getRoomId().substring(0, room.getRoomId().indexOf(".")));
				} else {
					roomInfo.setId(room.getRoomId());
				}

				if (StringUtils.isNotBlank(room.getDescription())) {
					roomInfo.setDescription(room.getDescription().replaceAll("\\<.*?\\>", "").trim());
				}

				if (CollectionUtils.isNotEmpty(room.getFacilities())) {
					List<String> facilities = new ArrayList<>();
					room.getFacilities().forEach(facility -> {
						if (Objects.isNull(facility.getGroupId())) {
							facilities.add(facility.getName());
						} else {
							facilities.add(masterFacilities.get(facility.getGroupId()));
						}
					});
					roomInfo.setRoomAmenities(facilities);
				}

				RoomAdditionalInfo roomAdditionalInfo = RoomAdditionalInfo.builder().build();
				roomAdditionalInfo.setMaxAdultAllowed(room.getMaxAdultAllowed());
				roomAdditionalInfo.setMaxChildrenAllowed(room.getMaxChildrenAllowed());
				roomAdditionalInfo.setMaxGuestAllowed(room.getMaxGuestAllowed());
				if (CollectionUtils.isNotEmpty(room.getBeds())) {
					List<Bed> beds = new ArrayList<>();
					for (com.tgs.services.hms.datamodel.vervotech.Bed vervotechBed : room.getBeds()) {
						Bed bed =
								Bed.builder().description(vervotechBed.getDescription()).count(vervotechBed.getCount())
										.size(vervotechBed.getSize()).type(vervotechBed.getType()).build();
						beds.add(bed);
					}
					roomAdditionalInfo.setBeds(beds);
				}

				if (Objects.nonNull(room.getArea())) {
					com.tgs.services.hms.datamodel.vervotech.Area vervotechArea = room.getArea();
					Area area = Area.builder().squareMeters(vervotechArea.getSquareMeters())
							.squareFeet(vervotechArea.getSquareFeet()).text(vervotechArea.getText()).build();
					roomAdditionalInfo.setArea(area);
				}
				roomAdditionalInfo.setViews(CollectionUtils.isNotEmpty(room.getViews()) ? room.getViews() : null);
				roomInfo.setRoomAdditionalInfo(roomAdditionalInfo);
				roomInfos.add(roomInfo);
			}
		}
		option.setRoomInfos(roomInfos);
		options.add(option);
		hInfo.setOptions(options);
	}

	private List<PointOfInterest> getPointOfInterests(
			List<com.tgs.services.hms.datamodel.vervotech.PointOfInterest> vervotechPointOfInterests) {

		List<PointOfInterest> pointOfInterests = new ArrayList<>();
		for (com.tgs.services.hms.datamodel.vervotech.PointOfInterest vervotechPointOfInterest : vervotechPointOfInterests) {
			PointOfInterest pointOfInterest = PointOfInterest.builder().distance(vervotechPointOfInterest.getDistance())
					.geoCode(vervotechPointOfInterest.getDistance()).name(vervotechPointOfInterest.getName())
					.type(vervotechPointOfInterest.getType()).unit(vervotechPointOfInterest.getUnit()).build();
			pointOfInterests.add(pointOfInterest);

		}
		return pointOfInterests;
	}

	private OpinionatedContent getOpinionatedContent(HotelContent hotelContent) {

		OpinionatedHotel opinionatedHotel = hotelContent.getOpinionatedHotel();
		OpinionatedContent opinionatedContent = null;
		try {
			if (Objects.nonNull(opinionatedHotel)) {
				opinionatedContent = new OpinionatedContent();
				com.tgs.services.hms.datamodel.vervotech.Address vervotechAddress = opinionatedHotel.getAddress();
				if (Objects.nonNull(vervotechAddress)) {
					Address address = Address.builder().addressLine1(vervotechAddress.getLine1())
							.addressLine2(vervotechAddress.getLine2())
							.city(City.builder().name(vervotechAddress.getCity()).code(vervotechAddress.getCode())
									.build())
							.state(State.builder().name(vervotechAddress.getState())
									.code(vervotechAddress.getStateCode()).build())
							.country(Country.builder().name(vervotechAddress.getCountry())
									.code(vervotechAddress.getCountryCode()).build())
							.postalCode(vervotechAddress.getPostalCode()).build();
					opinionatedContent.setAddress(address);
				}

				GeoCode geoCode = opinionatedHotel.getGeoCode();
				if (Objects.nonNull(geoCode)) {
					GeoLocation geolocation = GeoLocation.builder().latitude(String.valueOf(geoCode.getLat()))
							.longitude(String.valueOf(geoCode.getLong())).build();
					opinionatedContent.setGeolocation(geolocation);
				}
				opinionatedContent.setRating(opinionatedHotel.getRating());
				opinionatedContent.setPropertyType(opinionatedHotel.getPropertyType());
				opinionatedContent.setName(opinionatedHotel.getName());
			}
		} catch (Exception e) {
			log.info("Unable to process opinionated content for {}", GsonUtils.getGson().toJson(hotelContent), e);
		}
		return opinionatedContent;
	}

	private String createDescription(List<Description> descriptions) {

		Map<String, String> descriptionMap = new HashMap<>();
		for (Description description : descriptions) {
			descriptionMap.put(description.getType(), description.getText().replaceAll("\\<.*?\\>", ""));
		}

		return MapUtils.isNotEmpty(descriptionMap) ? GSON.toJson(descriptionMap) : null;
	}

	private void populateInstructions(ProviderHotel providerHotel, HotelInfo hotelInfo) {

		List<Instruction> instructions = new ArrayList<>();
		Map<String, String> checkinPolicyInstructionType = getCheckinPolicyInstruction(providerHotel.getCheckin());
		Map<String, String> policyInstructionType = getPolicies(providerHotel.getPolicies());
		Map<String, String> feesInstructionType = getHotelFees(providerHotel.getFees());

		if (MapUtils.isNotEmpty(policyInstructionType)) {
			instructions.add(Instruction.builder().type(InstructionType.POLICIES)
					.msg(GSON.toJson(policyInstructionType)).build());
		}

		if (MapUtils.isNotEmpty(checkinPolicyInstructionType)) {
			instructions.add(Instruction.builder().type(InstructionType.CHECKIN_INSTRUCTIONS)
					.msg(GSON.toJson(checkinPolicyInstructionType)).build());
		}

		if (MapUtils.isNotEmpty(feesInstructionType)) {
			instructions.add(
					Instruction.builder().type(InstructionType.FEES).msg(GSON.toJson(feesInstructionType)).build());
		}

		if (CollectionUtils.isNotEmpty(instructions)) {
			hotelInfo.setInstructions(instructions);
		}
	}

	private Map<String, String> getPolicies(List<Policy> policies) {

		Map<String, String> policyInstructionType = new HashMap<>();
		if (CollectionUtils.isNotEmpty(policies)) {

			for (Policy policy : policies) {
				if (StringUtils.isBlank(policy.getTitle())) {
					policy.setTitle("Additional Policy");
				}
			}

			Map<String, List<Policy>> groupByPolicyWithTitle =
					policies.stream().collect(Collectors.groupingBy(policy -> policy.getTitle()));

			groupByPolicyWithTitle.forEach((title, listOfText) -> {

				StringBuilder completePolicy = new StringBuilder();
				for (Policy policy : listOfText) {
					if (StringUtils.isNotBlank(policy.getText())) {
						String policyText = policy.getText().replaceAll("\\<.*?\\>", "").trim();
						if (policyText.charAt(policyText.length() - 1) != '.') {
							policyText = policyText + ". ";
						} else {
							policyText = policyText + " ";
						}
						completePolicy.append(policyText);
					}
				}
				if (completePolicy.length() > 0) {
					policyInstructionType.put(title, completePolicy.toString());
				}
			});
		}
		return policyInstructionType;
	}

	private Map<String, String> getHotelFees(List<Fee> fees) {

		Map<String, String> feesInstructionType = new HashMap<>();
		if (CollectionUtils.isNotEmpty(fees)) {

			for (Fee fee : fees) {
				if (StringUtils.isBlank(fee.getType())) {
					fee.setType("Additional Fee");
				}
			}

			Map<String, List<Fee>> groupByFeesWithType = fees.stream().collect(Collectors.groupingBy(f -> f.getType()));
			groupByFeesWithType.forEach((type, listOfFees) -> {

				StringBuilder completeFees = new StringBuilder();
				for (Fee fee : listOfFees) {
					if (StringUtils.isNotBlank(fee.getText())) {
						String feeText = fee.getText().replaceAll("\\<.*?\\>", "").trim();
						if (feeText.charAt(feeText.length() - 1) != '.') {
							feeText = feeText + ". ";
						} else {
							feeText = feeText + " ";
						}
						completeFees.append(feeText);
					}
				}
				if (completeFees.length() > 0) {
					feesInstructionType.put(type, completeFees.toString());
				}
			});
		}
		return feesInstructionType;
	}

	private Map<String, String> getCheckinPolicyInstruction(Checkin checkin) {

		Map<String, String> checkinPolicyInstructionType = new HashMap<>();

		List<String> checkinInstructions =
				Objects.nonNull(checkin) ? TgsCollectionUtils.getCleanStringList(checkin.getInstructions()) : null;

		if (CollectionUtils.isNotEmpty(checkinInstructions)) {

			StringBuilder completeInstruction = new StringBuilder();
			for (String instruction : checkinInstructions) {
				if (StringUtils.isNotBlank(instruction)) {
					instruction = instruction.replaceAll("\\<.*?\\>", "").trim();
					if (instruction.charAt(instruction.length() - 1) != '.') {
						instruction = instruction + ". ";
					} else {
						instruction = instruction + " ";
					}
					completeInstruction.append(instruction);
				}
			}
			if (completeInstruction.length() > 0) {
				checkinPolicyInstructionType.put("Instructions", completeInstruction.toString());
			}
		}

		List<String> checkinSpecialInstructions =
				Objects.nonNull(checkin) ? TgsCollectionUtils.getCleanStringList(checkin.getSpecialInstructions())
						: null;
		if (CollectionUtils.isNotEmpty(checkinSpecialInstructions)) {

			StringBuilder completeSpecialInstruction = new StringBuilder();
			for (String instruction : checkinSpecialInstructions) {
				if (StringUtils.isNotBlank(instruction)) {
					instruction = instruction.replaceAll("\\<.*?\\>", "").trim();
					if (instruction.charAt(instruction.length() - 1) != '.') {
						instruction = instruction + ". ";
					} else {
						instruction = instruction + " ";
					}
					completeSpecialInstruction.append(instruction);
				}
			}
			if (completeSpecialInstruction.length() > 0) {
				checkinPolicyInstructionType.put("Special Instructions", completeSpecialInstruction.toString());
			}
		}
		return checkinPolicyInstructionType;
	}

	@SuppressWarnings({"serial"})
	public List<MasterFacilitiesResponse> getMasterFacilities() throws IOException {

		HttpUtilsV2 httpUtils = null;
		try {
			List<MasterFacilitiesResponse> masterFacilities = null;
			VervotechBaseRequest contentRequest = createRequestToFetchMasterFacilities();
			httpUtils =
					VervotechUtil.getResponseURL(contentRequest, supplierConf, VervotechConstant.HOTEL_SERVICE_TYPE);
			httpUtils.getResponseFromType(new TypeToken<ArrayList<MasterFacilitiesResponse>>() {}.getType());
			String responseString = httpUtils.getResponseString();
			try {
				masterFacilities = GSON.fromJson(responseString,
						new TypeToken<ArrayList<MasterFacilitiesResponse>>() {}.getType());
				return masterFacilities;
			} catch (Exception e) {
				MasterFacilitiesResponse masterFacility = GSON.fromJson(responseString, MasterFacilitiesResponse.class);
				log.info("Unable to fetch hotel details due to {} and {}", masterFacility.getCode(),
						masterFacility.getMessage(), e);
			}
		} catch (IOException e) {
			log.info("Unable to fetch master facilities for url {}, headers {}", httpUtils.getUrlString(),
					httpUtils.getHeaderParams(), e);
		}
		return null;
	}

	public void deleteHotelStaticContent() throws IOException {

		log.info("Started fetching hotel static content to delete for request {}", GSON.toJson(staticDataRequest));
		Set<String> supplierNames = staticDataRequest.getProviders();
		if (CollectionUtils.isNotEmpty(supplierNames)) {
			for (String supplierName : supplierNames) {
				log.info("Started fetching hotel static content to delete for supplier {} and request {}", supplierName,
						GSON.toJson(staticDataRequest));
				String vervotechSupplierName = VervotechSupplierMapping
						.getVervotechSupplierTypeFromHotelSourceName(HotelSourceType.valueOf(supplierName))
						.getVervotechSupplierName();
				deleteHotelStaticContent(vervotechSupplierName);
				log.info("Finished fetching hotel static content to delete for supplier {} and request {}",
						supplierName, GSON.toJson(staticDataRequest));
			}
		} else {
			deleteHotelStaticContent(null);
		}
	}

	public void deleteHotelStaticContent(String supplierName) throws IOException {

		int offset = 1;
		int deletedHotelCount = 0;
		int totalHotelCount = 0;
		int unicaIdCount = 0;
		HttpUtilsV2 httpUtils = null;
		HotelIdentifierRequest hotelRequest = null;
		try {
			outer: for (int i = 0; i < 20000; i++) {
				for (int retryCount = 0; retryCount < 3; retryCount++) {
					int limit = Objects.nonNull(sourceConfig) && Objects.nonNull(sourceConfig.getSize())
							? Integer.parseInt(sourceConfig.getSize())
							: 100;
					try {
						hotelRequest = createRequestToFetchHotelIdentifier(offset, limit,
								HOTEL_IDENTIFIER_REQUEST_DELETED, supplierName);
						httpUtils = VervotechUtil.getResponseURL(hotelRequest, supplierConf,
								VervotechConstant.HOTEL_SERVICE_TYPE);
						httpUtils.setPrintResponseLog(false);
						HotelIdentifierResponse hotelIdentifierResponse =
								httpUtils.getResponse(HotelIdentifierResponse.class).orElse(null);
						List<Mapping> mappings = hotelIdentifierResponse.getMappings();
						if (CollectionUtils.isNotEmpty(mappings)) {
							List<Long> unicaIds =
									mappings.stream().map(Mapping::getUnicaId).collect(Collectors.toList());
							unicaIdCount += mappings.size();
							deletedHotelCount += deleteHotelMapping(mappings);
							totalHotelCount += getTotalHotelMapping(mappings);
							offset += limit;
							log.info(
									"Unica id fetched are {}. No of hotel ids fetched {} v/s deleted {}. Unica Ids Count {}, Offset {}, Limit {}",
									unicaIds, totalHotelCount, deletedHotelCount, unicaIdCount, offset, limit);
						} else {
							break outer;
						}
						break;
					} catch (Exception e) {
						log.info("Unable to fetch unica hotel ids for url {}, headers {}. Offset {} and limit {}",
								httpUtils.getUrlString(), httpUtils.getHeaderParams(), offset, limit, e);
					}
				}
			}
		} finally {
			log.info(
					"Finished fetching hotel static content to delete for request {}. Total no of unica ids fetched are {}, final response {}, headers {}. Final offset {} and limit {}",
					GSON.toJson(staticDataRequest), unicaIdCount, httpUtils.getResponseString(),
					httpUtils.getResponseHeaderParams(), hotelRequest.getOffset(), hotelRequest.getLimit());
		}
	}

	public int deleteHotelMapping(List<Mapping> mappings) {

		int deletedHotels = 0;
		for (Mapping mapping : mappings) {
			for (Hotel hotel : mapping.getHotels()) {
				try {
					String supplierName = VervotechSupplierMapping
							.getVervotechSupplierTypeFromVervotechSourceName(hotel.getProviderFamily())
							.getHotelSourceType().name();
					if (StringUtils.isNotBlank(staticDataRequest.getLastUpdatedTime())) {
						hotelInfoSaveManager.deleteAndCacheHotelInfo(String.valueOf(mapping.getUnicaId()), supplierName,
								hotel.getProviderHotelId(), staticDataService);
					} else {
						hotelInfoSaveManager.deleteHotelInfo(String.valueOf(mapping.getUnicaId()), supplierName,
								hotel.getProviderHotelId(), staticDataService);
					}
					deletedHotels++;
				} catch (Exception e) {
					log.info("Error while deleting hotel {}", hotel.getProviderHotelId(), e);
				}
			}
		}
		return deletedHotels;
	}

	private void saveOrUpdateHotelInfos(List<HotelInfo> hotelInfos) {

		for (HotelInfo hotelInfo : hotelInfos) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hotelInfo);
				if (StringUtils.isNotBlank(staticDataRequest.getLastUpdatedTime())) {
					hotelInfoSaveManager.saveAndCacheHotelInfo(dbHotelInfo, staticDataService);
				} else {
					hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, staticDataService);
				}
			} catch (Exception e) {
				log.info("Error while saving/updating hotel {}", GsonUtils.getGson().toJson(hotelInfo), e);
			}
		}
	}

	private static VervotechBaseRequest createRequestToFetchMasterFacilities() {

		return VervotechBaseRequest.builder().suffixOfURL(HOTEL_MASTER_FACILITIES_REQUEST).build();
	}

	private int getTotalHotelMapping(List<Mapping> mappings) {

		int deletedHotels = 0;
		if (CollectionUtils.isNotEmpty(mappings)) {
			for (Mapping mapping : mappings) {
				deletedHotels += mapping.getHotels().size();
			}
		}
		return deletedHotels;
	}
}
