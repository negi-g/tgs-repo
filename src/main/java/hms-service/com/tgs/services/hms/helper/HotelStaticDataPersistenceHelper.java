package com.tgs.services.hms.helper;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@InitializerGroup(group = InitializerGroup.Group.HOTEL)
public class HotelStaticDataPersistenceHelper extends InMemoryInitializer {

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelInfoService hotelInfoService;

	public HotelStaticDataPersistenceHelper() {
		super(null);
	}

	@Override
	public void process() {
		Runnable fetchStaticDataTask = () -> {

			HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();
			HotelGeneralPurposeOutput configuratorInfo =
					HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);

			final int limit = ObjectUtils.isEmpty(configuratorInfo.getStaticDataChunkSize()) ? 10000
					: configuratorInfo.getStaticDataChunkSize();

			final int concurrencyLevel =
					Math.min(20, ObjectUtils.isEmpty(configuratorInfo.getStaticDataConcurrencyLevel()) ? 2
							: configuratorInfo.getStaticDataConcurrencyLevel());
			final Object minAndMax[] = ((Object[]) hotelInfoService.findMinAndMaxIdHotelInfo()[0]);
			if (Objects.nonNull(configuratorInfo.getMinId())) {
				minAndMax[0] = configuratorInfo.getMinId();
			}

			if (Objects.nonNull(configuratorInfo.getMaxId())) {
				minAndMax[1] = configuratorInfo.getMaxId();
			}
			ThreadLocal<Long> from = new ThreadLocal<>();
			ThreadLocal<Long> to = new ThreadLocal<>();
			AtomicLong currFrom = new AtomicLong(0);
			AtomicLong currTo = new AtomicLong(0);
			AtomicInteger hotelCount = new AtomicInteger();
			try {
				if (!ObjectUtils.isEmpty(minAndMax) && !ObjectUtils.isEmpty(minAndMax[0])
						&& !ObjectUtils.isEmpty(minAndMax[1])) {

					final long THRESHOLD = (long) minAndMax[1];
					log.info("Started fetching hotel static data from database with limit {}, From {} - To {}", limit,
							minAndMax[0], (long) minAndMax[0] + limit);
					ExecutorService storeSearchResultExecutor = Executors.newFixedThreadPool(concurrencyLevel);
					for (int i = 0; i < 12000; i++) {
						storeSearchResultExecutor.submit(() -> {

							synchronized (this) {
								from.set((currFrom.get() == 0) ? (long) minAndMax[0] : currFrom.get());
								to.set((currTo.get() == 0) ? (long) minAndMax[0] + limit : currTo.get());
								currFrom.set(to.get() + 1);
								currTo.set(to.get() + limit);
							}

							if (from.get() > THRESHOLD) {
								log.info(
										"Finished fetching hotel static data from database as the threshold is reached. Final Hotel fetch count {}, From {} - To {}",
										hotelCount.get(), from.get(), to.get());
								return;
							}

							log.info(
									"Started fetching hotel static info from database. Total Hotel Count {}, From {} - To {}",
									hotelCount.get(), from.get(), to.get());
							List<DbHotelInfo> dbHotelInfo = hotelInfoService.findByIdBetween(from.get(), to.get());
							log.info(
									"Finished fetching hotel static info from database. Curr Hotel Count {}, Total Hotel Count {}, From {} - To {}",
									dbHotelInfo.size(), hotelCount.get(), from.get(), to.get());
							// List<HotelInfo> hotelInfoList = DbHotelInfo.toDomainList(dbHotelInfo);
							// log.info(
							// "Converted hotel static info from database into data model. Curr Hotel Count {}, Total
							// Hotel Count {}, From {} - To {}",
							// dbHotelInfo.size(), hotelCount.get(), from.get(), to.get());
							hotelCount.getAndAdd(dbHotelInfo.size());
							saveIntoCache(dbHotelInfo, staticDataService);
							log.info(
									"Persisted hotel static info into aerospike. Curr Hotel Count {}, Total Hotel Count {}, From {} - To {}",
									dbHotelInfo.size(), hotelCount.get(), from.get(), to.get());
							dbHotelInfo = null;
						});
					}
				} else {
					log.info("Unable to fetch hotel static info from database as the table is empty");
				}
			} finally {
				log.info(
						"Finished fetching hotel static data from database. Final Hotel fetch count {}, limit {}, concurrency level {}, From {} - To {}",
						hotelCount.get(), limit, concurrencyLevel, from.get(), to.get());
			}
		};

		Thread fetchStaticDataThread = new Thread(fetchStaticDataTask);
		fetchStaticDataThread.start();
	}

	private void saveIntoCache(List<DbHotelInfo> dbHotelInfoList, HotelStaticDataService staticDataService) {

		dbHotelInfoList.forEach(dbHotelInfo -> {
			try {
				cacheHandler.storeMasterHotel(dbHotelInfo.toBasicHotelInfo(), dbHotelInfo.toDetailHotelInfo(),
						staticDataService);
			} catch (Exception e) {
				log.info("Unable to save hotel static data into aerospike for id {}", dbHotelInfo.getId(), e);
			}
		});
	}

	@Override
	public void deleteExistingInitializer() {}

}

