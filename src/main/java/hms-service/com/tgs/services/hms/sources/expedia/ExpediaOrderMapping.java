package com.tgs.services.hms.sources.expedia;

import lombok.Getter;

@Getter
public enum ExpediaOrderMapping {

	BOOKED("SUCCESS"),
	PENDING("PENDING"),
	CANCELED("CANCELLED");
	
	public String getStatus() {
		return this.getCode();
	}

	private String code;

	ExpediaOrderMapping(String code) {
		this.code = code;
	}
}
