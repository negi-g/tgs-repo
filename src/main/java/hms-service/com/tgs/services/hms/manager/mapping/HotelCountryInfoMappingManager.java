package com.tgs.services.hms.manager.mapping;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.hms.datamodel.HotelCountryInfoMapping;
import com.tgs.services.hms.dbmodel.DbHotelCountryInfoMapping;
import com.tgs.services.hms.jparepository.HotelCountryInfoMappingService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelCountryInfoMappingManager {

	@Autowired
	HotelCountryInfoMappingService countryInfoService;

	@Autowired
	private GeneralServiceCommunicator gmsComm;

	private List<Document> docList;

	public void init() {

		docList = gmsComm.fetchGeneralRoleSpecificDocument(
				CollectionServiceFilter.builder().isConsiderHierarchy(true).key("HOTEL_COUNTRY").build());
	}

	public void saveHotelNationalityMappingInfoList(List<HotelCountryInfoMapping> hotelCountryInfoMappings)
			throws IOException {
		init();
		for (HotelCountryInfoMapping hotelCountryInfo : hotelCountryInfoMappings) {
			String countryId = null;
			if (!StringUtils.isBlank(countryId = isCountryExists(hotelCountryInfo)))
			saveNationalityMapping(hotelCountryInfo, countryId);
		}
	}

	private String isCountryExists(HotelCountryInfoMapping hotelCountryInfo) {
		return getCountryCodeFromCountryName(hotelCountryInfo.getSupplierCountryName());

	}

	private void saveNationalityMapping(HotelCountryInfoMapping countryInfoMapping, String countryId) {
		countryInfoMapping.setCountryId(countryId);

		DbHotelCountryInfoMapping dbNationalityinfoMapping = new DbHotelCountryInfoMapping().from(countryInfoMapping);
		try {
			dbNationalityinfoMapping = countryInfoService.save(dbNationalityinfoMapping);
			log.debug("New Country mapping object saved with id {}", dbNationalityinfoMapping.getId());
		} catch (Exception e) {
			DbHotelCountryInfoMapping oldMappingObject = countryInfoService.findByCountryIdAndSupplierName(
					dbNationalityinfoMapping.getCountryId(), dbNationalityinfoMapping.getSupplierName());
			Long id = oldMappingObject.getId();
			oldMappingObject = new GsonMapper<>(dbNationalityinfoMapping, oldMappingObject,
					DbHotelCountryInfoMapping.class).convert();
			oldMappingObject.setId(id);
			log.info("Updated City Info Mapping Object with cityId {} ,supplierName{} ",
					oldMappingObject.getCountryId(), oldMappingObject.getSupplierName());
			countryInfoService.save(oldMappingObject);

		}
	}

	public String getCountryCodeFromCountryName(String countryname) {

		String countryCode = "";

		if (CollectionUtils.isNotEmpty(docList)) {
			docList.get(0).getData();
			Map<String, List<Map<String, String>>> data = new Gson().fromJson(docList.get(0).getData(),
					new TypeToken<HashMap<String, List<Map<String, String>>>>() {}.getType());
			List<Map<String, String>> list = data.get("data");
			countryCode = list.stream().filter(row -> row.get("countryname").equalsIgnoreCase(countryname))
					.map(row -> row.get("countryid")).findFirst().orElse("");
		}
		return countryCode;
	}

	public HotelCountryInfoMapping getSupplierNationalityMapping(String countryId, String supplierName) {

		return countryInfoService.findByCountryIdAndSupplierName(countryId, supplierName).toDomain();

	}


}
