
package com.tgs.services.hms.manager.mapping;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.es.datamodel.ESSearchRequest;
import com.tgs.services.es.restmodel.ESAutoSuggestionResponse;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.ESHotel;
import com.tgs.services.hms.datamodel.HotelAdditionalInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMealQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.supplier.HotelMealBasis;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierMappingInfo;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.dbmodel.DbHotelMealBasis;
import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import com.tgs.services.hms.restmodel.HotelCacheSetRequest;
import com.tgs.services.hms.restmodel.HotelMealRequest;
import com.tgs.services.hms.service.DefaultStaticDataService;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelInfoSaveManager {

	@Autowired
	HotelInfoService infoService;

	@Autowired
	HotelRegionInfoService regionInfoService;

	@Autowired
	ElasticSearchCommunicator esCommunicator;

	@Autowired
	HotelCacheHandler cacheHandler;

	public void processHotelMealInfo(String filePath) throws Exception {
		List<Map<?, ?>> csvList = HotelUtils.getCSVListFromFile(filePath);
		saveHotelMealInfoBasis(csvList);
	}

	private void saveHotelMealInfoBasis(List<Map<?, ?>> csvList) {
		ExecutorService executor = Executors.newFixedThreadPool(10);
		for (Map<?, ?> csvEntry : csvList) {
			executor.submit(() -> {
				HotelMealBasis mealInfo = null;
				try {
					mealInfo = processHotelMealBasisInfo(csvEntry);
					saveHotelMealBasis(mealInfo);
				} catch (Exception e) {
					log.error("Unable to save meal basis for {}", mealInfo, e);
				}
			});
		}
	}

	public BulkUploadResponse saveHotelMealInfoList(HotelMealRequest unmappedMealRequest) throws Exception {
		if (unmappedMealRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(unmappedMealRequest.getUploadId());
		}

		String jobId = BulkUploadUtils.generateRandomJobId();
		final ContextData contextData = SystemContextHolder.getContextData();
		Runnable saveHotelMealInfoTask = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			for (HotelMealQuery mealInfo : unmappedMealRequest.getMealQuery()) {
				Future<?> f = executor.submit(() -> {
					SystemContextHolder.setContextData(contextData);
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(mealInfo.getRowId());
					try {
						saveHotelMealBasis(getMealBasisFromRequest(mealInfo));
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (RuntimeException e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error(
								"Unable to save hotel meal basis having supplier meal basis {} and frontend meal basis {}",
								mealInfo.getSMealBasis(), mealInfo.getFMealBasis(), e);
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, "hotelMealPersistenceHelper");
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting meal mappings for {} ", jobId, e);
			}
		};

		return BulkUploadUtils.processBulkUploadRequest(saveHotelMealInfoTask, unmappedMealRequest.getUploadType(),
				jobId);
	}

	private HotelMealBasis getMealBasisFromRequest(HotelMealQuery mealQuery) {
		return HotelMealBasis.builder().fMealBasis(mealQuery.getFMealBasis()).sMealBasis(mealQuery.getSMealBasis())
				.supplier(mealQuery.getSupplierName()).build();
	}

	private void saveHotelMealBasis(HotelMealBasis mealInfo) {
		if (StringUtils.isBlank(mealInfo.getSMealBasis()))
			throw new CustomGeneralException(SystemError.INVALID_SUPPLIER_MEAL_BASIS);

		if (StringUtils.isBlank(mealInfo.getFMealBasis()))
			throw new CustomGeneralException(SystemError.INVALID_FRONTEND_MEAL_BASIS);

		if (StringUtils.isBlank(mealInfo.getSupplier()))
			throw new CustomGeneralException(SystemError.NO_SUPPLIER_FOUND);

		mealInfo.setSMealBasis(mealInfo.getSMealBasis().trim().toUpperCase());
		mealInfo.setFMealBasis(mealInfo.getFMealBasis().trim().toUpperCase());
		mealInfo.setSupplier(mealInfo.getSupplier().trim().toUpperCase());
		DbHotelMealBasis dbMealBasis =
				infoService.findBySMealBasisAndSupplier(mealInfo.getSMealBasis(), mealInfo.getSupplier());
		if (ObjectUtils.isEmpty(dbMealBasis)) {
			dbMealBasis = new DbHotelMealBasis().from(mealInfo);
			dbMealBasis = infoService.save(dbMealBasis);
			log.debug("New hotel meal basis saved for {}", GsonUtils.getGson().toJson(dbMealBasis));
		} else {
			String oldFMealBasis = dbMealBasis.getFMealBasis();
			if (!oldFMealBasis.equals(mealInfo.getFMealBasis())) {
				dbMealBasis.setFMealBasis(mealInfo.getFMealBasis());
				log.debug("Updating frontend meal mapping from {} to {} for id {}", oldFMealBasis,
						mealInfo.getFMealBasis(), dbMealBasis.getId());
				dbMealBasis = infoService.save(dbMealBasis);
			}
		}
	}

	public Long saveHotelInfo(DbHotelInfo hotelInfo, HotelStaticDataService staticDataService) throws Exception {

		HotelInfo dummyHInfo = hotelInfo.toDomain();
		dummyHInfo.setOptions(new ArrayList<>(
				Arrays.asList(Option.builder().miscInfo(OptionMiscInfo.builder().supplierId(hotelInfo.getSupplierName())
						.supplierHotelId(hotelInfo.getSupplierHotelId()).build()).build())));
		DbHotelInfo oldDbObject = new DbHotelInfo().from(staticDataService.getHotelInfoFromDB(dummyHInfo));

		if (Objects.isNull(oldDbObject.getId())) {
			removeMiscInfo(hotelInfo.getOptions());
			hotelInfo = infoService.save(hotelInfo);
			log.debug("New hotel info object saved is {}", hotelInfo.getId());
			updateMapping(hotelInfo, hotelInfo.getSupplierHotelId(), hotelInfo.getSupplierName(),
					HotelSourceType.valueOf(hotelInfo.getSupplierName()), staticDataService);
			return hotelInfo.getId();
		} else {
			Long oldId = oldDbObject.getId();
			List<Instruction> updatedInstructions = getUpdatedInstructions(oldDbObject, hotelInfo);
			List<Image> imageList = getUpdatedImageList(oldDbObject, hotelInfo);

			DbHotelInfo updatedDbObject = new GsonMapper<>(hotelInfo, oldDbObject, DbHotelInfo.class).convert();
			updatedDbObject.setId(oldId);
			updatedDbObject.setInstructions(updatedInstructions);
			updatedDbObject.setImages(imageList);
			updatedDbObject.setAddress(hotelInfo.getAddress());
			log.debug("Updated Hotel Info Object hotelName {}, id {}", updatedDbObject.getName(),
					updatedDbObject.getId());
			infoService.save(updatedDbObject);
			updateMapping(updatedDbObject, updatedDbObject.getSupplierHotelId(), updatedDbObject.getSupplierName(),
					HotelSourceType.valueOf(updatedDbObject.getSupplierName()), staticDataService);
			return oldId;
		}
	}

	private void removeMiscInfo(List<Option> options) {

		if (CollectionUtils.isNotEmpty(options)) {
			for (Option option : options) {
				option.setMiscInfo(null);
			}
		}
	}

	public void saveAndCacheHotelInfo(DbHotelInfo dbHotelInfo, HotelStaticDataService staticDataService)
			throws Exception {

		Long dbHotelId = saveHotelInfo(dbHotelInfo, staticDataService);
		dbHotelInfo.setId(dbHotelId);

		HotelInfo hotelInfo = dbHotelInfo.toDomain();
		hotelInfo.setId(String.valueOf(dbHotelId));
		cacheHandler.storeMasterHotel(dbHotelInfo.toBasicHotelInfo(), dbHotelInfo.toDetailHotelInfo(),
				staticDataService);
	}

	private List<Image> getUpdatedImageList(DbHotelInfo oldDbObject, DbHotelInfo hotelInfo) {

		if (CollectionUtils.isEmpty(oldDbObject.getImages()))
			return hotelInfo.getImages();
		if (CollectionUtils.isEmpty(hotelInfo.getImages()))
			return oldDbObject.getImages();

		return hotelInfo.getImages().size() < oldDbObject.getImages().size() ? oldDbObject.getImages()
				: hotelInfo.getImages();

	}

	private List<Instruction> getUpdatedInstructions(DbHotelInfo oldDbObject, DbHotelInfo hotelInfo) {

		List<Instruction> oldInstructions = oldDbObject.getInstructions();
		List<Instruction> newInstructions = hotelInfo.getInstructions();
		List<Instruction> updatedInstruction = new ArrayList<>();
		if (CollectionUtils.isEmpty(oldInstructions)) {
			return newInstructions;
		}
		if (CollectionUtils.isEmpty(newInstructions)) {
			return oldInstructions;
		}
		for (Instruction inst : oldInstructions) {
			if (!containsInstructionType(newInstructions, inst.getType())) {
				updatedInstruction.add(inst);
			}
		}
		updatedInstruction.addAll(newInstructions);
		return updatedInstruction;
	}

	private boolean containsInstructionType(List<Instruction> instructions, InstructionType type) {
		return instructions.stream().filter(inst -> inst.getType().equals(type)).findFirst().isPresent();
	}

	private void updateMapping(DbHotelInfo dbHotelInfo, String supplierHotelId, String supplierName,
			HotelSourceType sourceType, HotelStaticDataService staticDataService) {
		if (staticDataService instanceof DefaultStaticDataService) {
			try {
				infoService.save(new DbHotelSupplierMapping().from(
						HotelSupplierMappingInfo.builder().hotelId(dbHotelInfo.getId()).supplierHotelId(supplierHotelId)
								.sourceName(sourceType.name()).supplierName(supplierName).build()));
			} catch (DataIntegrityViolationException e) {
				log.debug("Hotel supplier mapping already exists for hotel {}, supplier {} and constraint {}",
						dbHotelInfo.getId(), sourceType.name(), e.getMessage());
			}
		}
	}

	public String getStringValue(Object obj) {
		return obj == null ? null : obj.toString();
	}

	private HotelMealBasis processHotelMealBasisInfo(Map<?, ?> csvEntry) {
		HotelMealBasis mealInfo = HotelMealBasis.builder()
				.fMealBasis(String.valueOf(csvEntry.get("Frontend meal basis")))
				.supplier(String.valueOf(csvEntry.get("Supplier")))
				.sMealBasis(String.valueOf(csvEntry.get("Supplier meal basis"))).createdOn(LocalDateTime.now()).build();
		return mealInfo;
	}

	public HotelInfo saveMasterHotel(HotelStaticDataRequest staticDataRequest,
			HotelStaticDataService staticDataService) {

		HotelInfo hInfo = staticDataRequest.getHInfo();
		try {
			DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hInfo);
			dbHotelInfo = infoService.save(dbHotelInfo);
			cacheHandler.storeMasterHotel(dbHotelInfo.toBasicHotelInfo(), dbHotelInfo.toDetailHotelInfo(),
					staticDataService);
			esCommunicator.addDocument(getElasticSearchModelFromHotelInfo(dbHotelInfo.toDomain()), ESMetaInfo.HOTELINV);
			return dbHotelInfo.toDomain();
		} catch (Exception e) {
			log.error("Error While Storing Master Hotel With Name {}", hInfo.getName(), hInfo.getRating(), e);
			throw new CustomGeneralException(SystemError.ERROR_STORING_MASTER_HOTEL);
		}
	}

	public HotelInfo modifyMasterHotel(HotelStaticDataRequest staticDataRequest) {

		HotelInfo modifiedHInfo = staticDataRequest.getHInfo();
		try {
			DbHotelInfo oldDbHotel = infoService.findById(Long.valueOf(modifiedHInfo.getId()));
			DbHotelInfo updatedDbObject = new GsonMapper<>(modifiedHInfo, oldDbHotel, DbHotelInfo.class).convert();
			updatedDbObject.setId(oldDbHotel.getId());
			log.debug("Updated Hotel Info Object hotelName {}, id {}", updatedDbObject.getName(),
					updatedDbObject.getId());
			infoService.save(updatedDbObject);
			esCommunicator.addDocument(getElasticSearchModelFromHotelInfo(updatedDbObject.toDomain()),
					ESMetaInfo.HOTELINV);
			return updatedDbObject.toDomain();
		} catch (Exception e) {
			log.error("Error While Storing Master Hotel With Name {}, rating {}", modifiedHInfo.getName(),
					modifiedHInfo.getRating(), e);
			throw new CustomGeneralException(SystemError.ERROR_MODIFYING_MASTER_HOTEL);
		}
	}

	public void syncMasterHotelsWithElasticSearch() {

		List<ESHotel> esHotels = new ArrayList<>();
		for (int i = 0; i < 10000; i++) {
			log.info("Fetching Master Hotel partialInfo page {}", i);
			Pageable page = new PageRequest(i, 10000, Direction.DESC, "id");
			List<Object[]> list = infoService.findHotelNameIdAddress(page);
			if (CollectionUtils.isEmpty(list))
				break;
			list.stream().forEach(obj -> {
				Address hotelAddress = (Address) obj[2];
				String cityName = HotelUtils.getCityNameFromAddress(hotelAddress);
				String countryName = HotelUtils.getCountryNameFromAddress(hotelAddress);
				String rating = null;
				if (!Objects.isNull(obj[3]))
					rating = String.valueOf(obj[3]);
				ESHotel esHotel = ESHotel.builder().id(String.valueOf(obj[0])).name(String.valueOf(obj[1]))
						.cityName(cityName).countryName(countryName).rating(rating)
						.address(hotelAddress.getAddressLine1()).build();
				esHotels.add(esHotel);
			});
			esCommunicator.addBulkDocuments(esHotels, ESMetaInfo.HOTELINV);
			esHotels.clear();
			log.info("Fetched page {}", i);
		}
	}

	public HotelInfo fetchFromDb(String id) {
		DbHotelInfo dbHotel = infoService.findById(Long.valueOf(id));
		if (Objects.isNull(dbHotel)) {
			log.debug("Master Hotel Not Found for id {}", id);
			throw new CustomGeneralException(SystemError.MASTER_HOTEL_NOT_FOUND);
		}
		return dbHotel.toDomain();
	}

	private ESHotel getElasticSearchModelFromHotelInfo(HotelInfo hInfo) {

		String rating = null;
		Address address = hInfo.getAddress();
		String addressLine1 = address != null ? address.getAddressLine1() : null;
		String cityName = HotelUtils.getCityNameFromAddress(address);
		String countryName = HotelUtils.getCountryNameFromAddress(address);

		if (!Objects.isNull(hInfo.getRating()))
			rating = String.valueOf(hInfo.getRating());

		ESHotel esHotel = ESHotel.builder().name(hInfo.getName()).cityName(cityName).countryName(countryName)
				.rating(rating).address(addressLine1).id(hInfo.getId()).build();
		return esHotel;
	}

	public List<ESHotel> fetchHotelDataFromElasticSearch(HotelInfo hInfo) {

		ESHotel esHotel = getElasticSearchModelFromHotelInfo(hInfo);
		String source = GsonUtils.getGson().toJson(esHotel);
		ESSearchRequest request =
				ESSearchRequest.builder().source(source).metaInfo(ESMetaInfo.HOTELINV.getType()).build();
		ESAutoSuggestionResponse response = esCommunicator.getSuggestions(request);
		List<ESHotel> esHotels = new ArrayList<>();

		for (Map<String, ? extends Object> map : response.getSuggestions()) {
			ESHotel hotelData = ESHotel.builder().id((String) map.get("id")).name((String) map.get("name"))
					.cityName((String) map.get("cityName")).countryName((String) map.get("countryName"))
					.address((String) map.get("address")).rating((String) map.get("rating")).build();
			esHotels.add(hotelData);
		}
		return esHotels;
	}

	public List<HotelInfo> fetchHotelWithBasicInfoFromMasterHotelIdList(List<Long> hotelIdList) {

		List<Object[]> objectList = infoService.findHotelBasicInfoByIdIn(hotelIdList);
		if (CollectionUtils.isEmpty(objectList))
			return new ArrayList<>();
		List<HotelInfo> hInfoList = new ArrayList<>();
		objectList.forEach((obj -> {
			Integer rating = null;
			if (!Objects.isNull(obj[3]))
				rating = Integer.valueOf(String.valueOf(obj[3]));
			Address hotelAddress = (Address) obj[2];
			HotelInfo hInfo = HotelInfo.builder().id(String.valueOf(obj[0])).name(String.valueOf(obj[1]))
					.address(hotelAddress).rating(rating).build();
			hInfoList.add(hInfo);
		}));
		return hInfoList;
	}

	public void saveHotelInfos(HotelStaticDataRequest staticDataRequest) {

		List<HotelInfo> hotelInfos = staticDataRequest.getHotels();
		HotelSourceType sourceType = HotelSourceType.getHotelSourceType(staticDataRequest.getSourceId());

		for (HotelInfo hotelInfo : hotelInfos) {
			DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hotelInfo);
			dbHotelInfo.setSupplierHotelId(hotelInfo.getMiscInfo().getSupplierStaticHotelId());
			dbHotelInfo.setSupplierName(sourceType.name());
		}
	}

	public DbHotelInfo deleteHotelInfo(String unicaId, String supplierName, String supplierHotelId,
			HotelStaticDataService staticDataService) {

		try {
			HotelInfo dummyHInfo = HotelInfo.builder().unicaId(unicaId).build();
			dummyHInfo.setOptions(new ArrayList<>(Arrays.asList(Option.builder()
					.miscInfo(
							OptionMiscInfo.builder().supplierId(supplierName).supplierHotelId(supplierHotelId).build())
					.build())));
			DbHotelInfo dbHotelInfo = new DbHotelInfo().from(staticDataService.getHotelInfoFromDB(dummyHInfo));

			if (Objects.nonNull(dbHotelInfo.getId())) {
				if (Objects.isNull(dbHotelInfo.getAdditionalInfo())) {
					dbHotelInfo.setAdditionalInfo(HotelAdditionalInfo.builder().isDisabled(true).build());
				} else {
					dbHotelInfo.getAdditionalInfo().setIsDisabled(true);
				}
				dbHotelInfo.setProcessedOn(LocalDateTime.now());
				log.debug("Successfully deleted hotel info for id {}", dbHotelInfo.getId());
				infoService.save(dbHotelInfo);
				return dbHotelInfo;
			}
		} catch (Exception e) {
			log.info("Unable to delete hotel info for unica id {}, supplierName {}, supplierHotelId {}", unicaId,
					supplierName, supplierHotelId, e.getMessage());
		}
		return null;
	}

	public void deleteAndCacheHotelInfo(String unicaId, String supplierName, String supplierHotelId,
			HotelStaticDataService staticDataService) {

		DbHotelInfo dbHotelInfo = deleteHotelInfo(unicaId, supplierName, supplierHotelId, staticDataService);
		// if (Objects.nonNull(dbHotelInfo)) {
		// Long id = dbHotelInfo.getId();
		// HotelInfo hotelInfo = dbHotelInfo.toDomain();
		// hotelInfo.setId(String.valueOf(id));
		// if (cacheHandler.deleteMasterHotel(hotelInfo, staticDataService)) {
		// log.debug("Successfully deleted hotel info from cache for id {}", dbHotelInfo.getId());
		// }
		// }
	}

	public List getUncompressedDataSetFromCache(HotelCacheSetRequest hotelCacheSetRequest) {
		if (Objects.isNull(hotelCacheSetRequest.getKeys())) {
			log.info("Null key found for request {}", new Gson().toJson(hotelCacheSetRequest));
			return null;
		}
		return cacheHandler.getUncompressedDataSet(hotelCacheSetRequest);
	}
}
