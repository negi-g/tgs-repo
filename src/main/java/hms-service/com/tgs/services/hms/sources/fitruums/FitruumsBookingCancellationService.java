package com.tgs.services.hms.sources.fitruums;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.amazonaws.util.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.fitruums.book.BookingCancellationResult;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Service
public class FitruumsBookingCancellationService extends FitruumsBaseService {
	private Order order;
	private HotelInfo hInfo;

	public boolean cancelBooking() throws IOException {
		HttpUtils httpUtils = null;
		HashMap<String, String> cancelBookRequestMap = getBookingCancelMap(hInfo.getOptions().get(0));
		FitruumsMarshaller fitruumsMarshaller = new FitruumsMarshaller();
		BookingCancellationResult cancellationResult = null;
		String xmlResponse = null;
		try {
			listener = new RestAPIListener("");
			String cancelBookRequest = getKeyValuePair(cancelBookRequestMap);
			httpUtils = FitruumsUtil.getHttpUtils(cancelBookRequest, HotelUrlConstants.CANCEL_BOOKING_URL, null,
					supplierConf);
			xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (!StringUtils.isNullOrEmpty(xmlResponse)) {
				cancellationResult = fitruumsMarshaller.unmarshallXML(xmlResponse, BookingCancellationResult.class);
				if (String.valueOf(cancellationResult.getCode())
						.equals(FitruumsConstant.SUCCESSBOOKINGCANCELLATIONCODE.getValue())) {
					return true;
				}
			}
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(cancellationResult)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.FITRUUMS.name())
					.requestType(BaseHotelConstants.BOOKING_CANCELLATION).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

		}
		return false;
	}

	private HashMap<String, String> getBookingCancelMap(Option option) {
		HashMap<String, String> searchRequestmap = new HashMap<>();
		searchRequestmap = setBasicRequirement(searchRequestmap);
		searchRequestmap.put("bookingID", hInfo.getMiscInfo().getSupplierBookingId());
		return searchRequestmap;
	}
}
