package com.tgs.services.hms.jparepository.inventory;

import java.time.LocalDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.tgs.services.hms.dbmodel.inventory.DbHotelRatePlan;

@Repository
public interface HotelRatePlanRepository
		extends JpaRepository<DbHotelRatePlan, Long>, JpaSpecificationExecutor<DbHotelRatePlan> {


	public DbHotelRatePlan findBySupplierIdAndSupplierHotelIdAndRoomCategoryIdAndMealBasisIdAndRoomTypeIdAndValidOnAndIsDeleted(
			String supplierId, String supplierHotelId, String roomCategoryId, String mealBasisId, String roomTypeId,
			LocalDate validOn, boolean isDeleted);
}
