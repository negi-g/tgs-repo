package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class QTechHotelBookingCancellationFactory extends AbstractHotelBookingCancellationFactory {

	public QTechHotelBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo, Order order) {
		super(supplierConf, hInfo, order);
	}

	@Override
	public boolean cancelHotel() throws IOException {
		QTechBookingCancellationService cancellationService = QTechBookingCancellationService.builder().supplierConf(supplierConf)
				.sourceConfigOutput(sourceConfigOutput).hInfo(hInfo).order(order).build();
		return cancellationService.cancelBooking();
	}

	@Override
	public boolean getCancelHotelStatus() {
		return false;
	}

}
