package com.tgs.services.hms.sources.hotelbeds;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsRateCheckRequest;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsRateCheckResponse;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@SuperBuilder
public class HotelBedsPriceValidationService extends HotelBedsBaseService {

	private String bookingId;
	HotelBedsRateCheckResponse priceValidationResponse;
	public Option updatedOption;

	public void validate(HotelInfo hInfo) throws Exception {
		HttpUtilsV2 httpUtils = null;
		listener = new RestAPIListener("");
		try {
			HotelBedsRateCheckRequest priceValidationRequest = getRateCheckRequest(hInfo.getOptions().get(0));
			httpUtils = HotelBedsUtils.getHttpUtils(priceValidationRequest, null, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.BLOCK_ROOM_URL));
			priceValidationResponse = httpUtils.getResponse(HotelBedsRateCheckResponse.class).orElse(null);
			updateOptionWithNewPrice(priceValidationResponse, hInfo.getOptions().get(0));
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.REVIEW.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(priceValidationResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.HOTELBEDS.name())
						.requestType(BaseHotelConstants.PRICE_CHECK).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(bookingId)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private void updateOptionWithNewPrice(HotelBedsRateCheckResponse priceValidationResponse, Option option) {

		List<String> rateKeyList = new ArrayList<>();
		if (!ObjectUtils.isEmpty(priceValidationResponse.getError())) {
			throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
		}
		currency = priceValidationResponse.getHotel().getCurrency();
		priceValidationResponse.getHotel().getRooms().forEach(room -> {
			room.getRates().forEach(rate -> {
				rateKeyList.add(rate.getRateKey());
			});
		});
		List<Option> optionList = createOptionFromHotel(priceValidationResponse.getHotel(), true);
		option.getRoomInfos().forEach(room -> {
			{
				if (!rateKeyList.contains(room.getMiscInfo().getRatePlanCode())) {
					throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
				}
			}
		});

		updatedOption = optionList.get(0);
		option.setInstructions(updatedOption.getInstructions());
	}
}
