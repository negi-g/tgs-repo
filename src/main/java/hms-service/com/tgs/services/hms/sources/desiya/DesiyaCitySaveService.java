package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FilenameUtils;

import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;

import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class DesiyaCitySaveService extends DesiyaBaseService {

	private HotelRegionInfoMappingManager regionInfoMappingManager;
	private HotelStaticDataRequest staticDataRequest;
	private HotelSupplierRegionInfoManager supplierRegionInfoManager;


	public void saveDesiyaCities() throws IOException {

		String zipFolderUrl = supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_STATIC_DATA);
		HttpURLConnection connection = getHttpURLConnection(zipFolderUrl);
		InputStream in = connection.getInputStream();
		ZipInputStream zipIn = new ZipInputStream(in);
		ZipEntry entry = zipIn.getNextEntry();

		while ((entry = zipIn.getNextEntry()) != null) {
			if (FilenameUtils.getExtension(entry.getName()).equals("xml")
					&& FilenameUtils.getName(entry.getName()).equals("CityData.xml")) {
				DesiyaCitySaxParser saxParser =
						new DesiyaCitySaxParser(zipIn, regionInfoMappingManager, staticDataRequest,
								supplierRegionInfoManager);
				zipIn.close();
				break;
			}
		}

	}

}
