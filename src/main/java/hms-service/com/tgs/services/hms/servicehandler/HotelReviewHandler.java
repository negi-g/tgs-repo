package com.tgs.services.hms.servicehandler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.ImmutableMap;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.gson.FieldExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.FieldType;
import com.tgs.services.hms.datamodel.HotelBookingConditions;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelStaticDataChangeAlert;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSoldOut;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelInfoFilterationOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelCancellationPolicySearch;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.helper.HotelPriceValidation;
import com.tgs.services.hms.helper.analytics.HotelAnalyticsHelper;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.manager.HotelDetailManager;
import com.tgs.services.hms.manager.HotelSearchManager;
import com.tgs.services.hms.manager.HotelSearchResultProcessingManager;
import com.tgs.services.hms.mapper.HotelMethodExecutionInfoMapper;
import com.tgs.services.hms.mapper.HotelReviewToAnalyticsHotelReviewMapper;
import com.tgs.services.hms.mapper.HotelStaticDataChangeToAnalyticsHotelStaticDataMapper;
import com.tgs.services.hms.restmodel.HotelReviewRequest;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelReviewHandler extends ServiceHandler<HotelReviewRequest, HotelReviewResponse> {

	@Autowired
	HotelDetailManager detailManager;

	@Autowired
	HotelInfoService infoService;

	@Autowired
	HotelSearchManager searchManager;

	@Autowired
	HMSCachingServiceCommunicator cachingService;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelCancellationPolicySearch search;

	@Autowired
	HotelAnalyticsHelper analyticsHelper;

	@Autowired
	HotelSearchResultProcessingManager processingManager;

	@Autowired
	HotelPriceValidation priceValidation;

	@Autowired
	GeneralServiceCommunicator gmsComm;

	@Autowired
	MsgServiceCommunicator msgComm;

	private long startTime;

	@Override
	public void beforeProcess() throws Exception {

		HotelInfo hInfo = null;
		HotelSearchQuery searchQuery = null;
		String bookingId = null;
		startTime = System.currentTimeMillis();
		try {
			contextData.setValue(BaseHotelConstants.METHOD_EXECUTION_TYPE,
					BooleanUtils.isNotFalse(request.getPriceValidation()) ? BaseHotelConstants.PRICE_VALIDATION
							: BaseHotelConstants.NO_PRICE_VALIDATION);
			bookingId = HotelUtils.firstNonEmpty(request.getBookingId(),
					ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.HOTEL).build()));
			hInfo = cacheHandler.getCachedHotelById(request.getHotelId());
			String searchId = HotelUtils.getSearchId(request.getHotelId());
			searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
			if (Objects.isNull(searchQuery)) {
				throw new CustomGeneralException(SystemError.SEARCH_ID_NOT_FOUND);
			}

			Option oldOption = HotelUtils.filterOptionById(hInfo, request.getOptionId());
			if (Objects.isNull(oldOption)) {
				throw new CustomGeneralException(SystemError.OPTION_NO_LONGER_AVAILABLE,
						StringUtils.join(" You can use booking Id ", bookingId, " for reference"));
			}
			searchQuery.setSourceId(oldOption.getMiscInfo().getSourceId());
			searchQuery.setMiscInfo(
					HotelSearchQueryMiscInfo.builder().supplierId(oldOption.getMiscInfo().getSupplierId()).build());
			updateForRetry();
		} catch (CustomGeneralException e) {
			String message = StringUtils.join("Failed to review hotelId", request.getHotelId(), ", optionId ",
					request.getOptionId(), ", bookingId ", bookingId);
			RoomSoldOut soldOut = RoomSoldOut.builder().type(AlertType.SOLDOUT.name()).message(message).build();
			response.addAlert(soldOut);
			response.setStatus(new Status(HttpStatusCode.HTTP_200));
			response.setQuery(searchQuery);
			log.error("Failed to review hotelId {}, optionId {},error msg is {}", request.getHotelId(),
					request.getOptionId(), contextData.getErrorMessages(), e);

			if (Objects.nonNull(e.getError()) && e.getError().equals(SystemError.INVALID_HOTEL_STATIC_DATA)) {
				contextData.getErrorMessages().add(SystemError.INVALID_HOTEL_STATIC_DATA.getMessage());
			} else {
				contextData.getErrorMessages().add("Failed to review hotel because hotel is sold out");
			}
		} finally {
			response.setHInfo(hInfo);
			response.setQuery(searchQuery);
			response.setBookingId(bookingId);
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
					System.currentTimeMillis() - startTime);
		}
	}

	@Override
	public void process() throws Exception {
		String bookingId = response.getBookingId();
		HotelInfo hInfo = response.getHInfo();
		HotelSearchQuery searchQuery = response.getQuery();
		try {
			if (BooleanUtils.isNotFalse(request.getPriceValidation())) {
				try {

					HotelInfo oldHotel = new GsonMapper<>(hInfo, HotelInfo.class).convert();
					String searchId = HotelUtils.getSearchId(request.getHotelId());
					searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
					if (Objects.isNull(searchQuery)) {
						throw new CustomGeneralException(SystemError.SEARCH_ID_NOT_FOUND);
					}
					Option oldOption = HotelUtils.filterOptionById(hInfo, request.getOptionId());
					searchQuery.setSourceId(oldOption.getMiscInfo().getSourceId());
					searchQuery.setMiscInfo(HotelSearchQueryMiscInfo.builder()
							.supplierId(oldOption.getMiscInfo().getSupplierId()).build());
					oldOption.setCancellationPolicy(null);
					boolean isToSendFareDifference = isToShowFareDiff(searchQuery);
					Alert fareChange = null;
					ContextData copyContextData = contextData.deepCopy();
					if (Objects.nonNull(hInfo) && hInfo.getMiscInfo().getSearchKeyExpiryTime() != null && hInfo
							.getMiscInfo().getSearchKeyExpiryTime().isBefore(LocalDateTime.now().plusMinutes(6))) {
						HotelSearchManager.setSearchId(searchQuery);
						cacheHandler.storeSearchQueryInCache(searchQuery);
						log.info(
								"Doing new search  for source {} because  key will expire in 10 minutes on review page for hotel {} "
										+ " searchId {}",
								oldOption.getMiscInfo().getSourceId(), hInfo.getName(), searchQuery.getSearchId());

						hInfo.setOptions(new ArrayList<>(Arrays.asList(oldOption)));
						HotelInfo newHInfo =
								searchManager.doSearchAgainWithSameSuppliers(searchQuery, hInfo, contextData);
						fetchHotelDetailsIfRequired(newHInfo, searchQuery);
						Option filteredOption = HotelUtils.filterOptionByPriceAndRoomCategory(newHInfo, oldOption);

						if (Objects.isNull(filteredOption)) {
							log.error("Requested Option not available now for hotel {}", hInfo);
							contextData.getErrorMessages()
									.add("Option not available in new search result after filtering");
							throw new CustomGeneralException(SystemError.OPTION_NO_LONGER_AVAILABLE,
									StringUtils.join(" You can use booking Id ", bookingId, " for reference"));
						}

						detailManager.fetchHotelDetails(searchQuery, newHInfo, contextData);
						newHInfo.setOptions(new ArrayList<>(Arrays.asList(filteredOption)));
						SystemContextHolder.setContextData(copyContextData);
						search.getCancellationPolicy(searchQuery, newHInfo, filteredOption.getId(), bookingId);
						SystemContextHolder.setContextData(contextData);
						fareChange = priceValidation.updatePriceIfChanged(searchQuery, newHInfo, isToSendFareDifference,
								bookingId);
						hInfo = newHInfo;
					} else {
						SystemContextHolder.setContextData(copyContextData);
						search.getCancellationPolicy(searchQuery, hInfo, oldOption.getId(), bookingId);
						SystemContextHolder.setContextData(contextData);
						hInfo.setOptions(new ArrayList<>(Arrays.asList(oldOption)));
						fareChange = priceValidation.updatePriceIfChanged(searchQuery, hInfo, isToSendFareDifference,
								bookingId);
					}
					CancellationMiscInfo mInfo = null;
					if (hInfo.getOptions().get(0).getCancellationPolicy() == null
							|| ((mInfo = hInfo.getOptions().get(0).getCancellationPolicy().getMiscInfo()) != null
									&& (BooleanUtils.isFalse(mInfo.getIsBookingAllowed())
											|| BooleanUtils.isTrue(mInfo.getIsSoldOut()))
									&& !hInfo.getOptions().get(0).getIsOptionOnRequest())) {
						log.info("Request room is no longer available for hotel {}", hInfo);
						throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT,
								StringUtils.join(" You can use booking Id ", bookingId, " for reference"));
					}

					SystemContextHolder.getContextData().setExclusionStrategys(new ArrayList<>(
							Arrays.asList(new FieldExclusionStrategy(null, new ArrayList<>(Arrays.asList("rmi"))))));

					// setBookingNotes(hInfo);
					updateStaticData(hInfo, searchQuery, bookingId);
					updateDeadlineDateTimeForOnRequstBooking(hInfo);
					updateTermsAndConditionsIfApplicable(hInfo, searchQuery);
					updateFareComponents(hInfo);
					response.setBookingId(bookingId);
					setBookingConditions(response, hInfo);
					response.setHInfo(hInfo);
					response.setQuery(searchQuery);
					populateMissingParametersInHotelInfo(hInfo);
					storeInCache();
					sendStaticDataAlertIfDiff(oldHotel, hInfo, searchQuery);
					if (Objects.nonNull(fareChange)) {
						response.addAlert(fareChange);
						response.setPriceChanged(true);
					}
				} catch (CustomGeneralException e) {
					String message = StringUtils.join("Failed to review hotelId", request.getHotelId(), ", optionId ",
							request.getOptionId(), ", bookingId ", bookingId);
					RoomSoldOut soldOut = RoomSoldOut.builder().type(AlertType.SOLDOUT.name()).message(message).build();
					response.addAlert(soldOut);
					response.setStatus(new Status(HttpStatusCode.HTTP_200));
					response.setQuery(searchQuery);
					log.error("Failed to review hotelId {}, optionId {},error msg is {}", request.getHotelId(),
							request.getOptionId(), contextData.getErrorMessages(), e);

					if (Objects.nonNull(e.getError()) && e.getError().equals(SystemError.INVALID_HOTEL_STATIC_DATA)) {
						contextData.getErrorMessages().add(SystemError.INVALID_HOTEL_STATIC_DATA.getMessage());
					} else {
						contextData.getErrorMessages().add("Failed to review hotel because hotel is sold out");
					}
				} catch (Exception e) {
					String message = StringUtils.join("Unable to review hotel for hotelId ", request.getHotelId(),
							" optionId ", request.getOptionId(), " hInfo ", hInfo);
					RoomSoldOut soldOut = RoomSoldOut.builder().type(SystemError.SERVICE_EXCEPTION.getMessage())
							.message(message).build();
					contextData.getErrorMessages().add("Failed to review hotel due to service exception");
					log.error("Failed to review hotelId {}, optionId {},due to service exception", request.getHotelId(),
							request.getOptionId(), ", bookingId ", bookingId, e);
					response.addAlert(soldOut);
					response.addError(new ErrorDetail(SystemError.FAILED_TO_REVIEW));
				} finally {
					contextData.getReqIds().add(bookingId);
					storeSearchLogs(searchQuery, bookingId);
					sendReviewDataToAnalytics(hInfo, searchQuery, bookingId);
				}
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hInfo) ? null : Arrays.asList(hInfo),
					System.currentTimeMillis() - startTime);
			sendMethodExecutionDataToAnalytics(hInfo, searchQuery, contextData);
		}
	}

	/**
	 * This is primarily used for testing purpose. In order to test fare jump case we can define value in DB.
	 *
	 * @param searchQuery
	 * @return
	 */
	public boolean isToShowFareDiff(HotelSearchQuery searchQuery) {
		HotelGeneralPurposeOutput rule = HotelConfiguratorHelper.getHotelConfigRuleOutput(
				HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery),
				HotelConfiguratorRuleType.GNPURPOSE);
		if (Objects.nonNull(rule)) {
			return BooleanUtils.toBoolean(rule.getShowFareDiff());
		}
		return false;
	}

	public void sendStaticDataAlertIfDiff(HotelInfo oldHotel, HotelInfo newHotel, HotelSearchQuery searchQuery) {
		final long startTime = System.currentTimeMillis();
		try {
			if (!areBothHotelInfoEqual(oldHotel, newHotel)) {
				response.addAlert(HotelStaticDataChangeAlert.builder()
						.oldValue(String.join(";", oldHotel.getName(),
								HotelUtils.getCompleteAddress(oldHotel.getAddress())))
						.newValue(String.join(";", newHotel.getName(),
								HotelUtils.getCompleteAddress(newHotel.getAddress())))
						.field(FieldType.HOTELNAME.name()).type(AlertType.STATIC_DATA_ALERT.name()).build());
				sendStaticDataChangeToAnalytics(oldHotel, newHotel);
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(newHotel) ? null : Arrays.asList(newHotel),
					System.currentTimeMillis() - startTime);
		}
	}

	private void sendStaticDataChangeToAnalytics(HotelInfo oldHotel, HotelInfo newHotel) {
		HotelStaticDataChangeToAnalyticsHotelStaticDataMapper queryMapper =
				HotelStaticDataChangeToAnalyticsHotelStaticDataMapper.builder()
						.searchId(response.getQuery().getSearchId()).bookingId(response.getBookingId())
						.oldHotel(oldHotel).newHotel(newHotel).build();
		analyticsHelper.storeData(queryMapper);
	}

	public boolean areBothHotelInfoEqual(HotelInfo oldHotel, HotelInfo newHotel) {
		HotelInfoFilterationOutput filterationOutput =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.HOTELINFOFILTERATION);
		if (Objects.isNull(filterationOutput)) {
			log.info("No Filteration rule found for bookingid {}", response.getBookingId());
			return true;
		}
		/*
		 * Taking two separate list for char and words to avoid cases like 'hotel,' 'hotel-' '-the' '&a'
		 */
		List<String> excludedStrings = filterationOutput.getExcludedStrings();
		List<String> excludedChars = filterationOutput.getExcludedCharacters();

		String filteredOldName = HotelUtils.removeUnwantedWordsAndChars(oldHotel.getName().toLowerCase(),
				excludedStrings, excludedChars);
		String filteredNewName = HotelUtils.removeUnwantedWordsAndChars(newHotel.getName().toLowerCase(),
				excludedStrings, excludedChars);

		if (StringUtils.equalsIgnoreCase(filteredNewName, filteredOldName)) {
			return true;
		} else {
			String[] missingWords = HotelUtils.getUncommonWordsOfFirstParam(filteredOldName, filteredNewName);
			String[] newWords = HotelUtils.getUncommonWordsOfFirstParam(filteredNewName, filteredOldName);

			if (Objects.nonNull(newWords) && newWords.length > 0) {
				return false;
			}
			if (Objects.nonNull(missingWords)) {
				String[] newHotelAddressArray =
						HotelUtils.getCompleteAddress(newHotel.getAddress()).toLowerCase().split(" ");
				for (String word : missingWords) {
					if (!Arrays.asList(newHotelAddressArray).contains(word)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	private void sendReviewDataToAnalytics(HotelInfo hInfo, HotelSearchQuery searchQuery, String bookingId) {
		if (BooleanUtils.isNotFalse(request.getPriceValidation())) {
			contextData.getCheckPointInfo().put(SystemCheckPoint.REQUEST_FINISHED.name(),
					new ArrayList<>(Arrays.asList(CheckPointData.builder()
							.type(SystemCheckPoint.REQUEST_FINISHED.name()).time(System.currentTimeMillis()).build())));
			HotelReviewToAnalyticsHotelReviewMapper queryMapper = HotelReviewToAnalyticsHotelReviewMapper.builder()
					.user(SystemContextHolder.getContextData().getUser()).contextData(contextData).hInfo(hInfo)
					.searchQuery(searchQuery).bookingId(bookingId).build();
			analyticsHelper.storeData(queryMapper);
		}
	}

	private void updateFareComponents(HotelInfo hInfo) {
		hInfo.getOptions().get(0).getRoomInfos().stream().forEach(roomInfo -> {
			BaseHotelUtils.updatePerNightTotalFareComponents(roomInfo);
		});
	}

	private void updateTermsAndConditionsIfApplicable(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		HotelSourceConfigOutput rule = HotelConfiguratorHelper.getHotelConfigRuleOutput(
				HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery),
				HotelConfiguratorRuleType.SOURCECONFIG);
		if (Objects.isNull(rule))
			return;
		hInfo.setTermsAndConditions(rule.getTermsAndConditions());
	}

	private void updateDeadlineDateTimeForOnRequstBooking(HotelInfo hInfo) {
		if (hInfo.getOptions().get(0).getIsOptionOnRequest()
				&& hInfo.getOptions().get(0).getDeadlineDateTime().isBefore(LocalDateTime.now())) {
			hInfo.getOptions().get(0).setDeadlineDateTime(LocalDateTime.now().plusHours(6));
		}
	}

	/*
	 * private void setBookingNotes(HotelInfo hInfo) {
	 * 
	 * String bookingNotes = hInfo.getOptions().get(0).getInstructionFromType(InstructionType.BOOKINGNOTES);
	 * hInfo.getOptions().get(0).setNotes(hInfo.getOptions().get(0).getCancellationPolicy().getNotes()); }
	 */

	private void setBookingConditions(HotelReviewResponse response, HotelInfo hInfo) {
		HotelBookingConditions cond = HotelBookingConditions.builder().build();

		boolean isBlockingAllowed = false;
		long leadHours = 0;
		if (BooleanUtils.isTrue(hInfo.getOptions().get(0).getCancellationPolicy().getIsFullRefundAllowed())) {
			HotelGeneralPurposeOutput configuratorInfo =
					HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
			leadHours =
					configuratorInfo.getLeadTimeInHours() != null ? configuratorInfo.getLeadTimeInHours() : leadHours;
			if (hInfo.getOptions().get(0).getDeadlineDateTime().isAfter(LocalDateTime.now().plusHours(leadHours))) {
				isBlockingAllowed = true;
			}
		}
		cond.setIsBlockingAllowed(isBlockingAllowed);
		cond.setSessionTimeInSecond(getSessionTimeInSeconds(hInfo));
		response.setConditions(cond);
	}

	private void populateMissingParametersInHotelInfo(HotelInfo hInfo) {
		String searchId = HotelUtils.getSearchId(request.getHotelId());
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
		for (RoomInfo roomInfo : hInfo.getOptions().get(0).getRoomInfos()) {

			/*
			 * Frontend is pickingup CheckInDate & CheckOutDate from 0th room
			 */
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
		}
	}

	private int getSessionTimeInSeconds(HotelInfo hInfo) {

		int remainingTime =
				(int) Duration.between(LocalDateTime.now(), hInfo.getMiscInfo().getSearchKeyExpiryTime()).getSeconds();
		return remainingTime;
	}

	private void storeSearchLogs(HotelSearchQuery searchQuery, String bookingId) {
		if (searchQuery != null) {
			SystemContextHolder.getContextData()
					.setReqIds(new ArrayList<>(Arrays.asList(bookingId, searchQuery.getSearchId())));
			ExecutorUtils.getGeneralPurposeThreadPool().submit(() -> {
				LogUtils.copyLogHotel(searchQuery.getSearchId(), bookingId);
			});
		}
	}

	public void storeInCache() {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).key(response.getBookingId())
				.binValues(ImmutableMap.of(BinName.HOTELINFO.getName(), response)).compress(false).build();
		cachingService.store(metaInfo);
	}

	@Override
	public void afterProcess() throws Exception {
		SystemContextHolder.getContextData().setExclusionStrategys(new ArrayList<>(
				Arrays.asList(new FieldExclusionStrategy(null, Arrays.asList("links", "hwsi", "hoc")))));
	}

	private void fetchHotelDetailsIfRequired(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		searchManager.fetchHotelDetails(searchQuery, hInfo);
		processingManager.updateStaticData(hInfo, HotelUtils.getHotelSearchQuery(searchQuery, hInfo),
				OperationType.DETAIL_SEARCH);
		processingManager.processDetailResult(hInfo, searchQuery);
	}

	private void updateStaticData(HotelInfo hotelInfo, HotelSearchQuery searchQuery, String bookingId)
			throws Exception {
		final long startTime = System.currentTimeMillis();
		try {
			updateStaticDataFromCancellationPolicy(hotelInfo);
			HotelStaticDataService staticDataService = HotelUtils.getStaticDataService();
			HotelInfo dbHotelInfo = staticDataService.getHotelInfoFromDB(hotelInfo);
			if (StringUtils.isNotBlank(dbHotelInfo.getId())) {
				ClientGeneralInfo clientInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
				if (BooleanUtils.isTrue(clientInfo.getIsHotelReviewValidationEnabled())) {
					String supplierName = hotelInfo.getOptions().get(0).getMiscInfo().getSupplierId();
					staticDataService.validateHotelInfo(dbHotelInfo, supplierName, bookingId);
				}
				processingManager.populateMissingHotelBasicInfo(dbHotelInfo, hotelInfo, searchQuery);
				processingManager.populateMissingHotelDetailInfo(dbHotelInfo, hotelInfo, searchQuery);
			}
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Objects.isNull(hotelInfo) ? null : Arrays.asList(hotelInfo),
					System.currentTimeMillis() - startTime);
		}
	}

	private static void updateStaticDataFromCancellationPolicy(HotelInfo hotelInfo) {
		HotelCancellationPolicy cancellationPolicy = hotelInfo.getOptions().get(0).getCancellationPolicy();
		if (Objects.nonNull(cancellationPolicy.getMiscInfo())
				&& StringUtils.isNotBlank(cancellationPolicy.getMiscInfo().getHotelName())) {
			hotelInfo.setName(cancellationPolicy.getMiscInfo().getHotelName());
			hotelInfo.getAddress().setAddressLine1(cancellationPolicy.getMiscInfo().getHotelAddress());
			hotelInfo.getAddress().setAddressLine2(null);
		}
	}

	private void updateForRetry() {

		if (BooleanUtils.isFalse(request.getPriceValidation())) {
			response.setRetryInSecond(1);
		}
	}

	private void sendMethodExecutionDataToAnalytics(HotelInfo hotelInfo, HotelSearchQuery searchQuery,
			ContextData contextData) {

		if (Objects.nonNull(hotelInfo) && MapUtils.isNotEmpty(contextData.getMethodExecutionInfo())) {
			ContextData methodContextData = BaseHotelUtils.getNewContextDataForMethodExecutionInfo(contextData);
			HotelMethodExecutionInfoMapper methodExecutionMapper = HotelMethodExecutionInfoMapper.builder()
					.contextData(methodContextData).user(methodContextData.getUser()).searchQuery(searchQuery)
					.flowType(HotelFlowType.REVIEW).build();
			analyticsHelper.storeMethodData(methodExecutionMapper);
			log.info("Method execution info for booking id {} is {}", response.getBookingId(),
					GsonUtils.getGson().toJson(methodContextData.getMethodExecutionInfo()));
		} else {
			log.info(
					"Unable to store method execution info as not result found for for search id {} and supplier name {}",
					searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId());
		}
	}
}
