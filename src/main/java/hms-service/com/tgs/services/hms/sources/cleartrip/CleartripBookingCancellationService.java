package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.cleartrip.CleartripBaseRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripBookingCancellationResponse;
import com.tgs.services.hms.datamodel.cleartrip.CleartripRefundAPIResponse;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
public class CleartripBookingCancellationService {

	private HotelSourceConfigOutput sourceConfig;
	private HotelSupplierConfiguration supplierConfig;
	private Order order;
	private HotelInfo hInfo;

	private static final String BOOKING_CANCELLATION_PREFIX = "/hotels/api/v2/cancel/";
	private static final String REFUND_API_PREFIX = "/hotels/api/v2/refund-info/";

	public boolean cancelBooking() throws IOException {
		HttpUtilsV2 httpUtils = null;
		CleartripBaseRequest bookingCancellationRequest = null;
		CleartripBookingCancellationResponse bookingCancellationResponse = null;
		boolean isCancelled = false;
		try {
			Option option = hInfo.getOptions().get(0);
			setPenaltyAmount(option.getCancellationPolicy());
			bookingCancellationRequest = createBookingCancellationRequest();
			httpUtils = CleartripUtils.getResponseURLWithRequestBody(bookingCancellationRequest, supplierConfig);
			httpUtils.setPrintResponseLog(true);
			bookingCancellationResponse =
					httpUtils.getResponse(CleartripBookingCancellationResponse.class).orElse(null);
			isCancelled = isBookingCancelled(bookingCancellationResponse);
		} finally {
			if (Objects.nonNull(httpUtils)) {

				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.CANCELLATION.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(bookingCancellationResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.CLEARTRIP.name())
						.headerParams(httpUtils.getHeaderParams()).requestType(BaseHotelConstants.BOOKING_CANCELLATION)
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(order.getBookingId())
						.additionalinfo(hInfo.getOptions().get(0).getId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
		return isCancelled;
	}

	public Double fetchRefundInfo() throws IOException {
		HttpUtilsV2 httpUtils = null;
		RestAPIListener listener = new RestAPIListener("");
		CleartripBaseRequest refundAPIRequest = null;
		CleartripRefundAPIResponse refundAPIResponse = null;
		try {
			refundAPIRequest = createRefundAPIRequest();
			httpUtils = CleartripUtils.getResponseURL(refundAPIRequest, supplierConfig);
			refundAPIResponse = httpUtils.getResponse(CleartripRefundAPIResponse.class).orElse(null);

			if (!ObjectUtils.isEmpty(refundAPIResponse.getError())) {
				log.error("Unable to retrieve refund details for booking id {} due to {}", order.getBookingId(),
						refundAPIResponse.getError().getDetailedMessage());
				throw new CustomGeneralException("Unable to retrieve refund details");
			}
			return Double.parseDouble(refundAPIResponse.getSuccess().getRefund());
		} finally {
			if (Objects.nonNull(httpUtils)) {

				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.CANCELLATION.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(refundAPIResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.CLEARTRIP.name())
						.headerParams(httpUtils.getHeaderParams()).requestType(BaseHotelConstants.BOOKING_CANCELLATION)
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(order.getBookingId())
						.additionalinfo(hInfo.getOptions().get(0).getId()).prefix("Refund")
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

			}
		}
	}


	private void setPenaltyAmount(HotelCancellationPolicy cancellationPolicy) throws IOException {

		LocalDateTime currentTime = LocalDateTime.now();
		for (PenaltyDetails pd : cancellationPolicy.getPenalyDetails()) {
			if (pd.getFromDate().isBefore(currentTime) && pd.getToDate().isAfter(currentTime)) {
				if (BooleanUtils.isTrue(pd.getIsCancellationRestricted())) {
					Double penaltyAmount = fetchRefundInfo();
					pd.setPenaltyAmount(penaltyAmount);
				}
			}
		}
	}

	private boolean isBookingCancelled(CleartripBookingCancellationResponse bookingCancellationResponse) {

		boolean isBookingCancelled = false;
		if (bookingCancellationResponse == null) {
			log.error("Error while booking cancellation, response is null for bookingId {}", order.getBookingId());
			return isBookingCancelled;
		} else if (!ObjectUtils.isEmpty(bookingCancellationResponse.getError())) {
			log.error("Error while booking cancellation, response is {} for bookingId {}",
					bookingCancellationResponse.getError().getDetailedMessage(), order.getBookingId());
			return isBookingCancelled;
		} else if (!ObjectUtils.isEmpty(bookingCancellationResponse.getSuccess())) {
			log.info("Booking cancelled successfully, bookingId {} cancellation status {} ", order.getBookingId(),
					bookingCancellationResponse.getSuccess().getCancelStatus());
			isBookingCancelled = true;
		}
		return isBookingCancelled;

	}

	private CleartripBaseRequest createRefundAPIRequest() {
		return CleartripBaseRequest.builder().sellingCountry(null).sellingCurrency(null)
				.suffixOfURL(REFUND_API_PREFIX + hInfo.getMiscInfo().getSupplierBookingReference()).build();
	}

	private CleartripBaseRequest createBookingCancellationRequest() {
		return CleartripBaseRequest.builder().sellingCountry(null).sellingCurrency(null)
				.suffixOfURL(BOOKING_CANCELLATION_PREFIX + hInfo.getMiscInfo().getSupplierBookingReference()).build();
	}
}
