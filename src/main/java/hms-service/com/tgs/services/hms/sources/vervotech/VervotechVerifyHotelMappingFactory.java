package com.tgs.services.hms.sources.vervotech;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.tgs.services.base.datamodel.VerifyMappingRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractVerifyHotelMappingFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class VervotechVerifyHotelMappingFactory extends AbstractVerifyHotelMappingFactory {

	public VervotechVerifyHotelMappingFactory(HotelSupplierConfiguration supplierConf,
			VerifyMappingRequest verifyMappingData) {
		super(supplierConf, verifyMappingData);

	}

	@Override
	public void uploadVerifyHotelMapping() throws SftpException, JSchException, IOException {
		VervotechVerifyMappingService verifyMappingService =
				VervotechVerifyMappingService.builder().sourceConfig(sourceConfigOutput).supplierConf(supplierConf)
						.verifyMappingData(verifyMappingData).build();
		verifyMappingService.uploadHotelMapping();
	}

}
