package com.tgs.services.hms.helper;

import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUserReview;

@Service
public class HotelUserReviewSearch extends AbstractHotelUserReviewSearch {
	
	@Override
	public HotelUserReview reviewSearch(int sourceId ,  HotelInfo hInfo) {
		return super.reviewSearch(sourceId , hInfo);
	}

}
