package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
@SuperBuilder
public class DesiyaHotelStaticDataSaveService extends DesiyaBaseService {

	protected HotelInfoSaveManager hotelInfoSaveManager;

	public void process() throws IOException, JAXBException, SAXException, ParserConfigurationException {
		HotelStaticDataService staticDataService = (HotelStaticDataService) HotelUtils.getStaticDataService();
		String zipFolderUrl = supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_STATIC_DATA);
		HttpURLConnection connection = getHttpURLConnection(zipFolderUrl);
		InputStream in = connection.getInputStream();
		ZipInputStream zipIn = new ZipInputStream(in);
		ZipEntry entry = zipIn.getNextEntry();
		while ((entry = zipIn.getNextEntry()) != null) {
			if (FilenameUtils.getExtension(entry.getName()).equals("xml")
					&& FilenameUtils.getName(entry.getName()).equals("HotelOverview.xml")) {
				DesiyaSaxParser saxParser = new DesiyaSaxParser(zipIn, hotelInfoSaveManager, staticDataService);
				saxParser.parseDocument();
				zipIn.close();
				break;
			}
		}
	}
}
