package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.HotelCountryInfoMapping;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "hotelcountryinfomapping",
		uniqueConstraints = {@UniqueConstraint(columnNames = {"countryId", "supplierName"})})
public class DbHotelCountryInfoMapping extends BaseModel<DbHotelCountryInfoMapping, HotelCountryInfoMapping> {

	private String countryId;
	private String supplierName;
	private String supplierCountryId;
	private String supplierCountryName;
	@CreationTimestamp
	private LocalDateTime createdOn;


	@Override
	public HotelCountryInfoMapping toDomain() {
		return new GsonMapper<>(this, HotelCountryInfoMapping.class).convert();
	}

	@Override
	public DbHotelCountryInfoMapping from(HotelCountryInfoMapping dataModel) {
		return new GsonMapper<>(dataModel, this, DbHotelCountryInfoMapping.class).convert();
	}

}
