package com.tgs.services.hms.dbmodel.inventory;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.inventory.HotelRoomCategory;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@ToString
@Table(name = "roomCategory", uniqueConstraints = {@UniqueConstraint(columnNames = {"roomCategory"})})
public class DbRoomCategory extends BaseModel<DbRoomCategory, HotelRoomCategory> {
	
	@SerializedName("rc")
	private String roomCategory;
	
	@Override
	public HotelRoomCategory toDomain() {
		return new GsonMapper<>(this, HotelRoomCategory.class).convert();
	}

	@Override
	public DbRoomCategory from(HotelRoomCategory dataModel) {
		return new GsonMapper<>(dataModel, this, DbRoomCategory.class).convert();
	}

}
