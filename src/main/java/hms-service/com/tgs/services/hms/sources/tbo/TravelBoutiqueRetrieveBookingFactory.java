package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class TravelBoutiqueRetrieveBookingFactory extends AbstractRetrieveHotelBookingFactory {

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public TravelBoutiqueRetrieveBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		super(supplierConf, importBookingInfo);
	}

	@Override
	public void retrieveBooking() throws IOException {
		TravelBoutiqueRetrieveBookingService bookingService =
				TravelBoutiqueRetrieveBookingService.builder().sourceConfigOutput(sourceConfigOutput)
						.supplierConf(supplierConf).importBookingParams(importBookingParams).build();
		bookingService.retrieveBooking();
		bookingDetailResponse = bookingService.getBookingInfo();
	}

}
