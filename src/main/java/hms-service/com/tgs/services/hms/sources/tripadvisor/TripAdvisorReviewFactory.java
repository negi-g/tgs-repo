package com.tgs.services.hms.sources.tripadvisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUserReview;
import com.tgs.services.hms.datamodel.HotelUserReviewIdInfo;
import com.tgs.services.hms.sources.AbstractHotelUserReviewFactory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class TripAdvisorReviewFactory extends AbstractHotelUserReviewFactory {

	@Autowired
	TripAdvisorSearchService searchService;
	
	
	public TripAdvisorReviewFactory() {
		
	}
	
	@Override
	protected void searchHotelReview(HotelUserReview hReview) {
		searchService.doRatingSearch(hReview);
		
	}

	@Override
	protected void searchIdFromNameLatLong(HotelInfo hInfo, 
			HotelUserReviewIdInfo hUserReviewSupplierIdInfo) throws Exception {
		
		searchService.doIdSearch(hInfo, hUserReviewSupplierIdInfo);
		
	}
	
	

	

}
