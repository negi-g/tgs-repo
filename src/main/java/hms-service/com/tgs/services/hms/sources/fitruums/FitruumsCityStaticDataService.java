package com.tgs.services.hms.sources.fitruums;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.SpringContext;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.fitruums.Destination;
import com.tgs.services.hms.datamodel.fitruums.DestinationList;
import com.tgs.services.hms.datamodel.fitruums.FitruumsStaticDataRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class FitruumsCityStaticDataService extends FitruumsBaseService{
	private HotelStaticDataRequest staticDataRequest;
	private HotelSupplierRegionInfoManager supplierRegionInfoManager;
	private HotelRegionInfoMappingManager regionInfoMappingManager;

	public void init() {

		regionInfoMappingManager = (HotelRegionInfoMappingManager) SpringContext.getApplicationContext()
				.getBean("hotelRegionInfoMappingManager");

		supplierRegionInfoManager = (HotelSupplierRegionInfoManager) SpringContext.getApplicationContext()
				.getBean("hotelSupplierRegionInfoManager");
	}

	public List<HotelRegionInfoQuery> getFitruumsCityList() throws IOException {
		List<HotelRegionInfoQuery> hotelCityInfoQueries = new ArrayList<>();
		List<Destination> destinationList=GetCountryList();
		destinationList.forEach(destination->{
			HotelRegionInfoQuery HotelRegionInfoQuery = setInfo(destination);
			hotelCityInfoQueries.add(HotelRegionInfoQuery);
		});
		return hotelCityInfoQueries;
	}

	private HotelRegionInfoQuery setInfo(Destination destination) {
		HotelRegionInfoQuery regionInfoQuery = new HotelRegionInfoQuery();
		regionInfoQuery.setRegionId(destination.getDestinationId());
		regionInfoQuery.setRegionName(destination.getDestinationName());
		regionInfoQuery.setCountryName(destination.getCountryName());
		regionInfoQuery.setRegionType("CITY");
		regionInfoQuery.setCountryId(destination.getCountryId());
		regionInfoQuery.setSupplierName(HotelSourceType.FITRUUMS.name());
		return regionInfoQuery;
	}

	private List<Destination> GetCountryList() throws IOException {
		DestinationList destinationList = null;
		FitruumsMarshaller marshaller= new FitruumsMarshaller();
		FitruumsStaticDataRequest sRequest=FitruumsStaticDataRequest.builder().destinationCode("").build();
		HttpUtils httpUtils=FitruumsUtil.getHttpUtils(null, HotelUrlConstants.COUNTRY_INFO, sRequest, supplierConf);
		httpUtils.setRequestMethod("GET");
		String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
		if(xmlResponse!=null) {
		destinationList = marshaller.unmarshallXML(xmlResponse,DestinationList.class);
		}
		return destinationList.getDestinations().getDestination();
	}
}
