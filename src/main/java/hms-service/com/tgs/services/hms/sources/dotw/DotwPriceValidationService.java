package com.tgs.services.hms.sources.dotw;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwRoomRequest;
import com.tgs.services.hms.datamodel.dotw.DotwSelectedRoom;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class DotwPriceValidationService extends DotwBaseService {

	private String bookingId;
	private Option updatedOption;

	public void validate(HotelInfo hInfo) throws Exception {
		HttpUtils httpUtils = null;
		try {
			listener = new RestAPIListener("");
			Customer customer = createPriceValidationRequest(hInfo, supplierConf);
			String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
			log.info(xmlRequest);
			httpUtils = getHttpRequest(xmlRequest, supplierConf);
			log.info("Price Validation Request is {}", httpUtils.getPostData());
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			log.info(xmlResponse);
			result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
			if (result.getSuccessful() != null) {
				validateResponse(result, hInfo, supplierConf);
				hInfo.getMiscInfo().setSearchKeyExpiryTime(LocalDateTime.now().plusMinutes(15));
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.REVIEW.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());

				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.DOTW.name())
						.requestType(BaseHotelConstants.PRICE_CHECK).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(bookingId)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
				if (!ObjectUtils.isEmpty(result.getRequest()) && !ObjectUtils.isEmpty(result.getRequest().getError())
						&& result.getRequest().getError().getDetails() != null) {
					SystemContextHolder.getContextData().getErrorMessages()
							.add(result.getRequest().getError().getDetails());
				}
			}
		}
	}

	private Customer createPriceValidationRequest(HotelInfo hInfo, HotelSupplierConfiguration supplierConf) {

		Customer customer = getCustomer(supplierConf);
		customer.setProduct("hotel");
		Request request = new Request();
		request.setCommand("getrooms");
		request.setSearchRequest(getSearchCriteriaFromSearchQuery(searchQuery));
		updateRequestParametersForPriceValidationRequest(request, hInfo);
		request.getSearchRequest().setProductId(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		customer.setRequest(request);
		return customer;
	}


	private void updateRequestParametersForPriceValidationRequest(Request request, HotelInfo hInfo) {

		List<DotwRoomRequest> roomRequestList = request.getSearchRequest().getRooms().getRoom();
		int index = 0;
		Option option = hInfo.getOptions().get(0);
		for (DotwRoomRequest roomRequest : roomRequestList) {
			RoomInfo roomInfo = option.getRoomInfos().get(index);
			DotwSelectedRoom selectedRoom = new DotwSelectedRoom();
			selectedRoom.setAllocationDetails(roomInfo.getMiscInfo().getAllocationDetails());
			selectedRoom.setCode(roomInfo.getMiscInfo().getRoomTypeCode());
			selectedRoom.setSelectedRateBasis(roomInfo.getMiscInfo().getRatePlanCode());
			roomRequest.setSelectedRoom(selectedRoom);
			index++;
		}

	}

	public void validateResponse(Results result, HotelInfo hInfo, HotelSupplierConfiguration supplierConf) {

		List<String> roomIds = hInfo.getOptions().get(0).getRoomInfos().stream().map(room -> room.getId())
				.collect(Collectors.toList());
		List<Option> optionList = getOptionList(result.getHotel(), true);
		Option selectedOption = null;
		for (Option option : optionList) {
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				if (roomIds.contains(roomInfo.getId())) {
					RoomMiscInfo roomMiscInfo = roomInfo.getMiscInfo();
					if (roomMiscInfo.getIsRoomBlocked() != null && !roomMiscInfo.getIsRoomBlocked()) {
						throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
					}
					selectedOption = option;
					break;
				}
			}
			if (selectedOption != null)
				break;
		}

		if (selectedOption == null)
			throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
		checkIfAllRoomsBlocked(selectedOption);
		searchQuery.setSourceId(HotelSourceType.DOTW.getSourceId());

		// HotelUtils.setOptionCancellationPolicyFromRooms(new ArrayList<>(Arrays.asList(selectedOption)));
		// HotelUtils.setBufferTimeinCnp(new ArrayList<>(Arrays.asList(selectedOption)),searchQuery);

		updatedOption = selectedOption;
		Map<String, String> roomIdKeyAllocationDetailValue = new HashMap<>();
		updatedOption.getRoomInfos().forEach(room -> {
			roomIdKeyAllocationDetailValue.put(room.getId(), room.getMiscInfo().getAllocationDetails());
		});
		updateMiscInfo(hInfo.getOptions().get(0), roomIdKeyAllocationDetailValue);
	}


	private void updateMiscInfo(Option option, Map<String, String> updatedOption) {

		option.getRoomInfos().forEach(room -> {
			room.getMiscInfo().setAllocationDetails(updatedOption.get(room.getId()));
		});
	}


	private void checkIfAllRoomsBlocked(Option selectedOption) {

		for (RoomInfo roomInfo : selectedOption.getRoomInfos()) {
			RoomMiscInfo roomMiscInfo = roomInfo.getMiscInfo();
			if (!(roomMiscInfo.getIsRoomBlocked() != null && roomMiscInfo.getIsRoomBlocked()))
				throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);

		}

	}
}
