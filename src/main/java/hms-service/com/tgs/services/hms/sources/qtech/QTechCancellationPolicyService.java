package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelCancellationPolicyResult;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.BookingAllowedInfo;
import com.tgs.services.hms.datamodel.qtech.CancellationPolicyRequest;
import com.tgs.services.hms.datamodel.qtech.CancellationPolicyResponse;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
public class QTechCancellationPolicyService {

	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelSupplierConfiguration supplierConf;
	private HotelCancellationPolicyResult result;
	private CancellationPolicyResponse searchResponse;
	private HotelInfo hInfo;
	private String searchId;
	private LocalDateTime deadlineDateTime;
	private HttpUtils httpUtils;
	protected RestAPIListener listener;

	public void searchCancellationPolicy(String logKey) throws IOException {
		listener = new RestAPIListener("");
		CancellationPolicyRequest request = null;
		searchId = HotelUtils.getSearchId(hInfo.getId());
		try {
			request = createRequest();
			httpUtils = QTechUtil.getResponseURL(request, supplierConf, sourceConfigOutput);
			searchResponse = httpUtils.getResponse(CancellationPolicyResponse.class).orElse(null);
			createResponse();
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.CANCELLATION.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(searchResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder()
						.supplierName(supplierConf.getHotelSupplierCredentials().getUserName().toUpperCase())
						.requestType(BaseHotelConstants.CANCELLATIONPOLICY).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(logKey)
						.additionalinfo(hInfo.getOptions().get(0).getId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
				if (ObjectUtils.isEmpty(searchResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
			}
		}
	}

	public CancellationPolicyRequest createRequest() {
		CancellationPolicyRequest request = new CancellationPolicyRequest();
		request.setAction(QTechConstants.ACTION_C.getValue());
		request.setHotel_id(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		request.setUnique_id(hInfo.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		request.setSection_unique_id(hInfo.getOptions().get(0).getMiscInfo().getHotelOptionId());
		request.setGzip("no");
		return request;
	}

	public void createResponse() {
		result = new HotelCancellationPolicyResult();
		HotelCancellationPolicy cancellationPolicy = this.getCancellationPolicy();
		if (cancellationPolicy != null) {
			cancellationPolicy.setId(hInfo.getOptions().get(0).getId());
			hInfo.getOptions().get(0).setCancellationPolicy(cancellationPolicy);
			hInfo.getOptions().get(0).setDeadlineDateTime(deadlineDateTime);
			result.setHotel(hInfo);

		}
	}

	private HotelCancellationPolicy getCancellationPolicy() {

		BookingAllowedInfo bookingAllowedInfo = getBookingAllowedInfo();
		boolean isError = false;
		if (searchResponse == null) {
			log.info("Cancellation policy is empty");
			SystemContextHolder.getContextData().getErrorMessages().add("Cancellation policy is empty");
			isError = true;
		}

		if (searchResponse != null && searchResponse.getMessage().equalsIgnoreCase("fail")) {
			if (ObjectUtils.isEmpty(searchResponse.getMessageInfo())) {
				log.info(searchResponse.getBookingAllowedInfo().getMessageInfo());
				SystemContextHolder.getContextData().getErrorMessages()
						.add(searchResponse.getBookingAllowedInfo().getMessageInfo());
			} else {
				log.info(searchResponse.getMessageInfo());
				SystemContextHolder.getContextData().getErrorMessages().add(searchResponse.getMessageInfo());
			}
			isError = true;
		}

		if (searchResponse.getMessage().equalsIgnoreCase("success")
				&& StringUtils.isBlank(searchResponse.getAppliedAgentCharges())) {
			log.info("Applied agent charges are empty");
			SystemContextHolder.getContextData().getErrorMessages().add("Applied agent charges are empty");
			isError = true;
		}

		if (isError) {
			return null;
		}

		boolean isBookingAllowed = true;
		boolean isSoldOut = false;
		boolean isFullRefundAllowed = true;

		if (bookingAllowedInfo != null && bookingAllowedInfo.getBookingAllowed().equalsIgnoreCase("no")) {
			isBookingAllowed = false;
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Qtech booking not allowed due to " + bookingAllowedInfo.getMessageInfo());
			log.info(
					"Qtech booking not allowed Further due to  {}  isBookingAllowed-{} for searchId {} for supplier {}"
							+ " username {}",
					bookingAllowedInfo.getMessageInfo(), isBookingAllowed, searchId,
					this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName());
			return null;
		}
		if (bookingAllowedInfo != null && bookingAllowedInfo.getSoldOut().equalsIgnoreCase("yes")) {
			isSoldOut = true;
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Qtech booking not allowed due to due to " + bookingAllowedInfo.getMessageInfo());
			log.info(
					"Qtech booking not allowed Further due to {} isSoldOut-{}  for searchId {} for supplier {}"
							+ "for username {}",
					bookingAllowedInfo.getMessageInfo(), isSoldOut, searchId,
					this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName());
			return null;
		}

		if (Double.valueOf(searchResponse.getAppliedAgentCharges()) <= 0) {
			SystemContextHolder.getContextData().getErrorMessages().add("Got wrong cancellation charges from Qtech");
			log.info("Got wrong cancellation charges from Qtech i.e. {} for searchId {} ",
					searchResponse.getAppliedAgentCharges(), searchId);
		}

		List<PenaltyDetails> list = new ArrayList<PenaltyDetails>();
		int hours = Integer.parseInt(searchResponse.getCancellationHours());
		LocalDateTime checkInDate = hInfo.getOptions().get(0).getRoomInfos().get(0).getCheckInDate().atTime(12, 00);
		LocalDateTime targetDate = checkInDate.minusHours(hours + 24);
		LocalDateTime currentTime = LocalDateTime.now();
		String fromCurrency = HotelBaseSupplierUtils.getSupplierCurrency(supplierConf);

		Double cancellationCharges =
				HotelBaseSupplierUtils.getAmountBasedOnCurrency(Double.valueOf(searchResponse.getAppliedAgentCharges()),
						fromCurrency, supplierConf.getBasicInfo().getSupplierId(), BaseHotelConstants.DEFAULT_CURRENCY);
		if (cancellationCharges == 0.0 && CollectionUtils.isNotEmpty(hInfo.getOptions())) {
			cancellationCharges = hInfo.getOptions().get(0).getTotalPrice();
		}
		if (targetDate.isBefore(currentTime) || targetDate.isEqual(currentTime)) {
			isFullRefundAllowed = false;
			LocalDateTime fromDateTime = currentTime;
			if (currentTime.isAfter(checkInDate)) {
				fromDateTime = checkInDate;
			}
			deadlineDateTime = fromDateTime;
			PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(fromDateTime).toDate(checkInDate)
					.penaltyAmount(cancellationCharges).build();
			list.add(penaltyDetails1);
		} else {
			deadlineDateTime = targetDate;
			PenaltyDetails penaltyDetails1 =
					PenaltyDetails.builder().fromDate(currentTime).toDate(targetDate).penaltyAmount(0.0).build();
			if (cancellationCharges == 0.0) {
				cancellationCharges = hInfo.getOptions().get(0).getTotalPrice();
			}
			PenaltyDetails penaltyDetails2 = PenaltyDetails.builder().fromDate(targetDate).toDate(checkInDate)
					.penaltyAmount(cancellationCharges).build();
			list.add(penaltyDetails1);
			list.add(penaltyDetails2);
		}

		if (StringUtils.isBlank(hInfo.getOptions().get(0).getInstructionFromType(InstructionType.BOOKING_NOTES))) {
			hInfo.getOptions().get(0).getInstructions().add(Instruction.builder().type(InstructionType.BOOKING_NOTES)
					.msg(searchResponse.getContractComment()).build());
		}
		HotelCancellationPolicy cancellationPolicy =
				HotelCancellationPolicy.builder().isFullRefundAllowed(isFullRefundAllowed).penalyDetails(list)
						.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(isBookingAllowed).isSoldOut(isSoldOut)
								.hotelName(searchResponse.getHotelName()).hotelAddress(searchResponse.getHotelAddress())
								.build())
						.build();
		updateExpectedBookingAmountInOption();
		return cancellationPolicy;
	}

	private BookingAllowedInfo getBookingAllowedInfo() {

		if (searchResponse != null && searchResponse.getBookingAllowedInfo() != null) {
			return searchResponse.getBookingAllowedInfo();
		}
		return null;

	}

	private void updateExpectedBookingAmountInOption() {
		String fromCurrency = HotelBaseSupplierUtils.getSupplierCurrency(supplierConf);
		OptionMiscInfo optionMiscInfo = hInfo.getOptions().get(0).getMiscInfo();

		optionMiscInfo.setExpectedOptionBookingPrice(String.valueOf(HotelBaseSupplierUtils.getAmountBasedOnCurrency(
				Double.parseDouble(searchResponse.getTotalBookingAmount()), fromCurrency,
				supplierConf.getBasicInfo().getSupplierId(), BaseHotelConstants.DEFAULT_CURRENCY)));

		optionMiscInfo.setExpectedOptionBookingPriceInSupplierCurrency(searchResponse.getTotalBookingAmount());
	}

}
