package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class TravelbullzRetrieveBookingFactory extends AbstractRetrieveHotelBookingFactory {
	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public TravelbullzRetrieveBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		super(supplierConf, importBookingInfo);

	}

	@Override
	public void retrieveBooking() throws IOException, JAXBException {
		TravelbullzRetrieveBookingService bookingService = TravelbullzRetrieveBookingService.builder()
				.supplierConf(supplierConf).moneyExchnageComm(moneyExchnageComm).cacheHandler(cacheHandler)
				.sourceConfig(sourceConfigOutput).importBookingParams(importBookingParams).build();
		bookingService.retrieveBooking();
		bookingDetailResponse = bookingService.getBookingInfo();
	}

}
