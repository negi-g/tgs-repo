package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class TripjackBookingCancellationFactory extends AbstractHotelBookingCancellationFactory {

	public TripjackBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo, Order order) {
		super(supplierConf, hInfo, order);
	}

	@Override
	public boolean cancelHotel() throws IOException {
		String endpoint = supplierConf.getHotelSupplierCredentials().getUrl();
		TripjackBookingCancellationService cancellationService = TripjackBookingCancellationService.builder()
				.endpoint(endpoint).supplierConf(supplierConf).hInfo(hInfo).order(order).build();
		return cancellationService.cancelBooking();
	}

	@Override
	public boolean getCancelHotelStatus() throws IOException {
		return false;
	}

}
