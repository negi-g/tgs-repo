package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsBookResponse;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Service
public class HotelBedsBookingCancellationService extends HotelBedsBaseService {

	private Order order;
	private HotelInfo hInfo;
	HotelBedsBookResponse cancellationResponse;

	public boolean cancelBooking() throws IOException {
		HttpUtilsV2 httpUtils = null;
		try {
			listener = new RestAPIListener("");
			String supplierBookingReference = hInfo.getMiscInfo().getSupplierBookingReference();
			httpUtils = HotelBedsUtils.getPostBookingHttpUtils(supplierBookingReference, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.CANCEL_BOOKING_URL), HttpUtilsV2.REQ_METHOD_DELETE);
			cancellationResponse = httpUtils.getResponse(HotelBedsBookResponse.class).orElse(null);
			log.info("Response for cancellation for supplierreference {} is {}", supplierBookingReference,
					GsonUtils.getGson().toJson(cancellationResponse));

			if (cancellationResponse.getBooking() != null) {
				return cancellationResponse.getBooking().getStatus().equals("CANCELLED");
			}
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(cancellationResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.HOTELBEDS.name())
					.requestType(BaseHotelConstants.BOOKING_CANCELLATION).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.postData(httpUtils.getPostData()).logKey(order.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

		}
		return false;
	}
}

