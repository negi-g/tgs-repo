package com.tgs.services.hms.manager.mapping;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.es.datamodel.ESCityInfo;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfo;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.restmodel.HotelRegionPriorityUpdateQuery;
import com.tgs.services.hms.restmodel.HotelRegionPriorityUpdateRequest;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelRegionInfoSaveManager {

	@Autowired
	HotelRegionInfoService regionInfoService;

	@Autowired
	HotelRegionInfoMappingManager regionInfoMappingManager;

	@Autowired
	HotelSupplierRegionInfoManager supplierRegionInfoSaveManager;

	@Autowired
	private HotelCacheHandler cacheHandler;

	@Autowired
	ElasticSearchCommunicator esCommunicator;

	public void process(List<HotelRegionInfoQuery> regionInfoQueries, Boolean enableMasterData,
			Boolean isFullRegionNameExists) {
		String supplierName = regionInfoQueries.get(0).getSupplierName();

		log.info("Started persisting supplier region info into db for supplier {}", supplierName);
		supplierRegionInfoSaveManager
				.saveSupplierRegionInfo(HotelUtils.getSupplierRegionInfo(regionInfoQueries, supplierName));
		log.info("Finished persisting supplier region infos into db for supplier {}", supplierName);

		log.info("Started persisting master region infos into db for supplier {}", supplierName);
		saveRegionInfoList(regionInfoQueries);
		log.info("Finished persisting master region infos into db for supplier {}", supplierName);

		log.info("Started persisting region info mappings into db for supplier {}", supplierName);
		regionInfoMappingManager.saveRegionInfoMappingList(regionInfoQueries, enableMasterData, isFullRegionNameExists);
		log.info("Finished persisting region info mappings into db for supplier {}", supplierName);
	}

	public void updateRegionInfo(List<HotelRegionInfo> regionInfos) {
		for (HotelRegionInfo regionInfo : regionInfos) {
			DbHotelRegionInfo newHotelRegionInfo = new DbHotelRegionInfo().from(regionInfo);
			try {
				DbHotelRegionInfo dbRegionInfo =
						regionInfoService.findByRegionNameAndCountryNameAndRegionTypeAndFullRegionName(
								regionInfo.getRegionName(), regionInfo.getCountryName(), regionInfo.getRegionType(),
								regionInfo.getFullRegionName());

				if (Objects.nonNull(dbRegionInfo)) {
					boolean isToUpdateSupplierRegionInfo = BooleanUtils.isTrue(regionInfo.getEnabled())
							&& BooleanUtils.isNotTrue(dbRegionInfo.getEnabled());
					dbRegionInfo =
							new GsonMapper<>(newHotelRegionInfo, dbRegionInfo, DbHotelRegionInfo.class).convert();
					dbRegionInfo.setProcessedOn(LocalDateTime.now());
					dbRegionInfo = regionInfoService.saveRegionInfoIntoDB(dbRegionInfo);
					cacheHandler.updateRegionInfo(dbRegionInfo.toDomain());
					if (isToUpdateSupplierRegionInfo) {
						regionInfoMappingManager.saveSupplierRegionMappingIntoCache(dbRegionInfo.getId());
					}
					log.debug("Updated region info for {} ", dbRegionInfo.getId());
				} else {
					log.info("Unable to find region info for {}, id {}", regionInfo);
				}
			} catch (Exception e) {
				log.info("Error while updating region info for region {}", regionInfo, e);
			}
		}
	}

	private void saveRegionInfoList(List<HotelRegionInfoQuery> regionInfos) {

		for (HotelRegionInfoQuery regionInfoQuery : regionInfos) {
			try {
				HotelRegionInfo regionInfo = processRegionInfo(regionInfoQuery);
				saveRegion(regionInfo);
			} catch (Exception e) {
				log.error("Error while processing region info for {} due to {}", regionInfoQuery, e.getMessage());
			}
		}
	}

	public void saveRegion(HotelRegionInfo regionInfo) {
		DbHotelRegionInfo dbRegionInfo = new DbHotelRegionInfo().from(regionInfo);
		try {
			dbRegionInfo = regionInfoService.saveRegionInfoIntoDB(dbRegionInfo);
			log.debug("New City Info object saved with id {}", dbRegionInfo.getId());
		} catch (Exception e) {
			DbHotelRegionInfo oldRegionInfo = regionInfoService
					.findByRegionNameAndCountryNameAndRegionTypeAndFullRegionName(regionInfo.getRegionName(),
							regionInfo.getCountryName(), regionInfo.getRegionType(), regionInfo.getFullRegionName());
			oldRegionInfo = new GsonMapper<>(dbRegionInfo, oldRegionInfo, DbHotelRegionInfo.class).convert();
			log.debug("Updated City Info Object with cityName {}, countryName{} ", oldRegionInfo.getRegionName(),
					oldRegionInfo.getCountryName());
			regionInfoService.saveRegionInfoIntoDB(oldRegionInfo);
		}
	}

	public static HotelRegionInfo processRegionInfo(HotelRegionInfoQuery regionInfoQuery) {

		String regionName = ObjectUtils.firstNonNull(regionInfoQuery.getRegionName(), regionInfoQuery.getStateName());
		HotelRegionInfo regionInfo =
				HotelRegionInfo.builder().regionName(regionName).regionType(regionInfoQuery.getRegionType())
						.enabled(regionInfoQuery.getEnabled()).countryName(regionInfoQuery.getCountryName())
						.fullRegionName(ObjectUtils.firstNonNull(regionInfoQuery.getFullRegionName(), "")).build();
		return regionInfo;
	}

	public void storeRegionMasterListInElasticSearch() {

		for (int i = 0; i < 500; i++) {
			Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
			List<HotelRegionInfo> cityInfoChunk = DbHotelRegionInfo.toDomainList(regionInfoService.findAll(page));
			if (CollectionUtils.isEmpty(cityInfoChunk))
				break;
			List<ESCityInfo> esCityInfoList = getElasticSearchRegionInfo(cityInfoChunk);
			esCommunicator.addBulkDocuments(esCityInfoList, ESMetaInfo.HOTEL_CITY_MAPPING);
			log.debug("Fetched master region list from database, info list size is {}", cityInfoChunk.size());
		}
	}


	private List<ESCityInfo> getElasticSearchRegionInfo(List<HotelRegionInfo> regionInfoChunk) {

		List<ESCityInfo> regionInfoList = new ArrayList<>();
		regionInfoChunk.forEach(regionInfo -> {
			ESCityInfo esCity = ESCityInfo.builder().code(String.valueOf(regionInfo.getId()))
					.name(regionInfo.getRegionName()).country_name(regionInfo.getCountryName()).build();
			regionInfoList.add(esCity);
		});
		return regionInfoList;
	}

	public BulkUploadResponse updateRegionInfoPriority(HotelRegionPriorityUpdateRequest regionPriorityUpdateRequest)
			throws Exception {
		if (regionPriorityUpdateRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(regionPriorityUpdateRequest.getUploadId());
		}

		String jobId = BulkUploadUtils.generateRandomJobId();
		final ContextData contextData = SystemContextHolder.getContextData();
		Runnable saveHotelMealInfoTask = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			for (HotelRegionPriorityUpdateQuery regionPriorityUpdateQuery : regionPriorityUpdateRequest
					.getRegionPriorityUpdateQuery()) {
				Future<?> f = executor.submit(() -> {
					SystemContextHolder.setContextData(contextData);
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(regionPriorityUpdateQuery.getRowId());
					try {
						updateRegionInfoPriority(regionPriorityUpdateQuery, jobId);
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (RuntimeException e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.debug("Unable to update region priority for job id {}, city id {} to {} due to {}", jobId,
								regionPriorityUpdateQuery.getRegionId(), regionPriorityUpdateQuery.getPriority(),
								e.getMessage());
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", uploadInfo.getId());
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, BulkUploadUtils.INITIALIZATION_NOT_REQUIRED);
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting meal mappings for {} ", jobId, e);
			}
		};

		return BulkUploadUtils.processBulkUploadRequest(saveHotelMealInfoTask,
				regionPriorityUpdateRequest.getUploadType(),
				jobId);
	}

	private void updateRegionInfoPriority(HotelRegionPriorityUpdateQuery regionPriorityUpdateQuery, String jobId) {

		if (Objects.isNull(regionPriorityUpdateQuery.getRegionId()))
			throw new CustomGeneralException(SystemError.INVALID_HOTEL_REGION_ID);

		if (Objects.isNull(regionPriorityUpdateQuery.getPriority()))
			throw new CustomGeneralException(SystemError.INVALID_HOTEL_REGION_PRIORITY);

		DbHotelRegionInfo dbRegionInfo = regionInfoService.findById(regionPriorityUpdateQuery.getRegionId());
		if (Objects.nonNull(dbRegionInfo)) {
			Long oldPriority = Objects.isNull(dbRegionInfo.getPriority()) ? 0 : dbRegionInfo.getPriority();
			dbRegionInfo.setProcessedOn(LocalDateTime.now());
			dbRegionInfo.setPriority(regionPriorityUpdateQuery.getPriority());
			regionInfoService.saveRegionInfoIntoDB(dbRegionInfo);
			log.debug("Updated region priority for job id {}, city id {}, city name {} from {} to {}", jobId,
					dbRegionInfo.getId(), dbRegionInfo.getRegionName(), oldPriority,
					regionPriorityUpdateQuery.getPriority());
		} else {
			log.debug("Unable to update region priority for job id {}, city id {} to {} as city doesn't exist", jobId,
					regionPriorityUpdateQuery.getRegionId(), regionPriorityUpdateQuery.getPriority());
			throw new CustomGeneralException(SystemError.HOTEL_REGION_NOT_EXISTS);
		}
	}
}
