package com.tgs.services.hms.servicehandler;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.utils.HibernateUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelStaticInfo;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.restmodel.FetchHotelInfoResponse;

@Service
public class HotelInfoFetchHandler extends ServiceHandler<String,FetchHotelInfoResponse> {

	private final static long LIMIT = 250;
	
	@Autowired
	HotelInfoService hotelInfoService;
	
	@Override
	public void beforeProcess() throws Exception {
		
		if(StringUtils.isBlank(request))
			updateForFirstRequest();
		
	}

	private void updateForFirstRequest() {
		request = HibernateUtils.getNextDataSetCursorInfo(LIMIT, 0l);
	}

	@Override
	public void process() throws Exception {
		
		String next = null;
		long[] data = HibernateUtils.getDataFromRequest(request);
		List<DbHotelInfo> dbHotelList = hotelInfoService.findByIdGreaterThanOrderByIdAscLimit(data[0], data[1]);
		List<HotelInfo> hotelList =  DbHotelInfo.toDomainList(dbHotelList);
		if(hotelList.size() == LIMIT) {
			long lastAccessedId = Long.valueOf(hotelList.get(hotelList.size()-1).getId());
			next = HibernateUtils.getNextDataSetCursorInfo(LIMIT, lastAccessedId);
		}
		HotelStaticInfo fetchHotelInfo = HotelStaticInfo.builder().next(next).hotelInfoList(hotelList).build();
		response.setResponse(fetchHotelInfo);
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
