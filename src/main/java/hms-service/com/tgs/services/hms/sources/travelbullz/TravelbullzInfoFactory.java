package com.tgs.services.hms.sources.travelbullz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TravelbullzInfoFactory extends AbstractHotelInfoFactory {

	@Autowired
	protected HotelCacheHandler cacheHandler;

	public TravelbullzInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);

	}

	@Override
	protected void searchAvailableHotels() throws IOException, JAXBException, InterruptedException {
		Set<String> propertyIds = getPropertyIdsOfCity(HotelSourceType.TRAVELBULLZ.name());
		searchResult = getResultOfAllPropertyIds(propertyIds);
	}

	@Override
	protected HotelSearchResult doSearchForSupplier(Set<String> hotelids, int threadCount)
			throws IOException, JAXBException {
		List<String> hotelList = hotelids.stream().collect(Collectors.toList());
		TravelbullzSearchService searchService = TravelbullzSearchService.builder().supplierConf(supplierConf)
				.searchQuery(searchQuery).supplierRegionInfo(supplierRegionInfo).moneyExchnageComm(moneyExchnageComm)
				.sourceConfig(sourceConfigOutput).hotelIds(hotelList).cacheHandler(cacheHandler).build();
		searchService.doSearch();
		return searchService.getSearchResult();
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException, JAXBException {
		List<String> hotelList = new ArrayList<>();
		hotelList.add(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		TravelbullzSearchService searchService = TravelbullzSearchService.builder().supplierConf(supplierConf)
				.searchQuery(searchQuery).supplierRegionInfo(supplierRegionInfo).moneyExchnageComm(moneyExchnageComm)
				.sourceConfig(sourceConfigOutput).hotelIds(hotelList).cacheHandler(cacheHandler).build();
		try {
			searchService.doDetailSearch();
			hInfo.setOptions(null);
			hInfo.setOptions(searchService.getSearchResult().getHotelInfos().get(0).getOptions());
		} catch (IOException e) {
			throw new CustomGeneralException("Unable to fetch search result due to " + e.getMessage());
		}
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		boolean isCancellationPolicyAlreadySet = false;
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option cachedOption = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		for (Option op : hotel.getOptions()) {
			if (op.getId().equals(optionId) && !ObjectUtils.isEmpty(cachedOption.getCancellationPolicy())) {
				op.setCancellationPolicy(cachedOption.getCancellationPolicy());
				isCancellationPolicyAlreadySet = true;
			}
		}
		if (!isCancellationPolicyAlreadySet) {
			TravelbullzCancellationPolicyService cancellationPolicyService = TravelbullzCancellationPolicyService
					.builder().searchQuery(searchQuery).supplierConf(supplierConf).sourceConfig(sourceConfigOutput)
					.hInfo(hotel).cacheHandler(cacheHandler).moneyExchnageComm(moneyExchnageComm).build();
			cancellationPolicyService.searchCancellationPolicy(logKey);
		}
	}

}
