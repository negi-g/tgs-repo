package com.tgs.services.hms.sources.fitruums;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FitruumsInfoFactory extends AbstractHotelInfoFactory {

	public FitruumsInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);

	}

	@Override
	protected void searchAvailableHotels() throws IOException, JAXBException, InterruptedException {
		Set<String> propertyIds = getPropertyIdsOfCity(HotelSourceType.FITRUUMS.name());
		searchResult = getResultOfAllPropertyIds(propertyIds);

	}

	@Override
	protected HotelSearchResult doSearchForSupplier(Set<String> hotelids, int threadCount)
			throws IOException, JAXBException {
		String hotelList = String.join(",", hotelids);
		FitruumsSearchService searchService = FitruumsSearchService.builder().supplierConf(supplierConf)
				.searchQuery(searchQuery).moneyExchnageComm(moneyExchnageComm)
				.sourceConfig(sourceConfigOutput).hotelIds(hotelList).cacheHandler(cacheHandler).build();
		searchService.doSearch(threadCount);
		return searchService.getSearchResult();
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException, JAXBException {
		Set<String> propertyIds = new HashSet<>();
		propertyIds.add(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		String hotelList = String.join(",", propertyIds);
		FitruumsSearchService searchService = FitruumsSearchService.builder().supplierConf(supplierConf)
				.searchQuery(searchQuery).moneyExchnageComm(moneyExchnageComm)
				.sourceConfig(sourceConfigOutput).hotelIds(hotelList).cacheHandler(cacheHandler).build();
		try {
			searchService.doDetailSearch();
			hInfo.setOptions(null);
			hInfo.setOptions(searchService.getSearchResult().getHotelInfos().get(0).getOptions());
		} catch (IOException e) {
			throw new CustomGeneralException("Unable to fetch search result due to " + e.getMessage());
		}
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option option = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		hotel.getOptions().forEach(op -> {
			if (op.getId().equals(optionId)) {
				op.setCancellationPolicy(option.getCancellationPolicy());
			}
		});

	}

}
