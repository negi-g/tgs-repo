package com.tgs.services.hms.helper;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierMappingInfo;
import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;
import com.tgs.services.hms.jparepository.HotelInfoService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@InitializerGroup(group = InitializerGroup.Group.HOTEL)
public class HotelSupplierMappingPersistenceHelper extends InMemoryInitializer {

	@Autowired
	HotelInfoService hotelInfoService;

	@Autowired
	HotelCacheHandler cacheHandler;
	
	

	public HotelSupplierMappingPersistenceHelper() {
		super(null);
	}

	@Override
	public void process() {
		Runnable fetchSupplierMappingTask = () -> {
			int hotelMappingCount = 0;
			try {
				log.info("Started fetching hotelsuppliermapping from database");
				for (int i = 0; i < 500; i++) {
					Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
					List<HotelSupplierMappingInfo> supplierMappingList =
							DbHotelSupplierMapping.toDomainList(hotelInfoService.findAllHotelSupplierMapping(page));
					hotelMappingCount += supplierMappingList.size();
					log.debug("Fetched hotelsuppliermapping from database, mapping size is {}", hotelMappingCount);
					if (CollectionUtils.isEmpty(supplierMappingList))
						break;
					supplierMappingList.forEach((supplierMapping) -> {
						cacheHandler.storeSupplierMappingInCache(supplierMapping);
					});
					log.debug("Persisted hotel mapping info into cache, info list size is {}", hotelMappingCount);
				}
			} finally {
				log.info("Finished fetching hotel mapping data from database. Hotel mapping fetch count {}",
						hotelMappingCount);
			}
		};

		Thread fetchSupplierMappingThread = new Thread(fetchSupplierMappingTask);
		fetchSupplierMappingThread.start();
	}

	@Override
	public void deleteExistingInitializer() {}
}