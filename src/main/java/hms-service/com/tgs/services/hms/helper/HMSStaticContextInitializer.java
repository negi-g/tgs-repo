package com.tgs.services.hms.helper;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.sources.agoda.AgodaMarshaller;
import com.tgs.services.hms.sources.cleartrip.CleartripUtils;
import com.tgs.services.hms.sources.dotw.DotwMarshallerWrapper;
import com.tgs.services.hms.sources.expedia.ExpediaUtils;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;

@Service
public class HMSStaticContextInitializer {

	@Autowired
	Jaxb2Marshaller marshaller;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	GeneralServiceCommunicator gnComm;

	@Autowired
	MoneyExchangeCommunicator moneyExchnageComm;

	@Autowired
	HotelRegionInfoSaveManager masterRegionInfoSaveManager;

	@Autowired
	HotelSupplierRegionInfoManager supplierRegionInfoSaveManager;

	@Autowired
	HotelRegionInfoMappingManager regionInfoMappingManager;

	@PostConstruct
	public void init() {
		AgodaMarshaller.init(marshaller);
		DotwMarshallerWrapper.init(marshaller);
		ExpediaUtils.init(cacheHandler, gnComm);
		CleartripUtils.init(cacheHandler);
		HotelUtils.init(gnComm);
		HotelBaseSupplierUtils.init(cacheHandler, moneyExchnageComm, masterRegionInfoSaveManager,
				supplierRegionInfoSaveManager, regionInfoMappingManager);
	}
}
