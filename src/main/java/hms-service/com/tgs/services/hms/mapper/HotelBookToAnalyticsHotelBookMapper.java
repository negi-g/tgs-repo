package com.tgs.services.hms.mapper;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.hms.analytics.AnalyticsHotelQuery;
import com.tgs.services.hms.analytics.HotelBookingAnalyticsInfo;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelBookToAnalyticsHotelBookMapper extends Mapper<AnalyticsHotelQuery> {

	private User user;
	private ContextData contextData;
	private HotelSearchQuery searchQuery;
	private HotelBookingAnalyticsInfo bookingInfo;

	@Override
	protected void execute() throws CustomGeneralException {
		output = output != null ? output : AnalyticsHotelQuery.builder().build();
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		output = HotelReviewToAnalyticsHotelReviewMapper.builder().searchQuery(searchQuery)
				.hInfo(bookingInfo.getHInfo()).bookingId(bookingInfo.getBookingId()).user(user).contextData(contextData)
				.build().setOutput(output).convert();
		output.setAnalyticstype(HotelFlowType.BOOK.name());
		output.setPaymentmedium(bookingInfo.getPaymentMedium());
		output.setFlowtype(bookingInfo.getFlowType());
		output.setBookingstatus(bookingInfo.getOrderStatus());
		output.setPaymentstatus(bookingInfo.getPaymentStatus());
		output.setHoldbooking(bookingInfo.getOrderStatus() != null ? bookingInfo.getOrderStatus().equals("OH") : false);
		filterBookingOutput();
	}

	private void filterBookingOutput() {
		output.setExternalapiparsingtimems(null);
		output.setExternalapitimems(null);
		output.setNumberofadults(null);
		output.setNumberofchild(null);
		output.setSearchresultcount(null);
		output.setOptionscount(null);
	}
}
