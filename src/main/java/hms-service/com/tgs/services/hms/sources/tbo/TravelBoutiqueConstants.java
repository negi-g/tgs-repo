package com.tgs.services.hms.sources.tbo;

import lombok.Getter;

@Getter
public enum TravelBoutiqueConstants {

	HOTEL_AUTH_URL,
	HOTEL_SEARCH_URL,
	HOTEL_STATIC_DATA,
	HOTEL_ROOM_URL,
	BLOCK_ROOM_URL,
	BOOK_URL,
	CANCEL_BOOKING_URL,
	CANCEL_BOOKING_STATUS_URL,
	CONFIRM_BOOKING_URL,
	BOOKING_DETAIL,
	COUNTRY_INFO,
	CITY_INFO;
	
	
}

