package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.State;
import com.tgs.services.hms.datamodel.cleartrip.CityList;
import com.tgs.services.hms.datamodel.cleartrip.CityRequest;
import com.tgs.services.hms.datamodel.cleartrip.CityResponse;
import com.tgs.services.hms.datamodel.cleartrip.CleartripBaseRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripStaticResponseData;
import com.tgs.services.hms.datamodel.cleartrip.HotelList;
import com.tgs.services.hms.datamodel.cleartrip.HotelListRequest;
import com.tgs.services.hms.datamodel.cleartrip.HotelListResponse;
import com.tgs.services.hms.datamodel.cleartrip.HotelProfileData;
import com.tgs.services.hms.datamodel.cleartrip.HotelProfileLocationInfo;
import com.tgs.services.hms.datamodel.cleartrip.HotelProfilePolicyInfo;
import com.tgs.services.hms.datamodel.cleartrip.HotelProfileResponse;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfoMapping;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Getter
public class CleartripStaticDataInfoService {

	private HotelInfoSaveManager hotelInfoSaveManager;
	private HotelRegionInfoService regionInfoService;
	private HotelSourceConfigOutput sourceConfig;
	private HotelSupplierConfiguration supplierConf;
	private HotelStaticDataRequest staticDataRequest;
	private HotelStaticDataService staticDataService;

	private static final String CITY_LIST_SUFFIX = "/hotels/api/v2/content/city-list";
	private static final String HOTEL_LIST_SUFFIX = "/hotels/api/v2/content/hotel-list-city-based";
	private static final String HOTEL_PROFILE_SUFFIX = "/hotels/api/v2/content/hotel-profile/";
	private static final Integer MAX_RETRY_COUNT = 5;

	public void init() {

		hotelInfoSaveManager =
				(HotelInfoSaveManager) SpringContext.getApplicationContext().getBean("hotelInfoSaveManager");

		regionInfoService =
				(HotelRegionInfoService) SpringContext.getApplicationContext().getBean("hotelRegionInfoService");

		staticDataService = (HotelStaticDataService) HotelUtils.getStaticDataService();
	}

	public void getCityList() throws IOException {

		int page = 1;
		HttpUtilsV2 httpUtils = null;
		int size = !ObjectUtils.isEmpty(sourceConfig.getSize()) ? Integer.parseInt(sourceConfig.getSize()) : 1000;
		int maxCityHitCount =
				!ObjectUtils.isEmpty(sourceConfig.getCityHitCount()) ? sourceConfig.getCityHitCount() : 200;
		int retryCount = 0;
		try {
			while (page < maxCityHitCount) {
				try {
					CityRequest cityRequest = createRequestToFetchCityList(page, size);
					httpUtils = CleartripUtils.getResponseURL(cityRequest, supplierConf);
					httpUtils.setPrintResponseLog(false);
					CityResponse cityResponse = httpUtils.getResponse(CityResponse.class).orElse(null);
					CleartripStaticResponseData cityResponseData = cityResponse.getData();
					List<CityList> supplierCityList = null;
					if (ObjectUtils.isEmpty(cityResponseData)
							|| CollectionUtils.isEmpty((supplierCityList = cityResponseData.getCityList()))) {
						break;
					}
					if (page == 1) {
						log.info("City Fetched are {} ", httpUtils.getResponseString());
					}
					List<HotelRegionInfoQuery> regionInfoQueries = createRegionMappingResponse(supplierCityList);
					HotelBaseSupplierUtils.saveOrUpdateRegionInfo(regionInfoQueries,
							staticDataRequest.getIsMasterData(), false);
					retryCount = 0;
					log.info("No of cities fetched are {}", page * supplierCityList.size());
					page++;
				} catch (IOException e) {
					retryCount++;
					log.info("Unable to fetch city info for url {}, headers {}, link {}", httpUtils.getUrlString(),
							httpUtils.getHeaderParams(), httpUtils.getResponseHeaderParams(), e);
					if (retryCount > MAX_RETRY_COUNT)
						break;
				}
			}
		} finally {
			log.info("Total no of city fetched are {}, final response {}, headers {}", page * size,
					httpUtils.getResponseString(), httpUtils.getResponseHeaderParams());
		}
	}

	public void handleHotelStaticContent() throws IOException {

		List<String> cities = getCleartripCityIds();
		int totalFetchedHotels = 0;
		for (String city : cities) {
			int page = 0;
			int totalFetchedHotelsCityWise = 0;
			HttpUtilsV2 httpUtils = null;
			int size = !ObjectUtils.isEmpty(sourceConfig.getSize()) ? Integer.parseInt(sourceConfig.getSize()) : 500;
			int maxHotelHitCount =
					!ObjectUtils.isEmpty(sourceConfig.getCityHitCount()) ? sourceConfig.getHotelHitCount() : 20;
			int retryCount = 0;
			try {
				while (page < maxHotelHitCount) {
					try {
						page++;
						HotelListRequest hotelRequest = createRequestToFetchHotelList(page, size, city);
						httpUtils = CleartripUtils.getResponseURL(hotelRequest, supplierConf);
						httpUtils.setPrintResponseLog(false);
						HotelListResponse hotelListResponse =
								httpUtils.getResponse(HotelListResponse.class).orElse(null);
						CleartripStaticResponseData hotelListResponseData = hotelListResponse.getData();
						List<HotelList> supplierHotelList = null;
						if (!ObjectUtils.isEmpty(hotelListResponseData) && CollectionUtils
								.isNotEmpty((supplierHotelList = hotelListResponseData.getHotels()))) {
							fetchAndSaveHotelProfile(supplierHotelList);
						} else {
							break;
						}
						retryCount = 0;
						totalFetchedHotelsCityWise += supplierHotelList.size();
					} catch (IOException e) {
						retryCount++;
						log.info("Unable to fetch hotel info for url {}, headers {}, link {}", httpUtils.getUrlString(),
								httpUtils.getHeaderParams(), httpUtils.getResponseHeaderParams(), e);
						if (retryCount > MAX_RETRY_COUNT) {
							break;
						}
					} catch (Exception e) {
						log.info("Unable to fetch hotel info for url {}, headers {}, link {}", httpUtils.getUrlString(),
								httpUtils.getHeaderParams(), httpUtils.getResponseHeaderParams(), e);
					}
				}
			} finally {
				totalFetchedHotels += totalFetchedHotelsCityWise;
				log.info("Total no of hotels fetched for city {} are {}, final response {}, headers {}", city,
						totalFetchedHotelsCityWise, httpUtils.getResponseString(), httpUtils.getResponseHeaderParams());
			}
		}
		log.info("Total no of hotels fetched are {}", totalFetchedHotels);
	}

	private void fetchAndSaveHotelProfile(List<HotelList> supplierHotelList) throws IOException {
		supplierHotelList.forEach(supplierHotel -> {

			try {
				HotelProfileData profileData = fetchHotelProfile(supplierHotel);
				if (!ObjectUtils.isEmpty(profileData)) {

					HotelInfo hotelInfo = convertHotelProfileDataIntoHotelInfo(profileData);
					saveOrUpdateHotelInfo(hotelInfo);
				}
			} catch (Exception e) {
				log.info("Unable to fetch hotel profile for {}", GsonUtils.getGson().toJson(supplierHotel), e);
			}
		});
	}

	private void saveOrUpdateHotelInfo(HotelInfo hotelInfo) {
		try {
			DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hotelInfo);
			dbHotelInfo.setId(null);
			dbHotelInfo.setSupplierHotelId(hotelInfo.getId());
			dbHotelInfo.setSupplierName(HotelSourceType.CLEARTRIP.name());
			hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, staticDataService);
		} catch (Exception e) {
			log.info("Error while processing hotel {} due to {} ", hotelInfo.getId(), e);
		}
	}

	private HotelInfo convertHotelProfileDataIntoHotelInfo(HotelProfileData profileData) {

		try {
			HotelProfileLocationInfo locationInfo = profileData.getLocationInfo();
			Address address = Address.builder().addressLine1(profileData.getLocationInfo().getAddress())
					.cityName(locationInfo.getCityName())
					.countryName(locationInfo.getCountryName()).stateName(locationInfo.getStateName())
					.city(City.builder().name(locationInfo.getCityName()).code(String.valueOf(locationInfo.getCityId()))
							.build())
					.state(State.builder().name(locationInfo.getStateName()).code(locationInfo.getStateCode()).build())
					.country(Country.builder().name(locationInfo.getCountryName()).code(locationInfo.getCountryCode())
							.build())
					.postalCode(locationInfo.getZip()).build();

			GeoLocation geolocation = GeoLocation.builder().latitude(locationInfo.getLatitude())
					.longitude(locationInfo.getLongitude()).build();

			List<Image> images = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(profileData.getImages())) {
				profileData.getImages().forEach(image -> {
					Image img = Image.builder().build();
					img.setBigURL(image.getOriginalImage());
					img.setThumbnail(image.getThumbNailImage());
					images.add(img);
				});
			}

			String propertyType = null;
			if (!ObjectUtils.isEmpty(profileData.getProperty())) {
				propertyType = profileData.getProperty().getPropertyName();
			}

			HotelInfo hotelInfo = HotelInfo.builder().id(String.valueOf(profileData.getId()))
					.name(profileData.getHotelName()).propertyType(propertyType).address(address).images(images)
					.geolocation(geolocation).build();

			if (CollectionUtils.isNotEmpty(profileData.getAmenities())) {
				List<String> facilities = new ArrayList<>();
				profileData.getAmenities().forEach(amenity -> {
					facilities.add(amenity.getAmenityNameEn());
				});
				hotelInfo.setFacilities(facilities);
			}

			if (!ObjectUtils.isEmpty(profileData.getRatings())
					&& !ObjectUtils.isEmpty(profileData.getRatings().getStarRating())) {
				hotelInfo.setRating((int) Double.parseDouble(profileData.getRatings().getStarRating()));
				hotelInfo.setRating(hotelInfo.getRating() != 0 ? hotelInfo.getRating() : null);
			}
			populateCheckinInstructions(profileData.getPolicyInfo(), hotelInfo);

			if (!ObjectUtils.isEmpty(profileData.getPolicyInfo())) {
				hotelInfo.setDescription(StringEscapeUtils.unescapeHtml4(profileData.getBasicInfo().getDescription()));
			}
			return hotelInfo;
		} catch (Exception e) {
			log.info("Unable to convert hotel profile data into hotel info {}", GsonUtils.getGson().toJson(profileData),
					e);

		}
		return null;
	}

	public HotelProfileData fetchHotelProfile(HotelList supplierHotel) {

		HttpUtilsV2 httpUtils = null;
		try {
			CleartripBaseRequest hotelProfileRequest = createRequestToFetchHotelProfile(supplierHotel.getHotelId());
			httpUtils = CleartripUtils.getResponseURL(hotelProfileRequest, supplierConf);
			httpUtils.setPrintResponseLog(false);
			HotelProfileResponse hotelProfileResponse = httpUtils.getResponse(HotelProfileResponse.class).orElse(null);
			if (ObjectUtils.isEmpty(hotelProfileResponse)) {
				log.debug("Empty hotel profile info for url {}, headers {}, link {}", httpUtils.getUrlString(),
						httpUtils.getHeaderParams(), httpUtils.getResponseHeaderParams());
			}
			return hotelProfileResponse.getData();
		} catch (IOException e) {
			log.info("Unable to fetch hotel profile info for url {}, headers {}, link {}", httpUtils.getUrlString(),
					httpUtils.getHeaderParams(), httpUtils.getResponseHeaderParams(), e);
		}
		return null;
	}

	private CityRequest createRequestToFetchCityList(int pageNo, int size) {

		return CityRequest.builder().suffixOfURL(CITY_LIST_SUFFIX).pageNo(String.valueOf(pageNo))
				.size(String.valueOf(size)).build();
	}

	private HotelListRequest createRequestToFetchHotelList(int pageNo, int size, String cityId) {
		return HotelListRequest.builder().suffixOfURL(HOTEL_LIST_SUFFIX).pageNo(String.valueOf(pageNo))
				.size(String.valueOf(size)).cityId(cityId).build();
	}

	private CleartripBaseRequest createRequestToFetchHotelProfile(int supplierHotelId) {
		return CleartripBaseRequest.builder().suffixOfURL(HOTEL_PROFILE_SUFFIX + supplierHotelId).build();
	}

	private List<HotelRegionInfoQuery> createRegionMappingResponse(List<CityList> cityList) {
		List<HotelRegionInfoQuery> regionInfoQueries = new ArrayList<>();
		cityList.forEach(cityMapping -> {
			HotelRegionInfoQuery regionInfoQuery = new HotelRegionInfoQuery();
			regionInfoQuery.setRegionId(cityMapping.getId().toString());
			regionInfoQuery.setRegionName(cityMapping.getCityName());
			regionInfoQuery.setRegionType("CITY");
			regionInfoQuery.setCountryId(cityMapping.getCountryId());
			regionInfoQuery.setCountryName(cityMapping.getCountry().getCountryName());
			regionInfoQuery.setSupplierName(HotelSourceType.CLEARTRIP.name());
			regionInfoQuery.setStateId(cityMapping.getStateId());
			regionInfoQuery
					.setStateName(Objects.nonNull(cityMapping.getState()) ? cityMapping.getState().getStateName() : "");
			regionInfoQueries.add(regionInfoQuery);
		});
		return regionInfoQueries;
	}

	private List<String> getCleartripCityIds() {

		if (CollectionUtils.isEmpty(staticDataRequest.getCityIds())) {
			List<DbHotelRegionInfoMapping> supplierRegionInfos =
					regionInfoService.findBySupplierName(HotelSourceType.CLEARTRIP.name());
			return supplierRegionInfos.stream().map(regionInfo -> regionInfo.getId().toString())
					.collect(Collectors.toList());
		} else {
			return staticDataRequest.getCityIds();
		}
	}

	private void populateCheckinInstructions(HotelProfilePolicyInfo policyInfo, HotelInfo hotelInfo) {

		List<Instruction> instructions = new ArrayList<>();
		if (StringUtils.isNotBlank(policyInfo.getCheckInInstructions())) {
			instructions.add(Instruction.builder().type(InstructionType.CHECKIN_INSTRUCTIONS)
					.msg(policyInfo.getCheckInInstructions()).build());
		}

		if (StringUtils.isNotBlank(policyInfo.getSpecialCheckInInstructions())) {
			instructions.add(Instruction.builder().type(InstructionType.SPECIAL_INSTRUCTIONS)
					.msg(policyInfo.getSpecialCheckInInstructions()).build());
		}

		if (StringUtils.isNotBlank(policyInfo.getMandatoryFees())) {
			instructions.add(Instruction.builder().type(InstructionType.MANDATORY_FEES)
					.msg(policyInfo.getMandatoryFees()).build());
		}

		if (StringUtils.isNotBlank(policyInfo.getMandatoryFees())) {
			instructions.add(Instruction.builder().type(InstructionType.OPTIONAL_FEES)
					.msg(policyInfo.getOptionalExtra()).build());
		}

		if (StringUtils.isNotBlank(policyInfo.getMandatoryFees())) {
			instructions.add(Instruction.builder().type(InstructionType.KNOW_BEFORE_YOU_GO)
					.msg(policyInfo.getOtherInformation()).build());
		}

		hotelInfo.setInstructions(instructions);
	}
}
