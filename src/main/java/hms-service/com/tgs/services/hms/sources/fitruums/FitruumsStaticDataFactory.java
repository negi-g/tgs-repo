package com.tgs.services.hms.sources.fitruums;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FitruumsStaticDataFactory extends AbstractStaticDataInfoFactory {
	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	protected HotelCacheHandler hotelCacheHandler;

	@Autowired
	private HotelRegionInfoService regionInfoService;

	public FitruumsStaticDataFactory(HotelSupplierConfiguration supplierConf,
			HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);

	}

	@Override
	protected void getHotelStaticData() throws IOException, JAXBException {
		FitruumsHotelStaticDataService hotelStaticDataService = FitruumsHotelStaticDataService.builder()
				.supplierConf(supplierConf).staticDataRequest(staticDataRequest)
				.regionInfoService(regionInfoService).hotelInfoSaveManager(hotelInfoSaveManager).build();
		hotelStaticDataService.process();

	}

	@Override
	protected void getCityMappingData() throws IOException {
		FitruumsCityStaticDataService staticCityService = FitruumsCityStaticDataService.builder()
				.supplierConf(supplierConf).staticDataRequest(staticDataRequest).build();
		staticCityService.init();
		List<HotelRegionInfoQuery> regionData = staticCityService.getFitruumsCityList();
		log.info("Fitruums city size : " + regionData.size());
		HotelBaseSupplierUtils.saveOrUpdateRegionInfo(regionData, staticDataRequest.getIsMasterData(), false);

	}

	@Override
	protected Map<String, String> getRoomMappingData(HotelSearchQuery searchQuery, HotelInfo hotelInfo)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void getNationalityMappingData() throws IOException {
		// TODO Auto-generated method stub
		
	}

}
