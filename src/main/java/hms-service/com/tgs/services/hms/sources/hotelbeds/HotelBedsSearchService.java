package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedPaxType;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsSearchRequest;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsSearchResponse;
import com.tgs.services.hms.datamodel.hotelBeds.Occupancy;
import com.tgs.services.hms.datamodel.hotelBeds.Paxes;
import com.tgs.services.hms.datamodel.hotelBeds.Stay;
import com.tgs.services.hms.datamodel.hotelBeds.searchRequestHotel;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Getter
@Setter
@Service
@SuperBuilder
public class HotelBedsSearchService extends HotelBedsBaseService {

	private HotelSearchResult searchResult;
	private List<Integer> propertyIds;
	HotelBedsSearchResponse response;

	public void doSearch(int threadCount) throws IOException {
		searchResult = HotelSearchResult.builder().build();
		HttpUtilsV2 httpUtils = null;
		listener = new RestAPIListener("");
		try {
			HotelBedsSearchRequest searchRequest = getHotelBedsSearchRequest();
			httpUtils = HotelBedsUtils.getHttpUtils(searchRequest, null, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_SEARCH_URL));
			httpUtils.setPrintResponseLog(false);
			response = httpUtils.getResponse(HotelBedsSearchResponse.class).orElse(null);
			searchResult.setHotelInfos(getHotelListFromHotelBedsResult(response, false));
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.SEARCH.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(response)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.HOTELBEDS.name())
						.requestType(BaseHotelConstants.SEARCH).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId()).threadCount(threadCount)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	public void doDetailSearch() throws IOException {

		HttpUtilsV2 httpUtils = null;
		searchResult = HotelSearchResult.builder().build();
		HotelBedsSearchResponse response = null;
		listener = new RestAPIListener("");
		try {
			HotelBedsSearchRequest searchRequest = getHotelBedsSearchRequest();
			httpUtils = HotelBedsUtils.getHttpUtils(searchRequest, null, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_SEARCH_URL));
			response = httpUtils.getResponse(HotelBedsSearchResponse.class).orElse(null);
			searchResult.setHotelInfos(getHotelListFromHotelBedsResult(response, true));
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.DETAIL.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(response)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.HOTELBEDS.name())
						.requestType(BaseHotelConstants.DETAILSEARCH).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId())
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private List<HotelInfo> getHotelListFromHotelBedsResult(HotelBedsSearchResponse result, boolean isDetail) {

		List<HotelInfo> hInfos = new ArrayList<>();
		if (result == null || CollectionUtils.isEmpty(result.getHotels().getHotels())) {
			log.error("Empty Response From Supplier {} for searchId {}", supplierConf.getBasicInfo().getSupplierId(),
					searchQuery.getSearchId());
			return null;
		}
		searchQuery.setSourceId(HotelSourceType.HOTELBEDS.getSourceId());
		result.getHotels().getHotels().forEach(hotel -> {
			List<Option> optionList = createOptionFromHotel(hotel, isDetail);
			HotelInfo hInfo = HotelInfo.builder()
					.miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
							.supplierBookingReference(result.getAuditData().getToken())
							.supplierStaticHotelId(hotel.getCode() + "").build())
					.options(optionList).build();
			hInfos.add(hInfo);
		});
		HotelBaseSupplierUtils.populateMealInfo(hInfos, searchQuery);
		return hInfos;
	}

	private HotelBedsSearchRequest getHotelBedsSearchRequest() {

		String sourceMarket = "IN";
		Stay stay = Stay.builder().checkIn(searchQuery.getCheckinDate().toString())
				.checkOut(searchQuery.getCheckoutDate().toString()).build();
		List<Occupancy> occuancies = getOccupancy();
		HotelBedsSearchRequest request = HotelBedsSearchRequest.builder().stay(stay).occupancies(occuancies)
				.hotels(searchRequestHotel.builder().hotel(propertyIds).build()).sourceMarket(sourceMarket).build();
		return request;
	}

	private List<Occupancy> getOccupancy() {

		List<Occupancy> occupancyList = new ArrayList<>();
		searchQuery.getRoomInfo().forEach(room -> {
			List<Paxes> paxeList = new ArrayList<>();
			int childrenCount = room.getNumberOfChild() != null ? room.getNumberOfChild() : 0;
			if (childrenCount > 0) {
				room.getChildAge().forEach(age -> {
					Paxes paxes = Paxes.builder().age(age).type(HotelBedPaxType.CHILD.getCode()).build();
					paxeList.add(paxes);
				});
			}
			Occupancy occupancy = Occupancy.builder().adults(room.getNumberOfAdults()).children(childrenCount).rooms(1)
					.paxes(CollectionUtils.isNotEmpty(paxeList) ? paxeList : null).build();
			occupancyList.add(occupancy);
		});
		return occupancyList;
	}

}
