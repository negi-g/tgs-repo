package com.tgs.services.hms.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.base.TgsValidator;
import com.tgs.services.hms.restmodel.HotelReviewRequest;

@Component
public class HotelReviewValidator extends TgsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(target instanceof HotelReviewRequest) {
			HotelReviewRequest reviewRequest = (HotelReviewRequest) target;
			registerErrors(errors, "", reviewRequest);
		}
	}
}
