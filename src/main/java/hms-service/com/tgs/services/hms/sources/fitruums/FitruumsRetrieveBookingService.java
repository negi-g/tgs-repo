package com.tgs.services.hms.sources.fitruums;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.amazonaws.util.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.RoomAdditionalInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.fitruums.book.GetBookingInformationResult;
import com.tgs.services.hms.datamodel.fitruums.book.GetBookingInformationResult.Bookings.Booking;
import com.tgs.services.hms.datamodel.fitruums.book.GetBookingInformationResult.Bookings.Booking.Cancellationpolicies;
import com.tgs.services.hms.datamodel.fitruums.book.GetBookingInformationResult.Bookings.Booking.Prices;
import com.tgs.services.hms.datamodel.fitruums.book.GetBookingInformationResult.Bookings.Booking.Prices.Price;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Getter
@Setter
@SuperBuilder
public class FitruumsRetrieveBookingService extends FitruumsBaseService {
	private HotelImportBookingParams importBookingParams;
	private HotelImportedBookingInfo bookingInfo;

	public void retrieveBooking() throws IOException {
		HashMap<String, String> bookRetrieveMap = getBookRetrieveMap(importBookingParams.getBookingId());
		FitruumsMarshaller fitruumsMarshaller = new FitruumsMarshaller();
		String xmlResponse = null;
		HttpUtils httpUtils = null;
		GetBookingInformationResult retrieveBookingResult = null;
		try {
			listener = new RestAPIListener("");
			String retrievebookingRequest = getKeyValuePair(bookRetrieveMap);
			httpUtils = FitruumsUtil.getHttpUtils(retrievebookingRequest, HotelUrlConstants.CONFIRM_BOOKING_URL, null,
					supplierConf);
			xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (!StringUtils.isNullOrEmpty(xmlResponse)) {
				retrieveBookingResult =
						fitruumsMarshaller.unmarshallXML(xmlResponse, GetBookingInformationResult.class);
				if (Objects.nonNull(retrieveBookingResult.getBookings().getBooking())) {
					bookingInfo = createBookingDetailResponse(retrieveBookingResult.getBookings().getBooking());
				}
			}
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(retrieveBookingResult)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.FITRUUMS.name())
					.requestType(BaseHotelConstants.RETRIEVE_BOOKING).headerParams(httpUtils.getHeaderParams())
					.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
					.prefix(importBookingParams.getFlowType().getName()).postData(httpUtils.getPostData())
					.logKey(importBookingParams.getBookingId())
					.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
					.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

		}

	}

	private HotelImportedBookingInfo createBookingDetailResponse(Booking booking) {
		List<HotelInfo> hInfos = new ArrayList<>();
		searchQuery = getHotelSearchQuery(booking);
		HotelInfo hInfo = getHotelDetails(booking);
		hInfos.add(hInfo);
		HotelBaseSupplierUtils.populateMealInfo(hInfos, searchQuery, false);
		String orderStatus = StringUtils.isNullOrEmpty(booking.getBookingStatus()) ? OrderStatus.SUCCESS.name()
				: (booking.getBookingStatus().equalsIgnoreCase("NotActive") ? OrderStatus.CANCELLED.name()
						: OrderStatus.SUCCESS.name());
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		HotelImportedBookingInfo hBookingInfo = HotelImportedBookingInfo.builder()
				.bookingDate(getDate(booking.getBookingdate().toString())).hInfo(hInfo).searchQuery(searchQuery)
				.deliveryInfo(deliveryInfo).bookingCurrencyCode(booking.getCurrency()).orderStatus(orderStatus).build();
		return hBookingInfo;
	}

	private HotelSearchQuery getHotelSearchQuery(Booking booking) {
		HotelSearchQuery searchQuery =
				HotelSearchQuery.builder().checkinDate(getDate(booking.getCheckindate().toString()))
						.checkoutDate(getDate(booking.getCheckoutdate().toString()))
						.sourceId(HotelSourceType.FITRUUMS.getSourceId())
						.miscInfo(
								HotelSearchQueryMiscInfo.builder().supplierId(HotelSourceType.FITRUUMS.name()).build())
						.build();
		return searchQuery;
	}


	private LocalDate getDate(String dateTime) {
		String[] date = dateTime.split("T");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd");
		return LocalDate.parse(date[0], formatter);
	}

	private HotelInfo getHotelDetails(Booking booking) {
		List<Option> optionList = getHotelOption(booking);

		String hotelId = String.valueOf(booking.getHotelId());
		HotelMiscInfo hMiscInfo = HotelMiscInfo.builder().supplierStaticHotelId(hotelId)
				.supplierBookingReference(booking.getBookingnumber()).build();
		HotelInfo hInfo = HotelInfo.builder().options(optionList).miscInfo(hMiscInfo).name(booking.getHotelName())
				.contact(Contact.builder().phone(booking.getHotelPhone()).build()).build();
		HotelUtils.setBufferTimeinCnp(hInfo.getOptions(), searchQuery);
		return hInfo;
	}

	private List<Option> getHotelOption(Booking booking) {
		String mealMapping = booking.getMeal();
		List<RoomInfo> roomInfos = new ArrayList<>();
		List<Option> optionList = new ArrayList<>();
		int noOfRooms = booking.getNumberofrooms();
		Double price = getPrice(booking.getPrices(), booking.getCurrency());
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		String orderStatus = booking.getBookingStatus().toLowerCase().equals("active") ? OrderStatus.SUCCESS.name()
				: OrderStatus.CANCELLED.name();
		for (int i = 0; i < noOfRooms; i++) {
			RoomInfo room = new RoomInfo();
			List<String> amenities = new ArrayList<>();
			amenities.add(mealMapping);
			room.setRoomCategory(booking.getRoomType());
			room.setCheckInDate(getDate(booking.getCheckindate().toString()));
			room.setCheckOutDate(getDate(booking.getCheckoutdate().toString()));
			room.setRoomType(booking.getRoomType());
			setPerNightRoomPrice(room, price, numberOfNights, noOfRooms);
			room.setMiscInfo(RoomMiscInfo.builder().status(orderStatus).amenities(amenities).inclusive(price).build());
			room.setRoomAdditionalInfo(RoomAdditionalInfo.builder().roomId(booking.getRoomType()).build());
			roomInfos.add(room);
		}
		updatePriceWithClientCommission(roomInfos);
		Option option = Option.builder().totalPrice(price)
				.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
						.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
						.supplierHotelId(String.valueOf(booking.getHotelId()))
						.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
				.roomInfos(roomInfos).id(RandomStringUtils.random(20, true, true)).build();
		setOptionCancellationPolicy(booking.getCancellationpolicies(), option, price, noOfRooms);
		optionList.add(option);

		return optionList;
	}

	private Double getPrice(Prices prices, String currency) {
		for (Price price : prices.getPrice()) {
			if (price.getCurrency().equals(currency)) {
				return price.getValue().doubleValue();
			}
		}
		return null;
	}

	private void setOptionCancellationPolicy(List<Cancellationpolicies> cancellationpolicies, Option option,
			double fitruumsPrice, int noOfRooms) {
		LocalDateTime checkInDate = searchQuery.getCheckinDate().atTime(12, 00);
		boolean isFullRefundAllowed = true;
		LocalDateTime currentDate = LocalDateTime.now();
		LocalDateTime fromDate = null;
		List<PenaltyDetails> pds = new ArrayList<>();
		Collections.reverse(cancellationpolicies);
		for (Cancellationpolicies policy : cancellationpolicies) {
			if (Objects.isNull(policy.getDeadline())) {
				fromDate = LocalDateTime.now();
				isFullRefundAllowed = false;
			} else {
				Long deadline = (long) policy.getDeadline();
				fromDate = checkInDate.minusHours(deadline);
			}
			PenaltyDetails penaltyDetail = PenaltyDetails.builder().fromDate(fromDate).toDate(checkInDate)
					.penaltyAmount(fitruumsPrice * policy.getPercentage() / 100)
					.penaltyPercent((double) policy.getPercentage()).build();
			pds.add(penaltyDetail);
			checkInDate = fromDate;
		}
		if (isFullRefundAllowed) {
			PenaltyDetails penaltyDetail = PenaltyDetails.builder().fromDate(currentDate).toDate(checkInDate)
					.penaltyAmount(0.0).penaltyPercent(0.0).build();
			pds.add(0, penaltyDetail);
		}
		pds = pds.stream().sorted(Comparator.comparingDouble(PenaltyDetails::getPenaltyAmount))
				.collect(Collectors.toList());
		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder().id(option.getId())
				.isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds).miscInfo(CancellationMiscInfo.builder()
						.isBookingAllowed(true).isSoldOut(false).isCancellationPolicyBelongToRoom(true).build())
				.build();
		option.setDeadlineDateTime(HotelUtils.getDeadlineDateTimeFromPd(cancellationPolicy));
		option.setCancellationPolicy(cancellationPolicy);
	}
}
