package com.tgs.services.hms.manager.mapping;

import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.hms.datamodel.qtech.SupplierHotelIdMapping;
import com.tgs.services.hms.dbmodel.DbSupplierHotelIdMapping;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.service.HotelStaticDataService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SupplierHotelIdMappingSaveManager {
	@Autowired
	HotelInfoService infoService;

	public void process() throws Exception {

	}

	public boolean saveHotelIdMapping(DbSupplierHotelIdMapping dbHotelIdMapping,
			HotelStaticDataService staticDataService) {
		SupplierHotelIdMapping oldHotelidMappingObject =
				staticDataService.getHotelIdMappingFromDB(dbHotelIdMapping.getHotelId());

		if (Objects.isNull(oldHotelidMappingObject)) {
			dbHotelIdMapping = infoService.save(dbHotelIdMapping);
			log.debug("New hotelid mapping object saved with hotelid {}", dbHotelIdMapping.getHotelId());
			return true;
		} else {
			DbSupplierHotelIdMapping oldDbObject = new DbSupplierHotelIdMapping().from(oldHotelidMappingObject);
			Long oldId = oldDbObject.getId();

			DbSupplierHotelIdMapping updatedDbObject =
					new GsonMapper<>(dbHotelIdMapping, oldDbObject, DbSupplierHotelIdMapping.class).convert();
			updatedDbObject.setId(oldId);
			log.debug("Updated Hotelid mapping Object for supplier {}, hotelid {}, supplierhotelid {}",
					updatedDbObject.getSupplierName(), updatedDbObject.getHotelId(),
					updatedDbObject.getSupplierHotelId());
			dbHotelIdMapping = infoService.save(updatedDbObject);
			return false;
		}
	}

}
