package com.tgs.services.hms.sources.travelbullz;

import lombok.Getter;

@Getter
public enum TravelbullzOrderMapping {
	CONFIRMED("SUCCESS"),
	VOUCHERED("SUCCESS"),
	PENDING("PENDING"),
	CANCELLED("CANCELLED");
	
	public String getStatus() {
		return this.getCode();
	}

	private String code;
	TravelbullzOrderMapping(String code){
		this.code=code;
	}
}
