package com.tgs.services.hms.helper;

import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.FareChangeAlert;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;

@Service
public class HotelPriceValidation extends AbstractHotelPriceValidation {

	@Override
	public FareChangeAlert updatePriceIfChanged(HotelSearchQuery searchQuery, HotelInfo hInfo, boolean isToSendFareDifference, String bookingId) throws Exception {
		return super.updatePriceIfChanged(searchQuery, hInfo, isToSendFareDifference, bookingId);
	}
}
