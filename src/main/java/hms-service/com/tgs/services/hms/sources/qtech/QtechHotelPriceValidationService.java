package com.tgs.services.hms.sources.qtech;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.CancellationValidationRequest;
import com.tgs.services.hms.datamodel.qtech.PriceChangeResponse;
import com.tgs.services.hms.datamodel.qtech.RoomRates;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class QtechHotelPriceValidationService {

	private HotelSearchQuery searchQuery;
	private Option option;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConfigOutput;
	private String bookingId;
	private Option updatedOption;
	private RestAPIListener listener;
	private LocalDateTime currentTime;

	public void fetchPriceChanges() throws Exception {
		HttpUtils httpUtils = null;
		PriceChangeResponse priceChangeResponse = null;
		try {
			listener = new RestAPIListener("");
			CancellationValidationRequest validationRequest = createPriceValidationRequest();
			httpUtils = QTechUtil.getResponseURL(validationRequest, supplierConf, sourceConfigOutput);
			httpUtils.setPrintResponseLog(true);
			priceChangeResponse = httpUtils.getResponse(PriceChangeResponse.class).orElse(null);
			currentTime = LocalDateTime.now();
			updatedOption = createUpdatedOption(priceChangeResponse);
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.REVIEW.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (Objects.isNull(priceChangeResponse)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder()
						.supplierName(supplierConf.getHotelSupplierCredentials().getUserName().toUpperCase())
						.requestType(BaseHotelConstants.PRICE_CHECK).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(bookingId)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
			}
		}
	}

	private Option createUpdatedOption(PriceChangeResponse priceChangeResponse) {

		String currency = HotelBaseSupplierUtils.getSupplierCurrency(supplierConf);

		Map<String, String> roommBlockIdAsKeyRoomAsValue = option.getRoomInfos().stream()
				.collect(Collectors.toMap(RoomInfo::getId, room -> room.getMiscInfo().getRoomBookingId()));
		Option updatedOption = Option.builder().build();
		updatedOption.setId(priceChangeResponse.getSectionUniqueId());
		List<RoomInfo> roomInfos = new ArrayList<>();
		for (RoomRates roomRates : priceChangeResponse.getRoomRates()) {
			int numberOfRooms = roomRates.getNumberOfRooms();
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setMiscInfo(RoomMiscInfo.builder().roomBookingId(roomRates.getClassUniqueId())
					.secondarySupplier(supplierConf.getBasicInfo().getSupplierId()).build());

			QTechUtil.populatePriceInRoomInfo(roomInfo, roomRates.getRateBreakup(), currency);

			for (Entry<String, String> entry : roommBlockIdAsKeyRoomAsValue.entrySet()) {
				if (entry.getValue().equals(roomRates.getClassUniqueId())) {
					roomInfo.setId(entry.getKey());
					break;
				}
			}
			roomInfos.add(roomInfo);
			if (numberOfRooms > 1) {
				for (int r = 1; r < numberOfRooms; r++) {
					RoomInfo copyRoomInfo = new GsonMapper<>(roomInfo, RoomInfo.class).convert();
					roomInfos.add(copyRoomInfo);
				}
			}
		}
		updatedOption.setRoomInfos(roomInfos);
		return updatedOption;
	}

	private CancellationValidationRequest createPriceValidationRequest() throws InterruptedException {

		Thread.sleep(2000);
		CancellationValidationRequest validationRequest = new CancellationValidationRequest();
		validationRequest.setAction(QTechConstants.ACTION_CV.getValue());
		validationRequest.setIsValuation("yes");
		validationRequest.setHotel_id(option.getMiscInfo().getSupplierHotelId());
		validationRequest.setUnique_id(option.getMiscInfo().getSupplierSearchId());
		validationRequest.setSection_unique_id(option.getMiscInfo().getHotelOptionId());
		return validationRequest;
	}
}
