package com.tgs.services.hms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.hms.sources.tripadvisor.TripAdvisorSaveManager;

@RestController
@RequestMapping("/hms/v1")
public class ReviewSaveController {
	
	@Autowired
	TripAdvisorSaveManager saveManager;
	
	@RequestMapping(value = "/trip-advisor-mapping", method = RequestMethod.POST)
	protected String saveLocationMappingIntoDb(HttpServletRequest request, HttpServletResponse response) {
		saveManager.save();
		return "Records Saved Successfully";
	}
}
