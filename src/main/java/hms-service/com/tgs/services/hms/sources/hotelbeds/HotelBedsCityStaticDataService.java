package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.hotelBeds.CityResponse;
import com.tgs.services.hms.datamodel.hotelBeds.Country;
import com.tgs.services.hms.datamodel.hotelBeds.CountryResponse;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsStaticDataRequest;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.services.hms.utils.HotelBaseSupplierUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Builder
@Slf4j
public class HotelBedsCityStaticDataService {

	protected HotelSupplierConfiguration supplierConf;
	protected HotelSourceConfigOutput sourceConfig;
	protected HotelStaticDataRequest staticDataRequest;
	private HotelSupplierRegionInfoManager supplierRegionInfoManager;
	private HotelRegionInfoMappingManager regionInfoMappingManager;

	public Map<String, String> getHotelBedsCityMap() throws IOException {

		Map<String, String> countryNameCodeMap = new HashMap<String, String>();
		HotelBedsStaticDataRequest staticRequest = HotelBedsStaticDataRequest.builder().fields("all")
				.language(HotelBedsConstant.LANGUAGE.value).from(1).to(sourceConfig.getCityHitCount()).build();
		HttpUtilsV2 httpUtils = HotelBedsUtils.getHttpUtils(null, staticRequest, supplierConf,
				supplierConf.getHotelAPIUrl(HotelUrlConstants.COUNTRY_INFO));
		CountryResponse response = httpUtils.getResponse(CountryResponse.class).orElse(null);
		if (response != null) {
			countryNameCodeMap = getHashMapFromHotelBedsCountry(response);
		}
		return countryNameCodeMap;
	}

	private Map<String, String> getHashMapFromHotelBedsCountry(CountryResponse response) {

		return response.getCountries().stream()
				.collect(Collectors.toMap(Country::getIsoCode, country -> country.getDescription().getContent()));
	}

	public void saveHotelBedsCityList() throws IOException {
		Map<String, String> countryNameCodeMap = getHotelBedsCityMap();
		List<HotelRegionInfoQuery> regionInfoQueries = new ArrayList<>();

		for (Map.Entry<String, String> country : countryNameCodeMap.entrySet()) {
			HotelBedsStaticDataRequest staticRequest = HotelBedsStaticDataRequest.builder().fields("all")
					.countryCodes(country.getKey()).language(HotelBedsConstant.LANGUAGE.value).from(1)
					.to(sourceConfig.getCityHitCount()).build();
			HttpUtilsV2 httpUtils = HotelBedsUtils.getHttpUtils(null, staticRequest, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.CITY_INFO));

			CityResponse response = httpUtils.getResponse(CityResponse.class).orElse(null);
			log.info("Total cities in country : {} is {}", country.getValue(), response.getDestinations().size());
			response.getDestinations().forEach(destination -> {
				HotelRegionInfoQuery regionInfoQuery = new HotelRegionInfoQuery();
				if (Objects.nonNull(destination.getName())) {
					regionInfoQuery.setCountryId(String.valueOf(destination.getCode()));
					regionInfoQuery.setRegionName(destination.getName().getContent());
					regionInfoQuery.setRegionType("CITY");
					regionInfoQuery.setCountryName(country.getValue());
					regionInfoQuery.setCountryId(country.getKey());
					regionInfoQuery.setSupplierName(HotelSourceType.HOTELBEDS.name());
					regionInfoQueries.add(regionInfoQuery);
				}
			});
		}
		log.info("Total regions of HOTELBEDS are :" + regionInfoQueries.size());
		HotelBaseSupplierUtils.saveOrUpdateRegionInfo(regionInfoQueries, staticDataRequest.getIsMasterData(), false);
	}
}
