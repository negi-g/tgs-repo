package com.tgs.services.hms.sources.travelbullz;

import lombok.Getter;

@Getter
public enum TravelbullzPrefixMapping {
	MR("Mr."), MRS("Mrs."), MS("Ms.");

	public String getStatus() {
		return this.getTitle();
	}

	private String title;

	TravelbullzPrefixMapping(String title) {
		this.title = title;
	}
}
