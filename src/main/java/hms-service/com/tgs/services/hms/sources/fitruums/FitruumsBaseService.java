package com.tgs.services.hms.sources.fitruums;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomAdditionalInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.fitruums.CancellationPolicy;
import com.tgs.services.hms.datamodel.fitruums.Meal;
import com.tgs.services.hms.datamodel.fitruums.Room;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Getter
@Setter
@Slf4j
public class FitruumsBaseService {

	protected MoneyExchangeCommunicator moneyExchnageComm;
	protected HotelSupplierConfiguration supplierConf;
	protected HotelSearchQuery searchQuery;
	protected HotelSourceConfigOutput sourceConfig;
	protected HotelCacheHandler cacheHandler;
	protected RestAPIListener listener;

	protected List<RoomInfo> getRooms(Room fitruumsroom, String roomTypeId, Meal meal) {
		List<RoomInfo> roomList = new ArrayList<RoomInfo>();
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setNumberOfAdults(room.getNumberOfAdults());
			roomInfo.setNumberOfChild(room.getNumberOfChild());
			populateRoomInfo(roomInfo, fitruumsroom, meal);
			roomInfo.setId(roomTypeId);
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
			roomInfo.setRoomAdditionalInfo(RoomAdditionalInfo.builder().roomId(roomInfo.getId()).build());
			roomList.add(roomInfo);
		}
		updatePriceWithClientCommission(roomList);
		return roomList;
	}

	private void populateRoomInfo(RoomInfo roomInfo, Room fitruumsRoom, Meal meal) {
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		int noOfRooms = searchQuery.getRoomInfo().size();
		double price = meal.getPrices().getPrice().getValue();
		setPerNightRoomPrice(roomInfo, price, numberOfNights, noOfRooms);
		boolean isExtraBedIncluded = getExtraBedInfo(fitruumsRoom);
		roomInfo.setIsextraBedIncluded(isExtraBedIncluded);
		List<String> amenities = new ArrayList<>();
		amenities.add(String.valueOf(meal.getId()));
		RoomMiscInfo roomMiscInfo = RoomMiscInfo.builder().ratePlanCode(String.valueOf(meal.getId()))
				.roomIndex((int) fitruumsRoom.getId()).inclusive(price).amenities(amenities).build();
		roomInfo.setMiscInfo(roomMiscInfo);
	}

	private boolean getExtraBedInfo(Room fitruumsRoom) {

		if (StringUtils.isNotEmpty("" + fitruumsRoom.getExtrabeds())) {
			if (fitruumsRoom.getExtrabeds() >= 1)
				return true;
		}
		return false;
	}

	public String getDate(LocalDate date) {

		return date.format(DateTimeFormatter.ofPattern("uuuu-MM-dd"));
	}

	protected void setPerNightRoomPrice(RoomInfo roomInfo, double price, int numberOfNights, int noOfRooms) {

		List<PriceInfo> priceInfoList = new ArrayList<>();
		double pernightRoomSurcharge = price / numberOfNights;
		for (int j = 1; j <= numberOfNights; j++) {
			PriceInfo priceInfo = new PriceInfo();
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, pernightRoomSurcharge);
			fareComponents.put(HotelFareComponent.SBP, pernightRoomSurcharge);
			priceInfo.setDay(j);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
		}
		roomInfo.setPerNightPriceInfos(priceInfoList);

	}

	protected void setOptionCancellationPolicy(List<CancellationPolicy> list, Option option, int fitruumsPrice) {
		LocalDateTime checkInDate = searchQuery.getCheckinDate().atTime(12, 00);
		boolean isFullRefundAllowed = true;
		LocalDateTime currentDate = LocalDateTime.now();
		LocalDateTime fromDate = null;
		List<PenaltyDetails> pds = new ArrayList<>();
		Collections.reverse(list);
		for (CancellationPolicy policy : list) {
			if (Objects.isNull(policy.getDeadline())) {
				fromDate = LocalDateTime.now();
				isFullRefundAllowed = false;
			} else {
				Long deadline = Long.parseLong(policy.getDeadline());
				fromDate = checkInDate.minusHours(deadline);
			}
			PenaltyDetails penaltyDetail = PenaltyDetails.builder().fromDate(fromDate).toDate(checkInDate)
					.penaltyAmount((double) fitruumsPrice * policy.getPercentage() / 100)
					.penaltyPercent((double) policy.getPercentage()).build();
			pds.add(penaltyDetail);
			checkInDate = fromDate;
		}
		if (isFullRefundAllowed) {
			PenaltyDetails penaltyDetail = PenaltyDetails.builder().fromDate(currentDate).toDate(checkInDate)
					.penaltyAmount(0.0).penaltyPercent(0.0).build();
			pds.add(0, penaltyDetail);
		}
		pds = pds.stream().sorted(Comparator.comparingDouble(PenaltyDetails::getPenaltyAmount))
				.collect(Collectors.toList());
		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder().id(option.getId())
				.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build())
				.isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds).build();
		option.setDeadlineDateTime(HotelUtils.getDeadlineDateTimeFromPd(cancellationPolicy));
		option.setCancellationPolicy(cancellationPolicy);
	}

	public void updatePriceWithClientCommission(List<RoomInfo> rInfoList) {

		double supplierMarkup = sourceConfig.getSupplierMarkup() == null ? 0.0 : sourceConfig.getSupplierMarkup();

		for (RoomInfo roomInfo : rInfoList) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				double supplierGrossPrice = priceInfo.getFareComponents().get(HotelFareComponent.BF);
				priceInfo.getFareComponents().put(HotelFareComponent.CMU, (supplierGrossPrice * supplierMarkup) / 100);
				priceInfo.getFareComponents().put(HotelFareComponent.SNP, supplierGrossPrice);
				priceInfo.getFareComponents().put(HotelFareComponent.SGP, supplierGrossPrice);
				priceInfo.getFareComponents().put(HotelFareComponent.BF, supplierGrossPrice);
			}
		}
	}

	public String getKeyValuePair(HashMap<String, String> params) throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (Map.Entry<String, String> entry : params.entrySet()) {
			if (first) {
				first = false;
			} else
				result.append("&");
			result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
		}
		return result.toString();
	}

	protected HashMap<String, String> setBasicRequirement(HashMap<String, String> map) {
		map.put("userName", supplierConf.getHotelSupplierCredentials().getUserName());
		map.put("password", supplierConf.getHotelSupplierCredentials().getPassword());
		map.put("language", "en");

		return map;
	}

	public HashMap<String, String> setAdultChild() {
		List<RoomSearchInfo> roomList = searchQuery.getRoomInfo();
		HashMap<String, String> newMap = new HashMap<>();
		int noOfAdult = 0;
		int noOfChild = 0;
		int noOfInfant = 0;
		List<String> childAges = new ArrayList<>();
		for (RoomSearchInfo room : roomList) {
			noOfAdult += room.getNumberOfAdults();
			if (CollectionUtils.isNotEmpty(room.getChildAge()) && !Objects.isNull(room.getNumberOfChild())
					&& room.getNumberOfChild() > 0) {
				noOfChild += room.getNumberOfChild();
				for (int age : room.getChildAge()) {
					if (age < 2) {
						noOfInfant++;
					} else {
						childAges.add(String.valueOf(age));
					}
				}
			}
		}
		newMap.put("adults", String.valueOf(noOfAdult));
		newMap.put("children", String.valueOf(noOfChild - noOfInfant));
		newMap.put("infant", String.valueOf(noOfInfant));
		newMap.put("childrenAges", String.join(",", childAges) != null ? String.join(",", childAges) : "");

		return newMap;
	}

	public HashMap<String, String> getBookRetrieveMap(String bookingId) {
		HashMap<String, String> searchRequestmap = new HashMap<>();
		searchRequestmap = setBasicRequirement(searchRequestmap);
		searchRequestmap.put("bookingID", "");
		searchRequestmap.put("reference", bookingId);
		searchRequestmap.put("createdDateFrom", "");
		searchRequestmap.put("createdDateTo", "");
		searchRequestmap.put("arrivalDateFrom", "");
		searchRequestmap.put("arrivalDateTo", "");
		return searchRequestmap;
	}

}
