package com.tgs.services.hms.sources.qtech;

import lombok.Getter;

@Getter
public enum QTechConstants {
	
    HOTEL_BASE_URL("https://hotel.atlastravelsonline.com/ws/index.php"),
	ACTION_S("hotel_search"),
	ACTION_D("hotel_detail"),
	ACTION_C("hotel_cancellation_policy"),
	ACTION_B("hotel_reservation"),
	ACTION_BD("booking_detail"),
	ACTION_BC("cancel_the_booking"),
	ACTION_CV("per_night_rate_info"),
	USERNAME("techno_ws"),
	PASSWORD("tech@123"),
	NAME("QTECH");
	
	private String value;

	private QTechConstants(String value) {
		this.value = value;
	}

}