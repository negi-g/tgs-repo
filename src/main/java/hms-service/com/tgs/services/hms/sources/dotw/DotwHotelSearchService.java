package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.cxf.common.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelProbeLogsMetaInfo;
import com.tgs.services.hms.datamodel.HotelSearchPreferences;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwFilterComplexCriteria;
import com.tgs.services.hms.datamodel.dotw.DotwFilterCriteria;
import com.tgs.services.hms.datamodel.dotw.DotwHotel;
import com.tgs.services.hms.datamodel.dotw.Filter;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class DotwHotelSearchService extends DotwBaseService {

	private HotelSearchResult searchResult;
	private Set<String> propertyIds;

	public void doSearch(int threadCount) throws IOException, JAXBException {
		HttpUtils httpUtils = null;
		try {
			listener = new RestAPIListener("");
			Customer customer = getRequestForHotelSearch();
			String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest, supplierConf);
			httpUtils.setPrintResponseLog(false);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
			List<HotelInfo> hotelList = new ArrayList<>();
			if (Objects.nonNull(result.getSuccessful())) {
				hotelList = getHotelListFromDotwResult(result);
				updateHotelResult(hotelList);
			}
			searchResult = HotelSearchResult.builder().build();
			searchResult.setHotelInfos(hotelList);
		} catch (Exception e) {
			log.info("Unable to parse supplier result for searchid {} and request number {} due to",
					searchQuery.getSearchId(), threadCount, e);
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.SEARCH.name()));
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.DOTW.name())
						.requestType(BaseHotelConstants.SEARCH).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchQuery.getSearchId()).threadCount(threadCount)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (!ObjectUtils.isEmpty(result.getRequest()) && !ObjectUtils.isEmpty(result.getRequest().getError())
						&& result.getRequest().getError().getDetails() != null) {
					SystemContextHolder.getContextData().getErrorMessages()
							.add(result.getRequest().getError().getDetails());
				}
			}
		}
	}

	private void updateHotelResult(List<HotelInfo> hotelInfoList) {

		for (HotelInfo hInfo : hotelInfoList) {
			for (Option option : hInfo.getOptions()) {
				option.getMiscInfo().setIsNotRequiredOnDetail(true);
			}
		}
	}

	private Customer getRequestForHotelSearch() {

		Customer customer = getCustomer(supplierConf);
		customer.setLanguage("en");
		customer.setProduct("hotel");
		Request request = new Request();
		request.setCommand("searchhotels");
		request.setSearchRequest(getSearchCriteriaFromSearchQuery(searchQuery));

		Filter filter = new Filter();
		updateRatingFilterInRequest(filter);
		updatePropertyIdFilterInRequest(filter);
		// filter.setCity(supplierCityInfo.getSupplierCity());
		request.getRet().setFilters(filter);
		customer.setRequest(request);
		return customer;

	}

	private void updateRatingFilterInRequest(Filter filter) {

		DotwFilterComplexCriteria complexCondition = new DotwFilterComplexCriteria();
		HotelSearchPreferences hotelSearchPreferences = searchQuery.getSearchPreferences();

		if (!CollectionUtils.isEmpty(hotelSearchPreferences.getRatings())) {
			DotwFilterCriteria filterCriteria = new DotwFilterCriteria();
			filterCriteria.setFieldName("rating");
			filterCriteria.setFieldTest("in");
			filterCriteria.setFieldValue(getFieldValues(hotelSearchPreferences.getRatings()));
			if (hotelSearchPreferences.getFetchSpecialCategory() != null
					&& hotelSearchPreferences.getFetchSpecialCategory()) {
				filterCriteria.getFieldValue().add("55835");
			}
			complexCondition.getFilterCriteria().add(filterCriteria);
			filter.setCondition(complexCondition);
		}
	}

	private void updatePropertyIdFilterInRequest(Filter filter) {

		DotwFilterComplexCriteria complexCondition = new DotwFilterComplexCriteria();

		if (!CollectionUtils.isEmpty(propertyIds)) {
			DotwFilterCriteria filterCriteria = new DotwFilterCriteria();
			filterCriteria.setFieldName("hotelId");
			filterCriteria.setFieldTest("in");
			filterCriteria.setFieldValue(propertyIds);
			complexCondition.getFilterCriteria().add(filterCriteria);
			filter.setCondition(complexCondition);
		}
	}

	private Set<String> getFieldValues(List<Integer> input) {

		Set<String> list = new HashSet<>();
		for (Integer rating : input) {
			String dotwCodeForRating = getDotwCodeForRating(String.valueOf(rating));
			if (dotwCodeForRating != null)
				list.add(dotwCodeForRating);
		}
		return list;
	}

	private String getDotwCodeForRating(String rating) {

		if (rating.equals("1"))
			return "559";
		else if (rating.equals("2"))
			return "560";
		else if (rating.equals("3"))
			return "561";
		else if (rating.equals("4"))
			return "562";
		else if (rating.equals("5"))
			return "563";
		return null;

	}

	private List<HotelInfo> getHotelListFromDotwResult(Results result) {
		List<HotelInfo> hInfos = new ArrayList<>();
		for (DotwHotel hotel : result.getHotels()) {
			HotelInfo hInfo = HotelInfo
					.builder().name(hotel.getHotelName()).miscInfo(HotelMiscInfo.builder()
							.searchId(searchQuery.getSearchId()).supplierStaticHotelId(hotel.getHotelid()).build())
					.build();
			List<Option> optionList = getOptionList(hotel, false);
			if (!CollectionUtils.isEmpty(optionList)) {
				hInfo.setOptions(optionList);
				hInfos.add(hInfo);
			}
		}
		return hInfos;
	}

	public void doDetailSearch() throws IOException, JAXBException {
		HttpUtils httpUtils = null;
		String searchId = HotelUtils.getSearchId(hInfo.getId());
		try {
			listener = new RestAPIListener("");
			Customer customer = createDetailRequest(hInfo);
			String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
			if (result.getSuccessful() != null) {
				createDetailResponse(result, hInfo);
			}
		} finally {
			if (Objects.nonNull(httpUtils)) {
				List<CheckPointData> checkPointsMap = httpUtils.getCheckPoints();
				checkPointsMap.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
						.time(System.currentTimeMillis()).build());
				httpUtils.setCheckPoints(checkPointsMap);
				httpUtils.getCheckPoints().forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.DETAIL.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				HotelUtils.storeLogs(HotelProbeLogsMetaInfo.builder().supplierName(HotelSourceType.DOTW.name())
						.requestType(BaseHotelConstants.DETAILSEARCH).headerParams(httpUtils.getHeaderParams())
						.responseString(httpUtils.getResponseString()).urlString(httpUtils.getUrlString())
						.postData(httpUtils.getPostData()).logKey(searchId)
						.responseGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.requestGenerationTime(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name())).build());

				if (!ObjectUtils.isEmpty(result.getRequest()) && !ObjectUtils.isEmpty(result.getRequest().getError())
						&& result.getRequest().getError().getDetails() != null) {
					SystemContextHolder.getContextData().getErrorMessages()
							.add(result.getRequest().getError().getDetails());
				}
			}
		}
	}

	private void createDetailResponse(Results result, HotelInfo hInfo) {

		DotwHotel hotel = result.getHotel();
		List<Option> optionList = getOptionList(hotel, true);
		if (!CollectionUtils.isEmpty(optionList)) {
			for (Option option : optionList)
				option.getMiscInfo().setIsDetailHit(true);
			hInfo.setOptions(optionList);
		}
	}

	private Customer createDetailRequest(HotelInfo hInfo) {

		Customer customer = getCustomer(supplierConf);
		customer.setProduct("hotel");
		Request request = new Request();
		request.setCommand("getrooms");
		request.setSearchRequest(getSearchCriteriaFromSearchQuery(searchQuery));
		request.getSearchRequest().setProductId(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		customer.setRequest(request);
		return customer;
	}

}
