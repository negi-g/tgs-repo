package com.tgs.services.hms.sources.dotw;

import lombok.Getter;

@Getter
public enum DotwConstants {

	CURRENCY("520");
	
	private DotwConstants(String value) {
		this.value = value;
	}
	
	private String value;
	
}
