package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.base.SpringContext;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaCity;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaCountry;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaStaticDataRequest;
import com.tgs.services.hms.datamodel.agoda.statc.CityFeed;
import com.tgs.services.hms.datamodel.agoda.statc.CountryFeed;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.mapping.HotelRegionInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierRegionInfoManager;
import com.tgs.services.hms.restmodel.HotelRegionInfoQuery;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;

@Builder
public class AgodaCityStaticDataService {

	protected HotelSupplierConfiguration supplierConf;
	private HotelStaticDataRequest staticDataRequest;

	private HotelSupplierRegionInfoManager supplierRegionInfoManager;
	private HotelRegionInfoMappingManager regionInfoMappingManager;

	public void init() {

		regionInfoMappingManager = (HotelRegionInfoMappingManager) SpringContext.getApplicationContext()
				.getBean("hotelRegionInfoMappingManager");

		supplierRegionInfoManager = (HotelSupplierRegionInfoManager) SpringContext.getApplicationContext()
				.getBean("hotelSupplierRegionInfoManager");
	}

	public List<HotelRegionInfoQuery> getAgodaRegionList() throws IOException {

		List<HotelRegionInfoQuery> regions = new ArrayList<>();
		List<AgodaCountry> countryList = getAgodaCountryList();
		for (AgodaCountry agodaCountry : countryList) {
			List<AgodaCity> cityResult = getAgodaCityListForCountry(agodaCountry.getCountryId());
			if (CollectionUtils.isNotEmpty(cityResult)) {
				List<HotelRegionInfoQuery> cities = getRegionsFromAgodaCities(cityResult,
						agodaCountry.getCountryName());
				regions.addAll(cities);
			}
		}
		return regions;
	}

	private List<HotelRegionInfoQuery> getRegionsFromAgodaCities(List<AgodaCity> cityResult, String countryName) {

		List<HotelRegionInfoQuery> regions = new ArrayList<>();
		cityResult.forEach((agodaCity) -> {
			HotelRegionInfoQuery cityInfo = new HotelRegionInfoQuery();
			cityInfo.setRegionName(agodaCity.getCityName());
			cityInfo.setCountryName(countryName);
			cityInfo.setRegionId(String.valueOf(agodaCity.getCityId()));
			cityInfo.setRegionType("CITY");
			cityInfo.setCountryId(String.valueOf(agodaCity.getCountryId()));
			cityInfo.setSupplierName(AgodaConstants.NAME.getValue());
			regions.add(cityInfo);
		});
		return regions;
	}

	public List<AgodaCity> getAgodaCityListForCountry(Long countryId) throws IOException {

		AgodaStaticDataRequest staticRequest =
				AgodaStaticDataRequest.builder().feed_id("3").ocountry_id(String.valueOf(countryId)).build();
		HttpUtils httpUtils =
				AgodaUtil.getHttpUtils(null, HotelUrlConstants.HOTEL_STATIC_DATA, staticRequest, supplierConf);
		httpUtils.setRequestMethod("GET");
		String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);

		CityFeed cityFeed = AgodaMarshaller.unmarshallCityFeed((xmlResponse));
		if (cityFeed != null)
			return cityFeed.getCities();
		return null;
	}

	private List<AgodaCountry> getAgodaCountryList() throws IOException {

		AgodaStaticDataRequest staticRequest = AgodaStaticDataRequest.builder().feed_id("2").build();
		HttpUtils httpUtils =
				AgodaUtil.getHttpUtils(null, HotelUrlConstants.HOTEL_STATIC_DATA, staticRequest, supplierConf);
		httpUtils.setRequestMethod("GET");
		String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
		CountryFeed countryFeed = AgodaMarshaller.unmarshallCountryFeed(xmlResponse);
		if (countryFeed != null)
			return countryFeed.getCountry();
		return null;

	}
}
