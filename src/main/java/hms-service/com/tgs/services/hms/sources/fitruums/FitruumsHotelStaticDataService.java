package com.tgs.services.hms.sources.fitruums;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.fitruums.FitruumsStaticDataRequest;
import com.tgs.services.hms.datamodel.fitruums.GetStaticHotelsAndRoomsResult;
import com.tgs.services.hms.datamodel.fitruums.GetStaticHotelsAndRoomsResult.Hotels.Hotel;
import com.tgs.services.hms.datamodel.fitruums.GetStaticHotelsAndRoomsResult.Hotels.Hotel.Images.Image.FullSizeImage;
import com.tgs.services.hms.datamodel.fitruums.GetStaticHotelsAndRoomsResult.Hotels.Hotel.Images.Image.SmallImage;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.dbmodel.DbHotelRegionInfoMapping;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.service.HotelStaticDataService;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
public class FitruumsHotelStaticDataService extends FitruumsBaseService {

	protected HotelInfoSaveManager hotelInfoSaveManager;
	private HotelStaticDataRequest staticDataRequest;
	private HotelRegionInfoService regionInfoService;
	private HotelStaticDataService staticDataService;

	public void process() {
		List<String> cityList = getFitruumsCityIdList();
		FitruumsMarshaller marshaller = new FitruumsMarshaller();
		log.info("Total cities for FITRUUMS hotel search are {}", cityList.size());
		cityList.forEach(destination -> {
			FitruumsStaticDataRequest sRequest = FitruumsStaticDataRequest.builder().accommodationTypes("").hotelIDs("")
					.resortIDs("").destination(destination).build();
			HttpUtils httpUtils = FitruumsUtil.getHttpUtils(null, HotelUrlConstants.HOTEL_STATIC_DATA, sRequest,
					supplierConf);
			httpUtils.setRequestMethod("GET");
			String xmlResponse = null;
			try {
				xmlResponse = (String) httpUtils.getResponse(null).orElse(null);

				if (xmlResponse != null) {
					GetStaticHotelsAndRoomsResult hotelResponseList = marshaller.unmarshallXML(xmlResponse,
							GetStaticHotelsAndRoomsResult.class);
					List<HotelInfo> hotelList = getHotelListFromResponse(hotelResponseList);
					saveHotelInfoList(hotelList);
					log.info("TOTAL HOTELS FOR CITY {} ARE {}", destination,hotelResponseList.getHotels().getHotel().size());
				}
			} catch (IOException e) {
				log.info("ERROR WHILE SAVING HOTELS : {} ", e.getMessage());
			} finally {

			}
		});

	}

	private List<HotelInfo> getHotelListFromResponse(GetStaticHotelsAndRoomsResult hotelResponseList) {
		List<HotelInfo> hotelList = new ArrayList<>();
		if (hotelResponseList.getHotels() != null && hotelResponseList.getHotels().getHotel().size() > 0) {
			hotelResponseList.getHotels().getHotel().forEach(hotel -> {
				String description = hotel.getDescription() != null ? hotel.getDescription() : "";
				HotelInfo hInfo = HotelInfo.builder().name(hotel.getName().toUpperCase()).description(description)
						.geolocation(GetGeoLocation(hotel)).address(getaddress(hotel))
						.images(getImages(hotel.getImages().getImage())).supplierName(HotelSourceType.FITRUUMS.name())
						.cityName(hotel.getDestination().toUpperCase()).facilities(getFacilities(hotel))
						.countryName(hotel.getHotelAddrCountry().toUpperCase()).id(String.valueOf(hotel.getHotelId()))
						.build();
				hotelList.add(hInfo);
			});
		}
		return hotelList;
	}

	private List<String> getFacilities(Hotel hotel) {
		List<String> featuresList = new ArrayList<>();
		List<com.tgs.services.hms.datamodel.fitruums.GetStaticHotelsAndRoomsResult.Hotels.Hotel.Features.Feature> fitruumsFeature = hotel
				.getFeatures().getFeature();
		if (fitruumsFeature != null) {
			fitruumsFeature.forEach(featureResponse -> {
				featuresList.add(featureResponse.getName());
			});
		}
		return featuresList;
	}

	private List<Image> getImages(
			List<com.tgs.services.hms.datamodel.fitruums.GetStaticHotelsAndRoomsResult.Hotels.Hotel.Images.Image> list) {
		List<Image> images = new ArrayList<>();
		if (list != null) {

			list.forEach(imageResponse -> {

				if (imageResponse.getFullSizeImage() != null && imageResponse.getSmallImage() != null) {
					FullSizeImage fullImg = imageResponse.getFullSizeImage();
					SmallImage smallImg = imageResponse.getSmallImage();
					Image image = Image.builder().bigURL(fullImg.getUrl() != null ? fullImg.getUrl() : "")
							.thumbnail(smallImg.getUrl() != null ? smallImg.getUrl() : "").build();
					images.add(image);
				}
			});
		}
		return images;
	}

	private Address getaddress(Hotel hotel) {
		City city = City.builder().name(hotel.getHotelAddrCity().toUpperCase())
				.code(String.valueOf(hotel.getDestinationId())).build();
		String countryName = (hotel.getHotelAddrCountry().toUpperCase()) != null
				? hotel.getHotelAddrCountry().toUpperCase()
				: "";
		Country country = Country.builder().name(countryName).code(hotel.getHotelAddrCountrycode()).build();
		Address address = Address.builder().postalCode(String.valueOf(hotel.getHotelAddrZip()))
				.address(hotel.getHotelAddress() != null ? hotel.getHotelAddress() : "")
				.addressLine1(hotel.getHotelAddr1() != null ? hotel.getHotelAddr1() : "").city(city).country(country)
				.build();
		return address;
	}

	private GeoLocation GetGeoLocation(Hotel hotel) {

		GeoLocation geoLocation = null;
		if (hotel.getCoordinates() != null) {
			if (hotel.getCoordinates().getLatitude() != null || hotel.getCoordinates().getLongitude() != null) {
				geoLocation = GeoLocation.builder().latitude(String.valueOf(hotel.getCoordinates().getLatitude()))
						.longitude(String.valueOf(hotel.getCoordinates().getLongitude())).build();
			}
		}
		return geoLocation;
	}

	private List<String> getFitruumsCityIdList() {
		if (CollectionUtils.isEmpty(staticDataRequest.getCityIds())) {
			List<DbHotelRegionInfoMapping> supplierRegionInfos =
					regionInfoService.findBySupplierName(HotelSourceType.CLEARTRIP.name());
			return supplierRegionInfos.stream().map(regionInfo -> regionInfo.getId().toString())
					.collect(Collectors.toList());
		} else {
			return staticDataRequest.getCityIds();
		}
	}

	private void saveHotelInfoList(List<HotelInfo> hInfoList) {
		staticDataService = (HotelStaticDataService) HotelUtils.getStaticDataService();
		for (HotelInfo hInfo : hInfoList) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hInfo);
				dbHotelInfo.setSupplierHotelId(hInfo.getId());
				dbHotelInfo.setId(null);
				dbHotelInfo.setSupplierName(HotelSourceType.FITRUUMS.name());
				hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, staticDataService);
			} catch (Exception e) {
				log.error("Error While Storing Master Hotel With Name {} Rating {}", hInfo.getName(), hInfo.getRating(),
						e);
			}
		}
	}

}
