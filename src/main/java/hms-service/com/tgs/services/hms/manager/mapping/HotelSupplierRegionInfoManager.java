package com.tgs.services.hms.manager.mapping;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import com.tgs.services.hms.datamodel.HotelRegionInfoMapping;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelSupplierRegionInfo;
import com.tgs.services.hms.datamodel.HotelSupplierRegionQuery;
import com.tgs.services.hms.dbmodel.DbHotelSupplierRegionInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.jparepository.HotelRegionInfoService;
import com.tgs.services.hms.restmodel.HotelMappedSupplierCityResponse;
import com.tgs.services.hms.restmodel.HotelSupplierRegionResponse;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelSupplierRegionInfoManager {

	@Autowired
	HotelRegionInfoService regionInfoService;

	public void fetchAndSaveSupplierRegionInfo(HotelStaticDataRequest staticDataRequest) {
		try {
			staticDataRequest.setLanguage(HotelUtils.getLanguage());
			if (StringUtils.isBlank(staticDataRequest.getSupplierId())) {
				throw new CustomGeneralException(SystemError.INVALID_SUPPLIERID);
			}
			AbstractStaticDataInfoFactory staticDataInfoFactory =
					HotelSourceType.getStaticDataFactoryInstance(staticDataRequest.getSupplierId(), staticDataRequest);
			staticDataInfoFactory.getCityMappingInfo();
		} catch (Exception e) {
			log.error("Unable to handle supplier city info for {}", GsonUtils.getGson().toJson(staticDataRequest), e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}

	public void saveSupplierRegionInfo(List<HotelSupplierRegionInfo> supplierRegionInfos) {

		supplierRegionInfos.forEach(supplierRegionInfo -> {
			DbHotelSupplierRegionInfo dbSupplierRegionInfo = new DbHotelSupplierRegionInfo().from(supplierRegionInfo);
			dbSupplierRegionInfo.setProcessedOn(LocalDateTime.now());
			try {
				dbSupplierRegionInfo = regionInfoService.saveSupplierRegionInfo(dbSupplierRegionInfo);
				log.debug("New supplier region info object saved with region id {}", dbSupplierRegionInfo.getId());
			} catch (Exception e) {
				DbHotelSupplierRegionInfo oldSupplierRegionInfo = regionInfoService
						.findByRegionNameAndStateNameAndCountryNameAndSupplierNameAndRegionTypeAndFullRegionName(
								supplierRegionInfo.getRegionName(), supplierRegionInfo.getStateName(),
								supplierRegionInfo.getCountryName(), supplierRegionInfo.getSupplierName(),
								supplierRegionInfo.getRegionType(), supplierRegionInfo.getFullRegionName());
				oldSupplierRegionInfo = new GsonMapper<>(dbSupplierRegionInfo, oldSupplierRegionInfo,
						DbHotelSupplierRegionInfo.class).convert();
				log.debug("Updated Region Info Object with regionName {}, countryName {} and supplier {} ",
						oldSupplierRegionInfo.getRegionName(), oldSupplierRegionInfo.getCountryName(),
						oldSupplierRegionInfo.getSupplierName());
				regionInfoService.saveSupplierRegionInfo(oldSupplierRegionInfo);
			}
		});
	}

	public HotelSupplierRegionResponse getSupplierRegionInfo(HotelSupplierRegionQuery supplierRegionQuery) {

		HotelSupplierRegionResponse supplierRegionResponse = new HotelSupplierRegionResponse();
		if (ObjectUtils
				.firstNonNull(supplierRegionQuery.getRegionId(), supplierRegionQuery.getRegionName(),
						supplierRegionQuery.getStateId(), supplierRegionQuery.getStateName(),
						supplierRegionQuery.getCountryId(), supplierRegionQuery.getCountryName(), "NULL")
				.equals("NULL")) {
			throw new CustomGeneralException(SystemError.INVALID_REQUEST);

		} ;

		if (StringUtils.isBlank(supplierRegionQuery.getSupplierName()))
			throw new CustomGeneralException(SystemError.INVALID_SUPPLIERID);

		List<HotelSupplierRegionInfo> supplierRegionInfos = null;
		if (StringUtils.isNotBlank(supplierRegionQuery.getRegionName())) {
			supplierRegionInfos = DbHotelSupplierRegionInfo
					.toDomainList(regionInfoService.findByRegionNameContainingIgnoreCaseAndSupplierName(
							supplierRegionQuery.getRegionName(), supplierRegionQuery.getSupplierName()));
		}
		if (CollectionUtils.isEmpty(supplierRegionInfos)
				&& StringUtils.isNotBlank(supplierRegionQuery.getStateName())) {
			supplierRegionInfos = DbHotelSupplierRegionInfo
					.toDomainList(regionInfoService.findByStateNameContainingIgnoreCaseAndSupplierName(
							supplierRegionQuery.getStateName(), supplierRegionQuery.getSupplierName()));
		}
		if (CollectionUtils.isEmpty(supplierRegionInfos)
				&& StringUtils.isNotBlank(supplierRegionQuery.getCountryName())) {
			supplierRegionInfos = DbHotelSupplierRegionInfo
					.toDomainList(regionInfoService.findByCountryNameContainingIgnoreCaseAndSupplierName(
							supplierRegionQuery.getCountryName(), supplierRegionQuery.getSupplierName()));
		}
		if (CollectionUtils.isEmpty(supplierRegionInfos) && StringUtils.isNotBlank(supplierRegionQuery.getRegionId())) {
			supplierRegionInfos = DbHotelSupplierRegionInfo.toDomainList(regionInfoService
					.findByRegionIdAndSupplierName(supplierRegionQuery.getRegionId(),
							supplierRegionQuery.getSupplierName()));
		}
		if (CollectionUtils.isEmpty(supplierRegionInfos) && StringUtils.isNotBlank(supplierRegionQuery.getStateId())) {
			supplierRegionInfos = DbHotelSupplierRegionInfo.toDomainList(regionInfoService
					.findByStateIdAndSupplierName(supplierRegionQuery.getStateId(),
							supplierRegionQuery.getSupplierName()));
		}
		if (CollectionUtils.isEmpty(supplierRegionInfos)
				&& StringUtils.isNotBlank(supplierRegionQuery.getCountryId())) {
			supplierRegionInfos = DbHotelSupplierRegionInfo
					.toDomainList(regionInfoService.findByCountryIdAndSupplierName(
							supplierRegionQuery.getCountryId(), supplierRegionQuery.getSupplierName()));
		}
		if (CollectionUtils.isNotEmpty(supplierRegionInfos)) {
			supplierRegionInfos.forEach(supplierRegionInfo -> {
				String supplierRegionKey = getRegionKey(supplierRegionInfo, supplierRegionQuery);
				supplierRegionInfo.setSupplierRegionKey(supplierRegionKey);

			});
			supplierRegionResponse.setRegionInfos(supplierRegionInfos);
		}
		return supplierRegionResponse;
	}


	public HotelSupplierRegionInfo findByCityNameAndSupplierName(String cityName, HotelSourceType supplierName) {

		List<HotelSupplierRegionInfo> regionInfos = DbHotelSupplierRegionInfo
				.toDomainList(regionInfoService.findByRegionNameAndSupplierName(cityName, supplierName.name()));
		return regionInfos.get(0);
	}

	public HotelMappedSupplierCityResponse getMappedSupplierRegionInfo(HotelRegionInfo regionInfoQuery) {

		HotelMappedSupplierCityResponse supplierCityResponse = new HotelMappedSupplierCityResponse();
		List<HotelRegionInfoMapping> regionInfoMappings = null;
		List<HotelSupplierRegionInfo> allSupplierRegionInfos = new ArrayList<>();

		if (regionInfoQuery == null || regionInfoQuery.getId() == null
				|| StringUtils.isBlank(Long.toString(regionInfoQuery.getId())))
			throw new CustomGeneralException(SystemError.INVALID_CITY);

		regionInfoMappings = DbHotelSupplierRegionInfo
				.toDomainList(regionInfoService.findByRegionId(regionInfoQuery.getId()));

		for (HotelRegionInfoMapping regionInfoMapping : regionInfoMappings) {
			HotelSupplierRegionInfo supplierRegionInfo =
					regionInfoService.findSupplierRegionInfoById(regionInfoMapping.getSupplierRegionId()).toDomain();
			if (Objects.nonNull(supplierRegionInfo)) {
				supplierRegionInfo.setSupplierRegionKey(supplierRegionInfo.getSupplierRegionName());
				allSupplierRegionInfos.add(supplierRegionInfo);
			}
		}
		supplierCityResponse.setRegionInfos(allSupplierRegionInfos);
		return supplierCityResponse;
	}

	private String getRegionKey(HotelSupplierRegionInfo supplierRegionInfo,
			HotelSupplierRegionQuery supplierRegionQuery) {

		StringBuilder key = new StringBuilder();
		if (!StringUtils.isBlank(supplierRegionQuery.getRegionName()))
			key = key.append(supplierRegionInfo.getRegionName());
		else
			key = key.append("NA");
		if (!StringUtils.isBlank(supplierRegionQuery.getStateName())
				|| !StringUtils.isEmpty(supplierRegionQuery.getRegionName()))
			key = key.append("@" + firstNonEmpty(supplierRegionInfo.getStateName(), "NA"));
		else
			key = key.append("@NA");
		key = key.append("@" + firstNonEmpty(supplierRegionInfo.getCountryName(), "NA"));

		return key.toString();
	}

	private String firstNonEmpty(String... values) {
		if (values != null) {
			for (String val : values) {
				if (!StringUtils.isBlank(val)) {
					return val;
				}
			}
		}
		return null;
	}
}
