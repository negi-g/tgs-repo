package com.tgs.services.hms.mapper;

import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.MethodExecutionInfo;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.analytics.AnalyticsHotelMethodQuery;
import com.tgs.services.hms.analytics.HotelMethodExecutionInfo;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelMethodExecutionInfoMapper extends Mapper<AnalyticsHotelMethodQuery> {

	private User user;
	private ContextData contextData;
	private HotelSearchQuery searchQuery;
	private HotelFlowType flowType;

	@Override
	@SuppressWarnings("unchecked")
	protected void execute() throws CustomGeneralException {

		output = output != null ? output : AnalyticsHotelMethodQuery.builder().build();
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		output.setSearchId(searchQuery.getSearchId());
		output.setAnalyticstype(flowType.name());
		try {
			Map<String, Object> methodExecutionAdditionalInfo = new GsonMapper<>(output, Map.class).convert();
			Map<String, Object> flattenedMethodExecutionInfo = getFlattenMethodExecutionInfo();
			flattenedMethodExecutionInfo.putAll(methodExecutionAdditionalInfo);
			output.setMethodExecutionMapToPush(flattenedMethodExecutionInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Map<String, Object> getFlattenMethodExecutionInfo() {

		Map<String, Object> flattenedMethodExecutionInfo = new LinkedHashMap<>();
		Map<String, Map<String, MethodExecutionInfo>> map = contextData.getMethodExecutionInfo();
		final String methodExecutionType =
				contextData.getValueMap().getOrDefault(BaseHotelConstants.METHOD_EXECUTION_TYPE, "UNKNOWN").toString();
		String supplierName = BaseHotelUtils.getHotelSupplierIds(searchQuery, methodExecutionType).toString();

		Map<String, MethodExecutionInfo> methodWiseExecutionInfo = map.get(supplierName);
		Map<String, Object> additionMethodExecutionInfo = new LinkedHashMap<>();
		Map<String, Object> methodOnlyExecutionInfo = new LinkedHashMap<>();

		if (map.containsKey(supplierName)) {
			methodWiseExecutionInfo.forEach((methodName, methodInfo) -> {

				HotelMethodExecutionInfo methodExecutionInfo = (HotelMethodExecutionInfo) methodInfo;
				if (MapUtils.isEmpty(additionMethodExecutionInfo)) {
					additionMethodExecutionInfo.put("hotelCount", methodExecutionInfo.getHotelCount());
					additionMethodExecutionInfo.put("optionCount", methodExecutionInfo.getOptionCount());
					additionMethodExecutionInfo.put("supplierName", supplierName);
					additionMethodExecutionInfo.put("roomNight", methodExecutionInfo.getRoomNight());
					additionMethodExecutionInfo.put("type", methodExecutionInfo.getType());
					additionMethodExecutionInfo.put("subtype", methodExecutionInfo.getSubtype());
				}
				methodOnlyExecutionInfo.put(methodName, methodExecutionInfo.getTimeInMs());
			});
		} else {
			output.setErrormsg(SystemError.EMPTY_METHOD_EXECUTION_INFO.getMessage());
		}
		flattenedMethodExecutionInfo.putAll(methodOnlyExecutionInfo);
		flattenedMethodExecutionInfo.putAll(additionMethodExecutionInfo);
		return flattenedMethodExecutionInfo;
	}
}
