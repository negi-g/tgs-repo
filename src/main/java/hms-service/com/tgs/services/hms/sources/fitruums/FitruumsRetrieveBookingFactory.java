package com.tgs.services.hms.sources.fitruums;

import java.io.IOException;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class FitruumsRetrieveBookingFactory extends AbstractRetrieveHotelBookingFactory {
	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public FitruumsRetrieveBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		super(supplierConf, importBookingInfo);
	}

	@Override
	public void retrieveBooking() throws IOException, JAXBException {
		FitruumsRetrieveBookingService bookingService = FitruumsRetrieveBookingService.builder()
				.supplierConf(supplierConf).moneyExchnageComm(moneyExchnageComm).cacheHandler(cacheHandler)
				.sourceConfig(sourceConfigOutput).importBookingParams(importBookingParams).build();
		bookingService.retrieveBooking();
		bookingDetailResponse = bookingService.getBookingInfo();

	}

}
