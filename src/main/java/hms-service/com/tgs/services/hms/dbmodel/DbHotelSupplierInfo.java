package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.utils.TgsSecurityUtils;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierCredential;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@TypeDefs({ @TypeDef(name = "HotelSupplierCredentialType", typeClass = HotelSupplierCredentialType.class) })
@Table(name = "hotelsupplierinfo", uniqueConstraints = { @UniqueConstraint(columnNames = { "id", "name" }) })
@Audited
@ApiModel(value = "To map hotelsupplierinfo table with dbmodel")
public class DbHotelSupplierInfo extends BaseModel<DbHotelSupplierInfo, HotelSupplierInfo> {

	@ApiModelProperty(value = "To provide hotel supplier info created date")
	@CreationTimestamp
	private LocalDateTime createdOn;

	@ApiModelProperty(value = "To provide hotel supplier info processed date")
	@CreationTimestamp
	private LocalDateTime processedOn;

	@ApiModelProperty(value = "To provide hotel supplier credential info", example = "{'userName': 'Sachin', password: '123', 'url': 'http://xyz.com'}")
	@Type(type = "HotelSupplierCredentialType")
	private HotelSupplierCredential credentialInfo;

	@ApiModelProperty(value = "To check whether hotel supplier is active or not", example = "true")
	@Column
	private boolean enabled;

	@ApiModelProperty(value = "To uniquely identify hotel supplier source", example = "1")
	@Column
	private Integer sourceId;

	@ApiModelProperty(value = "To provide hotel supplier name", example = "QTech")
	@Column
	private String name;

	@ApiModelProperty(value = "To check whether hotel supplier is deleted or not.", example = "true")
	@Column
	private boolean isDeleted;

	@Override
	public HotelSupplierInfo toDomain() {
		return toDomain(true);
	}
	
	public HotelSupplierInfo toDomain(boolean decrypt) {
		HotelSupplierInfo si = new GsonMapper<>(this, HotelSupplierInfo.class).convert();
		if (decrypt) {
			HotelSupplierCredential creds = si.getCredentialInfo();
			if (creds != null) {
				try {
					creds.setPassword(TgsSecurityUtils.decryptData(creds.getPassword()));
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		return si;
	}

	@Override
	public DbHotelSupplierInfo from(HotelSupplierInfo dataModel) {
		return from(dataModel, true);
	}

	public DbHotelSupplierInfo from(HotelSupplierInfo dataModel, boolean encrypt) {
		HotelSupplierInfo storedDataModel = this.toDomain();
		HotelSupplierInfo merged = new GsonMapper<>(dataModel, storedDataModel, HotelSupplierInfo.class).convert();
		DbHotelSupplierInfo dbModel = new GsonMapper<>(merged, this, DbHotelSupplierInfo.class).convert();
		if (encrypt) {
			HotelSupplierCredential creds = dbModel.getCredentialInfo();
			if (creds != null) {
				try { 
					creds.setPassword(TgsSecurityUtils.encryptData(creds.getPassword()));
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		return dbModel;
	}
}
