package com.tgs.services.cms.datamodel;


import lombok.Getter;
import lombok.Setter;

import java.util.Map;

import com.tgs.services.base.datamodel.OrderType;

@Getter
@Setter
public class OrderTypeCommission {

    Map<OrderType, Integer> orderTypeCommission;

    public Integer getOrderTypeCommission(OrderType orderType) {
        return orderTypeCommission.getOrDefault(orderType, null);
    }
}
