package com.tgs.services.cms.datamodel.tourcode;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@ApiModel(value = "This is used to fetch tour code based on filter. You can pass either any one field or combination of fields depending upon your search criteria")
@SuperBuilder
public class TourCodeFilter extends QueryFilter {

    @ApiModelProperty(notes = "To fetch based on id ", example = "1")
    private Long id;

    @ApiModelProperty(notes = "To fetch based on supplierId ", example = "IndiGoCorp")
    private String supplierId;

    @ApiModelProperty(notes = "To fetch based on sourceid ", example = "5")
    private String sourceId;

    @ApiModelProperty(notes = "To fetch based on airline ", example = "G8")
    private String airline;

    @ApiModelProperty(notes = "To fetch based on tourcode ", example = "ABCDE")
    private String tourCode;

    @ApiModelProperty(notes = "To fetch based on priority ", example = "2")
    private Integer priority;

    @ApiModelProperty(notes = "To fetch based on deleted", example = "true")
    private Boolean isDeleted;

}
