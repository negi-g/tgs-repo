package com.tgs.services.cms.datamodel.commission;

import java.time.LocalDateTime;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.OrderType;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CommissionPlan extends DataModel {

	@ApiModelProperty(notes = "Which is already created Unique ID for plan,if need to update id is mandatory", example = "24")
	@SearchPredicate(type = PredicateType.IN)
	private Integer id;

	@ApiModelProperty(notes = "Creation Date of Commission Plan", example = "2018-09-27")
	private LocalDateTime createdOn;

	@ApiModelProperty(notes = "Processed on Date of Commission Rule", example = "2018-09-27")
	private LocalDateTime processedOn;

	@ApiModelProperty(notes = "plan to denote which Order", example = "AIR")
	@SearchPredicate(type = PredicateType.IN)
	private OrderType product;

	@ApiModelProperty(notes = "Description of Any Commission Plan", example = "DEFAULT")
	@SearchPredicate(type = PredicateType.EQUAL)
	private String description;

	@ApiModelProperty(notes = "Description of Any Commission Plan", example = "DEFAULT")
	@SearchPredicate(type = PredicateType.EQUAL)
	private String name;

	@ApiModelProperty(notes = "Plan is enabled or not ")
	@SearchPredicate(type = PredicateType.EQUAL)
	private Boolean enabled;

	private Integer ruleId;
}