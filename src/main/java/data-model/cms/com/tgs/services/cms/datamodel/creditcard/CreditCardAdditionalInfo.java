package com.tgs.services.cms.datamodel.creditcard;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class CreditCardAdditionalInfo {

	@SerializedName("bids")
	@ToString.Include
	List<Long> billingEntityIds;

}
