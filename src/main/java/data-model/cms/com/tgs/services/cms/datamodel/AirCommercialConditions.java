package com.tgs.services.cms.datamodel;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirCommercialConditions extends BaseCommericialConditions {

	@SerializedName("aoi")
	private Boolean isApplicableOnInfant;

	@SerializedName("aoas")
	private Boolean isApplicableOnAllSegment;
	
	@SerializedName("aob")
	private Boolean isApplicableOnBooking;
}
