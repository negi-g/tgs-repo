package com.tgs.services.cms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class HotelCommercialInfo extends CommercialInfo {

	private Boolean isOptionRequired;
}
