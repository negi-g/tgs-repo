package com.tgs.services.cms.datamodel.commission;

import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.RestExclude;
import com.tgs.services.base.ruleengine.BasicRuleCriteria;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.cms.datamodel.CommercialComponent;
import com.tgs.services.cms.datamodel.CommercialRuleCriteria;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder(toBuilder = true)
@Getter
@Setter
public class CommissionRule implements IRule {

	@ApiModelProperty(notes = "Which is already created Unique ID for Commissio Rule,if need to update id is mandatory",
			example = "24")
	private Integer id;

	@RestExclude
	@ApiModelProperty(notes = "Creation Date of Commission Rule", example = "2018-09-27")
	private LocalDateTime createdOn;

	@RestExclude
	@ApiModelProperty(notes = "ProcessedOn Date of CommissionRule", example = "2018-09-27")
	private LocalDateTime processedOn;

	@RestExclude
	@ApiModelProperty(notes = "To denote the Order", example = "AIR")
	private Product product;

	@RestExclude
	@ApiModelProperty(notes = "To denote rule is enabled or disabled", example = "TRUE")
	private Boolean enabled;

	@RestExclude
	@ApiModelProperty(notes = "To denote which airline, hotel id, package id ", example = "6E")
	private String code;

	private AirType airType;

	private SearchType searchType;

	@RestExclude
	@ApiModelProperty(notes = "To denote the priority the rule", example = "1")
	private Double priority;

	@ApiModelProperty(notes = "To denote the description of the rule", example = "Maharastra Plan")
	private String description;

	@RestExclude
	@ApiModelProperty(notes = "denote the inclusion criteria of rule", example = "")
	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = FlightBasicRuleCriteria.class),
			@ClassType(keys = {"H", "HOTEL"}, value = HotelBasicRuleCriteria.class)})
	private BasicRuleCriteria inclusionCriteria;

	@RestExclude
	@ApiModelProperty(notes = "denote the exclusion criteria of rule", example = "")
	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = FlightBasicRuleCriteria.class),
			@ClassType(keys = {"H", "HOTEL"}, value = HotelBasicRuleCriteria.class)})
	private BasicRuleCriteria exclusionCriteria;

	@RestExclude
	@ApiModelProperty(notes = "denote the commission criteria of rule", example = "")
	private CommercialRuleCriteria commercialCriteria;

	@Exclude
	private boolean isDeleted;

	@Override
	public CommercialRuleCriteria getOutput() {
		return commercialCriteria;
	}

	@Override
	public double getPriority() {
		return priority != null ? priority.doubleValue() : 0.0;
	}

	@Override
	public boolean getEnabled() {
		return enabled == null ? false : enabled;
	}

	public void cleanData() {
		if (getPriority() < 0.0) {
			setPriority(0.0);
		}
		if (inclusionCriteria != null)
			inclusionCriteria.cleanData();
		if (exclusionCriteria != null)
			exclusionCriteria.cleanData();

		List<CommercialComponent> commercialComponents = commercialCriteria.getComponents();
		if (CollectionUtils.isNotEmpty(commercialComponents)) {
			for (CommercialComponent commercialComponent : commercialComponents) {
				String expression = commercialComponent.getExpression();
				commercialComponent.setExpression(Pattern.compile("\\s+").matcher(expression).replaceAll(""));
			}
		}
	}

	public BasicRuleCriteria getInclusionRuleCriteria(BasicRuleCriteria defaultCriteria) {
		return inclusionCriteria != null ? inclusionCriteria : defaultCriteria;
	}

	@Override
	public boolean exitOnMatch() {
		return true;
	}

	public boolean isValidSource(Integer sourceId) {
		if (this.getProduct().equals(Product.AIR)) {
			FlightBasicRuleCriteria ruleCriteria = (FlightBasicRuleCriteria) this.getInclusionCriteria();
			if ((CollectionUtils.isEmpty(ruleCriteria.getSourceIds())
					|| ruleCriteria.getSourceIds().contains(sourceId))) {
				return true;
			}
		} else if (this.getProduct().equals(Product.HOTEL)) {
			HotelBasicRuleCriteria ruleCriteria = (HotelBasicRuleCriteria) this.getInclusionCriteria();
			if ((CollectionUtils.isEmpty(ruleCriteria.getSourceIds())
					|| ruleCriteria.getSourceIds().contains(sourceId))) {
				return true;
			}
		}
		return false;
	}

	public boolean isValidSupplier(String supplierId) {
		if (this.getProduct().equals(Product.AIR)) {
			FlightBasicRuleCriteria ruleCriteria = (FlightBasicRuleCriteria) this.getInclusionCriteria();
			return CollectionUtils.isEmpty(ruleCriteria.getSupplierIds())
					|| ruleCriteria.getSupplierIds().contains(supplierId);
		}
		return false;
	}
}
