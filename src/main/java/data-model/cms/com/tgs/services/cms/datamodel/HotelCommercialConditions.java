package com.tgs.services.cms.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelCommercialConditions extends BaseCommericialConditions {
	
	@SerializedName("aob")
	private Boolean isApplicableOnBooking;
}
