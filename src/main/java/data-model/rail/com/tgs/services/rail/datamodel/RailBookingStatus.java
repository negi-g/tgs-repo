package com.tgs.services.rail.datamodel;

import lombok.Getter;

@Getter
public enum RailBookingStatus {

	CONFIRMED("CNF"),
	TATKAL_WAITLIST("TQWL"),
	GENERAL_WAITLIST("GNWL"),
	RESERVATION_AGAINST_CANCELLATION("RAC"),
	REMOTE_LOCATION_WAITLIST("RLWL"),
	POOLED_QUOTA_WAITLIST("PQWL"),
	CANCELLED("CAN"),
	WAITLIST("WL"),
	ROAD_SIDE_WAITLIST("RSWL"),
	RELEASED("REL"),
	NO_ROOM("NR"),
	NO_SEAT_BERTH("NOSB");

	private String code;

	RailBookingStatus(String code) {
		this.code = code;
	}

	public static RailBookingStatus getEnumFromCode(String code) {
		for (RailBookingStatus bookingStatus : RailBookingStatus.values()) {
			if (bookingStatus.getCode().equals(code)) {
				return bookingStatus;
			}
		}
		return null;
	}
}
