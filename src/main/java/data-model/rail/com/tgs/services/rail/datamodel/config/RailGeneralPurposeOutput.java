package com.tgs.services.rail.datamodel.config;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailGeneralPurposeOutput implements IRuleOutPut, Cleanable {

	@SerializedName("amdcap")
	private Integer amendmentCap;


}
