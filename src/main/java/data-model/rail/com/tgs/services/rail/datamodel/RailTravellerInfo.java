package com.tgs.services.rail.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailTravellerInfo {

	private Long id;

	@SerializedName("fc")
	private FoodCode foodChoice;

	@SerializedName("bc")
	private String berthChoice;

	@SerializedName("ibro")
	private Boolean isBedRollOpted;

	@SerializedName("icbo")
	private Boolean isChildBerthOpted;

	@SerializedName("ico")
	private Boolean isConcessionOpted;

	@SerializedName("ifgco")
	private Boolean isForGoConcessionOpted;

	@SerializedName("cc")
	private ConcessionCode concessionChoice;

	@SerializedName("picf")
	private Boolean passengerIcardFlag;

	@SerializedName("pict")
	private String passengerCardType;

	@SerializedName("picn")
	private String passengerIcardNumber;

	@SerializedName("pictype")
	private RailPassengerIcardType passengerIcardType;

	@SerializedName("bkseat")
	private RailSeatAllocation bookingSeatAllocation;

	@SerializedName("crseat")
	private RailSeatAllocation currentSeatAllocation;

	@SerializedName("paxfare")
	private Double passengerNetFare;

	@SerializedName("nty")
	private String nationality;

	@SerializedName("gndr")
	private String gender;

	@SerializedName("fn")
	private String fullName;


	private Integer age;

	@SerializedName("pt")
	private RailPaxType paxType;

	private String dob;

	@SerializedName("itdrf")
	private Boolean isTdrFiled;

}
