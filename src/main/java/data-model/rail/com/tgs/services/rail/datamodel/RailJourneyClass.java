package com.tgs.services.rail.datamodel;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public enum RailJourneyClass {

	ANUBHUTI_ClASS("EA"), AC_FIRST_CLASS("1A"), EXECUTIVE_CHAIR_CAR("EC"), AC_2_TIER("2A"), FIRST_CLASS("FC"),
	AC_3_TIER("3A"), AC_3_TIER_ECO("3E"), AC_CHAIR_CAR("CC"), SLEEPER("SL"), SECOND_SITTING("2S");

	private String code;

	RailJourneyClass(String code) {
		this.code = code;
	}

	public String getName() {
		return this.code;
	}

	public static RailJourneyClass getEnumFromCode(String code) {
		return getRailJourneyClass(code);
	}

	private static RailJourneyClass getRailJourneyClass(String code) {
		for (RailJourneyClass journeyClass : RailJourneyClass.values()) {
			if (journeyClass.getCode().equals(code))
				return journeyClass;
		}
		return null;
	}

	public static String getCodeFromEnumName(String name) {
		for (RailJourneyClass journeyClass : RailJourneyClass.values()) {
			if (journeyClass.name().equals(name))
				return journeyClass.code;
		}
		return null;
	}

	public static List<String> getCodes(List<RailJourneyClass> journeyClasses) {
		List<String> types = new ArrayList<>();
		journeyClasses.forEach(type -> {
			types.add(type.getCode());
		});
		return types;
	}

}
