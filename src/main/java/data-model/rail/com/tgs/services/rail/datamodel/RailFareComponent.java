package com.tgs.services.rail.datamodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;

@Getter
public enum RailFareComponent {

	TF("TotalFare") {},
	NF("Net Fare") {},
	RF("Rail Base Fare") {},
	BF("Base Fare") {},
	SC("Superfast Charge"),
	CC("Catering Charge"),
	OC("Other Charges"),
	OT("Other Taxes"),
	ST("Service Tax"),
	TTF("Tatkal Fare"),
	FC("Fuel Charge"),
	TI("Travel Insurance"),
	TIT("Travel Insurance Tax"),
	RC("Reservation Charge"),
	WP("Wpservice Charge"),
	WPT("WpService Tax"),
	CONC("Concession"),
	RCF("Rail Cancellation Fee"),
	RAF("Rail Amendment Fee"),
	AAR("Amount Applicable for Refund"),
	MF("Management Fee") {
		@Override
		public List<RailFareComponent> dependentComponents() {
			return Arrays.asList(MFT);
		}
	},
	MFT("Management Fee Taxes") {},
	PC("Payment Charges") {
		@Override
		public List<RailFareComponent> dependentComponents() {
			return Arrays.asList(PCT);
		}
	},
	PCT("Payment Charges Taxes") {},
	DMF("Displayed Management Fee") {},
	DPC("Dispalyed Payment Charges");

	private String desc;

	RailFareComponent(String desc) {
		this.desc = desc;
	}

	public List<RailFareComponent> dependentComponents() {
		return new ArrayList<>();
	}

	public static RailFareComponent getEnumFromCode(String code) {
		for (RailFareComponent fareCompoment : RailFareComponent.values()) {
			if (fareCompoment.toString().equals(code)) {
				return fareCompoment;
			}
		}
		return null;
	}
}
