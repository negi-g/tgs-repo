package com.tgs.services.rail.datamodel.config;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.rail.datamodel.ruleengine.RailBasicRuleCriteria;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(toBuilder = true)
public class RailConfiguratorInfo extends DataModel implements IRule {

	@SearchPredicate(type = PredicateType.EQUAL)
	private Long id;

	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	private LocalDateTime createdOn;

	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	private LocalDateTime processedOn;

	@SearchPredicate(type = PredicateType.EQUAL)
	@SearchPredicate(type = PredicateType.IN)
	private RailConfiguratorRuleType ruleType;

	private RailBasicRuleCriteria inclusionCriteria;

	private RailBasicRuleCriteria exclusionCriteria;

	@NotNull
	@JsonAdapter(JsonStringSerializer.class)
	private String output;

	private Double priority;

	@SearchPredicate(type = PredicateType.EQUAL, filterName = "enable")
	private Boolean enabled;

	private Boolean exitOnMatch;

	@Exclude
	@SearchPredicate(type = PredicateType.EQUAL)
	private boolean isDeleted;

	private String description;

	@Override
	public IRuleOutPut getOutput() {
		IRuleOutPut ruleOutput = GsonUtils.getGson().fromJson(output, ruleType.getOutPutTypeToken().getType());
		ruleOutput.cleanData();
		return ruleOutput;
	}


	public String getSerializedOutput() {
		return output;
	}

	@Override
	public IRuleCriteria getInclusionCriteria() {
		return inclusionCriteria;
	}

	@Override
	public IRuleCriteria getExclusionCriteria() {
		return exclusionCriteria;
	}

	@Override
	public double getPriority() {
		return priority == null ? 0.0 : priority;
	}

	@Override
	public boolean getEnabled() {
		return enabled == null ? false : enabled;
	}

	public void cleanData() {
		if (getPriority() < 0.0) {
			setPriority(0.0);
		}
		if (inclusionCriteria != null)
			inclusionCriteria.cleanData();
		if (exclusionCriteria != null)
			exclusionCriteria.cleanData();
	}

}
