package com.tgs.services.rail.datamodel.message;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class RailAmendmentMailAttributes extends RailEmailAttributes {

	private String amount;

	private String reason;

	private String generationTime;
}
