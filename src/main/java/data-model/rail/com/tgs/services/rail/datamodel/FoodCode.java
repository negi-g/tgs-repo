package com.tgs.services.rail.datamodel;

import lombok.Getter;

@Getter
public enum FoodCode {

	Veg("V"), NonVeg("N"), NoFood("D");

	private String code;

	FoodCode(String code) {
		this.code = code;

	}

	public static FoodCode getEnumFromCode(String code) {
		for (FoodCode foodCode : FoodCode.values()) {
			if (foodCode.getCode().equals(code)) {
				return foodCode;
			}
		}
		return null;
	}


}
