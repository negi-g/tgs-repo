package com.tgs.services.rail.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailRunningDays {

	@SerializedName("rm")
	private boolean isRunningMon;

	@SerializedName("rt")
	private boolean isRunningTue;

	@SerializedName("rw")
	private boolean isRunningWed;

	@SerializedName("rth")
	private boolean isRunningThu;

	@SerializedName("rf")
	private boolean isRunningFri;

	@SerializedName("rs")
	private boolean isRunningSat;

	@SerializedName("ru")
	private boolean isRunningSun;

}
