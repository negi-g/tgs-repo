package com.tgs.services.rail.datamodel;

import lombok.Getter;

@Getter
public enum RailType {

	Suvidha_Train("SV"),
	Special_Tatkal_Train("ST"),
	Special_Train("SP"),
	Garib_Rath("G"),
	Yuva_Express("Y"),
	Duronto("D"),
	Rajdhani("R"),
	Shatabdi("S"),
	Jan_Shatabdi("JS"),
	Others("O");


	private String code;

	RailType(String code) {
		this.code = code;
	}

	public String getname() {
		return this.code;
	}

	public static RailType getEnumFromCode(String code) {
		for (RailType railType : RailType.values()) {
			if (railType.getCode().equals(code)) {
				return railType;
			}
		}
		return null;

	}


}
