package com.tgs.services.rail.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailSeatAllocation {

	@SerializedName("bc")
	private String berthchoice;

	@SerializedName("cid")
	private String coachId;

	@SerializedName("bthn")
	private Integer berthNo;

	@SerializedName("bthc")
	private String berthCode;

	@SerializedName("sts")
	private String bookingStatus;

}
