package com.tgs.services.rail.datamodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.SystemError;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailFareEnquiry implements Validatable {
	private String trainNo;
	private String fromStn;
	private String boardingstation;
	private LocalDateTime boardingTime;
	private String toStn;
	private String journeyClass;
	private LocalDate travelDate;
	private String bookingQuota;

	private LocalDateTime departure;

	private Boolean paymentEnqFlag;
	private String duration;

	private String searchId;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (trainNo == null) {
			errors.put("fromCityOrAirport", SystemError.INVALID_TRAINNO.getErrorDetail());
		}
		if (fromStn == null) {
			errors.put("toCityOrAirport", SystemError.INVALID_STATION.getErrorDetail());
		}
		if (toStn == null) {
			errors.put("toStn", SystemError.INVALID_STATION.getErrorDetail());
		}
		if (journeyClass == null) {
			errors.put("journeyClass", SystemError.INVALID_JOURNEYCLASS.getErrorDetail());
		}
		if (travelDate == null) {
			errors.put("travelDate", SystemError.INVALID_TRAVEL_DATE.getErrorDetail());
		}

		return errors;
	}


}
