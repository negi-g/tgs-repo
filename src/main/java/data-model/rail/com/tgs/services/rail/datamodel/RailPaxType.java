package com.tgs.services.rail.datamodel;

import lombok.Getter;

@Getter
public enum RailPaxType {

	ADULT("ADT"), CHILD("CHD"), INFANT("INF"), SENIOR("SNR");

	private String code;

	RailPaxType(String code) {
		this.code = code;
	}

	public static RailPaxType getRailPaxTypeFromCode(String code) {
		for (RailPaxType paxType : RailPaxType.values()) {
			if (paxType.getCode().equals(code))
				return paxType;
		}
		return null;
	}

	public static RailPaxType getEnumFromCode(String code) {
		return getRailPaxTypeFromCode(code);
	}

}
