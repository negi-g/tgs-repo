package com.tgs.services.rail.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailGstInfo {

	public Double canChrgCgst;
	public Double canChrgGst;
	public Double canChrgIgst;
	public Double canChrgSgst;
	public Double canChrgUgst;
	public String cancelInvoiceNo;
	public Double cgstRate;
	public Double creditCgst;
	public Double creditGst;
	public Double creditIgst;
	public Double creditSgst;
	public Double creditUgst;
	public Double gstRate;
	public String gstinSuplier;
	public Double igstRate;
	public String prsSuplierState;
	public String prsSuplierStateCode;
	public String sacCode;
	public Double sgstRate;
	public Double ugstRate;
	public String invoiceNumber;
	public Double irctcCgstCharge;
	public Double irctcIgstCharge;
	public Double irctcSgstCharge;
	public Double irctcUgstCharge;
	public Double prsCgstCharge;
	public Double prsIgstCharge;
	public Double prsSgstCharge;
	public Double prsUgstCharge;
	public String suplierAddress;
	public Double taxableAmt;
	public Double totalIrctcGst;
	public Double totalPRSGst;

}
