package com.tgs.services.rail.datamodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailBookingConfiguration {

	@SerializedName("spcltk")
	public Boolean specialTatkal;

	@SerializedName("uidflag")
	public String uidMandatoryFlag;

	@SerializedName("srctznappl")
	public Boolean seniorCitizenApplicable;

	@SerializedName("fce")
	public Boolean foodChoiceEnabled;

	@SerializedName("idr")
	public Boolean idRequired;

	@SerializedName("brflag")
	public Boolean bedRollFlagEnabled;

	@SerializedName("lba")
	public Boolean lowerBerthApplicable;

	@SerializedName("cbm")
	public Boolean childBerthMandatory;

	@SerializedName("tie")
	public Boolean travelInsuranceEnabled;

	@SerializedName("gstappl")
	public Boolean gstDetailInputFlag;

	@SerializedName("forgocnc")
	public Boolean forgoConcession;

	@SerializedName("applberths")
	public List<String> applicableBerthTypes;

	@SerializedName("foodop")
	public List<String> availableFoodDetails;

	@SerializedName("applIds")
	public List<String> applicableIds;

	@SerializedName("ppminl")
	public Integer passportMinLength;

	@SerializedName("ppmaxl")
	public Integer passportMaxLength;

	@SerializedName("picminl")
	public Integer paxIcardMinLength;

	@SerializedName("picmaxl")
	public Integer paxIcardMaxLength;


}
