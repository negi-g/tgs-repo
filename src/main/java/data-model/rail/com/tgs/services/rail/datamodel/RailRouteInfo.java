package com.tgs.services.rail.datamodel;

import java.time.LocalTime;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RailRouteInfo {

	private LocalTime arrivalTime;

	private Integer dayCount;

	private LocalTime departureTime;

	private Integer distance;

	private String haltTime;

	private Integer routeNumber;

	private Integer stnSerialNumber;

	private RailStationInfo stationInfo;


}
