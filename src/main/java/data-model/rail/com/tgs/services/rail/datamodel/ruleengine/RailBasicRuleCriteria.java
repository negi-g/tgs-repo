package com.tgs.services.rail.datamodel.ruleengine;

import java.util.List;
import org.springframework.util.CollectionUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.PriceFareRange;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.rail.datamodel.RailJourneyClass;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailBasicRuleCriteria extends GeneralBasicRuleCriteria {


	@SerializedName("bp")
	private List<TimePeriod> bookingPeriod;

	@SerializedName("tp")
	private List<TimePeriod> travelPeriod;

	@SerializedName("pfr")
	private PriceFareRange priceFareRange;

	@SerializedName("jc")
	private List<RailJourneyClass> journeyClasses;


	@Override
	public void cleanData() {
		super.cleanData();
		setTravelPeriod(TgsCollectionUtils.getNonNullElements(getTravelPeriod()));
		setBookingPeriod(TgsCollectionUtils.getNonNullElements(getBookingPeriod()));
		setJourneyClasses(TgsCollectionUtils.getNonNullElements(getJourneyClasses()));

		if (priceFareRange != null && priceFareRange.isVoid()) {
			setPriceFareRange(null);
		}
		if (CollectionUtils.isEmpty(bookingPeriod)) {
			setBookingPeriod(null);
		}

		if (CollectionUtils.isEmpty(travelPeriod)) {
			setTravelPeriod(null);
		}
	}

}
