package com.tgs.services.rail.datamodel.ruleengine;

import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailAPIURLRuleCriteria implements IRuleOutPut {

	// 0
	private String pnrEnquiryURl;

	// 1
	private String searchURL;

	// 2
	private String fareAvailabilityURL;

	// 3
	private String scheduleEnquiryURL;

	// 4
	private String boardingPointEnquiryURL;

	// 5
	private String boardingPointChangeURL;

	// 6
	private String vikalpTrainEnquiryURL;

	// 7
	private String vikalpOtpURL;

	// 8
	private String cancellationUrl;

	// 9
	private String historySearchUrl;

	// 10
	private String tdrFilingUrl;

	// 11
	private String pinEnquiryUrl;

	// 12
	private String bookingDetailsUrl;

}
