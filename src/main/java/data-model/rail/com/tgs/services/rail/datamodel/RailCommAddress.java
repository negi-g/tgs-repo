package com.tgs.services.rail.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailCommAddress {

	private String address;
	private String street;
	private String colony;
	private String pincode;
	private String state;
	private String postOffice;
	private String city;
}
