package com.tgs.services.rail.datamodel.config;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;

@Getter
public class RailClientFeeOutput implements IRuleOutPut, Cleanable {

	@SerializedName("mf")
	private String managementFee;

	@SerializedName("ta")
	private Double thresholdAmount;

	@SerializedName("dmf")
	private String ManagementFeeToDisplay;
}
