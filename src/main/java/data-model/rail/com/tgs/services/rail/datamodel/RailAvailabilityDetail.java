package com.tgs.services.rail.datamodel;

import java.time.LocalDate;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailAvailabilityDetail {

	@SerializedName("jd")
	private LocalDate journeyDate;

	@SerializedName("avlstatus")
	private String availabilityStatus;

	@SerializedName("pi")
	private RailPriceInfo priceInfo;

	public RailPriceInfo getPriceInfo() {
		if (priceInfo == null) {
			priceInfo = new RailPriceInfo();
		}
		return priceInfo;

	}


}
