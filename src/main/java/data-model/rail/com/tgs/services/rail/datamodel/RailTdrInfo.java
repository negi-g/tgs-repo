package com.tgs.services.rail.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailTdrInfo {

	private String rsnIndex;
	private String reason;
	private String eftFlag;
	private String eftAmount;
	private String eftNumber;
	private String eftDate;


}
