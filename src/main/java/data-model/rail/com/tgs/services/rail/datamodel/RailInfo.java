package com.tgs.services.rail.datamodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailInfo {

	private String name;
	private String number;
	private RailType type;
	private List<RailType> types;

	@SerializedName("to")
	private String trainOwner;


}
