package com.tgs.services.rail.datamodel.config;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;

@Getter
public class RailPGChargesOutput implements IRuleOutPut, Cleanable {

	@SerializedName("pg")
	private String pgCharges;

	@SerializedName("ta")
	private Double thresholdAmount;

	@SerializedName("dpg")
	private String pgChargesToDisplay;


}
