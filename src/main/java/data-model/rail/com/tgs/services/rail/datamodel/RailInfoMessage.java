package com.tgs.services.rail.datamodel;

import lombok.Setter;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
@Setter
public class RailInfoMessage {

	@SerializedName("msg")
	private String message;

	@SerializedName("pu")
	private boolean popup;

	@SerializedName("pn")
	private String paramName;

}
