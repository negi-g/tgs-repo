package com.tgs.services.rail.datamodel;

import lombok.Getter;

@Getter
public enum ConcessionCode {

	SRCTNW, SRCTZN, NOCONC;

}
