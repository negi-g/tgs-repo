package com.tgs.services.rail.datamodel.config;

import java.util.Arrays;
import java.util.List;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.rail.datamodel.ruleengine.RailBasicRuleCriteria;
import lombok.Getter;

@Getter
public enum RailConfiguratorRuleType {

	SOURCECONFIG(RailBasicRuleCriteria.class, new TypeToken<RailSourceConfigurationOutput>() {}),
	CLIENTFEE(RailBasicRuleCriteria.class, new TypeToken<RailClientFeeOutput>() {}),
	PGCHARGES(RailBasicRuleCriteria.class, new TypeToken<RailPGChargesOutput>() {}),
	GNPURPOSE(RailBasicRuleCriteria.class, new TypeToken<RailGeneralPurposeOutput>() {}),
	ONBOARD_PLAN(RailBasicRuleCriteria.class, new TypeToken<RailOnboardPlanOutput>() {}),
	TRAIN_CONFIG(RailBasicRuleCriteria.class, new TypeToken<TrainConfigOutput>() {});


	private Class<? extends IRuleCriteria> inputType;
	private TypeToken<? extends IRuleOutPut> outPutTypeToken;

	<T extends IRuleCriteria, RT extends IRuleOutPut> RailConfiguratorRuleType(Class<T> inputType,
			TypeToken<RT> outPutTypeToken) {
		this.inputType = inputType;
		this.outPutTypeToken = outPutTypeToken;
	}

	public List<RailConfiguratorRuleType> getRailConfigRuleType() {
		return Arrays.asList(RailConfiguratorRuleType.values());
	}

	public static RailConfiguratorRuleType getRuleType(String name) {
		for (RailConfiguratorRuleType ruleType : RailConfiguratorRuleType.values()) {
			if (ruleType.getName().equals(name)) {
				return ruleType;
			}
		}
		return null;
	}

	public String getName() {
		return this.name();
	}

	public String getCode() {
		return this.name();
	}

}
