package com.tgs.services.rail.datamodel;

import java.util.List;
import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class RailMessageAttributes extends EmailAttributes {

	List<RailJourneyInfo> journeyInfos;

}
