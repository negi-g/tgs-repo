package com.tgs.services.rail.datamodel.ruleengine;

import java.time.LocalDateTime;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.datamodel.PriceFareRange;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.rail.datamodel.RailFareComponent;

public enum RaillBasicRuleField implements IRuleField {

	USERIDS {
		@Override
		public boolean isInputValidAgainstFact(RailBasicFact fact, RailBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getUserIds()) || input.getUserIds().contains(fact.getUserId());
		}
	},
	TRAVELPERIOD {
		@Override
		public boolean isInputValidAgainstFact(RailBasicFact fact, RailBasicRuleCriteria input) {
			for (TimePeriod travelPeriod : input.getTravelPeriod()) {
				if ((travelPeriod.getStartTime().isEqual(fact.getTravelPeriod().getStartTime())
						|| travelPeriod.getStartTime().isBefore(fact.getTravelPeriod().getStartTime()))
						&& (travelPeriod.getEndTime().isEqual(fact.getTravelPeriod().getEndTime())
								|| travelPeriod.getEndTime().isAfter(fact.getTravelPeriod().getEndTime()))) {
					return true;
				}
			}
			return CollectionUtils.isEmpty(input.getTravelPeriod());
		}
	},
	BOOKINGPERIOD {
		@Override
		public boolean isInputValidAgainstFact(RailBasicFact fact, RailBasicRuleCriteria input) {
			LocalDateTime now = LocalDateTime.now();
			for (TimePeriod bookingPeriod : input.getBookingPeriod()) {
				if ((bookingPeriod.getStartTime().isEqual(now) || bookingPeriod.getStartTime().isBefore(now))
						&& (bookingPeriod.getEndTime().isEqual(now) || bookingPeriod.getEndTime().isAfter(now))) {
					return true;
				}
			}
			return CollectionUtils.isEmpty(input.getBookingPeriod());
		}
	},
	PRICEFARERANGE {
		@Override
		public boolean isInputValidAgainstFact(RailBasicFact fact, RailBasicRuleCriteria input) {
			boolean isValid = false;
			if (MapUtils.isNotEmpty(fact.getTripFareComponents())) {
				PriceFareRange priceFareRange = input.getPriceFareRange();
				AtomicDouble paxFare = new AtomicDouble(0);
				if (CollectionUtils.isEmpty(priceFareRange.getFareComponents())) {
					// Default if empty fare component , then consider BF
					priceFareRange.getFareComponents().add(FareComponent.BF);
				}
				priceFareRange.getFareComponents().forEach(fc -> {
					RailFareComponent rfc = RailFareComponent.getEnumFromCode(fc.toString());
					if (rfc != null) {
						paxFare.getAndAdd(fact.getTripFareComponents().getOrDefault(rfc, 0.0));
					}
				});
				isValid = priceFareRange.isValidFare(paxFare);
			}
			return isValid;
		}
	},
	JOURNEYCLASSES {
		@Override
		public boolean isInputValidAgainstFact(RailBasicFact fact, RailBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getJourneyClasses())
					|| input.getJourneyClasses().contains(fact.getJourneyClass());
		}

	},
	ROLES {
		@Override
		public boolean isInputValidAgainstFact(RailBasicFact fact, RailBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getRoles()) || input.getRoles().contains(fact.getRole());
		}

	};

	public abstract boolean isInputValidAgainstFact(RailBasicFact fact, RailBasicRuleCriteria input);

	@Override
	public boolean isValidAgainstFact(IFact fact, IRuleCriteria ruleCriteria) {
		return this.isInputValidAgainstFact((RailBasicFact) fact, (RailBasicRuleCriteria) ruleCriteria);
	}


}
