package com.tgs.services.rail.datamodel.config;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;

public class RailOnboardPlanOutput implements IRuleOutPut, Cleanable {

	@SerializedName("otpfee")
	private Double otpRegCharges;

	@SerializedName("otpdisf")
	private Double otpDiscountedRegCharges;

	@SerializedName("dcfee")
	private Double dcRegCharges;

	@SerializedName("dcdisf")
	private Double dcDiscountedRegCharges;

	@SerializedName("otpdcfee")
	private Double otpDcRegCharges;

	@SerializedName("otpdcdisf")
	private Double otpDcDiscountedRegCharges;


}
