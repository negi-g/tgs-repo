package com.tgs.services.rail.datamodel;

import lombok.Setter;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
@Setter
public class RailTDRReason {

	@SerializedName("rid")
	private String reasonIndex;

	@SerializedName("tdrsn")
	private String tdrReason;

}
