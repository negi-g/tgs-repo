package com.tgs.services.rail.datamodel;

import lombok.Getter;

@Getter
public enum RailBookingQuota {

	GENERAL("GN"),
	TATKAL("TQ"),
	LADIES("LD"),
	POOLED_QUOTA("PQ"),
	LOWER_BERTH("SS"),
	PHYSICALLY_HANDICAPPED("HP"),
	PARLIAMENT("PH"),
	FOREIGN_TOURIST("FT"),
	YUVA("YU");

	private String code;

	RailBookingQuota(String code) {
		this.code = code;
	}

	public static RailBookingQuota getEnumFromCode(String code) {
		for (RailBookingQuota bookingQuota : RailBookingQuota.values()) {
			if (bookingQuota.getCode().equals(code)) {
				return bookingQuota;
			}
		}
		return null;
	}
}
