package com.tgs.services.rail.datamodel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.SystemError;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class RailSearchQuery extends DataModel implements Validatable {

	@NonNull
	private RailStationInfo fromCityOrStation;

	@NonNull
	private RailStationInfo toCityOrStation;

	@NonNull
	private LocalDate travelDate;

	private String journeyClass;

	private String bookingQuota = "GN";

	private Boolean moreThanOneDay;

	private Boolean isLiveSearch;

	private List<Integer> sourceIds;

	@ApiModelProperty(hidden = true)
	private Long timeout;

	@NonNull
	@ApiModelProperty(
			value = "This field is required to uniquely identify search request. It should be unique for every request",
			example = "12345")
	@APIUserExclude
	private String searchId;

	@NonNull
	@ApiModelProperty(
			value = "This field is required to uniquely identify search. It should be unique for every request",
			example = "12345")
	@APIUserExclude
	private String requestId;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {

		FieldErrorMap errors = new FieldErrorMap();

		if (fromCityOrStation == null) {
			errors.put("fromCityOrStation", SystemError.INVALID_STATION.getErrorDetail());
		}
		if (toCityOrStation == null) {
			errors.put("toCityOrStation", SystemError.INVALID_STATION.getErrorDetail());
		}
		if (travelDate == null) {
			errors.put("travelDate", SystemError.INVALID_TRAVEL_DATE.getErrorDetail());
		}

		return errors;
	}

	public void addSourceId(Integer sourceId) {
		if (sourceIds == null) {
			sourceIds = new ArrayList<>();
		}
		sourceIds.add(sourceId);
	}


}
