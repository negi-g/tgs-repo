package com.tgs.services.rail.datamodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.GstInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailBookingRelatedInfo {

	@SerializedName("ti")
	private List<RailTravellerInfo> travellerInfos;

	@SerializedName("ctid")
	private String clientTransactionId;

	@SerializedName("tktadd")
	private RailCommAddress ticketAddress;

	@SerializedName("mn")
	private String mobileNumber;

	@SerializedName("quota")
	private String quota;

	@SerializedName("jc")
	private RailJourneyClass journeyClass;

	@SerializedName("lid")
	private String userLoginId;

	@SerializedName("rurl")
	private String returnUrl;

	@SerializedName("rsid")
	private String reservationId;

	@SerializedName("canid")
	private String cancellationId;

	@SerializedName("otp")
	public Boolean isThroughOTP;

	@SerializedName("ito")
	public Boolean isTravelInsuranceOpted;


	public String pnr;
	
	@SerializedName("gsti")
	private GstInfo gstInfo;
}
