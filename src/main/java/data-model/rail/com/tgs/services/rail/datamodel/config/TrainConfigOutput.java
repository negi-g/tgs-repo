package com.tgs.services.rail.datamodel.config;

import java.util.HashMap;
import java.util.Map;

import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;

@Getter
public class TrainConfigOutput implements IRuleOutPut, Cleanable {

	private Map<String, String> trainNumMap = new HashMap<String, String>();

}
