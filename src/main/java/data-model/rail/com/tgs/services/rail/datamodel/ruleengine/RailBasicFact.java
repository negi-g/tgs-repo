package com.tgs.services.rail.datamodel.ruleengine;

import java.util.Map;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailFareEnquiry;
import com.tgs.services.rail.datamodel.RailJourneyClass;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class RailBasicFact extends GeneralBasicFact implements IFact {

	private TimePeriod travelPeriod;

	private TimePeriod bookingPeriod;

	private String userId;

	private Map<RailFareComponent, Double> tripFareComponents;

	private RailJourneyClass journeyClass;

	public static RailBasicFact createFact() {
		return RailBasicFact.builder().build();
	}

	public RailBasicFact generateFact(RailFareEnquiry fareEnquiryQuery) {
		setJourneyClass(RailJourneyClass.getEnumFromCode(fareEnquiryQuery.getJourneyClass()));
		return this;
	}

	public RailBasicFact generateFact(RailJourneyInfo jounryInfo) {
		setJourneyClass(RailJourneyClass.getEnumFromCode(jounryInfo.getAvlJourneyClasses().get(0)));
		return this;
	}


}
