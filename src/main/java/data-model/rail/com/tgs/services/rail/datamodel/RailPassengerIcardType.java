package com.tgs.services.rail.datamodel;

import lombok.Getter;

@Getter
public enum RailPassengerIcardType {

	NULL_IDCARD,
	DRIVING_LICENSE,
	PASSPORT,
	PANCARD,
	GOVT_ICARD,
	STUDENT_ICARD,
	BANK_PASSBOOK,
	CREDIT_CARD,
	VOTER_ICARD,
	UNIQUE_ICARD;


}
