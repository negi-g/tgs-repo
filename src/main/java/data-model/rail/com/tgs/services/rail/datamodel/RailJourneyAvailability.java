package com.tgs.services.rail.datamodel;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailJourneyAvailability {

	@SerializedName("ravail")
	private List<RailAvailabilityDetail> railAvailabilityDetails;


	public List<RailAvailabilityDetail> getRailAvailabilityDetails() {
		if (railAvailabilityDetails == null) {
			railAvailabilityDetails = new ArrayList<RailAvailabilityDetail>();
		}
		return railAvailabilityDetails;

	}


}
