package com.tgs.services.rail.datamodel;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.helper.RestExclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailStationInfo extends DataModel {


	@RestExclude
	@SearchPredicate(type = PredicateType.EQUAL)
	private Long id;

	@SearchPredicate(type = PredicateType.EQUAL)
	@SearchPredicate(type = PredicateType.IN)
	private String code;

	private String name;

	@RestExclude
	private Boolean enabled;


}
