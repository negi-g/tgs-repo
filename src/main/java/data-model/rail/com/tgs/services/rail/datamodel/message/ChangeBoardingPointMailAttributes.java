package com.tgs.services.rail.datamodel.message;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class ChangeBoardingPointMailAttributes extends RailEmailAttributes {

	private String type;

	private String oldBoardingStation;

	private String newBoardingStation;

	private String newBoardingTime;

}
