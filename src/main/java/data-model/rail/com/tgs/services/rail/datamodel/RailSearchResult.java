package com.tgs.services.rail.datamodel;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailSearchResult {

	private List<RailJourneyInfo> journeys;

	private List<String> avlQuotaList;


}
