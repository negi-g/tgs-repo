package com.tgs.services.rail.datamodel;

import java.util.HashMap;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailPriceInfo {

	@SerializedName("tf")
	private Double totalFare;

	@SerializedName("fc")
	private Map<RailFareComponent, Double> fareComponents;

	@SerializedName("rci")
	private RailCommercialInfo railCommercialInfo;

	public Map<RailFareComponent, Double> getFareComponents() {
		if (fareComponents == null) {
			fareComponents = new HashMap<>();
		}
		return this.fareComponents;
	}

	public RailCommercialInfo getRailCommercialInfo() {
		if (railCommercialInfo == null) {
			railCommercialInfo = new RailCommercialInfo();
		}
		return railCommercialInfo;
	}
}
