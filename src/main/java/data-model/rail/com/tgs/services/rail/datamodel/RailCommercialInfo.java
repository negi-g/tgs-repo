package com.tgs.services.rail.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailCommercialInfo {

	@SerializedName("pgid")
	private Long pgId;

	@SerializedName("cfid")
	private Long clientFeeId;


}
