package com.tgs.services.rail.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailJourneyInfo {

	private String id;

	private Integer sNo;

	@SerializedName("ri")
	private RailInfo railInfo;

	@SerializedName("ds")
	private RailStationInfo departStationInfo;

	@SerializedName("as")
	private RailStationInfo arrivalStaionInfo;

	@SerializedName("bs")
	private RailStationInfo boardingStationInfo;

	@SerializedName("rus")
	private RailStationInfo reservationUptoStationInfo;

	@SerializedName("dt")
	private LocalDateTime departure;

	@SerializedName("bt")
	private LocalDateTime boardingTime;

	@SerializedName("quota")
	private String bookingQuota;

	@SerializedName("at")
	private LocalDateTime arrival;

	@SerializedName("dist")
	private Integer distance;

	private Integer days;

	private String duration;

	@SerializedName("avlclass")
	private List<String> avlJourneyClasses;

	@SerializedName("avlclassavl")
	private Map<String, RailJourneyAvailability> avlJourneyDetailsMap;

	@SerializedName("bi")
	private RailBookingRelatedInfo bookingRelatedInfo;

	@SerializedName("rundays")
	private RailRunningDays runningDays;

	private Boolean isRunAllDays;

	@SerializedName("bkgconfig")
	private RailBookingConfiguration bookingConfiguration;

	@SerializedName("afv")
	private Boolean avlblForVikalp;

	@SerializedName("msgl")
	private List<RailInfoMessage> infoMessages;

	public Boolean getIsRunOnAllDays() {
		if (runningDays != null) {
			return (runningDays.isRunningSun() && runningDays.isRunningMon() && runningDays.isRunningTue()
					&& runningDays.isRunningWed() && runningDays.isRunningThu() && runningDays.isRunningFri()
					&& runningDays.isRunningSat());
		}
		return false;
	}


}
