package com.tgs.services.rail.datamodel.config;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailSourceConfigurationOutput implements IRuleOutPut, Cleanable {

	@SerializedName("rurls")
	private List<String> railUrls;

	@SerializedName("proxy")
	private String proxyAddress;

	private String url;

	private String userName;

	private String password;

	private boolean isTestCredential;

	private String accountCode;

}
