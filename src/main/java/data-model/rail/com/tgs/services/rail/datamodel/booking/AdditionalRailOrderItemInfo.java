package com.tgs.services.rail.datamodel.booking;

import java.time.LocalDateTime;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.rail.datamodel.RailBookingConfiguration;
import com.tgs.services.rail.datamodel.RailCommAddress;
import com.tgs.services.rail.datamodel.RailInfo;
import com.tgs.services.rail.datamodel.RailJourneyClass;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AdditionalRailOrderItemInfo {

	@SerializedName("ri")
	private RailInfo railInfo;

	@SerializedName("di")
	private Integer distance;

	private Integer days;

	@SerializedName("pi")
	private RailPriceInfo priceInfo;

	@SerializedName("du")
	private Long duration;

	@SerializedName("bc")
	private RailBookingConfiguration bookingConfig;

	@SerializedName("tktadd")
	private RailCommAddress ticketAddress;

	@SerializedName("quota")
	private String quota;

	@SerializedName("jc")
	private RailJourneyClass journeyClass;

	@SerializedName("bs")
	private String boardingStation;

	@SerializedName("bt")
	private LocalDateTime boardingTime;

	@SerializedName("rus")
	private String reservationUptoStation;

	@SerializedName("rsid")
	private String reservationId;

	@SerializedName("obs")
	private String oldboardingStation;

	@SerializedName("odt")
	private LocalDateTime oldboardingDateTime;

	@SerializedName("av")
	private Boolean avblForVikalp;

	@SerializedName("canid")
	private String cancellationId;

	@SerializedName("vos")
	private Boolean vikalpOptStatus;

	@SerializedName("paxmn")
	private String passengerMobileNumber;

	@SerializedName("tn")
	private String optedTrainNumbers;

	@SerializedName("otp")
	private Boolean isThroughOTP;

	@SerializedName("ito")
	private Boolean isTravelInsuranceOpted;

	@SerializedName("retry")
	private Integer retryCount;

	private String pnr;
}
