package com.tgs.services.rail.datamodel.message;

import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class RailEmailAttributes extends EmailAttributes {

	private String bookingId;

	private String pnr;

	private String trainName;

	private String trainNumber;

	private String sector;

	private String boardingStation;

	private String paxName;

	private String departureTime;

	private String totalFare;


}
