package com.tgs.services.rail.datamodel.message;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class VikalpMailAttributes extends RailEmailAttributes {

	private String oldDeparture;

	private String optedTrains;

}
