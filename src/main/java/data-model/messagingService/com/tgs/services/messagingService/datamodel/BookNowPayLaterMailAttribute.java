package com.tgs.services.messagingService.datamodel;

import java.util.List;
import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import com.tgs.services.base.datamodel.HotelBnplMailData;

@Getter
@Setter
@SuperBuilder
public class BookNowPayLaterMailAttribute  extends EmailAttributes {
	
	private List<HotelBnplMailData> updatedStatus;
}
