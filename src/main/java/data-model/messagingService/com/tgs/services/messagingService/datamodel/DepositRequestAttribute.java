package com.tgs.services.messagingService.datamodel;

import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class DepositRequestAttribute extends EmailAttributes {

	private String agentId;
	
	private String agentName;
	
	private String depositType;
	
	private String transactionId;
	
	private String depositAmount;
	
	private String generationTime;
	
	private String bankName;
	
	private String drId;
	
	private String reason;
	
}
