package com.tgs.services.messagingService.datamodel;

import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class NoteMailAttribute extends EmailAttributes {
	
	private String bookingId;
	
	private String amendmentId;
	
	private String noteType;

}
