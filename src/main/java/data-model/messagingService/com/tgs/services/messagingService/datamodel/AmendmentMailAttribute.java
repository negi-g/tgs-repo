package com.tgs.services.messagingService.datamodel;

import com.tgs.services.base.datamodel.EmailAttributes;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AmendmentMailAttribute extends EmailAttributes {

	private String bookingId;

	private String amdId;

	private String sector;

	private String passengerName;

	private String amendmentType;

	private String amendmentComment;

	private String agentId;

	private String agentName;

	private String generationTime;

	private String bookingStatus;

	private String amendmentStatus;

	private String link;

	private String reason;
}
