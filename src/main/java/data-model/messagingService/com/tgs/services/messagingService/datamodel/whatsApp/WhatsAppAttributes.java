package com.tgs.services.messagingService.datamodel.whatsApp;

import java.util.List;

import com.tgs.services.base.datamodel.AttachmentMetadata;
import com.tgs.services.base.enums.MessageMedium;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class WhatsAppAttributes {
	private List<String> recipientNumbers;

	/** It is unique key to identify which template to pick */
	private String key;

	/** Module/key specific attributes */
	private List<String> attributes;
	private AttachmentMetadata attachmentData;

	@Builder.Default
	private MessageMedium medium = MessageMedium.WHATSAPP;

}
