package com.tgs.services.ffts.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ActionResult {
	private boolean actionPerformed;
	private boolean notified;
	private Double fareDrop;
}
