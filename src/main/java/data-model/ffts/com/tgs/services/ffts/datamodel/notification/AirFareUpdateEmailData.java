package com.tgs.services.ffts.datamodel.notification;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class AirFareUpdateEmailData {

	private String toEmailId;

	private String bccEmailId;

	private String fromEmailId;
}
