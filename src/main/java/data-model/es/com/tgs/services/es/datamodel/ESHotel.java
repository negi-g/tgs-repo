package com.tgs.services.es.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ESHotel {

	private String name;
	private String cityName;
	private String countryName;
	private String rating;
	private String id;
	private String address;
	
}
