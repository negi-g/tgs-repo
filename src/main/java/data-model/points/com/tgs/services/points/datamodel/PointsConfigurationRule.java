package com.tgs.services.points.datamodel;

import java.time.LocalDateTime;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString(onlyExplicitlyIncluded = true)
public class PointsConfigurationRule extends DataModel implements IRule {

	@ApiModelProperty(
			notes = "Already created Unique ID for Points configuration Rule,if need to update id is mandatory",
			example = "10")
	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	@ToString.Include
	private Integer id;

	@SearchPredicate(type = PredicateType.IN)
	private Product product;

	@ApiModelProperty(notes = "Type of points", example = "Loyalty Points")
	@SearchPredicate(type = PredicateType.IN)
	private PointsType type;

	@ApiModelProperty(notes = "Type of operation- CREDIT/ DEBIT", example = "CREDIT")
	@SearchPredicate(type = PredicateType.IN)
	private PaymentOpType operationType;

	@ApiModelProperty(notes = "denote the inclusion criteria of rule", example = "")
	private PointBasicRuleCriteria inclusionCriteria;

	@ApiModelProperty(notes = "denote the exclusion criteria of rule", example = "")
	private PointBasicRuleCriteria exclusionCriteria;

	@ApiModelProperty(notes = "denote the output criteria of rule", example = "")
	private PointsOutputCriteria outputCriteria;

	@ApiModelProperty(notes = "To denote rule is enabled or disabled", example = "TRUE")
	private boolean enabled;

	@ApiModelProperty(notes = "To denote rule is deleted", example = "TRUE")
	@SearchPredicate(type = PredicateType.EQUAL)
	private boolean isDeleted;

	@ApiModelProperty(notes = "To denote the priority the rule", example = "1")
	@SearchPredicate(type = PredicateType.EQUAL)
	private Double priority;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	@Override
	public GeneralBasicRuleCriteria getInclusionCriteria() {
		if (Product.AIR.equals(product))
			return inclusionCriteria.getFlightBasicRuleCriteria();
		else if (Product.HOTEL.equals(product))
			return inclusionCriteria.getHotelBasicRuleCriteria();
		else
			return inclusionCriteria.getGeneralBasicRuleCriteria();
	}

	@Override
	public GeneralBasicRuleCriteria getExclusionCriteria() {
		if (Product.AIR.equals(product))
			return exclusionCriteria.getFlightBasicRuleCriteria();
		else if (Product.HOTEL.equals(product))
			return exclusionCriteria.getHotelBasicRuleCriteria();
		else
			return exclusionCriteria.getGeneralBasicRuleCriteria();
	}

	@Override
	public PointsOutputCriteria getOutput() {
		return outputCriteria;
	}

	@Override
	public double getPriority() {
		return priority != null ? priority.doubleValue() : 0.0;
	}

}
