package com.tgs.services.points.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.PointsConfiguration;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.pms.datamodel.PaymentOpType;
import lombok.*;
import java.util.List;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = true)
public class ReedemPointsData {

	@ToString.Include
	@NonNull
	private String bookingId;

	@ToString.Include
	private String userId;

	@NonNull
	@ToString.Include
	private PointsType type;

	private Product product;

	@ToString.Include
	private PaymentOpType opType;

	@NonNull
	@SerializedName("pts")
	@ToString.Include
	private Double points;

	public List<PointsConfigurationRule> pointRules;

	public PointsConfiguration pointsConfiguration;

	public ReedemPointsData() {}

	public ReedemPointsData(String bookingId, PointsType type, Product product, PaymentOpType opType, Double points,
			List<PointsConfigurationRule> rules, PointsConfiguration configuration) {
		this.setBookingId(bookingId);
		this.setType(type);
		this.setProduct(product);
		this.setOpType(opType);
		this.setPoints(points);
		this.setPointRules(rules);
		this.setPointsConfiguration(configuration);
	}
}
