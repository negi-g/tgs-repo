package com.tgs.services.points.datamodel;

import com.tgs.services.base.ruleengine.BasicRuleCriteria;
import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import lombok.Getter;

@Getter
public class PointBasicRuleCriteria extends BasicRuleCriteria {

	private FlightBasicRuleCriteria flightBasicRuleCriteria;

	private HotelBasicRuleCriteria hotelBasicRuleCriteria;

	public GeneralBasicRuleCriteria getGeneralBasicRuleCriteria() {
		GeneralBasicRuleCriteria generalCriteria;
		if (flightBasicRuleCriteria != null)
			generalCriteria = (GeneralBasicRuleCriteria) flightBasicRuleCriteria;
		else
			generalCriteria = (GeneralBasicRuleCriteria) hotelBasicRuleCriteria;
		return generalCriteria;
	}

}
