package com.tgs.services.gms.datamodel;

import lombok.Getter;

@Getter
public enum ScheduleJobType {

	ONETIME, DAILY, WEEKLY
}
