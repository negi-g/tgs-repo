package com.tgs.services.gms.datamodel;

import lombok.Getter;

@Getter
public enum ScheduleJobStatus {

	PENDING, CANCELLED, EXECUTED, FAILED
}
