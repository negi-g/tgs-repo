package com.tgs.services.gms.datamodel.configurator;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@ApiModel(value = "This is used to fetch config rules based on search criteria. You can pass either any one field or combination of fields depending upon your search criteria")
@SuperBuilder
public class ConfiguratorFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch based on ruleType ", example = "GENERAL")
	private String ruleType;

	@ApiModelProperty(notes = "To fetch based on deleted config ", example = "true")
	private boolean isDeleted;

	@ApiModelProperty(notes = "To fetch based on exit on match", example = "false")
	private Boolean exitOnMatch;
}
