
package com.tgs.services.gms.datamodel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.ClientDefaultConfiguration;
import com.tgs.services.base.datamodel.DBSConfiguration;
import com.tgs.services.base.datamodel.DateFormatInfo;
import com.tgs.services.base.datamodel.ExternalPaymentNotificationConfiguration;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.PasswordAttemptConfiguration;
import com.tgs.services.base.datamodel.PointsConfiguration;
import com.tgs.services.base.datamodel.ProtectGroupConfiguration;
import com.tgs.services.base.datamodel.SecurityConfiguration;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientGeneralInfo implements IRuleOutPut {

	private GstInfo gstInfo;
	private String logoUrl;
	private String homePhone;
	private String workPhone;
	private String companyName;
	private Integer areaCode;
	private String title;
	private String name;
	private String address1;
	private String address2;

	private String city;
	private String countryCode;
	private String postalCode;
	private String nationality;
	private String email;
	private String fax;
	private String country;
	private LocalDate dob;
	private String state;
	private String stateCode;
	private Boolean isGstAllowed;

	@SerializedName("vms")
	private String verifyMappingSupplierId;

	@SerializedName("uca")
	private Boolean useClientAddress;

	@SerializedName("uagl")
	private Integer userAssignedGroupLimit;

	@SerializedName("gaul")
	private Integer groupAssignedUserLimit;

	@SerializedName("egfl")
	private Map<String, Integer> excludedGroupFromLimit;

	@SerializedName("iae")
	private Boolean isAccountingAnalyticsEnabled;

	@SerializedName("ucdi")
	private Boolean useClientDeliveryInfo;

	@SerializedName("iora")
	private Boolean isOnRequestAllowed;

	@SerializedName("orh")
	private Integer onRequestHours;

	@SerializedName("stis")
	private Integer searchTimeoutInSecond;

	@SerializedName("sris")
	private Integer searchRetryInSecond;

	private String currencyCode;

	private Map<DateFormatType, DateFormatInfo> dateFormats;

	@SerializedName("aeids")
	private List<String> altEmailIds;

	@SerializedName("asi")
	private List<String> accountingSupplierIds;

	@SerializedName("occid")
	private List<String> onboardingCcEmailds;

	@SerializedName("cau")
	private String cancellationAutomatedUserId;

	@SerializedName("rb")
	private String registrationBean;

	private DBSConfiguration dbsConfiguration;

	@SerializedName("epnc")
	private ExternalPaymentNotificationConfiguration externalPaymentNotificationConfiguration;

	@SerializedName("dc")
	private ClientDefaultConfiguration defaultConfiguration;

	@SerializedName("pgc")
	private ProtectGroupConfiguration protectGroupConfiguration;

	@Deprecated
	private String bookingIdPrefix;

	@SerializedName("dis")
	private Boolean decreaseInventorySeatsOnAmendment;

	@SerializedName("itr")
	private Boolean isTicketingRequiredForInventory;

	private String amdIdPrefix;

	@SerializedName("ric")
	private ReferenceIdConfiguration refIdConfiguration;

	@SerializedName("iic")
	private InvoiceIdConfiguration invoiceIdConfiguration;

	@SerializedName("pdr")
	private List<String> paymentDisabledRoles;

	@SerializedName("pder")
	private List<String> paymentDisabledEmulatedRoles;

	@SerializedName("sc")
	private SecurityConfiguration securityConfiguration;

	@SerializedName("pac")
	private PasswordAttemptConfiguration passwordAttemptConfiguration;

	@SerializedName("pc")
	private Map<PointsType, PointsConfiguration> pointsConfigurations;

	@SerializedName("aac")
	private Integer amendmentAssignCap;

	@SerializedName("sfc")
	private SecurityFilterConfiguration securityFilterConfiguration;

	@SerializedName("iuids")
	private List<String> internalUserIds;

	@SerializedName("aroles")
	private List<UserRole> allowedRegistrationWithoutAuth;

	@SerializedName("pg")
	private String partnerGroup;

	@SerializedName("naaa")
	private List<String> nAAmendmentActions;

	@SerializedName("uda")
	private Boolean userDuesApplicable;

	@SerializedName("ccca")
	private List<String> clientDepentCountriesAir;

	@SerializedName("tr")
	private ClientTaxRateInfo taxRateInfo;

	@SerializedName("vn")
	private String vatNumber;

	@SerializedName("crn")
	private String crNumber;

	@SerializedName("pavi")
	private PartnerApiVersionInfo partnerApiVersionInfo;

	@SerializedName("hukb")
	private String hotelStaticDataBean;

	@SerializedName("rme")
	private Boolean isRoomMappingEnabled;

	@SerializedName("hrv")
	private Boolean isHotelReviewValidationEnabled;

	@SerializedName("alcr")
	private List<UserRole> allowedLinkedUserCreationRoles;
	@SerializedName("atadrm")
	private Boolean allowToTriggerAbortDRMail;

	@SerializedName("pisn")
	private String polygonInfoSupplierName;

	@SerializedName("ima")
	private Boolean isMaskingAllowed;

	@SerializedName("iorle")
	private Boolean isOrderRetryLogicEnabled;

	@SerializedName("ruabs")
	private List<String> reportUsersAllowedforBiggerScan;

	@SerializedName("ei")
	private Map<String, Object> extraInfo;

	public ClientDefaultConfiguration getDefaultConfiguration() {
		if (defaultConfiguration == null) {
			defaultConfiguration = new ClientDefaultConfiguration();
		}
		return defaultConfiguration;
	}

	@Deprecated
	public String getBookingIdPrefix() {
		return StringUtils.isBlank(bookingIdPrefix) ? "" : bookingIdPrefix;
	}

	public ProtectGroupConfiguration getProtectGroupConfiguration() {
		return protectGroupConfiguration != null ? protectGroupConfiguration : new ProtectGroupConfiguration();
	}

	public SecurityConfiguration getSecurityConfiguration() {
		return securityConfiguration != null ? securityConfiguration : new SecurityConfiguration();
	}

	public SecurityFilterConfiguration getSecurityFilterConfiguration() {
		return securityFilterConfiguration != null ? securityFilterConfiguration : new SecurityFilterConfiguration();
	}

	public PasswordAttemptConfiguration getPasswordAttemptConfiguration() {
		if (passwordAttemptConfiguration == null) {
			passwordAttemptConfiguration = new PasswordAttemptConfiguration();
		}
		return passwordAttemptConfiguration;
	}

	public List<UserRole> getAllowedRegistrationWithoutAuth() {
		if (allowedRegistrationWithoutAuth == null) {
			allowedRegistrationWithoutAuth = new ArrayList<>();
			// Agent is allowed to register without token by default. This setting can be overridden from config.
			allowedRegistrationWithoutAuth.add(UserRole.AGENT);
		}
		return allowedRegistrationWithoutAuth;
	}

	public List<UserRole> getAllowedLinkedUserCreationRoles() {
		if (allowedLinkedUserCreationRoles == null) {
			allowedLinkedUserCreationRoles = new ArrayList<>();
			allowedLinkedUserCreationRoles.add(UserRole.AGENT);
		}
		return allowedLinkedUserCreationRoles;
	}

	public Map<PointsType, PointsConfiguration> getPointsConfigurations() {
		if (pointsConfigurations == null)
			pointsConfigurations = new HashMap<>();
		return pointsConfigurations;
	}

	public Map<FareComponent, Double> getTaxRates() {
		if (taxRateInfo != null && MapUtils.isNotEmpty(taxRateInfo.getTaxRates())) {
			return taxRateInfo.getTaxRates();
		}
		return null;
	}

	public String getNationality() {
		if (StringUtils.isBlank(nationality)) {
			nationality = "IN";
		}
		return nationality;
	}

	public PartnerApiVersionInfo getPartnerApiVersionInfo() {
		if (partnerApiVersionInfo == null) {
			partnerApiVersionInfo = PartnerApiVersionInfo.builder().build();
		}
		return partnerApiVersionInfo;
	}

	public Integer getUserAssignedGroupLimit() {
		return userAssignedGroupLimit != null ? userAssignedGroupLimit : 20;
	}

	public Integer getGroupAssignedUserLimit() {
		return groupAssignedUserLimit != null ? groupAssignedUserLimit : 500;
	}

	public Map<String, Integer> getExcludedGroupFromLimit() {
		return excludedGroupFromLimit != null ? excludedGroupFromLimit : new HashMap<>();
	}

	public boolean getIsMaskingAllowed() {
		return BooleanUtils.isTrue(isMaskingAllowed);
	}

	public List<String> getReportUsersAllowedforBiggerScan() {
		return CollectionUtils.isEmpty(reportUsersAllowedforBiggerScan) ? new ArrayList<>()
				: reportUsersAllowedforBiggerScan;
	}
}
