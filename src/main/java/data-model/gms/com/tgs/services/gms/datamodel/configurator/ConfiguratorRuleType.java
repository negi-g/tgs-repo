package com.tgs.services.gms.datamodel.configurator;

import java.util.Arrays;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.configurationmodel.EmailVendorConfiguration;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.configurationmodel.LogServiceOutput;
import com.tgs.services.base.configurationmodel.MoneyExchangeConfiguration;
import com.tgs.services.base.configurationmodel.SmsVendorConfiguration;
import com.tgs.services.base.configurationmodel.WhatsAppConfiguration;
import com.tgs.services.base.datamodel.APIPartnerMessageConfig;
import com.tgs.services.base.datamodel.BeanOutput;
import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.gms.datamodel.ClientBankConfiguration;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.ruleengine.AbuseControlOutput;
import com.tgs.services.gms.ruleengine.AccessControlOutput;
import com.tgs.services.gms.ruleengine.GeneralPurposeOutput;

import lombok.Getter;

@Getter
public enum ConfiguratorRuleType {

	GENERAL(GeneralBasicRuleCriteria.class, new TypeToken<GeneralPurposeOutput>() {}),
	CLIENTBANK(GeneralBasicRuleCriteria.class, new TypeToken<ClientBankConfiguration>() {}),
	LOGINFO(GeneralBasicRuleCriteria.class, new TypeToken<LogServiceOutput>() {}),
	FAREBREAKUP(GeneralBasicRuleCriteria.class, new TypeToken<FareBreakUpConfigOutput>() {}),
	CLIENTINFO(GeneralBasicRuleCriteria.class, new TypeToken<ClientGeneralInfo>() {}),
	EMAILCONFIG(GeneralBasicRuleCriteria.class, new TypeToken<EmailVendorConfiguration>() {}),
	ACCESSCONTROL(GeneralBasicRuleCriteria.class, new TypeToken<AccessControlOutput>() {}),
	SMSCONFIG(GeneralBasicRuleCriteria.class, new TypeToken<SmsVendorConfiguration>() {}),
	WHATSAPPCONFIG(GeneralBasicRuleCriteria.class, new TypeToken<WhatsAppConfiguration>() {}),
	BEANINFO(GeneralBasicRuleCriteria.class, new TypeToken<BeanOutput>() {}),
	ABUSECONTROL(GeneralBasicRuleCriteria.class, new TypeToken<AbuseControlOutput>() {}), 
	MESSAGECONTROL(GeneralBasicRuleCriteria.class, new TypeToken<APIPartnerMessageConfig>() {
	}),
	MONEYEXCHANGE(GeneralBasicRuleCriteria.class, new TypeToken<MoneyExchangeConfiguration>() {});

	private Class<? extends IRuleCriteria> inputType;
	private TypeToken<? extends IRuleOutPut> outPutTypeToken;

	private <T extends IRuleCriteria, RT extends IRuleOutPut> ConfiguratorRuleType(Class<T> inputType,
			TypeToken<RT> outPutTypeToken) {
		this.inputType = inputType;
		this.outPutTypeToken = outPutTypeToken;
	}

	public List<ConfiguratorRuleType> getConfigRuleType() {
		return Arrays.asList(ConfiguratorRuleType.values());
	}

	public static ConfiguratorRuleType getRuleType(String name) {
		for (ConfiguratorRuleType ruleType : ConfiguratorRuleType.values()) {
			if (ruleType.getName().equals(name)) {
				return ruleType;
			}
		}
		return null;
	}

	public String getName() {
		return this.name();
	}

}
