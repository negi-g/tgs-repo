package com.tgs.services.gms.datamodel;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
public class HolidayConfig {

    private Set<LocalDate> holidays;

    private WeekendOffConfig saturday;
}
