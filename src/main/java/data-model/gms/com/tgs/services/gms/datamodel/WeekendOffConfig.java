package com.tgs.services.gms.datamodel;

import lombok.Getter;

@Getter
public enum WeekendOffConfig {

    ALL,
    SECOND_OFF,
    ALTERNATE,  // 2nd and 4th Off
    NONE
}
