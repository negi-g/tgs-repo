package com.tgs.services.gms.datamodel.abusecontrol;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import com.google.gson.annotations.SerializedName;

public class ApplicationRateLimit {

	@SerializedName("apl")
	private Map<TimeUnit,RateLimit> allowedApiLimit;
	
	public Map<TimeUnit,RateLimit> getAllowedApiLimit() {
		if(allowedApiLimit == null)
			allowedApiLimit = new HashMap<>();
		return allowedApiLimit;
	}

}
