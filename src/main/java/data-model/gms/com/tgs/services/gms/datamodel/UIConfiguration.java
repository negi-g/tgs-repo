package com.tgs.services.gms.datamodel;

import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UIConfiguration {
	private Map<String, String> data;
}
