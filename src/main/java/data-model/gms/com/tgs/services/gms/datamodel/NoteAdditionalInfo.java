package com.tgs.services.gms.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NoteAdditionalInfo {
	
	@SearchPredicate(type = PredicateType.EQUAL, dbAttribute = "va")
	@SearchPredicate(type = PredicateType.EQUAL, destinationEntity = "Note", dbAttribute = "additionalInfo.va")
	@SerializedName("va")
	Boolean visibleToAgent;

}
