package com.tgs.services.gms.datamodel;

import java.time.LocalDateTime;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ScheduleJob {

	@SearchPredicate(type = PredicateType.EQUAL)
	public Long id;

	@SearchPredicate(type = PredicateType.EQUAL)
	private String name;

	@JsonAdapter(JsonStringSerializer.class)
	private String data;

	private LocalDateTime createdon;

	private ScheduleJobStatus status;

	private LocalDateTime processedOn;

	private ScheduleJobType type;

	@UserId
	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	private String userId;

	private String reportlink;

	private String comments;
}
