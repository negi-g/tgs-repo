package com.tgs.services.gms.datamodel;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.helper.UserId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Note {

	@SearchPredicate(type = PredicateType.EQUAL)
	private Long id;

	private LocalDateTime createdOn;

	@UserId
	@SearchPredicate(type = PredicateType.EQUAL)
	private String userId;

	private String userName;

	@SearchPredicate(type = PredicateType.EQUAL)
	private String bookingId;

	@SearchPredicate(type = PredicateType.EQUAL)
	private String amendmentId;

	@SearchPredicate(type = PredicateType.EQUAL)
	private NoteType noteType;

	@SearchPredicate(type = PredicateType.OBJECT)
	private NoteAdditionalInfo additionalInfo;

	@NotNull
	private String noteMessage;

	@ApiModelProperty(hidden = true)
	public static String getNoteMessage(String userId, String... message) {
		if (StringUtils.isBlank(userId)) {
			return StringUtils.join(message, " :SYSTEM GENERATED");
		}
		return StringUtils.join(message, " : ", userId);
	}

	public NoteAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) {
			additionalInfo = NoteAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}

}
