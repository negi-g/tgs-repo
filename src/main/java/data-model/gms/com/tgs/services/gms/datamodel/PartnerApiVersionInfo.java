package com.tgs.services.gms.datamodel;


import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PartnerApiVersionInfo {

	@SerializedName("av")
	private Integer airVersion;

	@SerializedName("hv")
	private Integer hotelVersion;


	public Integer getAirVersion() {
		return airVersion != null ? airVersion : 1;
	}

	public Integer getHotelVersion() {
		return hotelVersion != null ? hotelVersion : 1;
	}


}
