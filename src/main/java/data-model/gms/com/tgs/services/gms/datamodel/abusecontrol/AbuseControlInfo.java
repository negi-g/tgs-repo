package com.tgs.services.gms.datamodel.abusecontrol;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AbuseControlInfo {

	@SerializedName("blcs")
	private List<String> blackListedCombinations;

	@SerializedName("wlcs")
	private List<String> whiteListedCombinations;
	
	@SerializedName("aal")
	private List<ApiRateLimit> apiAccessLimit;
	
	public List<ApiRateLimit> getApiAccessLimit() {
		if(apiAccessLimit == null)
			apiAccessLimit = new ArrayList<>();
		return apiAccessLimit;
	}
	
	public List<String> getBlackListedCombinations() {
		if(blackListedCombinations == null)
			blackListedCombinations = new ArrayList<>();
		return blackListedCombinations;
	}

	public List<String> getWhiteListedCombinations() {
		if(whiteListedCombinations == null)
			whiteListedCombinations = new ArrayList<>();
		return whiteListedCombinations;
	}
}
