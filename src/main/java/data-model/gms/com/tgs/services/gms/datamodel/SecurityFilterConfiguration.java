package com.tgs.services.gms.datamodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SecurityFilterConfiguration {
	
	@SerializedName("esbf")
	private Boolean isSecurityFilterEnabled;
	
	@SerializedName("sdu")
	private List<String> securityDisabledUrls;
	
	@SerializedName("sk")
	private String securityKeywords;
}
