package com.tgs.services.gms.datamodel;

import java.util.Calendar;
import java.util.List;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class CollectionServiceFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch documents according to type")
	private String type;
	@ApiModelProperty(notes = "To fetch documents according to key")
	private String key;
	
	private List<String> keys;
	
	private List<String> types;

	@ApiModelProperty(notes = "To fetch according to expiry")
	private Calendar expiry;

	private Boolean isConsiderHierarchy;
}
