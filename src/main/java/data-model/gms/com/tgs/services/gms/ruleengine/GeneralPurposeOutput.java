package com.tgs.services.gms.ruleengine;

import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeneralPurposeOutput implements IRuleOutPut {

    private String value;
}
