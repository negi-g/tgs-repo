package com.tgs.services.gms.datamodel;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ApiModel(value = "This is being used to filter cities, You can pass either any one field or combination of fields depending upon your search criteria")
@SuperBuilder
public class CityInfoFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch information about any particular city ", example = "Kolhapur")
	private String name;
	@ApiModelProperty(notes = "To fetch all cities of a state ", example = "Maharashtra")
	private String state;
	@ApiModelProperty(notes = "To fetch all cities of a country ", example = "India")
	private String country;

	/*
	 * private Integer cityId; private Integer stateId; private Integer countryId;
	 */
}
