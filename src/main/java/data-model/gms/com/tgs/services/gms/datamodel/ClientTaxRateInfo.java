package com.tgs.services.gms.datamodel;


import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.FareComponent;
import lombok.Getter;
import java.util.Map;

@Getter
public class ClientTaxRateInfo {

	@SerializedName("trs")
	private Map<FareComponent, Double> taxRates;
}
