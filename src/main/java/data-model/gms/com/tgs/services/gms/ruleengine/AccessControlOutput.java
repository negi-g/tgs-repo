package com.tgs.services.gms.ruleengine;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.gms.datamodel.AccessControlInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessControlOutput implements IRuleOutPut {
	@SerializedName("acis")
	private List<AccessControlInfo> accessControlInformation;

}
