package com.tgs.services.gms.datamodel;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@ApiModel(value = "This is being used to fetch policies based on search criteria. You can pass either any one field or combination of fields depending upon your search criteria")
public class PolicyFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch policies based on type ", example = "ui")
	private String type;
	@ApiModelProperty(notes = "To fetch policies matching given name. It's not mandatory to pass exact name. System will find best match based on the value ", example = "viewAccountReport")
	private String name;
	@ApiModelProperty(notes = "To fetch policies based on key", example = "commission")
	private String key;
	@ApiModelProperty(notes = "To fetch policies based on group", example = "agent_group")
	private String groupName;
}
