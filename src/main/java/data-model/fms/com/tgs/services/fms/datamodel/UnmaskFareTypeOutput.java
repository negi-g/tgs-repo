package com.tgs.services.fms.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UnmaskFareTypeOutput implements IRuleOutPut, Cleanable {

	@SerializedName("uf")
	private Boolean unmaskFare;

}
