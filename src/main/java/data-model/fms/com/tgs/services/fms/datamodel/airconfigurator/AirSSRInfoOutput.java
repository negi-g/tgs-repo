package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.base.utils.TgsCollectionUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class AirSSRInfoOutput implements IRuleOutPut {

	@SerializedName("bssrl")
	private List<SSRInformation> baggageSsrList;

	@SerializedName("mssrl")
	private List<SSRInformation> mealSsrList;

	@SerializedName("essrl")
	private List<SSRInformation> extraSsrList;

	@SerializedName("sssrl")
	private List<SSRInformation> seatSsrList;

	@Override
	public void cleanData() {
		setBaggageSsrList(TgsCollectionUtils.getCleanList(baggageSsrList));	
		setMealSsrList(TgsCollectionUtils.getCleanList(mealSsrList));		
		setExtraSsrList(TgsCollectionUtils.getCleanList(extraSsrList));		
		setSeatSsrList(TgsCollectionUtils.getCleanList(seatSsrList));		
	}
}
