package com.tgs.services.fms.datamodel.airconfigurator;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.ObjectUtils;

@Getter
@Setter
@Builder
public class SourceDefaultSSR implements IRuleOutPut, Cleanable {

	@SerializedName("cc")
	private CabinClass cabinClass;

	@SerializedName("dsh")
	private Long departureStartHour;

	@SerializedName("deh")
	private Long departureEndHour;

	@SerializedName("code")
	private String code;

	@SerializedName("desc")
	private String description;

	public Long getDepartureStartHour() {
		return ObjectUtils.firstNonNull(departureStartHour, 0L);
	}

	public Long getDepartureEndHour() {
		// 365 * 24L will be edge departure hours
		return ObjectUtils.firstNonNull(departureEndHour, 365 * 24L);
	}

}
