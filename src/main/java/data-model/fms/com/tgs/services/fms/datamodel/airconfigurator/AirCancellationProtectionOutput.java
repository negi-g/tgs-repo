package com.tgs.services.fms.datamodel.airconfigurator;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirCancellationProtectionOutput implements IRuleOutPut {

	@SerializedName("cpp")
	private String cancellationProtectionPremium;

	@SerializedName("cpt")
	private String cancellationProtectionTax;

	@SerializedName("cpmf")
	private String cancellationProtectionManagementFee;

	@SerializedName("cpmft")
	private String cancellationProtectionManagementFeeTax;

	@SerializedName("cpac")
	private String cancellationProtectionAgentCommission;
}
