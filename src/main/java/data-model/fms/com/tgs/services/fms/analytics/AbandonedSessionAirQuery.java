package com.tgs.services.fms.analytics;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AbandonedSessionAirQuery extends AnalyticsAirQuery {

	private Integer countofsessions;

	private String expirytime;

	private String jobtriggeredtime;
}
