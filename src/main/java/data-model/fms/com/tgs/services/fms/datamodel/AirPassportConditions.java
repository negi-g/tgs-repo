package com.tgs.services.fms.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Conditions;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class AirPassportConditions extends Conditions implements IRuleOutPut {

	@SerializedName("pped")
	private boolean passportExpiryDate;

	@SerializedName("pid")
	private boolean passportIssueDate;

	@APIUserExclude
	@SerializedName("pe")
	private boolean isPassportEligible;

	@SerializedName("pm")
	private boolean passportMandatory;

	@SerializedName("dobe")
	private boolean isDobEligible;

}
