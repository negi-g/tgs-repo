package com.tgs.services.fms.datamodel;

public enum RefundableType {

    NON_REFUNDABLE(0, "Non Refundable"),
    REFUNDABLE(1, "Refundable"),
    PARTIAL_REFUNDABLE(2, "Partial Refundable");

    private Integer code;

    private String refundableType;

    RefundableType(int code, String refundableType) {
        this.code = code;
        this.refundableType = refundableType;
    }

    public Integer getRefundableType() {
        return this.code;
    }

    public String getRefundableTypeName() {
        return this.refundableType;
    }


    public static RefundableType getEnumFromCode(Integer code) {
        return getRefundableType(code);
    }

    private static RefundableType getRefundableType(Integer code) {
        for (RefundableType action : RefundableType.values()) {
            if (action.getRefundableType().equals(code))
                return action;
        }
        return null;
    }

}