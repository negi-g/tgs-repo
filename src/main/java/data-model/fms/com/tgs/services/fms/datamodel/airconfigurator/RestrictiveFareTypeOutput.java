package com.tgs.services.fms.datamodel.airconfigurator;

import lombok.Setter;
import lombok.Getter;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.utils.TgsCollectionUtils;

@Getter
@Setter
public class RestrictiveFareTypeOutput implements IRuleOutPut, Cleanable {

	@SerializedName("ift")
	private List<String> includedFareTypes;

	@SerializedName("eft")
	private List<String> excludedFareTypes;

	@Override
	public void cleanData() {
		setIncludedFareTypes(TgsCollectionUtils.getNonNullElements(includedFareTypes));
		setExcludedFareTypes(TgsCollectionUtils.getNonNullElements(excludedFareTypes));
	}

}
