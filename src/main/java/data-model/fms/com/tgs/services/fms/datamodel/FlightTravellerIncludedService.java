package com.tgs.services.fms.datamodel;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightTravellerIncludedService {

	@SerializedName("cB")
	private String cabinBaggage;
	@SerializedName("iB")
	private String includedBaggage;
	@SerializedName("aB")
	private String additionalPurchasedBaggage;
	private String meal;
	private String seatNo;
}
