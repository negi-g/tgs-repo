package com.tgs.services.fms.analytics;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class NoResultSectorAirQuery extends AnalyticsAirQuery {

	private Long noresulthitcount;

}
