package com.tgs.services.fms.datamodel.airconfigurator;

import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NetRemittanceOutput implements IRuleOutPut, Cleanable {

	@SerializedName("nrc")
	private String netRemittanceCode;

	@Override
	public void cleanData() {
		setNetRemittanceCode(StringUtils.defaultIfBlank(netRemittanceCode, null));
	}

}
