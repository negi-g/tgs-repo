package com.tgs.services.fms.ruleengine;

import java.util.List;
import java.util.Map;

import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightSSRRuleCriteria implements IRuleOutPut {

    Map<SSRType, List<? extends SSRInformation>> ssrList;

}