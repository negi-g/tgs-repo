package com.tgs.services.fms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class NonOperatingSectorInfo {

	private String source;
	
	private String destination;
	
	private Integer sourceId;
	
}
