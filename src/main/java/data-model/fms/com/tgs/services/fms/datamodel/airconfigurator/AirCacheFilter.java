package com.tgs.services.fms.datamodel.airconfigurator;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirCacheFilter {

	@SerializedName("sp")
	@Min(0)
	private Integer startPeriod = 0;

	@SerializedName("ep")
	@Min(0)
	private Integer endPeriod = 365;

	@NotNull
	@SerializedName("dt")
	private Long diffTime;
}
