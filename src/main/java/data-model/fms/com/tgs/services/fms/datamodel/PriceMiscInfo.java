package com.tgs.services.fms.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.HashMap;


import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.AirRules;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import com.tgs.services.base.helper.RestExclude;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.ObjectUtils;

@Getter
@Setter
@Builder
@ToString(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
public class PriceMiscInfo {

	/**
	 * This consists of Fare Key
	 */
	private String fareKey;

	@SerializedName("ac")
	@ToString.Include
	private String accountCode;

	@ToString.Include
	private Boolean isPrivateFare;

	@SerializedName("tc")
	@ToString.Include
	private String tourCode;

	/*
	 * To Identify Which Commission Applied EX: Refer CMS
	 */
	@SerializedName("cRId")
	@ToString.Include
	private Long commericialRuleId;

	@SerializedName("ccid")
	private Integer ccInfoId;

	@SerializedName("sBId")
	private String supplierBookingId;

	@SerializedName("tL")
	@ToString.Include
	private LocalDateTime timeLimit;

	@SerializedName("iata")
	@ToString.Include
	private Double iata;

	@SerializedName("pc")
	@ToString.Include
	private AirlineInfo platingCarrier;

	/**
	 * @implSpec : this key is used after booking also its required for supplier end 1. API used:
	 *           confirmFareBeforeTicket 2. Supplier : Amadeus
	 */
	private String segmentKey;

	@SerializedName("lfid")
	private Long logicalFlightId;

	private LocalDateTime reviewTimeLimit;

	private String sessionId;

	private String mealWayType, baggageWayType, weight;

	private String traceId;

	// This consists of Journey Sell Key Which Represents Journey Info
	private String journeyKey;

	/**
	 * This is used for Ifly to identify the fare
	 */
	private String fareLevel;

	private String tokenId;

	private String marriageGrp;

	@ForbidInAPIRequest
	private String inventoryId;

	@SerializedName("cs")
	private String classOfService;

	@SerializedName("fcs")
	private String fareClassOfService;

	@SerializedName("fs")
	private String fareSequence;

	@SerializedName("rn")
	private String ruleNumber;

	@SerializedName("fna")
	private String fareApplicationName;

	@SerializedName("frid")
	private Long fareRuleId;

	@SerializedName("fTid")
	private Integer fareTypeId;

	// used for supplier to confirm isEticket(confirm book::ticket number) eligible or not (Ex : MYSTIFLY,TBO)
	@SerializedName("isEtkt")
	private Boolean isEticket;

	@ToString.Include
	private Integer legNum;

	private String providerCode;

	private String effectiveDate;

	private String availablitySource;

	private String participationLevel;

	private Boolean linkavailablity;

	private String polledAvailabilityOption;

	private String availabilityDisplayType;

	private String fareRuleInfoRef;

	private String fareRuleInfoValue;


	private String creditShellPNR;

	private Boolean isUserCreditCard;

	/**
	 * bookingTravellerInfoRef,isPromotionalFare,airPriceSolutionKey,paxPricingInfo,fareInfoKeyMap are used Travelport
	 * sessionless UAPI
	 *
	 **/

	private Map<PaxType, List<String>> paxPricingInfo;

	private Map<PaxType, String> fareInfoKeyMap;

	private Map<PaxType, List<String>> bookingTravellerInfoRef;

	private Boolean isPromotionalFare;

	private String airPriceSolutionKey;

	private FareRuleInformation fareRuleInfo;

	@SerializedName("ftrid")
	private Long fareTypeRuleId;

	@SerializedName("iiss")
	private Boolean isInternationalSplitSearch;


	@SerializedName("cpri")
	private Long cancellationProtectionRuleId;

	/**
	 * Used by SQIVA supplier. This field is required only at the time of booking, So no need to persist it in the
	 * system.
	 */
	private String bookLegId;

	@SerializedName("hfrid")
	private Long holdFeeRuleId;

	@RestExclude
	private Map<String, Integer> classWiseSeatCounts;

	private String fareIndicator;

	@SerializedName("cpi")
	private PriceInfo comparitivePriceInfo;

	@SerializedName("ids")
	private Double inventoryDiscount;

	@SerializedName("idds")
	private Double inventoryDoubleDiscount;
	
	@SerializedName("rim")
	@Builder.Default
	@ToString.Include
	private Map<AirRules, Long> ruleIdMap = new HashMap<AirRules, Long>();

	@SerializedName("cftid")
	private Long changeFareTypeId;

	@SerializedName("slpi")
	private PriceInfo secondLeastPriceInfo;

	public FareRuleInformation getFareRuleInfo() {
		if (fareRuleInfo == null) {
			fareRuleInfo = new FareRuleInformation();
		}
		return fareRuleInfo;
	}

	public Double getInventoryDiscount() {
		return ObjectUtils.firstNonNull(inventoryDiscount, 0D);
	}
}
