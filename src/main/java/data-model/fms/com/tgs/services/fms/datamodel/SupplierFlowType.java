package com.tgs.services.fms.datamodel;


import lombok.Getter;

@Getter
public enum SupplierFlowType {

	LOGIN, SEARCH, ITINERARY_PRICE, BOOK, FARE_RULE, FARE_VALIDATE, STUB_CREATION;

}
