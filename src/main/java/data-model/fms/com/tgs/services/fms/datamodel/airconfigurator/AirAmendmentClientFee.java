package com.tgs.services.fms.datamodel.airconfigurator;


import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.VoidClean;
import com.tgs.services.base.enums.AirType;
import lombok.Getter;
import java.util.Map;
import org.apache.commons.collections.MapUtils;

@Getter
public class AirAmendmentClientFee implements VoidClean {

	@SerializedName("af")
	protected Map<AirType, Double> amendmentFee;

	@Override
	public boolean isVoid() {
		return MapUtils.isEmpty(amendmentFee);
	}
}
