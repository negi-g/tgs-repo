package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.Arrays;
import java.util.List;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.AirCustomPathOutput;
import com.tgs.services.fms.datamodel.SsrOsiConditions;
import com.tgs.services.fms.datamodel.AirPassportConditions;
import com.tgs.services.fms.datamodel.AirSearchGrpOutput;
import com.tgs.services.fms.datamodel.PWSReason;
import com.tgs.services.fms.ruleengine.FlightBasicAirConfigOutput;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.fms.datamodel.UnmaskFareTypeOutput;
import lombok.Getter;

@Getter
public enum AirConfiguratorRuleType {

	// ENUM Name Should not exceed 14 Character //

	PASSPORT(FlightBasicRuleCriteria.class, new TypeToken<AirPassportConditions>() {}),
	SEARCHGRPNG(FlightBasicRuleCriteria.class, new TypeToken<AirSearchGrpOutput>() {}),
	CUSTOM_PATH(FlightBasicRuleCriteria.class, new TypeToken<AirCustomPathOutput>() {}),
	GNPUPROSE(FlightBasicRuleCriteria.class, new TypeToken<AirGeneralPurposeOutput>() {}),
	APIPROMOTION(FlightBasicRuleCriteria.class, new TypeToken<FlightBasicAirConfigOutput>() {}),
	CLIENTFEE(FlightBasicRuleCriteria.class, new TypeToken<AirClientFeeOutput>() {}),
	CLIENTMARKUP(FlightBasicRuleCriteria.class, new TypeToken<AirClientMarkupOutput>() {}),
	MESSAGE(FlightBasicRuleCriteria.class, new TypeToken<ListOutput<MessageInfo>>() {}),
	SOURCECONFIG(FlightBasicRuleCriteria.class, new TypeToken<AirSourceConfigurationOutput>() {}),
	PWS_REASONS(FlightBasicRuleCriteria.class, new TypeToken<ListOutput<PWSReason>>() {}),
	SSR_INFO(FlightBasicRuleCriteria.class, new TypeToken<AirSSRInfoOutput>() {}),
	DOB(FlightBasicRuleCriteria.class, new TypeToken<DobOutput>() {}),
	CHANGE_CLASS(FlightBasicRuleCriteria.class, new TypeToken<ListOutput<String>>() {}),
	CANCELCONFIG(FlightBasicRuleCriteria.class, new TypeToken<AirSourceCancelConfiguration>() {}),
	SOURCETHREAD(FlightBasicRuleCriteria.class, new TypeToken<AirSourceThreadConfiguration>() {}),
	FILTER(FlightBasicRuleCriteria.class, new TypeToken<AirFilterConfiguration>() {}),
	NETREMITTANCE(FlightBasicRuleCriteria.class, new TypeToken<NetRemittanceOutput>() {}),
	FARE_TYPE(FlightBasicRuleCriteria.class, new TypeToken<AirFareTypeOutput>() {}),
	BOOKING_PARAM_CONF(FlightBasicRuleCriteria.class, new TypeToken<BookingParamOutput>() {}),
	CANCELLATION_PROTECTION(FlightBasicRuleCriteria.class, new TypeToken<AirCancellationProtectionOutput>() {}),
	HOLD_FEE(FlightBasicRuleCriteria.class, new TypeToken<HoldFeeChargeOutput>() {}),
	SSR_FILTER(FlightBasicRuleCriteria.class, new TypeToken<AirSSRFilterConfiguration>() {}),
	CACHE_TTL(FlightBasicRuleCriteria.class, new TypeToken<AirCacheTypeTTLConfiguration>() {}),
	SUPPLIER_EXCLUSION(FlightBasicRuleCriteria.class, new TypeToken<AirSupplierExclusionOutput>() {}),
	RESTRICTIVE_FARE_TYPE(FlightBasicRuleCriteria.class, new TypeToken<RestrictiveFareTypeOutput>() {}),
	CACHE_FILTER(FlightBasicRuleCriteria.class, new TypeToken<CacheFilterOutput>() {}),
	HOLD_CONFIG(FlightBasicRuleCriteria.class, new TypeToken<AirHoldConfigOutput>() {}),
	UNMASK_FARE(FlightBasicRuleCriteria.class, new TypeToken<UnmaskFareTypeOutput>() {}),
	SSROSI_CONFIG(FlightBasicRuleCriteria.class, new TypeToken<SsrOsiConditions>() {}),
	CHANGE_FARE_TYPE(FlightBasicRuleCriteria.class, new TypeToken<ChangeFareTypeOutput>() {});


	private Class<? extends IRuleCriteria> inputType;
	private TypeToken<? extends IRuleOutPut> outPutTypeToken;

	<T extends IRuleCriteria, RT extends IRuleOutPut> AirConfiguratorRuleType(Class<T> inputType,
			TypeToken<RT> outPutTypeToken) {
		this.inputType = inputType;
		this.outPutTypeToken = outPutTypeToken;
	}

	public List<AirConfiguratorRuleType> getAirConfigRuleType() {
		return Arrays.asList(AirConfiguratorRuleType.values());
	}

	public static AirConfiguratorRuleType getRuleType(String name) {
		for (AirConfiguratorRuleType ruleType : AirConfiguratorRuleType.values()) {
			if (ruleType.getName().equals(name)) {
				return ruleType;
			}
		}
		return null;
	}

	public String getName() {
		return this.name();
	}

}
