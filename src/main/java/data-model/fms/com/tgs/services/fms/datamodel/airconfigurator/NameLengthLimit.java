package com.tgs.services.fms.datamodel.airconfigurator;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.datamodel.VoidClean;
import lombok.Data;

@Data
public class NameLengthLimit implements VoidClean {

	@SerializedName("fN")
	Integer firstNameLength;

	@SerializedName("finml")
	Integer firstNameMinLength;

	@SerializedName("lN")
	Integer lastNameLength;

	@SerializedName("lnml")
	Integer lastNameMinLength;

	@SerializedName("n")
	Integer fullNameLength;

	@SerializedName("funml")
	Integer fullNameMinLength;

	/**
	 * Each length is set if it is null or update if less strict than in specified limit.
	 * 
	 * @param nameLengthLimit
	 */
	public void merge(NameLengthLimit nameLengthLimit) {
		if (nameLengthLimit == null) {
			return;
		}

		Integer fnl = nameLengthLimit.getFirstNameLength(), lnl = nameLengthLimit.getLastNameLength(),
				nl = nameLengthLimit.getFullNameLength(), fiNML = nameLengthLimit.getFirstNameMinLength(),
				lNML = nameLengthLimit.getLastNameMinLength(), fuNML = nameLengthLimit.getFullNameMinLength();

		if (fnl != null) {
			if (firstNameLength == null || firstNameLength > fnl) {
				firstNameLength = fnl;
			}
		}
		if (lnl != null) {
			if (lastNameLength == null || lastNameLength > lnl) {
				lastNameLength = lnl;
			}
		}
		if (nl != null) {
			if (fullNameLength == null || fullNameLength > nl) {
				fullNameLength = nl;
			}
		}
		if (fiNML != null) {
			if (firstNameMinLength == null || firstNameMinLength < fiNML) {
				firstNameMinLength = fiNML;
			}
		}

		if (lNML != null) {
			if (lastNameMinLength == null || lastNameMinLength < lNML) {
				lastNameMinLength = lNML;
			}
		}

		if (fuNML != null) {
			if (fullNameMinLength == null || fullNameMinLength < fuNML) {
				fullNameMinLength = fuNML;
			}
		}
	}

	/**
	 * 
	 * @param nl name length
	 * @return {@code true} if {@code this.getFullNameLength(), this.getFullNameMinLength()} is null or satisfies the
	 *         minimum and maximum length range.
	 */
	public void validateFullNameLength(int nl, FieldErrorMap errors) {
		if (fullNameMinLength != null && nl < fullNameMinLength) {
			registerErrors(errors, "fullName", "minimum", fullNameMinLength);
		}
		if (fullNameLength != null && nl > fullNameLength) {
			registerErrors(errors, "fullName", "maximum", fullNameLength);
		}
	}

	/**
	 * 
	 * @param fnl First name length
	 * @return {@code true} if {@code this.getFirstNameLength(), this.getFirstNameMinLength()} is null or satisfies the
	 *         minimum and maximum length range.
	 */
	public void validateFirstNameLength(int fnl, FieldErrorMap errors) {
		if (firstNameLength != null && fnl > firstNameLength) {
			registerErrors(errors, "firstName", "maximum", firstNameLength);
		}
		if (firstNameMinLength != null && fnl < firstNameMinLength) {
			registerErrors(errors, "firstName", "minimum", firstNameMinLength);
		}
	}

	/**
	 * 
	 * @param lnl Last name length
	 * @return {@code true} if {@code this.getLastNameLength(), this.getFirstNameMinLength()} is null or satisfies the
	 *         minimum and maximum length range.
	 */
	public void validateLastNameLength(int lnl, FieldErrorMap errors) {
		if (lastNameLength != null && lnl > lastNameLength) {
			registerErrors(errors, "lastName", "maximum", lastNameLength);
		}
		if (lastNameMinLength != null && lnl < lastNameMinLength) {
			registerErrors(errors, "lastName", "minimum", lastNameMinLength);
		}
	}

	private void registerErrors(FieldErrorMap errors, String fieldName, String condition, Integer lengthLimit) {
		if (errors != null) {
			errors.put(fieldName, SystemError.INVALID_LENGTH.getErrorDetail(fieldName, condition, lengthLimit));
		}
	}

	@Override
	public boolean isVoid() {
		return firstNameLength == null && lastNameLength == null && fullNameLength == null && lastNameMinLength == null
				&& firstNameMinLength == null && fullNameMinLength == null;
	}
}
