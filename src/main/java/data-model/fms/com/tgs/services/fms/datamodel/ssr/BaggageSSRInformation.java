package com.tgs.services.fms.datamodel.ssr;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BaggageSSRInformation extends SSRInformation {
	private Integer quantity;
	private String unit;
}
