package com.tgs.services.fms.datamodel.ssr;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class SSRMiscInfo {
	// for TBO
	private int journeyType;

	private String seatCodeType;

	private Map<String, String> paxSsrKeys;

	private String pricingCurrency;

	// for travelportSessionless
	// for TBO (source/destination)
	private String remarks;

	// for travelportSessionless
	private String supplierKey;


}
