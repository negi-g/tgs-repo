package com.tgs.services.fms.datamodel;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.TgsMapUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirSearchGrpOutput implements IRuleOutPut {

	private Map<String, List<Integer[]>> value;

	@Override
	public void cleanData() {
		if(MapUtils.isNotEmpty(value)) {
			setValue(TgsMapUtils.getCleanMap(value, null, t-> TgsCollectionUtils.isEmpty(Arrays.asList(t))));
		}
	}
}
