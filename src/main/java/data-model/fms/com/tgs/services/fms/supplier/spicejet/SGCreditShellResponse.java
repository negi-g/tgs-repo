package com.tgs.services.fms.supplier.spicejet;

import java.time.LocalDateTime;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SGCreditShellResponse {

	@SerializedName("Status")
	private Integer status;
	
	@SerializedName("TxnID")
	private Integer txnID;
	
	@SerializedName("BookingPNR")
	private String bookingPNR;
	
	@SerializedName("Amount")
	private Double amount;
	
	@SerializedName("CurrencyCode")
	private String currencyCode;
	
	@SerializedName("CreditShellPNR")
	private String creditShellPNR;
	
	@SerializedName("OrganizaionCode")
	private String organizaionCode;
	
	@SerializedName("ExpiryDate")
	private LocalDateTime expiryDate;
	
	@SerializedName("Error")
	private String error;
	
	@SerializedName("Remarks")
	private String remarks;

}
