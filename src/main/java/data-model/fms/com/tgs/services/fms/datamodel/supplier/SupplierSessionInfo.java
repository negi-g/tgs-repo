package com.tgs.services.fms.datamodel.supplier;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SupplierSessionInfo {

    private String sessionToken;

    private String tripKey;

    private Boolean isSameForAll;

}