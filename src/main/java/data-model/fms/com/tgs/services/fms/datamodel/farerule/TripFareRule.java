package com.tgs.services.fms.datamodel.farerule;

import java.util.LinkedHashMap;
import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TripFareRule {
    Map<String, FareRuleInformation> fareRule;

    public Map<String, FareRuleInformation> getFareRule() {
        if (fareRule == null) {
            fareRule = new LinkedHashMap<>();
        }
        return fareRule;
    }

}