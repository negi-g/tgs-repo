package com.tgs.services.fms.datamodel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AirCacheSearchResult {

	private LocalDateTime creationTime;
	private List<TripInfo> tripInfos;

	public List<TripInfo> getTripInfos() {
		if (tripInfos == null) {
			tripInfos = new ArrayList<>();
		}
		return tripInfos;
	}


}
