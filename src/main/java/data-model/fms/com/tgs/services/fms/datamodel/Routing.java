package com.tgs.services.fms.datamodel;

import lombok.Getter;

@Getter
public enum Routing {

	ALL("A"), DIRECT("D"), DIRECT_NONSTOP("N"), SINGLE_CONNECTING("");

	private String code;

	private Routing(String code) {
		this.code = code;
	}

	public static Routing getEnumFromCode(String code) {
		return getRouting(code);
	}

	public static Routing getRouting(String code) {
		for (Routing routingType : Routing.values()) {
			if (routingType.getCode().equals(code))
				return routingType;
		}
		return null;
	}

}
