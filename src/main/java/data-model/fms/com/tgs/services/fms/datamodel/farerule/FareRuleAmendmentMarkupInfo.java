package com.tgs.services.fms.datamodel.farerule;

import lombok.Setter;
import lombok.Getter;
import java.util.Set;
import com.tgs.services.base.enums.AmendmentType;
import com.google.gson.annotations.SerializedName;

@Getter
@Setter
public class FareRuleAmendmentMarkupInfo {

	@SerializedName("at")
	private Set<AmendmentType> amendmentTypes;

	private Double amount;

}
