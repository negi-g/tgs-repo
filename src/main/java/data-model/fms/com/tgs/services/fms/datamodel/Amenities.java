package com.tgs.services.fms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Amenities {

	private Boolean isWifi;
	private Boolean isSmokingAllowed;
	private String entType;
	private Boolean inFlightEntertainment;

}
