package com.tgs.services.fms.datamodel.farerule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.RestExclude;
import org.apache.commons.collections.MapUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.utils.TgsMapUtils;
import com.tgs.services.fms.datamodel.RefundableType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FareRuleInformation implements IRuleOutPut {

	@APIUserExclude
	@SerializedName("isML")
	private Boolean isMealIndicator;

	@APIUserExclude
	@SerializedName("isHB")
	private Boolean isHandBaggageIndicator;

	@APIUserExclude
	@SerializedName("rT")
	private RefundableType refundableType;

	/**
	 * Currently there are 4 types of keys are supported in the {@link #checkedInBaggage} viz. ADT,CNN,INF,DEFAULT.
	 * <p>
	 * If {@link #checkedInBaggage} doesn't have any value based on type then set in DEFAULT
	 */
	@APIUserExclude
	@SerializedName("cB")
	Map<String, String> checkedInBaggage;

	/**
	 * Currently there are 4 types of keys are supported in the {@link #handBaggage} viz. ADT,CNN,INF,DEFAULT.
	 * <p>
	 * If {@link #handBaggage} doesn't have any value based on type then set in DEFAULT
	 */
	@APIUserExclude
	@SerializedName("hB")
	Map<String, String> handBaggage;

	@APIUserExclude
	@SerializedName("cp")
	private String cancellationPolicy;

	@APIUserExclude
	@SerializedName("dcp")
	private String dateChangePolicy;

	@SerializedName("mi")
	private Map<String, String> miscInfo;

	@SerializedName("fr")
	Map<FareRulePolicyType, Map<FareRuleTimeWindow, FareRulePolicyContent>> fareRuleInfo;

	@APIUserExclude
	@SerializedName("amu")
	@RestExclude
	private List<FareRuleAmendmentMarkupInfo> amendmentMarkup;

	public String getCheckedInBagagge(PaxType paxType) {
		if (MapUtils.isNotEmpty(checkedInBaggage)) {
			return checkedInBaggage.getOrDefault(paxType.getType(), checkedInBaggage.get("DEFAULT"));
		}
		return null;
	}

	public String getHandBaggage(PaxType paxType) {
		if (MapUtils.isNotEmpty(handBaggage)) {
			return handBaggage.getOrDefault(paxType.getType(), handBaggage.get("DEFAULT"));
		}
		return null;
	}

	public Map<String, String> getMiscInfo() {
		if (miscInfo == null) {
			miscInfo = new HashMap<>();
		}
		return miscInfo;
	}

	public Map<FareRulePolicyType, Map<FareRuleTimeWindow, FareRulePolicyContent>> getFareRuleInfo() {
		if (fareRuleInfo == null) {
			fareRuleInfo = new HashMap<>();
		}
		return fareRuleInfo;
	}

	public List<FareRuleAmendmentMarkupInfo> getAmendmentMarkup() {
		if (amendmentMarkup == null) {
			amendmentMarkup = new ArrayList<>();
		}
		return amendmentMarkup;
	}

	public Double getAmendmentMarkupAmount(AmendmentType amendmentType) {
		for (FareRuleAmendmentMarkupInfo amendmentMarkup : getAmendmentMarkup()) {
			if (amendmentMarkup.getAmendmentTypes().contains(amendmentType)
					|| (AmendmentType.CANCELLATION_QUOTATION.equals(amendmentType)
							&& amendmentMarkup.getAmendmentTypes().contains(AmendmentType.CANCELLATION))
					|| (AmendmentType.REISSUE_QUOTATION.equals(amendmentType)
							&& amendmentMarkup.getAmendmentTypes().contains(AmendmentType.REISSUE))) {
				return amendmentMarkup.getAmount();
			}
		}
		return 0d;
	}

	public void cleanData() {
		if (MapUtils.isNotEmpty(handBaggage)) {
			handBaggage = TgsMapUtils.removeKeyIfEmpty(handBaggage);
		}
		if (MapUtils.isNotEmpty(checkedInBaggage)) {
			checkedInBaggage = TgsMapUtils.removeKeyIfEmpty(checkedInBaggage);
		}
	}
}
