package com.tgs.services.fms.datamodel;

import com.tgs.services.base.helper.Exclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@ToString
public class PNRCreditInfo {

	private String pnr;

	private Double creditBalance;

	private LocalDateTime cSExpiryDate;
	
	@Exclude
	private String bookingId;

	public Double getCreditBalance() {
		if (creditBalance == null) {
			creditBalance = 0d;
		}
		return creditBalance;
	}

}
