package com.tgs.services.fms.datamodel;

import java.util.List;
import com.tgs.services.base.datamodel.VoidClean;
import com.tgs.services.base.utils.TgsCollectionUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirPath implements VoidClean {
	private List<PathEdge> edges;
	
	@Override
	public void cleanData() {
		setEdges(TgsCollectionUtils.getCleanList(edges));
	}

	@Override
	public boolean isVoid() {
		return edges ==null;
	}
}
