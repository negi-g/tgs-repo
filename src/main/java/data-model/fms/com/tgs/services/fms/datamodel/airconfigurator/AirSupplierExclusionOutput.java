package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.utils.TgsCollectionUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirSupplierExclusionOutput implements IRuleOutPut, Cleanable {

	@SerializedName("esi")
	private List<String> excludedSupplierIds;

	@Override
	public void cleanData() {
		setExcludedSupplierIds(TgsCollectionUtils.getCleanStringList(getExcludedSupplierIds()));
	}
}
