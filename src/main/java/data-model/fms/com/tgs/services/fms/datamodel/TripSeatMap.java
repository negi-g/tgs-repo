package com.tgs.services.fms.datamodel;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.tgs.services.fms.datamodel.ssr.SeatInformation;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TripSeatMap {

    Map<String, SeatInformation> tripSeat;


    public Map<String, SeatInformation> getTripSeat() {
        if (tripSeat == null) {
            tripSeat = new HashMap<>();
        }
        return tripSeat;
    }

    public void prepareStructure() {
        if (MapUtils.isNotEmpty(tripSeat)) {
            tripSeat.forEach((tripKey, seats) -> {
                seats.prepareStructureData();
            });
        }

    }


}
