package com.tgs.services.fms.supplier.indigo;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountCreditData {

	String accountCreditId;
	String accountId;
	double amount;
	double available;
	String currencyCode;
	String expiration;
}
