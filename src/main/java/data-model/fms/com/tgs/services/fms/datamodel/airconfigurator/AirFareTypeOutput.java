package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.utils.TgsCollectionUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirFareTypeOutput implements IRuleOutPut {

	@SerializedName("ft")
	private String fareType;

	@SerializedName("cft")
	private List<String> comparativeFareTypes;

	@Override
	public void cleanData() {
		setComparativeFareTypes(TgsCollectionUtils.getNonNullElements(getComparativeFareTypes()));
	}

}
