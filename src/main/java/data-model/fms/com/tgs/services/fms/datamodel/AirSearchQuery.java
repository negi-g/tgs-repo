package com.tgs.services.fms.datamodel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Comparator;
import java.util.TreeSet;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import com.tgs.services.base.helper.RestExclude;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.helper.APIUserExclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public class AirSearchQuery {

	@Valid
	@NonNull
	@ApiModelProperty(
			value = "This field is used to set searchInfo. In case of onward , this list will have one element . For return and multiple it will contain 2 and more respectively.There is no need to set entire city/country information, only the  airport/city code inside from/to Aiport will suffice the requirement ",
			required = true)
	private List<RouteInfo> routeInfos;

	@APIUserExclude
	@RestExclude
	private Integer origRouteInfoSize;

	@ApiModelProperty(value = "In case you would like to search based on specific airline you can set airline code")
	private Set<AirlineInfo> preferredAirline;

	@ApiModelProperty(value = "If you are looking for any specific class , you can set this field")
	private CabinClass cabinClass = CabinClass.ECONOMY;

	@NonNull
	@ApiModelProperty(value = "Use this field to set information about the passenger")
	@NotNull
	private Map<PaxType, Integer> paxInfo;

	@NonNull
	@ApiModelProperty(
			value = "This field is required to uniquely identify search request. It should be unique for every request",
			example = "12345")
	@APIUserExclude
	private String searchId;

	@NonNull
	@ApiModelProperty(
			value = "This field is required to uniquely identify search. It should be unique for every request",
			example = "12345")
	@APIUserExclude
	private String requestId;

	@NonNull
	@ApiModelProperty(
			value = "Currently , there are 3 types(ONEWAY,RETURN,MULTICITY) of searches are support. Set appropriate option based on the search preference",
			example = "ONEWAY")
	private SearchType searchType;

	@RestExclude
	@APIUserExclude
	private SearchType origSearchType;

	@ApiModelProperty(
			value = "This field is used to set advance parameters. Search Response will depend upon modifiers passed in search query")
	private AirSearchModifiers searchModifiers;

	@ApiModelProperty(
			value = "This field will be returned when you will hit search-airline list. Passed the same value at the time of search")
	@APIUserExclude
	@RestExclude
	private List<Integer> sourceIds;

	@APIUserExclude
	@RestExclude
	private List<String> supplierIds;

	@ApiModelProperty(value = "This is useful to bifurcate between domestic and international")
	private Boolean isDomestic;

	@ApiModelProperty(hidden = true)
	@APIUserExclude
	public Boolean isCustomCombination;

	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	@APIUserExclude
	public Boolean isOneWay;

	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	@APIUserExclude
	public Boolean isDomesticMultiCity;

	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	@APIUserExclude
	private Boolean isDomesticReturn;

	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	@APIUserExclude
	public Boolean isMultiCity;

	@APIUserExclude
	private Boolean isLiveSearch;

	@APIUserExclude
	private AirFlowType flowType;

	/**
	 * Timeout in sec
	 */
	@ApiModelProperty(hidden = true)
	@APIUserExclude
	@RestExclude
	private Long timeout;

	@ApiModelProperty(hidden = true)
	public boolean isMultiCitySearch() {
		return searchType.equals(SearchType.MULTICITY);
	}

	public void addSourceId(Integer sourceId) {
		if (sourceIds == null) {
			sourceIds = new ArrayList<>();
		}
		sourceIds.add(sourceId);
	}

	@ApiModelProperty(hidden = true)
	public boolean isReturn() {
		if (routeInfos.size() == 1) {
			searchType = SearchType.ONEWAY;
		} else if (routeInfos.size() > 2) {
			searchType = SearchType.MULTICITY;
		} else {
			if (routeInfos.get(0).getFromCityOrAirport().getCode()
					.equals(routeInfos.get(1).getToCityOrAirport().getCode())
					&& routeInfos.get(0).getToCityOrAirport().getCode()
							.equals(routeInfos.get(1).getFromCityOrAirport().getCode())) {
				searchType = SearchType.RETURN;
			} else {
				searchType = SearchType.MULTICITY;
			}
		}
		return SearchType.RETURN.equals(getSearchType());
	}

	public static boolean getIsDomestic(List<RouteInfo> routeInfos) {
		boolean isDomestic = true;
		String country = routeInfos.get(0).getFromCityOrAirport().getCountry();
		for (RouteInfo routeInfo : routeInfos) {
			if (!routeInfo.getFromCityOrAirport().getCountry().equals(country)
					|| !routeInfo.getToCityOrAirport().getCountry().equals(country)) {
				isDomestic = false;
				break;
			}
		}
		return isDomestic;
	}

	public AirType getAirType() {
		return getIsDomestic() ? AirType.DOMESTIC : AirType.INTERNATIONAL;
	}

	public boolean isMultiCity() {
		return isMultiCity = SearchType.MULTICITY.equals(getSearchType());
	}

	public boolean isOneWay() {
		return isOneWay = SearchType.ONEWAY.equals(getSearchType());
	}

	public boolean isDomesticMultiCity() {
		return isDomesticMultiCity = (!isReturn() && isMultiCity() && getIsDomestic());
	}

	public boolean isDomesticReturn() {
		return isDomesticReturn = (isReturn() && getIsDomestic());
	}

	@ApiModelProperty(hidden = true)
	public boolean isIntl() {
		return !getIsDomestic();
	}

	@ApiModelProperty(hidden = true)
	public boolean isIntlReturn() {
		return !getIsDomestic() && isReturn();
	}

	public void addPreferredAirline(AirlineInfo airlineInfo) {
		if (preferredAirline == null) {
			preferredAirline = new HashSet<>();
		}
		preferredAirline.add(airlineInfo);
	}

	@ApiModelProperty(hidden = true)
	public String getPrefferedAirline() {
		if (preferredAirline == null || preferredAirline.isEmpty()) {
			return "";
		}
		String airline = "";
		Comparator<AirlineInfo> comparator = new Comparator<AirlineInfo>() {
			@Override
			public int compare(AirlineInfo o1, AirlineInfo o2) {
				return StringUtils.compare(o1.getCode(), o2.getCode());
			}
		};
		Set<AirlineInfo> airlineInfos = new TreeSet<>(comparator);
		airlineInfos.addAll(preferredAirline);
		for (AirlineInfo aInfo : airlineInfos) {
			airline = String.join(",", aInfo.getCode(), airline);
		}
		return airline.substring(0, airline.length() - 1);
	}

	@ApiModelProperty(hidden = true)
	public TripInfoType getTripType() {
		if (this.isDomesticReturn()) {
			return TripInfoType.RETURN;
		} else if (this.isReturn()) {
			return TripInfoType.COMBO;
		} else if (routeInfos.size() >= 2 && (!this.isDomesticMultiCity())) {
			return TripInfoType.COMBO;
		}
		return TripInfoType.ONWARD;
	}

	public Boolean isCustomCombination() {
		return isCustomCombination = BooleanUtils.isTrue(isCustomCombination);
	}

	public void setIsDomestic(String clientCountry, List<String> clientDependCountries) {
		boolean isDomestic = true;
		if (StringUtils.isNotBlank(clientCountry)) {
			for (RouteInfo routeInfo : this.getRouteInfos()) {
				if (CollectionUtils.isNotEmpty(clientDependCountries)) {
					if ((!routeInfo.getFromCityOrAirport().getCountry().equalsIgnoreCase(clientCountry)
							|| !routeInfo.getToCityOrAirport().getCountry().equalsIgnoreCase(clientCountry))
							&& (!clientDependCountries.contains(routeInfo.getFromCityOrAirport().getCountry())
									|| !clientDependCountries.contains(routeInfo.getToCityOrAirport().getCountry()))) {
						isDomestic = false;
						break;
					}
				} else if (!routeInfo.getFromCityOrAirport().getCountry().equalsIgnoreCase(clientCountry)
						|| !routeInfo.getToCityOrAirport().getCountry().equalsIgnoreCase(clientCountry)) {
					isDomestic = false;
					break;
				}
			}
		}
		this.isDomestic = isDomestic;
	}

	@ApiModelProperty(hidden = true)
	public void populateMissingParametersInAirSearchQuery(String clientCountry,
			List<String> considerClientDepCountries) {

		if (CollectionUtils.isNotEmpty(this.getPreferredAirline())) {
			boolean isAllBlank = true;
			for (AirlineInfo info : this.getPreferredAirline()) {
				if (StringUtils.isNotBlank(info.getCode())) {
					isAllBlank = false;
					break;
				}
			}
			if (isAllBlank) {
				this.setPreferredAirline(null);
			}
		}

		this.setIsDomestic(clientCountry, considerClientDepCountries);
		this.isIntl();
		this.isDomesticMultiCity();
		this.isDomesticReturn();
		this.isOneWay();
		this.isMultiCity();
		this.isReturn();
		this.isSetSearchType();
		this.isCustomCombination();
		if (this.getSearchModifiers() == null) {
			this.setSearchModifiers(new AirSearchModifiers());
			this.getSearchModifiers().setIsDirectFlight(false);
		}
		this.getSearchModifiers().setStoreSearchLog(true);
		if (this.getSearchModifiers().getSourceId() == null
				|| this.getSearchModifiers().getSourceId().intValue() == 0) {
			this.getSearchModifiers().setSourceId(null);
		}
		this.paxInfo();
	}

	@ApiModelProperty(hidden = true)
	private void paxInfo() {
		Map<PaxType, Integer> paxInfo = getPaxInfo();
		if (paxInfo.get(PaxType.CHILD) == null) {
			paxInfo.put(PaxType.CHILD, 0);
		}
		if (paxInfo.get(PaxType.INFANT) == null) {
			paxInfo.put(PaxType.INFANT, 0);
		}
		setPaxInfo(paxInfo);
	}

	@ApiModelProperty(hidden = true)
	public void isSetSearchType() {
		if (routeInfos != null && routeInfos.size() == 1) {
			this.setSearchType(SearchType.ONEWAY);
		} else if (routeInfos != null && routeInfos.size() == 2) {
			RouteInfo onwardRoute = routeInfos.get(0);
			RouteInfo returnRoute = routeInfos.get(1);

			if (onwardRoute.getFromCityAirportCode().equals(returnRoute.getToCityAirportCode())
					&& onwardRoute.getToCityAirportCode().equals(returnRoute.getFromCityAirportCode())) {
				this.setSearchType(SearchType.RETURN);
			}
		} else if (routeInfos != null && routeInfos.size() > 2) {
			this.setSearchType(SearchType.MULTICITY);
		}
	}

	public SearchType getOrigSearchType() {
		return origSearchType != null ? origSearchType : searchType;
	}

	@ApiModelProperty(hidden = true)
	public boolean isPNRCreditSearch() {
		if (getSearchModifiers() != null && getSearchModifiers().getPnrCreditInfo() != null
				&& StringUtils.isNotBlank(getSearchModifiers().getPnrCreditInfo().getPnr())) {
			return true;
		}
		return false;
	}

	@ApiModelProperty(hidden = true)
	public List<TripInfoType> getTripInfoTypes() {
		List<TripInfoType> tripInfoTypes = new ArrayList<>();
		boolean isSplitSearch = isSplitSearch();
		if (isOneWay || (isDomestic && SearchType.MULTICITY.equals(origSearchType))) {
			tripInfoTypes.add(TripInfoType.ONWARD);
		} else if (!isSplitSearch && (isIntlReturn() || (isIntl() && isMultiCity()))) {
			tripInfoTypes.add(TripInfoType.COMBO);
		} else if (isSplitSearch || isDomesticReturn) {
			tripInfoTypes.add(TripInfoType.ONWARD);
			tripInfoTypes.add(TripInfoType.RETURN);
		}
		return tripInfoTypes;
	}

	@ApiModelProperty(hidden = true)
	public boolean isIntlMultiCity() {
		return isIntl() && isMultiCity();
	}

	@ApiModelProperty(hidden = true)
	public CabinClass getCabinClass() {
		return ObjectUtils.firstNonNull(cabinClass, CabinClass.ECONOMY);
	}

	@ApiModelProperty(hidden = true)
	public boolean isSplitSearch() {
		return BooleanUtils.isTrue(getSearchModifiers().getIsInternationalSplitSearch());
	}
}
