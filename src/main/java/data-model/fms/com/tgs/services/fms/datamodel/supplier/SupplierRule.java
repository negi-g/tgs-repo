package com.tgs.services.fms.datamodel.supplier;

import java.time.LocalDateTime;

import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = true)
public class SupplierRule implements IRule {

	@ToString.Include
	private long id;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	@ToString.Include
	private Boolean enabled;

	@ToString.Include
	private AirType airType;

	@ToString.Include
	private SearchType searchType;

	@ToString.Include
	private Integer sourceId;

	@ToString.Include
	private String supplierId;

	private FlightBasicRuleCriteria inclusionCriteria;

	private FlightBasicRuleCriteria exclusionCriteria;

	private SupplierAdditionalInfo supplierAdditionalInfo;

	private Double priority;

	private Boolean exitOnMatch;

	private String description;

	@Exclude
	private Boolean isDeleted = Boolean.FALSE;

	@Override
	public IRuleOutPut getOutput() {
		return supplierAdditionalInfo;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public boolean exitOnMatch() {
		return exitOnMatch == null ? false : exitOnMatch;
	}

	@Override
	public boolean getEnabled() {
		return enabled;
	}

	@Override
	public double getPriority() {
		return priority == null ? 0.0 : priority;
	}

	public FlightBasicRuleCriteria getInclusionRuleCriteria(FlightBasicRuleCriteria defaultCriteria) {
		return inclusionCriteria != null ? inclusionCriteria : defaultCriteria;
	}

}
