package com.tgs.services.fms.datamodel.airconfigurator;


import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections.MapUtils;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class AirCacheTypeTTLConfiguration implements IRuleOutPut, Cleanable {


	@SerializedName("tm")
	private Map<String, Integer> ttlMap;

	@Override
	public void cleanData() {}

	public Integer getTtl(String cacheType) {
		return MapUtils.getIntValue(ttlMap, cacheType);
	}
}
