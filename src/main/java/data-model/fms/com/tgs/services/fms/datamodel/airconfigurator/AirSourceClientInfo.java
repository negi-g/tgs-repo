package com.tgs.services.fms.datamodel.airconfigurator;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class AirSourceClientInfo {

	@SerializedName("ad")
	private String address;

	@SerializedName("can")
	private String companyName;

	@SerializedName("city")
	private String city;

	@SerializedName("pin")
	private String postalCode;

	@SerializedName("country")
	private String country;

	@SerializedName("state")
	private String state;

}

