package com.tgs.services.fms.datamodel.ssr;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MealSSRInformation extends SSRInformation {

}
