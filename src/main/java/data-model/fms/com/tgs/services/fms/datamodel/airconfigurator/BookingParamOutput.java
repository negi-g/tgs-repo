package com.tgs.services.fms.datamodel.airconfigurator;

import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingParamOutput implements IRuleOutPut {

	private String email;
	private String mobile;
}
