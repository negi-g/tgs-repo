package com.tgs.services.fms.datamodel.supplier;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class SupplierAbandonedSessionInfo {

	private String supplierId;
	private String sourceId;
	private Integer countOfSessions;
	private String expiryTime;
	private String jobTriggeredTime;
}
