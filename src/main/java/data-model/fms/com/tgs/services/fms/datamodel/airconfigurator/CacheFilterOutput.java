package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;


@Getter
public class CacheFilterOutput implements IRuleOutPut {

	@SerializedName("ttlconfig")
	private List<AirCacheFilter> ttlConfig;
}
