package com.tgs.services.fms.datamodel;

import com.tgs.services.base.helper.DBExclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravellerMiscInfo {

	@DBExclude
	private String passengerKey;
}
