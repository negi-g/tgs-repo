package com.tgs.services.fms.datamodel.airconfigurator;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Map;

@ToString
@Getter
@Setter
public class AirSSRFilterConfiguration implements IRuleOutPut, Cleanable {


	@SerializedName("dsbd")
	private Map<SSRType, Boolean> isNotApplicable;

	@Override
	public void cleanData() {}


	public boolean isSSRApplicable(SSRType ssrType) {
		return BooleanUtils.isNotTrue(getIsNotApplicable().getOrDefault(ssrType, false));
	}


}
