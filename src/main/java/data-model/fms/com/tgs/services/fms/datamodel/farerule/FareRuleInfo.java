package com.tgs.services.fms.datamodel.farerule;

import java.time.LocalDateTime;

import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder(toBuilder = true)
@Getter
@Setter
public class FareRuleInfo implements IRule {

	private Long id;

	private AirType airType;

	private String airline;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	private Double priority;

	private Boolean enabled;

	private FlightBasicRuleCriteria inclusionCriteria;

	private FlightBasicRuleCriteria exclusionCriteria;

	private FareRuleInformation fareRuleInformation;

	@Exclude
	private boolean isDeleted;

	private String description;

	@Override
	public IRuleOutPut getOutput() {
		return fareRuleInformation;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public boolean exitOnMatch() {
		return true;
	}

	@Override
	public boolean getEnabled() {
		return enabled;
	}

	@Override
	public double getPriority() {
		return priority;
	}

	public FlightBasicRuleCriteria getInclusionRuleCriteria(FlightBasicRuleCriteria defaultCriteria) {
		return inclusionCriteria != null ? inclusionCriteria : defaultCriteria;
	}
	
	public AirType getAirType() {
		if (airType == null) {
			airType = AirType.ALL;
		}
		return airType;
	}
}
