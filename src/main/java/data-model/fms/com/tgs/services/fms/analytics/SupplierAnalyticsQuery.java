package com.tgs.services.fms.analytics;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class SupplierAnalyticsQuery extends AnalyticsAirQuery {

	protected Integer cacheapplied;

	protected Integer supplierhit;

	protected Integer tripsize;

	protected Integer batchsize;

	protected String stubname;

	protected String createdtime;


}
