package com.tgs.services.fms.datamodel.airconfigurator;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;

@Getter
@Setter
public class AirSourceThreadConfig {


	@SerializedName("ttoal")
	private Integer maxTimeOutAllowed;

	// In minutes
	@SerializedName("tffto")
	private Long timeFrameForTimeOut;


	@SerializedName("mL")
	private Integer maxLimit;

	@SerializedName("cto")
	private Boolean considerThreadTimeOut;

	public int getMaxLimit() {
		return ObjectUtils.firstNonNull(maxLimit, 0);
	}

	public int getMaxTimeOutAllowed() {
		return ObjectUtils.firstNonNull(maxTimeOutAllowed, 0);
	}

	public long getTimeFrameForTimeOut() {
		return ObjectUtils.firstNonNull(timeFrameForTimeOut, new Long(0));
	}

	public boolean isExceededThreadCount(int currentThreadCount) {
		return currentThreadCount > getMaxLimit();
	}

	public boolean isTimeOutThreadExceeded(int timeOutThreadCount, Long lastResetTime) {
		if (BooleanUtils.isTrue(considerThreadTimeOut)) {
			return timeOutThreadCount > getMaxTimeOutAllowed();
		}
		return false;
	}

	public boolean isTimeOutTimeFrameCompleted(Long lastResetTime) {
		long currentTimeinMs = System.currentTimeMillis();
		return ((currentTimeinMs - lastResetTime) > (getTimeFrameForTimeOut() * 1000 * 60));
	}
}
