package com.tgs.services.fms.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Conditions;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.BooleanUtils;

@Getter
@Setter
@SuperBuilder
public class SsrOsiConditions extends Conditions implements IRuleOutPut {

	@SerializedName("ior")
	private Boolean isOsiRequired;

	@SerializedName("isr")
	private Boolean isSeaSsrRequired;

	@SerializedName("iom")
	private Boolean isOsiMandatory;

	public boolean isConditionsNA() {
		return BooleanUtils.isFalse(isOsiRequired) && BooleanUtils.isFalse(isSeaSsrRequired)
				&& BooleanUtils.isFalse(isOsiMandatory);
	}

}
