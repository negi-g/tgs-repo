package com.tgs.services.fms.datamodel.ssr;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.Exclude;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
public class SeatInformation {

	@SerializedName("sData")
	private SeatPosition structureData;

	@SerializedName("sInfo")
	private List<SeatSSRInformation> seatsInfo;

	@Exclude
	private StringJoiner numColumns;

	public void prepareStructureData() {
		if (CollectionUtils.isNotEmpty(seatsInfo)) {
			parseSeatsInfo();
		}
	}

	private void parseSeatsInfo() {
		setSeatGroup();
		sortSeatsInfo();
		Map<Integer, List<SeatSSRInformation>> deckData = seatsInfo.stream()
				.collect(Collectors.groupingBy(SeatSSRInformation::getSeatGroup));
		AtomicInteger maxRow = new AtomicInteger(1);
		AtomicInteger maxColumn = new AtomicInteger(1);
		String numColumns = getColumns(deckData);
		deckData.forEach((row, seats) -> {
			AtomicInteger y = new AtomicInteger(1);
			Integer columnSize = numColumns.length();
			for (int colNo = 0; colNo < columnSize; colNo++) {
				Boolean isShiftWalkSpace = false;
				char currentColumn = numColumns.charAt(colNo);
				SeatSSRInformation currentSeat = getSeat(seats, currentColumn);

				if (currentSeat != null && colNo + 1 < columnSize) {
					Boolean isCurrentColumnAsileSeat = currentSeat.getIsAisle();
					SeatSSRInformation nextColumnSeat = getSeat(seats, numColumns.charAt(colNo + 1));
					if (nextColumnSeat != null && BooleanUtils.isTrue(nextColumnSeat.getIsAisle())
							&& BooleanUtils.isTrue(isCurrentColumnAsileSeat)) {
						isShiftWalkSpace = true;
					}
				}
				if (currentSeat == null) {
					y.getAndIncrement();
				} else {
					currentSeat.setSeatPosition(SeatPosition.builder().column(y.get()).row(row).build());
					if (maxColumn.get() < y.intValue()) {
						maxColumn.set(y.intValue());
					}
					y.getAndIncrement();
				}

				if (isShiftWalkSpace.booleanValue()) {
					y.getAndIncrement();
				}
			}
			if (maxRow.get() <= row) {
				maxRow.set(row);
			}
		});
		structureData = SeatPosition.builder().row(maxRow.get()).column(maxColumn.get()).build();
	}

	private SeatSSRInformation getSeat(List<SeatSSRInformation> seats, char currentColumn) {
		for (SeatSSRInformation ssrInformation : seats) {
			char currentCode = ssrInformation.getSeatColumn().charAt(0);
			if (currentCode == currentColumn) {
				return ssrInformation;
			}
		}
		return null;
	}

	private String getColumns(Map<Integer, List<SeatSSRInformation>> deckData) {
		char[] tempColumns = null;
		AtomicInteger maxColumnSize = new AtomicInteger(0);
		deckData.forEach((row, seats) -> {
			if (maxColumnSize.get() < seats.size()) {
				numColumns = new StringJoiner("");
				AtomicInteger seatIndex = new AtomicInteger(0);
				seats.forEach(currentSeat -> {
					String seatCode = currentSeat.getSeatColumn();
					if (!numColumns.toString().contains(seatCode)) {
						numColumns.add(seatCode);
					}
					if (seatIndex.get() < seats.size() - 1) {
						SeatSSRInformation nextColumn = seats.get(seatIndex.get() + 1);
						if (BooleanUtils.isTrue(currentSeat.getIsAisle())
								&& BooleanUtils.isTrue(nextColumn.getIsAisle())) {
							if (numColumns.toString().contains(currentSeat.getSeatColumn())) {
								numColumns.add(" ");
							}
						}
					}
					seatIndex.getAndIncrement();
				});
			}
			if (maxColumnSize.get() < seats.size()) {
				maxColumnSize.set(seats.size());
			}
		});
		tempColumns = numColumns.toString().toCharArray();
		// return sorted seat code [A,B,C, ,D]
		return new String(tempColumns);
	}

	private void setSeatGroup() {
		seatsInfo.forEach(seatInfo -> {
			seatInfo.setSeatGroup(Integer.valueOf(seatInfo.getSeatNo().replaceAll("[a-zA-Z]", "")));
		});
	}

	private void sortSeatsInfo() {
		Collections.sort(seatsInfo, new Comparator<SeatSSRInformation>() {
			@Override
			public int compare(SeatSSRInformation o1, SeatSSRInformation o2) {
				return Integer.compare(o1.getSeatGroup(), o2.getSeatGroup());
			}
		});
	}

}
