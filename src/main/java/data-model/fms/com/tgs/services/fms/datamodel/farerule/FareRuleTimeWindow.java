package com.tgs.services.fms.datamodel.farerule;

import lombok.Getter;

@Getter
public enum FareRuleTimeWindow {
    HOURS_0_2("0-2 Hours"),
    HOURS_2_4("2-4 Hours"),
    HOURS_4_24("4-24 Hours"),
    HOURS_24_365("24Hrs-365 Days"), // Must be DAYS_1_365
    NOSHOW("No Show"),
    NO_SHOW("No-Show"),
    HOURS_0_24("0-24 Hours"),
    HOURS_0_365("0 hours - 365 Days"), // Must be DAYS_0_365
    BEFORE_DEPARTURE("Before Departure"),
    AFTER_DEPARTURE("After Departure"),
    DEFAULT("ALL");

    private String timeWindow;

    FareRuleTimeWindow(String windowPeriod) {
        this.timeWindow = windowPeriod;
    }

    public static FareRuleTimeWindow getTimeWindow(String timeWindow) {
        for (FareRuleTimeWindow window : FareRuleTimeWindow.values()) {
            if (window.timeWindow.equalsIgnoreCase(timeWindow))
                return window;
        }
        return null;
    }

}
