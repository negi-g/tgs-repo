package com.tgs.services.fms.datamodel.supplier;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;

import com.tgs.services.fms.datamodel.SegmentInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SupplierConfiguration {
	private SupplierCredential supplierCredential;
	private SupplierAdditionalInfo supplierAdditionalInfo;

	/** Don't use this variable while sending any API call to backend */
	private SupplierBasicInfo basicInfo;
	
	public SupplierAdditionalInfo getSupplierAdditionalInfo() {
		if (supplierAdditionalInfo == null) {
			supplierAdditionalInfo = new SupplierAdditionalInfo();
		}
		return supplierAdditionalInfo;
	}

	@Deprecated
	public String getTicketingSupplierId(SegmentInfo segmentInfo, int priceIndex) {
		String supplierId = basicInfo.getSupplierId();
		if (supplierAdditionalInfo != null) {
			supplierId = ObjectUtils
					.firstNonNull(supplierAdditionalInfo.getTicketingSupplierId(segmentInfo, priceIndex), supplierId);
		}

		return supplierId;
	}

	public Integer getSourceId() {
		return basicInfo.getSourceId();
	}

	public String getSupplierId() {
		return basicInfo.getSupplierId();
	}

	@Deprecated
	public String getTicketingSupplierId(SegmentInfo segmentInfo) {
		return getTicketingSupplierId(segmentInfo, 0);
	}
	
	public boolean isTestCredential() {
		return supplierCredential != null ? BooleanUtils.isTrue(supplierCredential.getIsTestCredential()) : false;
	}
}
