package com.tgs.services.fms.datamodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.AirPath;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.TgsMapUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirCustomPathOutput implements IRuleOutPut {

	private Map<String, List<AirPath>> value;

	@Override
	public void cleanData() {
		if(MapUtils.isNotEmpty(value)) {
			Map<String,List<AirPath>> finalMap = new HashMap<>();
			value.forEach((k,v) -> { 
				finalMap.put(k, TgsCollectionUtils.getCleanList(v));
			}); 
			value = TgsMapUtils.getCleanMap(finalMap, TgsCollectionUtils::getNonNullElements, CollectionUtils::isEmpty);
		}
	}
}
