package com.tgs.services.fms.datamodel;

import com.tgs.services.base.BulkUploadQuery;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@ToString
public class SourceRouteInfo extends BulkUploadQuery {

	private Long id;

	@Pattern(regexp = "^[a-zA-Z*]{1,3}", flags = Pattern.Flag.CASE_INSENSITIVE,
			message = "'${validatedValue}' Must be valid source airport code or '*'")
	@NotNull
	@SearchPredicate(type = PredicateType.EQUAL)
	private String src;

	@Pattern(regexp = "^[a-zA-Z*]{1,3}", flags = Pattern.Flag.CASE_INSENSITIVE,
			message = "'${validatedValue}' Must be valid destination airport code or '*'")
	@NotNull
	@SearchPredicate(type = PredicateType.EQUAL)
	private String dest;

	@NotNull
	@SearchPredicate(type = PredicateType.EQUAL)
	private Integer sourceId;

	private boolean enabled;

	public boolean isSrcAndDestAreWild() {
		return StringUtils.equalsIgnoreCase(getSrc(), "*") && StringUtils.equalsIgnoreCase(getDest(), "*");
	}

}
