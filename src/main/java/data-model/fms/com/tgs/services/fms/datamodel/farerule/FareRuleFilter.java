package com.tgs.services.fms.datamodel.farerule;

import java.util.List;

import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.enums.AirType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@ApiModel(value = "This is used to fetch fare rules based on search criteria. You can pass either any one field or combination of fields depending upon your search criteria")
@SuperBuilder
public class FareRuleFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch based on id ", example = "1")
	private Long id;

	@ApiModelProperty(notes = "To fetch based on airType ", example = "DOMESTIC")
	private AirType airType;

	@ApiModelProperty(notes = "To fetch based on airline ", example = "G8")
	private String airline;

	@ApiModelProperty(notes = "To fetch based on source ids ", example = "1")
	private List<String> sourceIds;

	@ApiModelProperty(notes = "To fetch based on fare basis ", example = "TLAS")
	private String fareBasis;

	@ApiModelProperty(notes = "To fetch based on booking class ", example = "R")
	private String bookingClass;

	@ApiModelProperty(notes = "To fetch based on product class ", example = "RS")
	private String productClass;

	@ApiModelProperty(notes = "To fetch based on corporate codes ", example = "O")
	private String corporateCodes;

	@ApiModelProperty(notes = "To fetch based on exit on match", example = "false")
	private Boolean exitOnMatch;

	@ApiModelProperty(notes = "To fetch based on deleted rule", example = "true")
	private boolean isDeleted;
}