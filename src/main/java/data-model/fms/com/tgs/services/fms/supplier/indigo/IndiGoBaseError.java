package com.tgs.services.fms.supplier.indigo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class IndiGoBaseError {
	public IndiGoError indiGoError;
}
