package com.tgs.services.fms.datamodel;

import com.tgs.services.base.datamodel.AirportInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SectorInfo {
	private AirportInfo fromInfo;
	private AirportInfo toInfo;
}
