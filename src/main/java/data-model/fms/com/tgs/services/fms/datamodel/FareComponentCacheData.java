package com.tgs.services.fms.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.tgs.services.base.enums.FareComponent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FareComponentCacheData {

	private List<Map<FareComponent, Double>> segmentFares;

	public List<Map<FareComponent, Double>> getSegmentFares() {
		if (segmentFares == null) {
			segmentFares = new ArrayList<>();
		}
		return segmentFares;
	}

}
