package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.base.datamodel.VoidClean;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListOutput<V> implements IRuleOutPut {

	private List<V> values;

	@SuppressWarnings("unchecked")
	@Override
	public void cleanData() {
		if (CollectionUtils.isNotEmpty(values)) {
			if (VoidClean.class.isAssignableFrom(values.get(0).getClass())) {
				setValues((List<V>) TgsCollectionUtils.getCleanList((List<? extends VoidClean>) values));
			} else {
				setValues(TgsCollectionUtils.getNonNullElements(values));
			}
		}
	}

}
