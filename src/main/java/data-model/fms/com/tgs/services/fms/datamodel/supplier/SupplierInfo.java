package com.tgs.services.fms.datamodel.supplier;

import java.time.LocalDateTime;

import com.tgs.services.base.helper.Exclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SupplierInfo {

	private Long id;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	/**
	 * While creating new supplier there is no need to send id in the request.
	 * Database will automatically create a unique id/supplierId.
	 */

	private SupplierCredential credentialInfo;

	private Boolean enabled;

	private Integer sourceId;

	private String name;

	@Exclude
	private Boolean isDeleted = Boolean.FALSE;

	public String getSupplierId() {
		return name;
	}

}
