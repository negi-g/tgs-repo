package com.tgs.services.trip.datamodel;

import java.util.List;
import java.util.Map;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.PaxType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CorporateTripSearchQuery {
	
	private List<RouteInfo> routeInfos; 
	
	private Map<PaxType, Integer> paxInfo ;
	
	private AirSearchPreferences preferences;
	
	private List<CorporateTravellerInfo> travellerInfos;

	
}
