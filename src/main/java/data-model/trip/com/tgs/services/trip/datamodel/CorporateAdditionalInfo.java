package com.tgs.services.trip.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CorporateAdditionalInfo {
	
	private String company;
	
	private String lineItemId;

}
