package com.tgs.services.trip.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirSearchPreferences {

	private String minSeatingClass;
	private String maxSeatingClass;
	private Boolean bypassCache;
	private String sequenceNo;
	private Boolean isPersonal;
	private Boolean isApproved;

}
