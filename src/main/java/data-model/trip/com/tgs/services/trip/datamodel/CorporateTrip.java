package com.tgs.services.trip.datamodel;

import java.time.LocalDateTime;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import com.tgs.services.base.helper.UserId;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CorporateTrip extends DataModel{
	
	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private String tripId ;
	
	@UserId
	@SearchPredicate(type = PredicateType.IN)
	private String userId;
	
	@SearchPredicate(type = PredicateType.IN)
	private String bookingId;
	
	@SearchPredicate(type = PredicateType.IN)
	private String status;
	
	@SearchPredicate(type = PredicateType.IN)
	private String modificationType;
	
	private CorporateTripSearchQuery tripSearchQuery;
	
	private CorporateAdditionalInfo additionalInfo;
	
	private LocalDateTime createdOn;
	
	@SearchPredicate(type = PredicateType.LTE)
	@SearchPredicate(type = PredicateType.GTE)
	private LocalDateTime processedOn;
	
	@Exclude
	@ForbidInAPIRequest
	@SearchPredicate(type = PredicateType.EQUAL)
	private Boolean isPushed;

}
