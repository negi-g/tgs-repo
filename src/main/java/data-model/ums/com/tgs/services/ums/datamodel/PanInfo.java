package com.tgs.services.ums.datamodel;

import javax.validation.constraints.NotNull;
import org.apache.commons.lang.StringUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PanInfo {
	@NotNull
	@ApiModelProperty(required = true)
	private String number;
	/* Name on pan card */
	@NotNull
	@ApiModelProperty(required = true)
	private String panName;

	public void cleanData() {
		if (StringUtils.isBlank(number))
			number = null;
		if (StringUtils.isBlank(panName))
			panName = null;
	}
}
