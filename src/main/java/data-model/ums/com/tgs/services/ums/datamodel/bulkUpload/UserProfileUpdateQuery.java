package com.tgs.services.ums.datamodel.bulkUpload;

import java.util.Map;
import com.tgs.services.base.datamodel.UserProfile;
import com.tgs.services.ums.datamodel.User;
import lombok.Setter;

@Setter
public class UserProfileUpdateQuery extends UserUpdateQuery {

	Map<String, Object> data;

	private String employeeId;

	private String mobile;

	public User getUserFromRequest() {
		return User.builder().userId(getUserId()).employeeId(employeeId).mobile(mobile)
				.userProfile(UserProfile.builder().data(data).build()).build();
	}
}
