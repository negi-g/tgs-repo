package com.tgs.services.ums.datamodel.bulkUpload;

import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserConfiguration;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DistributorIdUpdate extends UserUpdateQuery {

	private String distributorId;

	public User getUserFromRequest() {
		return User.builder().userId(getUserId())
				.userConf(UserConfiguration.builder().distributorId(getDistributorId()).build()).build();
	}
}
