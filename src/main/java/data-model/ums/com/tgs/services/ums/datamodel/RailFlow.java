package com.tgs.services.ums.datamodel;

import lombok.Getter;

@Getter
public enum RailFlow {

	OTP, DSC

}
