package com.tgs.services.ums.datamodel;

import java.util.List;
import com.tgs.services.base.datamodel.Product;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class RegisterLinkedUserRequest {
	
	public String userId;
	
	public List<Product> allowedProducts;

}
