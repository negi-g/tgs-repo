package com.tgs.services.ums.datamodel;

import javax.validation.constraints.NotNull;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.RailAddressInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailAgentInfo {

	@NotNull
	@SerializedName("an")
	private String agencyName;

	@NotNull
	@SerializedName("cn")
	private String companyName;

	@NotNull
	private String email;

	@NotNull
	@SerializedName("mn")
	private String mobileNo;

	@SerializedName("pan")
	private String panNo;

	@NotNull
	private String dob;

	@NotNull
	@SerializedName("oadd")
	private RailAddressInfo officeAddress;


}
