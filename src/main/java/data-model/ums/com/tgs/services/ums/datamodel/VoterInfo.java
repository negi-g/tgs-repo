package com.tgs.services.ums.datamodel;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VoterInfo {
	
	
	@SerializedName("rt")
	private String RelationType;
	
	@SerializedName("gn")
	private String gender;

	@SerializedName("age")
	private String age;

	@SerializedName("en")
	private String EpicNo;

	@SerializedName("ci")
	private String ClientId;

	@SerializedName("dob")
	private String dob;

	@SerializedName("rn")
	private String RelationName;

	@SerializedName("name")
	private String name;

	@SerializedName("ar")
	private String area;

	@SerializedName("st")
	private String state;

	@SerializedName("hn")
	private String HouseNo;
	
	
}
