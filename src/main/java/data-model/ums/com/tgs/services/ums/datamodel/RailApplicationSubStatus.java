package com.tgs.services.ums.datamodel;

import lombok.Getter;

@Getter
public enum RailApplicationSubStatus {

	PENDING("PEN"),
	PAID("PAID"),

	KYC_INCOMPLETE("KI"),
	KYC_VER_PEND("KVP"),
	KYC_VERIFIED("KV"),
	KYC_HARDCOPY_RECV("KHC"),

	DSC_SUBMITTED("DS"),
	DSC_APPROVED("DA"),

	MR_APPROVED("MA"),
	MR_SUBMITTED("MS"),
	REJECTED("RJ");

	private String code;

	private RailApplicationSubStatus(String code) {
		this.code = code;
	}

	public String getStatus() {
		return this.getCode();
	}

	public static RailApplicationSubStatus getEnumFromCode(String code) {
		for (RailApplicationSubStatus subStatus : RailApplicationSubStatus.values()) {
			if (subStatus.getCode().equals(code)) {
				return subStatus;
			}
		}
		return null;
	}

}
