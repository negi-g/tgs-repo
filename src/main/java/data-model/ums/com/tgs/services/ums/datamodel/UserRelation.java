package com.tgs.services.ums.datamodel;

import java.time.LocalDateTime;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
public class UserRelation extends DataModel {

	@ForbidInAPIRequest
	private Long id;

	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	private String userId1;

	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	private String userId2;

	@ForbidInAPIRequest
	@SearchPredicate(type = PredicateType.IN)
	private String userName1;

	@ForbidInAPIRequest
	@SearchPredicate(type = PredicateType.IN)
	private String userName2;

	@ForbidInAPIRequest
	private LocalDateTime createdOn;

	@ForbidInAPIRequest
	private LocalDateTime processedOn;

	@SearchPredicate(type = PredicateType.EQUAL)
	private Integer depth;

	private Integer priority;

	public Integer getDepth() {
		return depth == null ? 0: depth;
	}

	public Integer getPriority() {
		return priority == null ? 0: priority;
	}

}
