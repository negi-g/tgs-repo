package com.tgs.services.ums.datamodel;

import java.util.Set;
import com.tgs.services.base.enums.UserRole;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AreaRoleMappingInfo {

	private Set<UserRole> userRoleSet;

	private boolean allRoles;

	private boolean midOfficeRoles;

	private Set<String> fields;

	private Set<String> userRoleGroups;

	private Set<UserRole> emulatedRolesSet;
}
