package com.tgs.services.ums.datamodel;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import com.tgs.services.base.datamodel.DocumentInfo;
import com.tgs.services.base.enums.UserRole;
import lombok.Getter;

@Getter
public enum RailApplicationStatus {

	NEW("NEW"),
	PAID("PD"),
	KYC_PENDING("KYCP"),
	KYC_COMPLETE("KYCC"),
	DSC_PENDING("DSCP"),
	MR_PENDING("MRP"),
	ACTIVATION_PENDING("AP"),
	COMPLETE("COMPL"),
	REJECTED("RJ");

	private String code;

	private RailApplicationStatus(String code) {
		this.code = code;
	}

	public String getStatus() {
		return this.getCode();
	}

	public static RailApplicationStatus getEnumFromCode(String code) {
		return getRailApplicationStatus(code);
	}

	public static RailApplicationStatus getRailApplicationStatus(String code) {
		for (RailApplicationStatus status : RailApplicationStatus.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}

	public static List<String> getCodes(List<RailApplicationStatus> RailApplicationStatuses) {
		List<String> statusCodes = new ArrayList<>();
		RailApplicationStatuses.forEach(RailApplicationStatus -> {
			statusCodes.add(RailApplicationStatus.getCode());
		});
		return statusCodes;
	}

	public static RailApplicationStatus getApplicationStatus(RailAdditionalInfo additionalInfo, User user) {
		RailApplicationStatus status = null;

		boolean isConditionSatisfiedTillNow = false;

		if (additionalInfo.getAgentInfo() != null) {
			status = RailApplicationStatus.NEW;
			isConditionSatisfiedTillNow = true;
		}
		if (isConditionSatisfiedTillNow && BooleanUtils.isTrue(additionalInfo.getIsPaymentSuccess())) {
			status = RailApplicationStatus.PAID;
		} else {
			isConditionSatisfiedTillNow = false;
		}
		updateMrAndDscStatus(additionalInfo, user);
		if (isConditionSatisfiedTillNow && CollectionUtils.isNotEmpty(additionalInfo.getDocumentsInfo())) {
			int count = additionalInfo.getDocumentsInfo().size();
			boolean isAllDocsVerified = true;
			for (DocumentInfo documentInfo : additionalInfo.getDocumentsInfo()) {
				if (!BooleanUtils.isTrue(documentInfo.getIsVerified())) {
					isAllDocsVerified = false;
					break;
				}
			}
			if (count >= 6 && isAllDocsVerified) {
				status = RailApplicationStatus.KYC_COMPLETE;
			} else {
				isConditionSatisfiedTillNow = false;
				status = RailApplicationStatus.KYC_PENDING;
			}
			updateKycSubStatus(additionalInfo, count, user, isAllDocsVerified);
		} else {
			isConditionSatisfiedTillNow = false;
		}

		if (isConditionSatisfiedTillNow && additionalInfo.getAllowedRailFlows().contains(RailFlow.DSC)) {
			status = RailApplicationStatus.DSC_PENDING;
			isConditionSatisfiedTillNow = false;
			if (StringUtils.isEmpty(additionalInfo.getDcNumber())) {

				isConditionSatisfiedTillNow = false;
			} else {
				status = RailApplicationStatus.MR_PENDING;
				isConditionSatisfiedTillNow = true;
			}
		}

		if (isConditionSatisfiedTillNow) {
			if (StringUtils.isNotEmpty(additionalInfo.getRegistedImeiNumber())
					|| StringUtils.isNotEmpty(additionalInfo.getRegisterMacId())) {
				if (StringUtils.isNotEmpty(additionalInfo.getRegistedImeiNumber())
						&& !additionalInfo.getRegistedImeiNumber().startsWith("IMEI-")) {
					additionalInfo.setRegistedImeiNumber(
							org.apache.commons.lang3.StringUtils.join("IMEI-", additionalInfo.getRegistedImeiNumber()));

				}
				status = RailApplicationStatus.ACTIVATION_PENDING;
			} else {
				status = RailApplicationStatus.MR_PENDING;
				isConditionSatisfiedTillNow = false;
			}
		}
		return status;

	}

	private static void updateMrAndDscStatus(RailAdditionalInfo additionalInfo, User user) {
		if (isAgentOrAgentStaff(user)) {
			// DSC sub status update
			if (CollectionUtils.isNotEmpty(additionalInfo.getAllowedRailFlows())
					&& additionalInfo.getAllowedRailFlows().contains(RailFlow.DSC)
					&& !RailApplicationSubStatus.DSC_APPROVED.equals(additionalInfo.getDscStatus())) {
				additionalInfo.setDscStatus(
						(StringUtils.isEmpty(additionalInfo.getDcNumber())) ? RailApplicationSubStatus.PENDING
								: RailApplicationSubStatus.DSC_SUBMITTED);
			}
			// MR sub status Update
			if (!RailApplicationSubStatus.MR_APPROVED.equals(additionalInfo.getMrStatus())) {
				additionalInfo.setMrStatus((StringUtils.isNotEmpty(additionalInfo.getRegistedImeiNumber())
						|| StringUtils.isNotEmpty(additionalInfo.getRegisterMacId()))
								? RailApplicationSubStatus.MR_SUBMITTED
								: RailApplicationSubStatus.PENDING);
			}
			if (!RailApplicationSubStatus.KYC_VERIFIED.equals(additionalInfo.getKycStatus())) {
				additionalInfo.setKycStatus(RailApplicationSubStatus.PENDING);
			}

		}
	}

	private static void updateKycSubStatus(RailAdditionalInfo additionalInfo, int count, User user,
			boolean isAllDocsVerified) {
		if (isAgentOrAgentStaff(user)
				&& !(RailApplicationSubStatus.KYC_VERIFIED.equals(additionalInfo.getKycStatus()))) {
			// KYC sub status update
			if (count < 6) {
				additionalInfo.setKycStatus(RailApplicationSubStatus.KYC_INCOMPLETE);
			} else if (!isAllDocsVerified) {
				additionalInfo.setKycStatus(RailApplicationSubStatus.KYC_VER_PEND);
			}
		}

	}

	private static boolean isAgentOrAgentStaff(User user) {
		return (UserRole.AGENT.equals(user.getRole()) || UserRole.AGENT_STAFF.equals(user.getRole()));
	}


}
