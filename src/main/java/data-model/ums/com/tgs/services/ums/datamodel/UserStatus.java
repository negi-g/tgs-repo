package com.tgs.services.ums.datamodel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public enum UserStatus {

	ENABLED("E"), DISABLED("D"), DEACTIVATED("DA");

	private String code;

	private UserStatus(String code) {
		this.code = code;
	}

	public String getStatus() {
		return this.getCode();
	}

	public static UserStatus getEnumFromCode(String code) {
		return getUserStatus(code);
	}

	public static UserStatus getUserStatus(String code) {
		for (UserStatus status : UserStatus.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}

	public static List<String> getCodes(List<UserStatus> userStatuses) {
		List<String> statusCodes = new ArrayList<>();
		userStatuses.forEach(userStatus -> {
			statusCodes.add(userStatus.getCode());
		});
		return statusCodes;
	}
}
