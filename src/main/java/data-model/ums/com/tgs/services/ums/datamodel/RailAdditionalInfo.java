package com.tgs.services.ums.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DocumentInfo;
import com.tgs.services.base.helper.JwtExclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class RailAdditionalInfo {

	private Boolean enabled;

	@JwtExclude
	@SerializedName("obsd")
	private LocalDateTime onboardingStartDate;

	@JwtExclude
	@SerializedName("iid")
	private String irctcAgentId;

	@JwtExclude
	@SerializedName("mac")
	private String registerMacId;

	@JwtExclude
	@SerializedName("imei")
	private String registedImeiNumber;

	@JwtExclude
	@SerializedName("ai")
	private RailAgentInfo agentInfo;

	@JwtExclude
	@SerializedName("di")
	private List<DocumentInfo> documentsInfo;

	@SerializedName("aps")
	@SearchPredicate(type = PredicateType.IN, destinationEntity = "User", dbAttribute = "railAdditionalInfo.aps")
	private RailApplicationStatus applicationStatus;

	@JwtExclude
	@SerializedName("alm")
	private List<RailFlow> allowedRailFlows;

	@JwtExclude
	@SerializedName("ips")
	private Boolean isPaymentSuccess;

	@JwtExclude
	@SerializedName("dcn")
	private String dcNumber;

	@JwtExclude
	@SerializedName("dcsd")
	private String dcStartDate;
	@JwtExclude
	@SerializedName("dced")
	private String dcEndDate;
	@JwtExclude
	@SerializedName("osd")
	private String otpStartDate;
	@JwtExclude
	@SerializedName("oed")
	private String otpEndDate;
	@JwtExclude
	@SerializedName("isd")
	private String issueDate;


	@JwtExclude
	@SerializedName("kycs")
	private RailApplicationSubStatus kycStatus;

	@JwtExclude
	@SerializedName("dscs")
	private RailApplicationSubStatus dscStatus;

	@JwtExclude
	@SerializedName("mrs")
	private RailApplicationSubStatus mrStatus;

	@JwtExclude
	@SerializedName("ops")
	private RailApplicationSubStatus offlinePaymentStatus;


	public RailAgentInfo getAgentInfo() {
		if (agentInfo == null) {
			return new RailAgentInfo();
		}
		return agentInfo;
	}


}
