package com.tgs.services.ums.datamodel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class UserDuesSummary {

	private BigDecimal totalDues;
	private LocalDateTime lockDate;
	private DueInfo currentDueInfo;
	private List<DueInfo> oldDueInfo;

	public BigDecimal getTotalDues() {
		totalDues = BigDecimal.ZERO;
		if (currentDueInfo != null) {
			totalDues = totalDues.add(currentDueInfo.getAmount());
		}
		if (CollectionUtils.isNotEmpty(oldDueInfo)) {
			for (DueInfo dueInfo : oldDueInfo) {
				totalDues = totalDues.add(dueInfo.getAmount());
			}
		}
		return totalDues;
	}

	public void setLockDate(LocalDateTime newLockTime) {
		if (newLockTime == null) {
			return;
		}
		if (lockDate == null) {
			lockDate = newLockTime;
		} else {
			if (lockDate.compareTo(newLockTime) > 0) {
				lockDate = newLockTime;
			}
		}
	}
}
