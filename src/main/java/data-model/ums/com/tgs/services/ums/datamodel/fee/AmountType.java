package com.tgs.services.ums.datamodel.fee;

import lombok.Getter;

@Getter
public enum AmountType {

    FIXED("F"),
    PERCENTAGE("P");

    private String code;

    private AmountType(String code) {
        this.code = code;
    }

    public AmountType getEnumFromCode(String code) {
        return getAmountType(code);
    }

    public static AmountType getAmountType(String code) {
        for (AmountType type : AmountType.values()) {
            if (type.getCode().equals(code) || type.name().equals(code)) {
                return type;
            }
        }
        return null;
    }

}
