package com.tgs.services.ums.datamodel;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;

@Getter
public enum ApplicationArea {

	AMENDMENT {
		@Override
		public Set<AreaRole> getAreaRoles() {
			Set<AreaRole> areaRoles = new HashSet<>();
			areaRoles.add(AreaRole.AMENDMENT_ADMIN);
			areaRoles.add(AreaRole.AMENDMENT_PROCESSOR);
			areaRoles.add(AreaRole.AMENDMENT_REQUESTER);
			return areaRoles;
		}
	},
	CART {
		@Override
		public Set<AreaRole> getAreaRoles() {
			Set<AreaRole> areaRoles = new HashSet<>();
			areaRoles.add(AreaRole.CART_ADMIN);
			areaRoles.add(AreaRole.CART_PROCESSOR);
			areaRoles.add(AreaRole.CART_REQUESTER);
			areaRoles.add(AreaRole.CART_HOTEL_BOOKING_ABORT);
			return areaRoles;
		}
	},
	DEPOSIT_REQUEST {
		@Override
		public Set<AreaRole> getAreaRoles() {
			Set<AreaRole> areaRoles = new HashSet<>();
			areaRoles.add(AreaRole.DEPOSIT_ADMIN);
			areaRoles.add(AreaRole.DEPOSIT_PROCESSOR);
			areaRoles.add(AreaRole.DEPOSIT_REQUESTER);
			return areaRoles;
		}
	},
	EMULATE {
		@Override
		public Set<AreaRole> getAreaRoles() {
			Set<AreaRole> areaRoles = new HashSet<>();
			areaRoles.add(AreaRole.EMULATE_AGENT);
			areaRoles.add(AreaRole.EMULATE_DISTRIBUTOR);
			return areaRoles;
		}
	},
	USER_FIELDS{
		@Override
		public Set<AreaRole> getAreaRoles() {
			Set<AreaRole> areaRoles = new HashSet<>();
			areaRoles.add(AreaRole.FIELDS_AGENT);
			return areaRoles;

		}
	},
	USERS,
	PAYMENT_PASSBOOK,
	COMMERCIAL,
	SUPPLIERS,
	BILLING,
	CREDIT {
		@Override
		public Set<AreaRole> getAreaRoles() {
			Set<AreaRole> areaRoles = new HashSet<>();
			areaRoles.add(AreaRole.CL_EDIT);
			areaRoles.add(AreaRole.CL_EXTENSION);
			areaRoles.add(AreaRole.CP_EDIT);
			return areaRoles;
		}
	},
	PAYMENT {
		@Override
		public Set<AreaRole> getAreaRoles() {
			Set<AreaRole> areaRoles = new HashSet<>();
			areaRoles.add(AreaRole.MISC_PAYMENT);
			areaRoles.add(AreaRole.FUND_PAYMENT);
			areaRoles.add(AreaRole.FUND_RECALL);
			areaRoles.add(AreaRole.FUND_ALLOCATE);
			return areaRoles;
		}
	};

	public Set<AreaRole> getAreaRoles() {
		return new HashSet<>();
	}
}
