package com.tgs.services.ums.datamodel.fee;

import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.SystemError;

import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserFeeAmount implements Validatable, IRuleOutPut {

	private String amountType;
	private Double value;
	private Double maxValue;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		AmountType type = AmountType.getAmountType(amountType);
		if (type == null) {
			errors.put("amountType", SystemError.INVALID_AMOUNT_TYPE.getErrorDetail());
		} else if (value == null) {
			errors.put("value", SystemError.INVALID_VALUE.getErrorDetail());
		} else if (AmountType.PERCENTAGE.equals(type)) {
			if (value > 100) {
				errors.put("value", SystemError.INVALID_PERCENTAGE.getErrorDetail());
			}
		}

		return errors;
	}

}
