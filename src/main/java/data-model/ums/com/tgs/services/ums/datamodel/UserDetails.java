package com.tgs.services.ums.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.UserBalanceSummary;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class UserDetails {

	// @UserId
	public String userId;

	public String username;

	public List<Product> allowedProducts;

	public UserDuesSummary userDuesSummary;

	private LocalDateTime createdOn;

	public UserBalanceSummary userBalanceSummary;

}
