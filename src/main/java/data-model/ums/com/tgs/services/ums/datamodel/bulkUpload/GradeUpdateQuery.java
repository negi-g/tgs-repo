package com.tgs.services.ums.datamodel.bulkUpload;

import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserAdditionalInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GradeUpdateQuery extends UserUpdateQuery{

	private String grade;
	
	public User getUserFromRequest() {
		return User.builder().userId(getUserId())
				.additionalInfo(UserAdditionalInfo.builder().grade(getGrade()).build())
				.build();
	}
}
