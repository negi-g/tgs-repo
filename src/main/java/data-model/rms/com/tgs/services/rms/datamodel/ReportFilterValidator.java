package com.tgs.services.rms.datamodel;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.utils.string.TgsStringUtils;

@Component
public class ReportFilterValidator extends TgsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof ReportFilter) {
			ReportFilter filter = (ReportFilter) target;

			if (filter.getReportType() == null && filter.getQueryId() == null) {
				rejectValue(errors, "queryId", SystemError.INVALID_QUERYID);
			}
			if (filter.getQueryFields() != null) {
				filter.getQueryFields().forEach((k, v) -> {
					String[] values = String.valueOf(v).toLowerCase().split(" ");
					if (TgsStringUtils.hasSQLKeywords(values)) {
						rejectValue(errors, "queryFields", SystemError.INVALID_QUERY_PARAM, k);
					}
				});
			}
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
