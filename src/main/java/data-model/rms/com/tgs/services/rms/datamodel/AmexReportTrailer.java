package com.tgs.services.rms.datamodel;

import com.tgs.services.base.helper.ReportParam;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class AmexReportTrailer {

	@ReportParam(beginIndex = 1, length = 4)
	private String recordType;

	@ReportParam(beginIndex = 5, length = 6)
	private String createDate;

	@ReportParam(beginIndex = 11, length = 6, isNumeric = true, isFiller = true)
	private String nonAirCount;

	@ReportParam(beginIndex = 17, length = 6, isNumeric = true)
	private String airCount;

	@ReportParam(beginIndex = 23, length = 796, isFiller = true)
	private String filler;
}
