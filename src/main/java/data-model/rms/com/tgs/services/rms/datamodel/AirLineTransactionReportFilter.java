package com.tgs.services.rms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AirLineTransactionReportFilter extends ReportFilter {

	/**
	 * It should be null , booking, amendment;
	 */
	private String selectionType;
	

}
