package com.tgs.services.rms.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.DefaultReportField;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(toBuilder = true)
public class RailTransactionResponse {

	// Product name : AIRLINE
	// Trip Type : Domestic/international
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Id")
	private String bookingId;

	@SerializedName("Pax")
	@DefaultReportField(nonMidOfficeRole = true)
	private String paxType;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Date")
	private String bookingDate;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Time")
	private String bookingTime;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Cart Generation Date")
	private String cartGenerationDate;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Cart Generation Time")
	private String cartGenerationTime;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Destination")
	private String departureStation;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Arrival")
	private String arrivalStation;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Boarding Station")
	private String boardingStation;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Booking User Name")
	private String loggedInUserName;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Booking Employee Name")
	private String bookingEmployeeName;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Departure Date")
	private String departureDate;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Arrival Date")
	private String arrivalDate;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Departure Time")
	private String departureTime;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Arrival Time")
	private String arrivalTime;

	@DefaultReportField
	@SerializedName("Reseller Id")
	private String bookingUserId;

	@DefaultReportField
	@SerializedName("Reseller Grade")
	private String bookingUserGrade;

	@DefaultReportField
	@SerializedName("Reseller Name")
	private String bookingUserName;

	@DefaultReportField
	@SerializedName("Sales Hierarchy")
	private String salesHierachy;

	@DefaultReportField
	@SerializedName("Reseller Email")
	private String bookingUserEmail;

	@DefaultReportField
	@SerializedName("Reseller City")
	private String bookingUserCity;

	@DefaultReportField
	@SerializedName("Reseller State")
	private String bookingUserState;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Reseller Account Code")
	private String bookingUserAccountcode;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Description")
	private String description;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Old Boarding Station")
	private String oldBoardingStation;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Payment Medium")
	private String paymentMedium;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Payment Status")
	private String paymentStatus;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Amendment Id")
	private String amendmentId;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Amendment Type")
	private String amendmentType;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Amendment Date")
	private String amendmentDate;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Train Name")
	private String trainName;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Train Number")
	private String trainNo;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Days to travel")
	private long days;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Type")
	private String bookingType;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Status")
	private String cartStatus;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Status")
	private String bookingStatus;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Current Status")
	private String currentStatus;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Invoice Status")
	private String invoiceStatus;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("System invoice id")
	private String systemInvoiceId;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Third party invoice id")
	private String thirdPartyInvoiceId;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Third party invoice status")
	private String thirdPartyInvoiceStatus;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Passenger Name")
	private String name;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Last modified User id")
	private String assignedUserId;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Last modified User name")
	private String assignedUserName;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("IRCTC Pnr")
	private String pnr;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("IRCTC Reference No")
	private String referenceNo;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Train Fare")
	private Double trainFare;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("IRCTC Charges")
	private Double irctcCharges;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("PG Charges")
	private Double pgCharges;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Agent Charges")
	private Double mf;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Travel Insurance Charges")
	private Double ti;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Displayed PG Charges")
	private Double displayedPgCharges;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Displayed Agent Charges")
	private Double dispalyedMf;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Customer Gross Fare")
	private Double grossFare;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Net Fare")
	private Double netFare;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Class")
	private String bookingClass;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Quota")
	private String bookingQuota;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Reseller Amendment Fee")
	private Double raf;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Rail Cancellation Fee")
	private Double rcf;


}

