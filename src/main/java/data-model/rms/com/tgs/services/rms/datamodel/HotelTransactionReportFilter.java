package com.tgs.services.rms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class HotelTransactionReportFilter extends ReportFilter { 

	private String selectionType;

}
