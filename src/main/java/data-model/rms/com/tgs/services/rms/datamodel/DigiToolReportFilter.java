package com.tgs.services.rms.datamodel;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class DigiToolReportFilter extends ReportFilter {

	@ApiModelProperty(notes = "To fetch report based on airline type")
	private List<String> airlineCodes;
	
}
