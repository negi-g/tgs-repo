package com.tgs.services.rms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
public class DSRFilter extends ReportFilter {

    private String selectionType;
}
