package com.tgs.services.rms.datamodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.tgs.services.base.enums.CabinClass;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AirBookingTransactionReportFilter extends ReportFilter {

	@ApiModelProperty(notes = "To fetch report based on departure date. For example if you want to fetch airlines departed after 25th May 2018.", example = "2018-05-25")
	private LocalDate departedOnAfterDate;
	@ApiModelProperty(notes = "To fetch report based on departure date. For example if you want to fetch airlines departed before 25th May 2018", example = "2018-05-25")
	private LocalDate departedOnBeforeDate;
	@ApiModelProperty(notes = "To fetch report based on booking date. For example if you want to fetch airlines booked before  25th May 2018 12:50", example = "2018-05-25T12:50")
	private LocalDateTime bookedOnBeforeDateTime;
	@ApiModelProperty(notes = "To fetch report based on booking date. For example if you want to fetch airlines booked after 25th May 2018 12:50", example = "2018-05-25T12:50")
	private LocalDateTime bookedOnAfterDateTime;
	@ApiModelProperty(notes = "To fetch report based on airline code ", example = "6E")
	private List<String> airlineCodes;
	@ApiModelProperty(notes = "To fetch report based on cabin class ")
	private CabinClass cabinClass;
	@ApiModelProperty(notes = "To fetch payment info in reports ")
	private Boolean fetchPayments;
	@ApiModelProperty(notes = "To fetch amendment info in reports ")
	private Boolean fetchAmendments;

}
