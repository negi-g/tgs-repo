package com.tgs.services.oms.datamodel;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public enum OrderFlowType {

	MANUAL_ORDER("M"),
	IMPORT_PNR("I"),
	REISSUED("RE"),
	ONLINE("O"),
	HOLD_CONFIRM("HC"),
	HOLD_ABORT("HA"),
	PNR_RELEASED("PR");

	public String getName() {
		return this.name();
	}

	private String code;

	private OrderFlowType(String code) {
		this.code = code;
	}

	public static OrderFlowType getEnumFromCode(String code) {
		return getOrderFlowType(code);
	}

	public static OrderFlowType getOrderFlowType(String code) {
		for (OrderFlowType flowType : OrderFlowType.values()) {
			if (flowType.getCode().equals(code)) {
				return flowType;
			}
		}
		return null;
	}

	public static List<String> getCodes(List<OrderFlowType> types) {
		List<String> orderCodes = new ArrayList<>();
		types.forEach(type -> {
			orderCodes.add(type.getCode());
		});
		return orderCodes;
	}
}
