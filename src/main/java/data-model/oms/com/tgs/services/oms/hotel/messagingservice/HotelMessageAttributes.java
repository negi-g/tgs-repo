package com.tgs.services.oms.hotel.messagingservice;

import java.time.LocalDate;
import java.util.List;
import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.hms.datamodel.HotelInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class HotelMessageAttributes extends EmailAttributes {

	private HotelInfo hInfo;
	private List<HotelMessageRoomInfo> roomInfoList;
	private List<String> cancellationPolicyList;
	private long cancellationDeadlinesHours;
	private String bookingId;
	private String bookingRefNo;
	private String bookingStatus;
	private String confirmedBy;
	private String userName;
	private LocalDate confirmationDate;
	private LocalDate checkInDate;
	private LocalDate checkOutDate;
	private long nights;
	private HotelMessagePriceInfo priceInfo;
	private String ssr;
	private String supplierName;
	private String disputeReason;
}
