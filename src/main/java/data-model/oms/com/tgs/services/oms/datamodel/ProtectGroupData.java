package com.tgs.services.oms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProtectGroupData {

	private String firstName;
	private String lastName;
	private String referenceId;
	private double orderAmount;
	private String insuranceEndDate;
	private String vendorSaleDate;
	private Boolean sold;
}
