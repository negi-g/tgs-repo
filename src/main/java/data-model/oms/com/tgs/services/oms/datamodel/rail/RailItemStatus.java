package com.tgs.services.oms.datamodel.rail;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.oms.datamodel.OrderStatus;
import lombok.Getter;

@Getter
public enum RailItemStatus {

	IN_PROGRESS("IP", OrderStatus.IN_PROGRESS) {
		@Override
		public Set<RailItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(PAYMENT_FAILED, PAYMENT_SUCCESS, ABORTED));
		}
	},

	PAYMENT_SUCCESS("PS", OrderStatus.PAYMENT_SUCCESS) {
		@Override
		public Set<RailItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS, ABORTED));
		}

	},

	SUCCESS("S", OrderStatus.SUCCESS) {
		@Override
		public Set<RailItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(CANCELLED));
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(Arrays.asList(AmendmentType.values()));
		}
	},
	PENDING("P", OrderStatus.PENDING) {
		@Override
		public Set<RailItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS));
		}
	},

	PAYMENT_FAILED("PF", OrderStatus.FAILED) {
		@Override
		public Set<RailItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(ABORTED));
		}
	},

	FAILED("F", OrderStatus.FAILED) {
		@Override
		public Set<RailItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	},

	ABORTED("A", OrderStatus.ABORTED) {
		@Override
		public Set<RailItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	},
	CANCELLED("C", OrderStatus.CANCELLED) {
		@Override
		public Set<RailItemStatus> nextStatusSet() {
			return new HashSet<>();
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(Arrays.asList(AmendmentType.CORRECTION));
		}
	},
	UNCONFIRMED("UC", OrderStatus.UNCONFIRMED) {
		@Override
		public Set<RailItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	},
	CANCELLATION_PENDING("CP", OrderStatus.CANCELLATION_PENDING) {
		@Override
		public Set<RailItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(CANCELLED, UNCONFIRMED));
		}
	};

	public String getStatus() {
		return this.getCode();
	}

	private String code;
	private OrderStatus orderStatus;

	RailItemStatus(String code, OrderStatus orderStatus) {
		this.code = code;
		this.orderStatus = orderStatus;
	}

	public static RailItemStatus getRailItemStatus(String code) {
		for (RailItemStatus role : values()) {
			if (role.getCode().equals(code)) {
				return role;
			}
		}
		return null;
	}

	public abstract Set<RailItemStatus> nextStatusSet();

	public Set<AmendmentType> validAmdTypes() {
		return new HashSet<>();
	}

	public static RailItemStatus getStatus(String status) {
		for (RailItemStatus role : values()) {
			if (role.getOrderStatus().getCode().equals(status)) {
				return role;
			}
		}
		return null;
	}

	public static RailItemStatus getEnumFromCode(String code) {
		return getRailItemStatus(code);
	}

}
