package com.tgs.services.oms.datamodel.rail;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class AdditionalInfoFilter {

	private String trainName;

	private String trainNo;

	private String pnr;
}
