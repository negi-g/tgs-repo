package com.tgs.services.oms.datamodel.misc;

import com.tgs.services.oms.datamodel.ItemDetails;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class MiscOrderDetails implements ItemDetails {

	private AdditionalMiscOrderItemInfo additionalInfo;

	private MiscPriceInfo priceInfo;
}
