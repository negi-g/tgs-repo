package com.tgs.services.oms.datamodel;

import lombok.Getter;

@Getter
public enum ReconciliationStatus {

	NORMAL("N"),
	MEDIUM("M"),
	CRITICAL("C");
	
	public String getName() {
		return this.name();
	}

	private String code;

	private ReconciliationStatus(String code) {
		this.code = code;
	}

	public static ReconciliationStatus getEnumFromCode(String code) {
		return getReconciliationStatus(code);
	}
	
	public static ReconciliationStatus getReconciliationStatus(String code) {
		for (ReconciliationStatus reconciliationStatus : ReconciliationStatus.values()) {
			if (reconciliationStatus.getCode().equals(code)) {
				return reconciliationStatus;
			}
		}
		return null;
	}
}