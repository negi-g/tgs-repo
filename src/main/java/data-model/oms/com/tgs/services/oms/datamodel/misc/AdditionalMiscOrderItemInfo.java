package com.tgs.services.oms.datamodel.misc;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AdditionalMiscOrderItemInfo {

	@SerializedName("des")
	private String description;

}
