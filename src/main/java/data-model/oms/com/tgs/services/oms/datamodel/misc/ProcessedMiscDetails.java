package com.tgs.services.oms.datamodel.misc;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.oms.datamodel.ProcessedItemDetails;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProcessedMiscDetails implements ProcessedItemDetails {

	@SerializedName("ainfo")
	private AdditionalMiscOrderItemInfo additionalInfo;

	@SerializedName("pi")
	private MiscPriceInfo priceInfo;
}
