package com.tgs.services.oms.datamodel.rail;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.oms.datamodel.ItemDetails;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class RailOrderDetails implements ItemDetails {

	private RailJourneyInfo journeyInfo;
	private List<RailTravellerInfo> travellerInfos;
	private RailPriceInfo priceInfo;
	@SerializedName("obs")
	private RailStationInfo oldBoardingStation;
	@SerializedName("vos")
	private Boolean vikalpOptStatus;

}
