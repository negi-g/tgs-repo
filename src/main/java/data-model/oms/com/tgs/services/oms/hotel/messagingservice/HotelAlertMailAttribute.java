package com.tgs.services.oms.hotel.messagingservice;

import java.util.List;
import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.hms.datamodel.HotelStaticDataChangeAlert;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class HotelAlertMailAttribute extends EmailAttributes {

	private String supplierName;
	private String unicaId;
	private String bookingId;
	private String message;
	private String alertSubject;
	private String mailHeadline;
	private String fromEmail;
	private List<HotelStaticDataChangeAlert> staticDataList;
	private HotelMessagePriceInfo priceInfo;

}
