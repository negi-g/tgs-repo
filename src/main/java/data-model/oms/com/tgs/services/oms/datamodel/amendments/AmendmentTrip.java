package com.tgs.services.oms.datamodel.amendments;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.helper.SystemError;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


@Builder
@Getter
@Setter
public class AmendmentTrip implements Validatable {

	@SerializedName("src")
	private String source;

	@SerializedName("dest")
	private String destination;

	private LocalDate departureDate;

	// applicable if request is for process amendment.
	private List<AmendmentTraveller> travellers;

	// applicable in case of Re-issue amemdment
	@SerializedName("ntd")
	private LocalDateTime nextTravelDate;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (StringUtils.isBlank(getSource())) {
			errors.put("source", SystemError.SOURCE_AIRPORT.getErrorDetail());
		}
		if (StringUtils.isBlank(getDestination())) {
			errors.put("destination", SystemError.DEST_AIRPORT.getErrorDetail());
		}
		if (departureDate == null) {
			errors.put("departureDate", SystemError.DEPARTURE_DATE.getErrorDetail());
		}
		if (validatingData instanceof AmendmentTripValidatingData) {
			AmendmentTripValidatingData data = (AmendmentTripValidatingData) validatingData;
			if (AmendmentType.REISSUE.equals(data.getAmendmentType())) {
				if (Objects.isNull(nextTravelDate)) {
					errors.put("nextTravelDate", SystemError.NEW_TRAVEL_DATE_REQ.getErrorDetail());
				}
			}
		}
		if (CollectionUtils.isNotEmpty(getTravellers())) {
			int index = 0;
			for (AmendmentTraveller traveller : getTravellers()) {
				errors.putAll(
						traveller.validate(validatingData).withPrefixToFieldNames("travellers[" + index++ + "]."));
			}
		}

		return errors;
	}
}
