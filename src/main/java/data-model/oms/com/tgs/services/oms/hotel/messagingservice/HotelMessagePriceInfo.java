package com.tgs.services.oms.hotel.messagingservice;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelMessagePriceInfo {
	
	private String basePrice;
	private String totalTax;
	private String totalAmount;
	private String discount;
}
