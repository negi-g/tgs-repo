package com.tgs.services.oms.datamodel.rail;

import java.time.LocalDateTime;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.rail.datamodel.RailInfo;
import com.tgs.services.rail.datamodel.RailStationInfo;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ProcessedJourneyInfo {

	@SerializedName("ri")
	private RailInfo railInfo;

	@SerializedName("du")
	private Long duration;

	@SerializedName("di")
	private Integer distance;

	@SerializedName("dt")
	private LocalDateTime departureTime;

	@SerializedName("at")
	private LocalDateTime arrivalTime;

	@SerializedName("ds")
	private RailStationInfo departStationInfo;

	@SerializedName("as")
	private RailStationInfo arrivalStaionInfo;

	@SerializedName("quota")
	private String bookingQuota;

	@SerializedName("otp")
	private Boolean isOtpBased;

	@SerializedName("pnr")
	private String pnr;
}
