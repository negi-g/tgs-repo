package com.tgs.services.oms.datamodel.amendments;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.Airline;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.oms.datamodel.PaxKey;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirAdditionalInfo {

	public AirAdditionalInfo() {
		paxKeys = new HashMap<>();
	}

	@SerializedName("fdep")
	// @SearchPredicate(type = PredicateType.GTE, dbAttribute = "fdep")
	// @SearchPredicate(type = PredicateType.LTE, dbAttribute = "fdep")
	private LocalDateTime firstDeparture;

	@SerializedName("cad")
	private LocalDateTime closestActionDate;

	/**
	 * Applicable in case of reissue
	 */
	@SerializedName("ntd")
	private LocalDate nextTravelDate;

	@SerializedName("intl")
	// @SearchPredicate(type = PredicateType.EQUAL, dbAttribute = "intl")
	private boolean international;

	@Airline
	@SerializedName("acode")
	private Set<String> airlineCodes;

	@SerializedName("pwsR")
	private Set<PWSReason> pwsReasons;

	@SerializedName("pwsuid")
	@UserId
	private String pwsUserId;

	@SerializedName("pwsET")
	private LocalDateTime pwsEnterTime;

	private Set<Integer> sourceIds;

	private Set<String> suppliers;

	@SerializedName("tCfee")
	private Double totalCancellationFee;

	@SerializedName("tRfee")
	private Double totalReIssueFee;

	@SerializedName("pssh")
	private List<AirOrderItem> orderPreviousSnapshot;

	@SerializedName("nssh")
	private List<AirOrderItem> orderNextSnapshot;

	private Map<String, Set<PaxKey>> paxKeys;

	@DBExclude
	@Exclude
	private Map<Long, Set<Long>> keyMap;

	public Set<Integer> getSourceIds() {
		if (sourceIds == null)
			sourceIds = new HashSet<>();
		return sourceIds;
	}

	public Set<String> getSuppliers() {
		if (suppliers == null)
			suppliers = new HashSet<>();
		return suppliers;
	}

	public Map<Long, Set<Long>> getKeyMap() {
		if (keyMap != null)
			return keyMap;
		Map<Long, Set<Long>> map = new HashMap<>();
		paxKeys.forEach((key, val) -> {
			Long sId = Long.valueOf(key);
			val.forEach(p -> {
				if (!map.containsKey(sId))
					map.put(sId, new HashSet<>());
				map.get(sId).add(p.getId());
			});
		});
		keyMap = map;
		return map;
	}

	public static Map<Long, Set<Long>> mergeKeyMap(Map<Long, Set<Long>> map1, Map<Long, Set<Long>> map2) {
		map2.forEach((k, set) -> {
			if (!map1.containsKey(k))
				map1.put(k, new HashSet<>());
			map1.get(k).addAll(set);
		});
		return map1;
	}

	public boolean amdContainsPax(Long segId, Long paxId) {
		getKeyMap();
		return keyMap.containsKey(segId) && keyMap.get(segId).contains(paxId);
	}

	public static boolean amdContainsPax(Map<Long, Set<Long>> amdKeyMap, Long segId, Long paxId) {
		return amdKeyMap.containsKey(segId) && amdKeyMap.get(segId).contains(paxId);
	}

	public boolean amdContainsSegment(Long segId) {
		getKeyMap();
		return keyMap.containsKey(segId);
	}

}
