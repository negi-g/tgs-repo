package com.tgs.services.oms.datamodel.hotel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessedRoomInfo {

	private String id;
	
	@SerializedName("mb")
	private String mealBasis;
	
	@SerializedName("rt")
	private String roomType;
}
