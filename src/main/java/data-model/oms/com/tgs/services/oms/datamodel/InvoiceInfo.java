package com.tgs.services.oms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class InvoiceInfo {

	private String name;

	private String email;

	private String phone;

	private String address;

	private String city;

	private String state;

	private String pincode;

	private String gstNumber;

	private String vatNumber;

	private String crNumber;
}
