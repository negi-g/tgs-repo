package com.tgs.services.oms.datamodel.amendments;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDate;


@Getter
@Setter
public class AmendmentTraveller implements Validatable {


	@SerializedName("fn")
	private String firstName;

	@SerializedName("ln")
	private String lastName;

	private LocalDate dob;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		return errors;
	}
}
