package com.tgs.services.oms.datamodel.rail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TravellerInfoFilter {

	@ApiModelProperty(notes = "To fetch booking based on traveller's first name", example = "Neha")
	private String passengerName;

}
