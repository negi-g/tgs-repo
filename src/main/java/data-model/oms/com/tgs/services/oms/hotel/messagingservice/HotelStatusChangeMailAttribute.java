package com.tgs.services.oms.hotel.messagingservice;

import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class HotelStatusChangeMailAttribute extends EmailAttributes {
	
	private String supplierName;
	private String bookingId;
	private String reason;
	private String oldValue;
	private String newValue;
	
}
