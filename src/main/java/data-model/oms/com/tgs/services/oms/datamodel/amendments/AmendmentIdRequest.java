package com.tgs.services.oms.datamodel.amendments;

import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

@Getter
public class AmendmentIdRequest {

    private List<String> amdIds;

    private LocalDate date;
}
