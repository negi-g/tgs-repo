package com.tgs.services.oms.datamodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tgs.services.pms.datamodel.Payment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProcessedBookingDetail {

	private ProcessedOrder order;

	private Map<String, ProcessedItemDetails> items;

	private List<Payment> payments;
	

	public Map<String, ProcessedItemDetails> getItems() {
		if (items == null) {
			items = new HashMap<>();
		}
		return items;
	}

}
