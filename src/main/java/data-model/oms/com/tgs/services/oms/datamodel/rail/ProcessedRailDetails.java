package com.tgs.services.oms.datamodel.rail;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.oms.datamodel.ProcessedItemDetails;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProcessedRailDetails implements ProcessedItemDetails {

	@SerializedName("tis")
	List<RailTravellerInfo> travellerInfo;

	@SerializedName("ji")
	private ProcessedJourneyInfo journeyInfo;
}
