package com.tgs.services.oms.datamodel.rail;

import java.time.LocalDate;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.oms.datamodel.OrderItemFilter;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class RailOrderItemFilter extends QueryFilter implements OrderItemFilter {

	private TravellerInfoFilter travellerInfoFilter;

	private AdditionalInfoFilter additionalInfoFilter;

	private LocalDate departedOnAfterDate;

	private LocalDate departedOnBeforeDate;
}
