package com.tgs.services.oms.datamodel.amendments;

public enum AmendmentAction {

    ASSIGN,
    ASSIGNME,
    PROCESS,
    PENDING_WITH_SUPPLIER {
        @Override
        public boolean showOnProcess() {
            return true;
        }
    },

    ABORT {
        @Override
        public boolean showOnProcess() {
            return true;
        }
    },
    MERGE {
        @Override
        public boolean showOnProcess() {
            return true;
        }
    },
    VIEW;

    public boolean showOnProcess() {
        return false;
    }
}
