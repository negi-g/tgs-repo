package com.tgs.services.oms.datamodel.air;

import java.time.LocalDate;
import java.util.List;

import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.oms.datamodel.OrderItemFilter;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AirOrderItemFilter extends QueryFilter implements OrderItemFilter {

	private TravellerInfoFilter travellerInfoFilter;
	private AdditionalInfoFilter additionalInfoFilter;
	@ApiModelProperty(notes = "To fetch booking according to the airline codes", allowableValues = "AI")
	private List<String> airlines;
	@ApiModelProperty(notes = "To fetch records based on departure date. For example if you want to fetch airlines departed after 25th May 2018.", example = "2018-05-25")
	private LocalDate departedOnAfterDate;
	@ApiModelProperty(notes = "To fetch records based on departure date. For example if you want to fetch airlines departed before 25th May 2018", example = "2018-05-25")
	private LocalDate departedOnBeforeDate;
	@ApiModelProperty(notes = "To fetch booking according to the suppliers")
	private List<String> supplierIds;
}
