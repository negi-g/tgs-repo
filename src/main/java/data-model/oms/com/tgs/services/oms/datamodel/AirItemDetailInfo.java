package com.tgs.services.oms.datamodel;

import com.tgs.services.fms.datamodel.SegmentInfo;
import lombok.Getter;
import lombok.Setter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class AirItemDetailInfo {

	public AirItemDetailInfo() {
		this.profileData = new HashMap<>();
	}

	private String reason;

	private Map<Long, List<SegmentInfo>> lff;

	private Map<String, Map<String, Object>> profileData;

	public Map<String, Map<String, Object>> getProfileData() {
		if (profileData == null) {
			profileData = new HashMap<>();
		}
		return profileData;
	}

	public Map<Long, List<SegmentInfo>> getLff() {
		if (lff == null) {
			lff = new HashMap<>();
		}
		return lff;
	}

}
