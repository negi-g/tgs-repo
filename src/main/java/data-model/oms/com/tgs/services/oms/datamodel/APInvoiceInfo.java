package com.tgs.services.oms.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APInvoiceInfo {

	@SerializedName("iid")
	private String invoiceId;

	@SerializedName("ips")
	private boolean invoicePushStatus;
}
