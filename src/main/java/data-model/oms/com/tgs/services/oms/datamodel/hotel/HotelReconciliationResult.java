package com.tgs.services.oms.datamodel.hotel;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class HotelReconciliationResult {

	private String fieldName;
	private Object newValue;
	private Object oldValue;
}
