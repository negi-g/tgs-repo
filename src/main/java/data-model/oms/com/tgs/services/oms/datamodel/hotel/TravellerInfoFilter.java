package com.tgs.services.oms.datamodel.hotel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravellerInfoFilter {

	@ApiModelProperty(notes = "To fetch booking based on traveller's first name", example = "Anshul")
	private String travellerFirstName;
	@ApiModelProperty(notes = "To fetch booking based on traveller's last name", example = "Goyal")
	private String travellerLastName;
	@ApiModelProperty(notes = "To fetch booking based on Traveller's booking reference" , example = "TG0100381")
	private String bookingReference;
	
}
