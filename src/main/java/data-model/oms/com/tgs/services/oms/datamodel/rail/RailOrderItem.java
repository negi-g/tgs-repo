package com.tgs.services.oms.datamodel.rail;

import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.datamodel.booking.AdditionalRailOrderItemInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class RailOrderItem extends DataModel {

	private List<RailTravellerInfo> travellerInfo;
	private String status;
	private String bookingId;
	private String fromStn;
	private String toStn;
	private LocalDateTime departureTime;
	private LocalDateTime arrivalTime;
	private Double amount;
	private AdditionalRailOrderItemInfo additionalInfo;
	private LocalDateTime createdOn;
	private LocalDateTime processedOn;
	private Long id;
	private String pnr;

}
