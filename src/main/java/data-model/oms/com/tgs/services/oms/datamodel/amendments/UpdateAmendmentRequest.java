package com.tgs.services.oms.datamodel.amendments;

import lombok.Getter;
import lombok.Setter;
import java.util.Set;

@Getter
@Setter
public class UpdateAmendmentRequest {

    private String amendmentId;
    private AmendmentStatus status;
    private String assignedUserId;
    private String finishingNotes;
    private Set<PWSReason> pwsReasons;

}
