package com.tgs.services.oms.datamodel;

import java.time.LocalDateTime;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class OrderDisputeInfo {

	@SerializedName("ds")
	private OrderDisputeStatus disputeStatus;

	@SerializedName("rsn")
	private String reason;

	@SerializedName("drat")
	private LocalDateTime disputeRaiseTime;

	@SerializedName("drst")
	private LocalDateTime disputeResolveTime;
}
