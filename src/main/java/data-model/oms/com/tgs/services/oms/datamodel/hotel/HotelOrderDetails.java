package com.tgs.services.oms.datamodel.hotel;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.oms.datamodel.ItemDetails;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelOrderDetails implements ItemDetails {


	private HotelInfo hInfo;
	
	private HotelSearchQuery query;

}
