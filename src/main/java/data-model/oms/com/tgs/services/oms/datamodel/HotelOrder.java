package com.tgs.services.oms.datamodel;

import java.util.List;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelOrder {

	private List<HotelOrderItem> items;
	private Order order;
	
}
