package com.tgs.services.oms.datamodel.misc;

import java.time.LocalDateTime;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.oms.datamodel.OrderItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class MiscOrderItem extends DataModel implements OrderItem {

	private String bookingId;

	private String status;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	private AdditionalMiscOrderItemInfo additionalInfo;

	private MiscPriceInfo priceInfo;

}
