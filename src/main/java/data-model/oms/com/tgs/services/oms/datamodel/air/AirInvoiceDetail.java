package com.tgs.services.oms.datamodel.air;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirInvoiceDetail {

	private FlightTravellerInfo travellerInfo;

	private List<String> sectors;

	private Set<String> ticketNumber;

	private Set<String> flightNumber;

	private Set<String> pnr;

	private AmendmentType amendmentType;

	private String AmendmentId;

	private Double nonRefundableAmount;

	private Set<String> classOfBooking;

	public List<String> getSectors() {
		if (sectors == null) {
			sectors = new ArrayList<>();
		}
		return sectors;
	}

	public Set<String> getTicketNumber() {
		if (ticketNumber == null) {
			ticketNumber = new LinkedHashSet<>();
		}
		return ticketNumber;
	}

	public Set<String> getFlightNumber() {
		if (flightNumber == null) {
			flightNumber = new LinkedHashSet<>();
		}
		return flightNumber;
	}

	public Set<String> getPnr() {
		if (pnr == null) {
			pnr = new LinkedHashSet<>();
		}
		return pnr;
	}

	public Set<String> getClassOfBooking() {
		if (classOfBooking == null) {
			classOfBooking = new LinkedHashSet<>();
		}
		return classOfBooking;
	}

}
