package com.tgs.services.oms.datamodel.misc;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import com.tgs.services.oms.datamodel.OrderStatus;
import lombok.Getter;

@Getter
public enum MiscItemStatus {


	IN_PROGRESS("IP", OrderStatus.IN_PROGRESS) {
		@Override
		public Set<MiscItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(PAYMENT_FAILED, PAYMENT_SUCCESS));
		}
	},

	PAYMENT_SUCCESS("PS", OrderStatus.PAYMENT_SUCCESS) {
		@Override
		public Set<MiscItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS));
		}

	},

	SUCCESS("S", OrderStatus.SUCCESS) {
		@Override
		public Set<MiscItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	},
	PENDING("P", OrderStatus.PENDING) {
		@Override
		public Set<MiscItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS));
		}
	},

	PAYMENT_FAILED("PF", OrderStatus.FAILED) {
		@Override
		public Set<MiscItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	},

	FAILED("F", OrderStatus.FAILED) {
		@Override
		public Set<MiscItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	};

	public String getStatus() {
		return this.getCode();
	}

	private String code;
	private OrderStatus orderStatus;

	MiscItemStatus(String code, OrderStatus orderStatus) {
		this.code = code;
		this.orderStatus = orderStatus;
	}

	public static MiscItemStatus getMiscItemStatus(String code) {
		for (MiscItemStatus role : values()) {
			if (role.getCode().equals(code)) {
				return role;
			}
		}
		return null;
	}

	public abstract Set<MiscItemStatus> nextStatusSet();

	public static MiscItemStatus getStatus(String status) {
		for (MiscItemStatus role : values()) {
			if (role.getOrderStatus().getCode().equals(status)) {
				return role;
			}
		}
		return null;
	}

	public static MiscItemStatus getEnumFromCode(String code) {
		return getMiscItemStatus(code);
	}


}
