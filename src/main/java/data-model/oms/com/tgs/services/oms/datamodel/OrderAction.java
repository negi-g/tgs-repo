package com.tgs.services.oms.datamodel;

import lombok.Getter;

@Getter
public enum OrderAction {

	ASSIGN, ASSIGNME, ABORT, RESYNC, MODIFY;

}
