package com.tgs.services.oms.datamodel.amendments;

import java.util.HashSet;
import java.util.Set;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.rail.datamodel.RailTdrInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailAdditionalInfo {

	public RailAdditionalInfo() {
		paxKeys = new HashSet<String>();
	}

	@SerializedName("pssh")
	private RailOrderItem orderPreviousSnapshot;

	@SerializedName("nssh")
	private RailOrderItem orderCurrentSnapshot;

	private Set<String> paxKeys;

	@SerializedName("canid")
	private String cancellationId;

	@SerializedName("aamt")
	private Double amendmentAmount;

	private RailTdrInfo tdrInfo;

	@SerializedName("iwo")
	private Boolean isWithoutOtp;

	@SearchPredicate(type = PredicateType.EQUAL, destinationEntity = "Amendment",
			dbAttribute = "additionalInfo.radi.pnr")
	private String pnr;


}
