package com.tgs.services.oms.datamodel.amendments;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.oms.datamodel.UserAssignment;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AmendmentAdditionalInfo {

	public AmendmentAdditionalInfo() {
		airAdditionalInfo = new AirAdditionalInfo();
		hotelAdditionalInfo = new HotelAdditionalInfo();
		railAdditionalInfo = new RailAdditionalInfo();
		userAssignments = new ArrayList<>();
	}

	@SerializedName("aadi")
	private AirAdditionalInfo airAdditionalInfo;

	@SerializedName("radi")
	private RailAdditionalInfo railAdditionalInfo;

	@SerializedName("uAss")
	private List<UserAssignment> userAssignments;

	private String agentRemarks;

	@SerializedName("fn")
	private String finishingNotes;

	@SerializedName("rIds")
	private Set<Short> raiseChecklistIds;

	@SerializedName("pIds")
	private Set<Short> processChecklistIds;

	@SerializedName("rec")
	private boolean recallCommission;

	private boolean returnTds;

	@SerializedName("aafr")
	private Double amountApplicableForRefund;

	@SerializedName("CRec")
	private Double totalCommissionRecalled;

	@SerializedName("pmrec")
	private Double totalPartnerMarkupRecalled;

	@SerializedName("pcrec")
	private Double totalPartnerCommissionRecalled;

	@SerializedName("cprec")
	private Double totalCpAgentCommissionRecalled;

	@SerializedName("afee")
	private Double totalAmendmentFee;

	@SerializedName("tds")
	private Double tdsToReturn;

	@SerializedName("pmtds")
	private Double partnerMarkUpTdsToReturn;

	@SerializedName("pctds")
	private Double partnerCommissionTdsToReturn;

	@SerializedName("cpatds")
	private Double cpAgentCommissionTdsToReturn;

	@SerializedName("iid")
	private String invoiceId;

	@SerializedName("odiff")
	private Double orderDiffAmount;

	@SerializedName("ras")
	private List<Double> chargedBySuppplierRefundAmounts;

	@SerializedName("nras")
	private List<Double> notChargedBySuppplierRefundAmounts;

	@SerializedName("hadi")
	private HotelAdditionalInfo hotelAdditionalInfo;

	@SerializedName("sR")
	private String systemRemarks;

	@SerializedName("recp")
	private boolean recallCPCommission;

	private Map<String, String> invoiceMap;

	@SerializedName("ips")
	@SearchPredicate(type = PredicateType.EQUAL, destinationEntity = "Amendment", dbAttribute = "additionalInfo.ips")
	private Boolean invoicePushStatus;

	public List<Double> getChargedBySuppplierRefundAmounts() {
		if (chargedBySuppplierRefundAmounts == null)
			chargedBySuppplierRefundAmounts = new ArrayList<>();
		return chargedBySuppplierRefundAmounts;
	}

	public List<Double> getNotChargedBySuppplierRefundAmounts() {
		if (notChargedBySuppplierRefundAmounts == null)
			notChargedBySuppplierRefundAmounts = new ArrayList<>();
		return notChargedBySuppplierRefundAmounts;
	}
}
