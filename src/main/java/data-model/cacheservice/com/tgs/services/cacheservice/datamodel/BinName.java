package com.tgs.services.cacheservice.datamodel;

import lombok.Getter;

@Getter
public enum BinName {
	/**
	 * length of binName should be less than 14 characters for aerospike
	 */

	USERID(null),
	ROLE(null),
	USERINFO(null),
	AT(null),
	SEARCHQUERYBIN(null),
	PRICEINFOBIN(null),
	AIRREVIEW(null),
	COMPRESS(null),
	PLAINDATA(null),
	APILOGS(null),
	STOREAT(null),
	BOOKINGID(null),
	PAYMENTS(null),
	ALLOWEDUSERID(null),
	CREDITLINE(null),
	CREDITBILL(null),
	NRCREDIT(null),
	SEATALLOCATION(null),
	RATEPLAN(null),
	AIRINVENTORY(null),
	SEARCHKEY(null),
	AIRINVENTORYID(null),
	SECTOR(null),
	DOCKEY(null),
	DOCTYPE(null),
	DOCDATA(null),
	USAGE(null),
	USERNAME(null),
	USERRELATION(null),
	USERBALANCE(null),
	USERPOINTS(null),
	USERPOINTSTYPE(null),
	SESSIONINFO(null),
	HOTELINFO(null),
	HOTELRESULTS(null),
	COMPLETEAT(null),
	HOTELDETAILS(null),
	HOTELRATING(null),
	SPLITCOUNT(null),
	SEARCHCOUNT(null),
	REVIEWID(null),
	COUNTRY(null),
	STATE(null),
	CITY(null),
	SUCCESSDEPOSIT(null),
	FARECOMPONENT(null),
	ROUTEINFO(null),
	RETRIEVEDBOOK(null),
	MEALBASIS(null),
	MONEYEXCHANGE(null),
	UPLOADRESPONSE(null),
	BEANNAME(null),
	APIKEY(null),
	RATING(null),
	DEVICEINFO(null),
	ID(null),
	COUNT(null),
	INFANTFARE(null),
	CANCELLATION(null),
	CONFIRMCANCEL(null),
	USERREVIEWID(null),
	UNMAPPEDUR(null),
	HOTELMANUAL(null),
	LPIDS(null),
	IATA(null),
	SIGNIN_ATTEMPT(null),
	HOTELMODIFY(null),
	POLICYID(null),
	UICONF(null),
	INVENTORYKEY(null),
	INVENTORYTIME(null),
	HOTELNAME(null),
	H_RATEPLAN(null),
	ROOMCATEGORY(null),
	SOURCEID(null),
	HOTEL_INFO(null),
	HOTEL_DETAIL(null),
	RAILREVIEW(null),
	GEOLOCATION(null),
	REGION_INFO(null),
	SUPHOTELINFO(null),
	HOTELID(null),
	GROUPUSERIDS(null),
	CREATIONTIME(String.class),
	SEARCHLIST(null);

	private Class classType;

	BinName(Class type) {
		this.classType = type;
	}
	
	public String getName() {
		return this.name();
	}
}
