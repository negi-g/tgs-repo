package com.tgs.services.cacheservice.datamodel;

public enum CacheNameSpace {

	STATIC_MAP, FLIGHT, GENERAL_PURPOSE, LOGS, USERS, HOTEL, HOTEL_SEARCH_RESULT, RAIL, FLIGHT_CACHE;

	public String getName() {
		return this.name().toLowerCase();
	}
}
