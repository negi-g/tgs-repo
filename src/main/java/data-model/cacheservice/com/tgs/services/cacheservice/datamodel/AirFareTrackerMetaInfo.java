package com.tgs.services.cacheservice.datamodel;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Setter
@Getter
@Accessors(chain = true)
public class AirFareTrackerMetaInfo extends CacheMetaInfo {
	
	private int indexInQueue;

	private String pnr;

	private String supplierId;

	private String loggedinUser;

	private Double actionThreshold;

	private List<String> searchSupplierIds;

	private String toEmailId;

	private String bccEmailId;

	private String fromEmailId;

	private LocalDateTime processedOn;
	
	private boolean isRequestDataUpdated;

}
