package com.tgs.services.analytics;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class QueueData {

    private String key;

    private String value;

}
