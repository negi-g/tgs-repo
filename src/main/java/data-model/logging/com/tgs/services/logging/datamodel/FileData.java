package com.tgs.services.logging.datamodel;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FileData {
	String filename;
	Object data;
}
