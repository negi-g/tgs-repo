package com.tgs.services.logging.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Builder
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class LogMetaInfo {
	private String bookingId;
	private String userId;
	private String searchId;
	private Integer trips;
	private long timeInMs;
	private String endPoint;
	private String supplierId;
	private Integer sourceId;
	private String searchtype;
	private String airtype;
	private String hostname;
	private Integer priceOptions;
	private Integer segmentSize;

	private String type;
	private Integer noOfEntries;

}
