package com.tgs.services.ims.datamodel.air;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class AirInventoryAdditionalInfo {

	@SerializedName("iha")
	private boolean isHoldAllowed;

	@SerializedName("iaoif")
	private boolean isApplOnInstantOfferFare;

	@SerializedName("htl")
	@Builder.Default
	@Min(5) // min 5 min
	@Max(2_16_000) // 5 months
	private Integer holdTimeLimit = 5;

	@SerializedName("dhbd")
	@Builder.Default
	private Integer disableHoldBeforeDeparture = 0;
}
