package com.tgs.services.ims.datamodel;

import java.time.LocalDateTime;
import java.util.List;

import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.ForbidInAPIRequest;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SeatAllocation extends DataModel {
	
	@ForbidInAPIRequest
	private Long id;

	private String inventoryId;

	private List<SeatAllocationInfo> allocationInfo;

	private Product product;

	@ForbidInAPIRequest
	private LocalDateTime createdOn;

}
