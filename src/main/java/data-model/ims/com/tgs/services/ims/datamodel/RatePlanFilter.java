package com.tgs.services.ims.datamodel;

import java.util.List;

import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class RatePlanFilter extends QueryFilter{

	@ApiModelProperty(notes = "To fetch rate plans based on rate plan id", example = "1001")
	private String id;
	
	@ApiModelProperty(notes = "To fetch rate plans matching given name. It's not mandatory to pass exact name. "
			+ "System will find best match based on the value ", example = "RP1")
	private String name;
	
	@ApiModelProperty(notes = "To fetch based on deleted", example = "true")
	private Boolean isDeleted;
	
	@ApiModelProperty(notes = "To fetch rate plans based on product")
	private Product product;
	
	@ApiModelProperty(value = "To fetch rate plans for a particular suppliers")
	private List<String> supplierIds;
	
}
