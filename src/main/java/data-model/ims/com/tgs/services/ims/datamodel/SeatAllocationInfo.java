package com.tgs.services.ims.datamodel;

import java.time.LocalDate;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import com.tgs.services.ims.datamodel.air.AirSeatAllocationAdditionalInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@GsonRunTimeAdaptorRequired(dependOn = "product")
@GsonPolymorphismMapping({ @ClassType(keys = { "AIR" }, value = AirSeatAllocationAdditionalInfo.class) })
public class SeatAllocationInfo {

	@ForbidInAPIRequest
	private Long id;

	@SerializedName("rid")
	private String ratePlanId;

	@SerializedName("rpn")
	@ForbidInAPIRequest
	private String ratePlanName;

	@SerializedName("sd")
	private LocalDate startDate;

	@SerializedName("ed")
	private LocalDate endDate;

	@SerializedName("ts")
	private Integer totalSeats;

	@SerializedName("ss")
	private Integer seatsSold;

	@ForbidInAPIRequest
	private boolean isDeleted;

	@SerializedName("frn")
	@DBExclude
	@ForbidInAPIRequest
	private String fareRuleName;

	@SerializedName("fid")
	private String fareRuleId;

	@Exclude
	private String inventoryId;

	@Exclude
	private LocalDate validOn;

}
