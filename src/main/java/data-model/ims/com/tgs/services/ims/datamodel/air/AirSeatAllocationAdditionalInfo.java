package com.tgs.services.ims.datamodel.air;

import org.apache.commons.lang3.ObjectUtils;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.gson.StringWithoutWhiteSpaceAdaptor;
import com.tgs.services.ims.datamodel.SeatAllocationInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirSeatAllocationAdditionalInfo extends SeatAllocationInfo {

	@SerializedName("aPnr")
	@JsonAdapter(StringWithoutWhiteSpaceAdaptor.class)
	private String airlinePnr;

	@SerializedName("dis")
	private Double discount;

	@SerializedName("ddis")
	private Double doubleDiscount;

	public Double getDiscount() {
		return ObjectUtils.firstNonNull(discount, 0D);
	}

	public Double getDoubleDiscount() {
		return ObjectUtils.firstNonNull(doubleDiscount, 0D);
	}
}
