package com.tgs.services.ims.datamodel;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.ims.datamodel.air.AirRatePlan;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@GsonRunTimeAdaptorRequired(dependOn = "product")
@GsonPolymorphismMapping({@ClassType(keys = {"AIR"}, value = AirRatePlan.class)})
public class RatePlan {

	private Long id;

	@NotNull
	@ApiModelProperty(required = true)
	private String name;

	private String description;

	@UserId
	private String supplierId;

	@ForbidInAPIRequest
	private LocalDateTime createdOn;

	@ForbidInAPIRequest
	private Boolean isDeleted;

	@ApiModelProperty(required = true)
	private Product product;

}
