package com.tgs.services.voucher.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.BasicRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import javax.validation.constraints.NotNull;

@Getter
public class VoucherRuleCriteriaOutput extends BasicRuleCriteria implements IRuleOutPut {

	@NotNull
	@SerializedName("exp")
	private String expression;

	@NotNull
	@SerializedName("ta")
	private Double thresholdAmount;

	public Double getDiscountAmount(double applicableDiscount) {
		if (getThresholdAmount() != null) {
			return (getThresholdAmount() < applicableDiscount) ? getThresholdAmount() : applicableDiscount;
		}
		return applicableDiscount;
	}

	public Double getDiscountAmountHotel(double applicableDiscount, Integer numberOfRooms, Integer numberOfnights) {

		if (getThresholdAmount() != null) {
			double perRoomDiscount = getThresholdAmount() / (numberOfRooms * numberOfnights);
			return (perRoomDiscount < applicableDiscount) ? perRoomDiscount : applicableDiscount;
		}
		return applicableDiscount;
	}

}
