package com.tgs.services.voucher.datamodel;

import javax.validation.constraints.NotNull;
import com.google.gson.Gson;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class VoucherConfigurationRule implements IRule {

	@ApiModelProperty(
			notes = "To denote the Product Type, Will be NA for General Criterias, applicable to all products",
			example = "AIR")
	private Product product;

	@ApiModelProperty(notes = "To denote the priority the rule", example = "1")
	private Double priority;


	@JsonAdapter(JsonStringSerializer.class)
	private String inclusionCriteria;

	@DBExclude
	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = FlightBasicRuleCriteria.class),
			@ClassType(keys = {"H", "HOTEL"}, value = HotelBasicRuleCriteria.class)})
	private GeneralBasicRuleCriteria inclusion;

	@JsonAdapter(JsonStringSerializer.class)
	private String exclusionCriteria;

	@DBExclude
	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = FlightBasicRuleCriteria.class),
			@ClassType(keys = {"H", "HOTEL"}, value = HotelBasicRuleCriteria.class)})
	private GeneralBasicRuleCriteria exclusion;

	@JsonAdapter(JsonStringSerializer.class)
	private String voucherCriteria;

	@DBExclude
	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = AirVoucherRuleCriteriaOutput.class),
			@ClassType(keys = {"H", "HOTEL"}, value = HotelVoucherRuleCriteriaOutput.class)})
	private VoucherRuleCriteriaOutput output;

	private Boolean canBeConsideredExclusively;


	@Override
	public double getPriority() {
		return priority != null ? priority.doubleValue() : 0.0;
	}

	@Override
	public GeneralBasicRuleCriteria getInclusionCriteria() {
		if (inclusion == null) {
			Gson gson = GsonUtils.getGsonBuilder().create();
			if (Product.AIR.equals(product)) {
				inclusion = gson.fromJson(inclusionCriteria, FlightBasicRuleCriteria.class);
			} else if (Product.HOTEL.equals(product)) {
				inclusion = gson.fromJson(inclusionCriteria, HotelBasicRuleCriteria.class);
			} else {
				inclusion = gson.fromJson(inclusionCriteria, GeneralBasicRuleCriteria.class);
			}
		}
		return inclusion;
	}

	@Override
	public GeneralBasicRuleCriteria getExclusionCriteria() {
		if (exclusion == null) {
			Gson gson = GsonUtils.getGsonBuilder().create();
			if (Product.AIR.equals(product)) {
				exclusion = gson.fromJson(exclusionCriteria, FlightBasicRuleCriteria.class);
			} else if (Product.HOTEL.equals(product)) {
				exclusion = gson.fromJson(exclusionCriteria, HotelBasicRuleCriteria.class);
			} else {
				exclusion = gson.fromJson(exclusionCriteria, GeneralBasicRuleCriteria.class);
			}
		}
		return exclusion;
	}

	@Override
	public VoucherRuleCriteriaOutput getOutput() {
		if (output == null) {
			Gson gson = GsonUtils.getGsonBuilder().create();
			if (Product.AIR.equals(product)) {
				output = gson.fromJson(voucherCriteria, AirVoucherRuleCriteriaOutput.class);
			} else if (Product.HOTEL.equals(product)) {
				output = gson.fromJson(voucherCriteria, HotelVoucherRuleCriteriaOutput.class);
			} else {
				output = gson.fromJson(voucherCriteria, VoucherRuleCriteriaOutput.class);
			}
		}
		return output;
	}

	@Override
	public boolean canBeConsideredExclusively() {
		return canBeConsideredExclusively;
	}

}
