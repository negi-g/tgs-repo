package com.tgs.services.hms.datamodel.tripadvisor;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelReviewMappingResponse {

	List<ReviewData> data;
	
	
}
