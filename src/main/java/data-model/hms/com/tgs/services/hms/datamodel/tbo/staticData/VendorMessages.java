package com.tgs.services.hms.datamodel.tbo.staticData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vendorMessage"
})
@XmlRootElement(name = "VendorMessages")
@Getter
@Setter
public class VendorMessages {

    @XmlElement(name = "VendorMessage", required = false)
    protected List<VendorMessages.VendorMessage> vendorMessage;

    public List<VendorMessages.VendorMessage> getVendorMessage() {
        if (vendorMessage == null) {
            vendorMessage = new ArrayList<VendorMessages.VendorMessage>();
        }
        return this.vendorMessage;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subSection"
    })
    @Getter
    @Setter
    public static class VendorMessage {

        @XmlElement(name = "SubSection", required = false)
        protected List<VendorMessages.VendorMessage.SubSection> subSection;
        @XmlAttribute(name = "Title", required = false)
        protected String title;
        @XmlAttribute(name = "InfoType", required = false)
        @XmlSchemaType(name = "unsignedByte")
        protected short infoType;

        public List<VendorMessages.VendorMessage.SubSection> getSubSection() {
            if (subSection == null) {
                subSection = new ArrayList<VendorMessages.VendorMessage.SubSection>();
            }
            return this.subSection;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "paragraph"
        })
        @Getter
        @Setter
        public static class SubSection {

            @XmlElement(name = "Paragraph", required = false)
            protected List<VendorMessages.VendorMessage.SubSection.Paragraph> paragraph;
            @XmlAttribute(name = "SubTitle")
            protected String subTitle;
            @XmlAttribute(name = "Category")
            protected String category;
            @XmlAttribute(name = "CategoryCode")
            @XmlSchemaType(name = "unsignedByte")
            protected Short categoryCode;

            public List<VendorMessages.VendorMessage.SubSection.Paragraph> getParagraph() {
                if (paragraph == null) {
                    paragraph = new ArrayList<VendorMessages.VendorMessage.SubSection.Paragraph>();
                }
                return this.paragraph;
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "url",
                "text"
            })
            @Getter
            @Setter
            public static class Paragraph {

                @XmlElement(name = "URL")
                protected String url;
                @XmlElement(name = "Text")
                protected VendorMessages.VendorMessage.SubSection.Paragraph.Text text;
                @XmlAttribute(name = "CreatorID")
                @XmlSchemaType(name = "unsignedByte")
                protected Short creatorID;
                @XmlAttribute(name = "Type")
                protected String type;

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "content"
                })
                @Getter
                @Setter
                public static class Text {

                    @XmlElementRefs({
                        @XmlElementRef(name = "p", namespace = "http://www.opentravel.org/OTA/2003/05", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "br", namespace = "http://www.opentravel.org/OTA/2003/05", type = JAXBElement.class, required = false)
                    })
                    @XmlMixed
                    protected List<Serializable> content;
                    @XmlAttribute(name = "TextFormat", required = false)
                    protected String textFormat;
                    @XmlAttribute(name = "ID")
                    @XmlSchemaType(name = "unsignedShort")
                    protected Integer id;

                    public List<Serializable> getContent() {
                        if (content == null) {
                            content = new ArrayList<Serializable>();
                        }
                        return this.content;
                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "content"
                    })
                    @Getter
                    @Setter
                    public static class P {

                        @XmlElementRefs({
                            @XmlElementRef(name = "ul", namespace = "http://www.opentravel.org/OTA/2003/05", type = JAXBElement.class, required = false),
                            @XmlElementRef(name = "br", namespace = "http://www.opentravel.org/OTA/2003/05", type = JAXBElement.class, required = false)
                        })
                        @XmlMixed
                        protected List<Serializable> content;
                        
                        public List<Serializable> getContent() {
                            if (content == null) {
                                content = new ArrayList<Serializable>();
                            }
                            return this.content;
                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "li"
                        })
                        @Getter
                        @Setter
                        public static class Ul {

                            protected List<String> li;

                            public List<String> getLi() {
                                if (li == null) {
                                    li = new ArrayList<String>();
                                }
                                return this.li;
                            }

                        }

                    }

                }

            }

        }

    }

}
