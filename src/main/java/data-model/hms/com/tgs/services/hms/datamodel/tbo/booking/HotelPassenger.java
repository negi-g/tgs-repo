package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Data;

@Data
public class HotelPassenger {

	private Integer Age;
	private String Email;
	private String FirstName;
	private String LastName;
	private Boolean LeadPassenger;
	private String MiddleName;
	private String PassportExpDate;
	private String PassportIssueDate;
	private String PassportNo;
	private Integer PaxType;
	private String Phoneno;
	private String Title;
}
