package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Builder;
import lombok.Data;
@Builder
@Data
public class AdvancedOptions {
	public String Currency;
}
