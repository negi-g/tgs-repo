package com.tgs.services.hms.datamodel.tbo.staticData;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "attribute"
})
@XmlRootElement(name = "Attributes")
@Getter
@Setter
public class Attributes {

    @XmlElement(name = "Attribute", required = false)
    protected List<Attributes.Attribute> attribute;

    public List<Attributes.Attribute> getAttribute() {
        if (attribute == null) {
            attribute = new ArrayList<Attributes.Attribute>();
        }
        return this.attribute;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @Getter
    @Setter
    public static class Attribute {

        @XmlAttribute(name = "AttributeName", required = false)
        protected String attributeName;
        @XmlAttribute(name = "AttributeType", required = false)
        protected String attributeType;

    }

}
