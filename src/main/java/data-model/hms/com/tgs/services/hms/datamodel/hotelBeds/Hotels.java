package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Hotels {

    private List<Hotel> hotels ;
    private String checkIn;
    private Integer total;
    private String checkOut;
}
