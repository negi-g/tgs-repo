package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TripType {

	private String name;
	private String value;
	private String localized_name; 
}
