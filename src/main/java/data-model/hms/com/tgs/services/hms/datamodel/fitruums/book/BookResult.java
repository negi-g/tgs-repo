package com.tgs.services.hms.datamodel.fitruums.book;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;
import com.tgs.services.hms.datamodel.fitruums.FitruumsBaseResponse;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"error", "booking"})
@XmlRootElement(name = "bookResult")
@Getter
@Setter
public class BookResult extends FitruumsBaseResponse {
	@XmlElement(name = "Error")
	protected Error error;
	@XmlElement(required = true)
	protected BookResult.Booking booking;

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "",
			propOrder = {"bookingnumber", "hotelId", "hotelName", "hotelAddress", "hotelPhone", "numberofrooms",
					"roomType", "roomEnglishType", "mealId", "meal", "mealLabel", "englishMeal", "englishMealLabel",
					"checkindate", "checkoutdate", "prices", "currency", "bookingdate", "bookingdateTimezone",
					"cancellationpolicies", "earliestNonFreeCancellationDateCET",
					"earliestNonFreeCancellationDateLocal", "yourref", "voucher", "bookedBy", "transferbooked",
					"paymentmethod", "hotelNotes", "englishHotelNotes", "roomNotes", "englishRoomNotes", "invoiceref"})
	@Getter
	@Setter
	public static class Booking {

		@XmlElement(required = true)
		protected String bookingnumber;
		@XmlElement(name = "hotel.id")
		@XmlSchemaType(name = "unsignedInt")
		protected long hotelId;
		@XmlElement(name = "hotel.name", required = true)
		protected String hotelName;
		@XmlElement(name = "hotel.address", required = true)
		protected String hotelAddress;
		@XmlElement(name = "hotel.phone")
		protected long hotelPhone;
		@XmlSchemaType(name = "unsignedByte")
		protected short numberofrooms;
		@XmlElement(name = "room.type", required = true)
		protected String roomType;
		@XmlElement(name = "room.englishType", required = true)
		protected String roomEnglishType;
		@XmlSchemaType(name = "unsignedByte")
		protected short mealId;
		@XmlElement(required = true)
		protected String meal;
		@XmlElement(required = true)
		protected Object mealLabel;
		@XmlElement(required = true)
		protected String englishMeal;
		@XmlElement(required = true)
		protected Object englishMealLabel;
		@XmlElement(required = true)
		@XmlSchemaType(name = "dateTime")
		protected XMLGregorianCalendar checkindate;
		@XmlElement(required = true)
		@XmlSchemaType(name = "dateTime")
		protected XMLGregorianCalendar checkoutdate;
		@XmlElement(required = true)
		protected BookResult.Booking.Prices prices;
		@XmlElement(required = true)
		protected String currency;
		@XmlElement(required = true)
		@XmlSchemaType(name = "dateTime")
		protected XMLGregorianCalendar bookingdate;
		@XmlElement(name = "bookingdate.timezone", required = true)
		protected String bookingdateTimezone;
		@XmlElement(required = true)
		protected List<BookResult.Booking.Cancellationpolicies> cancellationpolicies;
		@XmlElement(name = "earliestNonFreeCancellationDate.CET", required = true)
		@XmlSchemaType(name = "dateTime")
		protected XMLGregorianCalendar earliestNonFreeCancellationDateCET;
		@XmlElement(name = "earliestNonFreeCancellationDate.Local", required = true)
		@XmlSchemaType(name = "dateTime")
		protected XMLGregorianCalendar earliestNonFreeCancellationDateLocal;
		@XmlElement(required = true)
		protected Object yourref;
		@XmlElement(required = true)
		protected String voucher;
		@XmlElement(required = true)
		protected String bookedBy;
		@XmlSchemaType(name = "unsignedByte")
		protected short transferbooked;
		@XmlElement(required = true)
		protected BookResult.Booking.Paymentmethod paymentmethod;
		@XmlElement(required = true)
		protected BookResult.Booking.HotelNotes hotelNotes;
		@XmlElement(required = true)
		protected BookResult.Booking.EnglishHotelNotes englishHotelNotes;
		@XmlElement(required = true)
		protected Object roomNotes;
		@XmlElement(required = true)
		protected Object englishRoomNotes;
		@XmlElement(required = true)
		protected Object invoiceref;

		public List<BookResult.Booking.Cancellationpolicies> getCancellationpolicies() {
			if (cancellationpolicies == null) {
				cancellationpolicies = new ArrayList<BookResult.Booking.Cancellationpolicies>();
			}
			return this.cancellationpolicies;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"deadline", "percentage", "text"})
		@Getter
		@Setter
		public static class Cancellationpolicies {

			@XmlSchemaType(name = "unsignedByte")
			protected short deadline;
			@XmlSchemaType(name = "unsignedByte")
			protected short percentage;
			@XmlElement(required = true)
			protected String text;

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"englishHotelNote"})
		@Getter
		@Setter
		public static class EnglishHotelNotes {

			@XmlElement(required = true)
			protected BookResult.Booking.EnglishHotelNotes.EnglishHotelNote englishHotelNote;

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {"text"})
			public static class EnglishHotelNote {

				@XmlElement(required = true)
				protected String text;
				@XmlAttribute(name = "start_date", required = true)
				@XmlSchemaType(name = "dateTime")
				protected XMLGregorianCalendar startDate;
				@XmlAttribute(name = "end_date", required = true)
				@XmlSchemaType(name = "dateTime")
				protected XMLGregorianCalendar endDate;

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"hotelNote"})
		@Getter
		@Setter
		public static class HotelNotes {

			@XmlElement(required = true)
			protected BookResult.Booking.HotelNotes.HotelNote hotelNote;

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {"text"})
			public static class HotelNote {

				@XmlElement(required = true)
				protected String text;
				@XmlAttribute(name = "start_date", required = true)
				@XmlSchemaType(name = "dateTime")
				protected XMLGregorianCalendar startDate;
				@XmlAttribute(name = "end_date", required = true)
				@XmlSchemaType(name = "dateTime")
				protected XMLGregorianCalendar endDate;

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		@Getter
		@Setter
		public static class Paymentmethod {

			@XmlAttribute(name = "id", required = true)
			@XmlSchemaType(name = "unsignedByte")
			protected short id;
			@XmlAttribute(name = "name", required = true)
			protected String name;

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"price"})
		@Getter
		@Setter
		public static class Prices {

			@XmlElement(required = true)
			protected List<BookResult.Booking.Prices.Price> price;

			public List<BookResult.Booking.Prices.Price> getPrice() {
				if (price == null) {
					price = new ArrayList<BookResult.Booking.Prices.Price>();
				}
				return this.price;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {"value"})
			@Getter
			@Setter
			public static class Price {

				@XmlValue
				protected BigDecimal value;
				@XmlAttribute(name = "currency", required = true)
				protected String currency;
				@XmlAttribute(name = "paymentMethods", required = true)
				@XmlSchemaType(name = "unsignedByte")
				protected short paymentMethods;

			}

		}

	}

}
