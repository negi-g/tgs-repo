package com.tgs.services.hms.datamodel.dotw;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="rateBases")
@XmlAccessorType(XmlAccessType.FIELD)
public class RateBases {

	@XmlElement(name = "rateBasis")
	List<RateBasis> rateBasis;
	
}
