package com.tgs.services.hms.datamodel;

import com.tgs.services.base.datamodel.Alert;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class RoomSoldOut implements Alert {

	private String type;
	private String message;

	@Override
	public String getType() {
		return type;
	}
}
