
package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlRegistry;

import com.tgs.services.hms.datamodel.fitruums.book.PreBookResult;

@XmlRegistry
public class ObjectFactory {

	public ObjectFactory() {
	}

	public ResortList createResortList() {
		return new ResortList();
	}

	public ResortList.Resorts createResortListResorts() {
		return new ResortList.Resorts();
	}

	public ResortList.Resorts.Resort createResortListResortsResort() {
		return new ResortList.Resorts.Resort();
	}

	public PreBookResult createPreBookResult() {
		return new PreBookResult();
	}

	public CancellationPolicies createPreBookResultCancellationPolicies() {
		return new CancellationPolicies();
	}

	public Notes createPreBookResultNotes() {
		return new Notes();
	}

	public Price createPreBookResultPrice() {
		return new Price();
	}

	public CancellationPolicy createPreBookResultCancellationPoliciesCancellationPolicy() {
		return new CancellationPolicy();
	}

	public Note createPreBookResultNotesNote() {
		return new Note();
	}

}
