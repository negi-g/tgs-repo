package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Pricing {

	private Double roomRate;
	private Double hotelTaxes;
	private Double discount;
	private Double cashback;
	private Double totalFare;
	private Double totalFee;
	private Double serviceTax;
	private String currency;
}
