package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Data;

@Data
public class CheckHotelCancellationChargesRS {
	public HotelCancellationOption HotelOption;
	public String Currency;
}
