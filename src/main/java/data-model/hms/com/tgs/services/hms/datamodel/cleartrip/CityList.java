package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CityList {

	 private Integer id;
	 private String cityName;
	 private String stateId;
	 private String countryId;
	 private Double latitude;
	 private Double longitude;
	 private Country country;
	 private State state;
}
