package com.tgs.services.hms.datamodel.supplier;

import java.time.LocalDateTime;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.helper.Exclude;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HotelSupplierInfo extends DataModel {

	@SearchPredicate(type = PredicateType.EQUAL)
	private Long id;

	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	private LocalDateTime createdOn;
	private LocalDateTime processedOn;
	
	/**
	 * While creating new supplier there is no need to send id in the request.
	 * Database will automatically create a unique id/supplierId.
	 */
	private HotelSupplierCredential credentialInfo;

	@SearchPredicate(type = PredicateType.EQUAL, filterName = "enable")
	private Boolean enabled;

	@SearchPredicate(type = PredicateType.IN)
	private Integer sourceId;

	@SearchPredicate(type = PredicateType.IN)
	private String name;

	@Exclude
	@SearchPredicate(type = PredicateType.EQUAL)
	private Boolean isDeleted = Boolean.FALSE;

	public String getSupplierId() {
		return name;
	}

}
