package com.tgs.services.hms.datamodel.hotelconfigurator;

import com.google.api.client.repackaged.com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelClientMarkupOutput implements IRuleOutPut {

	@SerializedName("hcmu")
	private Double hotelClientMarkUp;


	@Override
	public void cleanData() {

		setHotelClientMarkUp(Objects.firstNonNull(hotelClientMarkUp, 0.0));
	}
}
