package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "id", "beds", "extrabeds", "meals", "cancellationPolicies", "notes", "isSuperDeal",
		"isBestBuy", "paymentMethods" })
public class Room {

	@XmlSchemaType(name = "unsignedInt")
	protected long id;
	@XmlSchemaType(name = "unsignedByte")
	protected short beds;
	@XmlSchemaType(name = "unsignedByte")
	protected short extrabeds;
	@XmlElement(required = true)
	protected Meals meals;
	@XmlElement(name = "cancellation_policies", required = true)
	protected CancellationPolicies cancellationPolicies;
	@XmlElement(required = true)
	protected Notes notes;
	protected boolean isSuperDeal;
	protected boolean isBestBuy;
	@XmlElement(required = true)
	protected PaymentMethods paymentMethods;

}
