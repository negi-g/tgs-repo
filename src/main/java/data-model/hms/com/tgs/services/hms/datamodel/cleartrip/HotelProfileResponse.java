package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class HotelProfileResponse {

	private String code;
	private HotelProfileData data;
}
