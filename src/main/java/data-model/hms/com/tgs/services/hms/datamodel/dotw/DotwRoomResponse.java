package com.tgs.services.hms.datamodel.dotw;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@XmlRootElement(name="room")
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwRoomResponse {

	@XmlAttribute
	private String adults;
	
	@XmlAttribute(name = "children")
	private String child;
	
	@XmlAttribute(name = "childrenages")
	private String childrenages;
	
	@XmlElement(name="roomType")
	private List<DotwRoomType> roomType;
	
}
