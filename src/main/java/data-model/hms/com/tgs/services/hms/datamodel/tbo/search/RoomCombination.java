package com.tgs.services.hms.datamodel.tbo.search;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RoomCombination {

	private List<String> RoomIndex;
	
	public RoomCombination() {
		
	}
	
	public RoomCombination(List<String> indexes) {
		this.RoomIndex = indexes;
	}
}
