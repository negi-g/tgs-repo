package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "errorMessages"
})
@XmlRootElement(name = "ConfirmCancellationResponseV2")
@Data
public class ConfirmCancellationResponseV2 {

    @XmlElement(name = "ErrorMessages")
    protected List<ErrorMessage> errorMessages;
    @XmlAttribute(name = "status", required = true)
    protected String status;

}
