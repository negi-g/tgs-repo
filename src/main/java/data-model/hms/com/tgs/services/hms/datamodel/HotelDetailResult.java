package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class HotelDetailResult implements HotelResult {
	private HotelInfo hotel;
	private HotelSearchQuery searchQuery;
}
