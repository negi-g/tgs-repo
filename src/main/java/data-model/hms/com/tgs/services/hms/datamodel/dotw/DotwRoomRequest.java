package com.tgs.services.hms.datamodel.dotw;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="room")
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwRoomRequest {
	
	@XmlAttribute(name = "runno")
	private String number;
	
	@XmlElement
	private String roomTypeCode;
	
	@XmlElement
	private String selectedRateBasis;
	
	@XmlElement
	private String allocationDetails;
	
	@XmlElement
	private Integer adultsCode;
	
	@XmlElement
	private Integer actualAdults;
	
	@XmlElement
	private DotwChildren children;
	
	@XmlElement
	private DotwActualChildren actualChildren; 
	
	@XmlElement
	private Integer extraBed;
	
	@XmlElement
	private Integer rateBasis;
	
	@XmlElement
	private String passengerNationality;
	
	@XmlElement
	private String passengerCountryOfResidence;
	
	@XmlElement(name = "roomTypeSelected")
	private DotwSelectedRoom selectedRoom;
	
	@XmlElementWrapper(name = "passengersDetails")
	@XmlElement(name = "passenger")
	private List<DotwPassenger> passenger;
	
	@XmlElementWrapper(name = "specialRequests")
	@XmlElement(name = "req")
	private List<String> req;
	
	@XmlElement
	private Integer beddingPreference;
	
}
