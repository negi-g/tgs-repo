package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class HotelKeyInfo {

	private String hotelName;
	private Integer hotelRating;
	private String cityName;
	private String countryName;
}
