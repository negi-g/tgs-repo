package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Review {

	private String Rating;
	private Integer Count;
	private String RatingImage;
	private String Provider;
}