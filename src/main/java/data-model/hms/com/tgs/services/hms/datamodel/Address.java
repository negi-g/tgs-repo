package com.tgs.services.hms.datamodel;

import java.util.Objects;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Address {

	@CustomSerializedName(key = FieldName.HOTEL_ADDRESS_LINE1)
	private String addressLine1;
	@CustomSerializedName(key = FieldName.HOTEL_ADDRESS_LINE2)
	@ToString.Exclude
	private String addressLine2;
	private String postalCode;
	@Deprecated
	private City city;
	@Deprecated
	private State state;
	@Deprecated
	private Country country;
	private String address;

	@CustomSerializedName(key = FieldName.CITY_NAME)
	private String cityName;
	@CustomSerializedName(key = FieldName.STATE_NAME)
	private String stateName;
	@CustomSerializedName(key = FieldName.COUNTRY_NAME)
	private String countryName;
	
	public boolean isAddressLine1Present() {
		return this.getAddressLine1() != null ? true : false;
	}

	public boolean isAddressLine2Present() {
		return this.getAddressLine2() != null ? true : false;
	}

	public boolean isCityPresent() {
		return Objects.nonNull(city) && !city.isEmpty();
	}
	
	public boolean isStatePresent() {
		return Objects.nonNull(state) && !state.isEmpty();
	}

	public boolean isCountryPresent() {
		return Objects.nonNull(country) && !country.isEmpty();
	}

}
