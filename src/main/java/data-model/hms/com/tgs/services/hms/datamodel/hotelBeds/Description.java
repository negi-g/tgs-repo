package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Description {

	private String content;
}
