package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "paymentMethod" })
public class PaymentMethods {

	@XmlElement(required = true)
	protected PaymentMethod paymentMethod;

}
