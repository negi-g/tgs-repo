package com.tgs.services.hms.datamodel.agoda.search;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="RateInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class RateInfo {
	
}
