package com.tgs.services.hms.datamodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.RestExclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoomInfo {

	private String id;

	@SerializedName("rc")
	@ToString.Exclude
	private String roomCategory;
	@SerializedName("rt")
	private String roomType;
	@SerializedName("srn")
	private String standardRoomName;
	@SerializedName("adt")
	private Integer numberOfAdults;
	@SerializedName("chd")
	private Integer numberOfChild;

	@SerializedName("des")
	@ToString.Exclude
	private String description;

	@SerializedName("mb")
	private String mealBasis;

	@SerializedName("tp")
	private Double totalPrice;

	@SerializedName("tfcs")
	private Map<HotelFareComponent, Double> totalFareComponents;

	@DBExclude
	@SerializedName("tafcs")
	private Map<HotelFareComponent, Map<HotelFareComponent, Double>> totalAddlFareComponents;

	@RestExclude
	@SerializedName("cnp")
	private HotelCancellationPolicy cancellationPolicy;

	@SerializedName("ddt")
	private LocalDateTime deadlineDateTime;

	@SerializedName("crd")
	private LocalDateTime cancellationRestrictedDateTime;

	@SerializedName("pis")
	private List<PriceInfo> perNightPriceInfos;

	@SerializedName("fcs")
	private List<String> roomAmenities;

	@SerializedName("imgs")
	@DBExclude
	@ToString.Exclude
	private List<String> images;


	private LocalDate checkInDate;

	private LocalDate checkOutDate;

	@SerializedName("ti")
	@ToString.Exclude
	private List<TravellerInfo> travellerInfo;


	@SerializedName("ssr")
	@ToString.Exclude
	private List<RoomSSR> roomSSR;

	@RestExclude
	private Boolean isDeleted;

	@SerializedName("iopr")
	private Boolean isOptionOnRequest;

	@SerializedName("rmi")
	@APIUserExclude
	private RoomMiscInfo miscInfo;

	@SerializedName("radi")
	private RoomAdditionalInfo roomAdditionalInfo;

	@SerializedName("op")
	private String occupancyPattern;

	@SerializedName("iexb")
	private Boolean isextraBedIncluded;

	@APIUserExclude
	@SerializedName("hosis")
	private List<HotelOrderSupplierInfo> hotelOrderSupplierInfos;

	@APIUserExclude
	@SerializedName("bnpld")
	private LocalDate bookNowPayLaterDate;

	@SerializedName("rexb")
	private Map<RoomBenefitType, List<RoomExtraBenefit>> roomExtraBenefits;

	@SerializedName("rsta")
	private String roomStatus;

	public Map<HotelFareComponent, Double> getTotalFareComponents() {
		if (totalFareComponents == null) {
			totalFareComponents = new HashMap<>();
		}
		return totalFareComponents;
	}

	public Map<HotelFareComponent, Map<HotelFareComponent, Double>> getTotalAddlFareComponents() {

		if (totalAddlFareComponents == null) {
			totalAddlFareComponents = new HashMap<>();
		}
		return this.totalAddlFareComponents;
	}

	public List<PriceInfo> getPerNightPriceInfos() {
		if (perNightPriceInfos == null)
			perNightPriceInfos = new ArrayList<>();
		return perNightPriceInfos;
	}

	public List<HotelOrderSupplierInfo> getHotelOrderSupplierInfos() {
		if (hotelOrderSupplierInfos == null)
			hotelOrderSupplierInfos = new ArrayList<>();
		return hotelOrderSupplierInfos;
	}

	public boolean getIsOptionOnRequest() {
		return BooleanUtils.isTrue(this.isOptionOnRequest);
	}

	public void cleanDataForSearch() {

		setDeadlineDateTime(null);
		setCancellationRestrictedDateTime(null);
		setCancellationPolicy(null);
		setRoomExtraBenefits(null);
		setMiscInfo(null);
		setOccupancyPattern(null);
		setRoomAmenities(null);
		setRoomAdditionalInfo(null);
		setRoomType(null);
		setNumberOfAdults(null);
		setNumberOfChild(null);

	}

	// We need to remove this after few months as we may have some Q-Tech bookings with extra space in room id's now.
	public String getId() {
		return StringUtils.trim(this.id);
	}
}
