package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelContent {

	private String UnicaId;
	private OpinionatedHotel OpinionatedHotel;
	private List<ProviderHotel> ProviderHotels;
}

