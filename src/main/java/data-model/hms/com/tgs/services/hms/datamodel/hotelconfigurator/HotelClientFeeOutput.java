package com.tgs.services.hms.datamodel.hotelconfigurator;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelClientFeeOutput implements IRuleOutPut {

	@SerializedName("mf")
	private Double managementFee;
	
	@SerializedName("mft")
	private Double managementFeeTax;
}
