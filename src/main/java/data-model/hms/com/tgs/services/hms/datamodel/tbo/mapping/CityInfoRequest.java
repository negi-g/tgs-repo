package com.tgs.services.hms.datamodel.tbo.mapping;

import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityInfoRequest extends TBOBaseRequest {
	
	private String TokenId;
	private String ClientId;
	private String EndUserIp;
	private String CountryCode;

}
 