package com.tgs.services.hms.datamodel.tbo.search;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.tgs.services.hms.datamodel.tbo.staticData.Address;
import com.tgs.services.hms.datamodel.tbo.staticData.Attributes;
import com.tgs.services.hms.datamodel.tbo.staticData.Award;
import com.tgs.services.hms.datamodel.tbo.staticData.ContactNumbers;
import com.tgs.services.hms.datamodel.tbo.staticData.HotelThemes;
import com.tgs.services.hms.datamodel.tbo.staticData.Policy;
import com.tgs.services.hms.datamodel.tbo.staticData.Position;
import com.tgs.services.hms.datamodel.tbo.staticData.Rooms;
import com.tgs.services.hms.datamodel.tbo.staticData.SpecialTags;
import com.tgs.services.hms.datamodel.tbo.staticData.VendorMessages;
import lombok.Getter;
import lombok.Setter;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "basicPropertyInfo"
})
@XmlRootElement(name = "ArrayOfBasicPropertyInfo")
@Getter
@Setter
public class ArrayOfBasicPropertyInfo {

    @XmlElement(name = "BasicPropertyInfo", required = false)
    protected List<ArrayOfBasicPropertyInfo.BasicPropertyInfo> basicPropertyInfo;

    public List<ArrayOfBasicPropertyInfo.BasicPropertyInfo>getBasicPropertyInfo() {
    	if(basicPropertyInfo == null) {
    		basicPropertyInfo = new ArrayList<ArrayOfBasicPropertyInfo.BasicPropertyInfo>();
    	}
    	return this.basicPropertyInfo;
    }
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vendorMessages",
        "position",
        "address",
        "contactNumbers",
        "award",
        "policy",
        "hotelThemes",
        "rooms",
        "specialTags",
        "attributes"
    })
    @Getter
    @Setter
    public static class BasicPropertyInfo {

        @XmlElement(name = "VendorMessages", namespace = "http://www.opentravel.org/OTA/2003/05", required = false)
        protected VendorMessages vendorMessages;
        @XmlElement(name = "Position", namespace = "http://www.opentravel.org/OTA/2003/05", required = false)
        protected Position position;
        @XmlElement(name = "Address", namespace = "http://www.opentravel.org/OTA/2003/05", required = false)
        protected Address address;
        @XmlElement(name = "ContactNumbers", namespace = "http://www.opentravel.org/OTA/2003/05", required = false)
        protected ContactNumbers contactNumbers;
        @XmlElement(name = "Award", namespace = "http://www.opentravel.org/OTA/2003/05", required = false)
        protected Award award;
        @XmlElement(name = "Policy", namespace = "http://www.opentravel.org/OTA/2003/05", required = false)
        protected Policy policy;
        @XmlElement(name = "HotelThemes", namespace = "http://www.opentravel.org/OTA/2003/05", required = false)
        protected HotelThemes hotelThemes;
        @XmlElement(name = "Rooms", namespace = "http://www.opentravel.org/OTA/2003/05", required = false)
        protected Rooms rooms;
        @XmlElement(name = "SpecialTags", namespace = "http://www.opentravel.org/OTA/2003/05", required = false)
        protected SpecialTags specialTags;
        @XmlElement(name = "Attributes", namespace = "http://www.opentravel.org/OTA/2003/05", required = false)
        protected Attributes attributes;
        @XmlAttribute(name = "BrandCode", required = false)
        @XmlSchemaType(name = "unsignedByte")
        protected short brandCode;
        @XmlAttribute(name = "TBOHotelCode", required = false)
        @XmlSchemaType(name = "unsignedInt")
        protected long tboHotelCode;
        @XmlAttribute(name = "HotelName", required = false)
        protected String hotelName;
        @XmlAttribute(name = "LocationCategoryCode", required = false)
        protected String locationCategoryCode;
        @XmlAttribute(name = "NoOfRooms", required = false)
        @XmlSchemaType(name = "unsignedByte")
        protected short noOfRooms;
        @XmlAttribute(name = "NoOfFloors", required = false)
        @XmlSchemaType(name = "unsignedByte")
        protected short noOfFloors;
        @XmlAttribute(name = "BuiltYear", required = false)
        @XmlSchemaType(name = "unsignedShort")
        protected int builtYear;
        @XmlAttribute(name = "RenovationYear", required = false)
        @XmlSchemaType(name = "unsignedByte")
        protected short renovationYear;
        @XmlAttribute(name = "HotelCategoryId", required = false)
        @XmlSchemaType(name = "unsignedByte")
        protected short hotelCategoryId;
        @XmlAttribute(name = "HotelCategoryName", required = false)
        protected String hotelCategoryName;
        @XmlAttribute(name = "IsHalal", required = false)
        protected boolean isHalal;

    }

}
