package com.tgs.services.hms.datamodel.dotw;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="validForOccupancy")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidForOccupancy {

	@XmlElement
	private Integer adults;
	@XmlElement
	private Integer children;
	@XmlElement
	private String childrenAges;
	@XmlElement
	private Integer extraBed;
	@XmlElement
	private String extraBedOccupant;
	
	
}
