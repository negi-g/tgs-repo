package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoomRequest", propOrder = {
    "rate",
    "childrenAges",
    "surcharges",
    "guestDetails",
    "reuseHints",
    "specialRequest"
})
@Data
public class RoomRequest {

    @XmlElement(name = "Rate", required = true)
    protected RateRequest rate;
    @XmlElement(name = "ChildrenAges")
    protected ChildrenAgesRequest childrenAges;
    @XmlElement(name = "Surcharges")
    protected SurchargeRequest surcharges;
    @XmlElement(name = "GuestDetails", required = true)
	protected BookingRequestV3.BookingDetails.Hotel.Rooms.Room.GuestDetails guestDetails;
	@XmlElement(name = "ReuseHints")
	protected BookingRequestV3.BookingDetails.Hotel.Rooms.Room.ReuseHints reuseHints;
	@XmlElement(name = "SpecialRequest")
	protected String specialRequest;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "promotionid")
    protected String promotionid;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "lineitemid", required = true)
    protected String lineitemid;
    @XmlAttribute(name = "rateplan")
    protected String rateplan;
   // protected RatePlanRequest rateplan;
    @XmlAttribute(name = "ratetype")
    protected String ratetype;
    //protected RateTypeRequest ratetype;
    @XmlAttribute(name = "ratechannel")
    protected String ratechannel;
    @XmlAttribute(name = "rateplanid")
    protected String rateplanid;
    @XmlAttribute(name = "currency", required = true)
    protected String currency;
    @XmlAttribute(name = "model", required = true)
    protected String model;
    @XmlAttribute(name = "ratecategoryid", required = true)
    protected String ratecategoryid;
    @XmlAttribute(name = "blockid", required = true)
    protected String blockid;
    @XmlAttribute(name = "booknowpaylaterdate")
    @XmlSchemaType(name = "date")
    protected String booknowpaylaterdate;
    @XmlAttribute(name = "roomtypenotguaranteed")
    protected Boolean roomtypenotguaranteed;
    @XmlAttribute(name = "count", required = true)
    protected int count;
    @XmlAttribute(name = "adults", required = true)
    protected int adults;
    @XmlAttribute(name = "children", required = true)
    protected int children;
}
