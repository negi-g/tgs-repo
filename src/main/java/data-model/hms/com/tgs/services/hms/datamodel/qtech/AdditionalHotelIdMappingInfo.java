package com.tgs.services.hms.datamodel.qtech;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AdditionalHotelIdMappingInfo {

	@SerializedName("scity")
	private String supplierCity;

	@SerializedName("sctry")
	private String supplierCountry;
}
