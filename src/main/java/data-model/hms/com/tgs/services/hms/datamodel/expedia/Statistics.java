package com.tgs.services.hms.datamodel.expedia;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Statistics {

	private Map<String, OptionStatistics> statistics;
}
