package com.tgs.services.hms.datamodel.expedia;

import java.util.Set;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ExpediaAdditionalSearchParams {

	private Set<String> property_id;
	private String country_code;
	private String language;
	private Boolean isPackageRate;

}
