package com.tgs.services.hms.datamodel.supplier;

import lombok.Getter;

@Getter
public enum SupplierService {

	HOTEL("H"),
	SIGHTSEEING("S"),
	TRANSFER("T");
	
	private String code;

	SupplierService(String code) {
		this.code = code;
	}
}
