
package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Data;

@Data
public class BookingDetailResponse {

    public GetBookingDetailResult GetBookingDetailResult;

}
