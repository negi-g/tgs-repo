package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CleartripStaticResponseData {

	private Integer totalCityCount;
	private List<CityList> cityList;
	private List<HotelList> hotels;
}
