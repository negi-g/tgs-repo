package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Occupancy {

	private List<Integer> childAges;
	private Integer numberOfAdults;
}
