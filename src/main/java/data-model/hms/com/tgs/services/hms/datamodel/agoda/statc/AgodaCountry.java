package com.tgs.services.hms.datamodel.agoda.statc;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "country")
public class AgodaCountry {

	@XmlElement(name = "country_id")
    protected Long countryId;
	
    @XmlElement(name = "continent_id")
    protected Long continentId;
    
    @XmlElement(name = "country_name")
    protected String countryName;
    
    @XmlElement(name = "country_translated")
    protected String countryTranslated;
    
    @XmlElement(name = "active_hotels")
    protected Long activeHotels;
    
    @XmlElement(name = "country_iso")
    protected String countryIso;
    
    @XmlElement(name = "country_iso2")
    protected String countryIso2;
    
    protected BigDecimal longitude;
    protected BigDecimal latitude;
	
}
