package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Rates {

	private String id;
	private String room_name;
}
