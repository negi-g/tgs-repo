package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CleartripCreateBookingSuccessResponse {

	private String tripId;
	private String confirmationNumber;
	private String itineraryId;
}
