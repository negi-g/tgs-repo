package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProviderHotel {

	private String Id;
	private Long UnicaId;
	private String Name;
	private String ProviderHotelId;
	private String ProviderFamily;
	private GeoCode GeoCode;
	private GeoCode ProviderGeocodes;
	private Contact Contact;
	private String Rating;
	private List<Review> Reviews;
	private String BrandName;
	private String BrandCode;
	private String ChainCode;
	private String ChainName;
	private String MasterPropertyType;
	private String PropertyType;
	private List<Facility> Facilities;
	private String HeroImage;
	private String ProviderHeroImageHref;
	private List<Image> Images;
	private List<Description> Descriptions;
	private List<String> OpiniatedFields;
	private String DateEntered;
	private String LastModified;
	private String LastModifiedDateTime;
	private Checkin Checkin;
	private Checkout Checkout;
	private List<Fee> Fees;
	private List<Policy> Policies;
	private Boolean Disabled;
	private Boolean MatchSuspect;
	private List<Room> Rooms;
	private List<PointOfInterest> PointOfInterests;
	private String MasterChainName;
	private String MasterChainCode;
	private String MasterBrandName;
	private String MasterBrandCode;
	private String ExtMasterId;
	private String MatchBy;
	private Integer PopularityScore;
	private Integer ContentScore;
	private String LicenseNumber;
	private List<Attributes> attributes;

}