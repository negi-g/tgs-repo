package com.tgs.services.hms.datamodel.tbo.staticData;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Award")
@Getter
@Setter
public class Award {

    @XmlAttribute(name = "Provider", required = false)
    protected String provider;
    @XmlAttribute(name = "Rating", required = false)
    protected BigDecimal rating;
    @XmlAttribute(name = "ReviewURL", required = false)
    protected String reviewURL;

}
