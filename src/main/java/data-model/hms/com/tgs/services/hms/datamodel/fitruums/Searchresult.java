
package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.tgs.services.hms.datamodel.fitruums.book.Error;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"error", "hotels"})
@XmlRootElement(name = "searchresult")
@Getter
@Setter
public class Searchresult extends FitruumsBaseResponse {
	@XmlElement(name = "Error")
	protected Error error;
	@XmlElement(required = true)
	protected Hotels hotels;

}
