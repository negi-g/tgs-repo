package com.tgs.services.hms.datamodel.agoda.search;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="Room")
@XmlAccessorType(XmlAccessType.FIELD)
public class Room {
	 
	 @XmlElement
	 private String StandardTranslation;
	 
	 @XmlElementWrapper(name = "Benefits")
	 @XmlElement(name="Benefit")
	 private List<Benefit> Benefits;
	 
	 @XmlElement(name="Benefit")
	 Benefit BenefitsObject;
	 
	 @XmlElement
	 MaxRoomOccupancy MaxRoomOccupancyObject;
	 
	 private String RemainingRooms;
	 
	 @XmlElement
	 private String Amendable;
	 
	
	 @XmlElement
	 private String LandingURL;
	 
	 @XmlAttribute
	 private String id;
	 
	 @XmlAttribute
	 private String promotionid;
	 
	 @XmlAttribute
	 private String name;
	 
	 @XmlAttribute
	 private String lineitemid;
	 
	 @XmlAttribute
	 private String rateplan;
	 
	 @XmlAttribute
	 private String ratetype;
	 
	 @XmlAttribute
	 private String currency;
	
	 @XmlAttribute
	 private String model;
	 
	 @XmlAttribute
	 private String ratecategoryid;
	 
	 @XmlAttribute
	 private String blockid;
	 
	 @XmlAttribute
	 private String booknowpaylaterdate;
}
