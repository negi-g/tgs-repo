package com.tgs.services.hms.datamodel;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TravelInfo {

	private LocalDate checkinDate;
	private LocalDate checkoutDate;
	private Integer selCountry;
	private Integer selCity;
}
