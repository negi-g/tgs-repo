package com.tgs.services.hms.datamodel.qtech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBookingCancellationResponse {
	
	private String Message;
	private String MessageInfo;

}
