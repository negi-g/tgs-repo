package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.helper.JsonStringSerializer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Document {

	private String key;

	@JsonAdapter(JsonStringSerializer.class)
	private String data;

	private LocalDateTime created_on;

	private Boolean enabled;

	/**
	 * TTL in second
	 */
	private Integer ttl;

	private Long id;

	private String type;
}
