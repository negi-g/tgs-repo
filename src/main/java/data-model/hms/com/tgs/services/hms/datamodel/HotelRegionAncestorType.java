package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRegionAncestorType {

	private String id;
	private String type;
}
