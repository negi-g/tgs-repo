package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Data;

@Data
public class AvailabilityRS {
	public String SearchKey;
	public Integer Count;
	public String Currency;
	public List<HotelResult> HotelResult;
}
