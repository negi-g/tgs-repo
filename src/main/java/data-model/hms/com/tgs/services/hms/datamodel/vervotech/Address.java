package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Address {

	private String Line1;
	private String Line2;
	private String City;
	private String Code;
	private String Locality;
	private String DestinationCode;
	private String State;
	private String StateCode;
	private String Country;
	private String CountryCode;
	private String PostalCode;
}
