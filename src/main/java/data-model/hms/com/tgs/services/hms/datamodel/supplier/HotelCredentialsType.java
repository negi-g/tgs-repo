package com.tgs.services.hms.datamodel.supplier;

import lombok.Getter;

@Getter
public enum HotelCredentialsType {
	FTP;

	public String getName() {
		return this.name();
	}
}
