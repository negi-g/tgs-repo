package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Name {

    private String languageCode;
    private String content;
}
