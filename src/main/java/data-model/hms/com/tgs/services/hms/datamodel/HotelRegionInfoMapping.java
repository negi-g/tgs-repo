package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class HotelRegionInfoMapping {

	private Long regionId;
	private Long supplierRegionId;
	private LocalDateTime createdOn;
	private String supplierName;
}
