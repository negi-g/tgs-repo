package com.tgs.services.hms.datamodel.tbo.staticData;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "specialTag"
})
@XmlRootElement(name = "SpecialTags")
@Getter
@Setter
public class SpecialTags {

    @XmlElement(name = "SpecialTag", required = false)
    protected SpecialTags.SpecialTag specialTag;

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @Getter
    @Setter
    public static class SpecialTag {

        @XmlAttribute(name = "SpecialTagId", required = false)
        @XmlSchemaType(name = "unsignedByte")
        protected short specialTagId;
        @XmlAttribute(name = "SpecialTagName", required = false)
        protected String specialTagName;

    }

}
