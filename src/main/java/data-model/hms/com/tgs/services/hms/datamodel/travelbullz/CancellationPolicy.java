package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CancellationPolicy {
	public String FromDate;
	public String ToDate;
	public Double CancellationPrice;
}
