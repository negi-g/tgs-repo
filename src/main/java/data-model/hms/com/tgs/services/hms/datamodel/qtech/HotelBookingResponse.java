package com.tgs.services.hms.datamodel.qtech;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelBookingResponse{

	private String Message;
	private String MessageInfo;
	private String Success;
	private BookingDetail BookingDetail;
	
}
