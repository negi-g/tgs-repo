package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PointOfInterest {

	private String Name;
	private String Type;
	private String Distance;
	private String Geocode;
	private String Unit;
}