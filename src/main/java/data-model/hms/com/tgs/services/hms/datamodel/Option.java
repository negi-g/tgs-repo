package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class Option {


	@CustomSerializedName(key = FieldName.ROOMINFO_LIST)
	List<RoomInfo> roomInfos;

	private String id;

	@CustomSerializedName(key = FieldName.OPTION_PRICE)
	Double totalPrice;

	@CustomSerializedName(key = FieldName.OPTION_CANCELLATION_POLICY)
	private HotelCancellationPolicy cancellationPolicy;

	@CustomSerializedName(key = FieldName.OPTION_DEADLINEDATETIME)
	private LocalDateTime deadlineDateTime;

	@SerializedName("crd")
	private LocalDateTime cancellationRestrictedDateTime;

	@CustomSerializedName(key = FieldName.HOTEL_INSTRUCTIONS)
	@ToString.Exclude
	private List<Instruction> instructions;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.OPTION_MISC_INFO)
	private OptionMiscInfo miscInfo;

	@DBExclude
	@CustomSerializedName(key = FieldName.IS_OPTION_ON_REQUEST)
	private Boolean isOptionOnRequest;

	/**
	 * This will be used post booking
	 */
	@CustomSerializedName(key = FieldName.ROOM_SSR_LIST)
	@ToString.Exclude
	private List<RoomSSR> roomSSRs;

	@CustomSerializedName(key = FieldName.IS_PACKAGE_RATE)
	private Boolean isPackageRate;

	@CustomSerializedName(key = FieldName.IS_PAN_REQUIRED)
	private Boolean isPanRequired;

	@CustomSerializedName(key = FieldName.OPTION_MAPPING_ID)
	private String optionMappingId;

	@CustomSerializedName(key = FieldName.IS_PASS_MANDATORY)
	private Boolean isPassportMandatory;
	
	@CustomSerializedName(key = FieldName.CANCELLATION_POLICY_BUFFER)
	private Integer cancellationPolicyBuffer;

	public List<Instruction> getInstructions() {
		if (instructions == null)
			instructions = new ArrayList<>();
		return instructions;
	}


	public HotelCancellationPolicy getCancellationPolicy() {

		if (this.cancellationPolicy != null)
			return this.cancellationPolicy;

		List<HotelCancellationPolicy> cpList = new ArrayList<>();
		List<RoomInfo> roomInfos = this.getRoomInfos();

		boolean isFullRefundAllowed = true;
		for (RoomInfo roomInfo : roomInfos) {
			if (roomInfo.getCancellationPolicy() == null)
				return null;
			HotelCancellationPolicy roomCancellationPolicy = roomInfo.getCancellationPolicy();
			cpList.add(roomCancellationPolicy);
			if (roomCancellationPolicy.getIsFullRefundAllowed() != null) {
				isFullRefundAllowed &= roomInfo.getCancellationPolicy().getIsFullRefundAllowed();
			}

		}

		HotelCancellationPolicy cp = HotelCancellationPolicy.builder().roomCancellationPolicyList(cpList)
				.id(this.getId()).isFullRefundAllowed(isFullRefundAllowed).build();

		this.cancellationPolicy = cp;
		return cp;
	}

	public String getInstructionFromType(InstructionType type) {

		if (!CollectionUtils.isEmpty(this.getInstructions())) {
			return this.getInstructions().stream().filter((instruction) -> instruction.getType().equals(type))
					.map(Instruction::getMsg).findFirst().orElse(null);
		}

		return null;
	}

	public boolean getIsOptionOnRequest() {
		return BooleanUtils.isTrue(this.isOptionOnRequest);
	}

	public int getPaxCount() {

		int sum = 0;
		for (RoomInfo roomInfo : roomInfos) {
			sum += (roomInfo.getNumberOfAdults() + roomInfo.getNumberOfChild());
		}
		return sum;
	}

	public void cleanDataForSearch() {
		setRoomSSRs(null);
		setIsPackageRate(null);
		setIsPanRequired(null);
		setCancellationPolicyBuffer(null);
		setDeadlineDateTime(null);
		setCancellationRestrictedDateTime(null);
		setCancellationPolicy(null);
		setInstructions(null);
	}
}
