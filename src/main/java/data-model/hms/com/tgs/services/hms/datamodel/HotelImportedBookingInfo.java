package com.tgs.services.hms.datamodel;

import java.time.LocalDate;
import com.tgs.services.base.datamodel.DeliveryInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class HotelImportedBookingInfo {

	private String bookingCurrencyCode;
	private String orderStatus;
	private LocalDate bookingDate;
	private String affiliateRefId;
	private HotelInfo hInfo;
	private HotelSearchQuery searchQuery;
	private DeliveryInfo deliveryInfo;
}
