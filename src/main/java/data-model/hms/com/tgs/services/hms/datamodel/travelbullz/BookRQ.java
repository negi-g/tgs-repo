package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BookRQ {
	public String BookingToken;
	public Double TotalPrice;
	public String InternalReference;
	@Builder.Default
	public List<HotelRoom> HotelRooms = null;
}
