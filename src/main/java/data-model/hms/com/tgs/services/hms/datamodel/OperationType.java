package com.tgs.services.hms.datamodel;

public enum OperationType {

	LIST_SEARCH, 
	DETAIL_SEARCH,
	REVIEW,
	BOOKING_DETAILS,
	CITY_MAPPING;
}
