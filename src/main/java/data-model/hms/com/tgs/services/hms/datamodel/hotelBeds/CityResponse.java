package com.tgs.services.hms.datamodel.hotelBeds;
import java.util.List;
import lombok.Data;

@Data
public class CityResponse {
	 
	   public Integer from;
	   public Integer to;
	   public Integer total;
	   public AuditData auditData;
	   public List<Destination> destinations = null;
}
