package com.tgs.services.hms.datamodel.tbo.bookingcancellation;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelBookingCancellationResponse {

	private HotelCancellationResult HotelChangeRequestResult;
	
	public boolean isSessionExpired() {
		
		if(this.getHotelChangeRequestResult() != null && this.getHotelChangeRequestResult().getResponseStatus() != null){
			Integer responseStatus = this.getHotelChangeRequestResult().getResponseStatus();
			if(responseStatus == 4) { return true; }
		}
		return false;
	}
	
}
