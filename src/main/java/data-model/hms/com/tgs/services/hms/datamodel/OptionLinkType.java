package com.tgs.services.hms.datamodel;

import lombok.Getter;

@Getter
public enum OptionLinkType {

	PRICE_CHECK("price_check"),
	ITINERARY("book"),
	RETRIEVE("retrieve"),
	RESUME("resume"),
	CANCEL("cancel"),
	COMPLETE_PAYMENT_SESSION("complete_payment_session");

	private String linkType;

	OptionLinkType(String linkType) {
		this.linkType = linkType;
	}

	public static OptionLinkType getOptionLinkType(String linkType) {
		for (OptionLinkType link : OptionLinkType.values()) {
			if (link.getLinkType().equals(linkType)) {
				return link;
			}
		}
		return null;
	}

}
