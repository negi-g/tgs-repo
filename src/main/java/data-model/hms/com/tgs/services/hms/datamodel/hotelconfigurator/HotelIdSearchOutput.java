package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.Set;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelIdSearchOutput implements IRuleOutPut {

	@SerializedName("hids")
	private Set<String> hotelIds;
}
