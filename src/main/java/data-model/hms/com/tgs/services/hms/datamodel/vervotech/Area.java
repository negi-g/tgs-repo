package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Area {

	private String SquareMeters;
	private String SquareFeet;
	private String Text;
}
