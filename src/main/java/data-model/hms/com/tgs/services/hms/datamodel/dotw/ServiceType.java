package com.tgs.services.hms.datamodel.dotw;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "serviceType", propOrder = {
    "cancellationPenalty"
})
public class ServiceType {

    @XmlElement
    protected List<CancellationPenaltyType> cancellationPenalty;
    @XmlAttribute(name = "runno")
    @XmlSchemaType(name = "unsignedByte")
    protected short runno;
    @XmlAttribute(name = "code")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger code;

        public List<CancellationPenaltyType> getCancellationPenalty() {
        if (cancellationPenalty == null) {
            cancellationPenalty = new ArrayList<CancellationPenaltyType>();
        }
        return this.cancellationPenalty;
    }
}
