package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ValueAdds {

	private String id;
	private String description;
	private String category;
	private String offer_type;
	private String frequency;
	private Integer person_count;
}
