package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Data;

@Data
public class ErrorType {

	private String type;
	private String code;
	private String description;
}
