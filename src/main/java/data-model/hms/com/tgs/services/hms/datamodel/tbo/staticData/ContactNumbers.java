package com.tgs.services.hms.datamodel.tbo.staticData;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "contactNumber"
})
@XmlRootElement(name = "ContactNumbers")
@Getter
@Setter
public class ContactNumbers {

    @XmlElement(name = "ContactNumber", required = false)
    protected List<ContactNumbers.ContactNumber> contactNumber;

    public List<ContactNumbers.ContactNumber> getContactNumber() {
        if (contactNumber == null) {
            contactNumber = new ArrayList<ContactNumbers.ContactNumber>();
        }
        return this.contactNumber;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @Getter
    @Setter
    public static class ContactNumber {

        @XmlAttribute(name = "PhoneNumber", required = false)
        protected String phoneNumber;
        @XmlAttribute(name = "PhoneTechType", required = false)
        protected String phoneTechType;
    }

}
