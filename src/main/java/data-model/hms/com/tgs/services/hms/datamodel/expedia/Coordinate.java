package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Coordinate {

	private Double latitude;
	private Double longitude;
}
