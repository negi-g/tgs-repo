package com.tgs.services.hms.datamodel.tbo.search;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelInfoResult {

	private String TraceId;
	private HotelDetail HotelDetails;
	private Integer ResponseStatus;
	private Error error;
	
}
