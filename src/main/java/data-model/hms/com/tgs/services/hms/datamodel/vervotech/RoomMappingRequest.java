package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;
import com.tgs.services.base.helper.Exclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RoomMappingRequest {

	@Exclude
	private String id;

	private Integer Index;
	private String Code;
	private String RoomName;
	private String Description;
	private String Provider;
	private List<String> Beds;
	private String providerHotelId;
	private String View;
	private Integer SmokingAllowed;
	private Double NightlyPrice;
	private String Currency;
}