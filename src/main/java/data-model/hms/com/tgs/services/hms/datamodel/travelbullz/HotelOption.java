package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HotelOption {
	public String HotelOptionId;
	public Double MinPrice;
	public Boolean IsCombineRoom;
	@Builder.Default
	public List<List<HotelRoom>> HotelRooms = null;
	
}
