package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CleartripBookingCancellationSuccessResponse {

	private String cancelStatus;
}
