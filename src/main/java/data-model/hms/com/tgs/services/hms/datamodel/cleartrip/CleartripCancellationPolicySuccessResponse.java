package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class CleartripCancellationPolicySuccessResponse {

	private String cancelPolicy;
	private String refundable;
	private String checkInInstructions;
	private String specialCheckInInstructions;
	private String hotelPolicy;
}
