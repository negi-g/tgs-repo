package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateResponse")
public class RateResponse {

    @XmlAttribute(name = "exclusive", required = true)
    protected BigDecimal exclusive;
    @XmlAttribute(name = "tax", required = true)
    protected BigDecimal tax;
    @XmlAttribute(name = "fees", required = true)
    protected BigDecimal fees;
    @XmlAttribute(name = "inclusive", required = true)
    protected BigDecimal inclusive;
    @XmlAttribute(name = "processingFee")
    protected BigDecimal processingFee;
}
