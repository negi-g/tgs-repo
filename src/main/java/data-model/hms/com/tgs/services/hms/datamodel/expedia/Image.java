package com.tgs.services.hms.datamodel.expedia;

import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Image {

	private Boolean hero_image;
	private Integer category;
	private String caption;
	private Map<String, URLSignature> links;
}
