package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Rating {
	
	private String rating_image_url;
	private String name;
	private String value;
	private String localized_name;

}
