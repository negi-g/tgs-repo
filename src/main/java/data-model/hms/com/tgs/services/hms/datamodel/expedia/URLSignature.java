package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class URLSignature {

	private String method;
	private String href;
}
