package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class InvoiceCompany {

	private String code;
	private String company;
	private String registrationNumber;
}
