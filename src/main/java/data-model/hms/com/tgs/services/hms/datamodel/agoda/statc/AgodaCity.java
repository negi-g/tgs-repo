package com.tgs.services.hms.datamodel.agoda.statc;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "city")
public class AgodaCity {

	@XmlElement(name = "city_id")
    protected Long cityId;
	
    @XmlElement(name = "country_id")
    protected Long countryId;
    
    @XmlElement(name = "city_name")
    protected String cityName;
    
    @XmlElement(name = "city_translated")
    protected String cityTranslated;
    
    @XmlElement(name = "active_hotels")
    protected Long activeHotels;
    
    protected BigDecimal longitude;
    protected BigDecimal latitude;
    
    @XmlElement(name = "no_area")
    protected Long noArea;
	
}
