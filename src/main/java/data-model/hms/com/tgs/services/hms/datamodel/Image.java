package com.tgs.services.hms.datamodel;

import java.util.List;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class Image {

	@CustomSerializedName(key = FieldName.HOTEL_THUMBNAILS)
	private String thumbnail;

	@CustomSerializedName(key = FieldName.HOTEL_IMAGE_URL)
	private String bigURL;

	@CustomSerializedName(key = FieldName.HOTEL_IMAGE_SIZE)
	private String size;

	@CustomSerializedName(key = FieldName.ROOMID_LIST)
	private List<String> roomIds;

}
