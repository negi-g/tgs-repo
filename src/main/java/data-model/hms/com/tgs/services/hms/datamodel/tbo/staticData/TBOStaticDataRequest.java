package com.tgs.services.hms.datamodel.tbo.staticData;

import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TBOStaticDataRequest extends TBOBaseRequest {

	private String CityId;
	private String EndUserIp;
	private String TokenId;
	private String ClientId;
}
