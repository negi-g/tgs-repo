package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="roomTypeSelected")
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwSelectedRoom {

	@XmlElement
	private String code;
	
	@XmlElement
	private String selectedRateBasis;
	
	@XmlElement
	private String allocationDetails;
	
}
