package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelbullzBookResponse extends TravelbullzBaseResponse{
	public List<ErrorType> Error ;
	public BookRS BookRS;
}
