package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
@Data
public class Country {
	private String CountryId;
	private String Name;
}
