package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.APIUserExclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class HotelWiseSearchInfo {

	@SerializedName("hotelid")
	private String hotelId;

	@SerializedName("hotelname")
	private String hotelName;

	@APIUserExclude
	@SerializedName("suppliername")
	private String supplierName;

	@SerializedName("cityname")
	private String cityName;

	@SerializedName("statename")
	private String stateName;

	@SerializedName("countryname")
	private String countryName;

	@SerializedName("lastmodifieddatetime")
	private LocalDateTime lastModifiedDateTime;

	@SerializedName("isdisabled")
	private Boolean isDisabled;

	@SerializedName("unicaid")
	private String unicaId;
}
