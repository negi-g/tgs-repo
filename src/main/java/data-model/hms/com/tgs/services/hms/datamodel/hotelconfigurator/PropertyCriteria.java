package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.hms.datamodel.HotelInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PropertyCriteria {

	@SerializedName("hnp")
	private String hotelNamePattern;
	@SerializedName("rt")
	private List<Integer> starRating;

	private boolean isValidStarRating(Integer starRating) {
		if (this.starRating == null || this.starRating.contains(starRating)) {
			return true;
		}
		return false;
	}

	private boolean isValidHotelNamePattern(String hotelName) {
		return StringUtils.containsIgnoreCase(hotelName, hotelNamePattern);
	}

	public boolean isPropertyCriteriaMatches(HotelInfo hInfo) {
		if (isValidHotelNamePattern(hInfo.getName()) && isValidStarRating(hInfo.getRating())) {
			return true;
		}
		return false;
	}
}
