package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="country")
@XmlAccessorType(XmlAccessType.FIELD)
public class Country {

	@XmlElement
	private String name;
	@XmlElement
	private String code;
	@XmlElement
	private String regionName;
	@XmlElement
	private String regionCode;
	
}
