package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CleartripHotelInfo {

	private String hotelId;
	private Integer hotelRank;
	private HotelBasicInfo hotelBasicInfo;
	private HotelBasicInfo basicInfo;
	private List<CleartripRoomRate> roomRates;
}
