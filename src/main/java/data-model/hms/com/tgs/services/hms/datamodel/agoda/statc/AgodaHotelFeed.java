package com.tgs.services.hms.datamodel.agoda.statc;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Hotel_feed")
public class AgodaHotelFeed {
	
	@XmlElementWrapper(name = "hotels")
	@XmlElement(name="hotel")
	private List<AgodaHotel> hotels;

}
