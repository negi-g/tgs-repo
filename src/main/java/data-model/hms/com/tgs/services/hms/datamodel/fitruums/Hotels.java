package com.tgs.services.hms.datamodel.fitruums;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "hotel" })
@Getter
@Setter
public class Hotels {

	@XmlElement(required = true)
	protected List<Hotel> hotel;

	public List<Hotel> getHotel() {
		if (hotel == null) {
			hotel = new ArrayList<Hotel>();
		}
		return this.hotel;
	}
}