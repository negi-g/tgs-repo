package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CheckHotelCancellationChargesRQ {
	public Integer BookingId;
	public String InternalReference;
	public String ReferenceNo;
}
