package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "roomtypeID", "rooms", "roomType", "sharedRoom", "sharedFacilities" })
@Getter
@Setter
public class Roomtype {

	@XmlElement(name = "roomtype.ID")
	@XmlSchemaType(name = "unsignedShort")
	protected int roomtypeID;
	@XmlElement(required = true)
	protected Rooms rooms;
	@XmlElement(name = "room.type", required = true)
	protected String roomType;
	protected boolean sharedRoom;
	@XmlElement(required = true, nillable = true)
	protected String sharedFacilities;

}
