package com.tgs.services.hms.datamodel.fitruums;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class FitruumsStaticDataRequest {
	private String userName;
	private String password;
	private String language;
	private String destinationCode;
	private String destinationID;
	private String sortBy;
	private String sortOrder;
	private String exactDestinationMatch;
	private String destination;
	private String hotelIDs;
	private String resortIDs;
	private String accommodationTypes;

}
