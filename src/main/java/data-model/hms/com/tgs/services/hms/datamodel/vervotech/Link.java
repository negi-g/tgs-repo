package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Link {

	private String Size;
	private String ProviderHref;
	private Boolean Disabled;
	private String Href;
}