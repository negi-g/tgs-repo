package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxFeesResponse", propOrder = {
    "taxServiceBreakdown"
})
@Data
public class TaxFeesResponse {

    @XmlElement(required = true)
    protected List<TaxFeeResponse> taxServiceBreakdown;
    public List<TaxFeeResponse> getTaxServiceBreakdown() {
        if (taxServiceBreakdown == null) {
            taxServiceBreakdown = new ArrayList<TaxFeeResponse>();
        }
        return this.taxServiceBreakdown;
    }

}
