package com.tgs.services.hms.datamodel.tbo.authentication;

import com.tgs.services.hms.datamodel.tbo.search.Error;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelAuthenticationResponse {

	private String TokenId;
	private Integer Status;
	private Error Error;
}
