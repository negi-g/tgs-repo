package com.tgs.services.hms.datamodel;


import org.apache.commons.lang3.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class City {
	private String code;
	private String name;

	public boolean isEmpty() {

		return StringUtils.isBlank(code) && StringUtils.isBlank(name);
	}
}