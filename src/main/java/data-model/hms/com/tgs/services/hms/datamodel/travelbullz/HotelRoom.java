package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HotelRoom {
	public String RoomNo;
	public String RoomTypeName;
	public String MealName;
	public Double Price;
	public String BookingStatus;
	public String RoomToken;
	public String MappedMealName;
	public String EssentialInformation;
	public List<CancellationPolicy> CancellationPolicy;
	public PriceChange PriceChange;
	public Integer UniqueId;
	public String IsLead;
	public String PaxType;
	public String Prefix;
	public String FirstName;
	public String LastName;
	public String ChildAge;
	public List<Passenger> Passenger;
	public Integer BookingDetailId;
	public Double RefundAmount;
	public String Ressage;
	public Integer CancelStatus;
}
