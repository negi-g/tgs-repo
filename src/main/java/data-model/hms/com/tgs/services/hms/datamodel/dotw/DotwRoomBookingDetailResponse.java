package com.tgs.services.hms.datamodel.dotw;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwRoomBookingDetailResponse {
	
	@XmlElement(name = "code")
	private String roomBookingId;

	@XmlElement
	private DotwServicePrice servicePrice;
	
	@XmlElement
	private String currency;
	
	@XmlElement
	private String status;
	
	@XmlElement(name = "serviceId")
	private String hotelId;
	
	@XmlElement(name = "serviceName")
	private String hotelName;
	
	@XmlElement
	private Integer adults;
	
	@XmlElement
	private DotwChildren children;
	
	@XmlElementWrapper(name = "cancellationRules")
	@XmlElement(name="rule")
	private List<DotwCancellationRule> cancellationRules;
	
	@XmlElementWrapper(name = "passengersDetails")
	@XmlElement(name="passenger")
	private List<DotwPassenger> passengerList;
	
	@XmlElement
	private String from;
	
	@XmlElement
	private String to;
	
	@XmlElement
	private String roomTypeCode;
	
	@XmlElement
	private String rateBasis;
	
	@XmlElement
	private String roomName;
	
	@XmlElement
	private String roomCategory;
	
	@XmlElementWrapper(name = "dates")
	@XmlElement(name="date")
	private List<DotwPerDayRoomPrice> date;
	
}
