package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class TravelbullzRequest {
	public String Token;
	public Request Request;
	public AdvancedOptions AdvancedOptions;
}
