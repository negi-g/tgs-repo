package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class Destination {

	public String code;
	public Name name;
	public String countryCode;
	public String isoCode;
	public List<Zone> zones = null;
	public List<Object> groupZones = null;
}
