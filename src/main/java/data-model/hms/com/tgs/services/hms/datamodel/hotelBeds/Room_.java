package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class Room_ {

	private String code;
	private String name;
	private List<Rate> rates = null;
}
