package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "userName", "password", "language", "currencies", "checkInDate", "checkOutDate",
		"numberOfRooms", "destination", "destinationID", "hotelIDs", "resortIDs", "accommodationTypes",
		"numberOfAdults", "numberOfChildren", "childrenAges", "infant", "sortBy", "sortOrder", "exactDestinationMatch",
		"blockSuperdeal", "mealIds", "showCoordinates", "showReviews", "referencePointLatitude",
		"referencePointLongitude", "maxDistanceFromReferencePoint", "minStarRating", "maxStarRating", "featureIds",
		"minPrice", "maxPrice", "themeIds", "excludeSharedRooms", "excludeSharedFacilities", "prioritizedHotelIds",
		"totalRoomsInBatch", "paymentMethodId", "customerCountry", "b2C" })
@XmlRootElement(name = "Search")
public class FitruumsSearchRequest {

	@XmlElement(required = true)
	protected String userName;
	@XmlElement(required = true)
	protected String password;
	@XmlElement(required = true)
	protected String language;
	@XmlElement(required = true)
	protected String currencies;
	@XmlElement(required = true)
	protected String checkInDate;
	@XmlElement(required = true)
	protected String checkOutDate;
	@XmlElement(required = true)
	protected String numberOfRooms;
	@XmlElement(required = true)
	protected String destination;
	@XmlElement(required = true)
	protected String destinationID;
	@XmlElement(required = true)
	protected String hotelIDs;
	@XmlElement(required = true)
	protected String resortIDs;
	@XmlElement(required = true)
	protected String accommodationTypes;
	@XmlElement(required = true)
	protected String numberOfAdults;
	@XmlElement(required = true)
	protected String numberOfChildren;
	@XmlElement(required = true)
	protected String childrenAges;
	@XmlElement(required = true)
	protected String infant;
	@XmlElement(required = true)
	protected String sortBy;
	@XmlElement(required = true)
	protected String sortOrder;
	@XmlElement(required = true)
	protected String exactDestinationMatch;
	@XmlElement(required = true)
	protected String blockSuperdeal;
	@XmlElement(required = true)
	protected String mealIds;
	@XmlElement(required = true)
	protected String showCoordinates;
	@XmlElement(required = true)
	protected String showReviews;
	@XmlElement(required = true)
	protected String referencePointLatitude;
	@XmlElement(required = true)
	protected String referencePointLongitude;
	@XmlElement(required = true)
	protected String maxDistanceFromReferencePoint;
	@XmlElement(required = true)
	protected String minStarRating;
	@XmlElement(required = true)
	protected String maxStarRating;
	@XmlElement(required = true)
	protected String featureIds;
	@XmlElement(required = true)
	protected String minPrice;
	@XmlElement(required = true)
	protected String maxPrice;
	@XmlElement(required = true)
	protected String themeIds;
	@XmlElement(required = true)
	protected String excludeSharedRooms;
	@XmlElement(required = true)
	protected String excludeSharedFacilities;
	@XmlElement(required = true)
	protected String prioritizedHotelIds;
	@XmlElement(required = true)
	protected String totalRoomsInBatch;
	@XmlElement(required = true)
	protected String paymentMethodId;
	@XmlElement(required = true)
	protected String customerCountry;
	@XmlElement(name = "b2c", required = true)
	protected String b2C;

}
