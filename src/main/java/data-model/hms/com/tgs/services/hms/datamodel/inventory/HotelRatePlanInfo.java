package com.tgs.services.hms.datamodel.inventory;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.utils.TgsCollectionUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelRatePlanInfo {

	@SerializedName("mp")
	private List<String> marketProfile;
	
	@SerializedName("en")
	private List<String> excludeNationality;
	
	@SerializedName("ebr")
	private List<ExtraBedRate> extraBedRates;
	
	@SerializedName("rp")
	private Integer releasePeriod;
	
	@SerializedName("bf")
	private Double baseFare;
	
	@SerializedName("ms")
	private Integer minStay;

	public void cleanData() {
		
		setMarketProfile(TgsCollectionUtils.getCleanStringList(getMarketProfile()));
		setExcludeNationality(TgsCollectionUtils.getCleanStringList(getExcludeNationality()));
	}
}
