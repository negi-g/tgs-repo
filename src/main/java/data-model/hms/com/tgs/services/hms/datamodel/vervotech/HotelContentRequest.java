package com.tgs.services.hms.datamodel.vervotech;

import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class HotelContentRequest extends VervotechBaseRequest {

	private Set<Long> unicaIds;
	private Boolean opinionatedContent;
	private Boolean allProvidersContent;
	private Set<String> providerPreferences;
	private Boolean ReturnAllProviderHotelsForUnicaId;
}
