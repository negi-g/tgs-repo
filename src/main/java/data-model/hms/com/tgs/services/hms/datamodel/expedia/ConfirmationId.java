package com.tgs.services.hms.datamodel.expedia;

import lombok.Data;

@Data
public class ConfirmationId {

	public String expedia;
	public String property;
}
