package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelbullzSearchResponse extends TravelbullzBaseResponse{
	public List<ErrorType> Error = null;
	public AvailabilityRS AvailabilityRS;
}
