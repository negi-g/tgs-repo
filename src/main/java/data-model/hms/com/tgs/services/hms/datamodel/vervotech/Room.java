package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Room {

	private String RoomId;
	private String Type;
	private String Description;
	private List<Facility> Facilities;
	private List<String> Views;
	private Integer MaxGuestAllowed;
	private Integer MaxAdultAllowed;
	private Integer MaxChildrenAllowed;
	private Area Area;
	private List<Bed> Beds;
}
