package com.tgs.services.hms.datamodel;

import com.tgs.services.base.datamodel.BookingConditions;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class HotelBookingConditions extends BookingConditions {

}
