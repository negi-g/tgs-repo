package com.tgs.services.hms.datamodel;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Contact {

	@CustomSerializedName(key = FieldName.HOTEL_PHONE)
	private String phone;
	@CustomSerializedName(key = FieldName.HOTEL_EMAIL)
	private String email;
	@ToString.Exclude
	private String fax;
	@CustomSerializedName(key = FieldName.HOTEL_WEBSITE)
	private String website;
}
