package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelInfoFilterationOutput implements IRuleOutPut {

	@SerializedName("es")
	private List<String> excludedStrings;

	@SerializedName("ec")
	private List<String> excludedCharacters;
}
