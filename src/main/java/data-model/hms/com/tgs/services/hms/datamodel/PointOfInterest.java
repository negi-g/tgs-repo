package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class PointOfInterest {

	private String name;
	private String type;
	private String distance;
	private String geoCode;
	private String unit;
}
