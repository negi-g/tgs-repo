package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlTransient
public class DotwRequestCriteria {

	@XmlElement
	private String fromDate;
	@XmlElement
	private String toDate;
	@XmlElement
	private String currency;
	
	@XmlElement(name = "rooms")
	private DotwRoomList rooms;
	
	@XmlElement
	private String productId;
	
	@XmlElement
	private String customerReference;
	
}
