package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HotelAdditionalInfo {

	@CustomSerializedName(key = FieldName.HOTEL_OPINIONATED_CONTENT)
	private OpinionatedContent opinionatedContent;

	@CustomSerializedName(key = FieldName.LAST_MODIFIED_DATE_TIME)
	private LocalDateTime lastModifiedDateTime;

	@CustomSerializedName(key = FieldName.HOTEL_POINT_OF_INTEREST)
	@ToString.Exclude
	private List<PointOfInterest> pointOfInterests;

	@CustomSerializedName(key = FieldName.IS_DISABLED)
	private Boolean isDisabled;
}
