package com.tgs.services.hms.datamodel.tbo.search;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelDetailRequest extends TBOBaseRequest {

	private String ResultIndex;
	private String HotelCode;
	private String EndUserIp;
	private String TokenId;
	private String TraceId;
}
