package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;

@Getter
public enum CleartripOrderMapping {

	
	SUCCESS("SUCCESS"), FAILED("FAILED"), INITIALIZING("IN_PROGRESS");

	public String getStatus() {
		return this.getCode();
	}

	private String code;

	CleartripOrderMapping(String code) {
		this.code = code;
	}
}
