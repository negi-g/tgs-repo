package com.tgs.services.hms.datamodel.qtech;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter 
@Builder
@ToString
public class Travellers{
	
	private String salutation;
	private String first_name;
	private String last_name;
	private Integer age;
}