package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Builder;
import lombok.Data;
@Builder
@Data
public class CancelRQ {
	public Integer BookingId;
	public Integer BookingDetailId;
	public String CancelCode;
	public Integer CancelAll;
	public String Reason;
}
