package com.tgs.services.hms.datamodel;

import java.util.List;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelRegionAdditionalInfo {

	@CustomSerializedName(key = FieldName.HOTEL_REGION_ANCESTOR_INFO)
	private List<HotelRegionAncestorType> ancestors;

	@CustomSerializedName(key = FieldName.POLYGON_INFO)
	private String polygonInfo;
}
