package com.tgs.services.hms.datamodel;

import java.math.BigDecimal;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class RoomMiscInfo {

	@SerializedName("rix")
	private Integer roomIndex;

	@SerializedName("rpc")
	private String ratePlanCode;

	@SerializedName("rtc")
	private String roomTypeCode;

	@SerializedName("btps")
	@ToString.Exclude
	private List<RoomBedType> bedTypes;

	@SerializedName("sup")
	@ToString.Exclude
	private List<String> supplements;

	@SerializedName("rtn")
	private String roomTypeName;

	private RoomPrice price;

	@SerializedName("iuca")
	private boolean isUnderCancellationAllowedForAgent;

	@SerializedName("ads")
	private String allocationDetails;

	@SerializedName("rbid")
	private String roomBookingId;

	@SerializedName("nprb")
	private Integer numberOfPassengerNameRequiredForbooking;

	private String status;

	@SerializedName("irb")
	private Boolean isRoomBlocked;

	@SerializedName("nt")
	@ToString.Exclude
	private String notes;
	/*
	 * Expedia Only
	 */
	@SerializedName("ams")
	private List<String> amenities;


	/*
	 * For Desiya Only
	 */
	@SerializedName("tba")
	private BigDecimal totalBaseAmount;

	@SerializedName("td")
	private BigDecimal totalDiscount;

	@SerializedName("ttx")
	private BigDecimal totalTaxes;

	@SerializedName("ca")
	private Double commissionAmount;

	/*
	 * Agoda
	 */
	@SerializedName("rbl")
	private String roomBlockId;

	@SerializedName("li")
	private Integer lineItemId;

	@SerializedName("rc")
	private String rateChannel;

	@SerializedName("ex")
	private Double exclusive;

	private Double tax;
	private Double fees;

	@SerializedName("inc")
	private Double inclusive;

	private String model;

	@SerializedName("rt")
	private String rateType;

	@SerializedName("rp")
	private String ratePlan;

	@SerializedName("sch")
	private List<RoomSurcharge> surcharges;

	/*
	 * Inventory
	 * 
	 */
	@SerializedName("rpid")
	private List<Long> ratePlanIdList;

	@SerializedName("ss")
	private String secondarySupplier;
	
	@SerializedName("irtbc")
	private Boolean isRoomToBeCancelled;

	public BigDecimal getTotalBaseAmount() {
		if (totalBaseAmount == null)
			return BigDecimal.ZERO;
		return totalBaseAmount;
	}

	public BigDecimal getTotalTaxes() {
		if (totalTaxes == null)
			return BigDecimal.ZERO;
		return totalTaxes;
	}

	public BigDecimal getTotalDiscount() {
		if (totalDiscount == null)
			return BigDecimal.ZERO;
		return totalDiscount;
	}

	public Double getCommissionAmount() {
		if (commissionAmount == null)
			return 0.0;
		return commissionAmount;
	}

	@APIUserExclude
	@CustomSerializedName(key = FieldName.CATEGORY_ID)
	private String categoryId;

}
