package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Data;

@Data
public class BookingDetailRS {
	public HotelBookOption HotelOption;
	public String Currency;
}
