package com.tgs.services.hms.datamodel.expedia;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Promotions {

	private Deal deal;
	
	private Map<String , ValueAdds> value_adds;
}
