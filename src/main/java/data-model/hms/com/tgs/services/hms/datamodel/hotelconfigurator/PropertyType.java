package com.tgs.services.hms.datamodel.hotelconfigurator;

public enum PropertyType {

	GUESTHOUSE,
	APARTMENT,
	HOSTEL,
	RESORT,
	HOMESTAY,
	VILLA,
	FARMHOUSE,
	HOTEL
}
