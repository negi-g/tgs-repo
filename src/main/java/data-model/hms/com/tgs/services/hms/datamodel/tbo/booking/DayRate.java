
package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Data;

@Data
public class DayRate {

    public Integer Amount;
    public String Date;

}
