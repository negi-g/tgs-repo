package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Zone {

	public Integer zoneCode;
	public String name;
	public Description description;
}
