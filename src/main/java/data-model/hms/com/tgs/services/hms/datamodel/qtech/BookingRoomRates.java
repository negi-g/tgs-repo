package com.tgs.services.hms.datamodel.qtech;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BookingRoomRates {
	
	private String Date;
	private String Day;
	private String DisplayNightlyRate;
	
}

