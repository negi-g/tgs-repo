package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class HotelMiscInfo {

	/*
	 * This is user to map search result with static data
	 */
	@APIUserExclude
	@CustomSerializedName(key = FieldName.SUPPLIER_STATIC_HOTEL_ID)
	private String supplierStaticHotelId;

	@CustomSerializedName(key = FieldName.SEARCH_KEY_EXPIRY_TIME)
	private LocalDateTime searchKeyExpiryTime;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.SUPPLIER_BOOKING_REFERENCE)
	private String supplierBookingReference;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.SUPPLIER_BOOKING_ID)
	private String supplierBookingId;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.SUPPLIER_BOOKING_CONFIRMATION)
	private String supplierBookingConfirmationNo;

	@CustomSerializedName(key = FieldName.HOTEL_BOOKING_REFERENCE)
	private String hotelBookingReference;

	/*
	 * This is TGS searchId
	 */
	@APIUserExclude
	@CustomSerializedName(key = FieldName.SEARCH_ID)
	private String searchId;

	@CustomSerializedName(key = FieldName.HOTEL_BOOKING_CANCELLATION_REFERENCE)
	private String hotelBookingCancellationReference;

	@CustomSerializedName(key = FieldName.IS_MEAL_ALREADY_MAPPED)
	private Boolean isMealAlreadyMapped;

	/*
	 * DESIYA
	 */
	@APIUserExclude
	private String correlationId;

	/*
	 * User Only For AGODA
	 */
	@CustomSerializedName(key = FieldName.SUPPLIER_BOOKING_URL)
	private String supplierBookingUrl;
	@APIUserExclude
	@CustomSerializedName(key = FieldName.CREDIT_CARD_ID)
	private Long creditCardAppliedId;

	@SerializedName("ipr")
	private Boolean isPanRequired;

	// this field is currently used for agoda to check if the booking status needs to be checked
	@APIUserExclude
	@SerializedName("ifbs")
	private Boolean isFailedFromSupplier;

	@APIUserExclude
	@SerializedName("tsb")
	private String tempSupplierBookingId;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.IS_OPINIONATED_CONTENT_REQUIRED)
	private Boolean isOpinionatedContentRequired;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.SUPPLIER_BNPL_STATUS)
	private String supplierBnplStatus;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.IS_BNPL_BOOKING)
	private Boolean isBnplBooking;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.IS_SUPPLIER_MAPPED)
	private Boolean isSupplierMapped;

	@APIUserExclude
	@SerializedName("sbfr")
	private String supplierBookingFailedReason;

	@APIUserExclude
	@SerializedName("at")
	private String alertType;

	@APIUserExclude
	@SerializedName("iqp")
	private Boolean isQuarantinePackage;
}
