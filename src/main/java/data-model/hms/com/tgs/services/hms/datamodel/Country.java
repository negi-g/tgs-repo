package com.tgs.services.hms.datamodel;



import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Country{
	private String code;
	
	@SearchPredicate(type = PredicateType.EQUAL, filterName = "country",  destinationEntity = "HotelSupplierInfo", dbAttribute = "credentialInfo.add.country")
	private String name;
	
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if (getClass() != obj.getClass()) { return false; }
		Country country = (Country)obj;
		
		if(!Objects.equals(this.getName(), country.getName()))
			return false;
		return true;
	}

	public boolean isEmpty() {

		return StringUtils.isBlank(code) && StringUtils.isBlank(name);
	}

}