package com.tgs.services.hms.datamodel.fitruums.book;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import com.tgs.services.hms.datamodel.fitruums.FitruumsBaseResponse;

import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "code", "cancellationPaymentMethod" })
@XmlRootElement(name = "result")
@Getter
@Setter
public class BookingCancellationResult extends FitruumsBaseResponse {

	@XmlElement(name = "Code")
	@XmlSchemaType(name = "unsignedByte")
	protected short code;
	@XmlElement(name = "CancellationPaymentMethod", required = true)
	protected BookingCancellationResult.CancellationPaymentMethod cancellationPaymentMethod;

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "cancellationfee", "cancellation" })
	@Getter
	@Setter
	public static class CancellationPaymentMethod {

		@XmlElement(required = true)
		protected List<BookingCancellationResult.CancellationPaymentMethod.Cancellationfee> cancellationfee;
		@XmlElement(required = true)
		protected BookingCancellationResult.CancellationPaymentMethod.Cancellation cancellation;
		@XmlAttribute(name = "id", required = true)
		@XmlSchemaType(name = "unsignedByte")
		protected short id;
		@XmlAttribute(name = "name", required = true)
		protected String name;

		public List<BookingCancellationResult.CancellationPaymentMethod.Cancellationfee> getCancellationfee() {
			if (cancellationfee == null) {
				cancellationfee = new ArrayList<BookingCancellationResult.CancellationPaymentMethod.Cancellationfee>();
			}
			return this.cancellationfee;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "activecancellationpolicy" })
		@Getter
		@Setter
		public static class Cancellation {

			@XmlElement(required = true)
			protected BookingCancellationResult.CancellationPaymentMethod.Cancellation.Activecancellationpolicy activecancellationpolicy;
			@XmlAttribute(name = "type", required = true)
			protected String type;

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "deadline", "percentage" })
			@Getter
			@Setter
			public static class Activecancellationpolicy {

				@XmlElement(required = true, nillable = true)
				protected Object deadline;
				@XmlSchemaType(name = "unsignedByte")
				protected short percentage;

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "value" })
		@Getter
		@Setter
		public static class Cancellationfee {

			@XmlValue
			protected BigDecimal value;
			@XmlAttribute(name = "currency", required = true)
			protected String currency;

		}

	}

}
