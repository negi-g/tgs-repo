package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Bed {

	private String Type;
	private String Description;
	private String Size;
	private Integer Count;
}
