package com.tgs.services.hms.datamodel.dotw;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwFilterComplexCriteria {

	@XmlElement(namespace = "http://us.dotwconnect.com/xsd/atomicCondition")
	private List<DotwFilterCriteria> condition;
	
	
	public List<DotwFilterCriteria> getFilterCriteria(){
		
		if(condition == null) {
			condition = new ArrayList<>();
		}
		return condition;
	}
}
