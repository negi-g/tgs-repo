package com.tgs.services.hms.datamodel.tbo.staticData;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Position")
@Getter
@Setter
public class Position {

    @XmlAttribute(name = "Latitude", required = false)
    protected BigDecimal latitude;
    @XmlAttribute(name = "Longitude", required = false)
    protected BigDecimal longitude;

}
