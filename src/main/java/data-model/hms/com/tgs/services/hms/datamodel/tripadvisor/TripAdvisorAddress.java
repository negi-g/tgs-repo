package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TripAdvisorAddress {
	private String street1;
	private String street2;
	private String city;
	private String state;
	private String country;
	private String postalcode;
	private String address_string;

}
