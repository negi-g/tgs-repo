package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoomSurcharge {

	private String id;
	private String charge;
	private String name;
	private Rate rate;
}
