package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorListResponse")
@Data
public class ErrorListResponse {

	@XmlAttribute(name = "hotelId", required = true)
	protected BigInteger hotelId;
	@XmlAttribute(name = "roomId")
	protected BigInteger roomId;
	@XmlAttribute(name = "uid", required = true)
	protected String uid;
	@XmlAttribute(name = "code", required = true)
	protected BigInteger code;
	@XmlAttribute(name = "message", required = true)
	protected String message;

}