package com.tgs.services.hms.datamodel.inventory;

import com.tgs.services.hms.datamodel.HotelInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class MappedHotelInfo {
	
	private HotelInfo hInfo;
	private String supplierHotelId;

}
