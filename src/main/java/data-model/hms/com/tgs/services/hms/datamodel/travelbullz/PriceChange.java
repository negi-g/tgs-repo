package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Data;

@Data
public class PriceChange {
	public Double OldPrice;
	public Double NewPrice;
}
