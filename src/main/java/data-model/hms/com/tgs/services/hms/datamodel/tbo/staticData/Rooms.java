package com.tgs.services.hms.datamodel.tbo.staticData;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import lombok.Getter;
import lombok.Setter;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "room"
})
@XmlRootElement(name = "Rooms")
@Getter
@Setter
public class Rooms {

    @XmlElement(name = "Room", required = false)
    protected List<Rooms.Room> room;

    public List<Rooms.Room> getRoom() {
        if (room == null) {
            room = new ArrayList<Rooms.Room>();
        }
        return this.room;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hotelCode",
        "roomTypeName",
        "roomId",
        "maxOccupancy",
        "minOccupancy",
        "allowExtraBed",
        "noOfExtraBed",
        "roomSizeFeet",
        "roomSizeMeter",
        "faciltities",
        "roomViews",
        "bedTypes",
        "roomImages",
        "roomDescription"
    })
    @Getter
    @Setter
    public static class Room {

        @XmlElement(name = "HotelCode")
        @XmlSchemaType(name = "unsignedInt")
        protected long hotelCode;
        @XmlElement(name = "RoomTypeName", required = false)
        protected String roomTypeName;
        @XmlElement(name = "RoomId")
        @XmlSchemaType(name = "unsignedShort")
        protected int roomId;
        @XmlElement(name = "MaxOccupancy")
        @XmlSchemaType(name = "unsignedByte")
        protected short maxOccupancy;
        @XmlElement(name = "MinOccupancy")
        @XmlSchemaType(name = "unsignedByte")
        protected short minOccupancy;
        @XmlElement(name = "AllowExtraBed", required = false)
        protected String allowExtraBed;
        @XmlElement(name = "NoOfExtraBed")
        @XmlSchemaType(name = "unsignedByte")
        protected short noOfExtraBed;
        @XmlElement(name = "RoomSizeFeet")
        @XmlSchemaType(name = "unsignedShort")
        protected int roomSizeFeet;
        @XmlElement(name = "RoomSizeMeter")
        @XmlSchemaType(name = "unsignedByte")
        protected short roomSizeMeter;
        @XmlElement(name = "Faciltities", required = false)
        protected Rooms.Room.Faciltities faciltities;
        @XmlElement(name = "RoomViews", required = false)
        protected Rooms.Room.RoomViews roomViews;
        @XmlElement(name = "BedTypes", required = false)
        protected Rooms.Room.BedTypes bedTypes;
        @XmlElement(name = "RoomImages", required = false)
        protected Rooms.Room.RoomImages roomImages;
        @XmlElement(name = "RoomDescription", required = false)
        protected Rooms.Room.RoomDescription roomDescription;

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "bedType"
        })
        @Getter
        @Setter
        public static class BedTypes {

            @XmlElement(name = "BedType", required = false)
            protected Rooms.Room.BedTypes.BedType bedType;

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "bedID",
                "bedName",
                "bedSize",
                "quantity"
            })
            @Getter
            @Setter
            public static class BedType {

                @XmlElement(name = "BedID")
                @XmlSchemaType(name = "unsignedByte")
                protected short bedID;
                @XmlElement(name = "BedName", required = false)
                protected String bedName;
                @XmlElement(name = "BedSize", required = false)
                protected Object bedSize;
                @XmlElement(name = "Quantity")
                @XmlSchemaType(name = "unsignedByte")
                protected short quantity;

            }

        }


        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "roomFacility"
        })
        @Getter
        @Setter
        public static class Faciltities {

            @XmlElement(name = "RoomFacility", required = false)
            protected List<Rooms.Room.Faciltities.RoomFacility> roomFacility;

            public List<Rooms.Room.Faciltities.RoomFacility> getRoomFacility() {
                if (roomFacility == null) {
                    roomFacility = new ArrayList<Rooms.Room.Faciltities.RoomFacility>();
                }
                return this.roomFacility;
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "facilityId",
                "facilityName"
            })
            @Getter
            @Setter
            public static class RoomFacility {

                @XmlElement(name = "FacilityId")
                @XmlSchemaType(name = "unsignedByte")
                protected short facilityId;
                @XmlElement(name = "FacilityName", required = false)
                protected String facilityName;
            }

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "subSection"
        })
        @Getter
        @Setter
        public static class RoomDescription {

            @XmlElement(name = "SubSection", required = false)
            protected Rooms.Room.RoomDescription.SubSection subSection;

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "paragraph"
            })
            @Getter
            @Setter
            public static class SubSection {

                @XmlElement(name = "Paragraph", required = false)
                protected Rooms.Room.RoomDescription.SubSection.Paragraph paragraph;
                @XmlAttribute(name = "SubTitle", required = false)
                protected String subTitle;

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "text"
                })
                @Getter
                @Setter
                public static class Paragraph {

                    @XmlElement(name = "Text", required = false)
                    protected Rooms.Room.RoomDescription.SubSection.Paragraph.Text text;

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "value"
                    })
                    @Getter
                    @Setter
                    public static class Text {

                        @XmlValue
                        protected String value;
                        @XmlAttribute(name = "TextFormat", required = false)
                        protected String textFormat;

                    }

                }

            }

        }


        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "roomImage"
        })
        @Getter
        @Setter
        public static class RoomImages {

            @XmlElement(name = "RoomImage", required = false)
            protected List<Rooms.Room.RoomImages.RoomImage> roomImage;

            public List<Rooms.Room.RoomImages.RoomImage> getRoomImage() {
                if (roomImage == null) {
                    roomImage = new ArrayList<Rooms.Room.RoomImages.RoomImage>();
                }
                return this.roomImage;
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "imageUrl",
                "imageType"
            })
            @Getter
            @Setter
            public static class RoomImage {

                @XmlElement(name = "ImageUrl", required = false)
                protected String imageUrl;
                @XmlElement(name = "ImageType")
                @XmlSchemaType(name = "unsignedByte")
                protected short imageType;
            }

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "roomView"
        })
        @Getter
        @Setter
        public static class RoomViews {

            @XmlElement(name = "RoomView", required = false)
            protected Rooms.Room.RoomViews.RoomView roomView;

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "viewID",
                "viewName"
            })
            @Getter
            @Setter
            public static class RoomView {

                @XmlElement(name = "ViewID")
                @XmlSchemaType(name = "unsignedByte")
                protected short viewID;
                @XmlElement(name = "ViewName", required = false)
                protected String viewName;
            }

        }

    }

}
