package com.tgs.services.hms.datamodel.fitruums;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "roomtype" })

@Getter
@Setter
public class Roomtypes {

	@XmlElement(required = true)
	protected List<Roomtype> roomtype;

	public List<Roomtype> getRoomtype() {
		if (roomtype == null) {
			roomtype = new ArrayList<Roomtype>();
		}
		return this.roomtype;
	}
}