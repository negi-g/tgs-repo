package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class HotelSupplierRegionInfo {

	private Long id;
	private String regionId;
	private String regionName;
	private String stateId;
	private String stateName;
	private String countryId;
	private String countryName;
	private String supplierName;
	private String supplierRegionName;
	private String supplierRegionKey;
	private String regionType;
	private HotelRegionAdditionalInfo additionalInfo;
	private String fullRegionName;
	private LocalDateTime createdOn;
	private LocalDateTime processedOn;
}
