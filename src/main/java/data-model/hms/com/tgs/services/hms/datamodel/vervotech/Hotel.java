package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Hotel {

	private String ProviderHotelId;
	private String ProviderFamily;
}