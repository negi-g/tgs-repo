package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelList {

	private Integer hotelId;
	private String hotelName;
	private String url;
}
