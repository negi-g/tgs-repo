package com.tgs.services.hms.datamodel.fitruums;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "cancellationPolicy" })
public class CancellationPolicies {

	@XmlElement(name = "cancellation_policy", required = true)

	protected List<CancellationPolicy> cancellationPolicy;

	public List<CancellationPolicy> getCancellationPolicy() {
		if (cancellationPolicy == null) {
			cancellationPolicy = new ArrayList<CancellationPolicy>();
		}
		return this.cancellationPolicy;
	}

}
