package com.tgs.services.hms.datamodel;

import lombok.Data;

@Data
public class Rate {

	protected double exclusive;
	protected double tax;
	protected double fees;
	protected double inclusive;
}
