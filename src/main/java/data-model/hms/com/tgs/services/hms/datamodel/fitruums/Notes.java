package com.tgs.services.hms.datamodel.fitruums;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"note"})
@Getter
@Setter
public class Notes {

	@XmlElement(name = "Note", required = true)
	protected List<Note> note;

	public List<Note> getNote() {
		if (note == null) {
			note = new ArrayList<>();
		}
		return this.note;
	}

}
