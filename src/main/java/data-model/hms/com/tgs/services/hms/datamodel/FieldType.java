package com.tgs.services.hms.datamodel;

public enum FieldType {
	HOTELNAME, SUPPLIER_NAME, SUPPLIER_HOTELID;
}
