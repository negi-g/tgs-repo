package com.tgs.services.hms.datamodel;



import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GeoLocation {

	@CustomSerializedName(key = FieldName.HOTEL_LONGITUDE)
	private String longitude;
	@CustomSerializedName(key = FieldName.HOTEL_LATITUDE)
	private String latitude;
}