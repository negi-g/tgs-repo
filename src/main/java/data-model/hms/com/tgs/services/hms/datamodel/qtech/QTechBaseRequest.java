package com.tgs.services.hms.datamodel.qtech;

import java.util.HashMap;
import java.util.Map;

import com.tgs.services.base.helper.Exclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QTechBaseRequest {

	@Exclude
	private Map<String, String> headerParams;
	private String action;
	private String username;
	private String password;
	private String gzip;

	public void setGzip(String gzip) {
		this.gzip = "no";
	}

	public Map<String, String> getHeaderParams() {
		if (headerParams == null) {
			headerParams = new HashMap<>();
		}
		if (isCompressionEnabled()) {
			headerParams.put("Accept-Encoding", "gzip");
		}
		return headerParams;
	}

	public Boolean isCompressionEnabled() {
		return true;
	}
}
