package com.tgs.services.hms.datamodel.tbo.mapping;

import javax.xml.bind.annotation.XmlElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Country {

	@XmlElement
	private String Code;
	@XmlElement
	private String Name;
	
	
}
