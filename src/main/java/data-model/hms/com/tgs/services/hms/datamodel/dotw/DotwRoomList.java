package com.tgs.services.hms.datamodel.dotw;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="rooms")
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwRoomList {
	
	@XmlAttribute
	private Integer no;
	
	private List<DotwRoomRequest> room;

}
