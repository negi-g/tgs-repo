package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Data;

@Data
public class HotelResult {
	public Double StartPrice;
	public Integer HotelId;
	public List<HotelOption> HotelOption ;
}
