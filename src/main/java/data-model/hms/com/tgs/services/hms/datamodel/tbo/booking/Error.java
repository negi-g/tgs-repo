
package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Data;

@Data
public class Error {

    public Integer ErrorCode;
    public String ErrorMessage;

}
