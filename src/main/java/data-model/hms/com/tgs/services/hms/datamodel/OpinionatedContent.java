package com.tgs.services.hms.datamodel;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OpinionatedContent {

	private String name;

	@CustomSerializedName(key = FieldName.HOTEL_GEOLOCATION)
	private GeoLocation geolocation;

	@CustomSerializedName(key = FieldName.HOTEL_ADDRESS)
	private Address address;

	@CustomSerializedName(key = FieldName.HOTEL_RATING)
	private String rating;

	@CustomSerializedName(key = FieldName.HOTEL_PROPERTY_TYPE)
	private String propertyType;
}
