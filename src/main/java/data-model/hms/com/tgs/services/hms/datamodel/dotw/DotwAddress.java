package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="fullAddress")
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwAddress {

	@XmlElement(name = "hotelStreetAddress")
	private String addressLine1;
	
	@XmlElement(name = "hotelZipCode")
	private String hotelZipCode;
	
	@XmlElement(name = "hotelCountry")
	private String hotelCountry;
	
	@XmlElement(name = "hotelState")
	private String hotelState;
	
	@XmlElement(name = "hotelCity")
	private String hotelCity;
	
}
