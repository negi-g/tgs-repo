package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class Travellers {
	
	private String Title;
	private String FirstName;
	private String Middlename;
	private String LastName;
	private String Phoneno;
	private String Email;
	private Integer PaxType;
	private Boolean LeadPassenger;
	private Integer Age;
	private String PAN;
	

}
