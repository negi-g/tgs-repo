package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelDetail {

	private String hotelId;
	private String address;
	private String city;
	private String checkInDate;
	private String checkOutDate;
}
