package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Checkin {

	private String BeginTime;
	private String EndTime;
	private List<String> Instructions;
	private List<String> SpecialInstructions;
	private Integer MinAge;
}