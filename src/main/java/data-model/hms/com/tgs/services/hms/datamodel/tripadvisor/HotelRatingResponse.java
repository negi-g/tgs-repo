package com.tgs.services.hms.datamodel.tripadvisor;

import java.util.HashMap;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRatingResponse {

	private TripAdvisorAddress address_obj;
	private String latitude;
	private String longitude;
	private String rating;
	private String rating_image_url;
	private String location_id;
	private List<TripType> trip_types;
	private String write_review;
	private List<Ancestor> ancestors;
	private String hours;
	private String percent_recommended;
	private HotelBooking hotel_booking;
	private HashMap<Integer , Integer> review_rating_count;
	private List<Rating> subratings;
	private RankingData ranking_data;
	private Integer num_reviews;
	private String name;
	private String web_url;
	private TripAdvisorPropertyCategory category;
}
