
package com.tgs.services.hms.datamodel.fitruums.book;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;
import com.tgs.services.hms.datamodel.fitruums.FitruumsBaseResponse;
import lombok.Getter;
import lombok.Setter;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"error", "notes", "preBookCode", "price", "cancellationPolicies"})
@XmlRootElement(name = "preBookResult")
@Getter
@Setter
public class PreBookResult extends FitruumsBaseResponse {
	@XmlElement(name = "Error", required = true)
	protected Error error;
	@XmlElement(name = "Notes", required = true)
	protected PreBookResult.Notes notes;
	@XmlElement(name = "PreBookCode", required = true)
	protected String preBookCode;
	@XmlElement(name = "Price", required = true)
	protected PreBookResult.Price price;
	@XmlElement(name = "CancellationPolicies", required = true)
	protected CancellationPolicies cancellationPolicies;

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {"note"})
	@Getter
	@Setter
	public static class Notes {

		@XmlElement(name = "Note", required = true)
		protected List<PreBookResult.Notes.Note> note;

		public List<PreBookResult.Notes.Note> getNote() {
			if (note == null) {
				note = new ArrayList<PreBookResult.Notes.Note>();
			}
			return this.note;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"text"})
		@Getter
		@Setter
		public static class Note {

			@XmlElement(required = true)
			protected String text;
			@XmlAttribute(name = "start_date", required = true)
			@XmlSchemaType(name = "dateTime")
			protected XMLGregorianCalendar startDate;
			@XmlAttribute(name = "end_date", required = true)
			@XmlSchemaType(name = "dateTime")
			protected XMLGregorianCalendar endDate;
		}
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {"value"})
	@Getter
	@Setter
	public static class Price {

		@XmlValue
		@XmlSchemaType(name = "unsignedShort")
		protected int value;
		@XmlAttribute(name = "currency", required = true)
		protected String currency;
	}
}
