package com.tgs.services.hms.datamodel.qtech;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelAmenities {
	
	private String AmenityName;
	
}