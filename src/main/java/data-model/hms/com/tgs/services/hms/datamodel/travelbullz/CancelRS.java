package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Data;

@Data
public class CancelRS {
	public HotelCancellationOption HotelOption;
	public String Currency;
}
