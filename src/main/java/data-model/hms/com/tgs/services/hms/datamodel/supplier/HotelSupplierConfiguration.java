package com.tgs.services.hms.datamodel.supplier;

import com.tgs.services.hms.datamodel.HotelUrlConstants;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@ApiModel(value = "To provide hotel supplier configuration such as hotel basic info, credentials and additional info.")
public class HotelSupplierConfiguration {
	

	@ApiModelProperty(notes = "To provide hotel credentials", example = "{'username':'abc', 'password':'123', 'url': 'https://xyz.com'}")
	private HotelSupplierCredential hotelSupplierCredentials;
	
	@ApiModelProperty(notes = "To provide hotel supplier basic info. Don't use this variable while sending any API call to backend.", example = "{'username':'abc', 'password':'123', 'url': 'https://xyz.com'}")
	private HotelSupplierBasicInfo basicInfo;
	
	@ApiModelProperty(notes = "To find if supplier is Enabled. Don't use this variable while sending any API call to backend.", example = "true")
	private Boolean enabled;
	
	public String getHotelAPIUrl(HotelUrlConstants api) {
		
		if(hotelSupplierCredentials != null) return hotelSupplierCredentials.getSupplierUrl().get(api);
		return null;
	}
}
