package com.tgs.services.hms.datamodel.agoda.statc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="child_and_extra_bed_policy")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaChildAndExtraBedPolicy {
	
	@XmlElement(name = "infant_age")
    protected Integer infantAge;
	
    @XmlElement(name = "children_age_from")
    protected Integer childrenAgeFrom;
    
    @XmlElement(name = "children_age_to")
    protected Integer childrenAgeTo;
    
    @XmlElement(name = "children_stay_free")
    protected Boolean childrenStayFree;
    
    @XmlElement(name = "min_guest_age")
    protected Integer minGuestAge;

}
