package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RateCheckRoomRequest {

    private String rateKey;
}
