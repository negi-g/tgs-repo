package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class AgentBookingTransactionQuery {

	private String agentName;

	private Double grossSale;

	private String agentCurrency;

	private Double grossSaleInBaseCurrency;

	private String systemCurrency;

	private Integer noOfBookings;

}
