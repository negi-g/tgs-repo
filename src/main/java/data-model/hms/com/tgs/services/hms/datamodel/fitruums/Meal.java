package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "id", "labelId", "prices", "discount" })
public class Meal {

	@XmlSchemaType(name = "unsignedByte")
	protected short id;
	@XmlElement(required = true, nillable = true)
	protected Object labelId;
	@XmlElement(required = true)
	protected Prices prices;
	@XmlElement(required = true, nillable = true)
	protected Object discount;

}
