package com.tgs.services.hms.datamodel;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class AdditionalHotelOrderItemInfo {

	private String propertyType;

	@SerializedName("phi")
	private ProcessedHotelInfo hInfo;

	@SerializedName("sbid")
	private String supplierBookingReference;

	@SerializedName("sbr")
	private String supplierBookingId;

	@SerializedName("sbcn")
	private String supplierBookingConfirmationNo;

	@SerializedName("si")
	private String supplierId;

	@SerializedName("hbr")
	private String hotelBookingReference;

	@SerializedName("bcr")
	private String bookingCancellationReference;

	@SerializedName("sbu")
	private String supplierBookingUrl;

	@SerializedName("cci")
	private Long creditCardAppliedId;

	// Agoda Specific
	@SerializedName("ifbs")
	private Boolean isFailedFromSupplier;

	@SerializedName("tsb")
	private String tempSupplierBookingId;

	@SerializedName("hosis")
	private List<HotelOrderSupplierInfo> hotelOrderSupplierInfos;

	@SerializedName("sbnpls")
	private String supplierBnplStatus;

	@SerializedName("ibnplb")
	private Boolean isBnplBooking;

	@SerializedName("sbfr")
	private String supplierBookingFailedReason;

	@SerializedName("at")
	private String alertType;

	@SerializedName("iqp")
	private Boolean isQuarantinePackage;

	public void addHotelOrderSupplierInfo(HotelOrderSupplierInfo hotelOrderSupplierInfo) {
		if (hotelOrderSupplierInfos == null) {
			hotelOrderSupplierInfos = new ArrayList<HotelOrderSupplierInfo>();
		}
		hotelOrderSupplierInfos.add(hotelOrderSupplierInfo);
	}
}
