package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchExclusionOutput implements IRuleOutPut {

	@SerializedName("uids")
	private List<String> unicaIds;

}
