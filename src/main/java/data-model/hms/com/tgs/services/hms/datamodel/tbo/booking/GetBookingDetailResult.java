package com.tgs.services.hms.datamodel.tbo.booking;

import java.util.List;
import lombok.Data;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomDetail;


@Data
public class GetBookingDetailResult {

	public Boolean VoucherStatus;
	public Integer ResponseStatus;
	public Error Error;
	public String TraceId;
	public Integer Status;
	public String HotelBookingStatus;
	public String ConfirmationNo;
	public String BookingRefNo;
	public Integer BookingId;
	public Boolean IsPriceChanged;
	public Boolean IsCancellationPolicyChanged;
	public List<HotelRoomDetail> HotelRoomsDetails = null;
	public String HotelPolicyDetail;
	public String InvoiceCreatedOn;
	public String InvoiceNo;
	public String HotelConfirmationNo;
	public String HotelName;
	public Integer StarRating;
	public String AddressLine1;
	public String AddressLine2;
	public String Latitude;
	public String Longitude;
	public String City;
	public String CheckInDate;
	public String CheckOutDate;
	public String LastCancellationDate;
	public Integer NoOfRooms;
	public String BookingDate;
	public String SpecialRequest;
	public Boolean IsDomestic;
	public String AgentReferenceNo;
}
