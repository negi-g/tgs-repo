package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Contact {

	private Address Address;
	private List<String> Phones;
	private String Fax;
	private List<String> Emails;
	private String Web;
	private String FormattedAddress;
}