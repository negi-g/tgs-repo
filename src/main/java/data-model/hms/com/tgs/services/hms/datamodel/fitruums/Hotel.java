package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "hotelId", "destinationId", "resortId", "transfer", "roomtypes", "notes", "distance",
		"codes" })
@Getter
@Setter
public class Hotel {

	@XmlElement(name = "hotel.id")
	@XmlSchemaType(name = "unsignedInt")
	protected long hotelId;
	@XmlElement(name = "destination_id")
	@XmlSchemaType(name = "unsignedShort")
	protected int destinationId;
	@XmlElement(name = "resort_id")
	@XmlSchemaType(name = "unsignedShort")
	protected int resortId;
	@XmlSchemaType(name = "unsignedByte")
	protected short transfer;
	@XmlElement(required = true)
	protected Roomtypes roomtypes;
	@XmlElement(required = true)
	protected Notes notes;
	@XmlElement(required = true, nillable = true)
	protected Object distance;
	@XmlElement(required = true)
	protected Object codes;
}
