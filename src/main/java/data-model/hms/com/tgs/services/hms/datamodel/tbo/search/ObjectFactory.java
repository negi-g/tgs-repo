package com.tgs.services.hms.datamodel.tbo.search;

import javax.xml.bind.annotation.XmlRegistry;
import com.tgs.services.hms.datamodel.tbo.search.ArrayOfBasicPropertyInfo.BasicPropertyInfo;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public ArrayOfBasicPropertyInfo createArrayOfBasicPropertyInfo() {
        return new ArrayOfBasicPropertyInfo();
    }
    
    public BasicPropertyInfo createArrayOfBasicPropertyInfoBasicPropertyInfo() {
        return new ArrayOfBasicPropertyInfo.BasicPropertyInfo();
    }

}
