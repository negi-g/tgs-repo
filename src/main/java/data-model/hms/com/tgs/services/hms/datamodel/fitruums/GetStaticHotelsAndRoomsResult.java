
package com.tgs.services.hms.datamodel.fitruums;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.tgs.services.hms.datamodel.fitruums.book.BookResult;

import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "hotels" })
@XmlRootElement(name = "getStaticHotelsAndRoomsResult")
@Getter
@Setter
public class GetStaticHotelsAndRoomsResult extends FitruumsBaseResponse {

	@XmlElement(required = true)
	protected GetStaticHotelsAndRoomsResult.Hotels hotels;

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "hotel" })
	@Getter
	@Setter
	public static class Hotels {

		@XmlElement(required = true)
		protected List<GetStaticHotelsAndRoomsResult.Hotels.Hotel> hotel;

		public List<GetStaticHotelsAndRoomsResult.Hotels.Hotel> getHotel() {
			if (hotel == null) {
				hotel = new ArrayList<GetStaticHotelsAndRoomsResult.Hotels.Hotel>();
			}
			return this.hotel;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "hotelId", "destinationId", "resortId", "transfer", "roomtypes", "notes",
				"distance", "codes", "type", "name", "hotelAddr1", "hotelAddr2", "hotelAddrZip", "hotelAddrCity",
				"hotelAddrState", "hotelAddrCountry", "hotelAddrCountrycode", "hotelAddress", "hotelMapurl", "headline",
				"description", "resort", "destination", "themes", "images", "features", "classification", "coordinates",
				"distanceTypes", "timeZone", "isBestBuy" })
		@Getter
		@Setter
		public static class Hotel {

			@XmlElement(name = "hotel.id")
			@XmlSchemaType(name = "unsignedInt")
			protected long hotelId;
			@XmlElement(name = "destination_id")
			@XmlSchemaType(name = "unsignedShort")
			protected int destinationId;
			@XmlElement(name = "resort_id")
			@XmlSchemaType(name = "unsignedShort")
			protected int resortId;
			@XmlSchemaType(name = "unsignedByte")
			protected short transfer;
			@XmlElement(required = true)
			protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.Roomtypes roomtypes;
			@XmlElement(required = true)
			protected Object notes;
			@XmlElement(required = true, nillable = true)
			protected Object distance;
			@XmlElement(required = true)
			protected Object codes;
			@XmlElement(required = true)
			protected String type;
			@XmlElement(required = true)
			protected String name;
			@XmlElement(name = "hotel.addr.1", required = true)
			protected String hotelAddr1;
			@XmlElement(name = "hotel.addr.2", required = true)
			protected Object hotelAddr2;
			@XmlElement(name = "hotel.addr.zip")
			@XmlSchemaType(name = "unsignedShort")
			protected int hotelAddrZip;
			@XmlElement(name = "hotel.addr.city", required = true)
			protected String hotelAddrCity;
			@XmlElement(name = "hotel.addr.state", required = true)
			protected Object hotelAddrState;
			@XmlElement(name = "hotel.addr.country", required = true)
			protected String hotelAddrCountry;
			@XmlElement(name = "hotel.addr.countrycode", required = true)
			protected String hotelAddrCountrycode;
			@XmlElement(name = "hotel.address", required = true)
			protected String hotelAddress;
			@XmlElement(name = "hotel.mapurl", required = true)
			protected String hotelMapurl;
			@XmlElement(required = true)
			protected String headline;
			@XmlElement(required = true)
			protected String description;
			@XmlElement(required = true)
			protected String resort;
			@XmlElement(required = true)
			protected String destination;
			@XmlElement(required = true)
			protected Object themes;
			@XmlElement(required = true)
			protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.Images images;
			@XmlElement(required = true)
			protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.Features features;
			@XmlSchemaType(name = "unsignedByte")
			protected short classification;
			@XmlElement(required = true)
			protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.Coordinates coordinates;
			@XmlElement(required = true)
			protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.DistanceTypes distanceTypes;
			@XmlElement(required = true)
			protected String timeZone;
			protected boolean isBestBuy;

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "latitude", "longitude" })
			@Getter
			@Setter
			public static class Coordinates {

				@XmlElement(required = true)
				protected BigDecimal latitude;
				@XmlElement(required = true)
				protected BigDecimal longitude;

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "distanceType" })
			@Getter
			@Setter
			public static class DistanceTypes {

				@XmlElement(required = true)
				protected List<GetStaticHotelsAndRoomsResult.Hotels.Hotel.DistanceTypes.DistanceType> distanceType;

				public List<GetStaticHotelsAndRoomsResult.Hotels.Hotel.DistanceTypes.DistanceType> getDistanceType() {
					if (distanceType == null) {
						distanceType = new ArrayList<GetStaticHotelsAndRoomsResult.Hotels.Hotel.DistanceTypes.DistanceType>();
					}
					return this.distanceType;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "hotelDistanceTypeId", "description", "distances" })
				@Getter
				@Setter
				public static class DistanceType {

					@XmlSchemaType(name = "unsignedByte")
					protected short hotelDistanceTypeId;
					@XmlElement(required = true)
					protected String description;
					@XmlElement(required = true)
					protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.DistanceTypes.DistanceType.Distances distances;

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "distance" })
					@Getter
					@Setter
					public static class Distances {

						@XmlElement(required = true)
						protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.DistanceTypes.DistanceType.Distances.Distance distance;

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "distanceInMeters", "placeName", "coordinates" })
						@Getter
						@Setter
						public static class Distance {

							@XmlElement(required = true)
							protected BigDecimal distanceInMeters;
							@XmlElement(required = true, nillable = true)
							protected String placeName;
							@XmlElement(required = true, nillable = true)
							protected Object coordinates;

						}

					}

				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "feature" })
			@Getter
			@Setter
			public static class Features {

				@XmlElement(required = true)
				protected List<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Features.Feature> feature;

				public List<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Features.Feature> getFeature() {
					if (feature == null) {
						feature = new ArrayList<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Features.Feature>();
					}
					return this.feature;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "")
				@Getter
				@Setter
				public static class Feature {

					@XmlAttribute(name = "id", required = true)
					@XmlSchemaType(name = "unsignedByte")
					protected short id;
					@XmlAttribute(name = "name", required = true)
					protected String name;

				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "image" })
			@Getter
			@Setter
			public static class Images {

				@XmlElement(required = true)
				protected List<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Images.Image> image;

				public List<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Images.Image> getImage() {
					if (image == null) {
						image = new ArrayList<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Images.Image>();
					}
					return this.image;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "fullSizeImage", "smallImage" })
				@Getter
				@Setter
				public static class Image {

					@XmlElement(required = true)
					protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.Images.Image.FullSizeImage fullSizeImage;
					@XmlElement(required = true)
					protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.Images.Image.SmallImage smallImage;
					@XmlAttribute(name = "id", required = true)
					@XmlSchemaType(name = "unsignedInt")
					protected long id;

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "")
					@Getter
					@Setter
					public static class FullSizeImage {

						@XmlAttribute(name = "url", required = true)
						protected String url;
						@XmlAttribute(name = "height", required = true)
						@XmlSchemaType(name = "unsignedShort")
						protected int height;
						@XmlAttribute(name = "width", required = true)
						@XmlSchemaType(name = "unsignedShort")
						protected int width;

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "")
					@Getter
					@Setter
					public static class SmallImage {

						@XmlAttribute(name = "url", required = true)
						protected String url;
						@XmlAttribute(name = "height", required = true)
						@XmlSchemaType(name = "unsignedShort")
						protected int height;
						@XmlAttribute(name = "width", required = true)
						@XmlSchemaType(name = "unsignedShort")
						protected int width;

					}

				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "roomtype" })
			@Getter
			@Setter
			public static class Roomtypes {

				@XmlElement(required = true)
				protected List<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Roomtypes.Roomtype> roomtype;

				public List<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Roomtypes.Roomtype> getRoomtype() {
					if (roomtype == null) {
						roomtype = new ArrayList<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Roomtypes.Roomtype>();
					}
					return this.roomtype;
				}

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "roomtypeID", "rooms", "roomType", "sharedRoom", "sharedFacilities" })
				@Getter
				@Setter
				public static class Roomtype {

					@XmlElement(name = "roomtype.ID")
					@XmlSchemaType(name = "unsignedShort")
					protected int roomtypeID;
					@XmlElement(required = true)
					protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.Roomtypes.Roomtype.Rooms rooms;
					@XmlElement(name = "room.type", required = true)
					protected String roomType;
					protected boolean sharedRoom;
					@XmlElement(required = true, nillable = true)
					protected String sharedFacilities;

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = { "room" })
					@Getter
					@Setter
					public static class Rooms {

						@XmlElement(required = true)
						protected List<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Roomtypes.Roomtype.Rooms.Room> room;

						public List<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Roomtypes.Roomtype.Rooms.Room> getRoom() {
							if (room == null) {
								room = new ArrayList<GetStaticHotelsAndRoomsResult.Hotels.Hotel.Roomtypes.Roomtype.Rooms.Room>();
							}
							return this.room;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = { "id", "beds", "extrabeds", "meals", "cancellationPolicies",
								"notes", "isSuperDeal", "isBestBuy", "paymentMethods", "features", "themes" })
						@Getter
						@Setter
						public static class Room {

							@XmlSchemaType(name = "unsignedInt")
							protected long id;
							@XmlSchemaType(name = "unsignedByte")
							protected short beds;
							@XmlSchemaType(name = "unsignedByte")
							protected short extrabeds;
							@XmlElement(required = true)
							protected Object meals;
							@XmlElement(name = "cancellation_policies", required = true)
							protected Object cancellationPolicies;
							@XmlElement(required = true)
							protected Object notes;
							protected boolean isSuperDeal;
							protected boolean isBestBuy;
							@XmlElement(required = true)
							protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.Roomtypes.Roomtype.Rooms.Room.PaymentMethods paymentMethods;
							@XmlElement(required = true)
							protected Object features;
							@XmlElement(required = true)
							protected Object themes;

							@XmlAccessorType(XmlAccessType.FIELD)
							@XmlType(name = "", propOrder = { "paymentMethod" })
							@Getter
							@Setter
							public static class PaymentMethods {

								@XmlElement(required = true)
								protected GetStaticHotelsAndRoomsResult.Hotels.Hotel.Roomtypes.Roomtype.Rooms.Room.PaymentMethods.PaymentMethod paymentMethod;

								@XmlAccessorType(XmlAccessType.FIELD)
								@XmlType(name = "")
								@Getter
								@Setter
								public static class PaymentMethod {

									@XmlAttribute(name = "id", required = true)
									@XmlSchemaType(name = "unsignedByte")
									protected short id;
									@XmlAttribute(name = "name", required = true)
									protected String name;

								}

							}

						}

					}

				}

			}

		}

	}

}
