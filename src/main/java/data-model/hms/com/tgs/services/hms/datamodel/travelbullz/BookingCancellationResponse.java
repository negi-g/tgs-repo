package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingCancellationResponse extends TravelbullzBaseResponse {
	public List<ErrorType> Error ;
	public CancelRS CancelRS;

}
