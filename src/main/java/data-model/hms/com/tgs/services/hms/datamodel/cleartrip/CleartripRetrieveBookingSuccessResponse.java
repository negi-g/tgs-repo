package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CleartripRetrieveBookingSuccessResponse {

	private String tripRef;
	private String roomCount;
	private String cancellationPolicy;
	private ContactDetail contactDetail;
	private PaymentDetail paymentDetail;
	private HotelDetail hotelDetail;
	private Pricing pricing;
	private BookingInfo bookingInfo;
	private List<Room> rooms;
}
