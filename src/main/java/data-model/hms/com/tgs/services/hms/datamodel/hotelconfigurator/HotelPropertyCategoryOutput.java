package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.Map;

import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelPropertyCategoryOutput implements IRuleOutPut {

	private Map<PropertyType, PropertyCriteria> propertyCategory;

}
