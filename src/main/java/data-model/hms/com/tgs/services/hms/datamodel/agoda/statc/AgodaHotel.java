package com.tgs.services.hms.datamodel.agoda.statc;

import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

import com.tgs.services.hms.datamodel.agoda.search.Room;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="hotel")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaHotel {

	@XmlElement(name = "hotel_id")
    protected String hotelId;
	
    @XmlElement(name = "hotel_name")
    protected String hotelName;
    
    @XmlElement(name = "translated_name")
    protected String translatedName;
    
    @XmlElement(name = "star_rating")
    protected BigDecimal starRating;
    
    @XmlElement(name = "continent_id")
    protected Long continentId;
    
    @XmlElement(name = "country_id")
    protected Long countryId;
    
    @XmlElement(name = "city_id")
    protected Long cityId;
    
    protected String longitude;
    protected String latitude;
    
    @XmlElement(name = "hotel_url")
    protected String hotelUrl;
    
    @XmlElement(name = "popularity_score")
    protected Long popularityScore;
    
    @XmlElement(name = "phone_no")
    protected String phoneNo;
    
    protected String remark;
    
    @XmlElement(name = "number_of_reviews")
    @XmlSchemaType(name = "unsignedInt")
    protected Long numberOfReviews;
    
    
    @XmlElement(name = "rating_average")
    protected BigDecimal ratingAverage;
    
    @XmlElement(name = "child_and_extra_bed_policy")
    protected AgodaChildAndExtraBedPolicy childAndExtraBedPolicy;
    
    @XmlElement(name = "accommodation_type")
    protected String accommodationType;
    
    @XmlElement(name = "nationality_restrictions")
    protected String nationalityRestrictions;
    
    @XmlElement(name = "external_hotel_code")
    protected String externalHotelCode;
    
    @XmlElement(name = "rate_channel")
    protected String rateChannel;
    
    @XmlElement(name = "gps_flag")
    protected String gpsFlag;
    
    @XmlElement(name = "hotel_bcom_id")
    @XmlSchemaType(name = "unsignedInt")
    protected Long hotelBcomId;
    
    @XmlElement(name = "tax_receipt_type")
    protected String taxReceiptType;
    
    @XmlElement(name = "rec_status")
    protected String recStatus;
	
	@XmlElementWrapper(name = "Rooms")
	@XmlElement(name="Room")
	private List<Room> Rooms;
	
	@XmlElement(name="child_and_extra_bed_policy")
	private AgodaChildAndExtraBedPolicy policy;
	
	
	
	
}
