package com.tgs.services.hms.datamodel.qtech;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelSearchRequest extends QTechBaseRequest {

	private String checkin_date;
	private String checkout_date;
	private String sel_country;
	private String sel_city;
	private String chk_ratings;
	private String sel_nationality;
	private String country_of_residence;
	private String sel_currency;
	private String availableonly;
	private String number_of_rooms;
	private String roomDetails;
	private String sel_hotel;
	private String timeout;
	private String static_data;
	private String limit_hotel_room_type;
	private String hotel_ids;
}
