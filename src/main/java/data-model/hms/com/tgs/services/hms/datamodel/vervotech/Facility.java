package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Facility {

	private Integer Id;
	private String Name;
	private Integer GroupId;
}