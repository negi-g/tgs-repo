package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoomsRequest", propOrder = {
    "room"
})
@Data
public class RoomsRequest {

    @XmlElement(name = "Room", required = true)
    protected List<RoomRequest> room;

    public List<RoomRequest> getRoom() {
        if (room == null) {
            room = new ArrayList<RoomRequest>();
        }
        return this.room;
    }

}
