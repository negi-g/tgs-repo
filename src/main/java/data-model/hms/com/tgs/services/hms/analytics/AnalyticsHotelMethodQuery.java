package com.tgs.services.hms.analytics;

import java.util.Map;
import com.tgs.services.base.BaseAnalyticsQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AnalyticsHotelMethodQuery extends BaseAnalyticsQuery {

	private String errormsg;
	private String searchId;
	private Map<String, Object> methodExecutionMapToPush;
}
