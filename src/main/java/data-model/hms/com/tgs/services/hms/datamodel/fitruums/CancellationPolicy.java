package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "deadline", "percentage", "text" })
@Getter
@Setter
public class CancellationPolicy {

	@XmlElement(required = true, nillable = true)
	protected String deadline;
	@XmlSchemaType(name = "unsignedByte")
	protected short percentage;
	@XmlElement()
	protected String text;
}
