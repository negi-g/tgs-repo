package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class HotelBedsStaticData {

    private Integer from;
    private Integer to;
    private Integer total;
    private AuditData auditData;
    private List<StaticHotel> hotels = null;
}
