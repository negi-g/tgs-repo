package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class CleartripProvisionalBookingRequest extends CleartripBaseRequest {

	private String affiliateTxnId;
	private String nri;
	private String hotelId;
	private String checkInDate;
	private String checkOutDate;
	private String roomTypeCode;
	private String bookingCode;
	private String bookingAmount;
	private List<Occupancy> occupancy;	
	private List<Customer> customerDetails;
}
