package com.tgs.services.hms.datamodel;

import java.time.temporal.ChronoUnit;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class HotelSupplierSearchCriteria {


	@SerializedName("ma")
	private Integer maxAdultInRoom;

	@SerializedName("mc")
	private Integer maxChildInRoom;

	@SerializedName("mi")
	private Integer maxInfantInRoom;

	@SerializedName("msl")
	private Integer maxStayLength;

	@SerializedName("mrc")
	private Integer maxRoomCount;

	@SerializedName("mca")
	private Integer maxChildAge;

	@SerializedName("iaprr")
	private Boolean isAdultPerRoomRequired;

	@SerializedName("mia")
	private Integer maxInfantAge;

	public boolean validateSearchCriteria(HotelSearchQuery searchQuery) {

		if (Objects.nonNull(this.getMaxAdultInRoom()) && !isAdultCountValid(this.getMaxAdultInRoom(), searchQuery)) {
			return false;
		}

		if (Objects.nonNull(this.getMaxChildInRoom()) && !isChildCountValid(this.getMaxChildInRoom(), searchQuery)) {
			return false;
		}

		if (Objects.nonNull(this.getMaxStayLength()) && !isStayLengthValid(this.getMaxStayLength(), searchQuery)) {
			return false;
		}

		if (Objects.nonNull(this.getMaxRoomCount()) && !isRoomCountValid(this.getMaxRoomCount(), searchQuery)) {
			return false;
		}

		if (Objects.nonNull(this.getMaxChildAge()) && !isChildAgeValid(this.getMaxChildAge(), searchQuery)) {
			return false;
		}

		if (Objects.nonNull(this.getMaxInfantAge()) && Objects.nonNull(this.getMaxInfantInRoom())
				&& !isInfantAgeValid(this.getMaxInfantAge(), this.getMaxInfantInRoom(), searchQuery)) {
			return false;
		}

		if (BooleanUtils.isTrue(this.getIsAdultPerRoomRequired()) && !isAdultPerRoomPresent(searchQuery)) {
			return false;
		}

		return true;

	}

	private static boolean isAdultPerRoomPresent(HotelSearchQuery searchQuery) {
		int totalNumberOfAdults = 0;
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			totalNumberOfAdults += room.getNumberOfAdults();
		}
		if (totalNumberOfAdults < searchQuery.getRoomInfo().size()) {
			return false;
		}
		return true;
	}


	private static boolean isInfantAgeValid(Integer maxInfantAge, Integer maxInfantInRoom,
			HotelSearchQuery searchQuery) {
		int infant = 0;
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			if (CollectionUtils.isNotEmpty(room.getChildAge()) && !Objects.isNull(room.getNumberOfChild())
					&& room.getNumberOfChild() > 0) {
				for (int age : room.getChildAge()) {
					if (age <= maxInfantAge) {
						infant++;
					}
				}
			}
		}
		if (infant > maxInfantInRoom) {
			return false;
		}
		return true;
	}

	private static boolean isChildAgeValid(Integer maxChildAge, HotelSearchQuery searchQuery) {
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			if (CollectionUtils.isNotEmpty(room.getChildAge()) && !Objects.isNull(room.getNumberOfChild())
					&& room.getNumberOfChild() > 0) {
				for (int age : room.getChildAge()) {
					if (age > maxChildAge)
						return false;
				}
			}
		}
		return true;
	}

	private static boolean isChildCountValid(Integer maxChildInRoom, HotelSearchQuery searchQuery) {
		for (RoomSearchInfo roomSearchInfo : searchQuery.getRoomInfo()) {
			if (roomSearchInfo.getNumberOfChild() > maxChildInRoom) {
				return false;
			}
		}
		return true;
	}

	private static boolean isRoomCountValid(Integer maxRoomCount, HotelSearchQuery searchQuery) {
		if (searchQuery.getRoomInfo().size() > maxRoomCount) {
			return false;
		}
		return true;
	}

	private static boolean isStayLengthValid(Integer maxStayLength, HotelSearchQuery searchQuery) {
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());

		if (numberOfNights > maxStayLength) {
			return false;
		}
		return true;
	}

	private static boolean isAdultCountValid(Integer maxAdultInRoom, HotelSearchQuery searchQuery) {

		for (RoomSearchInfo roomSearchInfo : searchQuery.getRoomInfo()) {
			if (roomSearchInfo.getNumberOfAdults() > maxAdultInRoom) {
				return false;
			}
		}
		return true;
	}
}

