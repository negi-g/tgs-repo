package com.tgs.services.hms.datamodel.tbo.staticData;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hotelTheme"
})
@XmlRootElement(name = "HotelThemes")
@Getter
@Setter
public class HotelThemes {

    @XmlElement(name = "HotelTheme", required = false)
    protected List<HotelThemes.HotelTheme> hotelTheme;

    public List<HotelThemes.HotelTheme> getHotelTheme() {
        if (hotelTheme == null) {
            hotelTheme = new ArrayList<HotelThemes.HotelTheme>();
        }
        return this.hotelTheme;
    }
    
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @Getter
    @Setter
    public static class HotelTheme {

        @XmlAttribute(name = "ThemeId", required = false)
        @XmlSchemaType(name = "unsignedByte")
        protected short themeId;
        @XmlAttribute(name = "ThemeName", required = false)
        protected String themeName;
    }

}
