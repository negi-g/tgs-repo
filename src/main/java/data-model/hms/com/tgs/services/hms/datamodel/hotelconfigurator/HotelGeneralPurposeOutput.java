package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelGeneralPurposeOutput implements IRuleOutPut {

	@SerializedName("proxy")
	private String proxyAddress;

	@SerializedName("dbe")
	private Boolean duplicateBookingEnabled;

	@SerializedName("hls")
	private Integer hotelListSizeLimit;

	@SerializedName("sfd")
	private Boolean showFareDiff;

	@SerializedName("src")
	private Integer searchRetryCount;

	@SerializedName("drc")
	private Integer detailRetryCount;

	@SerializedName("ccEmails")
	private Map<String, String> ccEmails;

	@SerializedName("alertEmails")
	private Map<String, String> alertMails;

	// List of suppliers which has no pan required
	@SerializedName("spnr")
	private List<String> suppliersPanCardNotReqd;

	@SerializedName("psl")
	private Integer propertyIdsSizeLimit;

	@SerializedName("sdcs")
	private Integer staticDataChunkSize;

	@SerializedName("sdcl")
	private Integer staticDataConcurrencyLevel;

	@SerializedName("lth")
	private Long leadTimeInHours;

	@SerializedName("slhcn")
	private List<String> supplierListForHotelConfirmationNumberJob;

	@SerializedName("aac")
	private Integer amendmentAssignCapacity;

	@SerializedName("sbdnp")
	private List<String> supplierBookingDetailNotPresent;

	@SerializedName("cbc")
	private Integer createdOnBufferTimeForbookingCancellation;

	@SerializedName("mt")
	private Integer maxThreadAllowed;

	@SerializedName("mnid")
	private Long minId;

	@SerializedName("mxid")
	private Long maxId;

	@SerializedName("ipc")
	private Boolean isPanOrPassConfigEnabled;

	@SerializedName("rmifdc")
	private Boolean roomMappingInFirstDetailCall;
	
	@SerializedName("muac")
	private Integer maxUnicaAllowedInConfig;
}
