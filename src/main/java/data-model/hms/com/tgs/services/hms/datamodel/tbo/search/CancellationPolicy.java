package com.tgs.services.hms.datamodel.tbo.search;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CancellationPolicy {
	
	private String BaseCurrency;
	private Double Charge;
	private Integer ChargeType;
	private String Currency;
	private LocalDateTime FromDate;
	private LocalDateTime ToDate;

}
