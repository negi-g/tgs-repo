package com.tgs.services.hms.datamodel;

import java.util.List;
import com.tgs.services.hms.datamodel.tripadvisor.ReviewData;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelUserReviewIdInfo {

	private String key;
	private String reviewId;
	private String hotelId;
	private String supplierId;
	private Integer rating;
	private String city;
	private List<ReviewData> reviewDataList;
	private UserReviewStaticData staticData;

}
