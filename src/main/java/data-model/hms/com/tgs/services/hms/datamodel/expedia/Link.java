package com.tgs.services.hms.datamodel.expedia;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Link {

	private Map<String, URLSignature> urlSignature;
}
