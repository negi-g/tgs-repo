package com.tgs.services.hms.datamodel.qtech;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BookingAllowedInfo {

	
	private String Message;
	private String SoldOut;
	private String MessageInfo;
	private String BookingAllowed;
	
}