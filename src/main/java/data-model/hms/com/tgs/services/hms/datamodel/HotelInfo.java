package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.enums.HotelSearchType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import com.tgs.services.base.helper.RestExclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@Builder
@Accessors(chain = true)
public class HotelInfo {

	@CustomSerializedName(key = FieldName.HOTEL_ID)
	private String id;

	@CustomSerializedName(key = FieldName.GIATA_ID)
	private String giataId;

	private String name;

	@CustomSerializedName(key = FieldName.HOTEL_IMAGES)
	@ToString.Exclude
	private List<Image> images;

	@CustomSerializedName(key = FieldName.HOTEL_TERMS_AND_CONDITIONS)
	@ToString.Exclude
	private TermsAndConditions termsAndConditions;

	@CustomSerializedName(key = FieldName.HOTEL_DESCRIPTION)
	@ToString.Exclude
	private String description;

	@CustomSerializedName(key = FieldName.HOTEL_LONG_DESCRIPTION)
	@ToString.Exclude
	private String longDescription;

	@CustomSerializedName(key = FieldName.HOTEL_RATING)
	private Integer rating;
	@CustomSerializedName(key = FieldName.HOTEL_GEOLOCATION)
	private GeoLocation geolocation;
	@CustomSerializedName(key = FieldName.HOTEL_ADDRESS)
	private Address address;
	@CustomSerializedName(key = FieldName.HOTEL_FACILITIES)
	@ToString.Exclude
	private List<String> facilities;
	@CustomSerializedName(key = FieldName.HOTEL_PROPERTY_TYPE)
	private String propertyType;
	@CustomSerializedName(key = FieldName.HOTEL_CONTACT)
	private Contact contact;

	private Set<HotelTag> tags;

	@CustomSerializedName(key = FieldName.HOTEL_INSTRUCTIONS)
	@ToString.Exclude
	private List<Instruction> instructions;

	@CustomSerializedName(key = FieldName.OPTIONS)
	private List<Option> options;

	/*
	 * This is user primarily for search to compress size
	 */
	@CustomSerializedName(key = FieldName.PROCESSED_OPTIONS)
	private List<ProcessedOption> processedOptions;

	@RestExclude
	@CustomSerializedName(key = FieldName.HOTEL_MISC_INFO)
	private HotelMiscInfo miscInfo;

	@RestExclude
	@CustomSerializedName(key = FieldName.PROCESSED_ON)
	private LocalDateTime processedOn;

	@RestExclude
	@CustomSerializedName(key = FieldName.CREATED_ON)
	private LocalDateTime createdOn;

	@RestExclude
	@CustomSerializedName(key = FieldName.HOTEL_ADDITIONAL_INFO)
	private HotelAdditionalInfo additionalInfo;

	@RestExclude
	@CustomSerializedName(key = FieldName.HOTEL_PROMOTION_INFO)
	private HotelPromotion promotionInfo;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.TRIP_ADVISOR_ID)
	private String userReviewSupplierId;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.SUPPLIER_SET)
	private Set<String> suppliers;
	/*
	 * Excluding city and country as already present in Address
	 */
	@ToString.Exclude
	private String cityName;
	@ToString.Exclude
	private String countryName;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.SUPPLIER_HOTEL_ID)
	private String supplierHotelId;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.SUPPLIER_NAME)
	private String supplierName;

	@CustomSerializedName(key = FieldName.UNICA_ID)
	private String unicaId;

	@CustomSerializedName(key = FieldName.LOCAL_HOTEL_CODE)
	private String localHotelCode;

	public HotelSearchType getSearchType() {
		if (this.getAddress() != null && this.getAddress().getCountry() != null
				&& this.getAddress().getCountry().getName() != null) {
			if (!this.getAddress().getCountry().getName().equalsIgnoreCase("India")) {
				return HotelSearchType.INTERNATIONAL;
			}
		}
		return HotelSearchType.DOMESTIC;
	}

	public boolean isOptionsNotEmpty() {
		return this != null && CollectionUtils.isNotEmpty(options);
	}

	public boolean isPriceInfosNotEmpty() {
		boolean isRoomsNotEmpty = true;
		boolean isPriceListNotEmpty = true;
		if (isOptionsNotEmpty()) {
			for (Option option : options) {
				isRoomsNotEmpty = CollectionUtils.isNotEmpty(option.getRoomInfos());
				if (!isRoomsNotEmpty) {
					return false;
				} else {
					for (RoomInfo roomInfo : option.getRoomInfos()) {
						isPriceListNotEmpty = CollectionUtils.isNotEmpty(roomInfo.getPerNightPriceInfos());
						if (!isPriceListNotEmpty) {
							return false;
						}
					}
				}
			}
		}
		return isPriceListNotEmpty;
	}

	public boolean isDomesticTrip(String clientCountry) {
		boolean isDomestic = true;
		if (!this.getAddress().getCountry().getName().equalsIgnoreCase(clientCountry)) {
			isDomestic = false;
		}
		return isDomestic;
	}

	public boolean isAddressPresent() {
		return this.getAddress() != null ? true : false;
	}


	public Set<String> getSuppliers() {
		if (suppliers == null)
			suppliers = new HashSet<>();
		return suppliers;

	}

	public List<Option> getOptions() {
		if (options == null) {
			options = new ArrayList<Option>();
		}
		return options;
	}


	public boolean isExpired() {

		boolean isExpired = false;
		if (this.getMiscInfo() != null && !ObjectUtils.isEmpty(this.getMiscInfo().getSearchKeyExpiryTime())
				&& this.getMiscInfo().getSearchKeyExpiryTime().isBefore(LocalDateTime.now())) {
			isExpired = true;
		}
		return isExpired;

	}

	public GeoLocation getGeolocation() {
		if (geolocation != null)
			return geolocation;
		return new GeoLocation("", "");
	}

	public Set<HotelTag> getTags() {

		if (this.tags == null) {
			tags = new HashSet<>();
		}
		return tags;
	}

	public boolean isCrossSellHotel() {
		return this.getTags().contains(HotelTag.CROSS_SELL);
	}

	public boolean isVoucherApplied() {
		Option option = getOptions().get(0);
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				if (MapUtils.isNotEmpty(priceInfo.getMatchedFareComponents(HotelFareComponent.voucherComponents()))) {
					return true;
				}
			}
		}
		return false;
	}

	public void cleanDataForSearch() {

		setTermsAndConditions(null);
		setMiscInfo(null);
		setDescription(null);
		setLongDescription(null);
		setFacilities(null);
		setInstructions(null);
	}

	public void populatedRepeatedInfoInHotel() {

		if (CollectionUtils.isNotEmpty(getImages())) {
			for (Image image : getImages()) {
				if (Objects.isNull(image.getBigURL())) {
					image.setBigURL(image.getThumbnail());
				}
			}
		}

		if (Objects.nonNull(address)) {
			address.setCity(City.builder().name(address.getCityName()).build());
			address.setCountry(Country.builder().name(address.getCountryName()).build());
			address.setState(State.builder().name(address.getStateName()).build());
		}
	}
}
