package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SurchargeResponse", propOrder = {
    "rate"
})
public class SurchargeResponse {

    @XmlElement(name = "Rate", required = true)
    protected RateResponse rate;
    @XmlAttribute(name = "id", required = true)
    protected BigInteger id;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "method", required = true)
    protected String method;
    @XmlAttribute(name = "charge", required = true)
    protected String charge;
    @XmlAttribute(name = "margin", required = true)
    protected String margin;
}
