package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoomBedType {

	private Integer BedTypeCode;
	private String BedTypeDescription;

}
