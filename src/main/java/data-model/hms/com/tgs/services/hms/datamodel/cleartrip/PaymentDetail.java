package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PaymentDetail {

	private String paymentType;
	private String amount;
	private String currency;
	private String status;
}
