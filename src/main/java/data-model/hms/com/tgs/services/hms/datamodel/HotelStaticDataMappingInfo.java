package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelStaticDataMappingInfo {

	private String hotelId;
	private String supplierhotelId;

}
