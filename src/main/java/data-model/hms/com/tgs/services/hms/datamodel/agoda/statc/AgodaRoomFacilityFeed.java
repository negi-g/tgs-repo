package com.tgs.services.hms.datamodel.agoda.statc;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@XmlRootElement(name="Roomtype_facility_feed")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaRoomFacilityFeed {

	@XmlElementWrapper(name = "roomtype_facilities")
	@XmlElement(name="roomtype_facility")
	private List<AgodaRoomFacility> roomFacilities;
	
}

