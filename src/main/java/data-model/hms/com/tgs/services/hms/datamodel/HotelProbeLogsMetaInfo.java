package com.tgs.services.hms.datamodel;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class HotelProbeLogsMetaInfo {

	private String supplierName;
	private String requestType;
	private String prefix;
	private String logKey;
	private String urlString;
	private Map<String, String> headerParams;
	private String postData;
	private int threadCount;
	private String additionalinfo;
	private String responseString;
	private Long requestGenerationTime;
	private Long responseGenerationTime;
}
