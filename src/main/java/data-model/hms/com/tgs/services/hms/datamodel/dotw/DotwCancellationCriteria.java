package com.tgs.services.hms.datamodel.dotw;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwCancellationCriteria {

	@XmlElement
	private String bookingType;
	
	@XmlElement
	private String bookingCode;
	
	@XmlElement
	private String confirm;
	
	@XmlElementWrapper(name = "testPricesAndAllocation")
	@XmlElement(name="service")
	private List<DotwServiceRequest> service;
}
