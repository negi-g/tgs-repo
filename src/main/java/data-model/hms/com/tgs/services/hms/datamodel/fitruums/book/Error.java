package com.tgs.services.hms.datamodel.fitruums.book;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"errorType", "message"})
public class Error {

	@XmlElement(name = "ErrorType", required = true)
	protected String errorType;
	@XmlElement(name = "Message", required = true)
	protected String message;

}
