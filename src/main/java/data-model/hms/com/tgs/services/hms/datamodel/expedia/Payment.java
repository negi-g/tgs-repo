package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Payment {

	private String type;
	private BillingContact billing_contact;
}
