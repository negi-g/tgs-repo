package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpediaBookingResponse {

	private String type;
	private String message;
	private List<Field> fields;
	private String itinerary_id;
	private Map<String, URLSignature> links;
}
