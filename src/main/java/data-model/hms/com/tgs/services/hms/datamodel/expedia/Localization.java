package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Localization {

	private Link links;
}
