package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Data;

@Data
public class HotelCancellationOption {
	public Integer BookingId;
	public Double TotalPrice;
	public Integer TotalCharge;
	public Double TotalRefund;
	public String CancelCode;
	public List<HotelRoom> HotelRooms ;
}
