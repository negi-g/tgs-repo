package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class OptionStatistics {

	private Integer id;
	private String name;
	private String value;
}
