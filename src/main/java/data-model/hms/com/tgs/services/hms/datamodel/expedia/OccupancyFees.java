package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OccupancyFees {

	private CurrencyParams mandatory_tax;
	private CurrencyParams resort_fee;
	private CurrencyParams mandatory_fee;
	private String per_person;
	private String per_night;
}
