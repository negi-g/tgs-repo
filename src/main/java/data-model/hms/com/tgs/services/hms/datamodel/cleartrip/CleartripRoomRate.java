package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CleartripRoomRate {

	private List<String> bedTypeList;
	private String bookingCode;
	private List<String> inclusions;
	private String refundable;
	private String cancelPolicy;
	private Double totalAmount;
	private Double totalTax;
	private Double totalDiscount;
	private CleartripRoomType roomType;
	private List<RateBreakDown> rateBreakdown;
}
