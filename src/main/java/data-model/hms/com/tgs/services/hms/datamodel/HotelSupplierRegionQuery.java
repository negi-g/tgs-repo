package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelSupplierRegionQuery {

	private String regionName;
	private String regionId;
	private String stateId;
	private String stateName;
	private String countryId;
	private String countryName;
	private String supplierName;
}
