package com.tgs.services.hms.datamodel;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.SystemError;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class HotelSearchQuery implements Validatable, HotelQuery {

	/*
	 * Update Equals method also if any change in data model 
	 */
	private LocalDate checkinDate;
	private LocalDate checkoutDate;
	private List<RoomSearchInfo> roomInfo;
	private HotelSearchCriteria searchCriteria;
	private HotelSearchPreferences searchPreferences;
	private String searchId;
	@APIUserExclude
	private Integer sourceId;
	@APIUserExclude
	private List<String> supplierIds;
	@APIUserExclude
	private List<Integer> sourceIds;
	private boolean isSearchCompleted;
	private Integer minHotel;

	private HotelSearchQueryMiscInfo miscInfo;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		if (checkinDate == null) {
			errors.put("checkinDate", SystemError.INVALID_CHECKIN_DATE.getErrorDetail());
		}
		if (checkoutDate == null) {
			errors.put("checkoutDate", SystemError.INVALID_CHECKOUT_DATE.getErrorDetail());
		}
		if (CollectionUtils.isNotEmpty(roomInfo)) {
			int i = 0;
			int totalPassengers = 0;
			for (RoomSearchInfo roomSearchInfo : roomInfo) {
				totalPassengers += (roomSearchInfo.getNumberOfAdults()
						+ (roomSearchInfo.getNumberOfChild() != null ? roomSearchInfo.getNumberOfChild() : 0));
				if (roomSearchInfo != null) {
					errors.putAll(roomSearchInfo.validate(null).withPrefixToFieldNames("roomInfo[" + i++ + "]."));
				}
			}
			if (totalPassengers > 25)
				errors.put("roomInfo", SystemError.INVALID_PASSENGERS_COUNT.getErrorDetail());
			if (roomInfo.size() > 5) {
				errors.put("roomInfo", SystemError.INVALID_ROOM_COUNT.getErrorDetail());
			}
		} else {
			errors.put("roomInfo", SystemError.INVALID_ROOM_INFO.getErrorDetail());
		}

		if (searchCriteria == null) {
			errors.put("searchCriteria", SystemError.INVALID_HOTEL_SEARCH_CRITERIA.getErrorDetail());
		}
		
		if (searchPreferences != null) {
			errors.putAll(searchPreferences.validate(validatingData).withPrefixToFieldNames("searchPreferences."));

			if (searchCriteria != null) {
				if (StringUtils.isBlank(searchPreferences.getHotelId())
						&& StringUtils.isBlank(searchCriteria.getRegionId())) {
					errors.put("searchCriteria", SystemError.INVALID_HOTEL_DESTINATION.getErrorDetail());
				}
			}
		}

		return errors;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((checkinDate == null) ? 0 : checkinDate.hashCode());
		result = prime * result + ((checkoutDate == null) ? 0 : checkoutDate.hashCode());
		result = prime * result + ((minHotel == null) ? 0 : minHotel.hashCode());
		result = prime * result + ((miscInfo == null) ? 0 : miscInfo.hashCode());
		result = prime * result + ((roomInfo == null) ? 0 : roomInfo.hashCode());
		result = prime * result + ((searchCriteria == null) ? 0 : searchCriteria.hashCode());
		result = prime * result + ((searchId == null) ? 0 : searchId.hashCode());
		result = prime * result + ((searchPreferences == null) ? 0 : searchPreferences.hashCode());
		result = prime * result + ((sourceId == null) ? 0 : sourceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		HotelSearchQuery other = (HotelSearchQuery) obj;
		
		if(!Objects.equals(this.getCheckinDate(), other.getCheckinDate())) { return false; }
		if(!Objects.equals(this.getCheckoutDate(), other.getCheckoutDate())) { return false; }
		if(!Objects.equals(this.getSearchCriteria(), other.getSearchCriteria())){ return false; }
		if(!Objects.equals(this.getSearchPreferences(), other.getSearchPreferences())){ return false; }
		if(!Objects.equals(this.getRoomInfo(), other.getRoomInfo())) { return false; }
		
		return true;
	}

	public HotelSearchQueryMiscInfo getMiscInfo() {
		if (miscInfo == null) {
			miscInfo = HotelSearchQueryMiscInfo.builder().build();
		}
		return miscInfo;
	}
}
