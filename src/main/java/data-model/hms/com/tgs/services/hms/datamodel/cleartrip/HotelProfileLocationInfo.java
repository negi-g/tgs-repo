package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelProfileLocationInfo {

	private String latitude;
	private String longitude;
	private String street;
	private String localityName;
	private String cityName;
	private String districtName;
	private String stateCode;
	private String stateName;
	private String countryCode;
	private String countryName;
	private String address;
	private String zip;
	private Integer localityId;
	private Integer districtId;
	private Integer cityId;
	private Integer countryId;
	private Integer stateId;
}
