package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheapestRoomResponse")
@Data
public class CheapestRoomResponse {

    @XmlAttribute(name = "exclusive", required = true)
    protected BigDecimal exclusive;
    @XmlAttribute(name = "tax", required = true)
    protected BigDecimal tax;
    @XmlAttribute(name = "fees", required = true)
    protected BigDecimal fees;
    @XmlAttribute(name = "inclusive", required = true)
    protected BigDecimal inclusive;

}
