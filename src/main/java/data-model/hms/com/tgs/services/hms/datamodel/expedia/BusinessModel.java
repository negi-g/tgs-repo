package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class BusinessModel {

	private Boolean expedia_collect;
	private Boolean property_collect;
}

