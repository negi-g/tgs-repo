package com.tgs.services.hms.datamodel.agoda.statc;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AgodaPolicy {

	private Integer infant_age;
	private Integer children_age_from;
	private Integer children_age_to;
	private Boolean children_stay_free;
	private Integer min_guest_age;
	
}
