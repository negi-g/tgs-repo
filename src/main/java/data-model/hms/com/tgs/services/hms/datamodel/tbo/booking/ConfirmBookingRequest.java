package com.tgs.services.hms.datamodel.tbo.booking;

import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class ConfirmBookingRequest extends TBOBaseRequest{

	private String EndUserIp;
	private String TokenId;
	private String BookingId;
	
}
