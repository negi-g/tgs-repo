package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class State {

	private Integer id;
	private String stateName;
	private String stateCode;
}
