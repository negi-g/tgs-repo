package com.tgs.services.hms.datamodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.DBExclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class HotelOrderSupplierInfo {

	@SerializedName("sbr")
	private String supplierBookingId;

	@SerializedName("si")
	private String supplierId;

	@SerializedName("tfcs")
	private Map<HotelFareComponent, Double> totalFareComponents;

	@DBExclude
	@SerializedName("tafcs")
	private Map<HotelFareComponent, Map<HotelFareComponent, Double>> totalAddlFareComponents;

	@SerializedName("pis")
	private List<PriceInfo> perNightPriceInfos;

	public Map<HotelFareComponent, Double> getTotalFareComponents() {
		if (totalFareComponents == null) {
			totalFareComponents = new HashMap<>();
		}
		return totalFareComponents;
	}

	public Map<HotelFareComponent, Map<HotelFareComponent, Double>> getTotalAddlFareComponents() {

		if (totalAddlFareComponents == null) {
			totalAddlFareComponents = new HashMap<>();
		}
		return this.totalAddlFareComponents;
	}

	public List<PriceInfo> getPerNightPriceInfos() {
		if (perNightPriceInfos == null)
			perNightPriceInfos = new ArrayList<>();
		return perNightPriceInfos;
	}
}
