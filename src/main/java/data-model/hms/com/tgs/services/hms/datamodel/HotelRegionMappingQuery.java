package com.tgs.services.hms.datamodel;

import com.tgs.services.base.BulkUploadQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelRegionMappingQuery extends BulkUploadQuery {

	private String regionName;
	private String countryName;
	private String regionType;
	private String fullRegionName;
	private Long supplierRegionInfoId;
	private String supplierRegionName;
}
