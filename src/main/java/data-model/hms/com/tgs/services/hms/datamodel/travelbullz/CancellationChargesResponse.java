package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CancellationChargesResponse extends TravelbullzBaseResponse{
	public List<ErrorType> Error ;
	public CheckHotelCancellationChargesRS CheckHotelCancellationChargesRS;
}
