package com.tgs.services.hms.datamodel.tbo.search;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelStaticDetailResponse {

	private String HotelData;
	private Error Error;
	private Integer Status;
	
}
