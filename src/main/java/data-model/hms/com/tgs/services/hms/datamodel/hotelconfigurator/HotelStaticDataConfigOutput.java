package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelStaticDataConfigOutput implements IRuleOutPut {

	@SerializedName("sup")
	private List<String> suppliers;

	@SerializedName("itpoc")
	private Boolean isToPreferOpinionatedContent;
}
