package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PropertyCatalogList {

	private List<PropertyCatalog> propertyCatalog;
}
