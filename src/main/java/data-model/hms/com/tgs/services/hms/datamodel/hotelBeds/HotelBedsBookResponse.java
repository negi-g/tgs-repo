package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBedsBookResponse  extends HotelBedsBaseResponse{

	private AuditData auditData;
	private Booking booking;
	private Error error;
}
