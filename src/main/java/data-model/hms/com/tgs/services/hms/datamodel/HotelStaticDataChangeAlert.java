package com.tgs.services.hms.datamodel;

import com.tgs.services.base.datamodel.Alert;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelStaticDataChangeAlert implements Alert {
	private String oldValue;
	private String newValue;
	private String field;
	private String type;

	@Override
	public String getType() {
		return type;
	}
}
