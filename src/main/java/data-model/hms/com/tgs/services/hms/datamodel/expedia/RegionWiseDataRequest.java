package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class RegionWiseDataRequest extends ExpediaBaseRequest {

	private String language;
	private String include;
}
