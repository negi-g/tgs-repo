package com.tgs.services.hms.datamodel;

import com.tgs.services.base.BulkUploadQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelMealQuery extends BulkUploadQuery {

	private String sMealBasis;
	private String fMealBasis;
	private String supplierName;
}
