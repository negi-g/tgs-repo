package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class AdditionalShoppingLink {

	private URLSignature additional_rates;
	private URLSignature recommendations;
}
