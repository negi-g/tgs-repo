package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "value" })
@Getter
@Setter
public class Price {

	@XmlValue
	@XmlSchemaType(name = "unsignedShort")
	protected int value;
	@XmlAttribute(name = "currency", required = true)
	protected String currency;
	@XmlAttribute(name = "paymentMethods", required = true)
	@XmlSchemaType(name = "unsignedByte")
	protected short paymentMethods;

}
