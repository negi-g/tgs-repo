package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CleartripSearchResponse extends CleartripBaseResponse {

	private CleartripSuccessSearchResponse success;
}
