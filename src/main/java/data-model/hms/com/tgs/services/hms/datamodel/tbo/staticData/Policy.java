package com.tgs.services.hms.datamodel.tbo.staticData;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Policy")
@Getter
@Setter
public class Policy {

    @XmlAttribute(name = "CheckInTime", required = false)
    protected String checkInTime;
    @XmlAttribute(name = "CheckOutTime", required = false)
    protected String checkOutTime;

}
