package com.tgs.services.hms.datamodel.tbo.bookingcancellation;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelCancellationResult {
	
	private Integer ChangeRequestStatus;
	private Integer ResponseStatus;
	private String ChangeRequestId;
}
