package com.tgs.services.hms.datamodel.agoda.statc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="picture")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaHotelImage {
	
	@XmlElement(name = "hotel_id")
    protected Long hotelId;
	
    @XmlElement(name = "picture_id")
    protected Long pictureId;
    
    protected String caption;
    protected String captionTranslated;
    
    @XmlElement(name = "URL")
    protected String url;

}
