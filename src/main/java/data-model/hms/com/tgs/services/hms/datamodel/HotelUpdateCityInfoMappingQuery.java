package com.tgs.services.hms.datamodel;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelUpdateCityInfoMappingQuery {

	private String cityName;
	private String countryName;
	private String cityId;
	private String countryId;
	private List<TopSuggestion> topSuggestionList;
}
