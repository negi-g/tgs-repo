package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CleartripErrorResponse {

	private Integer errorCode;
	private String errorMessage;
	private String detailedMessage;
}

