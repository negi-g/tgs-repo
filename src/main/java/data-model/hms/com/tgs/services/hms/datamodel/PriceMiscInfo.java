package com.tgs.services.hms.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class PriceMiscInfo {

	/*
	 * To Identify Which Commission Applied EX: Refer CMS
	 */
	@SerializedName("cRId")
	private Long commericialRuleId;

	@SerializedName("sp")
	private Double supplierPrice;
}
