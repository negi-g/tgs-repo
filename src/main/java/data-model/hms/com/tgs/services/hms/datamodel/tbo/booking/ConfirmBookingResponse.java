package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ConfirmBookingResponse {
	
	private boolean VoucherStatus;
	private Integer ResponseStatus;
	private Error Error;
	private String TraceId;
	private Integer Status;
	private String HotelBookingStatus;
	private String InvoiceNumber;
	private String ConfirmationNo;
	private String BookingRefNo;
	private String BookingId;

}
