package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpinionatedHotel {

	private String Name;
	private GeoCode GeoCode;
	private Address Address;
	private String Rating;
	private String PropertyType;
	private String ChainName;
	private String BrandName;
}
