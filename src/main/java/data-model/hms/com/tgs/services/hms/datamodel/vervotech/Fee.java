package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Fee {

	private String Type;
	private String Text;
}