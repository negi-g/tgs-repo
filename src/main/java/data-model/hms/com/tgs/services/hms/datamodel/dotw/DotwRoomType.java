package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="roomType")
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwRoomType {

	
	@XmlAttribute
	private String roomtypecode;
	
	@XmlElement(name="name")
	private String name;
	
	@XmlElement(name = "rateBases")
	private RateBases rateBases;
	
	@XmlElement(name="roomAmenities")
	private DotwRoomAmenities roomAmenities;
	
}
