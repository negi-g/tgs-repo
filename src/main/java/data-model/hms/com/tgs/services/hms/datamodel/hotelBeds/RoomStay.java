package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class RoomStay {

	private String stayType;
	private String order;
	private String description;
	private List<RoomStayFacility> roomStayFacilities = null;
}
