package com.tgs.services.hms.datamodel.tbo.bookingcancellation;

import com.tgs.services.hms.datamodel.tbo.search.Error;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BookingCancellationStatus {	
	
	private Integer ResponseStatus;
	private Error Error;
	private String TraceId;
	private Double ChangeRequestId;
	private Double RefundedAmount;
	private Double CancellationCharge;
	private Integer ChangeRequestStatus;
	
}
