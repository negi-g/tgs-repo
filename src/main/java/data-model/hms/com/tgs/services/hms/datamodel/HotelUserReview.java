package com.tgs.services.hms.datamodel;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelUserReview {

	private String id;
	private String hotelId;
	private Double review;
	private Map<Integer, Integer> reviewCount;
	private Integer totalcount;
	private String imageUrl;
	private List<SubReview> subReviews;
	private String percentRecommended;
	private String redirectUrl;
	
	
}
