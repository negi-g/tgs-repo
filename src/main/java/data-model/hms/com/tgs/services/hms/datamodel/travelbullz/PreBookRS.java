package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Data;

@Data
public class PreBookRS {
	public HotelBookOption HotelOption;
	public String Currency;
}
