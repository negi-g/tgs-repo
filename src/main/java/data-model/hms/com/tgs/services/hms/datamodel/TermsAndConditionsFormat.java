package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class TermsAndConditionsFormat {

	private TermsAndConditionsType type;
	private String info;
	private String label;
}
