package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoomResponse", propOrder = {
    "rateInfo",
    "cancellation",
    "benefits",
    "parentRoom",
    "maxRoomOccupancy"
})
@Data
public class RoomResponse {

    @XmlElement(required = true)
    protected RateInfoResponse rateInfo;
    @XmlElement(name = "Cancellation", required = true)
    protected CancellationResponse cancellation;
    protected BenefitsResponse benefits;
    protected ParentRoomResponse parentRoom;
    protected MaxRoomOccupancyResponse maxRoomOccupancy;
    @XmlAttribute(name = "id", required = true)
    protected BigInteger id;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "lineitemid", required = true)
    protected BigInteger lineitemid;
    @XmlAttribute(name = "rateplan", required = true)
    protected RatePlanResponse rateplan;
    @XmlAttribute(name = "ratetype", required = true)
    protected RateTypeResponse ratetype;
    @XmlAttribute(name = "currency", required = true)
    protected String currency;
    @XmlAttribute(name = "model", required = true)
    protected String model;
    @XmlAttribute(name = "ratecategoryid", required = true)
    protected BigInteger ratecategoryid;
    @XmlAttribute(name = "blockid", required = true)
    protected String blockid;
    @XmlAttribute(name = "standardTranslation", required = true)
    protected String standardTranslation;
    @XmlAttribute(name = "remainingRooms", required = true)
    protected BigInteger remainingRooms;
    @XmlAttribute(name = "promotionid")
    protected BigInteger promotionid;
    @XmlAttribute(name = "booknowpaylaterdate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar booknowpaylaterdate;

}
