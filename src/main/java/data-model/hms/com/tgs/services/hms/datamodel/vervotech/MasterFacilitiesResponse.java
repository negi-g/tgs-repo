package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MasterFacilitiesResponse {

	private Integer Id;
	private String Name;
	private String Culture;
	private String Type;
	private Integer code;
	private String message;
}
