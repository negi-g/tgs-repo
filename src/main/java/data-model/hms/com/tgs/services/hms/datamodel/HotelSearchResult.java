package com.tgs.services.hms.datamodel;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Builder
public class HotelSearchResult implements HotelResult {
	private String searchId;

	@SerializedName("his")
	private List<HotelInfo> hotelInfos;

	@SerializedName("mi")
	private HotelSearchResultMiscInfo miscInfo;

	@SerializedName("size")
	private Integer noOfHotelOptions;

	public Integer getNoOfHotelOptions() {
		noOfHotelOptions = hotelInfos == null ? 0 : hotelInfos.size();
		return noOfHotelOptions;
	}

	public List<HotelInfo> getHotelInfos() {
		if (hotelInfos == null) {
			hotelInfos = new ArrayList<>();
		}
		return hotelInfos;
	}

	public HotelSearchResultMiscInfo getMiscInfo() {
		if (miscInfo == null) {
			miscInfo = HotelSearchResultMiscInfo.builder().build();
		}
		return miscInfo;
	}

	public void populatedRepeatedInfoInSearchResult() {

		if (CollectionUtils.isNotEmpty(hotelInfos)) {

			for (HotelInfo hotelInfo : hotelInfos) {
				hotelInfo.populatedRepeatedInfoInHotel();
			}
		}
	}
}
