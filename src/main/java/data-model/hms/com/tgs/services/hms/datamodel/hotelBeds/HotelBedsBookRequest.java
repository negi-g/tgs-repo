package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class HotelBedsBookRequest extends HotelBedsBaseRequest {

	private Holder holder;
	private List<BookRequestRoom> rooms;
	private String clientReference;
	private String remark;
	private double tolerance;

}
