package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpediaRetrieveBookingResponse {

	private String type;
	private String message;
	private String itinerary_id;
	private String property_id;
	private String creation_date_time;
	private String affiliate_reference_id;
	private String email;
	private Phone phone;
	private List<Room> rooms;	
}
