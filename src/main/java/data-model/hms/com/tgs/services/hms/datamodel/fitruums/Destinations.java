package com.tgs.services.hms.datamodel.fitruums;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Destinations")
public class Destinations {

	@XmlElement(name = "Destination", required = true)
	protected List<Destination> destination;

	public List<Destination> getDestination() {
		if (destination == null) {
			destination = new ArrayList<Destination>();
		}
		return this.destination;
	}
}
