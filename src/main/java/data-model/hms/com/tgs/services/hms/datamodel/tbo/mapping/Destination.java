package com.tgs.services.hms.datamodel.tbo.mapping;

import lombok.Data;

@Data
public class Destination {

	public String CityName;
	public String CountryCode;
	public String CountryName;
	public Integer DestinationId;
	public String StateProvince;
	public Integer Type;
}
