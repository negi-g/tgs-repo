package com.tgs.services.hms.datamodel.agoda.search;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@XmlRootElement(name="AvailabilityLongResponseV2")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaSearchResponse {
	
	@XmlAttribute(name = "searchid")
	private String searchid;

	@XmlAttribute
	private String status;
	
	@XmlElementWrapper(name = "Hotels")
	@XmlElement(name="Hotel")
	private List<Hotel> Hotels;
	
}
