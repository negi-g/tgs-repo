package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class HotelBedsRateCheckRequest extends HotelBedsBaseRequest {

	private List<RateCheckRoomRequest> rooms ;
}
