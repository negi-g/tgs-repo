package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomMappingResponse {

	private String standardName;
	private String view;
	private String category;
	private String smokingAllowed;
	private List<String> bedTypes;
	private Boolean soleUse;
	private Boolean hasBalcony;
	private Boolean hasTerrace;
	private List<Integer> roomIndexes;
}
