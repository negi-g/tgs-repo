package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Terminal {

    private String terminalCode;
    private Integer distance;
}
