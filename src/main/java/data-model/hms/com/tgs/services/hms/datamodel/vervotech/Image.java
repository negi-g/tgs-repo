package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Image {

	private String Caption;
	private String Category;
	private List<Link> Links;
	private List<String> RoomId;
}