package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Wildcard {

	private String roomType;
	private String roomCode;
	private String characteristicCode;
	private HotelRoomDescription hotelRoomDescription;
}
