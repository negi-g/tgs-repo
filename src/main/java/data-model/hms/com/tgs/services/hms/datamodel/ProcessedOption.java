package com.tgs.services.hms.datamodel;

import java.util.List;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class ProcessedOption {

	@CustomSerializedName(key = FieldName.OPTION_FACILITIES)
	private List<String> facilities;

	@CustomSerializedName(key = FieldName.PROCESSED_OPTION_TOTAL_PRICE)
	private Double totalPrice;
	@CustomSerializedName(key = FieldName.PROCESSED_OPTION_DISCOUNTED_PRICE)
	private Double discountedPrice;

	@APIUserExclude
	@CustomSerializedName(key = FieldName.SUPPLIER_NAME)
	private String supplierName;

	@Exclude
	private Boolean isUpdated;
}
