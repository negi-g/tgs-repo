package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PromotionResponse")
@Data
public class PromotionResponse {

    @XmlAttribute(name = "savings", required = true)
    protected BigDecimal savings;
    @XmlAttribute(name = "text")
    protected String text;
}
