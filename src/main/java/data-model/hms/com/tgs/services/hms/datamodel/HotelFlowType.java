package com.tgs.services.hms.datamodel;

import lombok.Getter;

@Getter
public enum HotelFlowType {

	SEARCH("Search"),
	DETAIL("Detail"),
	REVIEW("Review"),
	BOOKING_DETAIL("Booking_Detail"),
	BOOK("Book"),
	CANCELLATION("Cancellation"),
	QUOTATION("Quotation"),
	AMENDMENT("Amendement"),
	STATIC_CHANGE("Static_Change"),
	RECONCILATION("Recon"),
	RESYNC("Resync"),
	IMPORT_BOOKING("Import_Book");

	HotelFlowType(String name) {
		this.name = name;
	}

	private String name;
}
