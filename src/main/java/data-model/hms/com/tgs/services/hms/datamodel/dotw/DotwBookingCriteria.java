package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType (propOrder={"fromDate","toDate","currency" , "productId"
		,"customerReference" , "rooms"})
public class DotwBookingCriteria extends DotwRequestCriteria {

}
