package com.tgs.services.hms.datamodel.agoda.statc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="hotel_description")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaHotelDescription {
	
	@XmlElement(name = "hotel_id")
    protected Long hotelId;
	
    protected String overview;
    protected String snippet;

}
