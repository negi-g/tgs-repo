package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Descriptions {
	
	private String amenities;
	private String renovations;
	private String national_ratings;
	private String business_amenities;
	private String rooms;
	private String attractions;
	private String location;
	private String headline;
}
