package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Room {
	public Integer RoomNo;
	public Integer NoofAdults;
	public Integer NoOfChild;
	@Builder.Default
	public List<Integer> ChildAge = null;

}
