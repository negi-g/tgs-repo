package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReviewData {

	private String location_id;
	private String name;

}
