package com.tgs.services.hms.datamodel.hotelconfigurator;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelPanConfigOutput implements IRuleOutPut {
	@SerializedName("isppr")
	private Boolean isPersonalPanRequired;

	@SerializedName("isopr")
	private Boolean isOnePanRequiredPerBooking;
}
