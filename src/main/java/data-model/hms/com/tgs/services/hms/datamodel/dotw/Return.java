package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="return")
@XmlAccessorType(XmlAccessType.FIELD)
public class Return {

	@XmlElement
	private Boolean getRooms;
	
	@XmlElement(name = "filters")
	private Filter filters;
	
	@XmlElement(name = "fields")
	private Field fields;
	
	
	
	public Field getFields() {
		if(fields == null) fields = new Field();
		return fields;
	}
	
}