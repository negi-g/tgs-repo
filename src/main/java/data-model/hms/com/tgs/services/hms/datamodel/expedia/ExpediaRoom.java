package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ExpediaRoom {

	private String title;
	private String given_name;
	private String family_name;
//	private boolean smoking;
//	private String special_request;
}
