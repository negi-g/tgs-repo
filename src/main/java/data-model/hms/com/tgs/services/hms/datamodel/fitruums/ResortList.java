
package com.tgs.services.hms.datamodel.fitruums;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "resorts" })
@XmlRootElement(name = "ResortList")
@Getter
@Setter
public class ResortList {

	@XmlElement(name = "Resorts", required = true)
	protected ResortList.Resorts resorts;

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "resort" })
	@Getter
	@Setter
	public static class Resorts {

		@XmlElement(name = "Resort", required = true)
		protected List<ResortList.Resorts.Resort> resort;

		public List<ResortList.Resorts.Resort> getResort() {
			if (resort == null) {
				resort = new ArrayList<ResortList.Resorts.Resort>();
			}
			return this.resort;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "resortId", "resortName", "destinationId", "destinationName", "countryName",
				"countryCode" })
		@Getter
		@Setter
		public static class Resort {

			@XmlElement(name = "ResortId", required = true)
			protected String resortId;
			@XmlElement(name = "ResortName", required = true)
			protected String resortName;
			@XmlElement(name = "destination_id", required = true)
			protected String destinationId;
			@XmlElement(name = "DestinationName", required = true)
			protected String destinationName;
			@XmlElement(name = "CountryName", required = true)
			protected String countryName;
			@XmlElement(name = "CountryCode", required = true)
			protected String countryCode;

		}

	}

}
