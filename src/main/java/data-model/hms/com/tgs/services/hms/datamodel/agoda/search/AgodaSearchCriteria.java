package com.tgs.services.hms.datamodel.agoda.search;

import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgodaSearchCriteria  {
	@XmlElement
	private String type;
	@XmlElement
	private String id;
	@XmlElement
	private String CheckIn;
	@XmlElement
	private String CheckOut;
	@XmlElement
	private String Rooms;
	@XmlElement
	private String Adults;
	@XmlElement
	private String Children;
	@XmlElement
	private List<ChildrenAges> ChildrenAges;
	@XmlElement
	private String Language;
	@XmlElement
	private String Currency;
	@XmlElement
	private String UserCountry;
	
}
