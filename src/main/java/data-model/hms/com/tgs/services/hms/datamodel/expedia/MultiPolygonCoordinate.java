package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MultiPolygonCoordinate {

	private Double center_longitude;
	private Double center_latitude;
	private MultiBoundingPolygon bounding_polygon;
}
