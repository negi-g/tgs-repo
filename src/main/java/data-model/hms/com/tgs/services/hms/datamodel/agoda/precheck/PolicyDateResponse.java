package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyDateResponse", propOrder = {
    "rate"
})
@Data
public class PolicyDateResponse {

    @XmlElement(required = true)
    protected RateResponse rate;
    @XmlAttribute(name = "before")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar before;
    @XmlAttribute(name = "after")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar after;

}
