package com.tgs.services.hms.datamodel.travelbullz;


import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
@Data
@Builder
public class Request {
	public List<Room> Rooms;
	private String CountryId;
	private String CityID;
	public String CheckInDate;
	public String CheckOutDate;
	public String NoofNights;
	public String Nationality;
	public Filters Filters;
	public String SearchKey;
	public HotelBookOption HotelOption;
	public BookRQ BookRQ;
	public BookingDetailRQ BookingDetailRQ;
	public CheckHotelCancellationChargesRQ CheckHotelCancellationChargesRQ;
	public CancelRQ CancelRQ;
	public BookingSearchRQ BookingSearchRQ;

}
