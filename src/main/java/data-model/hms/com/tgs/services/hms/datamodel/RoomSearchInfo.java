package com.tgs.services.hms.datamodel;

import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.SystemError;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@Getter
@Setter
public class RoomSearchInfo implements Validatable {
	
	/*
	 * Update Equals method also if any change in data model 
	 */
	
	private int numberOfAdults;
	private Integer numberOfChild;
	private List<Integer> childAge;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		if (numberOfAdults == 0) {
			errors.put("numberOfAdults", SystemError.INVALID_NUMBER_OF_ADULTS.getErrorDetail());
		}

		if (numberOfChild != null && numberOfChild == 0 && CollectionUtils.isNotEmpty(childAge)) {
			errors.put("childAge", SystemError.INVALID_CHILD_AGE_LIST.getErrorDetail());
		}
		if (numberOfChild != null && numberOfChild != 0) {
			if (CollectionUtils.isNotEmpty(childAge)) {
				for (Integer cAge : childAge) {
					if (cAge > 12 || cAge < 0) {
						errors.put("childAge", SystemError.HOTEL_CHILD_MAX_MIN_AGE_LIMIT.getErrorDetail());
						break;
					} else if (numberOfChild != childAge.size()) {
						errors.put("childAge", SystemError.INVALID_CHILD_AGE_LIST.getErrorDetail());
						break;
					}
				}
			} else {
				errors.put("childAge", SystemError.INVALID_CHILD_AGE_LIST.getErrorDetail());
			}
		}
		return errors;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((childAge == null) ? 0 : childAge.hashCode());
		result = prime * result + numberOfAdults;
		result = prime * result + ((numberOfChild == null) ? 0 : numberOfChild.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		RoomSearchInfo other = (RoomSearchInfo) obj;
		
		if(this.getNumberOfAdults() != other.getNumberOfAdults()) { return false; }
		if(ObjectUtils.compare(this.getNumberOfChild(), other.getNumberOfChild()) != 0) { return false; }
		if(!Objects.equals(this.getChildAge(), other.getChildAge())) { return false; }
		
		return true;
	}
}
