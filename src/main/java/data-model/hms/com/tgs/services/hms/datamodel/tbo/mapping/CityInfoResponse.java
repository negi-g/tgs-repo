package com.tgs.services.hms.datamodel.tbo.mapping;

import java.util.List;
import lombok.Data;

@Data
public class CityInfoResponse {
	
	public String TraceId;
	public String TokenId;
	public Integer Status;
	public List<Destination> Destinations = null;

}
