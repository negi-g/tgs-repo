package com.tgs.services.hms.datamodel.qtech;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PriceChangeResponse {
	private String Message;
	private String SectionUniqueId;
	private String StartTime;
	private String EndTime;
	private List<RoomRates> RoomRates;
}
