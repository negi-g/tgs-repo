package com.tgs.services.hms.datamodel.dotw;

import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@XmlType(namespace="http://us.dotwconnect.com/xsd/atomicCondition")
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwFilterCriteria {

	@XmlElement
	private String fieldName;
	
	@XmlElement
	private String fieldTest;
	
	@XmlElementWrapper(name = "fieldValues")
	@XmlElement(name="fieldValue")
	private Set<String> fieldValue;
}
