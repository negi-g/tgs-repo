package com.tgs.services.hms.datamodel.tbo.staticData;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import lombok.Getter;
import lombok.Setter;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "addressLine",
    "cityName",
    "postalCode",
    "stateProv",
    "countryName"
})
@XmlRootElement(name = "Address")
@Getter
@Setter
public class Address {

    @XmlElement(name = "AddressLine", required = false)
    protected List<String> addressLine;
    @XmlElement(name = "CityName", required = false)
    protected String cityName;
    @XmlElement(name = "PostalCode")
    @XmlSchemaType(name = "unsignedInt")
    protected long postalCode;
    @XmlElement(name = "StateProv", required = false)
    protected String stateProv;
    @XmlElement(name = "CountryName", required = false)
    protected Address.CountryName countryName;
    
    public List<String> getAddressLine() {
        if (addressLine == null) {
            addressLine = new ArrayList<String>();
        }
        return this.addressLine;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    @Getter
    @Setter
    public static class CountryName {
        @XmlValue
        protected String value;
        @XmlAttribute(name = "Code", required = false)
        protected String code;
    }

}
