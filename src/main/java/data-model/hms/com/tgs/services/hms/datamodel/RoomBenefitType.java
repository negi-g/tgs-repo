package com.tgs.services.hms.datamodel;

public enum RoomBenefitType {
	PROMOTION, SERVICE, BENEFIT;
}
