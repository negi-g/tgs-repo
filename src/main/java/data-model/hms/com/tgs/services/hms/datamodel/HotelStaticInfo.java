package com.tgs.services.hms.datamodel;

import java.util.List;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class HotelStaticInfo {

	@CustomSerializedName(key = FieldName.HOTEL_INFO_LIST)
	private List<HotelInfo> hotelInfoList;
	private String next;
}
