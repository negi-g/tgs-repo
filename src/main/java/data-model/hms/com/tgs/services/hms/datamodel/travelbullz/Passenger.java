package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Passenger {
	public Integer IsLead;
	public String PaxType;
	public String Name;
	public String ChildAge;
}
