package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cancellationPenalty", propOrder = {
    "penaltyApplied",
    "charge",
    "currency",
    "currencyShort"
})
public class CancellationPenaltyType {

    protected FormattedPriceType penaltyApplied;
    protected FormattedPriceType charge;
    @XmlSchemaType(name = "unsignedShort")
    protected int currency;
    @XmlElement(required = true)
    protected String currencyShort;

}
