package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@Builder
public class CountryRequest {
	
	private String Token;

}
