package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationMapperRequest {

	private String key;
	private String category;
	private String q;
	
}
