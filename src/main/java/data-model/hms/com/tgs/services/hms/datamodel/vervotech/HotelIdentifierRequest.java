package com.tgs.services.hms.datamodel.vervotech;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class HotelIdentifierRequest extends VervotechBaseRequest {

	private String lastUpdateDateTime;
	private String offset;
	private String limit;
	private String providerFamily;
}
