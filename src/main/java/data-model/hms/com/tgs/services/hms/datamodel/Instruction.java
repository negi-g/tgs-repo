package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class Instruction {

	private InstructionType type;
	private String msg;
}
