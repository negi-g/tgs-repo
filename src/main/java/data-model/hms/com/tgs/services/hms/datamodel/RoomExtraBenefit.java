package com.tgs.services.hms.datamodel;

import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoomExtraBenefit {
	private String type;
	private Set<String> values;

}
