package com.tgs.services.hms.datamodel.tbo.search;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class HotelStaticDetailRequest extends TBOBaseRequest {

	private String CityId;
	private String HotelId;
	private String EndUserIp;
	private String TokenId;
	private String ClientId;
}
