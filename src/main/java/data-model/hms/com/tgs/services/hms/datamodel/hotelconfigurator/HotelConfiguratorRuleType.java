package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.Arrays;
import java.util.List;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import lombok.Getter;

@Getter
public enum HotelConfiguratorRuleType {

	// ENUM Name Should not exceed 14 Character
	GNPURPOSE(HotelBasicRuleCriteria.class, new TypeToken<HotelGeneralPurposeOutput>() {}),
	PROMOTION(HotelBasicRuleCriteria.class, new TypeToken<HotelPromotionOutput>() {}),
	PROPERTY(HotelBasicRuleCriteria.class, new TypeToken<HotelPropertyCategoryOutput>() {}),
	CLIENTFEE(HotelBasicRuleCriteria.class, new TypeToken<HotelClientFeeOutput>() {}),
	CLIENTMARKUP(HotelBasicRuleCriteria.class, new TypeToken<HotelClientMarkupOutput>() {}),
	SUPPLIERCONFIG(HotelBasicRuleCriteria.class, new TypeToken<HotelSupplierConfigOutput>() {}),
	SOURCECONFIG(HotelBasicRuleCriteria.class, new TypeToken<HotelSourceConfigOutput>() {}),
	STATICDATACONFIG(HotelBasicRuleCriteria.class, new TypeToken<HotelStaticDataConfigOutput>() {}),
	HOTELINFOFILTERATION(HotelBasicRuleCriteria.class, new TypeToken<HotelInfoFilterationOutput>() {}),
	PASSPORTCONFIG(HotelBasicRuleCriteria.class, new TypeToken<HotelPassportConfigOutput>() {}),
	PANCONFIG(HotelBasicRuleCriteria.class, new TypeToken<HotelPanConfigOutput>() {}),
	SEARCHEXCLUSION(HotelBasicRuleCriteria.class, new TypeToken<SearchExclusionOutput>() {}),
	HOTELIDSEARCH(HotelBasicRuleCriteria.class, new TypeToken<HotelIdSearchOutput>() {});

	private Class<? extends IRuleCriteria> inputType;
	private TypeToken<? extends IRuleOutPut> outPutTypeToken;

	<T extends IRuleCriteria, RT extends IRuleOutPut> HotelConfiguratorRuleType(Class<T> inputType,
			TypeToken<RT> outPutTypeToken) {
		this.inputType = inputType;
		this.outPutTypeToken = outPutTypeToken;
	}

	public List<HotelConfiguratorRuleType> getAirConfigRuleType() {
		return Arrays.asList(HotelConfiguratorRuleType.values());
	}

	public static HotelConfiguratorRuleType getRuleType(String name) {
		for (HotelConfiguratorRuleType ruleType : HotelConfiguratorRuleType.values()) {
			if (ruleType.getName().equals(name)) {
				return ruleType;
			}
		}
		return null;
	}

	public String getName() {
		return this.name();
	}

	public String getCode() {
		return this.name();
	}

}
