package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelsResponse", propOrder = {
    "recommendation"
})
@Data
public class HotelsResponse {

    @XmlElement(required = true)
    protected List<HotelResponse> recommendation;

    public List<HotelResponse> getRecommendation() {
        if (recommendation == null) {
            recommendation = new ArrayList<HotelResponse>();
        }
        return this.recommendation;
    }

}
