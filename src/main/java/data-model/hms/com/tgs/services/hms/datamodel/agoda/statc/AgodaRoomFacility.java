package com.tgs.services.hms.datamodel.agoda.statc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@XmlRootElement(name="roomtype_facility")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaRoomFacility {

	@XmlElement
	private String hotel_id;
	
	@XmlElement
	private String hotel_room_type_id;
	
	@XmlElement
	private String property_name;
}