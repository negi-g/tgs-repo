package com.tgs.services.hms.datamodel.travelbullz;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Filters {
	public String IsRecommendedOnly;
	public String IsShowRooms;
	public String IsOnlyAvailable;
	public StarRating StarRating;
	public String HotelIds;
}
