package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HotelBookOption {
	public String HotelOptionId;
	@Builder.Default
	public List<HotelRoom> HotelRooms = null;
	public String HotelName;
	public String Nationality;
	public String BookingToken;
	public String Status;
	public Integer IsPriceChange;
	public String CheckInDate;
	public String CheckOutDate;
	public Double TotalPrice;
	public String LeadPaxName;
	public String BookingDate;
	public String ReferenceNo;
	public String InternalReference;
	public Integer BookingId;
	public String HotelId;
}
