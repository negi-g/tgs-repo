package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class HotelRegionInfo {

	private Long id;
	@SerializedName("name")
	private String regionName;
	@SerializedName("type")
	private String regionType;
	@SerializedName("fullName")
	private String fullRegionName;
	private String countryName;
	private LocalDateTime createdOn;
	private LocalDateTime processedOn;
	private Boolean enabled;
	private Long priority;
	private String iataCode;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass()) {
			return false;
		}
		HotelRegionInfo regionInfo = (HotelRegionInfo) obj;
		if (!Objects.equals(this.getRegionName(), regionInfo.getRegionName()))
			return false;
		if (!Objects.equals(this.getRegionType(), regionInfo.getRegionType()))
			return false;
		if (!Objects.equals(this.getCountryName(), regionInfo.getCountryName()))
			return false;
		return true;
	}
}
