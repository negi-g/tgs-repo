package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Data;

@Data
public class BookRS {
	public Integer BookingId;
	public String ReferenceNo;
	public String InternalReference;
	public String Currency;
}
