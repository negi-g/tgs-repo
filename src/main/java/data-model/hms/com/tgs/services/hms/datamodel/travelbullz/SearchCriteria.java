package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SearchCriteria {
	public Dates Dates;
}
