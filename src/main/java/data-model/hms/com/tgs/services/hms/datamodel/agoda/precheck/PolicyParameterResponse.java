package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyParameterResponse")
@Data
public class PolicyParameterResponse {

    @XmlAttribute(name = "days", required = true)
    protected BigInteger days;
    @XmlAttribute(name = "charge", required = true)
    protected String charge;
    @XmlAttribute(name = "value", required = true)
    protected BigInteger value;
    
}
