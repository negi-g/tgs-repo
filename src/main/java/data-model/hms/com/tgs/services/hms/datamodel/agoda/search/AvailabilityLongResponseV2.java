
package com.tgs.services.hms.datamodel.agoda.search;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;

import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.RateType;

import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"errorMessages", "hotels"})
@XmlRootElement(name = "AvailabilityLongResponseV2")
@Data
public class AvailabilityLongResponseV2 {

	@XmlElement(name = "ErrorMessages")
	protected AvailabilityLongResponseV2.ErrorMessages errorMessages;
	@XmlElement(name = "Hotels")
	protected AvailabilityLongResponseV2.Hotels hotels;
	@XmlAttribute(name = "searchid", required = true)
	protected long searchid;
	@XmlAttribute(name = "status", required = true)
	protected String status;

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {"errorMessage"})
	@Data
	public static class ErrorMessages {

		@XmlElement(name = "ErrorMessage", required = true)
		protected AvailabilityLongResponseV2.ErrorMessages.ErrorMessage errorMessage;

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"value"})
		@Data
		public static class ErrorMessage {

			@XmlValue
			protected String value;
			@XmlAttribute(name = "id", required = true)
			@XmlSchemaType(name = "unsignedShort")
			protected int id;
			@XmlAttribute(name = "subid", required = true)
			@XmlSchemaType(name = "unsignedByte")
			protected short subid;

		}

	}


	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {"hotel"})

	public static class Hotels {

		public void setHotel(List<AvailabilityLongResponseV2.Hotels.Hotel> hotel) {
			this.hotel = hotel;
		}

		@XmlElement(name = "Hotel", required = true)
		protected List<AvailabilityLongResponseV2.Hotels.Hotel> hotel;


		public List<AvailabilityLongResponseV2.Hotels.Hotel> getHotel() {
			if (hotel == null) {
				hotel = new ArrayList<AvailabilityLongResponseV2.Hotels.Hotel>();
			}
			return this.hotel;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"id", "CheapestRoom", "paxSettings", "rooms"})

		public static class Hotel {

			public String getId() {
				return id;
			}

			public void setId(String id) {
				this.id = id;
			}

			public AvailabilityLongResponseV2.Hotels.Hotel.PaxSettings getPaxSettings() {
				return paxSettings;
			}

			public void setPaxSettings(AvailabilityLongResponseV2.Hotels.Hotel.PaxSettings paxSettings) {
				this.paxSettings = paxSettings;
			}

			public AvailabilityLongResponseV2.Hotels.Hotel.Rooms getRooms() {
				return rooms;
			}

			public void setRooms(AvailabilityLongResponseV2.Hotels.Hotel.Rooms rooms) {
				this.rooms = rooms;
			}

			public CheapestRoom getCheapestRoom() {
				return CheapestRoom;
			}

			public void setCheapestRoom(CheapestRoom cheapestRoom) {
				CheapestRoom = cheapestRoom;
			}

			@XmlElement(name = "Id")
			@XmlSchemaType(name = "unsignedInt")
			protected String id;
			@XmlElement(name = "PaxSettings", required = true)
			protected AvailabilityLongResponseV2.Hotels.Hotel.PaxSettings paxSettings;
			@XmlElement(name = "Rooms", required = true)
			protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms rooms;

			@XmlElement(name = "CheapestRoom", required = true)
			protected CheapestRoom CheapestRoom;


			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "")
			@Data
			public static class CheapestRoom {

				@XmlAttribute(name = "exclusive", required = true)
				protected double exclusive;
				@XmlAttribute(name = "tax", required = true)
				protected double tax;
				@XmlAttribute(name = "fees", required = true)
				protected double fees;
				@XmlAttribute(name = "inclusive", required = true)
				protected double inclusive;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "")
			@Data
			public static class PaxSettings {

				@XmlAttribute(name = "submit", required = true)
				protected String submit;
				@XmlAttribute(name = "infantage", required = true)
				@XmlSchemaType(name = "unsignedInt")
				protected long infantage;
				@XmlAttribute(name = "childage", required = true)
				@XmlSchemaType(name = "unsignedInt")
				protected long childage;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {"room"})

			public static class Rooms {

				public void setRoom(List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room> room) {
					this.room = room;
				}


				@XmlElement(name = "Room", required = true)
				protected List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room> room;


				public List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room> getRoom() {
					if (room == null) {
						room = new ArrayList<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room>();
					}
					return this.room;
				}


				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "",
						propOrder = {"standardTranslation", "benefits", "parentRoom", "maxRoomOccupancy",
								"remainingRooms", "amendable", "bookWithoutCreditCard", "rateInfo", "cancellation",
								"dmcid", "landingURL", "reuseHints", "supplierFinancialData"})

				public static class Room {

					public String getStandardTranslation() {
						return standardTranslation;
					}

					public void setStandardTranslation(String standardTranslation) {
						this.standardTranslation = standardTranslation;
					}

					public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Benefits getBenefits() {
						return benefits;
					}

					public void setBenefits(AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Benefits benefits) {
						this.benefits = benefits;
					}

					public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.ParentRoom getParentRoom() {
						return parentRoom;
					}

					public void setParentRoom(
							AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.ParentRoom parentRoom) {
						this.parentRoom = parentRoom;
					}

					public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.MaxRoomOccupancy getMaxRoomOccupancy() {
						return maxRoomOccupancy;
					}

					public void setMaxRoomOccupancy(
							AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.MaxRoomOccupancy maxRoomOccupancy) {
						this.maxRoomOccupancy = maxRoomOccupancy;
					}

					public int getRemainingRooms() {
						return remainingRooms;
					}

					public void setRemainingRooms(int remainingRooms) {
						this.remainingRooms = remainingRooms;
					}

					public Boolean getAmendable() {
						return amendable;
					}

					public void setAmendable(Boolean amendable) {
						this.amendable = amendable;
					}

					public Boolean getBookWithoutCreditCard() {
						return bookWithoutCreditCard;
					}

					public void setBookWithoutCreditCard(Boolean bookWithoutCreditCard) {
						this.bookWithoutCreditCard = bookWithoutCreditCard;
					}

					public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo getRateInfo() {
						return rateInfo;
					}

					public void setRateInfo(AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo rateInfo) {
						this.rateInfo = rateInfo;
					}

					public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation getCancellation() {
						return cancellation;
					}

					public void setCancellation(
							AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation cancellation) {
						this.cancellation = cancellation;
					}

					public Long getDmcid() {
						return dmcid;
					}

					public void setDmcid(Long dmcid) {
						this.dmcid = dmcid;
					}

					public String getLandingURL() {
						return landingURL;
					}

					public void setLandingURL(String landingURL) {
						this.landingURL = landingURL;
					}

					public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.ReuseHints getReuseHints() {
						return reuseHints;
					}

					public void setReuseHints(
							AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.ReuseHints reuseHints) {
						this.reuseHints = reuseHints;
					}

					public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.SupplierFinancialData getSupplierFinancialData() {
						return supplierFinancialData;
					}

					public void setSupplierFinancialData(
							AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.SupplierFinancialData supplierFinancialData) {
						this.supplierFinancialData = supplierFinancialData;
					}

					public String getId() {
						return id;
					}

					public void setId(String id) {
						this.id = id;
					}

					public Long getPromotionid() {
						return promotionid;
					}

					public void setPromotionid(Long promotionid) {
						this.promotionid = promotionid;
					}

					public String getName() {
						return name;
					}

					public void setName(String name) {
						this.name = name;
					}

					public int getLineitemid() {
						return lineitemid;
					}

					public void setLineitemid(int lineitemid) {
						this.lineitemid = lineitemid;
					}

					public String getRateplan() {
						return rateplan;
					}

					public void setRateplan(String rateplan) {
						this.rateplan = rateplan;
					}

					public String getRatetype() {
						return ratetype;
					}

					public void setRatetype(String ratetype) {
						this.ratetype = ratetype;
					}

					public String getRatechannel() {
						return ratechannel;
					}

					public void setRatechannel(String ratechannel) {
						this.ratechannel = ratechannel;
					}

					public Long getRateplanid() {
						return rateplanid;
					}

					public void setRateplanid(Long rateplanid) {
						this.rateplanid = rateplanid;
					}

					public String getCurrency() {
						return currency;
					}

					public void setCurrency(String currency) {
						this.currency = currency;
					}

					public String getModel() {
						return model;
					}

					public void setModel(String model) {
						this.model = model;
					}

					public long getRatecategoryid() {
						return ratecategoryid;
					}

					public void setRatecategoryid(long ratecategoryid) {
						this.ratecategoryid = ratecategoryid;
					}

					public String getBlockid() {
						return blockid;
					}

					public void setBlockid(String blockid) {
						this.blockid = blockid;
					}

					public XMLGregorianCalendar getBooknowpaylaterdate() {
						return booknowpaylaterdate;
					}

					public void setBooknowpaylaterdate(XMLGregorianCalendar booknowpaylaterdate) {
						this.booknowpaylaterdate = booknowpaylaterdate;
					}

					public Boolean getPromoeligible() {
						return promoeligible;
					}

					public void setPromoeligible(Boolean promoeligible) {
						this.promoeligible = promoeligible;
					}

					@XmlElement(name = "StandardTranslation", required = true)
					protected String standardTranslation;
					@XmlElement(name = "Benefits")
					protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Benefits benefits;
					@XmlElement(name = "ParentRoom")
					protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.ParentRoom parentRoom;
					@XmlElement(name = "MaxRoomOccupancy")
					protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.MaxRoomOccupancy maxRoomOccupancy;
					@XmlElement(name = "RemainingRooms")
					@XmlSchemaType(name = "unsignedInt")
					protected int remainingRooms;
					@XmlElement(name = "Amendable")
					protected Boolean amendable;
					@XmlElement(name = "BookWithoutCreditCard")
					protected Boolean bookWithoutCreditCard;
					@XmlElement(name = "RateInfo", required = true)
					protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo rateInfo;
					@XmlElement(name = "Cancellation", required = true)
					protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation cancellation;
					@XmlElement(name = "DMCID")
					@XmlSchemaType(name = "unsignedInt")
					protected Long dmcid;
					@XmlElement(name = "LandingURL")
					protected String landingURL;
					@XmlElement(name = "ReuseHints")
					protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.ReuseHints reuseHints;
					@XmlElement(name = "SupplierFinancialData")
					protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.SupplierFinancialData supplierFinancialData;
					@XmlAttribute(name = "id", required = true)
					@XmlSchemaType(name = "unsignedInt")
					protected String id;
					@XmlAttribute(name = "promotionid")
					@XmlSchemaType(name = "unsignedInt")
					protected Long promotionid;
					@XmlAttribute(name = "name", required = true)
					protected String name;
					@XmlAttribute(name = "lineitemid", required = true)
					@XmlSchemaType(name = "positiveInteger")
					protected int lineitemid;
					@XmlAttribute(name = "rateplan", required = true)
					protected String rateplan;
					@XmlAttribute(name = "ratetype", required = true)
					protected String ratetype;
					@XmlAttribute(name = "ratechannel")
					protected String ratechannel;
					@XmlAttribute(name = "rateplanid")
					@XmlSchemaType(name = "unsignedInt")
					protected Long rateplanid;
					@XmlAttribute(name = "currency", required = true)
					protected String currency;
					@XmlAttribute(name = "model", required = true)
					protected String model;
					@XmlAttribute(name = "ratecategoryid", required = true)
					@XmlSchemaType(name = "unsignedInt")
					protected long ratecategoryid;
					@XmlAttribute(name = "blockid", required = true)
					protected String blockid;
					@XmlAttribute(name = "booknowpaylaterdate")
					@XmlSchemaType(name = "date")
					protected XMLGregorianCalendar booknowpaylaterdate;
					@XmlAttribute(name = "promoeligible")
					protected Boolean promoeligible;

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = {"benefit"})
					@Data
					public static class Benefits {

						@XmlElement(name = "Benefit", required = true)
						protected List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Benefits.Benefit> benefit;

						public List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Benefits.Benefit> getBenefit() {
							if (benefit == null) {
								benefit =
										new ArrayList<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Benefits.Benefit>();
							}
							return this.benefit;
						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"name", "translation"})
						@Data
						public static class Benefit {

							@XmlElement(name = "Name", required = true)
							protected String name;
							@XmlElement(name = "Translation", required = true)
							protected String translation;
							@XmlAttribute(name = "id", required = true)
							@XmlSchemaType(name = "unsignedInt")
							protected long id;

						}

					}

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "",
							propOrder = {"policyText", "policyTranslated", "policyParameters", "policyDates"})
					@Data
					public static class Cancellation {

						@XmlElement(name = "PolicyText")
						protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyText policyText;
						@XmlElement(name = "PolicyTranslated")
						protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyTranslated policyTranslated;
						@XmlElement(name = "PolicyParameters")
						protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyParameters policyParameters;
						@XmlElement(name = "PolicyDates")
						protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyDates policyDates;
						@XmlAttribute(name = "code")
						protected String code;

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"policyDate"})
						@Data
						public static class PolicyDates {

							@XmlElement(name = "PolicyDate", required = true)
							protected List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyDates.PolicyDate> policyDate;

							public List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyDates.PolicyDate> getPolicyDate() {
								if (policyDate == null) {
									policyDate =
											new ArrayList<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyDates.PolicyDate>();
								}
								return this.policyDate;
							}

							@XmlAccessorType(XmlAccessType.FIELD)
							@XmlType(name = "")
							@Data
							public static class PolicyDate {

								@XmlAttribute(name = "before")
								@XmlSchemaType(name = "date")
								protected String before;
								@XmlElement(name = "Rate")
								protected RateType rate;
								@XmlAttribute(name = "after")
								@XmlSchemaType(name = "date")
								protected String after;
							}

						}
						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"policyParameter"})
						@Data
						public static class PolicyParameters {

							@XmlElement(name = "PolicyParameter", required = true)
							protected List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyParameters.PolicyParameter> policyParameter;

							public List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyParameters.PolicyParameter> getPolicyParameter() {
								if (policyParameter == null) {
									policyParameter =
											new ArrayList<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyParameters.PolicyParameter>();
								}
								return this.policyParameter;
							}

							@XmlAccessorType(XmlAccessType.FIELD)
							@XmlType(name = "", propOrder = {"value"})
							@Data
							public static class PolicyParameter {

								@XmlValue
								@XmlSchemaType(name = "unsignedInt")
								protected long value;
								@XmlAttribute(name = "days", required = true)
								@XmlSchemaType(name = "unsignedInt")
								protected long days;
								@XmlAttribute(name = "charge", required = true)
								protected String charge;

							}

						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"value"})
						@Data
						public static class PolicyText {

							@XmlValue
							protected String value;
							@XmlAttribute(name = "language", required = true)
							protected String language;
						}
						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"value"})
						@Data
						public static class PolicyTranslated {

							@XmlValue
							protected String value;
							@XmlAttribute(name = "language", required = true)
							protected String language;

						}

					}
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "")
					@Data
					public static class MaxRoomOccupancy {

						@XmlAttribute(name = "normalbedding", required = true)
						@XmlSchemaType(name = "unsignedInt")
						protected long normalbedding;
						@XmlAttribute(name = "extrabeds", required = true)
						@XmlSchemaType(name = "unsignedInt")
						protected long extrabeds;

					}
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "")
					@Data
					public static class ParentRoom {

						@XmlAttribute(name = "id", required = true)
						protected String id;
						@XmlAttribute(name = "name", required = true)
						protected String name;
						@XmlAttribute(name = "translationname", required = true)
						protected String translationname;


					}


					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = {"rewardPoints", "promotion", "promotionType", "dailyRates",
							"taxServiceBreakdown", "rate", "included", "excluded", "surcharges", "totalPaymentAmount"})

					public static class RateInfo {

						public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.RewardPoints getRewardPoints() {
							return rewardPoints;
						}

						public void setRewardPoints(
								AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.RewardPoints rewardPoints) {
							this.rewardPoints = rewardPoints;
						}

						public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.Promotion getPromotion() {
							return promotion;
						}

						public void setPromotion(
								AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.Promotion promotion) {
							this.promotion = promotion;
						}

						public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.PromotionType getPromotionType() {
							return promotionType;
						}

						public void setPromotionType(
								AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.PromotionType promotionType) {
							this.promotionType = promotionType;
						}

						public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.DailyRates getDailyRates() {
							return dailyRates;
						}

						public void setDailyRates(
								AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.DailyRates dailyRates) {
							this.dailyRates = dailyRates;
						}

						public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.TaxServiceBreakdown getTaxServiceBreakdown() {
							return taxServiceBreakdown;
						}

						public void setTaxServiceBreakdown(
								AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.TaxServiceBreakdown taxServiceBreakdown) {
							this.taxServiceBreakdown = taxServiceBreakdown;
						}

						public String getIncluded() {
							return included;
						}

						public void setIncluded(String included) {
							this.included = included;
						}

						public String getExcluded() {
							return excluded;
						}

						public void setExcluded(String excluded) {
							this.excluded = excluded;
						}

						public AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.Surcharges getSurcharges() {
							return surcharges;
						}

						public void setSurcharges(
								AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.Surcharges surcharges) {
							this.surcharges = surcharges;
						}

						public RateType getRate() {
							return rate;
						}

						public void setRate(RateType rate) {
							this.rate = rate;
						}

						public RateType getTotalPaymentAmount() {
							return totalPaymentAmount;
						}

						public void setTotalPaymentAmount(RateType totalPaymentAmount) {
							this.totalPaymentAmount = totalPaymentAmount;
						}

						@XmlElement(name = "RewardPoints")
						protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.RewardPoints rewardPoints;
						@XmlElement(name = "Promotion")
						protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.Promotion promotion;
						@XmlElement(name = "PromotionType")
						protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.PromotionType promotionType;
						@XmlElement(name = "DailyRates")
						protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.DailyRates dailyRates;
						@XmlElement(name = "TaxServiceBreakdown")
						protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.TaxServiceBreakdown taxServiceBreakdown;
						@XmlElement(name = "Included")
						protected String included;
						@XmlElement(name = "Excluded")
						protected String excluded;
						@XmlElement(name = "Surcharges")
						protected AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.Surcharges surcharges;
						@XmlElement(name = "Rate")
						protected RateType rate;
						@XmlElement(name = "TotalPaymentAmount")
						protected RateType totalPaymentAmount;

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"exclusive", "tax", "fees", "inclusive"})
						@Data
						public static class RateType {

							@XmlAttribute(name = "exclusive", required = true)
							protected Double exclusive;
							@XmlAttribute(name = "tax", required = true)
							protected Double tax;
							@XmlAttribute(name = "fees", required = true)
							protected Double fees;
							@XmlAttribute(name = "inclusive", required = true)
							protected Double inclusive;
						}
						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"dailyRateDate"})
						@Data
						public static class DailyRates {

							@XmlElement(name = "DailyRateDate", required = true)
							protected List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.DailyRates.DailyRateDate> dailyRateDate;

							public List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.DailyRates.DailyRateDate> getDailyRateDate() {
								if (dailyRateDate == null) {
									dailyRateDate =
											new ArrayList<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.DailyRates.DailyRateDate>();
								}
								return this.dailyRateDate;
							}

							@XmlAccessorType(XmlAccessType.FIELD)
							@XmlType(name = "")
							@Data
							public static class DailyRateDate {

								@XmlAttribute(name = "date", required = true)
								@XmlSchemaType(name = "date")
								protected XMLGregorianCalendar date;
								@XmlAttribute(name = "exclusive", required = true)
								protected BigDecimal exclusive;
								@XmlAttribute(name = "tax", required = true)
								protected BigDecimal tax;
								@XmlAttribute(name = "fees", required = true)
								protected BigDecimal fees;
								@XmlAttribute(name = "processingfee")
								protected BigDecimal processingfee;
								@XmlAttribute(name = "inclusive", required = true)
								protected BigDecimal inclusive;
								@XmlAttribute(name = "margin")
								protected BigDecimal margin;
								@XmlAttribute(name = "taxduesupplier")
								protected BigDecimal taxduesupplier;
								@XmlAttribute(name = "reference")
								protected BigDecimal reference;

							}

						}
						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "")
						@Data
						public static class Promotion {

							@XmlAttribute(name = "savings", required = true)
							protected BigDecimal savings;
							@XmlAttribute(name = "text")
							protected String text;

						}
						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "")
						@Data
						public static class PromotionType {

							@XmlAttribute(name = "id", required = true)
							@XmlSchemaType(name = "unsignedInt")
							protected long id;
							@XmlAttribute(name = "text", required = true)
							protected String text;

						}
						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"value"})
						@Data
						public static class RewardPoints {

							@XmlValue
							@XmlSchemaType(name = "unsignedInt")
							protected long value;
							@XmlAttribute(name = "savings", required = true)
							protected BigDecimal savings;

						}
						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"surcharge"})
						@Data
						public static class Surcharges {

							@XmlElement(name = "Surcharge", required = true)
							protected List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.Surcharges.Surcharge> surcharge;

							public List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.Surcharges.Surcharge> getSurcharge() {
								if (surcharge == null) {
									surcharge =
											new ArrayList<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.Surcharges.Surcharge>();
								}
								return this.surcharge;
							}

							@XmlAccessorType(XmlAccessType.FIELD)
							@XmlType(name = "", propOrder = {"name","rate"})
							@Data
							public static class Surcharge {

								@XmlElement(name = "Name", required = true)
								protected String name;
								@XmlAttribute(name = "id", required = true)
								@XmlSchemaType(name = "unsignedInt")
								protected String id;
								@XmlAttribute(name = "method", required = true)
								protected String method;
								@XmlAttribute(name = "charge", required = true)
								protected String charge;
								@XmlAttribute(name = "margin", required = true)
								protected String margin;
								@XmlElement(name = "Rate")
								protected RateType rate;

							}

						}

						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"taxFee"})
						@Data
						public static class TaxServiceBreakdown {

							@XmlElement(name = "TaxFee")
							protected List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.TaxServiceBreakdown.TaxFee> taxFee;

							public List<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.TaxServiceBreakdown.TaxFee> getTaxFee() {
								if (taxFee == null) {
									taxFee = new ArrayList<AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.TaxServiceBreakdown.TaxFee>();
								}
								return this.taxFee;
							}

							@XmlAccessorType(XmlAccessType.FIELD)
							@XmlType(name = "")
							@Data
							public static class TaxFee {

								@XmlAttribute(name = "id", required = true)
								protected String id;
								@XmlAttribute(name = "type", required = true)
								protected String type;
								@XmlAttribute(name = "description", required = true)
								protected String description;
								@XmlAttribute(name = "translation", required = true)
								protected String translation;
								@XmlAttribute(name = "method", required = true)
								protected String method;
								@XmlAttribute(name = "taxable")
								protected String taxable;
								@XmlAttribute(name = "percent")
								protected BigDecimal percent;
								@XmlAttribute(name = "total", required = true)
								protected BigDecimal total;
								@XmlAttribute(name = "currency")
								protected String currency;
								@XmlAttribute(name = "amount")
								protected BigDecimal amount;
								@XmlAttribute(name = "taxprototypeid")
								@XmlSchemaType(name = "unsignedInt")
								protected Long taxprototypeid;
								@XmlAttribute(name = "taxduesupplier")
								protected BigDecimal taxduesupplier;
							}

						}

					}
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "")
					@Data
					public static class ReuseHints {


					}
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "")
					@Data
					public static class SupplierFinancialData {

						@XmlAttribute(name = "providerfee")
						protected BigDecimal providerfee;
						@XmlAttribute(name = "provideroverridesandcashback")
						protected BigDecimal provideroverridesandcashback;
						@XmlAttribute(name = "handicap")
						protected BigDecimal handicap;


					}

				}

			}

		}

	}

}
