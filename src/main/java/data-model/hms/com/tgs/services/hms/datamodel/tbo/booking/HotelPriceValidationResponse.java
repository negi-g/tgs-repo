package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelPriceValidationResponse {

	private BlockRoomResult BlockRoomResult;
	
	public boolean isSessionExpired() {
		
		if(this.getBlockRoomResult() != null 
				&& this.getBlockRoomResult().getResponseStatus() != null) {
			
			Integer responseStatus = this.getBlockRoomResult().getResponseStatus();
			if(responseStatus == 4) { return true; }
		}
		return false;
	}
	
}
