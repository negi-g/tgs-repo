package com.tgs.services.hms.datamodel.qtech;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelImages {
	
	private String ThumbnailUrl;
	private String BigUrl;

}