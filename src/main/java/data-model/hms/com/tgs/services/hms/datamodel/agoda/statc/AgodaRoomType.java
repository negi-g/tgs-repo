package com.tgs.services.hms.datamodel.agoda.statc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="roomtype")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaRoomType {
	
	@XmlElement
	private String hotel_room_type_id;
	
	@XmlElement
	private String standard_caption;
	
	@XmlElement
	private String hotel_room_type_picture;

}