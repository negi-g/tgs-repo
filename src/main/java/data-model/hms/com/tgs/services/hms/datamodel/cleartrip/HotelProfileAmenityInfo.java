package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelProfileAmenityInfo {

	private Integer id;
	private String amenityNameEn;
	private String amenityCategoryEn;
	private Integer amenityCategoryId;
}
