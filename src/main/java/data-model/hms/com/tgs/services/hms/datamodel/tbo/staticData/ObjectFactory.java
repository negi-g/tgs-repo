package com.tgs.services.hms.datamodel.tbo.staticData;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

    private final static QName _VendorMessagesVendorMessageSubSectionParagraphTextBr_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "br");
    private final static QName _VendorMessagesVendorMessageSubSectionParagraphTextP_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "p");
    private final static QName _VendorMessagesVendorMessageSubSectionParagraphTextPUl_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "ul");

    public ObjectFactory() {
    }

    public Address createAddress() {
        return new Address();
    }

    public ContactNumbers createContactNumbers() {
        return new ContactNumbers();
    }


    public Attributes createAttributes() {
        return new Attributes();
    }

    public HotelThemes createHotelThemes() {
        return new HotelThemes();
    }

    public VendorMessages createVendorMessages() {
        return new VendorMessages();
    }

    public SpecialTags createSpecialTags() {
        return new SpecialTags();
    }

    public Rooms createRooms() {
        return new Rooms();
    }

    public Rooms.Room createRoomsRoom() {
        return new Rooms.Room();
    }

    public Rooms.Room.RoomDescription createRoomsRoomRoomDescription() {
        return new Rooms.Room.RoomDescription();
    }

    public Rooms.Room.RoomDescription.SubSection createRoomsRoomRoomDescriptionSubSection() {
        return new Rooms.Room.RoomDescription.SubSection();
    }

    public Rooms.Room.RoomDescription.SubSection.Paragraph createRoomsRoomRoomDescriptionSubSectionParagraph() {
        return new Rooms.Room.RoomDescription.SubSection.Paragraph();
    }

    public Rooms.Room.RoomImages createRoomsRoomRoomImages() {
        return new Rooms.Room.RoomImages();
    }

    public Rooms.Room.BedTypes createRoomsRoomBedTypes() {
        return new Rooms.Room.BedTypes();
    }

    public Rooms.Room.RoomViews createRoomsRoomRoomViews() {
        return new Rooms.Room.RoomViews();
    }

    public Rooms.Room.Faciltities createRoomsRoomFaciltities() {
        return new Rooms.Room.Faciltities();
    }

    /**
     * Create an instance of {@link VendorMessages.VendorMessage }
     * 
     */
    public VendorMessages.VendorMessage createVendorMessagesVendorMessage() {
        return new VendorMessages.VendorMessage();
    }

    public VendorMessages.VendorMessage.SubSection createVendorMessagesVendorMessageSubSection() {
        return new VendorMessages.VendorMessage.SubSection();
    }

    public VendorMessages.VendorMessage.SubSection.Paragraph createVendorMessagesVendorMessageSubSectionParagraph() {
        return new VendorMessages.VendorMessage.SubSection.Paragraph();
    }

    public VendorMessages.VendorMessage.SubSection.Paragraph.Text createVendorMessagesVendorMessageSubSectionParagraphText() {
        return new VendorMessages.VendorMessage.SubSection.Paragraph.Text();
    }

    public VendorMessages.VendorMessage.SubSection.Paragraph.Text.P createVendorMessagesVendorMessageSubSectionParagraphTextP() {
        return new VendorMessages.VendorMessage.SubSection.Paragraph.Text.P();
    }

    public Policy createPolicy() {
        return new Policy();
    }

    public Address.CountryName createAddressCountryName() {
        return new Address.CountryName();
    }

    public Position createPosition() {
        return new Position();
    }

    public ContactNumbers.ContactNumber createContactNumbersContactNumber() {
        return new ContactNumbers.ContactNumber();
    }

    public Award createAward() {
        return new Award();
    }

    public Attributes.Attribute createAttributesAttribute() {
        return new Attributes.Attribute();
    }

    public HotelThemes.HotelTheme createHotelThemesHotelTheme() {
        return new HotelThemes.HotelTheme();
    }

    public SpecialTags.SpecialTag createSpecialTagsSpecialTag() {
        return new SpecialTags.SpecialTag();
    }

    public Rooms.Room.RoomDescription.SubSection.Paragraph.Text createRoomsRoomRoomDescriptionSubSectionParagraphText() {
        return new Rooms.Room.RoomDescription.SubSection.Paragraph.Text();
    }

    public Rooms.Room.RoomImages.RoomImage createRoomsRoomRoomImagesRoomImage() {
        return new Rooms.Room.RoomImages.RoomImage();
    }

    public Rooms.Room.BedTypes.BedType createRoomsRoomBedTypesBedType() {
        return new Rooms.Room.BedTypes.BedType();
    }

    public Rooms.Room.RoomViews.RoomView createRoomsRoomRoomViewsRoomView() {
        return new Rooms.Room.RoomViews.RoomView();
    }

    public Rooms.Room.Faciltities.RoomFacility createRoomsRoomFaciltitiesRoomFacility() {
        return new Rooms.Room.Faciltities.RoomFacility();
    }

    public VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.Ul createVendorMessagesVendorMessageSubSectionParagraphTextPUl() {
        return new VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.Ul();
    }

    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "br", scope = VendorMessages.VendorMessage.SubSection.Paragraph.Text.class)
    public JAXBElement<Object> createVendorMessagesVendorMessageSubSectionParagraphTextBr(Object value) {
        return new JAXBElement<Object>(_VendorMessagesVendorMessageSubSectionParagraphTextBr_QNAME, Object.class, VendorMessages.VendorMessage.SubSection.Paragraph.Text.class, value);
    }

    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "p", scope = VendorMessages.VendorMessage.SubSection.Paragraph.Text.class)
    public JAXBElement<VendorMessages.VendorMessage.SubSection.Paragraph.Text.P> createVendorMessagesVendorMessageSubSectionParagraphTextP(VendorMessages.VendorMessage.SubSection.Paragraph.Text.P value) {
        return new JAXBElement<VendorMessages.VendorMessage.SubSection.Paragraph.Text.P>(_VendorMessagesVendorMessageSubSectionParagraphTextP_QNAME, VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.class, VendorMessages.VendorMessage.SubSection.Paragraph.Text.class, value);
    }

    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "ul", scope = VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.class)
    public JAXBElement<VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.Ul> createVendorMessagesVendorMessageSubSectionParagraphTextPUl(VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.Ul value) {
        return new JAXBElement<VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.Ul>(_VendorMessagesVendorMessageSubSectionParagraphTextPUl_QNAME, VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.Ul.class, VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.class, value);
    }

    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "br", scope = VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.class)
    public JAXBElement<Object> createVendorMessagesVendorMessageSubSectionParagraphTextPBr(Object value) {
        return new JAXBElement<Object>(_VendorMessagesVendorMessageSubSectionParagraphTextBr_QNAME, Object.class, VendorMessages.VendorMessage.SubSection.Paragraph.Text.P.class, value);
    }

}
