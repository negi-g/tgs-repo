package com.tgs.services.hms.datamodel.hotelBeds;

import java.time.LocalDate;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Hotel {

    private Integer code;
	private LocalDate checkOut;
    private LocalDate checkIn;
    private String name;
    private String categoryCode;
    private String categoryName;
    private String destinationCode;
    private String destinationName;
    private Integer zoneCode;
    private String zoneName;
    private String latitude;
    private String longitude;
    private List<RoomResponse> rooms;
    private String minRate;
    private String maxRate;
    private String currency;
    private Upselling upselling;
}
