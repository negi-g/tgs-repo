package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BookingDetailRQ {
	public String BookingId;
	public String ReferenceNo;
	public String InternalReference;
}
