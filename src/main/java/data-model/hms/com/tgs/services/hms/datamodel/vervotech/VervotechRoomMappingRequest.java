package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class VervotechRoomMappingRequest extends VervotechBaseRequest {

	private List<RoomMappingRequest> roomMappingRequest;
	
}