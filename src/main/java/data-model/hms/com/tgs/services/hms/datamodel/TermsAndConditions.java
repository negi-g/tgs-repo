package com.tgs.services.hms.datamodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class TermsAndConditions {

	@SerializedName("sc")
	private List<TermsAndConditionsFormat> supplierConditions;
}
