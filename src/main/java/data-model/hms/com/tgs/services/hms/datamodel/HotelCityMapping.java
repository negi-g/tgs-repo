package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class HotelCityMapping {

	private String countryCode;
	private String countryId;
	private String cityName;
	private String countryName;
	private String supplierCityId;
	private String supplierCountryId;
	private String supplierStateId;
	private String supplierStateName;
}
