package com.tgs.services.hms.datamodel.tbo.mapping;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="Cities")
public class Cities {
	@XmlElement(name="City")
	private List<City> City;

}
