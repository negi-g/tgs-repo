package com.tgs.services.hms.datamodel.tbo.booking;

import com.tgs.services.hms.datamodel.tbo.search.Error;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class BookingDetailResult {	
	
	private boolean VoucherStatus;
	private Integer ResponseStatus;
	private Error Error;
	private String HotelBookingStatus;
	private Integer Status;

}
