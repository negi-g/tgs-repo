package com.tgs.services.hms.datamodel.fitruums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Destination")
public class Destination {

	@XmlElement(name = "destination_id", required = true)
	protected String destinationId;
	@XmlElement(name = "DestinationCode", required = true)
	protected String destinationCode;
	@XmlElement(name = "DestinationCode.2", required = true)
	protected String destinationCode2;
	@XmlElement(name = "DestinationCode.3", required = true)
	protected String destinationCode3;
	@XmlElement(name = "DestinationCode.4", required = true)
	protected String destinationCode4;
	@XmlElement(name = "DestinationName", required = true)
	protected String destinationName;
	@XmlElement(name = "CountryId", required = true)
	protected String countryId;
	@XmlElement(name = "CountryName", required = true)
	protected String countryName;
	@XmlElement(name = "CountryCode", required = true)
	protected String countryCode;
	@XmlElement(name = "TimeZone", required = true)
	protected String timeZone;

}
