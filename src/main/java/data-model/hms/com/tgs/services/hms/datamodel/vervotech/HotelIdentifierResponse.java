package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelIdentifierResponse {

	private Long Offset;
	private Long Limit;
	private Long Total;
	private List<Mapping> Mappings;
}