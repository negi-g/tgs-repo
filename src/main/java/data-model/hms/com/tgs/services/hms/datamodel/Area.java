package com.tgs.services.hms.datamodel;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class Area {

	@CustomSerializedName(key = FieldName.AREA_SQUARE_METERS)
	private String squareMeters;

	@CustomSerializedName(key = FieldName.AREA_SQUARE_FEET)
	private String squareFeet;

	@CustomSerializedName(key = FieldName.AREA_TEXT)
	private String text;
}
