package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Configuration {

	private String type;
	private String size;
	private Integer quantity;
}
