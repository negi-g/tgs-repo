
package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"precheckDetails"})
@XmlRootElement(name = "PrecheckRequest")
@Data
public class PrecheckRequest {

	@XmlElement(name = "PrecheckDetails", required = true)
	protected PrecheckDetailsRequest precheckDetails;
	@XmlAttribute(name = "siteid", required = true)
	protected String siteid;
	@XmlAttribute(name = "apikey", required = true)
	protected String apikey;
}
