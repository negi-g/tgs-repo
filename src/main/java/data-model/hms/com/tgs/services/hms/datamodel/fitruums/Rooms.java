package com.tgs.services.hms.datamodel.fitruums;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "room" })
public class Rooms {

	@XmlElement(required = true)
	protected List<Room> room;

	public List<Room> getRoom() {
		if (room == null) {
			room = new ArrayList<Room>();
		}
		return this.room;
	}

}
