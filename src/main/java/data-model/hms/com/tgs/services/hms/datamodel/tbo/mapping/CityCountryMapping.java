package com.tgs.services.hms.datamodel.tbo.mapping;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CityCountryMapping {

	private String tgsCityId;
	private String cityName;
	private String countryName;
	private String supplierCityId;
	private String supplierCountryId;
	private String supplierName;
	
}
