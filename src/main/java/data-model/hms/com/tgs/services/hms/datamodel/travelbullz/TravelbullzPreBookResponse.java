package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class TravelbullzPreBookResponse extends TravelbullzBaseResponse{
	public List<ErrorType> Error;
	public PreBookRS PreBookRS;
}
