
package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Data;

@Data
public class Price {

    private String CurrencyCode;
    private Integer RoomPrice;
    private Integer Tax;
    private Integer ExtraGuestCharge;
    private Integer ChildCharge;
    private Integer OtherCharges;
    private Integer Discount;
    private Integer PublishedPrice;
    private Integer PublishedPriceRoundedOff;
    private Integer OfferedPrice;
    private Integer OfferedPriceRoundedOff;
    private Integer AgentCommission;
    private Integer AgentMarkUp;
    private Double ServiceTax;
    private Integer TDS;

}
