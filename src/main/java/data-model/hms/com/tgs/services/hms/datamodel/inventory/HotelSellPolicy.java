package com.tgs.services.hms.datamodel.inventory;

import lombok.Getter;

@Getter
public enum HotelSellPolicy {

	NONE("N"), FREE_SELL("F"), STOP_SELL("S");

	public static HotelSellPolicy getEnumFromCode(String code) {
		return getSellPolicy(code);
	}

	public String getName() {
		return this.name();
	}

	private String code;

	private HotelSellPolicy(String code) {
		this.code = code;
	}

	public static HotelSellPolicy getSellPolicy(String code) {

		for (HotelSellPolicy mealBasisType : HotelSellPolicy.values()) {
			if (mealBasisType.getCode().equals(code))
				return mealBasisType;
		}
		return null;
	}
}
