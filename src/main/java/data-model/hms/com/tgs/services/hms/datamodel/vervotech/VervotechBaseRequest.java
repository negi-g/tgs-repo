package com.tgs.services.hms.datamodel.vervotech;

import java.util.HashMap;
import java.util.Map;
import com.tgs.services.base.helper.Exclude;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class VervotechBaseRequest {

	@Exclude
	private Map<String, String> headerParams;

	@Exclude
	private String suffixOfURL;

	public Map<String, String> getHeaderParams() {
		if (headerParams == null) {
			headerParams = new HashMap<>();
		}

		headerParams.put("Accept", "application/json");
		headerParams.put("Cache-Control", "no-cache");
		headerParams.put("Content-Type", "application/json");
		return headerParams;
	}
}