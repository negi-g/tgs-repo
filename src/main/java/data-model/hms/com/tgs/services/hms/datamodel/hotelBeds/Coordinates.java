package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Coordinates {

	private Double longitude;
	private Double latitude;
}
