package com.tgs.services.hms.datamodel.agoda.statc;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AgodaPolicyRemark {	
	
	private AgodaPolicy childPolicy;
	private String remark;
	
}