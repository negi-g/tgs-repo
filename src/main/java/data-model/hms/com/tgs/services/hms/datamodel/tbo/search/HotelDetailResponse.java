package com.tgs.services.hms.datamodel.tbo.search;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelDetailResponse {

	private HotelInfoResult HotelInfoResult;

	public boolean isSessionExpired() {
		
		if(this.getHotelInfoResult() != null && this.getHotelInfoResult().getResponseStatus() != null) {
			
			Integer responseStatus = this.getHotelInfoResult().getResponseStatus();
			if(responseStatus == 4) { return true; }
		}
		return false;
	}
	
}
