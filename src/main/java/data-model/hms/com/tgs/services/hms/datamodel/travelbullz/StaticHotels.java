package com.tgs.services.hms.datamodel.travelbullz;

import lombok.Data;

@Data
public class StaticHotels {
	private Integer HotelId;
	private String HotelName;
	private String Description;
	private String Latitude;
	private String Longitude;
	private String Address;
	private String Rating;
	private Integer CountryId;
	private String CountryName;
	private Integer CityId;
	private String CityName;
	private String HotelFrontImage;
	private String IsRecomondedHotel;
	private String IsActive;
	private String UpdatedDate;
	private String HotelContactNo;
	private String ReservationEmail;
	private String URL;
	
}
