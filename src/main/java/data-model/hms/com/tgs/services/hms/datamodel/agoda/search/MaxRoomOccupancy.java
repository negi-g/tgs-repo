package com.tgs.services.hms.datamodel.agoda.search;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="MaxRoomOccupancy")
@XmlAccessorType(XmlAccessType.FIELD)
public class MaxRoomOccupancy {
	
	@XmlAttribute
	private String normalbedding;
	
	@XmlAttribute
	private String extrabeds;
}
