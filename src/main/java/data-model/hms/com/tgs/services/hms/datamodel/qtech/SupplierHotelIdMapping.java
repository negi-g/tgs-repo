package com.tgs.services.hms.datamodel.qtech;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SupplierHotelIdMapping {

	private String id;

	@SerializedName("hid")
	private String hotelId;

	@SerializedName("shid")
	private String supplierHotelId;

	@SerializedName("sn")
	private String supplierName;

	@SerializedName("addinfo")
	private AdditionalHotelIdMappingInfo additionalInfo;
}
