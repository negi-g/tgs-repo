package com.tgs.services.hms.datamodel.agoda.statc;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Country_feed")
public class CountryFeed {

	@XmlElementWrapper(name = "countries")
	@XmlElement(name="country")
	private List<AgodaCountry> country;
	
}
