package com.tgs.services.hms.datamodel;

import java.util.List;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class RoomAdditionalInfo {

	@CustomSerializedName(key = FieldName.ROOM_ID)
	private String roomId;

	@CustomSerializedName(key = FieldName.MAX_GUEST_ALLOWED)
	private Integer maxGuestAllowed;

	@CustomSerializedName(key = FieldName.MAX_ADULT_ALLOWED)
	private Integer maxAdultAllowed;

	@CustomSerializedName(key = FieldName.MAX_CHILDREN_ALLOWED)
	private Integer maxChildrenAllowed;

	@CustomSerializedName(key = FieldName.VIEWS)
	@ToString.Exclude
	private List<String> views;

	@CustomSerializedName(key = FieldName.BEDS)
	@ToString.Exclude
	private List<Bed> beds;

	@CustomSerializedName(key = FieldName.AREA)
	@ToString.Exclude
	private Area area;
}
