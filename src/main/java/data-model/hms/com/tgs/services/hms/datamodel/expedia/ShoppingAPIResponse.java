package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ShoppingAPIResponse {

	private String property_id;
	private List<Room> rooms;
	private Integer score;
	private String type;
	private String message;
	private List<Field> fields;
}