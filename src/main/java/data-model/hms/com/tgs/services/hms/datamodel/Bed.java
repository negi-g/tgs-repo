package com.tgs.services.hms.datamodel;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class Bed {

	@CustomSerializedName(key = FieldName.BED_TYPE)
	private String type;

	@CustomSerializedName(key = FieldName.BED_DESCRIPTION)
	private String description;

	@CustomSerializedName(key = FieldName.BED_SIZE)
	private String size;

	@CustomSerializedName(key = FieldName.BED_COUNT)
	private Integer count;
}
