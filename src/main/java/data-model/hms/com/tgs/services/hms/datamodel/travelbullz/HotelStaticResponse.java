package com.tgs.services.hms.datamodel.travelbullz;

import java.util.List;
import lombok.Data;

@Data
public class HotelStaticResponse extends TravelbullzBaseResponse{
	private List<StaticHotels> HotelList;

}
