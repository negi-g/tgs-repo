package com.tgs.services.hms.datamodel.inventory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.HotelId;
import com.tgs.services.base.helper.RoomCategoryId;
import com.tgs.services.base.helper.RoomTypeId;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class HotelRatePlan implements Validatable {

	@SearchPredicate(type = PredicateType.IN, filterName = "ids")
	private Long id;

	private String name;

	@SerializedName("rcid")
	@RoomCategoryId
	@SearchPredicate(type = PredicateType.IN, filterName = "roomCategoryIds")
	private String roomCategoryId;

	private String roomCategoryName;

	@SerializedName("mbid")
	@SearchPredicate(type = PredicateType.IN, filterName = "mealBasisIds")
	private String mealBasisId;

	@SerializedName("mbn")
	private String mealBasisName;

	@UserId
	@SerializedName("sid")
	@SearchPredicate(type = PredicateType.IN, filterName = "supplierIds")
	private String supplierId;

	@SerializedName("shid")
	@SearchPredicate(type = PredicateType.IN, filterName = "supplierHotelIds")
	@HotelId
	private String supplierHotelId;

	@SerializedName("rtid")
	@RoomTypeId
	@SearchPredicate(type = PredicateType.IN, filterName = "roomTypeIds")
	private String roomTypeId;

	@SerializedName("rtn")
	private HotelRoomTypeInfo roomTypeInfo;

	@SerializedName("sp")
	private HotelSellPolicy sellPolicy;

	@SerializedName("rpi")
	private HotelRatePlanInfo ratePlanInfo;

	@SerializedName("iai")
	private HotelInventoryAllocationInfo inventoryAllocationInfo;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.EQUAL)
	private LocalDate validOn;

	private Boolean enabled;

	@Exclude
	@Builder.Default
	@SearchPredicate(type = PredicateType.EQUAL)
	private Boolean isDeleted = Boolean.FALSE;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		if (ObjectUtils.isEmpty(getEnabled())) {
			errors.put("enabled", SystemError.EMPTY_ENABLED_FLAG.getErrorDetail());
		}
		if (StringUtils.isBlank(getRoomCategoryId())) {
			errors.put("roomCategoryId", SystemError.INVALID_ROOM_CATEGORY_ID.getErrorDetail());
		}
		if (StringUtils.isBlank(getMealBasisId())) {
			errors.put("mealBasisId", SystemError.INVALID_MEAL_BASIS_ID.getErrorDetail());
		}
		if (StringUtils.isBlank(getSupplierId())) {
			errors.put("supplierId", SystemError.INVALID_SUPPLIERID.getErrorDetail());
		}
		if (StringUtils.isBlank(getSupplierHotelId())) {
			errors.put("supplierHotelId", SystemError.INVALID_SUPPLIER_HOTEL_ID.getErrorDetail());
		}
		if (StringUtils.isEmpty(getRoomTypeId())) {
			errors.put("roomTypeId", SystemError.INVALID_ROOM_TYPE_ID.getErrorDetail());
		}
		if (ObjectUtils.isEmpty(getValidOn()) || getValidOn().isBefore(LocalDate.now())) {
			errors.put("validOn", SystemError.INVALID_VALID_ON.getErrorDetail());
		}

		HotelRatePlanInfo ratePlanInfo = getRatePlanInfo();
		if (!ObjectUtils.isEmpty(ratePlanInfo)) {
			if (ObjectUtils.isEmpty(ratePlanInfo.getBaseFare()) || ratePlanInfo.getBaseFare() < 0) {
				errors.put("ratePlanInfo.baseFare", SystemError.INVALID_AMOUNT.getErrorDetail());
			}
		} else {
			errors.put("ratePlanInfo", SystemError.INVALID_RATE_PLAN_INFO.getErrorDetail());
		}

		HotelInventoryAllocationInfo allocationInfo = getInventoryAllocationInfo();
		if (!ObjectUtils.isEmpty(allocationInfo)) {
			if (!ObjectUtils.isEmpty(allocationInfo.getTotalInventory()) && allocationInfo.getTotalInventory() < 0) {
				errors.put("inventoryAllocationInfo.totalInventory",
						SystemError.INVALID_HOTEL_TOTAL_INVENTORY.getErrorDetail());
			}
			if (!ObjectUtils.isEmpty(allocationInfo.getSoldInventory())) {
				if (!ObjectUtils.isEmpty(allocationInfo.getTotalInventory())) {
					if (allocationInfo.getSoldInventory() < 0
							|| allocationInfo.getTotalInventory() < allocationInfo.getSoldInventory())
						errors.put("inventoryAllocationInfo.soldInventory",
								SystemError.INVALID_HOTEL_SOLD_INVENTORY.getErrorDetail());
				} else {
					errors.put("inventoryAllocationInfo.totalInventory",
							SystemError.INVALID_HOTEL_TOTAL_INVENTORY.getErrorDetail());
				}
			}
		}
		return errors;
	}

	public void cleanData() {

		HotelRatePlanInfo ratePlanInfo = null;
		if (Objects.nonNull(ratePlanInfo = getRatePlanInfo())) {
			ratePlanInfo.cleanData();
		}
	}
}
