package com.tgs.services.hms.datamodel.vervotech;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Mapping {

	private Long UnicaId;
	private List<Hotel> Hotels;
}
