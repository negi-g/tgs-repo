package com.tgs.services.hms.analytics;

import com.tgs.services.base.MethodExecutionInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class HotelMethodExecutionInfo extends MethodExecutionInfo {

	private String supplierName;
	private Integer hotelCount;
	private Integer optionCount;
	private Integer roomNight;
}
