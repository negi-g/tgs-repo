package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Builder;
import lombok.Data;
import java.util.List;

@Builder
@Data
public class HotelBedsStaticDataRequest {

	private String fields;
	private String language;
	private Integer from;
	private Integer to;
	private String useSecondaryLanguage;
	private List<String> codes;
	private String countryCodes;
}
