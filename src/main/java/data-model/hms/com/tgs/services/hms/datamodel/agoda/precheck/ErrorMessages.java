package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"errorMessage"})
@Data
public class ErrorMessages {
	@XmlElement(name = "ErrorMessage", required = true)
	protected List<ErrorMessage> errorMessage;
}
