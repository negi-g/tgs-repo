package com.tgs.services.ps.datamodel.configurator;

import com.tgs.services.base.ruleengine.IPolicyOutput;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BooleanOutput implements IPolicyOutput {

	Boolean output;
}
