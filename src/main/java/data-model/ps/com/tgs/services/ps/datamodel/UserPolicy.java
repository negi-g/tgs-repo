package com.tgs.services.ps.datamodel;

import java.time.LocalDateTime;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserPolicy extends DataModel {

	private Long id;

	@UserId
	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	private String userId;

	private UserPolicyInfo policyInfo;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	private LocalDateTime createdOn;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	private LocalDateTime processedOn;

}
