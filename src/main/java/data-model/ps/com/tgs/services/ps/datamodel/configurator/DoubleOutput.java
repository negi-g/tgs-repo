package com.tgs.services.ps.datamodel.configurator;

import com.tgs.services.base.ruleengine.IPolicyOutput;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class DoubleOutput implements IPolicyOutput {

	BigDecimal output;

}
