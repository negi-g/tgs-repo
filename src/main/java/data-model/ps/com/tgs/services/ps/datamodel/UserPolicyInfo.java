package com.tgs.services.ps.datamodel;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserPolicyInfo {

	@SerializedName("gid")
	private List<String> groupIds;

	@SerializedName("ps")
	private List<Policy> policies;
}
