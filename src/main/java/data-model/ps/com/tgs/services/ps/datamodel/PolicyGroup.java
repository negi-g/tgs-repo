package com.tgs.services.ps.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PolicyGroup extends DataModel {
	
	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private Long id;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	private boolean enabled;

	@ForbidInAPIRequest
	@SearchPredicate(type = PredicateType.EQUAL)
	private boolean isDeleted;

	@SearchPredicate(type = PredicateType.LIKE)
	private String name;

	@SearchPredicate(type = PredicateType.LIKE)
	private String description;

	private List<Policy> policies;
}
