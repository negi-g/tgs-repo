package com.tgs.services.base.gson;

import java.util.List;

import com.tgs.services.base.helper.AlwaysInclude;
import org.apache.commons.collections.CollectionUtils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.CustomSerializedName;

public class FieldExclusionStrategy implements ExclusionStrategy {
	List<String> fieldInclusions;

	List<String> fieldExclusions;

	public FieldExclusionStrategy(List<String> fieldInclusions, List<String> fieldExclusions) {
		super();
		this.fieldInclusions = fieldInclusions;
		this.fieldExclusions = fieldExclusions;
	}

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		
		CustomSerializedName customSerializedName = null;
		SerializedName serializedName = null;
		String fieldName = null;

		if (f.getAnnotation(AlwaysInclude.class) != null) return false;

		if((customSerializedName = f.getAnnotation(CustomSerializedName.class)) != null) {
			fieldName = customSerializedName.key().getName();
		}else if((serializedName = f.getAnnotation(SerializedName.class)) != null) {
			fieldName = serializedName.value();
		}else {
			fieldName  = f.getName();
		}
		

		// more priority to fieldExclusions
		if (CollectionUtils.isNotEmpty(fieldExclusions)) {
			return fieldExclusions.contains(fieldName);
		} else {
			return !fieldInclusions.contains(fieldName);
		}
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}

}
