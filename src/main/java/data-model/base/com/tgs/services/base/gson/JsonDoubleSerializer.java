package com.tgs.services.base.gson;

import java.lang.reflect.Type;
import java.math.BigDecimal;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class JsonDoubleSerializer implements JsonSerializer<Double> {
	@Override
	public JsonElement serialize(final Double doubleValue, final Type typeOfSrc, final JsonSerializationContext context) {
		BigDecimal value = BigDecimal.valueOf(doubleValue);
		value = value.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		return new JsonPrimitive(value);
	}
}
