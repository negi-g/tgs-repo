package com.tgs.services.base.datamodel;

import com.tgs.services.base.BaseAnalyticsQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class SignInAttemptQuery extends BaseAnalyticsQuery {

	private boolean limitcrossed;
	private boolean wrongpassword;
	private Integer attempts;

}
