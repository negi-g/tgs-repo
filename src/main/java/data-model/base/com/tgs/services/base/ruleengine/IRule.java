package com.tgs.services.base.ruleengine;

public interface IRule {

	public IRuleCriteria getInclusionCriteria();

	public IRuleCriteria getExclusionCriteria();

	public IRuleOutPut getOutput();

	public default String getName() {
		return null;
	}

	public default double getPriority() {
		return 0;
	}

	public default boolean exitOnMatch() {
		return false;
	}

	public default boolean getEnabled() {
		return true;
	}

	public default boolean canBeConsideredExclusively() {
		return true;
	}

}
