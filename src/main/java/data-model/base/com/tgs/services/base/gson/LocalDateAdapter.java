package com.tgs.services.base.gson;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LocalDateAdapter implements JsonSerializer<LocalDate>, JsonDeserializer<LocalDate> {
	DateTimeFormatter formatter = null;

	public LocalDateAdapter(DateTimeFormatter formatter) {
		if (formatter != null) {
			this.formatter = formatter;
		}
	}

	public LocalDateAdapter() {

	}

	@Override
	public JsonElement serialize(LocalDate localDate, Type type, JsonSerializationContext context) {
		if (formatter != null) {
			return new JsonPrimitive(localDate.format(formatter));
		}
		return new JsonPrimitive(localDate.toString());
	}

	@Override
	public LocalDate deserialize(JsonElement dateString, Type type, JsonDeserializationContext context)
			throws JsonParseException {
		if(StringUtils.isBlank(dateString.getAsString())) {
			log.error("Unable to parse empty string as date");
			throw new CustomGeneralException(SystemError.BAD_REQUEST);
		}
		LocalDate localDate = null;
		try {
			localDate = LocalDate.parse(dateString.getAsString());
		} catch (Exception e) {
			try {
				localDate = LocalDate.parse(dateString.getAsString(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			} catch (Exception ex) {
				localDate = LocalDate.parse(dateString.getAsString(), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
			}
		}
		return localDate;
	}

}
