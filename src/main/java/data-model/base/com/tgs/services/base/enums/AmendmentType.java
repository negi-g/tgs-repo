package com.tgs.services.base.enums;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.Getter;

@Getter
public enum AmendmentType {

	SSR("SSR") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_ABORT_ANCILLARY_EMAIL,
					EmailTemplateKey.AMENDMENT_ABORT_EMAIL);
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_PROCESSED_ANCILLARY_EMAIL,
					EmailTemplateKey.AMENDMENT_MERGE_EMAIL);
		}

		@Override
		public String prefix() {
			return "11";
		}

		@Override
		public boolean isVirtualPaymentAllowed() {
			return true;
		}

	},

	CANCELLATION("CAN") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_ABORT_CANCELLATION_EMAIL,
					EmailTemplateKey.AMENDMENT_ABORT_EMAIL);
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_PROCESSED_CANCELLATION_EMAIL,
					EmailTemplateKey.AMENDMENT_MERGE_EMAIL);
		}

		@Override
		public String prefix() {
			return "12";
		}

		@Override
		public boolean isVirtualPaymentAllowed() {
			return true;
		}
	},

	NO_SHOW("NS") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_ABORT_NOSHOW_EMAIL, EmailTemplateKey.AMENDMENT_ABORT_EMAIL);
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_PROCESSED_NOSHOW_EMAIL,
					EmailTemplateKey.AMENDMENT_MERGE_EMAIL);
		}

		@Override
		public String prefix() {
			return "13";
		}

		@Override
		public boolean isVirtualPaymentAllowed() {
			return true;
		}
	},

	VOIDED("VD") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_ABORT_VOID_EMAIL, EmailTemplateKey.AMENDMENT_ABORT_EMAIL);
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_PROCESSED_VOID_EMAIL,
					EmailTemplateKey.AMENDMENT_MERGE_EMAIL);
		}

		@Override
		public String prefix() {
			return "14";
		}

		@Override
		public boolean isVirtualPaymentAllowed() {
			return true;
		}
	},

	REISSUE("RE") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_ABORT_REISSUE_EMAIL,
					EmailTemplateKey.AMENDMENT_ABORT_EMAIL);
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_PROCESSED_REISSUE_EMAIL,
					EmailTemplateKey.AMENDMENT_MERGE_EMAIL);
		}

		@Override
		public String prefix() {
			return "15";
		}

		@Override
		public boolean isVirtualPaymentAllowed() {
			return true;
		}
	},

	FARE_CHANGE("FC") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return null;
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return null;
		}

		@Override
		public String prefix() {
			return "16";
		}

		@Override
		public boolean isVirtualPaymentAllowed() {
			return true;
		}
	},

	CANCELLATION_QUOTATION("CQ") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_CANCELLATION_QUOTATION_EMAIL,
					EmailTemplateKey.AMENDMENT_ABORT_EMAIL);
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_CANCELLATION_QUOTATION_EMAIL,
					EmailTemplateKey.AMENDMENT_MERGE_EMAIL);
		}

		@Override
		public String prefix() {
			return "17";
		}
	},

	REISSUE_QUOTATION("RQ") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_REISSUE_QUOTATION_EMAIL,
					EmailTemplateKey.AMENDMENT_ABORT_EMAIL);
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_REISSUE_QUOTATION_EMAIL,
					EmailTemplateKey.AMENDMENT_MERGE_EMAIL);
		}

		@Override
		public String prefix() {
			return "18";
		}
	},

	MISCELLANEOUS("MISC") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_ABORT_MISCELLANEOUS_EMAIL,
					EmailTemplateKey.AMENDMENT_ABORT_EMAIL);
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_MERGE_EMAIL);
		}

		@Override
		public String prefix() {
			return "19";
		}
	},
	CORRECTION("CORR") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_ABORT_MISCELLANEOUS_EMAIL,
					EmailTemplateKey.AMENDMENT_ABORT_EMAIL);
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_MERGE_EMAIL);
		}

		@Override
		public String prefix() {
			return "10";
		}
	},
	FULL_REFUND("FR") {
		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_ABORT_CANCELLATION_EMAIL,
					EmailTemplateKey.AMENDMENT_ABORT_EMAIL);
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_PROCESSED_CANCELLATION_EMAIL,
					EmailTemplateKey.AMENDMENT_MERGE_EMAIL);
		}

		@Override
		public String prefix() {
			return "20";
		}

		@Override
		public boolean isVirtualPaymentAllowed() {
			return true;
		}
	},
	HOTEL_CORRECTION("H_CORR") {

		@Override
		public List<EmailTemplateKey> abortMailKey() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String prefix() {
			return "31";
		}
	},
	SUPPLIER_CHANGE("SUP_CHAN") {

		@Override
		public List<EmailTemplateKey> abortMailKey() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String prefix() {
			return "32";
		}

		@Override
		public boolean sendRaiseMail() {
			return false;
		}

	},
	TDR("TDR") {

		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return null;
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return Arrays.asList(EmailTemplateKey.AMENDMENT_PROCESSED_TDR_EMAIL);
		}

		@Override
		public String prefix() {
			return "41";
		}

		@Override
		public boolean sendRaiseMail() {
			return false;
		}

	},
	OFFLINE_CANCEL("OFFCAN") {

		@Override
		public List<EmailTemplateKey> abortMailKey() {
			return null;
		}

		@Override
		public List<EmailTemplateKey> mergeMailKey() {
			return null;
		}

		@Override
		public String prefix() {
			return "42";
		}

		@Override
		public boolean sendRaiseMail() {
			return false;
		}

	};

	private String code;

	AmendmentType(String code) {
		this.code = code;
	}

	public static AmendmentType getEnumFromCode(String code) {
		return getAmendmentType(code);
	}

	public static AmendmentType getAmendmentType(String code) {
		for (AmendmentType type : AmendmentType.values()) {
			if (type.getCode().equals(code)) {
				return type;
			}
		}
		return null;
	}

	public abstract List<EmailTemplateKey> abortMailKey();

	public abstract List<EmailTemplateKey> mergeMailKey();

	public abstract String prefix();

	public boolean isVirtualPaymentAllowed() {
		return false;
	};

	public boolean sendRaiseMail() {
		return true;
	};

	public static AmendmentType getAmendmentTypeFromId(String amendmentId) {
		if (amendmentId == null)
			return null;
		String prefix = amendmentId.substring(3, 5);
		for (AmendmentType type : AmendmentType.values()) {
			if (type.prefix().equals(prefix)) {
				return type;
			}
		}
		return null;
	}

	public static Set<AmendmentType> airAmdApplBeforeDepartDate() {
		Set<AmendmentType> amendments = new HashSet<>();
		/*
		 * We have commented all the amendmentTypes because according to Tripjack team it's allowed to raise amendment
		 * after departure date as well : Ashu
		 * 
		 */
		// amendments.add(CANCELLATION_QUOTATION);
		// amendments.add(REISSUE_QUOTATION);
		return amendments;
	}
}
