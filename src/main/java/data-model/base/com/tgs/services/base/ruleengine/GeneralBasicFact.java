package com.tgs.services.base.ruleengine;

import java.time.LocalDateTime;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;


@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class GeneralBasicFact implements IFact {


	private UserRole role;
	protected String userId;
	private String partnerId;
	private ChannelType channelType;
	private List<String> groupIds;

	@SerializedName("at")
	private LocalDateTime applicableTime;

	private List<PaymentMedium> paymentMediums;

	private String bankName;

	// number of pax
	private Integer paxCount;

	private Boolean isPackage;

	public GeneralBasicFact generateFact(UserRole userRole) {
		setRole(userRole);
		return this;
	}


}

