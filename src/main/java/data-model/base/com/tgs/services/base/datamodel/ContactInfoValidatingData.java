package com.tgs.services.base.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class ContactInfoValidatingData implements ValidatingData {

	private boolean isEmergencyContactNameReq;

	private boolean isContactsReq;

	private boolean isEmailsReq;

}
