package com.tgs.services.base.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.datamodel.VoidChecked;
import com.tgs.services.base.datamodel.VoidClean;

public class TgsCollectionUtils {

	/**
	 * Creates a list having non-null elements of the given list.
	 * 
	 * @param list
	 * @return list containing only non-null elements or {@code null} if there's no non-null element
	 * 
	 * @see TgsCollectionUtils#getCleanList(List, UnaryOperator, Predicate)
	 * @see TgsCollectionUtils#isEmpty(Collection, Predicate)
	 */
	public static <E> List<E> getNonNullElements(List<E> list) {
		return getCleanNonNullElements(list, null);
	}

	/**
	 * Creates a list having non-null elements of the given list, cleaned using {@code elementCleaner}.
	 * 
	 * @param list
	 * @param elementCleaner {@code UnaryOperator} that performs cleaning operation
	 * @return clean list or {@code null} if there's no non-null element left after cleaning
	 * 
	 * @see TgsCollectionUtils#getCleanList(List, UnaryOperator, Predicate)
	 * @see TgsCollectionUtils#isEmpty(Collection, Predicate)
	 */
	public static <E> List<E> getCleanNonNullElements(List<E> list, UnaryOperator<E> elementCleaner) {
		return getCleanList(list, elementCleaner, Objects::isNull);
	}

	/**
	 * Creates a list having non-null {@code Cleanable} elements, and remove null and void elements
	 * ({@code VoidChecked}) from list.
	 * 
	 * @param list list to be cleaned
	 * @return clean list or {@code null} if "empty"
	 * 
	 * @see TgsCollectionUtils#getCleanList(List, UnaryOperator, Predicate)
	 * @see TgsCollectionUtils#isEmpty(Collection, Predicate)
	 * @link Cleanable
	 * @link VoidChecked
	 */
	public static <E extends VoidClean> List<E> getCleanList(List<E> list) {
		return getCleanList(list, Cleanable.Cleaner::clean, VoidChecked::voidCheck);
	}

	/**
	 * Creates an {@code ArrayList} having elements of the given list cleaned using {@code elementCleaner} and removed
	 * if found void (checked using {@code voidCheck}) thereafter.
	 * 
	 * @param list
	 * @param elementCleaner {@code UnaryOperator} that performs cleaning operation. Default is identity
	 *        {@code UnaryOperator#identity()}}.
	 * @param voidCheck {@code Predicate} that tests whether an element (including {@code null}) is void. Default is
	 *        {@code t -> false}.
	 * @return clean list or {@code null} if there's no non-void element left after cleaning
	 * 
	 *         Note: Care be taken while handling {@code null} in elementCleaner and voidCheck
	 * 
	 * @see TgsCollectionUtils#isEmpty(Collection, Predicate)
	 */
	public static <E> List<E> getCleanList(List<E> list, UnaryOperator<E> elementCleaner, Predicate<E> voidCheck) {
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}

		if (elementCleaner == null) {
			elementCleaner = UnaryOperator.identity();
		}

		if (voidCheck == null) {
			voidCheck = t -> false;
		}

		List<E> newList = new ArrayList<>();
		for (E element : list) {
			element = elementCleaner.apply(element);
			if (!voidCheck.test(element)) {
				newList.add(element);
			}
		}

		if (isEmpty(newList, voidCheck)) {
			return null;
		}

		return newList;
	}

	/**
	 * Creates new list by trimming non-blank {@code String} elements and removing blank elements in the given list.
	 * 
	 * @param strList
	 * @return clean list or {@code null} if no non-blank element left after trimming
	 * 
	 * @see TgsCollectionUtils#getCleanList(List, UnaryOperator, Predicate)
	 * @see TgsCollectionUtils#isEmpty(Collection, Predicate)
	 */
	public static List<String> getCleanStringList(List<String> strList) {
		return getCleanList(strList, StringUtils::trim, StringUtils::isBlank);
	}

	/**
	 * Behaves like {@link CollectionUtils#isEmpty(Collection)} if {@code voidCheck} is {@code null}.
	 * 
	 * @param coll
	 * @param voidCheck {@code Predicate} that tests whether an element (including {@code null}) is void
	 * @return {@code true} if {@code voidCheck} returns {@code false} for any of the element present in collection
	 *         {@code coll} (in other words, if there is at least one non-void element in the collection). Otherwise
	 *         {@code false}
	 */
	public static <E> boolean isEmpty(Collection<E> coll, Predicate<E> voidCheck) {
		if (coll == null || coll.size() == 0)
			return true;

		if (voidCheck == null) {
			voidCheck = Objects::isNull;
		}

		for (E element : coll) {
			if (!voidCheck.test(element)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * 
	 * @param coll
	 * @return {@code true} if there is at least one non-null element in the collection. Otherwise {@code false}
	 * 
	 * @see TgsCollectionUtils#isEmpty(Collection, Predicate)
	 */
	public static <E> boolean isEmpty(Collection<E> coll) {
		return isEmpty(coll, Objects::isNull);
	}

	/**
	 * 
	 * @param strColl
	 * @return {@code true} if there is at least one non-blank element in the collection. Otherwise {@code false}
	 * 
	 * @see TgsCollectionUtils#isEmpty(Collection, Predicate)
	 */
	public static boolean isEmptyStringCollection(Collection<String> strColl) {
		return isEmpty(strColl, StringUtils::isBlank);
	}

	public static <E> boolean haveNonNullIntersection(Collection<E> coll1, Collection<E> coll2) {
		if (CollectionUtils.isEmpty(coll1) || CollectionUtils.isEmpty(coll2))
			return false;

		for (E e1 : coll1) {
			for (E e2 : coll2) {
				if (Objects.equals(e1, e2)) {
					return true;
				}
			}
		}
		return false;
	}

	public static <T> List<List<T>> chunkList(List<T> list, int chunkSize) {
		if (chunkSize <= 0) {
			throw new IllegalArgumentException("Invalid chunk size: " + chunkSize);
		}
		List<List<T>> chunkList = new ArrayList<>(list.size() / chunkSize);
		for (int i = 0; i < list.size(); i += chunkSize) {
			List<T> innerList = new ArrayList<>();
			for (int j = 0; j < chunkSize; j++) {
				if (j + i < list.size())
					innerList.add(list.get(j + i));
			}
			chunkList.add(innerList);
		}
		return chunkList;
	}

	/**
	 * 
	 * Iterate list backwards, add keys (generated using {@code keyGenerator}) for each element to a set and remove
	 * element if
	 * <li>it is null, or
	 * <li>key generated for it already exists in the set.
	 * 
	 * @param list mutable list in which duplicate elements (and nulls) are to be removed
	 * @param keyGenerator {@code Function} used to generate {@code String} keys
	 */
	public static <E> void removeDuplicates(List<E> list, Function<E, String> keyGenerator) {
		if (keyGenerator != null && CollectionUtils.isNotEmpty(list)) {
			Set<String> keys = new HashSet<>();
			for (int i = list.size() - 1; i >= 0; i--) {
				E element;
				if ((element = list.get(i)) == null || !keys.add(keyGenerator.apply(element))) {
					list.remove(i);
				}
			}
		}
	}

	public static <T> int size(Collection<T> coll) {
		if (coll == null)
			return 0;
		return coll.size();
	}
}
