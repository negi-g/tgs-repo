package com.tgs.services.base.datamodel;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang.StringUtils;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString()
@Getter
@Setter
@Builder
public class AddressInfo {

	@NotNull
	@ApiModelProperty(required = true)
	private String address;

	@NotNull
	@ApiModelProperty(required = true)
	private String pincode;

	@Valid
	@NotNull
	@ApiModelProperty(required = true)
	private CityInfo cityInfo;

	public void cleanData() {
		if (StringUtils.isBlank(address))
			address = null;
		if (StringUtils.isBlank(pincode))
			pincode = null;
		if (cityInfo != null) {
			if (StringUtils.isBlank(cityInfo.getState()))
				cityInfo.setState(null);
			if (StringUtils.isBlank(cityInfo.getName()))
				cityInfo.setName(null);
			if (StringUtils.isBlank(cityInfo.getCountry()))
				cityInfo.setCountry(null);
		}
	}
}
