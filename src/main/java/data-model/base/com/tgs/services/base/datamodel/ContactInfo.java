package com.tgs.services.base.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsCollectionUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ContactInfo implements Validatable<ContactInfoValidatingData>, VoidClean {

	@SerializedName("ecn")
	private String emergencyContactName;

	private List<String> emails;

	private List<String> contacts;

	@Override
	public FieldErrorMap validate(ContactInfoValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (validatingData.isEmergencyContactNameReq() && StringUtils.isBlank(emergencyContactName)) {
			errors.put("emergencyContactName", SystemError.EMPTY_EMERGENCY_CONTACT_NAME.getErrorDetail());
		}

		int i = 0;

		if (validatingData.isEmailsReq() && CollectionUtils.isEmpty(getEmails())) {
			errors.put("emails", SystemError.INVALID_EMAIL.getErrorDetail());
		} else {
			for (String email : getEmails()) {
				if (!EmailValidator.getInstance().isValid(email)) {
					errors.put("emails[" + i++ + "]", SystemError.INVALID_EMAIL.getErrorDetail());
				}
			}
		}

		if (validatingData.isContactsReq() && CollectionUtils.isEmpty(getContacts())) {
			errors.put("contacts", SystemError.INVALID_MOBILE.getErrorDetail());
		} else {
			i = 0;

			for (String contact : getContacts()) {
				if (StringUtils.isEmpty(contact)) {
					errors.put("contacts[" + i++ + "]", SystemError.INVALID_MOBILE.getErrorDetail());
				}
			}
		}
		return errors;
	}
	
	public List<String> getEmails() {
		if(this.emails == null) {
			return this.emails;
		}
		List<String> emailIds = new ArrayList<>();
		for(String email: this.emails) {
			emailIds.add(getValidEmail(email));
		}
		return emailIds;
	}

	private String getValidEmail(String email) {
		if (email != null && email.contains("_")) {
			int count = email.indexOf("@");
			int underscoreLastCount = email.lastIndexOf("_");
			if (count < underscoreLastCount) {
				return email.substring(0, underscoreLastCount);
			}
		}
		return email;
	}
	
	@Override
	public void cleanData() {
		setEmails(TgsCollectionUtils.getCleanStringList(emails));
		setContacts(TgsCollectionUtils.getCleanStringList(contacts));
	}


	@Override
	public boolean isVoid() {
		return emails == null && contacts == null;
	}
}
