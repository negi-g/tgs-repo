package com.tgs.services.base.helper;

import lombok.Getter;

@Getter
public enum FieldName {


	/*
	 * Serialized Names For HotelInfo Datamodel
	 */
	HOTEL_MISC_INFO("mi"),
	CREATED_ON("co"),
	PROCESSED_ON("po"),
	GIATA_ID("gid"),
	UNICA_ID("uid"),
	OPTIONS("ops"),
	ROOM_MISC_INFO("rmi"),
	PROCESSED_OPTIONS("pops"),
	HOTEL_PROMOTION_INFO("pr"),
	TRIP_ADVISOR_ID("taid"),
	SUPPLIER_SET("ss"),
	SUPPLIER_NAME("sn"),
	IS_PREFERRED_HOTEL("iph"),
	IS_CROSS_SELL_HOTEL("icsh"),
	HOTEL_TERMS_AND_CONDITIONS("tac"),
	HOTEL_NAME("hn"),
	HOTEL_IMAGES("img"),
	HOTEL_INSTRUCTIONS("inst"),
	HOTEL_DESCRIPTION("des"),
	HOTEL_LONG_DESCRIPTION("ldes"),
	HOTEL_RATING("rt"),
	HOTEL_GEOLOCATION("gl"),
	HOTEL_ADDRESS("ad"),
	HOTEL_FACILITIES("fl"),
	HOTEL_ID("id"),
	HOTEL_PROPERTY_TYPE("pt"),
	HOTEL_CONTACT("cnt"),
	HOTEL_WEBSITE("wb"),
	IS_PACKAGE_RATE("ispr"),
	HOTEL_ADDITIONAL_INFO("hai"),
	HOTEL_OPINIONATED_CONTENT("hoc"),
	LOCAL_HOTEL_CODE("lhc"),
	HOTEL_POINT_OF_INTEREST("poi"),

	/*
	 * Serialized Names For Images Datamodel
	 */
	HOTEL_THUMBNAILS("tns"),
	HOTEL_IMAGE_URL("url"),
	HOTEL_IMAGE_SIZE("sz"),


	/*
	 * Serialized Names For Hotel Address Datamodel
	 */
	HOTEL_ADDRESS_LINE1("adr"),
	HOTEL_ADDRESS_LINE2("adr2"),
	CITY_NAME("ctn"),
	STATE_NAME("sn"),
	COUNTRY_NAME("cn"),

	/*
	 * Geolocation
	 */
	HOTEL_LONGITUDE("ln"),
	HOTEL_LATITUDE("lt"),

	/*
	 * Contact Info
	 */
	HOTEL_PHONE("ph"),
	HOTEL_EMAIL("em"),


	/*
	 * Serialized Names For HotelPromotion Datamodel
	 */
	IS_LOWEST_PRICE_ALLOWED("ilpa"),
	IS_FLEXIBLE_TIMING_ALLOWED("ifta"),
	// IS_PREFERRED_HOTEL("ph"),

	/*
	 * Serialized Names For Hotel MiscInfo
	 */
	SUPPLIER_STATIC_HOTEL_ID("sshid"),
	SEARCH_KEY_EXPIRY_TIME("set"),
	SUPPLIER_BOOKING_REFERENCE("sbid"),
	SUPPLIER_BOOKING_ID("sbr"),
	SUPPLIER_BOOKING_CONFIRMATION("sbc"),
	HOTEL_BOOKING_REFERENCE("hbr"),
	HOTEL_BOOKING_CANCELLATION_REFERENCE("bcr"),
	SUPPLIER_BOOKING_URL("sbu"),
	CREDIT_CARD_ID("cci"),
	IS_OPINIONATED_CONTENT_REQUIRED("iocr"),
	OPTION_MAPPING_ID("omid"),
	SUPPLIER_BNPL_STATUS("sbnpls"),
	IS_BNPL_BOOKING("ibnplb"),
	IS_SUPPLIER_MAPPED("ism"),
	IS_MEAL_ALREADY_MAPPED("imam"),
	SEARCH_ID("sid"),

	/*
	 * Serialized Names For Processed Options
	 */

	OPTION_FACILITIES("fc"),
	PROCESSED_OPTION_TOTAL_PRICE("tpc"),
	PROCESSED_OPTION_DISCOUNTED_PRICE("dpc"),
	OPTION_PRICE("tp"),

	/*
	 * Serialized Names For Room
	 */
	ROOM_ID("rid"),
	SUPPLIER_ROOM_ID("srid"),
	ROOM_TYPE_NAME("rtn"),
	MAX_GUEST_ALLOWED("mga"), 
	MAX_ADULT_ALLOWED("maa"),
	MAX_CHILDREN_ALLOWED("mca"),
	VIEWS("vi"), 
	BEDS("bds"),
	BED_TYPE("bt"),
	BED_DESCRIPTION("bd"),
	BED_SIZE("bs"),
	BED_COUNT("bc"),
	AREA("ar"),
	AREA_SQUARE_METERS("asm"),
	AREA_SQUARE_FEET("asf"),
	AREA_TEXT("at"),

	/*
	 * Serialized Names For Option
	 */

	ROOMINFO_LIST("ris"),
	ROOMID_LIST("rids"),
	OPTION_CANCELLATION_POLICY("cnp"),
	ROOM_CANCELLATION_POLICY_LIST("rcnp"),
	OPTION_DEADLINEDATETIME("ddt"),
	OPTION_MISC_INFO("omi"),
	IS_OPTION_ON_REQUEST("iopr"),
	ROOM_SSR_LIST("ssr"),

	/*
	 * Option Misc Info
	 */

	SUPPLIER_SEARCH_ID("ssid"),
	SECONDARY_SUPPLIER("ss"),
	SUPPLIER_HOTEL_ID("shid"),
	SUPPLIER_ID("sid"),
	SOURCE_ID("soid"),
	IS_MARKUP_UPDATED("imu"),
	OPTION_BOOKING_PRICE("obp"),
	IS_COMMISSION_APPLIED("ica"),
	IS_NOT_REQUIRED_ON_DETAIL("inrod"),
	CATEGORY_ID("catid"),

	/*
	 * Cancellation Policy
	 */
	IS_NO_REFUND_ALLOWED("inra"),
	IS_FULL_REFUND_ALLOWED("ifra"),
	PENALTY_DETAILS("pd"),
	CANCELLATION_POLICY_STRING("scnp"),
	CANCELLATION_POLICY_MISC_INFO("mi"),

	/*
	 * Cancellation Policy MiscInfo
	 */
	IS_BOOKING_ALLOWED("iba"),
	IS_SOLD_OUT("iso"),

	/*
	 * Penalty Details
	 */

	FROM_DATE("fdt"),
	TO_DATE("tdt"),
	PENALTY_AMOUNT("am"),
	PENALTY_PERCENT("pp"),
	PENALTY_ROOM_NIGHTS("rn"),
	CANCELLATION_TERMS_AND_CONDITIONS("tnc"),
	IS_CANCELLATION_RESTRICED("icr"),

	/*
	 * SSR
	 */
	REQUEST_MESSAGE("rm"),
	QUITE_ROOM_REQUIRED("qrr"),
	ARRIVAL_TIME("at"),


	/*
	 * Fetch Static Data
	 */
	HOTEL_CITY_INFO_LIST("cil"),
	HOTEL_INFO_LIST("hil"),

	IS_DISABLED("id"),
	IS_PAN_REQUIRED("ipr"),
	IS_PASS_MANDATORY("ipm"),
	CANCELLATION_POLICY_BUFFER("cpb"),
	LAST_MODIFIED_DATE_TIME("lmdt"),

	POLYGON_INFO("pi"),
	HOTEL_REGION_ANCESTOR_INFO("hrai");

	private String name;

	private FieldName(String name) {
		this.name = name;
	}


}
