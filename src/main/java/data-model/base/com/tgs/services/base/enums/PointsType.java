package com.tgs.services.base.enums;

import lombok.Getter;

@Getter
public enum PointsType {

	COIN("C");

	private String code;

	private PointsType(String code) {
		this.code = code;
	}

	public static PointsType getEnumFromCode(String code) {
		for (PointsType type : PointsType.values()) {
			if (type.getCode().equals(code)) {
				return type;
			}
		}
		return null;
	}

}
