package com.tgs.services.base.datamodel;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TimePeriod {
	private LocalDateTime startTime;
	private LocalDateTime endTime;

	@ApiModelProperty(hidden = true)
	public boolean isValid() {
		return endTime != null && startTime != null && !startTime.isAfter(endTime);
	}
}
