package com.tgs.services.base.gson;

import com.google.gson.*;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;

import java.lang.reflect.Type;
import java.math.BigDecimal;

public class JsonBigDecimalAdapter implements JsonSerializer<BigDecimal>, JsonDeserializer<BigDecimal> {

    @Override
    public BigDecimal deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            return BigDecimal.valueOf(Double.valueOf(json.getAsString())).setScale(2, BigDecimal.ROUND_HALF_UP);
        } catch (NumberFormatException e) {
            throw new CustomGeneralException(SystemError.INVALID_DATA_FORMAT,
                    "Excepting double number but found " + json.getAsString());
        }
    }

    @Override
    public JsonElement serialize(final BigDecimal value, final Type typeOfSrc, final JsonSerializationContext context) {
        return new JsonPrimitive(value.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    }
}
