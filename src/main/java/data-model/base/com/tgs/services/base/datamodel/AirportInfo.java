package com.tgs.services.base.datamodel;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.helper.RestExclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
@Accessors(chain = true)
public class AirportInfo {

	@RestExclude
	private Long id;

	// @Pattern(regexp = "^[a-zA-Z]{3}", flags = Pattern.Flag.CASE_INSENSITIVE,
	// message = "'${validatedValue}' Must be valid IATA code")
	@ToString.Include
	@NonNull
	@SearchPredicate(type = PredicateType.EQUAL)
	private String code;


	// @Pattern(regexp = "^[a-zA-Z0-9-()\\s]{1,255}", flags = Pattern.Flag.CASE_INSENSITIVE,
	// message = "Airport name doesn't allow any special characters")
	@SearchPredicate(type = PredicateType.LIKE)
	private String name;

	// @Pattern(regexp = "^[a-zA-Z0-9]{1,255}", flags = Pattern.Flag.CASE_INSENSITIVE,
	// message = "City code doesn't allow any special characters")
	private String cityCode;

	// @Pattern(regexp = "^[a-zA-Z0-9]{1,255}", flags = Pattern.Flag.CASE_INSENSITIVE,
	// message = "City doesn't allow any special characters")
	@SearchPredicate(type = PredicateType.EQUAL)
	private String city;

	// @Pattern(regexp = "^[a-zA-Z0-9]{1,255}", flags = Pattern.Flag.CASE_INSENSITIVE,
	// message = "Country name doesn't allow any special characters")
	@SearchPredicate(type = PredicateType.EQUAL)
	private String country;

	// @Pattern(regexp = "^[a-zA-Z]{2}", flags = Pattern.Flag.CASE_INSENSITIVE,
	// message = "'${validatedValue}' Must be valid Country code")
	private String countryCode;

	private String terminal;

	@RestExclude
	private Boolean enabled;
	@RestExclude
	private Double priority;

	/**
	 * <code>AirportInfo</code> instance must have at least a non-null code.<br>
	 * <u>Note:</u> Applicable only for domestic trips.
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof AirportInfo))
			return false;

		return code.equals(((AirportInfo) obj).code);
	}
}
