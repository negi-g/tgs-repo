package com.tgs.services.base.ruleengine;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.HotelSearchType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserHotelFeeRuleCriteria extends UserFeeRuleCriteria {

	private Boolean isDomestic;

	@SerializedName("hst")
	private HotelSearchType searchType;

}
