package com.tgs.services.base.datamodel;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VerifyMappingRequest {
	private List<VerifyHotelMappingQuery> verifyMappinglist;
}
