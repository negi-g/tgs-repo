package com.tgs.services.base.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class FareChangeAlert implements Alert {
	private Double oldFare;
	private Double newFare;
	private Double discount;
	private String type;

	@Override
	public String getType() {
		return type;
	}
}
