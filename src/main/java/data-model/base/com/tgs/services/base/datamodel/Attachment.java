package com.tgs.services.base.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Attachment {
	private String name;
	private String link;
}
