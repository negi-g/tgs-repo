package com.tgs.services.base.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class EncryptionData {
	
	private String aesKey;
	
}
