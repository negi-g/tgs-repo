package com.tgs.services.base.datamodel;

import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class ExternalPaymentNotificationConfiguration {

	@SerializedName("dbscs")
	private List<DBSConfiguration> dbsConfigurations;

	@SerializedName("rpcs")
	private List<RazorpayConfiguration> razorpayConfigurations;


	@SerializedName("ccavenconf")
	private Map<String, String> ccAvenueConf;

}
