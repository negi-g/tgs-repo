package com.tgs.services.base.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.tgs.services.base.datamodel.CurrencyConverter;

import java.io.IOException;
import java.math.BigDecimal;

public class BigDecimalUnitToCentAdaptorFactory implements TypeAdapterFactory {

    @SuppressWarnings("unchecked")
    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        return new TypeAdapter<T>() {
            @Override
            public void write(JsonWriter out, T value) throws IOException {
                if (value != null) {
                    BigDecimal decimal = CurrencyConverter.toUnit((Long)value);
                    out.value(decimal);
                } else {
                    out.nullValue();
                }
            }

            @Override
            public T read(JsonReader reader) throws IOException {
                if (reader.peek() == JsonToken.NULL) {
                    reader.nextNull();
                    return null;
                } else {
                    BigDecimal value = BigDecimal.valueOf(reader.nextDouble());
                    return (T) CurrencyConverter.toSubUnit(value);
                }
            }
        };
    }
}
