package com.tgs.services.base.datamodel;

public class BaseHotelConstants {

	public static final String ROOM_ONLY = "ROOM ONLY";
	public static final String DEFAULT_CURRENCY = "INR";

	public static final String SEARCH = "Search";
	public static final String DETAILSEARCH = "DetailSearch";
	public static final String PRICE_CHECK = "PriceCheck";
	public static final String BOOKING = "Booking";
	public static final String RETRIEVE_BOOKING = "RetrieveBooking";
	public static final String BOOKING_CANCELLATION = "BookingCancellation";
	public static final String CANCELLATIONPOLICY = "CancellationPolicy";
	/*
	 * Vervotech specific
	 */
	public static final String ROOM_MAPPING = "RoomMapping";
	public static final String STATIC_DATA = "StaticData";

	public static final String METHOD_EXECUTION_TYPE = "METHOD_EXECUTION_TYPE";
	public static final String METHOD_SUB_EXECUTION_TYPE = "METHOD_SUB_EXECUTION_TYPE";
	public static final String SINGLE_SUPPLIER = "SINGLE_SUPPLIER";
	public static final String MULTIPLE_SUPPLIER = "MULTIPLE_SUPPLIER";

	public static final String PRICE_VALIDATION = "PRICE_VALIDATION";
	public static final String NO_PRICE_VALIDATION = "NO_PRICE_VALIDATION";

	public static final String NO_SUPPLIER_HIT = "NO_SUPPLIER_HIT";
	public static final String SUPPLIER_HIT = "SUPPLIER_HIT";

	public static final String BEFORE_SEARCH = "BEFORE_SEARCH";
	public static final String AFTER_SEARCH = "AFTER_SEARCH";

	public static final String RULE_OUTPUT = "RULE_OUTPUT";
}
