package com.tgs.services.base.datamodel;

import com.tgs.services.base.helper.MaskedField;

public class Mask {

	// non-negative
	final private int unmaskedBeg, unmaskedEnd, maxMaskLength;
	final private char maskingCharacter;
	final private boolean maskSpecialCharacter;

	public Mask(int unmaskedBeg, int unmaskedEnd, int maxMaskLength, char maskingCharacter,
			boolean maskSpecialCharacter) {
		this.unmaskedBeg = unmaskedBeg < 0 ? 0 : unmaskedBeg;
		this.unmaskedEnd = unmaskedEnd < 0 ? 0 : unmaskedEnd;
		this.maxMaskLength = maxMaskLength;
		this.maskingCharacter = maskingCharacter;
		this.maskSpecialCharacter = maskSpecialCharacter;
	}

	public String mask(String val) {
		if (val == null) {
			return null;
		}
		final int beg = unmaskedBeg, last = val.length() - unmaskedEnd;

		// no masking
		if (last - beg <= 0) {
			return val;
		}

		String regex = maskSpecialCharacter ? "." : "\\w",
				maskedVal = val.substring(beg, last).replaceAll(regex, String.valueOf(maskingCharacter));
		if (maxMaskLength > 0) {
			maskedVal = maskedVal.substring(0, Math.min(maxMaskLength, maskedVal.length()));
		}
		return val.substring(0, beg) // unmaskedBeg number of unmasked characters at the beginning
				+ maskedVal // Math.min(maxMaskLength, val.length() - unmaskedEnd - unmaskedBeg)
							// number of MASKED characters
				+ val.substring(last); // unmaskedEnd number of unmasked characters at the end
	}

	/**
	 * Returns {@code Mask} created using values obtained from {@code maskedField}
	 * or default values, if null.
	 * 
	 * @param maskedField
	 * @return
	 */
	public static Mask get(MaskedField maskedField) {
		if (maskedField == null) {
			return new Mask(0, 0, 0, '*', true);
		}

		return new Mask(maskedField.unmaskedBeg(), maskedField.unmaskedEnd(), maskedField.maxMaskLength(),
				maskedField.maskingChar(), maskedField.maskSpecialCharacer());
	}
}
