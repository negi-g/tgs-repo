package com.tgs.services.base.helper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ReportParam {

	int beginIndex();

	int length();

	boolean isFiller() default false;

	boolean isNumeric() default false;

}
