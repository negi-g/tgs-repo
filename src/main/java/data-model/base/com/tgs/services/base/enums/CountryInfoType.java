package com.tgs.services.base.enums;

import java.util.List;
import java.util.Map;
import com.amazonaws.util.CollectionUtils;
import lombok.Getter;

@Getter
public enum CountryInfoType {
	COUNTRYNAME("countryname") {
		@Override
		protected String getValueFromCountryId(String countryid, List<Map<String, String>> doc) {
			return getFieldValue(this.getValue(), countryid, doc);
		}
	},
	NAME("name") {
		@Override
		protected String getValueFromCountryId(String countryid, List<Map<String, String>> doc) {
			return getFieldValue(this.getValue(), countryid, doc);
		}
	},
	DIAL_CODE("dial_code") {
		@Override
		protected String getValueFromCountryId(String countryid, List<Map<String, String>> doc) {
			return getFieldValue(this.getValue(), countryid, doc);
		}
	},
	COUNTRYID("countryid") {
		@Override
		protected String getValueFromCountryId(String countryid, List<Map<String, String>> doc) {
			return getFieldValue(this.getValue(), countryid, doc);
		}
	},
	CODE("code") {
		@Override
		protected String getValueFromCountryId(String countryid, List<Map<String, String>> doc) {
			return getFieldValue(this.getValue(), countryid, doc);
		}
	},
	ISOCODE("isocode") {
		@Override
		protected String getValueFromCountryId(String countryid, List<Map<String, String>> doc) {
			return getFieldValue(this.getValue(), countryid, doc);
		}
	};

	private String value;

	private CountryInfoType(String val) {
		this.value = val;
	}

	public static CountryInfoType getCountryInfoType(String val) {
		for (CountryInfoType infoType : CountryInfoType.values()) {
			if (infoType.getValue().equalsIgnoreCase(val)) {
				return infoType;
			}
		}
		return null;
	}

	public String getFieldValue(String field, String countryid, List<Map<String, String>> doc) {
		if (!CollectionUtils.isNullOrEmpty(doc)) {
			return doc.stream().filter(row -> row.get("countryid").equals(countryid)).map(row -> row.get(field))
					.findFirst().orElse("");
		}
		return null;
	}

	public static String getSpecificFieldFromCountryId(String countryid, String fieldName,
			List<Map<String, String>> doc) {
		CountryInfoType countryInfoType = CountryInfoType.getCountryInfoType(fieldName);
		return countryInfoType.getValueFromCountryId(countryid, doc);
	}

	protected abstract String getValueFromCountryId(String countryid, List<Map<String, String>> doc);
}
