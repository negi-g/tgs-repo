package com.tgs.services.base.helper;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.lang.annotation.Annotation;


@Builder
@Getter
@Setter
public class RequestContextHolder {

    private Annotation[] definedAnnotations;

    /**
     * This is similar to requestURI but in case of get Request it will remove all
     * dynamic parameters. For example if request URL is
     * http://localhost://fms/air/delete/1234 , this will return fms/air/delete
     */
    private String baseURL;
    private String endPoint;
    private String requestURI;
    private String httpRequestMethod;

    public <T extends Annotation> Annotation getAnnotation(Class<T> type) {
        for (Annotation ann : getDefinedAnnotations())
            if (ann.annotationType().equals(type)) {
                return ann;
            }
        return null;
    }
}
