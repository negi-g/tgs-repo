package com.tgs.services.base.gson;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class CalendarAdapter implements JsonSerializer<Calendar>, JsonDeserializer<Calendar> {
	// DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
	DateTimeFormatter formatter = null;

	public CalendarAdapter(DateTimeFormatter formatter) {
		if (formatter != null) {
			this.formatter = formatter;
		}
	}

	public CalendarAdapter() {

	}

	@Override
	public JsonElement serialize(Calendar calendar, Type type, JsonSerializationContext context) {
		ZonedDateTime tempDate = ZonedDateTime.ofInstant(calendar.toInstant(), calendar.getTimeZone().toZoneId());
		return new JsonPrimitive(tempDate.format(formatter));
	}

	@Override
	public Calendar deserialize(JsonElement calendarString, Type type, JsonDeserializationContext context)
			throws JsonParseException {
		LocalDateTime dateTime;
		if (!formatter.toString().contains("MinuteOfHour")) {
			dateTime = LocalDateTime.from(LocalDate.parse(calendarString.getAsString(), formatter).atStartOfDay());
		} else {
			dateTime = LocalDateTime.parse(calendarString.getAsString(), formatter);
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant()));
		return calendar;
	}

}
