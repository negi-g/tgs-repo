package com.tgs.services.base.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class HotelBnplMailData {
	
	private String bookingId;
	private String bnplStatus;
}
