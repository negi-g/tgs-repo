package com.tgs.services.base.ruleengine;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.datamodel.VoidClean;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.helper.Airline;
import com.tgs.services.base.utils.TgsCollectionUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Getter
@Setter
public class UserAirFeeRuleCriteria extends UserFeeRuleCriteria implements Validatable, VoidClean {

	@SerializedName("ril")
	private List<RouteInfo> routeInfoList;

	@SerializedName("al")
	@Airline
	private Set<String> airLineList;

	@Deprecated
	private Boolean isDomestic;

	@SerializedName("fl")
	private Set<String> fareTypeList;

	@SerializedName("at")
	private AirType airType;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (!CollectionUtils.isEmpty(routeInfoList)) {
			int i = 0;
			for (RouteInfo routeInfo : routeInfoList) {
				if (routeInfo != null) {
					errors.putAll(routeInfo.validate(null).withPrefixToFieldNames("routeInfoList[" + i++ + "]."));
				}
			}
		}

		return errors;
	}

	public AirType getAirType() {
		if (airType == null) {
			airType = AirType.ALL;
		}
		return airType;
	}

	@Override
	public boolean isVoid() {
		return routeInfoList == null && airLineList == null && fareTypeList == null;
	}

	@Override
	public void cleanData() {
		//setFareTypeList(new HashSet<>(TgsCollectionUtils.getCleanStringList(new ArrayList<>(getFareTypeList()))));
	}


}
