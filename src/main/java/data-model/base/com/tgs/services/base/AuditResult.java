package com.tgs.services.base;

import java.time.LocalDateTime;
import java.util.List;

import com.tgs.services.base.helper.UserId;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuditResult {

	private Object actualValue;

	private List<AuditChange> difference;

	@UserId(userRelation = true)
	private String userId;

	private String userIp;

	private LocalDateTime modifiedTime;
}
