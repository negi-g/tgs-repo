package com.tgs.services.base.datamodel;

import com.tgs.services.base.ErrorDetail;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * @author Abhineet Kumar, Technogram Solutions
 */
public class FieldErrorMap extends HashMap<String, ErrorDetail> {

    /**
     * Adds prefix to field-names (keys).
     *
     * @param prefix
     * @return {@code FieldErrorMap} with prefix-added-field-names
     */
    public FieldErrorMap withPrefixToFieldNames(String prefix) {
        if (StringUtils.isEmpty(prefix)) {
            return this;
        }

        // to avoid concurrent modification issues
        List<String> fieldNames = new ArrayList<>();
        for (Entry<String, ErrorDetail> entry : entrySet()) {
            fieldNames.add(entry.getKey());
        }

        for (String fieldName : fieldNames) {
            ErrorDetail errorDetail = get(fieldName);
            remove(fieldName);
            put(prefix + fieldName, errorDetail);
        }

        return this;
    }
}
