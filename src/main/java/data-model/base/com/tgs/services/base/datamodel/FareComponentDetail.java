package com.tgs.services.base.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FareComponentDetail {

    private String Code;

    private String name;

    private boolean airlineComponent;

    public boolean amenable;
}
