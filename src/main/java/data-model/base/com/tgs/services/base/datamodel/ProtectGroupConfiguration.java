package com.tgs.services.base.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ProtectGroupConfiguration {

	@SerializedName("url")
	private String url;

	@SerializedName("vi")
	private String vendorId;

	@SerializedName("ak")
	private String apiKey;

	@SerializedName("pc")
	private String productCode;

	@SerializedName("cc")
	private String currencyCode;

	@SerializedName("pr")
	private String premiumRate;

	@SerializedName("mra")
	private Double maxRedeemableAmount = 514332.50;
}
