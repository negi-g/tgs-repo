package com.tgs.services.base.helper;

public @interface NotNull {

    Class<?>[] groups() default {};

    boolean forUpdate() default false;

    boolean forCreate() default false;

}
