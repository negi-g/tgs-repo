package com.tgs.services.base.gson;

import java.io.IOException;
import java.text.DecimalFormat;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/**
 * 
 * This is mainly used to do conversion from amount to cents(paisa). Don't
 * change anything in this file otherwise this can lead to serious problem in
 * payment system
 * 
 * @author ashugupta
 *
 */
public class DoubleToLongAdapterFactory implements TypeAdapterFactory {

	@SuppressWarnings("unchecked")
	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		return new TypeAdapter<T>() {
			@Override
			public void write(JsonWriter out, T value) throws IOException {
				if (value != null) {
					Double doubleValue = ((Long) value).doubleValue() / 100;
					DecimalFormat df = new DecimalFormat("#.##");
					out.value(df.format(doubleValue));
				} else {
					out.nullValue();
				}
			}

			@Override
			public T read(JsonReader reader) throws IOException {
				if (reader.peek() == JsonToken.NULL) {
					reader.nextNull();
					return null;
				} else {
					Double value = reader.nextDouble();
					Long longValue = (long) (value * 100);
					return (T) longValue;
				}
			}
		};
	}
}
