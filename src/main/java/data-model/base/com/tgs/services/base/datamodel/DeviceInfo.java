package com.tgs.services.base.datamodel;

import com.tgs.services.base.enums.ChannelType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DeviceInfo {

	private String deviceId;
	private String userId;
	private String browser;
	private String browserVersion;
	private String os;
	private ChannelType channelType;

}
