package com.tgs.services.base.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.commons.collections.MapUtils;
import org.reflections.Reflections;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TgsObjectUtils {

	public static List<String> getNotNullFields(Object obj) {
		return getNotNullFields(obj, true,true);
	}

	public static List<String> getNotNullFields(Object obj, boolean requireEmptyCollection, boolean isRecursive) {
		List<String> notNullFields = new ArrayList<>();
		if (obj != null) {
			try {
				final List<Field> declaredFields = new ArrayList<>();
				Class<?> clazz = obj.getClass();
				while (clazz.getName().contains(ApplicationConstant.ADAPTABLE_PACKAGE)) {
					declaredFields.addAll(Lists.newArrayList(clazz.getDeclaredFields()));
					clazz = clazz.getSuperclass();
				}
				for (Field field : declaredFields) {
					field.setAccessible(true);
					if (field.get(obj) != null) {
						if (isRecursive && field.getType().getName().contains(ApplicationConstant.ADAPTABLE_PACKAGE)
								&& !(field.get(obj) instanceof Enum)) {
							List<String> fields = getNotNullFields(field.get(obj), requireEmptyCollection, isRecursive);
							fields.forEach(f -> {
								notNullFields.add(field.getName().concat(".").concat(f));
							});
						}
						/**
						 * This is to handle empty String collections, for example airlines= [], we
						 * don't need empty collection in not null fields
						 */
						else if (requireEmptyCollection || (!Collection.class.isAssignableFrom(field.getType())
								|| ((Collection.class.cast(field.get(obj)).size() > 1) || (Collection.class
										.cast(field.get(obj)).size() == 1
										&& (!(Collection.class.cast(field.get(obj)).iterator().next() instanceof String)
												|| !TgsCollectionUtils
												.isEmpty(Collection.class.cast(field.get(obj)))))))) {
							notNullFields.add(field.getName());
						}
					}
				}
			} catch (Exception e) {
				log.error("Error in converting Object to List", e);
			}
		}
		return notNullFields;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map<String, Object> getNotNullFieldValueMap(Object obj, boolean isRecursive,
			boolean accessSuperClassFields) {
		Map<String, Object> notNullFieldMap = new HashMap<>();
		if (obj != null) {
			try {
				if(Map.class.isAssignableFrom(obj.getClass())) {
					Map<String, Object> map = getNonNullFieldsOfMap(obj, isRecursive,accessSuperClassFields);
					map.forEach((key,val) -> notNullFieldMap.put(key, val) );
				}else {
					Field[] declaredFields = obj.getClass().getDeclaredFields();
					List<Field> declaredFieldsList = new ArrayList(Arrays.asList(declaredFields));
					if (accessSuperClassFields) {
						Class superClass = obj.getClass().getSuperclass();
						do {
							declaredFieldsList.addAll(Arrays.asList(superClass.getDeclaredFields()));
							superClass = superClass.getSuperclass();
						}while (superClass!=null);
					}
					for (Field field : declaredFieldsList) {
						field.setAccessible(true);
						Object value = field.get(obj);
						if (value != null) {
							if (isRecursive && field.getType().getName().contains(ApplicationConstant.ADAPTABLE_PACKAGE)
									&& !(value instanceof Enum)) {
								Map<String, Object> nestedFields = getNotNullFieldValueMap(value, isRecursive,
										accessSuperClassFields);
								nestedFields.forEach((k, v) -> notNullFieldMap.put(field.getName().concat(".").concat(k),
										nestedFields.get(k)));							
							} else if (Collection.class.isAssignableFrom(field.getType())) {
								int index = 0;
								for (Object val : (Collection) value) {
									if (!(val instanceof Enum) && val.getClass().getCanonicalName().contains(ApplicationConstant.ADAPTABLE_PACKAGE)) {
										Map<String, Object> nestedFields = getNotNullFieldValueMap(val, isRecursive,
												accessSuperClassFields);
										for (Map.Entry<String, Object> entry : nestedFields.entrySet()) {
											notNullFieldMap.put(entry.getKey().concat("["+String.valueOf(index)+"]") 
													, entry.getValue());
										}
										index++;
									}else {
										notNullFieldMap.put(field.getName().concat("["+String.valueOf(index++)+"]") 
												,val);
									}
								}

							} else if (Map.class.isAssignableFrom(field.getType())) {
								Map<String, Object> map = getNonNullFieldsOfMap(value, isRecursive,accessSuperClassFields);
								map.forEach((key,val) -> notNullFieldMap.put(field.getName().concat(".").concat(key), val) );
							}
							else
								notNullFieldMap.put(field.getName(), value);
						}
					}
				}
				
			} catch (Exception e) {
				log.error("Error in converting Object to Map", e);
			}
			
		}
		return notNullFieldMap;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Map<String, Object> getNonNullFieldsOfMap(Object obj, boolean isRecursive, boolean accessSuperClassFields) {
		Map<String, Object> notNullFieldMap = new HashMap<>();
		Map map = (Map) obj;
		map.forEach((k,v) -> {
			if(!(v instanceof Enum ) && v.getClass().getCanonicalName().contains(ApplicationConstant.ADAPTABLE_PACKAGE)
					|| Map.class.isAssignableFrom(v.getClass())) {
				Map<String, Object> nestedFields = getNotNullFieldValueMap(v, isRecursive,
						accessSuperClassFields);
				nestedFields.forEach((key,val) -> {
					notNullFieldMap.put(k.toString().concat(".").concat(key),val);
				});
			}else {
				notNullFieldMap.put(k.toString(), v);
			}	
		});
		return notNullFieldMap;
	}

	public static <T extends Annotation, V> Set<V> findAnnotationValues(Object obj, Class<T> anno) {
		Map<T,Set<V>> map = findAnnotationWithValues(obj, anno);
		return map.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
	}
	
	public static <T extends Annotation, V> Map<T,Set<V>> findAnnotationWithValues(Object obj, Class<T> anno) {
		Map<T,Set<V>> map = new HashMap<>();
		if (obj != null) {
			try {
				if (obj.getClass().isEnum()) {
					return map;
				}
				Field[] declaredFields = obj.getClass().getDeclaredFields();
				for (Field field : declaredFields) {
					field.setAccessible(true);
					Object value = field.get(obj);
					if (value != null) {
						T annoValue = field.getAnnotation(anno);

						if (Collection.class.isAssignableFrom(field.getType()) && field.getDeclaringClass().getName()
								.contains(ApplicationConstant.ADAPTABLE_PACKAGE)) {
							for (Iterator<Object> iter = Collection.class.cast(field.get(obj)).iterator(); iter
									.hasNext();) {
								Map<T,Set<V>> values = findAnnotationWithValues(iter.next(), anno);
								if(MapUtils.isNotEmpty(values)) {
									if(MapUtils.isNotEmpty(map)) {
										List<T> keys = new ArrayList<T>(map.keySet());
										Set<V> valuesSet = values.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
										map.get(keys.get(0)).addAll(valuesSet);
									}
									else
										map.putAll(values);
								}
							}
						}

						if (field.getType().getName().contains(ApplicationConstant.ADAPTABLE_PACKAGE)) {
							Map<T,Set<V>> values = findAnnotationWithValues(value, anno);
							if(MapUtils.isNotEmpty(values)) {
								if(MapUtils.isNotEmpty(map)) {
									List<T> keys = new ArrayList<T>(map.keySet());
									Set<V> valuesSet = values.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
									map.get(keys.get(0)).addAll(valuesSet);
								}
								else
									map.putAll(values);
							}

						} else if (annoValue != null) {
							Set<V> values = MapUtils.isNotEmpty(map) ? map.values().stream().flatMap(Collection::stream).collect(Collectors.toSet()) :new HashSet<>();
							values.add((V)value);
							map.put(annoValue,values);
						}
					}
				}
			} catch (Exception e) {
				log.error("Error in converting Object to Map", e);
			}
		}
		return map;
	}

	public static <T extends Annotation, V> Set<V> findMethodAnnotationValues(Object obj, Class<T> anno) {
		Set<V> values = new HashSet<>();
		if (obj != null) {
			try {
				if (obj.getClass().isEnum()) {
					return values;
				}
				Method[] declaredMethods = obj.getClass().getDeclaredMethods();
				Field[] declaredFields = obj.getClass().getDeclaredFields();
				for (Method method : declaredMethods) {
					method.setAccessible(true);
					
					Object annoValue = method.getAnnotation(anno);
					if(annoValue!=null) {
						Object value = method.invoke(obj);
						if(value instanceof Collection)
							values.addAll((Collection)value);
						else
							values.add((V) value);
					}
				}

				for (Field field : declaredFields) {
					field.setAccessible(true);
					Object value = field.get(obj);
					if (value != null) {
						if (Collection.class.isAssignableFrom(field.getType()) && field.getDeclaringClass().getName()
								.contains(ApplicationConstant.ADAPTABLE_PACKAGE)) {
							for (Iterator<Object> iter = Collection.class.cast(field.get(obj)).iterator(); iter
									.hasNext();) {
								values.addAll(findMethodAnnotationValues(iter.next(), anno));
							}
						}
						if (field.getType().getName().contains(ApplicationConstant.ADAPTABLE_PACKAGE)) {
							values.addAll(findMethodAnnotationValues(value, anno));
						}
					}
				}
			} catch (Exception e) {
				log.error("Error in fetching method annotation values", e);
			}
		}
		return values;
	}
	
	public static Map<Class<?>, Object> getNotNullClassValueMap(Object obj) {
		Map<Class<?>, Object> notNullFieldMap = new HashMap<>();
		if (obj != null) {
			try {
				Class<?> clazz = obj.getClass();
				if (clazz.isEnum()) {
					return notNullFieldMap;
				}
				Field[] declaredFields = obj.getClass().getDeclaredFields();
				for (Field field : declaredFields) {
					if (field.getType().getName().contains(ApplicationConstant.ADAPTABLE_PACKAGE)) {
						field.setAccessible(true);
						Object value = field.get(obj);
						if (value != null) {
							notNullFieldMap.putAll(getNotNullClassValueMap(value));
							notNullFieldMap.put(field.getType(), value);
						}
					}
				}
			} catch (Exception e) {
				log.error("Error in converting Object to Map", e);
			}
		}
		return notNullFieldMap;
	}

	@SuppressWarnings("unchecked")
	public static Set<String> getFieldsWithDifferentValues(Map currFields, Map prevFields){
		Set<String> fields = new HashSet<>();
		currFields.forEach((key,value)->{
			if(prevFields==null) {
				fields.add(key.toString());
			}	
			else if(value!=null && value.getClass().isArray()) {
				Map currVal = new HashMap<>();
				currVal.put(key, Arrays.asList(value));
				Map oldVal = new HashMap<>();
				oldVal.put(key,Arrays.asList(prevFields.remove(key)));
				fields.addAll(getFieldsWithDifferentValues(currVal,oldVal));
			}
			else if(value!=null && Collection.class.isAssignableFrom(value.getClass())) {
				Collection currVal = (Collection) value;
				Collection oldVal = (Collection) prevFields.remove(key);
				if(currVal.size()!=oldVal.size())
					fields.add(key.toString());
				else {
					Map<String, Object> currMap =new HashMap<>();
					Map<String, Object> prevMap =new HashMap<>();
					currVal.forEach(val-> {
						if(val.getClass().getName().contains(ApplicationConstant.ADAPTABLE_PACKAGE))
							currMap.putAll(getNotNullFieldValueMap(val,false,false));
					});
					oldVal.forEach(val-> {
						if(val.getClass().getName().contains(ApplicationConstant.ADAPTABLE_PACKAGE))
							prevMap.putAll(getNotNullFieldValueMap(val,false,false));
					});
					if(MapUtils.isNotEmpty(currMap) || MapUtils.isNotEmpty(prevMap)) {
						Set<String> diffFields = getFieldsWithDifferentValues(currMap, prevMap);
						diffFields.forEach(field->fields.add(key.toString().concat(".").concat(field)));
					}else {
						if(currVal!=null && oldVal!=null && !currVal.equals(oldVal)) {
							fields.add(key.toString());
						}
					}
				}
			}else if(value!=null && Map.class.isAssignableFrom(value.getClass())) {
				Map currMap = (Map) value;
				Map prevMap = (Map) prevFields.remove(key);
				Set<String> diffFields = getFieldsWithDifferentValues(currMap, prevMap);
				diffFields.forEach(field->fields.add(key.toString().concat(".").concat(field)));
			}else if(value!=null && !(value instanceof Enum ) && value.getClass().toString().contains(ApplicationConstant.ADAPTABLE_PACKAGE)) {
				Map<String, Object> currMap = getNotNullFieldValueMap(value,true,true);
				Map<String, Object> prevMap = getNotNullFieldValueMap(prevFields.remove(key),true,true);
				Set<String> diffFields = getFieldsWithDifferentValues(currMap, prevMap);
				diffFields.forEach(field->fields.add(key.toString().concat(".").concat(field)));
			}else if(value==null && prevFields.get(key)==null)
				prevFields.remove(key.toString());
			else if(!prevFields.containsKey(key) || value == null || !value.equals(prevFields.remove(key))) {
				fields.add(key.toString());
			}else if(prevFields!=null && value.equals(prevFields.get(key)))
				prevFields.remove(key.toString());
		});
		if(prevFields!=null) {
			prevFields.forEach((key,value)->{
				fields.add(key.toString());
			});
		}
		return fields;

	}

	@SafeVarargs
	public static <T> T firstNonNull(final Supplier<T>... values) {
		if (values != null) {
			for (final Supplier<T> val : values) {
				if (val != null && val.get() != null) {
					return val.get();
				}
			}
		}
		return null;
	}

	public static <T> Set<Class<? extends T>> findAllMatchingInheritedTypes(Class<T> classType, String packagePrefix) {
		Reflections reflections = new Reflections(packagePrefix);
		return reflections.getSubTypesOf(classType);
	}

	/**
	 * 
	 * @param clazz
	 *            Type of instance to be selected
	 * @param objs
	 *            Instances of random classes
	 * @return First instance of type {@code clazz} in the {@code objs} list, or
	 *         {@code null} if there isn't any
	 * 
	 *         NOT USED ANYWHERE
	 */
	public static <T> T selectInstance(Class<T> clazz, Object... objs) {
		List<T> tList = filterInstances(clazz, objs);
		return tList.isEmpty() ? null : tList.get(0);
	}

	/**
	 * 
	 * @param clazz
	 *            Type of instance to be selected
	 * @param objs
	 *            Instances of random classes
	 * @return List of instances of type {@code clazz} in the {@code objs} list, or
	 *         empty list if there isn't any
	 * 
	 *         NOT USED ANYWHERE
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> filterInstances(Class<T> clazz, Object... objs) {
		List<T> tList = new ArrayList<>();
		for (Object obj : objs) {
			if (clazz.isInstance(obj)) {
				tList.add((T) obj);
			}
		}
		return tList;
	}
}
