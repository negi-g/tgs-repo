package com.tgs.services.base.enums;

import lombok.Getter;

@Getter
public enum SearchType {
	
	ALL("A"), ONEWAY("O") ,RETURN("R"), MULTICITY("M");
	
	private String code;
	
	private SearchType (String code) {
		this.code = code;
	}
	
	public static SearchType getEnumFromCode(String code) {
		return getSearchType(code);
	}

	public static SearchType getSearchType(String code) {
		for (SearchType type : SearchType.values()) {
			if (type.getCode().equals(code)) {
				return type;
			}
		}
		return null;
	}
}
