package com.tgs.services.base.helper;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = {
        LocalDateTimeTypeValidator.class,
        DoubleTypeValidator.class,
        LongTypeValidator.class,
        StringTypeValidator.class,
        BooleanTypeValidator.class,
        PaymentMediumTypeValidator.class
})
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ForbidInAPIRequest {
    String message() default "Unsupported field for this request.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
