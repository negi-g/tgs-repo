package com.tgs.services.base;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class MethodExecutionInfo {

	private Long timeInMs;
	private String methodName;
	private String type;
	private String subtype;
}
