package com.tgs.services.base;

import lombok.Getter;

@Getter
public enum SystemCheckPoint {

	REQUEST_STARTED,
	EXTERNAL_API_STARTED,
	EXTERNAL_API_FINISHED,
	EXTERNAL_API_PARSING_FINISHED,
	REQUEST_FINISHED,
}
