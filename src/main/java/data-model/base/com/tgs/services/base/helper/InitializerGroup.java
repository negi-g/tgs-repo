package com.tgs.services.base.helper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME) //
@Target(ElementType.TYPE)
public @interface InitializerGroup {

	public Group group();

	public enum Group {
		FLIGHT, RAIL, HOTEL, GENERAL;
	}

}
