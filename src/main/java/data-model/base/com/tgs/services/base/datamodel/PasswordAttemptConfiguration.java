package com.tgs.services.base.datamodel;

import lombok.Setter;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import org.apache.commons.lang3.ObjectUtils;


@Getter
@Setter
public class PasswordAttemptConfiguration {

	private Boolean disabled;

	// In minutes and ttl should always > 0 (if 0 than from aerospike never expire)
	private Integer ttl;

	@SerializedName("ma")
	private Integer maxAttempts;
	
	public int getMaxAttempts() {
		return ObjectUtils.firstNonNull(maxAttempts, 10);
	}
	
	public int getTtl() {
		int timeToLive = ObjectUtils.firstNonNull(ttl, 60);
		return timeToLive <= 0 ? 1 : timeToLive;
	}


}
