package com.tgs.services.base.datamodel;

import java.util.List;
import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(
		value = "This is useful for pagination. In case you would like to restrict no of results per page , then appropriately set the value of pageNumber,size")
public class PageAttributes {

	private static final int DEFAULT_PAGE_NUMBER = 0;
	private static final int DEFAULT_SIZE = 5000;

	@NotNull
	@ApiModelProperty(
			notes = "Specify pageNumber for which you would like to fetch results. Note : It starts from Zero, for page#1 , value should be 0 ",
			required = true, example = "0")
	private Integer pageNumber;

	@NotNull
	@ApiModelProperty(notes = "Specify no of results per page wise ", example = "10")
	private Integer size;

	@ApiModelProperty(notes = "Specify the sort order by parameter")
	private List<SortByAttributes> sortByAttr;

	public Integer getPageNumber() {
		if (pageNumber != null)
			return pageNumber;
		return DEFAULT_PAGE_NUMBER;
	}

	public Integer getSize() {
		if (size != null)
			return size;
		return DEFAULT_SIZE;
	}

}
