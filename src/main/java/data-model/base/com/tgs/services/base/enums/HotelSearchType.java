package com.tgs.services.base.enums;

import lombok.Getter;

@Getter
public enum HotelSearchType {

	ALL("A"), DOMESTIC("D"), INTERNATIONAL("I");

	private String code;

	HotelSearchType(String code) {
		this.code = code;
	}

	public String getName() {
		return this.code;
	}

	public static HotelSearchType getEnumFromCode(String code) {
		return getHotelSearchType(code);
	}

	public static HotelSearchType getHotelSearchType(String code) {
		for (HotelSearchType action : HotelSearchType.values()) {
			if (action.getCode().equals(code))
				return action;
		}
		return null;
	}
}
