package com.tgs.services.base.helper;

import javax.validation.ConstraintValidatorContext;

public class StringTypeValidator extends NotSupportedValidator<String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null;
    }
}
