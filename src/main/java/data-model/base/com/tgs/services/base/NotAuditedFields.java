package com.tgs.services.base;

import java.util.List;

import lombok.Getter;

@Getter
public class NotAuditedFields {
	
	private String entity;
	
	//Fields which will be removed from response in AuditResult
	private List<String> fields;

}
