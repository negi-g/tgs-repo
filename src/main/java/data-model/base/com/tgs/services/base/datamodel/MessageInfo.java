package com.tgs.services.base.datamodel;

import org.apache.commons.lang3.StringUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class MessageInfo implements VoidClean {
	
	private MessageType type;
	private String message;
	
	@Override
	public void cleanData() {
		setMessage(StringUtils.defaultIfBlank(message, null));
	}

	@Override
	public boolean isVoid() {
		return type==null && message==null;
	}
}
