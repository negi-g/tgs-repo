package com.tgs.services.base.configurationmodel;

import java.util.List;

import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;

@Getter
public class MoneyExchangeConfiguration implements IRuleOutPut {
	
	private String baseUrl;
	private String userName;
	private String password;
	private List<String> type;
	private String bean;
	

}
