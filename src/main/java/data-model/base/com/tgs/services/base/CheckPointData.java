package com.tgs.services.base;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class CheckPointData {
	private String type;
	private String subType;
	private Long time;
	private boolean isParallel;
}
