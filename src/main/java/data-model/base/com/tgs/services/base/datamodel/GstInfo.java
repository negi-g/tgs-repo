package com.tgs.services.base.datamodel;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.GSTINValidator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GstInfo extends DataModel implements Validatable {

	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	private String userId;
	private Boolean enabled;
	@SearchPredicate(type = PredicateType.EQUAL)
	private String gstNumber;
	private String email;
	private String mobile;
	private String address;
	private String state;
	private String pincode;
	private String cityName;
	@SearchPredicate(type = PredicateType.LIKE)
	private String registeredName;
	private String phone;
	private Boolean isUsedInRail;

	@DBExclude
	private String bookingId;
	private String bookingUserId;
	private String accountCode;
	@SearchPredicate(type = PredicateType.LIKE)
	private String billingCompanyName;

	@Exclude
	@SearchPredicate(type = PredicateType.EQUAL)
	private Boolean isDeleted;

	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private Long id;

	@DBExclude
	private Boolean isSave;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		if (StringUtils.isNotBlank(getGstNumber())) {
			gstNumber = gstNumber.toUpperCase();
			if (!GSTINValidator.validGSTIN(getGstNumber())) {
				errors.put("gstNumber", SystemError.INVALID_GST.getErrorDetail());
			}
		}
		if (!BooleanUtils.isTrue(isUsedInRail) && !EmailValidator.getInstance().isValid(getEmail())) {
			errors.put("email", SystemError.INVALID_EMAIL.getErrorDetail());
		}
		if (StringUtils.isBlank(registeredName)) {
			errors.put("registeredName", SystemError.INVALID_NAME.getErrorDetail());
		}
		if (!BooleanUtils.isTrue(isUsedInRail) && StringUtils.isBlank(mobile)) {
			errors.put("mobile", SystemError.INVALID_MOBILE.getErrorDetail());
		}
		if (StringUtils.isBlank(address)) {
			errors.put("address", SystemError.INVALID_ADDRESS.getErrorDetail());
		}
		if (BooleanUtils.isTrue(isUsedInRail) && StringUtils.isBlank(pincode)) {
			errors.put("pincode", SystemError.INVALID_PINCODE.getErrorDetail());
		}
		if (BooleanUtils.isTrue(isUsedInRail) && StringUtils.isBlank(state)) {
			errors.put("state", SystemError.INVALID_STATE_NAME.getErrorDetail());
		}
		if (BooleanUtils.isTrue(isUsedInRail) && StringUtils.isBlank(cityName)) {
			errors.put("cityName", SystemError.INVALID_CITY_NAME.getErrorDetail());
		}
		return errors;
	}
	
	public String getEmail() {
		return getValidEmail(this.email);
	}

	public void cleanData() {
		if (StringUtils.isEmpty(this.gstNumber)) {
			this.gstNumber = "";
		}
		if (StringUtils.isEmpty(this.email)) {
			this.email = "";
		}
		if (StringUtils.isEmpty(this.mobile)) {
			this.mobile = "";
		}
		if (StringUtils.isEmpty(this.address)) {
			this.address = "";
		}
		if (StringUtils.isEmpty(this.state)) {
			this.state = "";
		}
		if (StringUtils.isEmpty(this.pincode)) {
			this.pincode = "";
		}
		if (StringUtils.isEmpty(this.cityName)) {
			this.cityName = "";
		}
		if (StringUtils.isEmpty(this.registeredName)) {
			this.registeredName = "";
		}
	}
	
	private String getValidEmail(String email) {
		if (email != null && email.contains("_")) {
			int count = email.indexOf("@");
			int underscoreLastCount = email.lastIndexOf("_");
			if (count < underscoreLastCount) {
				return email.substring(0, underscoreLastCount);
			}
		}
		return email;
	}
}
