package com.tgs.services.base.configurationmodel;

import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;

@Getter
public class EmailVendorConfiguration implements IRuleOutPut {

	private String keyId;

	private String beanName;

	private String applicationName;

	private String clientId;

	private String clientSecret;
	
	private String refreshToken;
}
