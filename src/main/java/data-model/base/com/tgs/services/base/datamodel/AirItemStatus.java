package com.tgs.services.base.datamodel;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import com.tgs.services.base.enums.AmendmentType;
import lombok.Getter;

@Getter
public enum AirItemStatus {
	IN_PROGRESS("IP") {
		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(ON_HOLD, HOLD_PENDING, PAYMENT_FAILED, PAYMENT_SUCCESS, ABORTED));
		}
	},

	ON_HOLD("OH") {
		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>(
					Arrays.asList(SUCCESS, TICKET_PENDING, CONFIRM_PENDING, ABORTED, PAYMENT_SUCCESS, UNCONFIRMED));
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(Arrays.asList(AmendmentType.CANCELLATION_QUOTATION, AmendmentType.REISSUE_QUOTATION,
					AmendmentType.MISCELLANEOUS));
		}
	},

	HOLD_PENDING("HP") {
		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(ON_HOLD, ABORTED));
		}
	},

	PAYMENT_SUCCESS("PS") {
		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS, PNR_PENDING, TICKET_PENDING, CONFIRM_PENDING, ABORTED));
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(Arrays.asList(AmendmentType.FARE_CHANGE));
		}
	},

	SUCCESS("S") {
		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(CANCELLED));
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(Arrays.asList(AmendmentType.values()));
		}
	},

	PNR_PENDING("PP") {
		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS, TICKET_PENDING, ABORTED));
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(
					Arrays.asList(AmendmentType.FARE_CHANGE, AmendmentType.CANCELLATION, AmendmentType.VOIDED));
		}
	},

	TICKET_PENDING("TP") {
		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS, CONFIRM_PENDING, ABORTED));
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(
					Arrays.asList(AmendmentType.FARE_CHANGE, AmendmentType.CANCELLATION, AmendmentType.VOIDED));
		}
	},

	CONFIRM_PENDING("CP") {
		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS, ABORTED));
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(Arrays.asList(AmendmentType.FARE_CHANGE));
		}
	},

	TO_CANCEL("TC") {

		// Not in use

		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(ABORTED));
		}
	},

	PAYMENT_FAILED("F") {
		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(ABORTED));
		}
	},

	ABORTED("A") {
		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	},

	UNCONFIRMED("UC") {

		// used when Hold PNR-Released

		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	},
	CANCELLED("C") {

		// used when Cancelling All Passengers

		@Override
		public Set<AirItemStatus> nextStatusSet() {
			return new HashSet<>();
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(Arrays.asList(AmendmentType.CORRECTION));
		}
	};

	public String getStatus() {
		return this.getCode();
	}

	private String code;

	AirItemStatus(String code) {
		this.code = code;
	}

	public static AirItemStatus getAirItemStatus(String code) {
		for (AirItemStatus role : values()) {
			if (role.getCode().equals(code)) {
				return role;
			}
		}
		return null;
	}
	
	public static AirItemStatus getEnumFromCode(String code) {
		return getAirItemStatus(code);
	}

	public abstract Set<AirItemStatus> nextStatusSet();

	public Set<AmendmentType> validAmdTypes() {
		return new HashSet<>();
	}
}
