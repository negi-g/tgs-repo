package com.tgs.services.base.datamodel;

import lombok.Getter;

@Getter
public enum DocumentSubType {

	MOBILE_BILL("MB"),
	INTERNET_BILL("IB"),
	CREDITCARD_STMNT("CCS"),
	BANK_STATEMENT("BS"),
	BANK_PASSBOOK("BP"),
	ELECTRIC_BILL("EB"),
	WATER_BILL("WB"),
	RENTAL_AGREEMENT("RA"),
	SHOP_CERTIFICATE("SC"),
	GUMASTA_LICENCE("GL"),
	OTHER_GOVT_CRT("OC"),
	VOTER_ID("VC"),
	DRIVING_LICENSE("DL"),
	RATION_CARD("RC"),
	GAS_BILL("GC"),
	PASSPORT("PP"),
	AADHAR_CARD("AC"),
	POSTOFFICE_PASSBOOK("PO");

	private String code;

	DocumentSubType(String code) {
		this.code = code;
	}

	public static DocumentSubType getEnumFromCode(String code) {
		for (DocumentSubType docSubType : DocumentSubType.values()) {
			if (docSubType.getCode().equals(code)) {
				return docSubType;
			}
		}
		return null;
	}

}
