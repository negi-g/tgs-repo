package com.tgs.services.base.datamodel;

import com.tgs.services.base.helper.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtConfiguration implements Configuration {

	private String encryptedKey;
	private String awsKeyId;
	private long accessTokenExpirationTime;
	private long accessTokenRefreshTime;
	private String regex;
}
