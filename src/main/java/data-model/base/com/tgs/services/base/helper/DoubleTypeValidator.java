package com.tgs.services.base.helper;

import javax.validation.ConstraintValidatorContext;

public class DoubleTypeValidator extends NotSupportedValidator<Double> {
    @Override
    public boolean isValid(Double value, ConstraintValidatorContext context) {
        return value == null;
    }
}
