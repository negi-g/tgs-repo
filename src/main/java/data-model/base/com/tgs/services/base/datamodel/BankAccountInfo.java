package com.tgs.services.base.datamodel;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class BankAccountInfo {

	private String bankName;

	private String accountNumber;

	private String ifscCode;

	private String comments;

	private String accountHolderName;
}
