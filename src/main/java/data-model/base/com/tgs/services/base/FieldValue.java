package com.tgs.services.base;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class FieldValue {

	private String field;
	
	private String value;
}
