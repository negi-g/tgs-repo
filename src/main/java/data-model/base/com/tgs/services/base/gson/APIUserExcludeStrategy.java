package com.tgs.services.base.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.tgs.services.base.helper.APIUserExclude;

public class APIUserExcludeStrategy implements ExclusionStrategy {

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getAnnotation(APIUserExclude.class) != null
				|| f.getDeclaredClass().getAnnotation(APIUserExclude.class) != null;
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}
}
