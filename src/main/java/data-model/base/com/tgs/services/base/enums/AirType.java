package com.tgs.services.base.enums;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public enum AirType {

	ALL("A"), DOMESTIC("D"), INTERNATIONAL("I"), SOTO("S");

	private String code;

	AirType(String code) {
		this.code = code;
	}

	public String getName() {
		return this.code;
	}

	public static AirType getEnumFromCode(String code) {
		return getAirType(code);
	}

	private static AirType getAirType(String code) {
		for (AirType action : AirType.values()) {
			if (action.getCode().equals(code))
				return action;
		}
		return null;
	}

	public static List<String> getCodes(List<AirType> airTypes) {
		List<String> types = new ArrayList<>();
		airTypes.forEach(type -> {
			types.add(type.getCode());
		});
		return types;
	}
}
