package com.tgs.services.base.ruleengine;

public interface IRuleField {

	public boolean isValidAgainstFact(IFact fact, IRuleCriteria ruleCriteria);

}
