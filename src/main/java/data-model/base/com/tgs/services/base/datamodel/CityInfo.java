package com.tgs.services.base.datamodel;

import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString()
@Getter
@Setter
@Builder
public class CityInfo {

	@NotNull
	@ApiModelProperty(required = true)
	private String name;

	@NotNull
	@ApiModelProperty(required = true)
	private String state;

	@NotNull
	@ApiModelProperty(required = true)
	private String country;
}
