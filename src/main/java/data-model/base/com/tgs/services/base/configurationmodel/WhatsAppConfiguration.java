package com.tgs.services.base.configurationmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;

@Getter
public class WhatsAppConfiguration implements IRuleOutPut {

    private String url;

    private String sourceId;

    private String apiKey;

    private String templateLocale;

    private String sourceName;
}

