package com.tgs.services.base.datamodel;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.SystemError;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TravellerInfo implements Validatable<TravellerInfoValidatingData> {

	@SerializedName("ti")
	private String title;

	@SerializedName("pt")
	private PaxType paxType;

	@SerializedName("fN")
	private String firstName;

	@SerializedName("lN")
	private String lastName;

	protected LocalDate dob;

	private Integer age;

	@SerializedName("pNum")
	private String passportNumber;

	@SerializedName("eD")
	private LocalDate expiryDate;

	@SerializedName("pNat")
	private String passportNationality;

	@SerializedName("pid")
	private LocalDate passportIssueDate;

	@SerializedName("ff")
	private Map<String, String> frequentFlierMap;

	@APIUserExclude
	@SerializedName("nbId")
	private String newBookingId;

	@APIUserExclude
	@SerializedName("obId")
	private String oldBookingId;

	@APIUserExclude
	@SerializedName("cp")
	private Double costPrice;

	// corporate employee
	@APIUserExclude
	@DBExclude
	private UserProfile userProfile;

	// corporate employee
	@APIUserExclude
	private String userId;

	@APIUserExclude
	private String invoice;

	@APIUserExclude
	private Long id;

	@SerializedName("pan")
	@APIUserExclude
	private String panNumber;

	@DBExclude
	@Exclude
	private Boolean isSave;
	
	@APIUserExclude
	@SerializedName("ats")
	private List<Attachment> attachments;

	@APIUserExclude
	@SerializedName("ed")
	private String extraDetail;

	public PaxType getPaxType() {
		return paxType;
	}

	public UserProfile getUserProfile() {
		return userProfile == null ? UserProfile.builder().build() : userProfile;
	}

	@Override
	public FieldErrorMap validate(TravellerInfoValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		// validate name
		if (StringUtils.isBlank(title)) {
			errors.put("title", SystemError.INVALID_TITLE.getErrorDetail());
		}
		// if (StringUtils.isBlank(firstName)) {
		// errors.put("firstName", SystemError.INVALID_NAME.getErrorDetail());
		// }
		if (StringUtils.isBlank(firstName) && StringUtils.isBlank(lastName)) {
			errors.put("firstName", SystemError.EMPTY_NAME.getErrorDetail());
		}
		if (getPaxType() == null) {
			errors.put("paxType", SystemError.INVALID_PAXTYPE.getErrorDetail());
		}
		return errors;
	}

	@ApiModelProperty(hidden = true)
	public String getMiddleName() {
		return StringUtils.EMPTY;
	}

	public String getFullName() {
		return StringUtils.join(title, " ", firstName, " ", lastName);
	}

	public String getPaxKey() {
		return StringUtils.join(firstName, " ", lastName);
	}
}
