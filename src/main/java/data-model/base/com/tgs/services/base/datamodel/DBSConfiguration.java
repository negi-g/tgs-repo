package com.tgs.services.base.datamodel;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

@Getter
public class DBSConfiguration {

	private String publicKey;
	
	private String privateKey; 
	
	private String passPhrase;
	
	private String userId;
	
	private String endPoint;
	
	@SerializedName("ais")
	private List<BankAccountInfo> accountInfos;
}
