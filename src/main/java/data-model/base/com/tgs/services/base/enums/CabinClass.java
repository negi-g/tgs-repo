package com.tgs.services.base.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.WordUtils;

import lombok.Getter;

@Getter
public enum CabinClass {

    ECONOMY("Y"),
    PREMIUM_ECONOMY("S") {
        @Override
        public List<CabinClass> getAllowedCabinClass() {
            List<CabinClass> allowedClass = Arrays.asList(CabinClass.PREMIUM_ECONOMY, CabinClass.BUSINESS,
                    CabinClass.PREMIUM_BUSINESS, CabinClass.FIRST, CabinClass.PREMIUMFIRST);
            return allowedClass;
        }

        @Override
        public boolean isAllowedCabinClass(CabinClass cabinClass) {
            return getAllowedCabinClass().contains(cabinClass);
        }
    },
    BUSINESS("C") {
        @Override
        public List<CabinClass> getAllowedCabinClass() {
            List<CabinClass> allowedClass = Arrays.asList(CabinClass.BUSINESS, CabinClass.PREMIUM_BUSINESS,
                    CabinClass.FIRST, CabinClass.PREMIUMFIRST);
            return allowedClass;
        }

        @Override
        public boolean isAllowedCabinClass(CabinClass cabinClass) {
            return getAllowedCabinClass().contains(cabinClass);
        }
    },
    PREMIUM_BUSINESS("J") {
        @Override
        public List<CabinClass> getAllowedCabinClass() {
            List<CabinClass> allowedClass = Arrays.asList(CabinClass.PREMIUM_BUSINESS, CabinClass.FIRST,
                    CabinClass.PREMIUMFIRST);
            return allowedClass;
        }

        @Override
        public boolean isAllowedCabinClass(CabinClass cabinClass) {
            return getAllowedCabinClass().contains(cabinClass);
        }
    },
    FIRST("F") {
        @Override
        public List<CabinClass> getAllowedCabinClass() {
            List<CabinClass> allowedClass = Arrays.asList(CabinClass.FIRST, CabinClass.PREMIUMFIRST);
            return allowedClass;
        }

        @Override
        public boolean isAllowedCabinClass(CabinClass cabinClass) {
            return getAllowedCabinClass().contains(cabinClass);
        }
    },
    PREMIUMFIRST("P") {
        @Override
        public List<CabinClass> getAllowedCabinClass() {
            List<CabinClass> allowedClass = Arrays.asList(CabinClass.PREMIUMFIRST);
            return allowedClass;
        }

        @Override
        public boolean isAllowedCabinClass(CabinClass cabinClass) {
            return getAllowedCabinClass().contains(cabinClass);
        }
    };

    private String code;

    private CabinClass(String code) {
        this.code = code;
    }

    public boolean isAllowedCabinClass(CabinClass cabinClass) {
        return true;
    }

    public List<CabinClass> getAllowedCabinClass() {
        return Arrays.asList(CabinClass.values());
    }

    public static CabinClass getEnumFromCode(String code) {
        return getCabinClass(code).get();
    }

    public static Optional<CabinClass> getCabinClass(String code) {
        for (CabinClass cc : CabinClass.values()) {
            if (cc.getCode().equals(code)) {
                return Optional.of(cc);
            }
        }
        return Optional.empty();
    }

    public String getName() {
        return this.name();
    }

    public static List<String> getCodes(List<CabinClass> classes) {
        List<String> classCodes = new ArrayList<>();
        classes.forEach(cls -> {
            classCodes.add(cls.getCode());
        });
        return classCodes;
    }

    /**
     * @return name to be displayed on UI.
     */
    public String getDisplayName() {
        return WordUtils.capitalizeFully(name().replace('_', ' '));
    }
}
