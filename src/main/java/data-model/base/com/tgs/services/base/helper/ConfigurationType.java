package com.tgs.services.base.helper;

import com.tgs.services.base.datamodel.JwtConfiguration;

import lombok.Getter;

/*
 * Make sure that name of enum constants shouldn't be less than 14 character because Aerospike doesn't support bin length more than 14 characters
 * */

@Getter
public enum ConfigurationType {
	JWTCONFIG(JwtConfiguration.class);

	private Class<? extends Configuration> classType;

	private ConfigurationType(Class<? extends Configuration> classType) {
		this.classType = classType;
	}

	@SuppressWarnings("rawtypes")
	public static Class getClassType(String type) {
		return ConfigurationType.valueOf(type).getClassType();
	}
}
