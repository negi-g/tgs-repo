package com.tgs.services.base.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class AttachmentMetadata {
	
	private String url;

	private byte[] fileData ;
	
	private String fileName;
}
