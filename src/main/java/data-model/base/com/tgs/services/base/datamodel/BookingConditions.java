package com.tgs.services.base.datamodel;

import java.time.LocalDateTime;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
public class BookingConditions extends Conditions {

	@SerializedName("isBA")
	private Boolean isBlockingAllowed;

	@SerializedName("st")
	private Integer sessionTimeInSecond;

	@SerializedName("sct")
	private LocalDateTime sessionCreationTime;

	@SerializedName("gst")
	private GstConditions gstInfo;

}
