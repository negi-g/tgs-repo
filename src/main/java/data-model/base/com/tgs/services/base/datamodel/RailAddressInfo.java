package com.tgs.services.base.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailAddressInfo {

	private String address;

	private String pincode;

	private RailCityInfo cityInfo;

	@SerializedName("po")
	private String postOffice;

	@SerializedName("lm")
	private String landMark;

}
