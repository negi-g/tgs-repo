package com.tgs.services.base.gson;


import com.tgs.services.base.CustomGeneralException;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public abstract class Mapper<RT> {

    public RT convert() throws CustomGeneralException {
        try {
            beforeExecution();
            execute();
        } catch (CustomGeneralException e) {
            log.error("Error in processing execute. Unable to convert to class {}.", getOutput().getClass(),
                    e.getMessage());
            throw e;
        }
        return getOutput();
    }

    protected abstract void execute() throws CustomGeneralException;

    protected RT output;

    public Mapper<RT> setOutput(RT response) {
        this.output = response;
        return this;
    }

    protected void beforeExecution() {

    }
}

