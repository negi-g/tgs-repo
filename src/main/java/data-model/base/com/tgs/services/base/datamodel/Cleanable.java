package com.tgs.services.base.datamodel;

public interface Cleanable {

	default void cleanData() {
		
	}

	public static class Cleaner {

		public static <C extends Cleanable> C clean(C cleanable) {
			if (cleanable != null) {
				cleanable.cleanData();
			}
			return cleanable;
		}
	}
}
