package com.tgs.services.base.enums;

public enum MessageMedium {

	SMS,
	WHATSAPP,
	EMAIL
}
