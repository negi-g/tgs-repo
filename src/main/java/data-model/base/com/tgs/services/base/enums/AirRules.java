package com.tgs.services.base.enums;

import lombok.Getter;

@Getter
public enum AirRules {

	COMM("COMMISSION"),
	CCID("CREDITCARD"),
	IATA("IATA"),
	TC("TOURCODE"),
	FR("FARERULE"),
	CFT("CHANGE_FARE_TYPE"),
	FT("FARETYPE"),
	HF("HOLDFEE"),
	HC("HOLDCONFIG"),
	DOB("DOB"),
	PP("PASSPORT");

	private String name;

	AirRules(String str) {
		this.name = str;
	}

	public static AirRules getEnumFromCode(String code) {
		for (AirRules status : AirRules.values()) {
			if (status.getName().equals(code)) {
				return status;
			}
		}
		return null;
	}
}
