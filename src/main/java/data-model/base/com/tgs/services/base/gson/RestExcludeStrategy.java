package com.tgs.services.base.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.tgs.services.base.helper.RestExclude;

public class RestExcludeStrategy implements ExclusionStrategy {
    @Override
    public boolean shouldSkipField(FieldAttributes f) {
        return f.getAnnotation(RestExclude.class) != null
                || f.getDeclaredClass().getAnnotation(RestExclude.class) != null;
    }

    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }
}
