package com.tgs.services.base.datamodel;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VerifyHotelMappingQuery {
	private String unicaId;
	private String bookingReferenceId;
	private String bookingClientName;
	private String providerName;
	private String providerHotelId;
	private String checkInDate;
	private String checkOutDate;
}
