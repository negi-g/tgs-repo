package com.tgs.services.base.datamodel;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.UserRole;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SecurityConfiguration {

	@SerializedName("ln2d")
	private boolean loginInUsing2d;

	@SerializedName("die")
	private Integer deviceInactivityExpriration;

	@SerializedName("ex2duids")
	private List<String> expemted2dLoginInUserIds;

	@SerializedName("ex2droles")
	private List<UserRole> exempted2dLoginUserRoles;
	
	@SerializedName("incpids")
	private List<String> includedPartnerIds;

	public List<String> getExpemted2dLoginInUserIds() {
		return expemted2dLoginInUserIds != null ? expemted2dLoginInUserIds : new ArrayList<>();
	}
	public List<UserRole> getExempted2dLoginUserRoles() {
		return exempted2dLoginUserRoles != null ? exempted2dLoginUserRoles : new ArrayList<>();
	}

	public List<String> getIncludedPartnerIds() {
		if (includedPartnerIds == null) {
			includedPartnerIds = new ArrayList<String>();
		}
		includedPartnerIds.add("0");
		return includedPartnerIds;
	}
}
