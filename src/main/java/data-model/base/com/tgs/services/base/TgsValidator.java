package com.tgs.services.base;

import java.util.Map.Entry;

import org.springframework.validation.Errors;

import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;

/**
 * @author Abhineet Kumar, Technogram Solutions
 * @see Validatable
 */
public abstract class TgsValidator {

	final public void registerErrors(Errors errors, String parentFieldName,
			Validatable<? extends ValidatingData> validatable) {
		registerErrors(errors, parentFieldName, validatable, null);
	}

	/**
	 * @param errors
	 * @param parentFieldName
	 * @param validatable
	 */
	public <VD extends ValidatingData> void registerErrors(Errors errors, String parentFieldName,
			Validatable<VD> validatable, VD validatingData) {
		if (errors == null || validatable == null) {
			return;
		}

		FieldErrorMap fieldErrorMap = validatable.validate(validatingData);

		if (fieldErrorMap == null) {
			return;
		}

		if (parentFieldName == null) {
			parentFieldName = "";
		}

		if (!parentFieldName.isEmpty()) {
			parentFieldName += ".";
		}

		for (Entry<String, ErrorDetail> errorInfoEntry : fieldErrorMap.entrySet()) {
			ErrorDetail errorInfo = errorInfoEntry.getValue();
			errors.rejectValue(parentFieldName + errorInfoEntry.getKey(), errorInfo.getErrCode(),
					errorInfo.getMessage());
		}
	}
}
