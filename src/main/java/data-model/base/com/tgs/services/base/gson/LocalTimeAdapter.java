package com.tgs.services.base.gson;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LocalTimeAdapter implements JsonSerializer<LocalTime>, JsonDeserializer<LocalDateTime> {
	DateTimeFormatter formatter = null;

	public LocalTimeAdapter(DateTimeFormatter formatter) {
		if (formatter != null) {
			this.formatter = formatter;
		}
	}

	public LocalTimeAdapter() {

	}

	@Override
	public JsonElement serialize(LocalTime localTime, Type type, JsonSerializationContext context) {
		if (formatter != null) {
			return new JsonPrimitive(localTime.format(formatter));
		}
		return new JsonPrimitive(localTime.toString());
	}

	@Override
	public LocalDateTime deserialize(JsonElement dateString, Type type, JsonDeserializationContext context)
			throws JsonParseException {
		if(StringUtils.isBlank(dateString.getAsString())) {
			log.error("Unable to parse empty string as date");
			throw new CustomGeneralException(SystemError.BAD_REQUEST);
		}
		if (formatter != null) {
			return LocalDateTime.parse(dateString.getAsString(), formatter);
		}
		return LocalDateTime.parse(dateString.getAsString());
	}
}
