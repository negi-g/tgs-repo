package com.tgs.services.base.enums;


public enum AlertType {
	FAREALERT, SOLDOUT, DUPLICATE_ORDER, STATIC_DATA_ALERT, COMMISSION_DIFF, BOOKING_ALERT, HOTEL_NOT_AVAILABLE, HOTEL_STATUS_CHANGE;

	public static AlertType getAlertTypeFromAlertName(String alertName) {
		for (AlertType alertType : AlertType.values()) {
			if (alertType.name().equals(alertName)) {
				return alertType;
			}
		}
		return null;
	}
}
