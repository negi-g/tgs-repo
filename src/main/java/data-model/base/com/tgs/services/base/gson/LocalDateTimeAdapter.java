package com.tgs.services.base.gson;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LocalDateTimeAdapter implements JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {
	DateTimeFormatter formatter = null;

	public LocalDateTimeAdapter(DateTimeFormatter formatter) {
		if (formatter != null) {
			this.formatter = formatter;
		}
	}

	public LocalDateTimeAdapter() {

	}

	@Override
	public JsonElement serialize(LocalDateTime localDateTime, Type type, JsonSerializationContext context) {
		if (formatter != null) {
			return new JsonPrimitive(localDateTime.format(formatter));
		}
		return new JsonPrimitive(localDateTime.toString());
	}

	@Override
	public LocalDateTime deserialize(JsonElement dateString, Type type, JsonDeserializationContext context)
			throws CustomGeneralException {
		if(StringUtils.isBlank(dateString.getAsString())) {
			log.error("Unable to parse empty string as date");
			throw new CustomGeneralException(SystemError.BAD_REQUEST);
		}
		LocalDateTime localDateTime = null;
		try {
			localDateTime = LocalDateTime.parse(dateString.getAsString());
		} catch (Exception e) {
			localDateTime = LocalDateTime.parse(dateString.getAsString(),
					DateTimeFormatter.ofPattern("dd/MM/yyyy'T'HH:mm"));
		}
		return localDateTime;

	}

}
