package com.tgs.services.base.datamodel;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class GstConditions {

	@SerializedName("gstappl")
	private Boolean gstApplicable;

	@SerializedName("igm")
	private Boolean isGSTMandatory;
}
