package com.tgs.services.base.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailCityInfo {

	private String name;

	private String state;

	private String country;
}
