package com.tgs.services.pms.datamodel.pg.easebuzz;

import com.google.common.base.Joiner;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class EasebuzzPgResponse extends PgResponse {

	private String hash;

	@SerializedName("txnid")
	private String refId;

	private String amount;
	private String email;
	private String phone;
	private String firstname;

	@SerializedName("net_amount_debit")
	private String totalAmountDebited;

	@SerializedName("issuing_bank")
	private String bank;

	@SerializedName("easepayid")
	private String eazebuzzTxnId;

	private String status;
	private String productinfo;
	private String error;
	private String error_Message;

	@Override
	public String getRefId() {
		return refId;
	}

	public String getPlainSignature(String salt, String key) {
		return Joiner.on('|').useForNull("").join(salt, status, "|||||||||", email, firstname, productinfo, amount, refId, key);
	}
}
