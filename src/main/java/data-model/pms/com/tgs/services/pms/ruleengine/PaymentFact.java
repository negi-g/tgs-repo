package com.tgs.services.pms.ruleengine;

import java.math.BigDecimal;
import java.util.List;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.DepositType;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
public class PaymentFact extends GeneralBasicFact {

	private Product product;

	private PaymentMedium medium;

	private String userCity;

	private String userState;
	
	private List<String> groupIds;

	private String subMedium;

	private String accountNumber;
	
	private Boolean isPaymentProcess;
	
	/**
	 * Currently it is being used for payment modes
	 */
	private String accountName;

	private DepositType depositType;

	private BigDecimal amount;
	
	private Boolean isPartner;

	public static PaymentFact createFact() {
		return PaymentFact.builder().build();
	}

	public PaymentFact generateFact(Payment payment) {
		if (payment != null) {
			setMedium(payment.getPaymentMedium());
			setProduct(payment.getProduct());
			setAmount(payment.getAmount());
		}
		return this;
	}

	public PaymentFact generateFact(PaymentRequest paymentRequest) {
		if (paymentRequest != null) {
			setMedium(paymentRequest.getPaymentMedium());
			setProduct(paymentRequest.getProduct());
			setUserId(paymentRequest.getPayUserId());
			setAmount(paymentRequest.getAmount());
		}
		return this;
	}

	public PaymentFact generateFact(DepositRequest depositRequest) {
		if (depositRequest == null) {
			return this;
		}
		setUserId(depositRequest.getUserId());
		setAccountNumber(depositRequest.getAccountNumber());
		setBankName(depositRequest.getBank());
		setDepositType(depositRequest.getType());
		setAmount(BigDecimal.valueOf(depositRequest.getRequestedAmount()).setScale(2, BigDecimal.ROUND_HALF_EVEN));
		return this;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
