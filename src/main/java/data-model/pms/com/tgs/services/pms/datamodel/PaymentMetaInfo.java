package com.tgs.services.pms.datamodel;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PaymentMetaInfo {

	Boolean fetchWalletBalance;

	Boolean fetchCreditLineBalance;

	Boolean fetchNRCredit;

	List<String> userIds;

}
