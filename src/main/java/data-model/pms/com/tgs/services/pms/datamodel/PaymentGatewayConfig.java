package com.tgs.services.pms.datamodel;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentGatewayConfig extends DataModel {

	@ApiModelProperty(notes = "To fetch based on id ", example = "1")
	@SearchPredicate(type = PredicateType.EQUAL)
	private Long id;
	
	@ApiModelProperty(notes = "To fetch based on gatewaytype ", example = "ATOM_AES")
	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private GateWayType gatewayType;

	@ApiModelProperty(notes = "To fetch based on gateway config name", example = "Atom Aes")
	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private String name;
	
	@ApiModelProperty(notes = "To fetch based on is deleted config", example = "true")
	@SearchPredicate(type = PredicateType.EQUAL)
	private boolean isDeleted;
	
	@ApiModelProperty(notes = "To fetch based on is hidden config", example = "true")
	@SearchPredicate(type = PredicateType.EQUAL)
	private boolean isHidden;
}
