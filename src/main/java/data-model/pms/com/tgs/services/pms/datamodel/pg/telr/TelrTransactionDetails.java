package com.tgs.services.pms.datamodel.pg.telr;

import java.util.Map;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TelrTransactionDetails {

	private String ref;
	private String cartid;
	private String amount;
	private Map<String, String> status;
	private Map<String, String> transaction;
}
