package com.tgs.services.pms.datamodel.pg;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EasyPayRefundInputData {

	@SerializedName("Paymode")
	private String paymode;
	@SerializedName("TransactionID")
	private String transactionId;
	@SerializedName("TransactionDate")
	private String transactionDate;
	@SerializedName("MerchantId")
	private String merchantId;
	@SerializedName("UserId")
	private String userId;
	@SerializedName("signature")
	private String signature;
	@SerializedName("RefundAmount")
	private String refundAmount;


}
