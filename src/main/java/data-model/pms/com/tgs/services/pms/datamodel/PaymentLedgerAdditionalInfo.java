package com.tgs.services.pms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentLedgerAdditionalInfo {

	private Double tds;
	private Double markup;
	private Double paymentFee;
}
