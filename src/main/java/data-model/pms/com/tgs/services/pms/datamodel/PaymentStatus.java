package com.tgs.services.pms.datamodel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public enum PaymentStatus {
	SUCCESS("S"), FAILURE("F"), PENDING("P"), REFUND_SUCCESS("RS"), INITIATE_REDIRECTION("IR");

	public String getPaymentStatus() {
		return this.getCode();
	}

	private String code;

	private PaymentStatus(String code) {
		this.code = code;
	}

	public static PaymentStatus getEnumFromCode(String code) {
		return getPaymentStatus(code);
	}

	public static PaymentStatus getPaymentStatus(String code) {
		for (PaymentStatus status : PaymentStatus.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}
	
	public static List<String> getCodes(List<PaymentStatus> status) {
		List<String> paymentStatus = new ArrayList<>();
		status.forEach(type -> {
			paymentStatus.add(type.getCode());
		});
		return paymentStatus;
	}
}
