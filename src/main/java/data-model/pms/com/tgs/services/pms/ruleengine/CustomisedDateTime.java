package com.tgs.services.pms.ruleengine;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
public class CustomisedDateTime {

    private Short relDays;

    @SerializedName("downv")
    private Short dayOfWeekNumeric;

    private String time;

    public static LocalDateTime getAbsoluteDateTime(CustomisedDateTime customisedDateTime) {
        if (customisedDateTime == null) return null;
        LocalDate date = LocalDate.now();
        if (customisedDateTime.relDays != null) {
            date = date.plusDays(customisedDateTime.relDays);
        }
        if (customisedDateTime.dayOfWeekNumeric != null) {
            int diff = DayOfWeek.of(customisedDateTime.dayOfWeekNumeric).compareTo(LocalDateTime.now().getDayOfWeek());
            if (diff < 0) diff += 7;
            date = date.plusDays(diff);
        }
        LocalTime time = LocalTime.parse(customisedDateTime.time);
        return date.atTime(time);
    }
}