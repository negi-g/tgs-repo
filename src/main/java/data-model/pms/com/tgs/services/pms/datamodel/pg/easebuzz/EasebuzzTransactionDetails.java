package com.tgs.services.pms.datamodel.pg.easebuzz;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EasebuzzTransactionDetails {
	

	@SerializedName("net_amount_debit")
	private String totalAmountDebited;
	
	private String status;
	
	@SerializedName("easepayid")
	private String easebuzzTxnId;
	
	private String error;
	
	@SerializedName("issuing_bank")
	private String issuingBank;
}

