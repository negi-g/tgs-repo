package com.tgs.services.pms.datamodel.pg.hdfcCcavenue;

import com.tgs.services.pms.datamodel.pg.PgResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class HdfcCcavenuePgResponse extends PgResponse {

	private String status;

	private String refId;

	private String crossSellUrl;

	private String result;

	private String encResp;

	private String orderNo;
	
	private String order_id;

	private String tracking_id;

	private String bank_ref_no;

	private String order_status;

	private String failure_message;

	private String payment_mode;

	private String card_name;

	private String status_message;

	private String status_code;

	private String amount;

	private String currency;

	@Override
	public String getRefId() {
		return refId;
	}

}
