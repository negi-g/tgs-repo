package com.tgs.services.pms.ruleengine;

import java.util.List;
import java.util.Optional;

import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.pms.datamodel.IncentiveSlab;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DepositIncentiveConfig extends PaymentConfigurationRuleOutput {

	private List<IncentiveSlab> slabList;

	public double getIncentive(double amount) {
		Optional<IncentiveSlab> slab = slabList.stream().filter(s -> s.getMinAmount() < amount && s.getMaxAmount() > amount).findFirst();
		return slab.map(IncentiveSlab::getIncentive).orElse(0d);
	}
}
