package com.tgs.services.pms.datamodel.pg.telr;

import com.tgs.services.pms.datamodel.pg.RefundInfo;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TelrRefundInfo implements RefundInfo {

	private String ivp_store;
	private String ivp_authkey;
	private String ivp_trantype;
	private String ivp_tranclass;
	private String ivp_currency;
	private String ivp_amount;
	private String tran_ref;
	private String ivp_test;
}