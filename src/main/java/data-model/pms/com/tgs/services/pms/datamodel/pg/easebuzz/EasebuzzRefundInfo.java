package com.tgs.services.pms.datamodel.pg.easebuzz;

import com.tgs.services.pms.datamodel.pg.RefundInfo;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class EasebuzzRefundInfo implements RefundInfo {

	private String txnid;
	private String refund_amount;
	private String phone;
	private String key;
	private String hash;
	private String amount;
	private String email;
}
