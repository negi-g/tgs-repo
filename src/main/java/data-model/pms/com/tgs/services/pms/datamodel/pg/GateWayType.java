package com.tgs.services.pms.datamodel.pg;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.pms.datamodel.EasyPayMerchantInfo;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.pg.credimax.CredimaxMerchantInfo;
import com.tgs.services.pms.datamodel.pg.easebuzz.EasebuzzMerchantInfo;
import com.tgs.services.pms.datamodel.pg.hdfcCcavenue.HdfcCcavenueMerchantInfo;
import com.tgs.services.pms.datamodel.pg.payu.PayUMerchantInfo;
import com.tgs.services.pms.datamodel.pg.razorpaybasic.RazorpayBasicMerchantInfo;
import com.tgs.services.pms.datamodel.pg.telr.TelrMerchantInfo;
import com.tgs.services.pms.ruleengine.PaymentGatewayConfigInfo;
import lombok.Getter;

@Getter
public enum GateWayType {

	EASYPAY_ICICI {

		@Override
		public EasyPayMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {

			String returnUrl = host.append("/pg/v1/eazypay_rurl").toString();
			String externalPgHost = isprod ? "eazypay.icicibank.com/" : "eazypayuat.icicibank.com/";
			return EasyPayMerchantInfo.builder()
					.trackingUrl(
							new StringBuilder("https://").append(externalPgHost).append("/EazyPGVerify").toString())
					.returnurl(returnUrl)
					.refundURL(new StringBuilder("https://").append(externalPgHost)
							.append("OnlineRefundService/rest/OnlineRefundService/OnlineRefundDetails").toString())
					.gatewayURL(new StringBuilder("https://").append(externalPgHost).append("EazyPG").toString())
					.gatewayType("EASYPAY_ICICI").requestType("GET").build();
		}

		@Override
		public Class<EasyPayMerchantInfo> getMerchantInfoClass() {
			return EasyPayMerchantInfo.class;
		}


	},
	ATOM_PAY {

		@Override
		public AtomMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {
			String returnUrl = host.append("/pg/v1/response_callback/atom").toString();
			String externalPgHost = isprod ? "payment.atomtech.in/" : "paynetzuat.atomtech.in/";
			return AtomMerchantInfo.builder()
					.trackingUrl(new StringBuilder("https://").append(externalPgHost).append("paynetz/vfts").toString())
					.ru(returnUrl)
					.refundURL(new StringBuilder("https://").append(externalPgHost).append("paynetz/rfts").toString())
					.gatewayURL(
							new StringBuilder("https://").append(externalPgHost).append("paynetz/epi/fts").toString())
					.gatewayType("ATOM_PAY").requestType("GET").ttype("NBFundTransfer").build();
		}

		@Override
		public Class<AtomMerchantInfo> getMerchantInfoClass() {
			return AtomMerchantInfo.class;
		}

	},
	HDFC_FSSNET {

		@Override
		public HdfcMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {
			String returnUrl = host.append("/pg/v1/response_callback/hdfc").toString();
			String externalPgHost = isprod ? "securepgtest.fssnet.co.in/" : "securepgtest.fssnet.co.in/";
			return HdfcMerchantInfo.builder().responseURL(returnUrl).errorURL(returnUrl)
					.gatewayURL(new StringBuilder("https://").append(externalPgHost)
							.append("ipayb/servlet/PaymentInitHTTPServlet").toString())
					.gatewayType("HDFC_FSSNET").build();
		}

		@Override
		public Class<HdfcMerchantInfo> getMerchantInfoClass() {
			return HdfcMerchantInfo.class;
		}

	},
	RAZOR_PAY_BASIC {

		@Override
		public RazorpayBasicMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {
			String returnUrl = host.append("/pg/v1/response_callback/razorpaybasic").toString();
			String externalPgHost = "api.razorpay.com/";
			return RazorpayBasicMerchantInfo.builder().callback_url(returnUrl)
					.gatewayURL(new StringBuilder("https://").append(externalPgHost).append("v1/checkout/embedded")
							.toString())
					.requestType("POST").gatewayType("RAZOR_PAY_BASIC").name("Razorpay Basic").build();
		}

		@Override
		public Class<RazorpayBasicMerchantInfo> getMerchantInfoClass() {
			return RazorpayBasicMerchantInfo.class;
		}

	},
	RAZOR_PAY {

		@Override
		public RazorPayMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {
			String returnUrl = host.append("/pg/v1/response_callback/razorpay").toString();
			return RazorPayMerchantInfo.builder().callback_url(returnUrl).gatewayType("RAZOR_PAY").build();
		}

		@Override
		public Class<RazorPayMerchantInfo> getMerchantInfoClass() {
			return RazorPayMerchantInfo.class;
		}

	},
	PAYU {
		@Override
		public PayUMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {
			String returnUrl = host.append("/pg/v1/response_callback/payu").toString();
			String trackExternalPgHost = isprod ? "info.payu.in/" : "test.payu.in/";
			String externalPgHost = "secure.payu.in/";
			return PayUMerchantInfo.builder().gatewayType("PAYU").requestType("POST")
					.refundURL(new StringBuilder("https://").append(trackExternalPgHost)
							.append("merchant/postservice.php").toString())
					.trackingUrl(new StringBuilder("https://").append(trackExternalPgHost)
							.append("merchant/postservice.php").toString())
					.furl(returnUrl).surl(returnUrl)
					.gatewayURL(new StringBuilder("https://").append(externalPgHost).append("_payment").toString())
					.build();
		}

		@Override
		public Class<PayUMerchantInfo> getMerchantInfoClass() {
			return PayUMerchantInfo.class;
		}

	},

	EASEBUZZ {

		@Override
		public EasebuzzMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {
			String returnUrl = host.append("/pg/v1/response_callback/easebuzz").toString();
			String externalPgHost = "pay.easebuzz.in/";
			String trackExternalPgHost = "dashboard.easebuzz.in/";
			return EasebuzzMerchantInfo.builder()
					.trackingUrl(new StringBuilder("https://")
							.append(trackExternalPgHost).append("transaction/v1/retrieve").toString())
					.surl(returnUrl).furl(returnUrl)
					.initiateRedirectionURL(new StringBuilder("https://").append(externalPgHost)
							.append("payment/initiateLink").toString())
					.refundURL(new StringBuilder("https://").append(trackExternalPgHost).append("transaction/v1/refund")
							.toString())
					.gatewayURL(new StringBuilder("https://").append(externalPgHost).append("pay").toString()).build();
		}

		@Override
		public Class<EasebuzzMerchantInfo> getMerchantInfoClass() {
			return EasebuzzMerchantInfo.class;
		}

	},

	CREDIMAX {

		@Override
		public CredimaxMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {
			String returnUrl = host.append("/pg/v1/response_callback/credimax").toString();
			String externalPgHost = "credimax.gateway.mastercard.com/";
			return CredimaxMerchantInfo.builder().callback_url(returnUrl)
					.baseUrl(new StringBuilder("https://").append(externalPgHost).append("api/rest/version/57/merchant")
							.toString())
					.apiOperation("CREATE_CHECKOUT_SESSION").interactionOperation("AUTHORIZE").gatewayType("CREDIMAX")
					.build();
		}

		@Override
		public Class<CredimaxMerchantInfo> getMerchantInfoClass() {
			return CredimaxMerchantInfo.class;
		}

	},
	ATOM_AES {

		@Override
		public AtomMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {
			String returnUrl = host.append("/pg/v1/response_callback/atom_aes").toString();
			String externalPgHost = isprod ? "payment.atomtech.in/" : "paynetzuat.atomtech.in/";
			return AtomMerchantInfo.builder()
					.trackingUrl(
							new StringBuilder("https://").append(externalPgHost).append("paynetz/vftsv2").toString())
					.ru(returnUrl)
					.refundURL(new StringBuilder("https://").append(externalPgHost).append("paynetz/rfts").toString())
					.gatewayURL(
							new StringBuilder("https://").append(externalPgHost).append("paynetz/epi/fts").toString())
					.requestType("GET").ttype("NBFundTransfer").gatewayType("ATOM_AES").build();
		}

		@Override
		public Class<AtomMerchantInfo> getMerchantInfoClass() {
			return AtomMerchantInfo.class;
		}

	},
	HDFC_CCAVENUE {
		@Override
		public HdfcCcavenueMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {
			String returnUrl = host.append("/pg/v1/response_callback/hdfc_ccavenue").toString();
			String trackExternalPgHost = mediumConfig.getGateWayInfo().getGatewaydomain() != null
					? mediumConfig.getGateWayInfo().getGatewaydomain()
					: (isprod ? "https://login.ccavenue.com/" : "https://logintest.ccavenue.com/");
			String externalPgHost = mediumConfig.getGateWayInfo().getGatewaydomain2() != null
					? mediumConfig.getGateWayInfo().getGatewaydomain2()
					: (isprod ? "https://secure.ccavenue.com/" : "https://test.ccavenue.com/");
			return HdfcCcavenueMerchantInfo.builder()
					.trackingUrl(new StringBuilder(trackExternalPgHost).append("apis/servlet/DoWebTrans").toString())
					.redirect_url(returnUrl).cancel_url(returnUrl)
					.refundURL(new StringBuilder(trackExternalPgHost).append("apis/servlet/DoWebTrans").toString())
					.gatewayURL(new StringBuilder(externalPgHost).append("transaction/transaction.do").toString())
					.gatewayType("HDFC_CCAVENUE").requestType("GET").version("1.2").build();
		}

		@Override
		public Class<HdfcCcavenueMerchantInfo> getMerchantInfoClass() {
			return HdfcCcavenueMerchantInfo.class;
		}

	},
	TELR {
		@Override
		public TelrMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
				PaymentGatewayConfigInfo mediumConfig) {
			String returnUrl = host.append("/pg/v1/response_callback/telr").toString();
			String externalPgHost = "secure.telr.com/gateway/";
			return TelrMerchantInfo.builder().returnUrl(returnUrl).transClass("ecom")
					.url(new StringBuilder("https://").append(externalPgHost).append("order.json").toString())
					.gatewayType("TELR").requestType("GET")
					.refundURL(new StringBuilder("https://").append(externalPgHost).append("remote.html").toString())
					.build();
		}

		@Override
		public Class<TelrMerchantInfo> getMerchantInfoClass() {
			return TelrMerchantInfo.class;
		}

	};

	public String getName() {
		return this.name();
	}

	public String getCode() {
		return this.name();
	}

	public static List<String> getNames(List<GateWayType> gatewayTypes) {
		List<String> list = new ArrayList<>();
		for (GateWayType gw : gatewayTypes) {
			list.add(gw.getName());
		}
		return list;
	}

	public static GateWayType getGateWayType(String gateWayName) {
		for (GateWayType gw : GateWayType.values()) {
			if (gw.name().equals(gateWayName)) {
				return gw;
			}
		}
		return null;
	}

	public abstract GatewayMerchantInfo getGatewayInfo(StringBuilder host, Boolean isprod,
			PaymentGatewayConfigInfo mediumConfig);

	public abstract Class<?> getMerchantInfoClass();

}
