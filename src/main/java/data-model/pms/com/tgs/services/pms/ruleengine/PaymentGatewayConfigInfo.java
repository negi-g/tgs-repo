
package com.tgs.services.pms.ruleengine;

import java.time.LocalDateTime;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.pms.datamodel.EasyPayMerchantInfo;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.pg.AtomMerchantInfo;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.HdfcMerchantInfo;
import com.tgs.services.pms.datamodel.pg.RazorPayMerchantInfo;
import com.tgs.services.pms.datamodel.pg.credimax.CredimaxMerchantInfo;
import com.tgs.services.pms.datamodel.pg.easebuzz.EasebuzzMerchantInfo;
import com.tgs.services.pms.datamodel.pg.hdfcCcavenue.HdfcCcavenueMerchantInfo;
import com.tgs.services.pms.datamodel.pg.payu.PayUMerchantInfo;
import com.tgs.services.pms.datamodel.pg.razorpaybasic.RazorpayBasicMerchantInfo;
import com.tgs.services.pms.datamodel.pg.telr.TelrMerchantInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentGatewayConfigInfo {
	
	private Integer id;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;
	
	private GateWayType gatewayType;
	
	private String name;
	
	private Boolean enabled;
	
	private Boolean isDeleted;
	
	private Boolean isHidden;
	
	private String termsAndConditions;
	
	@GsonRunTimeAdaptorRequired(dependOn = "gatewayType")
	@GsonPolymorphismMapping({ @ClassType(keys = { "EASYPAY_ICICI" }, value = EasyPayMerchantInfo.class),
			@ClassType(keys = { "ATOM_PAY" }, value = AtomMerchantInfo.class),
			@ClassType(keys = { "HDFC_FSSNET" }, value = HdfcMerchantInfo.class),
			@ClassType(keys = { "RAZOR_PAY" }, value = RazorPayMerchantInfo.class),
			@ClassType(keys = { "PAYU" }, value = PayUMerchantInfo.class),
			@ClassType(keys = { "EASEBUZZ" }, value = EasebuzzMerchantInfo.class),
			@ClassType(keys = { "RAZOR_PAY_BASIC" }, value = RazorpayBasicMerchantInfo.class),
			@ClassType(keys = { "CREDIMAX" }, value = CredimaxMerchantInfo.class),
			@ClassType(keys = {"ATOM_AES"}, value = AtomMerchantInfo.class),
			@ClassType(keys = { "HDFC_CCAVENUE"} , value = HdfcCcavenueMerchantInfo.class),
			@ClassType(keys = { "TELR"} , value = TelrMerchantInfo.class)})
	private GatewayMerchantInfo gateWayInfo;
}
