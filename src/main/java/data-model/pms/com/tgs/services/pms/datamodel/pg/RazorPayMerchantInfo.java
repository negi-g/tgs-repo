package com.tgs.services.pms.datamodel.pg;

import org.apache.commons.lang3.StringUtils;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class RazorPayMerchantInfo extends GatewayMerchantInfo {

	private String key;

	private String amount;
	private String currency;
	private String order_id;
	private String callback_url;
	private String secret_key;

	public void appendOrderIdToCallBack(String orderId) {
		callback_url = StringUtils.stripEnd(callback_url, "/");
		callback_url = callback_url + "/" + orderId;
	}
}
