package com.tgs.services.pms.datamodel.pg.payu;

import java.util.Map;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class PayUVerificationResponse {

	@SerializedName("transaction_details")
	private Map<String, PayUTransactionDetails> transactionDetails;

}
