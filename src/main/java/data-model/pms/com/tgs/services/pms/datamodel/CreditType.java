package com.tgs.services.pms.datamodel;

import lombok.Getter;

@Getter
public enum CreditType {

    REVOLVING("R"),
    NON_REVOLVING("NR");

    private String code;

    CreditType(String code) {
        this.code = code;
    }

    public static CreditType getEnumFromCode(String code) {
        return getCreditType(code);
    }

    public static CreditType getCreditType(String code) {
        for (CreditType creditType : CreditType.values()) {
            if (creditType.getCode().equals(code)) {
                return creditType;
            }
        }
        return null;
    }
}
