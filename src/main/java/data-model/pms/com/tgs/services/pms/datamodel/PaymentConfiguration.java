package com.tgs.services.pms.datamodel;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentConfiguration extends DataModel {
	
	@ApiModelProperty(notes = "To fetch based on id ", example = "1")
	@SearchPredicate(type = PredicateType.EQUAL)
	private Long id;
	
	@ApiModelProperty(notes = "To fetch based on payment medium ", example = "EXTERNAL_PAYMENT_MEDIUM")
	@SearchPredicate(type = PredicateType.EQUAL)
	private PaymentRuleType ruleType;
	
	@ApiModelProperty(notes = "To fetch based on is deleted rule", example = "TRUE")
	@SearchPredicate(type = PredicateType.EQUAL)
	private boolean isDeleted;

}
