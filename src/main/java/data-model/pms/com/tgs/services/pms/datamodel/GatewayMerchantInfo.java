package com.tgs.services.pms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class GatewayMerchantInfo {

	private String gatewayURL;
	private String requestType;

	// @Exclude
	private String refundURL;
	private String gatewayType;

	private String gatewaydomain;
	private String gatewaydomain2;


}
