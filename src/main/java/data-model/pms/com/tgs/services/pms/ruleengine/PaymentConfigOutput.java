
package com.tgs.services.pms.ruleengine;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.PaymentMedium;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentConfigOutput extends PaymentConfigurationRuleOutput {

	@SerializedName("dn")
	private String displayName;
	
	private String subTypeDn;
	
	@SerializedName("tnc")
	private String termsAndConditions;

	@SerializedName("pfe")
	private String paymentFeeExpression;
	
	private List<PaymentMedium> paymentMediums;
}
