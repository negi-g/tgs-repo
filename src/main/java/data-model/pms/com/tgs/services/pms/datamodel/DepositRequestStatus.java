package com.tgs.services.pms.datamodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.Getter;

@Getter
public enum DepositRequestStatus {
	SUBMITTED("S") {
		@Override
		public Set<DepositRequestStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(ABORTED, PROCESSING, PAYMENT_NOT_RECEIVED, PAYMENT_RECEIVED, ACCEPTED, REJECTED));
		}
	}, 
	ABORTED("AB") {
		@Override
		public Set<DepositRequestStatus> nextStatusSet() {
			return new HashSet<>();
		}
	}, 
	PROCESSING("P") {
		@Override
		public Set<DepositRequestStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(ABORTED, ACCEPTED, REJECTED, PAYMENT_NOT_RECEIVED, PAYMENT_RECEIVED));
		}
	}, 
	PAYMENT_NOT_RECEIVED("PNR") {
		@Override
		public Set<DepositRequestStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(ABORTED, PROCESSING, PAYMENT_RECEIVED, ACCEPTED, REJECTED));
		}
	}, 
	PAYMENT_RECEIVED("PR") {
		@Override
		public Set<DepositRequestStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(ABORTED, PROCESSING, PAYMENT_NOT_RECEIVED, ACCEPTED, REJECTED));
		}
	}, 
	ACCEPTED("A") {
		@Override
		public Set<DepositRequestStatus> nextStatusSet() {
			return new HashSet<>();
		}
	}, 
	REJECTED("R") {
		@Override
		public Set<DepositRequestStatus> nextStatusSet() {
			return new HashSet<>();
		}
	};

	private String code;

	private DepositRequestStatus(String code) {
		this.code = code;
	}

	public static DepositRequestStatus getEnumFromCode(String code) {
		return getDepositStatus(code);
	}

	private static DepositRequestStatus getDepositStatus(String code) {
		for (DepositRequestStatus status : DepositRequestStatus.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}

	public static List<String> getCodes(List<DepositRequestStatus> depReqStatuses) {
		List<String> codes = new ArrayList<>();
		depReqStatuses.forEach(depReqStatus -> {
			codes.add(depReqStatus.getCode());
		});
		return codes;
	}
	
	public abstract Set<DepositRequestStatus> nextStatusSet();

}
