package com.tgs.services.gms.datamodel.Incidence;

import com.google.common.reflect.TypeToken;
import lombok.Getter;

@Getter
public enum IncidenceType {

    BILL_DUE_NOTIFICATION(new TypeToken<BillDueNotification>() {}),
    CREDIT_DI(new TypeToken<CreditDI>() {}),
    PARTIAL_BILL_PAY(new TypeToken<PartialBillPayment>() {}),
    UNLOCK_CREDIT(new TypeToken<AutoUnlockCredit>() {}),
    LOCK_CREDIT(new TypeToken<LockCredit>() {});

    private TypeToken<? extends IncidenceData> outPutTypeToken;

    <RT extends IncidenceData> IncidenceType(TypeToken<RT> outPutTypeToken) {
        this.outPutTypeToken = outPutTypeToken;
    }

    public static IncidenceType getEnumFromCode(String code) {
        return getIncidenceType(code);
    }

    private static IncidenceType getIncidenceType(String code) {
        for (IncidenceType incidenceType : IncidenceType.values()) {
            if (incidenceType.getCode().equals(code)) {
                return incidenceType;
            }
        }
        return null;
    }

    public String getCode() {
        return this.name();
    }
}
