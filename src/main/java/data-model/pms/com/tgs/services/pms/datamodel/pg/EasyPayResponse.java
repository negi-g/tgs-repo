package com.tgs.services.pms.datamodel.pg;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class EasyPayResponse extends PgResponse {

	@SerializedName("Response Code")
	private String responseCode;

	@SerializedName("ReferenceNo")
	private String referenceNo;

	@SerializedName("Unique Ref Number")
	private String uniqueRefId;

	@SerializedName("Service Tax Amount")
	private String serviceTax;

	@SerializedName("Processing Fee Amount")
	private String processingFee;

	@SerializedName("Total Amount")
	private String totalAmount;

	@SerializedName("Transaction Amount")
	private String transactionAmount;

	@SerializedName("Transaction Date")
	private String transactionDate;

	@SerializedName("Interchange Value")
	private String interchangeValue;

	@SerializedName("Payment Mode")
	private String paymentMode;

	@SerializedName("ID")
	private String merchantId;

	@SerializedName("TDR")
	private String tdr;

	@SerializedName("TPS")
	private String tps;

	@SerializedName("RS")
	private String rs;

	@SerializedName("RSV")
	private String rsv;

	@Override
	public String getRefId() {
		return referenceNo;
	}


}
