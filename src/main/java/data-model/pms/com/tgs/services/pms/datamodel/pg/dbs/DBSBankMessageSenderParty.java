package com.tgs.services.pms.datamodel.pg.dbs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DBSBankMessageSenderParty {

	private String name;
	private String accountNo;
}
