package com.tgs.services.pms.datamodel;

import java.time.LocalDateTime;
import java.util.List;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.gson.JsonDoubleDefaultSerializer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneyExchangeInfo {

	private Long id;

	@SearchPredicate(type = PredicateType.IN, filterName = "types")	
	@SearchPredicate(type = PredicateType.EQUAL)
	private String type;

	@SearchPredicate(type = PredicateType.EQUAL)
	private String fromCurrency;

	@SearchPredicate(type = PredicateType.EQUAL)
	private String toCurrency;

	@JsonAdapter(JsonDoubleDefaultSerializer.class)
	private Double exchangeRate;

	private String markup;

	@SearchPredicate(type = PredicateType.EQUAL)
	private Boolean isEnabled;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;
	
	private List<String> types;

}
