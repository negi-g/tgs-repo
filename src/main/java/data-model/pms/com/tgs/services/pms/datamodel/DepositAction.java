package com.tgs.services.pms.datamodel;

public enum DepositAction {

	ASSIGN, ASSIGNME, ABORT, PROCESS, UPDATE;

}
