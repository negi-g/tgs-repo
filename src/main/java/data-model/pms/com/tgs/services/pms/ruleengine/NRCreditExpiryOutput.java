package com.tgs.services.pms.ruleengine;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class NRCreditExpiryOutput extends PaymentConfigurationRuleOutput {

    @SerializedName("cExp")
    private CustomisedDateTime creditExpiry;

    @SerializedName("diExp")
    private CustomisedDateTime dIExpiry;

}
