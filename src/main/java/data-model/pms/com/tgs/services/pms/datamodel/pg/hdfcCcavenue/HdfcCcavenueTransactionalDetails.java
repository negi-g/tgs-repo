package com.tgs.services.pms.datamodel.pg.hdfcCcavenue;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HdfcCcavenueTransactionalDetails {
	
	private String reference_no;
	
	private String order_no;
	
	private String order_currncy;
	
	private String order_amt;
	
	private String order_status;
	
	private String order_fraud_status;
	
	private String order_bank_response;
	
	private String error_desc;
	
	private String status;
	
	private String error_code;
	
	private String enc_error_code;
	
	private String enc_response;
	
}