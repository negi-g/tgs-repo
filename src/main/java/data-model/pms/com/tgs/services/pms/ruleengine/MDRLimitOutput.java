package com.tgs.services.pms.ruleengine;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class MDRLimitOutput extends PaymentConfigurationRuleOutput {

	@SerializedName("min")
	private double minAmount;

	@SerializedName("max")
	private double maxAmount;
}
