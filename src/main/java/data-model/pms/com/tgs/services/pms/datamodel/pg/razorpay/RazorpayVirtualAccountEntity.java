package com.tgs.services.pms.datamodel.pg.razorpay;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RazorpayVirtualAccountEntity {

	private String name;

	private String entity;

	private String status;

	private String description;

	private Double amount_expected;

//	private List<String> notes;

	private Double amount_paid;

	private String customer_id;

	private Long created_at;

	private List<RazorpayReceiver> receivers;
}
