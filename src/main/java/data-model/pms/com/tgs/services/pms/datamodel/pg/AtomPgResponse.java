package com.tgs.services.pms.datamodel.pg;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class AtomPgResponse extends PgResponse {

	@SerializedName("mer_txn")
	private String refId;

	@SerializedName("merchant_id")
	private String merchantId;

	private String encdata;

	private String login;

	@SerializedName("mmp_txn")
	private String atomTxnId;

	@SerializedName("surcharge")
	private double atomPaymentFee;

	@SerializedName("f_code")
	private String f_code;

	@SerializedName("prod")
	private String prod;

	@SerializedName("bank_name")
	private String bankName;

	@SerializedName("desc")
	private String desc;

	@SerializedName("discriminator")
	private String discriminator;

	@SerializedName("amt")
	private String amt;

	@SerializedName("bank_txn")
	private String bank_txn;

	@SerializedName("signature")
	private String signature;

	@Override
	public String getRefId() {
		return refId;
	}

	// mmp_txn, mer_txn, f_code, prod, discriminator, amt, bank_txn
	public String getPlainSignature() {
		return new StringBuilder(atomTxnId).append(refId).append(f_code).append(prod).append(discriminator).append(amt)
				.append(bank_txn).toString();
	}
}
