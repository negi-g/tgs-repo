package com.tgs.services.pms.datamodel.notification;

import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class PaymentMessageAttributes extends EmailAttributes {
	
	private String userId;

	private String referenceId;

	private String merchantTxnId;
	
	private String paidUsing;
	
	private String bank;

	private String amount;
	
	private String paymentFee;
	
	private String type;
	
	private String status;

	private String processed;
	
	private String failureReason;
}
