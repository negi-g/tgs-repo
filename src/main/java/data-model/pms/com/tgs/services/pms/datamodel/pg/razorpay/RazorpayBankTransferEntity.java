package com.tgs.services.pms.datamodel.pg.razorpay;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RazorpayBankTransferEntity {

	private String id;

	private String entity;

	private String payment_id;

	private String mode;

	private String bank_reference;

	private double amount;

	private RazorpayPayerBankAccount payer_bank_account;

	private String virtual_account_id;
}
