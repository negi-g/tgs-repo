package com.tgs.services.gms.datamodel.Incidence;

import com.tgs.services.pms.datamodel.CreditLockAction;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LockCredit implements IncidenceData {

    private CreditLockAction lockAction;
}
