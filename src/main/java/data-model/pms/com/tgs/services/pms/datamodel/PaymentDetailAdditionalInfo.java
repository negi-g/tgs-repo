package com.tgs.services.pms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentDetailAdditionalInfo {
	private long tds;
}
