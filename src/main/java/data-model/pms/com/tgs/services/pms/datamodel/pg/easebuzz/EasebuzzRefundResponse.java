package com.tgs.services.pms.datamodel.pg.easebuzz;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class EasebuzzRefundResponse {

	@SerializedName("easebuzz_id")
	private String easebuzzTxnId;

	@SerializedName("refund_amount")
	private String amount;
	
	@SerializedName("refund_id")
	private String refundId;
	
	private String reason;
	
	private  Boolean status;

}
