package com.tgs.services.pms.datamodel;

import java.time.LocalDate;

import com.tgs.services.base.datamodel.DataModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cycle extends DataModel {

    private int id;
    private LocalDate start;
    private LocalDate end;
    private LocalDate lock;
    private LocalDate due;

}
