package com.tgs.services.pms.datamodel;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.DocumentType;
import com.tgs.services.base.datamodel.Product;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CreditPolicy extends DataModel {

	@NotNull
	private String policyId;

	private Long creditLimit;

	private List<Product> products;

	@NotNull
	private CreditBillCycleType billCycleType;

	@NotNull
	@Valid
	private CreditAdditionalInfo additionalInfo;

	private LocalDateTime createdOn;

	private List<DocumentType> documents;

	private Long id;

	public CreditAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null)
			additionalInfo = CreditAdditionalInfo.builder().build();
		return additionalInfo;
	}
}
