package com.tgs.services.pms.datamodel;

import com.tgs.services.base.datamodel.EmailAttributes;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class FundEmailAttributes extends EmailAttributes {

	private String bookingId;

	private String refId;

	private String incentivePercentage;

	private String incentiveAmount;

	private String usableBalance;

	private String previousBalance;

	private String processedAmount;

	private String dueDate;

	private String lockDate;

}
