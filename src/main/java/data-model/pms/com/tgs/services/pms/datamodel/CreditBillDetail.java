package com.tgs.services.pms.datamodel;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CreditBillDetail extends CreditBill {

    private CreditLine creditLine;

    private List<Payment> chargedPayments;

    private List<Payment> settlementPayments;

    CreditBillDetail(String billNumber, String userId, Long creditId, BillAdditionalInfo additionalInfo, LocalDateTime createdOn, LocalDateTime processedOn, LocalDateTime paymentDueDate, LocalDateTime lockDate, BigDecimal billAmount, BigDecimal settledAmount, Boolean isSettled, Long daysSinceDueDate, Integer extensionCount, Long id, String salesUserId, String phone) {
        super(billNumber, userId, creditId, additionalInfo, createdOn, processedOn, paymentDueDate, lockDate, billAmount, settledAmount, isSettled, daysSinceDueDate, extensionCount, id, salesUserId, phone);
    }

    public List<Payment> getChargedPayments() {
        if (chargedPayments == null) return new ArrayList<>();
        return chargedPayments;
    }

    public List<Payment> getSettlementPayments() {
        if (settlementPayments == null) return new ArrayList<>();
        return settlementPayments;
    }
}
