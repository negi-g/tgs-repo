package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.enums.PointsType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserPoint extends DataModel {

	private Long id;

	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	private String userId;

	@SearchPredicate(type = PredicateType.IN)
	private PointsType type;

	private BigDecimal balance;

	private LocalDateTime processedOn;

	public BigDecimal getBalance() {
		return balance == null ? BigDecimal.ZERO : balance;
	}
}
