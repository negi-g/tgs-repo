package com.tgs.services.pms.datamodel;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

@Getter
@Setter
public class DIRequest {

    private Boolean diProcessed;

    private BigDecimal di;

    private String comment;

    private String rc;

    private String dh;

    public String getRc() {
        return StringUtils.isBlank(rc) ? null : rc;
    }

    public String getDh() {
        return StringUtils.isBlank(dh) ? null : dh;
    }

    public BigDecimal getDi() {
        return di == null ? BigDecimal.ZERO : di;
    }
}
