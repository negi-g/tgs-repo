package com.tgs.services.pms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Builder
public class BillAdditionalInfo {

    private CreditType creditType;

    private Set<Long> chargedPaymentIds;

    private Set<Long> settlementPaymentIds;

    private boolean dueDateNotificationSent;

    private boolean isThresholdSettled;

    private boolean bpnSent;

    private String policy;

    private LocalDateTime settledDate;

    private LocalDateTime originalDueDate;

    private LocalDateTime originalLockDate;

    private LocalDateTime cycleStart;

    private LocalDateTime cycleEnd;

    private LocalDateTime extensionRecallTime;

    private BigDecimal extensionAmount;

    public Set<Long> getSettlementPaymentIds() {
        if (settlementPaymentIds == null) settlementPaymentIds = new HashSet<>();
        return settlementPaymentIds;
    }

    public Set<Long> getChargedPaymentIds() {
        if (chargedPaymentIds == null) chargedPaymentIds = new HashSet<>();
        return chargedPaymentIds;
    }

    public BigDecimal getExtensionAmount() {
        return extensionAmount == null ? BigDecimal.ZERO : extensionAmount;
    }
}
