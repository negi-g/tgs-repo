package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserWallet extends DataModel {

	private Long id;

	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	@UserId
	private String userId;

	private BigDecimal balance;

	private LocalDateTime processedOn;
	
	private WalletStatus status;
	
	private String comments;

	public BigDecimal getBalance() {
		return balance == null ? BigDecimal.ZERO : balance;
	}
}
