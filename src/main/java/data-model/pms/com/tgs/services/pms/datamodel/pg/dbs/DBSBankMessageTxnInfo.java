package com.tgs.services.pms.datamodel.pg.dbs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DBSBankMessageTxnInfo {
	
	private String txnType;
	private String customerReference;
	private String txnRefId;
	private String txnDate;
	private String valueDt;
	private DBSBankMessageReceivingParty receivingParty;
	private DBSBankMessageAmtDtls amtDtls;
	private DBSBankMessageSenderParty senderParty;
	private DBSBankMessageRmtInf rmtInf;
}
