package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentProcessingResult {

	private PaymentStatus status;

	@Setter(lombok.AccessLevel.NONE)
	private ExternalPaymentInfo externalPaymentInfo;

	public ExternalPaymentInfo getExternalPaymentInfo() {
		if (externalPaymentInfo == null) {
			externalPaymentInfo = new ExternalPaymentInfo();
		}
		return externalPaymentInfo;
	}

	@Getter
	@Setter
	@NoArgsConstructor(access = lombok.AccessLevel.PRIVATE)
	public static class ExternalPaymentInfo {
		private String gatewayComment;
		private String gatewayStatusCode;
		private String merchantTxnId;
		private BigDecimal gatewayPaymentFee;
		private BigDecimal totalAmount;
	}
}
