package com.tgs.services.pms.datamodel;

import lombok.Getter;

@Getter
public enum TemporaryExtensionType {

    BOUNDED("B"),
    UNBOUNDED("UB");

    private String code;

    TemporaryExtensionType(String val) {
        code = val;
    }

    public static TemporaryExtensionType getEnumFromCode(String code) {
        return getTemporaryExtensionType(code);
    }

    public static TemporaryExtensionType getTemporaryExtensionType(String code) {
        for (TemporaryExtensionType value : TemporaryExtensionType.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return null;
    }
}

