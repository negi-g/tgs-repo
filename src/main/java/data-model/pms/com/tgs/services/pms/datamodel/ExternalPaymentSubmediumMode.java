package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ExternalPaymentSubmediumMode {

	private BigDecimal paymentFee;

	private BigDecimal partialpaymentFee;

	private Double priority;

	private Integer ruleId;
	
	private GateWayType subType;
	
	private String subTypeDn;
	
	private String tnc;

}