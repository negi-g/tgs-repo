package com.tgs.services.pms.datamodel.pg.razorpaybasic;

import java.util.Map;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RazorpayBasicTransactionDetails {
	
	@SerializedName("error_code")
	private String errorCode;

	@SerializedName("amount")
	private Integer amount;
	
	@SerializedName("fee")
	private Integer additionalCharges;
	
	@SerializedName("tax")
	private Integer tax;
	
	@SerializedName("id")
	private String razorPayBasictxnId;
	
	@SerializedName("error_reason")
	private String errorReason;
	
	@SerializedName("error_description")
	private String errorDesc;

	private String status;
}
