package com.tgs.services.pms.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.DataModel;
import lombok.Getter;

import java.util.List;

@Getter
public class CalendarCycles extends DataModel {

    @SerializedName(value = "cycles", alternate = "bspCycles")
    private List<Cycle> cycles;
}
