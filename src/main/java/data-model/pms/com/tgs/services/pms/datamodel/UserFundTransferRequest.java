package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import lombok.Getter;

@Getter
public class UserFundTransferRequest {	

	private String sourceUserId;
	
	private String targetUserId;

	private BigDecimal amount;

	private String comments;
}
