package com.tgs.services.pms.datamodel.pg.razorpay;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RazorpayPaymentEntity {

	private String id;

	private String entity;

	private double amount;

	private String currency;

	private String status;

	private String order_id;

	private String invoice_id;

	private Boolean international;

	private String method;

	private Double amount_refunded;

	private Double amount_transferred;

	private String refund_status;

	private Boolean captured;

	private String description;

	private String card_id;

	private String bank;

	private String wallet;

	private String vpa;

	private String email;

	private String contact;

	private List<String> notes;

	private double fee;

	private double tax;

	private String error_code;

	private String error_description;

	private Long created_at;
}
