package com.tgs.services.pms.datamodel.pg;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.pms.datamodel.Payment;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class HdfcPgResponse extends PgResponse {

	@SerializedName("trackid")
	private String refId;

	private String paymentid;

	private String result;

	private String auth;

	private String ref;

	private String tranid;

	// might be "amount" - confusion in doc
	private String amt;

	private String ErrorText;

	private String ErrorNo;

	public boolean verifyResponse(Payment payment) {
		return paymentid != null && result != null && auth != null && amt != null && ref != null && refId != null
				&& tranid != null && result.equals("CAPTURED")

				&& payment.getMerchantTxnId().equals(paymentid) && payment.getRefId().equals(refId);
	}
}
