package com.tgs.services.pms.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.helper.DBExclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
public class NRCreditAdditionalInfo {

    private LocalDateTime dIExpiry;

    @SearchPredicate(type = PredicateType.IN)
    @SearchPredicate(type = PredicateType.IN, destinationEntity = "NRCredit", dbAttribute = "additionalInfo.policy", filterName = "policyIdIn")
    private String policy;

    @SerializedName("ocexp")
    private LocalDateTime originalCreditExpiry;

    @SerializedName("ocl")
    private BigDecimal originalCreditLimit;

    @SerializedName("cext")
    private BigDecimal creditExtension;

    private List<String> incidenceList;

    private String comments;

    @DBExclude
    private String refId;

    public List<String> getIncidenceList() {
        if (incidenceList == null)
            incidenceList = new ArrayList<>();
        return incidenceList;
    }
}
