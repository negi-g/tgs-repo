package com.tgs.services.pms.datamodel.pg;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EasyPayRefundResponse {

	@SerializedName("Status")
	private String status;

	@SerializedName("Message")
	private String message;

	@SerializedName("signature")
	private String signature;

	@SerializedName("ErrorMessage")
	private String errorMessage;

	@SerializedName("Total Refunded Amount")
	private String refundedAmount;

	@SerializedName("TransactionID")
	private String transactionId;

}
