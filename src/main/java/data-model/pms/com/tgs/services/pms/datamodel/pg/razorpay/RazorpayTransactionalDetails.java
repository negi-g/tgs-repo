package com.tgs.services.pms.datamodel.pg.razorpay;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RazorpayTransactionalDetails {

	@SerializedName("amount")
	private Integer amount;
	
	@SerializedName("receipt")
	private String razorPaytxnId;

	private String status;
}
