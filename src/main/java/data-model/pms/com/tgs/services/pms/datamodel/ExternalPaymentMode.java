package com.tgs.services.pms.datamodel;

import java.util.List;
import com.tgs.services.base.enums.PaymentMedium;
import lombok.Getter;

@Getter
public class ExternalPaymentMode extends PaymentMode{
	
	private List<ExternalPaymentSubmediumMode> subModes;

	public ExternalPaymentMode(PaymentMedium mode, List<ExternalPaymentSubmediumMode> subModes, String displayname) {
		super(mode, null, null, null, displayname);
		this.subModes = subModes;
	}
}
