package com.tgs.services.pms.datamodel.pg;

import lombok.Getter;

@Getter
public class RazorPayError {

    private String code;
    private String description;
    private String field;

    public RazorPayError(String description) {
        this.description = description;
    }

    public RazorPayError() {
    }
}
