package com.tgs.services.pms.datamodel.pg.payu;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class PayUTransactionDetails {

	@SerializedName("mihpayid")
	private String payUTxnId;

	@SerializedName("transaction_amount")
	private String amount;
	
	@SerializedName("additional_charges")
	private String additionalCharges;
	
	@SerializedName("error_Message")
	private String errorMessage;
	
	@SerializedName("PG_TYPE")
	private String pgType;

	private String status;
}
