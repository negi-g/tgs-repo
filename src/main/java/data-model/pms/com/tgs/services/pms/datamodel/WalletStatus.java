package com.tgs.services.pms.datamodel;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public enum WalletStatus {
	
	ACTIVE("A"), BLOCKED("B");

	private String code;

	private WalletStatus(String code) {
		this.code = code;
	}

	public String getStatus() {
		return this.getCode();
	}

	public static WalletStatus getEnumFromCode(String code) {
		return getWalletStatus(code);
	}

	public static WalletStatus getWalletStatus(String code) {
		for (WalletStatus status : WalletStatus.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}

	public static List<String> getCodes(List<WalletStatus> userStatuses) {
		List<String> statusCodes = new ArrayList<>();
		userStatuses.forEach(userStatus -> {
			statusCodes.add(userStatus.getCode());
		});
		return statusCodes;
	}
	
}
