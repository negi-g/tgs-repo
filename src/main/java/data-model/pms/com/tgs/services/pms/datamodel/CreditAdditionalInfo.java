package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.ObjectUtils;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DocumentType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CreditAdditionalInfo {

	private BigDecimal maxTemporaryExt;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT, destinationEntity = "CreditLine", dbAttribute = "additionalInfo.curTemporaryExt")
	@SearchPredicate(type = PredicateType.LT, destinationEntity = "CreditLine", dbAttribute = "additionalInfo.curTemporaryExt")
	private BigDecimal curTemporaryExt;

	private BigDecimal preApprovedOd;

	private String comments;

	private Short billCycleDays;

	private DayOfWeek billingDOW;

	private Short billingDOM;

	private Short paymentDueHours;

	private Short lockAfterHours;

	// For BSP and Calendar cycle
	private LocalDateTime lockDate;
	
	//For Calendar cycle
	private LocalDateTime dueDate;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT, destinationEntity = "CreditLine", dbAttribute = "additionalInfo.maxLimit")
	@SearchPredicate(type = PredicateType.LT, destinationEntity = "CreditLine", dbAttribute = "additionalInfo.maxLimit")
	private BigDecimal maxLimit;

	private boolean diAllowed;

	private boolean dynamicReset;

	@SerializedName("teType")
	private TemporaryExtensionType temporaryExtensionType;

	@Builder.Default
	private List<DocumentType> exemptedDocs = new ArrayList<>();

	private String approverId;

	private List<String> incidenceList;

	@SerializedName("lot")
	private LocalDateTime lastOrderTransaction;
	
	private String associatedCalendar;

	public List<String> getIncidenceList() {
		if (incidenceList == null) incidenceList = new ArrayList<>();
		return incidenceList;
	}

	public BigDecimal getCurTemporaryExt() {
		return ObjectUtils.defaultIfNull(curTemporaryExt, BigDecimal.ZERO);
	}

	public BigDecimal getMaxTemporaryExt() {
		return ObjectUtils.defaultIfNull(maxTemporaryExt, BigDecimal.ZERO);
	}

	public BigDecimal getPreApprovedOd() {
		return ObjectUtils.defaultIfNull(preApprovedOd, BigDecimal.ZERO);
	}

	public BigDecimal getMaxLimit() {
		return maxLimit == null ? BigDecimal.valueOf(Long.MAX_VALUE) : maxLimit;
	}

	public Short getPaymentDueHours() {
		return paymentDueHours == null ? 0 : paymentDueHours;
	}

	public Short getLockAfterHours() {
		return lockAfterHours == null ? 0 : lockAfterHours;
	}

	public LocalDateTime getLockDate() {
		return lockDate == null ? LocalDateTime.now().plusDays(8) : lockDate;
	}

	public TemporaryExtensionType getTemporaryExtensionType() {
		return temporaryExtensionType == null ? TemporaryExtensionType.BOUNDED : temporaryExtensionType;
	}

}
