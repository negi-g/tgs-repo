package com.tgs.services.pms.datamodel.pg.razorpay;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RazorpayBankTransfer {

	private RazorpayBankTransferEntity entity;
}
