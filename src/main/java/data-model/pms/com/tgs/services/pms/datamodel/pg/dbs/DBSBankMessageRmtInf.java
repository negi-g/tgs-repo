package com.tgs.services.pms.datamodel.pg.dbs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DBSBankMessageRmtInf {

	private String paymentDetails;
	private String addtlInf;
}
