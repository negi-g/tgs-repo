package com.tgs.services.pms.datamodel.pg.telr;

import org.apache.commons.lang3.StringUtils;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class TelrMerchantInfo extends GatewayMerchantInfo {
	private String amount;
	private String currency;
	private String storeId;
	private String returnUrl;
	private String orderAuthKey;
	private String remoteAuthKey;
	private String test;
	private String url;
	private String description;
	private String transClass;
	private String o;
	
	public void appendRefIdToCallBack(String refId) {
		returnUrl = StringUtils.stripEnd(returnUrl, "/");
		returnUrl = returnUrl + "/" + refId;
	}
	

}
