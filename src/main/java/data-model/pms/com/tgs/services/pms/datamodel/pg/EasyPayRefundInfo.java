package com.tgs.services.pms.datamodel.pg;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EasyPayRefundInfo implements RefundInfo {

	@SerializedName("MerchantId")
	private String merchantid;
	@SerializedName("inputdata")
	private String inputData;


}
