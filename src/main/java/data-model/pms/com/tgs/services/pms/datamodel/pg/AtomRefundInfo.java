package com.tgs.services.pms.datamodel.pg;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AtomRefundInfo implements RefundInfo {

	private String merchantid;
	private String pwd;
	private String atomtxnid;
	private String refundamt;
	private String txndate;
	private String merefundref;
	private String prodid;
}
