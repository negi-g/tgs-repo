package com.tgs.services.pms.datamodel.pg.telr;

import com.tgs.services.pms.datamodel.pg.PgResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class TelrPgResponse extends PgResponse{
	
	public String refId;
	
	@Override
	public String getRefId() {
		return refId;
	}

}
