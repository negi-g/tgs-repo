package com.tgs.services.gms.datamodel.Incidence;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.google.gson.Gson;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.helper.JsonStringSerializer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Incidence extends DataModel {

	private Long id;

	@SearchPredicate(type = PredicateType.IN)
	private String incidenceId;

	@SearchPredicate(type = PredicateType.LIKE)
	private String name;

	@SearchPredicate(type = PredicateType.IN)
	private IncidenceType incidenceType;

	@NotNull
	@JsonAdapter(JsonStringSerializer.class)
	private String incidenceData;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	private LocalDateTime createdOn;

	public IncidenceData getIncidenceData() {
		return new Gson().fromJson(incidenceData, incidenceType.getOutPutTypeToken().getType());
	}
}
