package com.tgs.services.pms.datamodel;

import java.time.LocalDateTime;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExternalPaymentStatusInfo {

	@Setter(AccessLevel.NONE)
	private LocalDateTime createdOn;

	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private String refId;

	private PaymentStatus status;

	private String comment;

}
