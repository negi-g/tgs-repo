package com.tgs.services.accounting.excelaccounting.air;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.datamodel.ProtectGroupColumn;
import com.tgs.services.accounting.datamodel.VarargsFunction;
import com.tgs.services.accounting.utils.AirAccountingUtils;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirCancellationProtectionOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class ProtectAirExcelAccountingStatementPreparation
		extends AbstractProtectAirExcelAccountingStatementPreparation {

	@Autowired
	private UserServiceCommunicator usCommunicator;

	@Autowired
	private GstInfoService gstService;

	@Autowired
	protected AirAccountingUtils airAccountingUtils;

	@Autowired
	protected FMSCommunicator fmsCommunicator;

	private Map<ProtectGroupColumn, CharSequence> statementParams = new HashMap<>();

	@Override
	protected Map<ProtectGroupColumn, CharSequence> getColumnValues(DbOrder order, List<DbAirOrderItem> airOrderItems,
			int serialNo) {
		statementParams.clear();

		DbOrderBilling orderBilling = gstService.findByBookingId(order.getBookingId());


		List<AirOrderItem> domainOrderItems = DbAirOrderItem.toDomainList(airOrderItems);

		final User bookingUser = usCommunicator.getUserFromCache(order.getBookingUserId());

		int paxIndex = (serialNo - 1) % airOrderItems.get(0).getTravellerInfo().size();

		final FlightTravellerInfo travellerInfo = airOrderItems.get(0).getTravellerInfo().get(paxIndex);

		// pricing for given traveller
		final Map<FareComponent, Double> fareComponents =
				AirBookingUtils.getMergedComponents(domainOrderItems, paxIndex);

		// BigDecimal behaves abnormally when used with double. Use String instead.
		final Function<Double, BigDecimal> getDoubleAsBigDecimal =
				doubleVal -> new BigDecimal(doubleVal.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final VarargsFunction<FareComponent, Double> getFareComponentsAsDouble = components -> {
			double sum = 0.0;
			for (FareComponent fareComponent : components) {
				sum += fareComponents.getOrDefault(fareComponent, 0.0);
			}
			return sum;
		};

		// Get sum of all FareComponents as BigDecimal
		final VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal = components -> {
			return getDoubleAsBigDecimal.apply(getFareComponentsAsDouble.apply(components));
		};

		setString(ProtectGroupColumn.DOC_NOS, order.getAdditionalInfo().getProtectGroupInvoiceInfo().getInvoiceId());
		setString(ProtectGroupColumn.DOC_SRNO, String.format("%03d", serialNo)); // 3-digit srno

		setString(ProtectGroupColumn.IDATE,
				airOrderItems.get(0).getCreatedOn().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

		String clientCode = bookingUser.getAdditionalInfo().getAccountingCode();
		if (StringUtils.isBlank(clientCode)) {
			clientCode = "CT01ET";
		} else {
			clientCode = "C".concat(clientCode);
		}
		setString(ProtectGroupColumn.CCODE, clientCode);

		if (UserUtils.isCorporate(bookingUser)) {
			if (orderBilling != null) {
				setString(ProtectGroupColumn.CCODE,
						(StringUtils.isNotBlank(orderBilling.getInfo().getAccountCode()))
								? "C".concat(orderBilling.getInfo().getAccountCode())
								: clientCode);
			}
		}
		if (order.getAdditionalInfo().getFirstUpdateTime() != null) {
			LocalDateTime bookingTime = order.getAdditionalInfo().getFirstUpdateTime().get("S");
			setString(ProtectGroupColumn.SRV_DATE, bookingTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		} else {
			setString(ProtectGroupColumn.SRV_DATE,
					order.getCreatedOn().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		}

		String paxName = airAccountingUtils.getPaxName(travellerInfo);
		setString(ProtectGroupColumn.PAX, paxName);

		LocalDateTime departureDate = airOrderItems.get(airOrderItems.size() - 1).getDepartureTime();
		setString(ProtectGroupColumn.NARR_3, departureDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		setString(order.getBookingId(), ProtectGroupColumn.NARRATION, ProtectGroupColumn.NARR_2);
		setBigDecimal(ProtectGroupColumn.SRV_CHRG1C, getFareComponentsAsBigDecimal.apply(FareComponent.CPMF));
		setBigDecimal(ProtectGroupColumn.BASIC_C, getBASIC_C(getFareComponentsAsBigDecimal));
		setBigDecimal(ProtectGroupColumn.BASIC_S, getBASIC_C(getFareComponentsAsBigDecimal));

		GstInfo gstInfo = orderBilling != null ? orderBilling.toDomain() : null;
		boolean isGstApplicable = OmsHelper.isIgstApplicable(bookingUser, gstInfo);
		if (isGstApplicable) {
			setBigDecimal(ProtectGroupColumn.SERV_TAXC, getFareComponentsAsBigDecimal.apply(FareComponent.CPMFT));
			setBigDecimal(ProtectGroupColumn.SERV_EDUC, BigDecimal.valueOf(0));
			setBigDecimal(ProtectGroupColumn.SERV_CS1C, BigDecimal.valueOf(0));
		} else {
			setBigDecimal(ProtectGroupColumn.SERV_TAXC, BigDecimal.valueOf(0));
			setBigDecimal(ProtectGroupColumn.SERV_EDUC, getFareComponentsAsBigDecimal.apply(FareComponent.CPMFT)
					.divide(new BigDecimal(2), 2, RoundingMode.HALF_UP));
			setBigDecimal(ProtectGroupColumn.SERV_CS1C, getFareComponentsAsBigDecimal.apply(FareComponent.CPMFT)
					.divide(new BigDecimal(2), 2, RoundingMode.HALF_UP));
		}

		// Fixed Values
		setString(ProtectGroupColumn.DOC_PRF, "BW");
		setString(ProtectGroupColumn.IDM_FLAG, "M");
		setString(ProtectGroupColumn.XO_REF, "H");
		setString(ProtectGroupColumn.LOC_CODE, "000");
		setString(ProtectGroupColumn.CST_CODE, "001");
		setString("INR", ProtectGroupColumn.Curcode_S, ProtectGroupColumn.Curcode_C);
		setString(ProtectGroupColumn.GCODE, "GI0303");
		setString(ProtectGroupColumn.SCODE, "SP001J");
		setString(ProtectGroupColumn.STX_CENVAT, "C");
		setString(ProtectGroupColumn.NARR_1, "Protect flight CXLN protection");
		setString(ProtectGroupColumn.DISC_PAIDM2, "N");
		setString("RB", ProtectGroupColumn.BROK_PAIDM1, ProtectGroupColumn.DISC_RECDM1);
		setString(ProtectGroupColumn.DISC_PAIDM1, "VL");
		setString("S", ProtectGroupColumn.SRV_CHRG3_H, ProtectGroupColumn.SRV_CHRG2_H, ProtectGroupColumn.SRV_PAIDM2);
		setString("B", ProtectGroupColumn.GST_S_M, ProtectGroupColumn.GST_C_M, ProtectGroupColumn.STX_METHOD);
		setBigDecimal(ProtectGroupColumn.SAC_CODE1, BigDecimal.valueOf(998559));
		setBigDecimal(ProtectGroupColumn.GST_C_R, BigDecimal.valueOf(0));
		setBigDecimal(ProtectGroupColumn.DISC_PAID1, getFareComponentsAsBigDecimal.apply(FareComponent.CPAC));
		BigDecimal cpCommission = getFareComponentsAsBigDecimal.apply(FareComponent.CPAC);
		BigDecimal rate = BigDecimal.ZERO;
		if (cpCommission.compareTo(BigDecimal.ZERO) > 0) {
			Long cancellationProtectionRuleId =
					domainOrderItems.get(0).getAdditionalInfo().getCancellationProtectionRuleId();
			if (cancellationProtectionRuleId != null) {
				AirConfiguratorInfo airConfigInfo = fmsCommunicator.getAirConfigRule(
						AirConfiguratorRuleType.CANCELLATION_PROTECTION, cancellationProtectionRuleId);
				AirCancellationProtectionOutput output =
						(AirCancellationProtectionOutput) airConfigInfo.getIRuleOutPut();
				String commissionRate = output.getCancellationProtectionAgentCommission();
				if (StringUtils.isNotEmpty(commissionRate)) {
					rate = BigDecimal.valueOf(Double.valueOf(commissionRate));
				}
			}
		}
		setBigDecimal(ProtectGroupColumn.DISC_PAIDV1, rate);
		setBigDecimal(ProtectGroupColumn.TDC_PAIDV1, BigDecimal.valueOf(bookingUser.getAdditionalInfo().getTdsRate()));
		setBigDecimal(ProtectGroupColumn.TDS_C, getFareComponentsAsBigDecimal.apply(FareComponent.CPACT));
		setString(ProtectGroupColumn.GST_TYPE, "G");
		setString("", ProtectGroupColumn.IL_REF, ProtectGroupColumn.VD_REF, ProtectGroupColumn.DCODE,
				ProtectGroupColumn.ECODE, ProtectGroupColumn.BCODE, ProtectGroupColumn.REFR_KEY,
				ProtectGroupColumn.XO_NOS, ProtectGroupColumn.NARR_4, ProtectGroupColumn.NARR_5,
				ProtectGroupColumn.NARR_6, ProtectGroupColumn.TAX_C, ProtectGroupColumn.TAX_S,
				ProtectGroupColumn.Created_By, ProtectGroupColumn.Created_On, ProtectGroupColumn.Pay_Type,
				ProtectGroupColumn.SCODE_B, ProtectGroupColumn.TICKETNO);

		setBigDecimal(BigDecimal.ZERO, ProtectGroupColumn.BASIC_NONACC, ProtectGroupColumn.TDS1_CRate,
				ProtectGroupColumn.TDS1_C, ProtectGroupColumn.RAF_P, ProtectGroupColumn.RAF_C,
				ProtectGroupColumn.SERV_CS1P, ProtectGroupColumn.SERV_EDUP, ProtectGroupColumn.SERV_TAXP,
				ProtectGroupColumn.GST_S_R, ProtectGroupColumn.TDS_B, ProtectGroupColumn.TDB_PAIDV1,
				ProtectGroupColumn.TDS_P, ProtectGroupColumn.TDS_PAIDV1, ProtectGroupColumn.SRV_CHRG3P,
				ProtectGroupColumn.SRV_CHRG2P, ProtectGroupColumn.SRV_CHRG1P, ProtectGroupColumn.SRV_CHRG3C,
				ProtectGroupColumn.SRV_CHRG2C, ProtectGroupColumn.BROK_PAID1, ProtectGroupColumn.BROK_PAIDV1,
				ProtectGroupColumn.DISC_RECD1, ProtectGroupColumn.DISC_RECDV1, ProtectGroupColumn.NOS_PAX_C,
				ProtectGroupColumn.NOS_PAX_I, ProtectGroupColumn.TAX_C, ProtectGroupColumn.TAX_S);
		setBigDecimal(BigDecimal.ONE, ProtectGroupColumn.R_O_E_C, ProtectGroupColumn.R_O_E_S,
				ProtectGroupColumn.NOS_PAX_A);

		return statementParams;
	}

	protected BigDecimal getBASIC_C(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.CPP, FareComponent.CPT);
	}

	protected BigDecimal getBASIC_S(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.CPP, FareComponent.CPT);
	}

	protected final void setString(ProtectGroupColumn column, String str) {
		setString(statementParams, column, str);
	}

	protected final void setBigDecimal(ProtectGroupColumn column, BigDecimal bigDecimal) {
		setBigDecimal(statementParams, column, bigDecimal);
	}

	protected final void setString(String str, ProtectGroupColumn... columns) {
		setString(statementParams, str, columns);
	}

	protected final void setBigDecimal(BigDecimal bigDecimal, ProtectGroupColumn... columns) {
		setBigDecimal(statementParams, bigDecimal, columns);
	}

	public void setString(Map<ProtectGroupColumn, CharSequence> statementParams, String str,
			ProtectGroupColumn... columns) {
		for (ProtectGroupColumn column : columns) {
			setString(statementParams, column, str);
		}
	}

	public void setString(Map<ProtectGroupColumn, CharSequence> statementParams, ProtectGroupColumn column,
			String str) {
		if (column.hasLength()) {
			str = StringUtils.left(str, column.getLength());
		}
		str = StringUtils.defaultString(str).replace("'", "''");
		statementParams.put(column, new StringBuilder("'").append(str).append("'").toString());
	}

	public void setBigDecimal(Map<ProtectGroupColumn, CharSequence> statementParams, ProtectGroupColumn column,
			BigDecimal bigDecimal) {
		statementParams.put(column, bigDecimal == null ? null : bigDecimal.toString());
	}

	public void setBigDecimal(Map<ProtectGroupColumn, CharSequence> statementParams, BigDecimal bigDecimal,
			ProtectGroupColumn... columns) {
		for (ProtectGroupColumn column : columns) {
			setBigDecimal(statementParams, column, bigDecimal);
		}

	}
}
