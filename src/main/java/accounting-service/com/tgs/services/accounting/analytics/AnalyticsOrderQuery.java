package com.tgs.services.accounting.analytics;

import com.tgs.services.base.BaseAnalyticsQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
public class AnalyticsOrderQuery extends BaseAnalyticsQuery {
	private String bookingId;
	private String pnr;
}
