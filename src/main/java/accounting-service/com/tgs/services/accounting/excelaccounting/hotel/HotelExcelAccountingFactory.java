package com.tgs.services.accounting.excelaccounting.hotel;

import java.util.List;
import org.springframework.stereotype.Component;
import com.tgs.services.base.SpringContext;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;

@Component
public class HotelExcelAccountingFactory {
	public static AbstractHotelExcelAccounting getHotelExcelAccountingFactory(DbOrder order,
			List<DbHotelOrderItem> hotelOrderItems) {
		AbstractHotelExcelAccounting prepareStatement = null;
		switch (hotelOrderItems.get(0).getSupplierId()) {
			case "EXPEDIA":
				prepareStatement = SpringContext.getApplicationContext()
						.getBean(ExpediaExcelAccountingStatementPreparation.class, order, hotelOrderItems);
				break;
			case "CLEARTRIP":
				prepareStatement = SpringContext.getApplicationContext()
						.getBean(CleartripExcelAccountingStatementPreparation.class, order, hotelOrderItems);
				break;
			case "AGODA":
				prepareStatement = SpringContext.getApplicationContext()
						.getBean(AgodaExcelAccountingStatementPreparation.class, order, hotelOrderItems);
				break;
			case "DESIYA":
				prepareStatement = SpringContext.getApplicationContext()
						.getBean(DesiyaExcelAccountingStatementPreparation.class, order, hotelOrderItems);
				break;
			default:
				prepareStatement = SpringContext.getApplicationContext()
						.getBean(DefaultExcelAccountingStatementPreparation.class, order, hotelOrderItems);

		}
		return prepareStatement;
	}
}
