package com.tgs.services.accounting.excelaccounting.hotel;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class ExpediaExcelAccountingStatementPreparation extends AbstractHotelExcelAccounting {

	public ExpediaExcelAccountingStatementPreparation(DbOrder order, List<DbHotelOrderItem> hotelOrderItems) {
		super(order, hotelOrderItems);
	}

	@Override
	protected BigDecimal getSRV_CHRG1P() {
		BigDecimal totalSupplierNetPrice = getTotalPriceFromComponenet(HotelFareComponent.SNP);
		BigDecimal totalSupplierGrossPrice = getTotalPriceFromComponenet(HotelFareComponent.SGP);
		return totalSupplierGrossPrice.subtract(totalSupplierNetPrice);
	}

	@Override
	protected BigDecimal getDISC_PAIDV1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getDISC_RECDV1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getDISC_PAID1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getDISC_RECD1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getSRV_CHRG2C() {
		BigDecimal totalNetPrice = getTotalPriceFromComponenet(HotelFareComponent.NF);
		BigDecimal totalServiceCharge = getTotalPriceFromComponenet(HotelFareComponent.MF);
		BigDecimal totalGST = getTotalPriceFromComponenet(HotelFareComponent.CGST)
				.add(getTotalPriceFromComponenet(HotelFareComponent.SGST))
				.add(getTotalPriceFromComponenet(HotelFareComponent.IGST));
		return totalNetPrice.subtract(totalServiceCharge).subtract(totalGST);
	}

	@Override
	protected BigDecimal getTDC_PAIDV1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getTDS_C() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getTDS_PAIDV1() {
		return BigDecimal.valueOf(5);
	}

	@Override
	protected BigDecimal getTDS_P() {
		return BigDecimal.ZERO;
	}

	@Override
	protected String getPay_Type() {
		return "";
	}

}
