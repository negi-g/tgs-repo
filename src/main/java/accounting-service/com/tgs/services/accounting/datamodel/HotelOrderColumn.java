package com.tgs.services.accounting.datamodel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import lombok.Getter;

@Getter
public enum HotelOrderColumn {
	DOC_PRF(2, true),
	DOC_NOS(7, true),
	DOC_SRNO(3, true),
	IDM_FLAG(1),
	IL_REF(19),
	VD_REF,
	IDATE,
	CCODE(6),
	DCODE(6),
	ECODE(6),
	BCODE(6),
	NARRATION(35),
	XO_REF(1),
	LOC_CODE(3),
	CST_CODE(3),
	Curcode_C(3),
	Curcode_S(3),
	REFR_KEY,
	GCODE,
	HCODE,
	SCODE(6),
	HOTEL_NAME(35),
	XO_NOS(9),
	TICKETNO,
	PAX,
	CHECK_IN_DATE,
	CHECK_OUT_DATE,
	ROOMVIEW,
	MEALPLAN,
	ROOMTYPE,
	TARIFFTYPE,
	PKG_CODE,
	CITY,
	ROOM_SGL_NOS,
	ROOM_SGL_PAX,
	ROOM_SGL_RATE,
	ROOM_SGL_PURMTH,
	ROOM_SGL_PURVAL,
	ROOM_DBL_NOS,
	ROOM_DBL_PAX,
	ROOM_DBL_RATE,
	ROOM_DBL_PURMTH,
	ROOM_DBL_PURVAL,
	ROOM_TWN_NOS,
	ROOM_TWN_PAX,
	ROOM_TWN_RATE,
	ROOM_TWN_PURMTH,
	ROOM_TWN_PURVAL,
	ROOM_TRP_NOS,
	ROOM_TRP_PAX,
	ROOM_TRP_RATE,
	ROOM_TRP_PURMTH,
	ROOM_TRP_PURVAL,
	ROOM_QAD_NOS,
	ROOM_QAD_PAX,
	ROOM_QAD_RATE,
	ROOM_QAD_PURMTH,
	ROOM_QAD_PURVAL,
	ROOM_ADT_NOS,
	ROOM_ADT_PAX,
	ROOM_ADT_RATE,
	ROOM_ADT_PURMTH,
	ROOM_ADT_PURVAL,
	ROOM_CHD_NOS,
	ROOM_CHD_PAX,
	ROOM_CHD_RATE,
	ROOM_CHD_PURMTH,
	ROOM_CHD_PURVAL,
	ROOM_CWB_NOS,
	ROOM_CWB_PAX,
	ROOM_CWB_RATE,
	ROOM_CWB_PURMTH,
	ROOM_CWB_PURVAL,
	ROOM_FOC_NOS,
	ROOM_FOC_PAX,
	ROOM_FOC_RATE,
	ROOM_FOC_PURMTH,
	ROOM_FOC_PURVAL,
	STX_CENVAT,
	STX_METHOD,
	NOS_PAX_A,
	NOS_PAX_C,
	NOS_PAX_I,
	NARR_1,
	NARR_2,
	NARR_3,
	NARR_4,
	NARR_5,
	NARR_6,
	R_O_E_C,
	R_O_E_S,
	BASIC_C,
	BASIC_S,
	TAX_C,
	TAX_S,
	DISC_PAIDM1,
	DISC_PAIDM2,
	DISC_RECDM1,
	BROK_PAIDM1,
	DISC_PAIDV1,
	DISC_RECDV1,
	DISC_PAID1,
	DISC_RECD1,
	BROK_PAID1,
	SRV_PAIDM2,
	SRV_CHRG1C,
	SRV_CHRG2C,
	SRV_CHRG2_H,
	SRV_CHRG3C,
	SRV_CHRG3_H,
	RAF_C,
	SRV_CHRG1P,
	SRV_CHRG2P,
	SRV_CHRG3P,
	RAF_P,
	GST_C_M,
	GST_C_R,
	SERV_TAXC,
	SERV_EDUC,
	TDC_PAIDV1,
	TDS_C,
	GST_S_M,
	GST_S_R,
	SERV_TAXP,
	SERV_EDUP,
	TDS_PAIDV1,
	TDS_P,
	TDB_PAIDV1,
	TDS_B,
	Created_By,
	Created_On,
	SERV_CS1C,
	SERV_CS2C,
	SERV1_CS1C,
	SERV1_CS2C,
	SERV2_CS1C,
	SERV2_CS2C,
	SERV3_CS1C,
	SERV3_CS2C,
	SERV_CS1P,
	SERV_CS2P,
	SERV1_CS1P,
	SERV1_CS2P,
	SERV3_CS1P,
	SERV3_CS2P,
	GST_TYPE,
	SAC_CODE1,
	Pay_Type,
	SCODE_B;


	// All columns separated by comma
	public final static String ALL;

	private int length;
	private boolean notNull;

	static {
		StringJoiner sj = new StringJoiner(", ");
		for (HotelOrderColumn column : HotelOrderColumn.values()) {
			sj.add(column.name());
		}
		ALL = sj.toString();
	}

	HotelOrderColumn() {
		this(-1);
	}

	HotelOrderColumn(int maxLength) {
		this(maxLength, false);
	}

	HotelOrderColumn(boolean notNull) {
		this(-1, notNull);
	}

	HotelOrderColumn(int length, boolean notNull) {
		this.length = length;
		this.notNull = notNull;
	}

	public boolean hasLength() {
		return length >= 0;
	}

	public static Set<HotelOrderColumn> getBlankColumns() {
		Set<HotelOrderColumn> list = new HashSet<>();

		return list;
	}

}
