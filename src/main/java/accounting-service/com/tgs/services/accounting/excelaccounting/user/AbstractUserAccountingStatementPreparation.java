package com.tgs.services.accounting.excelaccounting.user;

import java.sql.SQLException;
import java.util.Map;
import java.util.StringJoiner;
import com.tgs.services.accounting.datamodel.UserColumn;
import com.tgs.services.ums.datamodel.User;

public abstract class AbstractUserAccountingStatementPreparation {

	final private static String INSERT_SQL_TEMPLATE = "INSERT INTO %s (" + UserColumn.ALL + ") VALUES (%s);";

	public String prepareInsertSql(String tableName, User user) throws SQLException {
		Map<UserColumn, CharSequence> statementParams = getColumnValues(user);
		StringJoiner joiner = new StringJoiner(", ");
		for (UserColumn column : UserColumn.values()) {
			joiner.add(statementParams.get(column));
		}
		return String.format(INSERT_SQL_TEMPLATE, tableName, joiner.toString());
	}

	protected abstract Map<UserColumn, CharSequence> getColumnValues(User user);

}
