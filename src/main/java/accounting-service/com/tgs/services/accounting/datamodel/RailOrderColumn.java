package com.tgs.services.accounting.datamodel;

import java.util.StringJoiner;
import lombok.Getter;

@Getter
public enum RailOrderColumn {
	DOC_PRF(2, true),
	DOC_NOS(7, true),
	DOC_SRNO(3, true),
	IDM_FLAG(1),
	IL_REF(19),
	VD_REF(19),
	IDATE,
	CCODE(6),
	DCODE(6),
	ECODE(6),
	BCODE(6),
	NARRATION(35),
	XO_REF(1),
	LOC_CODE(3),
	CST_CODE(3),
	Curcode_C(3),
	Curcode_S(3),
	REFR_KEY(10),
	GCODE,
	SCODE(6),
	XO_NOS(9),
	PNR_NO(10),
	TICKETNO(11),
	PAX(30),
	SECTOR(9),
	BOOK_CLASS,
	TRAIN_NO,
	JOURNEY_DATE,
	NOS_PAX_A,
	NOS_PAX_C,
	NOS_PAX_I,
	R_O_E_C,
	R_O_E_S,
	BASIC_FARE,
	BROK_PAIDM1(2),
	BROK_PAIDV1,
	BROK_PAID1,
	SRV_CHRG1C,
	SRV_CHRG2C,
	SRV_CHRG3C,
	RAF_C,
	SRV_CHRG1P,
	SRV_CHRG2P,
	SRV_CHRG3P,
	RAF_P,
	SERV_TAXC,
	SERV_EDUC,
	SERV_TAXP,
	SERV_EDUP,
	XXL_C,
	XXL_P,
	TAX_4,
	TAX_2,
	Created_By(15),
	Created_On,
	SERV_CS1C,
	SERV_CS2C,
	SERV_CS1P,
	SERV_CS2P;


	// All columns separated by comma
	public final static String ALL;

	private int length;
	private boolean notNull;

	static {
		StringJoiner sj = new StringJoiner(", ");
		for (RailOrderColumn column : RailOrderColumn.values()) {
			sj.add(column.name());
		}
		ALL = sj.toString();
	}

	RailOrderColumn() {
		this(-1);
	}

	RailOrderColumn(int maxLength) {
		this(maxLength, false);
	}

	RailOrderColumn(boolean notNull) {
		this(-1, notNull);
	}

	RailOrderColumn(int length, boolean notNull) {
		this.length = length;
		this.notNull = notNull;
	}

	public boolean hasLength() {
		return length >= 0;
	}

}
