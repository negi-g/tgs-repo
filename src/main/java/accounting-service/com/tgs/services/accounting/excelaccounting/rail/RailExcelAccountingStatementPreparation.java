package com.tgs.services.accounting.excelaccounting.rail;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Function;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.datamodel.RailOrderColumn;
import com.tgs.services.accounting.datamodel.VarargsFunction;
import com.tgs.services.accounting.utils.RailAccountingUtils;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.gms.datamodel.InvoiceIdData;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.manager.AbstractInvoiceIdManager;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.ums.datamodel.User;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class RailExcelAccountingStatementPreparation {

	@Autowired
	private UserServiceCommunicator usCommunicator;

	@Autowired
	private AbstractInvoiceIdManager invoiceIdManager;

	@Autowired
	protected RailAccountingUtils railAccountingUtils;

	@Autowired
	protected RailCommunicator railComm;

	final private static String INSERT_SQL_TEMPLATE = "INSERT INTO %s (" + RailOrderColumn.ALL + ") VALUES (%s)";

	private Map<RailOrderColumn, CharSequence> statementParams = new HashMap<>();

	public String prepareInsertSql(String tableName, DbOrder order, DbRailOrderItem railOrderItem, int serialNo)
			throws SQLException {
		Map<RailOrderColumn, CharSequence> statementParams = getColumnValues(order, railOrderItem, serialNo);
		StringJoiner joiner = new StringJoiner(", ");
		for (RailOrderColumn column : RailOrderColumn.values()) {
			joiner.add(statementParams.get(column));
		}
		return String.format(INSERT_SQL_TEMPLATE, tableName, joiner.toString());
	}

	protected Map<RailOrderColumn, CharSequence> getColumnValues(DbOrder order, DbRailOrderItem dbRailOrderItem,
			int serialNo) {
		statementParams.clear();
		ProductMetaInfo productMetaInfo = Product.getProductMetaInfoFromId(order.getBookingId());

		Order domainOrder = order.toDomain();

		final User bookingUser = usCommunicator.getUserFromCache(order.getBookingUserId());

		RailOrderItem railOrderItem = dbRailOrderItem.toDomain();
		int paxIndex = (serialNo - 1) % railOrderItem.getTravellerInfo().size();
		final RailTravellerInfo travellerInfo = railOrderItem.getTravellerInfo().get(paxIndex);

		String invoiceId = order.getAdditionalInfo().getInvoiceId();
		if (StringUtils.isBlank(invoiceId)) {
			invoiceId = invoiceIdManager.getInvoiceIdFor(domainOrder, productMetaInfo);
			order.getAdditionalInfo().setInvoiceId(invoiceId);
		}

		// Fare needs to be pushed only for 1st pax
		final Map<RailFareComponent, Double> fareComponents =
				serialNo == 1 ? dbRailOrderItem.getAdditionalInfo().getPriceInfo().getFareComponents()
						: new HashMap<>();

		// BigDecimal behaves abnormally when used with double. Use String instead.
		final Function<Double, BigDecimal> getDoubleAsBigDecimal =
				doubleVal -> new BigDecimal(doubleVal.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final VarargsFunction<RailFareComponent, Double> getFareComponentsAsDouble = components -> {
			double sum = 0.0;
			for (RailFareComponent fareComponent : components) {
				sum += fareComponents.getOrDefault(fareComponent, 0.0);
			}
			return sum;
		};

		// Get sum of all FareComponents as BigDecimal
		final VarargsFunction<RailFareComponent, BigDecimal> getFareComponentsAsBigDecimal = components -> {
			return getDoubleAsBigDecimal.apply(getFareComponentsAsDouble.apply(components));
		};

		setString("", RailOrderColumn.IL_REF, RailOrderColumn.VD_REF, RailOrderColumn.DCODE, RailOrderColumn.ECODE,
				RailOrderColumn.BCODE, RailOrderColumn.XO_NOS, RailOrderColumn.REFR_KEY); // empty
		setString(RailOrderColumn.LOC_CODE, "000");
		setString(RailOrderColumn.CST_CODE, "002");
		setString(RailOrderColumn.GCODE, "GI03RL");
		setString(RailOrderColumn.BROK_PAIDM1, "VL");
		setString("R", RailOrderColumn.IDM_FLAG, RailOrderColumn.XO_REF);
		setString("INR", RailOrderColumn.Curcode_C, RailOrderColumn.Curcode_S);

		setBigDecimal(BigDecimal.ONE, RailOrderColumn.R_O_E_C, RailOrderColumn.R_O_E_S);
		setBigDecimal(BigDecimal.ZERO, RailOrderColumn.SRV_CHRG2P, RailOrderColumn.SRV_CHRG3P, RailOrderColumn.XXL_C,
				RailOrderColumn.XXL_P, RailOrderColumn.SERV_CS2C, RailOrderColumn.SERV_CS2P, RailOrderColumn.RAF_C,
				RailOrderColumn.SERV_TAXC, RailOrderColumn.SERV_CS1C, RailOrderColumn.SERV_CS1P,
				RailOrderColumn.BROK_PAIDV1, RailOrderColumn.BROK_PAID1, RailOrderColumn.SERV_EDUC,
				RailOrderColumn.SERV_TAXP, RailOrderColumn.SERV_EDUP);
		setString(RailOrderColumn.Created_By, "null");
		setBigDecimal(BigDecimal.ZERO, RailOrderColumn.BROK_PAIDV1, RailOrderColumn.BROK_PAID1);
		String supplierCode = railComm.getRailSourceConfiguration().getAccountCode();
		setString(RailOrderColumn.SCODE, "S".concat(supplierCode));

		final InvoiceIdData invoiceIdData = invoiceIdManager.getInvoiceIdData(invoiceId, domainOrder, productMetaInfo);
		setString(RailOrderColumn.DOC_PRF, invoiceIdData.getPrefix());
		setString(RailOrderColumn.DOC_NOS, String.valueOf(invoiceIdData.getNumericInvoiceId()));
		setString(RailOrderColumn.DOC_SRNO, String.format("%03d", serialNo)); // 3-digit srno
		setString(RailOrderColumn.IDATE,
				railOrderItem.getCreatedOn().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		String clientCode = bookingUser.getAdditionalInfo().getAccountingCode();
		if (StringUtils.isBlank(clientCode)) {
			clientCode = "CT01ET";
		} else {
			clientCode = "C".concat(clientCode);
		}
		setString(RailOrderColumn.CCODE, clientCode);
		setString(RailOrderColumn.NARRATION, order.getBookingId());
		setString(RailOrderColumn.PNR_NO, railOrderItem.getAdditionalInfo().getPnr());
		String ticketNo = new StringBuilder(railOrderItem.getAdditionalInfo().getPnr()).append(serialNo).toString();
		setString(RailOrderColumn.TICKETNO, ticketNo);
		setString(RailOrderColumn.PAX, travellerInfo.getFullName());
		setString(RailOrderColumn.SECTOR, StringUtils.leftPad(dbRailOrderItem.getFromStn(), 4, ' ').concat("/")
				.concat(StringUtils.rightPad(dbRailOrderItem.getToStn(), 4, ' ')));
		setString(RailOrderColumn.BOOK_CLASS, dbRailOrderItem.getAdditionalInfo().getJourneyClass().getCode());
		setString(RailOrderColumn.TRAIN_NO, dbRailOrderItem.getAdditionalInfo().getRailInfo().getNumber());
		LocalDateTime journeyDate = ObjectUtils.firstNonNull(railOrderItem.getAdditionalInfo().getBoardingTime(),
				railOrderItem.getDepartureTime());
		setString(RailOrderColumn.JOURNEY_DATE, journeyDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		setBigDecimal(RailOrderColumn.NOS_PAX_A, travellerInfo.getAge() > 11 ? BigDecimal.ONE : BigDecimal.ZERO);
		setBigDecimal(RailOrderColumn.NOS_PAX_C,
				travellerInfo.getAge() >= 5 && travellerInfo.getAge() <= 11 ? BigDecimal.ONE : BigDecimal.ZERO);
		setBigDecimal(RailOrderColumn.NOS_PAX_I,
				Objects.isNull(travellerInfo.getAge()) || travellerInfo.getAge() < 5 ? BigDecimal.ONE
						: BigDecimal.ZERO);
		setBigDecimal(RailOrderColumn.BASIC_FARE, getFareComponentsAsBigDecimal.apply(RailFareComponent.BF));
		setBigDecimal(RailOrderColumn.TAX_2, getFareComponentsAsBigDecimal.apply(RailFareComponent.WP)
				.add(getFareComponentsAsBigDecimal.apply(RailFareComponent.WPT)));
		setBigDecimal(RailOrderColumn.TAX_4, getFareComponentsAsBigDecimal.apply(RailFareComponent.TI)
				.add(getFareComponentsAsBigDecimal.apply(RailFareComponent.TIT)));
		setBigDecimal(RailOrderColumn.SRV_CHRG2C, getFareComponentsAsBigDecimal.apply(RailFareComponent.MF));
		setBigDecimal(RailOrderColumn.SRV_CHRG3C, getFareComponentsAsBigDecimal.apply(RailFareComponent.PC));
		setBigDecimal(RailOrderColumn.SRV_CHRG1P, BigDecimal.ZERO);
		setBigDecimal(RailOrderColumn.RAF_P, getFareComponentsAsBigDecimal.apply(RailFareComponent.RCF));
		return statementParams;
	}

	protected final void setString(RailOrderColumn column, String str) {
		railAccountingUtils.setString(statementParams, column, str);
	}

	protected final void setString(String str, RailOrderColumn... columns) {
		railAccountingUtils.setString(statementParams, str, columns);
	}

	protected final void setBigDecimal(RailOrderColumn column, BigDecimal bigDecimal) {
		railAccountingUtils.setBigDecimal(statementParams, column, bigDecimal);
	}

	protected final void setBigDecimal(BigDecimal bigDecimal, RailOrderColumn... columns) {
		railAccountingUtils.setBigDecimal(statementParams, bigDecimal, columns);
	}

}
