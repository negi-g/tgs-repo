package com.tgs.services.accounting.datamodel;

import java.util.StringJoiner;
import lombok.Getter;

@Getter
public enum SupplierOrderColumn {
	DOC_PRF(2, true),
	DOC_NOS(7, true),
	DOC_SRNO(3, true),
	IDM_FLAG(1),
	IL_REF(19),
	CCODE(6),
	DCODE(6),
	ECODE(6),
	BCODE(6),
	NARRATION(35),
	XO_REF(1),
	LOC_CODE(3),
	CST_CODE(3),
	Curcode_C(3),
	ACODE(6),
	PNR_NO(8),
	TICKETNO(11),
	PAX(30),
	SECTOR(19),
	CRS_ID(2),
	FARE_BASIS(10),
	DEAL_CODE(15),
	NOS_PAX_A,
	FLT_DTLS1(18),
	FLT_DTLS2(18),
	FLT_DTLS3(18),
	FLT_DTLS4(18),
	R_O_E_C,
	BASIC_PBL,
	BASIC_FARE,
	TAX_1,
	TAX_2,
	TAX_3,
	TAX_4,
	TAX_5,
	TAX_6,
	TAX_7,
	TAX_8,
	DISC_PAIDM1(2),
	DISC_PAIDM2(3),
	DISC_PAIDM3(3),
	BROK_PAIDM1(2),
	DISC_PAIDV1,
	DISC_PAIDV2,
	DISC_PAIDV3,
	BROK_PAIDV1,
	DISC_PAID1,
	DISC_PAID2,
	DISC_PAID3,
	BROK_PAID1,
	SRV_CHRG1C,
	SRV_CHRG2C,
	SRV_CHRG3C,
	SRV_CHRG4C,
	SRV_CHRG5C,
	RAF_C,
	SERV_TAXC,
	SERV_EDUC,
	SERV_CS1C,
	TDC_PAIDV1,
	TDS_C,
	TDB_PAIDV1,
	TDS_B,
	Created_By(15),
	Pay_Type(1),
	XXL_C,
	SERV1_TAXC,
	SERV1_EDUC,
	SERV1_CS1C,
	SERV3_TAXC,
	SERV3_EDUC,
	SERV3_CS1C,
	Stax_PayBy(1),
	TDC_Index,
	DISC_PAIDM5(3),
	DISC_PAIDV5,
	DISC_PAID5,
	SRV_CHRG2_H(1),
	IDATE,
	REFR_KEY(10),
	SRV_CHRG1_H(1),
	SRV_CHRG3_H(1),
	VD_REF(19),
	SaleDATE,
	Created_On,
	SERV_CS2C,
	SERV1_CS2C,
	SERV2_CS1C,
	SERV2_CS2C,
	SERV3_CS2C,
	NARRATION_5,
	NARRATION_6,
	GST_TYPE,
	GST_CalcOn,
	PAX_DTLS,
	C_Loginname,
	TAXABLE_S_1,
	TAXABLE_S_2,
	TAXABLE_S_3;

	// All columns separated by comma
	public final static String ALL;

	private int length;
	private boolean notNull;

	static {
		StringJoiner sj = new StringJoiner(", ");
		for (SupplierOrderColumn column : SupplierOrderColumn.values()) {
			sj.add(column.name());
		}
		ALL = sj.toString();
	}

	SupplierOrderColumn() {
		this(-1);
	}

	SupplierOrderColumn(int maxLength) {
		this(maxLength, false);
	}

	SupplierOrderColumn(boolean notNull) {
		this(-1, notNull);
	}

	SupplierOrderColumn(int length, boolean notNull) {
		this.length = length;
		this.notNull = notNull;
	}

	public boolean hasLength() {
		return length >= 0;
	}
}
