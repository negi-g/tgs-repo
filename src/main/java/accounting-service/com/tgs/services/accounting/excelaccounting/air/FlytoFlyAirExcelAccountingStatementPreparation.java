package com.tgs.services.accounting.excelaccounting.air;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.datamodel.OrderColumn;
import com.tgs.services.accounting.datamodel.VarargsFunction;
import com.tgs.services.accounting.excelaccounting.amendment.AirExcelAccountingStatementPreparation;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;


@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class FlytoFlyAirExcelAccountingStatementPreparation extends AirExcelAccountingStatementPreparation {

	@Override
	protected String getCSTCode() {
		return "000";
	}

	@Override
	protected BigDecimal getSRV_CHRG1P(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.SMF).divide(new BigDecimal(1.18), 2,
				RoundingMode.HALF_UP);
	}

	@Override
	protected BigDecimal getSRV_CHRG1C(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.SMF).divide(new BigDecimal(1.18), 2,
				RoundingMode.HALF_UP);
	}

	@Override
	protected BigDecimal getTax3(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal,
			Integer sourceId) {
		BigDecimal taxFare = super.getTax3(getFareComponentsAsBigDecimal, sourceId);
		List<Integer> sourceIdList = Arrays.asList(17, 18, 19);
		if (sourceIdList.contains(sourceId)) {
			taxFare = taxFare.subtract(getFareComponentsAsBigDecimal.apply(FareComponent.SMF));
		}
		return taxFare;
	}

	@Override
	protected String getSupplierCode(String supplierCode) {
		setString(OrderColumn.XO_REF, "C");
		return supplierCode;
	}

	@Override
	protected BigDecimal getCommissionAmount(DbAirOrderItem airOrderItem,
			VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.SC);

	}

	@Override
	protected String getSRV_CHRG1() {
		return "N";
	}

	@Override
	protected String getSRV_CHRG2() {
		return "T";
	}

	@Override
	protected String getSRV_CHRG3() {
		return "N";
	}
}
