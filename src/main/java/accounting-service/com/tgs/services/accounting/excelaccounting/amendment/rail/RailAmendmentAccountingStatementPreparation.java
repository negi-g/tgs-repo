package com.tgs.services.accounting.excelaccounting.amendment.rail;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Function;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.tgs.services.accounting.datamodel.RailOrderColumn;
import com.tgs.services.accounting.datamodel.VarargsFunction;
import com.tgs.services.accounting.utils.RailAccountingUtils;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.manager.AbstractAmendmentInvoiceIdManager;
import com.tgs.services.rail.datamodel.RailFareComponent;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class RailAmendmentAccountingStatementPreparation {


	@Autowired
	protected AbstractAmendmentInvoiceIdManager invoiceIdManager;

	@Autowired
	protected RailAccountingUtils railAccountingUtils;


	final private static String INSERT_SQL_TEMPLATE = "INSERT INTO %s (" + RailOrderColumn.ALL + ") VALUES (%s)";

	final private static String SELECT_SQL_TEMPLATE = "SELECT * FROM %s WHERE NARRATION IN ('%s');";

	private Map<RailOrderColumn, CharSequence> invoiceData = new HashMap<>();


	public String prepareInsertSql(String tableName, Map<RailOrderColumn, CharSequence> existingInvoiceData,
			Amendment amendment, Long paxId, int serialNo) throws SQLException {
		Map<RailOrderColumn, CharSequence> statementParams =
				getColumnValues(existingInvoiceData, amendment, paxId, serialNo);
		StringJoiner joiner = new StringJoiner(", ");
		for (RailOrderColumn column : RailOrderColumn.values()) {
			joiner.add(statementParams.get(column));
		}
		return String.format(INSERT_SQL_TEMPLATE, tableName, joiner.toString());
	}

	protected Map<RailOrderColumn, CharSequence> getColumnValues(Map<RailOrderColumn, CharSequence> existingInvoiceData,
			Amendment amendment, Long paxId, int serialNo) {

		invoiceData = existingInvoiceData;

		ProductMetaInfo productMetaInfo = Product.getProductMetaInfoFromId(amendment.getBookingId());
		String invoiceId = null;
		invoiceId = amendment.getAdditionalInfo().getInvoiceId();
		if (StringUtils.isBlank(invoiceId)) {
			invoiceId = invoiceIdManager.getInvoiceId(amendment, productMetaInfo);
			amendment.getAdditionalInfo().setInvoiceId(invoiceId);
		}

		Integer numericInvoiceId = invoiceIdManager.getNumericInvoiceId(invoiceId, amendment, productMetaInfo);

		setString(invoiceIdManager.getInvoiceIdPrefix(amendment, productMetaInfo), RailOrderColumn.DOC_PRF);
		setString(String.valueOf(numericInvoiceId), RailOrderColumn.DOC_NOS);

		setBigDecimal(BigDecimal.ZERO, RailOrderColumn.SERV_CS1C, RailOrderColumn.SERV_CS1P, RailOrderColumn.SERV_CS2C,
				RailOrderColumn.SERV_CS2P);


		// Fare needs to be pushed only for 1st pax
		final Map<RailFareComponent, Double> fareComponents = serialNo == 1
				? amendment.getModifiedInfo().getRailOrderItem().getAdditionalInfo().getPriceInfo().getFareComponents()
				: new HashMap<>();

		// BigDecimal behaves abnormally when used with double. Use String instead.
		final Function<Double, BigDecimal> getDoubleAsBigDecimal =
				doubleVal -> new BigDecimal(doubleVal.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final VarargsFunction<RailFareComponent, Double> getFareComponentsAsDouble = components -> {
			double sum = 0.0;
			for (RailFareComponent fareComponent : components) {
				sum += fareComponents.getOrDefault(fareComponent, 0.0);
			}
			return sum;
		};

		// Get sum of all FareComponents as BigDecimal
		final VarargsFunction<RailFareComponent, BigDecimal> getFareComponentsAsBigDecimal = components -> {
			return getDoubleAsBigDecimal.apply(getFareComponentsAsDouble.apply(components));
		};

		setBigDecimal(RailOrderColumn.BASIC_FARE, getFareComponentsAsBigDecimal.apply(RailFareComponent.AAR)
				.add(getFareComponentsAsBigDecimal.apply(RailFareComponent.RCF)));
		setBigDecimal(BigDecimal.ZERO, RailOrderColumn.SRV_CHRG3C, RailOrderColumn.SRV_CHRG2C, RailOrderColumn.TAX_4,
				RailOrderColumn.TAX_2, RailOrderColumn.SERV_EDUC, RailOrderColumn.SERV_TAXC, RailOrderColumn.SRV_CHRG3P,
				RailOrderColumn.SRV_CHRG2P, RailOrderColumn.SRV_CHRG1C, RailOrderColumn.SRV_CHRG1P,
				RailOrderColumn.SERV_TAXP, RailOrderColumn.SERV_EDUP, RailOrderColumn.XXL_C, RailOrderColumn.XXL_P);
		setBigDecimal(RailOrderColumn.RAF_C, getFareComponentsAsBigDecimal.apply(RailFareComponent.RCF));
		setBigDecimal(RailOrderColumn.RAF_P, getFareComponentsAsBigDecimal.apply(RailFareComponent.RCF));


		setString(RailOrderColumn.NARRATION, amendment.getAmendmentId());
		setString(RailOrderColumn.IDATE, amendment.getProcessedOn().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		LocalDateTime journeyDate = ObjectUtils.firstNonNull(
				amendment.getRailAdditionalInfo().getOrderPreviousSnapshot().getAdditionalInfo().getBoardingTime(),
				amendment.getRailAdditionalInfo().getOrderPreviousSnapshot().getDepartureTime());
		setString(RailOrderColumn.JOURNEY_DATE, journeyDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

		return existingInvoiceData;
	}

	protected final void setString(RailOrderColumn column, String str) {
		railAccountingUtils.setString(invoiceData, column, str);
	}

	protected final void setString(String str, RailOrderColumn... columns) {
		railAccountingUtils.setString(invoiceData, str, columns);
	}


	protected final void setBigDecimal(BigDecimal bigDecimal, RailOrderColumn... columns) {
		railAccountingUtils.setBigDecimal(invoiceData, bigDecimal, columns);
	}

	protected final void setBigDecimal(RailOrderColumn column, BigDecimal bigDecimal) {
		railAccountingUtils.setBigDecimal(invoiceData, column, bigDecimal);
	}


	public String prepareSelectSql(String tableName, Set<String> bookingIds) {
		return String.format(SELECT_SQL_TEMPLATE, tableName, Joiner.on("','").join(bookingIds));
	}

}
