package com.tgs.services.accounting.excelaccounting.hotel;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import com.tgs.services.accounting.conf.AccoutingDbConfiguration;
import com.tgs.services.accounting.utils.AccountingUtils;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.hotel.HotelModifyBookingManager;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;
import com.tgs.utils.common.SimpleCache;
import lombok.extern.slf4j.Slf4j;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
@Slf4j
public class HotelExcelAccounting {

	final private static int BATCH_SIZE = 1;

	@Autowired
	private AccoutingDbConfiguration dbConf;

	@Autowired
	private AccountingUtils accountingUtils;

	@Autowired
	private HotelOrderItemService hotelItemService;

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	private HotelModifyBookingManager modifyBookingManager;

	@Autowired
	private OrderService orderService;

	public void pushInvoicesFor(OrderFilter orderFilter) {
		if (orderFilter == null) {
			return;
		}
		Connection con = null;
		Statement statement = null;
		try {
			con = accountingUtils.getConnection();
			statement = con.createStatement();
			con.setAutoCommit(false);

			List<Object[]> orderObjectsList = hotelItemService.findByJsonSearch(orderFilter);


			for (int index = 0; index < orderObjectsList.size(); index++) {
				DbHotelOrderItem dbOrderItem = (DbHotelOrderItem) orderObjectsList.get(index)[0];
				DbOrder order = (DbOrder) orderObjectsList.get(index)[1];
				String bookingId = dbOrderItem.getBookingId();
				Amendment amendment = amendmentService.getFirstAmendment(bookingId);
				// Picking the HotelorderItem from first Snapshot
				if (amendment != null) {
					HotelInfo hotelInfo =
							amendment.getAdditionalInfo().getHotelAdditionalInfo().getOrderPreviousSnapshot();
					HotelOrderItem oldOrderItem = getHotelOrder(hotelInfo, order, dbOrderItem);
					DbHotelOrderItem dbOldOrderItem = new DbHotelOrderItem().from(oldOrderItem);
					log.info("Fetching data from amendment for product : HOTEL and booking id: {} and amendment id: {}",
							bookingId, amendment.getAmendmentId());
					Object[] objectList = {dbOldOrderItem, order};
					orderObjectsList.set(index, objectList);
				}
			}

			log.debug("[HotelExcelAccounting] Changed order snapshots as well from db");
			Map<DbOrder, List<DbHotelOrderItem>> orderItemsGroup = groupOrderItems(orderObjectsList);
			log.debug("[HotelExcelAccounting] Grouped orders");

			List<DbOrder> ordersInBatch = new ArrayList<>();
			Map<String, List<String>> sqlsLogs = new HashMap<>();
			Iterator<Entry<DbOrder, List<DbHotelOrderItem>>> orderEntryIterator = orderItemsGroup.entrySet().iterator();

			while (orderEntryIterator.hasNext()) {
				try {
					Entry<DbOrder, List<DbHotelOrderItem>> orderEntry = orderEntryIterator.next();
					DbOrder order = orderEntry.getKey();
					List<DbHotelOrderItem> items = orderEntry.getValue();
					if (BooleanUtils.isNotTrue(order.getAdditionalInfo().getInvoicePushStatus())) {
						List<String> sqlList = createSqlsFor(order, items);
						sqlsLogs.put(order.getBookingId(), sqlList);
						if (!CollectionUtils.isEmpty(sqlList)) {
							for (String sql : sqlList) {
								statement.addBatch(sql);
								ordersInBatch.add(order);
							}
						}
					}
					/**
					 * Execute batch if batch-size is crossed or if it is the last order.
					 */
					if (ordersInBatch.size() >= BATCH_SIZE || !orderEntryIterator.hasNext()) {
						boolean status = true;
						try {
							statement.executeBatch();
							con.commit();
						} catch (Exception e) {
							status = false;
							log.error("Failed to insert invoice for bookingIds {} due to {}", sqlsLogs.keySet(),
									e.getMessage(), e);
							for (String key : sqlsLogs.keySet()) {
								LogUtils.log(key, "HotelAccounting", e);
							}
						} finally {
							/**
							 * Batch should be cleared even if it fails so that upcoming batches can be served.
							 */
							statement.clearBatch();
							if (status) {
								for (DbOrder order_local : ordersInBatch) {
									order_local.getAdditionalInfo().setInvoicePushStatus(status);
								}
								orderService.saveWithoutProcessedOn(ordersInBatch);
							}
							ordersInBatch.clear();
						}
						accountingUtils.logSqls(sqlsLogs, status);
						sqlsLogs.clear();
					}
				} catch (Exception e) {
					log.error(
							"Failed to insert invoice for bookingIds {}. Trying to build connections again. Reason: {} ",
							sqlsLogs.keySet(), e.getMessage(), e);
					for (String key : sqlsLogs.keySet()) {
						LogUtils.log(key, "HotelAccounting", e);
					}
					accountingUtils.tryClosingResources(con, statement);
					con = accountingUtils.getConnection();
					statement = con.createStatement();
				}
			}
			log.info("[HotelExcelAccounting] Pushed orders in accounting package");

		} catch (Exception e) {
			log.error("Failed to insert invoice for orderFilter {} due to {}", orderFilter, e);
		} finally {
			LogUtils.clearLogList();
			accountingUtils.tryClosingResources(con, statement);
		}
	}

	private Map<DbOrder, List<DbHotelOrderItem>> groupOrderItems(List<Object[]> orderObjectsList) {
		Map<DbOrder, List<DbHotelOrderItem>> orderItemsGroup = new HashMap<>();
		List<DbHotelOrderItem> orderItems = null;
		DbOrder previousOrder = null;
		SimpleCache<String, DbOrder> orderCache = new SimpleCache<String, DbOrder>() {
			@Override
			protected String getKey(DbOrder value) {
				return value.getBookingId();
			}
		};
		for (Object[] orderObjects : orderObjectsList) {
			DbHotelOrderItem orderItem = (DbHotelOrderItem) orderObjects[0];
			DbOrder order = (DbOrder) orderObjects[1];
			order = orderCache.getCached(order);
			if (!order.equals(previousOrder)) {
				if (previousOrder != null) {
					orderItemsGroup.put(previousOrder, orderItems);
				}
				orderItems = new ArrayList<>();
			}
			orderItems.add(orderItem);
			previousOrder = order;
		}
		if (previousOrder != null) {
			orderItemsGroup.put(previousOrder, orderItems);
		}
		return orderItemsGroup;
	}

	private List<String> createSqlsFor(DbOrder order, List<DbHotelOrderItem> hotelOrderItems) {
		if (order == null) {
			return null;
		}
		AbstractHotelExcelAccounting statementPreparation = null;
		List<String> sqlList = new ArrayList<>();
		try {
			statementPreparation = HotelExcelAccountingFactory.getHotelExcelAccountingFactory(order, hotelOrderItems);
			sqlList.add(statementPreparation.prepareInsertSql(dbConf.getHotelTableName()));
		} catch (Exception e) {
			log.error("Failed to prepare invoice statements for bookingId {} due to {} ", order.getBookingId(),
					e.getMessage(), e);
			LogUtils.log(order.getBookingId(), "HotelAccounting", e);
			return null;
		}
		return sqlList;
	}

	private HotelOrderItem getHotelOrder(HotelInfo hotelInfo, DbOrder order, DbHotelOrderItem dbOrderItem) {
		HotelPostBookingBaseRequest request = new HotelPostBookingBaseRequest();
		BaseHotelUtils.updateTotalFareComponents(hotelInfo, null);
		request.setBookingId(order.getBookingId());
		request.setHInfo(hotelInfo);
		request.setDeliveryInfo(order.getDeliveryInfo());
		HotelOrder hotelOrder = modifyBookingManager.getHotelOrderFromUpdatedHotelInfo(request);
		HotelOrderItem hotelOrderItem = hotelOrder.getItems().stream()
				.filter(orderItem -> orderItem.getId().equals(dbOrderItem.getId())).findFirst().orElse(null);
		return hotelOrderItem;
	}
}
