package com.tgs.services.accounting.servicehandler;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.accounting.conf.AccoutingDbConfiguration;
import com.tgs.services.accounting.excelaccounting.air.ProtectAirExcelAccountingStatementPreparation;
import com.tgs.services.accounting.utils.AccountingUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.air.AirOrderItemRepository;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.utils.common.SimpleCache;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProtectAccountingServiceHandler extends ServiceHandler<OrderFilter, BaseResponse> {

	final private static int BATCH_SIZE = 1;

	@Autowired
	private AccoutingDbConfiguration dbConf;

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private AccountingUtils accountingUtils;

	@Autowired
	private AirOrderItemRepository airItemRepository;

	@Autowired
	private ProtectAirExcelAccountingStatementPreparation protectStatementPreparation;

	@Override
	public void beforeProcess() throws Exception {
		if (CollectionUtils.isEmpty(request.getProducts())) {
			throw new CustomGeneralException(SystemError.INVALID_PRODUCT);
		}
	}

	@Override
	public void process() throws Exception {
		try {
			pushInvoicesFor(request);
		} catch (Exception exp) {
			log.error("Pushing Protect Group Order failed for {} due to {}", GsonUtils.getGson().toJson(request),
					exp.getMessage(), exp);
			throw new CustomGeneralException(SystemError.INVOICE_FAILED);
		}
	}

	@Override
	public void afterProcess() throws Exception {}

	private void pushInvoicesFor(OrderFilter orderFilter) {
		if (orderFilter == null) {
			return;
		}
		Connection con = null;
		Statement statement = null;
		try {
			con = accountingUtils.getConnection();
			statement = con.createStatement();
			con.setAutoCommit(false);
			List<Object[]> orderObjectsList = new ArrayList<>();

			orderObjectsList = airOrderItemService.findByJsonSearch(orderFilter);

			Map<DbOrder, List<DbAirOrderItem>> orderItemsGroup = groupOrderItems(orderObjectsList);
			List<DbOrder> ordersInBatch = new ArrayList<>();
			Map<String, List<String>> sqlsLogs = new HashMap<>();
			Iterator<Entry<DbOrder, List<DbAirOrderItem>>> orderEntryIterator = orderItemsGroup.entrySet().iterator();


			while (orderEntryIterator.hasNext()) {
				try {
					Entry<DbOrder, List<DbAirOrderItem>> orderEntry = orderEntryIterator.next();
					DbOrder order = orderEntry.getKey();
					if (BooleanUtils.isNotTrue(order.getAdditionalInfo().getIsCancallationProtectionOpted())) {
						continue;
					}

					if (order.getAdditionalInfo().getProtectGroupInvoiceInfo().getInvoiceId() == null) {
						String invoiceId = airItemRepository.getNextRefundProtectInvoiceId() + "";
						order.getAdditionalInfo().getProtectGroupInvoiceInfo().setInvoiceId(invoiceId);
					}
					List<DbAirOrderItem> items = orderEntry.getValue();
					if (BooleanUtils.isNotTrue(order.getAdditionalInfo().getIsPushedToProtectGroupAccounting())) {
						List<String> sqlList = createSqlsFor(order, items);
						sqlsLogs.put(order.getBookingId(), sqlList);
						if (!CollectionUtils.isEmpty(sqlList)) {
							for (String sql : sqlList) {
								statement.addBatch(sql);
								ordersInBatch.add(order);
							}
						}
					}
					/**
					 * Execute batch if batch-size is crossed or if it is the last order.
					 */
					if (ordersInBatch.size() >= BATCH_SIZE || !orderEntryIterator.hasNext()) {
						boolean status = true;
						try {
							statement.executeBatch();
							con.commit();
						} catch (Exception e) {
							status = false;
							log.error("Failed to push protect group order for bookingIds {} due to ", sqlsLogs.keySet(),
									e);
							for (String key : sqlsLogs.keySet()) {
								LogUtils.log(key, "ProtectGroupAccounting", e);
							}
						} finally {
							/**
							 * Batch should be cleared even if it fails so that upcoming batches can be served.
							 */
							statement.clearBatch();
							for (DbOrder order_local : ordersInBatch) {
								order.getAdditionalInfo().getProtectGroupInvoiceInfo().setInvoicePushStatus(status);
								order_local.getAdditionalInfo().setIsPushedToProtectGroupAccounting(status);;
							}
							orderService.saveWithoutProcessedOn(ordersInBatch);
							ordersInBatch.clear();
						}
						accountingUtils.logSqls(sqlsLogs, status);
						sqlsLogs.clear();
					}
				} catch (Exception e) {
					log.error(
							"Failed to push protect group order for bookingIds {}. Trying to build connections again. Reason: {} ",
							sqlsLogs.keySet(), e.getMessage(), e);
					for (String key : sqlsLogs.keySet()) {
						LogUtils.log(key, "ProtectGroupAccounting", e);
					}
					accountingUtils.tryClosingResources(con, statement);
					con = accountingUtils.getConnection();
					statement = con.createStatement();
				}
			}
		} catch (Exception e) {
			log.error("Failed to push protect group order for orderFilter {} due to {}", orderFilter, e);
		} finally {
			LogUtils.clearLogList();
			accountingUtils.tryClosingResources(con, statement);
		}
	}

	private Map<DbOrder, List<DbAirOrderItem>> groupOrderItems(List<Object[]> orderObjectsList) {
		Map<DbOrder, List<DbAirOrderItem>> orderItemsGroup = new HashMap<>();
		List<DbAirOrderItem> orderItems = null;
		DbOrder previousOrder = null;
		SimpleCache<String, DbOrder> orderCache = new SimpleCache<String, DbOrder>() {
			@Override
			protected String getKey(DbOrder value) {
				return value.getBookingId();
			}
		};
		for (Object[] orderObjects : orderObjectsList) {
			DbAirOrderItem orderItem = (DbAirOrderItem) orderObjects[0];
			DbOrder order = (DbOrder) orderObjects[1];
			order = orderCache.getCached(order);
			if (!order.equals(previousOrder)) {
				if (previousOrder != null) {
					orderItemsGroup.put(previousOrder, orderItems);
				}
				orderItems = new ArrayList<>();
			}
			orderItems.add(orderItem);
			previousOrder = order;
		}
		if (previousOrder != null) {
			orderItemsGroup.put(previousOrder, orderItems);
		}
		return orderItemsGroup;
	}

	private List<String> createSqlsFor(DbOrder order, List<DbAirOrderItem> airOrderItems) {
		if (order == null) {
			return null;
		}
		List<String> sqlList = new ArrayList<>();
		try {
			final int travellersCount = airOrderItems.get(0).getTravellerInfo().size();
			// grouping by PNR
			Map<String, List<DbAirOrderItem>> pnrWiseAirOrderItems = airOrderItems.stream()
					.collect(Collectors.groupingBy(orderItem -> orderItem.getTravellerInfo().get(0).getPnr()));
			if (airOrderItems.size() > 1) {
				log.debug("Original airOrderItems for booking id {} are {}", GsonUtils.getGson().toJson(airOrderItems));
				log.debug("Grouped airOrderItems for booking id {} are {}",
						GsonUtils.getGson().toJson(pnrWiseAirOrderItems));
			}
			int srno = 1;
			for (List<DbAirOrderItem> perPnrOrderItemList : pnrWiseAirOrderItems.values()) {
				for (int i = 0; i < travellersCount; i++) {
					sqlList.add(protectStatementPreparation.prepareInsertSql(
							dbConf.getCancellationProtectionTableName(), order, perPnrOrderItemList, srno));
					srno++;
				}
			}
		} catch (Exception e) {
			log.error("Failed to prepare sql statements for bookingId {} due to {} ", order.getBookingId(),
					e.getMessage(), e);
			LogUtils.log(order.getBookingId(), "ProtectGroupAccounting", e);
			return null;
		}
		return sqlList;
	}
}
