package com.tgs.services.accounting.excelaccounting.hotel;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.datamodel.HotelOrderColumn;
import com.tgs.services.accounting.utils.HotelAccountingUtils;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.gms.datamodel.InvoiceIdData;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.AbstractInvoiceIdManager;
import com.tgs.services.ums.datamodel.User;
import lombok.Setter;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
@Setter
public abstract class AbstractHotelExcelAccounting {

	@Autowired
	private UserServiceCommunicator usCommunicator;

	@Autowired
	private AbstractInvoiceIdManager invoiceIdManager;

	@Autowired
	private HMSCommunicator hmsComm;

	@Autowired
	protected HotelAccountingUtils hotelAccountingUtils;

	protected DbOrder order;

	protected List<DbHotelOrderItem> dbHotelOrderItems;

	final private static String INSERT_SQL_TEMPLATE = "INSERT INTO %s (" + HotelOrderColumn.ALL + ") VALUES (%s)";

	private Map<HotelOrderColumn, CharSequence> statementParams = new HashMap<>();

	public AbstractHotelExcelAccounting(DbOrder order, List<DbHotelOrderItem> hotelOrderItems) {
		this.order = order;
		this.dbHotelOrderItems = hotelOrderItems;
	}

	public String prepareInsertSql(String tableName) throws SQLException {
		Map<HotelOrderColumn, CharSequence> statementParams = getColumnValues();
		StringJoiner joiner = new StringJoiner(", ");
		for (HotelOrderColumn column : HotelOrderColumn.values()) {
			joiner.add(statementParams.get(column));
		}
		return String.format(INSERT_SQL_TEMPLATE, tableName, joiner.toString());
	}

	protected Map<HotelOrderColumn, CharSequence> getColumnValues() {
		statementParams.clear();
		ProductMetaInfo productMetaInfo = Product.getProductMetaInfoFromId(order.getBookingId());

		Order domainOrder = order.toDomain();
		DbHotelOrderItem firstOrderItem = dbHotelOrderItems.get(0);

		final User bookingUser = usCommunicator.getUserFromCache(order.getBookingUserId());

		String invoiceId = order.getAdditionalInfo().getInvoiceId();
		if (StringUtils.isBlank(invoiceId)) {
			invoiceId = invoiceIdManager.getInvoiceIdFor(domainOrder, productMetaInfo);
			order.getAdditionalInfo().setInvoiceId(invoiceId);
		}

		final InvoiceIdData invoiceIdData = invoiceIdManager.getInvoiceIdData(invoiceId, domainOrder, productMetaInfo);
		setString(String.valueOf(invoiceIdData.getNumericInvoiceId()), HotelOrderColumn.DOC_NOS);

		String clientCode = bookingUser.getAdditionalInfo().getAccountingCode();
		if (StringUtils.isBlank(clientCode)) {
			clientCode = "CT01ET";
		} else {
			clientCode = "C".concat(clientCode);
		}
		setString(clientCode, HotelOrderColumn.CCODE);

		LocalDateTime voucherDate = ObjectUtils.firstNonNull(
				order.getAdditionalInfo().getFirstUpdateTime().get(OrderStatus.SUCCESS.name()), order.getCreatedOn());
		setString(voucherDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")), HotelOrderColumn.IDATE);
		setString(order.getBookingId(), HotelOrderColumn.NARRATION);

		setString("", HotelOrderColumn.IL_REF, HotelOrderColumn.VD_REF, HotelOrderColumn.DCODE, HotelOrderColumn.ECODE,
				HotelOrderColumn.BCODE, HotelOrderColumn.REFR_KEY, HotelOrderColumn.TICKETNO, HotelOrderColumn.PKG_CODE,
				HotelOrderColumn.SCODE_B, HotelOrderColumn.Created_On, HotelOrderColumn.Created_By,
				HotelOrderColumn.NARR_3, HotelOrderColumn.NARR_4, HotelOrderColumn.NARR_5, HotelOrderColumn.NARR_6,
				HotelOrderColumn.PKG_CODE, HotelOrderColumn.SCODE_B, HotelOrderColumn.PKG_CODE,
				HotelOrderColumn.SCODE_B, HotelOrderColumn.PKG_CODE, HotelOrderColumn.SCODE_B);
		setString("HW", HotelOrderColumn.DOC_PRF);
		setString("H", HotelOrderColumn.IDM_FLAG, HotelOrderColumn.XO_REF);
		setString("7", HotelOrderColumn.CST_CODE);
		setString("R", HotelOrderColumn.TARIFFTYPE);
		setString("RB", HotelOrderColumn.ROOM_SGL_PURMTH, HotelOrderColumn.ROOM_DBL_PURMTH,
				HotelOrderColumn.ROOM_TWN_PURMTH, HotelOrderColumn.ROOM_TRP_PURMTH, HotelOrderColumn.ROOM_QAD_PURMTH,
				HotelOrderColumn.ROOM_ADT_PURMTH, HotelOrderColumn.ROOM_CHD_PURMTH, HotelOrderColumn.ROOM_CWB_PURMTH,
				HotelOrderColumn.ROOM_FOC_PURMTH);
		setString("GI03HT", HotelOrderColumn.GCODE);
		setString("HZ0000", HotelOrderColumn.HCODE);
		setString("INR", HotelOrderColumn.Curcode_C, HotelOrderColumn.Curcode_S);
		setString("0", HotelOrderColumn.LOC_CODE, HotelOrderColumn.ROOMVIEW, HotelOrderColumn.MEALPLAN,
				HotelOrderColumn.ROOMTYPE, HotelOrderColumn.CITY, HotelOrderColumn.ROOM_SGL_NOS,
				HotelOrderColumn.ROOM_SGL_PAX, HotelOrderColumn.ROOM_SGL_RATE, HotelOrderColumn.ROOM_SGL_PURVAL,
				HotelOrderColumn.ROOM_TWN_NOS, HotelOrderColumn.ROOM_TWN_PAX, HotelOrderColumn.ROOM_TWN_RATE,
				HotelOrderColumn.ROOM_TWN_PURVAL, HotelOrderColumn.ROOM_TRP_NOS, HotelOrderColumn.ROOM_TRP_PAX,
				HotelOrderColumn.ROOM_TRP_RATE, HotelOrderColumn.ROOM_TRP_PURVAL, HotelOrderColumn.ROOM_TRP_PURVAL,
				HotelOrderColumn.ROOM_QAD_NOS, HotelOrderColumn.ROOM_QAD_PAX, HotelOrderColumn.ROOM_QAD_RATE,
				HotelOrderColumn.ROOM_QAD_PURVAL, HotelOrderColumn.ROOM_ADT_NOS, HotelOrderColumn.ROOM_ADT_PAX,
				HotelOrderColumn.ROOM_ADT_RATE, HotelOrderColumn.ROOM_ADT_PURVAL, HotelOrderColumn.ROOM_CHD_NOS,
				HotelOrderColumn.ROOM_CHD_PAX, HotelOrderColumn.ROOM_CHD_RATE, HotelOrderColumn.ROOM_CHD_PURVAL,
				HotelOrderColumn.ROOM_CWB_NOS, HotelOrderColumn.ROOM_CWB_PAX, HotelOrderColumn.ROOM_CWB_RATE,
				HotelOrderColumn.ROOM_CWB_PURVAL, HotelOrderColumn.ROOM_FOC_NOS, HotelOrderColumn.ROOM_FOC_PAX,
				HotelOrderColumn.ROOM_FOC_RATE, HotelOrderColumn.TAX_S, HotelOrderColumn.TAX_C,
				HotelOrderColumn.BROK_PAID1, HotelOrderColumn.SRV_CHRG3C, HotelOrderColumn.SRV_CHRG3_H,
				HotelOrderColumn.RAF_C, HotelOrderColumn.SRV_CHRG2P, HotelOrderColumn.SRV_CHRG3P,
				HotelOrderColumn.RAF_P, HotelOrderColumn.GST_C_R, HotelOrderColumn.GST_S_R, HotelOrderColumn.SERV_TAXP,
				HotelOrderColumn.SERV_EDUP, HotelOrderColumn.TDB_PAIDV1, HotelOrderColumn.TDS_B,
				HotelOrderColumn.SERV_CS2C, HotelOrderColumn.SERV1_CS1C, HotelOrderColumn.SERV1_CS2C,
				HotelOrderColumn.SERV2_CS1C, HotelOrderColumn.SERV2_CS2C, HotelOrderColumn.SERV3_CS1C,
				HotelOrderColumn.SERV3_CS2C, HotelOrderColumn.SERV_CS1P, HotelOrderColumn.SERV_CS2P,
				HotelOrderColumn.SERV1_CS1P, HotelOrderColumn.SERV1_CS2P, HotelOrderColumn.SERV3_CS1P,
				HotelOrderColumn.SERV3_CS2P, HotelOrderColumn.GST_TYPE, HotelOrderColumn.ROOM_FOC_PURVAL);

		setString("1", HotelOrderColumn.DOC_SRNO, HotelOrderColumn.R_O_E_C, HotelOrderColumn.R_O_E_S);
		setString("VL", HotelOrderColumn.DISC_PAIDM1, HotelOrderColumn.DISC_RECDM1, HotelOrderColumn.BROK_PAIDM1);
		setString("C", HotelOrderColumn.STX_CENVAT);
		setString("N", HotelOrderColumn.DISC_PAIDM2, HotelOrderColumn.SRV_PAIDM2);
		setString("S", HotelOrderColumn.STX_METHOD, HotelOrderColumn.GST_C_M, HotelOrderColumn.GST_S_M);
		setString("B", HotelOrderColumn.SRV_CHRG2_H);
		setString("998551", HotelOrderColumn.SAC_CODE1);

		setString(firstOrderItem.getHotel(), HotelOrderColumn.HOTEL_NAME);

		setString(firstOrderItem.getAdditionalInfo().getSupplierBookingReference(), HotelOrderColumn.XO_NOS);

		TravellerInfo firstTraveller = firstOrderItem.getRoomInfo().getTravellerInfo().get(0);

		String pax = StringUtils.joinWith(" ", firstTraveller.getTitle() + ".", firstTraveller.getFirstName(),
				firstTraveller.getLastName());

		setString(pax.trim(), HotelOrderColumn.PAX);


		setString(firstOrderItem.getCheckInDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
				HotelOrderColumn.CHECK_IN_DATE);
		setString(firstOrderItem.getCheckOutDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
				HotelOrderColumn.CHECK_OUT_DATE);
		setString(Integer.toString(dbHotelOrderItems.size()), HotelOrderColumn.ROOM_DBL_NOS);


		long noOfNights = Duration.between(firstOrderItem.getCheckInDate().atStartOfDay(),
				firstOrderItem.getCheckOutDate().atStartOfDay()).toDays();
		setString("for " + Long.toString(noOfNights) + " Night", HotelOrderColumn.NARR_2);


		int totalPaxCount = dbHotelOrderItems.stream().mapToInt(ordetItem -> {
			return ordetItem.getRoomInfo().getTravellerInfo().size();
		}).sum();

		setString(Integer.toString(totalPaxCount), HotelOrderColumn.ROOM_DBL_PAX);

		BigDecimal totalSupplierNetPrice = getTotalPriceFromComponenet(HotelFareComponent.SNP);

		setBigDecimal(totalSupplierNetPrice.divide(BigDecimal.valueOf(dbHotelOrderItems.size())),
				HotelOrderColumn.ROOM_DBL_RATE);
		setBigDecimal(totalSupplierNetPrice, HotelOrderColumn.ROOM_DBL_PURVAL, HotelOrderColumn.BASIC_C,
				HotelOrderColumn.BASIC_S);

		setString(Integer.toString(getTotalPaxCount(dbHotelOrderItems, "ADT")), HotelOrderColumn.NOS_PAX_A);
		setString(Integer.toString(getTotalPaxCount(dbHotelOrderItems, "CHD")), HotelOrderColumn.NOS_PAX_C);
		setString(Integer.toString(getTotalPaxCount(dbHotelOrderItems, "INF")), HotelOrderColumn.NOS_PAX_I);


		String roomName = dbHotelOrderItems.stream().map(orderItem -> {
			return orderItem.getRoomName();
		}).collect(Collectors.joining(","));

		setString(roomName, HotelOrderColumn.NARR_1);


		BigDecimal totalServiceCharge = getTotalPriceFromComponenet(HotelFareComponent.MF);

		setBigDecimal(totalServiceCharge, HotelOrderColumn.SRV_CHRG1C);


		BigDecimal totalIGST = getTotalPriceFromComponenet(HotelFareComponent.IGST);

		setBigDecimal(totalIGST, HotelOrderColumn.SERV_TAXC);

		BigDecimal totalCGST = getTotalPriceFromComponenet(HotelFareComponent.CGST);

		setBigDecimal(totalCGST, HotelOrderColumn.SERV_EDUC);

		BigDecimal totalSGST = getTotalPriceFromComponenet(HotelFareComponent.SGST);

		setBigDecimal(totalSGST, HotelOrderColumn.SERV_CS1C);

		HotelSupplierInfo supplierInfo =
				hmsComm.getHotelSupplierInfo(firstOrderItem.getAdditionalInfo().getSupplierId());

		String supplierCode = supplierInfo.getCredentialInfo().getAccountingCode();

		setString(supplierCode, HotelOrderColumn.SCODE);

		setBigDecimal(getSRV_CHRG1P(), HotelOrderColumn.SRV_CHRG1P);

		setBigDecimal(getDISC_PAIDV1(), HotelOrderColumn.DISC_PAIDV1);

		setBigDecimal(getDISC_RECDV1(), HotelOrderColumn.DISC_RECDV1);

		setBigDecimal(getDISC_PAID1(), HotelOrderColumn.DISC_PAID1);

		setBigDecimal(getDISC_RECD1(), HotelOrderColumn.DISC_RECD1);

		setBigDecimal(getTDC_PAIDV1(), HotelOrderColumn.TDC_PAIDV1);

		setBigDecimal(getTDS_C(), HotelOrderColumn.TDS_C);

		setBigDecimal(getTDS_PAIDV1(), HotelOrderColumn.TDS_PAIDV1);

		setBigDecimal(getTDS_P(), HotelOrderColumn.TDS_P);

		setBigDecimal(getTDS_P(), HotelOrderColumn.TDS_P);

		setString(getPay_Type(), HotelOrderColumn.Pay_Type);

		setBigDecimal(getSRV_CHRG2C(), HotelOrderColumn.SRV_CHRG2C);

		return statementParams;
	}

	private int getTotalPaxCount(List<DbHotelOrderItem> dbHotelOrderItems, String paxType) {
		return dbHotelOrderItems.stream().mapToInt(orderItem -> {
			return orderItem.getRoomInfo().getTravellerInfo().stream()
					.filter(traveller -> PaxType.getStringReprestation(traveller.getPaxType()).equals(paxType))
					.collect(Collectors.toList()).size();
		}).sum();
	}

	protected final void setString(String str, HotelOrderColumn... columns) {
		hotelAccountingUtils.setString(statementParams, str, columns);
	}

	protected final void setBigDecimal(HotelOrderColumn column, BigDecimal bigDecimal) {
		hotelAccountingUtils.setBigDecimal(statementParams, column, bigDecimal);
	}

	protected final void setBigDecimal(BigDecimal bigDecimal, HotelOrderColumn... columns) {
		hotelAccountingUtils.setBigDecimal(statementParams, bigDecimal, columns);
	}

	protected BigDecimal getTotalPriceFromComponenet(HotelFareComponent fc) {
		return dbHotelOrderItems.stream().map(ordetItem -> {
			Double value = ordetItem.getRoomInfo().getTotalFareComponents().getOrDefault(fc, 0.0d);
			return new BigDecimal(value.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		}).reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	protected abstract BigDecimal getSRV_CHRG1P();

	protected abstract BigDecimal getDISC_PAIDV1();

	protected abstract BigDecimal getDISC_RECDV1();

	protected abstract BigDecimal getDISC_PAID1();

	protected abstract BigDecimal getDISC_RECD1();

	protected abstract BigDecimal getSRV_CHRG2C();

	protected abstract BigDecimal getTDC_PAIDV1();

	protected abstract BigDecimal getTDS_C();

	protected abstract BigDecimal getTDS_PAIDV1();

	protected abstract BigDecimal getTDS_P();

	protected abstract String getPay_Type();
}
