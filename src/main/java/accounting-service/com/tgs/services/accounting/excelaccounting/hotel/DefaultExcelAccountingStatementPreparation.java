package com.tgs.services.accounting.excelaccounting.hotel;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class DefaultExcelAccountingStatementPreparation extends AbstractHotelExcelAccounting {

	public DefaultExcelAccountingStatementPreparation(DbOrder order, List<DbHotelOrderItem> hotelOrderItems) {
		super(order, hotelOrderItems);
	}

	@Override
	protected BigDecimal getSRV_CHRG1P() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getDISC_PAIDV1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getDISC_RECDV1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getDISC_PAID1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getDISC_RECD1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getSRV_CHRG2C() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getTDC_PAIDV1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getTDS_C() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getTDS_PAIDV1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getTDS_P() {
		return BigDecimal.ZERO;
	}

	@Override
	protected String getPay_Type() {
		return "";
	}

}
