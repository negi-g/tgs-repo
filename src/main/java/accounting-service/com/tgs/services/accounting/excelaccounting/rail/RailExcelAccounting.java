package com.tgs.services.accounting.excelaccounting.rail;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import com.tgs.services.accounting.conf.AccoutingDbConfiguration;
import com.tgs.services.accounting.utils.AccountingUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import lombok.extern.slf4j.Slf4j;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
@Slf4j
public class RailExcelAccounting {

	final private static int BATCH_SIZE = 1;

	@Autowired
	private AccoutingDbConfiguration dbConf;

	@Autowired
	private RailOrderItemService railItemService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private AccountingUtils accountingUtils;

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	private RailExcelAccountingStatementPreparation statementPreparation;

	public void pushInvoicesFor(OrderFilter orderFilter) {
		if (orderFilter == null) {
			return;
		}
		Connection con = null;
		Statement statement = null;
		try {
			con = accountingUtils.getConnection();
			statement = con.createStatement();
			con.setAutoCommit(false);
			List<Object[]> orderObjectsList = railItemService.findByJsonSearch(orderFilter);
			for (int index = 0; index < orderObjectsList.size(); index++) {
				DbRailOrderItem dbOrderItem = (DbRailOrderItem) orderObjectsList.get(index)[0];
				DbOrder order = (DbOrder) orderObjectsList.get(index)[1];
				String bookingId = dbOrderItem.getBookingId();
				Amendment amendment = amendmentService.getFirstRailAmendment(bookingId);
				if (amendment != null) {
					RailOrderItem oldOrderItem =
							amendment.getAdditionalInfo().getRailAdditionalInfo().getOrderPreviousSnapshot();
					DbRailOrderItem dbOldOrderItem = new DbRailOrderItem().from(oldOrderItem);
					log.info("Fetching data from amendment for product : RAIL and booking id: {} and amendment id: {}",
							bookingId, amendment.getAmendmentId());
					Object[] objectList = {dbOldOrderItem, order};
					orderObjectsList.set(index, objectList);
				}
			}
			Map<DbOrder, DbRailOrderItem> orderItemsGroup = groupOrderItems(orderObjectsList);
			List<DbOrder> ordersInBatch = new ArrayList<>();
			Map<String, List<String>> sqlsLogs = new HashMap<>();
			Iterator<Entry<DbOrder, DbRailOrderItem>> orderEntryIterator = orderItemsGroup.entrySet().iterator();
			while (orderEntryIterator.hasNext()) {

				try {
					Entry<DbOrder, DbRailOrderItem> orderEntry = orderEntryIterator.next();
					DbOrder order = orderEntry.getKey();
					DbRailOrderItem items = orderEntry.getValue();
					if (BooleanUtils.isNotTrue(order.getAdditionalInfo().getInvoicePushStatus())) {
						List<String> sqlList = createSqlsFor(order, items);
						sqlsLogs.put(order.getBookingId(), sqlList);
						if (!CollectionUtils.isEmpty(sqlList)) {
							for (String sql : sqlList) {
								statement.addBatch(sql);
								ordersInBatch.add(order);
							}
						}
					}
					/**
					 * Execute batch if batch-size is crossed or if it is the last order.
					 */
					if (ordersInBatch.size() >= BATCH_SIZE || !orderEntryIterator.hasNext()) {
						boolean status = true;
						try {
							statement.executeBatch();
							con.commit();
						} catch (Exception e) {
							status = true;
							if (!StringUtils.containsAny(e.getMessage(), "duplicate key in object")) {
								status = false;
								log.error("Failed to insert invoice for bookingIds {} due to ", sqlsLogs.keySet(), e);
							}
							for (String key : sqlsLogs.keySet()) {
								LogUtils.log(key, "RailAccounting", e);
							}
						} finally {
							statement.clearBatch();
							for (DbOrder order_local : ordersInBatch) {
								order_local.getAdditionalInfo().setInvoicePushStatus(status);
							}
							orderService.saveWithoutProcessedOn(ordersInBatch);
							ordersInBatch.clear();
						}
						accountingUtils.logSqls(sqlsLogs, status);
						sqlsLogs.clear();
					}
				} catch (Exception e) {
					log.error(
							"Failed to insert invoice for bookingIds {}. Trying to build connections again. Reason: {} ",
							sqlsLogs.keySet(), e.getMessage(), e);
					for (String key : sqlsLogs.keySet()) {
						LogUtils.log(key, "RailAccounting", e);
					}
					accountingUtils.tryClosingResources(con, statement);
					con = accountingUtils.getConnection();
					statement = con.createStatement();
				}

			}

		} catch (Exception e) {
			log.error("Failed to insert invoice for orderFilter {} due to {}", orderFilter, e);
		} finally {
			LogUtils.clearLogList();
			accountingUtils.tryClosingResources(con, statement);
		}
	}

	private Map<DbOrder, DbRailOrderItem> groupOrderItems(List<Object[]> orderObjectsList) {
		Map<DbOrder, DbRailOrderItem> orderItemsGroup = new HashMap<>();
		for (Object[] orderObjects : orderObjectsList) {
			DbRailOrderItem orderItem = (DbRailOrderItem) orderObjects[0];
			DbOrder order = (DbOrder) orderObjects[1];
			orderItemsGroup.put(order, orderItem);
		}
		return orderItemsGroup;
	}

	private List<String> createSqlsFor(DbOrder order, DbRailOrderItem railOrderItem) {
		if (order == null) {
			return null;
		}
		List<String> sqlList = new ArrayList<>();
		try {
			final int travellersCount = railOrderItem.getTravellerInfo().size();
			int srno = 1;
			for (int i = 0; i < travellersCount; i++) {
				sqlList.add(
						statementPreparation.prepareInsertSql(dbConf.getRailTableName(), order, railOrderItem, srno));
				srno++;
			}
		} catch (Exception e) {
			log.error("Failed to prepare invoice statements for bookingId {} due to {} ", order.getBookingId(),
					e.getMessage(), e);
			LogUtils.log(order.getBookingId(), "RailAccounting", e);
			return null;
		}
		return sqlList;
	}


}
