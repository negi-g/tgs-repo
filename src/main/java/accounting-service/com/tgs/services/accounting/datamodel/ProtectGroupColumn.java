package com.tgs.services.accounting.datamodel;

import java.util.StringJoiner;
import lombok.Getter;

@Getter
public enum ProtectGroupColumn {

	DOC_PRF(2, true),
	DOC_NOS(7, true),
	DOC_SRNO(3, true),
	IDM_FLAG(1),
	IL_REF(19),
	VD_REF(19),
	IDATE,
	CCODE(6),
	DCODE(6),
	ECODE(6),
	BCODE(6),
	NARRATION(35),
	XO_REF(1),
	LOC_CODE(3),
	CST_CODE(3),
	Curcode_C(3),
	Curcode_S(3),
	REFR_KEY(10),
	GCODE(6),
	SCODE(6),
	XO_NOS(9),
	SRV_DATE,
	TICKETNO(11),
	PAX(30),
	STX_CENVAT,
	STX_METHOD,
	NOS_PAX_A,
	NOS_PAX_C,
	NOS_PAX_I,
	NARR_1,
	NARR_2,
	NARR_3,
	NARR_4,
	NARR_5,
	NARR_6,
	R_O_E_C,
	R_O_E_S,
	BASIC_C,
	BASIC_S,
	TAX_C,
	TAX_S,
	DISC_PAIDM1(2),
	DISC_PAIDV1,
	DISC_PAID1,
	DISC_PAIDM2(3),
	DISC_RECDM1(2),
	DISC_RECDV1,
	DISC_RECD1,
	BROK_PAIDM1(2),
	BROK_PAIDV1,
	BROK_PAID1,
	SRV_CHRG1C,
	SRV_PAIDM2(1),
	SRV_CHRG2C,
	SRV_CHRG2_H(1),
	SRV_CHRG3C,
	SRV_CHRG3_H(1),
	SRV_CHRG1P,
	SRV_CHRG2P,
	SRV_CHRG3P,
	TDC_PAIDV1,
	TDS_C,
	TDS_PAIDV1,
	TDS_P,
	TDB_PAIDV1,
	TDS_B,
	GST_C_M,
	GST_C_R,
	SERV_TAXC,
	SERV_EDUC,
	SERV_CS1C,
	GST_S_M,
	GST_S_R,
	SERV_TAXP,
	SERV_EDUP,
	SERV_CS1P,
	Created_By(15),
	Created_On,
	RAF_C,
	RAF_P,
	GST_TYPE,
	SAC_CODE1,
	BASIC_NONACC,
	TDS1_CRate,
	TDS1_C,
	Pay_Type(1),
	SCODE_B(6);

	// All columns separated by comma
	public final static String ALL;

	private int length;
	private boolean notNull;

	static {
		StringJoiner sj = new StringJoiner(", ");
		for (ProtectGroupColumn column : ProtectGroupColumn.values()) {
			sj.add(column.name());
		}
		ALL = sj.toString();
	}

	ProtectGroupColumn() {
		this(-1);
	}

	ProtectGroupColumn(int maxLength) {
		this(maxLength, false);
	}

	ProtectGroupColumn(boolean notNull) {
		this(-1, notNull);
	}

	ProtectGroupColumn(int length, boolean notNull) {
		this.length = length;
		this.notNull = notNull;
	}

	public boolean hasLength() {
		return length >= 0;
	}
}
