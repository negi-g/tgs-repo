package com.tgs.services.accounting.utils;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.datamodel.OrderColumn;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.oms.datamodel.air.AirOrderItem;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class AirAccountingUtils {
	
	public void setString(Map<OrderColumn, CharSequence> statementParams, OrderColumn column, String str) {
		if (column.hasLength()) {
			str = StringUtils.left(str, column.getLength());
		}
		str = StringUtils.defaultString(str).replace("'", "''");
		statementParams.put(column, new StringBuilder("'").append(str).append("'").toString());
	}

	public void setString(Map<OrderColumn, CharSequence> statementParams, String str, OrderColumn... columns) {
		for (OrderColumn column : columns) {
			setString(statementParams, column, str);
		}
	}

	public void setBigDecimal(Map<OrderColumn, CharSequence> statementParams, OrderColumn column, BigDecimal bigDecimal) {
		statementParams.put(column, bigDecimal == null ? null : bigDecimal.toString());
	}

	public void setBigDecimal(Map<OrderColumn, CharSequence> statementParams, BigDecimal bigDecimal, OrderColumn... columns) {
		for (OrderColumn column : columns) {
			setBigDecimal(statementParams, column, bigDecimal);
		}
	}

	public String getSector(List<AirOrderItem> airOrderItems) {
		StringBuilder sb = new StringBuilder();
		if (CollectionUtils.isEmpty(airOrderItems))
			return "";

		AirOrderItem orderItem = null;
		for (Iterator<AirOrderItem> iterator = airOrderItems.iterator(); iterator.hasNext();) {
			orderItem = iterator.next();
			sb.append(orderItem.getSource() + "/");
		}
		sb.append(orderItem.getDest());
		return sb.toString();
	}
	
	public String getPaxName(TravellerInfo travellerInfo) {
		String paxName = new StringBuilder(StringUtils.defaultString(travellerInfo.getTitle())).append(" ")
				.append(StringUtils.defaultString(travellerInfo.getFirstName())).append(" ")
				.append(StringUtils.defaultString(travellerInfo.getLastName())).toString();
		return paxName;
	}
}
