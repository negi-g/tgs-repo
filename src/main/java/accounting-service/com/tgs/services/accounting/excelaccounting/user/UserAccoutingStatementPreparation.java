package com.tgs.services.accounting.excelaccounting.user;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.datamodel.UserColumn;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.ums.datamodel.CinInfo;
import com.tgs.services.ums.datamodel.Directors;
import com.tgs.services.ums.datamodel.GSTInfo;
import com.tgs.services.ums.datamodel.User;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class UserAccoutingStatementPreparation extends AbstractUserAccountingStatementPreparation {

	@Autowired
	FMSCommunicator fmsCommunicator;

	private Map<UserColumn, CharSequence> statementParams = new HashMap<>();

	private static Map<String, String> gstStateMap;

	// Instantiating the static map
	static {
		gstStateMap = new HashMap<>();
		gstStateMap.put("ANDHRA PRADESH (NEW)", "SAD37");
		gstStateMap.put("ANDHRA PRADESH", "SAP28");
		gstStateMap.put("ARUNACHAL PRADESH", "SAR12");
		gstStateMap.put("ASSAM", "SAS18");
		gstStateMap.put("BIHAR", "SBH10");
		gstStateMap.put("CHATTISGARH", "SCT22");
		gstStateMap.put("GOA", "SGA30");
		gstStateMap.put("GUJARAT", "SGJ24");
		gstStateMap.put("HIMACHAL PRADESH", "SHP02");
		gstStateMap.put("HARYANA", "SHR06");
		gstStateMap.put("JHARKHAND", "SJH20");
		gstStateMap.put("JAMMU & KASHMIR", "SJK01");
		gstStateMap.put("KARNATAKA", "SKA29");
		gstStateMap.put("KERALA", "SKL32");
		gstStateMap.put("MEGHALAYA", "SME17");
		gstStateMap.put("MAHARASHTRA", "SMH27");
		gstStateMap.put("MIZORAM", "SMI15");
		gstStateMap.put("MANIPUR", "SMN14");
		gstStateMap.put("MADHYA PRADESH", "SMP23");
		gstStateMap.put("NAGALAND", "SNL13");
		gstStateMap.put("ODISHA", "SOR21");
		gstStateMap.put("PUNJAB", "SPB03");
		gstStateMap.put("RAJASTHAN", "SRJ08");
		gstStateMap.put("SIKKIM", "SSK11");
		gstStateMap.put("TAMILNADU", "STN33");
		gstStateMap.put("TRIPURA", "STR16");
		gstStateMap.put("TELANGANA", "STS36");
		gstStateMap.put("UTTAR PRADESH", "SUP09");
		gstStateMap.put("UTTARAKHAND", "SUT05");
		gstStateMap.put("WEST BENGAL", "SWB19");
		gstStateMap.put("ANDAMAN & NICOBAR ISLAND", "UAN35");
		gstStateMap.put("CHANDIGARH", "UCH04");
		gstStateMap.put("DAMAN & DIU", "UDD25");
		gstStateMap.put("DELHI", "UDL07");
		gstStateMap.put("DADRA & NAGAR HAVELI", "UDN26");
		gstStateMap.put("LAKSHADWEEP ISLANDS", "ULD31");
		gstStateMap.put("PONDICHERRY", "UPY34");
	}


	@Override
	protected Map<UserColumn, CharSequence> getColumnValues(User user) {
		statementParams.clear();
		setString("B96500", UserColumn.COCODE);
		setString("", UserColumn.GNAME_1, UserColumn.FAXNO, UserColumn.EMAILNO_1, UserColumn.C_ITNO_2,
				UserColumn.C_LOGINNAME, UserColumn.C_LOGINPWD, UserColumn.CREATED_BY, UserColumn.AGENCY_ID);
		setString("PORTAL CUSTOMER", UserColumn.NAVCODE);
		setString("91", UserColumn.MOBILENO_1);
		setString("A", UserColumn.STATUS_DL);
		setString("-1", UserColumn.CREDIT_LIMIT);
		setString("Y", UserColumn.CHRG_TDS);
		setString(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), UserColumn.CREATED_ON);
		setString("N", UserColumn.LOCK_BILL, UserColumn.PRINT_SC1, UserColumn.PRINT_SC2, UserColumn.PRINT_SC3);
		setString("000", UserColumn.CATG_CODE, UserColumn.FAML_CODE, UserColumn.SALM_CODE, UserColumn.COLL_CODE,
				UserColumn.DISC_CODE);

		setString(StringUtils.join(user.getName(), "(", user.getUserId(), ")"), UserColumn.GNAME);
		setString(user.getEmail(), UserColumn.EMAILNO);
		setString(user.getMobile(), UserColumn.MOBILENO_2, UserColumn.TELNO);

		CinInfo cinInfo = user.getAdditionalInfo().getCinInfo();
		setString(cinInfo != null ? cinInfo.getCinNumber() : "", UserColumn.C_MESG_1);
		Directors director =
				cinInfo != null && CollectionUtils.isNotEmpty(cinInfo.getDirectors()) ? cinInfo.getDirectors().get(0)
						: null;
		setString(director != null ? director.getDin_number() : "", UserColumn.C_MESG_2);

		AddressInfo addressInfo = user.getAddressInfo();
		String address = addressInfo != null ? addressInfo.getAddress() : "";
		String state = addressInfo.getCityInfo().getState();
		if (address.length() <= 50) {
			setString(address, UserColumn.ADDRESS1);
		} else {
			String address1 = StringUtils.left(address, 50);
			setString(address1, UserColumn.ADDRESS1);
			String address2 = StringUtils.substring(address, 50, address.length());
			setString(StringUtils.left(address2, 50), UserColumn.ADDRESS2);
		}
		setString(addressInfo.getCityInfo().getName(), UserColumn.ADDRESS3);
		setString(gstStateMap.getOrDefault(state.toUpperCase(), "00000"), UserColumn.GST_STATE);

		GSTInfo gstInfo = user.getGstInfo();
		String gstNumber = gstInfo != null && gstInfo.getGstNumber() != null ? gstInfo.getGstNumber() : "";
		setString(gstNumber, UserColumn.GST_NUMBER);

		String panNumber =
				user.getPanInfo() != null && user.getPanInfo().getNumber() != null ? user.getPanInfo().getNumber() : "";
		setString(panNumber, UserColumn.C_ITNO_1);

		if ("proprietor".equals(user.getAdditionalInfo().getFirmType())
				|| "partnership".equals(user.getAdditionalInfo().getFirmType())) {
			String panName =
					user.getPanInfo() != null && user.getPanInfo().getPanName() != null ? user.getPanInfo().getPanName()
							: "";
			setString(panName, UserColumn.CONPER);
		} else {
			setString("", UserColumn.CONPER);
		}

		String accountingCode = RandomStringUtils.randomAlphanumeric(5).toUpperCase();
		setString("C".concat(accountingCode), UserColumn.GCODE);
		user.getAdditionalInfo().setAccountingCode(accountingCode);

		List<AirportInfo> airportInfo = fmsCommunicator.fetchByCity(addressInfo.getCityInfo().getName());
		String airportCode = CollectionUtils.isNotEmpty(airportInfo) ? airportInfo.get(0).getCode() : "ZZZ";
		setString(airportCode, UserColumn.CITY_CODE);
		return statementParams;
	}

	protected final void setString(String str, UserColumn... columns) {
		setString(statementParams, str, columns);
	}

	public void setString(Map<UserColumn, CharSequence> statementParams, String str, UserColumn... columns) {
		for (UserColumn column : columns) {
			setString(statementParams, column, str);
		}
	}

	public void setString(Map<UserColumn, CharSequence> statementParams, UserColumn column, String str) {
		if (column.hasLength()) {
			str = StringUtils.left(str, column.getLength());
		}
		str = StringUtils.defaultString(str).replace("'", "''");
		statementParams.put(column, new StringBuilder("'").append(str).append("'").toString());
	}
}
