package com.tgs.services.accounting.excelaccounting.hotel;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class CleartripExcelAccountingStatementPreparation extends AbstractHotelExcelAccounting {

	public CleartripExcelAccountingStatementPreparation(DbOrder order, List<DbHotelOrderItem> hotelOrderItems) {
		super(order, hotelOrderItems);
	}

	@Override
	protected BigDecimal getSRV_CHRG1P() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getDISC_PAIDV1() {
		BigDecimal totalSupplierGrossPrice = getTotalPriceFromComponenet(HotelFareComponent.SGP);
		BigDecimal totalNetPrice = getTotalPriceFromComponenet(HotelFareComponent.NF);
		BigDecimal totalServiceCharge = getTotalPriceFromComponenet(HotelFareComponent.MF);
		BigDecimal totalGST = getTotalPriceFromComponenet(HotelFareComponent.CGST)
				.add(getTotalPriceFromComponenet(HotelFareComponent.SGST))
				.add(getTotalPriceFromComponenet(HotelFareComponent.IGST));
		return totalSupplierGrossPrice.subtract(totalNetPrice.subtract(totalServiceCharge).subtract(totalGST));
	}

	@Override
	protected BigDecimal getDISC_RECDV1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getDISC_PAID1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getDISC_RECD1() {
		return BigDecimal.ZERO;
	}

	@Override
	protected BigDecimal getSRV_CHRG2C() {
		BigDecimal totalNetPrice = getTotalPriceFromComponenet(HotelFareComponent.NF);
		BigDecimal totalServiceCharge = getTotalPriceFromComponenet(HotelFareComponent.MF);
		BigDecimal totalGST = getTotalPriceFromComponenet(HotelFareComponent.CGST)
				.add(getTotalPriceFromComponenet(HotelFareComponent.SGST))
				.add(getTotalPriceFromComponenet(HotelFareComponent.IGST));
		return totalNetPrice.subtract(totalServiceCharge).subtract(totalGST);
	}

	@Override
	protected BigDecimal getTDC_PAIDV1() {
		return BigDecimal.valueOf(5);
	}

	@Override
	protected BigDecimal getTDS_C() {
		BigDecimal price = getDISC_PAID1();
		BigDecimal tds = getTDC_PAIDV1();
		return price.multiply(tds).divide(BigDecimal.valueOf(100));
	}

	@Override
	protected BigDecimal getTDS_PAIDV1() {
		return BigDecimal.valueOf(5);
	}

	@Override
	protected BigDecimal getTDS_P() {
		BigDecimal price = getDISC_RECD1();
		return price.multiply(BigDecimal.valueOf(5)).divide(BigDecimal.valueOf(100));
	}

	@Override
	protected String getPay_Type() {
		return "";
	}

}
