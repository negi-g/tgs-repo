package com.tgs.services.accounting.excelaccounting.air;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.datamodel.SupplierOrderColumn;
import com.tgs.services.accounting.datamodel.VarargsFunction;
import com.tgs.services.accounting.utils.AirAccountingUtils;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.gms.datamodel.InvoiceIdData;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.air.AirOrderItemFilter;
import com.tgs.services.oms.datamodel.air.TravellerInfoFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.AbstractInvoiceIdManager;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class QuotaAirExcelAccountingStatementPreparation extends AbstractQuotaAirExcelAccountingStatementPreparation {

	@Autowired
	private UserServiceCommunicator usCommunicator;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private AbstractInvoiceIdManager invoiceIdManager;

	@Autowired
	protected AirAccountingUtils airAccountingUtils;

	@Autowired
	private GstInfoService gstService;

	private Map<SupplierOrderColumn, CharSequence> statementParams = new HashMap<>();

	@Override
	protected Map<SupplierOrderColumn, CharSequence> getColumnValues(DbOrder order, List<DbAirOrderItem> airOrderItems,
			int serialNo, int pnrCountAlreadyPushed) {
		statementParams.clear();

		ProductMetaInfo productMetaInfo = Product.getProductMetaInfoFromId(order.getBookingId());

		Order domainOrder = order.toDomain();

		DbOrderBilling orderBilling = gstService.findByBookingId(order.getBookingId());

		final User bookingUser = usCommunicator.getUserFromCache(order.getBookingUserId());

		List<AirOrderItem> domainOrderItems = DbAirOrderItem.toDomainList(airOrderItems);

		int paxIndex = (serialNo - 1) % airOrderItems.get(0).getTravellerInfo().size();

		final FlightTravellerInfo travellerInfo = airOrderItems.get(0).getTravellerInfo().get(paxIndex);

		boolean isCreditShellUsed =
				(BaseUtils.calculateAvailablePNRCredit(travellerInfo.getFareDetail()) > 0) ? true : false;

		String invoiceId = null;
		if (UserRole.corporate(bookingUser.getRole())) {
			invoiceId = travellerInfo.getInvoice();
			if (StringUtils.isBlank(invoiceId)) {
				invoiceId = invoiceIdManager.getInvoiceIdFor(domainOrder, productMetaInfo,
						airOrderItems.get(0).getSupplierId());
				travellerInfo.setInvoice(invoiceId);
				airOrderItemService.save(airOrderItems);
			}
		} else {
			invoiceId = order.getAdditionalInfo().getInvoiceId();
			if (StringUtils.isBlank(invoiceId)) {
				invoiceId = invoiceIdManager.getInvoiceIdFor(domainOrder, productMetaInfo,
						airOrderItems.get(0).getSupplierId());
				order.getAdditionalInfo().setInvoiceId(invoiceId);
			}
		}

		// pricing for given traveller
		final Map<FareComponent, Double> fareComponents =
				AirBookingUtils.getMergedComponents(domainOrderItems, paxIndex);

		// BigDecimal behaves abnormally when used with double. Use String instead.
		final Function<Double, BigDecimal> getDoubleAsBigDecimal =
				doubleVal -> new BigDecimal(doubleVal.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final VarargsFunction<FareComponent, Double> getFareComponentsAsDouble = components -> {
			double sum = 0.0;
			for (FareComponent fareComponent : components) {
				sum += fareComponents.getOrDefault(fareComponent, 0.0);
			}
			return sum;
		};

		// Get sum of all FareComponents as BigDecimal
		final VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal = components -> {
			return getDoubleAsBigDecimal.apply(getFareComponentsAsDouble.apply(components));
		};

		// Airline Code + flightNumber + Class of Booking + O + TRAVELDATE
		final Function<Integer, String> getFlightDetail = orderItemIndex -> {
			if (orderItemIndex < domainOrderItems.size()) {
				AirOrderItem orderItem = domainOrderItems.get(orderItemIndex);
				String classOfBooking = orderItem.getTravellerInfo().get(paxIndex).getFareDetail().getClassOfBooking();
				classOfBooking = StringUtils.defaultString(StringUtils.left(classOfBooking, 1));
				// FLIGHT CODE + ONE SPACE +FTF + ONE SPACE+ alphabet O followed by TRAVEL DATE
				return new StringBuilder(orderItem.getPlatingCarrier()).append(" FTF ").append("O").append(
						orderItem.getDepartureTime().toLocalDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
						.toString();
			}
			return "";
		};

		final PaxType paxType = travellerInfo.getPaxType();

		final AirlineInfo airlineInfo = fmsCommunicator.getAirlineInfo(domainOrderItems.get(0).getPlatingCarrier());

		final boolean isDomestic = AirType.DOMESTIC.getName().equals(productMetaInfo.getSubProduct());

		setString("", SupplierOrderColumn.CRS_ID, SupplierOrderColumn.FARE_BASIS, SupplierOrderColumn.DEAL_CODE,
				SupplierOrderColumn.FLT_DTLS2, SupplierOrderColumn.FLT_DTLS3, SupplierOrderColumn.FLT_DTLS4,
				SupplierOrderColumn.Pay_Type, SupplierOrderColumn.Stax_PayBy, SupplierOrderColumn.BCODE,
				SupplierOrderColumn.NARRATION_6, SupplierOrderColumn.NARRATION_5, SupplierOrderColumn.PAX_DTLS,
				SupplierOrderColumn.IL_REF, SupplierOrderColumn.C_Loginname, SupplierOrderColumn.VD_REF,
				SupplierOrderColumn.DCODE, SupplierOrderColumn.ECODE, SupplierOrderColumn.REFR_KEY); // empty

		setString("VL", SupplierOrderColumn.DISC_PAIDM1); // fixed

		setString("RBN", SupplierOrderColumn.DISC_PAIDM2, SupplierOrderColumn.DISC_PAIDM3); // fixed

		setBigDecimal(BigDecimal.ONE, SupplierOrderColumn.R_O_E_C); // fixed

		setBigDecimal(BigDecimal.ZERO, SupplierOrderColumn.TAX_4, SupplierOrderColumn.TAX_5, SupplierOrderColumn.TAX_1,
				SupplierOrderColumn.DISC_PAID1, SupplierOrderColumn.SRV_CHRG2C, SupplierOrderColumn.SRV_CHRG3C,
				SupplierOrderColumn.TDS_C, SupplierOrderColumn.SERV1_CS1C, SupplierOrderColumn.SERV1_EDUC,
				SupplierOrderColumn.SERV1_TAXC, SupplierOrderColumn.TAXABLE_S_3, SupplierOrderColumn.TAXABLE_S_2,
				SupplierOrderColumn.TAXABLE_S_1, SupplierOrderColumn.SERV3_CS2C, SupplierOrderColumn.SERV2_CS2C,
				SupplierOrderColumn.SERV2_CS1C, SupplierOrderColumn.BASIC_PBL, SupplierOrderColumn.SERV1_CS2C,
				SupplierOrderColumn.SERV_CS2C, SupplierOrderColumn.TAX_8, SupplierOrderColumn.TAX_7,
				SupplierOrderColumn.DISC_PAIDV1, SupplierOrderColumn.CST_CODE, SupplierOrderColumn.TAX_4,
				SupplierOrderColumn.TAX_6, SupplierOrderColumn.DISC_PAIDV2, SupplierOrderColumn.DISC_PAIDV3,
				SupplierOrderColumn.BROK_PAIDV1, SupplierOrderColumn.DISC_PAID2, SupplierOrderColumn.DISC_PAID3,
				SupplierOrderColumn.BROK_PAID1, SupplierOrderColumn.SRV_CHRG2C, SupplierOrderColumn.SRV_CHRG4C,
				SupplierOrderColumn.SRV_CHRG5C, SupplierOrderColumn.RAF_C, SupplierOrderColumn.SERV_TAXC,
				SupplierOrderColumn.SERV_EDUC, SupplierOrderColumn.SERV_CS1C, SupplierOrderColumn.TDC_PAIDV1,
				SupplierOrderColumn.TDB_PAIDV1, SupplierOrderColumn.TDS_B, SupplierOrderColumn.XXL_C,
				SupplierOrderColumn.SERV3_TAXC, SupplierOrderColumn.SERV3_EDUC, SupplierOrderColumn.SERV3_CS1C,
				SupplierOrderColumn.DISC_PAIDV5, SupplierOrderColumn.DISC_PAID5); // fixed

		setString("INR", SupplierOrderColumn.Curcode_C); // fixed

		final InvoiceIdData invoiceIdData = invoiceIdManager.getInvoiceIdData(invoiceId, domainOrder, productMetaInfo);
		setString(SupplierOrderColumn.DOC_PRF, "QW");
		setString(SupplierOrderColumn.DOC_NOS, String.valueOf(invoiceIdData.getNumericInvoiceId()));
		setString(SupplierOrderColumn.DOC_SRNO, String.format("%03d", serialNo)); // 3-digit srno
		setString(SupplierOrderColumn.IDM_FLAG, isDomestic ? "D" : "I");

		String clientCode = bookingUser.getAdditionalInfo().getAccountingCode();
		if (StringUtils.isBlank(clientCode)) {
			clientCode = "CT01ET";
		} else {
			clientCode = "C".concat(clientCode);
		}
		setString(SupplierOrderColumn.CCODE, clientCode);

		setString(SupplierOrderColumn.LOC_CODE, "000"); // fixed
		if (UserUtils.isCorporate(bookingUser)) {
			if (orderBilling != null) {
				setString(SupplierOrderColumn.CCODE,
						(StringUtils.isNotBlank(orderBilling.getInfo().getAccountCode()))
								? "C".concat(orderBilling.getInfo().getAccountCode())
								: clientCode);
			}
		}
		setString(SupplierOrderColumn.ACODE, getACODE(airlineInfo));
		setString(SupplierOrderColumn.XO_REF, "E");
		setString(SupplierOrderColumn.NARRATION, order.getBookingId());
		setString(SupplierOrderColumn.PNR_NO, travellerInfo.getPnr());

		boolean isReissuedCartWithSamePnr = false;
		if (OrderFlowType.REISSUED.equals(order.getAdditionalInfo().getFlowType())) {
			String oldBookingId = travellerInfo.getOldBookingId();
			String pnr = travellerInfo.getPnr();
			AirOrderItemFilter airOrderFilter = AirOrderItemFilter.builder()
					.travellerInfoFilter(TravellerInfoFilter.builder().pnr(pnr).build()).build();
			OrderFilter filter = OrderFilter.builder().products(Arrays.asList(OrderType.AIR)).bookingId(oldBookingId)
					.createdOnAfterDate(LocalDate.now().minusMonths(5)).itemFilter(airOrderFilter).build();
			List<Object[]> orderObjectsList = airOrderItemService.findByJsonSearch(filter);
			isReissuedCartWithSamePnr = CollectionUtils.isNotEmpty(orderObjectsList);
		}

		String ticketNo = travellerInfo.getTicketNumber();

		if (StringUtils.isBlank(ticketNo)) {
			ticketNo = new StringBuilder(travellerInfo.getPnr()).append("-").append(serialNo).toString();
			ticketNo = getTicketNumber(ticketNo, pnrCountAlreadyPushed, isReissuedCartWithSamePnr);
		} else {
			/**
			 * In order to handle conjunction numbers, Conjunction Tickets are those which consist of more than 4
			 * segments. It consist of "-" hypen or "/" slash
			 */
			if (ticketNo.contains("-")) {
				ticketNo = ticketNo.substring(0, ticketNo.indexOf("-"));
			}
			if (ticketNo.contains("/")) {
				ticketNo = ticketNo.substring(0, ticketNo.indexOf("/"));
			}
		}
		setString(SupplierOrderColumn.TICKETNO, StringUtils.right(ticketNo, 10));

		String paxName = airAccountingUtils.getPaxName(travellerInfo);
		setString(SupplierOrderColumn.PAX, paxName);
		setString(SupplierOrderColumn.SECTOR, airAccountingUtils.getSector(domainOrderItems));
		setBigDecimal(SupplierOrderColumn.NOS_PAX_A, PaxType.ADULT.equals(paxType) ? BigDecimal.ONE : BigDecimal.ZERO);
		setString(SupplierOrderColumn.FLT_DTLS1, getFlightDetail.apply(0));
		setBigDecimal(SupplierOrderColumn.BASIC_FARE, getFareComponentsAsBigDecimal.apply(FareComponent.BF));
		setBigDecimal(SupplierOrderColumn.TAX_2, getFareComponentsAsBigDecimal.apply(FareComponent.AGST));

		if (isCreditShellUsed) {
			setBigDecimal(SupplierOrderColumn.TAX_3, BigDecimal.ZERO);

			BigDecimal airlineFare = getFareComponentsAsBigDecimal.apply(Arrays.stream(FareComponent.values())
					.filter(FareComponent::airlineComponent).toArray(length -> new FareComponent[length]));
			BigDecimal creditShellBalance = getFareComponentsAsBigDecimal.apply(FareComponent.CS);
			if (airlineFare.compareTo(creditShellBalance) > 0) {
				setBigDecimal(SupplierOrderColumn.TAX_3, airlineFare.subtract(creditShellBalance));
			}
		} else {
			setBigDecimal(SupplierOrderColumn.TAX_3,
					getTax3(getFareComponentsAsBigDecimal, airOrderItems.get(0).getAdditionalInfo().getSourceId()));
		}

		setBigDecimal(SupplierOrderColumn.SRV_CHRG1C,
				getFareComponentsAsBigDecimal.apply(FareComponent.MF, FareComponent.CMU));

		setString(SupplierOrderColumn.BROK_PAIDM1, "RB");
		setString(SupplierOrderColumn.Created_By, "null"); // fixed
		setString("RDG", SupplierOrderColumn.DISC_PAIDM5); // fixed
		setString(SupplierOrderColumn.IDATE,
				airOrderItems.get(0).getCreatedOn().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		setString(SupplierOrderColumn.SaleDATE,
				airOrderItems.get(0).getCreatedOn().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		setString(SupplierOrderColumn.SRV_CHRG1_H, "N");
		setString(SupplierOrderColumn.SRV_CHRG2_H, "T");
		setString(SupplierOrderColumn.SRV_CHRG3_H, "N");
		setString("G", SupplierOrderColumn.GST_TYPE); // fixed
		setString("S", SupplierOrderColumn.GST_CalcOn);

		statementParams.put(SupplierOrderColumn.TDC_Index, "-1");

		return statementParams;
	}

	protected BigDecimal getTax3(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal,
			Integer sourceId) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.AT, FareComponent.UDF, FareComponent.PSF,
				FareComponent.RCF, FareComponent.OT, FareComponent.WO, FareComponent.WC, FareComponent.YM,
				FareComponent.OC, FareComponent.BP, FareComponent.MP, FareComponent.SP);
	}

	protected String getACODE(AirlineInfo airlineInfo) {
		String airlineCode = airlineInfo.getCode();
		switch (airlineCode) {
			case "G8":
				return airlineCode + "042";
			case "6E":
				return airlineCode + "009";
			case "SG":
				return airlineCode + "016";
			case "I5":
				return airlineCode + "003";
			case "UK":
				return airlineCode + "229";
			default:
				return airlineCode
						+ StringUtils.defaultString(StringUtils.leftPad(airlineInfo.getAccountingCode(), 3, '0'));
		}
	}

	protected final void setString(SupplierOrderColumn column, String str) {
		setString(statementParams, column, str);
	}

	protected final void setBigDecimal(SupplierOrderColumn column, BigDecimal bigDecimal) {
		setBigDecimal(statementParams, column, bigDecimal);
	}

	protected final void setString(String str, SupplierOrderColumn... columns) {
		setString(statementParams, str, columns);
	}

	protected final void setBigDecimal(BigDecimal bigDecimal, SupplierOrderColumn... columns) {
		setBigDecimal(statementParams, bigDecimal, columns);
	}

	public void setString(Map<SupplierOrderColumn, CharSequence> statementParams, String str,
			SupplierOrderColumn... columns) {
		for (SupplierOrderColumn column : columns) {
			setString(statementParams, column, str);
		}
	}

	public void setString(Map<SupplierOrderColumn, CharSequence> statementParams, SupplierOrderColumn column,
			String str) {
		if (column.hasLength()) {
			str = StringUtils.left(str, column.getLength());
		}
		str = StringUtils.defaultString(str).replace("'", "''");
		statementParams.put(column, new StringBuilder("'").append(str).append("'").toString());
	}

	public void setBigDecimal(Map<SupplierOrderColumn, CharSequence> statementParams, SupplierOrderColumn column,
			BigDecimal bigDecimal) {
		statementParams.put(column, bigDecimal == null ? null : bigDecimal.toString());
	}

	public void setBigDecimal(Map<SupplierOrderColumn, CharSequence> statementParams, BigDecimal bigDecimal,
			SupplierOrderColumn... columns) {
		for (SupplierOrderColumn column : columns) {
			setBigDecimal(statementParams, column, bigDecimal);
		}
	}

	protected String getTicketNumber(String ticketNo, int pnrCountAlreadyPushed, boolean isReissuedCartWithSamePnr) {
		// if same PNR has already been pushed like TEST-1, for next booking it should be TEST-11 (TEST-21,.. if
		// multiple pax) , for third booking TEST-12 , and so on...
		if (pnrCountAlreadyPushed > 0) {
			ticketNo = new StringBuilder(ticketNo).append(pnrCountAlreadyPushed).toString();
		}
		// In case of reissued bookings, if old pnr is same as new pnr, we need to append "R" to the new accounting row.
		if (isReissuedCartWithSamePnr) {
			ticketNo = ticketNo.concat("R");
		}
		return ticketNo;
	}

}
