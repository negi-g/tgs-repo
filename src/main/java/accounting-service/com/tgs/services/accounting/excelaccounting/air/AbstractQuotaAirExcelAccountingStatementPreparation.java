package com.tgs.services.accounting.excelaccounting.air;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import com.tgs.services.accounting.datamodel.SupplierOrderColumn;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;

public abstract class AbstractQuotaAirExcelAccountingStatementPreparation {
	
	final private static String INSERT_SQL_TEMPLATE = "INSERT INTO %s (" + SupplierOrderColumn.ALL + ") VALUES (%s)";
	
	/**
	 * 
	 * @param order
	 * @param airOrderItems with same PNR, for same supplier
	 * @param paxSerialNo Serial number of each entry/sql (starting from 1)
	 * @throws SQLException
	 */
	public String prepareInsertSql(String tableName, DbOrder order, List<DbAirOrderItem> airOrderItems, int serialNo,
			int pnrCountAlreadyPushed) throws SQLException {
		Map<SupplierOrderColumn, CharSequence> statementParams =
				getColumnValues(order, airOrderItems, serialNo, pnrCountAlreadyPushed);
		StringJoiner joiner = new StringJoiner(", ");
		for (SupplierOrderColumn column : SupplierOrderColumn.values()) {
			joiner.add(statementParams.get(column));
		}
		return String.format(INSERT_SQL_TEMPLATE, tableName, joiner.toString());
	}
	
	protected abstract Map<SupplierOrderColumn, CharSequence> getColumnValues(DbOrder order, List<DbAirOrderItem> airOrderItems,
			int serialNo, int pnrCountAlreadyPushed);

}
