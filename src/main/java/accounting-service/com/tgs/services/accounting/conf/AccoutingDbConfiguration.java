package com.tgs.services.accounting.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import lombok.Getter;

@Getter
@Component
public class AccoutingDbConfiguration {

	@Value("${air.accounting.database.server}")
	private String serverAddress;

    @Value("${air.accounting.database.name}")
	private String databaseName;

	@Value("${air.accounting.database.username}")
	private String username;

	@Value("${air.accounting.database.password}")
	private String password;

	@Value("${air.accounting.database.table}")
	private String tableName;

	@Value("${air.accounting.database.supplier.table}")
	private String supplierTableName;

	@Value("${air.accounting.database.user.table}")
	private String userTableName;

	@Value("${air.accounting.database.protect.table}")
	private String cancellationProtectionTableName;

	@Value("${rail.accounting.database.table}")
	private String railTableName;

	@Value("${hotel.accounting.database.table}")
	private String hotelTableName;
}
