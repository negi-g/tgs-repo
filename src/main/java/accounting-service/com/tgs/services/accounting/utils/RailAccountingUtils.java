package com.tgs.services.accounting.utils;

import java.math.BigDecimal;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.datamodel.RailOrderColumn;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class RailAccountingUtils {

	public void setString(Map<RailOrderColumn, CharSequence> statementParams, RailOrderColumn column, String str) {
		if (column.hasLength()) {
			str = StringUtils.left(str, column.getLength());
		}
		str = StringUtils.defaultString(str).replace("'", "''");
		statementParams.put(column, new StringBuilder("'").append(str).append("'").toString());
	}

	public void setString(Map<RailOrderColumn, CharSequence> statementParams, String str, RailOrderColumn... columns) {
		for (RailOrderColumn column : columns) {
			setString(statementParams, column, str);
		}
	}

	public void setBigDecimal(Map<RailOrderColumn, CharSequence> statementParams, RailOrderColumn column,
			BigDecimal bigDecimal) {
		statementParams.put(column, bigDecimal == null ? null : bigDecimal.toString());
	}

	public void setBigDecimal(Map<RailOrderColumn, CharSequence> statementParams, BigDecimal bigDecimal,
			RailOrderColumn... columns) {
		for (RailOrderColumn column : columns) {
			setBigDecimal(statementParams, column, bigDecimal);
		}
	}

}
