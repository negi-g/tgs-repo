package com.tgs.services.accounting.excelaccounting.amendment.rail;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.filters.AmendmentFilter;
import com.tgs.services.accounting.conf.AccoutingDbConfiguration;
import com.tgs.services.accounting.datamodel.OrderColumn;
import com.tgs.services.accounting.datamodel.RailOrderColumn;
import com.tgs.services.accounting.utils.AccountingUtils;
import com.tgs.services.accounting.utils.RailAccountingUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.Amendments.RailAmendmentManager;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import lombok.extern.slf4j.Slf4j;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
@Slf4j
public class RailAmendmentExcelAccounting {

	final private static int BATCH_SIZE = 1;

	@Autowired
	private AccoutingDbConfiguration dbConf;

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	private AccountingUtils accountingUtils;

	@Autowired
	protected RailAccountingUtils railAccountingUtils;

	@Autowired
	protected RailAmendmentAccountingStatementPreparation statementPreparation;

	private static List<AmendmentType> notToBePushedAmendments = Arrays.asList(AmendmentType.CORRECTION);


	public void pushInvoicesFor(AmendmentFilter filter) {
		if (filter == null) {
			return;
		}

		Connection con = null;
		Statement statement = null;
		try {
			List<Amendment> amendments = amendmentService.search(filter);
			log.debug("Amendment list size for rail amendment accounting is {} and amendment ids are {}",
					amendments.size(), amendments.size());
			Set<String> bookingIds = amendments.stream().map(a -> a.getBookingId()).collect(Collectors.toSet());
			log.debug("Booking ids found for rail amendment accounting are : {} ", bookingIds.toString());
			Map<String, Map<RailOrderColumn, CharSequence>> rowsData = getExistingRows(bookingIds);
			log.debug("Existing data for rail amendment accouting are : {} ", GsonUtils.getGson().toJson(rowsData));
			con = accountingUtils.getConnection();
			statement = con.createStatement();
			con.setAutoCommit(false);
			Map<String, List<String>> sqlsLogs = new HashMap<>();
			List<Amendment> amendmentsInBatch = new ArrayList<>();
			Iterator<Amendment> amendmentEntryItr = amendments.iterator();
			while (amendmentEntryItr.hasNext()) {
				Amendment amendment = null;
				try {
					amendment = amendmentEntryItr.next();
					if (!notToBePushedAmendments.contains(amendment.getAmendmentType())
							&& BooleanUtils.isNotTrue(amendment.getAdditionalInfo().getInvoicePushStatus())) {
						List<String> sqlList = createSqlsFor(amendment, rowsData);
						log.debug("Sqls prepared for rail amendment accouting for amendmentId {}, are {}",
								amendment.getAmendmentId(), sqlList.toString());
						sqlsLogs.put(amendment.getBookingId(), sqlList);
						for (String sql : sqlList) {
							statement.addBatch(sql);
							amendmentsInBatch.add(amendment);
						}
					}
					/**
					 * Execute batch if batch-size is crossed or if it is the last order.
					 */
					if (amendmentsInBatch.size() >= BATCH_SIZE || !amendmentEntryItr.hasNext()) {
						boolean status = true;
						try {
							statement.executeBatch();
							con.commit();
						} catch (Exception e) {
							status = false;
							log.error("Failed to insert rail amendment invoice for bookingIds {} due to {} ",
									sqlsLogs.keySet(), e.getMessage(), e);
							for (String key : sqlsLogs.keySet()) {
								LogUtils.log(key, "RailAmendmentAccounting", e);
							}
						} finally {
							/**
							 * Batch should be cleared even if it fails so that upcoming batches can be served.
							 */
							statement.clearBatch();
							for (Amendment order_local : amendmentsInBatch) {
								order_local.getAdditionalInfo().setInvoicePushStatus(status);
							}
							amendmentService.saveWithoutProcessedOn(amendmentsInBatch);
							amendmentsInBatch.clear();
						}
						accountingUtils.logSqls(sqlsLogs, status);
						sqlsLogs.clear();
					}
				} catch (CustomGeneralException e) {
					log.error("No RailTraveller Found for  bookingId {}", amendment.getBookingId());
				} catch (Exception e) {
					log.error(
							"Failed to insert rail amendment invoice for bookingIds {}. Trying to build connections again. Reason: {}",
							sqlsLogs.keySet(), e.getMessage(), e);
					for (String key : sqlsLogs.keySet()) {
						LogUtils.log(key, "RailAmendmentAccounting", e);
					}
					accountingUtils.tryClosingResources(con, statement);
					con = accountingUtils.getConnection();
					statement = con.createStatement();
				}
			}
		} catch (Exception e) {
			log.error("Failed to insert rail amendment invoice for amendment Filter {} due to {}",
					GsonUtils.getGson().toJson(filter), e.getMessage(), e);
		} finally {
			LogUtils.clearLogList();
			accountingUtils.tryClosingResources(con, statement);
		}
	}


	private List<String> createSqlsFor(Amendment amendment, Map<String, Map<RailOrderColumn, CharSequence>> rowsData) {

		RailOrderItem modifiedRailItem = amendment.getModifiedInfo().getRailOrderItem();
		List<String> sqls = new ArrayList<>();
		Set<Long> paxIds = new HashSet<>();
		modifiedRailItem.getTravellerInfo().forEach(traveller -> {
			paxIds.add(traveller.getId());
		});

		AtomicInteger serialNo = new AtomicInteger(1);
		paxIds.forEach(paxId -> {
			RailOrderItem oldRailItem =
					amendment.getAdditionalInfo().getRailAdditionalInfo().getOrderPreviousSnapshot();

			RailTravellerInfo oldTravellerInfo =
					oldRailItem.getTravellerInfo().stream().filter(t -> t.getId().equals(paxId)).findAny().orElse(null);

			if (oldTravellerInfo == null) {
				throw new CustomGeneralException("No Traveller Found for rail amendment accounting ");
			}
			String paxName = oldTravellerInfo.getFullName();
			String key = getUniqueKey(amendment.getBookingId(), paxName);
			Map<RailOrderColumn, CharSequence> existingInvoiceData = rowsData.get(key);
			log.debug("Existing sale invoice data for key {} is {}", key, existingInvoiceData);

			if (existingInvoiceData == null)
				log.info("Invoice data not found in accounting package for key {}", key);
			else {
				try {
					String sql = statementPreparation.prepareInsertSql(dbConf.getRailTableName(), existingInvoiceData,
							amendment, paxId, serialNo.get());
					log.debug("Sql prepared for rail amendment accounting is {} ", sql);
					sqls.add(sql);
				} catch (SQLException e) {
					log.error("Failed to prepare rail amendment invoice data for amendment ids {} pax id {} due to {}",
							amendment.getAmendmentId(), paxId, e.getMessage(), e);
				}
			}
			serialNo.getAndIncrement();

		});
		return sqls;
	}


	private Map<String, Map<RailOrderColumn, CharSequence>> getExistingRows(Set<String> bookingIds)
			throws SQLException {


		Connection con = null;
		Statement statement = null;
		Map<String, Map<RailOrderColumn, CharSequence>> rowDataPerKey = new HashMap<>();
		try {
			con = accountingUtils.getConnection();
			statement = con.createStatement();
			con.setAutoCommit(false);
			String selectStmt = statementPreparation.prepareSelectSql(dbConf.getRailTableName(), bookingIds);
			log.debug("Select statement for rail amendment accounting is :  {} ", selectStmt);
			ResultSet resultSet = statement.executeQuery(selectStmt);
			log.debug("Resultset size  for rail amendment accounting is{} ", resultSet.getFetchSize());
			while (resultSet.next()) {
				try {
					Map<RailOrderColumn, CharSequence> rowData = resultSetToHashMap(resultSet);
					log.debug("Rowdata in the result set for rail amendment accouting is :  {} ",
							GsonUtils.getGson().toJson(rowData));
					String bookingId = (String) rowData.get(RailOrderColumn.NARRATION);
					String paxName = (String) rowData.get(RailOrderColumn.PAX);
					/**
					 * So the value that is return by the Column will have additional "'{bookingId}'" at front and back,
					 * so we have to remove those.
					 *
					 */
					String key = getUniqueKey(bookingId.substring(1, bookingId.length() - 1),
							paxName.substring(1, paxName.length() - 1));
					log.debug("Unique key generated for rail amendment accounting is : {} ", key);
					rowDataPerKey.put(key, rowData);
				} catch (Exception e) {
					log.error("Unable to convert result set for rail amendment accounting for selectStmt {} due to {} ",
							selectStmt, e.getMessage(), e);
				}
			}
		} finally {
			accountingUtils.tryClosingResources(con, statement);
		}
		return rowDataPerKey;
	}

	private String getUniqueKey(String bookingId, String paxName) {
		return bookingId.trim().concat("_").concat(paxName);
	}

	public Map<RailOrderColumn, CharSequence> resultSetToHashMap(ResultSet rs) throws SQLException {
		ResultSetMetaData metadata = rs.getMetaData();
		int columns = metadata.getColumnCount();
		log.debug("Column count for rail amendment accounting is :  {}", columns);
		Map<RailOrderColumn, CharSequence> row = new HashMap<>(columns);
		for (int i = 1; i <= columns; i++) {
			log.debug("Column name for metadata for rail amendment accounting is : {} ", metadata.getColumnName(i));
			RailOrderColumn column = null;
			try {
				column = RailOrderColumn.valueOf(metadata.getColumnName(i));
			} catch (Exception e) {
				log.debug("No mapping found for rail amendment accounting {} ", metadata.getColumnName(i));
			}
			// Avoid extra columns not known to us
			if (column != null) {
				row.put(column,
						rs.getObject(i) != null ? "'".concat(String.valueOf(rs.getObject(i))).concat("'") : "''");
			}
		}
		return row;
	}


}
