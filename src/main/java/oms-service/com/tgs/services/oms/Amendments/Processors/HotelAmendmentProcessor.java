package com.tgs.services.oms.Amendments.Processors;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.Amendments.HotelAmendmentManager;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.hotel.HotelModifyBookingManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.manager.hotel.HotelPostBookingManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.hotel.HotelAmendOrderRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Getter
@Setter
@Slf4j
public abstract class HotelAmendmentProcessor implements AmendmentProcessor {

	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	protected AmendmentActionValidator actionValidator;

	@Autowired
	HotelModifyBookingManager modifyBookingManager;

	@Autowired
	HotelAmendmentManager amendmentManager;

	@Autowired
	HotelPostBookingManager manualOrderManager;

	@Autowired
	OrderService orderService;

	@Autowired
	HotelOrderItemManager itemManager;

	protected HotelAmendOrderRequest hotelOrderRequest;

	protected Amendment amendment;

	protected String requestNote;

	public void process() {
		validate();
		processAmendment();
	}

	protected abstract void processAmendment();

	protected abstract void validate();

	protected Double getDifferentialPayment(Amendment amendment, HotelInfo modifiedHotelInfo) {

		Amendment dummyAmendment = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(amendment), Amendment.class);
		amendmentManager.setCurrentOrderSnapshotAfterProcessing(dummyAmendment, modifiedHotelInfo);
		amendmentManager.setPreviousOrderSnapshotAfterProcessing(dummyAmendment, DbHotelOrderItemListToHotelInfo
				.builder().itemList(itemManager.getItemList(amendment.getBookingId())).build().convert());

		return amendmentManager.getAmendmentSnapshot(dummyAmendment).getOptions().get(0).getRoomInfos().stream()
				.mapToDouble((roomInfo) -> roomInfo.getTotalPrice()).sum();

	}

	protected void updateModifiedHotelInfoInAmendment(Amendment amendment, HotelInfo modifiedHotelInfo) {

		modifiedHotelInfo.getOptions().get(0).getRoomInfos()
				.forEach((roomInfo) -> BaseHotelUtils.flattenFareComponents(roomInfo));
		log.debug("Modified Hotel is {}", GsonUtils.getGson().toJson(modifiedHotelInfo));
		amendment.getModifiedInfo().setHotelInfo(modifiedHotelInfo);

	}

	protected void updateNote(Amendment amendment) {

		Note note = Note.builder().bookingId(amendment.getBookingId()).amendmentId(amendment.getAmendmentId())
				.noteType(NoteType.AMENDMENT).noteMessage(requestNote).userId(amendment.getAssignedUserId()).build();

		List<Note> noteList =
				gmsCommunicator.getNotes(NoteFilter.builder().amendmentId(amendment.getAmendmentId()).build());
		if (!noteList.isEmpty())
			note.setId(noteList.get(0).getId());
		gmsCommunicator.addNote(note);

	}

	protected HotelOrder getModifiedHotelOrder() {

		BaseHotelUtils.updateTotalFareComponents(hotelOrderRequest.getHInfo(), null);
		log.debug("modified hotel after manualOrderManager for booking id {} is {}", hotelOrderRequest.getBookingId(),
				GsonUtils.getGson().toJson(hotelOrderRequest.getHInfo()));
		hotelOrderRequest.setRoomKeys(amendment.getAdditionalInfo().getHotelAdditionalInfo().getRoomKeys());
		HotelOrder modifiedHotelOrder = modifyBookingManager.getHotelOrderFromUpdatedHotelInfo(hotelOrderRequest);

		return modifiedHotelOrder;

	}

}
