package com.tgs.services.oms.servicehandler;

import static com.tgs.services.oms.datamodel.OrderStatus.ABORTED;
import static com.tgs.services.oms.datamodel.OrderStatus.CANCELLED;
import static com.tgs.services.oms.datamodel.OrderStatus.FAILED;
import static com.tgs.services.oms.datamodel.OrderStatus.ON_HOLD;
import static com.tgs.services.oms.datamodel.OrderStatus.PENDING;
import static com.tgs.services.oms.datamodel.OrderStatus.SUCCESS;
import static com.tgs.services.oms.datamodel.OrderStatus.UNCONFIRMED;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ArrayList;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.oms.utils.OrderUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.datamodel.UserProfile;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.ProcessedFlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.oms.datamodel.AirItemDetail;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AirOrderDetails;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.air.AirItemDetailService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.restcontroller.air.OrderActionValidator;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.oms.servicehandler.air.AirInvoiceHandler;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BookingDetailHandler extends ServiceHandler<BookingDetailRequest, BookingDetailResponse> {

	protected final int MAX_RETRIES = 20;

	protected int currentRetry = 0;

	@Autowired
	private OrderService orderService;

	@Autowired
	private AirOrderItemService itemService;

	@Autowired
	private GeneralServiceCommunicator gSCommunicator;

	@Autowired
	private UserServiceCommunicator userComm;

	@Autowired
	private FMSCommunicator fmsComm;

	@Autowired
	private OrderActionValidator actionValidator;

	@Autowired
	private OrderManager orderManger;

	@Autowired
	private AirItemDetailService itemDetailService;

	private static final List<OrderStatus> ALLOWED_ORDER_STATUS =
			Arrays.asList(ABORTED, CANCELLED, SUCCESS, ON_HOLD, FAILED, PENDING, UNCONFIRMED);

	@Override
	public void beforeProcess() throws Exception {
		response = new BookingDetailResponse();
		ProductMetaInfo metaInfo = Product.getProductMetaInfoFromId(request.getBookingId());
		if (!metaInfo.getProduct().equals(Product.AIR)) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
	}

	private void setPaxInvoice(Order order, List<DbAirOrderItem> items) {
		boolean save = false;
		if (MapUtils.isEmpty(order.getAdditionalInfo().getInvoiceMap()))
			return;
		Map<String, String> map = order.getAdditionalInfo().getInvoiceMap();
		log.debug("BookingId = {}, Pax invoice map = {}", order.getBookingId(), new Gson().toJson(map));
		for (DbAirOrderItem item : items) {
			for (FlightTravellerInfo pax : item.getTravellerInfo()) {
				String invoice = map.get(pax.pnrKey());
				if (StringUtils.isEmpty(pax.getInvoice()) && StringUtils.isNotEmpty(invoice)) {
					log.debug("Setting pax invoice. BookingId: {}, pnrKey = {}, invoice = {}", order.getBookingId(),
							pax.pnrKey(), invoice);
					save = true;
					pax.setInvoice(invoice);
				}
			}
		}
		if (save) {
			itemService.save(items);
		}
	}

	@Override
	public void process() throws Exception {
		try {
			OrderFilter filter = OrderFilter.builder().bookingIds(Arrays.asList(request.getBookingId())).build();
			if (SystemContextHolder.getContextData().getUser() != null
					&& UserRole.GUEST.equals(SystemContextHolder.getContextData().getUser().getRole())) {
				User user = userComm.getUser(
						UserFilter.builder().email(request.getEmail()).roles(Arrays.asList(UserRole.CUSTOMER)).build());
				log.info("User found with id {} ", user.getUserId());
				filter.setBookingUserIds(Arrays.asList(user.getUserId()));
			}
			boolean isApiPartnerFlow = isApiPartnerFlow();
			List<DbOrder> dbOrders = orderService.findAll(filter);
			if (CollectionUtils.isEmpty(dbOrders)) {
				response.setRetryInSecond(3);
				if (this.isRetryRequired(isApiPartnerFlow)) {
					this.retry(isApiPartnerFlow, null);
					return;
				} else {
					throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
				}
			}

			Order order = dbOrders.get(0).toDomain();

			if (order.getStatus() == OrderStatus.FAILED && order.getReason() != null
					&& order.getReason().contains("Unable to process payment") && isApiPartnerFlow
					&& OrderUtils.isOrderRetryLogicEnabled()) {
				log.info("Payment status is failed for API Partner for bookingid {}", order.getBookingId());
				response.setRetryInSecond(2);
				if (this.isRetryRequired(isApiPartnerFlow)) {
					this.retry(isApiPartnerFlow, null);
					return;
				}
			}


			if (SystemContextHolder.getContextData().getUser() != null
					&& !UserRole.GUEST.equals(SystemContextHolder.getContextData().getUser().getRole())) {
				UserServiceHelper.checkAndReturnAllowedUserId(
						SystemContextHolder.getContextData().getUser().getLoggedInUserId(),
						Arrays.asList(order.getBookingUserId()));
				UserServiceHelper.returnPartnerIds(SystemContextHolder.getContextData().getUser().getLoggedInUserId(),
						true, Arrays.asList(order.getPartnerId()));
				order.setActionList(
						actionValidator.validActions(SystemContextHolder.getContextData().getUser(), order));
			}
			/**
			 * In case Booking Detail is called within 5 min of boooking and bookingUserId is equal to loggedIn User
			 * then we will store in logs
			 */
			if (order.getBookingUserId().equals(UserUtils.getUserId(SystemContextHolder.getContextData().getUser()))
					&& order.getCreatedOn().isAfter(LocalDateTime.now().minusMinutes(5))) {
				SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
			}
			response.setOrder(order);
			AirOrderDetails airDetails = new AirOrderDetails();

			User bookingUser = userComm.getUserFromCache(order.getBookingUserId());
			GeneralBasicFact fact = GeneralBasicFact.builder().build();
			fact.generateFact(bookingUser.getRole());
			FareBreakUpConfigOutput fbConfig = gSCommunicator.getConfigRule(ConfiguratorRuleType.FAREBREAKUP, fact);

			List<DbAirOrderItem> airOrderItems = itemService.findByBookingIdWithPNRMasked(request.getBookingId());

			AirItemDetail airItemDetail = itemDetailService.get(request.getBookingId());
			setPaxInvoice(order, airOrderItems);
			AirInvoiceHandler.setProfileFieldsDb(order.getBookingId(), order.getBookingUserId(), airOrderItems);
			List<SegmentInfo> segmentList =
					AirBookingUtils.convertAirOrderItemListToSegmentInfoList(airOrderItems, null);
			airDetails.setPnrStatus(AirBookingUtils.getPNRStatus(order, segmentList, bookingUser));
			AirBookingUtils.setIsInternatinalSplitSearch(segmentList, airDetails);

			if (BooleanUtils.toBoolean(request.getRequirePaxPricing())) {
				setTravellerProfile(segmentList, airItemDetail);
				airDetails.setSegmentInfos(segmentList);

				for (SegmentInfo segmentInfo : segmentList) {
					segmentInfo.getBookingRelatedInfo().getTravellerInfo().forEach(t -> {
						t.getFareDetail().setFareComponents(t.getFareDetail().populateMappedFareComponent(fbConfig));
					});
					// fmsComm.updateCancellationAndRescheduleFees(segmentInfo, bookingUser);
				}
				AirBookingUtils.setTimeLimit(segmentList, airDetails);

				if (isApiPartnerFlow && BooleanUtils.isTrue(request.getRequirePaxPricing())) {
					List<TripInfo> tripInfos = BookingUtils.createTripListFromSegmentList(segmentList);
					airDetails.setTripInfos(tripInfos);
					AirBookingUtils.setTripTotalPriceInfo(tripInfos, fbConfig);
					airDetails.setTotalPriceInfo(AirBookingUtils.getTotalPriceInfo(segmentList, request.getPaxNo(),
							fbConfig, isApiPartnerFlow));
					List<ProcessedFlightTravellerInfo> travellerInfo = AirBookingUtils
							.generateProcessedTravellerInfo(segmentList, request.getPaxNo(), bookingUser);
					airDetails.setTravellerInfos(travellerInfo);
					setAPIUserFareComponents(tripInfos, fbConfig);
				}

			} else {
				List<TripInfo> tripList = BookingUtils.createTripListFromSegmentList(segmentList);
				airDetails.setTripInfos(tripList);
				List<ProcessedFlightTravellerInfo> travellerInfo =
						AirBookingUtils.generateProcessedTravellerInfo(segmentList, request.getPaxNo(), bookingUser);
				airDetails.setTravellerInfos(travellerInfo);
				AirBookingUtils.setTimeLimit(segmentList, airDetails);
				AirBookingUtils.setPriceInfoList(segmentList);

				airDetails
						.setSearchQuery(BaseUtils.combineSearchQuery(BaseUtils.getSearchQueryFromTripInfos(tripList)));
				airDetails.setTotalPriceInfo(
						AirBookingUtils.getTotalPriceInfo(segmentList, request.getPaxNo(), fbConfig, isApiPartnerFlow));
				AirBookingUtils.setTripTotalPriceInfo(tripList, fbConfig);
				airDetails.setTermsConditions(fmsComm.getTermsAndConditions(tripList, bookingUser));
				AirBookingUtils.removeTravellerInfoFromSegmentList(segmentList);
				response.setBookingUser(bookingUser);
			}
			response.getItemInfos().put(order.getOrderType().name(), airDetails);
			response.setGstInfo(orderManger.getGstInfo(request.getBookingId()));
			if (!(order.getStatus().equals(OrderStatus.SUCCESS) || order.getStatus().equals(OrderStatus.ON_HOLD)
					|| order.getStatus().equals(OrderStatus.FAILED) || order.getStatus().equals(OrderStatus.PENDING)
					|| OrderStatus.ABORTED.equals(order.getStatus())
					|| OrderStatus.UNCONFIRMED.equals(order.getStatus()))
					&& LocalDateTime.now().minusMinutes(5).isBefore(order.getProcessedOn())) {
				response.setRetryInSecond(3);
			}

			if (isRetryRequired(isApiPartnerFlow)) {
				this.retry(isApiPartnerFlow, order);
			}
		} catch (Exception e) {
			if (e instanceof CustomGeneralException) {
				log.info("Unable to find details for bookingId {} cause {}", request.getBookingId(), e.getMessage());
				throw e;
			}
			log.error("Unable to find fetch booking details for bookingId {}", request.getBookingId(), e);
			SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
			throw new CustomGeneralException(SystemError.ORDER_FETCH_FAILED);
		}
	}

	private void setAPIUserFareComponents(List<TripInfo> tripInfos, FareBreakUpConfigOutput fbConfig) {
		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				for (FlightTravellerInfo travellerInfo : segmentInfo.getTravellerInfo()) {
					PriceInfo travellerPrice =
							AirBookingUtils.generateFareComponents(null, travellerInfo, fbConfig, true);
					travellerInfo.getFareDetail()
							.setFareComponents(travellerPrice.getTotalFareDetail().getFareComponents());
					travellerInfo.getFareDetail()
							.setAddlFareComponents(travellerPrice.getTotalFareDetail().getAddlFareComponents());
				}
			}
		}
	}

	private void setTravellerProfile(List<SegmentInfo> segmentList, AirItemDetail airItemDetail) {
		if (airItemDetail != null) {
			Map<String, Map<String, Object>> profileData = airItemDetail.getInfo().getProfileData();
			if (MapUtils.isNotEmpty(profileData)) {
				for (SegmentInfo segment : segmentList) {
					for (FlightTravellerInfo traveller : segment.getBookingRelatedInfo().getTravellerInfo()) {
						if (MapUtils.isNotEmpty(profileData.get(traveller.getPaxKey()))) {
							UserProfile userProfile = UserProfile.builder().build();
							userProfile.setData(profileData.get(traveller.getPaxKey()));
							traveller.setUserProfile(userProfile);
						}
					}
				}
			}
		}
	}

	private boolean isApiPartnerFlow() {
		User user = SystemContextHolder.getContextData().getUser();
		Integer airApiVersion = UserUtils.getUserAirApiVersion(user);
		return UserUtils.isApiUserRequest(user) && airApiVersion != null && airApiVersion >= 1;
	}

	private void retry(boolean isApiPartnerFlow, Order order) throws Exception {
		OrderStatus currentStatus = order != null ? order.getStatus() : null;
		log.info("Retrying booking retrieve logic for the booking Id {} status {}", request.getBookingId(),
				currentStatus);
		Thread.sleep(response.getRetryInSecond() * 1000);
		currentRetry++;
		response.setRetryInSecond(null);
		this.process();
	}

	private boolean isRetryRequired(boolean isApiPartnerFlow) {
		return isApiPartnerFlow && Objects.nonNull(response.getRetryInSecond()) && (currentRetry < MAX_RETRIES);
	}

	@Override
	public void afterProcess() throws Exception {
		if (isApiPartnerFlow()) {
			OrderStatus orderStatus = response.getOrder().getStatus();
			if (!ALLOWED_ORDER_STATUS.contains(orderStatus)) {
				response.getOrder().setStatus(OrderStatus.PENDING);
			}

		}
	}

}
