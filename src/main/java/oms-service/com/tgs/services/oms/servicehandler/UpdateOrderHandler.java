package com.tgs.services.oms.servicehandler;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAction;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.restcontroller.air.OrderActionValidator;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.oms.servicehandler.air.AbortOrderHandler;

import lombok.Setter;

@Service
public class UpdateOrderHandler extends ServiceHandler<Order, BookingDetailResponse> {

	@Autowired
	private OrderService orderService;

	@Autowired
	private OrderActionValidator actionValidator;

	@Autowired
	private AbortOrderHandler abortOrderHandler;

	@Setter
	private OrderAction action;

	private Order order;

	@Override
	public void beforeProcess() throws Exception {
		order = orderService.findByBookingId(request.getBookingId()).toDomain();
		order.setReason(request.getReason());
		order.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), order));
		validateAction();
	}

	private void validateAction() {
		if (order.getStatus().equals(OrderStatus.SUCCESS)) {
			throw new CustomGeneralException(SystemError.CANNOT_UPDATE_ORDER);
		}

		// validate actions here
		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), order).contains(action))
			throw new CustomGeneralException(SystemError.ORDER_INVALID_ACTION);

	}

	@Override
	public void process() throws Exception {

		if (action.equals(OrderAction.ASSIGN)) {

			order.getAdditionalInfo().setAssignedUserId(request.getAdditionalInfo().getAssignedUserId());
			order.getAdditionalInfo().setAssignedTime(LocalDateTime.now());
		}
		if (action.equals(OrderAction.ASSIGNME)) {
			/**
			 * Admin can assign the cart to himself, even if it was assigned to someone else
			 */
			if (StringUtils.isEmpty(order.getAdditionalInfo().getAssignedUserId())
					|| SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.ADMIN)) {
				order.getAdditionalInfo().setAssignedUserId(SystemContextHolder.getContextData().getUser().getUserId());
				order.getAdditionalInfo().setAssignedTime(LocalDateTime.now());
			} else {
				throw new CustomGeneralException(SystemError.ORDER_ALREADY_ASSIGNED);
			}
		}
		if (action.equals(OrderAction.ABORT)) {
			// reverse payment
			abortOrderHandler.abort(order.getBookingId());
			// AbortHandler might have updated the order
			order = orderService.findByBookingId(request.getBookingId()).toDomain();
		}

		orderService.save(DbOrder.create(order));
		order.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), order));
		response.setOrder(order);
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
