package com.tgs.services.oms.manager.hotel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.mapper.RoomInfoToDbHotelOrderItemMapper;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;
import com.tgs.services.ums.datamodel.User;

@Service
public class HotelModifyBookingManager {

	@Autowired
	private HotelOrderItemManager itemManager;

	@Autowired
	private OrderService orderService;

	@Autowired
	HotelPostBookingManager postOrderManager;

	@Autowired
	private UserServiceCommunicator usCommunicator;

	public HotelOrder getHotelOrderFromUpdatedHotelInfo(HotelPostBookingBaseRequest request) {

		String bookingId = request.getBookingId();
		HotelInfo updatedHInfo = request.getHInfo();
		DeliveryInfo deliveryInfo = request.getDeliveryInfo();
		Set<String> roomKeys = request.getRoomKeys();

		DbOrder oldDbOrder = orderService.findByBookingId(bookingId);

		if (oldDbOrder == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);

		User bookingUser = usCommunicator.getUserFromCache(oldDbOrder.getBookingUserId());
		List<DbHotelOrderItem> oldDbOrderItems = itemManager.getItemList(bookingId);
		String itemStatusCode = oldDbOrderItems.get(0).getStatus();

		Map<String, RoomInfo> updatedRoomMap = updatedHInfo.getOptions().get(0).getRoomInfos().stream()
				.collect(Collectors.toMap(RoomInfo::getId, Function.identity()));

		List<DbHotelOrderItem> resultSet = new ArrayList<>();
		Iterator<DbHotelOrderItem> oldItemIterator = oldDbOrderItems.iterator();
		int index = 0;
		boolean isFirstItemDeleted = false;
		Double orderMarkup = 0.0, orderAmount = 0.0;

		while (oldItemIterator.hasNext()) {
			DbHotelOrderItem oldItem = oldItemIterator.next();
			String mapper = fetchMapperFromDbItem(oldItem);
			RoomInfo updatedRoom = updatedRoomMap.get(mapper);
			DbHotelOrderItem modifiedItem = null;
			if (CollectionUtils.isEmpty(updatedRoom.getHotelOrderSupplierInfos())) {
				updatedRoom.setHotelOrderSupplierInfos(oldItem.getAdditionalInfo().getHotelOrderSupplierInfos());
			}
			if (BooleanUtils.isTrue(updatedRoom.getIsDeleted())) {

				if (index == 0)
					isFirstItemDeleted = true;
				modifiedItem = updateOldOrderItemFromRoomInfo(oldItem, updatedRoom, index, updatedHInfo, bookingId,
						bookingUser);
				oldItem.getRoomInfo().setIsDeleted(true);

			} else {

				if (isFirstItemDeleted) {
					index = 0;
					isFirstItemDeleted = false;
				}
				if (CollectionUtils.isEmpty(roomKeys) || roomKeys.contains(mapper)) {
					modifiedItem = updateOldOrderItemFromRoomInfo(oldItem, updatedRoom, index, updatedHInfo, bookingId,
							bookingUser);
					if (!ObjectUtils.isEmpty(updatedRoom.getRoomStatus())
							&& !modifiedItem.getStatus().equals(updatedRoom.getRoomStatus())) {
						modifiedItem.setStatus(updatedRoom.getRoomStatus());
					}
				} else {
					modifiedItem = oldItem;
				}
				orderMarkup += modifiedItem.getMarkup();
				orderAmount += modifiedItem.getAmount();

			}
			resultSet.add(modifiedItem);
			updatedRoomMap.remove(mapper);
			index++;
		}


		if (!updatedRoomMap.isEmpty()) {
			for (String key : updatedRoomMap.keySet()) {
				RoomInfo rInfo = updatedRoomMap.get((key));
				DbHotelOrderItem newItem =
						updateOldOrderItemFromRoomInfo(null, rInfo, index, updatedHInfo, bookingId, bookingUser);
				newItem.setStatus(itemStatusCode);
				resultSet.add(newItem);
				orderMarkup += newItem.getMarkup();
				orderAmount += newItem.getAmount();
				index++;
			}
		}
		Order order = updateOrder(oldDbOrder, orderMarkup, orderAmount, bookingId, deliveryInfo);
		List<HotelOrderItem> items = DbHotelOrderItem.toDomainList(resultSet);
		return HotelOrder.builder().items(items).order(order).build();

	}

	private DbHotelOrderItem updateOldOrderItemFromRoomInfo(DbHotelOrderItem oldItem, RoomInfo updatedRoomInfo,
			int index, HotelInfo updatedHInfo, String bookingId, User bookingUser) {

		DbHotelOrderItem updatedItem = RoomInfoToDbHotelOrderItemMapper.builder().roomInfo(updatedRoomInfo)
				.roomTravellerInfo(updatedRoomInfo.getTravellerInfo()).index(index).bookingId(bookingId)
				.hInfo(updatedHInfo).bookingUser(bookingUser).build().setOutput(oldItem).convert();
		return updatedItem;

	}

	private String fetchMapperFromDbItem(DbHotelOrderItem item) {
		return item.getRoomInfo().getId();
	}


	private Order updateOrder(DbOrder oldDbOrder, Double orderMarkup, Double orderAmount, String bookingId,
			DeliveryInfo deliveryInfo) {

		oldDbOrder.setMarkup(orderMarkup);
		oldDbOrder.setAmount(orderAmount);
		updateDeliveryInfo(oldDbOrder.getDeliveryInfo(), deliveryInfo);
		return oldDbOrder.toDomain();
	}


	private void updateDeliveryInfo(DeliveryInfo oldDeliveryInfo, DeliveryInfo updatedDeliveryInfo) {

		if (!CollectionUtils.isEmpty(updatedDeliveryInfo.getContacts()))
			oldDeliveryInfo.setContacts(updatedDeliveryInfo.getContacts());
		if (!CollectionUtils.isEmpty(updatedDeliveryInfo.getEmails()))
			oldDeliveryInfo.setEmails(updatedDeliveryInfo.getEmails());

	}

}
