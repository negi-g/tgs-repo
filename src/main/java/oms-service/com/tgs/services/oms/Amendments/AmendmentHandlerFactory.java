package com.tgs.services.oms.Amendments;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.Amendments.Processors.*;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.restmodel.air.AmendmentRequest;
import com.tgs.services.oms.servicehandler.air.amendment.AbstractAmendmentHandler;
import com.tgs.services.oms.servicehandler.air.amendment.ReIssueAmendmentHandler;
import com.tgs.services.oms.servicehandler.air.amendment.CancellationAmendmentHandler;
import com.tgs.services.ums.datamodel.User;
import java.util.List;

public final class AmendmentHandlerFactory {

	public static AirAmendmentProcessor initHandler(Amendment amendment, List<AirOrderItem> modItems,
			List<DbAirOrderItem> orgItems, User bookingUser) {

		AirAmendmentProcessor amendmentHandler = null;
		switch (amendment.getAmendmentType()) {
			case FARE_CHANGE:
				amendmentHandler = SpringContext.getApplicationContext().getBean(FareChangeProcessor.class);
				break;
			case SSR:
				amendmentHandler = SpringContext.getApplicationContext().getBean(SsrAmendmentProcessor.class);
				break;
			case CANCELLATION_QUOTATION:
				amendmentHandler = SpringContext.getApplicationContext().getBean(CancellationProcessor.class);
				break;
			case CANCELLATION:
				amendmentHandler = SpringContext.getApplicationContext().getBean(CancellationProcessor.class);
				break;
			case NO_SHOW:
				amendmentHandler = SpringContext.getApplicationContext().getBean(NoShowProcessor.class);
				break;
			case VOIDED:
				amendmentHandler = SpringContext.getApplicationContext().getBean(VoidProcessor.class);
				break;
			case REISSUE:
				amendmentHandler = SpringContext.getApplicationContext().getBean(ReIssueProcessor.class);
				break;
			case REISSUE_QUOTATION:
				amendmentHandler = SpringContext.getApplicationContext().getBean(ReIssueProcessor.class);
				break;
			case MISCELLANEOUS:
				amendmentHandler = SpringContext.getApplicationContext().getBean(MiscAmendmentProcessor.class);
				break;
			case CORRECTION:
				amendmentHandler = SpringContext.getApplicationContext().getBean(AirCorrectionProcessor.class);
				break;
			case FULL_REFUND:
				amendmentHandler = SpringContext.getApplicationContext().getBean(CancellationProcessor.class);
				break;
			default:
				throw new RuntimeException("AmendmentType is Null or Not Supported.");
		}
		amendmentHandler.setAmendment(amendment);
		amendmentHandler.setModifiedItems(modItems);
		amendmentHandler.setOrgItems(orgItems);
		amendmentHandler.setBookingUser(bookingUser);
		return amendmentHandler;
	}

	public static AbstractAmendmentHandler initChargeHandler(AmendmentRequest amendmentRequest) {
		AbstractAmendmentHandler handler = null;
		switch (amendmentRequest.getType()) {
			case CANCELLATION:
				handler = SpringContext.getApplicationContext().getBean(CancellationAmendmentHandler.class);
				break;
			default:
				throw new CustomGeneralException(SystemError.CONTACT_SUPPORT);
		}
		return handler;
	}

	public static AbstractAmendmentHandler initPartnerSubmitHandler(AmendmentRequest amendmentRequest) {
		AbstractAmendmentHandler handler = null;
		switch (amendmentRequest.getType()) {
			case CANCELLATION:
				handler = SpringContext.getApplicationContext().getBean(CancellationAmendmentHandler.class);
				break;
			default:
				throw new CustomGeneralException(SystemError.CONTACT_SUPPORT);
		}
		return handler;
	}
}
