package com.tgs.services.oms.dbmodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.BillingEntityInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.dbmodel.BaseModel;

import com.tgs.services.base.runtime.database.CustomTypes.BillingEntityInfoType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

@Entity
@Table(name="order_billing")
@Getter
@Setter
@TypeDefs({ @TypeDef(name = "BillingEntityInfoType", typeClass = BillingEntityInfoType.class) })
public class DbOrderBilling extends BaseModel<DbOrderBilling, GstInfo> {

	@Column
	private String gstNumber;
	@Column
	private String email;
	@Column
	private String mobile;
	@Column
	private String address;
	@Column
	private String state;
	@Column
	private String pincode;
	@Column
	private String cityName;
	@Column
	private String registeredName;
	@Column
	private String bookingId;
	@Column
	private String bookingUserId;
	@Column
	@Type(type = "BillingEntityInfoType")
	private BillingEntityInfo info;
	@Column
	private String billingCompanyName;

	public BillingEntityInfo getInfo() {
		return info == null ? BillingEntityInfo.builder().build() : info;
	}

	@Override
	public GstInfo toDomain() {
		GstInfo gstInfo = new GsonMapper<>(this, GstInfo.class).convert();
		gstInfo.setAccountCode(this.getInfo().getAccountCode());
		return gstInfo;
	}

	@Override
	public DbOrderBilling from(GstInfo dataModel) {
		DbOrderBilling db = new GsonMapper<>(dataModel, this, DbOrderBilling.class).convert();
		db.setInfo(BillingEntityInfo.builder().accountCode(dataModel.getAccountCode()).build());
		return db;
	}
}
