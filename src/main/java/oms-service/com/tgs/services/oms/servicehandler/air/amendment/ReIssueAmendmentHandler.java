package com.tgs.services.oms.servicehandler.air.amendment;

import com.tgs.services.oms.restmodel.air.AirAmendmentPaxInfo;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.oms.restmodel.air.AmendmentChargesResponse;
import org.springframework.stereotype.Service;

@Service
public class ReIssueAmendmentHandler extends AbstractAmendmentHandler {

	@Override
	public void preSetDetails() {

	}


	@Override
	public AirAmendmentPaxInfo buildInternalAmendmentRequest(AirOrderInfo airOrderInfo) {
		AirAmendmentPaxInfo paxInfo = super.buildInternalAmendmentRequest(airOrderInfo);
		paxInfo.setNextTravelDate(getAmendmentRequest().getTrips().get(0).getNextTravelDate());
		return paxInfo;
	}

	@Override
	public AmendmentChargesResponse buildChargesResponse() {
		AmendmentChargesResponse chargesResponse = super.buildChargesResponse();
		return chargesResponse;
	}

}
