package com.tgs.services.oms.servicehandler.rail;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.rail.RailAmdBulkCancelRequest;
import com.tgs.services.oms.restmodel.rail.RailAmendmentRequest;
import com.tgs.services.oms.restmodel.rail.RailBulkAmendmentRequest;
import com.tgs.services.base.helper.SystemError;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailBulkCancelManager {

	@Autowired
	protected GeneralServiceCommunicator generalService;
	@Autowired
	protected RailOrderItemService railOrderItemService;

	public BulkUploadResponse raiseBulkAmendments(@Valid RailAmdBulkCancelRequest railBulkAmendmentUploadRequest)
			throws Exception {

		if (railBulkAmendmentUploadRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(railBulkAmendmentUploadRequest.getUploadId());
		}
		log.info("Number of amendments to be raised : {} ", railBulkAmendmentUploadRequest.getAmendemntList().size());
		String jobId = BulkUploadUtils.generateRandomJobId();
		final ContextData contextData = SystemContextHolder.getContextData();
		Runnable uploadRailAmendments = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			for (RailBulkAmendmentRequest railAmendmentRequest : railBulkAmendmentUploadRequest.getAmendemntList()) {
				Future<?> f = executor.submit(() -> {
					SystemContextHolder.setContextData(contextData);
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(railAmendmentRequest.getRowId());
					try {
						AmendmentResponse response = raiseRailAmendment(railAmendmentRequest);
						if (response.getAmendmentItems().get(0).getStatus().equals(AmendmentStatus.SUCCESS)) {
							log.info("Amendment Success for bookingId {}", railAmendmentRequest.getBookingId());
							uploadInfo.setStatus(UploadStatus.SUCCESS);
						} else {
							log.info("Amendment Failed for bookingId {}", railAmendmentRequest.getBookingId());
							uploadInfo.setStatus(UploadStatus.FAILED);
							List<Note> notes =
									generalService.getNotes(NoteFilter.builder().noteType(NoteType.CANCELLATION_REASON)
											.bookingId(railAmendmentRequest.getBookingId()).build());
							log.info("Notes size for bookingid  {} is {}", railAmendmentRequest.getBookingId(),
									notes.size());
							if (CollectionUtils.isNotEmpty(notes)) {
								notes.sort(Comparator.comparing(Note::getCreatedOn));
								uploadInfo.setErrorMessage(notes.get(notes.size() - 1).getNoteMessage());
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error("Unable to Raise Amendment for  bookingId {} , type {} , agentCanId {} , pnr {}",
								railAmendmentRequest.getBookingId(), railAmendmentRequest.getType(),
								railAmendmentRequest.getAgentCanId(), railAmendmentRequest.getPnr(), e.getMessage(), e);
					} finally {
						uploadInfo.setPnr(railAmendmentRequest.getPnr());
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, BulkUploadUtils.INITIALIZATION_NOT_REQUIRED);
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while doing bulk rail cancellation {} ", jobId, e);
			}
		};
		return BulkUploadUtils.processBulkUploadRequest(uploadRailAmendments,
				railBulkAmendmentUploadRequest.getUploadType(), jobId);

	}

	private AmendmentResponse raiseRailAmendment(RailBulkAmendmentRequest amdRequest) throws Exception {
		RailAmendmentRequest amendmentRequest = populateMissingParameters(amdRequest);
		RaiseRailAmendmentHandler railAmendmentHandler =
				SpringContext.getApplicationContext().getBean(RaiseRailAmendmentHandler.class);
		railAmendmentHandler.initData(amendmentRequest,
				AmendmentResponse.builder().amendmentItems(new ArrayList<>()).build());
		log.error("RailAmendmentHandler Instance : {} {} ", railAmendmentHandler.hashCode(),
				railAmendmentHandler.getRequest());
		return railAmendmentHandler.getResponse();
	}

	private RailAmendmentRequest populateMissingParameters(RailBulkAmendmentRequest amdRequest) {
		RailAmendmentRequest request = new RailAmendmentRequest();

		if (amdRequest.getBookingId() != null) {
			request.setBookingId(amdRequest.getBookingId());
		} else {
			String bookingId = null;
			bookingId = railOrderItemService.findBookingIdByPnr(amdRequest.getPnr());
			if (StringUtils.isNotBlank(bookingId)) {
				request.setBookingId(bookingId);
			}
			else{
				log.error("No Booking Found for PNR {}", amdRequest.getPnr());
				throw new CustomGeneralException(SystemError.NO_BOOKING_FOUND);
			}
		}
		request.setAgentCanId(amdRequest.getAgentCanId());
		request.setPaxIds(amdRequest.getPaxIds());
		log.info("Inside Rail Amendment Bulk Cancellation Manager , updating amendment request for bookingId {}",
				amdRequest.getBookingId());
		request.setIsWithoutOtp(true);
		if (Objects.isNull(amdRequest.getType())) {
			request.setType(AmendmentType.OFFLINE_CANCEL);
		}
		if (Objects.isNull(amdRequest.getPaxIds())) {
			Set<String> paxSet = new HashSet<String>();
			paxSet.add("1");
			request.setPaxIds(paxSet);
		}
		if (StringUtils.isEmpty(amdRequest.getRemarks())) {
			request.setRemarks("Bulk Cancel");
		}
		if (StringUtils.isEmpty(amdRequest.getAgentCanId())) {
			throw new CustomGeneralException("Agent Cancellation Id can't be Empty");
		}
		return request;

	}

}
