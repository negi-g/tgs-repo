package com.tgs.services.oms.Amendments.Processors;

import static com.tgs.services.base.enums.FareComponent.*;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.oms.Amendments.AmendmentHelper;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.mapper.AirOrderItemToSegmentInfoMapper;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CancellationProcessor extends AirAmendmentProcessor {

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Override
	public void processAmendment() {
		double totalAmtApplicableForRefund, totalCancelFee, commissionRecall, totalTds, partnerMarkUpRecalled,
				partnerCommissionRecalled, partnerMarkUpTds, partnerCommTds, totalCpAgentCommissionRecalled,
				cpAgentCommissionTdsToReturn;
		totalAmtApplicableForRefund = totalCancelFee =
				commissionRecall = totalTds = partnerMarkUpRecalled = partnerCommissionRecalled = partnerMarkUpTds =
						partnerCommTds = totalCpAgentCommissionRecalled = cpAgentCommissionTdsToReturn = 0d;
		amendment.getAdditionalInfo().setRecallCommission(BaseUtils.isRecalCommission(true, bookingUser.getRole()));
		amendment.getAdditionalInfo().setReturnTds(AmendmentHelper.returnTDS(patchedItems.get(0).getCreatedOn()));
		for (AirOrderItem airOrderItem : patchedItems) {
			boolean isSSRefundable = fmsCommunicator.isSSRRefundable(airOrderItem, bookingUser);
			boolean isPassThrough = BooleanUtils.isTrue(airOrderItem.getAdditionalInfo().getIsUserCreditCard());
			Set<FareComponent> refundableComponents = AirBookingUtils.amendmentRefundableComponents(airOrderItem,
					isCancellationNewFlow(), isSSRefundable);
			for (FlightTravellerInfo traveller : airOrderItem.getTravellerInfo()) {
				traveller.setStatus(TravellerStatus.CANCELLED);
				double paxCancelFee = 0d;
				double cppComponents =
						traveller.getFareDetail().getComponentsSum(getCancellationProtectionComponents(false));
				totalAmtApplicableForRefund += handleRefundableComponents(traveller, refundableComponents, bookingUser,
						isSSRefundable, amendment);
				log.info("totalAmtApplicableForRefund {} for booking {} traveller {}", totalAmtApplicableForRefund,
						airOrderItem.getBookingId(), traveller.getId());
				double totalPaxFare = traveller.getFareDetail().getFareComponents().getOrDefault(TF, 0d);
				paxCancelFee = traveller.getFareDetail()
						.getComponentsSum(AirBookingUtils.getPaxCancelComponents(isPassThrough));
				paxCancelFee -= traveller.getFareDetail().getFareComponents().getOrDefault(PMU, 0d);
				if (!amendment.getAdditionalInfo().isRecallCPCommission()) {
					paxCancelFee += cppComponents;
				}

				totalCancelFee += paxCancelFee;
				traveller.getFareDetail().setPaxCancellationFee(paxCancelFee);
				if (!amendment.getAdditionalInfo().isRecallCPCommission())
					paxCancelFee -= cppComponents;
				traveller.getFareDetail().getFareComponents().put(TF, totalPaxFare + paxCancelFee);
				commissionRecall += AirBookingUtils.getGrossCommissionForPax(traveller, bookingUser, false);
				// partnerMarkUpRecalled += AirBookingUtils.getPartnerMarkUpForPax(traveller);
				partnerCommissionRecalled += AirBookingUtils.getPartnerCommissionForPax(traveller);
				totalTds += traveller.getFareDetail().getFareComponents().getOrDefault(TDS, 0d);
				partnerMarkUpTds += traveller.getFareDetail().getFareComponents().getOrDefault(PMTDS, 0d);
				partnerCommTds += traveller.getFareDetail().getFareComponents().getOrDefault(PCTDS, 0d);
				log.info("[CancelCp] CP Agent commission recall {} and CP components are {}",
						amendment.getAdditionalInfo().isRecallCPCommission(),
						GsonUtils.getGson().toJson(traveller.getFareDetail().getFareComponents()));
				if (amendment.getAdditionalInfo().isRecallCPCommission()) {
					totalCpAgentCommissionRecalled +=
							traveller.getFareDetail().getFareComponents().getOrDefault(CPAC, 0d);
					cpAgentCommissionTdsToReturn +=
							traveller.getFareDetail().getFareComponents().getOrDefault(CPACT, 0d);
					zeroOut(traveller.getFareDetail(), FareComponent.getCancellationProtectionComponents(true));
				}
				zeroOut(traveller.getFareDetail(), FareComponent.getAllCommisionComponents());
				zeroOut(traveller.getFareDetail(), FareComponent.voucherComponents());
				// zeroOut(traveller.getFareDetail(), FareComponent.pointComponents());
				if (amendment.getAdditionalInfo().isReturnTds()) {
					traveller.getFareDetail().getFareComponents().put(TDS, 0d);
				}
			}
		}
		amendment.getAdditionalInfo().setAmountApplicableForRefund(totalAmtApplicableForRefund);
		amendment.getAirAdditionalInfo().setTotalCancellationFee(totalCancelFee);
		amendment.getAdditionalInfo().setTotalCommissionRecalled(commissionRecall);
		amendment.getAdditionalInfo().setTotalPartnerMarkupRecalled(partnerMarkUpRecalled);
		amendment.getAdditionalInfo().setTotalPartnerCommissionRecalled(partnerCommissionRecalled);
		amendment.getAdditionalInfo().setTdsToReturn(totalTds);
		amendment.getAdditionalInfo().setPartnerMarkUpTdsToReturn(partnerMarkUpTds);
		amendment.getAdditionalInfo().setPartnerCommissionTdsToReturn(partnerCommTds);
		amendment.getAdditionalInfo().setTotalCpAgentCommissionRecalled(totalCpAgentCommissionRecalled);
		amendment.getAdditionalInfo().setCpAgentCommissionTdsToReturn(cpAgentCommissionTdsToReturn);
	}


	public static double handleRefundableComponents(FlightTravellerInfo traveller, Set<FareComponent> refComponents,
			User bookingUser, boolean isSSRefundable, Amendment amendment) {
		String amendmentId = amendment.getAmendmentId();
		if (!MapUtils.isEmpty(traveller.getFareDetail().getFareComponents())) {
			Set<FareComponent> refundableComponents = refComponents;
			double refundableAmount = traveller.getFareDetail().getComponentsSum(refundableComponents);
			for (Map.Entry<FareComponent, Double> entry : traveller.getFareDetail().getOrgManualRefundableFC()
					.entrySet()) {
				FareComponent fc = entry.getKey();
				Double value = entry.getValue();
				refundableAmount += value - traveller.getFareDetail().getFareComponents().getOrDefault(fc, 0d);
			}
			log.info("{} refundableAmount b4 RP {} ", amendmentId, refundableAmount);
			refundableAmount -= traveller.getFareDetail().getFareComponents().getOrDefault(RP, 0d);
			log.info("{} refundableAmount after RP {} ", amendmentId, refundableAmount);
			double maxRefundableAmount = traveller.getFareDetail().getFareComponents().getOrDefault(TF, 0d)
					- traveller.getFareDetail().getFareComponents().getOrDefault(PF, 0d)
					- traveller.getFareDetail().getFareComponents().getOrDefault(MF, 0d)
					- traveller.getFareDetail().getFareComponents().getOrDefault(MFT, 0d)
					- traveller.getFareDetail().getFareComponents().getOrDefault(RP, 0d)
					- ObjectUtils.firstNonNull(traveller.getFareDetail().getPreviousAmendmentFee(), 0d)
					- ObjectUtils.firstNonNull(traveller.getFareDetail().getPreviousAmendmentMarkUp(), 0d);
			log.info("{} maxRefundableAmount after RP {} ", amendmentId, maxRefundableAmount);
			if (!isSSRefundable) {
				maxRefundableAmount -= traveller.getFareDetail().getComponentsSum(FareComponent.getSSRComponents());
			}
			if (amendment.getAdditionalInfo().isRecallCPCommission())
				refundableAmount +=
						traveller.getFareDetail().getComponentsSum(getCancellationProtectionComponents(false));
			traveller.getFareDetail().setMaxRefundableAmount(maxRefundableAmount);
			refundableAmount = (maxRefundableAmount < refundableAmount) ? maxRefundableAmount : refundableAmount;
			log.info("{} refundableAmount after comparison {} ", amendmentId, refundableAmount);
			if (UserRole.corporate(bookingUser.getRole())) {
				zeroOut(traveller.getFareDetail(), FareComponent.getAllCommisionComponents());
			}

			zeroOut(traveller.getFareDetail(), refundableComponents);
			traveller.getFareDetail().getFareComponents().put(AAR, refundableAmount);
			double finalRefundableAmount = refundableAmount;
			traveller.getFareDetail().getFareComponents().computeIfPresent(TF,
					(key, val) -> val - finalRefundableAmount);
			return traveller.getFareDetail().getFareComponents().get(AAR);
		}
		return 0d;
	}


	@Override
	public void PrePopulate() {
		if (amendment.getStatus().equals(AmendmentStatus.ASSIGNED)) {
			AmendmentType amendmentType = amendment.getAmendmentType();
			patchedItems.forEach(airOrderItem -> {
				AirOrderItemToSegmentInfoMapper mapper = AirOrderItemToSegmentInfoMapper.builder()
						.fmsCommunicator(fmsCommunicator).item(new DbAirOrderItem().from(airOrderItem)).build();
				SegmentInfo segmentInfo = mapper.convert();
				fmsCommunicator.updateCancellationAndRescheduleFees(segmentInfo, bookingUser,
						AmendmentType.CANCELLATION);
				resetAmendmentMarkup(segmentInfo);
				if (AmendmentType.CANCELLATION_QUOTATION.equals(amendmentType)
						|| AmendmentType.CANCELLATION.equals(amendmentType)
						|| (AmendmentType.FULL_REFUND.equals(amendmentType)
								&& segmentInfo.getSegmentNum().intValue() == 0)) {
					fmsCommunicator.updateClientAmendmentMarkup(amendmentType, segmentInfo, bookingUser);
				}
				setUpdatedFDForTravellers(segmentInfo, airOrderItem);
				airOrderItem.getTravellerInfo().forEach(pax -> {
					Map<FareComponent, Double> fc = pax.getFareDetail().getFareComponents();
					if (fc.getOrDefault(ACF, 0d) > fc.getOrDefault(BF, 0d)) {
						fc.put(ACF, fc.getOrDefault(BF, 0d));
					}
				});
			});
		}
	}

}
