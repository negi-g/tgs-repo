package com.tgs.services.oms.servicehandler.air;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.air.AirConfirmBookingManager;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.restmodel.air.AirConfirmBookingRequest;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirConfirmBookingHandler extends ServiceHandler<AirConfirmBookingRequest, BookingResponse> {

	@Autowired
	@Qualifier("AirConfirmBookingManager")
	private AirConfirmBookingManager bookingManager;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private AirBookingFareConfirmationHandler fareConfirmationHandler;

	protected final int MAX_RETRIES = 5;

	protected int currentRetry = 0;

	@Override
	public void beforeProcess() throws Exception {
		try {
			CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.FLIGHT.getName())
					.set(CacheSetName.FARE_VALIDATE_CALL_TIME.getName())
					.keys(Arrays.asList(request.getBookingId()).toArray(new String[0])).build();
			Map<String, Map<String, String>> resultSet = cachingCommunicator.get(metaInfo, String.class);
			// if fare validation api is not called in last 15 minutes
			if (MapUtils.isEmpty(resultSet) || resultSet.get(request.getBookingId()) == null
					|| resultSet.get(request.getBookingId()).get(BinName.STOREAT.getName()) == null) {
				log.info("Calling fare validate from confirm booking validator for booking id {}",
						request.getBookingId());
				BookingRequest fareValidateRequest = new BookingRequest();
				fareValidateRequest.setBookingId(request.getBookingId());
				fareConfirmationHandler.initData(fareValidateRequest, new BookingResponse());
				fareConfirmationHandler.getResponse();
				log.info("Fare validation passed for booking {}", request.getBookingId());
			}
		} catch (CustomGeneralException e) {
			LogUtils.log(request.getBookingId(), "ConfirmFareBeforeTicket", e);
			throw e;
		} catch (Exception e) {
			LogUtils.log(request.getBookingId(), "ConfirmFareBeforeTicket", e);
			throw new CustomGeneralException(SystemError.FARE_NO_LONGER_AVAILABLE);
		}
	}

	@Override
	public void process() throws Exception {
		Order order = null;
		List<TripInfo> tripList = null;
		StringJoiner exceptions = new StringJoiner("-");
		try {
			bookingManager.setBookingRequest(request);
			SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
			bookingManager.setBookingUser(SystemContextHolder.getContextData().getUser());
			bookingManager.setLoggedInUser(SystemContextHolder.getContextData().getUser());
			order = orderManager.findByBookingId(request.getBookingId(), order);
			if (Objects.isNull(order)) {
				throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
			}

			boolean isApiPartnerFlow = isApiPartnerFlow();

			if (order.getStatus() != OrderStatus.ON_HOLD) {
				response.setRetryInSecond(3);
				if (this.isRetryRequired(isApiPartnerFlow)) {
					this.retry(isApiPartnerFlow, order);
					return;
				} else {
					throw new CustomGeneralException(SystemError.ORDER_INVALID_ACTION);
				}
			}

			order = orderManager.findByBookingId(order.getBookingId(), order);

			if (CollectionUtils.isNotEmpty(request.getPaymentInfos())) {
				request.getPaymentInfos().forEach(p -> p.setProduct(Product.AIR));
			}

			bookingManager.buildPaymentInfos();

			order = bookingManager.processBooking();

			List<SegmentInfo> segmentList =
					AirBookingUtils.convertAirOrderItemListToSegmentInfoList(bookingManager.getItems(), null);
			tripList = BookingUtils.createTripListFromSegmentList(segmentList);

			if (isConfirmBookingAllowed(order)
					&& order.getAdditionalInfo().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
				fmsCommunicator.doConfirmBooking(tripList, order);
			} else {
				// case when emulate or manual order
				bookingManager.updateItemStatus();
			}
			fmsCommunicator.addBookToAnalytics(order, tripList, request.getPaymentInfos(), exceptions.toString(),
					AirAnalyticsType.CONFIRM_BOOK, null);
		} catch (DataIntegrityViolationException e) {
			log.error("Unable to process order for booking Id {}", request.getBookingId(), e);
			exceptions.add(e.getMessage());
			throw new CustomGeneralException(SystemError.DUPLICATE_ORDER);
		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			exceptions.add(e.getMessage());
			log.error("Unable to process order for booking Id {}", request.getBookingId(), e);
			throw new CustomGeneralException(SystemError.INVALID_REQUEST);
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public boolean isConfirmBookingAllowed(Order order) {
		boolean isAllowed = true;
		if (order.getAdditionalInfo() != null) {
			OrderFlowType flowType = order.getAdditionalInfo().getFlowType();
			if (flowType != null
					&& (flowType.equals(OrderFlowType.IMPORT_PNR) || flowType.equals(OrderFlowType.MANUAL_ORDER))) {
				isAllowed = false;
			}
		}
		return isAllowed;
	}

	private boolean isApiPartnerFlow() {
		User user = SystemContextHolder.getContextData().getUser();
		Integer airApiVersion = UserUtils.getUserAirApiVersion(user);
		return UserUtils.isApiUserRequest(user) && airApiVersion != null && airApiVersion >= 1;
	}

	private void retry(boolean isApiPartnerFlow, Order order) throws Exception {
		OrderStatus currentStatus = order != null ? order.getStatus() : null;
		log.info("Retrying confirm booking logic for the booking Id {} status {} retry number :{}",
				request.getBookingId(), currentStatus, currentRetry);
		Thread.sleep(response.getRetryInSecond() * 1000);
		currentRetry++;
		response.setRetryInSecond(null);
		this.process();
	}

	private boolean isRetryRequired(boolean isApiPartnerFlow) {
		return isApiPartnerFlow && Objects.nonNull(response.getRetryInSecond()) && (currentRetry < MAX_RETRIES);
	}
}
