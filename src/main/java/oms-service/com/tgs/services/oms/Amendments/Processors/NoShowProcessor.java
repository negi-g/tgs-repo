package com.tgs.services.oms.Amendments.Processors;

import static com.tgs.services.base.enums.FareComponent.*;
import java.util.List;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.Amendments.AmendmentHelper;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.mapper.AirOrderItemToSegmentInfoMapper;
import com.tgs.services.oms.utils.air.AirBookingUtils;

@Service
public class NoShowProcessor extends AirAmendmentProcessor {

	@Override
	public void processAmendment() {

		double totalAmtApplicableForRefund, totalCancelFee, commissionRecall, totalTds, partnerMarkUpRecalled,
				partnerCommissionRecalled, partnerMarkUpTds, partnerCommTds, totalCpAgentCommissionRecalled,
				cpAgentCommissionTdsToReturn;

		totalAmtApplicableForRefund = totalCancelFee =
				commissionRecall = totalTds = partnerMarkUpRecalled = partnerCommissionRecalled = partnerMarkUpTds =
						partnerCommTds = totalCpAgentCommissionRecalled = cpAgentCommissionTdsToReturn = 0d;

		if (amendment.getAdditionalInfo().isRecallCommission()) {
			amendment.getAdditionalInfo().setReturnTds(AmendmentHelper.returnTDS(patchedItems.get(0).getCreatedOn()));
		}

		for (AirOrderItem airOrderItem : patchedItems) {

			for (FlightTravellerInfo traveller : airOrderItem.getTravellerInfo()) {

				traveller.setStatus(TravellerStatus.NO_SHOW);
				double paxCancelFee = 0d;
				totalAmtApplicableForRefund += handleRefundableComponents(traveller, airOrderItem);
				double totalPaxFare = traveller.getFareDetail().getFareComponents().getOrDefault(TF, 0d);

				paxCancelFee += traveller.getFareDetail().getFareComponents().getOrDefault(ACF, 0d);
				paxCancelFee += traveller.getFareDetail().getFareComponents().getOrDefault(ACFT, 0d);
				paxCancelFee += traveller.getFareDetail().getFareComponents().getOrDefault(CCF, 0d);
				paxCancelFee += traveller.getFareDetail().getFareComponents().getOrDefault(CCFT, 0d);
				paxCancelFee += traveller.getFareDetail().getFareComponents().getOrDefault(CAMU, 0d);
				if (!amendment.getAdditionalInfo().isRecallCPCommission())
					paxCancelFee +=
							traveller.getFareDetail().getComponentsSum(getCancellationProtectionComponents(false));


				totalCancelFee += paxCancelFee;
				traveller.getFareDetail().setPaxCancellationFee(paxCancelFee);
				if (!amendment.getAdditionalInfo().isRecallCPCommission())
					paxCancelFee -=
							traveller.getFareDetail().getComponentsSum(getCancellationProtectionComponents(false));
				traveller.getFareDetail().getFareComponents().put(TF, totalPaxFare + paxCancelFee);

				commissionRecall += AirBookingUtils.getGrossCommissionForPax(traveller, bookingUser, false);
				// partnerMarkUpRecalled += AirBookingUtils.getPartnerMarkUpForPax(traveller);
				partnerCommissionRecalled += AirBookingUtils.getPartnerCommissionForPax(traveller);
				totalTds += traveller.getFareDetail().getFareComponents().getOrDefault(TDS, 0d);
				partnerMarkUpTds += traveller.getFareDetail().getFareComponents().getOrDefault(PMTDS, 0d);
				partnerCommTds += traveller.getFareDetail().getFareComponents().getOrDefault(PCTDS, 0d);
				if (amendment.getAdditionalInfo().isRecallCPCommission()) {
					totalCpAgentCommissionRecalled +=
							traveller.getFareDetail().getFareComponents().getOrDefault(CPAC, 0d);
					cpAgentCommissionTdsToReturn +=
							traveller.getFareDetail().getFareComponents().getOrDefault(CPACT, 0d);
					zeroOut(traveller.getFareDetail(), FareComponent.getCancellationProtectionComponents(true));
				}
				if (amendment.getAdditionalInfo().isRecallCommission()) {
					zeroOut(traveller.getFareDetail(), FareComponent.getAllCommisionComponents());
				}
				if (amendment.getAdditionalInfo().isReturnTds()) {
					traveller.getFareDetail().getFareComponents().put(TDS, 0d);
				}
			}
		}
		amendment.getAdditionalInfo().setAmountApplicableForRefund(totalAmtApplicableForRefund);
		amendment.getAirAdditionalInfo().setTotalCancellationFee(totalCancelFee);
		amendment.getAdditionalInfo().setTotalCommissionRecalled(commissionRecall);
		amendment.getAdditionalInfo().setTotalPartnerMarkupRecalled(partnerMarkUpRecalled);
		amendment.getAdditionalInfo().setTotalPartnerCommissionRecalled(partnerCommissionRecalled);
		amendment.getAdditionalInfo().setTdsToReturn(totalTds);
		amendment.getAdditionalInfo().setPartnerMarkUpTdsToReturn(partnerMarkUpTds);
		amendment.getAdditionalInfo().setPartnerCommissionTdsToReturn(partnerCommTds);
		amendment.getAdditionalInfo().setTotalCpAgentCommissionRecalled(totalCpAgentCommissionRecalled);
		amendment.getAdditionalInfo().setCpAgentCommissionTdsToReturn(cpAgentCommissionTdsToReturn);
	}


	private double handleRefundableComponents(FlightTravellerInfo traveller, AirOrderItem airOrderItem) {
		/*
		 * Set<FareComponent> refundableFareComponents = FareComponent.getNoShowRefundableComponents(); double
		 * refundableAmount = traveller.getFareDetail().getComponentsSum(refundableFareComponents);
		 * traveller.getFareDetail().getFareComponents().put(AAR, refundableAmount);
		 * 
		 * zeroOut(traveller.getFareDetail(), refundableFareComponents);
		 * traveller.getFareDetail().getFareComponents().computeIfPresent(TF, (key, val) -> val - refundableAmount);
		 * return traveller.getFareDetail().getFareComponents().get(AAR);
		 */
		return CancellationProcessor.handleRefundableComponents(traveller, FareComponent.getRefundableComponents(false),
				bookingUser, false, amendment);
	}


	@Override
	public void PrePopulate() {
		/*
		 * if (amendment.getStatus().equals(AmendmentStatus.ASSIGNED)) { patchedItems.forEach(airOrderItem -> {
		 * airOrderItem.getTravellerInfo().forEach(t -> t.setStatus(TravellerStatus.NO_SHOW));
		 * airOrderItem.getTravellerInfo().forEach(t -> { Map<FareComponent, Double> fareComponents =
		 * t.getFareDetail().getFareComponents(); fareComponents.put(FareComponent.AAR,
		 * t.getFareDetail().getComponentsSum(FareComponent.getNoShowRefundableComponents())); }); }); }
		 */

		if (amendment.getStatus().equals(AmendmentStatus.ASSIGNED)) {
			List<TripInfo> trips = airOrderCommunicator.findTripByBookingId(patchedItems.get(0).getBookingId());
			patchedItems.forEach(airOrderItem -> {
				AirOrderItemToSegmentInfoMapper mapper = AirOrderItemToSegmentInfoMapper.builder()
						.fmsCommunicator(fmsCommunicator).item(new DbAirOrderItem().from(airOrderItem)).build();
				SegmentInfo segmentInfo = mapper.convert();
				trips.forEach(trip -> {
					trip.getSegmentInfos().forEach(segmentInf -> {
						if (segmentInf.getId().equals(segmentInfo.getId())
								&& segmentInf.getSegmentNum().intValue() == 0) {
							resetAmendmentMarkup(segmentInfo);
							fmsCommunicator.updateClientAmendmentMarkup(AmendmentType.NO_SHOW, segmentInfo,
									bookingUser);
							setUpdatedFDForTravellers(segmentInfo, airOrderItem);
						}
					});
				});
			});
		}
	}
}
