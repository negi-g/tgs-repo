package com.tgs.services.oms.hibernate.rail;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.rail.datamodel.RailTravellerInfo;

public class RailTravellerInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		List<RailTravellerInfo> travellerList = new ArrayList<>();
		return travellerList.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<RailTravellerInfo>>() {}.getType();
	}
}
