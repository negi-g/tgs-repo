package com.tgs.services.oms.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tgs.services.oms.dbmodel.DbOrder;

@Repository
public interface OrderRepository extends JpaRepository<DbOrder, Long>, JpaSpecificationExecutor<DbOrder> {

	DbOrder findByBookingId(String bookingId);

	List<DbOrder> findByBookingIdIn(List<String> bookingIds);
	
	@Query(value = "delete from orders where bookingId=:bookingId and status in ('F', 'IP') returning bookingId", nativeQuery = true)
	List<String> deleteFailedAndNewOrderByBookingId(@Param("bookingId") String bookingId);
}
