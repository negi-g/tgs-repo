package com.tgs.services.oms.servicehandler.hotel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.hotel.HotelForceCancellationRequest;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.hotel.HotelBookingUtils;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.ruleengine.RefundOutput;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class HotelBookingCancellationHandler {

	@Autowired
	HotelOrderItemService itemService;

	@Autowired
	OrderManager orderManager;

	@Autowired
	private PaymentServiceCommunicator paymentServiceCommunicator;

	@Autowired
	HotelOrderItemManager itemManager;

	@Autowired
	HMSCommunicator hmsComm;

	@Autowired
	protected HotelOrderMessageHandler messageHandler;

	@Autowired
	protected GeneralServiceCommunicator gmsCommunicator;

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public void bookingCancel(HotelPostBookingBaseRequest cancelBookingRequest, boolean isJob) {
		List<DbHotelOrderItem> orderItems = null;
		String bookingId = cancelBookingRequest.getBookingId();
		Order order = null;
		try {
			log.info("Cancelling booking for bookingId {}", bookingId);
			order = orderManager.findByBookingId(bookingId, null);
			orderItems = itemService.findByBookingIdOrderByIdAsc(bookingId);
			HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(orderItems).build().convert();
			boolean isBookingCancelled = hmsComm.callSupplierCancelBooking(hInfo, order.getBookingId());
			if (isBookingCancelled) {
				HotelItemStatus itemStatus = HotelItemStatus.CANCELLED;
				Double totalVoucherDiscountAmount = getVoucherDiscountFromOrder(hInfo, order, null);
				Double totalChargedAmount = order.getAmount() - totalVoucherDiscountAmount;

				if (!OrderUtils.isPaymentAlreadyDone(order, PaymentTransactionType.REFUND,
						new BigDecimal(totalChargedAmount - getRefundCharges(hInfo)))) {
					order.setPayments(paymentServiceCommunicator.fetchPaymentByBookingId(order.getBookingId()).stream()
							.filter(x -> x.getStatus().equals(PaymentStatus.SUCCESS)).collect(Collectors.toList()));
					if (CollectionUtils.isNotEmpty(order.getPayments())) {
						refundPayments(order, hInfo);
					} else if (isJob) {
						itemStatus = HotelItemStatus.UNCONFIRMED;
					}
				}
				order.getAdditionalInfo().setPaymentStatus(PaymentStatus.REFUND_SUCCESS);
				gmsCommunicator.addNote(Note.builder().noteType(NoteType.CANCELLATION_REASON)
						.noteMessage(StringUtils.isBlank(cancelBookingRequest.getReason()) ? "no reason specified"
								: cancelBookingRequest.getReason())
						.bookingId(order.getBookingId()).build());

				itemManager.updateOrderItem(hInfo, order, itemStatus);
			} else {
				itemManager.updateOrderItem(hInfo, order, null);
				messageHandler.sendBookingEmail(order, orderItems,
						EmailTemplateKey.HOTEL_BOOKING_CANCELLATION_FAILED_EMAIL, null);
				gmsCommunicator
						.addNote(Note.builder().bookingId(bookingId).noteType(NoteType.BOOKING_CANCELLATION_FAILED)
								.noteMessage("Booking Cancellation Failed From Supplier").build());
				throw new CustomGeneralException(SystemError.ORDER_CANCELLATION_FAILED);
			}
		} catch (CustomGeneralException e) {
			log.info("Booking Cannot Be Cancelled {}", bookingId, e);
			throw e;
		} catch (Exception e) {
			log.error("Unable to cancelbooking for bookingid {}", bookingId, e);
			messageHandler.sendBookingEmail(order, orderItems, EmailTemplateKey.HOTEL_BOOKING_CANCELLATION_FAILED_EMAIL,
					null);
			gmsCommunicator.addNote(Note.builder().bookingId(bookingId).noteType(NoteType.BOOKING_CANCELLATION_FAILED)
					.noteMessage("Booking Cancellation Failed ").build());
			throw new CustomGeneralException(SystemError.ORDER_CANCELLATION_FAILED);
		}
	}

	public void cancelBooking(HotelPostBookingBaseRequest cancelBookingRequest, boolean isJob) {
		List<DbHotelOrderItem> orderItems = null;
		String bookingId = cancelBookingRequest.getBookingId();
		log.info("Cancelling booking for bookingId {}", bookingId);
		Order order = orderManager.findByBookingId(bookingId, null);
		if (order == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		orderItems = itemService.findByBookingIdOrderByIdAsc(bookingId);
		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(orderItems).build().convert();
		HotelBookingUtils.canCancel(hInfo, order, true);
		itemManager.updateOrderItem(hInfo, order, HotelItemStatus.CANCELLATION_PENDING);
		bookingCancel(cancelBookingRequest, isJob);

	}


	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public void forcefullyCancelBooking(HotelForceCancellationRequest cancellationRequest) {
		Order order = null;
		try {
			log.info("Forcefully cancelling booking for bookingId {}", cancellationRequest.getBookingid());
			order = orderManager.findByBookingId(cancellationRequest.getBookingid(), null);
			if (order == null)
				throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);

			List<DbHotelOrderItem> orderItems =
					itemService.findByBookingIdOrderByIdAsc(cancellationRequest.getBookingid());
			HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(orderItems).build().convert();
			HotelBookingUtils.canCancel(hInfo, order, true);
			HotelItemStatus itemStatus = HotelItemStatus.CANCELLED;
			order.setPayments(paymentServiceCommunicator.fetchPaymentByBookingId(order.getBookingId()).stream()
					.filter(x -> x.getStatus().equals(PaymentStatus.SUCCESS)).collect(Collectors.toList()));
			if (CollectionUtils.isNotEmpty(order.getPayments()) && cancellationRequest.getAmount() > 0
					&& !OrderUtils.isPaymentAlreadyDone(order, PaymentTransactionType.REFUND,
							new BigDecimal(cancellationRequest.getAmount()))) {
				forcefullyRefundPayments(order, hInfo, cancellationRequest.getAmount());
			}
			order.getAdditionalInfo().setIsForceCancellation(true);
			order.getAdditionalInfo().setIsReconciledWithSupplier(cancellationRequest.isReconciledWithSupplier());
			order.setReason(cancellationRequest.getReason());
			gmsCommunicator.addNote(Note.builder().noteType(NoteType.FORCEFULLY_CANCELLATION_REASON)
					.noteMessage(StringUtils.isBlank(cancellationRequest.getReason()) ? "no reason specified"
							: cancellationRequest.getReason())
					.bookingId(order.getBookingId()).build());

			order.getAdditionalInfo().setPaymentStatus(PaymentStatus.REFUND_SUCCESS);
			itemManager.updateOrderItem(hInfo, order, itemStatus);
		} catch (CustomGeneralException e) {
			log.info("Booking Cannot Be Cancelled for booking id {}", cancellationRequest.getBookingid());
			throw e;
		} catch (Exception e) {
			log.error("Unable to cancelbooking for bookingid {}", cancellationRequest.getBookingid());
			throw new CustomGeneralException(SystemError.ORDER_CANCELLATION_FAILED);
		}
	}

	private void forcefullyRefundPayments(Order order, HotelInfo hInfo, Double amount) {
		List<PaymentRequest> paymentRequests = createRefundPayments(order, hInfo, BigDecimal.valueOf(amount));
		if (CollectionUtils.isNotEmpty(paymentRequests)) {
			List<Payment> refundPayments = paymentServiceCommunicator.doPaymentsUsingPaymentRequests(paymentRequests);
			order.getPayments().addAll(refundPayments);
		}
	}

	private void refundPayments(Order order, HotelInfo hInfo) {
		List<PaymentRequest> paymentRequests = null;
		Double refundCharges = getRefundCharges(hInfo);
		Double totalVoucherDiscountAmount = getVoucherDiscountFromOrder(hInfo, order, null);
		Double totalChargedAmount = order.getAmount() - totalVoucherDiscountAmount;
		BigDecimal amount = new BigDecimal(totalChargedAmount - refundCharges);
		if (amount.doubleValue() <= 0.0)
			return;
		else
			paymentRequests = createRefundPayments(order, hInfo, amount);
		List<Payment> refundPayments = paymentServiceCommunicator.doPaymentsUsingPaymentRequests(paymentRequests);
		order.getPayments().addAll(refundPayments);
	}

	private List<PaymentRequest> createRefundPayments(Order order, HotelInfo hInfo, BigDecimal amount) {
		List<PaymentRequest> refundPayments = new ArrayList<>();
		Payment payment = order.getPayments().stream()
				.filter(p -> p.getType().getOpType().equals(PaymentTransactionType.PAID_FOR_ORDER.getOpType()))
				.findFirst().get();
		PaymentFact fact = PaymentFact.builder().medium(payment.getPaymentMedium()).build();
		List<PaymentConfigurationRule> rules = paymentServiceCommunicator.getApplicableRulesOutput(fact,
				PaymentRuleType.REFUND, payment.getPaymentMedium());
		PaymentMedium refundMedium = PaymentMedium.FUND_HANDLER;
		if (!CollectionUtils.isEmpty(rules)) {
			refundMedium = ((RefundOutput) rules.get(0).getOutput()).getMedium();
		}
		if (refundMedium.equals(PaymentMedium.FUND_HANDLER)) {
			refundPayments
					.add(HotelBookingUtils.createFundHandlerRequests(payment, PaymentTransactionType.REFUND, amount));
		} else {
			payment.setPaymentMedium(refundMedium);
			refundPayments.add(HotelBookingUtils.createRefundPayments(payment, PaymentTransactionType.REFUND, amount));
		}
		return refundPayments;
	}

	public Double getRefundCharges(HotelInfo hInfo) {
		Option option = hInfo.getOptions().get(0);
		double refundCharges = 0;

		HotelCancellationPolicy cp = option.getCancellationPolicy();
		if (CollectionUtils.isNotEmpty(cp.getRoomCancellationPolicyList())) {
			refundCharges += getRefundChargesFromRoomCancellationPolicy(cp);
		} else {
			refundCharges += getRefundChargesFromOptionCancellationPolicy(cp);
		}


		for (RoomInfo roomInfo : option.getRoomInfos()) {
			refundCharges += roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF)
					.getOrDefault(HotelFareComponent.MF, 0d)
					+ roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF)
							.getOrDefault(HotelFareComponent.MFT, 0d)
					+ roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF)
							.getOrDefault(HotelFareComponent.PF, 0d)
					+ roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF)
							.getOrDefault(HotelFareComponent.RP, 0d);
		}
		return refundCharges;
	}


	private void updateSupplierCancellationChargeForRoom(HotelInfo hInfo, RoomInfo roomInfo) {

		if (roomInfo.getRoomStatus().equals(OrderStatus.CANCELLED.getCode()))
			return;
		Option option = hInfo.getOptions().get(0);
		double refundCharges = 0;
		int totalRooms = option.getRoomInfos().size();

		HotelCancellationPolicy cp = option.getCancellationPolicy();
		if (CollectionUtils.isNotEmpty(cp.getRoomCancellationPolicyList())) {
			refundCharges += getRefundChargesFromRoomCancellationPolicy(cp);
		} else {
			refundCharges += getRefundChargesFromOptionCancellationPolicy(cp);
		}

		refundCharges /= totalRooms;

		int roomNights = roomInfo.getPerNightPriceInfos().size();
		double perNightSupplierCharge = refundCharges / roomNights;
		roomInfo.getPerNightPriceInfos().forEach(perNight -> {
			perNight.getAddlFareComponents().get(HotelFareComponent.TAF).put(HotelFareComponent.SCF,
					roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF)
							.getOrDefault(HotelFareComponent.SCF, perNightSupplierCharge));
		});

		roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF).put(HotelFareComponent.SCF,
				roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF).getOrDefault(HotelFareComponent.SCF,
						refundCharges));
	}

	private double getRefundChargesFromOptionCancellationPolicy(HotelCancellationPolicy cp) {

		/*
		 * If current datetime exceeds last cancellation policy datetime.
		 */
		LocalDateTime currentTime = LocalDateTime.now();
		if (currentTime.isAfter(cp.getPenalyDetails().get(cp.getPenalyDetails().size() - 1).getToDate())) {
			return cp.getPenalyDetails().get(cp.getPenalyDetails().size() - 1).getPenaltyAmount();
		}

		double refundCharges = 0;
		for (PenaltyDetails pd : cp.getPenalyDetails()) {
			refundCharges += getRefundChargeFromPenaltyDetail(pd);
		}

		return refundCharges;
	}

	private double getRefundChargeFromPenaltyDetail(PenaltyDetails pd) {

		LocalDateTime currentTime = LocalDateTime.now();
		if (pd.getFromDate().isBefore(currentTime) && pd.getToDate().isAfter(currentTime)) {
			return pd.getPenaltyAmount();
		}
		return 0;
	}

	private double getRefundChargesFromRoomCancellationPolicy(HotelCancellationPolicy cp) {

		double refundCharges = 0;
		List<HotelCancellationPolicy> cpList = cp.getRoomCancellationPolicyList();

		/*
		 * If current datetime exceeds last cancellation policy datetime.
		 */
		LocalDateTime currentTime = LocalDateTime.now();
		if (currentTime.isAfter(cpList.get(cpList.size() - 1).getPenalyDetails()
				.get(cpList.get(cpList.size() - 1).getPenalyDetails().size() - 1).getToDate())) {
			return cpList.get(cpList.size() - 1).getPenalyDetails()
					.get(cpList.get(cpList.size() - 1).getPenalyDetails().size() - 1).getPenaltyAmount();
		}

		for (HotelCancellationPolicy roomCancellationPolicy : cpList) {
			for (PenaltyDetails pd : roomCancellationPolicy.getPenalyDetails()) {
				refundCharges += getRefundChargeFromPenaltyDetail(pd);
			}
		}

		return refundCharges;
	}

	public double getVoucherDiscountFromOrder(HotelInfo hInfo, Order order, Set<String> roomKeys) {
		double totalVoucherDiscountAmount = 0.0;
		if (order.getAdditionalInfo().getVoucherCode() != null) {
			totalVoucherDiscountAmount = hInfo.getOptions().get(0).getRoomInfos().stream().mapToDouble(roomInfo -> {
				if (CollectionUtils.isEmpty(roomKeys) || roomKeys.contains(roomInfo.getId()))
					return roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF).get(HotelFareComponent.VD);
				return 0.0;
			}).sum();

		}
		return totalVoucherDiscountAmount;
	}

	public double getPartialRefundAmount(HotelInfo hInfo, Order order, Set<String> roomKeys) {
		double totalVoucherDiscountAmount = getVoucherDiscountFromOrder(hInfo, order, roomKeys);
		double totalAmount = hInfo.getOptions().get(0).getRoomInfos().stream().mapToDouble(roomInfo -> {
			double totalFare = 0.0;
			if (CollectionUtils.isEmpty(roomKeys) || roomKeys.contains(roomInfo.getId())) {
				totalFare = roomInfo.getTotalFareComponents().get(HotelFareComponent.NF);
			}
			return totalFare;
		}).sum();
		double totalChargedAmount = totalAmount - totalVoucherDiscountAmount;
		return totalChargedAmount - getPartialRefundCharges(hInfo, roomKeys);
	}

	public void updateCancellationFare(HotelInfo hInfo, Set<String> roomKeys) {
		hInfo.getOptions().get(0).getRoomInfos().forEach(roomInfo -> {
			if (CollectionUtils.isEmpty(roomKeys) || roomKeys.contains(roomInfo.getId())) {
				updateSupplierCancellationChargeForRoom(hInfo, roomInfo);
			}
		});
	}

	/**
	 * Updates the Supplier Cancellation Fee from Cancellation Policy if not available and calculates the amount that
	 * needs to be refunded for selected rooms.
	 * 
	 * @param hInfo
	 * @param roomKeys
	 * @return cancellationCharges for the Cancelling Rooms
	 */
	public double getPartialRefundCharges(HotelInfo hInfo, Set<String> roomKeys) {
		updateCancellationFare(hInfo, roomKeys);
		double refundCharges = 0;
		Option option = hInfo.getOptions().get(0);

		for (RoomInfo roomInfo : option.getRoomInfos()) {
			if (CollectionUtils.isEmpty(roomKeys) || roomKeys.contains(roomInfo.getId())) {
				refundCharges += getRoomRefundCharges(roomInfo);
			}
		}
		return refundCharges;
	}

	public double getRoomRefundCharges(RoomInfo roomInfo) {
		return HotelFareComponent.getNonRefundableFareComponenets().stream()
				.mapToDouble(fareComponenet -> roomInfo.getTotalFareComponents().getOrDefault(fareComponenet, roomInfo
						.getTotalAddlFareComponents().get(HotelFareComponent.TAF).getOrDefault(fareComponenet, 0d)))
				.sum();
	}

	public void updateRefundAmount(HotelInfo hInfo, Amendment amendment) {
		
		if(CollectionUtils.isNotEmpty(paymentServiceCommunicator.fetchPaymentByBookingId(amendment.getBookingId()).stream()
				.filter(x -> x.getStatus().equals(PaymentStatus.SUCCESS)).collect(Collectors.toList()))) {
			Set<String> roomKeys = amendment.getAdditionalInfo().getHotelAdditionalInfo().getRoomKeys();
			updateCancellationFare(hInfo, roomKeys);
			BaseHotelUtils.updateTotalFareComponents(hInfo, null);
			Option option = hInfo.getOptions().get(0);

			for (RoomInfo roomInfo : option.getRoomInfos()) {
				if (CollectionUtils.isEmpty(roomKeys) || roomKeys.contains(roomInfo.getId())) {
					double refundAmount =
							roomInfo.getTotalFareComponents().get(HotelFareComponent.NF) - getRoomRefundCharges(roomInfo);
					
					// Added this condition as said. We do not allow extra charge for Cancellation Amendment.
					// Should remove the condition once CNP price is matched with BF for all suppliers
					if(refundAmount < 0) {
						refundAmount = 0;
					}
					int totalNights = roomInfo.getPerNightPriceInfos().size();
					double perNightRefund = refundAmount / totalNights;
					for (PriceInfo perNightPrice : roomInfo.getPerNightPriceInfos()) {
						perNightPrice.getFareComponents().put(HotelFareComponent.AAR, perNightRefund);
					}
				}
			}
			BaseHotelUtils.updateTotalFareComponents(hInfo, null);
		}
	}


}
