package com.tgs.services.oms.manager.rail;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.rail.RailItemStatus;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.manager.BookingManager;
import com.tgs.services.oms.restmodel.rail.RailBookingRequest;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailSearchQuery;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.datamodel.booking.AdditionalRailOrderItemInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Getter
@Setter
@Slf4j
public class RailBookingManager extends BookingManager {

	protected RailBookingRequest railBookingRequest;
	protected DbRailOrderItem item;
	protected RailJourneyInfo journeyInfo;
	protected RailSearchQuery searchQuery;
	protected List<RailTravellerInfo> travellerInfos;

	protected Double orderAmount;
	protected Double markup;

	@Autowired
	protected RailOrderItemManager itemManager;

	@Autowired
	protected GeneralCachingCommunicator cachingCommunicator;


	public Order processBooking() {
		railBookingRequest = (RailBookingRequest) bookingRequest;
		bookingRequest.setGstInfo(Optional.ofNullable(journeyInfo.getBookingRelatedInfo().getGstInfo()).orElse(null));
		this.storeGstInfo();
		this.storeOrderItems();
		this.createOrder();
		this.addOrderToAudit(AuditAction.ORDER_CREATE);
		log.info("Order items successfully stored for bookingId {}", railBookingRequest.getBookingId());
		this.doPayment();
		this.doPostPayment();
		return order;
	}

	private void doPostPayment() {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
				.set(CacheSetName.EXTERNAL_PAYMENTS.getName()).key("successurl" + railBookingRequest.getBookingId())
				.build();
		/**
		 * Storing success Url which will be used post redirection from IRCTC
		 */
		cachingCommunicator.store(metaInfo, BinName.PAYMENTS.name(),
				Arrays.asList(SystemContextHolder.getContextData().getHttpHeaders().getSuccessUrl()), false, false,
				600);
	}

	private void storeOrderItems() {
		DbRailOrderItem item = new DbRailOrderItem();
		item.setAmount(orderAmount);
		item.setBookingId(railBookingRequest.getBookingId());
		item.setArrivalTime(journeyInfo.getArrival());
		item.setDepartureTime(journeyInfo.getDeparture());
		item.setFromStn(journeyInfo.getDepartStationInfo().getCode());
		item.setToStn(journeyInfo.getArrivalStaionInfo().getCode());
		item.setStatus(RailItemStatus.IN_PROGRESS.getCode());
		item.setTravellerInfo(travellerInfos);
		AdditionalRailOrderItemInfo additionalInfo = AdditionalRailOrderItemInfo.builder().build();
		additionalInfo.setRailInfo(journeyInfo.getRailInfo());
		additionalInfo.setDistance(journeyInfo.getDistance());
		additionalInfo.setDuration(Long.parseLong(journeyInfo.getDuration()));
		additionalInfo.setDays(journeyInfo.getDays());
		additionalInfo.setPriceInfo(journeyInfo.getAvlJourneyDetailsMap().get(journeyInfo.getAvlJourneyClasses().get(0))
				.getRailAvailabilityDetails().get(0).getPriceInfo());
		additionalInfo.setTicketAddress(journeyInfo.getBookingRelatedInfo().getTicketAddress());
		additionalInfo.setQuota(journeyInfo.getBookingRelatedInfo().getQuota());
		additionalInfo.setJourneyClass(journeyInfo.getBookingRelatedInfo().getJourneyClass());
		additionalInfo.setBoardingStation(
				journeyInfo.getBoardingStationInfo() != null ? journeyInfo.getBoardingStationInfo().getCode() : null);
		additionalInfo.setBoardingTime(journeyInfo.getBoardingTime());
		additionalInfo.setPassengerMobileNumber(journeyInfo.getBookingRelatedInfo().getMobileNumber());
		additionalInfo.setIsThroughOTP(journeyInfo.getBookingRelatedInfo().getIsThroughOTP());
		additionalInfo.setIsTravelInsuranceOpted(journeyInfo.getBookingRelatedInfo().getIsTravelInsuranceOpted());
		item.setAdditionalInfo(additionalInfo);
		orderTotal = railBookingRequest.getPaymentInfos().get(0).getAmount().doubleValue();
		item.setAmount(orderTotal);
		this.item = item;
		itemManager.save(item, order);	
	}

	@Override
	public void updateItemStatus(OrderStatus orderStatus) {
		RailItemStatus itemStatus = orderStatus.equals(OrderStatus.PAYMENT_SUCCESS) ? RailItemStatus.PAYMENT_SUCCESS
				: RailItemStatus.PAYMENT_FAILED;
		item.setStatus(itemStatus.getCode());
		itemManager.save(item, order);
	}

	@Override
	protected List<PaymentRequest> createVirualPaymentRequest(List<PaymentRequest> paymentInfos,
			GstInfo orderBillingEntity) {
		return null;
	}

	@Override
	protected void applyVoucher() {

	}


}
