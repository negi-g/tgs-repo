package com.tgs.services.oms.communicator.impl;

import java.time.LocalDateTime;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.communicator.RailOrderItemCommunicator;
import com.tgs.services.oms.datamodel.rail.RailItemStatus;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.manager.rail.RailOrderItemManager;
import com.tgs.services.oms.mapper.rail.RailOrderItemToJourneyInfoMapper;
import com.tgs.services.oms.servicehandler.rail.RailAbortHandler;
import com.tgs.services.oms.utils.rail.RailBookingUtils;
import com.tgs.services.rail.datamodel.RailJourneyInfo;

@Service
public class RailOrderItemCommImpl implements RailOrderItemCommunicator {


	@Autowired
	protected RailOrderItemService itemService;

	@Autowired
	@Lazy
	protected RailOrderItemManager itemManager;

	@Autowired
	@Lazy
	protected RailCommunicator railComm;

	@Autowired
	@Lazy
	protected RailAbortHandler abortHandler;

	@Override
	public RailJourneyInfo getJourneyInfo(String bookingId) {
		return RailOrderItemToJourneyInfoMapper.builder().railComm(railComm)
				.item(itemService.findByBookingId(bookingId)).build().convert();
	}

	@Override
	public void updateOrderItem(RailItemStatus itemStatus, RailJourneyInfo journeyInfo, String bookingId) {
		DbRailOrderItem item = itemService.findByBookingId(bookingId);
		item.setStatus(itemStatus.getCode());
		if (RailItemStatus.SUCCESS.equals(itemStatus)) {
			item.setTravellerInfo(journeyInfo.getBookingRelatedInfo().getTravellerInfos());
			item.getAdditionalInfo().setReservationUptoStation(journeyInfo.getReservationUptoStationInfo().getCode());
			item.getAdditionalInfo().setBoardingStation(journeyInfo.getBoardingStationInfo().getCode());
			item.getAdditionalInfo().setReservationId(journeyInfo.getBookingRelatedInfo().getReservationId());
			item.getAdditionalInfo().setAvblForVikalp(journeyInfo.getAvlblForVikalp());
			item.getAdditionalInfo().setPnr(journeyInfo.getBookingRelatedInfo().getPnr());
			item.setPnr(journeyInfo.getBookingRelatedInfo().getPnr());
			item.getAdditionalInfo().setRailInfo(journeyInfo.getRailInfo());
		}
		itemManager.save(item, null);
	}

	@Override
	public RailOrderItem updateOrderItem(String bookingId, String newBoardingStation, LocalDateTime newBoardingDate) {
		DbRailOrderItem item = itemService.findByBookingId(bookingId);
		item.getAdditionalInfo().setOldboardingStation(item.getAdditionalInfo().getBoardingStation());
		item.getAdditionalInfo().setBoardingStation(newBoardingStation);
		item.getAdditionalInfo().setOldboardingDateTime(item.getAdditionalInfo().getBoardingTime());
		item.getAdditionalInfo().setBoardingTime((newBoardingDate));
		item = itemService.save(item);
		return item.toDomain();

	}

	@Override
	public RailOrderItem getRailOrderItem(String bookingId) {
		DbRailOrderItem item = itemService.findByBookingId(bookingId);
		return item.toDomain();
	}

	@Override
	public void updateOrderItem(String bookingId, String optedTrainNumbers) {
		DbRailOrderItem item = itemService.findByBookingId(bookingId);
		item.getAdditionalInfo().setOptedTrainNumbers(optedTrainNumbers);
		item.getAdditionalInfo().setVikalpOptStatus(StringUtils.isNotEmpty(optedTrainNumbers));
		itemService.save(item);
	}

	@Override
	public void deleteFromCache(String key) {
		RailBookingUtils.deleteKeyFromCache(key);
	}

	@Override
	public void abortBooking(String bookingId) {
		abortHandler.abortBooking(bookingId);
	}

}
