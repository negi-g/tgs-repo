package com.tgs.services.oms.servicehandler.rail;

import static com.tgs.services.base.helper.SystemError.ORDER_NOT_FOUND;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteAdditionalInfo;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.Amendments.RailAmendmentManager;
import com.tgs.services.oms.Amendments.RailAmendmentOtpManager;
import com.tgs.services.oms.Amendments.RailMessagingClient;
import com.tgs.services.oms.Amendments.RailTdrFilingManager;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAdditionalInfo;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.amendments.RailAdditionalInfo;
import com.tgs.services.oms.datamodel.rail.RailItemStatus;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.manager.rail.RailOrderItemManager;
import com.tgs.services.oms.mapper.rail.RailOrderItemToJourneyInfoMapper;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.rail.RailAmendmentRequest;
import com.tgs.services.oms.restmodel.rail.RailCancellationRequest;
import com.tgs.services.oms.restmodel.rail.RailCancellationResponse;
import com.tgs.services.oms.restmodel.rail.RailOtpVerificationRequest;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RaiseRailAmendmentHandler extends ServiceHandler<RailAmendmentRequest, AmendmentResponse> {

	@Autowired
	private OrderService orderService;

	@Autowired
	private RailOrderItemService railOrderItemService;

	@Autowired
	private AmendmentService amendmendService;

	@Autowired
	private RailCancellationHandler cancellationHandler;

	@Autowired
	private RailCommunicator railComm;

	@Autowired
	protected UserServiceCommunicator userService;

	protected Order originalOrder;

	@Setter
	protected Amendment amendment;

	protected DbRailOrderItem orderItem;

	protected RailJourneyInfo journeyInfo;

	@Autowired
	private RailOrderItemManager itemManager;

	@Autowired
	private GeneralServiceCommunicator generalServiceCommunicator;

	@Autowired
	private RailAmendmentManager railAmendmentManager;

	@Autowired
	private RailTdrFilingManager railTdrFilingManager;

	@Autowired
	protected RailMessagingClient messagingClient;

	@Override
	public void beforeProcess() throws Exception {
		originalOrder = orderService.findByBookingId(request.getBookingId()).toDomain();
		if (originalOrder == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		UserServiceHelper.checkAndReturnAllowedUserId(SystemContextHolder.getContextData().getEmulateOrLoggedInUserId(),
				Arrays.asList(originalOrder.getBookingUserId()));

		orderItem = railOrderItemService.findByBookingId(request.getBookingId());
		log.info("OrderItem found for bookingId {}", request.getBookingId());
		journeyInfo = RailOrderItemToJourneyInfoMapper.builder().item(orderItem).railComm(railComm).build().convert();
		if (orderItem == null)
			throw new CustomGeneralException(ORDER_NOT_FOUND);

		validateAmend();
		if (StringUtils.isNotBlank(request.getAmendmentId())) {
			log.info("Processing TDR Amendment for bookingId {}, amendmentId {}", request.getBookingId(),
					request.getAmendmentId());
			amendment = amendmendService.findByAmendmentId(request.getAmendmentId());
			request.setIsWithoutOtp(true);
			request.setIsTdrProcess(true);
			request.setTdrInfo(amendment.getRailAdditionalInfo().getTdrInfo());
		} else {
			railAmendmentManager.checkAmendStatus(request.getBookingId(), request.getPaxIds());
			amendment = createAmendment(originalOrder);
		}
	}

	private void validateAmend() {
		if (CollectionUtils.isEmpty(request.getPaxIds()))
			throw new CustomGeneralException(SystemError.NO_PAX_SELECTED);

		boolean isPendingOrder = railAmendmentManager.checkPendingOrder(request.getType(), orderItem);
		if (isPendingOrder) {
			throw new CustomGeneralException(SystemError.AMENDMENT_ALREADY_PROCESSED);
		}
	}

	@Override
	public void process() throws Exception {

		RailAdditionalInfo railAdditionalInfo = amendment.getRailAdditionalInfo();
		if (request.getType() == AmendmentType.CANCELLATION || request.getType() == AmendmentType.OFFLINE_CANCEL
				|| BooleanUtils.isTrue(request.getIsTdrProcess())) {
			try {

				railAdditionalInfo.setOrderPreviousSnapshot(orderItem.toDomain());
				RailCancellationRequest canRequest = RailCancellationRequest.builder()
						.isOfflineCancellation(((request.getType() == AmendmentType.OFFLINE_CANCEL) ? true : false)
								|| BooleanUtils.isTrue(request.getIsTdrProcess()))
						.agentCanId(request.getAgentCanId())
						.reservationId(orderItem.getAdditionalInfo().getReservationId()).amendment(amendment)
						.bookingId(request.getBookingId()).paxIds(request.getPaxIds())
						.priceInfo(orderItem.getAdditionalInfo().getPriceInfo()).journeyInfo(journeyInfo).build();
				log.info("Initializing Cancellationhandler for Rail for bookingId {}", request.getBookingId());
				canRequest.setReservationId(orderItem.getAdditionalInfo().getReservationId());
				cancellationHandler.initData(canRequest, new RailCancellationResponse());
				RailCancellationResponse cancellationResponse = cancellationHandler.getResponse();
				log.info("Cancellation Response received for bookingId {}", request.getBookingId());
				setAmendmentInfo(amendment, cancellationResponse, orderItem);
				updateOrderItem(cancellationResponse);
				updateCancelledPaxIds(railAdditionalInfo);
				railAdditionalInfo.setOrderCurrentSnapshot(orderItem.toDomain());
				railAdditionalInfo.setCancellationId(journeyInfo.getBookingRelatedInfo().getCancellationId());
				railAdditionalInfo.setIsWithoutOtp(request.getIsWithoutOtp());
				amendment.getAdditionalInfo().setRailAdditionalInfo(railAdditionalInfo);
				response.setRailCancellationResponse(cancellationResponse);
				log.info("OrderItem Saved for bookingId {}", request.getBookingId());
				itemManager.save(orderItem, originalOrder);
				if (BooleanUtils.isTrue(request.getIsWithoutOtp())
						&& BooleanUtils.isTrue(cancellationResponse.getIsCancelSuccess())) {
					amendment = amendmendService.save(amendment);
					RailOtpVerificationRequest otpRequest = new RailOtpVerificationRequest();
					otpRequest.setAmendmentId(amendment.getAmendmentId());
					otpRequest.setBookingId(amendment.getBookingId());
					otpRequest.setCancellationId(railAdditionalInfo.getCancellationId());
					otpRequest.setPnrNo(orderItem.getAdditionalInfo().getPnr());
					otpRequest.setIsWithoutOtp(true);
					RailAmendmentOtpManager otpManager = SpringContext.getApplicationContext()
							.getBean(RailAmendmentOtpManager.class);
					otpManager.submitOtp(otpRequest);
					// To fetch the updated amendment, because amendment has been processed.
					amendment = amendmendService.findByAmendmentId(amendment.getAmendmentId());
				}
			} catch (CustomGeneralException e) {
				Note note = Note.builder().bookingId(amendment.getBookingId()).noteType(NoteType.CANCELLATION_REASON)
						.noteMessage(e.getMessage())
						.additionalInfo(NoteAdditionalInfo.builder().visibleToAgent(true).build()).build();

				generalServiceCommunicator.addNote(note);
				log.info("Amendment Cancellation Failed for BookingId {} ,due to {}", request.getBookingId(),
						e.getMessage(), e);
				amendment.setStatus(AmendmentStatus.REJECTED);
				amendment.setAssignedUserId(null);
				amendment.setAssignedOn(null);

			} catch (Exception e) {
				log.error("Amendment Auto Cancelled Failed for BookingId {} ", request.getBookingId(), e);
				amendment.setStatus(AmendmentStatus.REJECTED);
				amendment.setAssignedUserId(null);
				amendment.setAssignedOn(null);
			}
		} else if (request.getType() == AmendmentType.TDR) {
			railTdrFilingManager.fileTdr(request, amendment, orderItem.getAdditionalInfo().getReservationId());
		}
		amendment = amendmendService.save(amendment);
		response.getAmendmentItems().add(amendment);

	}

	private void updateCancelledPaxIds(RailAdditionalInfo railAdditionalInfo) {
		Set<String> alreadyCancelledPaxIds = getPaxCancelledIds(railAdditionalInfo.getOrderPreviousSnapshot());
		log.info("Already Cancelled Pax Ids are : {}", alreadyCancelledPaxIds);
		Set<String> allCancelledPaxIds = getPaxCancelledIds(orderItem.toDomain());
		log.info("All Cancelled Pax Ids are : {}", allCancelledPaxIds);
		if (!allCancelledPaxIds.isEmpty()) {
			allCancelledPaxIds.removeAll(alreadyCancelledPaxIds);
			log.info("Cancelled Pax Ids are : {}", allCancelledPaxIds);
			railAdditionalInfo.setPaxKeys(allCancelledPaxIds);
		}

	}

	private Set<String> getPaxCancelledIds(RailOrderItem orderPreviousSnapshot) {
		Set<String> paxIds = new HashSet<String>();
		for (RailTravellerInfo traveller : orderPreviousSnapshot.getTravellerInfo()) {
			if (traveller.getCurrentSeatAllocation() != null
					&& StringUtils.isNotBlank(traveller.getCurrentSeatAllocation().getBookingStatus())
					&& traveller.getCurrentSeatAllocation().getBookingStatus().equals("CAN")) {
				paxIds.add(String.valueOf(traveller.getId()));
			}
		}
		return paxIds;
	}

	private void updateOrderItem(RailCancellationResponse cancellationResponse) {
		updateItemStatus(cancellationResponse.getJourneyInfo().getBookingRelatedInfo().getTravellerInfos());
		orderItem.setTravellerInfo(cancellationResponse.getJourneyInfo().getBookingRelatedInfo().getTravellerInfos());
		orderItem.getAdditionalInfo().setPriceInfo(cancellationResponse.getPriceInfo());
		orderItem.getAdditionalInfo()
				.setCancellationId(cancellationResponse.getJourneyInfo().getBookingRelatedInfo().getCancellationId());
	}

	private void updateItemStatus(List<RailTravellerInfo> travellerInfos) {
		if (CollectionUtils.isNotEmpty(travellerInfos)) {
			int totalTravWithSeat = getTravellersWithSeat(travellerInfos);
			int cancelledPaxCount = 0;
			for (RailTravellerInfo travInfo : travellerInfos) {
				if (travInfo.getCurrentSeatAllocation() != null
						&& StringUtils.isNotBlank(travInfo.getCurrentSeatAllocation().getBookingStatus())
						&& travInfo.getCurrentSeatAllocation().getBookingStatus().equals("CAN")) {
					cancelledPaxCount++;
				}
			}
			if (cancelledPaxCount == totalTravWithSeat) {
				originalOrder.setStatus(OrderStatus.CANCELLED);
				orderItem.setStatus(RailItemStatus.CANCELLED.getCode());
			}
		}

	}

	private int getTravellersWithSeat(List<RailTravellerInfo> travellerInfos) {
		int travCount = 0;
		for (RailTravellerInfo travInfo : travellerInfos) {
			if (travInfo.getCurrentSeatAllocation() != null || travInfo.getBookingSeatAllocation() != null) {
				travCount++;
			}
		}
		return travCount;
	}

	private void setAmendmentInfo(Amendment amendment, RailCancellationResponse cancellationResponse,
			DbRailOrderItem orderItem) {
		amendment.setAmount((-1)
				* cancellationResponse.getPriceInfo().getFareComponents().getOrDefault(RailFareComponent.AAR, 0.0));
		if (BooleanUtils.isTrue(cancellationResponse.getIsCancelSuccess())) {
			amendment.setStatus(AmendmentStatus.OTP_PENDING);
			log.debug("Cancellation is been raised with booking id {}",amendment.getBookingId());
			if (AmendmentType.CANCELLATION.equals(amendment.getAmendmentType())) {
				messagingClient.sendSMS(amendment, SmsTemplateKey.RAIL_RAISE_CANCELLATION_SMS);
			}
			if (AmendmentType.OFFLINE_CANCEL.equals(amendment.getAmendmentType())) {
				messagingClient.sendSMS(amendment, SmsTemplateKey.RAIL_RAISE_OFFLINE_CANCEL_SMS);
			}

		} else {
			throw new CustomGeneralException("Amendment Rejected");
		}
		amendment.setAssignedUserId(getUserIdFromConfigurator());
		amendment.setAssignedOn(LocalDateTime.now());
	}

	private String getUserIdFromConfigurator() {
		ClientGeneralInfo clientGeneralInfo = (ClientGeneralInfo) generalServiceCommunicator
				.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO).getOutput();
		return clientGeneralInfo.getCancellationAutomatedUserId();
	}

	private Amendment createAmendment(Order order) {
		User bookingUser = userService.getUserFromCache(originalOrder.getBookingUserId());
		log.info("Creating Amendment for bookingId {}", order.getBookingId());
		String amendmentType = request.getType().getCode();
		Amendment amendment = Amendment.builder()
				.amendmentId(ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.AMENDMENT).build()))
				.loggedInUserId(SystemContextHolder.getContextData().getUser().getUserId())
				.bookingId(order.getBookingId()).bookingUserId(order.getBookingUserId()).amount(0.0)
				.status(AmendmentStatus.REQUESTED).amendmentType(AmendmentType.getAmendmentType(amendmentType))
				.partnerId(bookingUser.getPartnerId()).channelType(SystemContextHolder.getChannelType())
				.orderType(OrderType.RAIL).build();
		setAdditionalInfo(amendment);
		return amendment;
	}

	private void setAdditionalInfo(Amendment amendment) {
		AmendmentAdditionalInfo additionalInfo = new AmendmentAdditionalInfo();
		additionalInfo.setAgentRemarks(request.getRemarks());
		additionalInfo.getRailAdditionalInfo().setPaxKeys(request.getPaxIds());
		additionalInfo.getRailAdditionalInfo().setPnr(orderItem.getAdditionalInfo().getPnr());
		if (request.getTdrInfo() != null) {
			additionalInfo.getRailAdditionalInfo().setTdrInfo(request.getTdrInfo());
		}
		amendment.setAdditionalInfo(additionalInfo);
	}

	@Override
	public void afterProcess() throws Exception {
	}

}
