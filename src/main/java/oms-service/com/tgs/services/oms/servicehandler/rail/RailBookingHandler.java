package com.tgs.services.oms.servicehandler.rail;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.DuplicateBookingAlert;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.rail.RailBookingManager;
import com.tgs.services.oms.restmodel.rail.RailBookingRequest;
import com.tgs.services.oms.restmodel.rail.RailBookingResponse;
import com.tgs.services.oms.utils.rail.RailBookingUtils;
import com.tgs.services.rail.restmodel.RailFareEnquiryResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailBookingHandler extends ServiceHandler<RailBookingRequest, RailBookingResponse> {

	@Autowired
	protected RailBookingManager bookingManager;

	@Autowired
	private PaymentServiceCommunicator paymentServiceComm;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	protected RailCommunicator railComm;

	@Autowired
	protected RailOrderItemService railOrderItemSrv;

	@Autowired
	protected GeneralCachingCommunicator cachingService;

	@Override
	public void beforeProcess() throws Exception {
		ServiceUtils.isProductEnabled(Product.RAIL);
	}

	@Override
	public void process() throws Exception {
		Order order = null;
		StringJoiner exceptions = new StringJoiner("-");

		try {
			RailFareEnquiryResponse fareEnquiryResponse = railComm.getRailFareEnquiryResponse(request.getBookingId());
			SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
			request.setType(OrderType.RAIL);
			response.setBookingId(request.getBookingId());
			User loggedInUser = SystemContextHolder.getContextData().getUser();
			if ("TQ".equals(fareEnquiryResponse.getJourneyInfo().getBookingQuota())) {
				String key = loggedInUser.getUserId().concat("_")
						.concat(fareEnquiryResponse.getJourneyInfo().getRailInfo().getNumber());
				checkDuplicateBooking(key, request.getBookingId(), true);
			}
			checkDuplicateBooking(loggedInUser.getUserId(), request.getBookingId(), false);
			removeExistingData();
			bookingManager.setBookingUser(loggedInUser);
			bookingManager.setLoggedInUser(loggedInUser);
			bookingManager.setBookingRequest(request);
			bookingManager.setJourneyInfo(fareEnquiryResponse.getJourneyInfo());
			bookingManager.setTravellerInfos(
					fareEnquiryResponse.getJourneyInfo().getBookingRelatedInfo().getTravellerInfos());
			if (CollectionUtils.isNotEmpty(request.getPaymentInfos())) {
				request.getPaymentInfos().forEach(p -> p.setProduct(Product.RAIL));
			}
			bookingManager.buildPaymentInfos();
			order = bookingManager.processBooking();
			order = orderManager.findByBookingId(order.getBookingId(), order);
		} catch (DataIntegrityViolationException e) {
			log.error("Unable to process order for booking Id {}", request.getBookingId(), e);
			exceptions.add(e.getMessage());
			throw new CustomGeneralException(SystemError.DUPLICATE_ORDER);
		} catch (CustomGeneralException e) {
			exceptions.add(e.getMessage());
			throw e;
		} catch (Exception e) {
			exceptions.add(e.getMessage());
			log.error("Unable to process order for booking Id {}", request.getBookingId(), e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		} finally {
			List<Alert> alerts = SystemContextHolder.getContextData().getAlerts();
			if (CollectionUtils.isNotEmpty(alerts)) {
				alerts.forEach(alert -> response.addAlert(alert));
			}
		}
	}

	/**
	 * Remove any existing data related to previous failed booking.
	 */
	private void removeExistingData() {
		orderManager.deleteExistingFailedAndNewBooking(request.getBookingId());
		railOrderItemSrv.deleteItemsWithNoOrderByBookingId(request.getBookingId());
		paymentServiceComm.deleteFailedPaymentsByRefId(request.getBookingId());
	}


	private void checkDuplicateBooking(String key, String bookingId, boolean isTatkal) {
		int ttl = isTatkal ? 15 * 60 : 10 * 60;
		Set<String> duplicateKeys = new HashSet<>();
		String existingDuplicateBookingId = null;
		try {
			log.info("New key and bookingId are {} {}", key, bookingId);
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.BOOKINGID.getName(), bookingId);
			cachingService.store(CacheMetaInfo.builder().createOnly(true).namespace(CacheNameSpace.RAIL.getName())
					.set(CacheSetName.RAIL_DUP.getName()).key(key).build(), binMap, false, true, ttl);
		} catch (CachingLayerException e) {
			log.info("Error is {} ", e.getMessage(), e);
			if (e.isCausedBy(CachingLayerError.DUPLICATE_KEY)) {
				String storedBookingId = RailBookingUtils.getStoredBookingId(key);
				log.info("Stored bookingid is {}", storedBookingId);
				boolean isDuplicateKey = true;
				Order storedOrder = orderManager.findByBookingId(storedBookingId, null);
				List<OrderStatus> failureStatusList = Arrays.asList(OrderStatus.FAILED, OrderStatus.ABORTED);
				if (storedOrder != null && failureStatusList.contains(storedOrder.getStatus())) {
					isDuplicateKey = false;
					RailBookingUtils.saveInCache(bookingId, key, ttl);
				}
				if (isDuplicateKey) {
					existingDuplicateBookingId = storedBookingId;
					duplicateKeys.add(key);
					log.info(
							"For tatkal. duplicate key {} encountered for bookingId {}. BookingId already stored with the key is {}",
							key, bookingId, storedBookingId);
				}
			} else {
				log.info("Deleting key from cache {} ", key);
				RailBookingUtils.deleteKeyFromCache(key);
				throw e;
			}
			if (CollectionUtils.isNotEmpty(duplicateKeys)) {
				// RailBookingUtils.deleteKeyFromCache(key);
				SystemContextHolder.getContextData().setAlreadyExistingBookingId(existingDuplicateBookingId);
				String errorMessage =
						String.join("", "Duplicate Booking for Agent. You have an existing booking under the cart ID ",
								existingDuplicateBookingId, ".Please, check your cart.");
				SystemContextHolder.getContextData()
						.addAlert(DuplicateBookingAlert.builder().type(AlertType.DUPLICATE_ORDER.name())
								.message(errorMessage).alreadyExistingBookingId(existingDuplicateBookingId).build());
				if (isTatkal)
					throw new CustomGeneralException(SystemError.TATKAL_DUPLICATE_ORDER,
							StringUtils.join(" Existing booking Id is ", existingDuplicateBookingId));
				else
					throw new CustomGeneralException(SystemError.ONE_SESSION_WITH_IRCTC,
							StringUtils.join(" Existing booking Id is ", existingDuplicateBookingId));
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
