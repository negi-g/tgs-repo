package com.tgs.services.oms.hibernate.hotel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.oms.datamodel.hotel.ProcessedRoomInfo;

public class ProcessedRoomInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		List<ProcessedRoomInfo> processedRoomInfos = new ArrayList<>();
		return processedRoomInfos.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<ProcessedRoomInfo>>() {
		}.getType();
	}
}
