package com.tgs.services.oms.jparepository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.utils.springframework.data.SpringDataUtils;

public enum OrderSearchPredicate {
	BOOKINGID {
		@Override
		public void addPredicate(Root<DbOrder> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				OrderFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotEmpty(filter.getBookingId())) {
				predicates.add(criteriaBuilder.equal(root.get("bookingId"), filter.getBookingId()));
			}
		}

	},
	BOOKINGIDS {
		@Override
		public void addPredicate(Root<DbOrder> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				OrderFilter filter, List<Predicate> predicates) {
			if (!CollectionUtils.isEmpty(filter.getBookingIds())) {
				Expression<String> exp = root.get("bookingId");
				predicates.add(exp.in(filter.getBookingIds()));
			}
		}

	},
	STATUSES {
		@Override
		public void addPredicate(Root<DbOrder> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				OrderFilter filter, List<Predicate> predicates) {
			if (!CollectionUtils.isEmpty(filter.getStatuses())) {
				Expression<String> exp = root.get("status");
				List<String> statusCodes = OrderStatus.getCodes(filter.getStatuses());
				if (!statusCodes.isEmpty())
					predicates.add(exp.in(statusCodes));
			}
		}
	},
	CHANNELS {
		@Override
		public void addPredicate(Root<DbOrder> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				OrderFilter filter, List<Predicate> predicates) {
			if (!CollectionUtils.isEmpty(filter.getChannels())) {
				Expression<String> exp = root.get("channelType");
				List<String> channelCodes = ChannelType.getCodes(filter.getChannels());
				if (!channelCodes.isEmpty())
					predicates.add(exp.in(channelCodes));
			}
		}
	},
	PRODUCTS {
		@Override
		public void addPredicate(Root<DbOrder> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				OrderFilter filter, List<Predicate> predicates) {
			if (CollectionUtils.isNotEmpty(filter.getProducts())) {
				Expression<String> exp = root.get("orderType");
				List<String> orderTypeCodes = OrderType.getCodes(filter.getProducts());
				if (!orderTypeCodes.isEmpty())
					predicates.add(exp.in(orderTypeCodes));
			}
		}
	},
	BOOKINGUSERID {
		@Override
		public void addPredicate(Root<DbOrder> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				OrderFilter filter, List<Predicate> predicates) {
			if (CollectionUtils.isNotEmpty(filter.getBookingUserIds())) {
				Expression<String> exp = root.get("bookingUserId");
				predicates.add(exp.in(filter.getBookingUserIds()));
			}
		}
	},
	CREATED_ON {
		@Override
		public void addPredicate(Root<DbOrder> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				OrderFilter filter, List<Predicate> predicates) {
			SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
		}
	},
	PROCESSED_ON {
		@Override
		public void addPredicate(Root<DbOrder> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				OrderFilter filter, List<Predicate> predicates) {

			if (filter.getProcessedOnBeforeDateTime() != null) {
				predicates.add(criteriaBuilder.lessThan(root.get("processedOn"), filter.getProcessedOnBeforeDateTime()));
			} else if (filter.getProcessedOnBeforeDate() != null) {
				predicates.add(criteriaBuilder.lessThan(root.get("processedOn"),
						LocalDateTime.of(filter.getProcessedOnBeforeDate(), LocalTime.MAX)));
			}

			if (filter.getProcessedOnAfterDateTime() != null) {
				predicates.add(criteriaBuilder.greaterThan(root.get("processedOn"), filter.getProcessedOnAfterDateTime()));
			} else if (filter.getProcessedOnAfterDate() != null) {
				predicates.add(criteriaBuilder.greaterThan(root.get("processedOn"),
						LocalDateTime.of(filter.getProcessedOnAfterDate(), LocalTime.MIN)));
			}
		}
	};

	public abstract void addPredicate(Root<DbOrder> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
			OrderFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnOrderFilter(Root<DbOrder> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, OrderFilter filter) {
		List<Predicate> predicates = new ArrayList<>();
		for (OrderSearchPredicate OrderCol : OrderSearchPredicate.values()) {
			OrderCol.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}
