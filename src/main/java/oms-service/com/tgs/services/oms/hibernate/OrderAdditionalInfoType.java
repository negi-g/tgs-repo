package com.tgs.services.oms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.oms.datamodel.OrderAdditionalInfo;

public class OrderAdditionalInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return OrderAdditionalInfo.class;
	}
}
