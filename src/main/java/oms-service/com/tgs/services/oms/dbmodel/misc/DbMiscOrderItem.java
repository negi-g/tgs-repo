package com.tgs.services.oms.dbmodel.misc;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.oms.datamodel.misc.AdditionalMiscOrderItemInfo;
import com.tgs.services.oms.datamodel.misc.MiscOrderItem;
import com.tgs.services.oms.datamodel.misc.MiscPriceInfo;
import com.tgs.services.oms.hibernate.misc.AdditionalMiscOrderItemInfoType;
import com.tgs.services.oms.hibernate.misc.MiscPriceInfoType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "MiscOrderItem")
@Table(name = "miscorderitem")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({@TypeDef(name = "MiscPriceInfoType", typeClass = MiscPriceInfoType.class),
		@TypeDef(name = "AdditionalMiscOrderItemInfoType", typeClass = AdditionalMiscOrderItemInfoType.class)})
public class DbMiscOrderItem extends BaseModel<DbMiscOrderItem, MiscOrderItem> {

	@Column
	private String bookingId;

	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private String status;

	@Column
	@Type(type = "AdditionalMiscOrderItemInfoType")
	private AdditionalMiscOrderItemInfo additionalInfo;

	@Column
	@Type(type = "MiscPriceInfoType")
	private MiscPriceInfo priceInfo;

	public MiscOrderItem toDomain() {
		return new GsonMapper<>(this, MiscOrderItem.class).convert();
	}

	public DbMiscOrderItem from(MiscOrderItem dataModel) {
		return new GsonMapper<>(dataModel, new DbMiscOrderItem(), DbMiscOrderItem.class).convert();
	}
}
