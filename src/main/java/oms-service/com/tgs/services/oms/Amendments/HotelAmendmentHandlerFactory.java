package com.tgs.services.oms.Amendments;

import com.tgs.services.base.SpringContext;
import com.tgs.services.oms.Amendments.Processors.HotelAmendmentProcessor;
import com.tgs.services.oms.Amendments.Processors.HotelCancellationProcessor;
import com.tgs.services.oms.Amendments.Processors.HotelCorrectionProcessor;
import com.tgs.services.oms.Amendments.Processors.HotelSupplierChangeProcessor;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.restmodel.hotel.HotelAmendOrderRequest;

public final class HotelAmendmentHandlerFactory {
	
	public static HotelAmendmentProcessor initHandler(Amendment amendment,HotelAmendOrderRequest hotelOrderRequest,String note) {
		
		HotelAmendmentProcessor amendmentHandler = null;
		switch(amendment.getAmendmentType()) {
			case CORRECTION:
				amendmentHandler = SpringContext.getApplicationContext().getBean(HotelCorrectionProcessor.class);
				break;
			case SUPPLIER_CHANGE:
				amendmentHandler = SpringContext.getApplicationContext().getBean(HotelSupplierChangeProcessor.class);
				break;
			case CANCELLATION:
				amendmentHandler = SpringContext.getApplicationContext().getBean(HotelCancellationProcessor.class);
				break;
			default:
				throw new RuntimeException("AmendmentType is Null or Not Supported.");
		}
		
		amendmentHandler.setHotelOrderRequest(hotelOrderRequest);
		amendmentHandler.setAmendment(amendment);
		amendmentHandler.setRequestNote(note);
		return amendmentHandler;
	}

}
