package com.tgs.services.oms.Amendments.Processors;

import java.util.List;
import java.util.Map;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.datamodel.booking.AdditionalRailOrderItemInfo;
import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public abstract class RailAmendmentProcessor implements AmendmentProcessor {

	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	protected AmendmentActionValidator actionValidator;

	protected Amendment amendment;

	protected DbRailOrderItem modifiedItem;

	protected DbRailOrderItem orgItem;

	public RailOrderItem patchedItem;

	protected boolean mergeAmendment;

	protected abstract void processAmendment();

	protected abstract void validate();

	public void process() {
		validate();
		processAmendment();
	}

	public RailOrderItem patchRailOrderItem(boolean mergeAmendment) {
		this.mergeAmendment = mergeAmendment;
		DbRailOrderItem dbRailOrderItem = patchRailOrderItem();
		patchedItem = dbRailOrderItem.toDomain();
		if (mergeAmendment)
			return patchedItem;
		return patchedItem;
	}

	private Double getDifferentialPayment(RailPriceInfo orgPriceInfo, RailPriceInfo modPriceInfo) {
		Double differentialPayment = 0d;
		if (MapUtils.isNotEmpty(modPriceInfo.getFareComponents())) {
			for (Map.Entry<RailFareComponent, Double> entry : modPriceInfo.getFareComponents().entrySet()) {
				RailFareComponent key = entry.getKey();
				double amendVal = entry.getValue();
				differentialPayment += amendVal - orgPriceInfo.getFareComponents().getOrDefault(key, 0d);
			}
		}
		return differentialPayment;
	}

	protected DbRailOrderItem patchRailOrderItem() {
		DbRailOrderItem patchedItem = orgItem;
		AdditionalRailOrderItemInfo orgAdditionalInfo = patchedItem.getAdditionalInfo();
		AdditionalRailOrderItemInfo modAdditionalInfo = modifiedItem.getAdditionalInfo();
		if (modAdditionalInfo != null && modAdditionalInfo.getPriceInfo() != null
				&& MapUtils.isNotEmpty(modAdditionalInfo.getPriceInfo().getFareComponents())) {
			amendment.setAmount(
					getDifferentialPayment(orgAdditionalInfo.getPriceInfo(), modAdditionalInfo.getPriceInfo()));
		}
		RailPriceInfo copyPriceInfo = null;
		if (modAdditionalInfo != null) {
			patchedItem.setAdditionalInfo(patchRailAdditionalInfo(modAdditionalInfo, orgAdditionalInfo));
			if (orgAdditionalInfo.getPriceInfo() != null
					&& MapUtils.isNotEmpty(orgAdditionalInfo.getPriceInfo().getFareComponents())) {
				copyPriceInfo = new GsonMapper<>(orgAdditionalInfo.getPriceInfo(), RailPriceInfo.class).convert();
			}
		}
		List<RailTravellerInfo> orgTravs = patchedItem.getTravellerInfo();
		String curStatus = patchedItem.getStatus();
		patchedItem = new GsonMapper<>(modifiedItem, patchedItem, DbRailOrderItem.class).convert();

		if (copyPriceInfo != null) {
			if (mergeAmendment)
				updateTotalFare(copyPriceInfo);
			patchedItem.getAdditionalInfo().setPriceInfo(copyPriceInfo);
		}

		// patchedItem.setTravellerInfo(patchTravellerInfoList(modifiedItem.getTravellerInfo(), orgTravs));
		patchedItem.setStatus(curStatus);
		return patchedItem;
	}

	private void updateTotalFare(RailPriceInfo priceInfo) {


		Map<RailFareComponent, Double> fareComponents = priceInfo.getFareComponents();
		if (MapUtils.isNotEmpty(fareComponents)) {
			{
				Double netFare = fareComponents.getOrDefault(RailFareComponent.BF, 0.0)
						+ fareComponents.getOrDefault(RailFareComponent.TI, 0.0)
						+ fareComponents.getOrDefault(RailFareComponent.TIT, 0.0)
						+ fareComponents.getOrDefault(RailFareComponent.WP, 0.0)
						+ fareComponents.getOrDefault(RailFareComponent.WPT, 0.0);

				Double totalFare = netFare + fareComponents.getOrDefault(RailFareComponent.MF, 0.0)
						+ fareComponents.getOrDefault(RailFareComponent.MFT, 0.0)
						+ fareComponents.getOrDefault(RailFareComponent.PC, 0.0)
						+ fareComponents.getOrDefault(RailFareComponent.PCT, 0.0);

				fareComponents.put(RailFareComponent.TF, totalFare);
				fareComponents.put(RailFareComponent.NF, netFare);

				// Amount for this amendment will be put in AAR , it can be Positive or negative both,
				fareComponents.put(RailFareComponent.AAR, amendment.getAmount());
			}
		}

	}

	private AdditionalRailOrderItemInfo patchRailAdditionalInfo(AdditionalRailOrderItemInfo modAdditionalInfo,
			AdditionalRailOrderItemInfo orgAdditionalInfo) {
		RailPriceInfo orgPriceInfo = orgAdditionalInfo.getPriceInfo();
		RailPriceInfo modPriceInfo = modAdditionalInfo.getPriceInfo();
		if (modPriceInfo != null && !MapUtils.isEmpty(modPriceInfo.getFareComponents())) {
			for (Map.Entry<RailFareComponent, Double> entry : modPriceInfo.getFareComponents().entrySet()) {
				RailFareComponent key = entry.getKey();
				Double amendVal = entry.getValue();
				orgPriceInfo.getFareComponents().put(key, amendVal);
			}
		}
		orgAdditionalInfo.setPriceInfo(orgPriceInfo);
		return orgAdditionalInfo;
	}


}
