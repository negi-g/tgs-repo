package com.tgs.services.oms.servicehandler.hotel;


import static com.tgs.services.base.helper.SystemError.ORDER_NOT_FOUND;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.oms.Amendments.HotelAmendmentManager;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAdditionalInfo;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.hotel.HotelAmendmentRequest;
import com.tgs.services.oms.restmodel.hotel.HotelCancellationResponse;
import com.tgs.services.oms.restmodel.hotel.RoomCancellationDetail;

@Service
public class RaiseHotelAmendmentHandler extends ServiceHandler<HotelAmendmentRequest, AmendmentResponse> {

	@Autowired
	protected HotelOrderItemManager hotelOrderItemManager;

	@Autowired
	private OrderService orderService;

	@Autowired
	protected AmendmentService amendmendService;

	@Autowired
	protected HotelAmendmentManager hotelAmendmentManager;

	@Autowired
	private HotelOrderMessageHandler messageHandler;

	@Autowired
	protected HMSCommunicator hmsComm;

	@Autowired
	protected GeneralCachingCommunicator cachingService;

	@Autowired
	HotelBookingCancellationHandler hotelBookingCancellationHandler;

	protected Order originalOrder;

	protected List<DbHotelOrderItem> orderItemList;

	protected Amendment amendment;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {

		originalOrder = orderService.findByBookingId(request.getBookingId()).toDomain();
		if (originalOrder == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		UserServiceHelper.checkAndReturnAllowedUserId(SystemContextHolder.getContextData().getEmulateOrLoggedInUserId(),
				Arrays.asList(originalOrder.getBookingUserId()));
		orderItemList = hotelOrderItemManager.getItemList(request.getBookingId());
		if (CollectionUtils.isEmpty(orderItemList))
			throw new CustomGeneralException(ORDER_NOT_FOUND);

		boolean isAmendmentTypeValidForOrderItem =
				hotelAmendmentManager.checkIfAmendmentTypeValid(request, orderItemList);
		if (!isAmendmentTypeValidForOrderItem)
			throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);

		Set<String> roomKeys = request.getRoomKeys() != null ? request.getRoomKeys() : new HashSet<>();

		hotelAmendmentManager.checkAmendStatus(request.getBookingId(), roomKeys);

		amendment = createAmendment(null);

		if (!request.getType().equals(AmendmentType.CANCELLATION)) {
			amendment = amendmendService.save(amendment);
			if (amendment.getAmendmentType().sendRaiseMail()) {
				messageHandler.sendAmendmentEmail(originalOrder, orderItemList, EmailTemplateKey.HOTEL_AMENDMENT_EMAIL,
						null, amendment);
			}
		} else {
			storeCancellationRequestInCache(amendment);
		}
		response.getAmendmentItems().add(amendment);

	}

	@Override
	public void afterProcess() throws Exception {}


	protected Amendment createAmendment(String amendmentId) {

		String amendmentType = request.getType().getCode();
		String amedRefId = StringUtils.isBlank(amendmentId)
				? ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.AMENDMENT).build())
				: amendmentId;

		Amendment amendment = Amendment.builder().amendmentId(amedRefId)
				.amendmentId(ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.AMENDMENT).build()))
				.loggedInUserId(SystemContextHolder.getContextData().getUser().getUserId())
				.bookingId(originalOrder.getBookingId()).bookingUserId(originalOrder.getBookingUserId())
				.status(AmendmentStatus.REQUESTED).amendmentType(AmendmentType.getAmendmentType(amendmentType))
				.orderType(OrderType.HOTEL).build();
		setAdditionalInfo(amendment);
		return amendment;
	}

	private void setAdditionalInfo(Amendment amendment) {

		AmendmentAdditionalInfo additionalInfo = new AmendmentAdditionalInfo();
		additionalInfo.setAgentRemarks(request.getRemarks());
		additionalInfo.getHotelAdditionalInfo().setRoomKeys(request.getRoomKeys());
		amendment.setAdditionalInfo(additionalInfo);
		if (amendment.getAmendmentType().equals(AmendmentType.CANCELLATION)) {
			setAutoCancellation(amendment);
		}

	}

	private void setAutoCancellation(Amendment amendment) {
		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(orderItemList).build().convert();
		Set<String> roomKeys = amendment.getAdditionalInfo().getHotelAdditionalInfo().getRoomKeys() != null
				? amendment.getAdditionalInfo().getHotelAdditionalInfo().getRoomKeys()
				: new HashSet<>();
		updateRoomsToCancel(hInfo, roomKeys);
		int cancelledRoomCount = orderItemList.stream().filter(orderItem -> orderItem.getStatus().equals("C"))
				.collect(Collectors.toList()).size();
		int cancellationPendingCount = orderItemList.stream().filter(orderItem -> orderItem.getStatus().equals("CP"))
				.collect(Collectors.toList()).size();
		int nonCancelledRoomCount = orderItemList.size() - cancelledRoomCount;

		boolean isPartialCancellation = nonCancelledRoomCount != roomKeys.size() && roomKeys.size() != 0;

		if (!isPartialCancellation || (hmsComm.isPartialCancellationAllowed(hInfo, request.getBookingId())
				&& hotelAmendmentManager.isAllRoomsWithSamePrice(orderItemList)) && cancellationPendingCount == 0) {
			amendment.getAdditionalInfo().getHotelAdditionalInfo().setAutoCancellationAllowed(true);
			setCancellationDetailResponse(amendment);
		}
		amendment.getAdditionalInfo().getHotelAdditionalInfo().setIsPartialCancellation(isPartialCancellation);
	}
	
	private void updateRoomsToCancel (HotelInfo hInfo, Set<String> roomKeys) {
		if (!CollectionUtils.isEmpty(roomKeys)) {
			hInfo.getOptions().get(0).getRoomInfos().forEach(roomInfo -> {
				if (roomKeys.contains(roomInfo.getId())) {
					if(roomInfo.getMiscInfo() == null) {
						roomInfo.setMiscInfo(RoomMiscInfo.builder().isRoomToBeCancelled(true).build());
					} else {
						roomInfo.getMiscInfo().setIsRoomToBeCancelled(true);
					}
				}
			});
		}
	}

	private void storeCancellationRequestInCache(Amendment amendment) {

		request.setAmendmentId(amendment.getAmendmentId());
		if (request.getType().equals(AmendmentType.CANCELLATION)) {
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.CANCELLATION.getName(), GsonUtils.getGson().toJson(request));
			cachingService.store(CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONREQUEST.name())
					.key(amendment.getAmendmentId()).build(), binMap, false, true, 3600);
		}
	}

	private void setCancellationDetailResponse(Amendment amendment) {
		Set<String> roomKeys = amendment.getAdditionalInfo().getHotelAdditionalInfo().getRoomKeys() != null
				? amendment.getAdditionalInfo().getHotelAdditionalInfo().getRoomKeys()
				: new HashSet<>();
		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(orderItemList).build().convert();
		Double refundAmount = hotelBookingCancellationHandler.getPartialRefundAmount(hInfo, originalOrder, roomKeys);
		Double totalCharges = hotelBookingCancellationHandler.getPartialRefundCharges(hInfo, roomKeys);
		
		// Added this condition as said. We do not allow extra charge for Cancellation Amendment.
		// Should remove the condition once CNP price is matched with BF for all suppliers
		if(refundAmount < 0d) {
			totalCharges += refundAmount;
			refundAmount = 0d;
		}
		hotelBookingCancellationHandler.updateRefundAmount(hInfo, amendment);
		amendment.getAdditionalInfo().setOrderDiffAmount(refundAmount * -1);
		amendment.setAmendmentAmount();

		List<RoomCancellationDetail> roomCancellationDetail = new ArrayList<RoomCancellationDetail>();

		// Room-wise price details
		hInfo.getOptions().get(0).getRoomInfos().forEach(roomInfo -> {
			if (CollectionUtils.isEmpty(roomKeys) || roomKeys.contains(roomInfo.getId())) {
				RoomCancellationDetail detail =
						RoomCancellationDetail.builder().id(roomInfo.getId()).roomCategory(roomInfo.getRoomCategory())
								.roomType(roomInfo.getRoomType()).standardRoomName(roomInfo.getStandardRoomName())
								.totalFareComponents(roomInfo.getTotalFareComponents())
								.totalAddlFareComponents(roomInfo.getTotalAddlFareComponents())
								.perNightPriceInfos(roomInfo.getPerNightPriceInfos()).build();
				roomCancellationDetail.add(detail);
			}
		});

		HotelCancellationResponse hotelCancellationResponse = HotelCancellationResponse.builder()
				.totalAmountToRefund(refundAmount).totalCancellationFare(totalCharges)
				.autoCancellationAllowed(
						amendment.getAdditionalInfo().getHotelAdditionalInfo().getAutoCancellationAllowed())
				.roomCancellationDetail(roomCancellationDetail).build();

		response.setHotelCancellationResponse(hotelCancellationResponse);

	}

}
