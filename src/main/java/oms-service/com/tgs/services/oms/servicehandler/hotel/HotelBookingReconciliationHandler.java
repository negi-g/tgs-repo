package com.tgs.services.oms.servicehandler.hotel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelBookingReconciliation;
import com.tgs.services.oms.datamodel.hotel.HotelBookingReconciliationAdditionalInfo;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.oms.datamodel.hotel.HotelReconciliationResult;
import com.tgs.services.oms.dbmodel.hotel.DbHotelBookingReconciliation;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.hotel.HotelBookingReconciliationService;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.mapper.HotelInfoToHotelReconciliationInfo;
import com.tgs.services.oms.restmodel.hotel.HotelBookingReconciliationRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingReconciliationResponse;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelBookingReconciliationHandler extends ServiceHandler<HotelBookingReconciliationRequest, BaseResponse> {

	@Autowired
	private OrderService orderService;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private HotelOrderItemService itemService;

	@Autowired
	private HMSCommunicator hmsCommunicator;

	@Autowired
	private UserServiceCommunicator userComm;

	@Autowired
	private HotelBookingReconciliationService reconciliationService;

	@Override
	public void beforeProcess() throws Exception {
		log.info("Starting booking reconcilation flow for request {}", new Gson().toJson(request));
	}

	@Override
	public void process() throws Exception {

		Order order = orderService.findByBookingId(request.getBookingId()).toDomain();
		if (Objects.isNull(order)) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}

		isReconciliationAllowedForOrderStatus(order.getStatus(), request.getBookingId());
		List<DbHotelOrderItem> dbOrderItems = itemService.findByBookingIdOrderByIdAsc(request.getBookingId());
		try {
			if (isReconiliationAvailableForSupplier(dbOrderItems.get(0))) {
				if (StringUtils.isBlank(dbOrderItems.get(0).getAdditionalInfo().getSupplierBookingReference())) {
					log.info(
							"Unable to start booking reconcilation flow for booking id {} as supplier reference is missing.",
							request.getBookingId());
					throw new CustomGeneralException(SystemError.SUPPLIER_REFERENCE_MISSING);
				}
				HotelBookingReconciliation newReconciliationInfo =
						getUpdatedBookingInfoToReconcile(order, dbOrderItems);
				saveOrUpdateReconciledInfo(newReconciliationInfo, order);
			} else {
				log.info(
						"Unable to start booking reconcilation flow for booking id {} as supplier do not allow reconcilation.",
						request.getBookingId());
			}
		} finally {
			log.info("Finished booking reconcilation flow for booking id {} with order status {}",
					request.getBookingId(), order.getStatus());
		}
	}

	private boolean isReconiliationAvailableForSupplier(DbHotelOrderItem orderItem) {
		HotelGeneralPurposeOutput configuratorInfo = (HotelGeneralPurposeOutput) hmsCommunicator
				.getHotelConfigRule(HotelConfiguratorRuleType.GNPURPOSE).getOutput();
		List<String> supplierBookingDetailNotPresent = configuratorInfo.getSupplierBookingDetailNotPresent();
		if (CollectionUtils.isNotEmpty(supplierBookingDetailNotPresent)) {
			if (supplierBookingDetailNotPresent.contains(orderItem.getSupplierId())) {
				log.info("Booking reconcilation not available for supplier {} for booking id {}",
						orderItem.getSupplierId(), orderItem.getBookingId());
				return false;
			}
		}
		return true;
	}

	private HotelBookingReconciliation getUpdatedBookingInfoToReconcile(Order order,
			List<DbHotelOrderItem> dbOrderItems) {
		try {
			log.info(
					"Started supplier booking reconcilation flow for booking id {} and supplier id {} with order status {}",
					order.getBookingId(), dbOrderItems.get(0).getSupplierId(), order.getStatus());
			HotelImportBookingParams importBookingParams = HotelImportBookingParams.builder()
					.supplierBookingId(dbOrderItems.get(0).getAdditionalInfo().getSupplierBookingReference())
					.supplierId(dbOrderItems.get(0).getSupplierId()).bookingId(order.getBookingId())
					.flowType(HotelFlowType.RECONCILATION).deliveryInfo(order.getDeliveryInfo())
					.marketingFees(getMarketingFees(dbOrderItems)).build();
			User bookingUser = userComm.getUserFromCache(order.getBookingUserId());
			HotelImportedBookingInfo bookingInfo = hmsCommunicator.retrieveBooking(importBookingParams, bookingUser);
			return HotelInfoToHotelReconciliationInfo.builder().hotelInfo(bookingInfo.getHInfo())
					.deliveryInfo(bookingInfo.getDeliveryInfo()).bookingStatus(bookingInfo.getOrderStatus())
					.bookingDate(Objects.isNull(bookingInfo.getBookingDate()) ? order.getCreatedOn().toLocalDate()
							: bookingInfo.getBookingDate())
					.bookingId(order.getBookingId()).userId(SystemContextHolder.getContextData().getUser().getUserId())
					.build().convert();
		} catch (Exception e) {
			log.error("Unable to convert supplier info to reconciled info for booking id {}", order.getBookingId(), e);
			throw new CustomGeneralException(SystemError.SUPPLIER_INFO_PROCESS_ERROR);
		} finally {
			log.info(
					"Finished supplier booking reconcilation flow for booking id {} and supplier id {} with order status {}",
					order.getBookingId(), dbOrderItems.get(0).getSupplierId(), order.getStatus());
		}
	}

	private HotelBookingReconciliationResponse createReconResponse(Order order, List<DbHotelOrderItem> dbOrderItems) {
		HotelBookingReconciliationResponse response = new HotelBookingReconciliationResponse();

		HotelBookingReconciliation oldReconciliationInfo = createHotelReconciliationInfoFromDB(order, dbOrderItems);
		DbHotelBookingReconciliation dbNewReconciliationInfo =
				reconciliationService.findByBookingId(order.getBookingId());
		if (Objects.nonNull(dbNewReconciliationInfo)) {
			HotelBookingReconciliation newReconciliationInfo = dbNewReconciliationInfo.toDomain();
			response.setHotelInfos(reconcileHotelInfo(oldReconciliationInfo, newReconciliationInfo));
			response.setRoomInfos(
					reconcileRoomsInfo(oldReconciliationInfo.getRoomInfos(), newReconciliationInfo.getRoomInfos()));
			response.setVerifyingReason(dbNewReconciliationInfo.getAdditionalInfo().getVerifyingReason());

		} else {
			HotelBookingReconciliation newReconciliationInfo = getUpdatedBookingInfoToReconcile(order, dbOrderItems);
			saveOrUpdateReconciledInfo(newReconciliationInfo, order);
			response.setHotelInfos(reconcileHotelInfo(oldReconciliationInfo, newReconciliationInfo));
			response.setRoomInfos(
					reconcileRoomsInfo(oldReconciliationInfo.getRoomInfos(), newReconciliationInfo.getRoomInfos()));
		}
		DbHotelOrderItem orderItem = dbOrderItems.get(0);
		AdditionalHotelOrderItemInfo hotelInfo = orderItem.getAdditionalInfo();
		response.setBookingId(orderItem.getBookingId());
		response.setBookingRefNo(hotelInfo.getSupplierBookingReference());
		response.setSupplier(orderItem.getSupplierId());
		response.setHotelName(hotelInfo.getHInfo().getName());
		response.setDescription(hotelInfo.getHInfo().getName());
		return response;
	}

	private HotelBookingReconciliationResponse createDummyReconResponse(Order order,
			List<DbHotelOrderItem> dbOrderItems) {
		HotelBookingReconciliationResponse response = new HotelBookingReconciliationResponse();

		HotelBookingReconciliation oldReconciliationInfo = createHotelReconciliationInfoFromDB(order, dbOrderItems);
		response.setHotelInfos(getDummyHotelInfo(oldReconciliationInfo));
		List<RoomInfo> roomInfos = new ArrayList<>();

		response.setRoomInfos(reconcileDummyRoomsInfoById(oldReconciliationInfo.getRoomInfos(), roomInfos));

		DbHotelOrderItem orderItem = dbOrderItems.get(0);
		AdditionalHotelOrderItemInfo hotelInfo = orderItem.getAdditionalInfo();
		response.setBookingId(orderItem.getBookingId());
		response.setBookingRefNo(hotelInfo.getSupplierBookingReference());
		response.setSupplier(orderItem.getSupplierId());
		response.setHotelName("Unknown");
		response.setDescription("Unknown");
		response.setIsSupplierInfoAvailable(false);

		return response;
	}

	public List<HotelReconciliationResult> getDummyHotelInfo(HotelBookingReconciliation oldReconciliationInfo) {
		HotelBookingReconciliationAdditionalInfo oldReconciliationAddlInfo = oldReconciliationInfo.getAdditionalInfo();


		List<HotelReconciliationResult> hotelInfos = new ArrayList<>();
		hotelInfos.add(HotelReconciliationResult.builder().fieldName("bookingStatus")
				.oldValue(oldReconciliationInfo.getBookingStatus()).newValue("Unknown").build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("bookingDate")
				.oldValue(oldReconciliationInfo.getBookingDate()).newValue("Unknown").build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("hotelName")
				.oldValue(oldReconciliationInfo.getHotelName()).newValue("Unknown").build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("city")
				.oldValue(oldReconciliationAddlInfo.getAddressInfo().getCityInfo().getName()).newValue("Unknown")
				.build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("country")
				.oldValue(oldReconciliationAddlInfo.getAddressInfo().getCityInfo().getCountry()).newValue("Unnknown")
				.build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("checkIn")
				.oldValue(oldReconciliationInfo.getCheckInDate()).newValue("Unknown").build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("checkOut")
				.oldValue(oldReconciliationInfo.getCheckOutDate()).newValue("Unknown").build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("currency")
				.oldValue(oldReconciliationAddlInfo.getCurrency()).newValue("Unknown").build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("cancellationDeadline")
				.oldValue(oldReconciliationInfo.getCancellationDeadline()).newValue("Unknown").build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("pinCode")
				.oldValue(oldReconciliationAddlInfo.getAddressInfo().getPincode()).newValue("Unknown").build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("totalPrice")
				.oldValue(oldReconciliationInfo.getAmount()).newValue("Unknown").build());

		return hotelInfos;
	}

	private HotelBookingReconciliation createHotelReconciliationInfoFromDB(Order order,
			List<DbHotelOrderItem> dbOrderItems) {

		HotelInfo oldHotelInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(dbOrderItems).build().convert();
		return HotelInfoToHotelReconciliationInfo.builder().hotelInfo(oldHotelInfo)
				.deliveryInfo(order.getDeliveryInfo()).bookingStatus(order.getStatus().name())
				.bookingDate(order.getCreatedOn().toLocalDate()).build().convert();
	}

	public HotelBookingReconciliationResponse getReconciliationInfo(
			HotelBookingReconciliationRequest reconciliationRequest) {
		HotelBookingReconciliationResponse response = new HotelBookingReconciliationResponse();
		Order order = orderService.findByBookingId(reconciliationRequest.getBookingId()).toDomain();
		if (Objects.isNull(order)) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}
		isReconciliationAllowedForOrderStatus(order.getStatus(), reconciliationRequest.getBookingId());
		List<DbHotelOrderItem> dbOrderItems =
				itemService.findByBookingIdOrderByIdAsc(reconciliationRequest.getBookingId());
		if (isReconiliationAvailableForSupplier(dbOrderItems.get(0))) {
			response = createReconResponse(order, dbOrderItems);
		} else {
			response = createDummyReconResponse(order, dbOrderItems);
		}
		return response;
	}

	private List<HotelReconciliationResult> reconcileHotelInfo(HotelBookingReconciliation oldReconciliationInfo,
			HotelBookingReconciliation newReconciliationInfo) {

		List<HotelReconciliationResult> hotelInfos = new ArrayList<>();
		HotelBookingReconciliationAdditionalInfo oldReconciliationAddlInfo = oldReconciliationInfo.getAdditionalInfo();
		HotelBookingReconciliationAdditionalInfo newReconciliationAddlInfo = newReconciliationInfo.getAdditionalInfo();

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("bookingStatus")
				.oldValue(oldReconciliationInfo.getBookingStatus())
				.newValue(ObjectUtils.firstNonNull(newReconciliationInfo.getBookingStatus(), "null")).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("bookingDate")
				.oldValue(oldReconciliationInfo.getBookingDate())
				.newValue(ObjectUtils.firstNonNull(newReconciliationInfo.getBookingDate(), "Unknown")).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("hotelName")
				.oldValue(oldReconciliationInfo.getHotelName())
				.newValue(ObjectUtils.firstNonNull(newReconciliationInfo.getHotelName(), "Unknown")).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("city")
				.oldValue(oldReconciliationAddlInfo.getAddressInfo().getCityInfo().getName()).newValue(ObjectUtils
						.firstNonNull(newReconciliationAddlInfo.getAddressInfo().getCityInfo().getName(), "Unknown"))
				.build());

		hotelInfos
				.add(HotelReconciliationResult.builder().fieldName("country")
						.oldValue(oldReconciliationAddlInfo.getAddressInfo().getCityInfo().getCountry())
						.newValue(ObjectUtils.firstNonNull(
								newReconciliationAddlInfo.getAddressInfo().getCityInfo().getCountry(), "Unnknown"))
						.build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("checkIn")
				.oldValue(oldReconciliationInfo.getCheckInDate())
				.newValue(ObjectUtils.firstNonNull(newReconciliationInfo.getCheckInDate(), "Unknown")).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("checkOut")
				.oldValue(oldReconciliationInfo.getCheckOutDate())
				.newValue(ObjectUtils.firstNonNull(newReconciliationInfo.getCheckOutDate(), "Unknown")).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("currency")
				.oldValue(oldReconciliationAddlInfo.getCurrency())
				.newValue(ObjectUtils.firstNonNull(newReconciliationAddlInfo.getCurrency(), "Unknown")).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("cancellationDeadline")
				.oldValue(oldReconciliationInfo.getCancellationDeadline())
				.newValue(ObjectUtils.firstNonNull(newReconciliationInfo.getCancellationDeadline(), "Unknown"))
				.build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("pinCode")
				.oldValue(oldReconciliationAddlInfo.getAddressInfo().getPincode())
				.newValue(ObjectUtils.firstNonNull(newReconciliationAddlInfo.getAddressInfo().getPincode(), "Unknown"))
				.build());

		hotelInfos.add(
				HotelReconciliationResult.builder().fieldName("totalPrice").oldValue(oldReconciliationInfo.getAmount())
						.newValue(ObjectUtils.firstNonNull(newReconciliationInfo.getAmount(), "Unknown")).build());

		return hotelInfos;
	}

	private List<List<HotelReconciliationResult>> reconcileRoomsInfo(List<RoomInfo> oldRoomInfos,
			List<RoomInfo> newRoomInfos) {

		if (isIdPresent(newRoomInfos.get(0))) {
			return reconcileRoomsInfoById(oldRoomInfos, newRoomInfos);
		} else {
			return reconcileRoomsInfoByType(oldRoomInfos, newRoomInfos);
		}
	}

	private List<List<HotelReconciliationResult>> reconcileRoomsInfoById(List<RoomInfo> oldRoomInfos,
			List<RoomInfo> newRoomInfos) {

		Map<String, RoomInfo> oldRoomInfoMap =
				oldRoomInfos.stream().collect(Collectors.toMap(RoomInfo::getId, roomInfo -> roomInfo));

		Map<String, RoomInfo> newRoomInfoMap =
				newRoomInfos.stream().collect(Collectors.toMap(RoomInfo::getId, roomInfo -> roomInfo));

		List<List<HotelReconciliationResult>> roomReconciliationInfos = new ArrayList<>();
		Set<String> traversedIds = new HashSet<>();
		oldRoomInfoMap.forEach((id, oldRoomInfo) -> {
			RoomInfo newRoomInfo = newRoomInfoMap.get(id);
			List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
			perRoomReconciliationInfos
					.add(HotelReconciliationResult.builder().fieldName("mealBasis").oldValue(oldRoomInfo.getMealBasis())
							.newValue(Objects.isNull(newRoomInfo) ? null : newRoomInfo.getMealBasis()).build());

			perRoomReconciliationInfos
					.add(HotelReconciliationResult.builder().fieldName("roomType").oldValue(oldRoomInfo.getRoomType())
							.newValue(Objects.isNull(newRoomInfo) ? null : oldRoomInfo.getRoomType()).build());

			roomReconciliationInfos.add(perRoomReconciliationInfos);

			traversedIds.add(id);
		});

		newRoomInfoMap.forEach((id, newRoomInfo) -> {
			if (!traversedIds.contains(id)) {
				List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
				perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("mealBasis")
						.newValue(newRoomInfo.getMealBasis()).build());

				perRoomReconciliationInfos
						.add(HotelReconciliationResult.builder().fieldName("roomType").newValue("Unknown").build());
				roomReconciliationInfos.add(perRoomReconciliationInfos);
			}
		});

		return roomReconciliationInfos;
	}

	private List<List<HotelReconciliationResult>> reconcileDummyRoomsInfoById(List<RoomInfo> oldRoomInfos,
			List<RoomInfo> newRoomInfos) {

		Map<String, RoomInfo> oldRoomInfoMap =
				oldRoomInfos.stream().collect(Collectors.toMap(RoomInfo::getId, roomInfo -> roomInfo));

		List<List<HotelReconciliationResult>> roomReconciliationInfos = new ArrayList<>();
		Set<String> traversedIds = new HashSet<>();
		oldRoomInfoMap.forEach((id, oldRoomInfo) -> {
			List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
			perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("mealBasis")
					.oldValue(oldRoomInfo.getMealBasis()).newValue("Unknown").build());

			perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("roomType")
					.oldValue(oldRoomInfo.getRoomType()).newValue("Unknown").build());

			roomReconciliationInfos.add(perRoomReconciliationInfos);

			traversedIds.add(id);
		});
		return roomReconciliationInfos;
	}

	private List<List<HotelReconciliationResult>> reconcileRoomsInfoByType(List<RoomInfo> oldRoomInfos,
			List<RoomInfo> newRoomInfos) {

		Map<String, List<RoomInfo>> oldRoomInfoMap =
				oldRoomInfos.stream().collect(Collectors.groupingBy(roomInfo -> roomInfo.getRoomType()));

		Map<String, List<RoomInfo>> newRoomInfoMap =
				newRoomInfos.stream().collect(Collectors.groupingBy(roomInfo -> roomInfo.getRoomType()));

		List<List<HotelReconciliationResult>> roomReconciliationInfos = new ArrayList<>();
		Set<RoomInfo> traversedIds = new HashSet<>();
		oldRoomInfoMap.forEach((roomType, groupedOldRoomInfos) -> {
			List<RoomInfo> groupedNewRoomInfos = newRoomInfoMap.get(roomType);
			if (CollectionUtils.isNotEmpty(groupedNewRoomInfos)) {
				for (RoomInfo oldRoomInfo : groupedOldRoomInfos) {

					for (Iterator<RoomInfo> groupedNewRoomInfoIterator =
							groupedNewRoomInfos.iterator(); groupedNewRoomInfoIterator.hasNext();) {

						RoomInfo newRoomInfo = groupedNewRoomInfoIterator.next();
						List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
						perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("mealBasis")
								.oldValue(oldRoomInfo.getMealBasis())
								.newValue(ObjectUtils.firstNonNull(newRoomInfo.getMealBasis(), "Unknown")).build());

						perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("roomType")
								.oldValue(oldRoomInfo.getRoomType()).newValue(newRoomInfo.getRoomType()).build());
						roomReconciliationInfos.add(perRoomReconciliationInfos);
						groupedNewRoomInfoIterator.remove();
						traversedIds.add(newRoomInfo);
						break;
					}
				}
			} else {
				for (RoomInfo oldRoomInfo : groupedOldRoomInfos) {
					List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
					perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("mealBasis")
							.oldValue(oldRoomInfo.getMealBasis()).newValue("Unknown").build());

					perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("roomType")
							.oldValue(oldRoomInfo.getRoomType()).newValue("Unknown").build());
					roomReconciliationInfos.add(perRoomReconciliationInfos);
				}
			}

		});

		newRoomInfoMap.forEach((roomType, groupedNewRoomInfos) -> {

			for (RoomInfo newRoomInfo : groupedNewRoomInfos) {
				if (!traversedIds.contains(newRoomInfo)) {
					List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
					perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("mealBasis")
							.newValue(ObjectUtils.firstNonNull(newRoomInfo.getMealBasis(), "Unknown"))
							.oldValue("Unknown").build());

					perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("roomType")
							.oldValue("Unknown").newValue(newRoomInfo.getRoomType()).build());
					roomReconciliationInfos.add(perRoomReconciliationInfos);
				}
			}
		});

		return roomReconciliationInfos;
	}

	private void saveOrUpdateReconciledInfo(HotelBookingReconciliation newReconciliationInfo, Order order) {
		try {
			log.info("Started to save hotel reconciliation info for booking Id {} having order status {}",
					newReconciliationInfo.getBookingId(), order.getStatus());
			DbHotelBookingReconciliation oldDBReconciliationInfo =
					reconciliationService.findByBookingId(newReconciliationInfo.getBookingId());
			if (Objects.isNull(oldDBReconciliationInfo)) {
				oldDBReconciliationInfo = new DbHotelBookingReconciliation().from(newReconciliationInfo);
				order.getAdditionalInfo().setIsReconciledWithSupplier(true);
				orderManager.saveWithoutFetch(order);
			} else {
				DbHotelBookingReconciliation newDBReconciliationInfo =
						new DbHotelBookingReconciliation().from(newReconciliationInfo);
				oldDBReconciliationInfo = new GsonMapper<>(newDBReconciliationInfo, oldDBReconciliationInfo,
						DbHotelBookingReconciliation.class).convert();
			}
			reconciliationService.save(oldDBReconciliationInfo);
		} catch (Exception e) {
			log.error("Unable to save hotel reconciliation info {} ", GsonUtils.getGson().toJson(newReconciliationInfo),
					e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
		log.info("Succesfully saved hotel reconciliation info for booking Id {} having order status {}",
				newReconciliationInfo.getBookingId(), order.getStatus());
	}

	public void updateReconciledBooking(HotelBookingReconciliationRequest reconciliationRequest) {
		Order order = orderService.findByBookingId(reconciliationRequest.getBookingId()).toDomain();
		if (Objects.isNull(order)) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}
		List<DbHotelOrderItem> dbOrderItems =
				itemService.findByBookingIdOrderByIdAsc(reconciliationRequest.getBookingId());
		if (isReconiliationAvailableForSupplier(dbOrderItems.get(0))) {

			DbHotelBookingReconciliation oldDBReconciliationInfo =
					reconciliationService.findByBookingId(reconciliationRequest.getBookingId());
			if (!Objects.isNull(oldDBReconciliationInfo)) {
				oldDBReconciliationInfo.getAdditionalInfo()
						.setVerifyingReason(reconciliationRequest.getVerifyingReason());
			}
			reconciliationService.save(oldDBReconciliationInfo);
		}
		order.getAdditionalInfo().setIsReconciliationVerified(reconciliationRequest.getIsVerified());
		if (reconciliationRequest.getReconciliationStatus() != null) {
			order.getAdditionalInfo().setReconciliationStatus(reconciliationRequest.getReconciliationStatus());

		}
		orderManager.saveWithoutProcessedOn(order);
	}

	private void isReconciliationAllowedForOrderStatus(OrderStatus orderStatus, String bookingId) {

		switch (orderStatus) {
			case SUCCESS:
				break;
			case CANCELLED:
				break;
			case UNCONFIRMED:
				break;
			default: {
				log.info("Booking reconcilation not allowed for booking id {} having status {}", bookingId,
						orderStatus);
				throw new CustomGeneralException(SystemError.RECONCILIATION_NOT_ALLOWED);
			}
		}
	}

	private double getMarketingFees(List<DbHotelOrderItem> orderItems) {

		DbHotelOrderItem firstOrderItem = orderItems.get(0);
		return firstOrderItem.getRoomInfo().getPerNightPriceInfos().get(0).getFareComponents()
				.getOrDefault(HotelFareComponent.TMF, 0.0d);
	}

	private boolean isIdPresent(RoomInfo roomInfo) {

		return StringUtils.isNotBlank(roomInfo.getId());
	}

	@Override
	public void afterProcess() throws Exception {

	}
}
