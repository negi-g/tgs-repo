package com.tgs.services.oms.Amendments.Processors;

import java.util.List;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.mapper.AirOrderItemToSegmentInfoMapper;

@Service
public class MiscAmendmentProcessor extends AirAmendmentProcessor {


	@Override
	public void processAmendment() {}

	@Override
	public void PrePopulate() {
		if (amendment.getStatus().equals(AmendmentStatus.ASSIGNED)) {
			List<TripInfo> trips = airOrderCommunicator.findTripByBookingId(patchedItems.get(0).getBookingId());
			patchedItems.forEach(airOrderItem -> {
				AirOrderItemToSegmentInfoMapper mapper = AirOrderItemToSegmentInfoMapper.builder()
						.fmsCommunicator(fmsCommunicator).item(new DbAirOrderItem().from(airOrderItem)).build();
				SegmentInfo segmentInfo = mapper.convert();
				trips.forEach(trip -> {
					trip.getSegmentInfos().forEach(segmentInf -> {
						if (segmentInf.getId().equals(segmentInfo.getId())
								&& segmentInf.getSegmentNum().intValue() == 0) {
							resetAmendmentMarkup(segmentInfo);
							fmsCommunicator.updateClientAmendmentMarkup(AmendmentType.MISCELLANEOUS, segmentInfo,
									bookingUser);
							setUpdatedFDForTravellers(segmentInfo, airOrderItem);
						}
					});
				});
			});
		}
	}
}
