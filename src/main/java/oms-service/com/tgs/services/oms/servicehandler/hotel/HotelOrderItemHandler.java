package com.tgs.services.oms.servicehandler.hotel;

import java.util.Arrays;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;

import com.tgs.services.ums.datamodel.User;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelOrderItemHandler extends ServiceHandler<List<HotelOrderItem>, BaseResponse> {

	@Autowired
	HotelOrderItemManager itemManager;

	@Autowired
	private OrderManager orderManager;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() throws Exception {

		User user = SystemContextHolder.getContextData().getUser();
		request.forEach(hotelOrderItem -> { // Enhanced for loop ?
			if (hotelOrderItem.getId() != null) {
				DbHotelOrderItem dbHotelOrderItem = itemManager.findByOrderItemId(hotelOrderItem.getId());
				log.info("Updating order and item details for bookingId {}", dbHotelOrderItem.getBookingId());
				Order order = orderManager.findByBookingId(dbHotelOrderItem.getBookingId(), null);
				updateSupplierId(dbHotelOrderItem, hotelOrderItem);
				itemManager.save(Arrays.asList(dbHotelOrderItem), order, null);
				log.info("Updation for order and item details completed for bookingId {}",
						dbHotelOrderItem.getBookingId());
			}
		});
	}

	private void updateSupplierId(DbHotelOrderItem dbHotelOrderItem, HotelOrderItem hotelOrderItem) {
		if (StringUtils.isNotBlank(hotelOrderItem.getSupplierId())) {
			dbHotelOrderItem.setSupplierId(hotelOrderItem.getSupplierId());
		}

	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
