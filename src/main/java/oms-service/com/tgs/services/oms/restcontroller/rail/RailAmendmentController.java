package com.tgs.services.oms.restcontroller.rail;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.oms.Amendments.RailAmendmentManager;
import com.tgs.services.oms.Amendments.RailAmendmentOtpManager;
import com.tgs.services.oms.Amendments.RailTdrFilingManager;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.datamodel.amendments.UpdateAmendmentRequest;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.rail.ProcessRailAmendmentRequest;
import com.tgs.services.oms.restmodel.rail.RailAmdBulkCancelRequest;
import com.tgs.services.oms.restmodel.rail.RailAmendmentDetailResponse;
import com.tgs.services.oms.restmodel.rail.RailAmendmentRequest;
import com.tgs.services.oms.restmodel.rail.RailInvoiceResponse;
import com.tgs.services.oms.restmodel.rail.RailOtpVerificationRequest;
import com.tgs.services.oms.restmodel.rail.RailOtpVerificationResponse;
import com.tgs.services.oms.servicehandler.rail.ProcessRailAmendmentHandler;
import com.tgs.services.oms.servicehandler.rail.RailAmendmentInvoiceHandler;
import com.tgs.services.oms.servicehandler.rail.RailBulkCancelManager;
import com.tgs.services.oms.servicehandler.rail.RaiseRailAmendmentHandler;
import com.tgs.services.oms.servicehandler.rail.UpdateRailAmendmentHandler;

@RequestMapping("/oms/v1/rail/amendment")
@RestController
public class RailAmendmentController {

	@Autowired
	protected RaiseRailAmendmentHandler raiseRailAmendmentHandler;

	@Autowired
	protected UpdateRailAmendmentHandler updateRailAmendmentHandler;

	@Autowired
	protected ProcessRailAmendmentHandler processRailAmendmentHandler;

	@Autowired
	protected RailAmendmentManager railAmendmentManager;

	@Autowired
	protected RailAmendmentOtpManager otpManager;

	@Autowired
	protected RailTdrFilingManager tdrFilingManager;

	@Autowired
	protected RailAmendmentInvoiceHandler amendmentInvoiceHandler;

	@Autowired
	protected RailBulkCancelManager bulkCancelManager;

	@RequestMapping(value = "/raise", method = RequestMethod.POST)
	protected AmendmentResponse raise(@RequestBody @Valid RailAmendmentRequest request) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
		raiseRailAmendmentHandler.initData(request,
				AmendmentResponse.builder().amendmentItems(new ArrayList<>()).build());
		return raiseRailAmendmentHandler.getResponse();
	}

	@RequestMapping(value = "/process", method = RequestMethod.POST)
	protected AmendmentResponse process(HttpServletRequest request,
			@RequestBody @Valid ProcessRailAmendmentRequest amendment) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(amendment.getAmendmentId());
		processRailAmendmentHandler.initData(amendment, AmendmentResponse.builder().build());
		AmendmentResponse response = processRailAmendmentHandler.getResponse();
		if (response != null && CollectionUtils.isNotEmpty(response.getAmendmentItems()))
			SystemContextHolder.getContextData().getReqIds().add(response.getAmendmentItems().get(0).getBookingId());
		return response;

	}

	@RequestMapping(value = "/update/{action}", method = RequestMethod.POST)
	protected AmendmentResponse update(HttpServletRequest request, @PathVariable @NotNull AmendmentAction action,
			@RequestBody @Valid UpdateAmendmentRequest amendmentRequest) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(amendmentRequest.getAmendmentId());
		updateRailAmendmentHandler.setAction(action);
		AmendmentResponse response = AmendmentResponse.builder()
				.amendmentItems(
						Stream.of(updateRailAmendmentHandler.process(amendmentRequest)).collect(Collectors.toList()))
				.build();
		if (response != null && CollectionUtils.isNotEmpty(response.getAmendmentItems()))
			SystemContextHolder.getContextData().getReqIds().add(response.getAmendmentItems().get(0).getBookingId());
		return response;
	}

	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	@RequestMapping(value = "/detail/{amendmentId}", method = RequestMethod.GET)
	protected RailAmendmentDetailResponse getDetail(HttpServletRequest request,
			@PathVariable @NotNull String amendmentId) {
		return railAmendmentManager.getAmendmentDetail(amendmentId);
	}

	@RequestMapping(value = "/otp-submit", method = RequestMethod.POST)
	protected RailOtpVerificationResponse submitCancelOtp(HttpServletRequest request,
			@RequestBody @Valid RailOtpVerificationRequest railOtpRequest) {
		return otpManager.submitOtp(railOtpRequest);
	}

	@RequestMapping(value = "/otp-resend", method = RequestMethod.POST)
	protected RailOtpVerificationResponse resendOtp(HttpServletRequest request,
			@RequestBody @Valid RailOtpVerificationRequest railOtpRequest) {
		return otpManager.resendOtp(railOtpRequest);
	}

	@RequestMapping(value = "/invoice/{amendmentId}", method = RequestMethod.GET)
	protected RailInvoiceResponse invoice(@PathVariable @NotNull String amendmentId) throws Exception {
		amendmentInvoiceHandler.initData(amendmentId, new RailInvoiceResponse());
		return amendmentInvoiceHandler.getResponse();
	}

	@RequestMapping(value = "/tdr/reason/list", method = RequestMethod.POST)
	protected RailAmendmentDetailResponse getTdrReasonsList(HttpServletRequest request,
			@RequestBody RailAmendmentRequest railAmendmentRequest) {
		return tdrFilingManager.getTdrReasons(railAmendmentRequest);
	}

	@RequestMapping(value = "/bulk-cancel", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse bulkkCancel(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody RailAmdBulkCancelRequest railBulkAmendmentUploadRequest) throws Exception {
		return bulkCancelManager.raiseBulkAmendments(railBulkAmendmentUploadRequest);
	}


}

