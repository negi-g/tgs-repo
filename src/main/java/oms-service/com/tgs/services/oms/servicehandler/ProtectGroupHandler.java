package com.tgs.services.oms.servicehandler;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.LogData;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.ProtectGroupConfiguration;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.ProtectGroupData;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.common.SimpleCache;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProtectGroupHandler extends ServiceHandler<OrderFilter, BaseResponse> {

	final private static List<OrderStatus> STATUSES = Arrays.asList(OrderStatus.SUCCESS);

	@Autowired
	private OrderService orderService;

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;

	private static ProtectGroupConfiguration protectGroupConfiguration;


	@Override
	public void beforeProcess() throws Exception {
		final LocalDateTime now = LocalDateTime.now();
		if (request.getProcessedOnAfterDateTime() == null && request.getProcessedOnAfterDate() == null) {
			request.setProcessedOnAfterDateTime(now.minusHours(2));
		}
		if (request.getProcessedOnBeforeDateTime() == null && request.getProcessedOnBeforeDate() == null) {
			request.setProcessedOnBeforeDateTime(now);
		}
		request.setStatuses(STATUSES);
	}

	@Override
	public void process() throws Exception {
		Map<String, Object> params = new HashMap<>();
		List<Object[]> orderObjectsList = airOrderItemService.findByJsonSearch(request);
		Map<DbOrder, List<DbAirOrderItem>> orderItemsGroup = groupOrderItems(orderObjectsList);
		log.info("Found {} number of orders to push to protect group", orderItemsGroup.size());
		Iterator<Entry<DbOrder, List<DbAirOrderItem>>> orderEntryIterator = orderItemsGroup.entrySet().iterator();

		while (orderEntryIterator.hasNext()) {
			Entry<DbOrder, List<DbAirOrderItem>> orderEntry = orderEntryIterator.next();
			DbOrder order = orderEntry.getKey();
			List<DbAirOrderItem> items = orderEntry.getValue();
			double totalAmount = 0;
			for (DbAirOrderItem item : items) {
				for (FlightTravellerInfo travellerInfo : item.getTravellerInfo()) {
					totalAmount +=
							travellerInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.TF, 0.0);
				}
			}

			if (BooleanUtils.isNotTrue(order.getAdditionalInfo().getIsPushedToProtectGroup())) {
				boolean status = true;
				try {
					ProtectGroupData protectGroupData = new ProtectGroupData();
					boolean isProtectGroupOpted =
							BooleanUtils.toBoolean(order.getAdditionalInfo().getIsCancallationProtectionOpted());

					protectGroupData.setReferenceId(order.getBookingId());
					protectGroupData.setSold(isProtectGroupOpted);

					protectGroupData
							.setFirstName(StringUtils.isBlank(items.get(0).getTravellerInfo().get(0).getFirstName())
									? items.get(0).getTravellerInfo().get(0).getLastName()
									: items.get(0).getTravellerInfo().get(0).getFirstName());
					protectGroupData
							.setLastName(StringUtils.isBlank(items.get(0).getTravellerInfo().get(0).getLastName())
									? items.get(0).getTravellerInfo().get(0).getFirstName()
									: items.get(0).getTravellerInfo().get(0).getLastName());
					protectGroupData.setOrderAmount(totalAmount);
					boolean isReissue = false;

					if (OrderFlowType.REISSUED.equals(order.getAdditionalInfo().getFlowType())) {
						// only change is insurance end date - which is max of curr departure date and new departure
						// date
						log.info("Pushing reissue order with booking id {} to protect group", order.getBookingId());
						isReissue = true;
						protectGroupData.setOrderAmount(0);
						String oldbookingId = items.get(0).getTravellerInfo().get(0).getOldBookingId();
						isProtectGroupOpted = BooleanUtils.toBoolean(orderService.findByBookingId(oldbookingId)
								.getAdditionalInfo().getIsCancallationProtectionOpted());
						protectGroupData.setSold(isProtectGroupOpted);
						protectGroupData.setReferenceId(oldbookingId);
					}

					if (!isProtectGroupOpted) {
						setDummyData(protectGroupData);
					}

					DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

					if (order.getAdditionalInfo().getFirstUpdateTime() != null) {
						LocalDateTime bookingTime = order.getAdditionalInfo().getFirstUpdateTime().get("S");
						protectGroupData
								.setVendorSaleDate(bookingTime.atZone(ZoneId.systemDefault()).format(dateFormatter));
					} else {
						protectGroupData.setVendorSaleDate(
								order.getCreatedOn().atZone(ZoneId.systemDefault()).format(dateFormatter));
					}
					LocalDateTime departureDate = items.get(items.size() - 1).getDepartureTime();
					protectGroupData
							.setInsuranceEndDate(departureDate.atZone(ZoneId.systemDefault()).format(dateFormatter));


					log.info("Pushing data for bookingid {} to refund protect", order.getBookingId());
					ClientGeneralInfo clientInfo = gmsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
					protectGroupConfiguration = clientInfo.getProtectGroupConfiguration();
					params.put(order.getBookingId(), callProtectGroupApi(protectGroupData, isReissue));
				} catch (Exception e) {
					log.error("protect group posting failed for booking id {} due to {}", order.getBookingId(), e);
				} finally {
					status = BooleanUtils.isTrue((Boolean) params.get(order.getBookingId()));
					if (status) {
						log.info("Order with booking id {} successfully posted", order.getBookingId());
						order.getAdditionalInfo().setIsPushedToProtectGroup(status);
						orderService.saveWithoutProcessedOn(order);
					} else {
						log.info("Order with booking id {} not posted", order.getBookingId());
					}
				}
			}
		}
		log.info("Completed {} number of orders to push to protect group", orderItemsGroup.size());
		response.setMetaInfo(params);
	}

	@Override
	public void afterProcess() throws Exception {

	}

	private void setDummyData(ProtectGroupData protectGroupData) {
		protectGroupData.setFirstName("DUMMY");
		protectGroupData.setLastName("DUMMY");
		protectGroupData.setOrderAmount(1);
	}

	private Map<String, String> setHeaders(String postData, ProtectGroupConfiguration conf)
			throws UnsupportedEncodingException {
		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("Content-Type", "application/json");
		headerParams.put("X-RefundProtect-VendorId", conf.getVendorId());
		headerParams.put("X-RefundProtect-AuthToken", conf.getApiKey());
		return headerParams;
	}

	private String protectGroupRequestData(ProtectGroupData protectGroupData, ProtectGroupConfiguration conf) {
		Map<String, Object> params = new HashMap<>();
		Map<String, Object> productData = new HashMap<>();
		productData.put("productCode", conf.getProductCode());
		productData.put("currencyCode", conf.getCurrencyCode());
		productData.put("productPrice", protectGroupData.getOrderAmount());
		productData.put("premiumRate", conf.getPremiumRate());
		productData.put("sold", protectGroupData.getSold());
		productData.put("OfferingMethod", "OPT-IN");
		productData.put("insuranceEndDate", protectGroupData.getInsuranceEndDate());
		params.put("vendorCode", conf.getVendorId());
		params.put("vendorSalesReferenceId", protectGroupData.getReferenceId());
		params.put("vendorSalesDate", protectGroupData.getVendorSaleDate());
		params.put("customerFirstName", protectGroupData.getFirstName());
		params.put("customerLastName", protectGroupData.getLastName());
		params.put("products", Lists.newArrayList(productData));
		return GsonUtils.getGson().toJson(params);
	}

	private boolean callProtectGroupApi(ProtectGroupData protectGroupData, boolean isReissue) throws Exception {
		boolean success = false;
		String responseString = "";
		String postData = "";
		HttpUtils httpUtils = null;
		try {
			postData = protectGroupRequestData(protectGroupData, protectGroupConfiguration);
			Map<String, String> headerParams = setHeaders(postData, protectGroupConfiguration);
			String url = protectGroupConfiguration.getUrl();
			httpUtils = HttpUtils.builder().urlString(url.concat("salesoffering")).headerParams(headerParams)
					.postData(postData).requestMethod("POST").build();
			if (isReissue)
				httpUtils.setRequestMethod("PATCH");

			responseString = (String) httpUtils.getResponse(null).orElse(new String());
			log.info("Refund Protectgroup URL {}, postdata {}, response {},protectGroupConfiguration {}", url, postData,
					responseString, protectGroupConfiguration);
			/**
			 * Ok means it has successfully posted , Duplicate Vendor sales also means that bookingid has been
			 * successfully posted earlier
			 */
			if (!(responseString.contains("Ok") || responseString.contains("Duplicate Vendor Sales"))) {
				throw new Exception(responseString);
			}
			success = true;
		} catch (Exception exp) {
			if (protectGroupData.getSold()) {
				log.error("Sold#true, Unable to post data to protect group configuration {}, postData {}",
						protectGroupConfiguration, postData, exp);
			} else {
				log.info("Unable to post data to protect group configuration {}, postData {}",
						protectGroupConfiguration, postData, exp);
			}


		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "RQ"),
					formatRQRS(httpUtils.getResponseString(), "RS"));
			LogUtils.addToLogList(LogData.builder().type("ProtectGroup").ttl((long) 7 * 24 * 60 * 60)
					.key(protectGroupData.getReferenceId()).logData(endPointRQRS).logType("AirSupplierAPILogs")
					.build());
			LogUtils.clearLogList();

		}
		return success;
	}

	public String formatRQRS(String message, String type) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(message)) {
			buffer.append(type + " : " + LocalDateTime.now().toString()).append("\n\n").append(message).append("\n\n");
		}
		return buffer.toString();
	}

	private Map<DbOrder, List<DbAirOrderItem>> groupOrderItems(List<Object[]> orderObjectsList) {
		Map<DbOrder, List<DbAirOrderItem>> orderItemsGroup = new HashMap<>();
		List<DbAirOrderItem> orderItems = null;
		DbOrder previousOrder = null;
		SimpleCache<String, DbOrder> orderCache = new SimpleCache<String, DbOrder>() {
			@Override
			protected String getKey(DbOrder value) {
				return value.getBookingId();
			}
		};
		for (Object[] orderObjects : orderObjectsList) {
			DbAirOrderItem orderItem = (DbAirOrderItem) orderObjects[0];
			DbOrder order = (DbOrder) orderObjects[1];
			order = orderCache.getCached(order);
			if (!order.equals(previousOrder)) {
				if (previousOrder != null) {
					orderItemsGroup.put(previousOrder, orderItems);
				}
				orderItems = new ArrayList<>();
			}
			orderItems.add(orderItem);
			previousOrder = order;
		}
		if (previousOrder != null) {
			orderItemsGroup.put(previousOrder, orderItems);
		}
		return orderItemsGroup;
	}
}
