package com.tgs.services.oms.Amendments;

import com.tgs.services.base.SpringContext;
import com.tgs.services.oms.Amendments.Processors.RailAmendmentProcessor;
import com.tgs.services.oms.Amendments.Processors.RailCorrectionProcessor;
import com.tgs.services.oms.Amendments.Processors.RailMiscellaneousProcessor;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.ums.datamodel.User;

public class RailAmendmentHandlerFactory {

	public static RailAmendmentProcessor initHandler(Amendment amendment, DbRailOrderItem modItem,
			DbRailOrderItem orgItem, User bookingUser) {

		RailAmendmentProcessor amendmentHandler = null;
		switch (amendment.getAmendmentType()) {
			case CORRECTION:
				amendmentHandler = SpringContext.getApplicationContext().getBean(RailCorrectionProcessor.class);
				break;
			case MISCELLANEOUS:
				amendmentHandler = SpringContext.getApplicationContext().getBean(RailMiscellaneousProcessor.class);
				break;
			case CANCELLATION:
				amendmentHandler = SpringContext.getApplicationContext().getBean(RailMiscellaneousProcessor.class);
				break;
			case TDR:
				amendmentHandler = SpringContext.getApplicationContext().getBean(RailCorrectionProcessor.class);
				break;
			case OFFLINE_CANCEL:
				amendmentHandler = SpringContext.getApplicationContext().getBean(RailMiscellaneousProcessor.class);
				break;
			default:
				throw new RuntimeException("AmendmentType is Null or Not Supported.");
		}
		amendmentHandler.setModifiedItem(modItem);
		amendmentHandler.setOrgItem(orgItem);
		amendmentHandler.setAmendment(amendment);
		return amendmentHandler;
	}
}
