package com.tgs.services.oms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tgs.services.oms.dbmodel.DbOrderBilling;

import java.util.List;

public interface GstInfoRepository extends JpaRepository<DbOrderBilling, Long>, JpaSpecificationExecutor<DbOrderBilling> {

	DbOrderBilling findByBookingId(String bookingId);

	List<DbOrderBilling> findByBookingIdIn(List<String> bookingIds);
}
