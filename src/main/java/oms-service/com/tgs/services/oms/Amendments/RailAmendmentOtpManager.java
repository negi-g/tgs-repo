package com.tgs.services.oms.Amendments;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.amendments.UpdateAmendmentRequest;
import com.tgs.services.oms.datamodel.amendments.RailAdditionalInfo;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.restmodel.rail.RailOtpVerificationRequest;
import com.tgs.services.oms.restmodel.rail.RailOtpVerificationResponse;
import com.tgs.services.oms.servicehandler.rail.UpdateRailAmendmentHandler;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Lazy
public class RailAmendmentOtpManager {

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	private RailCommunicator railCommunicator;

	public RailOtpVerificationResponse submitOtp(RailOtpVerificationRequest railOtpRequest) {
		RailOtpVerificationResponse response = new RailOtpVerificationResponse();

		Amendment amendment = amendmentService.findByAmendmentId(railOtpRequest.getAmendmentId());
		RailAdditionalInfo railAdditionalInfo = amendment.getRailAdditionalInfo();
		log.info("Amendment found for amendmentId {}", railOtpRequest.getAmendmentId());
		if (amendment == null)
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);

		try {

			railOtpRequest.setBookingId(amendment.getBookingId());
			if (amendment.getStatus().equals(AmendmentStatus.OTP_PENDING)) {
				
				boolean isSuccess = false;
				
				if (BooleanUtils.isTrue(railOtpRequest.getIsWithoutOtp())) {
					isSuccess = true;
					railAdditionalInfo.setIsWithoutOtp(true);
					
				} else {
					isSuccess = railCommunicator.submitOtpRequest(railOtpRequest);
				}
				response.setIsOtpVerified(isSuccess);
				amendmentService.save(amendment);
				
				if (BooleanUtils.isTrue(isSuccess)) {
					UpdateAmendmentRequest updateAmendRequest = new UpdateAmendmentRequest();
					updateAmendRequest.setAmendmentId(amendment.getAmendmentId());
					updateAmendRequest.setStatus(amendment.getStatus());
					// updateAmendRequest.setFinishingNotes("done by agent");
					try {
						log.info("Merging amendment for amendmentId {}", railOtpRequest.getAmendmentId());
						UpdateRailAmendmentHandler updateRailHandler =
								SpringContext.getApplicationContext().getBean(UpdateRailAmendmentHandler.class);
						updateRailHandler.setAction(AmendmentAction.MERGE);
						amendment = updateRailHandler.process(updateAmendRequest);
					} catch (Exception e) {
						log.error("Erorr Occured during updataion of amendment {} due to {}",
								amendment.getAmendmentId(), e.getMessage(), e);
						throw e;
					}
				}
			} else {
				throw new CustomGeneralException(SystemError.AMENDMENT_NA_SUPPORTED);
			}
			amendmentService.save(amendment);
		} catch (Exception e) {
			if (e instanceof CustomGeneralException) {
				log.info("Invalid otpcode for {} due to {}", amendment.getAmendmentId(), e.getMessage());
			} else {
				log.error("Error Occured while Submitting OTP for amendmentId {} due to {}", amendment.getAmendmentId(),
						e.getMessage(), e);
			}
			response.addError(new ErrorDetail(SystemError.SUBMIT_OTP.getErrorCode(),
					SystemError.SUBMIT_OTP.getMessage(e.getMessage())));
		}
		return response;
	}

	public RailOtpVerificationResponse resendOtp(RailOtpVerificationRequest railOtpRequest) {
		RailOtpVerificationResponse otpResponse = null;
		try {
			otpResponse = new RailOtpVerificationResponse();
			boolean isOtpVerified = railCommunicator.resendOtp(railOtpRequest);
			otpResponse.setIsOtpVerified(isOtpVerified);
		} catch (Exception e) {
			log.error("Erorr Occured during resending OTP due to {}", e.getMessage(), e);
			otpResponse.addError(new ErrorDetail(SystemError.RESEND_OTP.getErrorCode(),
					SystemError.RESEND_OTP.getMessage(e.getMessage())));
		}
		return otpResponse;
	}

}
