package com.tgs.services.oms.servicehandler.rail;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.rail.RailItemStatus;
import com.tgs.services.oms.datamodel.rail.RailOrderItemFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.rail.RailOrderItemManager;
import com.tgs.services.oms.restmodel.rail.RailOrderProcessingRequest;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.restmodel.RailCurrentStatus;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailOrderProcessingEngine {

	@Autowired
	protected OrderManager orderManager;

	@Autowired
	protected RailOrderItemService itemService;

	@Autowired
	protected RailOrderItemManager itemManager;

	@Autowired
	protected RailCommunicator railComm;

	@Autowired
	protected RailAbortHandler abortHandler;

	@Autowired
	protected GeneralServiceCommunicator gmsCommunicator;


	public void processOrder(RailOrderProcessingRequest request) {
		final LocalDateTime now = LocalDateTime.now();

		if (request.getCreatedOnBefore() == null && request.getCreatedOnAfter() == null) {
			request.setCreatedOnBefore(now.minusMinutes(15));
			request.setCreatedOnAfter(now.minusMinutes(90));
		}

		if (OrderStatus.PAYMENT_SUCCESS.equals(request.getOrderStatus())) {
			request.setCreatedOnBefore(now.minusMinutes(10));
			OrderFilter orderFilter = generateOrderFilter(OrderStatus.PAYMENT_SUCCESS, request);
			processPaymentSuccessBooking(orderFilter);

		} else {
			OrderFilter orderFilter = generateOrderFilter(OrderStatus.PENDING, request);
			processPendingOrder(orderFilter);
		}


	}


	public void processPaymentSuccessBooking(OrderFilter orderFilter) {

		List<Object[]> orderList = itemManager.findInfoByOrderFilter(orderFilter);
		if (CollectionUtils.isEmpty(orderList)) {
			return;
		}
		for (Object[] object : orderList) {
			DbOrder dbOrder = (DbOrder) object[1];
			Order order = dbOrder.toDomain();
			DbRailOrderItem orderItem = (DbRailOrderItem) object[0];
			log.info("Changing Order Status from {} to {} for bookingId {}", order.getStatus(), OrderStatus.PENDING,
					order.getBookingId());
			orderItem.setStatus(RailItemStatus.PENDING.getCode());
			order.setStatus(OrderStatus.PENDING);
			itemManager.saveOrderAndItem(orderItem, order);
		}
	}


	public void processPendingOrder(OrderFilter orderFilter) {
		List<Object[]> orderList = itemManager.findInfoByOrderFilter(orderFilter);
		if (CollectionUtils.isEmpty(orderList)) {
			return;
		}
		for (Object[] object : orderList) {
			DbOrder dbOrder = (DbOrder) object[1];
			Order order = dbOrder.toDomain();
			DbRailOrderItem orderItem = (DbRailOrderItem) object[0];
			log.info("Changing Order Status from {} to {} for bookingId {}", order.getStatus(), OrderStatus.PENDING,
					order.getBookingId());
			RailCurrentStatus railCurrentStatus = railComm.getLiveBookingDetails(order.getBookingId());
			updateOrderAndOrderItem(railCurrentStatus, order, orderItem);
		}
	}


	private void updateOrderAndOrderItem(RailCurrentStatus railCurrentStatus, Order order, DbRailOrderItem orderItem) {
		try {
			orderItem.getAdditionalInfo()
					.setRetryCount(ObjectUtils.firstNonNull(orderItem.getAdditionalInfo().getRetryCount(), 0) + 1);

			if (railCurrentStatus.getJourneyInfo() != null && orderItem.getAdditionalInfo().getRetryCount() < 4) {
				RailJourneyInfo journeyInfo = railCurrentStatus.getJourneyInfo();
				orderItem.getAdditionalInfo().setReservationId(journeyInfo.getBookingRelatedInfo().getReservationId());
				orderItem.getAdditionalInfo().setBoardingStation(journeyInfo.getBoardingStationInfo().getCode());
				orderItem.getAdditionalInfo()
						.setReservationUptoStation(journeyInfo.getReservationUptoStationInfo().getCode());
				orderItem.getAdditionalInfo().setAvblForVikalp(journeyInfo.getAvlblForVikalp());
				orderItem.getAdditionalInfo().getRailInfo().setTrainOwner(journeyInfo.getRailInfo().getTrainOwner());
				orderItem.getAdditionalInfo().setPnr(journeyInfo.getBookingRelatedInfo().getPnr());
				orderItem.setTravellerInfo(journeyInfo.getBookingRelatedInfo().getTravellerInfos());
				boolean isBookingConfirmed = getBookingStatus(journeyInfo, order.getBookingId());
				if (isBookingConfirmed) {
					order.setStatus(OrderStatus.SUCCESS);
					orderItem.setStatus(RailItemStatus.SUCCESS.getCode());
					itemManager.save(orderItem, order);
				} else {
					itemManager.saveOrderAndItem(orderItem, order);
				}
			} else if (orderItem.getAdditionalInfo().getRetryCount() > 3) {
				itemManager.saveOrderAndItem(orderItem, order);
				abortHandler.abortBooking(order.getBookingId());
				/**
				 * This is the case when User was not proceeding further after Payment Success, Below message is added
				 * for better clarification to Mid-Office Roles
				 */
				Note note = Note.builder().bookingId(order.getBookingId()).noteType(NoteType.ABORT_REASON)
						.noteMessage("Request Timeout").build();
				gmsCommunicator.addNote(note);
			} else {
				itemManager.saveOrderAndItem(orderItem, order);
			}
		} catch (Exception e) {
			log.error("Error Occured while updating OrderItem for bookingId {}, due to {}", order.getBookingId(), e);
		}
	}


	private boolean getBookingStatus(RailJourneyInfo journeyInfo, String bookingId) {
		boolean isSuccess = false;
		if (StringUtils.isNotEmpty(journeyInfo.getBookingRelatedInfo().getPnr())) {
			isSuccess = true;
		}
		return isSuccess;
	}

	public OrderFilter generateOrderFilter(OrderStatus orderStatus, RailOrderProcessingRequest request) {
		OrderFilter orderFilter = OrderFilter.builder().statuses(Arrays.asList(orderStatus))
				.createdOnAfterDateTime(request.getCreatedOnAfter())
				.createdOnBeforeDateTime(request.getCreatedOnBefore()).products(Arrays.asList(OrderType.RAIL))
				.railItemFilter(RailOrderItemFilter.builder().build()).build();
		return orderFilter;
	}


}
