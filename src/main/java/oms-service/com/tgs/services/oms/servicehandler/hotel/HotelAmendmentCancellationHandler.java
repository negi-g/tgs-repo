package com.tgs.services.oms.servicehandler.hotel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.oms.Amendments.AmendmentHelper;
import com.tgs.services.oms.Amendments.HotelAmendmentHandlerFactory;
import com.tgs.services.oms.Amendments.Processors.HotelAmendmentProcessor;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.amendments.UpdateAmendmentRequest;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.hotel.HotelAmendOrderRequest;
import com.tgs.services.oms.restmodel.hotel.HotelAmendmentRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelAmendmentCancellationHandler extends RaiseHotelAmendmentHandler {

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private HotelOrderItemService itemService;

	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	private UpdateHotelAmendmentHandler updateHotelAmendmentHandler;

	@Autowired
	protected HotelOrderMessageHandler messageHandler;

	@Override
	public void beforeProcess() throws Exception {
		if (isCancellationRequestNotInCache()) {
			List<ErrorDetail> errorMessage = new ArrayList<>();
			errorMessage.add(ErrorDetail.builder().message("Raise Amendment Before Confirming").build());
			response.setErrors(errorMessage);
			return;
		}

		originalOrder = orderManager.findByBookingId(request.getBookingId(), null);
		if (originalOrder == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);

	}

	@Override
	public void process() throws Exception {

		orderItemList = itemService.findByBookingIdOrderByIdAsc(request.getBookingId());
		log.info("Started creating ammendment for booking id {}", request.getBookingId());
		amendment = createAmendment(request.getAmendmentId());
		log.info("Successfully created ammendment with id {} for booking id {}", amendment.getId(),
				request.getBookingId());
		if (BooleanUtils
				.isNotTrue(amendment.getAdditionalInfo().getHotelAdditionalInfo().getAutoCancellationAllowed())) {
			amendmendService.save(amendment);
			return;
		}
		amendment.setAssignedUserId(getUserIdFromConfigurator());
		amendment.setStatus(AmendmentStatus.ASSIGNED);
		amendment.setAssignedOn(LocalDateTime.now());
		boolean isBookingCancelled = cancelBooking();
		if (isBookingCancelled) {
			mergeAmendment();
		}
	}

	private boolean cancelBooking() {

		String bookingId = request.getBookingId();
		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(orderItemList).build().convert();
		boolean isBookingCancelled = false;
		if (!CollectionUtils.isEmpty(request.getRoomKeys())) {
			hInfo.getOptions().get(0).getRoomInfos().forEach(roomInfo -> {
				if (request.getRoomKeys().contains(roomInfo.getId())) {
					if (roomInfo.getMiscInfo() == null) {
						roomInfo.setMiscInfo(RoomMiscInfo.builder().isRoomToBeCancelled(true).build());
					} else {
						roomInfo.getMiscInfo().setIsRoomToBeCancelled(true);
					}
				}
			});
		}
		try {
			processAmendment();
			log.info("Started supplier booking cancellation flow for booking id {} and ammendment id {}",
					request.getBookingId(), amendment.getId());
			isBookingCancelled = hmsComm.callSupplierCancelBooking(hInfo, request.getBookingId());
			log.info(
					"Finished supplier booking cancellation flow for booking id {} and ammendment id {} with cancellation status {}",
					request.getBookingId(), amendment.getId(), isBookingCancelled);
			if (!isBookingCancelled) {
				throw new CustomGeneralException(SystemError.ORDER_CANCELLATION_FAILED);
			}
		} catch (Exception e) {
			log.info("Booking Cannot Be Cancelled {}", bookingId, e);
			messageHandler.sendBookingEmail(originalOrder, orderItemList,
					EmailTemplateKey.HOTEL_BOOKING_CANCELLATION_FAILED_EMAIL, null);
			gmsCommunicator.addNote(
					Note.builder().bookingId(request.getBookingId()).noteType(NoteType.BOOKING_CANCELLATION_FAILED)
							.noteMessage("Booking Cancellation Failed From Supplier").build());
			amendment.setStatus(AmendmentStatus.REQUESTED);

		} finally {
			AmendmentHelper.updateAssignments(null, amendment);
			amendmendService.save(amendment);
			response.getAmendmentItems().add(0, amendment);
		}
		return isBookingCancelled;
	}

	private void processAmendment() {

		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(orderItemList).build().convert();
		hotelBookingCancellationHandler.updateCancellationFare(hInfo, request.getRoomKeys());
		HotelAmendOrderRequest hotelOrderRequest = new HotelAmendOrderRequest();
		HotelSearchQuery searchQuery = HotelSearchQuery.builder().checkinDate(orderItemList.get(0).getCheckInDate())
				.checkoutDate(orderItemList.get(0).getCheckOutDate()).build();
		hotelOrderRequest.setHInfo(hInfo);
		hotelOrderRequest.setBookingId(amendment.getBookingId());
		hotelOrderRequest.setDeliveryInfo(originalOrder.getDeliveryInfo());
		hotelOrderRequest.setRoomKeys(request.getRoomKeys());
		hotelOrderRequest.setSearchQuery(searchQuery);
		hotelOrderRequest.setSkipValidation(true);

		HotelAmendmentProcessor amendmentHandler =
				HotelAmendmentHandlerFactory.initHandler(amendment, hotelOrderRequest, "");

		amendmentHandler.process();
	}

	private void mergeAmendment() {
		UpdateAmendmentRequest request = new UpdateAmendmentRequest();
		request.setAmendmentId(amendment.getAmendmentId());
		updateHotelAmendmentHandler.setAction(AmendmentAction.MERGE);
		updateHotelAmendmentHandler.setSkipValidation(true);
		try {
			Amendment amendmentResponse = updateHotelAmendmentHandler.process(request);
			response.getAmendmentItems().add(0, amendmentResponse);
		} catch (Exception e) {
			e.printStackTrace();
			response.getAmendmentItems().add(0, amendment);
		}
	}

	private boolean isCancellationRequestNotInCache() {

		if (!StringUtils.isEmpty(request.getAmendmentId())) {
			HotelAmendmentRequest amendmentRequest = GsonUtils.getGson().fromJson(
					cachingService.get(
							CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONREQUEST.name())
									.key(request.getAmendmentId()).build(),
							String.class, false, true, BinName.CANCELLATION.getName())
							.get(BinName.CANCELLATION.getName()),
					HotelAmendmentRequest.class);

			if (amendmentRequest != null) {
				request.setRoomKeys(amendmentRequest.getRoomKeys());
				request.setType(AmendmentType.CANCELLATION);
				request.setBookingId(amendmentRequest.getBookingId());
				return false;
			}
			return true;
		}
		return false;

	}

	private String getUserIdFromConfigurator() {
		ClientGeneralInfo clientGeneralInfo =
				(ClientGeneralInfo) gmsCommunicator.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO).getOutput();
		return clientGeneralInfo.getCancellationAutomatedUserId();
	}
}
