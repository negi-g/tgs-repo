package com.tgs.services.oms.servicehandler.air;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.DealInventoryCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.ims.datamodel.InventoryMsgAttributes;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.restmodel.air.AirReleasePNRRequest;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirReleasePNRHandler extends ServiceHandler<AirReleasePNRRequest, BaseResponse> {

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private UserServiceCommunicator umsCommunicator;

	@Autowired
	private MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	private DealInventoryCommunicator inventoryCommunicator;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {

		boolean isReleaseSuccess = false;
		Order order = orderManager.findByBookingId(request.getBookingId(), null);

		if (CollectionUtils.isEmpty(request.getPnrs())) {
			throw new CustomGeneralException(SystemError.INVALID_CANCELLATION_PNR);
		}
		if (order == null) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
		if (OrderStatus.UNCONFIRMED.equals(order.getStatus())) {
			throw new CustomGeneralException(SystemError.PNR_ALREADY_CANCELLED);
		}
		User bookingUser = umsCommunicator.getUserFromCache(order.getBookingUserId());
		if (!OrderType.AIR.equals(order.getOrderType())
				|| !(OrderStatus.ON_HOLD.equals(order.getStatus()) || OrderStatus.UNCONFIRMED.equals(order.getStatus()))
				|| !AirBookingUtils.isAllowedFlowTypeForUnHold(order, bookingUser)) {
			throw new CustomGeneralException(SystemError.ORDER_INVALID_ACTION);
		}

		try {
			List<DbAirOrderItem> dbAirorderItems = airOrderItemService.findByBookingId(request.getBookingId());
			List<SegmentInfo> segmentInfos =
					AirBookingUtils.convertAirOrderItemListToSegmentInfoList(dbAirorderItems, null);
			List<TripInfo> tripInfos = BookingUtils.createTripListFromSegmentList(segmentInfos);
			Map<String, BookingSegments> cancellationSegmentMap =
					filterCancellationSegments(fmsCommunicator.getSupplierWiseBookingSegmentsUsingPNR(tripInfos),
							request.getPnrs(), order, bookingUser);
			log.info("Cancelling bookingid {} for segments  {}", order.getBookingId(), cancellationSegmentMap);
			isReleaseSuccess = fmsCommunicator.releasePnr(cancellationSegmentMap, order);

			sendSeatsSoldEmail(order, segmentInfos, "HOLD_UNCONFIRMED");
		} finally {
			if (isReleaseSuccess) {
				order.getAdditionalInfo().setFlowType(OrderFlowType.PNR_RELEASED);
				orderManager.save(order);
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

	private Map<String, BookingSegments> filterCancellationSegments(
			Map<String, BookingSegments> supplierWiseBookingSegments, List<String> cancellationPNR, Order order,
			User bookingUser) {

		Map<String, BookingSegments> cancellationSegments = new HashMap<>();
		for (String cancelPNR : cancellationPNR) {
			if (StringUtils.isEmpty(cancelPNR)) {
				throw new CustomGeneralException(SystemError.INVALID_CANCELLATION_PNR, cancelPNR);
			}
			Pair<String, BookingSegments> bookingSegmentsMap =
					AirReleasePNRHandler.getBookingSegmentFromAirlinePNR(cancelPNR, supplierWiseBookingSegments);
			if (bookingSegmentsMap == null
					|| !AirBookingUtils.isUnHoldAllowed(bookingSegmentsMap.getValue().getSegmentInfos(),
							order.getAdditionalInfo().getFlowType(), bookingUser)) {
				throw new CustomGeneralException(SystemError.INVALID_CANCELLATION_PNR, cancelPNR);
			}

			if (cancellationSegments.get(bookingSegmentsMap.getKey()) == null) {
				cancellationSegments.put(bookingSegmentsMap.getKey(), bookingSegmentsMap.getValue());
			}
		}
		return cancellationSegments;
	}

	private static Pair<String, BookingSegments> getBookingSegmentFromAirlinePNR(String airlinePNR,
			Map<String, BookingSegments> supplierWiseBookingSegments) {
		Pair<String, BookingSegments> keyValue = null;
		for (Entry<String, BookingSegments> entry : supplierWiseBookingSegments.entrySet()) {
			if (entry.getValue().getBookingSegmentsPNR().contains(airlinePNR)) {
				keyValue = new Pair<String, BookingSegments>(entry.getKey(), entry.getValue());
				break;
			}
		}
		return keyValue;
	}

	public AirReleasePNRRequest getCancelRequest(String bookingId) {
		if (StringUtils.isEmpty(bookingId)) {
			throw new CustomGeneralException(SystemError.ORDER_OR_BOOKINGID_MANDATORY);
		}
		Order order = orderManager.findByBookingId(bookingId, null);
		if (order == null) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
		AirReleasePNRRequest cancelRequest = new AirReleasePNRRequest();
		List<DbAirOrderItem> dbAirorderItems = airOrderItemService.findByBookingId(bookingId);
		Set<String> pnrs = new HashSet<>();

		for (DbAirOrderItem item : dbAirorderItems) {
			pnrs.add(item.getTravellerInfo().get(0).getPnr());
		}
		cancelRequest.setBookingId(bookingId);
		cancelRequest.setPnrs(pnrs.stream().collect(Collectors.toList()));
		return cancelRequest;
	}

	private void sendSeatsSoldEmail(Order order, List<SegmentInfo> segmentInfos, String status) {
		AbstractMessageSupplier<InventoryMsgAttributes> emailAttributeSupplier =
				new AbstractMessageSupplier<InventoryMsgAttributes>() {
					@Override
					public InventoryMsgAttributes get() {
						String inventoryId = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getInventoryId();;
						if (StringUtils.isNotBlank(inventoryId)) {
							LocalDate departureDate = segmentInfos.get(0).getDepartTime().toLocalDate();
							LocalDate arrivateDate =
									segmentInfos.get(segmentInfos.size() - 1).getArrivalTime().toLocalDate();
							String source = segmentInfos.get(0).getDepartureAirportCode();

							String dest = segmentInfos.get(segmentInfos.size() - 1).getArrivalAirportCode();

							Set<String> flightNumbers = segmentInfos.stream().filter(s -> s.getFlightNumber() != null)
									.map(s -> s.getFlightNumber()).collect(Collectors.toSet());

							String pnr = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getPnr();
							SupplierInfo supplierInfo = fmsCommunicator
									.getSupplierInfo(segmentInfos.get(0).getPriceInfo(0).getSupplierId());
							User user =
									umsCommunicator.getUserFromCache(supplierInfo.getCredentialInfo().getUserName());
							InventoryMsgAttributes emailAttributes =
									InventoryMsgAttributes.builder().toEmailId(user.getEmail()).supplier(user.getName())
											.refId(order.getBookingId()).role(user.getRole()).inventoryId(inventoryId)
											.airlinePnr(pnr).flightDetails(flightNumbers.toString())
											.travelDate(DateFormatterHelper.formatDate(departureDate,
													DateFormatType.BOOKING_EMAIL_FORMAT))
											.bookingDate(DateFormatterHelper.formatDate(LocalDate.now(),
													DateFormatType.BOOKING_EMAIL_FORMAT))
											.departure(source).arrival(
													dest)
											.departTime(DateFormatterHelper.format(segmentInfos.get(0).getDepartTime(),
													"HH:mm"))
											.arrivalTime(DateFormatterHelper.format(
													segmentInfos.get(segmentInfos.size() - 1).getArrivalTime(),
													"HH:mm"))
											.timeLimit(
													BookingUtils.getTimeLimitString(segmentInfos.get(0).getTimeLimit()))
											.key(EmailTemplateKey.UNCONFIMRED_INVENTORY_ORDER_EMAIL.name())
											.status(status).build();


							emailAttributes.setInventoryName(inventoryCommunicator.getInventoryName(inventoryId));

							List<String> paxNames = new ArrayList<>();
							segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo().forEach(traveller -> {
								String tName = traveller.getFullName() + "("
										+ PaxType.getStringReprestation(traveller.getPaxType()) + ")";
								if (PaxType.INFANT.equals(traveller.getPaxType()) && traveller.getDob() != null)
									tName.concat("DOB: ").concat(traveller.getDob().toString());
								paxNames.add(tName);
							});
							emailAttributes.setPaxNames(Joiner.on("<br/>").join(paxNames));
							return emailAttributes;
						}
						return null;
					}
				};
		msgSrvCommunicator.sendMail(emailAttributeSupplier.getAttributes());
	}

}
