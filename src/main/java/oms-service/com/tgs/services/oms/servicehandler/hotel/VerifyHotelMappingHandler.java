package com.tgs.services.oms.servicehandler.hotel;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.VerifyHotelMappingQuery;
import com.tgs.services.base.datamodel.VerifyMappingRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.ProcessedHotelInfo;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.DbHotelOrder;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class VerifyHotelMappingHandler extends ServiceHandler<OrderFilter, BaseResponse> {
	@Autowired
	HotelOrderItemManager hotelOrderItemManager;

	@Autowired
	private HMSCommunicator hmsCommunicator;

	@Autowired
	private GeneralServiceCommunicator gmsComn;

	private static DateTimeFormatter dd_mm_yyyy = DateTimeFormatter.ofPattern("ddMMYYYY");

	private static String currentTimestamp = LocalDate.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd"));

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		List<DbHotelOrder> hotelOrders = new ArrayList<>();
		OrderFilter orderFilter = OrderFilter.builder().build();
		orderFilter.setProducts(Arrays.asList(OrderType.HOTEL));
		orderFilter.setStatuses(Arrays.asList(OrderStatus.SUCCESS));
		orderFilter.setCreatedOnBeforeDate(request.getCreatedOnBeforeDate());
		orderFilter.setCreatedOnAfterDate(request.getCreatedOnAfterDate());
		hotelOrders = hotelOrderItemManager.getHotelOrderList(orderFilter);
		if (CollectionUtils.isEmpty(hotelOrders)) {
			log.info("No success hotel orders found from date {} to date {}", request.getCreatedOnBeforeDate(),
					request.getCreatedOnAfterDate());
			return;
		}
		log.info("Total hotel order fetched from date {} to date {} are {}", request.getCreatedOnBeforeDate(),
				request.getCreatedOnAfterDate(), hotelOrders.size());
		uploadHotelMappings(hotelOrders);
	}

	private void uploadHotelMappings(List<DbHotelOrder> hotelOrders) throws SftpException, JSchException, IOException {
		VerifyMappingRequest verifyMappingDataList = getOrderContent(hotelOrders);
		ConfiguratorInfo configuratorInfo = gmsComn.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO);
		ClientGeneralInfo clientIfo = (ClientGeneralInfo) configuratorInfo.getOutput();
		log.info("Started verify mapping for supplier {} on date {}", clientIfo.getVerifyMappingSupplierId(),
				currentTimestamp);
		hmsCommunicator.verifyHotelMapping(clientIfo.getVerifyMappingSupplierId(), verifyMappingDataList);
	}

	private VerifyMappingRequest getOrderContent(List<DbHotelOrder> hotelOrders) {
		VerifyMappingRequest verifyMapping = VerifyMappingRequest.builder().build();
		List<VerifyHotelMappingQuery> verifyMappinglist = new ArrayList<>();
		hotelOrders.forEach(order -> {
			verifyMappinglist.add(getFieldsFromOrder(order));
		});
		verifyMapping.setVerifyMappinglist(verifyMappinglist);
		return verifyMapping;
	}

	private VerifyHotelMappingQuery getFieldsFromOrder(DbHotelOrder order) {
		ProcessedHotelInfo processedInfo = order.getOrderItems().get(0).getAdditionalInfo().getHInfo();

		return VerifyHotelMappingQuery.builder().unicaId(processedInfo.getUnicaId())
				.bookingReferenceId(order.getOrder().getBookingId()).bookingClientName("TRIPJACK")
				.providerHotelId(processedInfo.getSupplierHotelId())
				.providerName(order.getOrderItems().get(0).getAdditionalInfo().getSupplierId())
				.checkInDate(order.getOrderItems().get(0).getCheckInDate().format(dd_mm_yyyy))
				.checkOutDate(order.getOrderItems().get(0).getCheckOutDate().format(dd_mm_yyyy)).build();
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
