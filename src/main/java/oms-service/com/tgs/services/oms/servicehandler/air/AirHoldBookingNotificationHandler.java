package com.tgs.services.oms.servicehandler.air;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.fms.datamodel.AirMessageAttributes;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AdditionalInfoFilter;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.air.AirOrderItemFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirHoldBookingNotificationHandler {

	@Autowired
	private AirOrderItemService orderItemService;

	@Autowired
	private GeneralServiceCommunicator gsCommunicator;

	@Autowired
	private OrderService orderService;

	@Autowired
	private MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	protected UserServiceCommunicator userService;

	public void processNotificationJob(Integer minsFrom, Integer minsTo) {

		LocalDateTime createdAfterTime = LocalDateTime.now().minusMonths(3);
		LocalDateTime createdBeforeTime = LocalDateTime.now();


		LocalDateTime currentTime = LocalDateTime.now();

		AdditionalInfoFilter additionalInfoFilter =
				AdditionalInfoFilter.builder().holdTimeLimitBeforeDateTime(currentTime.plusMinutes(minsTo.longValue()))
						.holdTimeLimitAfterDateTime(currentTime.plusMinutes(minsFrom.longValue())).build();

		AirOrderItemFilter itemFilter = AirOrderItemFilter.builder().additionalInfoFilter(additionalInfoFilter).build();

		OrderFilter orderFilter = OrderFilter.builder().statuses(Arrays.asList(OrderStatus.ON_HOLD))
				.createdOnAfterDateTime(createdAfterTime).createdOnBeforeDateTime(createdBeforeTime)
				.itemFilter(itemFilter).build();

		List<Object[]> results = orderItemService.findByJsonSearch(orderFilter);

		Set<String> bookingIds = new HashSet<>();
		Map<String, DbOrder> ordersMap = new HashMap<>();
		for (Object[] result : results) {
			String bookingId = ((DbAirOrderItem) result[0]).getBookingId();
			bookingIds.add(bookingId);
			ordersMap.put(bookingId, (DbOrder) result[1]);
		}

		log.info("Hold Booking Notification Orders Found {} ", CollectionUtils.size(bookingIds));
		for (String bookingId : bookingIds) {
			DbOrder order = ordersMap.get(bookingId);
			log.info("Sending hold expiry notification for bookingId {}", bookingId);
			sendNotificationMail(order);
		}
	}

	private void sendNotificationMail(DbOrder order) {
		User user = userService.getUserFromCache(order.getBookingUserId());
		List<DbAirOrderItem> items = orderItemService.findByBookingId(order.getBookingId());
		List<AirOrderItem> airOrderItems = DbAirOrderItem.toDomainList(items);
		AbstractMessageSupplier<AirMessageAttributes> emailAttributeSupplier =
				new AbstractMessageSupplier<AirMessageAttributes>() {
					@Override
					public AirMessageAttributes get() {
						AirMessageAttributes emailAttributes = AirMessageAttributes.builder().toEmailId(user.getEmail())
								.bookingId(order.getBookingId()).agentName(user.getName())
								.status(OrderStatus.getEnumFromCode(order.getStatus()).toString())
								.timeLimit(AirBookingUtils.getMostRestrictiveTimeLimit(airOrderItems)
										.truncatedTo(ChronoUnit.MINUTES).toString())
								.key(EmailTemplateKey.AIR_HOLD_EXPIRATION_NOTIFY_EMAIL.name()).build();
						return emailAttributes;
					}
				};
		msgSrvCommunicator.sendMail(emailAttributeSupplier.getAttributes());
	}


}
