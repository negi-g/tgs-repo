package com.tgs.services.oms.mapper.rail;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.rail.datamodel.RailBookingRelatedInfo;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPaxType;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import lombok.Builder;

@Builder
public class RailOrderItemToJourneyInfoMapper extends Mapper<RailJourneyInfo> {


	private DbRailOrderItem item;
	private RailCommunicator railComm;
	private OrderManager orderManger ;

	@Override
	protected void execute() throws CustomGeneralException {
		if (output == null) {
			output = new RailJourneyInfo();
		}
		output.setDeparture(item.getDepartureTime());
		output.setArrival(item.getArrivalTime());
		output.setDepartStationInfo(railComm.getRailStationInfo(item.getFromStn()));
		output.setArrivalStaionInfo(railComm.getRailStationInfo(item.getToStn()));

		output.setReservationUptoStationInfo(railComm.getRailStationInfo(
				ObjectUtils.firstNonNull(item.getAdditionalInfo().getReservationUptoStation(), item.getToStn())));
		output.setBoardingStationInfo(railComm.getRailStationInfo(
				ObjectUtils.firstNonNull(item.getAdditionalInfo().getBoardingStation(), item.getFromStn())));
		output.setBoardingTime(item.getAdditionalInfo().getBoardingTime());
		output.setBookingQuota(item.getAdditionalInfo().getQuota());
		output.setDays(item.getAdditionalInfo().getDays());
		output.setDistance(item.getAdditionalInfo().getDistance());
		output.setDuration(String.valueOf(item.getAdditionalInfo().getDuration()));
		output.setRailInfo(item.getAdditionalInfo().getRailInfo());
		output.setDays(item.getAdditionalInfo().getDays());
		output.setAvlJourneyClasses(Arrays.asList(item.getAdditionalInfo().getJourneyClass().getCode()));
		output.setAvlblForVikalp(item.getAdditionalInfo().getAvblForVikalp());
		RailBookingRelatedInfo bookingRelatedInfo = RailBookingRelatedInfo.builder().build();
		bookingRelatedInfo.setTravellerInfos(item.getTravellerInfo());
		bookingRelatedInfo.setTicketAddress(item.getAdditionalInfo().getTicketAddress());
		bookingRelatedInfo.setJourneyClass(item.getAdditionalInfo().getJourneyClass());
		bookingRelatedInfo.setReservationId(item.getAdditionalInfo().getReservationId());
		bookingRelatedInfo.setMobileNumber(item.getAdditionalInfo().getPassengerMobileNumber());
		bookingRelatedInfo.setIsThroughOTP(item.getAdditionalInfo().getIsThroughOTP());
		bookingRelatedInfo.setIsTravelInsuranceOpted(item.getAdditionalInfo().getIsTravelInsuranceOpted());
		bookingRelatedInfo.setPnr(item.getAdditionalInfo().getPnr());
		setTravellerPaxType(bookingRelatedInfo.getTravellerInfos());

		if (orderManger != null) {
			GstInfo gstinfo = orderManger.getGstInfo(item.getBookingId());
			if (gstinfo != null && StringUtils.isNotBlank(gstinfo.getGstNumber())) {
				bookingRelatedInfo.setGstInfo(gstinfo);
			}
		}
		
		output.setBookingRelatedInfo(bookingRelatedInfo);
	}

	private void setTravellerPaxType(List<RailTravellerInfo> travellerInfos) {
		for (RailTravellerInfo traveller : travellerInfos) {
			traveller.setPaxType(getRailPaxType(traveller.getAge(), traveller.getGender()));
		}
	}

	private RailPaxType getRailPaxType(Integer age, String gender) {
		if (java.util.Objects.isNull(age) || age < 5)
			return RailPaxType.INFANT;
		else if (age >= 5 && age <= 11)
			return RailPaxType.CHILD;
		else if (age >= 58 && "F".equals(gender))
			return RailPaxType.SENIOR;
		else if (age >= 60)
			return RailPaxType.SENIOR;
		else
			return RailPaxType.ADULT;
	}
}
