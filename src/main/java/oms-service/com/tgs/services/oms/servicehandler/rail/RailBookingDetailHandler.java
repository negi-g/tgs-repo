package com.tgs.services.oms.servicehandler.rail;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.oms.dbmodel.DbOrder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.datamodel.ItemDetails;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.rail.RailOrderDetails;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.mapper.rail.RailOrderItemToJourneyInfoMapper;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RailBookingDetailHandler extends ServiceHandler<BookingDetailRequest, BookingDetailResponse> {


	@Autowired
	private OrderService orderService;

	@Autowired
	private UserServiceCommunicator userComm;

	@Autowired
	private RailCommunicator railComm;

	@Autowired
	private RailOrderItemService itemService;
	
	@Autowired
	private OrderManager orderManager;
	
	@Override
	public void beforeProcess() throws Exception {
		response = new BookingDetailResponse();
		ProductMetaInfo metaInfo = Product.getProductMetaInfoFromId(request.getBookingId());
		if (!metaInfo.getProduct().equals(Product.RAIL)) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
	}

	@Override
	public void process() throws Exception {
		try {
			OrderFilter filter = OrderFilter.builder().bookingIds(Arrays.asList(request.getBookingId())).build();
			List<DbOrder> dbOrders = orderService.findAll(filter);
			if (CollectionUtils.isEmpty(dbOrders)) {
				throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
			}
			Order order = new GsonMapper<>(dbOrders.get(0), Order.class).convert();
			if (SystemContextHolder.getContextData().getUser() != null
					&& !UserRole.GUEST.equals(SystemContextHolder.getContextData().getUser().getRole())) {
				UserServiceHelper.checkAndReturnAllowedUserId(
						SystemContextHolder.getContextData().getUser().getLoggedInUserId(),
						Arrays.asList(order.getBookingUserId()));
			}
			User bookingUser = userComm.getUserFromCache(order.getBookingUserId());
			response.setBookingUser(User.builder().userId(bookingUser.getUserId()).build());
			response.setOrder(order);
			DbRailOrderItem dbOrderItem = itemService.findByBookingId(order.getBookingId());
			RailJourneyInfo journeyInfo =
					RailOrderItemToJourneyInfoMapper.builder().railComm(railComm).item(dbOrderItem).orderManger(orderManager).build().convert();
			String obs = dbOrderItem.getAdditionalInfo().getOldboardingStation();
			RailStationInfo oldBoardingStation = StringUtils.isNotEmpty(obs) ? railComm.getRailStationInfo(obs) : null;
			RailOrderDetails orderDetails = RailOrderDetails.builder().oldBoardingStation(oldBoardingStation)
					.vikalpOptStatus(dbOrderItem.getAdditionalInfo().getVikalpOptStatus())
					.priceInfo(dbOrderItem.getAdditionalInfo().getPriceInfo()).journeyInfo(journeyInfo).build();
			Map<String, ItemDetails> map = new HashMap<>();
			map.put(OrderType.RAIL.getName(), orderDetails);
			response.setItemInfos(map);
			checkForOrderStatus();
		} catch (Exception e) {
			if (e instanceof CustomGeneralException) {
				log.info("Unable to find details for bookingId {} cause {}", request.getBookingId(), e.getMessage());
				throw e;
			}
			log.error("Unable to find any order corresponding to bookingId {}", request.getBookingId(), e);
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
	}


	private void checkForOrderStatus() {
		Order order = new GsonMapper<>(orderService.findByBookingId(request.getBookingId()), Order.class).convert();
		if (!(order.getStatus().equals(OrderStatus.SUCCESS) || order.getStatus().equals(OrderStatus.ON_HOLD)
				|| order.getStatus().equals(OrderStatus.FAILED) || order.getStatus().equals(OrderStatus.PENDING)
				|| OrderStatus.ABORTED.equals(order.getStatus()))
				&& LocalDateTime.now().minusMinutes(5).isBefore(order.getCreatedOn())) {
			response.setRetryInSecond(10);
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
