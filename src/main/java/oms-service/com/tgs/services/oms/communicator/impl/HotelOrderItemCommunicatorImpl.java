package com.tgs.services.oms.communicator.impl;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.hotel.HotelBnplStatusUpdateRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingInfoUpdateRequest;
import com.tgs.services.oms.servicehandler.hotel.HotelOrderMessageHandler;

@Service
public class HotelOrderItemCommunicatorImpl implements HotelOrderItemCommunicator {

	@Autowired
	private HotelOrderItemManager itemManager;

	@Autowired
	private HotelOrderItemService service;

	@Autowired
	OrderManager orderManager;

	@Autowired
	private HotelOrderMessageHandler messageHandler;

	@Override
	public void updateOrderItem(HotelInfo hInfo, Order order, HotelItemStatus itemStatus) {
		itemManager.updateOrderItem(hInfo, order, itemStatus);
	}

	@Override
	public void updateOrderItemStatus(Order order, HotelItemStatus itemStatus) {
		itemManager.updateOrderItemStatus(order, itemStatus);
	}

	@Override
	public List<HotelOrderItem> getHotelOrderItems(List<String> bookingIds) {
		if (CollectionUtils.isEmpty(bookingIds)) {
			return new ArrayList<>();
		}
		return service.findByBookingId(bookingIds);
	}

	@Override
	public HotelInfo getHotelInfo(String bookingId) {
		List<DbHotelOrderItem> items = itemManager.getItemList(bookingId);
		return DbHotelOrderItemListToHotelInfo.builder().itemList(items).build().convert();
	}

	@Override
	public void updateHotelReferenceNumber(HotelBookingInfoUpdateRequest request) {

		Order order = orderManager.findByBookingId(request.getBookingId(), null);
		if (order == null) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
		List<DbHotelOrderItem> items = itemManager.getItemList(request.getBookingId());
		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(items).build().convert();
		if (hInfo.getMiscInfo() == null) {
			hInfo.setMiscInfo(HotelMiscInfo.builder().build());
		}
		HotelItemStatus itemStatus = null;
		if (request.getHotelBookingReference() != null) {
			OrderStatus orderStatus = order.getStatus();
			if (!(orderStatus.equals(OrderStatus.SUCCESS) || orderStatus.equals(OrderStatus.ON_HOLD))) {
				throw new CustomGeneralException(SystemError.HOTEL_BOOKING_REFERENCE_CANNOT_BE_UPDATED,
						"As Order is in " + orderStatus + " Status");
			}
			hInfo.getMiscInfo().setHotelBookingReference(request.getHotelBookingReference());
		}

		itemManager.updateOrderItem(hInfo, order, itemStatus);
		if (request.getHotelBookingReference() != null) {
			items.get(0).getAdditionalInfo().setHotelBookingReference(request.getHotelBookingReference());
			messageHandler.sendBookingEmail(order, items, EmailTemplateKey.HOTEL_BOOKING_CONFIRMATION_EMAIL, null);
			messageHandler.sendBookingSms(order, items, SmsTemplateKey.HOTEL_BOOKING_CONFIRMATION_SMS, null);
		}
	}

	@Override
	public void updateHotelBnplStatus(HotelBnplStatusUpdateRequest request) {
		Order order = orderManager.findByBookingId(request.getBookingId(), null);
		if (order == null) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
		List<DbHotelOrderItem> items = itemManager.getItemList(request.getBookingId());
		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(items).build().convert();
		if (hInfo.getMiscInfo() == null) {
			hInfo.setMiscInfo(HotelMiscInfo.builder().build());
		}
		HotelItemStatus itemStatus = null;
		if (request.getBnplStatus() != null) {
			hInfo.getMiscInfo().setSupplierBnplStatus(request.getBnplStatus());
		}
		itemManager.updateOrderItem(hInfo, order, itemStatus);

	}


}
