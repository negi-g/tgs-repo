package com.tgs.services.oms.dbmodel.air;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.oms.datamodel.AirItemDetail;
import com.tgs.services.oms.datamodel.AirItemDetailInfo;
import com.tgs.services.oms.hibernate.air.AirItemDetailInfoType;
import com.tgs.services.oms.hibernate.air.LowestFareFlightInfoType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@Audited
@Entity(name = "airitemdetail")
@Table(name = "airitemdetail")
@TypeDefs({
        @TypeDef(name = "AirItemDetailInfoType", typeClass = AirItemDetailInfoType.class),
        @TypeDef(name = "LowestFareFlightInfoType", typeClass = LowestFareFlightInfoType.class)
})
public class DbAirItemDetail extends BaseModel<DbAirItemDetail, AirItemDetail> {

    @Column
    private String bookingId;

    @Column
    @Type(type = "AirItemDetailInfoType")
    private AirItemDetailInfo info;

    @Column
    @CreationTimestamp
    private LocalDateTime createdOn;

    public AirItemDetailInfo getInfo() {
        return info == null ? new AirItemDetailInfo() : info;
    }

    public AirItemDetail toDomain() {
        return new GsonMapper<>(this, AirItemDetail.class).convert();
    }

    public DbAirItemDetail from(AirItemDetail dataModel) {
        return new GsonMapper<>(dataModel, DbAirItemDetail.class).convert();
    }
}
