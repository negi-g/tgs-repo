package com.tgs.services.oms.servicehandler.misc;

import java.util.StringJoiner;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.manager.AbstractInvoiceIdManager;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.misc.MiscBookingManager;
import com.tgs.services.oms.restmodel.misc.MiscBookingRequest;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.ums.datamodel.RailApplicationStatus;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MiscBookingHandler extends ServiceHandler<MiscBookingRequest, BookingResponse> {

	@Autowired
	protected MiscBookingManager bookingManager;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private AbstractInvoiceIdManager invoiceIdManager;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		Order order = null;
		StringJoiner exceptions = new StringJoiner("-");
		MiscBookingRequest bookingRequest = request;
		try {
			if (StringUtils.isEmpty(request.getBookingId())) {
				String bookingId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.MISC).build());
				request.setBookingId(bookingId);
			}
			request.setType(OrderType.MISC);
			request.setFlowType(OrderFlowType.ONLINE);
			User bookingUser =
					bookingRequest.getUserId() != null ? userService.getUserFromCache(bookingRequest.getUserId())
							: SystemContextHolder.getContextData().getUser();
			bookingManager.setBookingUser(bookingUser);
			bookingManager.setLoggedInUser(SystemContextHolder.getContextData().getUser());
			bookingManager.setBookingRequest(bookingRequest);
			double orderTotal = 0;
			if (CollectionUtils.isNotEmpty(request.getPaymentInfos())) {
				for (PaymentRequest payment : request.getPaymentInfos()) {
					payment.setPayUserId(bookingUser.getUserId());
					payment.setRefId(request.getBookingId());
					payment.setProduct(Product.MISC);
					payment.getAdditionalInfo().setComments(request.getDescription());
					orderTotal += payment.getAmount().doubleValue();
				}
				bookingRequest.setPaymentInfos(request.getPaymentInfos());
			}
			bookingManager.setOrderTotal(orderTotal);
			bookingManager.buildPaymentInfos();
			order = bookingManager.processBooking();
			order = orderManager.findByBookingId(order.getBookingId(), order);
			if (OrderStatus.PAYMENT_SUCCESS.equals(order.getStatus())) {
				bookingManager.updateItemStatus(OrderStatus.SUCCESS);
				order.setStatus(OrderStatus.SUCCESS);
				invoiceIdManager.setInvoiceIdFor(order, Product.getProductMetaInfoFromId(order.getBookingId()));
				order = orderManager.save(order);
				if (StringUtils.isNotEmpty(request.getDescription()) && request.getDescription().contains("Rail")) {
					bookingUser.getRailAdditionalInfo().setIsPaymentSuccess(true);
					bookingUser.getRailAdditionalInfo().setApplicationStatus(RailApplicationStatus.PAID);
					userService.updateUser(bookingUser);
				}
			}
			log.info("Misc order created for booking id {} and order id {}", request.getBookingId(), order.getId());
		} catch (Exception e) {
			exceptions.add(e.getMessage());
			if (e instanceof CustomGeneralException
					&& ((CustomGeneralException) e).getError().equals(SystemError.INSUFFICIENT_BALANCE)) {
				log.info("Misc Order Booking Failed for bookingId {} and cause {} ", request.getBookingId(),
						e.getMessage(), e);
				throw e;
			} else {
				log.error("Misc Order Booking Failed for bookingId {} and cause {} ", request.getBookingId(),
						e.getMessage(), e);
				throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {
		response.setBookingId(request.getBookingId());
	}


}
