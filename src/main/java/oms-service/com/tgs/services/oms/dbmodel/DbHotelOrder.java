package com.tgs.services.oms.dbmodel;

import java.util.List;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class DbHotelOrder {
	
	private DbOrder order;
	private List<DbHotelOrderItem> orderItems;

}
