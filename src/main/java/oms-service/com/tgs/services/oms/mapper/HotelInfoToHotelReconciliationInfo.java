package com.tgs.services.oms.mapper;

import java.time.LocalDate;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.CityInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.datamodel.hotel.HotelBookingReconciliation;
import com.tgs.services.oms.datamodel.hotel.HotelBookingReconciliationAdditionalInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class HotelInfoToHotelReconciliationInfo extends Mapper<HotelBookingReconciliation> {

	private HotelInfo hotelInfo;
	private DeliveryInfo deliveryInfo;
	private LocalDate bookingDate;
	private String bookingStatus;
	private String bookingId;
	private String userId;

	@Override
	protected void execute() throws CustomGeneralException {
		if (output == null) {
			output = new HotelBookingReconciliation();
		}

		Option option = hotelInfo.getOptions().get(0);
		RoomInfo firstRoomInfo = option.getRoomInfos().get(0);
		output.setCheckInDate(firstRoomInfo.getCheckInDate());
		output.setCheckOutDate(firstRoomInfo.getCheckOutDate());
		output.setAdditionalInfo(HotelBookingReconciliationAdditionalInfo.builder()
				.addressInfo(AddressInfo.builder()
						.cityInfo(CityInfo.builder()
								.name(StringUtils.upperCase(hotelInfo.getAddress().getCity().getName()))
								.country(StringUtils.upperCase(hotelInfo.getAddress().getCountry().getName())).build())
						.build())
				.currency(BaseHotelUtils.getCurrency()).deliveryInfo(deliveryInfo).build());
		output.setCancellationDeadline(option.getDeadlineDateTime());
		output.setHotelName(hotelInfo.getName());
		output.setBookingStatus(bookingStatus);
		output.setBookingDate(bookingDate);
		output.setRoomInfos(option.getRoomInfos());
		output.setBookingId(bookingId);
		output.setAmount(option.getTotalPrice());
		output.setReconcilerUserId(userId);
	}
}
