package com.tgs.services.oms.restcontroller.misc;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.oms.restmodel.InvoiceRequest;
import com.tgs.services.oms.restmodel.OrderResponse;
import com.tgs.services.oms.restmodel.misc.MiscBookingRequest;
import com.tgs.services.oms.restmodel.misc.MiscInvoiceResponse;
import com.tgs.services.oms.servicehandler.misc.MiscBookingDetailHandler;
import com.tgs.services.oms.servicehandler.misc.MiscBookingHandler;
import com.tgs.services.oms.servicehandler.misc.MiscInvoiceHandler;
import com.tgs.services.oms.servicehandler.misc.MiscOrderListingHandler;

@RestController
@RequestMapping("/oms/v1/misc")
public class MiscOrderController {

	@Autowired
	protected MiscBookingHandler bookingHandler;

	@Autowired
	protected MiscOrderListingHandler orderListingHandler;

	@Autowired
	protected MiscInvoiceHandler invoiceHandler;

	@Autowired
	protected MiscBookingDetailHandler bookingDetailHandler;

	@RequestMapping(value = "/book", method = RequestMethod.POST)
	protected BookingResponse orderCreate(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody MiscBookingRequest bookingRequest) throws Exception {
		bookingHandler.initData(bookingRequest, new BookingResponse());
		return bookingHandler.getResponse();
	}

	@RequestMapping(value = "/orders", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected OrderResponse getOrderList(HttpServletRequest request, @Valid @RequestBody OrderFilter orderFilter,
			BindingResult result) throws Exception {
		orderFilter.setProducts(Arrays.asList(OrderType.MISC));
		orderFilter.setBookingUserIds(UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), orderFilter.getBookingUserIds()));
		FiltersValidator validator =
				(FiltersValidator) SpringContext.getApplicationContext().getBean("filtersValidator");
		validator.validate(orderFilter, result);
		if (result.hasErrors()) {
			OrderResponse response = new OrderResponse();
			response.setErrors(validator.getErrorDetailFromBindingResult(result));
			return response;
		} else {
			orderListingHandler.initData(orderFilter, new OrderResponse());
			return orderListingHandler.getResponse();
		}
	}

	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	@RequestMapping(value = "/booking-details", method = RequestMethod.POST)
	protected BookingDetailResponse getBookingDetails(HttpServletRequest request,
			@RequestBody BookingDetailRequest detailRequest) throws Exception {
		bookingDetailHandler.initData(detailRequest, new BookingDetailResponse());
		return bookingDetailHandler.getResponse();
	}

	@RequestMapping(value = "/invoice", method = RequestMethod.POST)
	protected MiscInvoiceResponse getInvoiceDetail(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody InvoiceRequest miscInvoiceRequest) throws Exception {
		invoiceHandler.initData(miscInvoiceRequest, new MiscInvoiceResponse());
		return invoiceHandler.getResponse();
	}
}
