package com.tgs.services.oms.jparepository.hotel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.tgs.services.oms.dbmodel.hotel.DbHotelBookingReconciliation;

@Repository
public interface HotelBookingReconciliationRepository extends JpaRepository<DbHotelBookingReconciliation, Long>,
		JpaSpecificationExecutor<DbHotelBookingReconciliation> {

	
	DbHotelBookingReconciliation findByBookingId(String bookingId);
}
