package com.tgs.services.oms.jparepository.misc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.misc.DbMiscOrderItem;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MiscOrderItemService {

	@Autowired
	private MiscOrderItemRepository miscItemRepository;

	@PersistenceContext
	private EntityManager em;

	public DbMiscOrderItem save(DbMiscOrderItem orderItem) {
		orderItem.setProcessedOn(LocalDateTime.now());
		return miscItemRepository.saveAndFlush(orderItem);
	}

	public DbMiscOrderItem findByBookingId(String bookingId) {
		return miscItemRepository.findByBookingId(bookingId);
	}

	public Integer getNextInvoiceId() {
		return miscItemRepository.getNextInvoiceId();
	}

	public List<Object[]> findByJsonSearch(OrderFilter orderFilter) {
		List<Object[]> results = new ArrayList<>();

		for (int i = 0; i < 20; i++) {

			StringBuilder query = new StringBuilder(
					"SELECT {a.*} , {b.*} from miscorderitem a JOIN orders b " + "ON a.bookingId = b.bookingId ");
			List<String> queryParams = new ArrayList<>();
			if (orderFilter != null) {
				if (CollectionUtils.isNotEmpty(orderFilter.getBookingUserIds())) {
					queryParams.add("b.bookinguserid IN('"
							.concat(Joiner.on("','").join(orderFilter.getBookingUserIds())).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getLoggedInUserIds())) {
					queryParams.add("b.loggedinuserid IN('"
							.concat(Joiner.on("','").join(orderFilter.getLoggedInUserIds())).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getBookingIds())) {
					queryParams.add(
							"b.bookingId IN('".concat(Joiner.on("','").join(orderFilter.getBookingIds())).concat("')"));
				}
				if (StringUtils.isNotEmpty(orderFilter.getBookingId())) {
					queryParams.add("b.bookingId = '".concat(orderFilter.getBookingId().concat("'")));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getStatuses())) {
					List<String> statusCodes = OrderStatus.getCodes(orderFilter.getStatuses());
					queryParams.add("b.status IN ('".concat(Joiner.on("','").join(statusCodes)).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getChannels())) {
					List<String> channelCodes = ChannelType.getCodes(orderFilter.getChannels());
					queryParams.add("b.channeltype IN ('".concat(Joiner.on("','").join(channelCodes)).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getProducts())) {
					List<String> orderCodes = OrderType.getCodes(orderFilter.getProducts());
					queryParams.add("b.ordertype IN ('".concat(Joiner.on("','").join(orderCodes)).concat("')"));
				}
				if (orderFilter.getCreatedOnAfterDateTime() != null) {
					queryParams.add(
							"b.createdOn >= '".concat(orderFilter.getCreatedOnAfterDateTime().toString()).concat("'"));
				}
				if (orderFilter.getCreatedOnBeforeDateTime() != null) {
					queryParams.add(
							"b.createdOn <= '".concat(orderFilter.getCreatedOnBeforeDateTime().toString()).concat("'"));
				}
				if (orderFilter.getCreatedOnAfterDate() != null) {
					queryParams
							.add("b.createdOn >= '".concat(orderFilter.getCreatedOnAfterDate().toString()).concat("'"));
				}
				if (orderFilter.getCreatedOnBeforeDate() != null) {
					queryParams.add(
							"b.createdOn <= '".concat(orderFilter.getCreatedOnBeforeDate().toString()).concat("'"));
				}
				if (orderFilter.getProcessedOnAfterDateTime() != null) {
					queryParams.add("b.processedOn >= '".concat(orderFilter.getProcessedOnAfterDateTime().toString())
							.concat("'"));
				}
				if (orderFilter.getProcessedOnBeforeDateTime() != null) {
					queryParams.add("b.processedOn <= '".concat(orderFilter.getProcessedOnBeforeDateTime().toString())
							.concat("'"));
				}
				if (orderFilter.getProcessedOnAfterDate() != null) {
					queryParams.add(
							"b.processedOn >= '".concat(orderFilter.getProcessedOnAfterDate().toString()).concat("'"));
				}
				if (orderFilter.getProcessedOnBeforeDate() != null) {
					queryParams.add(
							"b.processedOn <= '".concat(orderFilter.getProcessedOnBeforeDate().toString()).concat("'"));
				}
			}
			if (CollectionUtils.isNotEmpty(queryParams)) {
				query.append(" WHERE ").append(Joiner.on(" AND ").join(queryParams));
			}
			if (CollectionUtils.isNotEmpty(orderFilter.getSortByAttr())) {
				Direction direction = Direction.fromString(orderFilter.getSortByAttr().get(0).getOrderBy());
				List<String> properties = orderFilter.getSortByAttr().get(0).getParams();
				query.append(" order by a.").append(Joiner.on(",a.").join(properties)).append(" " + direction.name());
			} else {
				query.append(" order by ").append("a.createdOn,a.bookingid");
			}
			query.append(" limit 1500 offset " + (i * 1500)).append(" ;");
			log.debug("[MiscOrderSearch] Query formed is {} ", query.toString());

			EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
			List<Object[]> pageResults = entityManager.unwrap(Session.class).createNativeQuery(query.toString())
					.addEntity("a", DbMiscOrderItem.class)
					.setFetchSize(orderFilter.getPageAttr() != null ? orderFilter.getPageAttr().getSize() : 5)
					.addEntity("b", DbOrder.class).getResultList();
			entityManager.close();
			if (CollectionUtils.isNotEmpty(pageResults))
				results.addAll(pageResults);

			if (CollectionUtils.isEmpty(pageResults) || pageResults.size() < 1500) {
				break;
			}
		}
		return results;
	}
}
