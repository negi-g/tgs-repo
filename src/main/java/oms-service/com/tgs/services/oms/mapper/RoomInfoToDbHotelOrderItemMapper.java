package com.tgs.services.oms.mapper;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo.AdditionalHotelOrderItemInfoBuilder;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelOrderSupplierInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSSR;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;

@Builder
public class RoomInfoToDbHotelOrderItemMapper extends Mapper<DbHotelOrderItem> {

	private HotelInfo hInfo;
	private String bookingId;
	private List<RoomSSR> roomSSR;
	private List<TravellerInfo> roomTravellerInfo;
	private RoomInfo roomInfo;
	private int index;
	private User bookingUser;

	@Override
	protected void execute() throws CustomGeneralException {
		if (output == null) {
			output = DbHotelOrderItem.builder().build();
		}

		if (roomInfo == null) {
			roomInfo = new RoomInfo();
		}

		output.setBookingId(ObjectUtils.firstNonNull(bookingId, output.getBookingId()));
		output.setHotel(ObjectUtils.firstNonNull(hInfo.getName(), output.getHotel()));
		output.setStatus(ObjectUtils.firstNonNull(output.getStatus(), HotelItemStatus.IN_PROGRESS.getStatus()));

		roomInfo.setTravellerInfo(ObjectUtils.firstNonNull(roomInfo.getTravellerInfo(), roomTravellerInfo));
		roomInfo.setRoomSSR(ObjectUtils.firstNonNull(roomInfo.getRoomSSR(), roomSSR));
		roomInfo.setCancellationPolicy(ObjectUtils.firstNonNull(roomInfo.getCancellationPolicy(),
				hInfo.getOptions().get(0).getCancellationPolicy()));
		roomInfo.setDeadlineDateTime(ObjectUtils.firstNonNull(roomInfo.getDeadlineDateTime(),
				hInfo.getOptions().get(0).getDeadlineDateTime()));
		roomInfo.setIsOptionOnRequest(hInfo.getOptions().get(0).getIsOptionOnRequest());
		if (!CollectionUtils.isEmpty(roomInfo.getTravellerInfo())) {
			if (roomInfo.getTotalFareComponents().get(HotelFareComponent.CGST) == null
					|| roomInfo.getTotalFareComponents().get(HotelFareComponent.IGST) == null)
				OmsHelper.updateGstComponent(roomInfo, bookingUser);
		}
		BaseHotelUtils.flattenFareComponents(roomInfo);
		String supplierId = hInfo.getOptions().get(0).getMiscInfo().getSupplierId();
		List<HotelOrderSupplierInfo> hotelOrderSupplierInfos = roomInfo.getHotelOrderSupplierInfos();
		roomInfo.setHotelOrderSupplierInfos(null);
		roomInfo.setDescription(null);
		roomInfo.setRoomAmenities(null);
		roomInfo.setRoomAdditionalInfo(null);
		output.setRoomInfo(ObjectUtils.firstNonNull(roomInfo, output.getRoomInfo()));
		output.setSupplierId(ObjectUtils.firstNonNull(supplierId, output.getSupplierId()));
		output.setRoomName(ObjectUtils.firstNonNull(roomInfo.getRoomType(), output.getRoomInfo().getRoomType()));
		output.setAmount(ObjectUtils.firstNonNull(updateAmountInOrderItem(), output.getAmount()));
		output.setCheckInDate(ObjectUtils.firstNonNull(roomInfo.getCheckInDate(), output.getCheckInDate()));
		output.setCheckOutDate(ObjectUtils.firstNonNull(roomInfo.getCheckOutDate(), output.getCheckOutDate()));
		output.setAdditionalInfo(getAdditionalHotelOrderItemInfo());
		output.getAdditionalInfo().setHotelOrderSupplierInfos(hotelOrderSupplierInfos);
		output.setMarkup(ObjectUtils.firstNonNull(roomInfo.getTotalFareComponents().get(HotelFareComponent.MU), 0.0));
		output.getAdditionalInfo().setSupplierBnplStatus(hInfo.getMiscInfo().getSupplierBnplStatus());
		output.getAdditionalInfo().setIsBnplBooking(hInfo.getMiscInfo().getIsBnplBooking());
		if (index == 0) {
			output.getAdditionalInfo()
					.setHInfo(HotelInfoToProcessedHotelInfoMapper.builder().hInfo(hInfo).build().convert());
		}
	}

	public AdditionalHotelOrderItemInfo getAdditionalHotelOrderItemInfo() {

		AdditionalHotelOrderItemInfoBuilder builder = AdditionalHotelOrderItemInfo.builder();
		AdditionalHotelOrderItemInfo additionalHotelInfo = output.getAdditionalInfo();

		if (additionalHotelInfo != null) {
			builder.hotelBookingReference(additionalHotelInfo.getHotelBookingReference());
			builder.supplierBookingId(additionalHotelInfo.getSupplierBookingId());
			builder.supplierBookingReference(additionalHotelInfo.getSupplierBookingReference());
			builder.supplierBookingConfirmationNo(additionalHotelInfo.getSupplierBookingConfirmationNo());
			builder.supplierId(additionalHotelInfo.getSupplierId());
			builder.bookingCancellationReference(additionalHotelInfo.getBookingCancellationReference());
			builder.supplierBookingUrl(additionalHotelInfo.getSupplierBookingUrl());
			builder.creditCardAppliedId(additionalHotelInfo.getCreditCardAppliedId());
			builder.isFailedFromSupplier(additionalHotelInfo.getIsFailedFromSupplier());
			builder.tempSupplierBookingId(additionalHotelInfo.getTempSupplierBookingId());
			builder.hotelOrderSupplierInfos(additionalHotelInfo.getHotelOrderSupplierInfos());
			builder.supplierBnplStatus(additionalHotelInfo.getSupplierBnplStatus());
			builder.isBnplBooking(additionalHotelInfo.getIsBnplBooking());
			builder.supplierBookingFailedReason(additionalHotelInfo.getSupplierBookingFailedReason());
			builder.alertType(additionalHotelInfo.getAlertType());
			builder.isQuarantinePackage(additionalHotelInfo.getIsQuarantinePackage());
		}

		if (hInfo != null) {
			HotelMiscInfo oldHMiscInfo = hInfo.getMiscInfo();
			if (oldHMiscInfo.getHotelBookingReference() != null) {
				builder.hotelBookingReference(oldHMiscInfo.getHotelBookingReference());
			}
			if (oldHMiscInfo.getSupplierBookingId() != null) {
				builder.supplierBookingId(oldHMiscInfo.getSupplierBookingId());
			}
			if (oldHMiscInfo.getSupplierBookingReference() != null) {
				builder.supplierBookingReference(oldHMiscInfo.getSupplierBookingReference());
			}
			if (StringUtils.isNotBlank(hInfo.getOptions().get(0).getMiscInfo().getSupplierId())) {
				builder.supplierId(hInfo.getOptions().get(0).getMiscInfo().getSupplierId());
			}
			if (StringUtils.isNotBlank(oldHMiscInfo.getHotelBookingCancellationReference())) {
				builder.bookingCancellationReference(oldHMiscInfo.getHotelBookingCancellationReference());
			}
			if (StringUtils.isNotBlank(oldHMiscInfo.getSupplierBookingConfirmationNo())) {
				builder.supplierBookingConfirmationNo(oldHMiscInfo.getSupplierBookingConfirmationNo());
			}
			if (StringUtils.isNotBlank(oldHMiscInfo.getSupplierBookingUrl())) {
				builder.supplierBookingUrl(oldHMiscInfo.getSupplierBookingUrl());
			}
			if (oldHMiscInfo.getCreditCardAppliedId() != null) {
				builder.creditCardAppliedId(oldHMiscInfo.getCreditCardAppliedId());
			}
			if (oldHMiscInfo.getIsFailedFromSupplier() != null) {
				builder.isFailedFromSupplier(oldHMiscInfo.getIsFailedFromSupplier());
			}
			if (oldHMiscInfo.getTempSupplierBookingId() != null) {
				builder.tempSupplierBookingId(oldHMiscInfo.getTempSupplierBookingId());
			}
			if (oldHMiscInfo.getSupplierBnplStatus() != null) {
				builder.supplierBnplStatus(oldHMiscInfo.getSupplierBnplStatus());
			}
			if (oldHMiscInfo.getIsBnplBooking() != null) {
				builder.isBnplBooking(oldHMiscInfo.getIsBnplBooking());
			}
			if (oldHMiscInfo.getSupplierBookingFailedReason() != null) {
				builder.supplierBookingFailedReason(oldHMiscInfo.getSupplierBookingFailedReason());
			}
			if (oldHMiscInfo.getAlertType() != null) {
				builder.alertType(oldHMiscInfo.getAlertType());
			}
			if (oldHMiscInfo.getIsQuarantinePackage() != null) {
				builder.isQuarantinePackage(oldHMiscInfo.getIsQuarantinePackage());
			}
		}

		return builder.build();

	}

	public Double updateAmountInOrderItem() {
		Double amount = roomInfo.getTotalFareComponents().get(HotelFareComponent.NF);
		return amount;
	}

}
