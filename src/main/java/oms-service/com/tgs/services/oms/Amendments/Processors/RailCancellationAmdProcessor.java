package com.tgs.services.oms.Amendments.Processors;

import org.springframework.stereotype.Service;

@Service
public class RailCancellationAmdProcessor extends RailAmendmentProcessor {

	@Override
	protected void processAmendment() {}

	@Override
	protected void validate() {}

}
