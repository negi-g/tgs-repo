package com.tgs.services.oms.servicehandler.hotel;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.hotel.HotelBookingInfoUpdateRequest;
import com.tgs.services.pms.datamodel.Payment;


@Service
public class HotelBookingInfoUpdateHandler extends ServiceHandler<HotelBookingInfoUpdateRequest, BaseResponse> {

	@Autowired
	HotelOrderItemManager itemManager;

	@Autowired
	OrderManager orderManager;

	@Autowired
	private HotelOrderMessageHandler messageHandler;

	@Autowired
	private PaymentServiceCommunicator paymentServiceCommunicator;


	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() throws Exception {


		Order order = orderManager.findByBookingId(request.getBookingId(), null);
		if (order == null) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
		List<DbHotelOrderItem> items = itemManager.getItemList(request.getBookingId());
		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(items).build().convert();
		if (hInfo.getMiscInfo() == null) {
			hInfo.setMiscInfo(HotelMiscInfo.builder().build());
		}
		HotelItemStatus itemStatus = null;
		if (request.getHotelBookingReference() != null) {
			OrderStatus orderStatus = order.getStatus();
			if (!(orderStatus.equals(OrderStatus.SUCCESS) 
					|| orderStatus.equals(OrderStatus.ON_HOLD))) {
				throw new CustomGeneralException(SystemError.HOTEL_BOOKING_REFERENCE_CANNOT_BE_UPDATED,
						"As Order is in " + orderStatus + " Status");
			}
			hInfo.getMiscInfo().setHotelBookingReference(request.getHotelBookingReference());
		}
		
		if (order.getStatus().equals(OrderStatus.PENDING)) {
			itemStatus = HotelItemStatus.ON_HOLD;
			hInfo.getMiscInfo().setSupplierBookingReference(request.getSupplierBookingId());
			hInfo.getMiscInfo().setSupplierBookingId(request.getSupplierBookingId());
			List<Payment> payments = paymentServiceCommunicator.fetchPaymentByBookingId(order.getBookingId());
			if (CollectionUtils.isNotEmpty(payments)) {
				itemStatus = HotelItemStatus.SUCCESS;
			}
		}
		itemManager.updateOrderItem(hInfo, order, itemStatus);
		if (request.getHotelBookingReference() != null) {
			items.get(0).getAdditionalInfo().setHotelBookingReference(request.getHotelBookingReference());
			messageHandler.sendBookingEmail(order, items, EmailTemplateKey.HOTEL_BOOKING_CONFIRMATION_EMAIL, null);
			messageHandler.sendBookingSms(order, items, SmsTemplateKey.HOTEL_BOOKING_CONFIRMATION_SMS, null);
		}
	}


	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
