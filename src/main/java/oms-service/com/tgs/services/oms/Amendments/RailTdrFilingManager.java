package com.tgs.services.oms.Amendments;

import static com.tgs.services.base.helper.SystemError.*;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.restmodel.rail.RailAmendmentDetailResponse;
import com.tgs.services.oms.restmodel.rail.RailAmendmentRequest;
import com.tgs.services.rail.datamodel.RailTDRReason;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailTdrFilingManager {

	@Autowired
	private OrderService orderService;

	@Autowired
	private RailOrderItemService railOrderItemService;

	@Autowired
	private RailCommunicator railCommunicator;

	@Autowired
	protected RailMessagingClient messagingClient;

	public void fileTdr(RailAmendmentRequest railAmendmentRequest, Amendment amendment, String transactionId) {
		try {
			boolean isSuccess = railCommunicator.submitTDRRequest(railAmendmentRequest.getTdrInfo(), transactionId,
					railAmendmentRequest.getPaxIds(), amendment.getBookingId());
			log.debug("TDR Filled for booking id {} with status {}", amendment.getBookingId(), isSuccess);
			if (isSuccess) {
				amendment.setStatus(AmendmentStatus.TDR_FILED);
				log.debug("Sending sms when tdr is filed for booking id {}", amendment.getBookingId());
				messagingClient.sendMail(amendment, Arrays.asList(EmailTemplateKey.RAIL_RAISE_TDR_EMAIL));
				messagingClient.sendSMS(amendment, SmsTemplateKey.RAIL_TDR_FILED_SMS);
			}
		} catch (Exception e) {
			log.error("Error Occured during fetching TDR reasons for bookingId {} due to {}",
					railAmendmentRequest.getBookingId(), e);
		}
	}

	public RailAmendmentDetailResponse getTdrReasons(RailAmendmentRequest railAmendmentRequest) {
		RailAmendmentDetailResponse amendmentResponse = RailAmendmentDetailResponse.builder().build();
		try {
			Order originalOrder = orderService.findByBookingId(railAmendmentRequest.getBookingId()).toDomain();
			if (originalOrder == null)
				throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
			UserServiceHelper.checkAndReturnAllowedUserId(
					SystemContextHolder.getContextData().getEmulateOrLoggedInUserId(),
					Arrays.asList(originalOrder.getBookingUserId()));

			DbRailOrderItem orderItem = railOrderItemService.findByBookingId(railAmendmentRequest.getBookingId());
			log.info("OrderItem found for bookingId {}", railAmendmentRequest.getBookingId());
			if (orderItem == null)
				throw new CustomGeneralException(ORDER_NOT_FOUND);
			String transactionId = orderItem.getAdditionalInfo().getReservationId();
			List<RailTDRReason> tdrReasons = railCommunicator.getReasonsList(transactionId,
					railAmendmentRequest.getBookingId());
			amendmentResponse.setTdrReasonList(tdrReasons);
		} catch (Exception e) {
			log.error("Error Occured during fetching TDR reasons for bookingId {} due to {}",
					railAmendmentRequest.getBookingId(), e);
		}
		return amendmentResponse;
	}

}
