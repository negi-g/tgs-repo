package com.tgs.services.oms.servicehandler.air;

import java.util.List;
import java.util.StringJoiner;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.oms.manager.air.AirManualBookingManager;
import com.tgs.services.oms.restmodel.air.AirManualBookingRequest;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirManualOrderHandler extends ServiceHandler<AirManualBookingRequest, BookingResponse> {

	@Autowired
	private AirManualBookingManager manualBookingManager;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	protected FMSCommunicator fmsCommunicator;

	@Override
	public void beforeProcess() throws Exception {
		if (request.getBookingId() == null) {
			throw new CustomGeneralException(SystemError.KEYS_EXPIRED);
		}
	}

	@Override
	public void process() throws Exception {
		Order order = null;
		StringJoiner exceptions = new StringJoiner("-");
		AirManualBookingRequest bookingRequest = request;
		try {
			if (request.isForReIssue()) {
				manualBookingManager.setTotalCommission(0d);
				manualBookingManager.setTotalTds(0d);
				manualBookingManager.setTotalMarkup(0d);
				manualBookingManager.setOrderTotal(0d);
				manualBookingManager.setPaymentTotal(0d);
			} else {
				bookingRequest = manualBookingManager.getBookingRequest(request);
			}

			OrderFlowType flowType = request.isForReIssue() ? OrderFlowType.REISSUED
					: ObjectUtils.firstNonNull(bookingRequest.getFlowType(), OrderFlowType.MANUAL_ORDER);

			if (bookingRequest != null) {
				bookingRequest.setType(OrderType.AIR);
				bookingRequest.setFlowType(flowType);
				manualBookingManager.setBookingUser(userService.getUserFromCache(bookingRequest.getUserId()));
				if (!bookingRequest.isForReIssue()) {
					updateGstComponents(manualBookingManager.getBookingUser(), bookingRequest.getAirOrderItems(),
							bookingRequest.getGstInfo());
				}
				manualBookingManager.setItems(manualBookingManager.getAirOrderItems(bookingRequest.getAirOrderItems()));
				manualBookingManager.setLoggedInUser(SystemContextHolder.getContextData().getUser());
				if (CollectionUtils.isNotEmpty(request.getPaymentInfos())) {
					for (PaymentRequest payment : request.getPaymentInfos()) {
						payment.setPayUserId(userService.getUserFromCache(bookingRequest.getUserId()).getUserId());
						payment.setRefId(request.getBookingId());
						payment.setProduct(Product.AIR);
					}
					bookingRequest.setPaymentInfos(request.getPaymentInfos());
				}
				manualBookingManager.setBookingRequest(bookingRequest);
				manualBookingManager.setPaxProfiles(bookingRequest.getTravellerProfiles());
				order = manualBookingManager.processBooking();
				log.info("Manual order created for booking id {} and order id {}", request.getBookingId(),
						order.getId());
			} else {
				throw new CustomGeneralException(SystemError.KEYS_EXPIRED);
			}
		} catch (Exception e) {
			exceptions.add(e.getMessage());
			CustomGeneralException customExcep = null;
			if (e instanceof CustomGeneralException) {
				customExcep = (CustomGeneralException) e;
			}

			if (customExcep != null && (customExcep.getError().equals(SystemError.INSUFFICIENT_BALANCE)
					|| customExcep.getError().equals(SystemError.USER_WALLET_BLOCKED_ACCESS_DENIED))) {
				log.info("Manual Order Booking Failed due to payment for bookingId {} and cause {}",
						request.getBookingId(), e.getMessage());
				throw e;
			} else {
				log.error("Manual Order Booking Failed for bookingId {} and cause ", request.getBookingId(), e);
				throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
			}
		} finally {
			List<SegmentInfo> segmentList =
					AirBookingUtils.convertAirOrderItemListToSegmentInfoList(manualBookingManager.getItems(), null);
			List<TripInfo> tripList = BookingUtils.createTripListFromSegmentList(segmentList);

			AirAnalyticsType analyticsType = AirAnalyticsType.MANUAL_ORDER;
			if (OrderFlowType.IMPORT_PNR.equals(bookingRequest.getFlowType())) {
				analyticsType = AirAnalyticsType.IMPORT_PNR;
			}
			fmsCommunicator.addBookToAnalytics(order, tripList, request.getPaymentInfos(), exceptions.toString(),
					analyticsType, null);
		}
	}

	@Override
	public void afterProcess() throws Exception {
		response.setBookingId(request.getBookingId());
	}

	private void updateGstComponents(User bookinguser, List<AirOrderItem> airOrderItems, GstInfo gstInfo) {
		for (AirOrderItem item : airOrderItems) {
			item.setBookingId(request.getBookingId());
			for (FlightTravellerInfo traveller : item.getTravellerInfo()) {
				if (traveller.getFareDetail() != null) {
					OmsHelper.updateGstComponents(traveller.getFareDetail(), bookinguser, gstInfo);
				}
			}
		}

	}
}
