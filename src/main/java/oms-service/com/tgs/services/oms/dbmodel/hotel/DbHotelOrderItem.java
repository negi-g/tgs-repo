package com.tgs.services.oms.dbmodel.hotel;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.hibernate.hotel.AdditionalHotelOrderItemInfoType;
import com.tgs.services.oms.hibernate.hotel.RoomInfoType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "HotelOrderItem")
@Table(name = "hotelorderitem")
@Builder
@Audited
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({ @TypeDef(name = "AdditionalHotelOrderItemInfoType", typeClass = AdditionalHotelOrderItemInfoType.class),
		@TypeDef(name = "RoomInfoType", typeClass = RoomInfoType.class) })
public class DbHotelOrderItem extends SuperBaseModel<DbHotelOrderItem, HotelOrderItem> {

	/**
	 * Whenever you are adding a column in this class, make sure to add it in
	 * corresponding data model class as well
	 */
	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hotel_generator")
	@SequenceGenerator(name = "hotel_generator", sequenceName = "hotelorderitem_id_seq", allocationSize = 1)
	private Long id;

	@Column
	private String bookingId;

	@Column
	private String hotel; // Hotel Name

	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	private LocalDate checkInDate;

	@Column
	private LocalDate checkOutDate;

	@Column
	private String supplierId;

	@Column
	private String roomName;

	/**
	 * Total Amount should be exclusive of markup Amount
	 */
	@Column
	private Double amount;

	@Column
	private Double markup;

	@Column
	private String status;

	@Column
	@Type(type = "AdditionalHotelOrderItemInfoType")
	private AdditionalHotelOrderItemInfo additionalInfo;

	@Column
	@Type(type = "RoomInfoType")
	private RoomInfo roomInfo;

	@Override
	public HotelOrderItem toDomain() {
		return new GsonMapper<>(this, HotelOrderItem.class).convert();
	}

	public DbHotelOrderItem from(HotelOrderItem dataModel) {
		return new GsonMapper<>(dataModel, new DbHotelOrderItem(), DbHotelOrderItem.class).convert();
	}

}
