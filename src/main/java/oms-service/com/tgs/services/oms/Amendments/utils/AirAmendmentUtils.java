package com.tgs.services.oms.Amendments.utils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.cancel.JourneyCancellationDetail;
import com.tgs.services.oms.datamodel.amendments.AmendmentTrip;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import org.apache.commons.collections4.CollectionUtils;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

public class AirAmendmentUtils {

	private static final String DELIMITER = "_";

	public static void updateAdditionalInfoFromOrgItems(AirOrderItem modifiedItem, List<DbAirOrderItem> orgItems) {
		DbAirOrderItem matchedItem = getMatchedItem(modifiedItem, orgItems);
		if (matchedItem != null) {
			modifiedItem.getAdditionalInfo().setStops(matchedItem.getAdditionalInfo().getStops());
			modifiedItem.getAdditionalInfo()
					.setDepartureTerminal(matchedItem.getAdditionalInfo().getDepartureTerminal());
			modifiedItem.getAdditionalInfo().setArrivalTerminal(matchedItem.getAdditionalInfo().getArrivalTerminal());
			modifiedItem.getAdditionalInfo().setOperatingAirline(matchedItem.getAdditionalInfo().getOperatingAirline());
			modifiedItem.getAdditionalInfo().setStopOverAirport(matchedItem.getAdditionalInfo().getStopOverAirport());
			modifiedItem.getAdditionalInfo().setSupplierRuleId(matchedItem.getAdditionalInfo().getSupplierRuleId());
			modifiedItem.getAdditionalInfo().setType(matchedItem.getAdditionalInfo().getType());
			modifiedItem.getAdditionalInfo().setPlatingCarrier(matchedItem.getAdditionalInfo().getPlatingCarrier());
			modifiedItem.getAdditionalInfo().setEquipType(matchedItem.getAdditionalInfo().getEquipType());
			modifiedItem.getAdditionalInfo().setOrgSupplierName(matchedItem.getAdditionalInfo().getOrgSupplierName());
		}
	}

	public static DbAirOrderItem getMatchedItem(AirOrderItem modifiedItem, List<DbAirOrderItem> orgItems) {
		if (CollectionUtils.isNotEmpty(orgItems) && modifiedItem != null) {
			List<DbAirOrderItem> matchedItem = orgItems.stream()
					.filter(item -> item.getId().equals(modifiedItem.getId())).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(matchedItem)) {
				return matchedItem.get(0);
			}
		}
		return null;
	}

	public static String createKey(String departureAirportCode, String arrivalAirportCode, String deptDate) {
		return StringUtils.join(departureAirportCode, DELIMITER, arrivalAirportCode, DELIMITER, deptDate);
	}
}
