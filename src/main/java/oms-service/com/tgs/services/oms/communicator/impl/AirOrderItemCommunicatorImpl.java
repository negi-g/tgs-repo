package com.tgs.services.oms.communicator.impl;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.air.AirOrderItemManager;

@Service
public class AirOrderItemCommunicatorImpl implements AirOrderItemCommunicator {

    @Autowired
    private AirOrderItemManager itemManager;

    @Autowired
    private AirOrderItemService service;

    @Override
    public List<SegmentInfo> updateOrderAndItem(List<SegmentInfo> segmentInfos, Order order, AirItemStatus itemStatus) {
        return itemManager.updateOrderAndItem(segmentInfos, order, itemStatus);
    }

    @Override
    public List<TripInfo> findTripByBookingId(String bookingId) {
        return itemManager.findTripsByBookingIds(Arrays.asList(bookingId)).get(bookingId);
    }
    
    @Override
    public HotelSearchQuery getHotelSearchQuery(String bookingId) {
    	List<TripInfo> tripInfoList = findTripByBookingId(bookingId);
		HotelSearchQuery searchQuery = BaseUtils.getHotelSearchQuery(tripInfoList, bookingId);
    	return searchQuery;
    }

    @Override
    public List<AirOrderItem> findItems(String bookingId) {
        return BaseModel.toDomainList(itemManager.findByBookingId(bookingId));
    }

    @Override
    public List<AirOrderItem> getAirOrderItems(List<String> bookingId) {
        return BaseModel.toDomainList(service.findByBookingId(bookingId));
    }

    @Override
    public List<SegmentInfo> checkAndUpdateOrderStatus(List<SegmentInfo> segmentInfos, Order order) {
       return itemManager.checkAndUpdateOrderStatus(segmentInfos,order);
    }
    
	@Override
	public Order getOrderByBookingId(String bookingId) {
		return itemManager.getOrderByBookingId(bookingId);
	}
}
