package com.tgs.services.oms.Amendments.Processors;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.datamodel.booking.AdditionalRailOrderItemInfo;

@Service
public class RailCorrectionProcessor extends RailAmendmentProcessor {

	@Override
	protected void validate() {
		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment)
				.contains(AmendmentAction.PROCESS))
			throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);

	}

	@Override
	protected void processAmendment() {
		Double differentialPayment = amendment.getAmount();
		amendment.getAdditionalInfo().setOrderDiffAmount(differentialPayment);
		amendment.setAmendmentAmount();
	}


	protected List<RailTravellerInfo> patchTravellerInfoList(List<RailTravellerInfo> modList,
			List<RailTravellerInfo> orgList) {
		if (CollectionUtils.isEmpty(modList))
			return orgList;
		modList.forEach(amdPax -> {
			List<RailTravellerInfo> matchList =
					orgList.stream().filter(ml -> ml.getId().equals(amdPax.getId())).collect(Collectors.toList());
			if (CollectionUtils.isEmpty(matchList))
				throw new RuntimeException(String.format(
						"RailOrderItem patching failed for amendmentId %s.Pax Id - \"%s\" not found in original order items.",
						amendment.getAmendmentId(), amdPax.getId()));
			RailTravellerInfo patchedTrav = matchList.get(0);
			orgList.remove(patchedTrav);
			patchedTrav = new GsonMapper<>(amdPax, patchedTrav, RailTravellerInfo.class).convert();
			orgList.add(patchedTrav);
		});
		return orgList;
	}


	protected AdditionalRailOrderItemInfo patchRailAdditionalInfo(AdditionalRailOrderItemInfo modAdditionalInfo,
			AdditionalRailOrderItemInfo orgAdditionalInfo) {
		RailPriceInfo orgPriceInfo = orgAdditionalInfo.getPriceInfo();
		RailPriceInfo modPriceInfo = modAdditionalInfo.getPriceInfo();
		if (modPriceInfo != null && !MapUtils.isEmpty(modPriceInfo.getFareComponents())) {
			for (Map.Entry<RailFareComponent, Double> entry : modPriceInfo.getFareComponents().entrySet()) {
				RailFareComponent key = entry.getKey();
				Double amendVal = entry.getValue();
				orgPriceInfo.getFareComponents().put(key, amendVal);
			}
		}
		orgAdditionalInfo.setPriceInfo(orgPriceInfo);
		return orgAdditionalInfo;
	}

}
