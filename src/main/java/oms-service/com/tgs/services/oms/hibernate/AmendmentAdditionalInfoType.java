package com.tgs.services.oms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.oms.datamodel.amendments.AmendmentAdditionalInfo;

public class AmendmentAdditionalInfoType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return AmendmentAdditionalInfo.class;
    }
}
