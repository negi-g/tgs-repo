package com.tgs.services.oms.manager.misc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.ProcessedBookingDetail;
import com.tgs.services.oms.datamodel.ProcessedItemDetails;
import com.tgs.services.oms.datamodel.ProcessedOrder;
import com.tgs.services.oms.datamodel.misc.MiscItemStatus;
import com.tgs.services.oms.datamodel.misc.ProcessedMiscDetails;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.misc.DbMiscOrderItem;
import com.tgs.services.oms.jparepository.misc.MiscOrderItemService;
import com.tgs.services.oms.manager.AbstractInvoiceIdManager;
import com.tgs.services.oms.manager.ItemManager;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.servicehandler.misc.MiscOrderActionValidator;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.ums.datamodel.RailApplicationStatus;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MiscOrderItemManager implements ItemManager {

	@Autowired
	private OrderManager orderManager;

	@Autowired
	protected MiscOrderItemService itemService;

	@Autowired
	protected MiscOrderActionValidator actionValidator;

	@Autowired
	private AbstractInvoiceIdManager invoiceIdManager;

	@Autowired
	private UserServiceCommunicator userService;

	public List<ProcessedBookingDetail> getProcessedItemDetails(OrderFilter orderFilter) {

		List<Object[]> orderList = findInfoByOrderFilter(orderFilter);
		if (orderList == null || orderList.size() == 0) {
			return new ArrayList<>();
		}

		List<ProcessedOrder> orders = new ArrayList<>();
		List<ProcessedBookingDetail> pbds = new ArrayList<>();
		Map<String, DbMiscOrderItem> itemsMap = new HashMap<>();
		for (Object[] object : orderList) {
			DbOrder order = (DbOrder) object[1];
			itemsMap.put(order.getBookingId(), (DbMiscOrderItem) object[0]);
			ProcessedOrder pOrder = new GsonMapper<>(order, ProcessedOrder.class).convert();
			orders.add(pOrder);
		}

		for (ProcessedOrder pOrder : orders) {
			DbMiscOrderItem dbOrderItem = itemsMap.get(pOrder.getBookingId());
			pbds.add(ProcessedBookingDetail.builder().order(pOrder).items(getProcessedItemMap(dbOrderItem)).build());
		}

		updateActionList(pbds);
		return pbds;
	}


	public List<Object[]> findInfoByOrderFilter(OrderFilter filter) {
		return itemService.findByJsonSearch(filter);
	}


	public Map<String, ProcessedItemDetails> getProcessedItemMap(DbMiscOrderItem item) {
		ProcessedItemDetails processedItemDetails = ProcessedMiscDetails.builder()
				.additionalInfo(item.getAdditionalInfo()).priceInfo(item.getPriceInfo()).build();
		Map<String, ProcessedItemDetails> map = new HashMap<>();
		map.put(OrderType.MISC.getName(), processedItemDetails);
		return map;

	}


	public void updateActionList(List<ProcessedBookingDetail> pbds) {
		pbds.forEach(pbd -> {
			try {
				if (pbd.getOrder() != null) {
					pbd.getOrder()
							.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(),
									Order.builder().status(pbd.getOrder().getStatus())
											.additionalInfo(pbd.getOrder().getAdditionalInfo()).build()));
				}
			} catch (Exception e) {
				log.error("Unable to parse booking information for bookingId {}", pbd.getOrder().getBookingId(), e);
			}
		});
	}


	@Override
	public void processBooking(String bookingId, Order order, PaymentStatus paymentStatus, double paymentFee) {
		log.info("Inside MiscOrderItem Manager for bookingId {}", bookingId);
		DbMiscOrderItem miscOrderitem = itemService.findByBookingId(bookingId);
		MiscItemStatus itemStatus;
		if (PaymentStatus.SUCCESS.equals(paymentStatus)) {
			log.info("Processing MisOrder After PaymentSuccess for bookingId {}", bookingId);
			itemStatus = MiscItemStatus.SUCCESS;
			order.setStatus(OrderStatus.SUCCESS);
			invoiceIdManager.setInvoiceIdFor(order, Product.getProductMetaInfoFromId(order.getBookingId()));
			if (miscOrderitem.getAdditionalInfo() != null
					&& StringUtils.isNotEmpty(miscOrderitem.getAdditionalInfo().getDescription())
					&& miscOrderitem.getAdditionalInfo().getDescription().contains("Rail")) {

				User bookingUser =
						order.getBookingUserId() != null ? userService.getUserFromCache(order.getBookingUserId())
								: SystemContextHolder.getContextData().getUser();
				log.info(
						"Updating payment status and Application status for Rail Onboarding for bookingId {}, userId {}",
						bookingId, bookingUser.getUserId());
				bookingUser.getRailAdditionalInfo().setIsPaymentSuccess(true);
				bookingUser.getRailAdditionalInfo().setApplicationStatus(RailApplicationStatus.PAID);
				userService.updateUser(bookingUser);
			}
		} else {
			itemStatus = MiscItemStatus.PAYMENT_FAILED;
			order.setReason(SystemError.PAYMENT_FAILED.getMessage());
			order.setStatus(OrderStatus.FAILED);
		}
		miscOrderitem.setStatus(itemStatus.getCode());
		itemService.save(miscOrderitem);
		order.getAdditionalInfo().setPaymentStatus(paymentStatus);
		order = orderManager.save(order);


	}
}
