package com.tgs.services.oms.jparepository.rail;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;

public interface RailOrderItemRepository
		extends JpaRepository<DbRailOrderItem, Long>, JpaSpecificationExecutor<DbRailOrderItem> {

	List<DbRailOrderItem> findByBookingId(String bookingId);
	List<DbRailOrderItem> findByPnr(String pnr);

	@Modifying(clearAutomatically = true)
	@Query(value = "delete from railorderitem where bookingid = :bookingId and (select count(*) from orders where bookingid = :bookingId) = 0",
			nativeQuery = true)
	void deleteItemsWithNoOrderByBookingId(@Param("bookingId") String bookingId);

	@Query(value = "SELECT nextval('atlas_rail_invoice_id_seq')", nativeQuery = true)
	Integer getNextInvoiceId();
}
