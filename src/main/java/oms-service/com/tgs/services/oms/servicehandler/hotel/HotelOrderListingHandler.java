package com.tgs.services.oms.servicehandler.hotel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.ProcessedBookingDetail;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.OrderResponse;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelOrderListingHandler extends ServiceHandler<OrderFilter, OrderResponse>{
	
	@Autowired
	HotelOrderItemManager itemManager;
	
	
	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws Exception {
		
		if (request.getPageAttr() == null) {
			request.setPageAttr(new PageAttributes());
			request.getPageAttr().setSize(10000);
		}
		
		List<ProcessedBookingDetail> pbds = itemManager.getProcessedItemDetails(request);
		response.setDetails(pbds);
		
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	
}
