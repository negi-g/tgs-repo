package com.tgs.services.oms.mapper;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.apache.commons.lang3.ObjectUtils;
import com.google.common.base.Joiner;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.ims.datamodel.InventoryOrder;
import com.tgs.services.ims.datamodel.air.AirInventoryOrderInfo;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Builder
@Getter
@Setter
@Slf4j
public class AirOrderItemToInventoryOrderUpdateMapper extends Mapper<InventoryOrder> {

	private List<AirOrderItem> airOrderItems;
	private InventoryOrder inventoryOrder;


	@Override
	protected void execute() throws CustomGeneralException {
		output = inventoryOrder;
		AirOrderItem srcAirOrder = airOrderItems.get(0);
		AirOrderItem destAirOrder = airOrderItems.get(airOrderItems.size() - 1);
		Set<String> flightNumbers = airOrderItems.stream().filter(s -> s.getFlightNumber() != null)
				.map(s -> s.getFlightNumber()).collect(Collectors.toSet());
		log.info("Inventory Update requested for Reference Id {}", inventoryOrder.getReferenceId());
		List<FlightTravellerInfo> airOrderTravellers = srcAirOrder.getTravellerInfo().stream()
				.sorted(Comparator.comparingLong(FlightTravellerInfo::getId)).collect(Collectors.toList());

		AirInventoryOrderInfo invAddtitionalInfo = AirInventoryOrderInfo.builder()
				.pnr(airOrderTravellers.get(0).getPnr()).source(srcAirOrder.getSource()).dest(destAirOrder.getDest())
				.flightNumbers(Joiner.on(",").join(flightNumbers)).airline(srcAirOrder.getAirlinecode()).build();
		output.setAdditionalInfo(invAddtitionalInfo);

		List<FlightTravellerInfo> inventoryOrderTravellers = output.getTravellerInfo();
		for (FlightTravellerInfo airTraveller : airOrderTravellers) {
			for (FlightTravellerInfo invTraveller : inventoryOrderTravellers) {
				if (Objects.equals(airTraveller.getId(), invTraveller.getId())) {
					invTraveller.setFirstName(airTraveller.getFirstName());
					invTraveller.setLastName(airTraveller.getLastName());
					invTraveller.setTitle(airTraveller.getTitle());
					invTraveller.setPaxType(airTraveller.getPaxType());
					invTraveller.setPnr(airTraveller.getPnr());
					invTraveller.setStatus(airTraveller.getStatus());

				}
			}
		}


	}

}

