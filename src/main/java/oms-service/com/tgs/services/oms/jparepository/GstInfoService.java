package com.tgs.services.oms.jparepository;

import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.dbmodel.BaseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.oms.dbmodel.DbOrderBilling;

import java.util.List;

@Service
public class GstInfoService {

	@Autowired
	private GstInfoRepository gstRepo;

	public DbOrderBilling save(DbOrderBilling gstInfo) {
		return gstRepo.saveAndFlush(gstInfo);
	}

	public DbOrderBilling findByBookingId(String bookingId) {
		return gstRepo.findByBookingId(bookingId);
	}

	public List<GstInfo> findByBookingIdIn(List<String> bookingIds) {
		List<DbOrderBilling> db = gstRepo.findByBookingIdIn(bookingIds);
		return BaseModel.toDomainList(db);
	}

}
