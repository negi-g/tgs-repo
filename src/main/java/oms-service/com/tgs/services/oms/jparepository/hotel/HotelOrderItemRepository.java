package com.tgs.services.oms.jparepository.hotel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;

@Repository
public interface HotelOrderItemRepository
		extends JpaRepository<DbHotelOrderItem, Long>, JpaSpecificationExecutor<DbHotelOrderItem> {

	List<DbHotelOrderItem> findByBookingId(String bookingId);

	List<DbHotelOrderItem> findByBookingIdOrderByIdAsc(String bookingId);

	List<DbHotelOrderItem> findByBookingIdIn(List<String> bookingIds);

	@Modifying(clearAutomatically = true)
	@Query(value = "delete from hotelorderitem where bookingid = :bookingId and (select count(*) from orders where bookingid = :bookingId) = 0", nativeQuery = true)
	void deleteItemsWithNoOrderByBookingId(@Param("bookingId") String bookingId);
	
	@Query(value = "SELECT nextval('atlas_hotel_invoice_id_seq')", nativeQuery = true)
	Integer getNextInvoiceId();

}
