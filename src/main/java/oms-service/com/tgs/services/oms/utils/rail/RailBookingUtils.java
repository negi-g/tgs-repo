package com.tgs.services.oms.utils.rail;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentTransactionType;

public class RailBookingUtils {

	private static GeneralCachingCommunicator cachingService;

	public static void init(GeneralCachingCommunicator cachingService) {
		RailBookingUtils.cachingService = cachingService;
	}

	public static void saveInCache(String bookingId, String key, Integer ttl) {
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.BOOKINGID.getName(), bookingId);
		cachingService.store(CacheMetaInfo.builder().namespace(CacheNameSpace.RAIL.getName())
				.set(CacheSetName.RAIL_DUP.getName()).key(key).build(), binMap, false, true, ttl);
	}

	public static String getStoredBookingId(String key) {
		Map<String, String> readBinMap =
				cachingService.get(
						CacheMetaInfo.builder().namespace(CacheNameSpace.RAIL.getName())
								.set(CacheSetName.RAIL_DUP.getName()).key(key).build(),
						String.class, false, true, BinName.BOOKINGID.getName());
		return readBinMap.get(BinName.BOOKINGID.getName());
	}

	public static void deleteKeyFromCache(String key) {
		cachingService.delete(CacheMetaInfo.builder().namespace(CacheNameSpace.RAIL.getName())
				.set(CacheSetName.RAIL_DUP.getName()).key(key).build());
	}

	public static PaymentRequest createRefundPayments(Payment p, PaymentTransactionType transactionType,
			BigDecimal amount) {
		return PaymentRequest.builder().build().setPayUserId(p.getPayUserId()).setProduct(p.getProduct())
				.setOpType(PaymentOpType.CREDIT).setRefId(p.getRefId()).setOriginalPaymentRefId(p.getPaymentRefId())
				.setAmount(amount.abs()).setPaymentMedium(p.getPaymentMedium()).setTransactionType(transactionType);
	}

	public static PaymentRequest createAPHPayments(Payment payment, PaymentTransactionType transactionType,
			BigDecimal amount) {
		return PaymentRequest.builder().build().setPayUserId(payment.getPayUserId()).setProduct(payment.getProduct())
				.setOpType(PaymentOpType.DEBIT).setRefId(payment.getRefId())
				.setOriginalPaymentRefId(payment.getPaymentRefId()).setAmount(amount.abs()).setTds(payment.getTds())
				.setTransactionType(transactionType);
	}

	public static PaymentRequest createFundHandlerRequests(Payment payment, PaymentTransactionType transactionType,
			BigDecimal amount) {
		return PaymentRequest.builder().build().setPayUserId(payment.getPayUserId()).setProduct(payment.getProduct())
				.setOpType(PaymentOpType.CREDIT).setRefId(payment.getRefId())
				.setOriginalPaymentRefId(payment.getPaymentRefId()).setAmount(amount.abs())
				.setTransactionType(transactionType);
	}
}
