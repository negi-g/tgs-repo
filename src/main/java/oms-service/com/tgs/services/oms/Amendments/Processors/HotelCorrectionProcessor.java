package com.tgs.services.oms.Amendments.Processors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelCorrectionProcessor extends HotelAmendmentProcessor {

	@Override
	protected void validate() {

		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment)
				.contains(AmendmentAction.PROCESS))
			throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);
		manualOrderManager.updateNewHotelInfoInRequestForModifyOrder(hotelOrderRequest);

	}

	@Override
	protected void processAmendment() {
		HotelOrder modifiedHotelOrder = getModifiedHotelOrder();
		HotelInfo modifiedHotelInfoWithDeleted = DbHotelOrderItemListToHotelInfo.builder()
				.itemList(new DbHotelOrderItem().toDbList(modifiedHotelOrder.getItems())).build().convert();
		log.debug("Modified hotelinfo with deleted rooms for {} is {}", hotelOrderRequest.getBookingId(),
				GsonUtils.getGson().toJson(modifiedHotelInfoWithDeleted));

		amendmentManager.updateCommonAmounts(modifiedHotelOrder.getItems(), amendment);

		Double differentialPayment = getDifferentialPayment(amendment, modifiedHotelInfoWithDeleted);
		amendment.getAdditionalInfo().setOrderDiffAmount(differentialPayment);
		amendment.setAmendmentAmount();
		updateModifiedHotelInfoInAmendment(amendment, modifiedHotelInfoWithDeleted);
		if (StringUtils.isNotBlank(requestNote))
			updateNote(amendment);

	}

}
