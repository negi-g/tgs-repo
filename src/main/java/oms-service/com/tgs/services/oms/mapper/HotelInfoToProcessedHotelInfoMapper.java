package com.tgs.services.oms.mapper;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.ProcessedHotelInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class HotelInfoToProcessedHotelInfoMapper extends Mapper<ProcessedHotelInfo> {

	private HotelInfo hInfo;

	@Override
	protected void execute() throws CustomGeneralException {
		output = new GsonMapper<>(hInfo, ProcessedHotelInfo.class).convert();
	}

}
