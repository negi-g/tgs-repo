package com.tgs.services.oms.restcontroller;

import com.tgs.services.base.datamodel.ContactInfoValidatingData;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.AirBookingConditions;
import com.tgs.services.oms.restmodel.BookingRequest;

@Component
public abstract class BookingRequestValidator extends TgsValidator implements Validator {

	@Override
	public void validate(Object target, Errors errors) {

		if (!(target instanceof BookingRequest)) {
			return;
		}

		BookingRequest bookingRequest = (BookingRequest) target;

		AirBookingConditions conditions = getBookingConditions(bookingRequest);

		if (bookingRequest.getDeliveryInfo() == null) {
			rejectValue(errors, "deliveryInfo", SystemError.NULL_DELIVERY_INFO);
		} else {
			registerErrors(errors, "deliveryInfo", bookingRequest.getDeliveryInfo());
		}

		if (conditions != null && BooleanUtils.isTrue(conditions.getIsEmergencyContactReq())) {
			if (bookingRequest.getContactInfo() == null) {
				rejectValue(errors, "contactInfo", SystemError.NULL_CONTACT_INFO);
			} else {
				ContactInfoValidatingData validatingData = ContactInfoValidatingData.builder()
						.isEmergencyContactNameReq(true).isContactsReq(true).isEmailsReq(true).build();
				registerErrors(errors, "contactInfo", bookingRequest.getContactInfo(), validatingData);
			}
		}

		if (conditions != null && conditions.getGstInfo() != null
				&& BooleanUtils.isTrue(conditions.getGstInfo().getIsGSTMandatory())) {
			if (bookingRequest.getGstInfo() == null
					|| StringUtils.isBlank(bookingRequest.getGstInfo().getGstNumber())) {
				rejectValue(errors, "gstInfo", SystemError.NULL_GST_INFO);
			}
		}

		if (bookingRequest.getGstInfo() != null && StringUtils.isNotBlank(bookingRequest.getGstInfo().getGstNumber())) {
			registerErrors(errors, "gstInfo", bookingRequest.getGstInfo());
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}

	public abstract AirBookingConditions getBookingConditions(BookingRequest bookingRequest);
}
