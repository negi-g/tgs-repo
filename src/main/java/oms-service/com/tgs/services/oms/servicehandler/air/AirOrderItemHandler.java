package com.tgs.services.oms.servicehandler.air;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.InventoryOrderFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.CorporateTripServiceCommunicator;
import com.tgs.services.base.communicator.DealInventoryCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.ims.datamodel.InventoryOrder;
import com.tgs.services.ims.datamodel.air.AirInventoryOrderInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.air.AirOrderItemManager;
import com.tgs.services.oms.mapper.AirOrderItemToInventoryOrderUpdateMapper;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirOrderItemHandler extends ServiceHandler<List<AirOrderItem>, BaseResponse> {

	@Autowired
	AirOrderItemManager itemManager;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private DealInventoryCommunicator invCommunicator;

	@Autowired
	private CorporateTripServiceCommunicator corpTripCommunicator;


	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		User user = SystemContextHolder.getContextData().getUser();
		Order order = null;
		String bookingId = null;
		try {
			for (AirOrderItem airOrderItem : request) {
				if (airOrderItem.getId() != null) {
					DbAirOrderItem dbAirOrderItem = itemManager.findByAirOrderItemId(airOrderItem.getId());
					bookingId = dbAirOrderItem.getBookingId();
					log.info("Updating order and item details for bookingId {}", dbAirOrderItem.getBookingId());
					order = orderManager.findByBookingId(dbAirOrderItem.getBookingId(), null);

					/**
					 * Post successful order if staff want to change any detail without any assignment , they will be
					 * allow to do that. For example : SupplierId change post success status
					 */
					if (!user.getUserId().equals(order.getAdditionalInfo().getAssignedUserId())
							&& !order.getStatus().equals(OrderStatus.SUCCESS)) {
						throw new CustomGeneralException(SystemError.DIFFERENT_ASSIGNED_USER);
					}

					if (airOrderItem.getAirlinecode() != null) {
						airOrderItem.setAirlinecode(airOrderItem.getAirlinecode().toUpperCase());
						AirlineInfo airlineInfo = fmsCommunicator.getAirlineInfo(airOrderItem.getAirlinecode());
						if (airlineInfo == null) {
							throw new CustomGeneralException(SystemError.INVALID_AIRLINE_CODE,
									airOrderItem.getAirlinecode());
						}
					}


					if (StringUtils.isNotBlank(order.getAdditionalInfo().getTripId()))
						updateStatusInCorporateTrip(order);

					if (Objects.nonNull(airOrderItem.getAdditionalInfo())
							&& Objects.nonNull(airOrderItem.getAdditionalInfo().getTimeLimit())) {

						if (dbAirOrderItem.getDepartureTime()
								.isBefore(airOrderItem.getAdditionalInfo().getTimeLimit())) {
							throw new CustomGeneralException(SystemError.TIME_LIMIT_EXTENSION_FAILURE);
						}
						dbAirOrderItem.getAdditionalInfo()
								.setTimeLimit(airOrderItem.getAdditionalInfo().getTimeLimit());
					}

					updateTravellerData(dbAirOrderItem, airOrderItem);
					updateSupplierId(dbAirOrderItem, airOrderItem);
					updateAirlineAndFlightNumber(dbAirOrderItem, airOrderItem);
					itemManager.save(dbAirOrderItem, order,
							itemManager.getAirItemStatus(dbAirOrderItem.toDomain(), order));
					log.info("Updation for order and item details completed for bookingId {}", order.getBookingId());
				}


			}
			List<DbAirOrderItem> dbModifiedAirOrderItems = itemManager.findByBookingId(order.getBookingId());
			List<AirOrderItem> modifiedAirOrderItems = BaseModel.toDomainList(dbModifiedAirOrderItems);
			itemManager.save(dbModifiedAirOrderItems, order, null);
			updateInventoryOrder(modifiedAirOrderItems);
		} finally {
			SystemContextHolder.getContextData().getReqIds().add(bookingId);
		}
	}


	private void updateStatusInCorporateTrip(Order order) {
		String status = OrderStatus.SUCCESS.equals(order.getStatus()) ? "Booked" : "In_Progress";
		corpTripCommunicator.linkBookingIdWithTrip(order.getAdditionalInfo().getTripId(), order.getBookingId(), status);

	}

	@Deprecated
	private void updatePNRInInventoryOrder(String bookingId, List<FlightTravellerInfo> travellersList,
			String inventoryId) {
		InventoryOrderFilter filter = InventoryOrderFilter.builder().referenceIdIn(Arrays.asList(bookingId))
				.inventoryIdIn(Arrays.asList(inventoryId)).build();
		List<InventoryOrder> inventories = invCommunicator.fetchInventoryOrders(filter);
		if (CollectionUtils.isNotEmpty(inventories)) {
			InventoryOrder invOrder = inventories.get(0);
			updateTravellerPNR(invOrder, travellersList);
			((AirInventoryOrderInfo) invOrder.getAdditionalInfo()).setPnr(travellersList.get(0).getPnr());
			invCommunicator.saveInventoryOrder(invOrder);
		}
	}

	private void updateInventoryOrder(List<AirOrderItem> airOrderItems) {
		List<AirOrderItem> airOrdersWithInvId = airOrderItems.stream()
				.filter(airOrderItem -> StringUtils.isNotBlank(airOrderItem.getAdditionalInfo().getInventoryId()))
				.collect(Collectors.toList());
		if (CollectionUtils.isEmpty(airOrdersWithInvId))
			return;
		String bookingId = airOrdersWithInvId.get(0).getBookingId();
		InventoryOrderFilter filter = InventoryOrderFilter.builder().referenceIdIn(Arrays.asList(bookingId)).build();
		List<InventoryOrder> inventoryOrders = invCommunicator.fetchInventoryOrders(filter);
		for (InventoryOrder invOrder : inventoryOrders) {
			List<AirOrderItem> matchingAirOrderItems = airOrdersWithInvId
					.stream().filter(airOrderItem -> StringUtils
							.equals(airOrderItem.getAdditionalInfo().getInventoryId(), invOrder.getInventoryId()))
					.collect(Collectors.toList());
			InventoryOrder updatedInventoryOrder = AirOrderItemToInventoryOrderUpdateMapper.builder()
					.airOrderItems(matchingAirOrderItems).inventoryOrder(invOrder).build().convert();
			invCommunicator.saveInventoryOrder(updatedInventoryOrder);
		}

	}


	private void updateAirlineAndFlightNumber(DbAirOrderItem dbAirOrderItem, AirOrderItem airOrderItem) {
		if (StringUtils.isNotBlank(airOrderItem.getAirlinecode())) {
			if (!airOrderItem.getAirlinecode().equals(dbAirOrderItem.getAirlinecode())
					|| !dbAirOrderItem.getFlightNumber().equals(airOrderItem.getFlightNumber())) {

				dbAirOrderItem.setAirlinecode(airOrderItem.getAirlinecode());
				dbAirOrderItem.setFlightNumber(airOrderItem.getFlightNumber());

				if (StringUtils.isBlank(airOrderItem.getFlightNumber()))
					throw new CustomGeneralException(SystemError.INVALID_FLIGHTNUMBER);
			}
		}
	}


	private void updateSupplierId(DbAirOrderItem dbAirOrderItem, AirOrderItem airOrderItem) {
		if (StringUtils.isNotBlank(airOrderItem.getSupplierId())) {
			if (!airOrderItem.getSupplierId().equals(dbAirOrderItem.getSupplierId())) {
				if (fmsCommunicator.getSupplierInfo(airOrderItem.getSupplierId()) == null)
					throw new CustomGeneralException(SystemError.INVALID_FBRC_SUPPLIERID, airOrderItem.getSupplierId());
				if (dbAirOrderItem.getAdditionalInfo().getOrgSupplierName() == null)
					dbAirOrderItem.getAdditionalInfo().setOrgSupplierName(dbAirOrderItem.getSupplierId());
				dbAirOrderItem.setSupplierId(airOrderItem.getSupplierId());

			}
		}
	}

	private DbAirOrderItem updateTravellerData(DbAirOrderItem dbAirOrderItem, AirOrderItem airOrderItem) {
		List<FlightTravellerInfo> dbTravellers = dbAirOrderItem.getTravellerInfo();
		airOrderItem.getTravellerInfo().forEach(dmTravellerInfo -> {
			FlightTravellerInfo travellerInfo1 = dbTravellers.stream()
					.filter(travellerInfo2 -> dmTravellerInfo.getId().equals(travellerInfo2.getId())).findFirst().get();
			new GsonMapper<>(dmTravellerInfo, travellerInfo1, FlightTravellerInfo.class).convert();
		});
		return dbAirOrderItem;
	}

	@Override
	public void afterProcess() throws Exception {

	}

	private void updateTravellerPNR(InventoryOrder invOrder, List<FlightTravellerInfo> travellersList) {
		invOrder.getTravellerInfo().forEach(traveller -> {
			travellersList.forEach(newTravellerList -> {
				if (traveller.getId().equals(newTravellerList.getId())) {
					traveller.setPnr(newTravellerList.getPnr());
				}
			});
		});
	}
}
