package com.tgs.services.oms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.base.gson.DoubleToLongAdapterFactory;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAdditionalInfo;
import com.tgs.services.oms.datamodel.amendments.ModifiedInfo;
import com.tgs.services.oms.hibernate.AmendmentAdditionalInfoType;
import com.tgs.services.oms.hibernate.ModifiedInfoType;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "amendment")
@Table(name = "amendment")
@TypeDefs({@TypeDef(name = "AmendmentAdditionalInfoType", typeClass = AmendmentAdditionalInfoType.class),
		@TypeDef(name = "ModifiedInfoType", typeClass = ModifiedInfoType.class)})
@Getter
@Setter
@Audited
public class DbAmendment extends SuperBaseModel<DbAmendment, Amendment> {

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "amendment_generator")
	@SequenceGenerator(name = "amendment_generator", sequenceName = "amendment_id_seq", allocationSize = 1)
	private Long id;
	@Column(updatable = false)
	private String amendmentId;

	@Column(updatable = false)
	private String bookingId;

	@Column(updatable = false)
	private String amendmentType;

	@Column
	private String status;

	@Column
	@JsonAdapter(DoubleToLongAdapterFactory.class)
	private Long amount;

	@Column
	@Type(type = "AmendmentAdditionalInfoType")
	private AmendmentAdditionalInfo additionalInfo;

	@Column
	@Type(type = "ModifiedInfoType")
	private ModifiedInfo modifiedInfo;

	@Column(updatable = false)
	private String loggedInUserId;

	@Column
	private String bookingUserId;

	@Column
	private String partnerId;

	@Column
	private String assignedUserId;

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	private LocalDateTime assignedOn;

	@Column
	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private String orderType;

	@Column
	private String channelType;

	public static DbAmendment create(Amendment amendment) {
		return new DbAmendment().from(amendment);
	}

	@Override
	public Amendment toDomain() {
		return new GsonMapper<>(this, Amendment.class).convert();
	}

	@Override
	public DbAmendment from(Amendment amendment) {
		return new GsonMapper<>(amendment, this, DbAmendment.class).convert();
	}

}
