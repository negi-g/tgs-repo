package com.tgs.services.oms.jparepository.rail;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.base.Joiner;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.rail.AdditionalInfoFilter;
import com.tgs.services.oms.datamodel.rail.RailOrderItemFilter;
import com.tgs.services.oms.datamodel.rail.TravellerInfoFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailOrderItemService {

	@Autowired
	protected RailOrderItemRepository railItemRepository;

	@PersistenceContext
	private EntityManager em;

	public DbRailOrderItem save(DbRailOrderItem item) {
		setTravellerId(item);
		return railItemRepository.saveAndFlush(item);
	}

	private void setTravellerId(DbRailOrderItem item) {
		long counter = 1;
		for (RailTravellerInfo travellerInfo : item.getTravellerInfo()) {
			if (travellerInfo.getId() != null)
				return;
			travellerInfo.setId(counter++);
		}

	}

	public DbRailOrderItem findByBookingId(String bookingId) {
		List<DbRailOrderItem> dbOrderItems = railItemRepository.findByBookingId(bookingId);
		return dbOrderItems.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findByJsonSearch(OrderFilter orderFilter) {

		List<Object[]> results = new ArrayList<>();

		for (int i = 0; i < 20; i++) {

			StringBuilder query = new StringBuilder(
					"SELECT {a.*} , {b.*} from railorderitem a JOIN orders b " + "ON a.bookingId = b.bookingId ");
			List<String> queryParams = new ArrayList<>();
			if (orderFilter != null) {
				if (CollectionUtils.isNotEmpty(orderFilter.getBookingUserIds())) {
					queryParams.add("b.bookinguserid IN('"
							.concat(Joiner.on("','").join(orderFilter.getBookingUserIds())).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getLoggedInUserIds())) {
					queryParams.add("b.loggedinuserid IN('"
							.concat(Joiner.on("','").join(orderFilter.getLoggedInUserIds())).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getBookingIds())) {
					queryParams.add(
							"b.bookingId IN('".concat(Joiner.on("','").join(orderFilter.getBookingIds())).concat("')"));
				}
				if (StringUtils.isNotEmpty(orderFilter.getBookingId())) {
					queryParams.add("b.bookingId = '".concat(orderFilter.getBookingId().concat("'")));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getStatuses())) {
					List<String> statusCodes = OrderStatus.getCodes(orderFilter.getStatuses());
					queryParams.add("b.status IN ('".concat(Joiner.on("','").join(statusCodes)).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getChannels())) {
					List<String> channelCodes = ChannelType.getCodes(orderFilter.getChannels());
					queryParams.add("b.channeltype IN ('".concat(Joiner.on("','").join(channelCodes)).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getProducts())) {
					List<String> orderCodes = OrderType.getCodes(orderFilter.getProducts());
					queryParams.add("b.ordertype IN ('".concat(Joiner.on("','").join(orderCodes)).concat("')"));
				}
				if (orderFilter.getCreatedOnAfterDateTime() != null) {
					queryParams.add(
							"b.createdOn >= '".concat(orderFilter.getCreatedOnAfterDateTime().toString()).concat("'"));
				}
				if (orderFilter.getCreatedOnBeforeDateTime() != null) {
					queryParams.add(
							"b.createdOn <= '".concat(orderFilter.getCreatedOnBeforeDateTime().toString()).concat("'"));
				}
				if (orderFilter.getCreatedOnAfterDate() != null) {
					queryParams
							.add("b.createdOn >= '".concat(orderFilter.getCreatedOnAfterDate().toString()).concat("'"));
				}
				if (orderFilter.getCreatedOnBeforeDate() != null) {
					queryParams.add(
							"b.createdOn <= '".concat(orderFilter.getCreatedOnBeforeDate().toString()).concat("'"));
				}
				if (orderFilter.getProcessedOnAfterDateTime() != null) {
					queryParams.add("b.processedOn >= '".concat(orderFilter.getProcessedOnAfterDateTime().toString())
							.concat("'"));
				}
				if (orderFilter.getProcessedOnBeforeDateTime() != null) {
					queryParams.add("b.processedOn <= '".concat(orderFilter.getProcessedOnBeforeDateTime().toString())
							.concat("'"));
				}
				if (orderFilter.getProcessedOnAfterDate() != null) {
					queryParams.add(
							"b.processedOn >= '".concat(orderFilter.getProcessedOnAfterDate().toString()).concat("'"));
				}
				if (orderFilter.getProcessedOnBeforeDate() != null) {
					queryParams.add(
							"b.processedOn <= '".concat(orderFilter.getProcessedOnBeforeDate().toString()).concat("'"));
				}
				RailOrderItemFilter railFilter = orderFilter.getRailItemFilter();
				if (railFilter != null) {
					TravellerInfoFilter travellerInfoFilter = railFilter.getTravellerInfoFilter();
					AdditionalInfoFilter additionalInfoFilter = railFilter.getAdditionalInfoFilter();
					if (railFilter.getDepartedOnAfterDate() != null) {
						LocalDateTime time = LocalDateTime.of(railFilter.getDepartedOnAfterDate(), LocalTime.MIN);
						queryParams.add("a.departuretime >= '".concat(time.toString()).concat("'"));
					}

					if (railFilter.getDepartedOnBeforeDate() != null) {
						LocalDateTime time = LocalDateTime.of(railFilter.getDepartedOnBeforeDate(), LocalTime.MAX);
						queryParams.add("a.departuretime <= '".concat(time.toString()).concat("'"));
					}

					if (railFilter.getCreatedOnAfterDate() != null) {
						LocalDateTime time = LocalDateTime.of(railFilter.getCreatedOnAfterDate(), LocalTime.MIN);
						queryParams.add("a.createdon >= '".concat(time.toString()).concat("'"));
					}

					if (railFilter.getCreatedOnBeforeDate() != null) {
						LocalDateTime time = LocalDateTime.of(railFilter.getCreatedOnBeforeDate(), LocalTime.MAX);
						queryParams.add("a.createdon <= '".concat(time.toString()).concat("'"));
					}

					if (travellerInfoFilter != null) {
						queryParams.add(createQueryForArrayOfJsonObjectsWithFunction(travellerInfoFilter));
						String str = createQueryForArrayOfJsonObjectsWithoutFunction(travellerInfoFilter);
						if (str != null)
							queryParams.add(createQueryForArrayOfJsonObjectsWithoutFunction(travellerInfoFilter));
					}
					if (additionalInfoFilter != null) {
						queryParams.add(createQueryForJsonObject(additionalInfoFilter));
					}
				}
			}
			if (CollectionUtils.isNotEmpty(queryParams)) {
				query.append(" WHERE ").append(Joiner.on(" AND ").join(queryParams));
			}
			if (orderFilter.getRailItemFilter() != null
					&& CollectionUtils.isNotEmpty(orderFilter.getRailItemFilter().getSortByAttr())) {
				Direction direction = Direction
						.fromString(orderFilter.getRailItemFilter().getSortByAttr().get(0).getOrderBy());
				List<String> properties = orderFilter.getRailItemFilter().getSortByAttr().get(0).getParams();
				query.append(" order by a.").append(Joiner.on(",a.").join(properties)).append(" " + direction.name());
			} else {
				query.append(" order by ").append("a.createdOn,a.bookingid, a.id");
			}
			query.append(" limit 1500 offset " + (i * 1500)).append(" ;");
			log.debug("[RailOrderSearch] Query formed is {} ", query.toString());

			EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
			List<Object[]> pageResults = entityManager.unwrap(Session.class).createNativeQuery(query.toString())
					.addEntity("a", DbRailOrderItem.class)
					.setFetchSize(orderFilter.getPageAttr() != null ? orderFilter.getPageAttr().getSize() : 5)
					.addEntity("b", DbOrder.class).getResultList();
			entityManager.close();
			if (CollectionUtils.isNotEmpty(pageResults))
				results.addAll(pageResults);

			if (CollectionUtils.isEmpty(pageResults) || pageResults.size() < 1500) {
				break;
			}
		}
		return results;

	}

	private String createQueryForJsonObject(AdditionalInfoFilter additionalInfoFilter) {
		List<String> queryParams = new ArrayList<>();
		if (StringUtils.isNotEmpty(additionalInfoFilter.getPnr())) {
			queryParams.add("a.additionalInfo->>'pnr' IN ('".concat(additionalInfoFilter.getPnr()).concat("')"));
		}
		return Joiner.on(" AND ").join(queryParams);
	}

	private String createQueryForArrayOfJsonObjectsWithFunction(TravellerInfoFilter travellerInfoFilter) {
		StringBuilder query = new StringBuilder(
				" EXISTS ( SELECT FROM jsonb_array_elements(a.travellerinfo) tinfo WHERE ");
		List<String> queryParams = new ArrayList<>();

		if (travellerInfoFilter.getPassengerName() != null) {
			queryParams.add("lower(tinfo->>'fn') LIKE '%".concat(travellerInfoFilter.getPassengerName().toLowerCase())
					.concat("%'"));
		}
		if (!queryParams.isEmpty())
			query.append(Joiner.on(" AND ").join(queryParams)).append(")");
		else
			query.append("true)");
		return query.toString();
	}

	private String createQueryForArrayOfJsonObjectsWithoutFunction(TravellerInfoFilter travellerInfoFilter) {
		List<String> queryParams = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(queryParams))
			return Joiner.on(" AND ").join(queryParams);
		return null;
	}

	@Transactional
	public void deleteItemsWithNoOrderByBookingId(String bookingId) {
		railItemRepository.deleteItemsWithNoOrderByBookingId(bookingId);
	}

	public Integer getNextInvoiceId() {
		return railItemRepository.getNextInvoiceId();
	}

	public String findBookingIdByPnr(String pnr) {
		List<DbRailOrderItem> dbOrderItems = railItemRepository.findByPnr(pnr);
		if (CollectionUtils.isNotEmpty(dbOrderItems))
			return dbOrderItems.get(0).getBookingId();

		return null;
	}

}
