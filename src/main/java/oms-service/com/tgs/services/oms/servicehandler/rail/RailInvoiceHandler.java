package com.tgs.services.oms.servicehandler.rail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.RailCityInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.oms.datamodel.InvoiceInfo;
import com.tgs.services.oms.datamodel.InvoiceType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.mapper.rail.RailOrderItemToJourneyInfoMapper;
import com.tgs.services.oms.restmodel.rail.RailInvoiceRequest;
import com.tgs.services.oms.restmodel.rail.RailInvoiceResponse;
import com.tgs.services.oms.servicehandler.InvoiceHandler;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import com.tgs.services.ums.datamodel.RailAgentInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailInvoiceHandler extends InvoiceHandler<RailInvoiceRequest, RailInvoiceResponse> {

	@Autowired
	protected RailOrderItemService itemService;

	@Autowired
	protected RailCommunicator railComm;

	@Autowired
	protected OrderManager orderManager;

	@Autowired
	protected UserServiceCommunicator userServiceCommunicator;

	@Autowired
	protected GeneralServiceCommunicator generalServiceCommunicator;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {

		try {

			DbRailOrderItem dbOrderItem = itemService.findByBookingId(request.getId());
			RailJourneyInfo journeyInfo =
					RailOrderItemToJourneyInfoMapper.builder().item(dbOrderItem).railComm(railComm).build().convert();
			RailPriceInfo priceInfo = dbOrderItem.getAdditionalInfo().getPriceInfo();

			Order order = orderManager.findByBookingId(request.getId(), new Order());
			if (order == null) {
				response.addError(new ErrorDetail(SystemError.ORDER_NOT_FOUND));
				return;
			}
			User user = userServiceCommunicator.getUserFromCache(order.getBookingUserId());
			response.setIrctcAgentId(user.getRailAdditionalInfo().getIrctcAgentId());
			response.setCreatedOn(order.getCreatedOn());
			response.setInvoiceId(order.getAdditionalInfo().getInvoiceId());
			response.setIrctcTransactionId(dbOrderItem.getAdditionalInfo().getReservationId());
			InvoiceInfo agentInfo = getUpdatedAgentInvoiceInfo(user);
			if (request.getType() == InvoiceType.AGENCY) {
				response.setFrom(getClientInvoiceInfo(
						(ClientGeneralInfo) generalServiceCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
								GeneralBasicFact.builder().applicableTime(order.getCreatedOn()).build())));
				response.setTo(agentInfo);
			} else {
				response.setFrom(agentInfo);
				response.setTo(getCustomerInvoiceInfo(order));
			}
			setFareComponents(priceInfo);
			response.setPriceInfo(priceInfo);
			response.setJourneyInfo(journeyInfo);
			response.setGstInfo(orderManager.getGstInfo(request.getId()));
			response.setBookingId(order.getBookingId());
		} catch (Exception e) {
			log.error("Error occured while fetching invoice details for bookingId {} due to {}", request.getId(),
					e.getMessage(), e);
		}
	}

	public InvoiceInfo getUpdatedAgentInvoiceInfo(User user) {
		InvoiceInfo invoiceInfo = getAgentInvoiceInfo(user);
		RailAgentInfo agentInfo = user.getRailAdditionalInfo().getAgentInfo();
		invoiceInfo.setEmail(agentInfo.getEmail());
		invoiceInfo.setPhone(agentInfo.getMobileNo());
		invoiceInfo.setName(agentInfo.getAgencyName());
		if (agentInfo.getOfficeAddress() != null) {
			invoiceInfo.setAddress(agentInfo.getOfficeAddress().getAddress());
			RailCityInfo cityInfo = agentInfo.getOfficeAddress().getCityInfo();
			if (cityInfo != null) {
				invoiceInfo.setCity(cityInfo.getName());
				invoiceInfo.setState(cityInfo.getState());
			}
			invoiceInfo.setPincode(agentInfo.getOfficeAddress().getPincode());
		}
		return invoiceInfo;
	}


	private void setFareComponents(RailPriceInfo priceInfo) {
		if (request.getType() == InvoiceType.AGENCY) {
			priceInfo.getFareComponents().remove(RailFareComponent.DMF);
			priceInfo.getFareComponents().remove(RailFareComponent.DPC);
		} else {
			Double fareDiffOfDisplayedAndActualCharges =
					priceInfo.getFareComponents().getOrDefault(RailFareComponent.DMF, 0.0)
							+ priceInfo.getFareComponents().getOrDefault(RailFareComponent.DPC, 0.0)
							- priceInfo.getFareComponents().getOrDefault(RailFareComponent.MF, 0.0)
							- priceInfo.getFareComponents().getOrDefault(RailFareComponent.PC, 0.0);
			priceInfo.getFareComponents().put(RailFareComponent.TF,
					priceInfo.getFareComponents().get(RailFareComponent.TF) + fareDiffOfDisplayedAndActualCharges);
			priceInfo.getFareComponents().remove(RailFareComponent.MF);
			priceInfo.getFareComponents().remove(RailFareComponent.PC);
		}
	}

	@Override
	public void afterProcess() throws Exception {}


}
