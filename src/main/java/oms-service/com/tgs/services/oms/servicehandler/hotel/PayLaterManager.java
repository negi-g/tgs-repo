package com.tgs.services.oms.servicehandler.hotel;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItemFilter;
import com.tgs.services.oms.dbmodel.DbHotelOrder;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PayLaterManager {

	@Autowired
	HotelOrderItemManager itemManager;

	@Autowired
	HotelBookingCancellationHandler bookingCancellationHandler;

	@Autowired
	OrderService orderService;

	@Autowired
	HotelOrderMessageHandler orderMessageHandler;

	@Autowired
	private HMSCommunicator hmsCommunicator;

	public void processCancellationJob() {

		log.info("Cancelling unconfirmed boookings");
		LocalDateTime currentTime = LocalDateTime.now();
		OrderFilter orderFilter = OrderFilter.builder().statuses(Arrays.asList(OrderStatus.ON_HOLD))
				.hotelItemFilter(HotelOrderItemFilter.builder().deadlineDateBeforeDateTime(currentTime)
						.deadlineDateAfterDateTime(currentTime.minusHours(7 * 24)).build())
				.build();
		List<DbHotelOrder> hotelOrderList = itemManager.getHotelOrderList(orderFilter);

		cancelBookingIfAllowed(hotelOrderList);
	}

	private void cancelBookingIfAllowed(List<DbHotelOrder> hotelOrderList) {
		if (CollectionUtils.isEmpty(hotelOrderList)) {
			log.info("There are no orders found which is pending for cancellation");
			return;
		}

		HotelGeneralPurposeOutput hotelGeneralPurposeOutput = (HotelGeneralPurposeOutput) hmsCommunicator
				.getHotelConfigRule(HotelConfiguratorRuleType.GNPURPOSE).getOutput();
		int createdOnbuffer =
				ObjectUtils.isEmpty(hotelGeneralPurposeOutput.getCreatedOnBufferTimeForbookingCancellation()) ? 30
						: hotelGeneralPurposeOutput.getCreatedOnBufferTimeForbookingCancellation();
		log.info(
				"Checking if cancellation is allowed having size {}, createdOnbuffer {}, createdOnBefore {}",
				hotelOrderList.size(), createdOnbuffer, LocalDateTime.now().minusMinutes(createdOnbuffer));
		hotelOrderList =
				hotelOrderList.stream()
						.filter(hotelOrder -> hotelOrder.getOrder().getCreatedOn()
								.isBefore(LocalDateTime.now().minusMinutes(createdOnbuffer)))
						.collect(Collectors.toList());
		log.info(
				"Checked if cancellation is allowed, current hotelorderitem size {}, createdOnbuffer {}, createdOnBefore {}",
				hotelOrderList.size(), createdOnbuffer, LocalDateTime.now().minusMinutes(createdOnbuffer));
		for (DbHotelOrder hotelOrder : hotelOrderList) {
			log.info("Cancelling hotelorder for bookingId {} and created on {}", hotelOrder.getOrder().getBookingId(),
					hotelOrder.getOrder().getCreatedOn());
			cancelSupplierBooking(hotelOrder);
		}
	}

	private void cancelSupplierBooking(DbHotelOrder hotelOrder) {
		try {
			HotelPostBookingBaseRequest cancelSupplierRequest = new HotelPostBookingBaseRequest();
			cancelSupplierRequest.setBookingId(hotelOrder.getOrder().getBookingId());
			cancelSupplierRequest.setReason(hotelOrder.getOrder().getReason());
			if (StringUtils.isBlank(cancelSupplierRequest.getReason()))
				cancelSupplierRequest.setReason("Cancelled through auto release process");

			bookingCancellationHandler.cancelBooking(cancelSupplierRequest, true);
		} catch (Exception e) {
			log.error("Unable to cancel booking from supplier for bookingId {}", hotelOrder.getOrder().getBookingId(),
					e);
		}
	}

	public void processNotificationJob(String hoursFrom, String hoursTo) {
		log.info("Processing notificationjob hoursFrom {}, hoursTo {}", hoursFrom, hoursTo);
		LocalDateTime currentTime = LocalDateTime.now();
		OrderFilter orderFilter = OrderFilter.builder().statuses(Arrays.asList(OrderStatus.ON_HOLD))
				.hotelItemFilter(HotelOrderItemFilter.builder()
						.deadlineDateBeforeDateTime(currentTime.plusHours(Long.valueOf(hoursTo)))
						.deadlineDateAfterDateTime(currentTime.plusHours(Long.valueOf(hoursFrom))).build())
				.build();
		List<DbHotelOrder> hotelOrderList = itemManager.getHotelOrderList(orderFilter);
		if (CollectionUtils.isEmpty(hotelOrderList)) {
			log.info("There is no order for which notification is pending");
			return;
		}
		for (DbHotelOrder hotelOrder : hotelOrderList) {
			Order order = hotelOrder.getOrder().toDomain();
			log.info("Sending reminder notification for bookingId {}", order.getBookingId());
			orderMessageHandler.sendBookingEmail(order, hotelOrder.getOrderItems(),
					EmailTemplateKey.HOTEL_BOOKING_CANCELLATION_DEADLINE_EMAIL, null);
			orderMessageHandler.sendBookingSms(order, hotelOrder.getOrderItems(),
					SmsTemplateKey.HOTEL_BOOKING_CANCELLATION_DEADLINE_SMS, null);

		}
	}


}
