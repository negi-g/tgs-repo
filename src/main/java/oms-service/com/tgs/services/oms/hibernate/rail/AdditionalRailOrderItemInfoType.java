package com.tgs.services.oms.hibernate.rail;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.rail.datamodel.booking.AdditionalRailOrderItemInfo;

public class AdditionalRailOrderItemInfoType extends CustomUserType {

	@Override
	public Class<?> returnedClass() {
		return AdditionalRailOrderItemInfo.class;
	}
}
