package com.tgs.services.oms.servicehandler.hotel;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.hotel.HotelOrderStatusUpdateRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelOrderStatusUpdateHandler extends ServiceHandler<HotelOrderStatusUpdateRequest, BaseResponse> {

	@Autowired
	private OrderService orderService;

	@Autowired
	protected HotelOrderItemManager hotelOrderItemManager;

	@Autowired
	HotelOrderMessageHandler messageHandler;

	private Order order;

	private List<DbHotelOrderItem> orderItemList;

	private HotelItemStatus currentStatus;

	@Override
	public void beforeProcess() throws Exception {

		if (StringUtils.isEmpty(request.getBookingId())) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}

		if (StringUtils.isEmpty(request.getRemarks())) {
			throw new CustomGeneralException(SystemError.REMARKS);
		}

		User user = SystemContextHolder.getContextData().getUser();
		order = orderService.findByBookingId(request.getBookingId()).toDomain();
		if (order == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);

		if (!user.getUserId().equals(order.getAdditionalInfo().getAssignedUserId())) {
			throw new CustomGeneralException(SystemError.DIFFERENT_ASSIGNED_USER);
		}

		orderItemList = hotelOrderItemManager.getItemList(request.getBookingId());
		currentStatus = HotelItemStatus.getHotelItemStatus(orderItemList.get(0).getStatus());

		if (!getIsAllItemSameStatus(orderItemList) || request.getStatus() == null
				|| !currentStatus.nextStatusSet().contains(HotelItemStatus.getHotelItemStatus(request.getStatus()))) {
			throw new CustomGeneralException(SystemError.ORDER_INVALID_ACTION);
		}

		order.getAdditionalInfo().setStatusChangeReason(request.getRemarks());


	}

	@Override
	public void process() throws Exception {
		log.info("Updating the Order status for {} from {} to {}", request.getBookingId(), order.getStatus(),
				HotelItemStatus.getHotelItemStatus(request.getStatus()));
		hotelOrderItemManager.save(orderItemList, order, HotelItemStatus.getHotelItemStatus(request.getStatus()));
	}

	@Override
	public void afterProcess() throws Exception {
		messageHandler.sendOrderStatusUpdateMail(order, orderItemList, currentStatus,
				EmailTemplateKey.HOTEL_STATUS_CHANGE_ALERT);
	}

	private boolean getIsAllItemSameStatus(List<DbHotelOrderItem> items) {

		HotelItemStatus itemStatus = HotelItemStatus.getHotelItemStatus(items.get(0).getStatus());
		for (DbHotelOrderItem item : items) {
			if (!item.getStatus().equals(itemStatus.getCode())) {
				return false;
			}
		}
		return true;
	}

}
