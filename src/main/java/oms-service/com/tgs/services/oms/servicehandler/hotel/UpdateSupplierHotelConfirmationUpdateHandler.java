package com.tgs.services.oms.servicehandler.hotel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.ProcessedHotelInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.oms.dbmodel.DbHotelOrder;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.hotel.HotelBookingInfoUpdateRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UpdateSupplierHotelConfirmationUpdateHandler extends ServiceHandler<OrderFilter, BaseResponse> {

	@Autowired
	HotelOrderItemManager hotelOrderItemManager;

	@Autowired
	private HMSCommunicator hmsCommunicator;

	@Autowired
	private HotelOrderItemCommunicator orderItemCommunicator;

	final private static List<OrderStatus> STATUSES = Arrays.asList(OrderStatus.SUCCESS, OrderStatus.ON_HOLD);

	@Override
	public void beforeProcess() throws Exception {

		final LocalDateTime now = LocalDateTime.now();
		if (request.getCreatedOnAfterDateTime() == null && request.getCreatedOnAfterDate() == null) {
			request.setCreatedOnAfterDateTime(now.minusHours(36));
		}
		if (request.getCreatedOnBeforeDateTime() == null && request.getCreatedOnBeforeDate() == null) {
			request.setCreatedOnBeforeDateTime(now.minusMinutes(5));
		}
		request.setStatuses(STATUSES);
		request.setProducts(Arrays.asList(OrderType.HOTEL));
	}

	@Override
	public void process() throws Exception {

		List<DbHotelOrder> hotelOrders = new ArrayList<>();
		final LocalDateTime now = LocalDateTime.now();

		try {
			List<DbHotelOrder> orders = hotelOrderItemManager.getHotelOrderList(request);
			if (CollectionUtils.isNotEmpty(orders)) {
				List<DbHotelOrder> hotelOrdersAgoda = getFilteredAgodaOrder(orders);
				updateHotelReferenceAgodaOrder(hotelOrdersAgoda);

				hotelOrders = orders.stream().filter(
						order -> isValidOrder(order) && order.getOrder().getCreatedOn().isAfter(now.minusHours(24)))
						.collect(Collectors.toList());
				log.info("number of filtered orders with empty hotelref is {}", hotelOrders.size());

				updateHotelReferenceInHotel(hotelOrders);

			}
		} catch (Exception ex) {
			log.error("Exception while updating hotel reference number ", ex);
			throw ex;
		}
	}

	private boolean isValidOrder(DbHotelOrder order) {

		HotelGeneralPurposeOutput configuratorInfo = (HotelGeneralPurposeOutput) hmsCommunicator
				.getHotelConfigRule(HotelConfiguratorRuleType.GNPURPOSE, null).getOutput();

		List<String> supplierValidForHotelRefernceJob =
				configuratorInfo != null && configuratorInfo.getSupplierListForHotelConfirmationNumberJob() != null
						? configuratorInfo.getSupplierListForHotelConfirmationNumberJob()
						: Arrays.asList("AGODA", "EXPEDIA");
		return supplierValidForHotelRefernceJob.contains(order.getOrderItems().get(0).getSupplierId())
				&& validHotelInfo(order.getOrderItems().get(0).getAdditionalInfo().getHInfo())
				&& StringUtils.isBlank(order.getOrderItems().get(0).getAdditionalInfo().getHotelBookingReference());

	}

	private void updateHotelReferenceInHotel(List<DbHotelOrder> hotelOrders) {

		List<String> bookingIds =
				hotelOrders.stream().map(order -> order.getOrder().getBookingId()).collect(Collectors.toList());
		log.info("Filtered booking ids to update hotel reference number are {}", bookingIds);
		if (CollectionUtils.isNotEmpty(bookingIds)) {
			hotelOrders.forEach(hotelOrder -> {
				String bookingId = hotelOrder.getOrder().getBookingId();
				try {
					HotelInfo hotelInfo = orderItemCommunicator.getHotelInfo(bookingId);
					String supplierBookingReference = hotelInfo.getMiscInfo().getSupplierBookingReference();
					String supplierId = hotelInfo.getOptions().get(0).getMiscInfo().getSupplierId();
					AdditionalHotelOrderItemInfo additionalInfo = hotelOrder.getOrderItems().get(0).getAdditionalInfo();
					if (CollectionUtils.isNotEmpty(additionalInfo.getHotelOrderSupplierInfos())) {
						int size = additionalInfo.getHotelOrderSupplierInfos().size();
						supplierId = additionalInfo.getHotelOrderSupplierInfos().get(size - 1).getSupplierId();
						supplierBookingReference =
								additionalInfo.getHotelOrderSupplierInfos().get(size - 1).getSupplierBookingId();
					}

					HotelImportBookingParams importBookingParams =
							HotelImportBookingParams.builder().bookingId(bookingId)
									.supplierBookingId(supplierBookingReference).supplierId(supplierId).build();
					String hotelRef = hmsCommunicator.getHotelConfirmationNumber(importBookingParams);
					if (StringUtils.isNotBlank(hotelRef)) {
						hotelRef = hotelRef + "- CNF#";

						HotelBookingInfoUpdateRequest updateRequest =
								HotelBookingInfoUpdateRequest.builder().hotelBookingReference(hotelRef)
										.supplierBookingId(importBookingParams.getSupplierBookingId())
										.bookingId(importBookingParams.getBookingId()).build();
						log.info("Hotel reference number updated for booking id {} ", bookingId);

						orderItemCommunicator.updateHotelReferenceNumber(updateRequest);
					}
				} catch (Exception e) {
					log.info("Error while updating hotel reference number for booking id {}", bookingId, e);
				}
			});
		}
		log.info("Finished updating filtered booking ids hotel reference number are {}", bookingIds);
	}

	private void updateHotelReferenceAgodaOrder(List<DbHotelOrder> hotelOrders) {
		List<String> bookingIds =
				hotelOrders.stream().map(order -> order.getOrder().getBookingId()).collect(Collectors.toList());
		log.info("Filtered booking ids for agoda to update hotel reference number are {}", bookingIds);
		if (CollectionUtils.isNotEmpty(bookingIds)) {
			hotelOrders.forEach(hotelOrder -> {
				String bookingId = hotelOrder.getOrder().getBookingId();

				try {
					HotelInfo hotelInfo = orderItemCommunicator.getHotelInfo(bookingId);
					String supplierBookingReference = hotelInfo.getMiscInfo().getSupplierBookingReference();

					AdditionalHotelOrderItemInfo additionalInfo = hotelOrder.getOrderItems().get(0).getAdditionalInfo();
					if (CollectionUtils.isNotEmpty(additionalInfo.getHotelOrderSupplierInfos())) {
						int size = additionalInfo.getHotelOrderSupplierInfos().size();
						supplierBookingReference =
								additionalInfo.getHotelOrderSupplierInfos().get(size - 1).getSupplierBookingId();
					}
					String hotelRef = supplierBookingReference + "- CNF#";

					HotelBookingInfoUpdateRequest updateRequest =
							HotelBookingInfoUpdateRequest.builder().hotelBookingReference(hotelRef)
									.supplierBookingId(supplierBookingReference).bookingId(bookingId).build();
					log.info("Hotel reference number as supplier booking id updated for booking id {} ", bookingId);
					orderItemCommunicator.updateHotelReferenceNumber(updateRequest);
				} catch (Exception e) {
					log.info("Error while updating agoda hotel reference number for booking id {}", bookingId, e);
				}
			});
		}
		log.info("Finished updating filtered booking ids for agoda to update hotel reference number are {}",
				bookingIds);
	}

	private boolean validHotelInfo(ProcessedHotelInfo hInfo) {
		return hInfo != null && hInfo.getRating() > 2 && !hInfo.getName().toUpperCase().matches("OYO .*");
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	private List<DbHotelOrder> getFilteredAgodaOrder(List<DbHotelOrder> orders) {
		final LocalDateTime now = LocalDateTime.now();
		List<DbHotelOrder> filteredOrderItem = new ArrayList<>();
		for (DbHotelOrder order : orders) {
			String supplierName = order.getOrderItems().get(0).getSupplierId();
			AdditionalHotelOrderItemInfo additionalInfo = order.getOrderItems().get(0).getAdditionalInfo();
			if (CollectionUtils.isNotEmpty(additionalInfo.getHotelOrderSupplierInfos())) {
				int size = additionalInfo.getHotelOrderSupplierInfos().size();
				supplierName = additionalInfo.getHotelOrderSupplierInfos().get(size - 1).getSupplierId();
			}

			if (order.getOrder().getCreatedOn().isBefore(now.minusHours(24)) && supplierName.equals("AGODA")
					&& isValidOrder(order)) {
				filteredOrderItem.add(order);
			}
		}
		return filteredOrderItem;
	}

}
