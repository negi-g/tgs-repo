package com.tgs.services.oms.servicehandler.rail;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.Amendments.RailAmendmentManager;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.restmodel.rail.RailInvoiceResponse;

@Service
public class RailAmendmentInvoiceHandler extends ServiceHandler<String, RailInvoiceResponse> {

	@Autowired
	private RailAmendmentManager amendmentManager;

	@Autowired
	private AmendmentService amendmentService;


	@Override
	public void beforeProcess() throws Exception {
		Amendment amendment = amendmentService.findByAmendmentId(request);
		if (!amendment.getStatus().equals(AmendmentStatus.SUCCESS))
			throw new CustomGeneralException(SystemError.AMENDMENT_IN_PROGRESS);

		UserServiceHelper.checkAndReturnAllowedUserId(SystemContextHolder.getContextData().getEmulateOrLoggedInUserId(),
				Arrays.asList(amendment.getBookingUserId()));
	}

	@Override
	public void process() throws Exception {
		response = amendmentManager.getAmendmentInvoiceDetails(request, null);
	}

	@Override
	public void afterProcess() throws Exception {}

}
