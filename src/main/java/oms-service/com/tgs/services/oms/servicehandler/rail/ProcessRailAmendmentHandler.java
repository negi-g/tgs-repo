package com.tgs.services.oms.servicehandler.rail;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.Amendments.RailAmendmentHandlerFactory;
import com.tgs.services.oms.Amendments.Processors.RailAmendmentProcessor;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.rail.ProcessRailAmendmentRequest;
import com.tgs.services.ums.datamodel.User;

@Service
public class ProcessRailAmendmentHandler extends ServiceHandler<ProcessRailAmendmentRequest, AmendmentResponse> {


	@Autowired
	private AmendmentService service;

	@Autowired
	private AmendmentActionValidator actionValidator;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private RailOrderItemService itemService;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Transactional
	@Override
	public void process() {
		Double differentialPayment = null;
		Amendment amendment = service.findByAmendmentId(request.getAmendmentId());
		User bookingUser = userService.getUserFromCache(amendment.getBookingUserId());


		if (request.getModifiedItem() != null) {
			DbRailOrderItem orgOrderItem = itemService.findByBookingId(amendment.getBookingId());
			RailAmendmentProcessor amendmentHandler = RailAmendmentHandlerFactory.initHandler(amendment,
					new DbRailOrderItem().from(request.getModifiedItem()), orgOrderItem, bookingUser);
			amendmentHandler.patchRailOrderItem(false);
			amendmentHandler.process();
			amendment.getModifiedInfo().setRailOrderItem(amendmentHandler.patchedItem);
		}
		differentialPayment = amendment.getAmount();
		amendment.setStatus(AmendmentStatus.PROCESSING);
		service.save(amendment);
		amendment
				.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment));
		response.setAmendmentItems(Arrays.asList(amendment));
		response.setDifferentialPayment(differentialPayment);

	}

	@Override
	public void afterProcess() throws Exception {

	}

}
