package com.tgs.services.oms.servicehandler.air;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.oms.restmodel.air.RetrieveTicketStatusRequest;
import com.tgs.services.oms.restmodel.air.RetrieveTicketStatusResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RetrieveTicketStatusHandler
		extends ServiceHandler<RetrieveTicketStatusRequest, RetrieveTicketStatusResponse> {

	@Autowired
	private FMSCommunicator fmsComm;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() throws Exception {
		log.info("Retrieving ticket status for ticket number {}, supplier id {}", request.getTicketNumbers(),
				request.getSupplierId());
		try {
			response = fmsComm.retrieveTicketStatus(request);
		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			log.error("Unable to fetch ticket status for ticket number {}  and cause {}", request.getTicketNumbers(),
					e);
		}
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
