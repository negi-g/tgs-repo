package com.tgs.services.oms.dbmodel.rail;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.oms.hibernate.rail.AdditionalRailOrderItemInfoType;
import com.tgs.services.oms.hibernate.rail.RailTravellerInfoType;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.datamodel.booking.AdditionalRailOrderItemInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "RailOrderItem")
@Table(name = "railorderitem")
@Builder
@Audited
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({@TypeDef(name = "RailTravellerInfoType", typeClass = RailTravellerInfoType.class),
		@TypeDef(name = "AdditionalRailOrderItemInfoType", typeClass = AdditionalRailOrderItemInfoType.class)})
public class DbRailOrderItem extends SuperBaseModel<DbRailOrderItem, RailOrderItem> {

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "railorder_generator")
	@SequenceGenerator(name = "railorder_generator", sequenceName = "railorderitem_id_seq", allocationSize = 1)
	private Long id;

	@Column
	private String bookingId;

	@Column
	private String fromStn;

	@Column
	private String toStn;

	@CreationTimestamp	
	private LocalDateTime createdOn;

	@UpdateTimestamp
	private LocalDateTime processedOn;

	@Column
	private LocalDateTime departureTime;

	@Column
	private LocalDateTime arrivalTime;

	@Column
	@Type(type = "RailTravellerInfoType")
	private List<RailTravellerInfo> travellerInfo;

	@Column
	private String status;

	@Column
	private Double amount;

	@Column
	@Type(type = "AdditionalRailOrderItemInfoType")
	private AdditionalRailOrderItemInfo additionalInfo;

	public RailOrderItem toDomain() {
		return new GsonMapper<>(this, RailOrderItem.class).convert();
	}

	public DbRailOrderItem from(RailOrderItem dataModel) {
		return new GsonMapper<>(dataModel, new DbRailOrderItem(), DbRailOrderItem.class).convert();
	}

	@Column
	private String pnr;

}
