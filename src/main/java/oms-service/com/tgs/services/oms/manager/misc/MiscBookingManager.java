package com.tgs.services.oms.manager.misc;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAdditionalInfo;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.misc.AdditionalMiscOrderItemInfo;
import com.tgs.services.oms.datamodel.misc.MiscItemStatus;
import com.tgs.services.oms.datamodel.misc.MiscPriceInfo;
import com.tgs.services.oms.dbmodel.misc.DbMiscOrderItem;
import com.tgs.services.oms.jparepository.misc.MiscOrderItemService;
import com.tgs.services.oms.manager.BookingManager;
import com.tgs.services.oms.restmodel.misc.MiscBookingRequest;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Getter
@Setter
@Slf4j
@Qualifier("MiscBookingManager")
public class MiscBookingManager extends BookingManager {

	protected MiscBookingRequest miscBookingRequest;

	protected Double orderAmount;

	DbMiscOrderItem item;


	@Autowired
	protected MiscOrderItemService itemService;

	public Order processBooking() {
		miscBookingRequest = (MiscBookingRequest) bookingRequest;
		this.storeOrderItem();
		this.createOrder();
		this.addOrderToAudit(AuditAction.ORDER_CREATE);
		log.info("Order successfully stored for bookingId {}", miscBookingRequest.getBookingId());
		this.doPayment();
		return order;
	}

	protected void storeOrderItem() {
		item = new DbMiscOrderItem();
		item.setBookingId(miscBookingRequest.getBookingId());
		AdditionalMiscOrderItemInfo additionalInfo =
				AdditionalMiscOrderItemInfo.builder().description(miscBookingRequest.getDescription()).build();
		MiscPriceInfo priceInfo = MiscPriceInfo.builder().totalFare(orderTotal).build();
		item.setAdditionalInfo(additionalInfo);
		item.setPriceInfo(priceInfo);
		item.setStatus(MiscItemStatus.IN_PROGRESS.getCode());
		itemService.save(item);
	}

	@Override
	public void updateItemStatus(OrderStatus orderStatus) {
		MiscItemStatus itemStatus = getMiscItemStatus(orderStatus);
		item.setStatus(itemStatus.getCode());
		itemService.save(item);
	}

	@Override
	protected List<PaymentRequest> createVirualPaymentRequest(List<PaymentRequest> paymentInfos,
			GstInfo orderBillingEntity) {
		return null;
	}

	@Override
	protected void applyVoucher() {}

	public MiscItemStatus getMiscItemStatus(OrderStatus orderStatus) {
		MiscItemStatus currentStatus = MiscItemStatus.getMiscItemStatus(item.getStatus());
		MiscItemStatus nextStatus = currentStatus;
		if (OrderStatus.SUCCESS.equals(orderStatus)) {
			nextStatus = MiscItemStatus.SUCCESS;
		} else if (OrderStatus.FAILED.equals(order.getStatus())) {
			nextStatus = MiscItemStatus.FAILED;
		} else {
			OrderAdditionalInfo additionalInfo = order.getAdditionalInfo();
			PaymentStatus paymentStatus = PaymentStatus.PENDING;
			if (additionalInfo != null && additionalInfo.getPaymentStatus() != null) {
				paymentStatus = additionalInfo.getPaymentStatus();
			}
			if (PaymentStatus.SUCCESS.equals(paymentStatus)) {
				nextStatus = MiscItemStatus.PAYMENT_SUCCESS;
			} else {
				nextStatus = MiscItemStatus.PAYMENT_FAILED;
			}
		}
		return nextStatus;
	}

}
