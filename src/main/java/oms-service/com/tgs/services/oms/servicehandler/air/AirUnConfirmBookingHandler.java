package com.tgs.services.oms.servicehandler.air;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AdditionalInfoFilter;
import com.tgs.services.oms.datamodel.air.AirOrderItemFilter;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.restmodel.air.AirReleasePNRRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirUnConfirmBookingHandler {

	@Autowired
	private AirReleasePNRHandler releasePNRHandler;

	@Autowired
	private AirOrderItemService orderItemService;

	@Autowired
	private OrderService orderService;


	public void processCancellationJob() {


		// minutes in a day 24*60=1440
		Integer bufferMinutes = 1440;
		LocalDateTime currentTime = LocalDateTime.now();
		LocalDateTime timeLimitBuffer = currentTime.minusMinutes(bufferMinutes);

		AdditionalInfoFilter additionalInfoFilter =
				AdditionalInfoFilter.builder().holdTimeLimitBeforeDateTime(timeLimitBuffer).build();
		AirOrderItemFilter itemFilter = AirOrderItemFilter.builder().additionalInfoFilter(additionalInfoFilter).build();

		// Taking bookings from current date to last 15 days.
		LocalDateTime createdAfterTime = LocalDateTime.now().minusDays(15);
		LocalDateTime createdBeforeTime = LocalDateTime.now();

		OrderFilter orderFilter = OrderFilter.builder().statuses(Arrays.asList(OrderStatus.ON_HOLD))
				.createdOnAfterDateTime(createdAfterTime).createdOnBeforeDateTime(createdBeforeTime)
				.itemFilter(itemFilter).build();

		List<Object[]> results = orderItemService.findByJsonSearch(orderFilter);


		Set<String> bookingIds = new HashSet<>();
		for (Object[] result : results) {
			String bookingId = ((DbAirOrderItem) result[0]).getBookingId();
			bookingIds.add(bookingId);
		}

		log.info("Hold Booking Notification Orders Found {} ", CollectionUtils.size(bookingIds));

		for (String bookingId : bookingIds) {
			log.info("[OrderJobHanlder] Unconfirming BookingId {}", bookingId);
			List<DbAirOrderItem> dbItems = orderItemService.findByBookingId(bookingId);
			Set<String> pnrs = new HashSet<>();
			dbItems.forEach(item -> {
				pnrs.add(item.getTravellerInfo().get(0).getPnr());
			});
			AirReleasePNRRequest releasePNRRequest = new AirReleasePNRRequest();
			releasePNRRequest.setBookingId(bookingId);
			releasePNRRequest.setPnrs(new ArrayList<String>(pnrs));
			releasePNRHandler.initData(releasePNRRequest, new BaseResponse());
			try {
				releasePNRHandler.getResponse();
			} catch (Exception e) {
				log.error("[OrderJobHanlder] Error occured while unconfirming for {} ", bookingId, e);
			}
		}

	}


}
