package com.tgs.services.oms.restcontroller.rail;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.oms.restmodel.OrderResponse;
import com.tgs.services.oms.restmodel.rail.RailBookingRequest;
import com.tgs.services.oms.restmodel.rail.RailBookingResponse;
import com.tgs.services.oms.restmodel.rail.RailInvoiceRequest;
import com.tgs.services.oms.restmodel.rail.RailInvoiceResponse;
import com.tgs.services.oms.restmodel.rail.RailOrderProcessingRequest;
import com.tgs.services.oms.servicehandler.rail.RailAbortHandler;
import com.tgs.services.oms.servicehandler.rail.RailBookingDetailHandler;
import com.tgs.services.oms.servicehandler.rail.RailBookingHandler;
import com.tgs.services.oms.servicehandler.rail.RailInvoiceHandler;
import com.tgs.services.oms.servicehandler.rail.RailOrderListingHandler;
import com.tgs.services.oms.servicehandler.rail.RailOrderMessageHandler;
import com.tgs.services.oms.servicehandler.rail.RailOrderProcessingEngine;
import com.tgs.services.rail.restmodel.RailMessageRequest;
import com.tgs.services.ums.datamodel.AreaRole;

@RestController
@RequestMapping("/oms/v1/rail")
public class RailOrderController {

	@Autowired
	RailOrderListingHandler orderListingHandler;

	@Autowired
	protected RailBookingHandler bookingHandler;

	@Autowired
	private RailOrderMessageHandler msgHandler;

	@Autowired
	protected RailBookingDetailHandler bookingDetailHandler;

	@Autowired
	protected RailInvoiceHandler invoiceHandler;

	@Autowired
	protected AuditsHandler auditsHandler;

	@Autowired
	protected RailAbortHandler abortOrderHandler;

	@Autowired
	protected RailOrderProcessingEngine orderProcessingEngine;

	@RequestMapping(value = "/book", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.BOOKING_REQUEST,
			includedRoles = {UserRole.AGENT, UserRole.AGENT_STAFF, UserRole.CORPORATE, UserRole.CORPORATE_STAFF},
			emulatedAllowedRoles = {UserRole.CALLCENTER, UserRole.CORPORATE, UserRole.ADMIN, UserRole.AGENT,
					UserRole.SUPERVISOR})
	protected RailBookingResponse railBook(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody RailBookingRequest bookingRequest) throws Exception {
		bookingHandler.initData(bookingRequest, new RailBookingResponse());
		return bookingHandler.getResponse();
	}

	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	@RequestMapping(value = "/booking-details", method = RequestMethod.POST)
	protected BookingDetailResponse getBookingDetails(HttpServletRequest request,
			@RequestBody BookingDetailRequest detailRequest) throws Exception {
		bookingDetailHandler.initData(detailRequest, new BookingDetailResponse());
		return bookingDetailHandler.getResponse();
	}

	@RequestMapping(value = "/orders", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected OrderResponse getOrderList(HttpServletRequest request, @Valid @RequestBody OrderFilter orderFilter,
			BindingResult result) throws Exception {
		orderFilter.setProducts(Arrays.asList(OrderType.RAIL));
		List<String> allowedUserIds = BaseUtils.getCheckAndAllowedUserIds(orderFilter.getBookingUserIds());
		orderFilter.setBookingUserIds(BaseUtils.getUserIds(allowedUserIds, orderFilter.getGroupIds()));
		FiltersValidator validator =
				(FiltersValidator) SpringContext.getApplicationContext().getBean("filtersValidator");
		validator.validate(orderFilter, result);
		if (result.hasErrors()) {
			OrderResponse response = new OrderResponse();
			response.setErrors(validator.getErrorDetailFromBindingResult(result));
			return response;
		} else {
			orderListingHandler.initData(orderFilter, new OrderResponse());
			return orderListingHandler.getResponse();
		}
	}

	@RequestMapping(value = "/invoice", method = RequestMethod.POST)
	protected RailInvoiceResponse getInvoiceDetail(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody RailInvoiceRequest railInvoiceRequest) throws Exception {
		invoiceHandler.initData(railInvoiceRequest, new RailInvoiceResponse());
		return invoiceHandler.getResponse();
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditsHandler.fetchAudits(auditsRequestData, DbRailOrderItem.class, "bookingId"));
		return auditResponse;
	}

	@RequestMapping(value = "/msg-ticket", method = RequestMethod.POST)
	protected BaseResponse sendETicket(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid RailMessageRequest msgRequest) throws Exception {
		msgHandler.initData(msgRequest, new BaseResponse());
		return msgHandler.getResponse();
	}

	@RequestMapping(value = "/abort/{bookingId}", method = RequestMethod.GET)
	protected BaseResponse abortBooking(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String bookingId) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(bookingId);
		abortOrderHandler.abortBooking(bookingId);
		return new BaseResponse();
	}

	@RequestMapping(value = "/job/process/booking", method = RequestMethod.POST)
	protected BaseResponse processBooking(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RailOrderProcessingRequest orderProcessingRequest) throws Exception {
		orderProcessingEngine.processOrder(orderProcessingRequest);
		return new BaseResponse();
	}


}
