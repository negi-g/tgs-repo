package com.tgs.services.oms.Amendments;

import com.tgs.filters.AmendmentFilter;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.amendments.PWSReason;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Service
public class AmendmentCronManager {

    @Autowired
    private AmendmentService amendmentService;

    public AmendmentResponse checkPWSStatus(Short scanHours) {

        List<Amendment> amendmentList = amendmentService.search(AmendmentFilter.builder()
                .statusIn(Arrays.asList(AmendmentStatus.PENDING_WITH_SUPPLIER))
                .build());

        AmendmentResponse amendmentResponse = AmendmentResponse.builder().amendmentItems(new ArrayList<>()).build();
        amendmentList.forEach(amendment -> {
            Short hours = 6;
            if (!CollectionUtils.isEmpty(amendment.getAirAdditionalInfo().getPwsReasons())) {
                hours = amendment.getAirAdditionalInfo().getPwsReasons().stream()
                        .max(Comparator.comparing(PWSReason::getHoldTime)).get().getHoldTime();
            }
            LocalDateTime pwsEndTime = amendment.getAirAdditionalInfo().getPwsEnterTime().plusHours(hours);
            if (pwsEndTime.isBefore(LocalDateTime.now())) {
                amendment.setStatus(AmendmentStatus.PENDING_REASSIGN);
                amendmentService.save(amendment);
                amendmentResponse.getAmendmentItems().add(amendment);
            }
        });
        return amendmentResponse;
    }
}
