package com.tgs.services.oms.hibernate.misc;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.oms.datamodel.misc.AdditionalMiscOrderItemInfo;

public class AdditionalMiscOrderItemInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return AdditionalMiscOrderItemInfo.class;
	}

}
