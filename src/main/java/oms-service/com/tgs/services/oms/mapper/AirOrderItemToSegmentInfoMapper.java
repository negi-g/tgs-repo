package com.tgs.services.oms.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.gson.GsonUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.google.gson.reflect.TypeToken;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Builder
@Getter
@Setter
@Slf4j
public class AirOrderItemToSegmentInfoMapper extends Mapper<SegmentInfo> {

	private FMSCommunicator fmsCommunicator;

	private DbAirOrderItem item;
	private SegmentInfo segmentInfo;

	@Override
	protected void execute() throws CustomGeneralException {

		output = new SegmentInfo();
		output.setArrivalAirportInfo(fmsCommunicator.getAirportInfo(item.getDest())
				.setTerminal(item.getAdditionalInfo().getArrivalTerminal()));
		output.setDepartAirportInfo(fmsCommunicator.getAirportInfo(item.getSource())
				.setTerminal(item.getAdditionalInfo().getDepartureTerminal()));
		output.setFlightDesignator(FlightDesignator.builder()
				.airlineInfo(fmsCommunicator.getAirlineInfo(item.getAirlinecode())).flightNumber(item.getFlightNumber())
				.equipType(item.getAdditionalInfo().getEquipType()).build());
		output.setDepartTime(item.getDepartureTime());

		SupplierInfo supplierInfo = fmsCommunicator.getSupplierInfo(item.getSupplierId());
		if (Objects.isNull(supplierInfo)) {
			throw new CustomGeneralException(SystemError.INVALID_FBRC_SUPPLIERID,
					SystemError.INVALID_FBRC_SUPPLIERID.getMessage(item.getSupplierId()));
		}
		AdditionalAirOrderItemInfo addItemInfo = item.getAdditionalInfo();

		log.debug("Supplier rule id is {}", item.getAdditionalInfo().getSupplierRuleId());
		log.debug("Supplier id from supplier info {} ", supplierInfo.getSupplierId());
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(SupplierBasicInfo.builder().ruleId(item.getAdditionalInfo().getSupplierRuleId())
				.supplierId(supplierInfo.getSupplierId()).supplierName(supplierInfo.getName())
				.sourceId(supplierInfo.getSourceId()).build());

		priceInfo.setFareDetails(getFareDetails());

		priceInfo.setMiscInfo(
				PriceMiscInfo.builder().accountCode(addItemInfo.getAccountCode()).tourCode(addItemInfo.getTourCode())
						.ccInfoId(addItemInfo.getCcId()).commericialRuleId(addItemInfo.getCommercialRuleId())
						.accountCode(addItemInfo.getAccountCode()).timeLimit(addItemInfo.getTimeLimit())
						.iata(addItemInfo.getIata()).inventoryId(addItemInfo.getInventoryId())
						.legNum(item.getAdditionalInfo().getLegNum()).holdFeeRuleId(addItemInfo.getHoldFeeRuleId())
						.changeFareTypeId(addItemInfo.getChangeFareTypeId()).build());

		if (StringUtils.isNotEmpty(addItemInfo.getPlatingCarrier())) {
			priceInfo.getMiscInfo().setPlatingCarrier(fmsCommunicator.getAirlineInfo(addItemInfo.getPlatingCarrier()));
		}
		if (CollectionUtils.isNotEmpty(addItemInfo.getMessages())) {
			priceInfo.setMessages(addItemInfo.getMessages());

		}
		priceInfo.getMiscInfo().setFareRuleId(item.getAdditionalInfo().getFareRuleId());
		priceInfo.getMiscInfo().setTimeLimit(item.getAdditionalInfo().getTimeLimit());
		output.getPriceInfoList().add(priceInfo);
		output.setArrivalTime(item.getArrivalTime());
		output.setDuration(item.getAdditionalInfo().getDuration());
		output.setSegmentNum(item.getAdditionalInfo().getSegmentNo());

		if (StringUtils.isNotBlank(item.getAdditionalInfo().getOperatingAirline())) {
			output.setOperatedByAirlineInfo(
					fmsCommunicator.getAirlineInfo(item.getAdditionalInfo().getOperatingAirline()));
		}
		if (item.getAdditionalInfo().getIsEticket() != null) {
			output.getFlightDesignator().getAirlineInfo()
					.setIsTkRequired(BooleanUtils.isTrue(item.getAdditionalInfo().getIsEticket()));
			output.getPriceInfo(0).getMiscInfo()
					.setIsEticket(BooleanUtils.isTrue(item.getAdditionalInfo().getIsEticket()));
		}
		output.setBookingRelatedInfo(SegmentBookingRelatedInfo.builder().travellerInfo(item.getTravellerInfo())
				.status(AirItemStatus.getAirItemStatus(item.getStatus())).build());

		if (CollectionUtils.isNotEmpty(item.getAdditionalInfo().getStopOverAirport())) {
			output.setStopOverAirports(new ArrayList<>());
			for (String code : item.getAdditionalInfo().getStopOverAirport()) {
				output.getStopOverAirports().add(fmsCommunicator.getAirportInfo(code));
			}
		}


		if (BooleanUtils.isTrue(item.getAdditionalInfo().getIsPrivateFare())) {
			output.getPriceInfo(0).getMiscInfo().setIsPrivateFare(item.getAdditionalInfo().getIsPrivateFare());
		}
		if (item.getAdditionalInfo().getCreditShellPNR() != null) {
			output.getPriceInfo(0).getMiscInfo().setCreditShellPNR(item.getAdditionalInfo().getCreditShellPNR());
		}
		if (BooleanUtils.isTrue(item.getAdditionalInfo().getIsUserCreditCard())) {
			output.getPriceInfo(0).getMiscInfo().setIsUserCreditCard(item.getAdditionalInfo().getIsUserCreditCard());
		}
		if (item.getAdditionalInfo().getFareTypeRuleId() != null) {
			output.getPriceInfo(0).getMiscInfo().setFareTypeRuleId(item.getAdditionalInfo().getFareTypeRuleId());
		}
		if (item.getAdditionalInfo().getCancellationProtectionRuleId() != null) {
			output.getPriceInfo(0).getMiscInfo()
					.setCancellationProtectionRuleId(item.getAdditionalInfo().getCancellationProtectionRuleId());
		}
		if (BooleanUtils.isTrue(item.getAdditionalInfo().getIsInternationalSplitSearch())) {
			output.getPriceInfo(0).getMiscInfo()
					.setIsInternationalSplitSearch(item.getAdditionalInfo().getIsInternationalSplitSearch());
		}
		if (item.getId() != null) {
			output.setId(String.valueOf(item.getId()));
		}
		output.setStops(item.getAdditionalInfo().getStops());
		output.setSegmentNum(item.getAdditionalInfo().getSegmentNo());
		output.getPriceInfo(0).setFareIdentifier(item.getAdditionalInfo().getFareIdentifier());
		String originalFareType = item.getAdditionalInfo().getOriginalFareType();
		if (StringUtils.isNotBlank(originalFareType)) {
			output.getPriceInfo(0).setOriginalFareType(originalFareType);
		}
		output.getMiscInfo().setSearchType(item.getAdditionalInfo().getSearchType());
		output.getMiscInfo().setAirType(AirType.getEnumFromCode(item.getAdditionalInfo().getType()));
		output.setIsArrivalNextDay();
		if (segmentInfo != null) {
			output.setSsrInfo(segmentInfo.getSsrInfo());
			output.setIsReturnSegment(segmentInfo.getIsReturnSegment());
			output.setMiscInfo(segmentInfo.getMiscInfo());
			if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
				Long logicalFlightId = segmentInfo.getPriceInfoList().get(0).getMiscInfo().getLogicalFlightId();
				PriceMiscInfo segmentPriceMisc = segmentInfo.getPriceInfoList().get(0).getMiscInfo();
				output.getPriceInfo(0).getMiscInfo().setLogicalFlightId(logicalFlightId);
				output.getPriceInfo(0).getMiscInfo().setMealWayType(segmentPriceMisc.getMealWayType());
				output.getPriceInfo(0).getMiscInfo().setWeight(segmentPriceMisc.getWeight());
				output.getPriceInfo(0).getMiscInfo().setBaggageWayType(segmentPriceMisc.getBaggageWayType());
				output.getPriceInfo(0).getMiscInfo().setTraceId(segmentPriceMisc.getTraceId());
				output.getPriceInfo(0).getMiscInfo().setJourneyKey(segmentPriceMisc.getJourneyKey());
				output.getPriceInfo(0).getMiscInfo().setMarriageGrp(segmentPriceMisc.getMarriageGrp());
				output.getPriceInfo(0).getMiscInfo().setTokenId(segmentPriceMisc.getTokenId());
				output.getPriceInfo(0).getMiscInfo().setInventoryId(segmentPriceMisc.getInventoryId());
				output.getPriceInfo(0).getMiscInfo().setFareTypeId(segmentPriceMisc.getFareTypeId());
				output.getPriceInfo(0).getMiscInfo().setLegNum(segmentPriceMisc.getLegNum());
				output.getPriceInfo(0).getMiscInfo().setProviderCode(segmentPriceMisc.getProviderCode());
				output.getPriceInfo(0).getMiscInfo().setEffectiveDate(segmentPriceMisc.getEffectiveDate());
				output.getPriceInfo(0).getMiscInfo().setPaxPricingInfo(segmentPriceMisc.getPaxPricingInfo());
				output.getPriceInfo(0).getMiscInfo().setFareKey(segmentPriceMisc.getFareKey());
				output.getPriceInfo(0).getMiscInfo().setFareLevel(segmentPriceMisc.getFareLevel());
				output.getPriceInfo(0).getMiscInfo().setCreditShellPNR(segmentPriceMisc.getCreditShellPNR());
				output.getPriceInfo(0).getMiscInfo().setFareRuleId(segmentPriceMisc.getFareRuleId());
				output.getPriceInfo(0).getMiscInfo().setFareTypeRuleId(segmentPriceMisc.getFareTypeRuleId());
				output.getPriceInfo(0).getMiscInfo().setFareRuleInfoRef(segmentPriceMisc.getFareRuleInfoRef());
				output.getPriceInfo(0).getMiscInfo().setAirPriceSolutionKey(segmentPriceMisc.getAirPriceSolutionKey());
				output.getPriceInfo(0).getMiscInfo()
						.setBookingTravellerInfoRef(segmentPriceMisc.getBookingTravellerInfoRef());
				output.getPriceInfo(0).getMiscInfo().setIsPromotionalFare(segmentPriceMisc.getIsPromotionalFare());
				output.getPriceInfo(0).getMiscInfo().setFareInfoKeyMap(segmentPriceMisc.getFareInfoKeyMap());
				output.getPriceInfo(0).getMiscInfo().setSegmentKey(segmentPriceMisc.getSegmentKey());
			}
			updateSSRMiscInfo();
		}
		if (item.getAdditionalInfo().getIsReturnSegment() != null && Objects.nonNull(output)) {
			output.setIsReturnSegment(item.getAdditionalInfo().getIsReturnSegment());
		}
	}

	private void updateSSRMiscInfo() {
		if (CollectionUtils.isNotEmpty(output.getTravellerInfo()) && MapUtils.isNotEmpty(segmentInfo.getSsrInfo())) {

			List<BaggageSSRInformation> baggageSSRs = GsonUtils.getGson().fromJson(
					GsonUtils.getGson().toJson(segmentInfo.getSsrInfo().get(SSRType.BAGGAGE)),
					new TypeToken<List<BaggageSSRInformation>>() {}.getType());
			List<MealSSRInformation> mealSSRs =
					GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(segmentInfo.getSsrInfo().get(SSRType.MEAL)),
							new TypeToken<List<MealSSRInformation>>() {}.getType());
			List<SeatSSRInformation> seatSSRs =
					GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(segmentInfo.getSsrInfo().get(SSRType.SEAT)),
							new TypeToken<List<SeatSSRInformation>>() {}.getType());

			for (FlightTravellerInfo travellerInfo : output.getTravellerInfo()) {
				if (travellerInfo.getSsrBaggageInfo() != null) {
					travellerInfo.setSsrBaggageInfo(getBaggageSSRInfo(travellerInfo.getSsrBaggageInfo(), baggageSSRs));
				}

				if (travellerInfo.getSsrMealInfo() != null) {
					travellerInfo.setSsrMealInfo(getMealSSRInfo(travellerInfo.getSsrMealInfo(), mealSSRs));
				}

				if (travellerInfo.getSsrSeatInfo() != null) {
					travellerInfo.setSsrSeatInfo(getSeatSSR(travellerInfo.getSsrSeatInfo(), seatSSRs));
				}
			}
		}
	}

	private BaggageSSRInformation getBaggageSSRInfo(SSRInformation ssrBaggageInfo,
			List<BaggageSSRInformation> baggageSSRs) {
		return baggageSSRs.stream()
				.filter(baggage -> StringUtils.equalsIgnoreCase(baggage.getCode(), ssrBaggageInfo.getCode()))
				.findFirst().orElse(new GsonMapper<>(ssrBaggageInfo, BaggageSSRInformation.class).convert());
	}

	private MealSSRInformation getMealSSRInfo(SSRInformation ssrMealInfo, List<MealSSRInformation> mealSSRs) {
		return mealSSRs.stream().filter(meal -> StringUtils.equalsIgnoreCase(meal.getCode(), ssrMealInfo.getCode()))
				.findFirst().orElse(new GsonMapper<>(ssrMealInfo, MealSSRInformation.class).convert());
	}

	private SeatSSRInformation getSeatSSR(SSRInformation ssrSeatInfo, List<SeatSSRInformation> seatSSRs) {
		return seatSSRs.stream().filter(meal -> StringUtils.equalsIgnoreCase(meal.getCode(), ssrSeatInfo.getCode()))
				.findFirst().orElse(new GsonMapper<>(ssrSeatInfo, SeatSSRInformation.class).convert());
	}

	private Map<PaxType, FareDetail> getFareDetails() {
		Map<PaxType, FareDetail> map = new HashMap<>();
		item.getTravellerInfo().forEach(t -> {
			if (!map.keySet().contains(t.getPaxType())) {
				map.put(t.getPaxType(), t.getFareDetail());
			}
		});

		return map;
	}
}
