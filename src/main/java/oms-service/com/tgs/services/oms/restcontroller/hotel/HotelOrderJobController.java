package com.tgs.services.oms.restcontroller.hotel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.restmodel.hotel.UpdateOrderRequest;
import com.tgs.services.oms.servicehandler.hotel.HotelUpdateOrderStatusHandler;
import com.tgs.services.oms.servicehandler.hotel.PayLaterManager;
import com.tgs.services.oms.servicehandler.hotel.UpdateBookNowPayLaterStatusHandler;
import com.tgs.services.oms.servicehandler.hotel.UpdateSupplierBookingStatusHandler;
import com.tgs.services.oms.servicehandler.hotel.UpdateSupplierHotelConfirmationUpdateHandler;
import com.tgs.services.oms.servicehandler.hotel.VerifyHotelMappingHandler;


@RestController
@RequestMapping("/oms/v1/hotel/job")
public class HotelOrderJobController {

	@Autowired
	PayLaterManager payLaterManager;

	@Autowired
	private HotelUpdateOrderStatusHandler orderStatusHandler;

	@Autowired
	private UpdateSupplierBookingStatusHandler supplierBookingStatusService;

	@Autowired
	private UpdateSupplierHotelConfirmationUpdateHandler supplierHotelConfirmationUpdateHandler;

	@Autowired
	private UpdateBookNowPayLaterStatusHandler bookNowPayLaterStatusHandler;

	@Autowired
	private VerifyHotelMappingHandler verifyHotelMappingHandler;

	@RequestMapping(value = "/cancel-unconfirmed-booking", method = RequestMethod.POST)
	protected BaseResponse payLater(HttpServletRequest request) {
		payLaterManager.processCancellationJob();
		return new BaseResponse();
	}

	@RequestMapping(value = "/notify-unconfirmed-booking/{hoursFrom}/{hoursTo}", method = RequestMethod.POST)
	protected BaseResponse notifyFirst(HttpServletRequest request, @PathVariable String hoursFrom,
			@PathVariable String hoursTo) {
		payLaterManager.processNotificationJob(hoursFrom, hoursTo);
		return new BaseResponse();
	}

	@RequestMapping(value = "/update-order-status", method = RequestMethod.POST)
	protected BaseResponse updateOrderStatus(HttpServletRequest request, @Valid @RequestBody OrderFilter orderFilter)
			throws Exception {
		orderStatusHandler.initData(orderFilter, new BaseResponse());
		return orderStatusHandler.getResponse();
	}

	@RequestMapping(value = "/update-status", method = RequestMethod.POST)
	protected void updateOrderStatus(@RequestBody UpdateOrderRequest retrieveBookingDetailRequest,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		supplierBookingStatusService.initData(retrieveBookingDetailRequest, new BaseResponse());
		supplierBookingStatusService.getResponse();
	}

	@RequestMapping(value = "/update-hotelconfirmation-number", method = RequestMethod.POST)
	protected BaseResponse updateHotelConfirmationNumber(HttpServletRequest request,
			@Valid @RequestBody OrderFilter orderFilter) throws Exception {
		supplierHotelConfirmationUpdateHandler.initData(orderFilter, new BaseResponse());
		return supplierHotelConfirmationUpdateHandler.getResponse();
	}

	@RequestMapping(value = "/update-bnpl-status", method = RequestMethod.POST)
	protected BaseResponse updateBookNowPayLaterStatus(HttpServletRequest request,
			@Valid @RequestBody OrderFilter orderFilter) throws Exception {
		bookNowPayLaterStatusHandler.initData(orderFilter, new BaseResponse());
		return bookNowPayLaterStatusHandler.getResponse();
	}

	@RequestMapping(value = "/verify-hotel-mapping", method = RequestMethod.POST)
	protected BaseResponse verifyHotelMapping(HttpServletRequest request, @Valid @RequestBody OrderFilter orderFilter)
			throws Exception {
		verifyHotelMappingHandler.initData(orderFilter, new BaseResponse());
		return verifyHotelMappingHandler.getResponse();
	}

}
