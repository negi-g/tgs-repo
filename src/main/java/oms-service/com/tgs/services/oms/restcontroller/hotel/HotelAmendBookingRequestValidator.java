package com.tgs.services.oms.restcontroller.hotel;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.tgs.services.oms.restmodel.hotel.HotelAmendOrderRequest;


@Component
public class HotelAmendBookingRequestValidator extends HotelPostBookingBaseRequestValidator{

	@Override
	public boolean supports(Class<?> clazz) {
		return HotelAmendOrderRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
	}

}
