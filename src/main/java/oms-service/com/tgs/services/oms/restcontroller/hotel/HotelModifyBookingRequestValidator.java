package com.tgs.services.oms.restcontroller.hotel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.hotel.HotelAmendOrderRequest;


@Component
public class HotelModifyBookingRequestValidator extends HotelPostBookingBaseRequestValidator{
	
	@Autowired
	HotelOrderItemManager itemManager;

	@Override
	public boolean supports(Class<?> clazz) {
		return HotelAmendOrderRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		
		HotelAmendOrderRequest request = (HotelAmendOrderRequest)target;
		List<DbHotelOrderItem> items = itemManager.getItemList(request.getBookingId());
		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(items).build().convert();
		String oldCity = hInfo.getAddress().getCity().getName();
		String oldCountry = hInfo.getAddress().getCountry().getName();
		
		
		HotelInfo updatedHotelInfo = request.getHInfo();
		String newCity = updatedHotelInfo.getAddress().getCity().getName();
		String updatedCountry = updatedHotelInfo.getAddress().getCountry().getName();
		
		if(!oldCity.equalsIgnoreCase(newCity)) 
			rejectValue(errors, "hInfo.address.city", SystemError.CITY_CANNOT_BE_UPDATED);
		
		if(!oldCountry.equalsIgnoreCase(updatedCountry))
			rejectValue(errors, "hInfo.address.country", SystemError.COUNTRY_CANNOT_BE_UPDATED);
			
		
		
	}

}
