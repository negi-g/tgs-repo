package com.tgs.services.oms.jparepository.air;

import com.tgs.services.oms.dbmodel.air.DbAirItemDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AitItemDetailRepository extends JpaRepository<DbAirItemDetail, Long>, JpaSpecificationExecutor<DbAirItemDetail> {

    DbAirItemDetail findByBookingId(String bookingId);
}
