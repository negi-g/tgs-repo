package com.tgs.services.oms.manager.air;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.manager.BookingManager;
import com.tgs.services.oms.mapper.SegmentInfoToAirOrderItemMapper;
import com.tgs.services.oms.restmodel.air.AirImportPnrBookingRequest;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Setter
@Getter
@Qualifier("AirImportPnrBookingManager")
public class AirImportPnrBookingManager extends BookingManager {
	private AirImportPnrBookingRequest airImportPnrBookingRequest;
	protected List<TripInfo> tripInfos;
	protected List<DbAirOrderItem> items;

	protected AirItemStatus itemStatus;

	@Autowired
	protected FMSCommunicator fmsComm;
	@Autowired
	protected AirOrderItemManager itemManager;

	// @Transactional
	public Order processBooking() {
		airImportPnrBookingRequest = (AirImportPnrBookingRequest) bookingRequest;
		this.storeGstInfo();
		this.storeOrderItem();
		this.createOrder();

		/**
		 * After duplicate booking implementation this can be moved to storeOrderItem function itself
		 */
		itemManager.save(items, order, itemStatus);
		log.info("Orderitems successfully stored for bookingId {}", airImportPnrBookingRequest.getBookingId());
		return order;
	}

	@Override
	public void updateItemStatus(OrderStatus orderStatus) {}

	protected void storeOrderItem() {
		items = new ArrayList<>();
		AirSearchQuery searchQuery = fmsComm.getSearchQueryFromTripInfos(tripInfos);
		paymentTotal = 0d;
		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				DbAirOrderItem orderItem = SegmentInfoToAirOrderItemMapper.builder().segmentInfo(segmentInfo)
						.searchQuery(searchQuery).bookingId(airImportPnrBookingRequest.getBookingId())
						.bookingUser(bookingUser).fmsCommunicator(fmsComm)
						.gstInfo(airImportPnrBookingRequest.getGstInfo()).build().convert();
				items.add(orderItem);
				orderTotal += orderItem.getAmount();
				totalMarkup += orderItem.getMarkup();
				totalCommission += AirBookingUtils.getGrossCommission(Arrays.asList(orderItem), bookingUser, true);
				totalTds += AirBookingUtils.getTDS(Arrays.asList(orderItem));
				paymentTotal += AirBookingUtils.getPaymentTotal(Arrays.asList(orderItem), bookingUser);
			}
		}
	}

	@Override
	protected List<PaymentRequest> createVirualPaymentRequest(List<PaymentRequest> paymentInfos,
			GstInfo orderBillingEntity) {
		return paymentInfos;
	}

	@Override
	protected void applyVoucher() {

	}
}
