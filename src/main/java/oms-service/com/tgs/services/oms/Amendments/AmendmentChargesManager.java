package com.tgs.services.oms.Amendments;

import java.util.*;

import com.tgs.services.oms.Amendments.utils.AirAmendmentUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.datamodel.PaxKey;
import com.tgs.services.oms.restmodel.air.AirAmendmentPaxInfo;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.oms.restmodel.air.AmendmentRequest;
import com.tgs.services.oms.datamodel.amendments.AmendmentTrip;
import com.tgs.services.oms.datamodel.amendments.AmendmentTraveller;
import com.tgs.services.oms.restmodel.air.PaxAmendmentCharges;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AmendmentChargesManager {

	public AirAmendmentPaxInfo buildAirAmendmentPaxInfoRequest(String amendmentId, AmendmentRequest req,
			AirOrderInfo airOrderInfo) {
		AirAmendmentPaxInfo request = new AirAmendmentPaxInfo();
		request.setAmendmentId(amendmentId);
		request.setBookingId(req.getBookingId());
		request.setType(req.getType());
		request.setPaxKeys(getKeys(req.getTrips(), airOrderInfo));
		request.setRemarks(req.getRemarks());
		return request;
	}

	public Map<String, Set<PaxKey>> getKeys(List<AmendmentTrip> amendmentTrips, AirOrderInfo airOrderInfo) {
		Map<String, Set<PaxKey>> set = new LinkedHashMap<>();
		List<TripInfo> enquiredTrips = null;
		if (Objects.nonNull(airOrderInfo) && CollectionUtils.isNotEmpty(airOrderInfo.getTripInfoList())) {
			enquiredTrips = getTripsfromAirOrder(amendmentTrips, airOrderInfo.getTripInfoList());
			if (CollectionUtils.isEmpty(enquiredTrips) || amendmentTrips.size() != enquiredTrips.size()) {
				throw new CustomGeneralException(SystemError.SECTOR_DEPT_DATE_INVALID);
			}
		}
		for (TripInfo trip : enquiredTrips) {
			String tripKey = AirAmendmentUtils.createKey(trip.getDepartureAirportCode(), trip.getArrivalAirportCode(),
					trip.getDepartureTime().toLocalDate().toString());
			for (SegmentInfo segment : trip.getSegmentInfos()) {
				set.put(segment.getId(), getPaxId(segment, tripKey, amendmentTrips));
			}

		}
		return set;
	}

	private Set<PaxKey> getPaxId(SegmentInfo segment, String tripKey, List<AmendmentTrip> amendmentTrips) {
		Set<PaxKey> set = new LinkedHashSet<>();
		List<String> travellerKeys = null;
		AmendmentTrip trip = getTrip(amendmentTrips, tripKey);
		if (Objects.nonNull(trip) && CollectionUtils.isNotEmpty(trip.getTravellers())) {
			travellerKeys = getTravellerKeys(trip.getTravellers());
		}
		for (FlightTravellerInfo traveller : segment.getTravellerInfo()) {
			PaxKey paxKey = new PaxKey();
			// String dob = Objects.nonNull(traveller.getDob()) ? traveller.getDob().toString() : "";
			String dob = "";
			if (Objects.nonNull(travellerKeys) && travellerKeys.contains(AirAmendmentUtils
					.createKey(traveller.getFirstName().toUpperCase(), traveller.getLastName().toUpperCase(), dob))) {
				paxKey.setId(traveller.getId());
			}
			if (Objects.isNull(travellerKeys)) {
				paxKey.setId(traveller.getId());
			}
			if (Objects.nonNull(paxKey.getId())) {
				set.add(paxKey);
			}
		}
		return set;
	}

	private List<String> getTravellerKeys(List<AmendmentTraveller> travellers) {
		List<String> travellerKeys = new ArrayList<String>();
		for (AmendmentTraveller traveller : travellers) {
			// String dob = Objects.nonNull(traveller.getDob()) ? traveller.getDob().toString() : "";
			String dob = "";
			travellerKeys.add(AirAmendmentUtils.createKey(traveller.getFirstName().toUpperCase(),
					traveller.getLastName().toUpperCase(), dob));
		}
		return travellerKeys;
	}

	private AmendmentTrip getTrip(List<AmendmentTrip> trips, String tripKey) {
		if (CollectionUtils.isNotEmpty(trips)) {
			for (AmendmentTrip trip : trips) {
				if (tripKey.equals(AirAmendmentUtils.createKey(trip.getSource().toUpperCase(),
						trip.getDestination().toUpperCase(), trip.getDepartureDate().toString()))) {
					return trip;
				}

			}
		}
		return null;
	}

	private List<TripInfo> getTripsfromAirOrder(List<AmendmentTrip> trips, List<TripInfo> tripInfoList) {
		List<TripInfo> tripList = new ArrayList<TripInfo>();
		for (TripInfo tripInfo : tripInfoList) {
			String tripKey = AirAmendmentUtils.createKey(tripInfo.getDepartureAirportCode(),
					tripInfo.getArrivalAirportCode(), tripInfo.getDepartureTime().toLocalDate().toString());
			if (CollectionUtils.isNotEmpty(trips)) {
				for (AmendmentTrip trip : trips) {
					String key = AirAmendmentUtils.createKey(trip.getSource().toUpperCase(),
							trip.getDestination().toUpperCase(), trip.getDepartureDate().toString());
					if (tripKey.equalsIgnoreCase(key)) {
						tripList.add(tripInfo);
					}
				}
			} else {
				tripList.add(tripInfo);
			}
		}
		return tripList;
	}


}
