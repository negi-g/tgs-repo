package com.tgs.services.oms.servicehandler.hotel;


import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.Amendments.HotelAmendmentHandlerFactory;
import com.tgs.services.oms.Amendments.Processors.HotelAmendmentProcessor;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.hotel.ProcessHotelAmendmentRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProcessHotelAmendmentHandler extends ServiceHandler<ProcessHotelAmendmentRequest, AmendmentResponse> {

	@Autowired
	private AmendmentService service;

	@Autowired
	private AmendmentActionValidator actionValidator;

	@Override
	public void beforeProcess() {}

	@Transactional
	@Override
	public void process() {
		try {
			Double differentialPayment = null;

			Amendment amendment = service.findByAmendmentId(request.getAmendmentId());
			HotelAmendmentProcessor amendmentHandler =
					HotelAmendmentHandlerFactory.initHandler(amendment, request.getModifiedInfo(), request.getNote());
			amendmentHandler.process();

			differentialPayment = amendment.getAmount();

			amendment.setStatus(AmendmentStatus.PROCESSING);
			service.save(amendment);
			amendment.setActionList(
					actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment));
			response.setAmendmentItems(Arrays.asList(amendment));
			response.setDifferentialPayment(differentialPayment);
		} catch (Exception ex) {
			log.info("Exception while processing amendment for amendment id {} and request {}",
					request.getAmendmentId(), GsonUtils.getGson().toJson(request), ex);
			throw ex;
		}
	}

	@Override
	public void afterProcess() {}
}
