package com.tgs.services.oms.Amendments;

import static com.tgs.services.base.enums.AmendmentType.CORRECTION;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.constraints.NotNull;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.AmendmentFilter;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.oms.Amendments.Processors.RailAmendmentProcessor;
import com.tgs.services.oms.datamodel.InvoiceType;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.rail.RailItemStatus;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.mapper.rail.RailOrderItemToJourneyInfoMapper;
import com.tgs.services.oms.restmodel.rail.RailAmendmentDetailResponse;
import com.tgs.services.oms.restmodel.rail.RailInvoiceRequest;
import com.tgs.services.oms.restmodel.rail.RailInvoiceResponse;
import com.tgs.services.oms.servicehandler.rail.RailInvoiceHandler;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailAmendmentManager {

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	private GeneralServiceCommunicator generalService;

	@Autowired
	private RailInvoiceHandler railInvoiceHandler;

	@Autowired
	private RailOrderItemService itemService;

	@Autowired
	private RailCommunicator railComm;

	@Autowired
	private UserServiceCommunicator userService;

	public RailAmendmentDetailResponse getAmendmentDetail(@NotNull String amendmentId) {

		Amendment amendment = amendmentService.findByAmendmentId(amendmentId);

		if (amendment == null)
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);

		DbRailOrderItem orgOrderItem = itemService.findByBookingId(amendment.getBookingId());

		User bookingUser = userService.getUserFromCache(amendment.getBookingUserId());

		RailAmendmentProcessor amendmentHandler =
				RailAmendmentHandlerFactory.initHandler(amendment, new DbRailOrderItem(), orgOrderItem, bookingUser);

		RailOrderItem orderItem = amendmentHandler.patchRailOrderItem(false);

		orgOrderItem = new DbRailOrderItem().from(orderItem);

		RailJourneyInfo journeyInfo =
				RailOrderItemToJourneyInfoMapper.builder().railComm(railComm).item(orgOrderItem).build().convert();

		List<Note> notes = generalService.getNotes(NoteFilter.builder().amendmentId(amendmentId).build());

		RailAmendmentDetailResponse response = RailAmendmentDetailResponse.builder().amendment(amendment)
				.journeyInfo(journeyInfo).priceInfo(orgOrderItem.getAdditionalInfo().getPriceInfo())
				.note(CollectionUtils.isEmpty(notes) ? "" : notes.get(0).getNoteMessage()).build();

		return response;

	}


	public RailInvoiceResponse getAmendmentInvoiceDetails(String amendmentId, String invoiceId) throws Exception {
		Amendment amendment = amendmentService.findByAmendmentId(amendmentId);

		RailOrderItem amendmentSnapshot =
				getAmendmentSnapshot(amendment, !amendment.getAmendmentType().equals(CORRECTION));
		RailInvoiceRequest invoiceRequest =
				new RailInvoiceRequest(amendment.getBookingId(), InvoiceType.AGENCY, amendmentSnapshot);
		invoiceRequest.setInvoiceId(invoiceId);
		railInvoiceHandler.initData(invoiceRequest, new RailInvoiceResponse());
		RailInvoiceResponse invoiceResponse = railInvoiceHandler.getResponse();
		if (amendment != null) {
			log.debug("Fetching invoice details for amendmentId {}", amendment.getAmendmentId());
			invoiceResponse.setPriceInfo(
					amendment.getRailAdditionalInfo().getOrderCurrentSnapshot().getAdditionalInfo().getPriceInfo());
			filterPassengers(invoiceResponse.getJourneyInfo(), amendment);
		}
		invoiceResponse.setAmendmentId(amendment.getAmendmentId());
		invoiceResponse.setCreatedOn(amendment.getProcessedOn());
		invoiceResponse.setAmendmentType(amendment.getAmendmentType());
		return invoiceResponse;
	}


	private void filterPassengers(RailJourneyInfo journeyInfo, Amendment amendment) {
		log.info("Filtering passengers for Invoice for amendmentId {} and paxKeys {}", amendment.getAmendmentId(),
				amendment.getRailAdditionalInfo().getPaxKeys());
		if (Objects.nonNull(amendment.getRailAdditionalInfo().getPaxKeys())
				&& CollectionUtils.isNotEmpty(amendment.getRailAdditionalInfo().getPaxKeys())) {
			Iterator<RailTravellerInfo> itr = journeyInfo.getBookingRelatedInfo().getTravellerInfos().iterator();
			while (itr.hasNext()) {
				if (!amendment.getRailAdditionalInfo().getPaxKeys().contains(String.valueOf(itr.next().getId()))) {
					itr.remove();
				}
			}
		}
	}


	public RailOrderItem getAmendmentSnapshot(Amendment originalAmendment, boolean keepOldTf) {

		Amendment amendment = new GsonMapper<Amendment>(originalAmendment, null, Amendment.class).convert();

		if (SystemContextHolder.getContextData().getEmulateOrLoggedInUserId() != null)
			UserServiceHelper.checkAndReturnAllowedUserId(
					SystemContextHolder.getContextData().getEmulateOrLoggedInUserId(),
					Arrays.asList(amendment.getBookingUserId()));

		if (amendment.getStatus().equals(AmendmentStatus.SUCCESS)) {

			RailOrderItem previousOrderSnapshot =
					filterPaxItems(amendment, amendment.getRailAdditionalInfo().getOrderPreviousSnapshot());
			RailOrderItem currentOrderSnapshot =
					filterPaxItems(amendment, amendment.getRailAdditionalInfo().getOrderCurrentSnapshot());

			Map<RailFareComponent, Double> newFcs =
					currentOrderSnapshot.getAdditionalInfo().getPriceInfo().getFareComponents();
			Map<RailFareComponent, Double> oldFcs =
					previousOrderSnapshot.getAdditionalInfo().getPriceInfo().getFareComponents();
			Stream.of(RailFareComponent.values()).forEach(fc -> {
				if (fc.equals(RailFareComponent.TF) && keepOldTf) {
					newFcs.put(fc, oldFcs.get(RailFareComponent.TF));
					return;
				}
				newFcs.put(fc, newFcs.getOrDefault(fc, 0d) - oldFcs.getOrDefault(fc, 0d));
			});
			return currentOrderSnapshot;
		} else
			throw new CustomGeneralException(SystemError.AMENDMENT_IN_PROGRESS);

	}


	private RailOrderItem filterPaxItems(Amendment amendment, RailOrderItem orderItemSnapshot) {
		return orderItemSnapshot;
	}

	public void checkAmendStatus(String bookingId, Set<String> paxKeys) {
		Set<String> paxIds = new HashSet<String>();
		AmendmentFilter filter = AmendmentFilter.builder().bookingIdIn(Collections.singletonList(bookingId)).build();

		List<Amendment> amendments = amendmentService.search(filter).stream().filter(
				a -> !a.getStatus().equals(AmendmentStatus.SUCCESS) && !a.getStatus().equals(AmendmentStatus.REJECTED))
				.collect(Collectors.toList());
		if (CollectionUtils.isEmpty(amendments)) {
			return;
		}

		for (Amendment amd : amendments) {
			if (amd.getRailAdditionalInfo().getPaxKeys() != null)
				paxIds.addAll(amd.getRailAdditionalInfo().getPaxKeys());
		}
		if (CollectionUtils.isEmpty(paxIds) && CollectionUtils.isEmpty(paxKeys) && amendments.size() > 0) {
			throw new CustomGeneralException(SystemError.AMENDMENT_EXISTS);
		}
		checkPaxAmendStatus(paxKeys, paxIds);
	}

	private void checkPaxAmendStatus(Set<String> paxKeys, Set<String> paxIds) {


		if (CollectionUtils.isEmpty(paxKeys) && !CollectionUtils.isEmpty(paxIds)) {
			throw new CustomGeneralException(SystemError.AMENDMENT_EXISTS);
		}
		if (!CollectionUtils.isEmpty(paxKeys) && CollectionUtils.isEmpty(paxIds)) {
			throw new CustomGeneralException(SystemError.AMENDMENT_EXISTS);
		}
		for (String key : paxKeys) {
			if (paxIds.contains(key)) {
				throw new CustomGeneralException(SystemError.AMENDMENT_EXISTS);
			}
		}

	}


	public boolean checkPendingOrder(AmendmentType amendmentType, DbRailOrderItem orderItem) {
		boolean orderPending = false;
		if (!RailItemStatus.getRailItemStatus(orderItem.getStatus()).validAmdTypes().contains(amendmentType)) {
			orderPending = true;
		}
		return orderPending;
	}

}
