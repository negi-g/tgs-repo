package com.tgs.services.oms.hibernate;

import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;

public class DeliveryInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return DeliveryInfo.class;
	}
}
