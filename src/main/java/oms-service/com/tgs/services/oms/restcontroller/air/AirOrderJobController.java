package com.tgs.services.oms.restcontroller.air;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tgs.services.oms.servicehandler.air.AirHoldBookingNotificationHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.servicehandler.air.AirUnConfirmBookingHandler;

@RequestMapping("/oms/v1/air/job")
@RestController
public class AirOrderJobController {

	@Autowired
	AirUnConfirmBookingHandler airOrderJobHandler;

	@Autowired
	AirHoldBookingNotificationHandler notificationHandler;

	@RequestMapping(value = "/cancel-unconfirmed-booking", method = RequestMethod.GET)
	protected BaseResponse payLater(HttpServletRequest request, HttpServletResponse response) {
		airOrderJobHandler.processCancellationJob();
		return new BaseResponse();
	}


	@RequestMapping(value = "/notify-unconfirmed-booking/{minsFrom}/{minsTo}", method = RequestMethod.POST)
	protected BaseResponse notifyFirst(HttpServletRequest request, @PathVariable Integer minsFrom,
			@PathVariable Integer minsTo) {
		notificationHandler.processNotificationJob(minsFrom, minsTo);
		return new BaseResponse();
	}
}
