package com.tgs.services.oms.hibernate.hotel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.oms.datamodel.hotel.HotelBookingReconciliationAdditionalInfo;

public class HotelReconciliationAdditionalInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return HotelBookingReconciliationAdditionalInfo.class;
	}
}
