package com.tgs.services.oms.servicehandler.rail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AttachmentMetadata;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.MessageMedium;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import org.springframework.beans.factory.annotation.Value;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.rail.datamodel.message.RailEmailAttributes;
import com.tgs.services.rail.datamodel.RailJourneyClass;
import com.tgs.services.rail.datamodel.RailPaxType;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.datamodel.booking.AdditionalRailOrderItemInfo;
import com.tgs.services.rail.datamodel.RailBookingQuota;
import com.tgs.services.rail.restmodel.RailMessageRequest;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailOrderMessageHandler extends ServiceHandler<RailMessageRequest, BaseResponse> {

	@Autowired
	protected MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	protected UserServiceCommunicator userService;

	@Autowired
	protected OrderManager orderManager;

	@Autowired
	protected RailOrderItemService itemService;

	@Value("${bookingUrl}")
	private String bookingUrl;

	@Override
	public void beforeProcess() throws Exception {
		if (request.getMode().equals(MessageMedium.EMAIL)) {
			Order order = new GsonMapper<>(orderManager.findByBookingId(request.getBookingId()), Order.class).convert();
			if (OrderStatus.SUCCESS.equals(order.getStatus()))
				sendBookingEmail(order, EmailTemplateKey.RAIL_BOOK_EMAIL);
			else if (OrderStatus.FAILED.equals(order.getStatus()))
				sendBookingEmail(order, EmailTemplateKey.RAIL_FAILED_EMAIL);
		} else if (request.getMode().equals(MessageMedium.SMS)) {
			Order order = new GsonMapper<>(orderManager.findByBookingId(request.getBookingId()), Order.class).convert();
			if (OrderStatus.SUCCESS.equals(order.getStatus()))
				sendBookingSms(order, SmsTemplateKey.RAIL_BOOKING_SUCCESS_SMS);
			else if (OrderStatus.FAILED.equals(order.getStatus()))
				sendBookingSms(order, SmsTemplateKey.RAIL_BOOKING_FAILED_SMS);
		}

	}

	@Override
	public void process() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	public void sendBookingEmail(Order order, EmailTemplateKey templateKey) {
		try {
			ExecutorUtils.getCommunicationThreadPool().submit(() -> {
				if (request != null
						|| (order != null && CollectionUtils.isNotEmpty(order.getDeliveryInfo().getEmails()))) {
					RailEmailAttributes messageAttributes = getMessageAttributes(templateKey, order);
					ExecutorService executor = Executors.newSingleThreadExecutor();
					ContextData contextData = SystemContextHolder.getContextData();
					Future<?> future = executor.submit(() -> generatePdf(messageAttributes, order, contextData));
					try {
						future.get(30, TimeUnit.SECONDS);
					} catch (InterruptedException | ExecutionException | TimeoutException e) {
						log.error("Unable to generate PDF for bookingid in 30 seconds {}", order.getBookingId(), e);
						future.cancel(true);
					}
					executor.shutdownNow();
					sendEmail(order, messageAttributes);
				}
			});
		} catch (Exception ex) {
			log.error("Error while sending message for bookingId {}. Ex: {}", order.getBookingId(), ex.getMessage());
		}
	}

	public void sendBookingSms(Order order, SmsTemplateKey templateKey) {
		AbstractMessageSupplier<SmsAttributes> msgAttributes = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				Map<String, String> attributes = new HashMap<>();
				attributes.put("bookingId", order.getBookingId());
				User bookingUser = userService.getUserFromCache(order.getBookingUserId());
				attributes.put("contact", bookingUser.getMobile());
				if (templateKey.equals(SmsTemplateKey.RAIL_BOOKING_SUCCESS_SMS)) {
					DbRailOrderItem orderItem = itemService.findByBookingId(order.getBookingId());
					AdditionalRailOrderItemInfo additionalinfo = orderItem.getAdditionalInfo();
					attributes.put("bookingUser", bookingUser.getName());
					attributes.put("pnr", additionalinfo.getPnr());
					attributes.put("fromstn", orderItem.getFromStn());
					attributes.put("tostn", orderItem.getToStn());
					attributes.put("trainno", additionalinfo.getRailInfo().getNumber());
					attributes.put("trainname", additionalinfo.getRailInfo().getName());
					attributes.put("jclass", additionalinfo.getJourneyClass().toString().replaceAll("_", " "));
					attributes.put("jclasscode",
							RailJourneyClass.getCodeFromEnumName(additionalinfo.getJourneyClass().toString()));
					attributes.put("quota", RailBookingQuota.getEnumFromCode(additionalinfo.getQuota().toString())
							.toString().replaceAll("_", " "));

					List<RailTravellerInfo> paxNames = orderItem.getTravellerInfo().stream().filter(
							t -> t.getPaxType().equals(RailPaxType.ADULT) || t.getPaxType().equals(RailPaxType.SENIOR))
							.collect(Collectors.toList());

					String paxname = paxNames.get(0).getFullName();
					Integer paxcount = orderItem.getTravellerInfo().size() - 1;
					;
					StringBuffer pax = new StringBuffer();
					if (paxcount > 0) {
						pax.append(paxname).append("+").append(paxcount);
					} else {
						pax.append(paxname);
					}
					attributes.put("paxname", pax.toString());
					StringJoiner joinPax = new StringJoiner(",");
					for (RailTravellerInfo info : orderItem.getTravellerInfo()) {
						StringJoiner joinSts = new StringJoiner("/");
						if (Objects.nonNull(info.getCurrentSeatAllocation())) {
							joinSts.add(StringUtils.defaultIfEmpty(info.getCurrentSeatAllocation().getCoachId(), ""));
							
							if (Objects.nonNull(info.getCurrentSeatAllocation().getBerthNo())) {
								joinSts.add(info.getCurrentSeatAllocation().getBerthNo().toString());
							} else {
								joinSts.add("");
							}
							
							if (StringUtils.isNotBlank(info.getCurrentSeatAllocation().getBookingStatus())) {
								joinSts.add(info.getCurrentSeatAllocation().getBookingStatus());
							}
							
							joinPax.add(joinSts.toString());
						}
					}
					attributes.put("seatsts", joinPax.toString());
					attributes.put("doj", DateFormatterHelper.formatDateTime(
							ObjectUtils.firstNonNull(additionalinfo.getBoardingTime(), orderItem.getDepartureTime()),
							DateFormatType.BOOKING_EMAIL_FORMAT));

				}

				log.debug("Attributes of bookingid {} with sms attributes as {}", order.getBookingId(), attributes);
				return SmsAttributes.builder().key(templateKey.name())
						.recipientNumbers(OrderUtils.getRailReceipientNumbers(order, bookingUser, request))
						.attributes(attributes).partnerId(order.getPartnerId()).role(bookingUser.getRole()).build();
			}
		};
		if (OrderUtils.isMessageSendingRequired(templateKey.name(), MessageMedium.SMS,
				userService.getUserFromCache(order.getBookingUserId()), order))
			msgSrvCommunicator.sendMessage(msgAttributes.getAttributes());

	}

	public void sendEmail(Order order, RailEmailAttributes messageAttributes) {
		if (OrderUtils.isMessageSendingRequired(messageAttributes.getKey(), MessageMedium.EMAIL,
				userService.getUserFromCache(order.getBookingUserId()), order)) {
			msgSrvCommunicator.sendMail(messageAttributes);
		}
	}

	public RailEmailAttributes getMessageAttributes(EmailTemplateKey templateKey, Order order) {
		AbstractMessageSupplier<RailEmailAttributes> emailAttributeSupplier = new AbstractMessageSupplier<RailEmailAttributes>() {
			@Override
			public RailEmailAttributes get() {
				User bookingUser = userService.getUserFromCache(order.getBookingUserId());
				String toEmailId = OrderUtils.getToEmailId(order, bookingUser, request);
				Double calculatedTotalFare = 0.0;
				log.info("Sending Email to emailIds {}", toEmailId);
				RailEmailAttributes messageAttributes = RailEmailAttributes.builder().build();
				messageAttributes.setBookingId(order.getBookingId());
				messageAttributes.setToEmailId(toEmailId);
				messageAttributes.setKey(templateKey.name());
				if (OrderStatus.SUCCESS.equals(order.getStatus())) {
					DbRailOrderItem orderItem = itemService.findByBookingId(order.getBookingId());
					messageAttributes.setPnr(orderItem.getAdditionalInfo().getPnr());
					messageAttributes.setTrainName(orderItem.getAdditionalInfo().getRailInfo().getName());
					messageAttributes.setTrainNumber(orderItem.getAdditionalInfo().getRailInfo().getNumber());
					messageAttributes.setSector(orderItem.getFromStn().concat("/").concat(orderItem.getToStn()));

					if (orderItem.getAdditionalInfo() != null && orderItem.getAdditionalInfo().getPriceInfo() != null
							&& MapUtils.isNotEmpty(orderItem.getAdditionalInfo().getPriceInfo().getFareComponents())) {
						calculatedTotalFare = orderItem.getAdditionalInfo().getPriceInfo().getFareComponents()
								.getOrDefault(RailFareComponent.TF, 0.0)
								+ (orderItem.getAdditionalInfo().getPriceInfo().getFareComponents()
										.getOrDefault(RailFareComponent.DMF, 0.0)
										- orderItem.getAdditionalInfo().getPriceInfo().getFareComponents()
												.getOrDefault(RailFareComponent.MF, 0.0))
								+ (orderItem.getAdditionalInfo().getPriceInfo().getFareComponents()
										.getOrDefault(RailFareComponent.DPC, 0.0)
										- orderItem.getAdditionalInfo().getPriceInfo().getFareComponents()
												.getOrDefault(RailFareComponent.PC, 0.0));
					}

					messageAttributes.setTotalFare(calculatedTotalFare.toString());

					List<String> paxNames = orderItem.getTravellerInfo().stream().map(t -> t.getFullName())
							.collect(Collectors.toList());
					messageAttributes.setPaxName(paxNames.toString());
					messageAttributes.setBoardingStation(ObjectUtils
							.firstNonNull(orderItem.getAdditionalInfo().getBoardingStation(), orderItem.getFromStn()));
					messageAttributes
							.setDepartureTime(
									DateFormatterHelper.formatDateTime(
											ObjectUtils.firstNonNull(orderItem.getAdditionalInfo().getBoardingTime(),
													orderItem.getDepartureTime()),
											DateFormatType.BOOKING_EMAIL_FORMAT));
				}
				return messageAttributes;
			}
		};
		return emailAttributeSupplier.getAttributes();
	}

	private void generatePdf(RailEmailAttributes messageAttributes, Order order, ContextData contextData) {
		String url = bookingUrl.concat("?bookingId=").concat(order.getBookingId());
		url = StringUtils.join(url, "&product=rail");
		log.debug("URL to generate booking confirmation pdf is {} ", url);
		String fileName = "Booking_Details_" + order.getBookingId() + ".pdf";
		Map<String, String> headerParams = getHeaderParams(contextData);
		HttpUtils httpUtils = HttpUtils.builder().urlString(url).headerParams(headerParams).timeout(30000).build();
		try {
			byte[] readData = httpUtils.getByteArrayFromInputStream();
			AttachmentMetadata attachmentData = AttachmentMetadata.builder().fileData(readData)
					.fileName(fileName.toString()).build();
			messageAttributes.setAttachmentData(attachmentData);
		} catch (Exception e) {
			log.info("Unable to generate PDF for url {} , filename {} ", url, fileName, e);
		}
		log.debug("[SendGridES] HtmltoPdf generated");
	}

	private Map<String, String> getHeaderParams(ContextData contextData) {
		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("Accept-Encoding", "gzip");
		headerParams.put("Content-Encoding", "gzip");
		headerParams.put("deviceid", "System");
		headerParams.put("Accept", "*/*");
		headerParams.put("Authorization", contextData.getHttpHeaders().getJwtToken());
		headerParams.put("allowexternal", "true");
		return headerParams;
	}

}
