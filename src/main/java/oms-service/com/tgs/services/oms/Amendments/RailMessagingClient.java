package com.tgs.services.oms.Amendments;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.MessageMedium;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.message.RailAmendmentMailAttributes;
import com.tgs.services.ums.datamodel.User;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailMessagingClient {

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	private RailOrderItemService railOrderItemService;
	
	@Autowired
	private OrderManager orderManager;

	private Order order;

	private DbRailOrderItem orderItem;

	private User user;

	public void sendMail(Amendment amendment, List<EmailTemplateKey> emailTemplateKeys) {
		AbstractMessageSupplier<RailAmendmentMailAttributes> msgAttributes =
				new AbstractMessageSupplier<RailAmendmentMailAttributes>() {
					@Override
					public RailAmendmentMailAttributes get() {
						setDependencies(amendment);
						List<String> travellers = orderItem.getTravellerInfo().stream().filter(
								t -> amendment.getRailAdditionalInfo().getPaxKeys().contains(t.getId().toString()))
								.map(t -> t.getFullName()).collect(Collectors.toList());
						RailAmendmentMailAttributes mailAttributes = RailAmendmentMailAttributes.builder().build();
						mailAttributes.setPnr(orderItem.getAdditionalInfo().getPnr());
						mailAttributes.setBookingId(amendment.getBookingId());
						mailAttributes.setToEmailId(user.getEmail());
						mailAttributes.setRole(user.getRole());
						mailAttributes.setPartnerId(amendment.getPartnerId());
						mailAttributes.setReason(
								amendment.getAdditionalInfo().getRailAdditionalInfo().getTdrInfo().getReason());
						mailAttributes.setSector(orderItem.getFromStn().concat("/").concat(orderItem.getToStn()));
						mailAttributes.setTrainName(orderItem.getAdditionalInfo().getRailInfo().getName());
						mailAttributes.setTrainNumber(orderItem.getAdditionalInfo().getRailInfo().getNumber());
						mailAttributes.setPaxName(travellers.toString());
						mailAttributes.setAmount(orderItem.getAdditionalInfo().getPriceInfo().getFareComponents()
								.getOrDefault(RailFareComponent.AAR, 0.0).toString());
						if (amendment.getCreatedOn() != null) {
							mailAttributes.setGenerationTime(DateFormatterHelper
									.formatDateTime(amendment.getCreatedOn(), DateFormatType.AMENDMENT_EMAIL_FORMAT));
						}
						mailAttributes.setKeys(emailTemplateKeys);
						return mailAttributes;
					}
				};
				if (OrderUtils.isMessageSendingRequired(msgAttributes.get().getKey(), MessageMedium.EMAIL, user, order)) {
					msgSrvCommunicator.sendMail(msgAttributes.getAttributes());
				}	
	}

	public void sendSMS(Amendment amendment, SmsTemplateKey smsTemplateKey) {
		AbstractMessageSupplier<SmsAttributes> msgAttributes = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				setDependencies(amendment);
				Map<String, String> attributeMap = new HashMap<>();
				attributeMap.put("amdId", amendment.getAmendmentId());
				attributeMap.put("bookingId", amendment.getBookingId());
				attributeMap.put("pnr",orderItem.getPnr());
				attributeMap.put("amount",amendment.getAmount().toString());
				attributeMap.put("paxnames",pax(amendment));
				
				SmsAttributes smsAttributes = SmsAttributes.builder().key(smsTemplateKey.name())
						.recipientNumbers(OrderUtils.getRailReceipientNumbers(order, user, null)).attributes(attributeMap).partnerId(amendment.getPartnerId())
						.role(user.getRole()).build();
				log.debug("Attributes for amendment sms of type {} with attributes {}",amendment.getAmendmentType(),attributeMap);
				return smsAttributes;
			}
		};
		
		if (OrderUtils.isMessageSendingRequired(msgAttributes.get().getKey(), MessageMedium.SMS, user, order)) {
			msgSrvCommunicator.sendMessage(msgAttributes.getAttributes());
		}
	}

	private void setDependencies(Amendment amendment) {
		 user = userService.getUserFromCache(amendment.getBookingUserId());
		 orderItem = railOrderItemService.findByBookingId(amendment.getBookingId());
		 order = orderManager.findByBookingId(amendment.getBookingId());
	}
	private String pax(Amendment amendment) {
		StringBuilder paxString = new StringBuilder();
		List<String> travellers = orderItem.getTravellerInfo().stream().filter(
				t -> amendment.getRailAdditionalInfo().getPaxKeys().contains(t.getId().toString()))
				.map(t -> t.getFullName()).collect(Collectors.toList());
		travellers.forEach(
				pax -> paxString.append(pax).append(","));
		return paxString.substring(0, paxString.length() - 1);
	}

}
