package com.tgs.services.oms.servicehandler.rail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.user.BasePaymentUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAdditionalInfo;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.rail.RailItemStatus;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.rail.RailOrderItemManager;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.rail.RailBookingUtils;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.ruleengine.RefundOutput;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailAbortHandler {


	@Autowired
	protected OrderManager orderManager;

	@Autowired
	protected PaymentServiceCommunicator paymentServiceCommunicator;

	@Autowired
	protected RailOrderItemService itemService;

	@Autowired
	protected UserServiceCommunicator userService;


	@Autowired
	protected RailOrderItemManager itemManager;


	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public void abortBooking(String bookingId) {
		log.info("Aborting booking for bookingId {}", bookingId);
		Order order = orderManager.findByBookingId(bookingId, null);
		if (order == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		DbRailOrderItem orderItem = itemService.findByBookingId(bookingId);

		order.setPayments(paymentServiceCommunicator.fetchPaymentByBookingId(order.getBookingId()).stream()
				.filter(x -> x.getStatus().equals(PaymentStatus.SUCCESS)).collect(Collectors.toList()));
		if (CollectionUtils.isNotEmpty(order.getPayments())) {
			boolean isPaymentReversed = reversePayments(order);
			if (isPaymentReversed) {
				if (order.getAdditionalInfo() == null) {
					order.setAdditionalInfo(OrderAdditionalInfo.builder().build());
				}
				order.getAdditionalInfo().setPaymentStatus(PaymentStatus.REFUND_SUCCESS);
			}
		}
		orderItem.setStatus(RailItemStatus.ABORTED.getCode());
		order.setStatus(OrderStatus.ABORTED);
		itemManager.save(orderItem, order);
	}

	private boolean reversePayments(Order order) {
		Double amountToReverse = 0.0;
		boolean isPaymentReversed = false;
		for (Payment payment : order.getPayments()) {
			if (payment.getType().equals(PaymentTransactionType.PAID_FOR_ORDER)) {
				amountToReverse += payment.getAmount().doubleValue();
			}
		}
		if (OrderUtils.isPaymentAlreadyDone(order, PaymentTransactionType.REVERSE, new BigDecimal(amountToReverse)))
			return true;
		else {
			List<PaymentRequest> paymentRequests = createReversePayments(order);
			List<PaymentRequest> clubbedPayments = BasePaymentUtils.club(paymentRequests);
			List<Payment> reversePayments = paymentServiceCommunicator.doPaymentsUsingPaymentRequests(clubbedPayments);
			order.getPayments().addAll(reversePayments);
			if (CollectionUtils.isNotEmpty(reversePayments)) {
				isPaymentReversed = true;
			}
		}
		return isPaymentReversed;
	}

	private List<PaymentRequest> createReversePayments(Order order) {
		List<PaymentRequest> paymentRequestList = new ArrayList<>();
		for (Payment p : order.getPayments()) {
			if (p.getAmount().compareTo(BigDecimal.ZERO) != 0) {
				if (p.getOpType().equals(PaymentOpType.CREDIT)) {
					PaymentTransactionType txnType =
							BasePaymentUtils.getReverseTransactionType(PaymentOpType.DEBIT, p.getType());
					paymentRequestList.add(RailBookingUtils.createAPHPayments(p, txnType, p.getAmount()));
					continue;
				}
				PaymentFact fact = PaymentFact.builder().medium(p.getPaymentMedium()).build();
				List<PaymentConfigurationRule> rules = paymentServiceCommunicator.getApplicableRulesOutput(fact,
						PaymentRuleType.REFUND, p.getPaymentMedium());
				PaymentMedium reverseMedium = PaymentMedium.FUND_HANDLER;
				if (!CollectionUtils.isEmpty(rules)) {
					reverseMedium = ((RefundOutput) rules.get(0).getOutput()).getMedium();
				}
				if (reverseMedium.equals(PaymentMedium.FUND_HANDLER)) {
					paymentRequestList.add(RailBookingUtils.createFundHandlerRequests(p, PaymentTransactionType.REVERSE,
							p.getAmount()));
				} else {
					p.setPaymentMedium(reverseMedium);
					paymentRequestList.add(
							RailBookingUtils.createRefundPayments(p, PaymentTransactionType.REVERSE, p.getAmount()));
				}
			}
		}
		return paymentRequestList;
	}
}
