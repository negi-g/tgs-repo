package com.tgs.services.oms.servicehandler.hotel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelModifyBookingManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.manager.hotel.HotelPostBookingManager;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.ruleengine.RefundOutput;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelModifyBookingHandler extends ServiceHandler<HotelPostBookingBaseRequest, BookingResponse> {
	
	@Autowired
	HotelPostBookingManager manualBookingManager;
	
	@Autowired
	HotelModifyBookingManager modifyBookingManager;
	
	@Autowired
	private HotelOrderItemManager itemManager;
	
	@Autowired
	private OrderManager orderManager;
	
	@Autowired
	private PaymentServiceCommunicator pmsComm;
	
	@Autowired
	HMSCachingServiceCommunicator cachingService;
	
	@Override
	public void beforeProcess() throws Exception {
		
		log.info("Request is {}", GsonUtils.getGson().toJson(request));
		
		if(StringUtils.isEmpty(request.getBookingId()))
			throw new CustomGeneralException(SystemError.KEYS_EXPIRED);
		
		Order order = orderManager.findByBookingId(request.getBookingId(), null);
		if(order == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		
		if(!(order.getStatus() == OrderStatus.SUCCESS 
				|| order.getStatus() == OrderStatus.ON_HOLD))
			throw new CustomGeneralException(SystemError.BOOKING_CANNOT_BE_UPDATED);
		
	}

	@Override
	public void process() throws Exception {
		
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(request.getBookingId()).namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).compress(false).bins(new String[] {BinName.HOTELMODIFY.getName()})
				.build();
		HotelOrder updatedHotelOrder  = cachingService.fetchValue(HotelOrder.class, metaInfo);
		Order oldOrder = orderManager.findByBookingId(request.getBookingId(), null);
		Order updatedOrder = updatedHotelOrder.getOrder();
		if(updatedOrder.getStatus() == OrderStatus.SUCCESS)
			makePayment(updatedOrder, oldOrder);
		HotelItemStatus oldItemStatus = oldOrder.getStatus() == OrderStatus.SUCCESS 
				? HotelItemStatus.SUCCESS : HotelItemStatus.ON_HOLD;
		List<HotelOrderItem> items = updatedHotelOrder.getItems();
		updateStatus(items, oldItemStatus);
		List<DbHotelOrderItem> dbItems = new DbHotelOrderItem().toDbList(items);
		orderManager.save(updatedOrder);
		itemManager.save(dbItems, updatedOrder, oldItemStatus);
		
	}
	

	private void updateStatus(List<HotelOrderItem> items ,HotelItemStatus itemStatus) {
		
		for(HotelOrderItem item : items) {
			item.setStatus(itemStatus.getCode());
		}
		
	}
	
	private void makePayment(Order updatedOrder, Order oldOrder) {
		
		String bookingId = updatedOrder.getBookingId();
		
		Double diffPayment = updatedOrder.getAmount() - oldOrder.getAmount();
		if(diffPayment == 0) return;
		boolean isRefund = diffPayment < 0;
		
		log.info("Going for payment as isRefund is {} , diffPayment is {} for bookingId {}"
				, isRefund, diffPayment, oldOrder.getBookingId());
		
		PaymentAdditionalInfo additionalInfo =
				PaymentAdditionalInfo.builder().allowForDisabledMediums(true).build();
		List<PaymentRequest> paymentRequestList = new ArrayList<>();
		PaymentRequest paymentRequest = WalletPaymentRequest.builder().build()
				.setAmount(Math.abs(diffPayment)).setProduct(Product.getProductFromId(bookingId))
				.setOpType(isRefund ? PaymentOpType.CREDIT : PaymentOpType.DEBIT)
				.setPayUserId(oldOrder.getBookingUserId()).setRefId(bookingId)
				.setTransactionType(
						isRefund ? PaymentTransactionType.REFUND : PaymentTransactionType.PAID_FOR_ORDER)
				.setAdditionalInfo(additionalInfo);
		
		if (isRefund) {
			evalRefundMedium(paymentRequest, oldOrder);
		}
		paymentRequestList.add(paymentRequest);
		doPayment(paymentRequestList);
		
	}
	
	@Transactional
	private void doPayment(List<PaymentRequest> paymentRequestList) {
		pmsComm.doPaymentsUsingPaymentRequests(paymentRequestList);
	}

	
	private void evalRefundMedium(PaymentRequest paymentRequest, Order order) {
		PaymentMedium medium = PaymentMedium.FUND_HANDLER;
		List<Payment> paymentList = pmsComm.search(PaymentFilter.builder()
				.refId(order.getBookingId()).type(PaymentTransactionType.PAID_FOR_ORDER).build());

		paymentList.sort(Comparator.comparing(Payment::getCreatedOn));
		PaymentMedium bookingMedium = paymentList.get(0).getPaymentMedium();
		BigDecimal extMediumAmount = Payment.sum(paymentList.stream()
				.filter(p -> p.getPaymentMedium().isExternalPaymentMedium()).collect(Collectors.toList()));
		PaymentFact fact = PaymentFact.builder().medium(bookingMedium).build();
		List<PaymentConfigurationRule> rules =
				pmsComm.getApplicableRulesOutput(fact, PaymentRuleType.REFUND, bookingMedium);
		if (!CollectionUtils.isEmpty(rules)) {
			medium = ((RefundOutput) rules.get(0).getOutput()).getMedium();
		}
		if (medium.isExternalPaymentMedium() && extMediumAmount.compareTo(paymentRequest.getAmount()) < 0) {
			medium = PaymentMedium.FUND_HANDLER;
		}
		if (!medium.equals(PaymentMedium.FUND_HANDLER)) {
			paymentRequest.setPaymentMedium(medium);
		}
	}
	@Override
	public void afterProcess() throws Exception {
		
		log.info("Response is {}", GsonUtils.getGson().toJson(response));
		
	}

}
