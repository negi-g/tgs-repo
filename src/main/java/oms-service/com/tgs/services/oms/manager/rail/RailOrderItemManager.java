package com.tgs.services.oms.manager.rail;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.MessageMedium;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.SecurityConstants;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.ProcessedBookingDetail;
import com.tgs.services.oms.datamodel.ProcessedItemDetails;
import com.tgs.services.oms.datamodel.ProcessedOrder;
import com.tgs.services.oms.datamodel.rail.ProcessedJourneyInfo;
import com.tgs.services.oms.datamodel.rail.ProcessedRailDetails;
import com.tgs.services.oms.datamodel.rail.RailItemStatus;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.rail.RailOrderItemService;
import com.tgs.services.oms.manager.AbstractInvoiceIdManager;
import com.tgs.services.oms.manager.ItemManager;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.servicehandler.rail.RailOrderActionValidator;
import com.tgs.services.oms.servicehandler.rail.RailOrderMessageHandler;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.rail.restmodel.RailMessageRequest;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailOrderItemManager implements ItemManager {

	@Autowired
	protected RailOrderItemService itemService;

	@Autowired
	protected RailOrderActionValidator actionValidator;

	@Autowired
	protected RailCommunicator railComm;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	protected UserServiceCommunicator userService;

	@Autowired
	private AbstractInvoiceIdManager invoiceIdManager;

	@Autowired
	private RailOrderMessageHandler msgHandler;

	@Value("${domain}")
	private String domain;

	@Value("${commProtocol}")
	private String protocol;

	@Override
	public void processBooking(String bookingId, Order order, PaymentStatus paymentStatus, double paymentFee) {
		// TODO Auto-generated method stub

	}

	public List<ProcessedBookingDetail> getProcessedItemDetails(OrderFilter orderFilter) {

		List<Object[]> orderList = findInfoByOrderFilter(orderFilter);
		if (orderList == null || orderList.size() == 0) {
			return new ArrayList<>();
		}

		Map<String, DbOrder> orderMap = new HashMap<>();
		List<DbRailOrderItem> orderItemList = new ArrayList<DbRailOrderItem>();
		List<ProcessedOrder> orders = new ArrayList<>();
		List<ProcessedBookingDetail> pbds = new ArrayList<>();

		for (Object[] object : orderList) {
			DbOrder order = (DbOrder) object[1];
			orderItemList.add((DbRailOrderItem) object[0]);
			ProcessedOrder pOrder = new GsonMapper<>(order, ProcessedOrder.class).convert();
			if (orderMap.isEmpty() || (!orderMap.isEmpty() && !orderMap.containsKey(order.getBookingId()))) {
				orders.add(pOrder);
				orderMap.put(order.getBookingId(), order);
			}
		}
		Map<String, List<DbRailOrderItem>> itemsMap = orderItemList.stream()
				.collect(Collectors.groupingBy(DbRailOrderItem::getBookingId));

		for (ProcessedOrder pOrder : orders) {
			List<DbRailOrderItem> dbOrderItems = itemsMap.get(pOrder.getBookingId());
			pbds.add(ProcessedBookingDetail.builder().order(pOrder).items(getProcessedItemMap(dbOrderItems)).build());
		}

		updateActionList(pbds);
		return pbds;
	}

	public List<Object[]> findInfoByOrderFilter(OrderFilter filter) {
		return itemService.findByJsonSearch(filter);
	}

	public Map<String, ProcessedItemDetails> getProcessedItemMap(List<DbRailOrderItem> itemList) {
		ProcessedItemDetails processedItemDetails = getProcessedItem(itemList.get(0));
		Map<String, ProcessedItemDetails> map = new HashMap<>();
		map.put(OrderType.RAIL.getName(), processedItemDetails);
		return map;

	}

	public ProcessedItemDetails getProcessedItem(DbRailOrderItem dbOrderItem) {
		ProcessedJourneyInfo journeyInfo = ProcessedJourneyInfo.builder().departureTime(dbOrderItem.getDepartureTime())
				.arrivalTime(dbOrderItem.getArrivalTime()).railInfo(dbOrderItem.getAdditionalInfo().getRailInfo())
				.duration(dbOrderItem.getAdditionalInfo().getDuration())
				.departStationInfo(railComm.getRailStationInfo(dbOrderItem.getFromStn()))
				.arrivalStaionInfo(railComm.getRailStationInfo(dbOrderItem.getToStn()))
				.distance(dbOrderItem.getAdditionalInfo().getDistance())
				.isOtpBased(BooleanUtils.isTrue(dbOrderItem.getAdditionalInfo().getIsThroughOTP()))
				.pnr(dbOrderItem.getAdditionalInfo().getPnr()).build();
		ProcessedRailDetails processedRailDetails = ProcessedRailDetails.builder()
				.travellerInfo(dbOrderItem.getTravellerInfo()).journeyInfo(journeyInfo).build();
		return processedRailDetails;
	}

	public void updateActionList(List<ProcessedBookingDetail> pbds) {

		pbds.forEach(pbd -> {
			try {
				if (pbd.getOrder() != null) {
					pbd.getOrder()
							.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(),
									Order.builder().status(pbd.getOrder().getStatus())
											.additionalInfo(pbd.getOrder().getAdditionalInfo()).build()));
				}
			} catch (Exception e) {
				log.error("Unable to parse booking information for bookingId {}", pbd.getOrder().getBookingId(), e);
			}
		});

	}

	public DbRailOrderItem findByBookingId(String bookingId) {
		DbRailOrderItem item = itemService.findByBookingId(bookingId);
		return item;
	}

	public void save(DbRailOrderItem item, Order order) {
		itemService.save(item);
		order = orderManager.findByBookingId(item.getBookingId(), order);
		if (order != null)
			updateOrderStatus(order, item);
	}

	public void saveOrderAndItem(DbRailOrderItem item, Order order) {
		itemService.save(item);
		orderManager.save(order);
	}

	private void updateOrderStatus(Order order, DbRailOrderItem item) {
		OrderStatus oldOrderStatus = order.getStatus();
		OrderStatus status = getOrderStatus(item);
		order.setStatus(status);
		order.getAdditionalInfo().setFirstUpdateTime(statusfirstUpdateTime(order));
		order = orderManager.save(order);
		if (!oldOrderStatus.equals(status)) {
			if (status.equals(OrderStatus.SUCCESS)) {
				orderManager.updateLastTransactionTime(Product.RAIL, order.getBookingUserId());
				invoiceIdManager.setInvoiceIdFor(order, Product.getProductMetaInfoFromId(order.getBookingId()));
				orderManager.save(order);
				log.debug("Sending auto Email with booking id {}", order.getBookingId());
				httpReqToSendEmail(order);
				log.debug("Sending Booking SMS with booking id {}", order.getBookingId());
				msgHandler.sendBookingSms(order, SmsTemplateKey.RAIL_BOOKING_SUCCESS_SMS);
			}
		}
	}

	public Map<String, LocalDateTime> statusfirstUpdateTime(Order order) {
		OrderStatus status = order.getStatus();
		Map<String, LocalDateTime> firstTxnTime = order.getAdditionalInfo().getFirstUpdateTime();
		if (OrderStatus.SUCCESS.equals(status) || OrderStatus.PENDING.equals(status)
				|| OrderStatus.ABORTED.equals(status)) {
			if (firstTxnTime != null && firstTxnTime.get(status.getCode()) == null)
				firstTxnTime.put(status.getCode(), LocalDateTime.now());
		}
		return firstTxnTime;
	}

	private OrderStatus getOrderStatus(DbRailOrderItem item) {
		RailItemStatus itemStatus = RailItemStatus.getRailItemStatus(item.getStatus());
		return itemStatus.getOrderStatus();
	}

	private void httpReqToSendEmail(Order order) {
		try {
			Map<String, String> headerParams = new HashMap<>();
			headerParams.put("Content-Type", "application/json");
			headerParams.put("deviceid", "System");
			if (SystemContextHolder.getContextData().getHttpHeaders().getJwtToken() != null) {
				headerParams.put("Authorization", SecurityConstants.TOKEN_PREFIX
						+ SystemContextHolder.getContextData().getHttpHeaders().getJwtToken());
			} else if (SystemContextHolder.getContextData().getHttpHeaders().getApikey() != null) {
				headerParams.put(SecurityConstants.API_HEADER_KEY,
						SystemContextHolder.getContextData().getHttpHeaders().getApikey());
			}
			User bookingUser = userService.getUserFromCache(order.getBookingUserId());
			String toEmails = OrderUtils.getToEmailId(order, bookingUser, null);
			RailMessageRequest request = RailMessageRequest.builder().mode(MessageMedium.EMAIL)
					.bookingId(order.getBookingId()).toIds(toEmails).build();
			String strPostData = GsonUtils.getGson().toJson(request);
			log.debug("HeaderParams for rail success Email are {} and request body is {} for booking id {}",
					headerParams, strPostData, order.getBookingId());
			String url = protocol + "://" + domain + "/oms/v1/rail/msg-ticket";
			HttpUtils httpUtils = HttpUtils.builder().urlString(url)
					.headerParams(headerParams).postData(strPostData).build();
			BaseResponse response = httpUtils.getResponse(BaseResponse.class).get();
			String resp = null;
			 if (response!= null) {
				resp =GsonUtils.getGson().toJson(response);
			}
			log.info("Rail Booking Http Response {} for bookingid {} with url {}", resp, order.getBookingId(),url);
		} catch (Exception e) {
			log.error("Unable to send booking confirmation email for bookingid {}", order.getBookingId(), e);
		}
	}
}
