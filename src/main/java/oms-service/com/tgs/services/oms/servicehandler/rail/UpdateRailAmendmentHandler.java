package com.tgs.services.oms.servicehandler.rail;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import com.tgs.filters.AmendmentFilter;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.Amendments.AmendmentHelper;
import com.tgs.services.oms.Amendments.RailAmendmentHandlerFactory;
import com.tgs.services.oms.Amendments.RailMessagingClient;
import com.tgs.services.oms.Amendments.Processors.RailAmendmentProcessor;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.amendments.UpdateAmendmentRequest;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.rail.RailOrderItemManager;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.ruleengine.RefundOutput;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UpdateRailAmendmentHandler {

	@Autowired
	protected OrderService orderService;

	@Autowired
	protected AmendmentActionValidator actionValidator;

	@Autowired
	protected AmendmentService service;

	@Autowired
	protected RailOrderItemManager railOrderItemManager;

	@Autowired
	protected PaymentServiceCommunicator paymentServiceCommunicator;

	@Autowired
	protected UserServiceCommunicator userService;

	@Autowired
	protected RailMessagingClient messagingClient;

	@Setter
	private AmendmentAction action;

	private DbOrder dbOrder;

	private Amendment amendment;

	private RailOrderItem orgRailOrderItem;

	@Setter
	private boolean paymentRequired;

	private static final short AMD_ASSIGNMENT_CAP = 3;

	private void validateAction() {

		if (amendment.getStatus().equals(AmendmentStatus.SUCCESS)
				|| amendment.getStatus().equals(AmendmentStatus.REJECTED))
			throw new CustomGeneralException(SystemError.AMENDMENT_NOT_UPDATABLE);

		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment).contains(action))
			throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);
	}

	public Amendment process(UpdateAmendmentRequest request) throws Exception {

		amendment = service.findByAmendmentId(request.getAmendmentId());
		// validateAction();
		paymentRequired = !AmendmentHelper.isQuotation(amendment)
				&& !amendment.getStatus().equals(AmendmentStatus.PAYMENT_SUCCESS);

		if (!StringUtils.isEmpty(request.getFinishingNotes())) {
			amendment.getAdditionalInfo().setFinishingNotes(request.getFinishingNotes());
		}

		if (action.equals(AmendmentAction.ASSIGN)) {
			assign(request.getAssignedUserId());
		}

		if (action.equals(AmendmentAction.ASSIGNME)) {
			assign(SystemContextHolder.getContextData().getUser().getUserId());
		}

		if (action.equals(AmendmentAction.ABORT)) {
			amendment.setStatus(AmendmentStatus.REJECTED);
			AmendmentHelper.updateAssignments(null, amendment);
		}

		if (action.equals(AmendmentAction.PENDING_WITH_SUPPLIER)) {
			amendment.setStatus(AmendmentStatus.PENDING_WITH_SUPPLIER);
			amendment.setAssignedUserId(null);
			AmendmentHelper.updateAssignments(null, amendment);
		}
		if (action.equals(AmendmentAction.MERGE)) {
			processPayment();
			merge();
		}
		service.save(amendment);
		sendMail();
		sendSms();
		amendment
				.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment));
		return amendment;
	}

	@Transactional
	private void processPayment() {

		if (!paymentRequired || amendment.getAmount() == 0)
			return;
		boolean refund = amendment.getAmount() < 0;
		List<PaymentRequest> paymentRequestList = new ArrayList<>();
		try {
			PaymentAdditionalInfo additionalInfo =
					PaymentAdditionalInfo.builder().allowForDisabledMediums(true).build();
			PaymentRequest paymentRequest =
					WalletPaymentRequest.builder().build().setAmount(Math.abs(amendment.getAmount()))
							.setProduct(Product.getProductFromId(amendment.getBookingId()))
							.setAmendmentId(amendment.getAmendmentId())
							.setOpType(refund ? PaymentOpType.CREDIT : PaymentOpType.DEBIT)


							.setPayUserId(amendment.getBookingUserId()).setRefId(amendment.getBookingId())
							.setTransactionType(
									refund ? PaymentTransactionType.REFUND : PaymentTransactionType.PAID_FOR_AMENDMENT)
							.setAdditionalInfo(additionalInfo);
			if (refund) {
				evalRefundMedium(paymentRequest);
				amendment.getRailAdditionalInfo().setAmendmentAmount(amendment.getAmount());
			}
			paymentRequestList.add(paymentRequest);

			paymentServiceCommunicator.doPaymentsUsingPaymentRequests(paymentRequestList);
			amendment.setStatus(AmendmentStatus.PAYMENT_SUCCESS);
		} catch (Exception ex) {
			log.error("Error Occured during process of payment due to {}", ex);
			amendment.setStatus(AmendmentStatus.PAYMENT_FAILED);
			throw ex;
		} finally {
			service.save(amendment);
		}
	}

	private void evalRefundMedium(PaymentRequest paymentRequest) {

		PaymentMedium medium = PaymentMedium.FUND_HANDLER;
		List<Payment> paymentList = paymentServiceCommunicator.search(PaymentFilter.builder()
				.refId(amendment.getBookingId()).type(PaymentTransactionType.PAID_FOR_ORDER).build());

		paymentList.sort(Comparator.comparing(Payment::getCreatedOn));
		PaymentMedium bookingMedium = paymentList.get(0).getPaymentMedium();
		BigDecimal extMediumAmount = Payment.sum(paymentList.stream()
				.filter(p -> p.getPaymentMedium().isExternalPaymentMedium()).collect(Collectors.toList()));
		PaymentFact fact = PaymentFact.builder().medium(bookingMedium).build();
		List<PaymentConfigurationRule> rules =
				paymentServiceCommunicator.getApplicableRulesOutput(fact, PaymentRuleType.REFUND, bookingMedium);
		if (!CollectionUtils.isEmpty(rules)) {
			medium = ((RefundOutput) rules.get(0).getOutput()).getMedium();
		}
		if (medium.isExternalPaymentMedium() && extMediumAmount.compareTo(paymentRequest.getAmount()) < 0) {
			medium = PaymentMedium.FUND_HANDLER;
		}
		if (!medium.equals(PaymentMedium.FUND_HANDLER)) {
			paymentRequest.setPaymentMedium(medium);
		}
	}

	private void assign(String userId) {
		List<Amendment> amendments = service.search(AmendmentFilter.builder()
				.assignedUserIdIn(Collections.singletonList(userId)).statusIn(AmendmentHelper.assignedStatuses())
				.orderTypeIn(Collections.singletonList(OrderType.RAIL)).build());

		if (amendments.size() >= AMD_ASSIGNMENT_CAP) {
			throw new CustomGeneralException(SystemError.AMENDMENT_ASSIGNMENT_LIMIT);
		}

		amendment.setAssignedUserId(userId);
		amendment.setAssignedOn(LocalDateTime.now());
		if (amendment.getStatus().equals(AmendmentStatus.REQUESTED))
			amendment.setStatus(AmendmentStatus.ASSIGNED);
		else
			amendment.setStatus(AmendmentStatus.PROCESSING);
		AmendmentHelper.updateAssignments(userId, amendment);
	}


	@Transactional
	private void merge() {
		log.info("Merging Amendment for order {} ", amendment.getAmendmentId());
		dbOrder = orderService.findByBookingId(amendment.getBookingId());
		applyAmendment();
		amendment.setStatus(AmendmentStatus.SUCCESS);
		AmendmentHelper.updateAssignments(null, amendment);
		service.save(amendment);
	}

	private void applyAmendment() {
		DbRailOrderItem orgDbRailOrderItem = railOrderItemManager.findByBookingId(amendment.getBookingId());
		orgRailOrderItem = orgDbRailOrderItem.toDomain();
		if (!(AmendmentType.CANCELLATION.equals(amendment.getAmendmentType())
				|| AmendmentType.OFFLINE_CANCEL.equals(amendment.getAmendmentType())
				|| AmendmentType.TDR.equals(amendment.getAmendmentType()))) {
			amendment.getRailAdditionalInfo().setOrderPreviousSnapshot(orgRailOrderItem);
		}
		DbRailOrderItem modItem = new DbRailOrderItem();
		if (amendment.getModifiedInfo() != null & amendment.getModifiedInfo().getRailOrderItem() != null) {
			modItem = new DbRailOrderItem().from(amendment.getModifiedInfo().getRailOrderItem());
		}
		RailAmendmentProcessor amendmentHandler = RailAmendmentHandlerFactory.initHandler(amendment, modItem,
				orgDbRailOrderItem, userService.getUserFromCache(amendment.getBookingUserId()));

		if (dbOrder == null) {
			dbOrder = orderService.findByBookingId(amendment.getBookingId());
		}
		RailOrderItem patchedOrderItem = amendmentHandler.patchRailOrderItem(true);
		updateOrderTotal(dbOrder, patchedOrderItem);
		save(dbOrder, patchedOrderItem);
		amendment.getModifiedInfo().setRailOrderItem(patchedOrderItem);
		amendment.getRailAdditionalInfo().setOrderCurrentSnapshot(patchedOrderItem);

	}

	private void updateOrderTotal(DbOrder order, RailOrderItem patchedOrderItem) {
		order.setAmount(patchedOrderItem.getAmount());
	}

	@Transactional
	private void save(DbOrder dbOrder, RailOrderItem railOrderItem) {
		railOrderItemManager.save(new DbRailOrderItem().from(railOrderItem), dbOrder.toDomain());
	}

	private void sendMail() {
		if (action.equals(AmendmentAction.MERGE)) {
			if (amendment.getAmendmentType().equals(AmendmentType.TDR)) {
				messagingClient.sendMail(amendment, amendment.getAmendmentType().mergeMailKey());
			}
		}
	}
	private void sendSms() {
		if (action.equals(AmendmentAction.MERGE)) {
			if (amendment.getAmendmentType().equals(AmendmentType.TDR)) {
				messagingClient.sendSMS(amendment,SmsTemplateKey.RAIL_TDR_SMS);
			}
		}
	}
}
