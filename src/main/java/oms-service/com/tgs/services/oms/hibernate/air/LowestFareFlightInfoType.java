package com.tgs.services.oms.hibernate.air;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.fms.datamodel.LowestFareFlightInfo;

public class LowestFareFlightInfoType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return LowestFareFlightInfo.class;
    }
}
