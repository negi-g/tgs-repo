package com.tgs.services.oms.hibernate.air;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;

public class FlightTravellerInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		List<FlightTravellerInfo> travellerList = new ArrayList<>();
		return travellerList.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<FlightTravellerInfo>>() {
		}.getType();
	}

}
