package com.tgs.services.oms.jparepository.misc;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.tgs.services.oms.dbmodel.misc.DbMiscOrderItem;

@Repository
public interface MiscOrderItemRepository
		extends JpaRepository<DbMiscOrderItem, Long>, JpaSpecificationExecutor<DbMiscOrderItem> {

	DbMiscOrderItem findByBookingId(String bookingId);

	@Query(value = "SELECT nextval('misc_invoice_id_seq')", nativeQuery = true)
	Integer getNextInvoiceId();
}
