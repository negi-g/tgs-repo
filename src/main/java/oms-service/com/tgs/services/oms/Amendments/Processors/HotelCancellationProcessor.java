package com.tgs.services.oms.Amendments.Processors;

import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.servicehandler.hotel.HotelBookingCancellationHandler;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelCancellationProcessor extends HotelAmendmentProcessor {

	@Autowired
	protected HotelBookingCancellationHandler hotelCancellationHandler;

	@Override
	protected void processAmendment() {
		updateRoomToCancel();
		manualOrderManager.updateNewHotelInfoInRequestForModifyOrder(hotelOrderRequest);
		hotelOrderRequest.setRoomKeys(amendment.getAdditionalInfo().getHotelAdditionalInfo().getRoomKeys());
		HotelOrder modifiedHotelOrder = getModifiedHotelOrder();
		HotelInfo modifiedHotelInfo = DbHotelOrderItemListToHotelInfo.builder()
				.itemList(new DbHotelOrderItem().toDbList(modifiedHotelOrder.getItems())).build().convert();

		Double amendmentFee = amendmentManager.updateCommonAmounts(modifiedHotelOrder.getItems(), amendment);

		Double differentialPayment = getDifferentialPayment(amendment, modifiedHotelInfo);

		amendment.getAdditionalInfo().setOrderDiffAmount(differentialPayment + amendmentFee);
		amendment.setAmendmentAmount();
		updateModifiedHotelInfoInAmendment(amendment, modifiedHotelInfo);
		if (StringUtils.isNotBlank(requestNote))
			updateNote(amendment);
	}

	@Override
	protected void validate() {
		if (!BooleanUtils.isTrue(hotelOrderRequest.getSkipValidation())) {
			if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment)
					.contains(AmendmentAction.PROCESS))
				throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);
		}

	}

	private void updateRoomToCancel() {
		BaseHotelUtils.updateTotalFareComponents(hotelOrderRequest.getHInfo(), null);
		HotelInfo hInfo = hotelOrderRequest.getHInfo();
		Set<String> roomKeys = amendment.getAdditionalInfo().getHotelAdditionalInfo().getRoomKeys();
		hInfo.getOptions().get(0).getRoomInfos().stream().forEach(roomInfo -> {
			if (CollectionUtils.isEmpty(roomKeys) || roomKeys.contains(roomInfo.getId())) {
				roomInfo.setRoomStatus(OrderStatus.CANCELLED.getCode());
			}
		});
		hotelCancellationHandler.updateRefundAmount(hInfo, amendment);
	}

	@Override
	protected Double getDifferentialPayment(Amendment amendment, HotelInfo modifiedHotelInfo) {
		Set<String> roomKeys = amendment.getAdditionalInfo().getHotelAdditionalInfo().getRoomKeys();
		return modifiedHotelInfo.getOptions().get(0).getRoomInfos().stream().mapToDouble(roomInfo -> {
			if (CollectionUtils.isEmpty(roomKeys) || roomKeys.contains(roomInfo.getId())) {
				return roomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.AAR, 0d) * -1d;
			}
			return 0d;
		}).sum();
	}

}
