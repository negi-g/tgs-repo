package com.tgs.services.oms.restcontroller.hotel;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.datamodel.TravellerInfoValidatingData;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.HotelBookingConditions;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.oms.restcontroller.validator.HotelSupplierBookingValidator;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingRequest;

@Component
public class HotelBookingRequestValidator extends TgsValidator implements Validator {

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	HotelSupplierBookingValidator supplierValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {

		HotelReviewResponse reviewResponse = null;
		if (target instanceof HotelBookingRequest) {
			HotelBookingRequest bookingRequest = (HotelBookingRequest) target;
			if (bookingRequest.getBookingId() == null) {
				rejectValue(errors, "bookingId", SystemError.INVALID_BOOKING_ID);
			}
			DeliveryInfo deliveryInfo = bookingRequest.getDeliveryInfo();
			if (deliveryInfo != null) {
				validateDeliveryInfo(deliveryInfo, errors);
				registerErrors(errors, "deliveryInfo", deliveryInfo);
			} else {
				rejectValue(errors, "deliveryInfo", SystemError.NULL_DELIVERY_INFO);
			}
			List<RoomTravellerInfo> roomTravellerInfoList = bookingRequest.getRoomTravellerInfo();

			if (roomTravellerInfoList != null) {
				CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
						.set(CacheSetName.HOTEL_BOOKING.getName()).key(bookingRequest.getBookingId())
						.bins(new String[] {BinName.HOTELINFO.getName()}).kyroCompress(false).build();
				reviewResponse = cacheService.fetchValue(HotelReviewResponse.class, metaInfo);

				if (reviewResponse != null) {
					List<RoomSearchInfo> roomInfoList = reviewResponse.getQuery().getRoomInfo();
					if (roomInfoList.size() != roomTravellerInfoList.size()) {
						rejectValue(errors, "roomTravellerInfo", SystemError.INVALID_HOTEL_TRAVELLERS_LIST);
					}
					validateTravellerInfo(TravellerInfoValidatingData.builder().build(), roomTravellerInfoList, errors,
							reviewResponse);

				} else {
					rejectValue(errors, "bookingId", SystemError.EXPIRED_BOOKING_ID);
				}
			} else {
				rejectValue(errors, "roomTravellerInfo", SystemError.INVALID_HOTEL_TRAVELLERS);
			}
		}
		if (!errors.hasErrors())
			supplierValidator.validateForSupplier(target, reviewResponse.getQuery(), errors);
	}

	private void validateDeliveryInfo(DeliveryInfo deliveryInfo, Errors errors) {

		if (CollectionUtils.isEmpty(deliveryInfo.getCode())) {
			rejectValue(errors, "deliveryInfo.code", SystemError.NULL_COUNTRY_CODE);
		}
		if (deliveryInfo.getCode().get(0).length() > 5 || !deliveryInfo.getCode().get(0).contains("+")) {
			rejectValue(errors, "deliveryInfo.code", SystemError.INVALID_COUNTRY_CODE);
		}
	}

	public void validateTravellerInfo(TravellerInfoValidatingData validatingData,
			List<RoomTravellerInfo> roomTravellerInfoList, Errors errors, HotelReviewResponse reviewResponse) {
		Option option = reviewResponse.getHInfo().getOptions().get(0);
		Boolean isPassportMandatory = option.getIsPassportMandatory();
		Boolean isPanMandatory = option.getIsPanRequired();
		Boolean isOnePanRequiredPerBooking = BooleanUtils.isTrue(option.getMiscInfo().getIsOnePanRequiredPerBooking());
		int travellerIndex = 0;

		for (RoomTravellerInfo roomTravellerInfo : roomTravellerInfoList) {
			List<TravellerInfo> travellers = roomTravellerInfo.getTravellerInfo();
			if (!CollectionUtils.isEmpty(travellers)) {
				String travellerField = "roomTravellerInfo[" + travellerIndex + "].travellerInfo[";
				validateTravellerPersonalInfo(travellers, travellerField, errors);
				if (BooleanUtils.isTrue(isPassportMandatory)) {
					validateTravellerPassportInfo(travellers, travellerField, errors);
				}
				if (BooleanUtils.isTrue(isPanMandatory)) {
					if (isOnePanRequiredPerBooking) {
						if (travellerIndex == 0) {
							validateTravellerPanInfo(travellers, travellerField, errors);
						} else {
							continue;
						}
					} else {
						validateTravellerPanInfo(travellers, travellerField, errors);
					}
				}
			}
			travellerIndex++;
		}
	}

	private boolean isPanAvailable(List<TravellerInfo> travellers) {

		return travellers.stream().filter(traveller -> StringUtils.isNotBlank(traveller.getPanNumber())).findFirst()
				.isPresent();
	}

	private boolean isPassportAvailable(List<TravellerInfo> travellers) {

		return travellers.stream().filter(traveller -> StringUtils.isNotBlank(traveller.getPassportNumber()))
				.findFirst().isPresent();
	}

	private void validateTravellerPassportInfo(List<TravellerInfo> travellers, String roomTravellerField,
			Errors errors) {

		if (!isPassportAvailable(travellers)) {
			rejectValue(errors, roomTravellerField + "0].passportNumber", SystemError.EMPTY_PASSPORT_NUMBER);
		}
	}

	public void validateTravellerPersonalInfo(List<TravellerInfo> travellers, String roomTravellerField,
			Errors errors) {
		List<String> allowedPaxTitle = Arrays.asList("Mr", "Master", "Mrs", "Ms");

		for (TravellerInfo traveller : travellers) {
			int i = 0;
			if (traveller != null) {
				if (traveller.getTitle() == null) {
					rejectValue(errors, roomTravellerField + i++ + "].title", SystemError.INVALID_TITLE);
				}
				if (traveller.getPaxType().equals(PaxType.CHILD)) {
					if (!traveller.getTitle().equals("Master")) {
						rejectValue(errors, roomTravellerField + i++ + "].title", SystemError.INVALID_CHILD_TITLE);
					}
				}
				if (!allowedPaxTitle.contains(traveller.getTitle())) {
					rejectValue(errors, roomTravellerField + i++ + "].title", SystemError.INVALID_TITLE);
				}
				if (traveller.getFirstName() == null
						|| !TgsStringUtils.containsOnlyAlphabetAndSpace(traveller.getFirstName())) {
					rejectValue(errors, roomTravellerField + i++ + "].firstName", SystemError.FIRSTNAME_VALIDATION);
				}
				if (traveller.getLastName() == null
						|| !TgsStringUtils.containsOnlyAlphabetAndSpace(traveller.getLastName())) {
					rejectValue(errors, roomTravellerField + i++ + "].lastName", SystemError.LASTNAME_VALIDATION);
				}
			}
		}

	}

	private void validatePAN(String pan, Errors errors, String roomTravellerField) {

		if (pan.length() != 10 || !pan.toUpperCase().matches("[A-Z]{5}[0-9]{4}[A-Z]{1}")) {
			rejectValue(errors, roomTravellerField + "0].panNumber", SystemError.INVALID_PAN_NUMBER);
		}
	}

	public HotelBookingConditions getBookingConditions(BookingRequest bookingRequest) {
		return null;
	}

	protected void rejectValue(Errors errors, String field, SystemError systemError) {
		errors.rejectValue(field, systemError.errorCode(), systemError.getMessage());
	}

	private void validateTravellerPanInfo(List<TravellerInfo> travellers, String roomTravellerField, Errors errors) {

		if (isPanAvailable(travellers)) {
			String panNumber = travellers.stream().filter(traveller -> StringUtils.isNotBlank(traveller.getPanNumber()))
					.findFirst().get().getPanNumber();
			validatePAN(panNumber, errors, roomTravellerField);
		} else {
			rejectValue(errors, roomTravellerField + "0].panNumber", SystemError.EMPTY_PAN_NUMBER);
		}
	}
}
