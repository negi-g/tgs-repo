package com.tgs.services.oms.servicehandler.air;

import java.util.Map;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.datamodel.AirItemDetail;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.dbmodel.air.DbAirItemDetail;
import com.tgs.services.oms.jparepository.air.AirItemDetailService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.restmodel.air.AirItemDetailRequest;
import com.tgs.services.ums.datamodel.User;

@Service
public class AirItemDetailUpdateHandler extends ServiceHandler<AirItemDetailRequest, BaseResponse> {

	@Autowired
	private AirItemDetailService detailService;

	@Autowired
	private OrderManager orderManager;

	@Override
	public void beforeProcess() throws Exception {
		User user = SystemContextHolder.getContextData().getUser();
		Order order = orderManager.findByBookingId(request.getBookingId(), null);

		if (order == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);

		if (!user.getUserId().equals(order.getAdditionalInfo().getAssignedUserId())
				&& !user.getParentUserId().equals(order.getBookingUserId()))
			throw new CustomGeneralException(SystemError.DIFFERENT_ASSIGNED_USER);

	}

	@Override
	public void process() throws Exception {

		AirItemDetail airItemDetail = detailService.get(request.getBookingId());

		if (airItemDetail == null) {
			airItemDetail = new AirItemDetail();
			airItemDetail.setBookingId(request.getBookingId());
		}

		Map<String, Map<String, Object>> profileData = airItemDetail.getInfo().getProfileData();

		for (FlightTravellerInfo traveller : request.getTravellers()) {
			if (MapUtils.isNotEmpty(traveller.getUserProfile().getData())) {
				profileData.put(traveller.getPaxKey(), traveller.getUserProfile().getData());
			}
		}

		if (MapUtils.isNotEmpty(profileData)) {
			detailService.save(new DbAirItemDetail().from(airItemDetail));
		}

	}

	@Override
	public void afterProcess() throws Exception {

	}

}
