package com.tgs.services.oms.hibernate.air;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.oms.datamodel.AirItemDetailInfo;

public class AirItemDetailInfoType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return AirItemDetailInfo.class;
    }

}
