package com.tgs.services.oms.servicehandler.misc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.oms.datamodel.InvoiceInfo;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.misc.DbMiscOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.misc.MiscOrderItemService;
import com.tgs.services.oms.restmodel.InvoiceRequest;
import com.tgs.services.oms.restmodel.misc.MiscInvoiceResponse;
import com.tgs.services.oms.servicehandler.InvoiceHandler;
import com.tgs.services.ums.datamodel.User;

@Service
public class MiscInvoiceHandler extends InvoiceHandler<InvoiceRequest, MiscInvoiceResponse> {

	@Autowired
	protected OrderService orderService;

	@Autowired
	protected MiscOrderItemService miscOrderItemService;

	@Autowired
	protected UserServiceCommunicator userServiceCommunicator;

	@Autowired
	protected GeneralServiceCommunicator generalServiceCommunicator;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		DbOrder dbOrder = orderService.findByBookingId(request.getId());
		DbMiscOrderItem orderItem = miscOrderItemService.findByBookingId(request.getId());
		User user = userServiceCommunicator.getUserFromCache(dbOrder.getBookingUserId());
		response.setAmount(dbOrder.getAmount());
		response.setBookingId(dbOrder.getBookingId());
		response.setCreatedOn(dbOrder.getCreatedOn());
		response.setInvoiceId(dbOrder.getAdditionalInfo().getInvoiceId());
		response.setDescription(orderItem.getAdditionalInfo().getDescription());
		response.setFrom(getClientInvoiceInfo(
				(ClientGeneralInfo) generalServiceCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
						GeneralBasicFact.builder().applicableTime(dbOrder.getCreatedOn()).build())));
		InvoiceInfo agentInfo = getAgentInvoiceInfo(user);
		response.setTo(agentInfo);
	}

	@Override
	public void afterProcess() throws Exception {

	}
}
