package com.tgs.services.oms.servicehandler.rail;

import java.util.Arrays;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.oms.Amendments.RailMessagingClient;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.restmodel.rail.RailCancellationRequest;
import com.tgs.services.oms.restmodel.rail.RailCancellationResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailCancellationHandler extends ServiceHandler<RailCancellationRequest, RailCancellationResponse> {

	@Autowired
	private RailCommunicator railComm;

	@Autowired
	private RailMessagingClient messagingClient;

	@Autowired
	protected AmendmentService amdService;
	
	@Override
	public void beforeProcess() throws Exception {


	}

	@Override
	public void process() throws Exception {
		response = railComm.cancelPassengers(request);
		log.debug("Errors in response with bookingid {} and errors {} ", request.getBookingId(),response.getErrors());
		if (CollectionUtils.isNotEmpty(response.getErrors())) {
			messagingClient.sendMail(amdService.findByAmendmentId(request.getAmendmentId()),
								Arrays.asList(EmailTemplateKey.RAIL_CANCELLATION_EMAIL));
			
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
