package com.tgs.services.oms.servicehandler.misc;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.ProcessedBookingDetail;
import com.tgs.services.oms.manager.misc.MiscOrderItemManager;
import com.tgs.services.oms.restmodel.OrderResponse;

@Service
public class MiscOrderListingHandler extends ServiceHandler<OrderFilter, OrderResponse> {


	@Autowired
	protected MiscOrderItemManager itemManager;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		if (request.getPageAttr() == null) {
			request.setPageAttr(new PageAttributes());
			request.getPageAttr().setSize(10000);
		}
		List<ProcessedBookingDetail> pbds = itemManager.getProcessedItemDetails(request);
		response.setDetails(pbds);
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
