package com.tgs.services.oms.servicehandler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.restmodel.ItemDetailResponse;
import com.tgs.services.oms.utils.air.AirBookingUtils;

@Service
public class ItemDetailHandler extends ServiceHandler<String, ItemDetailResponse> {

	@Autowired
	private AirOrderItemService itemService;

	@Override
	public void beforeProcess() throws Exception {
	}

	@Override
	public void process() throws Exception {
		List<DbAirOrderItem> dbItemList = itemService.findByBookingId(request);
		for (DbAirOrderItem dbItem : dbItemList) {
			AirOrderItem item = dbItem.toDomain();
			AirBookingUtils.setAirlineNAirportInfo(item);
			response.getItems().add(item);
		}
	}

	@Override
	public void afterProcess() throws Exception {
	}

}
