package com.tgs.services.oms.servicehandler.hotel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.datamodel.HotelBnplMailData;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.oms.dbmodel.DbHotelOrder;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;

import com.tgs.services.oms.restmodel.hotel.HotelBnplStatusUpdateRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UpdateBookNowPayLaterStatusHandler extends ServiceHandler<OrderFilter, BaseResponse> {

	@Autowired
	HotelOrderItemManager hotelOrderItemManager;
	
	@Autowired
	private HMSCommunicator hmsCommunicator;

	@Autowired
	private HotelOrderItemCommunicator orderItemCommunicator;
	
	@Autowired
	private HotelOrderMessageHandler messageHandler;
	
	@Override
	public void beforeProcess() throws Exception {
		
		final LocalDate now = LocalDate.now();
		
		if(request.getHotelItemFilter().getBnplAfterDate() == null) {
			request.getHotelItemFilter().setBnplAfterDate(now.minusDays(1));
		}
		if(request.getHotelItemFilter().getBnplBeforeDate() == null) {
			request.getHotelItemFilter().setBnplBeforeDate(now);
		}
		request.setProducts(Arrays.asList(OrderType.HOTEL));
	}

	@Override
	public void process() throws Exception {
		
		try {
			List<DbHotelOrder> orders = hotelOrderItemManager.getHotelOrderList(request);
			if (CollectionUtils.isNotEmpty(orders)) {
				List<DbHotelOrder> hotelOrdersAgoda = getFilteredAgodaOrder(orders);
				updateBnplStatusInHotel(hotelOrdersAgoda);
			}
		} catch (Exception ex) {
			log.error("Exception while updating hotel book now pay later status ", ex);
			throw ex;
		}
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	private List<DbHotelOrder> getFilteredAgodaOrder(List<DbHotelOrder> orders) {
		List<DbHotelOrder> filteredOrderItem = new ArrayList<>();
		for (DbHotelOrder order : orders) {
			String supplierName = order.getOrderItems().get(0).getSupplierId();
			AdditionalHotelOrderItemInfo additionalInfo = order.getOrderItems().get(0).getAdditionalInfo();
			if (CollectionUtils.isNotEmpty(additionalInfo.getHotelOrderSupplierInfos())) {
				int size = additionalInfo.getHotelOrderSupplierInfos().size();
				supplierName = additionalInfo.getHotelOrderSupplierInfos().get(size - 1).getSupplierId();
			}

			if (supplierName.equals("AGODA") && !additionalInfo.getSupplierBnplStatus().equalsIgnoreCase("BookingCharged")
					&& BooleanUtils.isTrue(additionalInfo.getIsBnplBooking())) {
				filteredOrderItem.add(order);
			}
		}
		return filteredOrderItem;
	}
	
	private void updateBnplStatusInHotel(List<DbHotelOrder> hotelOrders) {

		List<HotelBnplMailData> updatedStatus = new ArrayList<>();
		List<String> bookingIds =
				hotelOrders.stream().map(order -> order.getOrder().getBookingId()).collect(Collectors.toList());
		log.info("Filtered booking ids to update bnpl status are {}", bookingIds);
		if (CollectionUtils.isNotEmpty(bookingIds)) {
			hotelOrders.forEach(hotelOrder -> {
				String bookingId = hotelOrder.getOrder().getBookingId();
				try {
					HotelInfo hotelInfo = orderItemCommunicator.getHotelInfo(bookingId);
					String supplierBookingReference = hotelInfo.getMiscInfo().getSupplierBookingReference();
					String supplierId = hotelInfo.getOptions().get(0).getMiscInfo().getSupplierId();
					String oldbnplStatus = hotelInfo.getMiscInfo().getSupplierBnplStatus();
					AdditionalHotelOrderItemInfo additionalInfo = hotelOrder.getOrderItems().get(0).getAdditionalInfo();
					if (CollectionUtils.isNotEmpty(additionalInfo.getHotelOrderSupplierInfos())) {
						int size = additionalInfo.getHotelOrderSupplierInfos().size();
						supplierId = additionalInfo.getHotelOrderSupplierInfos().get(size - 1).getSupplierId();
						supplierBookingReference =
								additionalInfo.getHotelOrderSupplierInfos().get(size - 1).getSupplierBookingId();
					}

					HotelImportBookingParams importBookingParams =
							HotelImportBookingParams.builder().bookingId(bookingId)
									.supplierBookingId(supplierBookingReference).supplierId(supplierId).build();
					String bnplStatus = hmsCommunicator.getBookNowPayLaterStatus(importBookingParams);
					if (StringUtils.isNotBlank(bnplStatus) && !oldbnplStatus.equals(bnplStatus)) {
						
						HotelBnplStatusUpdateRequest updateRequest = 
								HotelBnplStatusUpdateRequest.builder().bookingId(importBookingParams.getBookingId())
								.bnplStatus(bnplStatus).build();
						
						orderItemCommunicator.updateHotelBnplStatus(updateRequest);
						log.info("Hotel bnpl status updated for booking id {} ", bookingId);
					} else {
						bnplStatus = oldbnplStatus;
					}
					if(StringUtils.isNotBlank(bnplStatus) && !bnplStatus.equalsIgnoreCase("BookingCharged")) {
						updatedStatus.add(HotelBnplMailData.builder().bnplStatus(bnplStatus).bookingId(bookingId).build());
					}
				} catch (Exception e) {
					log.info("Error while updating bnpl status for booking id {}", bookingId, e);
				}
			});
			if(updatedStatus.size() > 0) {
				messageHandler.sendBookNowPayLaterMail(updatedStatus, EmailTemplateKey.HOTEL_BOOK_NOW_PAY_LATER_EMAIL,  null);
			}
		}
		log.info("Finished updating filtered booking ids bnpl status are {}", bookingIds);
	}

}
