package com.tgs.services.oms.Amendments.Processors;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelOrderSupplierInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;

@Service
public class HotelSupplierChangeProcessor extends HotelAmendmentProcessor {

	@Override
	protected void processAmendment()  {
		HotelOrder modifiedHotelOrder = getModifiedHotelOrder();
		HotelInfo modifiedHotelInfoWithDeleted = DbHotelOrderItemListToHotelInfo.builder()
				.itemList(new DbHotelOrderItem().toDbList(modifiedHotelOrder.getItems())).build().convert();
		
		// No charging or refund in Supplier Change Amendment 
		amendment.getAdditionalInfo().setOrderDiffAmount(0d);
		amendment.setAmendmentAmount();
		updateModifiedHotelInfoInAmendment(amendment, modifiedHotelInfoWithDeleted);
		if (StringUtils.isNotBlank(requestNote))
			updateNote(amendment);
	}

	@Override
	protected void validate() {
		
		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment)
				.contains(AmendmentAction.PROCESS))
			throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);
		
	}
	
	@Override
	protected HotelOrder getModifiedHotelOrder() {
		BaseHotelUtils.updateTotalFareComponents(hotelOrderRequest.getHInfo(), null);
		DbOrder oldDbOrder = orderService.findByBookingId(amendment.getBookingId());
		List <RoomInfo> roomInfos = hotelOrderRequest.getHInfo().getOptions().get(0).getRoomInfos();
		List<DbHotelOrderItem> itemList = itemManager.getItemList(amendment.getBookingId());
		for(DbHotelOrderItem DbHotelOrder : itemList) {
			RoomInfo roomInfo = roomInfos.stream().filter(x -> x.getId().equals(DbHotelOrder.getRoomInfo().getId().trim())).findFirst().orElse(null);
			if(roomInfo!=null) {
				HotelOrderSupplierInfo hotelOrderSupplierInfo = HotelOrderSupplierInfo.builder()
						.supplierBookingId(hotelOrderRequest.getSupplierBookingId())
						.supplierId(hotelOrderRequest.getSupplier())
						.totalFareComponents(roomInfo.getTotalFareComponents())
						.totalAddlFareComponents(roomInfo.getTotalAddlFareComponents())
						.perNightPriceInfos(roomInfo.getPerNightPriceInfos()).build();
				
				List<HotelOrderSupplierInfo> hotelOrderSupplierInfos = DbHotelOrder.getAdditionalInfo().getHotelOrderSupplierInfos();
				if(hotelOrderSupplierInfos == null) {
					hotelOrderSupplierInfos = new ArrayList<HotelOrderSupplierInfo>();
				}
				hotelOrderSupplierInfos.add(hotelOrderSupplierInfo);
				DbHotelOrder.getAdditionalInfo().setHotelOrderSupplierInfos(hotelOrderSupplierInfos);;
			}
		}
		return HotelOrder.builder().items(DbHotelOrderItem.toDomainList(itemList)).order(oldDbOrder.toDomain()).build();
	}

}
