package com.tgs.services.oms.restcontroller.hotel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;

@Component
public class HotelPostBookingBaseRequestValidator extends TgsValidator implements Validator {

	@Autowired
	HMSCommunicator hmsComm;
	
	@Autowired
	UserServiceCommunicator umsComm;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return HotelPostBookingBaseRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		HotelPostBookingBaseRequest bookingRequest = (HotelPostBookingBaseRequest) target;
		validateSearchQuery(bookingRequest, errors);
		validateDeliveryInfo(bookingRequest, errors);
		validateHotelInformation(bookingRequest, errors);
		
		if(bookingRequest.getTotalMarkup() != null && bookingRequest.getTotalMarkup() < 0)
			rejectValue(errors, "totalMarkup", SystemError.INVALID_VALUE);
		
		if(StringUtils.isBlank(bookingRequest.getSupplier()))
			rejectValue(errors, "supplier", SystemError.NO_SUPPLIER_FOUND);
		else if(hmsComm.getHotelSupplierInfo(bookingRequest.getSupplier()) == null)
			rejectValue(errors, "supplier", SystemError.INVALID_SUPPLIERID);
		
		
		if(StringUtils.isBlank(bookingRequest.getSupplierBookingId()))
			rejectValue(errors, "supplierBookingId", SystemError.NULL_SUPPLIER_BOOKING_ID);
		
		if(StringUtils.isBlank(bookingRequest.getUserId()))
			rejectValue(errors, "userId", SystemError.INVALID_USERID);
		else if(umsComm.getUserFromCache(bookingRequest.getUserId()) == null)
			rejectValue(errors, "userId", SystemError.INVALID_USERID);
		
	}
	

	private void validateDeliveryInfo(HotelPostBookingBaseRequest request, Errors errors) {
		
		DeliveryInfo deliveryInfo = request.getDeliveryInfo();
		
		if(deliveryInfo == null) {
			rejectValue(errors, "deliveryInfo", SystemError.NULL_DELIVERY_INFO);
			return;
		}
		if(CollectionUtils.isEmpty(deliveryInfo.getContacts()))
			rejectValue(errors, "deliveryInfo", SystemError.EMPTY_CONTACT_LIST);
		if(CollectionUtils.isEmpty(deliveryInfo.getEmails()))
			rejectValue(errors, "deliveryInfo", SystemError.EMPTY_EMAIL_LIST);
		
	}

	private void validateSearchQuery(HotelPostBookingBaseRequest request, Errors errors) {
		
		HotelSearchQuery searchQuery = request.getSearchQuery();
		if(searchQuery == null) {
			rejectValue(errors, "searchQuery", SystemError.NULL_CHECKIN_CHECKOUT_DATE);
			return;
		}
		
		LocalDate currentDate = LocalDate.now();
		if(searchQuery.getCheckinDate() == null)
			rejectValue(errors, "searchQuery.checkinDate", SystemError.NULL_CHECKIN_DATE);
		else if(searchQuery.getCheckinDate().isBefore(currentDate))
			rejectValue(errors, "searchQuery.checkinDate", SystemError.HOTEL_CHECKINDATE_VALIDATION);
		
		if(searchQuery.getCheckoutDate() == null)
			rejectValue(errors, "searchQuery.checkoutDate", SystemError.NULL_CHECKOUT_DATE);
		else if(searchQuery.getCheckoutDate().isBefore(searchQuery.getCheckinDate()))
			rejectValue(errors, "searchQuery.checkoutDate", SystemError.INVALID_CHECKIN_CHECKOUT_DATE);
		
	}

	private void validateHotelInformation(HotelPostBookingBaseRequest request, Errors errors) {
		
		HotelInfo hInfo = request.getHInfo();
		HotelSearchQuery searchQuery = request.getSearchQuery();
		if(hInfo == null) {
			rejectValue(errors, "hInfo", SystemError.EMPTY_HOTEL_INFORMATION);
			return;
		}
		if(StringUtils.isBlank(hInfo.getName())) 
			rejectValue(errors, "name" , SystemError.HOTEL_NAME_EMPTY);
		
		validateHotelAddress(hInfo.getAddress(), errors);
		
		Option option = hInfo.getOptions().get(0);
		LocalDateTime deadlineDateTime = option.getDeadlineDateTime();
		if(deadlineDateTime != null) {
			LocalTime time = LocalTime.from(deadlineDateTime);
			if(deadlineDateTime.isAfter(request.getSearchQuery().getCheckinDate().atTime(time))) {
				rejectValue(errors,"hInfo.options[0].deadlineDateTime",SystemError.INVALID_DEADLINEDATETIME);
			}
		}
		
		
		int index = 0;
		for(RoomInfo ri : option.getRoomInfos()) {
			validateRoom(ri, index, errors);
			index++;
		}
	}

	private void validateHotelAddress(Address address, Errors errors) {
		
		if(address == null) {
			rejectValue(errors, "hInfo.address", SystemError.NULL_HOTEL_ADDRESS);
			return;
		}
		
		if(StringUtils.isBlank(address.getAddressLine1()))
			rejectValue(errors, "hInfo.address", SystemError.NULL_HOTEL_ADDRESS);
		
		City city = address.getCity();
		if(city == null)
			rejectValue(errors, "hInfo.address.city", SystemError.NULL_CITY_INFORMATION);
		else if(StringUtils.isBlank(city.getName()))
			rejectValue(errors, "hInfo.address.city.name", SystemError.NULL_CITY_INFORMATION);
		
		Country country = address.getCountry();
		if(country == null)
			rejectValue(errors, "hInfo.address.country", SystemError.NULL_COUNTRY_INFORMATION);
		else if(StringUtils.isBlank(country.getName()))
			rejectValue(errors, "hInfo.address.country.name", SystemError.NULL_COUNTRY_INFORMATION);
		
		
		
	}

	private void validateRoom(RoomInfo ri, int index , Errors errors) {
		
		List<TravellerInfo> travellerInfo = ri.getTravellerInfo();
		if(CollectionUtils.isEmpty(travellerInfo)) return;
		int numberOfAdults = 0;
		int tiIndex = 0;
		for(TravellerInfo ti : travellerInfo) {
			if(ti.getPaxType().equals(PaxType.ADULT)) numberOfAdults++;
			tiIndex++;
		}
		
		if(numberOfAdults == 0)
			rejectValue(errors,"hInfo.options[0].roomInfos["+index+"].travellerInfo["+tiIndex+"]",SystemError.INVALID_NUMBER_OF_ADULTS);
		
		
		List<PriceInfo> priceInfoList = ri.getPerNightPriceInfos();
		if(!CollectionUtils.isEmpty(priceInfoList) 
				&& priceInfoList.get(0).getFareComponents().containsKey(HotelFareComponent.BF) 
				&& ri.getTotalFareComponents().containsKey(HotelFareComponent.BF)) {
			Double totalSupplierPrice = ri.getTotalFareComponents().get(HotelFareComponent.BF);
			Double perNightTotalSupplierPrice = 0.0;
			for(PriceInfo priceInfo : priceInfoList) {
				perNightTotalSupplierPrice += priceInfo.getFareComponents()
						.getOrDefault(HotelFareComponent.BF, 0.0);
			}
			
			if(Double.compare(totalSupplierPrice, perNightTotalSupplierPrice) != 0) {
				rejectValue(errors,"hInfo.options[0].roomInfos["+index+"]", SystemError.INVALID_ROOM_PRICE);
			}
			
		}
		
		if(CollectionUtils.isEmpty(priceInfoList) 
				&& !ri.getTotalFareComponents().containsKey(HotelFareComponent.BF)) {
			rejectValue(errors,"hInfo.options[0].roomInfos["+index+"]", SystemError.NULL_SUPPLIER_PRICE);
		}
		
		
	}

	protected void rejectValue(Errors errors, String field, SystemError systemError) {
		errors.rejectValue(field, systemError.errorCode(), systemError.getMessage());
	}

}
