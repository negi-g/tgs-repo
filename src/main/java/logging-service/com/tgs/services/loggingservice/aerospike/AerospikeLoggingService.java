package com.tgs.services.loggingservice.aerospike;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Predicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AerospikeLoggingService {

	final private static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss.SSS");

	final private static int MAX_PARTITIONS = 10;

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;


	void storeLog(LogData logData) {
		int ttl = 60 * 60;
		if (logData.getTtl() != null) {
			ttl = logData.getTtl().intValue();
		}
		int maxTry = 4;
		int currentPartition = RandomUtils.nextInt(MAX_PARTITIONS);
		String logKey = getKeyForMapPartition(logData.getKey(), currentPartition);
		do {
			try {
				log.debug("Storing logkey {}", logKey);
				CacheMetaInfo metaInfo = getCacheMetaInfo(logKey);
				cachingCommunicator.store(metaInfo, BinName.APILOGS.getName(), Arrays.asList(logData), true, false,
						ttl);
				break;
			} catch (CachingLayerException e) {
				CachingLayerError cle = CachingLayerError.getEnumFromErrorCode(e.getErrorCode());
				if (cle != null
						&& (CachingLayerError.HOT_KEY_ERROR.equals(cle) || CachingLayerError.WRITE_ERROR.equals(cle))) {
					/**
					 * Thread-safe: Two threads which will fail must be trying to write to same partition
					 */
					currentPartition = RandomUtils.nextInt(MAX_PARTITIONS);
					log.debug("{} for key {}, generated new key {} for fetch", cle.getMessage(), logKey,
							getKeyForMapPartition(logData.getKey(), currentPartition));
					logKey = getKeyForMapPartition(logData.getKey(), currentPartition);
				} else {
					throw e;
				}
			}
		} while (--maxTry != 0);
	}

	/**
	 * 
	 * @param key
	 * @return non-null list of logs
	 */
	public List<LogData> getLogs(String key) {
		Map<String, List<LogData>> result = new HashMap<>();
		for (int partition = 0; partition < MAX_PARTITIONS; partition++) {
			String copiedKey = getKeyForMapPartition(key, partition);
			CacheMetaInfo metaInfo = getCacheMetaInfo(copiedKey);
			Map<String, List<LogData>> temp =
					cachingCommunicator.getList(metaInfo, LogData.class, true, false, new String[0]);
			if (MapUtils.isNotEmpty(temp)) {
				for (String logKey : temp.keySet()) {
					List<LogData> logDataList = temp.get(logKey);
					if (CollectionUtils.isEmpty(logDataList)) {
						continue;
					}
					if (result.get(logKey) == null) {
						result.put(logKey, new ArrayList<>());
					}
					result.get(logKey).addAll(logDataList);
				}
			}
		}

		List<LogData> logList = new ArrayList<>();
		for (List<LogData> logListFetched : result.values()) {
			if (logListFetched != null)
				logList.addAll(logListFetched);
		}
		return logList;
	}

	/**
	 * Get logs from aerospike as {@code Map<String, String>}, sorted and formatted.
	 * 
	 * @param key
	 * @return
	 */
	public Map<String, String> getFormattedLogs(String key) {
		return getFormattedLogs(key, false, l -> true);
	}

	/**
	 * Get filtered logs from aerospike as {@code Map<String, String>}, sorted and formatted.
	 * 
	 * @param key
	 * @param filter non-null filter
	 * @return
	 */
	public Map<String, String> getFormattedLogs(String key, boolean showTime, Predicate<LogData> filter) {
		List<LogData> logList = getLogs(key);
		logList.sort(new Comparator<LogData>() {
			@Override
			public int compare(LogData o1, LogData o2) {
				if (o1 == null || o1.getGenerationTime() == null) {
					log.debug("[ProbeLogCheck] Object1 is {}", GsonUtils.getGson().toJson(o1));
					return -1;
				}
				if (o2 == null || o2.getGenerationTime() == null) {
					log.debug("[ProbeLogCheck] Object2 is {}", GsonUtils.getGson().toJson(o2));
					return 1;
				}

				return o1.getGenerationTime().compareTo(o2.getGenerationTime());
			}

		});

		/**
		 * Used LinkedHashMap to maintain order
		 */
		Map<String, String> formattedLogs = new LinkedHashMap<>();
		for (LogData logData : logList) {
			log.debug("Fetching log for key {}, log type {}", logData.getKey(), logData.getType());
			if (logData != null && StringUtils.isNotBlank(logData.getLogData()) && filter.test(logData)) {
				StringJoiner logKey = new StringJoiner(":");
				logKey.add(logData.getType());
				logKey.add(logData.getLoggedInUserId());
				logKey.add(logData.getGenerationTime().format(FORMATTER));
				if (showTime && Objects.nonNull(logData.getResponseTime())) {
					logKey.add(" API Response time = " + logData.getResponseTime() + " ms");
				}
				formattedLogs.put(logKey.toString(), logData.getLogData().replaceAll("'", "&#x27;"));
			}
		}
		return formattedLogs;
	}

	private CacheMetaInfo getCacheMetaInfo(String logKey) {
		return CacheMetaInfo.builder().key(logKey).namespace(CacheNameSpace.LOGS.getName()).kyroCompress(true)
				.set(CacheSetName.LOGS.getName()).build();
	}

	private String getKeyForMapPartition(String key, int partition) {
		if (partition == 0) {
			return key;
		}
		return new StringBuilder(key).append('_').append(partition).toString();
	}

}
