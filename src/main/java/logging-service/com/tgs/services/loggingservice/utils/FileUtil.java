package com.tgs.services.loggingservice.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.util.IO;

public class FileUtil {

	public static void writeZipFile(OutputStream outputStream, Map<String, String> filesData) throws IOException {
		if (filesData == null || filesData.isEmpty())
			return;

		ZipOutputStream zos = new ZipOutputStream(outputStream);

		for (String fileName : filesData.keySet()) {
			ZipEntry zipEntry = new ZipEntry(appendTextExtension(fileName));
			zos.putNextEntry(zipEntry);
			Object data = filesData.get(fileName);
			if (data == null)
				zos.write("null".getBytes());
			else
				zos.write(data.toString().getBytes());
		}
		zos.close();
	}

	public static Map<String, String> readZipFileAsString(InputStream inputStream) throws IOException {
		if (inputStream == null)
			return null;

		Map<String, String> filesData = new LinkedHashMap<>();
		ZipInputStream zis = new ZipInputStream(inputStream);

		ZipEntry zipEntry;
		while ((zipEntry = zis.getNextEntry()) != null) {
			String fileName = removeTextExtension(zipEntry.getName());
			long size = zipEntry.getSize();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IO.copy(zis, baos, size);
			filesData.put(fileName, baos.toString());
		}
		zis.close();

		return filesData;
	}

	private static String appendTextExtension(String fileName) {
		return fileName + ".txt";
	}

	private static String removeTextExtension(String fileName) {
		if (StringUtils.isBlank(fileName)) {
			return fileName;
		}
		int lastIndexOfDot = fileName.lastIndexOf('.');
		if (lastIndexOfDot < 0) {
			return fileName;
		}
		return fileName.substring(0, lastIndexOfDot);
	}
}
