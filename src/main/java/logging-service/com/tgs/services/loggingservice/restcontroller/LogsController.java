package com.tgs.services.loggingservice.restcontroller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.loggingservice.aerospike.AerospikeLogBackupService;

@RestController
@RequestMapping("/logs/v1")
public class LogsController {

	@Autowired
	private AerospikeLogBackupService aerospikeLogBackupService;

	@RequestMapping(value = "/aerospike/job/backup-service/start", method = RequestMethod.GET)
	protected @ResponseBody BaseResponse backup() throws Exception {
		aerospikeLogBackupService.startBackupService();
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/store", method = RequestMethod.POST)
	protected @ResponseBody BaseResponse StoreLogs(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody LogData logData) throws Exception {
		LogUtils.store(Arrays.asList(LogData.builder().generationTime(logData.getGenerationTime()).logData(GsonUtils.getGson().toJson(logData))
				.key(logData.getKey()).ttl(logData.getTtl()).type(logData.getType()).logType(logData.getLogType() != null ? logData.getLogType(): "AirSupplierAPILogs")
				.build()));
		return new BaseResponse();
	}
}
