package com.tgs.services.loggingservice;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.ums.datamodel.User;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.LogData;
import com.tgs.services.base.LoggingClient;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.LogServiceOutput;
import com.tgs.services.base.configurationmodel.LoggingClientInfo;
import com.tgs.services.base.configurationmodel.LoggingInfo;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.loggingservice.aerospike.AerospikeLogging;
import lombok.Getter;

@Getter
@Service
public class LogManager {

	@Autowired
	GeneralServiceCommunicator gmsService;

	@Autowired
	FileLogging fileLogging;

	@Autowired
	AerospikeLogging aerospikeLogging;

	@Autowired
	ElasticSearchLogging esLogging;

	@Autowired
	ServiceHealthLogging fileHealthLogging;

	@Autowired
	MissingInfoLogging missingInfoLogging;

	public LoggingClient getLoggingClient(String client) {
		if (client.equals("HEALTHLOGGING")) {
			return fileHealthLogging;
		} else if (client.equals("ELASTICSEARCHLOGGING")) {
			return esLogging;
		} else if (client.equals("FILELOGGING")) {
			return fileLogging;
		} else if (client.equals("AEROLOGGING")) {
			return aerospikeLogging;
		} else if (client.equals("MISSINGINFOLOGGING")) {
			return missingInfoLogging;
		}
		return null;
	}

	public List<LoggingClientInfo> getLogInfo(LogData logData) {
		List<LoggingInfo> logInfoList = null;
		GeneralBasicFact basicFact = GeneralBasicFact.builder().applicableTime(LocalDateTime.now()).build();
		User user = SystemContextHolder.getContextData().getUser();
		if (user != null) {
			basicFact.setUserId(user.getParentUserId());
		}
		LogServiceOutput serviceOutput = gmsService.getConfigRule(ConfiguratorRuleType.LOGINFO, basicFact);
		if (serviceOutput != null) {
			logInfoList = serviceOutput.getLogInfoMap().get(logData.getLogType());
		}

		if (CollectionUtils.isNotEmpty(logInfoList)) {
			for (LoggingInfo info : logInfoList) {
				if (((StringUtils.isBlank(info.getKey()) && CollectionUtils.isEmpty(info.getKeys()))
						|| (StringUtils.isNotBlank(info.getKey()) && StringUtils.isNotBlank(logData.getKey())
								&& logData.getKey().toLowerCase().matches(info.getKey())))
						&& validOtherParams(logData, info)) {
					return info.getClients();
				} else if (StringUtils.isNotBlank(logData.getKey()) && CollectionUtils.isNotEmpty(info.getKeys())) {
					for (String key : info.getKeys()) {
						if (logData.getKey().toLowerCase().matches(key) && validOtherParams(logData, info)) {
							return info.getClients();
						}
					}
				}
			}
		}

		return new ArrayList<>();
	}

	public boolean validOtherParams(LogData logData, LoggingInfo info) {
		/**
		 * logData.getUserId()!=null and logData.getUserRole()!= null is the temporary behavior , it will be removed
		 * post some testing
		 */

		if (CollectionUtils.isNotEmpty(info.getExcludedTypes())
				&& (StringUtils.isBlank(logData.getType()) || isValidType(logData, info.getExcludedTypes()))) {
			return false;
		}

		if (CollectionUtils.isNotEmpty(info.getUserRoles())
				&& (logData.getUserRole() != null && !info.getUserRoles().contains(logData.getUserRole()))) {
			return false;
		}
		if (CollectionUtils.isNotEmpty(info.getUserIds())
				&& (logData.getUserId() != null && !info.getUserIds().contains(logData.getUserId()))) {
			return false;
		}

		if (CollectionUtils.isNotEmpty(info.getIncludedTypes()) && StringUtils.isNotBlank(logData.getType())) {
			return isValidType(logData, info.getIncludedTypes());
		}

		return true;
	}

	private boolean isValidType(LogData logData, List<String> types) {
		if (CollectionUtils.isNotEmpty(types)) {
			return types.stream().anyMatch(chars -> TgsStringUtils.matchesAnyIgnoreCase(chars, logData.getType()));
		}
		return false;
	}

}
