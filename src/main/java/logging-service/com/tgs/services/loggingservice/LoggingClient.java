package com.tgs.services.loggingservice;

public enum LoggingClient {
	HEALTHLOGGING, ELASTICSEARCHLOGGING, FILELOGGING, AEROLOGGING
}
