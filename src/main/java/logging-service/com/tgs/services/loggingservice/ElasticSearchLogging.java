package com.tgs.services.loggingservice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;
import com.tgs.services.base.LogData;
import com.tgs.services.base.LoggingClient;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;


@Service
public class ElasticSearchLogging implements LoggingClient {


	@Autowired
	KafkaServiceCommunicator serviceCommunicator;


	@Override
	public void store(List<LogData> logDataList) {
		logDataList.forEach(logData -> {
			store(logData);
		});
	}

	@Override
	public void store(LogData logData) {
		logData.setLogData(null);
		logData.setSupplier(null);
		logData.setType(null);
		logData.setLogType(null);
		QueueData queueData = QueueData.builder().key("health").value(GsonUtils.getGson().toJson(logData)).build();
		serviceCommunicator.queue(QueueDataType.ELASTICSEARCH, queueData);
	}

	@Override
	public List<LogData> getLogs(LogData logData) {
		// TODO Auto-generated method stub
		return null;
	}
}
