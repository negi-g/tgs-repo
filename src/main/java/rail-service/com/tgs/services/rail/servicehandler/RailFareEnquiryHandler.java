package com.tgs.services.rail.servicehandler;

import java.util.concurrent.Future;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.rail.datamodel.RailFareEnquiry;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailSearchQuery;
import com.tgs.services.rail.restmodel.RailFareEnquiryRequest;
import com.tgs.services.rail.restmodel.RailFareEnquiryResponse;
import com.tgs.services.rail.sources.irctc.IrctcSearchManager;
import com.tgs.services.rail.utils.RailUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailFareEnquiryHandler extends ServiceHandler<RailFareEnquiryRequest, RailFareEnquiryResponse> {

	@Override
	public void beforeProcess() throws Exception {}

	public RailJourneyInfo getFareAvailability(RailFareEnquiry fareEnquiry, String searchId) {
		RailJourneyInfo newJourneyInfo = null;
		try {
			fareEnquiry.setSearchId(searchId);
			IrctcSearchManager searchManager =
					(IrctcSearchManager) SpringContext.getApplicationContext().getBean("irctcSearchManager");
			searchManager.init();
			newJourneyInfo = searchManager.doSearchFareAvailability(fareEnquiry);
		} catch (Exception e) {
			log.error("Error occured while fetching fare availability due to {}", e.getMessage());
			throw e;
		}
		return newJourneyInfo;
	}

	@Override
	public void process() throws Exception {
		User user = SystemContextHolder.getContextData().getUser();
		if (RailUtils.isUserDisabled(user)) {
			response.addError(new ErrorDetail(SystemError.INACTIVE_RAIL_ACCOUNT.getErrorCode(),
					SystemError.INACTIVE_RAIL_ACCOUNT.getMessage()));

		} else {
			try {
				RailFareEnquiry fareEnquiry = RailUtils.preBuildFareEnquiryRequest(request);
				fareEnquiry.setPaymentEnqFlag(false);
				RailSearchQuery searchQuery = RailUtils.fetchSearchQueryFromRequest(fareEnquiry, request.getSearchId());
				log.info("getFareAvailability for searchId {}", searchQuery.getSearchId());
				Future<RailJourneyInfo> futureTask = ExecutorUtils.getRailSearchThreadPool()
						.submit(() -> getFareAvailability(fareEnquiry, request.getSearchId()));
				RailJourneyInfo newJourneyInfo = futureTask.get();
				if (newJourneyInfo != null) {
					response.setJourneyInfo(newJourneyInfo);
				}
			} catch (Exception e) {
				response.addError(new ErrorDetail(SystemError.FARE_ENQUIRY_FAILED.getErrorCode(),
						SystemError.FARE_ENQUIRY_FAILED.getMessage(e.getMessage())));
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {}


}
