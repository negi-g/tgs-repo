package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.TrainBtwnStnsRespDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.restmodel.VikalpTrainListResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcVikalpTrainListManager extends IrctcSearchManager {

	private static final String VIKALP_ENQUIRY_SUFFIX = "/tatktservices/vikalpTrainList";

	public VikalpTrainListResponse retrieveVikalpTrainList(String transactionId) throws IOException {
		VikalpTrainListResponse trainRouteResponse = VikalpTrainListResponse.builder().build();
		try {
			String resp = sendGET(getVikalpTrainEnquiryUrl(transactionId));
			TrainBtwnStnsRespDTO response = GsonUtils.getGson().fromJson(resp, TrainBtwnStnsRespDTO.class);
			if (StringUtils.isEmpty(response.getErrorMessage()))
				return parseVikalpTrainListResponse(response);
			else
				throw new CustomGeneralException(response.getErrorMessage());
		} catch (IOException e) {
			log.error("Error occured during vikalp train list enquiry of transactionId {} because {} ", transactionId,
					e.getMessage());
		}
		return trainRouteResponse;
	}

	private String getVikalpTrainEnquiryUrl(String transactionId) {
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(sourceConfiguration.getUrl(), VIKALP_ENQUIRY_SUFFIX);
		} else {
			apirUrl = apiURLS.getVikalpTrainEnquiryURL();
		}
		apirUrl = apirUrl.concat("/").concat(transactionId);
		log.debug("IRCTC vikalp enquiry URL {}", apirUrl);
		return apirUrl;
	}

	private VikalpTrainListResponse parseVikalpTrainListResponse(TrainBtwnStnsRespDTO irctcTrainRouteResponse) {
		List<RailJourneyInfo> journeys = getJourneys(irctcTrainRouteResponse, null);
		return VikalpTrainListResponse.builder().journeyInfo(journeys)
				.vikalpInSpecialTrains(irctcTrainRouteResponse.getVikalpInSpecialTrains()).build();
	}

}
