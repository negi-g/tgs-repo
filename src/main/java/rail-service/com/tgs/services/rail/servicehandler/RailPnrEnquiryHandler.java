package com.tgs.services.rail.servicehandler;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.rail.restmodel.RailPnrEnquiryRequest;
import com.tgs.services.rail.restmodel.RailPnrEnquiryResponse;
import com.tgs.services.rail.sources.irctc.IrctcPnrRetrieveManager;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailPnrEnquiryHandler extends ServiceHandler<RailPnrEnquiryRequest, RailPnrEnquiryResponse> {

	@Autowired
	IrctcPnrRetrieveManager retrieveManager;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		try {
			retrieveManager.init();
			response = retrieveManager.retrievePnrDetails(request.getPnrNumber());
			log.info("Pnr enquiry response is {}", GsonUtils.getGson().toJson(response));
		} catch (Exception e) {
			response.addError(new ErrorDetail(SystemError.PNR_ENQUIRY_FAILED.getErrorCode(),
					SystemError.PNR_ENQUIRY_FAILED.getMessage(e.getMessage())));
			LogUtils.log(request.getPnrNumber(), "PnrEnquiry", e);
		}
	}

	@Override
	public void afterProcess() throws Exception {
		if (StringUtils.isBlank(request.getPnrNumber())) {
			response.addError(new ErrorDetail(SystemError.NO_PNR_FOUND));
		}
	}

}
