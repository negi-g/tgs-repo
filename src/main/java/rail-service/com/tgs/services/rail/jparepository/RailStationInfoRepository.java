package com.tgs.services.rail.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.tgs.services.rail.dbmodel.DbRailStationInfo;

public interface RailStationInfoRepository
		extends JpaRepository<DbRailStationInfo, Long>, JpaSpecificationExecutor<DbRailStationInfo> {

	DbRailStationInfo findByCode(String code);

	@Query(value = "SELECT a.* FROM railstationinfo a WHERE a.code ilike :searchQuery% or a.name ilike :searchQuery%",
			nativeQuery = true)
	public List<DbRailStationInfo> fetchRailStations(@Param("searchQuery") String searchQuery);


}
