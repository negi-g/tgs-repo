package com.tgs.services.rail.restcontroller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.jparepository.RailStationInfoService;
import com.tgs.services.rail.restmodel.RailStationInfoResponse;
import com.tgs.services.rail.restmodel.RailUserAvailabilityRequest;
import com.tgs.services.rail.restmodel.RailUserAvailabilityResponse;
import com.tgs.services.rail.servicehandler.RailStationHandler;
import com.tgs.services.rail.servicehandler.RailUserAvailabilityHandler;

@RestController
@RequestMapping("/rail/v1")
public class RailCommonController {

	@Autowired
	RailStationHandler stationHandler;

	@Autowired
	RailStationInfoService stationService;

	@Autowired
	RailUserAvailabilityHandler railUserHandler;

	@RequestMapping(value = "/station/save", method = RequestMethod.POST)
	protected RailStationInfoResponse saveStation(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RailStationInfo stationInfo) throws Exception {
		stationHandler.initData(stationInfo, new RailStationInfoResponse());
		return stationHandler.getResponse();
	}


	@RequestMapping(value = "/stations/search/{query}", method = RequestMethod.GET)
	protected RailStationInfoResponse getRailStationsList(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("query") String searchQuery) {
		List<RailStationInfo> railStationInfoList = new ArrayList<>();
		stationService.fetchRailStations(searchQuery).forEach(railStationInfo -> railStationInfoList
				.add(new GsonMapper<>(railStationInfo, RailStationInfo.class).convert()));
		RailStationInfoResponse searchResponse = new RailStationInfoResponse();
		searchResponse.setStationInfos(railStationInfoList);
		return searchResponse;
	}

	@RequestMapping(value = "/check/user-availability", method = RequestMethod.POST)
	protected RailUserAvailabilityResponse checkAvailability(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RailUserAvailabilityRequest avlRequest) throws Exception {
		railUserHandler.initData(avlRequest, RailUserAvailabilityResponse.builder().build());
		return railUserHandler.getResponse();
	}


}
