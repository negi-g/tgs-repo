package com.tgs.services.rail.servicehandler;

import java.util.Objects;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.dbmodel.DbRailStationInfo;
import com.tgs.services.rail.jparepository.RailStationInfoService;
import com.tgs.services.rail.restmodel.RailStationInfoResponse;

@Service
public class RailStationHandler extends ServiceHandler<RailStationInfo, RailStationInfoResponse> {

	@Autowired
	RailStationInfoService stationService;

	@Autowired
	SyncService syncService;

	@Override
	public void beforeProcess() throws Exception {
		if (StringUtils.isBlank(request.getCode()) || StringUtils.isBlank(request.getName())) {
			throw new CustomGeneralException("Code or Name cannot be blank");
		}
	}

	@Override
	public void process() throws Exception {
		DbRailStationInfo stationInfo = null;
		if (Objects.nonNull(request.getId())) {
			stationInfo = stationService.findById(request.getId());
		}
		if (Objects.nonNull(request.getCode())) {
			stationInfo = stationService.findbyCode(request.getCode());
		}
		stationInfo = Optional.ofNullable(stationInfo).orElse(new DbRailStationInfo()).from(request);
		stationInfo = stationService.save(stationInfo);
		request.setId(stationInfo.getId());
		response.getStationInfos().add(stationInfo.toDomain());
		syncService.sync("rail", stationInfo.toDomain());
	}

	@Override
	public void afterProcess() throws Exception {


	}

}
