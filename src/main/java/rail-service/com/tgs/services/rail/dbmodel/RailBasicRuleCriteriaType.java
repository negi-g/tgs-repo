package com.tgs.services.rail.dbmodel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.rail.datamodel.ruleengine.RailBasicRuleCriteria;

public class RailBasicRuleCriteriaType extends CustomUserType {

	@Override
	public Class<?> returnedClass() {
		return RailBasicRuleCriteria.class;
	}
}
