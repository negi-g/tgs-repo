package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.IrctcStationScheduleDTO;
import com.irctc.datamodel.IrctcTrainRouteDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.rail.datamodel.RailInfo;
import com.tgs.services.rail.datamodel.RailRouteInfo;
import com.tgs.services.rail.datamodel.RailRunningDays;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.helper.RailStationHelper;
import com.tgs.services.rail.restmodel.RailTrainScheduleResponse;
import com.tgs.services.rail.utils.RailTempUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcTrainScheduleRetrieveManager extends IrctcServiceManager {

	private static final String SCHEDULE_ENQUIRY_SUFFIX = "/taenqservices/trnscheduleEnq";

	public RailTrainScheduleResponse retrieveTrainSchedule(String trainNo, String journeyDate, String depStn,
			String searchKey) throws IOException {
		RailTrainScheduleResponse trainRouteResponse = RailTrainScheduleResponse.builder().build();
		String trainScheduleUrl = StringUtils.EMPTY;
		String resp = StringUtils.EMPTY;
		String newTrainNo = RailTempUtils.getNewTrainNo(trainNo);
		log.debug("Old train no. in train schedule is {} and new train no. is {}", trainNo, newTrainNo);
		try {
			trainScheduleUrl = getTrainScheduleUrl(newTrainNo, journeyDate, depStn);
			resp = sendGET(trainScheduleUrl);
			IrctcTrainRouteDTO response = GsonUtils.getGson().fromJson(resp, IrctcTrainRouteDTO.class);
			if (StringUtils.isEmpty(response.getErrorMessage()))
				return parseTrainRouteResponse(response);
			else
			// Temp handling - to be removed after 23rd nov, this is for the trains whose
			// new number has yet to be
			// changed
			if (!newTrainNo.equals(trainNo)) {
				log.info("Retrying train schedule of old train number {} with new train number  {}", newTrainNo,
						trainNo);
				trainScheduleUrl = getTrainScheduleUrl(trainNo, journeyDate, depStn);
				resp = sendGET(trainScheduleUrl);
				IrctcTrainRouteDTO newResponse = GsonUtils.getGson().fromJson(resp, IrctcTrainRouteDTO.class);
				if (StringUtils.isEmpty(newResponse.getErrorMessage()))
					return parseTrainRouteResponse(newResponse);
				else
					throw new CustomGeneralException(newResponse.getErrorMessage());

			} else {
				throw new CustomGeneralException(response.getErrorMessage());
			}
		} catch (IOException e) {
			log.error("Error occured during schedule enquiry of train no  {} because {} ", trainNo, e.getMessage());
		} finally {
			String endPointRQRS = StringUtils.join(formatUrl(trainScheduleUrl), formatRQRS(resp, "Train Schedule RS"));
			listener.addLog(LogData.builder().key(searchKey).logData(endPointRQRS).type("Train Schedule RS").build());
		}
		return trainRouteResponse;
	}

	private String getTrainScheduleUrl(String trainNo, String journeyDate, String depStn) {
		String apiUrl = null;
		if (apiURLS == null) {
			apiUrl = StringUtils.join(sourceConfiguration.getUrl(), SCHEDULE_ENQUIRY_SUFFIX);
		} else {
			apiUrl = apiURLS.getScheduleEnquiryURL();
		}
		apiUrl = apiUrl.concat("/").concat(trainNo);
		apiUrl = StringUtils.join(apiUrl, "?journeyDate=", journeyDate, "&startingStationCode=", depStn);
		log.debug("IRCTC train schedule enquiry URL {}", apiUrl);
		return apiUrl;
	}

	private RailTrainScheduleResponse parseTrainRouteResponse(IrctcTrainRouteDTO irctcTrainRouteResponse) {
		RailInfo railInfo = RailInfo.builder().name(irctcTrainRouteResponse.getTrainName())
				.number(irctcTrainRouteResponse.getTrainNumber()).build();
		RailStationInfo sourceStation = RailStationHelper.getRailStation(irctcTrainRouteResponse.getStationFrom());
		RailStationInfo destStation = RailStationHelper.getRailStation(irctcTrainRouteResponse.getStationTo());
		RailRunningDays runningDays = parseRunningDays(irctcTrainRouteResponse);
		List<RailRouteInfo> routeinfos = new ArrayList<>();
		irctcTrainRouteResponse.getStationList().forEach(p -> routeinfos.add(convertRailRouteInfo(p)));
		RailTrainScheduleResponse response = RailTrainScheduleResponse.builder().railInfo(railInfo)
				.arrivalStaionInfo(destStation)
				.departStationInfo(sourceStation).runningDays(runningDays).routeInfos(routeinfos).build();
		return response;
	}

	private RailRouteInfo convertRailRouteInfo(IrctcStationScheduleDTO stationInfo) {
		RailStationInfo railStationInfo = RailStationHelper.getRailStation(stationInfo.getStationCode());
		RailRouteInfo routeInfo = RailRouteInfo.builder().arrivalTime(convertToLocalTime(stationInfo.getArrivalTime()))
				.departureTime(convertToLocalTime(stationInfo.getDepartureTime()))
				.distance(Integer.valueOf(stationInfo.getDistance())).haltTime(stationInfo.getHaltTime())
				.routeNumber(Integer.valueOf(stationInfo.getRouteNumber()))
				.stnSerialNumber(Integer.valueOf(stationInfo.getStnSerialNumber())).stationInfo(railStationInfo)
				.dayCount(Integer.valueOf(stationInfo.getDayCount())).build();
		return routeInfo;
	}

	private RailRunningDays parseRunningDays(IrctcTrainRouteDTO irctcTrainRouteResponse) {
		RailRunningDays runningDays = RailRunningDays.builder()
				.isRunningSun(StringUtils.equalsIgnoreCase("Y", irctcTrainRouteResponse.getTrainRunsOnSun()))
				.isRunningMon(StringUtils.equalsIgnoreCase("Y", irctcTrainRouteResponse.getTrainRunsOnMon()))
				.isRunningTue(StringUtils.equalsIgnoreCase("Y", irctcTrainRouteResponse.getTrainRunsOnTue()))
				.isRunningWed(StringUtils.equalsIgnoreCase("Y", irctcTrainRouteResponse.getTrainRunsOnWed()))
				.isRunningThu(StringUtils.equalsIgnoreCase("Y", irctcTrainRouteResponse.getTrainRunsOnThu()))
				.isRunningFri(StringUtils.equalsIgnoreCase("Y", irctcTrainRouteResponse.getTrainRunsOnFri()))
				.isRunningSat(StringUtils.equalsIgnoreCase("Y", irctcTrainRouteResponse.getTrainRunsOnSat())).build();
		return runningDays;
	}

	// Departure, arrival, halt Time of 1st and last station can be "--"
	private LocalTime convertToLocalTime(String time) {
		if (!"--".equals(time))
			return LocalTime.parse(time);
		return null;
	}
}
