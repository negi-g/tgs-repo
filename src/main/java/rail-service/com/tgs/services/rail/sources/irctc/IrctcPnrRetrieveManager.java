package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.PassengerDetailDTO;
import com.irctc.datamodel.PnrEnquiryResponseDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.rail.datamodel.RailInfo;
import com.tgs.services.rail.datamodel.RailJourneyClass;
import com.tgs.services.rail.datamodel.RailSeatAllocation;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.helper.RailStationHelper;
import com.tgs.services.rail.restmodel.RailPnrEnquiryResponse;
import com.tgs.services.rail.utils.RailParsingHelper;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcPnrRetrieveManager extends IrctcServiceManager {

	private static final String PNR_ENQUIRY_SUFFIX = "/taenqservices/pnrenquiry";

	public RailPnrEnquiryResponse retrievePnrDetails(String pnr) throws IOException {
		String resp = StringUtils.EMPTY;
		String retrievePnrUrl = StringUtils.EMPTY;
		try {
			retrievePnrUrl = getPnrEnquiryUrl(pnr);
			resp = sendGET(retrievePnrUrl);
			PnrEnquiryResponseDTO response = GsonUtils.getGson().fromJson(resp, PnrEnquiryResponseDTO.class);
			if (StringUtils.isEmpty(response.getErrorMessage()))
				return parsePnrEnquiryResponse(response);
			else
				throw new CustomGeneralException(response.getErrorMessage());
		} catch (IOException e) {
			log.error("Error occured during Pnr Enquiry of Pnr  {} because {} ", pnr, e.getMessage());
			throw e;
		} finally {
			String endPointRQRS = StringUtils.join(formatUrl(retrievePnrUrl), formatRQRS(resp, "RetrivePnrDetails"));
			listener.addLog(LogData.builder().key(pnr).logData(endPointRQRS).type("Irctc-PnrRetrieve-Req").build());
		}
	}

	private String getPnrEnquiryUrl(String pnr) {
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(sourceConfiguration.getUrl(), PNR_ENQUIRY_SUFFIX);
		} else {
			apirUrl = apiURLS.getPnrEnquiryURl();
		}
		apirUrl = apirUrl.concat("/").concat(pnr);
		// String url = "https://testngetjp.irctc.co.in/eticketing/webservices/taenqservices/pnrenquiry/";
		log.info("IRCTC Pnr enquiry URL {}", apirUrl);
		return apirUrl;
	}

	private RailPnrEnquiryResponse parsePnrEnquiryResponse(PnrEnquiryResponseDTO irctcPnrResponse) {
		RailInfo railInfo = RailInfo.builder().name(irctcPnrResponse.getTrainName())
				.number(irctcPnrResponse.getTrainNumber()).build();
		RailStationInfo sourceStation = RailStationHelper.getRailStation(irctcPnrResponse.getSourceStation());
		RailStationInfo destStation = RailStationHelper.getRailStation(irctcPnrResponse.getDestinationStation());
		RailStationInfo boardingStation = RailStationHelper.getRailStation(irctcPnrResponse.getBoardingPoint());
		RailStationInfo reservationUptoStation =
				RailStationHelper.getRailStation(irctcPnrResponse.getReservationUpto());
		List<RailTravellerInfo> travellerInfos = new ArrayList<>();
		for (PassengerDetailDTO pax : RailParsingHelper.getValueFromIrctc(irctcPnrResponse.getPassengerList(),
				PassengerDetailDTO.class)) {
			travellerInfos.add(convertRailTravellerInfo(pax));
		}
		RailPnrEnquiryResponse pnrResponse = RailPnrEnquiryResponse.builder().pnrNumber(irctcPnrResponse.getPnrNumber())
				.journeyDate(convertStringToDate(irctcPnrResponse.getDateOfJourney(), "yyyy-MM-dd'T'HH:mm:ss"))
				.railInfo(railInfo).sourceStation(sourceStation).destinationStation(destStation)
				.reservationUptoStation(reservationUptoStation).boardingStation(boardingStation)
				.journeyClass(RailJourneyClass.getEnumFromCode(irctcPnrResponse.getJourneyClass()))
				.passengerCount(Integer.valueOf(irctcPnrResponse.getNumberOfpassenger()))
				.chartStatus(irctcPnrResponse.getChartStatus()).quota(irctcPnrResponse.getQuota())
				.bookingFare(Double.valueOf(irctcPnrResponse.getBookingFare())).travellerInfo(travellerInfos)
				.ticketFare(Double.valueOf(irctcPnrResponse.getTicketFare())).build();
		return pnrResponse;
	}

	private RailTravellerInfo convertRailTravellerInfo(PassengerDetailDTO irctcPaxInfo) {
		RailSeatAllocation bookingSeatAllocation = RailSeatAllocation.builder()
				.coachId(irctcPaxInfo.getBookingCoachId()).berthNo(Integer.valueOf(irctcPaxInfo.getBookingBerthNo()))
				.berthchoice(irctcPaxInfo.getBookingBerthCode()).bookingStatus(irctcPaxInfo.getBookingStatus()).build();
		// If status is CNF, and then current berth no is 0 in irctc response. Actual seat allocation details are
		// available in bookingSeatAllocation
		RailSeatAllocation currentSeatAllocation =
				RailSeatAllocation.builder().coachId(StringUtils.isBlank(irctcPaxInfo.getCurrentCoachId()) ? irctcPaxInfo.getBookingCoachId() : irctcPaxInfo.getCurrentCoachId())
						.berthNo(StringUtils.isBlank(irctcPaxInfo.getCurrentCoachId())
								&& "0".equals(irctcPaxInfo.getCurrentBerthNo()) ? Integer.valueOf(irctcPaxInfo.getBookingBerthNo())
										: Integer.valueOf(irctcPaxInfo.getCurrentBerthNo()))
						.berthchoice(StringUtils.isBlank(irctcPaxInfo.getCurrentBerthCode()) ? irctcPaxInfo.getBookingBerthCode() : irctcPaxInfo.getCurrentBerthCode()).bookingStatus(irctcPaxInfo.getCurrentStatus())
						.build();
		RailTravellerInfo travellerInfo = RailTravellerInfo.builder().bookingSeatAllocation(bookingSeatAllocation)
				.currentSeatAllocation(currentSeatAllocation).gender(irctcPaxInfo.getPassengerGender())
				.nationality(irctcPaxInfo.getPassengerNationality()).build();
		travellerInfo.setAge(Integer.valueOf(irctcPaxInfo.getPassengerAge()));
		travellerInfo.setNationality(irctcPaxInfo.getPassengerNationality());
		return travellerInfo;
	}

}
