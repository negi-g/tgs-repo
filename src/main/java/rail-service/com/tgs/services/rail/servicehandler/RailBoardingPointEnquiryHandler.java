package com.tgs.services.rail.servicehandler;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.RailOrderItemCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.rail.restmodel.RailBoardingPointEnquiryRequest;
import com.tgs.services.rail.restmodel.RailBoardingStationListResponse;
import com.tgs.services.rail.sources.irctc.IrctcBoardingPointsRetrieveManager;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailBoardingPointEnquiryHandler
		extends ServiceHandler<RailBoardingPointEnquiryRequest, RailBoardingStationListResponse> {

	@Autowired
	protected IrctcBoardingPointsRetrieveManager retrieveManager;

	@Autowired
	protected RailOrderItemCommunicator orderItemCommunicator;

	@Override
	public void beforeProcess() throws Exception {
		if (StringUtils.isEmpty(request.getBookingId()) && StringUtils.isEmpty(request.getJourneyId()))
			throw new CustomGeneralException(SystemError.INVALID_BOARDING_REQUEST);
	}

	@Override
	public void process() throws Exception {
		retrieveManager.init();
		String key = StringUtils.EMPTY;
		try {
			if (StringUtils.isEmpty(request.getBookingId())) {
				String[] journeyIdData = request.getJourneyId().split("_");
				String journeyDate = journeyIdData[5].split("T")[0].replaceAll("-", "");
				key = journeyIdData[0];
				response = retrieveManager.retrieveBoardingPoints(journeyIdData[4], journeyDate, journeyIdData[2],
						journeyIdData[3], request.getJourneyClass(), key);
			} else {
				RailOrderItem railOrderItem = orderItemCommunicator.getRailOrderItem(request.getBookingId());
				String trainNo = railOrderItem.getAdditionalInfo().getRailInfo().getNumber();
				String journeyDate = (railOrderItem.getAdditionalInfo().getBoardingTime() != null
						? railOrderItem.getAdditionalInfo().getBoardingTime()
						: railOrderItem.getDepartureTime()).toLocalDate().toString().replaceAll("-", "");
				String boardingFrom = ObjectUtils.firstNonNull(railOrderItem.getAdditionalInfo().getBoardingStation(),
						railOrderItem.getFromStn());
				String toStation = ObjectUtils.firstNonNull(
						railOrderItem.getAdditionalInfo().getReservationUptoStation(), railOrderItem.getToStn());
				key = request.getBookingId();
				response = retrieveManager.retrieveBoardingPoints(trainNo, journeyDate, boardingFrom, toStation,
						railOrderItem.getAdditionalInfo().getJourneyClass().getCode(), key);
			}
			log.debug("Rail boaridng point enquiry response is {}", GsonUtils.getGson().toJson(response));
		} catch (Exception e) {
			log.error("Error occured during boarding point enquiry for searchKey : {} , due to {}", key, e.getMessage(),
					e);
		}
	}

	@Override
	public void afterProcess() throws Exception {}

}
