package com.tgs.services.rail.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.rail.restmodel.RailCitiesResponse;
import com.tgs.services.rail.restmodel.RailPostOfficeEnquiryRequest;
import com.tgs.services.rail.restmodel.RailPostOfficeListResponse;
import com.tgs.services.rail.sources.irctc.IrctcCitiesRetrieveManager;
import com.tgs.services.rail.sources.irctc.IrctcPostOfficeRetrieveManager;

@RestController
@RequestMapping("/rail/v1/pin")
public class RailPostOfficeController {

	@Autowired
	protected IrctcCitiesRetrieveManager irctcCitiesRetrieveManager;

	@Autowired
	protected IrctcPostOfficeRetrieveManager postOfficeRetreiveManager;

	@RequestMapping(value = "/city/{pinCode}", method = RequestMethod.GET)
	protected RailCitiesResponse getCityList(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("pinCode") String pinCode) throws Exception {
		RailCitiesResponse cityList = RailCitiesResponse.builder().build();
		try {
			irctcCitiesRetrieveManager.init();
			cityList = irctcCitiesRetrieveManager.retrieveCitiesList(pinCode);
		} catch (Exception e) {
			cityList.addError(new ErrorDetail(SystemError.PINCODE_ENQUIRY.getErrorCode(),
					SystemError.PINCODE_ENQUIRY.getMessage(e.getMessage())));
		}
		return cityList;
	}

	@RequestMapping(value = "/postoffice", method = RequestMethod.POST)
	protected RailPostOfficeListResponse getCityList(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RailPostOfficeEnquiryRequest postOfficeEnquiryRequest) throws Exception {
		RailPostOfficeListResponse postOfficeList = RailPostOfficeListResponse.builder().build();
		try {
			postOfficeRetreiveManager.init();
			postOfficeList = postOfficeRetreiveManager.retrievePostOfficeList(postOfficeEnquiryRequest.getPinCode(),
					postOfficeEnquiryRequest.getCity());
		} catch (Exception e) {
			postOfficeList.addError(new ErrorDetail(SystemError.PINCODE_ENQUIRY.getErrorCode(),
					SystemError.PINCODE_ENQUIRY.getMessage(e.getMessage())));
		}
		return postOfficeList;
	}
}
