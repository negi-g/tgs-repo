package com.tgs.services.rail.servicehandler;

import java.time.LocalDate;
import java.time.LocalDateTime;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.RailOrderItemCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.rail.restmodel.RailScheduleEnquiryRequest;
import com.tgs.services.rail.restmodel.RailTrainScheduleResponse;
import com.tgs.services.rail.sources.irctc.IrctcTrainScheduleRetrieveManager;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailScheduleHandler extends ServiceHandler<RailScheduleEnquiryRequest, RailTrainScheduleResponse> {

	@Autowired
	protected IrctcTrainScheduleRetrieveManager retrieveManager;

	@Autowired
	protected RailOrderItemCommunicator orderItemCommunicator;

	@Override
	public void beforeProcess() throws Exception {
		if (StringUtils.isEmpty(request.getBookingId()) && StringUtils.isEmpty(request.getJourneyId()))
			throw new CustomGeneralException(SystemError.INVALID_BOARDING_REQUEST);
	}

	@Override
	public void process() throws Exception {

		String trainNo = "";
		String journeyDate = "";
		String depStn = "";
		String searchKey = "";
		try {
			if (StringUtils.isNotEmpty(request.getBookingId())) {
				RailOrderItem railOrderItem = orderItemCommunicator.getRailOrderItem(request.getBookingId());
				trainNo = railOrderItem.getAdditionalInfo().getRailInfo().getNumber();
				journeyDate = railOrderItem.getDepartureTime().toLocalDate().toString().replaceAll("-", "");
				depStn = railOrderItem.getFromStn();
				searchKey = request.getBookingId();

			} else {
				String[] journeyData = request.getJourneyId().split("_");
				trainNo = journeyData[4];
				journeyDate = LocalDateTime.parse(journeyData[5]).toLocalDate().toString().replaceAll("-", "");
				depStn = journeyData[2];
				searchKey = journeyData[0];

			}
			retrieveManager.init();
			response = retrieveManager.retrieveTrainSchedule(trainNo, journeyDate, depStn, searchKey);
			log.debug("Rail schedule response is {}", GsonUtils.getGson().toJson(response));
		} catch (Exception e) {
			log.error(
					"Error occured while fetching Rail Schedule for trainNo: {}, journeyDate: {}, depStn: {} due to {}",
					trainNo, journeyDate, depStn, e.getMessage(), e);
		}
	}

	@Override
	public void afterProcess() throws Exception {}

}
