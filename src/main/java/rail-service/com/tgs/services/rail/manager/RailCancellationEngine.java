package com.tgs.services.rail.manager;

import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.oms.restmodel.rail.RailCancellationRequest;
import com.tgs.services.oms.restmodel.rail.RailCancellationResponse;
import com.tgs.services.oms.restmodel.rail.RailOtpVerificationRequest;
import com.tgs.services.rail.datamodel.RailTDRReason;
import com.tgs.services.rail.datamodel.RailTdrInfo;
import com.tgs.services.rail.sources.irctc.IrctcCancellationManager;
import com.tgs.services.rail.sources.irctc.IrctcCancellationOtpManager;
import com.tgs.services.rail.sources.irctc.IrctcTdrManager;

@Service
public class RailCancellationEngine {

	@Autowired
	protected IrctcCancellationManager cancellationManager;

	@Autowired
	protected IrctcCancellationOtpManager irctcOtpManager;

	@Autowired
	protected IrctcTdrManager irctcTdrManager;

	public RailCancellationResponse cancelPassengers(RailCancellationRequest request) {
		cancellationManager.setJourneyInfo(request.getJourneyInfo());
		cancellationManager.setPriceInfo(request.getPriceInfo());
		cancellationManager.setBookingId(request.getBookingId());
		cancellationManager.init();
		boolean isCancelSuccess = false;
		if (BooleanUtils.isTrue(request.getIsOfflineCancellation())) {
			isCancelSuccess = cancellationManager.cancelPassengersOffline(request.getReservationId(),
					request.getAgentCanId(), request.getPaxIds());
		} else {
			isCancelSuccess = cancellationManager.cancelPassengers(request.getReservationId(),
					request.getAmendment().getAmendmentId(), request.getPaxIds());
		}
		RailCancellationResponse cancellationResponse = new RailCancellationResponse();
		cancellationResponse.setIsCancelSuccess(isCancelSuccess);
		cancellationResponse.setJourneyInfo(cancellationManager.getJourneyInfo());
		cancellationResponse.setPriceInfo(cancellationManager.getPriceInfo());
		return cancellationResponse;
	}

	public boolean submitOtpRequest(RailOtpVerificationRequest railOtpRequest) {
		irctcOtpManager.init();
		irctcOtpManager.setOtpToken(railOtpRequest.getOtpToken());
		irctcOtpManager.setCancellationId(railOtpRequest.getCancellationId());
		irctcOtpManager.setRequestType(railOtpRequest.getRequestType());
		irctcOtpManager.setPnrNo(railOtpRequest.getPnrNo());
		irctcOtpManager.setBookingId(railOtpRequest.getBookingId());
		return irctcOtpManager.submitOtpRequest();
	}

	public boolean resendOtp(RailOtpVerificationRequest railOtpRequest) {
		irctcOtpManager.init();
		irctcOtpManager.setOtpToken(railOtpRequest.getOtpToken());
		irctcOtpManager.setCancellationId(railOtpRequest.getCancellationId());
		irctcOtpManager.setRequestType(railOtpRequest.getRequestType());
		irctcOtpManager.setPnrNo(railOtpRequest.getPnrNo());
		return irctcOtpManager.resendOtp();
	}

	public List<RailTDRReason> fetchReasons(String transactionId, String bookingId) {
		irctcTdrManager.init();
		irctcTdrManager.setBookingId(bookingId);
		return irctcTdrManager.getAvailableReasons(transactionId);
	}

	public boolean submitTDR(RailTdrInfo tdrInfo, String transactionId, Set<String> paxIds, String bookingId) {
		irctcTdrManager.init();
		irctcTdrManager.setBookingId(bookingId);
		return irctcTdrManager.fileForTdr(tdrInfo, transactionId, paxIds);
	}


}
