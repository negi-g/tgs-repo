package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.api.client.repackaged.com.google.common.base.Objects;
import com.irctc.datamodel.BookingResponseDTO;
import com.irctc.datamodel.GSTDetailsDTO;
import com.irctc.datamodel.GstChargeDTO;
import com.irctc.datamodel.PassengerDetailDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.RailOrderItemCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.rail.datamodel.RailGstInfo;
import com.tgs.services.rail.datamodel.RailSeatAllocation;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.helper.RailStationHelper;
import com.tgs.services.rail.restmodel.RailCurrentStatus;
import com.tgs.services.rail.utils.RailParsingHelper;
import com.tgs.services.rail.utils.RailUtils;
import com.tgs.services.ums.datamodel.RailAgentInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcBookingRetrieveManager extends IrctcServiceManager {

	@Autowired
	protected RailOrderItemCommunicator itemComm;

	@Autowired
	protected OrderServiceCommunicator orderService;

	@Autowired
	protected UserServiceCommunicator userService;

	private static final String BOOKING_DETAILS_SUFFIX = "/tatktservices/bookingdetails";

	public RailCurrentStatus fetchLiveBookingDetails(String bookingId) throws IOException {
		BookingResponseDTO bookingResponse = getBookingResponse(bookingId);
		if (StringUtils.isEmpty(bookingResponse.getBookingErrorMessage())
				&& StringUtils.isEmpty(bookingResponse.getErrorMessage()))
			return processBooking(bookingResponse, bookingId);
		else
			throw new CustomGeneralException(
					Objects.firstNonNull(bookingResponse.getErrorMessage(), bookingResponse.getBookingErrorMessage()));
	}


	public RailCurrentStatus processBooking(BookingResponseDTO bookingResponse, String bookingId) {
		RailOrderItem railOrderItem = itemComm.getRailOrderItem(bookingId);
		journeyInfo = itemComm.getJourneyInfo(bookingId);
		Order order = orderService.findByBookingId(bookingId);
		User user = userService.getUserFromCache(order.getBookingUserId());
		RailGstInfo railGstInfo = null;
		GstInfo gstDetails = null;
		if (StringUtils.isEmpty(bookingResponse.getBookingErrorMessage())) {
			journeyInfo.setBoardingStationInfo(RailStationHelper.getRailStation(bookingResponse.getBoardingStn()));
			journeyInfo
					.setReservationUptoStationInfo(RailStationHelper.getRailStation(bookingResponse.getResvnUptoStn()));
			journeyInfo.getBookingRelatedInfo().setReservationId(bookingResponse.getReservationId());
			journeyInfo.setAvlblForVikalp(bookingResponse.getAvlForVikalp());
			journeyInfo.getRailInfo()
					.setTrainOwner(String.valueOf(Objects.firstNonNull(bookingResponse.getTrainOwner(), 0)));
			journeyInfo.getBookingRelatedInfo().setPnr(bookingResponse.getPnrNumber());
			List<RailTravellerInfo> travellerInfos = journeyInfo.getBookingRelatedInfo().getTravellerInfos();
			travellerInfos.forEach(traveller -> {
				updateTravellerInfo(traveller, bookingResponse);
			});
			GstChargeDTO gstChargeDto = bookingResponse.getGstCharge();
			String gstInfo = GsonUtils.getGson().toJson(gstChargeDto);
			railGstInfo = GsonUtils.getGson().fromJson(gstInfo, RailGstInfo.class);

			if (bookingResponse.getGstDetailsDTO() != null) {
				gstDetails = updateGstDetails(bookingResponse.getGstDetailsDTO());
			}

		}
		RailAgentInfo railAgentInfo = user.getRailAdditionalInfo().getAgentInfo();
		return RailCurrentStatus.builder().journeyInfo(journeyInfo).createdOn(railOrderItem.getCreatedOn())
				.agentName(railAgentInfo.getCompanyName()).corporateName(railAgentInfo.getAgencyName())
				.agentEmail((StringUtils.isNotEmpty(user.getRailAdditionalInfo().getAgentInfo().getEmail()))
						? user.getRailAdditionalInfo().getAgentInfo().getEmail()
						: user.getEmail())
				.agentMobile((StringUtils.isNotEmpty(user.getRailAdditionalInfo().getAgentInfo().getMobileNo()))
						? user.getRailAdditionalInfo().getAgentInfo().getMobileNo()
						: user.getMobile())
				.agentAddress(
						railAgentInfo.getOfficeAddress() != null ? railAgentInfo.getOfficeAddress().getAddress() : null)
				.priceInfo(railOrderItem.getAdditionalInfo().getPriceInfo()).gstInfo(railGstInfo).gstDetails(gstDetails)
				.build();
	}

	private GstInfo updateGstDetails(GSTDetailsDTO gstDetailsDTO) {
		GstInfo gstDetails = new GstInfo();
		gstDetails.setGstNumber(gstDetailsDTO.getGstIn());
		gstDetails.setAddress(gstDetailsDTO.getFlat());
		gstDetails.setRegisteredName(gstDetailsDTO.getNameOnGst());
		gstDetails.setCityName(gstDetailsDTO.getCity());
		gstDetails.setPincode(gstDetailsDTO.getPin());
		gstDetails.setState(gstDetailsDTO.getState());
		return gstDetails;
	}


	private void updateTravellerInfo(RailTravellerInfo travellerInfo, BookingResponseDTO bookingResponse) {
		for (PassengerDetailDTO passengerDto : RailParsingHelper.getValueFromIrctc(bookingResponse.getPsgnDtlList(),
				PassengerDetailDTO.class)) {
			if (travellerInfo.getId() == Long.parseLong(passengerDto.getPassengerSerialNumber())) {
				travellerInfo.setPassengerNetFare((double) passengerDto.getPassengerNetFare());
				travellerInfo.setBerthChoice(passengerDto.getPassengerBerthChoice());
				RailSeatAllocation bookingSeatAllocation =
						RailSeatAllocation.builder().coachId(passengerDto.getBookingCoachId())
								.berthNo(Integer.valueOf(passengerDto.getBookingBerthNo()))
								.berthchoice(passengerDto.getBookingBerthCode())
								.bookingStatus(passengerDto.getBookingStatus()).build();
				RailSeatAllocation currentSeatAllocation =
						RailSeatAllocation.builder().coachId(passengerDto.getCurrentCoachId())
								.berthNo(StringUtils.isEmpty(passengerDto.getCurrentCoachId())
										&& "0".equals(passengerDto.getCurrentBerthNo()) ? null
												: Integer.valueOf(passengerDto.getCurrentBerthNo()))
								.berthchoice(passengerDto.getCurrentBerthCode())
								.bookingStatus(passengerDto.getCurrentStatus()).build();
				travellerInfo.setCurrentSeatAllocation(currentSeatAllocation);
				travellerInfo.setBookingSeatAllocation(bookingSeatAllocation);
				travellerInfo.setIsBedRollOpted(Boolean.parseBoolean(passengerDto.getPassengerBedrollChoice()));
				travellerInfo.setPaxType(RailUtils.getRailPaxType(travellerInfo.getAge(), travellerInfo.getGender()));
			}
		}
	}

	private String getBookingDetailsUrl(String bookingId) {
		String apirUrl = apiURLS == null ? StringUtils.join(sourceConfiguration.getUrl(), BOOKING_DETAILS_SUFFIX)
				: apiURLS.getBookingDetailsUrl();
		apirUrl = StringUtils.join(apirUrl, "/", bookingId);
		log.debug("API Url fetch booking details is {}", apirUrl);
		return apirUrl;
	}


	public BookingResponseDTO getBookingResponse(String bookingId) {
		String resp = StringUtils.EMPTY;
		String bookingDetailUrl = StringUtils.EMPTY;
		BookingResponseDTO bookingResponse = null;
		try {
			bookingDetailUrl = getBookingDetailsUrl(bookingId);
			resp = sendGET(bookingDetailUrl);
			bookingResponse = GsonUtils.getGson().fromJson(resp, BookingResponseDTO.class);
		} catch (IOException e) {
			log.error("Error occured while fetching booking details of bookingId because {} ", bookingId,
					e.getMessage(), e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(resp, "BookingDetailsUrl"));
			listener.addLog(
					LogData.builder().key(bookingId).logData(endPointRQRS).type("Irctc-BookingDetails-Req").build());
		}
		return bookingResponse;

	}
}
