package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.BoardingPointChangeDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.rail.restmodel.ChangeBoardingPointRequest;
import com.tgs.services.rail.restmodel.ChangeBoardingPointResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcChangeBoardingPointManager extends IrctcServiceManager {

	private static final String BOARDING_POINT_CHANGE_SUFFIX = "/tatktservices/changeBoardingPoint";

	public ChangeBoardingPointResponse changeBoardingPoint(String pnr, ChangeBoardingPointRequest changeBoardingRequest)
			throws IOException {
		String resp = StringUtils.EMPTY;
		String boardingChangeUrl = StringUtils.EMPTY;
		String newBoardingStation = StringUtils.EMPTY;
		try {
			newBoardingStation = changeBoardingRequest.getNewBoardingStation();
			boardingChangeUrl = getBoardingPointChangeUrl(pnr, newBoardingStation);
			resp = sendGET(boardingChangeUrl);
			BoardingPointChangeDTO response = GsonUtils.getGson().fromJson(resp, BoardingPointChangeDTO.class);
			if (BooleanUtils.isTrue(response.getSuccess()))
				return parseBoardingPointChangeResponse(response);
			else
				throw new CustomGeneralException(response.getError());
		} catch (IOException e) {
			log.error("Error occured during change point enquiry of pnr {} newBoardingStation {} because {} ", pnr,
					newBoardingStation, e.getMessage());
		} finally {
			String endPointRQRS =
					StringUtils.join(formatUrl(boardingChangeUrl), formatRQRS(resp, "BoardingPointChange"));
			listener.addLog(LogData.builder().key(changeBoardingRequest.getBookingId()).logData(endPointRQRS)
					.type("Irctc-ChangeBoardingPoint-Req").build());
		}
		return ChangeBoardingPointResponse.builder().build();
	}

	private ChangeBoardingPointResponse parseBoardingPointChangeResponse(BoardingPointChangeDTO response) {
		return ChangeBoardingPointResponse.builder().pnr(response.getPnr())
				.newBoardingDate(convertStringToDate(response.getNewBoardingDate(), "yyyy-MM-dd'T'HH:mm:ss"))
				.newBoardingPoint(response.getNewBoardingPoint()).oldBoardingPoint(response.getOldBoardingPoint())
				.build();
	}

	private String getBoardingPointChangeUrl(String pnr, String newBoardingStation) {
		// https://testngetjp.irctc.co.in/eticketing/webservices/taenqservices/changeBoardingPoint/2303530566/CNB
		String apirUrl = apiURLS == null ? StringUtils.join(sourceConfiguration.getUrl(), BOARDING_POINT_CHANGE_SUFFIX)
				: apiURLS.getBoardingPointChangeURL();
		apirUrl = StringUtils.join(apirUrl, "/", pnr, "/", newBoardingStation);
		log.debug("API Url to change boarding station is {}", apirUrl);
		return apirUrl;
	}
}
