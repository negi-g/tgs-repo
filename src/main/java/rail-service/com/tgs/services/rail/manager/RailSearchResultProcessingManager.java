package com.tgs.services.rail.manager;

import java.util.Arrays;
import java.util.Iterator;
import org.springframework.stereotype.Service;
import com.tgs.services.rail.datamodel.RailJourneyClass;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailSearchQuery;
import com.tgs.services.rail.datamodel.RailSearchResult;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RailSearchResultProcessingManager {

	public void processSearchResult(RailSearchResult searchResult, RailSearchQuery railSearchQuery, User user) {
		if (searchResult.getJourneys() != null) {
			for (Iterator<RailJourneyInfo> iterator = searchResult.getJourneys().iterator(); iterator.hasNext();) {
				RailJourneyInfo journeyInfo = iterator.next();
				RailJourneyClass journeyClass = RailJourneyClass.getEnumFromCode(railSearchQuery.getJourneyClass());
				if (journeyClass != null) {
					if (!journeyInfo.getAvlJourneyClasses().contains(journeyClass.getCode())) {
						iterator.remove();
					} else {
						journeyInfo.getAvlJourneyClasses().retainAll(Arrays.asList(journeyClass.getCode()));
					}
				}
			}
		}
	}

}
