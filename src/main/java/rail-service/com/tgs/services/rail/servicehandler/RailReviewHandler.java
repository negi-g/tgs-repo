package com.tgs.services.rail.servicehandler;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.RailCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.rail.datamodel.RailFareEnquiry;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.helper.RailCacheType;
import com.tgs.services.rail.helper.RailStationHelper;
import com.tgs.services.rail.restmodel.RailFareEnquiryResponse;
import com.tgs.services.rail.restmodel.RailReviewBookingRequest;
import com.tgs.services.rail.sources.irctc.IrctcReviewManager;
import com.tgs.services.rail.utils.RailUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailReviewHandler extends ServiceHandler<RailReviewBookingRequest, RailFareEnquiryResponse> {


	@Autowired
	protected IrctcReviewManager reviewManager;

	@Autowired
	protected RailCachingServiceCommunicator cachingService;


	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		String bookingId = null;
		User user = SystemContextHolder.getContextData().getUser();
		if (RailUtils.isUserDisabled(user)) {
			response.addError(new ErrorDetail(SystemError.INACTIVE_RAIL_ACCOUNT.getErrorCode(),
					SystemError.INACTIVE_RAIL_ACCOUNT.getMessage()));
		} else {
			try {
				log.info("Review Request is {}", GsonUtils.getGson().toJson(request));
				RailFareEnquiry fareEnquiry = RailUtils.preBuildFareEnquiryRequest(request.getFareEnquiryRequest());
				log.info("fareEnquiry Request is {}", GsonUtils.getGson().toJson(fareEnquiry));
				request.setFareEnquiry(fareEnquiry);
				reviewManager.init();
				RailJourneyInfo journeyInfo = reviewManager.doReviewJourney(request);
				if (journeyInfo != null) {
					journeyInfo.getBookingRelatedInfo().setTravellerInfos(request.getTravellerInfos());
					setTravellerPaxType(journeyInfo);
					bookingId = journeyInfo.getBookingRelatedInfo().getClientTransactionId();
					journeyInfo.setBoardingStationInfo(StringUtils.isNotEmpty(fareEnquiry.getBoardingstation())
							? RailStationHelper.getRailStation(fareEnquiry.getBoardingstation())
							: null);
					journeyInfo.setBoardingTime(fareEnquiry.getBoardingTime());
				}
				response.setJourneyInfo(journeyInfo);
			} catch (Exception e) {
				log.error("Exception Occured while reviewing journey for bookingid {} due to {}", bookingId,
						e.getMessage(), e);
				response.addError(new ErrorDetail(SystemError.FARE_ENQUIRY_FAILED.getErrorCode(),
						SystemError.FARE_ENQUIRY_FAILED.getMessage(e.getMessage())));
			} finally {
				if (StringUtils.isNotBlank(bookingId)) {
					storeFareEnquiryResponseInCache(bookingId);
				}
			}
		}
	}


	private void setTravellerPaxType(RailJourneyInfo journeyInfo) {
		for (RailTravellerInfo travellerInfo : journeyInfo.getBookingRelatedInfo().getTravellerInfos()) {
			travellerInfo.setPaxType(RailUtils.getRailPaxType(travellerInfo.getAge(), travellerInfo.getGender()));
		}


	}

	private void storeFareEnquiryResponseInCache(String bookingId) {
		cachingService.store(bookingId, BinName.RAILREVIEW.getName(), response,
				CacheMetaInfo.builder().set(CacheSetName.RAIL_REVIEW.name()).compress(true).plainData(false)
						.expiration(RailCacheType.REVIEWKEYEXPIRATION.getTtl()).build());
	}


	@Override
	public void afterProcess() throws Exception {}


}
