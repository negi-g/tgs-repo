package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.StatusDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import lombok.Builder;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Setter
@Slf4j
@Builder
@Service
public class IrctcCancellationOtpManager extends IrctcServiceManager {

	private static final String CANCELLATION_SUFFIX = "/tatktservices/canOtpAuthentication";

	public boolean submitOtpRequest() {
		StatusDTO otpResponse = null;
		String response = StringUtils.EMPTY;
		if (requestType != null) {
			String otpServiceUrl = getOtpServiceUrl();
			try {
				response = sendGET(otpServiceUrl);
				otpResponse = GsonUtils.getGson().fromJson(response, StatusDTO.class);
				int statusCode = Integer.valueOf(otpResponse.getStatus());
				if (statusCode == 0) {
					log.info("Message received from irctc is {}", otpResponse.getMessageInfo());
					return true;
				} else {
					throw new CustomGeneralException(otpResponse.getMessageInfo());
				}
			} catch (IOException e) {
				log.error("Error Occured during OTP sending/ verification due to {}", e.getMessage());
			} finally {
				String endPointRQRS =
						StringUtils.join(formatUrl(otpServiceUrl), formatRQRS(response, "OtpSubmitServiceUrl"));
				listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
						.type("Irctc-Submit-OtpService-Req").build());
			}
		}
		return false;

	}

	private String getOtpServiceUrl() {
		String apiUrl = null;
		if (apiURLS == null) {
			apiUrl = StringUtils.join(sourceConfiguration.getUrl(), CANCELLATION_SUFFIX);
		} else {
			apiUrl = apiURLS.getCancellationUrl();
		}
		apiUrl = StringUtils.join(apiUrl, "/", pnrNo, "/", cancellationId, "/", requestType);
		if (requestType.equals("2")) {
			apiUrl = StringUtils.join(apiUrl, "?otpcode=", otpToken);
		}
		log.debug("API Url for OTP service for rail is  {}", apiUrl);
		return apiUrl;
	}

	public boolean resendOtp() {
		StatusDTO otpResponse = null;
		String otpServiceUrl = StringUtils.EMPTY;
		String response = StringUtils.EMPTY;
		try {
			otpServiceUrl = getOtpServiceUrl();
			response = sendGET(otpServiceUrl);
			otpResponse = GsonUtils.getGson().fromJson(response, StatusDTO.class);
			int statusCode = Integer.valueOf(otpResponse.getStatus());
			if (statusCode == 0) {
				log.info("Message received from irctc is {}", otpResponse.getMessageInfo());
				return true;
			} else {
				throw new CustomGeneralException(otpResponse.getMessageInfo());
			}
		} catch (IOException e) {
			log.error("Error Occured during OTP sending/ verification due to {}", e.getMessage());
		} finally {
			String endPointRQRS =
					StringUtils.join(formatUrl(otpServiceUrl), formatRQRS(response, "ResendOtpServiceUrl"));
			listener.addLog(LogData.builder().key(cancellationId).logData(endPointRQRS)
					.type("Irctc-Resend-OtpService-Req").build());
		}
		return false;
	}

}
