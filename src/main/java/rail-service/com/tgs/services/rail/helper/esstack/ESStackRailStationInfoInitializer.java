package com.tgs.services.rail.helper.esstack;

import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.RailStationInfoFilter;
import com.tgs.services.base.ESStackInitializer;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.rail.dbmodel.DbRailStationInfo;
import com.tgs.services.rail.jparepository.RailStationInfoService;

@Service
public class ESStackRailStationInfoInitializer extends ESStackInitializer {

	@Autowired
	RailStationInfoService stationInfoService;

	@Autowired
	ElasticSearchCommunicator esCommunicator;

	@Override
	public void process() {
		List<DbRailStationInfo> railStations = stationInfoService.findAll(RailStationInfoFilter.builder().build());
		if (CollectionUtils.isNotEmpty(railStations)) {
			this.deleteExistingData();
			esCommunicator.addBulkDocument(railStations, ESMetaInfo.RAIL_STATION);
		}

	}

	public void deleteExistingData() {
		esCommunicator.deleteIndex(ESMetaInfo.RAIL_STATION);
	}

}
