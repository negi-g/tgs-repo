package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.CancellationDetailListDTO;
import com.irctc.datamodel.CancellationResponseDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.utils.RailParsingHelper;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
@Service
public class IrctcCancellationManager extends IrctcServiceManager {

	private static final String CANCELLATION_SUFFIX = "/tatktservices/cancel";

	private static final String REFUND_DETAILS_SUFFIX = "/tatktservices/refunddetails";

	public CancellationResponseDTO getCancellationResponse(String reservationId, String agentCanTxnId,
			Set<String> paxIds) {
		CancellationResponseDTO cancellationResponse = null;
		String res = StringUtils.EMPTY;
		String cancellationUrl = StringUtils.EMPTY;
		try {
			String passengerToken = createPassengerToken(paxIds);
			cancellationUrl = getCancellationUrl(reservationId, agentCanTxnId, passengerToken);
			res = sendGET(cancellationUrl);
			cancellationResponse = GsonUtils.getGson().fromJson(res, CancellationResponseDTO.class);
			if (StringUtils.isEmpty(cancellationResponse.getMessage()))
				parseCancellationResponse(cancellationResponse, paxIds);
			else
				throw new CustomGeneralException(cancellationResponse.getMessage());
		} catch (IOException e) {
			log.error("Error occured during cancellation for reservationId {} because {} ", null, e.getMessage(), e);
		} finally {
			String endPointRQRS =
					StringUtils.join(formatUrl(cancellationUrl), formatRQRS(res, "CancellationDetailUrl"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("Irctc-CancellationDetails-Req")
					.build());
		}
		return cancellationResponse;
	}


	private void parseCancellationResponse(CancellationResponseDTO response, Set<String> paxIds) {
		journeyInfo.getBookingRelatedInfo().setCancellationId(response.getCancellationId());
		priceInfo.getFareComponents().put(RailFareComponent.RCF,
				priceInfo.getFareComponents().getOrDefault(RailFareComponent.RCF, 0.0)
						+ Double.valueOf(response.getCashDeducted()));
		priceInfo.getFareComponents().put(RailFareComponent.BF, priceInfo.getFareComponents().get(RailFareComponent.BF)
				- Double.valueOf(response.getAmountCollected()));
		priceInfo.getFareComponents().put(RailFareComponent.TF, priceInfo.getFareComponents().get(RailFareComponent.TF)
				- Double.valueOf(response.getAmountCollected()));
		priceInfo.getFareComponents().put(RailFareComponent.AAR, Double.valueOf(response.getRefundAmount()));
		priceInfo.getFareComponents().put(RailFareComponent.NF, priceInfo.getFareComponents().get(RailFareComponent.NF)
				- Double.parseDouble(response.getAmountCollected()));
		priceInfo.setTotalFare(priceInfo.getFareComponents().get(RailFareComponent.TF));
		updateTravellerStatus(paxIds, response);
	}


	private void updateTravellerStatus(Set<String> paxIds, CancellationResponseDTO response) {
		List<String> nameList = getTravellerNameList(RailParsingHelper.getValueFromIrctc(response.getName()));
		log.info("Name set received from irctc for cancellationId {} is : {}", response.getCancellationId(), nameList);
		int cancelledPaxCount = 0;
		for (RailTravellerInfo travellerInfo : journeyInfo.getBookingRelatedInfo().getTravellerInfos()) {
			if (isNameMatches(travellerInfo.getFullName(), nameList)) {
				if (travellerInfo.getCurrentSeatAllocation() != null
						&& !("CAN".equalsIgnoreCase(travellerInfo.getCurrentSeatAllocation().getBookingStatus()))) {
					cancelledPaxCount++;
					travellerInfo.getCurrentSeatAllocation().setBookingStatus("CAN");
				}
			}
		}
		if (cancelledPaxCount == 0) {
			throw new CustomGeneralException("Please pass correct Cancellation Id");
		}
		if (StringUtils.isNotEmpty(response.getMessage()) && response.getMessage().contains("Cancellation Initiated")) {
			throw new CustomGeneralException("TDR is not yet processed by IRCTC , Cancellation Initiated");
		}
	}


	private List<String> getTravellerNameList(List<String> irctcNameList) {
		List<String> nameList = new ArrayList<String>();
		for (String travName : irctcNameList) {
			travName = travName.toUpperCase().replaceAll("\\s+", " ").trim();
			nameList.add(travName);
		}
		return nameList;
	}


	private boolean isNameMatches(String travName, List<String> nameList) {
		String modTravName = travName.toUpperCase().replaceAll("\\s+", " ").trim();
		boolean isNamePresentInSet = nameList.contains(modTravName);
		//nameList.remove(modTravName);
		return isNamePresentInSet;
	}


	public boolean cancelPassengers(String reservationId, String agentCanTxnId, Set<String> paxIds) {
		CancellationResponseDTO cancellationResponse = getCancellationResponse(reservationId, agentCanTxnId, paxIds);
		boolean isSuccess = Boolean.parseBoolean(cancellationResponse.getSuccess());
		if (!isSuccess) {
			throw new CustomGeneralException(cancellationResponse.getMessage());
		}
		return isSuccess;
	}


	private String getCancellationUrl(String reservationId, String agentCanTxnId, String passenegrToken) {
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(sourceConfiguration.getUrl(), CANCELLATION_SUFFIX);
		} else {
			apirUrl = apiURLS.getCancellationUrl();
		}
		apirUrl = StringUtils.join(apirUrl, "/", reservationId, "/", agentCanTxnId, "/", passenegrToken);
		log.debug("API Url for cancellation is  {}", apirUrl);
		return apirUrl;
	}


	public boolean cancelPassengersOffline(String reservationId, String agentCanId, Set<String> paxIds) {
		CancellationDetailListDTO cancellationResponse = null;
		String res = StringUtils.EMPTY;
		String refundDetailsUrl = StringUtils.EMPTY;
		try {
			refundDetailsUrl = getRefundDetailsUrl(reservationId, agentCanId);
			res = sendGET(refundDetailsUrl);
			cancellationResponse = GsonUtils.getGson().fromJson(res, CancellationDetailListDTO.class);
			if (StringUtils.isEmpty(cancellationResponse.getMessage())) {
				parseOfflineCancellationResponse(cancellationResponse, paxIds, agentCanId);
				return true;
			} else
				throw new CustomGeneralException(cancellationResponse.getMessage());
		} catch (IOException e) {
			log.error("Error occured during cancellation for reservationId {} because {} ", reservationId,
					e.getMessage(), e);
		} finally {
			String endPointRQRS = StringUtils.join(formatUrl(refundDetailsUrl), formatRQRS(res, "RefundDetailUrl"));
			listener.addLog(
					LogData.builder().key(bookingId).logData(endPointRQRS).type("Irctc-RefundDetails-Req").build());
		}
		return false;
	}


	private void parseOfflineCancellationResponse(CancellationDetailListDTO cancellationResponse, Set<String> paxIds,
			String agentCanId) {
		List<CancellationResponseDTO> responseList =
				RailParsingHelper.getValueFromIrctc(cancellationResponse.getCanList(), CancellationResponseDTO.class);
		CancellationResponseDTO response = getCancellationResponseDto(responseList, agentCanId);
		if (response != null) {
			parseCancellationResponse(response, paxIds);
		} else {
			throw new CustomGeneralException("Please pass correct Cancellation Id");
		}
	}


	private CancellationResponseDTO getCancellationResponseDto(List<CancellationResponseDTO> responseList,
			String agentCanId) {
		for (CancellationResponseDTO cancellationRes : responseList) {
			if (cancellationRes.getCancellationId().equals(agentCanId)) {
				return cancellationRes;
			}
		}
		return null;
	}


	private String getRefundDetailsUrl(String reservationId, String agentCanId) {
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(sourceConfiguration.getUrl(), REFUND_DETAILS_SUFFIX);
		} else {
			apirUrl = apiURLS.getCancellationUrl();
		}
		apirUrl = StringUtils.join(apirUrl, "/", reservationId, "?agentCanId", agentCanId);
		log.debug("API Url for refund Details after cancellation is  {}", apirUrl);
		return apirUrl;
	}
}
