package com.tgs.services.rail.servicehandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.RailOrderItemCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.rail.datamodel.message.VikalpMailAttributes;
import com.tgs.services.rail.restmodel.OptVikapRequest;
import com.tgs.services.rail.sources.irctc.IrctcOptVikalpManager;
import com.tgs.services.ums.datamodel.User;

@Service
public class RailVikalpHandler extends ServiceHandler<OptVikapRequest, BaseResponse> {

	@Autowired
	protected IrctcOptVikalpManager optManager;

	@Autowired
	protected RailOrderItemCommunicator orderItemCommunicator;

	@Autowired
	private MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	protected OrderServiceCommunicator orderService;

	@Autowired
	private UserServiceCommunicator userService;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		try {
			RailOrderItem railOrderItem = orderItemCommunicator.getRailOrderItem(request.getBookingId());
			String transactionId = railOrderItem.getAdditionalInfo().getReservationId();
			optManager.init();
			boolean result = optManager.optforVikalp(transactionId, request.getToken(), request.getSpecialTrainFlag());
			orderItemCommunicator.updateOrderItem(request.getBookingId(), result ? request.getTrainNumbers() : "");
			if (result)
				sendMail(railOrderItem, request.getTrainNumbers());
		} catch (Exception e) {
			response.addError(new ErrorDetail(SystemError.VIKALP_OPT.getErrorCode(),
					SystemError.VIKALP_OPT.getMessage(e.getMessage())));
		}

	}

	private void sendMail(RailOrderItem railOrderItem, String optedTrainNumbers) {
		AbstractMessageSupplier<VikalpMailAttributes> msgAttributes =
				new AbstractMessageSupplier<VikalpMailAttributes>() {
					@Override
					public VikalpMailAttributes get() {
						Order order = orderService.findByBookingId(railOrderItem.getBookingId());
						User user = userService.getUserFromCache(order.getBookingUserId());
						VikalpMailAttributes mailAttributes = VikalpMailAttributes.builder().build();
						mailAttributes.setBookingId(railOrderItem.getBookingId());
						mailAttributes.setPnr(railOrderItem.getAdditionalInfo().getPnr());
						mailAttributes.setToEmailId(user.getEmail());
						mailAttributes.setRole(user.getRole());
						mailAttributes.setPartnerId(order.getPartnerId());
						mailAttributes.setOldDeparture(
								DateFormatterHelper.formatDateTime(railOrderItem.getAdditionalInfo().getBoardingTime(),
										DateFormatType.BOOKING_EMAIL_FORMAT));
						mailAttributes.setTrainName(railOrderItem.getAdditionalInfo().getRailInfo().getName());
						mailAttributes.setTrainNumber(railOrderItem.getAdditionalInfo().getRailInfo().getNumber());
						mailAttributes.setOptedTrains(optedTrainNumbers);
						mailAttributes.setKey(EmailTemplateKey.VIKALP_OPT_EMAIL.name());
						return mailAttributes;
					}
				};
		msgSrvCommunicator.sendMail(msgAttributes.getAttributes());
	}


	@Override
	public void afterProcess() throws Exception {}

}
