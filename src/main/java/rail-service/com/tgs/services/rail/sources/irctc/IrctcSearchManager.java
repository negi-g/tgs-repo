
package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.AvailabilityDTO;
import com.irctc.datamodel.AvlFareResponseDTO;
import com.irctc.datamodel.EnquiryRequestDTO;
import com.irctc.datamodel.RailInformationMessage;
import com.irctc.datamodel.TrainBtwnStnsDTO;
import com.irctc.datamodel.TrainBtwnStnsRespDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.rail.datamodel.RailAvailabilityDetail;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailFareEnquiry;
import com.tgs.services.rail.datamodel.RailInfo;
import com.tgs.services.rail.datamodel.RailJourneyAvailability;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import com.tgs.services.rail.datamodel.RailRunningDays;
import com.tgs.services.rail.datamodel.RailSearchQuery;
import com.tgs.services.rail.datamodel.RailSearchResult;
import com.tgs.services.rail.datamodel.RailType;
import com.tgs.services.rail.utils.RailParsingHelper;
import com.tgs.services.rail.utils.RailUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcSearchManager extends IrctcServiceManager {

	private static final String SERACH_ENQUIRY_SUFFIX = "/taenqservices/tatwnstns";


	public RailSearchResult doSearch(RailSearchQuery searchQuery) {
		RailSearchResult searchResult = new RailSearchResult();
		String resp = StringUtils.EMPTY;
		String searchUrl = StringUtils.EMPTY;

		try {
			searchUrl = getSearchUrl(searchQuery);
			resp = sendGET(searchUrl);
			TrainBtwnStnsRespDTO response = GsonUtils.getGson().fromJson(resp, TrainBtwnStnsRespDTO.class);
			if (StringUtils.isBlank(response.getErrorMessage())) {
				searchResult = parseSearchReponse(response, searchQuery);
			} else {
				throw new CustomGeneralException(response.getErrorMessage());
			}
		} catch (IOException e) {
			log.error("Error Occured during train search , for searchId {} , due to {}", searchQuery.getSearchId(), e);
		} finally {
			String endPointRQRS = org.apache.commons.lang3.StringUtils.join(formatUrl(searchUrl),
					formatRQRS(resp, "TranBtwStations"));
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(endPointRQRS)
					.type("Irctc-SearchService-Req").build());
		}
		return searchResult;

	}

	private RailSearchResult parseSearchReponse(TrainBtwnStnsRespDTO searchResponse, RailSearchQuery searchQuery) {
		RailSearchResult searchResult = new RailSearchResult();
		List<RailJourneyInfo> journeys = getJourneys(searchResponse, searchQuery);
		List<String> avlQuotaList = RailParsingHelper.getValueFromIrctc(searchResponse.getQuotaList());
		searchResult.setJourneys(journeys);
		if (CollectionUtils.isNotEmpty(avlQuotaList)) {
			Collections.sort(avlQuotaList);
			avlQuotaList.remove("PT");
			avlQuotaList.remove("HP");
		}
		searchResult.setAvlQuotaList(avlQuotaList);
		return searchResult;
	}

	public List<RailJourneyInfo> getJourneys(TrainBtwnStnsRespDTO searchResponse, RailSearchQuery searchQuery) {
		List<RailJourneyInfo> railJourneys = new ArrayList<RailJourneyInfo>();
		for (TrainBtwnStnsDTO trainBtwnStn : RailParsingHelper.getValueFromIrctc(searchResponse.getTrainBtwnStnsList(),
				TrainBtwnStnsDTO.class)) {
			RailJourneyInfo journeyInfo = new RailJourneyInfo();
			journeyInfo.setSNo(trainBtwnStn.getSNo());
			LocalTime departureTime = LocalTime.parse(trainBtwnStn.getDepartureTime());
			long duration = calculateDuration(trainBtwnStn.getDuration());
			journeyInfo.setDeparture(
					searchQuery != null ? LocalDateTime.of(searchQuery.getTravelDate(), departureTime) : null);
			journeyInfo.setArrival(journeyInfo.getDeparture().plusMinutes(duration));
			Period period =
					Period.between(journeyInfo.getDeparture().toLocalDate(), journeyInfo.getArrival().toLocalDate());
			journeyInfo.setDays(period.getDays());
			journeyInfo.setDuration(String.valueOf(duration));
			journeyInfo.setDistance(trainBtwnStn.getDistance());
			// journeyInfo.setDuration(duration);
			journeyInfo.setRunningDays(populateRunningDays(trainBtwnStn));
			journeyInfo.setIsRunAllDays(journeyInfo.getIsRunOnAllDays());
			journeyInfo.setAvlJourneyClasses(RailParsingHelper.getValueFromIrctc(trainBtwnStn.getAvlClasses()));
			journeyInfo.setDepartStationInfo(getStationInfo(trainBtwnStn.getFromStnCode()));
			journeyInfo.setArrivalStaionInfo(getStationInfo(trainBtwnStn.getToStnCode()));
			journeyInfo.setRailInfo(getRailInfo(trainBtwnStn));
			railJourneys.add(journeyInfo);
		}
		return railJourneys;
	}

	private RailRunningDays populateRunningDays(TrainBtwnStnsDTO trainBtwnStn) {
		RailRunningDays runningDays =
				RailRunningDays.builder().isRunningSun(StringUtils.equalsIgnoreCase("Y", trainBtwnStn.getRunningSun()))
						.isRunningMon(StringUtils.equalsIgnoreCase("Y", trainBtwnStn.getRunningMon()))
						.isRunningTue(StringUtils.equalsIgnoreCase("Y", trainBtwnStn.getRunningTue()))
						.isRunningWed(StringUtils.equalsIgnoreCase("Y", trainBtwnStn.getRunningWed()))
						.isRunningThu(StringUtils.equalsIgnoreCase("Y", trainBtwnStn.getRunningThu()))
						.isRunningFri(StringUtils.equalsIgnoreCase("Y", trainBtwnStn.getRunningFri()))
						.isRunningSat(StringUtils.equalsIgnoreCase("Y", trainBtwnStn.getRunningSat())).build();
		return runningDays;
	}

	private long calculateDuration(String duration) {
		long totalMinutes = 0;
		try {
			String[] date = duration.split(":");
			long hours = Integer.valueOf(date[0]);
			long minutes = Integer.valueOf(date[1]);
			totalMinutes = (hours * 60 + minutes);
		} catch (Exception e) {
			log.error("Error occured during parsing of duration {}", e.getMessage());
		}
		return totalMinutes;
	}


	private RailInfo getRailInfo(TrainBtwnStnsDTO trainBtwnStn) {
		RailInfo railInfo = RailInfo.builder().build();
		railInfo.setName(trainBtwnStn.getTrainName());
		railInfo.setNumber(trainBtwnStn.getTrainNumber());
		railInfo.setTypes(getRailTypes(RailParsingHelper.getValueFromIrctc(trainBtwnStn.getTrainType())));
		railInfo.setTrainOwner(trainBtwnStn.getTrainOwner());
		return railInfo;
	}


	private List<RailType> getRailTypes(List<String> railTypesFromIrctc) {
		List<RailType> railTypes = new ArrayList<RailType>();
		for (String railType : railTypesFromIrctc) {
			railTypes.add(RailType.getEnumFromCode(railType));
		}
		return railTypes;
	}

	public RailJourneyInfo doSearchFareAvailability(RailFareEnquiry fareEnquiry) {
		String enquiryUrl = StringUtils.EMPTY;
		String resp = StringUtils.EMPTY;
		try {
			RailJourneyInfo journeyInfo = null;
			EnquiryRequestDTO enquiryRequest = getEnquiryRequest(fareEnquiry);
			enquiryUrl = getFareAvailabilityUrl(fareEnquiry);
			postData = GsonUtils.getGson().toJson(enquiryRequest);
			resp = sendPOST(enquiryUrl);
			AvlFareResponseDTO fareResponse = GsonUtils.getGson().fromJson(resp, AvlFareResponseDTO.class);
			if (StringUtils.isBlank(fareResponse.getErrorMessage())) {
				journeyInfo = parseJourney(fareResponse, fareEnquiry);
			} else {
				throw new CustomGeneralException(fareResponse.getErrorMessage());
			}
			return journeyInfo;
		} catch (IOException e) {
			log.error("Error Occured while Searching fare {}", e);
		} finally {
			String endPointRQRS = org.apache.commons.lang3.StringUtils.join(formatUrl(enquiryUrl),
					formatRQRS(postData, "FareAvailabilityRQ"), formatRQRS(resp, "FareAvailabilityRS"));
			listener.addLog(LogData.builder().key(fareEnquiry.getSearchId()).logData(endPointRQRS)
					.type("Irctc-FareAvailability-Req").build());
		}

		return null;
	}


	private EnquiryRequestDTO getEnquiryRequest(RailFareEnquiry fareEnquiry) {
		EnquiryRequestDTO enquiryRequest = EnquiryRequestDTO.builder().build();
		enquiryRequest.setMasterId(sourceConfiguration.getUserName());
		enquiryRequest.setReservationChoice((short) 99);
		enquiryRequest.setEnquiryType("3");
		enquiryRequest.setMoreThanOneDay(true);
		return enquiryRequest;
	}

	public String getSearchUrl(RailSearchQuery searchQuery) {
		StringBuilder searchUrl = null;
		if (apiURLS == null) {
			searchUrl = new StringBuilder(sourceConfiguration.getUrl()).append(SERACH_ENQUIRY_SUFFIX);
		} else {
			searchUrl = new StringBuilder(apiURLS.getPnrEnquiryURl());
		}
		String srcStn = searchQuery.getFromCityOrStation().getCode();
		String dstStn = searchQuery.getToCityOrStation().getCode();
		String travelDate = searchQuery.getTravelDate().toString().replaceAll("-", "");

		searchUrl.append("/").append(srcStn).append("/").append(dstStn).append("/").append(travelDate);
		log.info("Search listing enquiry URL {}", searchUrl.toString());
		return searchUrl.toString();
	}

	public RailJourneyInfo parseJourney(AvlFareResponseDTO fareResponse, RailFareEnquiry fareEnquiry) {
		RailJourneyInfo journeyInfo = buildJourney(fareResponse, fareEnquiry);
		RailPriceInfo priceInfo = populatePriceInfo(fareResponse, journeyInfo, fareEnquiry.getSearchId());

		RailJourneyAvailability journeyAvailability = new RailJourneyAvailability();
		List<RailAvailabilityDetail> railAvailabilityList = new ArrayList<RailAvailabilityDetail>();
		for (AvailabilityDTO avlDay : RailParsingHelper.getValueFromIrctc(fareResponse.getAvlDayList(),
				AvailabilityDTO.class)) {
			RailAvailabilityDetail railAvailability = new RailAvailabilityDetail();
			railAvailability.setAvailabilityStatus(avlDay.getAvailablityStatus());
			railAvailability
					.setJourneyDate(convertStringToDate(avlDay.getAvailablityDate(), "dd-MM-yyyy").toLocalDate());
			railAvailability.setPriceInfo(priceInfo);
			railAvailabilityList.add(railAvailability);
		}
		journeyAvailability.setRailAvailabilityDetails(railAvailabilityList);

		Map<String, RailJourneyAvailability> avlDetailsMap = new HashMap<String, RailJourneyAvailability>();
		avlDetailsMap.put(journeyInfo.getAvlJourneyClasses().get(0), journeyAvailability);
		journeyInfo.setAvlJourneyDetailsMap(avlDetailsMap);
		journeyInfo.setInfoMessages(getInfoMessagesFromIrctc(RailParsingHelper
				.getValueFromIrctc(fareResponse.getInformationMessage(), RailInformationMessage.class)));
		journeyInfo.getInfoMessages().add(getTravelInsuranceMsg(fareResponse.getBkgCfg()));
		return journeyInfo;
	}


	private RailJourneyInfo buildJourney(AvlFareResponseDTO fareResponse, RailFareEnquiry fareEnquiry) {

		RailJourneyInfo journeyInfo = new RailJourneyInfo();
		journeyInfo.setRailInfo(buildRailInfo(fareResponse));
		journeyInfo.setArrivalStaionInfo(getStationInfo(fareResponse.getTo()));
		journeyInfo.setDepartStationInfo(getStationInfo(fareResponse.getFrom()));
		journeyInfo.setDistance(Integer.parseInt(fareResponse.getDistance()));
		journeyInfo.setBookingQuota(fareEnquiry.getBookingQuota());
		journeyInfo.setDuration(fareEnquiry.getDuration());
		LocalDateTime departureDate = fareEnquiry.getDeparture();
		LocalTime departureTime = departureDate.toLocalTime();
		journeyInfo.setDeparture(fareEnquiry.getTravelDate().atTime(departureTime));
		journeyInfo.setArrival(journeyInfo.getDeparture().plusMinutes(Long.parseLong(fareEnquiry.getDuration())));
		journeyInfo.setAvlJourneyClasses(RailParsingHelper.getValueFromIrctc(fareResponse.getEnqClass()));
		journeyInfo.setBookingConfiguration(
				getBookingConfiguration(fareResponse.getBkgCfg(), journeyInfo.getAvlJourneyClasses()));
		return journeyInfo;
	}

	private RailInfo buildRailInfo(AvlFareResponseDTO fareResponse) {
		RailInfo railInfo = RailInfo.builder().build();
		railInfo.setName(fareResponse.getTrainName());
		railInfo.setNumber(fareResponse.getTrainNo());
		return railInfo;
	}

	private RailPriceInfo populatePriceInfo(AvlFareResponseDTO fareResponse, RailJourneyInfo journeyInfo,
			String searchId) {
		RailPriceInfo priceInfo = new RailPriceInfo();
		priceInfo.setTotalFare(fareResponse.getTotalCollectibleAmount());
		priceInfo.getFareComponents().put(RailFareComponent.RF, Double.parseDouble(fareResponse.getBaseFare()));
		priceInfo.getFareComponents().put(RailFareComponent.CC, Double.parseDouble(fareResponse.getCateringCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.FC, Double.parseDouble(fareResponse.getFuelAmount()));
		priceInfo.getFareComponents().put(RailFareComponent.WP, Double.parseDouble(fareResponse.getWpServiceCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.WPT, Double.parseDouble(fareResponse.getWpServiceTax()));
		priceInfo.getFareComponents().put(RailFareComponent.OC, Double.parseDouble(fareResponse.getOtherCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.RC,
				Double.parseDouble(fareResponse.getReservationCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.ST, Double.parseDouble(fareResponse.getServiceTax()));
		priceInfo.getFareComponents().put(RailFareComponent.SC, Double.parseDouble(fareResponse.getSuperfastCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.TTF, Double.parseDouble(fareResponse.getTatkalFare()));
		priceInfo.getFareComponents().put(RailFareComponent.TF, fareResponse.getTotalCollectibleAmount());
		priceInfo.getFareComponents().put(RailFareComponent.NF, fareResponse.getTotalCollectibleAmount());
		priceInfo.getFareComponents().put(RailFareComponent.TI,
				Double.parseDouble(fareResponse.getTravelInsuranceCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.TIT,
				Double.parseDouble(fareResponse.getTravelInsuranceServiceTax()));
		// This is the ticket fare shown on UI, all commercial calculations will be done on this fare
		Double baseFare = fareResponse.getTotalCollectibleAmount()
				- priceInfo.getFareComponents().getOrDefault(RailFareComponent.WP, 0.0)
				- priceInfo.getFareComponents().getOrDefault(RailFareComponent.WPT, 0.0);
		priceInfo.getFareComponents().put(RailFareComponent.BF, baseFare);
		User user = SystemContextHolder.getContextData().getUser();
		RailUtils.updateAgentCharges(priceInfo, journeyInfo, user, searchId);
		RailUtils.updatePGCharges(priceInfo, journeyInfo, user, searchId);
		return priceInfo;
	}
}
