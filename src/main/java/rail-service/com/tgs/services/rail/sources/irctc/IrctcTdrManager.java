package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.HistoryEnquiryDTO;
import com.irctc.datamodel.StatusDTO;
import com.irctc.datamodel.TdrReason;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.rail.datamodel.RailTDRReason;
import com.tgs.services.rail.datamodel.RailTdrInfo;
import com.tgs.services.rail.utils.RailParsingHelper;
import com.tgs.services.ums.datamodel.User;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class IrctcTdrManager extends IrctcServiceManager {

	private static final String HISTORY_SEARCH_SUFFIX = "/tabkhservices/historySearchByTxnId";

	private static final String TDR_FILING_SUFFIX = "/tatktservices/fileTDR";


	public List<RailTDRReason> getAvailableReasons(String transactionId) {

		List<RailTDRReason> availableReasonsList = null;
		HistoryEnquiryDTO historyEnquiryResponse = null;
		String res = StringUtils.EMPTY;
		String historySearchUrl = StringUtils.EMPTY;
		try {
			historySearchUrl = getHistorySearchUrl(transactionId);
			res = sendGET(historySearchUrl);
			historyEnquiryResponse = GsonUtils.getGson().fromJson(res, HistoryEnquiryDTO.class);
			if (StringUtils.isEmpty(historyEnquiryResponse.getErrorMessage())) {
				availableReasonsList = new ArrayList<RailTDRReason>();

				for (TdrReason tdrReason : RailParsingHelper
						.getValueFromIrctc(historyEnquiryResponse.getTdrReasonList(), TdrReason.class)) {
					RailTDRReason availableReason = new RailTDRReason();
					availableReason.setReasonIndex(tdrReason.getReasonIndex());
					availableReason.setTdrReason(tdrReason.getTdrReason());
					availableReasonsList.add(availableReason);
				}
			} else {
				throw new CustomGeneralException(historyEnquiryResponse.getErrorMessage());
			}
		} catch (IOException e) {
			log.error("Error occured during cancellation for transactionId {} because {} ", transactionId,
					e.getMessage(), e);
		} finally {
			String endPointRQRS =
					StringUtils.join(formatUrl(historySearchUrl), formatRQRS(res, "HistorySearchByTxnIdRS"));
			listener.addLog(
					LogData.builder().key(bookingId).logData(endPointRQRS).type("Irctc-HistorySearch-Req").build());
		}
		return availableReasonsList;
	}

	private String getHistorySearchUrl(String transactionId) {
		String apirUrl = null;
		String userId = null;
		User user = SystemContextHolder.getContextData().getUser();
		if(user != null && user.getRailAdditionalInfo() !=null) {
			 userId = user.getRailAdditionalInfo().getIrctcAgentId();
		}
	
		if (apiURLS == null) {
			apirUrl = StringUtils.join(sourceConfiguration.getUrl(), HISTORY_SEARCH_SUFFIX);
		} else {
			apirUrl = apiURLS.getHistorySearchUrl();
		}
		apirUrl = StringUtils.join(apirUrl, "/", userId, "/", transactionId, "?currentStatus=T");
		log.debug("API Url for History Search is  {}", apirUrl);
		return apirUrl;
	}

	public boolean fileForTdr(RailTdrInfo tdrInfo, String transactionId, Set<String> paxIds) {
		boolean isSuccess = false;
		StatusDTO tdrResponse = null;
		String res = StringUtils.EMPTY;
		String tdrFilingUrl = StringUtils.EMPTY;
		try {
			String passengerToken = createPassengerToken(paxIds);
			tdrFilingUrl = getTdrFilingUrl(transactionId, tdrInfo, passengerToken);
			res = sendGET(tdrFilingUrl);
			tdrResponse = GsonUtils.getGson().fromJson(res, StatusDTO.class);
			if (BooleanUtils.isTrue(Boolean.parseBoolean(tdrResponse.getUpdateFlag()))) {
				isSuccess = true;
			}
		} catch (IOException e) {
			log.error("Error occured during Filing for TDR for transactionId {} because {} ", transactionId,
					e.getMessage(), e);
		} finally {
			String endPointRQRS = StringUtils.join(formatUrl(tdrFilingUrl), formatRQRS(res, "FileTdrRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("Irctc-FileTdr-Req").build());
		}
		return isSuccess;
	}

	private String getTdrFilingUrl(String transactionId, RailTdrInfo tdrInfo, String passengerToken) {
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(sourceConfiguration.getUrl(), TDR_FILING_SUFFIX);
		} else {
			apirUrl = apiURLS.getTdrFilingUrl();
		}
		if (tdrInfo.getEftFlag().equals("1")) {
			apirUrl = StringUtils.join(apirUrl, "/", transactionId, "/", passengerToken, "/", tdrInfo.getRsnIndex(), "",
					"?eftFlag=", tdrInfo.getEftFlag(), "&eftAmount=", tdrInfo.getEftAmount(), "&eftNumber=",
					tdrInfo.getEftNumber(), "&eftDate=", tdrInfo.getEftDate());
		} else {
			apirUrl = StringUtils.join(apirUrl, "/", transactionId, "/", passengerToken, "/", tdrInfo.getRsnIndex());

		}
		log.debug("API Url for Filing TDR is  {}", apirUrl);
		return apirUrl;
	}

}
