package com.tgs.services.rail.sources.irctc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import com.irctc.datamodel.BookingConfig;
import com.irctc.datamodel.RailInformationMessage;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.rail.datamodel.RailBookingConfiguration;
import com.tgs.services.rail.datamodel.RailFareEnquiry;
import com.tgs.services.rail.datamodel.RailInfoMessage;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.datamodel.config.RailSourceConfigurationOutput;
import com.tgs.services.rail.datamodel.ruleengine.RailAPIURLRuleCriteria;
import com.tgs.services.rail.helper.RailStationHelper;
import com.tgs.services.rail.utils.RailParsingHelper;
import com.tgs.services.rail.utils.RailUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class IrctcServiceManager {

	protected String cancellationId;

	protected String otpToken;

	protected String requestType;

	protected String pnrNo;

	protected String postData;

	protected String bookingId;

	protected RailSourceConfigurationOutput sourceConfiguration;

	protected RailAPIURLRuleCriteria apiURLS;

	protected RailJourneyInfo journeyInfo;

	protected RailPriceInfo priceInfo;

	private static final String FARE_AVAILABILITY_SUFFIX = "/taenqservices/avlFareenquiry";

	protected RestAPIListener listener;

	public void init() {
		sourceConfiguration = RailUtils.getRailSourceConfigOutput();
		apiURLS = RailUtils.getRailEndPointURL(sourceConfiguration);
		listener = new RestAPIListener("");
	}

	protected Map<String, String> getHeaderParams() {
		HashMap<String, String> headermap = new HashMap<String, String>();
		String encoding = fetchEncodedCredentials();
		headermap.put("Authorization", "Basic " + encoding);
		return headermap;
	}

	private String fetchEncodedCredentials() {
		String encoding = "";
		try {
			String username = sourceConfiguration.getUserName();
			String password = sourceConfiguration.getPassword();
			String key = username.concat(":").concat(password);
			encoding = Base64.getEncoder().encodeToString(key.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.error("Unsupported Encoding UTF-8");
		}
		return encoding;
	}

	public String sendGET(String url) throws IOException {
		CloseableHttpClient httpClient = getCloseableHttpClient();
		HttpGet httpGet = new HttpGet(url);
		httpGet.addHeader("User-Agent", "User-Agent");
		addHeaderForGet(httpGet);
		/**
		 * Uncomment for local , as we have to pass proxy
		 */
		HttpHost proxy = RailUtils.getProxy(sourceConfiguration);
		if (proxy != null) {
			RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
			httpGet.setConfig(config);
		}
		CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = reader.readLine()) != null) {
			response.append(inputLine);
		}
		reader.close();
		log.debug("Response String is " + response.toString());
		httpClient.close();
		return response.toString();
	}


	public String sendPOST(String url) throws IOException {
		log.info("Request URL : {}", url);

		CloseableHttpClient httpClient = getCloseableHttpClient();
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("User-Agent", "User-Agent");
		addHeaderForPost(httpPost);
		HttpHost proxy = RailUtils.getProxy(sourceConfiguration);
		if (proxy != null) {
			RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
			httpPost.setConfig(config);
		}
		log.info("Request generated is : {}", postData);
		StringEntity entity = new StringEntity(postData);
		httpPost.setEntity(entity);
		CloseableHttpResponse httpResponse = httpClient.execute(httpPost);

		log.debug("POST Response Status:: " + httpResponse.getStatusLine().getStatusCode());

		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = reader.readLine()) != null) {
			response.append(inputLine);
		}
		reader.close();

		// log result
		log.debug("Response String is " + response.toString());
		httpClient.close();
		return response.toString();

	}

	private void addHeaderForGet(HttpGet httpGet) {
		String encoding = fetchEncodedCredentials();
		httpGet.addHeader("Authorization", "Basic " + encoding);
	}

	private void addHeaderForPost(HttpPost httpPost) {
		String encoding = fetchEncodedCredentials();
		httpPost.addHeader("Authorization", "Basic " + encoding);
		httpPost.addHeader("Content-Type", "application/json");
	}

	public static CloseableHttpClient getCloseableHttpClient() {
		CloseableHttpClient httpClient = null;
		try {
			httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
					.setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
						public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
							return true;
						}
					}).build()).build();
		} catch (KeyManagementException e) {
			log.error("KeyManagementException in creating http client instance", e);
		} catch (NoSuchAlgorithmException e) {
			log.error("NoSuchAlgorithmException in creating http client instance", e);
		} catch (KeyStoreException e) {
			log.error("KeyStoreException in creating http client instance", e);
		}
		return httpClient;
	}

	public static LocalDateTime convertStringToDate(String date, String format) {
		LocalDateTime journeyDate = null;
		try {
			journeyDate = TgsDateUtils.convertStringToDate(date, format);
		} catch (ParseException e) {
			log.error("Unable to parse date {} ", date);
		}
		return journeyDate;
	}

	public String getFareAvailabilityUrl(RailFareEnquiry fareEnquiry) {
		StringBuilder enquiryUrl = null;
		if (apiURLS == null) {
			enquiryUrl = new StringBuilder(sourceConfiguration.getUrl()).append(FARE_AVAILABILITY_SUFFIX);
		} else {
			enquiryUrl = new StringBuilder(apiURLS.getFareAvailabilityURL());
		}
		String srcStn = fareEnquiry.getFromStn();
		String dstStn = fareEnquiry.getToStn();
		String travelDate = fareEnquiry.getTravelDate().toString().replaceAll("-", "");
		String trainNo = fareEnquiry.getTrainNo();
		String enqFlag = BooleanUtils.isTrue(fareEnquiry.getPaymentEnqFlag()) ? "Y" : "N";
		String bookingQuota = fareEnquiry.getBookingQuota();
		if (enqFlag.equalsIgnoreCase("Y") && bookingQuota.equalsIgnoreCase("SS")) {
			bookingQuota = "GN";
		}
		enquiryUrl.append("/" + trainNo + "/" + travelDate + "/" + srcStn + "/" + dstStn + "/"
				+ fareEnquiry.getJourneyClass() + "/" + bookingQuota + "/" + enqFlag);
		return enquiryUrl.toString();
	}


	public RailBookingConfiguration getBookingConfiguration(BookingConfig bkgCfg, List<String> avlClasses) {
		RailBookingConfiguration config = new RailBookingConfiguration();
		config.setBedRollFlagEnabled(bkgCfg.getBedRollFlagEnabled());
		config.setChildBerthMandatory(bkgCfg.getChildBerthMandatory());
		config.setApplicableBerthTypes(RailParsingHelper.getValueFromIrctc(bkgCfg.getApplicableBerthTypes()));
		config.setFoodChoiceEnabled(bkgCfg.getFoodChoiceEnabled());
		config.setAvailableFoodDetails(RailParsingHelper.getValueFromIrctc(bkgCfg.getFoodDetails()));
		config.setForgoConcession(bkgCfg.getForgoConcession());
		config.setGstDetailInputFlag(bkgCfg.getGstDetailInputFlag());
		if (avlClasses.contains("SL") || avlClasses.contains("2S")) {
			config.setGstDetailInputFlag(false);
		}
		config.setIdRequired(bkgCfg.getIdRequired());
		config.setUidMandatoryFlag(bkgCfg.getUidMandatoryFlag());
		config.setTravelInsuranceEnabled(bkgCfg.getTravelInsuranceEnabled());
		config.setSeniorCitizenApplicable(bkgCfg.getSeniorCitizenApplicable());
		config.setLowerBerthApplicable(bkgCfg.getLowerBerthApplicable());
		config.setApplicableIds(RailParsingHelper.getValueFromIrctc(bkgCfg.getValidIdCardTypes()));
		config.setPaxIcardMaxLength(Integer.valueOf(bkgCfg.getMaxIdCardLength()));
		config.setPaxIcardMinLength(Integer.valueOf(bkgCfg.getMinIdCardLength()));
		config.setPassportMaxLength(Integer.valueOf(bkgCfg.getMaxPassportLength()));
		config.setPassportMinLength(Integer.valueOf(bkgCfg.getMinPassportLength()));
		return config;
	}

	public List<RailInfoMessage> getInfoMessagesFromIrctc(List<RailInformationMessage> infoMsgsFromIrctc) {

		List<RailInfoMessage> msgList = new ArrayList<RailInfoMessage>();
		for (RailInformationMessage infoMsg : infoMsgsFromIrctc) {
			RailInfoMessage msgObj = new RailInfoMessage();
			msgObj.setMessage(infoMsg.getMessage());
			msgObj.setParamName(infoMsg.getParamName());
			msgObj.setPopup(Boolean.parseBoolean(infoMsg.getPopup()));
			msgList.add(msgObj);
		}
		return msgList;
	}

	public RailStationInfo getStationInfo(String stationCode) {
		RailStationInfo stationInfo = RailStationHelper.getRailStation(stationCode);
		return stationInfo;
	}

	public String createPassengerToken(Set<String> paxIds) {
		String s = "NNNNNN";
		char token[] = s.toCharArray();
		for (String paxId : paxIds) {
			int currId = Integer.valueOf(paxId) - 1;
			if (currId >= 0 && currId < 6)
				token[currId] = 'Y';
		}
		return String.valueOf(token);
	}

	public String formatRQRS(String message, String type) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(message)) {
			buffer.append(type + " : " + LocalDateTime.now().toString()).append("\n\n").append(message).append("\n\n");
		}
		return buffer.toString();
	}

	public String formatUrl(String url) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(url)) {
			buffer.append(StringUtils.substringAfter(url, "/eticketing/webservices"));
		}
		return buffer.toString();
	}

	public RailInfoMessage getTravelInsuranceMsg(BookingConfig bookingConfig) {
		RailInfoMessage insuranceMsg = new RailInfoMessage();
		insuranceMsg.setParamName("TravelInsurance");
		insuranceMsg.setMessage(bookingConfig.getTravelInsuranceFareMsg());
		return insuranceMsg;
	}

}
