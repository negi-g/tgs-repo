package com.tgs.services.rail.restcontroller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.TgsValidator;
import com.tgs.services.rail.restmodel.RailReviewBookingRequest;

@Component
public class RailReviewRequestValidator extends TgsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (!(target instanceof RailReviewBookingRequest)) {
			return;
		}
		RailReviewBookingRequest reviewRequest = (RailReviewBookingRequest) target;

		if (reviewRequest.getGstInfo() != null && StringUtils.isNotBlank(reviewRequest.getGstInfo().getGstNumber())) {
			registerErrors(errors, "gstInfo", reviewRequest.getGstInfo());
		}


	}

}
