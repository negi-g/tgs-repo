package com.tgs.services.rail.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.tgs.services.rail.dbmodel.DbRailConfiguratorRule;

@Repository
public interface RailConfiguratorRepository
		extends JpaRepository<DbRailConfiguratorRule, Long>, JpaSpecificationExecutor<DbRailConfiguratorRule> {

}
