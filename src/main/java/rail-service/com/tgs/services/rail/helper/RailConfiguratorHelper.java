package com.tgs.services.rail.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.filters.RailConfiguratorInfoFilter;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.rail.datamodel.config.RailConfiguratorInfo;
import com.tgs.services.rail.datamodel.config.RailConfiguratorRuleType;
import com.tgs.services.rail.datamodel.ruleengine.RaillBasicRuleField;
import com.tgs.services.rail.dbmodel.DbRailConfiguratorRule;
import com.tgs.services.rail.jparepository.RailConfiguratorService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@InitializerGroup(group = InitializerGroup.Group.RAIL)
public class RailConfiguratorHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap railConfigRules;

	private static Map<RailConfiguratorRuleType, List<RailConfiguratorInfo>> inMemoryRailConfigRules;

	private static final String FIELD = "rail_config";

	static Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(RaillBasicRuleField.class);

	@Autowired
	RailConfiguratorService railConfiguratorService;


	public RailConfiguratorHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap railConfigRules) {
		super(configurationHashMap);
		RailConfiguratorHelper.railConfigRules = railConfigRules;
	}

	@Override
	public void process() {
		processInMemory();

		if (MapUtils.isEmpty(inMemoryRailConfigRules)) {
			return;
		}

		inMemoryRailConfigRules.forEach((ruleType, rules) -> {
			railConfigRules.put(ruleType.getName(), FIELD, rules,
					CacheMetaInfo.builder().set(CacheSetName.RAIL_CONFIGURATOR.getName())
							.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
		});
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	private void processInMemory() {
		log.info("Processing InMemory RailConfigRules.");
		List<DbRailConfiguratorRule> configRules =
				railConfiguratorService.findAll(RailConfiguratorInfoFilter.builder().deleted(false).build());
		Map<RailConfiguratorRuleType, List<RailConfiguratorInfo>> ruleMap = null;
		if (CollectionUtils.isNotEmpty(configRules)) {
			List<RailConfiguratorInfo> configuratorInfos = DbRailConfiguratorRule.toDomainList(configRules);
			ruleMap = configuratorInfos.stream().collect(Collectors.groupingBy(RailConfiguratorInfo::getRuleType));
		}
		inMemoryRailConfigRules = Optional
				.<Map<RailConfiguratorRuleType, List<RailConfiguratorInfo>>>ofNullable(ruleMap).orElseGet(HashMap::new);
	}

	@Override
	public void deleteExistingInitializer() {
		log.info("Deleting InMemoryRailConfigRules.");
		if (inMemoryRailConfigRules != null) {
			log.info("Clearing InMemoryRailConfigRules.");
			inMemoryRailConfigRules.clear();
		}
		railConfigRules.truncate(CacheSetName.RAIL_CONFIGURATOR.getName());
	}

	public static List<RailConfiguratorInfo> getRailConfiguratorRules(RailConfiguratorRuleType ruleType) {
		List<RailConfiguratorInfo> rules = new ArrayList<>();
		List<RailConfiguratorInfo> configuratorInfos = inMemoryRailConfigRules.get(ruleType);
		if (CollectionUtils.isNotEmpty(configuratorInfos)) {
			inMemoryRailConfigRules.get(ruleType).forEach(rule -> {
				rules.add(rule.toBuilder().build());
			});
		}
		return rules;
	}

	public static RailConfiguratorInfo getRuleInfo(IFact railFact, RailConfiguratorRuleType ruleType) {
		List<RailConfiguratorInfo> applicableRules = null;
		List<RailConfiguratorInfo> railconfigrules = getRailConfiguratorRules(ruleType);
		if (CollectionUtils.isEmpty(railconfigrules)) {
			return null;
		}
		applicableRules = getApplicableRules(railFact, railconfigrules);
		if (CollectionUtils.isEmpty(applicableRules)) {
			return null;
		}
		return applicableRules.get(0);
	}

	@SuppressWarnings("unchecked")
	public static <T> T getRuleOutPut(IFact railFact, List<RailConfiguratorInfo> railconfigrules) {
		List<RailConfiguratorInfo> configuratorInfos = getApplicableRules(railFact, railconfigrules);
		if (CollectionUtils.isNotEmpty(configuratorInfos)) {
			return (T) configuratorInfos.get(0).getOutput();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static List<RailConfiguratorInfo> getApplicableRules(IFact railFact,
			List<RailConfiguratorInfo> railconfigrules) {
		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(railconfigrules, railFact, fieldResolverMap);
		List<RailConfiguratorInfo> rules = (List<RailConfiguratorInfo>) ruleEngine.fireAllRules();
		return rules;
	}
}
