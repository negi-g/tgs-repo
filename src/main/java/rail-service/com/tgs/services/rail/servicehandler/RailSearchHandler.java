package com.tgs.services.rail.servicehandler;

import java.util.Arrays;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.SearchQueryValidator;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailSearchQuery;
import com.tgs.services.rail.datamodel.RailSearchResult;
import com.tgs.services.rail.manager.RailSearchResultProcessingManager;
import com.tgs.services.rail.restmodel.RailSearchRequest;
import com.tgs.services.rail.restmodel.RailSearchResponse;
import com.tgs.services.rail.sources.irctc.IrctcSearchManager;
import com.tgs.services.rail.utils.RailUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.air.NoSearchResultException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailSearchHandler extends ServiceHandler<RailSearchRequest, RailSearchResponse> {

	@Autowired
	protected RailSearchResultProcessingManager searchResultProcessingManager;

	public static final String DELIM = "_";

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		User user = SystemContextHolder.getContextData().getUser();
		if (RailUtils.isUserDisabled(user)) {
			response.addError(new ErrorDetail(SystemError.INACTIVE_RAIL_ACCOUNT.getErrorCode(),
					SystemError.INACTIVE_RAIL_ACCOUNT.getMessage()));
		} else {
			RailSearchResult searchResult =
					getAvailableRailSchedule(SystemContextHolder.getContextData(), request.getSearchQuery());
			response.setSearchId(request.getSearchQuery().getSearchId());
			if (searchResult != null) {
				searchResultProcessingManager.processSearchResult(searchResult, request.getSearchQuery(),
						SystemContextHolder.getContextData().getUser());
				generateJourneyKeys(searchResult, response.getSearchId());
			}
			response.setSearchResult(searchResult);
		}

	}

	@Override
	public void afterProcess() throws Exception {

	}

	private RailSearchResult getAvailableRailSchedule(ContextData contextData, RailSearchQuery searchQuery) {
		RailSearchResult searchResult = null;
		try {
			IrctcSearchManager searchManager =
					(IrctcSearchManager) SpringContext.getApplicationContext().getBean("irctcSearchManager");
			LogUtils.log(LogTypes.RAIL_SUPPLIER_SEARCH_START, LogMetaInfo.builder().searchId(searchQuery.getSearchId())
					.timeInMs(System.currentTimeMillis()).build(), null);
			log.debug("Searching available train schedules for supplier IRCTC searchQuery {}", searchQuery);
			searchManager.init();
			searchResult = searchManager.doSearch(searchQuery);
			LogUtils.log(LogTypes.RAIL_SUPPLIER_SEARCH_END, LogMetaInfo.builder().searchId(searchQuery.getSearchId())
					.timeInMs(System.currentTimeMillis()).build(), LogTypes.RAIL_SUPPLIER_SEARCH_START);
		} catch (NoSearchResultException nos) {
			log.debug("No serach results found on IRCTC for searchQuery {}, due to {}",
					GsonUtils.getGson().toJson(searchQuery), nos.getMessage(), nos);
			LogUtils.log(searchQuery.getSearchId(), "RailSearch", nos);
		} catch (Exception e) {
			response.addError(new ErrorDetail(SystemError.SEARCH_FAILED.getErrorCode(),
					SystemError.SEARCH_FAILED.getMessage(e.getMessage())));
		} finally {
			LogUtils.clearLogList();
		}
		return searchResult;
	}


	private void generateJourneyKeys(RailSearchResult searchResult, String searchId) {
		int journeyIndex = 0;
		for (RailJourneyInfo journeyInfo : searchResult.getJourneys()) {
			String uniqueJourneyId = generateUniqueJourneyId(journeyInfo, searchId, journeyIndex++);
			journeyInfo.setId(uniqueJourneyId);
		}
	}

	private String generateUniqueJourneyId(RailJourneyInfo journeyInfo, String searchId, int journeyIndex) {
		return StringUtils.join(Arrays.asList(searchId, journeyIndex, getJourneyKey(journeyInfo)), DELIM);
	}

	public String getJourneyKey(RailJourneyInfo journeyInfo) {
		return StringUtils.join(Arrays.asList(journeyInfo.getDepartStationInfo().getCode().trim(),
				journeyInfo.getArrivalStaionInfo().getCode().trim(), journeyInfo.getRailInfo().getNumber(),
				journeyInfo.getDeparture().toString(), journeyInfo.getDuration()), DELIM);
	}

}
