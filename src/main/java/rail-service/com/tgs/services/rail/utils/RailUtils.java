package com.tgs.services.rail.utils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.http.HttpHost;
import com.tgs.services.base.utils.BaseRailUtils;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailFareEnquiry;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPaxType;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import com.tgs.services.rail.datamodel.RailSearchQuery;
import com.tgs.services.rail.datamodel.config.RailClientFeeOutput;
import com.tgs.services.rail.datamodel.config.RailConfiguratorInfo;
import com.tgs.services.rail.datamodel.config.RailConfiguratorRuleType;
import com.tgs.services.rail.datamodel.config.RailPGChargesOutput;
import com.tgs.services.rail.datamodel.config.RailSourceConfigurationOutput;
import com.tgs.services.rail.datamodel.ruleengine.RailAPIURLRuleCriteria;
import com.tgs.services.rail.datamodel.ruleengine.RailBasicFact;
import com.tgs.services.rail.helper.RailConfiguratorHelper;
import com.tgs.services.rail.helper.RailStationHelper;
import com.tgs.services.rail.restmodel.RailFareEnquiryRequest;
import com.tgs.services.rail.restmodel.RailReviewBookingRequest;
import com.tgs.services.ums.datamodel.RailAdditionalInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RailUtils {

	public static RailAPIURLRuleCriteria getRailEndPointURL(RailSourceConfigurationOutput configurationOutput) {
		if (configurationOutput != null && CollectionUtils.isNotEmpty(configurationOutput.getRailUrls())) {
			List<String> urlList = configurationOutput.getRailUrls();
			RailAPIURLRuleCriteria railUrls = new RailAPIURLRuleCriteria();
			railUrls.setPnrEnquiryURl(getURL(urlList, 0));
			return railUrls;
		}
		return null;
	}

	public static String getURL(List<String> urlList, int index) {
		try {
			return urlList.get(index);
		} catch (IndexOutOfBoundsException e) {
			log.debug("URL is Empty for Index {}", index);
			return "";
		}
	}

	public static RailSourceConfigurationOutput getRailSourceConfigOutput() {
		RailBasicFact railFact = RailBasicFact.createFact();
		List<RailConfiguratorInfo> railconfigrules =
				RailConfiguratorHelper.getRailConfiguratorRules(RailConfiguratorRuleType.SOURCECONFIG);
		if (!CollectionUtils.isEmpty(railconfigrules)) {
			return RailConfiguratorHelper.getRuleOutPut(railFact, railconfigrules);
		}
		return null;
	}

	public static void populateMissingParamtersInRailSearchQuery(RailSearchQuery searchQuery) {
		searchQuery
				.setFromCityOrStation(RailStationHelper.getRailStation(searchQuery.getFromCityOrStation().getCode()));
		searchQuery.setToCityOrStation(RailStationHelper.getRailStation(searchQuery.getToCityOrStation().getCode()));

	}

	public static HttpHost getProxy(RailSourceConfigurationOutput sourceConfiguration) {
		HttpHost httpHost = null;
		String proxyAddress = sourceConfiguration.getProxyAddress();
		if (StringUtils.isNotBlank(proxyAddress)) {
			httpHost = new HttpHost(proxyAddress.split(":")[0], Integer.valueOf(proxyAddress.split(":")[1]), "http");
		}
		return httpHost;
	}


	public static RailBasicFact getRailBasicFactOnJounreyInfo(RailJourneyInfo journeyInfo, User user) {
		RailBasicFact railBasicFact = RailBasicFact.builder().build().generateFact(journeyInfo);
		BaseUtils.createRailFactOnUser(railBasicFact, user);
		return railBasicFact;
	}


	public static void updateAgentCharges(RailPriceInfo priceInfo, RailJourneyInfo journeyInfo, User user,
			String refId) {
		log.info("Updating Agent Charges for referenceId  {}", refId);
		RailBasicFact railBasicFact = getRailBasicFactOnJounreyInfo(journeyInfo, user);
		RailConfiguratorInfo configInfo =
				RailConfiguratorHelper.getRuleInfo(railBasicFact, RailConfiguratorRuleType.CLIENTFEE);
		if (Objects.nonNull(configInfo)) {
			RailClientFeeOutput clientFeeOutput = (RailClientFeeOutput) configInfo.getOutput();
			String mfExpression;
			if (Objects.nonNull(clientFeeOutput)
					&& StringUtils.isNotEmpty(mfExpression = clientFeeOutput.getManagementFee())) {
				log.info("Applying agent charges with expression {} , for refId {} , and ruleId {}", mfExpression,
						refId, configInfo.getId());
				priceInfo.getRailCommercialInfo().setClientFeeId(configInfo.getId());
				Double mfFee = BaseRailUtils.evaluateRailExpression(mfExpression, priceInfo);
				if (clientFeeOutput.getThresholdAmount() != null && mfFee > clientFeeOutput.getThresholdAmount()) {
					mfFee = clientFeeOutput.getThresholdAmount();
				}
				priceInfo.getFareComponents().put(RailFareComponent.MF, mfFee);
				priceInfo.getFareComponents().put(RailFareComponent.TF,
						priceInfo.getFareComponents().getOrDefault(RailFareComponent.TF, 0.0) + mfFee);
			}
			if (Objects.nonNull(clientFeeOutput)
					&& StringUtils.isNotEmpty(mfExpression = clientFeeOutput.getManagementFeeToDisplay())) {
				priceInfo.getRailCommercialInfo().setClientFeeId(configInfo.getId());
				Double mfFee = BaseRailUtils.evaluateRailExpression(mfExpression, priceInfo);
				priceInfo.getFareComponents().put(RailFareComponent.DMF, mfFee);
			}
		} else {
			log.info("There is no Applicable Rule for ruleType CLIENTFEE for refId {} ", refId);
		}
	}

	public static void updatePGCharges(RailPriceInfo priceInfo, RailJourneyInfo journeyInfo, User user, String refId) {
		log.info("Updating PGCharges for referenceId  {}", refId);
		RailBasicFact railBasicFact = getRailBasicFactOnJounreyInfo(journeyInfo, user);
		railBasicFact.setTripFareComponents(priceInfo.getFareComponents());
		RailConfiguratorInfo configInfo =
				RailConfiguratorHelper.getRuleInfo(railBasicFact, RailConfiguratorRuleType.PGCHARGES);
		if (Objects.nonNull(configInfo)) {
			RailPGChargesOutput pgChargesOutput = (RailPGChargesOutput) configInfo.getOutput();
			String pgExpression;
			if (Objects.nonNull(pgChargesOutput)
					&& StringUtils.isNotEmpty(pgExpression = pgChargesOutput.getPgCharges())) {
				log.info("Applying PGCharges with expression {} , for refId {}, and ruleId {}", pgExpression, refId,
						configInfo.getId());
				priceInfo.getRailCommercialInfo().setPgId(configInfo.getId());
				Double pgCharges = BaseRailUtils.evaluateRailExpression(pgExpression, priceInfo);
				if (pgChargesOutput.getThresholdAmount() != null && pgCharges > pgChargesOutput.getThresholdAmount()) {
					pgCharges = pgChargesOutput.getThresholdAmount();
				}
				priceInfo.getFareComponents().put(RailFareComponent.PC, pgCharges);
				priceInfo.getFareComponents().put(RailFareComponent.TF,
						priceInfo.getFareComponents().getOrDefault(RailFareComponent.TF, 0.0) + pgCharges);

			}
			if (Objects.nonNull(pgChargesOutput)
					&& StringUtils.isNotEmpty(pgExpression = pgChargesOutput.getPgChargesToDisplay())) {
				priceInfo.getRailCommercialInfo().setPgId(configInfo.getId());
				Double pgCharges = BaseRailUtils.evaluateRailExpression(pgExpression, priceInfo);
				priceInfo.getFareComponents().put(RailFareComponent.DPC, pgCharges);
			}
		} else {
			log.info("There is no Applicable Rule for ruleType PGCHARGES for refId {} ", refId);
		}
	}

	public static RailFareEnquiry preBuildFareEnquiryRequest(RailFareEnquiryRequest request) {
		String[] journeyData = request.getJourneyId().split("_");
		request.setSearchId(journeyData[0]);
		RailFareEnquiry fareEnquiry = new RailFareEnquiry();
		fareEnquiry.setFromStn(journeyData[2]);
		fareEnquiry.setToStn(journeyData[3]);
		fareEnquiry.setTrainNo(journeyData[4]);
		fareEnquiry.setDeparture(LocalDateTime.parse(journeyData[5]));
		fareEnquiry.setDuration(journeyData[6]);
		fareEnquiry.setJourneyClass(request.getJourneyClass());
		fareEnquiry.setTravelDate(request.getTravelDate());
		fareEnquiry.setBoardingstation(request.getNewBoardingPoint());
		fareEnquiry.setBoardingTime(request.getNewBoardingTime());
		fareEnquiry.setPaymentEnqFlag(true);
		String bookingQuota = request.getQuota();
		if (StringUtils.isBlank(request.getQuota())) {
			bookingQuota = "GN";
		}
		fareEnquiry.setBookingQuota(bookingQuota);
		return fareEnquiry;
	}

	public static RailSearchQuery fetchSearchQueryFromRequest(RailFareEnquiry fareEnquiry, String searchId) {
		RailSearchQuery searchQuery = new RailSearchQuery();
		searchQuery.setFromCityOrStation(RailStationHelper.getRailStation(fareEnquiry.getFromStn()));
		searchQuery.setToCityOrStation(RailStationHelper.getRailStation(fareEnquiry.getToStn()));
		searchQuery.setSearchId(searchId);
		searchQuery.setTravelDate(fareEnquiry.getTravelDate());
		return searchQuery;
	}

	public static void populateMissingParamtersInReviewRequest(RailReviewBookingRequest reviewRequest) {
		reviewRequest.setWsLoginUserId(null);
		reviewRequest.setMasterId(null);
	}


	public static boolean isUserDisabled(User user) {
		RailAdditionalInfo railAdditionalInfo = user.getRailAdditionalInfo();
		if (railAdditionalInfo != null && BooleanUtils.isFalse(railAdditionalInfo.getEnabled())) {
			return true;
		}
		return false;
	}

	public static RailPaxType getRailPaxType(Integer age, String gender) {
		if (Objects.isNull(age) || age < 5)
			return RailPaxType.INFANT;
		else if (age >= 5 && age <= 11)
			return RailPaxType.CHILD;
		else if (age >= 58 && "F".equals(gender))
			return RailPaxType.SENIOR;
		else if (age >= 60)
			return RailPaxType.SENIOR;
		else
			return RailPaxType.ADULT;
	}

}
