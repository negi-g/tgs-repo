package com.tgs.services.rail.servicehandler;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.rail.datamodel.RailMessageAttributes;
import com.tgs.services.rail.restmodel.RailMessageRequest;

@Service
public class RailMessageHandler extends ServiceHandler<RailMessageRequest, BaseResponse> {

	@Autowired
	protected MsgServiceCommunicator msgSrvCommunicator;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		processEmailContent(request.getJourneyIds(), EmailTemplateKey.RAIL_SEARCH_EMAIL);

	}

	@Override
	public void afterProcess() throws Exception {}

	public void processEmailContent(List<String> journeyIds, EmailTemplateKey templateKey) {
		if (CollectionUtils.isEmpty(journeyIds)) {
			return;
		}
		RailMessageAttributes messageAttributes = getMessageAttributes(templateKey, request.getToIds());


		sendEmail(messageAttributes);
	}

	public void sendEmail(RailMessageAttributes messageAttributes) {
		msgSrvCommunicator.sendMail(messageAttributes);
	}

	public RailMessageAttributes getMessageAttributes(EmailTemplateKey templateKey, String toEmail) {
		AbstractMessageSupplier<RailMessageAttributes> messageAttributes =
				new AbstractMessageSupplier<RailMessageAttributes>() {
					@Override
					public RailMessageAttributes get() {
						RailMessageAttributes messageAttributes = RailMessageAttributes.builder().build();
						messageAttributes.setToEmailId(toEmail);
						messageAttributes.setKey(templateKey.name());
						return messageAttributes;
					}
				};
		return messageAttributes.get();
	}

}
