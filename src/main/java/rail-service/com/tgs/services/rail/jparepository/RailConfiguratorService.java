package com.tgs.services.rail.jparepository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.filters.RailConfiguratorInfoFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.rail.dbmodel.DbRailConfiguratorRule;
import com.tgs.services.rail.helper.RailConfiguratorHelper;

@Service
public class RailConfiguratorService extends SearchService<DbRailConfiguratorRule> {

	@Autowired
	RailConfiguratorRepository railConfiguratorRepository;

	@Autowired
	RailConfiguratorHelper railConfiguratorHelper;

	public List<DbRailConfiguratorRule> findAll(RailConfiguratorInfoFilter queryFilter) {
		return super.search(queryFilter, railConfiguratorRepository);
	}

	public DbRailConfiguratorRule findById(Long id) {
		return railConfiguratorRepository.findOne(id);
	}

	public DbRailConfiguratorRule save(DbRailConfiguratorRule raillConfigRule) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			if (!ObjectUtils.isEmpty(raillConfigRule.getInclusionCriteria()))
				raillConfigRule.getInclusionCriteria().cleanData();
			if (!ObjectUtils.isEmpty(raillConfigRule.getExclusionCriteria()))
				raillConfigRule.getExclusionCriteria().cleanData();
			raillConfigRule.setProcessedOn(LocalDateTime.now());
			raillConfigRule = railConfiguratorRepository.saveAndFlush(raillConfigRule);
		}
		railConfiguratorHelper.process();
		return raillConfigRule;
	}
}
