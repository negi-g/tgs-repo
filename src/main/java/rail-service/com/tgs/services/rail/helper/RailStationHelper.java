package com.tgs.services.rail.helper;

import java.util.HashMap;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.RailStationInfoFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.jparepository.RailStationInfoService;

@Service
@InitializerGroup(group = InitializerGroup.Group.RAIL)
public class RailStationHelper extends InMemoryInitializer {

	private static HashMap<String, RailStationInfo> railStationHashMap;

	@Autowired
	RailStationInfoService railStationService;

	@Autowired
	public RailStationHelper(CustomInMemoryHashMap configurationHashMap) {
		super(configurationHashMap);
	}

	@Override
	@PostConstruct
	public void process() {
		railStationHashMap = new HashMap<String, RailStationInfo>();
		railStationService.findAll(RailStationInfoFilter.builder().enabled(true).build()).forEach(station -> {
			railStationHashMap.put(station.getCode(), new GsonMapper<>(station, RailStationInfo.class).convert());
		});

	}

	public static RailStationInfo getRailStation(String code) {
		RailStationInfo stationInfo = railStationHashMap.get(code);
		if (stationInfo == null) {
			throw new RuntimeException("There is no station mapping found in our database for code " + code);
		}
		return stationInfo;
	}

	@Override
	public void deleteExistingInitializer() {
		railStationHashMap = new HashMap<>();
	}


}
