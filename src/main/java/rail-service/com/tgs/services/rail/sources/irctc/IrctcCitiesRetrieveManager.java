package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.AddressDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.rail.restmodel.RailCitiesResponse;
import com.tgs.services.rail.utils.RailParsingHelper;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcCitiesRetrieveManager extends IrctcSearchManager {

	private static final String PIN_SUFFIX = "/userregistrationservice/pin";

	public RailCitiesResponse retrieveCitiesList(String pinCode) throws IOException {
		RailCitiesResponse cityListResponse = RailCitiesResponse.builder().build();
		try {
			String resp = sendGET(getFetchCityUrl(pinCode));
			AddressDTO response = GsonUtils.getGson().fromJson(resp, AddressDTO.class);
			if (StringUtils.isEmpty(response.getError())) {
				cityListResponse.setCityList(RailParsingHelper.getValueWithoutReplacingSpclChars(response.getCityList()));
				cityListResponse.setState(response.getState());
			} else
				throw new CustomGeneralException(response.getError());
		} catch (IOException e) {
			log.error("Error occured during pin code enquiry for pincode {} because {} ", pinCode, e.getMessage());
		}
		return cityListResponse;
	}

	private String getFetchCityUrl(String pinCode) {
		// https://testngetjp.irctc.co.in/eticketing/webservices/userregistrationservice/pin/110034
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(sourceConfiguration.getUrl(), PIN_SUFFIX);
		} else {
			apirUrl = apiURLS.getPinEnquiryUrl();
		}
		apirUrl = apirUrl.concat("/").concat(pinCode);
		log.debug("IRCTC fetch city enquiry URL {}", apirUrl);
		return apirUrl;
	}

}
