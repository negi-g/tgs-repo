package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.UserAvailabilityDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.rail.restmodel.RailUserAvailabilityRequest;
import com.tgs.services.rail.restmodel.RailUserAvailabilityResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcUserAvlManager extends IrctcServiceManager {

	private static final String USER_AVL_URL = "/authenticationservice/checkUserAvailability";


	public RailUserAvailabilityResponse checkUserAvailability(RailUserAvailabilityRequest request) {
		RailUserAvailabilityResponse userAvlResponse = RailUserAvailabilityResponse.builder().build();
		String res = StringUtils.EMPTY;
		String userAvlSearchUrl = StringUtils.EMPTY;
		try {
			userAvlSearchUrl = getUserAvlSearchUrl(request);
			res = sendGET(userAvlSearchUrl);
			UserAvailabilityDTO userAvlDto = GsonUtils.getGson().fromJson(res, UserAvailabilityDTO.class);
			if (StringUtils.isEmpty(userAvlDto.getErrorMessage())) {
				if (StringUtils.isNotEmpty(request.getEmail())) {
					userAvlResponse.setEmailAvailable(Boolean.parseBoolean(userAvlDto.getEmailAvailable()));
				}
				if (StringUtils.isNotEmpty(request.getMobileNo())) {
					userAvlResponse.setMobileAvailable(Boolean.parseBoolean(userAvlDto.getMobileAvailable()));
				}
				if (StringUtils.isNotEmpty(request.getAgentPan())) {
					userAvlResponse.setAgentPanAvailable(Boolean.parseBoolean(userAvlDto.getUserPanAvailable()));
				}
			} else {
				throw new CustomGeneralException(userAvlDto.getErrorMessage());
			}
		} catch (IOException e) {
			log.error("Error occured during User Avl check due to {} ", e);
		}
		return userAvlResponse;
	}

	private String getUserAvlSearchUrl(RailUserAvailabilityRequest request) {
		String apiUrl = StringUtils.join(sourceConfiguration.getUrl(), USER_AVL_URL);
		apiUrl = StringUtils.join(apiUrl, "?");
		if (StringUtils.isNotEmpty(request.getEmail())) {
			apiUrl = StringUtils.join(apiUrl, "email=", request.getEmail(), "&");
		}
		if (StringUtils.isNotEmpty(request.getMobileNo())) {
			apiUrl = StringUtils.join(apiUrl, "mobile=", request.getMobileNo(), "&");
		}
		if (StringUtils.isNotEmpty(request.getAgentPan())) {
			apiUrl = StringUtils.join(apiUrl, "userpan=", request.getAgentPan(), "&");
		}
		StringUtils.removeEnd(apiUrl, "&");
		log.debug("API Url for Check User Avl Search is  {}", apiUrl);
		return apiUrl;
	}


}
