package com.tgs.services.rail.dbmodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.RestExclude;
import com.tgs.services.rail.datamodel.RailStationInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "railstationinfo")
public class DbRailStationInfo extends BaseModel<DbRailStationInfo, RailStationInfo> {


	@Column(unique = true)
	private String code;

	@Column
	private String name;

	@RestExclude
	@Column
	private Boolean enabled;

	@Override
	public DbRailStationInfo from(RailStationInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbRailStationInfo.class).convert();
	}

	@Override
	public RailStationInfo toDomain() {
		return new GsonMapper<>(this, RailStationInfo.class).convert();
	}

}
