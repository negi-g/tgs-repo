package com.tgs.services.rail.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.communicator.RailOrderItemCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.rail.restmodel.OptVikapRequest;
import com.tgs.services.rail.restmodel.VikalpTrainListResponse;
import com.tgs.services.rail.servicehandler.RailVikalpHandler;
import com.tgs.services.rail.sources.irctc.IrctcVikalpTrainListManager;

@RestController
@RequestMapping("/rail/v1/vikalp")
public class RailVikalpController {

	@Autowired
	protected IrctcVikalpTrainListManager vikalpTrainManager;

	@Autowired
	protected RailOrderItemCommunicator orderItemCommunicator;

	@Autowired
	protected RailVikalpHandler vikalpHandler;

	@RequestMapping(value = "/trainlist/{bookingId}", method = RequestMethod.GET)
	protected VikalpTrainListResponse getVikalpTrainList(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("bookingId") String bookingId) throws Exception {
		VikalpTrainListResponse vikalpResponse = VikalpTrainListResponse.builder().build();
		try {
			RailOrderItem railOrderItem = orderItemCommunicator.getRailOrderItem(bookingId);
			vikalpTrainManager.init();
			vikalpResponse =
					vikalpTrainManager.retrieveVikalpTrainList(railOrderItem.getAdditionalInfo().getReservationId());
		} catch (Exception e) {
			vikalpResponse.addError(new ErrorDetail(SystemError.VIKALP_TRAIN_ENQUIRY.getErrorCode(),
					SystemError.VIKALP_TRAIN_ENQUIRY.getMessage(e.getMessage())));
		}
		return vikalpResponse;
	}

	@RequestMapping(value = "/opt", method = RequestMethod.POST)
	protected BaseResponse optForVikalp(HttpServletRequest request, HttpServletResponse response,
			@RequestBody OptVikapRequest optVikalpRequest) throws Exception {
		vikalpHandler.initData(optVikalpRequest, new BaseResponse());
		return vikalpHandler.getResponse();
	}
}
