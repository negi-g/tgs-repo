package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.AddressDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.rail.restmodel.RailPostOfficeListResponse;
import com.tgs.services.rail.utils.RailParsingHelper;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcPostOfficeRetrieveManager extends IrctcSearchManager {

	private static final String PIN_SUFFIX = "/userregistrationservice/pin";

	public RailPostOfficeListResponse retrievePostOfficeList(String pinCode, String city) throws IOException {
		RailPostOfficeListResponse postOfficeListResponse = RailPostOfficeListResponse.builder().build();
		try {
			String resp = sendGET(getFetchPostOfficeUrl(pinCode, city));
			AddressDTO response = GsonUtils.getGson().fromJson(resp, AddressDTO.class);
			if (StringUtils.isEmpty(response.getError())) {
				postOfficeListResponse.setCity(response.getCity());
				postOfficeListResponse.setState(response.getState());
				postOfficeListResponse
						.setPostOffices(RailParsingHelper.getValueWithoutReplacingSpclChars(response.getPostofficeList()));
			} else
				throw new CustomGeneralException(response.getError());
		} catch (IOException e) {
			log.error("Error occured during pin code enquiry for pincode {} because {} ", pinCode, e.getMessage());
		}
		return postOfficeListResponse;
	}

	private String getFetchPostOfficeUrl(String pinCode, String city) {
		// https://testngetjp.irctc.co.in/eticketing/webservices/userregistrationservice/pin/110034?&city=North West
		// Delhi
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(sourceConfiguration.getUrl(), PIN_SUFFIX);
		} else {
			apirUrl = apiURLS.getPinEnquiryUrl();
		}
		apirUrl = apirUrl.concat("/").concat(pinCode).concat("?&city=").concat(city.replaceAll("\\s", "%20"));
		log.debug("IRCTC fetch city enquiry URL {}", apirUrl);
		return apirUrl;
	}
}
