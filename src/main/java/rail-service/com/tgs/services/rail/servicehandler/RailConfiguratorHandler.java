package com.tgs.services.rail.servicehandler;

import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.RailConfiguratorInfoFilter;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.rail.datamodel.config.RailConfiguratorInfo;
import com.tgs.services.rail.datamodel.ruleengine.RailBasicRuleCriteria;
import com.tgs.services.rail.dbmodel.DbRailConfiguratorRule;
import com.tgs.services.rail.jparepository.RailConfiguratorService;
import com.tgs.services.rail.restmodel.RailConfigRuleTypeResponse;


@Service
public class RailConfiguratorHandler extends ServiceHandler<RailConfiguratorInfo, RailConfigRuleTypeResponse> {

	@Autowired
	private RailConfiguratorService configService;

	@Autowired
	SyncService syncService;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		DbRailConfiguratorRule rule = null;
		if (Objects.nonNull(request.getId())) {
			rule = configService.findById(request.getId());
		}
		request.cleanData();
		rule = new DbRailConfiguratorRule().from(request);
		rule = configService.save(rule);
		request.setId(rule.getId());
		syncService.sync("rail", rule.toDomain());

	}

	@Override
	public void afterProcess() throws Exception {}

	public List<DbRailConfiguratorRule> findAll(RailConfiguratorInfoFilter queryFilter) {
		return configService.findAll(queryFilter);
	}

	public BaseResponse deleteConfigRule(Long id) throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		DbRailConfiguratorRule railConfiguratorRule = configService.findById(id);
		if (Objects.nonNull(railConfiguratorRule)) {
			railConfiguratorRule.setDeleted(Boolean.TRUE);
			railConfiguratorRule.setEnabled(Boolean.FALSE);
			configService.save(railConfiguratorRule);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updateConfigStatus(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbRailConfiguratorRule dbConfigRule = configService.findById(id);
		if (Objects.nonNull(dbConfigRule)) {
			dbConfigRule.setEnabled(status);
			configService.save(dbConfigRule);
			syncService.sync("rail", null);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}
}
