package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.irctc.datamodel.BookingResponseDTO;
import com.irctc.datamodel.PassengerDetailDTO;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.RailOrderItemCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteAdditionalInfo;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.rail.RailItemStatus;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailSeatAllocation;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.helper.RailStationHelper;
import com.tgs.services.rail.utils.RailParsingHelper;
import com.tgs.services.rail.utils.RailUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcBookingManager extends IrctcServiceManager {

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private RailOrderItemCommunicator itemComm;

	@Autowired
	private OrderServiceCommunicator orderService;

	@Autowired
	protected GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	protected IrctcBookingRetrieveManager retrieveManager;

	protected RailJourneyInfo journeyInfo;

	public void redirect(String refId, HttpServletResponse response) throws IOException {
		try {
			CacheMetaInfo cacheMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
					.set(CacheSetName.EXTERNAL_PAYMENTS.getName()).key("successurl" + refId)
					.typeOfT(new TypeToken<List<String>>() {
					}.getType()).build();
			List<String> successUrl = cachingCommunicator
					.get(cacheMetaInfo, List.class, false, false, null, BinName.PAYMENTS.name())
					.get(BinName.PAYMENTS.name());
			response.sendRedirect(successUrl.get(0));
		} catch (Exception e) {
			log.error("Failed to redirect for bookingId {} due to ", refId, e);
		}
		response.getWriter().println("OK");
	}

	public void processBooking(BookingResponseDTO bookingResponse, String bookingId) {
		try {
			String resp = GsonUtils.getGson().toJson(bookingResponse);
			String endPointRQRS = StringUtils.join(formatRQRS(resp, "BookingRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("Irctc-Book-API").build());
			journeyInfo = itemComm.getJourneyInfo(bookingId);
			if (StringUtils.isNotEmpty(bookingResponse.getBookingErrorMessage())
					|| StringUtils.isNotEmpty(bookingResponse.getErrorMessage())) {
				itemComm.abortBooking(bookingId);
				String message = StringUtils.isNotEmpty(bookingResponse.getBookingErrorMessage())
						? bookingResponse.getBookingErrorMessage()
						: bookingResponse.getErrorMessage();
				Note note = Note.builder().bookingId(bookingId).noteType(NoteType.AUTO_ABORT).noteMessage(message)
						.additionalInfo(NoteAdditionalInfo.builder().visibleToAgent(true).build()).build();
				gmsCommunicator.addNote(note);
			} else {
				journeyInfo.setBoardingStationInfo(RailStationHelper.getRailStation(bookingResponse.getBoardingStn()));
				journeyInfo.setReservationUptoStationInfo(
						RailStationHelper.getRailStation(bookingResponse.getResvnUptoStn()));
				journeyInfo.getBookingRelatedInfo().setReservationId(bookingResponse.getReservationId());
				journeyInfo.getRailInfo().setTrainOwner(String.valueOf(bookingResponse.getTrainOwner()));
				journeyInfo.setAvlblForVikalp(bookingResponse.getAvlForVikalp());
				journeyInfo.getBookingRelatedInfo().setPnr(bookingResponse.getPnrNumber());
				List<RailTravellerInfo> travellerInfos = journeyInfo.getBookingRelatedInfo().getTravellerInfos();
				travellerInfos.forEach(traveller -> {
					updateTravellerInfo(traveller, bookingResponse);
				});
				itemComm.updateOrderItem(RailItemStatus.SUCCESS, journeyInfo, bookingId);
			}
			deleteSessionKeyFromCache(bookingId);
			log.info("Order SuccessFully Stored for bookingId {}", bookingId);
		} finally {
			LogUtils.log(LogTypes.RAILBOOKING_PROCESS,
					LogMetaInfo.builder().searchId(bookingId).timeInMs(System.currentTimeMillis()).build(), null);
			LogUtils.clearLogList();
		}
	}

	public void processFailedBooking(String bookingId, String responseStr) {
		try {
			String endPointRQRS = StringUtils.join(formatRQRS(responseStr, "BookingRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("Irctc-Book-API").build());
			journeyInfo = itemComm.getJourneyInfo(bookingId);
			RailOrderItem railOrderItem = itemComm.getRailOrderItem(bookingId);
			/*
			 * This check is to avoid aborting success Bookings,
			 */
			if (!railOrderItem.getStatus().equalsIgnoreCase("S")) {
				// itemComm.updateOrderItem(RailItemStatus.PENDING, journeyInfo, bookingId);
				itemComm.abortBooking(bookingId);
				Note note = Note.builder().bookingId(bookingId).noteType(NoteType.AUTO_ABORT)
						.noteMessage("Booking cancelled by the user").build();
				gmsCommunicator.addNote(note);
				deleteSessionKeyFromCache(bookingId);
			}

		} finally {
			LogUtils.log(LogTypes.RAILBOOKING_PROCESS,
					LogMetaInfo.builder().searchId(bookingId).timeInMs(System.currentTimeMillis()).build(), null);
			LogUtils.clearLogList();
		}
	}

	private void deleteSessionKeyFromCache(String bookingId) {
		Order order = orderService.findByBookingId(bookingId);
		String key = order.getBookingUserId();
		itemComm.deleteFromCache(key);
	}

	private void updateTravellerInfo(RailTravellerInfo travellerInfo, BookingResponseDTO bookingResponse) {
		for (PassengerDetailDTO passengerDto : RailParsingHelper.getValueFromIrctc(bookingResponse.getPsgnDtlList(),
				PassengerDetailDTO.class)) {
			if (travellerInfo.getId() == Long.parseLong(passengerDto.getPassengerSerialNumber())) {
				travellerInfo.setPassengerNetFare((double) passengerDto.getPassengerNetFare());
				travellerInfo.setBerthChoice(passengerDto.getPassengerBerthChoice());
				RailSeatAllocation bookingSeatAllocation = RailSeatAllocation.builder()
						.coachId(passengerDto.getBookingCoachId())
						.berthNo(Integer.valueOf(passengerDto.getBookingBerthNo()))
						.berthchoice(passengerDto.getBookingBerthCode()).bookingStatus(passengerDto.getBookingStatus())
						.build();
				RailSeatAllocation currentSeatAllocation = RailSeatAllocation.builder()
						.coachId(passengerDto.getCurrentCoachId())
						.berthNo(StringUtils.isEmpty(passengerDto.getCurrentCoachId())
								&& "0".equals(passengerDto.getCurrentBerthNo()) ? null
										: Integer.valueOf(passengerDto.getCurrentBerthNo()))
						.berthchoice(passengerDto.getCurrentBerthCode()).bookingStatus(passengerDto.getCurrentStatus())
						.build();
				travellerInfo.setCurrentSeatAllocation(currentSeatAllocation);
				travellerInfo.setBookingSeatAllocation(bookingSeatAllocation);
				travellerInfo.setIsBedRollOpted(Boolean.parseBoolean(passengerDto.getPassengerBedrollChoice()));
				travellerInfo.setPaxType(RailUtils.getRailPaxType(travellerInfo.getAge(), travellerInfo.getGender()));
			}
		}
	}

	public void checkCancelledBookings(String bookingId) {
		try {
			retrieveManager.init();
			log.info("Confirming for booking whether to Cancel or not for bookingId : {}", bookingId);
			BookingResponseDTO bookingResponse = retrieveManager.getBookingResponse(bookingId);
			processBooking(bookingResponse, bookingId);
		} catch (Exception e) {
			log.error("Exception Occured while Confirming whether to Cancel or not for bookingId : {} due to {}",
					bookingId, e.getMessage(), e);
		}
	}

}
