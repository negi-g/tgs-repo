package com.tgs.services.rail.restcontroller;

import java.util.Base64;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.irctc.datamodel.BookingRequestDTO;
import com.irctc.datamodel.BookingResponseDTO;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.rail.restmodel.ChangeBoardingPointRequest;
import com.tgs.services.rail.restmodel.RailBoardingPointEnquiryRequest;
import com.tgs.services.rail.restmodel.RailBoardingStationListResponse;
import com.tgs.services.rail.restmodel.RailCurrentStatus;
import com.tgs.services.rail.restmodel.RailFareEnquiryRequest;
import com.tgs.services.rail.restmodel.RailFareEnquiryResponse;
import com.tgs.services.rail.restmodel.RailPnrEnquiryRequest;
import com.tgs.services.rail.restmodel.RailPnrEnquiryResponse;
import com.tgs.services.rail.restmodel.RailReviewBookingRequest;
import com.tgs.services.rail.restmodel.RailScheduleEnquiryRequest;
import com.tgs.services.rail.restmodel.RailSearchRequest;
import com.tgs.services.rail.restmodel.RailSearchResponse;
import com.tgs.services.rail.restmodel.RailTrainScheduleResponse;
import com.tgs.services.rail.servicehandler.RailBoardingPointChangeHandler;
import com.tgs.services.rail.servicehandler.RailBoardingPointEnquiryHandler;
import com.tgs.services.rail.servicehandler.RailFareEnquiryHandler;
import com.tgs.services.rail.servicehandler.RailLiveBookingDetailHandler;
import com.tgs.services.rail.servicehandler.RailPnrEnquiryHandler;
import com.tgs.services.rail.servicehandler.RailReviewHandler;
import com.tgs.services.rail.servicehandler.RailScheduleHandler;
import com.tgs.services.rail.servicehandler.RailSearchHandler;
import com.tgs.services.rail.sources.irctc.IrctcBookingManager;
import com.tgs.services.rail.utils.RailUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/rail/v1")
@Slf4j
public class RailController {

	@Autowired
	protected RailPnrEnquiryHandler pnrEnquiryHandler;

	@Autowired
	protected RailScheduleHandler scheduleHandler;

	@Autowired
	protected RailFareEnquiryHandler fareEnquiryHandler;

	@Autowired
	protected RailReviewHandler railReviewHandler;

	@Autowired
	protected RailSearchHandler searchHandler;

	@Autowired
	protected IrctcBookingManager bookingManager;

	@Autowired
	protected RailBoardingPointEnquiryHandler boardingPointEnquiryHandler;

	@Autowired
	protected RailBoardingPointChangeHandler boardingPointChangeHandler;

	@Autowired
	protected RailReviewRequestValidator railReviewRequestValidator;

	@Autowired
	protected RailLiveBookingDetailHandler bookingDetailHandler;

	@InitBinder("railReviewBookingRequest")
	public void initBinderForRailReviewRequest(WebDataBinder binder) {
		binder.setValidator(railReviewRequestValidator);
	}

	@RequestMapping(value = "/pnr-enquiry", method = RequestMethod.POST)
	protected RailPnrEnquiryResponse getPnrEnquiry(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RailPnrEnquiryRequest railPnrEnquiryRequest) throws Exception {
		pnrEnquiryHandler.initData(railPnrEnquiryRequest, RailPnrEnquiryResponse.builder().build());
		return pnrEnquiryHandler.getResponse();
	}

	@RequestMapping(value = "/route", method = RequestMethod.POST)
	protected RailTrainScheduleResponse getTrainRoute(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RailScheduleEnquiryRequest railRouteRequest) throws Exception {
		scheduleHandler.initData(railRouteRequest, RailTrainScheduleResponse.builder().build());
		return scheduleHandler.getResponse();
	}

	@RequestMapping(value = "/fare-enquiry", method = RequestMethod.POST)
	protected RailFareEnquiryResponse getFareEnquiry(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RailFareEnquiryRequest fareEnquiryRequest) throws Exception {
		fareEnquiryHandler.initData(fareEnquiryRequest, new RailFareEnquiryResponse());
		return fareEnquiryHandler.getResponse();
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	protected RailSearchResponse search(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RailSearchRequest searchRequest) throws Exception {
		ServiceUtils.isProductEnabled(Product.RAIL);
		RailUtils.populateMissingParamtersInRailSearchQuery(searchRequest.getSearchQuery());
		log.debug("Got search request for searchQuery {}", searchRequest.getSearchQuery());
		searchRequest.getSearchQuery()
				.setSearchId(StringUtils.join("irctc-", RandomStringUtils.random(10, false, true)));
		searchHandler.initData(searchRequest, new RailSearchResponse());
		return searchHandler.getResponse();
	}

	@RequestMapping(value = "/review", method = RequestMethod.POST)
	protected RailFareEnquiryResponse reviewBooking(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody RailReviewBookingRequest railReviewRequest) throws Exception {
		railReviewHandler.initData(railReviewRequest, new RailFareEnquiryResponse());
		return railReviewHandler.getResponse();
	}

	@RequestMapping(value = "/booking/{bookingId}", method = RequestMethod.POST)
	protected void processBooking(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("bookingId") String bookingId) throws Exception {
				try {
					Enumeration<String> headerNames = request.getHeaderNames();
					if (headerNames != null) {
						while (headerNames.hasMoreElements()) {
							String headerName = headerNames.nextElement();
							log.info("[ProcessBooking] Request header for bookingId {} received is {} and value is {} ",
									bookingId, GsonUtils.getGson().toJson(headerName), request.getHeader(headerName));
						}
					}
					log.info("[ProcessBooking] Remote Addr for bookingId {} is {}", bookingId,
							GsonUtils.getGson().toJson(request.getRemoteAddr()));
		
				} catch (Exception e) {
					log.info("Exception while fetchimg request headers for bookingId {}{}", e.getMessage(), bookingId);
				}
		String responseStr = GsonUtils.getGson().toJson(HttpUtils.getRequestParams(request));
		BookingRequestDTO bookingRequest = GsonUtils.getGson().fromJson(responseStr, BookingRequestDTO.class);
		log.info("Message received from irctc {}", responseStr);
		byte[] actualBytes = Base64.getDecoder().decode(bookingRequest.getData());
		String actualData = new String(actualBytes);
		BookingResponseDTO bookingResponse = GsonUtils.getGson().fromJson(actualData, BookingResponseDTO.class);
		log.info("Booking Response receieved is : {}", bookingResponse);
		bookingManager.init();
		bookingManager.checkCancelledBookings(bookingId);
		bookingManager.redirect(bookingId, response);
	}

	@RequestMapping(value = "/booking/{bookingId}", method = RequestMethod.GET)
	protected void processCancelledBooking(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("bookingId") String bookingId, @RequestParam("CancelButton") String isCancelled)
			throws Exception {
		try {
			Enumeration<String> headerNames = request.getHeaderNames();
			if (headerNames != null) {
				while (headerNames.hasMoreElements()) {
					String headerName = headerNames.nextElement();
					log.info("[IRCTCAutoAbort] Request header for bookingId {} received is {} and value is {} ",
							bookingId, GsonUtils.getGson().toJson(headerName), request.getHeader(headerName));
				}
			}
			log.info("[IRCTCAutoAbort] Remote Addr for bookingId {} is {}", bookingId,
					GsonUtils.getGson().toJson(request.getRemoteAddr()));

		} catch (Exception e) {
			log.info("Execption while fetchimg request headers for bookingId {}{}", e.getMessage(), bookingId);
		}
		bookingManager.init();
		bookingManager.checkCancelledBookings(bookingId);
		// String responseStr = GsonUtils.getGson().toJson(HttpUtils.getRequestParams(request));
		// log.info("Message received from irctc for cancelled booking is {}", responseStr);
		// bookingManager.processFailedBooking(bookingId, responseStr);
		bookingManager.redirect(bookingId, response);
	}

	@RequestMapping(value = "/boardingpoint-enq", method = RequestMethod.POST)
	protected RailBoardingStationListResponse getBoardingPoints(HttpServletRequest request,
			HttpServletResponse response, @RequestBody RailBoardingPointEnquiryRequest boardingEnquiryRequest)
			throws Exception {
		boardingPointEnquiryHandler.initData(boardingEnquiryRequest, new RailBoardingStationListResponse());
		return boardingPointEnquiryHandler.getResponse();
	}

	@RequestMapping(value = "/boardingpoint-change", method = RequestMethod.POST)
	protected BaseResponse changeBoardingPoint(HttpServletRequest request, HttpServletResponse response,
			@RequestBody ChangeBoardingPointRequest boardingChangeRequest) throws Exception {
		boardingPointChangeHandler.initData(boardingChangeRequest, new BaseResponse());
		return boardingPointChangeHandler.getResponse();
	}

	@RequestMapping(value = "/booking-details/{bookingId}", method = RequestMethod.GET)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected RailCurrentStatus fetchLiveStatusFromIrctc(@PathVariable("bookingId") String bookingId) throws Exception {
		bookingDetailHandler.initData(bookingId, RailCurrentStatus.builder().build());
		return bookingDetailHandler.getResponse();
	}

}
