package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.OptVikalpDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcOptVikalpManager extends IrctcServiceManager {

	private static final String VIKALP_OPT_SUFFIX = "/tatktservices/optVikalp";

	public boolean optforVikalp(String transactionId, String token, boolean specialTrainFlag) throws IOException {
		try {
			String resp = sendGET(
					getOptForVikalpUrl(transactionId, token, BooleanUtils.isTrue(specialTrainFlag) ? "1" : "0"));
			OptVikalpDTO response = GsonUtils.getGson().fromJson(resp, OptVikalpDTO.class);
			if (StringUtils.isEmpty(response.getError()) && CollectionUtils.isEmpty(response.getErrorList())) {
				return true;
			} else {
				log.info("[OptVikalp] Error message is {}", response.getError());
				log.info("[OptVikalp] Error messages are {}", GsonUtils.getGson().toJson(response.getErrorList()));
				throw new CustomGeneralException(response.getError());
			}
		} catch (IOException e) {
			log.error("Error occured while opting for vikalp for transactionId {} because {} ", transactionId,
					e.getMessage());
		}
		return false;
	}

	private String getOptForVikalpUrl(String transactionId, String token, String specialTrainFlag) {
		// https://testngetjp.irctc.co.in/eticketing/webservices/tatktservices/optVikalp/200000058851215/0%231/1
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(sourceConfiguration.getUrl(), VIKALP_OPT_SUFFIX);
		} else {
			apirUrl = apiURLS.getVikalpOtpURL();
		}
		// Encoded form
		token = token.replaceAll("#", "%23");
		apirUrl = StringUtils.join(apirUrl, "/", transactionId, "/", token, "/", specialTrainFlag);
		log.debug("IRCTC opt vikalp URL {}", apirUrl);
		return apirUrl;
	}
}
