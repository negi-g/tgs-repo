package com.tgs.services.rail.dbmodel;

import java.time.LocalDateTime;
import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.rail.datamodel.config.RailConfiguratorInfo;
import com.tgs.services.rail.datamodel.ruleengine.RailBasicRuleCriteria;
import lombok.Getter;
import lombok.Setter;

@Audited
@Getter
@Setter
@Entity
@Table(name = "railconfiguratorrule")
@TypeDefs({@TypeDef(name = "RailBasicRuleCriteriaType", typeClass = RailBasicRuleCriteriaType.class)})
public class DbRailConfiguratorRule extends BaseModel<DbRailConfiguratorRule, RailConfiguratorInfo> {


	@Column(updatable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private boolean enabled;

	@Column
	@Nonnull
	private String ruleType;

	@Column
	@Type(type = "RailBasicRuleCriteriaType")
	private RailBasicRuleCriteria inclusionCriteria;

	@Column
	@Type(type = "RailBasicRuleCriteriaType")
	private RailBasicRuleCriteria exclusionCriteria;

	@Column
	@Nonnull
	@JsonAdapter(JsonStringSerializer.class)
	private String output;

	@Column
	private double priority;

	@Column
	private Boolean exitOnMatch;

	@Column
	private boolean isDeleted;

	@Column
	private String description;

	@Override
	public RailConfiguratorInfo toDomain() {
		return new GsonMapper<>(this, RailConfiguratorInfo.class).convert();
	}

	@Override
	public DbRailConfiguratorRule from(RailConfiguratorInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbRailConfiguratorRule.class).convert();
	}

}
