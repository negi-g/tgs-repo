package com.tgs.services.rail.servicehandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.rail.restmodel.RailUserAvailabilityRequest;
import com.tgs.services.rail.restmodel.RailUserAvailabilityResponse;
import com.tgs.services.rail.sources.irctc.IrctcUserAvlManager;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailUserAvailabilityHandler
		extends ServiceHandler<RailUserAvailabilityRequest, RailUserAvailabilityResponse> {

	@Autowired
	private IrctcUserAvlManager irctcUserManager;

	@Override
	public void beforeProcess() throws Exception {


	}

	@Override
	public void process() throws Exception {
		try {
			irctcUserManager.init();
			response = irctcUserManager.checkUserAvailability(request);
		} catch (Exception e) {
			log.error("Error Occured while checking for User Availability due to {}", e);
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
