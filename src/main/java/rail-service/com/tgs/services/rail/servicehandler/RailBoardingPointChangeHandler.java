package com.tgs.services.rail.servicehandler;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.RailOrderItemCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.rail.datamodel.message.ChangeBoardingPointMailAttributes;
import com.tgs.services.rail.restmodel.ChangeBoardingPointRequest;
import com.tgs.services.rail.restmodel.ChangeBoardingPointResponse;
import com.tgs.services.rail.sources.irctc.IrctcChangeBoardingPointManager;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailBoardingPointChangeHandler extends ServiceHandler<ChangeBoardingPointRequest, BaseResponse> {

	@Autowired
	protected IrctcChangeBoardingPointManager boardingPointChangeManager;

	@Autowired
	protected RailOrderItemCommunicator orderItemCommunicator;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	protected OrderServiceCommunicator orderService;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		try {
			RailOrderItem railOrderItem = orderItemCommunicator.getRailOrderItem(request.getBookingId());
			String pnr = railOrderItem.getAdditionalInfo().getPnr();

			boardingPointChangeManager.init();
			if (StringUtils.isEmpty(request.getNewBoardingStation())) {
				throw new CustomGeneralException("New Boarding Station can't be empty");

			}
			ChangeBoardingPointResponse boardingPointChangeResponse =
					boardingPointChangeManager.changeBoardingPoint(pnr, request);
			log.debug("Boarding Point change response is {}", GsonUtils.getGson().toJson(boardingPointChangeResponse));
			railOrderItem = orderItemCommunicator.updateOrderItem(request.getBookingId(),
					boardingPointChangeResponse.getNewBoardingPoint(),
					boardingPointChangeResponse.getNewBoardingDate());
			sendMail(railOrderItem);
		} catch (Exception e) {
			response.addError(new ErrorDetail(SystemError.BOARDING_CHANGE.getErrorCode(),
					SystemError.BOARDING_CHANGE.getMessage(e.getMessage())));
		}
	}


	@Override
	public void afterProcess() throws Exception {}

	private void sendMail(RailOrderItem railOrderItem) {
		AbstractMessageSupplier<ChangeBoardingPointMailAttributes> msgAttributes =
				new AbstractMessageSupplier<ChangeBoardingPointMailAttributes>() {
					@Override
					public ChangeBoardingPointMailAttributes get() {
						Order order = orderService.findByBookingId(railOrderItem.getBookingId());
						User user = userService.getUserFromCache(order.getBookingUserId());
						ChangeBoardingPointMailAttributes mailAttributes =
								ChangeBoardingPointMailAttributes.builder().build();
						mailAttributes.setBookingId(railOrderItem.getBookingId());
						mailAttributes.setPnr(railOrderItem.getAdditionalInfo().getPnr());
						mailAttributes.setType("Change Boarding Station");
						mailAttributes.setToEmailId(user.getEmail());
						mailAttributes.setRole(user.getRole());
						mailAttributes.setPartnerId(order.getPartnerId());
						mailAttributes.setOldBoardingStation(railOrderItem.getAdditionalInfo().getOldboardingStation());
						mailAttributes.setNewBoardingStation(railOrderItem.getAdditionalInfo().getBoardingStation());
						mailAttributes.setNewBoardingTime(
								DateFormatterHelper.formatDateTime(railOrderItem.getAdditionalInfo().getBoardingTime(),
										DateFormatType.BOOKING_EMAIL_FORMAT));
						mailAttributes.setTrainName(railOrderItem.getAdditionalInfo().getRailInfo().getName());
						mailAttributes.setTrainNumber(railOrderItem.getAdditionalInfo().getRailInfo().getNumber());
						mailAttributes.setKey(EmailTemplateKey.BOARDING_STATION_CHANGE_EMAIL.name());
						return mailAttributes;
					}
				};
		msgSrvCommunicator.sendMail(msgAttributes.getAttributes());
	}

}
