package com.tgs.services.rail.jparepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import com.tgs.filters.RailStationInfoFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.rail.dbmodel.DbRailStationInfo;
import com.tgs.services.rail.helper.RailStationHelper;

@Service
public class RailStationInfoService extends SearchService<DbRailStationInfo> {

	@Autowired
	RailStationInfoRepository stationRepository;

	@Autowired
	RailStationHelper stationHelper;

	public DbRailStationInfo findById(Long id) {
		return stationRepository.findOne(id);
	}

	public DbRailStationInfo save(DbRailStationInfo stationInfo) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			stationInfo = stationRepository.saveAndFlush(stationInfo);
		}
		stationHelper.process();
		return stationInfo;
	}

	public DbRailStationInfo findbyCode(String code) {
		return stationRepository.findByCode(code);
	}

	public List<DbRailStationInfo> findAll(RailStationInfoFilter railStationFilter) {
		return super.search(railStationFilter, stationRepository);
	}

	public List<DbRailStationInfo> fetchRailStations(@Param("searchQuery") String searchQuery) {
		List<DbRailStationInfo> stationsList = stationRepository.fetchRailStations(searchQuery);
		/*
		 * Comparator<DbRailStationInfo> compareByName = (DbRailStationInfo d1, DbRailStationInfo d2) ->
		 * d1.getName().compareTo(d2.getName()); Collections.sort(stationsList, compareByName);
		 */
		return stationsList;
	}
}
