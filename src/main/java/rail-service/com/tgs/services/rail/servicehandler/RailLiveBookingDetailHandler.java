package com.tgs.services.rail.servicehandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.rail.restmodel.RailCurrentStatus;
import com.tgs.services.rail.sources.irctc.IrctcBookingRetrieveManager;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailLiveBookingDetailHandler extends ServiceHandler<String, RailCurrentStatus> {

	@Autowired
	protected IrctcBookingRetrieveManager irctcBookingRetrieveManager;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() throws Exception {
		irctcBookingRetrieveManager.init();
		try {
			response = irctcBookingRetrieveManager.fetchLiveBookingDetails(request);
		} catch (Exception e) {
			response.addError(new ErrorDetail(SystemError.LIVE_DETAILS.getErrorCode(),
					SystemError.LIVE_DETAILS.getMessage(e.getMessage())));
			log.error("Error occured while fetching live details for bookingId {} due to {}", request, e.getMessage(),
					e);
		}

	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
