package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.BoardingPointEnquiryDTO;
import com.irctc.datamodel.BoardingStationDTO;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.restmodel.RailBoardingStationListResponse;
import com.tgs.services.rail.utils.RailParsingHelper;
import com.tgs.services.rail.utils.RailTempUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcBoardingPointsRetrieveManager extends IrctcServiceManager {

	private static final String BOARDING_POINT_ENQUIRY_SUFFIX = "/taenqservices/boardingstationenq";

	public RailBoardingStationListResponse retrieveBoardingPoints(String trainNo, String journeyDate,
			String fromStation, String toStation, String journeyClass, String bookingId) throws IOException {
		String boradingEnquiryUrl = StringUtils.EMPTY;
		String resp = StringUtils.EMPTY;
		String newTrainNo = RailTempUtils.getNewTrainNo(trainNo);
		log.debug("Old train no. is {} and new train no. is {}", trainNo, newTrainNo);
		RailBoardingStationListResponse boardingStations = new RailBoardingStationListResponse();
		try {
			boradingEnquiryUrl = getBoardingPointEnquiryUrl(newTrainNo, journeyDate, fromStation, toStation,
					journeyClass);
			resp = sendGET(boradingEnquiryUrl);
			BoardingPointEnquiryDTO response = GsonUtils.getGson().fromJson(resp, BoardingPointEnquiryDTO.class);
			if (StringUtils.isEmpty(response.getErrorMessage()))
				return parseBoardingPointEnquiryResponse(response);
			else {
				// Temp handling - to be removed after 23rd nov, this is for the trains whose
				// new number has yet to be
				// changed
				if (!newTrainNo.equals(trainNo)) {
					log.info("Retrying boarding point enquiry of old train number {} with new train number  {}",
							newTrainNo, trainNo);
					boradingEnquiryUrl = getBoardingPointEnquiryUrl(trainNo, journeyDate, fromStation, toStation,
							journeyClass);
					resp = sendGET(boradingEnquiryUrl);
					BoardingPointEnquiryDTO newResponse = GsonUtils.getGson().fromJson(resp,
							BoardingPointEnquiryDTO.class);
					if (StringUtils.isEmpty(newResponse.getErrorMessage()))
						return parseBoardingPointEnquiryResponse(newResponse);
					else
						throw new CustomGeneralException(newResponse.getErrorMessage());

				} else {
					throw new CustomGeneralException(response.getErrorMessage());
				}
			}
		} catch (IOException e) {
			log.error("Error occured during boarding point enquiry of train no  {} journey date  {} because {} ",
					trainNo, journeyDate, e.getMessage(), e);
		} finally {
			String endPointRQRS = StringUtils.join(formatUrl(boradingEnquiryUrl),
					formatRQRS(resp, "BoardingPointEnquiry"));
			listener.addLog(
					LogData.builder().key(bookingId).logData(endPointRQRS).type("Irctc-BoardingPointEnquiry").build());
		}
		return boardingStations;
	}

	private String getBoardingPointEnquiryUrl(String trainNo, String journeyDate, String fromStation, String toStation,
			String journeyClass) {
		// https://testngetjp.irctc.co.in/eticketing/webservices/taenqservices/boardingstationenq/12952/20210219/NDLS/MMCT/1A
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(sourceConfiguration.getUrl(), BOARDING_POINT_ENQUIRY_SUFFIX);
		} else {
			apirUrl = apiURLS.getBoardingPointEnquiryURL();
		}
		apirUrl = StringUtils.join(apirUrl, "/", trainNo, "/", journeyDate, "/", fromStation, "/", toStation, "/",
				journeyClass);
		log.debug("API Url for boarding point enquiry is {}", apirUrl);
		return apirUrl;
	}

	private RailBoardingStationListResponse parseBoardingPointEnquiryResponse(
			BoardingPointEnquiryDTO irctcPnrEnquiryResponse) {
		RailBoardingStationListResponse response = new RailBoardingStationListResponse();
		List<RailStationInfo> stations = new ArrayList<>();
		for (BoardingStationDTO station : RailParsingHelper
				.getValueFromIrctc(irctcPnrEnquiryResponse.getBoardingStationList(), BoardingStationDTO.class)) {
			String[] stationNameCode = station.getStnNameCode().split("-");
			stations.add(
					RailStationInfo.builder().code(stationNameCode[1].trim()).name(stationNameCode[0].trim()).build());
		}
		response.setStations(stations);
		return response;
	}

}
