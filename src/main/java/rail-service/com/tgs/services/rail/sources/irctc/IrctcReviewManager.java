package com.tgs.services.rail.sources.irctc;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.irctc.datamodel.AvailabilityDTO;
import com.irctc.datamodel.AvlFareResponseDTO;
import com.irctc.datamodel.EnquiryRequestDTO;
import com.irctc.datamodel.GSTDetailsDTO;
import com.irctc.datamodel.InfantDTO;
import com.irctc.datamodel.PassengerDetailDTO;
import com.irctc.datamodel.RailInformationMessage;
import com.irctc.datamodel.TicketAddress;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.rail.datamodel.RailAvailabilityDetail;
import com.tgs.services.rail.datamodel.RailBookingRelatedInfo;
import com.tgs.services.rail.datamodel.RailCommAddress;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailFareEnquiry;
import com.tgs.services.rail.datamodel.RailInfo;
import com.tgs.services.rail.datamodel.RailJourneyAvailability;
import com.tgs.services.rail.datamodel.RailJourneyClass;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import com.tgs.services.rail.restmodel.RailReviewBookingRequest;
import com.tgs.services.rail.utils.RailParsingHelper;
import com.tgs.services.rail.utils.RailUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IrctcReviewManager extends IrctcServiceManager {

	@Value("${domain}")
	private String domain;

	@Value("${commProtocol}")
	private String protocol;

	private String buildReturnUrl() {
		StringBuilder url = new StringBuilder(protocol).append("://").append(domain);
		url.append("/rail/v1/booking");
		return url.toString();
	}


	public RailJourneyInfo doReviewJourney(RailReviewBookingRequest request) throws Exception {
		String enquiryUrl = "";
		String resp = "";
		String clientTransactionId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.RAIL).build());
		try {
			RailJourneyInfo journeyInfo = null;
			log.info("Doing review for bookingId {}", clientTransactionId);
			EnquiryRequestDTO fareEnquiryRequest = createFareEnquiryRequest(request, clientTransactionId);
			enquiryUrl = getFareAvailabilityUrl(request.getFareEnquiry());
			log.info("Enquiry Url : {}", enquiryUrl);
			postData = GsonUtils.getGson().toJson(fareEnquiryRequest);
			log.info("Request generated for Passenger Details , {}", postData);
			User user = SystemContextHolder.getContextData().getUser();
			resp = sendPOST(enquiryUrl);
			AvlFareResponseDTO fareResponse = GsonUtils.getGson().fromJson(resp, AvlFareResponseDTO.class);
			if (StringUtils.isBlank(fareResponse.getErrorMessage())) {
				journeyInfo = parseJourney(fareResponse, request, clientTransactionId);
				RailBookingRelatedInfo bookingRelatedInfo = RailBookingRelatedInfo.builder().build();
				bookingRelatedInfo.setClientTransactionId(clientTransactionId);
				bookingRelatedInfo.setTicketAddress(request.getTktAddress());
				bookingRelatedInfo.setMobileNumber(request.getMobileNumber());
				bookingRelatedInfo.setJourneyClass(
						RailJourneyClass.getEnumFromCode(request.getFareEnquiryRequest().getJourneyClass()));
				bookingRelatedInfo.setQuota(request.getFareEnquiry().getBookingQuota());
				bookingRelatedInfo.setUserLoginId(user.getRailAdditionalInfo().getIrctcAgentId());
				bookingRelatedInfo.setIsThroughOTP(request.isThroughOTP());
				bookingRelatedInfo.setIsTravelInsuranceOpted(request.getTravelInsuranceOpted());
				bookingRelatedInfo.setReturnUrl(buildReturnUrl());
				if (request.getGstInfo() != null && StringUtils.isNotBlank(request.getGstInfo().getGstNumber())) {
					bookingRelatedInfo.setGstInfo(request.getGstInfo());
				}
				
				journeyInfo.setBookingRelatedInfo(bookingRelatedInfo);
			} else {
				throw new CustomGeneralException(fareResponse.getErrorMessage());
			}
			return journeyInfo;
		} catch (IOException e) {
			log.error("Error Occured during review of bookingid {}, due to {}", clientTransactionId, e.getMessage(), e);
			throw e;
		} finally {
			String endPointRQRS = StringUtils.join(formatUrl(enquiryUrl), formatRQRS(postData, "ReviewRQ"),
					formatRQRS(resp, "ReviewRS"));
			listener.addLog(LogData.builder().key(request.getBookingId()).logData(endPointRQRS).type("Irctc-Review-Req")
					.build());
		}
	}

	private RailJourneyInfo parseJourney(AvlFareResponseDTO fareResponse, RailReviewBookingRequest reviewRequest,
			String clientTransactionId) {

		RailJourneyInfo journeyInfo = buildJourney(fareResponse, reviewRequest.getFareEnquiry());
		RailPriceInfo priceInfo = populatePriceInfo(fareResponse, journeyInfo, clientTransactionId);
		RailJourneyAvailability journeyAvailability = new RailJourneyAvailability();
		List<RailAvailabilityDetail> railAvailabilityList = new ArrayList<RailAvailabilityDetail>();
		List<AvailabilityDTO> avlDay =
				RailParsingHelper.getValueFromIrctc(fareResponse.getAvlDayList(), AvailabilityDTO.class);
		RailAvailabilityDetail railAvailability = new RailAvailabilityDetail();
		railAvailability.setAvailabilityStatus(avlDay.get(0).getAvailablityStatus());
		railAvailability
				.setJourneyDate(convertStringToDate(avlDay.get(0).getAvailablityDate(), "dd-mm-yyyy").toLocalDate());
		railAvailability.setPriceInfo(priceInfo);
		railAvailabilityList.add(railAvailability);
		journeyAvailability.setRailAvailabilityDetails(railAvailabilityList);
		Map<String, RailJourneyAvailability> avlDetailsMap = new HashMap<String, RailJourneyAvailability>();
		avlDetailsMap.put(journeyInfo.getAvlJourneyClasses().get(0), journeyAvailability);
		journeyInfo.setAvlJourneyDetailsMap(avlDetailsMap);
		journeyInfo.setInfoMessages(getInfoMessagesFromIrctc(RailParsingHelper
				.getValueFromIrctc(fareResponse.getInformationMessage(), RailInformationMessage.class)));
		journeyInfo.getInfoMessages().add(getTravelInsuranceMsg(fareResponse.getBkgCfg()));
		return journeyInfo;
	}

	private RailPriceInfo populatePriceInfo(AvlFareResponseDTO fareResponse, RailJourneyInfo journeyInfo,
			String clientTransactionId) {
		RailPriceInfo priceInfo = new RailPriceInfo();
		priceInfo.setTotalFare(fareResponse.getTotalCollectibleAmount());
		priceInfo.getFareComponents().put(RailFareComponent.RF, Double.parseDouble(fareResponse.getBaseFare()));
		priceInfo.getFareComponents().put(RailFareComponent.CC, Double.parseDouble(fareResponse.getCateringCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.FC, Double.parseDouble(fareResponse.getFuelAmount()));
		priceInfo.getFareComponents().put(RailFareComponent.WP, Double.parseDouble(fareResponse.getWpServiceCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.WPT, Double.parseDouble(fareResponse.getWpServiceTax()));
		priceInfo.getFareComponents().put(RailFareComponent.OC, Double.parseDouble(fareResponse.getOtherCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.RC,
				Double.parseDouble(fareResponse.getReservationCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.ST, Double.parseDouble(fareResponse.getServiceTax()));
		priceInfo.getFareComponents().put(RailFareComponent.SC, Double.parseDouble(fareResponse.getSuperfastCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.TTF, Double.parseDouble(fareResponse.getTatkalFare()));
		priceInfo.getFareComponents().put(RailFareComponent.TF, fareResponse.getTotalCollectibleAmount());
		priceInfo.getFareComponents().put(RailFareComponent.NF, fareResponse.getTotalCollectibleAmount());
		priceInfo.getFareComponents().put(RailFareComponent.CONC,
				Double.parseDouble(fareResponse.getTotalConcession()) * (-1));
		priceInfo.getFareComponents().put(RailFareComponent.TI,
				Double.parseDouble(fareResponse.getTravelInsuranceCharge()));
		priceInfo.getFareComponents().put(RailFareComponent.TIT,
				Double.parseDouble(fareResponse.getTravelInsuranceServiceTax()));
		// This is the ticket fare shown on UI, all commercial calculations will be done on this fare
		Double baseFare = fareResponse.getTotalCollectibleAmount()
				- priceInfo.getFareComponents().getOrDefault(RailFareComponent.WP, 0.0)
				- priceInfo.getFareComponents().getOrDefault(RailFareComponent.WPT, 0.0)
				- priceInfo.getFareComponents().getOrDefault(RailFareComponent.TI, 0.0)
				- priceInfo.getFareComponents().getOrDefault(RailFareComponent.TIT, 0.0);
		priceInfo.getFareComponents().put(RailFareComponent.BF, baseFare);
		User user = SystemContextHolder.getContextData().getUser();
		RailUtils.updateAgentCharges(priceInfo, journeyInfo, user, clientTransactionId);
		RailUtils.updatePGCharges(priceInfo, journeyInfo, user, clientTransactionId);
		return priceInfo;
	}

	private RailJourneyInfo buildJourney(AvlFareResponseDTO fareResponse, RailFareEnquiry fareEnquiry) {
		RailJourneyInfo journeyInfo = new RailJourneyInfo();
		journeyInfo.setRailInfo(buildRailInfo(fareResponse));
		journeyInfo.setArrivalStaionInfo(getStationInfo(fareResponse.getTo()));
		journeyInfo.setDepartStationInfo(getStationInfo(fareResponse.getFrom()));
		journeyInfo.setDistance(Integer.parseInt(fareResponse.getDistance()));
		journeyInfo.setDuration(fareEnquiry.getDuration());
		journeyInfo.setBookingQuota(fareEnquiry.getBookingQuota());
		LocalDateTime departureDate = fareEnquiry.getDeparture();
		LocalTime departureTime = departureDate.toLocalTime();
		journeyInfo.setDeparture(fareEnquiry.getTravelDate().atTime(departureTime));
		journeyInfo.setArrival(journeyInfo.getDeparture().plusMinutes(Long.parseLong(fareEnquiry.getDuration())));
		journeyInfo.setAvlJourneyClasses(RailParsingHelper.getValueFromIrctc(fareResponse.getEnqClass()));
		journeyInfo.setBookingConfiguration(
				getBookingConfiguration(fareResponse.getBkgCfg(), journeyInfo.getAvlJourneyClasses()));
		return journeyInfo;
	}

	private RailInfo buildRailInfo(AvlFareResponseDTO fareResponse) {
		RailInfo railInfo = RailInfo.builder().build();
		railInfo.setName(fareResponse.getTrainName());
		railInfo.setNumber(fareResponse.getTrainNo());
		return railInfo;
	}

	private EnquiryRequestDTO createFareEnquiryRequest(RailReviewBookingRequest reviewRequest,
			String clientTransactionId) {
		User user = SystemContextHolder.getContextData().getUser();
		if (Objects.isNull(user.getRailAdditionalInfo())
				|| StringUtils.isEmpty(user.getRailAdditionalInfo().getIrctcAgentId())
				|| (StringUtils.isEmpty(user.getRailAdditionalInfo().getRegistedImeiNumber())
						&& StringUtils.isEmpty(user.getRailAdditionalInfo().getRegisterMacId()))) {
			throw new CustomGeneralException(SystemError.NOT_ONBOARDED.getMessage());

		}
		EnquiryRequestDTO enquiryRequest = EnquiryRequestDTO.builder().build();
		enquiryRequest.setMasterId(sourceConfiguration.getUserName());

		enquiryRequest.setWsUserLogin(user.getRailAdditionalInfo().getIrctcAgentId());
		enquiryRequest.setAgentDeviceId(getRegisteredDeviceId(user));
		enquiryRequest.setEnquiryType("3");
		enquiryRequest.setMobileNumber(reviewRequest.getMobileNumber());
		enquiryRequest.setClientTransactionId(clientTransactionId);
		reviewRequest.setBookingId(clientTransactionId);
		enquiryRequest.setReservationChoice(Short.parseShort(reviewRequest.getReservationChoice()));
		enquiryRequest.setReservationMode(getReservationMode(reviewRequest.isThroughOTP()));
		enquiryRequest.setPassengerList(createPassengerList(reviewRequest.getTravellerInfos()));
		enquiryRequest.setCoachId(reviewRequest.getPreffCoachId());
		enquiryRequest.setTravelInsuranceOpted(reviewRequest.getTravelInsuranceOpted());
		setInfantIfPresent(enquiryRequest, reviewRequest.getTravellerInfos());
		enquiryRequest.setMoreThanOneDay(true);
		enquiryRequest.setTktAddress(getTicketAddress(reviewRequest.getTktAddress()));
		enquiryRequest.setBoardingStation(reviewRequest.getFareEnquiry().getBoardingstation());
		if (reviewRequest.getGstInfo() != null && StringUtils.isNotEmpty(reviewRequest.getGstInfo().getGstNumber())) {
			enquiryRequest.setGstDetails(buildGstDetails(reviewRequest.getGstInfo()));
		}
		return enquiryRequest;
	}

	private GSTDetailsDTO buildGstDetails(GstInfo gstInfo) {
		GSTDetailsDTO gstDetails = GSTDetailsDTO.builder().build();
		gstDetails.setGstIn(gstInfo.getGstNumber());
		gstDetails.setNameOnGst(gstInfo.getRegisteredName());
		gstDetails.setCity(gstInfo.getCityName());
		gstDetails.setFlat(gstInfo.getAddress());
		gstDetails.setPin(gstInfo.getPincode());
		gstDetails.setState(gstInfo.getState());
		return gstDetails;
	}


	private void setInfantIfPresent(EnquiryRequestDTO enquiryRequest, @NonNull List<RailTravellerInfo> travellerInfos) {

		List<RailTravellerInfo> infants = new ArrayList<RailTravellerInfo>();
		for (RailTravellerInfo travInfo : travellerInfos) {
			if (travInfo.getAge() < 5) {
				infants.add(travInfo);
			}
		}
		if (infants.size() > 0) {
			List<InfantDTO> infantDtoList = new ArrayList<InfantDTO>();
			int index = 1;
			for (RailTravellerInfo infantInfo : infants) {
				InfantDTO infant = InfantDTO.builder().build();
				infant.setName(infantInfo.getFullName());
				infant.setAge(infantInfo.getAge().byteValue());
				infant.setGender(infantInfo.getGender());
				infant.setInfantSerialNumber((short) index);
				index++;
				infantDtoList.add(infant);
			}
			enquiryRequest.setInfantList(infantDtoList);
		}


	}

	private TicketAddress getTicketAddress(RailCommAddress commAddress) {
		TicketAddress tktAddress = TicketAddress.builder().pinCode(commAddress.getPincode())
				.street(commAddress.getStreet()).city(commAddress.getCity()).postOffice(commAddress.getPostOffice())
				.stateName(commAddress.getState()).address(commAddress.getAddress()).colony(commAddress.getColony())
				.build();
		return tktAddress;
	}

	private List<PassengerDetailDTO> createPassengerList(List<RailTravellerInfo> travellerInfos) {
		int index = 1;

		List<PassengerDetailDTO> passengerList = new ArrayList<PassengerDetailDTO>();
		for (RailTravellerInfo travellerInfo : travellerInfos) {
			if (travellerInfo.getAge() >= 5) {
				PassengerDetailDTO passengerDetail = PassengerDetailDTO.builder().build();
				passengerDetail.setPassengerSerialNumber(String.valueOf(index));
				passengerDetail.setPassengerName(travellerInfo.getFullName());
				passengerDetail.setPassengerNationality(travellerInfo.getNationality());
				passengerDetail.setPassengerAge(String.valueOf(travellerInfo.getAge()));
				if (travellerInfo.getAge() != null && (travellerInfo.getAge() >= 5 && travellerInfo.getAge() <= 11)) {
					passengerDetail.setChildBerthFlag(
							travellerInfo.getIsChildBerthOpted() != null ? travellerInfo.getIsChildBerthOpted()
									: false);
				}
				passengerDetail.setPassengerGender(travellerInfo.getGender());
				if (travellerInfo.getBerthChoice() != null) {
					passengerDetail.setPassengerBerthChoice(travellerInfo.getBerthChoice());
				}
				if (travellerInfo.getIsBedRollOpted() != null) {
					passengerDetail.setPassengerBedrollChoice(String.valueOf(travellerInfo.getIsBedRollOpted()));
				}
				if (travellerInfo.getFoodChoice() != null) {
					passengerDetail.setPassengerFoodChoice(travellerInfo.getFoodChoice().getCode());
				}
				if (BooleanUtils.isTrue(travellerInfo.getPassengerIcardFlag())) {
					if (StringUtils.isNotEmpty(travellerInfo.getDob()))
						passengerDetail.setPsgnConcDOB(travellerInfo.getDob().replace("-", ""));
					passengerDetail.setPassengerIcardFlag(travellerInfo.getPassengerIcardFlag());
					passengerDetail.setPassengerCardType(travellerInfo.getPassengerCardType());
					passengerDetail.setPassengerCardNumber(travellerInfo.getPassengerIcardNumber());
				}
				if (BooleanUtils.isTrue(travellerInfo.getIsConcessionOpted())) {
					passengerDetail.setConcessionOpted(true);
				}
				if (BooleanUtils.isTrue(travellerInfo.getIsForGoConcessionOpted())
						&& BooleanUtils.isTrue(travellerInfo.getIsConcessionOpted())) {
					passengerDetail.setForGoConcessionOpted(true);
				} else {
					passengerDetail.setForGoConcessionOpted(false);

				}
				if (travellerInfo.getConcessionChoice() != null) {
					passengerDetail.setPassengerConcession(travellerInfo.getConcessionChoice().toString());
					passengerDetail.setConcessionOpted(true);
					passengerDetail.setForGoConcessionOpted(true);
				}
				passengerList.add(passengerDetail);
				index++;
			}
		}
		return passengerList;
	}


	private String getRegisteredDeviceId(User user) {
		ChannelType channelType = SystemContextHolder.getChannelType();
		String agentDeviceId =
				ChannelType.MOBILE.equals(channelType) ? user.getRailAdditionalInfo().getRegistedImeiNumber()
						: user.getRailAdditionalInfo().getRegisterMacId();
		return agentDeviceId;
	}

	private String getReservationMode(boolean isThroughOTP) {
		// User is using digital certificate
		if (!isThroughOTP)
			return "WS_TA_B2B";
		else {
			ChannelType channelType = SystemContextHolder.getChannelType();
			if (ChannelType.MOBILE.equals(channelType))
				return "B2B_MOBILE_OTP";
			else
				return "B2B_WEB_OTP";
		}
	}
}
