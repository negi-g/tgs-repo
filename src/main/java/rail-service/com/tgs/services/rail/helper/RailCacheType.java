package com.tgs.services.rail.helper;

public enum RailCacheType {

	REVIEWKEYEXPIRATION(8 * 60 * 60);
	private int ttl;

	private RailCacheType(int ttl) {
		this.ttl = ttl;
	}

	public int getTtl() {
		return ttl;
	}

}
