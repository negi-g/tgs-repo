package com.tgs.services.rail.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.RailConfiguratorInfoFilter;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.rail.datamodel.config.RailConfiguratorInfo;
import com.tgs.services.rail.datamodel.ruleengine.RailBasicFact;
import com.tgs.services.rail.dbmodel.DbRailConfiguratorRule;
import com.tgs.services.rail.helper.RailConfiguratorHelper;
import com.tgs.services.rail.restmodel.RailConfigRuleTypeResponse;
import com.tgs.services.rail.servicehandler.RailConfiguratorHandler;
import com.tgs.services.ums.datamodel.AreaRole;

@RestController
@RequestMapping("/rail/v1/config")
public class RailConfiguratorController {

	@Autowired
	protected RailConfiguratorHandler railConfigHandler;

	@Autowired
	protected AuditsHandler auditHandler;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected RailConfigRuleTypeResponse saveOrUpdateRailConfigRule(HttpServletRequest request,
			HttpServletResponse response, @RequestBody RailConfiguratorInfo railConfiguratorInfo) throws Exception {
		railConfigHandler.initData(railConfiguratorInfo, new RailConfigRuleTypeResponse());
		return railConfigHandler.getResponse();
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected RailConfigRuleTypeResponse getRailConfigRuleType(HttpServletRequest request,
			HttpServletResponse httpResoponse, @RequestBody RailConfiguratorInfoFilter queryFilter) throws Exception {
		RailConfigRuleTypeResponse ruleTypeResponse = new RailConfigRuleTypeResponse();
		railConfigHandler.findAll(queryFilter).forEach(railConfig -> {
			ruleTypeResponse.getRailConfiguratorInfos()
					.add(new GsonMapper<>(railConfig, RailConfiguratorInfo.class).convert());
		});
		return ruleTypeResponse;
	}

	@RequestMapping(value = "/fetch/rule", method = RequestMethod.POST)
	protected RailConfigRuleTypeResponse getApplicableConfigRule(HttpServletRequest request,
			HttpServletResponse httpResoponse, @RequestBody RailConfiguratorInfoFilter queryFilter) throws Exception {
		RailConfigRuleTypeResponse ruleTypeResponse = new RailConfigRuleTypeResponse();
		RailBasicFact fact = RailBasicFact.createFact();
		fact = BaseUtils.createRailFactOnUser(fact, SystemContextHolder.getContextData().getUser());
		RailConfiguratorInfo configInfo = RailConfiguratorHelper.getRuleInfo(fact, queryFilter.getRuleType());
		if (configInfo != null) {
			ruleTypeResponse.getRailConfiguratorInfos().add(configInfo);
		}
		return ruleTypeResponse;

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected BaseResponse deleteConfigRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return railConfigHandler.deleteConfigRule(id);
	}

	@RequestMapping(value = "/railconfig-audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbRailConfiguratorRule.class, ""));
		return auditResponse;
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updateConfigStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return railConfigHandler.updateConfigStatus(id, status);
	}

}
