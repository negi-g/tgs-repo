package com.tgs.services.rail.communicator.impl;

import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.oms.restmodel.rail.RailCancellationRequest;
import com.tgs.services.oms.restmodel.rail.RailCancellationResponse;
import com.tgs.services.oms.restmodel.rail.RailOtpVerificationRequest;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.datamodel.RailTDRReason;
import com.tgs.services.rail.datamodel.RailTdrInfo;
import com.tgs.services.rail.datamodel.config.RailSourceConfigurationOutput;
import com.tgs.services.rail.helper.RailStationHelper;
import com.tgs.services.rail.manager.RailBookingEngine;
import com.tgs.services.rail.manager.RailCancellationEngine;
import com.tgs.services.rail.restmodel.RailCurrentStatus;
import com.tgs.services.rail.restmodel.RailFareEnquiryResponse;
import com.tgs.services.rail.utils.RailUtils;

@Service
public class RailCommunicatorImpl implements RailCommunicator {

	@Autowired
	protected RailBookingEngine bookingEngine;

	@Autowired
	protected RailCancellationEngine cancellationEngine;

	@Override
	public RailFareEnquiryResponse getRailFareEnquiryResponse(String bookingId) {
		return bookingEngine.getRailFareEnquiryResponse(bookingId);
	}

	@Override
	public RailStationInfo getRailStationInfo(String code) {
		if (StringUtils.isEmpty(code)) {
			return null;
		}
		return RailStationHelper.getRailStation(code);
	}

	@Override
	public RailCancellationResponse cancelPassengers(RailCancellationRequest request) {
		return cancellationEngine.cancelPassengers(request);
	}

	@Override
	public boolean submitOtpRequest(RailOtpVerificationRequest railOtpRequest) {
		return cancellationEngine.submitOtpRequest(railOtpRequest);
	}

	@Override
	public boolean resendOtp(RailOtpVerificationRequest railOtpRequest) {
		return cancellationEngine.resendOtp(railOtpRequest);
	}

	@Override
	public List<RailTDRReason> getReasonsList(String transactionId, String bookingId) {
		return cancellationEngine.fetchReasons(transactionId, bookingId);
	}

	@Override
	public boolean submitTDRRequest(RailTdrInfo tdrInfo, String transactionId, Set<String> paxIds, String bookingId) {
		return cancellationEngine.submitTDR(tdrInfo, transactionId, paxIds, bookingId);
	}

	@Override
	public RailCurrentStatus getLiveBookingDetails(String bookingId) {
		return bookingEngine.fetchLiveBookingDetails(bookingId);
	}

	@Override
	public RailSourceConfigurationOutput getRailSourceConfiguration() {
		return RailUtils.getRailSourceConfigOutput();
	}

}
