package com.tgs.services.rail.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.gson.GsonUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * In case of single value, irctc api returns single object In case of multiple values, irctc api returns List. This
 * class is helpful in doing all such parsings
 * 
 * @param value
 * @return
 */
@Slf4j
public class RailParsingHelper {

	public static List<String> getValueFromIrctc(Object obj) {
		List<String> list = new ArrayList<>();
		String str = GsonUtils.getGson().toJson(obj);
		if (StringUtils.isEmpty(str))
			return list;
		String[] strArray = str.split(",");
		for (String s : strArray) {
			if (!"null".equals(s))
				list.add(s.replaceAll("[^a-zA-Z0-9\\s.-]+", ""));
		}
		return list;
	}

	/**
	 * For now using this method to fetch cities and post office list only. Need to check if this can be used at all
	 * other places and we can deprecate using getValueFromIrctc
	 */
	public static List<String> getValueWithoutReplacingSpclChars(Object obj) {
		List<String> list = new ArrayList<>();
		String str = GsonUtils.getGson().toJson(obj);
		if (StringUtils.isEmpty(str))
			return list;
		String[] strArray = str.split(",");
		for (String s : strArray) {
			if (!"null".equals(s))
				list.add(s.replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", ""));
		}
		return list;
	}

	public static <T> List<T> getValueFromIrctc(Object value, Class<T> classOfT) {
		List<T> list = new ArrayList<>();
		String str = "";
		try {
			str = GsonUtils.getGson().toJson(value);
			RailParsingHelper s = new RailParsingHelper();
			list = GsonUtils.getGson().fromJson(str, s.new ListWithElements<T>(classOfT));
		} catch (Exception e) {
			log.debug("Unable to convert str {} to list of {}", str, classOfT);
			list = new ArrayList<>();
			T object = GsonUtils.getGson().fromJson(str, classOfT);
			list.add(object);
		}
		return list;
	}

	public class ListWithElements<T> implements ParameterizedType {

		private Class<T> elementsClass;

		public ListWithElements(Class<T> elementsClass) {
			this.elementsClass = elementsClass;
		}

		public Type[] getActualTypeArguments() {
			return new Type[] {elementsClass};
		}

		public Type getRawType() {
			return List.class;
		}

		public Type getOwnerType() {
			return null;
		}
	}
}
