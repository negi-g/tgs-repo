package com.tgs.services.rail.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.RailCachingServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.rail.restmodel.RailCurrentStatus;
import com.tgs.services.rail.restmodel.RailFareEnquiryResponse;
import com.tgs.services.rail.sources.irctc.IrctcBookingRetrieveManager;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailBookingEngine {

	@Autowired
	protected RailCachingServiceCommunicator cachingService;

	@Autowired
	protected IrctcBookingRetrieveManager bookingRetrieveManager;


	public RailFareEnquiryResponse getRailFareEnquiryResponse(String bookingId) {
		RailFareEnquiryResponse reviewResponse = cachingService.fetchValue(bookingId, RailFareEnquiryResponse.class,
				CacheSetName.RAIL_REVIEW.getName(), BinName.RAILREVIEW.getName());
		if (reviewResponse == null) {
			throw new CustomGeneralException(SystemError.EXPIRED_BOOKING_ID);
		}
		return reviewResponse;
	}

	public RailCurrentStatus fetchLiveBookingDetails(String bookingId) {
		RailCurrentStatus currentStatus = RailCurrentStatus.builder().build();
		bookingRetrieveManager.init();
		try {
			currentStatus = bookingRetrieveManager.fetchLiveBookingDetails(bookingId);
		} catch (Exception e) {
			log.error("Erorr Occured while fetching Live Booking Details for bookingId {} due to {}", bookingId, e);
		}
		return currentStatus;
	}


}
