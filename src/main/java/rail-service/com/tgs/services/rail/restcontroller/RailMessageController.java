package com.tgs.services.rail.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.restmodel.RailMessageRequest;
import com.tgs.services.rail.servicehandler.RailMessageHandler;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("/rail/v1/msg")
@RestController
@Slf4j
public class RailMessageController {

	@Autowired
	protected RailMessageHandler messageHandler;

	@RequestMapping(value = "/itinerary", method = RequestMethod.POST)
	protected BaseResponse sendEticket(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid RailMessageRequest emailRequest) throws Exception {
		messageHandler.initData(emailRequest, new BaseResponse());
		return messageHandler.getResponse();
	}
}
