package com.tgs.services.cms.flight.impl;

import java.util.Calendar;
import java.util.List;

import com.tgs.services.fms.datamodel.SectorInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightCommissionRuleInput {

	private List<SectorInfo> sectors;
	private List<SectorInfo> excludingSectors;
	private Calendar travelStartPeriod;
	private Calendar travelEndPeriod;
	private Calendar bookingStartPeriod;
	private Calendar bookingEndPeriod;

	private String platingCarrier;
	private List<String> fareBasisList;
	private List<String> bookingClassList;
	private List<String> excludedbookingClassList;

	private List<Calendar> excludedTravelDates;
	private List<Calendar> excludedBookingDates;
	private List<Integer> supplierIds;

	// Should have support for time interval, commission can be adjusted according
	// to time window for example night time we can have lower commission
}
