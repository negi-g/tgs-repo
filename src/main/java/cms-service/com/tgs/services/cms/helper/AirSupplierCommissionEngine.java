package com.tgs.services.cms.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.AirRules;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cms.datamodel.commission.air.AirCommercialComponent;
import com.tgs.services.cms.datamodel.commission.air.AirCommercialRuleCriteria;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionRule;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirSupplierCommissionEngine {

	public void processAirCommission(TripInfo tripInfo, User user) {
		try {
			AirType airType = BaseUtils.getTripType(tripInfo, true);
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(tripInfo, airType);
				BaseUtils.createFactOnUser(flightFact, user);
				if (segmentInfo.getBookingRelatedInfo() != null) {
					List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
					travellerInfos.forEach(traveller -> {
						// Post Booking
						PriceInfo priceInfo = PriceInfo.builder().build();
						priceInfo.setSupplierBasicInfo(segmentInfo.getSupplierInfo());
						Map<PaxType, FareDetail> fareDetailMap = new HashMap<>();
						fareDetailMap.put(traveller.getPaxType(), traveller.getFareDetail());
						priceInfo.setFareDetails(fareDetailMap);
						flightFact.generateFact(priceInfo);
						applyIATACommission(priceInfo, segmentInfo, flightFact, airType);
					});
				} else {
					if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
						for (int priceIndex = 0; priceIndex < segmentInfo.getPriceInfoList().size(); priceIndex++) {
							// Pre booking
							flightFact.generateFact(tripInfo, priceIndex);
							PriceInfo priceInfo = segmentInfo.getPriceInfoList().get(priceIndex);
							flightFact.generateFact(priceInfo);
							applyIATACommission(priceInfo, segmentInfo, flightFact, airType);
						}
					}
				}
			});
		} catch (Exception e) {
			log.error("Air Commission not applied on trip {}", tripInfo.toString(), e);
			throw new CustomGeneralException(SystemError.COMMISSION_FAILED);
		}
	}

	public void applyIATACommission(PriceInfo priceInfo, SegmentInfo segmentInfo, FlightBasicFact flightFact,
			AirType airType) {
		log.debug("Checking iata commission for airlineCode {}", flightFact.getAirline());
		AirCommissionRule airCommissionRule = AirCommissionHelper.getAirCommissionRule(flightFact, priceInfo);
		log.debug("Found commission rules {} for airlineCode {}", airCommissionRule, flightFact.getAirline());
		if (airCommissionRule != null) {
			AirCommercialRuleCriteria ruleCriteria = airCommissionRule.getCommissionCriteria();
			if (Objects.nonNull(airCommissionRule) && MapUtils.isNotEmpty(ruleCriteria.getCommission())) {
				AirCommercialComponent component = ruleCriteria.getCommission().getOrDefault(airType,
						ruleCriteria.getCommission().get(AirType.ALL));
				if (component != null) {
					priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
						if (CommercialUtils.isAirRuleApplicable(segmentInfo, component, paxType)) {
							CommercialType type = CommercialType
									.valueOf(com.tgs.services.cms.datamodel.commission.CommercialType.IATA.getName());
							Double amount = type.getCommission(component.getExpression(), fareDetail);
							segmentInfo.getPriceInfo(0).getMiscInfo().setIata(amount);
							segmentInfo.getPriceInfo(0).getMiscInfo().getRuleIdMap()
								.put(AirRules.IATA, airCommissionRule.getId());
						}
					});
				}
			}
		}
	}

}
