package com.tgs.services.cms.servicehandler;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.FieldTransform;
import com.tgs.services.cms.datamodel.creditcard.CreditCardFilter;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.cms.dbmodel.DbCreditCardInfo;
import com.tgs.services.cms.jparepository.creditcard.CreditCardInfoService;
import com.tgs.services.cms.restmodel.creditcard.CreditCardRequest;
import com.tgs.services.cms.restmodel.creditcard.CreditCardResponse;
import com.tgs.utils.common.UpdateMaskedField;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CreditCardInfoHandler extends ServiceHandler<CreditCardRequest, CreditCardResponse> {

	@Autowired
	CreditCardInfoService cardInfoService;

	@Override
	public void beforeProcess() throws Exception {
		DbCreditCardInfo creditCardInfo = null;
		if (Objects.nonNull(request.getCardInfo()) && Objects.nonNull(request.getCardInfo().getId())) {
			creditCardInfo = cardInfoService.findById(request.getCardInfo().getId());
		}
		try {
			UpdateMaskedField.update(request.getCardInfo());
			creditCardInfo = Optional.ofNullable(creditCardInfo).orElseGet(() -> new DbCreditCardInfo())
					.from(request.getCardInfo());
			creditCardInfo.setProcessedOn(LocalDateTime.now());
			creditCardInfo = cardInfoService.save(creditCardInfo);
			request.getCardInfo().setId(creditCardInfo.getId());
		} catch (Exception e) {
			log.error("Unable to save the credit card info ", e);
			throw new CustomGeneralException("Credit Card Save Or Update Failed ");
		}
	}

	@Override
	public void process() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {
		FieldTransform.mask(request.getCardInfo());
		response.getCardInfoList().add(request.getCardInfo());
	}

	public CreditCardResponse getCreditCardList(CreditCardFilter cardFilter) {
		cardFilter.setDeleted(false);
		CreditCardResponse cardResponse = new CreditCardResponse();
		cardResponse.getCardInfoList().addAll(DbCreditCardInfo.toDomainList(cardInfoService.findAll(cardFilter)));
		cardResponse.getCardInfoList().forEach(cardInfo -> {
			FieldTransform.mask(cardInfo);
		});
		return cardResponse;
	}

	public CreditCardInfo getCard(CreditCardFilter filter) {
		DbCreditCardInfo creditCardInfo = cardInfoService.findAll(filter).get(0);
		return creditCardInfo.toDomain();
	}

	public BaseResponse deleteCreditCard(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbCreditCardInfo creditCardInfo = cardInfoService.findById(id);
		if (Objects.nonNull(creditCardInfo)) {
			creditCardInfo.setDeleted(Boolean.TRUE);
			creditCardInfo.setEnabled(Boolean.FALSE);
			cardInfoService.save(creditCardInfo);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updateCreditCardStatus(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbCreditCardInfo creditCardInfo = cardInfoService.findById(id);
		if (Objects.nonNull(creditCardInfo)) {
			creditCardInfo.setEnabled(status);
			cardInfoService.save(creditCardInfo);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}
}
