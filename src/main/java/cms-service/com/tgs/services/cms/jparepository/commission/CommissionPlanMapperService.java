
package com.tgs.services.cms.jparepository.commission;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.tgs.services.cms.datamodel.commission.CommissionPlan;
import com.tgs.services.cms.dbmodel.CommissionPlanMapper;
import com.tgs.services.cms.dbmodel.DbCommissionPlan;
import com.tgs.services.cms.helper.CommissionHelper;

@Service
public class CommissionPlanMapperService {

	@Autowired
	CommissionPlanMapperRepository mapperRepository;

	@Autowired
	CommissionHelper commHelper;

	public List<CommissionPlanMapper> findAll() {
		Sort sort = new Sort(Direction.DESC, Arrays.asList("processedOn"));
		return mapperRepository.findAll(sort);
	}

	public List<CommissionPlanMapper> save(List<CommissionPlanMapper> commissionPlanMapppers, CommissionPlan plan) {
		List<CommissionPlanMapper> savedMappers = new ArrayList<>(); 
		for (CommissionPlanMapper mapper : commissionPlanMapppers) {
			mapper.setProcessedOn(LocalDateTime.now());
			savedMappers.add(mapperRepository.save(mapper));
		}
		commHelper.storeInCache(commissionPlanMapppers, new DbCommissionPlan().from(plan));
		return savedMappers;
	}

	public CommissionPlanMapper findById(Long id) {
		return mapperRepository.findOne(id);
	}

	public Long deleteByCommPlanId(Integer commissionPlanId) {
		return mapperRepository.deleteByCommPlanId(commissionPlanId);
	}

	public List<CommissionPlanMapper> findByCommPlanID(Integer planId) {
		return mapperRepository.findBycommPlanIdOrderByProcessedOnDesc(planId);
	}

	public List<CommissionPlanMapper> findByCommissionRuleId(Integer id) {
		return mapperRepository.findByCommissionRuleId(id);
	}
}