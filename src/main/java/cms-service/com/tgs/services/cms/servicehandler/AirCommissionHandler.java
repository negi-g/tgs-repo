package com.tgs.services.cms.servicehandler;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionFilter;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionRule;
import com.tgs.services.cms.dbmodel.DBAirCommissionRule;
import com.tgs.services.cms.helper.AirCommissionHelper;
import com.tgs.services.cms.jparepository.commission.air.AirCommissionService;
import com.tgs.services.cms.restmodel.commission.air.AirCommissionResponse;


@Service
public class AirCommissionHandler extends ServiceHandler<AirCommissionRule, AirCommissionResponse> {

	@Autowired
	AirCommissionService airCommissionService;

	@Autowired
	AirCommissionHelper commHelper;
	
	@Autowired
	SyncService syncService;

	@Override
	public void beforeProcess() throws Exception {
		DBAirCommissionRule dbAirCommissionRule = null;
		if (Objects.nonNull(request) && Objects.nonNull(request.getId())) {
			dbAirCommissionRule = airCommissionService.findById(request.getId());
		}
		try {
			dbAirCommissionRule = Optional.ofNullable(dbAirCommissionRule).orElse(new DBAirCommissionRule())
					.from(request);
			dbAirCommissionRule = airCommissionService.save(dbAirCommissionRule);
			request.setId(dbAirCommissionRule.getId());
			syncService.sync("cms", dbAirCommissionRule.toDomain());
		} catch (Exception e) {
			throw new Exception("Air Commission Save/Update Failed {}", e);
		}

		/**
		 * Saves rule in cache. No need to reload cache.
		 */
		commHelper.process();
	}

	@Override
	public void process() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {
		response.getAirCommissionRules().add(request);
	}

	public AirCommissionResponse getAirCommissionList(AirCommissionFilter airCommissionFilter) {
		AirCommissionResponse airCommissionResponse = new AirCommissionResponse();
		List<DBAirCommissionRule> dbAirCommissionRuleList = airCommissionService.findAll(airCommissionFilter);
		airCommissionResponse.setAirCommissionRules(DBAirCommissionRule.toDomainList(dbAirCommissionRuleList));
		return airCommissionResponse;
	}

	public BaseResponse deleteAirCommissionRule(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DBAirCommissionRule dbAirCommissionRule = airCommissionService.findById(id);
		if (Objects.nonNull(dbAirCommissionRule)) {
			dbAirCommissionRule.setDeleted(true);
			dbAirCommissionRule.setEnabled(false);
			airCommissionService.save(dbAirCommissionRule);
			syncService.sync("cms", dbAirCommissionRule.toDomain(), "DELETE");
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updateAirCommissionRuleStatus(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DBAirCommissionRule dbAirCommissionRule = airCommissionService.findById(id);
		if (Objects.nonNull(dbAirCommissionRule)) {
			dbAirCommissionRule.setEnabled(status);
			airCommissionService.save(dbAirCommissionRule);
			syncService.sync("cms", null);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

}
