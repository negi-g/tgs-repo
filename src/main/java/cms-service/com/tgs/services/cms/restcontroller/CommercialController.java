package com.tgs.services.cms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.cms.jparepository.commission.CommissionPlanService;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/cms/v1")
@CustomRequestProcessor(areaRole = AreaRole.CMS_SERVICE,
		includedRoles = {UserRole.ADMIN, UserRole.SUPERVISOR, UserRole.CALLCENTER, UserRole.SALES},
		emulatedAllowedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
public class CommercialController {

	@Autowired
	CommissionPlanService planService;

	@RequestMapping(value = "/commission/plan/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updatePlanStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		com.tgs.services.cms.dbmodel.DbCommissionPlan plan = planService.findById(id);
		if (plan != null) {
			plan.setEnabled(BooleanUtils.isTrue(status));
			planService.save(plan);
		} else {
			throw new CustomGeneralException(SystemError.INVALID_PLAN_ID);
		}
		return baseResponse;
	}

}
