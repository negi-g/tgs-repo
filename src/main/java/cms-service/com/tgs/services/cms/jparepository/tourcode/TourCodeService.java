package com.tgs.services.cms.jparepository.tourcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.tgs.services.cms.datamodel.tourcode.TourCodeFilter;
import com.tgs.services.cms.dbmodel.DbTourCode;
import com.tgs.services.cms.helper.TourCodeHelper;
import com.tgs.utils.springframework.data.SpringDataUtils;

@Service
public class TourCodeService {

	@Autowired
	TourCodeRepository tourCodeRepository;

	@Autowired
	TourCodeHelper tcHelper;

	public List<DbTourCode> findAll() {
		Sort sort = new Sort(Direction.DESC, Arrays.asList("airline", "priority", "supplierid", "processedOn"));
		return tourCodeRepository.findAll();
	}

	public DbTourCode save(DbTourCode tourCode) {
		tourCode.cleanData();
		DbTourCode tc = tourCodeRepository.save(tourCode);
		tcHelper.process();
		return tc;
	}

	public DbTourCode findById(Long id) {
		return tourCodeRepository.findOne(id);
	}

	public List<DbTourCode> findAllByEnabled(Boolean enabled) {
		return tourCodeRepository.findByEnabled(enabled);
	}

	public List<DbTourCode> findAll(TourCodeFilter queryFilter) {
		PageRequest request = SpringDataUtils.getPageRequestFromFilter(queryFilter);
		Specification<DbTourCode> specification = new Specification<DbTourCode>() {
			@Override
			public Predicate toPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				predicates = (TourCodeSearchPredicate.getPredicateListBasedOnTourCodeFilter(root, query, cb,
						queryFilter));
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};

		return tourCodeRepository.findAll(specification, request).getContent();
	}
}