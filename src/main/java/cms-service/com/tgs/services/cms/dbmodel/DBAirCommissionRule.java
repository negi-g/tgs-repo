package com.tgs.services.cms.dbmodel;

import java.time.LocalDateTime;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.runtime.database.CustomTypes.AirCommercialRuleCriteriaType;
import com.tgs.services.base.runtime.database.CustomTypes.FlightBasicRuleCriteriaType;
import com.tgs.services.cms.datamodel.commission.air.AirCommercialRuleCriteria;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionRule;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@TypeDefs({ @TypeDef(name = "FlightBasicRuleCriteriaType", typeClass = FlightBasicRuleCriteriaType.class),
		@TypeDef(name = "AirCommercialRuleCriteriaType", typeClass = AirCommercialRuleCriteriaType.class) })
@Table(name = "aircommissionrule")
@Audited
public class DBAirCommissionRule extends BaseModel<DBAirCommissionRule, AirCommissionRule> {

	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private boolean enabled;

	@Column
	private double priority;

	@Column
	@Nonnull
	private String airline;

	@Nonnull
	private String supplierId;

	@Column
	@Nonnull
	@Type(type = "FlightBasicRuleCriteriaType")
	private FlightBasicRuleCriteria inclusionCriteria;

	@Column
	@Type(type = "FlightBasicRuleCriteriaType")
	private FlightBasicRuleCriteria exclusionCriteria;

	@Column
	@Type(type = "AirCommercialRuleCriteriaType")
	private AirCommercialRuleCriteria commissionCriteria;

	@Column
	private boolean isDeleted;

	@Column
	private String description;

	public void cleanData() {
		if (inclusionCriteria != null)
			inclusionCriteria.cleanData();
		if (exclusionCriteria != null)
			exclusionCriteria.cleanData();
	}

	@Override
	public DBAirCommissionRule from(AirCommissionRule dataModel) {
		return new GsonMapper<>(dataModel, this, DBAirCommissionRule.class).convert();
	}
	
	@Override
	public AirCommissionRule toDomain() {
		return new GsonMapper<>(this, AirCommissionRule.class).convert();
	}
}
