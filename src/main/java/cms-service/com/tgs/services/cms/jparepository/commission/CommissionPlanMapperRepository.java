package com.tgs.services.cms.jparepository.commission;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.services.cms.dbmodel.CommissionPlanMapper;

@Repository
public interface CommissionPlanMapperRepository
		extends JpaRepository<CommissionPlanMapper, Long>, JpaSpecificationExecutor<CommissionPlanMapper> {

	public List<CommissionPlanMapper> findBycommPlanIdOrderByProcessedOnDesc(Integer commPlanId);

	public List<CommissionPlanMapper> findByCommissionRuleId(Integer commissionRuleId);

	@Transactional
	Long deleteByCommPlanId(Integer commissionPlanId);
}