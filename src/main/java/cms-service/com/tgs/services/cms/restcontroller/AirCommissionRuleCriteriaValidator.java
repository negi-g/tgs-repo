package com.tgs.services.cms.restcontroller;

import java.util.Objects;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.cms.datamodel.commission.air.AirCommercialRuleCriteria;

@Service
public class AirCommissionRuleCriteriaValidator {

	public void validateCriteria(Errors errors, String fieldName, AirCommercialRuleCriteria commissionCriteria) {
		if (commissionCriteria == null) {
			return;
		}

		if (MapUtils.isEmpty(commissionCriteria.getCommission())) {
			errors.rejectValue(fieldName + ".commission", SystemError.NULL_VALUE.errorCode(),
					SystemError.NULL_VALUE.getMessage());
		} else {

			commissionCriteria.getCommission().forEach((airType, airCommercialComponent) -> {
				if (Objects.isNull(airCommercialComponent)) {
					errors.rejectValue(fieldName + ".commission", SystemError.NULL_VALUE.errorCode(),
							SystemError.NULL_VALUE.getMessage());
				} else {
					if (StringUtils.isBlank(airCommercialComponent.getExpression())) {
						errors.rejectValue(fieldName + ".commission", SystemError.NULL_VALUE.errorCode(),
								SystemError.NULL_VALUE.getMessage());
					}
					if (airCommercialComponent.getThresholdAmount() != null
							&& airCommercialComponent.getThresholdAmount().doubleValue() < 0) {
						errors.rejectValue(fieldName + ".commission", SystemError.NEGATIVE_VALUE.errorCode(),
								SystemError.NEGATIVE_VALUE.getMessage());
					}
				}
			});
		}
	}
}
