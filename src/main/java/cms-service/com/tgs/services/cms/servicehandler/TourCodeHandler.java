package com.tgs.services.cms.servicehandler;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.cms.datamodel.tourcode.TourCode;
import com.tgs.services.cms.dbmodel.DbTourCode;
import com.tgs.services.cms.jparepository.tourcode.TourCodeService;
import com.tgs.services.cms.restmodel.tourcode.TourCodeListRequest;
import com.tgs.services.cms.restmodel.tourcode.TourCodeRequest;
import com.tgs.services.cms.restmodel.tourcode.TourCodeResponse;


@Service
public class TourCodeHandler extends ServiceHandler<TourCodeRequest, TourCodeResponse> {

	@Autowired
	TourCodeService tourCodeService;

	@Override
	public void beforeProcess() throws Exception {
		DbTourCode tourCode = null;
		if (Objects.nonNull(request.getTourCode()) && Objects.nonNull(request.getTourCode().getId())) {
			tourCode = tourCodeService.findById(request.getTourCode().getId());
		}
		try {
			tourCode = Optional.ofNullable(tourCode).orElseGet(() -> new DbTourCode());
			tourCode = tourCodeService.save(tourCode.from(request.getTourCode()));
			request.getTourCode().setId(tourCode.getId());
		} catch (Exception e) {
			throw new Exception("Commission Save Or Update Failed {}", e);
		}
	}

	@Override
	public void process() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {
		response.getTourCodeList().add(request.getTourCode());
	}

	public TourCodeResponse getTourCodeList(TourCodeListRequest tourCodeRequest) {
		TourCodeResponse tourCodeListResponse = new TourCodeResponse();
		if (tourCodeRequest.getTourCodeFilter() != null) {
			List<TourCode> tourCodeList = DbTourCode
					.toDomainList(tourCodeService.findAll(tourCodeRequest.getTourCodeFilter()));
			tourCodeListResponse.getTourCodeList().addAll(tourCodeList);
		}
		return tourCodeListResponse;
	}

	public BaseResponse deleteTourCode(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbTourCode tourCode = tourCodeService.findById(id);
		if (tourCode != null) {
			tourCode.setDeleted(Boolean.TRUE);
			tourCode.setEnabled(Boolean.FALSE);
			tourCodeService.save(tourCode);
			return baseResponse;
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
			baseResponse.getStatus().setSuccess(false);
		}
		return baseResponse;
	}

	public BaseResponse updateTourCodeStatus(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbTourCode tourCode = tourCodeService.findById(id);
		if (tourCode != null) {
			tourCode.setEnabled(status);
			tourCodeService.save(tourCode);
			return baseResponse;
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
			baseResponse.getStatus().setSuccess(false);
		}
		return baseResponse;
	}
}
