package com.tgs.services.cms.jparepository.commission.air;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.tgs.services.cms.datamodel.commission.air.AirCommissionFilter;
import com.tgs.services.cms.dbmodel.DBAirCommissionRule;
import com.tgs.utils.springframework.data.SpringDataUtils;

public enum AirCommissionSearchPredicate {
	CREATED_ON {
		@Override
		public void addPredicate(Root<DBAirCommissionRule> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirCommissionFilter filter, List<Predicate> predicates) {
			SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
		}
	},
	ENABLED {
		@Override
		public void addPredicate(Root<DBAirCommissionRule> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirCommissionFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getEnabled()))
				predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));
		}
	},
	PRIORITY {
		@Override
		public void addPredicate(Root<DBAirCommissionRule> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirCommissionFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getPriority()))
				predicates.add(criteriaBuilder.equal(root.get("priority"), filter.getPriority()));
		}
	},
	SUPPLIERID {
		@Override
		public void addPredicate(Root<DBAirCommissionRule> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirCommissionFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotEmpty(filter.getSupplierId()))
				predicates.add(criteriaBuilder.equal(root.get("supplierId"), filter.getSupplierId()));
		}
	},
	SOURCEID {
		@Override
		public void addPredicate(Root<DBAirCommissionRule> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirCommissionFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotEmpty(filter.getSourceId()))
				predicates.add(criteriaBuilder.equal(root.get("sourceId"), filter.getSourceId()));
		}
	},
	ISDELETED {
		@Override
		public void addPredicate(Root<DBAirCommissionRule> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirCommissionFilter filter, List<Predicate> predicates) {
			predicates.add(criteriaBuilder.equal(root.get("isDeleted"), filter.isDeleted()));
		}
	},
	AIRLINE {
		@Override
		public void addPredicate(Root<DBAirCommissionRule> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirCommissionFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotEmpty(filter.getAirline()))
				predicates.add(criteriaBuilder.equal(root.get("airline"), filter.getAirline()));
		}
	};

	public abstract void addPredicate(Root<DBAirCommissionRule> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, AirCommissionFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnAirCommissionFilter(Root<DBAirCommissionRule> root,
			CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, AirCommissionFilter filter) {
		List<Predicate> predicates = new ArrayList<>();
		for (AirCommissionSearchPredicate ruleSearchPredicate : AirCommissionSearchPredicate.values()) {
			ruleSearchPredicate.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}
