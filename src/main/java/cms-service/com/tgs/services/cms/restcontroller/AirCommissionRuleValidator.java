package com.tgs.services.cms.restcontroller;

import java.util.Objects;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.validator.rule.FlightBasicRuleCriteriaValidator;
import com.tgs.services.cms.datamodel.commission.air.AirCommercialRuleCriteria;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionRule;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

@Service
public class AirCommissionRuleValidator implements Validator {

	@Autowired
	FlightBasicRuleCriteriaValidator flightBasicRuleCriteriaValidator;

	@Autowired
	AirCommissionRuleCriteriaValidator criteriaValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}

		if (target instanceof AirCommissionRule) {

			AirCommissionRule rule = (AirCommissionRule) target;

			if (StringUtils.isBlank(rule.getAirline()))
				errors.rejectValue("airline", SystemError.NULL_VALUE.errorCode(), SystemError.NULL_VALUE.getMessage());

			FlightBasicRuleCriteria inclusionCriteria = (FlightBasicRuleCriteria) rule.getInclusionCriteria(),
					exclusionCriteria = (FlightBasicRuleCriteria) rule.getExclusionCriteria();

			flightBasicRuleCriteriaValidator.validateCriteria(errors, "inclusionCriteria", inclusionCriteria);
			flightBasicRuleCriteriaValidator.validateCriteria(errors, "exclusionCriteria", exclusionCriteria);

			AirCommercialRuleCriteria commissionCriteria = (AirCommercialRuleCriteria) rule.getOutput();
			criteriaValidator.validateCriteria(errors, "commissionCriteria", commissionCriteria);


		}

	}


}
