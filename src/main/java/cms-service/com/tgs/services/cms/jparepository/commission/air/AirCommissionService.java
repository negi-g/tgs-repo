package com.tgs.services.cms.jparepository.commission.air;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionFilter;
import com.tgs.services.cms.dbmodel.DBAirCommissionRule;
import com.tgs.services.cms.helper.AirCommissionHelper;

@Service
public class AirCommissionService {

	@Autowired
	AirCommissionRepository airCommissionRepository;

	@Autowired
	AirCommissionHelper acHelper;

	public List<DBAirCommissionRule> findAllByEnabled(Boolean enabled) {
		return airCommissionRepository.findByEnabledOrderByProcessedOnDesc(enabled);
	}

	public DBAirCommissionRule findById(Long id) {
		return airCommissionRepository.findOne(id);
	}

	public List<DBAirCommissionRule> findAll() {
		Sort sort = new Sort(Direction.DESC, Arrays.asList("processedOn"));
		return airCommissionRepository.findAll(sort);
	}

	public DBAirCommissionRule save(DBAirCommissionRule dbAirCommissionRule) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			dbAirCommissionRule.cleanData();
			dbAirCommissionRule.setProcessedOn(LocalDateTime.now());
			dbAirCommissionRule = airCommissionRepository.saveAndFlush(dbAirCommissionRule);
		}
		AirCommissionHelper.cacheAirCommissionRule(dbAirCommissionRule.toDomain());
		return dbAirCommissionRule;
	}

	public List<DBAirCommissionRule> findAll(AirCommissionFilter queryFilter) {
		Specification<DBAirCommissionRule> specification = new Specification<DBAirCommissionRule>() {
			@Override
			public Predicate toPredicate(Root<DBAirCommissionRule> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				predicates = (AirCommissionSearchPredicate.getPredicateListBasedOnAirCommissionFilter(root, query, cb,
						queryFilter));
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};

		Sort sort = null;
		if (CollectionUtils.isNotEmpty(queryFilter.getSortByAttr())) {
			Direction direction = Direction.fromString(queryFilter.getSortByAttr().get(0).getOrderBy());
			sort = new Sort(direction, queryFilter.getSortByAttr().get(0).getParams());
		} else {
			sort = new Sort(Direction.DESC, Arrays.asList("processedOn"));
		}
		return airCommissionRepository.findAll(specification, sort);
	}
}
