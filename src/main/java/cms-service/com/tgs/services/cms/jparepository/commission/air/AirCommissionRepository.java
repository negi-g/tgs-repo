package com.tgs.services.cms.jparepository.commission.air;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.cms.dbmodel.DBAirCommissionRule;

@Repository
public interface AirCommissionRepository extends JpaRepository<DBAirCommissionRule, Long>, JpaSpecificationExecutor<DBAirCommissionRule> {

	public List<DBAirCommissionRule> findByEnabled(boolean enabled);

	public List<DBAirCommissionRule> findByEnabledOrderByProcessedOnDesc(Boolean enabled);

}
