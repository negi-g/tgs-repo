package com.tgs.services.cms.jparepository.commission;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.cms.dbmodel.DbCommissionPlan;

@Repository
public interface CommissionPlanRepository
		extends JpaRepository<DbCommissionPlan, Long>, JpaSpecificationExecutor<DbCommissionPlan> {

	public DbCommissionPlan findByIdOrName(Long id,String name);

}