package com.tgs.services.cms.dbmodel;


import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.cms.datamodel.commission.CommissionPlan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Getter
@Setter
@Table(name = "commissionplan")
@Audited
public class DbCommissionPlan extends BaseModel<DbCommissionPlan, CommissionPlan> {

    @CreationTimestamp
    private LocalDateTime createdOn;
    
    @CreationTimestamp
	private LocalDateTime processedOn;

    @Column
    private String product;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private boolean enabled;

    @Override
    public CommissionPlan toDomain() {
        return new GsonMapper<>(this, CommissionPlan.class).convert();
    }

    public static DbCommissionPlan create(CommissionPlan dataModel) {
        return new DbCommissionPlan().from(dataModel);
    }

    public DbCommissionPlan from(CommissionPlan dataModel) {
        return new GsonMapper<>(dataModel, this, DbCommissionPlan.class).convert();
    }

}