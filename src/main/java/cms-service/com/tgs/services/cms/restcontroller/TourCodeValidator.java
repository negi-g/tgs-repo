package com.tgs.services.cms.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.validator.rule.FlightBasicRuleCriteriaValidator;
import com.tgs.services.cms.datamodel.tourcode.TourCode;
import com.tgs.services.cms.restmodel.tourcode.TourCodeRequest;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

@Service
public class TourCodeValidator implements Validator {

	@Autowired
	FlightBasicRuleCriteriaValidator criteriaValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}

		if (target instanceof TourCodeRequest) {
			TourCode rule = ((TourCodeRequest) target).getTourCode();
			if (rule == null) {
				rejectValue(errors, "tourCode", SystemError.NULL_RULE);
			} else {
				criteriaValidator.validateCriteria(errors, "tourCode.inclusionCriteria",
						(FlightBasicRuleCriteria) rule.getInclusionCriteria());
				criteriaValidator.validateCriteria(errors, "tourCode.exclusionCriteria",
						(FlightBasicRuleCriteria) rule.getExclusionCriteria());
			}
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
