package com.tgs.services.cms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.envers.Audited;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.cms.datamodel.commission.CommissionRule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Getter
@Setter
@Audited
@Table(name = "commissionrule")
public class DbCommissionRule extends BaseModel<DbCommissionRule, CommissionRule> {

	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	@NonNull
	private String product;

	@Column
	private boolean enabled;

	@Column
	private String code;

	@Column
	private String airType;

	@Column
	private String searchType;

	@Column
	private Double priority;

	@Column
	@JsonAdapter(JsonStringSerializer.class)
	private String inclusionCriteria;

	@Column
	@JsonAdapter(JsonStringSerializer.class)
	private String exclusionCriteria;

	@Column
	@JsonAdapter(JsonStringSerializer.class)
	private String commercialCriteria;

	@Column
	private boolean isDeleted;

	@Column
	private String description;

	@Override
	public CommissionRule toDomain() {
		return new GsonMapper<>(this, CommissionRule.class, true).convert();
	}

	@Override
	public DbCommissionRule from(CommissionRule dataModel) {
		return new GsonMapper<>(dataModel, this, DbCommissionRule.class).convert();
	}

}