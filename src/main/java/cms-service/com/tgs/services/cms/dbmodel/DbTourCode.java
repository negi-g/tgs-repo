package com.tgs.services.cms.dbmodel;

import java.time.LocalDateTime;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.runtime.database.CustomTypes.AirCommercialComponentType;
import com.tgs.services.base.runtime.database.CustomTypes.FlightBasicRuleCriteriaType;
import com.tgs.services.cms.datamodel.commission.air.AirCommercialComponent;
import com.tgs.services.cms.datamodel.tourcode.TourCode;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Audited
@TypeDefs({ @TypeDef(name = "FlightBasicRuleCriteriaType", typeClass = FlightBasicRuleCriteriaType.class),
		@TypeDef(name = "AirCommercialComponentType", typeClass = AirCommercialComponentType.class) })
@Table(name="tourcode")
public class DbTourCode extends BaseModel<DbTourCode, TourCode> {

	@CreationTimestamp
	@Column(name = "createdon")
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private boolean enabled;

	@Column
	private String airType;

	@Column
	private String searchType;

	@Column
	@Nonnull
	private String supplierId;

	@Column
	@Nonnull
	private String sourceId;

	@Column
	private String airline;

	@Column
	private double priority;

	@Column
	@Nonnull
	private String tourCode;

	@Column
	@Nonnull
	@Type(type = "FlightBasicRuleCriteriaType")
	private FlightBasicRuleCriteria inclusionCriteria;

	@Column
	@Type(type = "FlightBasicRuleCriteriaType")
	private FlightBasicRuleCriteria exclusionCriteria;

	@Column
	@Type(type = "AirCommercialComponentType")
	private AirCommercialComponent tourCodeCriteria;

	@Column(columnDefinition = "boolean default false")
	private boolean isDeleted;

	@Column
	private String description;

	public void cleanData() {
		if (inclusionCriteria != null)
			inclusionCriteria.cleanData();
		if (exclusionCriteria != null)
			exclusionCriteria.cleanData();
	}
	
	@Override
	public TourCode toDomain() {
		return new GsonMapper<>(this, TourCode.class).convert();
	}

	@Override
	public DbTourCode from(TourCode dataModel) {
		return new GsonMapper<>(dataModel, this, DbTourCode.class).convert();
	}
}