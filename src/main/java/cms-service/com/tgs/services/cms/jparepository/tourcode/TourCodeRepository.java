package com.tgs.services.cms.jparepository.tourcode;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.cms.dbmodel.DbTourCode;

@Repository
public interface TourCodeRepository extends JpaRepository<DbTourCode, Long>, JpaSpecificationExecutor<DbTourCode> {

    public List<DbTourCode> findByEnabled(Boolean enabled);
}