package com.tgs.services.cms.jparepository.creditcard;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.tgs.services.cms.datamodel.creditcard.CreditCardFilter;
import com.tgs.services.cms.dbmodel.DbCreditCardInfo;
import com.tgs.services.cms.helper.CreditCardHelper;
import com.tgs.utils.springframework.data.SpringDataUtils;

@Service
public class CreditCardInfoService {

	@Autowired
	CreditCardInfoRepository cardInfoRepository;

	@Autowired
	CreditCardHelper helper;

	public DbCreditCardInfo save(DbCreditCardInfo creditCardInfo) {
		DbCreditCardInfo cI = cardInfoRepository.saveAndFlush(creditCardInfo);
		/**
		 * To handle the case when supplierId is updated, reload whole credit card data in cache. supplierId is primary 
		 * key in CreditCardHelper.
		 */
		helper.process();
//		helper.refreshCache(cI.getSupplierId());
		return cI;
	}

	public DbCreditCardInfo findById(Long id) {
		return cardInfoRepository.findOne(id);
	}

	public List<DbCreditCardInfo> findAll(CreditCardFilter queryFilter) {
		PageRequest request = SpringDataUtils.getPageRequestFromFilter(queryFilter);
		Specification<DbCreditCardInfo> specification = new Specification<DbCreditCardInfo>() {
			@Override
			public Predicate toPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				predicates = (CreditCardSearchPredicate.getPredicateListBasedOnCreditCardFilter(root, query, cb,
						queryFilter));
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};

		return cardInfoRepository.findAll(specification, request).getContent();
	}

}