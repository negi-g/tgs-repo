package com.tgs.services.cms.jparepository.commission;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.cms.datamodel.commission.CommissionFilter;
import com.tgs.services.cms.dbmodel.DbCommissionRule;
import com.tgs.services.cms.helper.CommissionHelper;
import com.tgs.utils.springframework.data.SpringDataUtils;

@Service
public class CommissionRuleService {

	@Autowired
	CommissionRuleRepository ruleRepository;

	@Autowired
	CommissionHelper cl;

	public List<DbCommissionRule> findAll() {
		Sort sort = new Sort(Direction.DESC, Arrays.asList("processedOn"));
		return ruleRepository.findAll(sort);
	}

	public DbCommissionRule save(DbCommissionRule commissionRule) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			commissionRule.setProcessedOn(LocalDateTime.now());
			commissionRule = ruleRepository.saveAndFlush(commissionRule);
		}
		cl.updateCache(commissionRule.getId());
		return commissionRule;
	}

	public List<DbCommissionRule> findAll(CommissionFilter filter) {
		Specification<DbCommissionRule> specfication = new Specification<DbCommissionRule>() {

			@Override
			public Predicate toPredicate(Root<DbCommissionRule> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (filter.getRuleIds() != null) {
					Expression<Integer> exp = root.get("id");
					predicates.add(exp.in(filter.getRuleIds()));
				}

				if (filter.getDeleted() != null) {
					predicates.add(criteriaBuilder.equal(root.get("isDeleted"), filter.getDeleted()));
				}

				if (filter.getAirType() != null && !filter.getAirType().equals(AirType.ALL)) {
					Predicate airtypePredicate = criteriaBuilder.or(root.get("airType").isNull(), root.get("airType")
							.in(Arrays.asList(filter.getAirType().getName(), AirType.ALL.getName())));
					predicates.add(airtypePredicate);
				}

				if (filter.getEnabled() != null) {
					predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));
				}

				if (filter.getProduct() != null) {
					predicates.add(
							criteriaBuilder.equal(root.get("product"), Product.valueOf(filter.getProduct()).getCode()));
				}

				if (!TgsCollectionUtils.isEmptyStringCollection(filter.getCodes())) {
					predicates.add(root.get("code").in(filter.getCodes()));
				}

				SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
		Sort sort = null;
		if (CollectionUtils.isNotEmpty(filter.getSortByAttr())) {
			Direction direction = Direction.fromString(filter.getSortByAttr().get(0).getOrderBy());
			sort = new Sort(direction, filter.getSortByAttr().get(0).getParams());
		} else {
			sort = new Sort(Direction.DESC, Arrays.asList("code", "priority", "processedOn"));
		}

		return ruleRepository.findAll(specfication, sort);
	}

	public DbCommissionRule findByIdAndEnabled(int id) {
		return ruleRepository.findByIdAndEnabled(Long.valueOf(id), Boolean.TRUE);
	}

	public DbCommissionRule findById(long id) {
		return ruleRepository.findOne(id);
	}

	public List<DbCommissionRule> findAllByEnabled(Boolean enabled) {
		return ruleRepository.findByEnabledOrderByProcessedOnDesc(enabled);
	}
}