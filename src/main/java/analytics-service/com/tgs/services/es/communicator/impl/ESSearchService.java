package com.tgs.services.es.communicator.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;
import com.tgs.services.es.utils.ESUtils;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.es.servicehandler.ESServiceHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ESSearchService extends ESServiceHandler {

	public List<Map<String, ? extends Object>> searchDocument(@NotNull String source, @NotNull ESMetaInfo metaInfo) {
		List<Map<String, ? extends Object>> output = new ArrayList<>();
		SearchSourceBuilder sourceBuilder = SearchSourceBuilder.searchSource();
		BoolQueryBuilder builder = QueryBuilders.boolQuery();
		builder.should().addAll(metaInfo.getQuerybuilder(source));
		sourceBuilder.query(builder);
		sourceBuilder.size(metaInfo.documentSize());
		metaInfo.addSortingFields(sourceBuilder);
		sourceBuilder.postFilter(metaInfo.postFilter());
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.source(sourceBuilder);
		if (!metaInfo.equals(ESMetaInfo.ALL)) {
			searchRequest.indices(ESUtils.getIndex(metaInfo, getSetPrefix()));
		}
		try {
			SearchResponse searchResponse = client.search(searchRequest);
			SearchHit[] searchHits = searchResponse.getHits().getHits();
			Arrays.stream(searchHits).forEach(searchHit -> {
				output.add(searchHit.getSourceAsMap());
			});
		} catch (Exception e) {
			log.error("ES Unable to search data source {}", source, e);
		}
		return output;
	}


	public ActionListener<SearchResponse> searchActionResponseListener() {
		ActionListener<SearchResponse> bulkResponseActionListener = new ActionListener<SearchResponse>() {

			@Override
			public void onResponse(SearchResponse searchResponse) {
				if (searchResponse.status().getStatus() == 200) {
					SearchHit[] searchHits = searchResponse.getHits().getHits();
					for (SearchHit searchHit : searchHits) {
						// output.add(searchHit.getSourceAsMap());
					}
				}
			}

			@Override
			public void onFailure(Exception e) {
				log.error("Data Searching Failed ", e);
			}
		};
		return bulkResponseActionListener;
	}

}
