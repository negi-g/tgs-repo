package com.tgs.services.es.helper;

import java.io.IOException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ESStackInitializer;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.es.communicator.impl.ESDeleteService;
import com.tgs.services.es.communicator.impl.ESIndexService;
import com.tgs.services.es.communicator.impl.ESSearchService;
import com.tgs.services.es.datamodel.ESMetaInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ElasticSearchHelper {

	@Autowired
	ESIndexService indexService;

	@Autowired
	ESDeleteService deleteService;

	@Autowired
	ESSearchService searchService;

	@Autowired
	ApplicationContext context;

	@Value("${setPrefix}")
	private String setPrefix;

	@Autowired
	GeneralCachingCommunicator cachingService;

	@Autowired
	Environment environment;

	private static final String ES_STATIC_MAP_KEY = "ES_STATIC_MAP";
	private static final int TTL_SECONDS = 3600;

	public void addDocument(@NotNull Object input, @NotNull ESMetaInfo metaInfo) {
		indexService.addDocument(input, metaInfo);
	}

	@Deprecated
	public void addBulkDocument(@NotNull List<? extends Object> input, @NotNull ESMetaInfo metaInfo) {
		indexService.addDocumentsInSingleClient(input, metaInfo);
	}

	public void addBulkDocuments(@NotNull List<? extends Object> input, @NotNull ESMetaInfo metaInfo) {
		indexService.addBulkDocuments(input, metaInfo);
	}

	public void addSupplierLogs(@NotEmpty List<LogData> logDatas, @NotNull ESMetaInfo metaInfo) throws Exception {
		indexService.addSupplierLogs(logDatas, metaInfo);
	}

	public List<Map<String, ? extends Object>> searchDocument(@NotNull String source, @NotNull String esMetaInfo) {
		ESMetaInfo searchMetaInfo = ESMetaInfo.getESMetaInfo(esMetaInfo);
		return searchService.searchDocument(source, searchMetaInfo);
	}

	public void createIndex(ESMetaInfo esMetaInfo) throws IOException {
		indexService.createIndex(esMetaInfo);
	}

	public void deleteDocumentById(Object value, ESMetaInfo esMetaInfo) {
		Document<Object> document = new Document<Object>(value);
		deleteService.deleteDocument(document, esMetaInfo);
	}

	public void deleteIndex(ESMetaInfo esMetaInfo) {
		deleteService.deleteIndex(esMetaInfo);
	}

	@Scheduled(initialDelay = 5 * 1000, fixedDelay = 7 * 24 * 60 * 60 * 1000)
	public void reloadAll() throws Exception {
		log.info("Doing Auto Reload of ElasticSearch Static Data");
		doReload("load", "all", true);
	}

	public BaseResponse doReload(String operationType, String reloadType, boolean isLastReloadCheckRequired) {
		BaseResponse baseResponse = new BaseResponse();
		try {
			if (isLastReloadCheckRequired && isReloadAllowed()) {
				setLastReloadedAt();
			}
			Set<Class<? extends ESStackInitializer>> allIntializers = new HashSet<>();
			if ("all".equalsIgnoreCase(reloadType)) {
				allIntializers =
						TgsObjectUtils.findAllMatchingInheritedTypes(ESStackInitializer.class, "com.tgs.services");
			} else {
				allIntializers.add((Class<? extends ESStackInitializer>) Class.forName(reloadType));
			}
			reload(allIntializers);
		} catch (Exception e) {
			log.error("ElasticSearch static Reload Failed  for ", e);
			baseResponse.addError(
					ErrorDetail.builder().message("Elastic Search Reload Failed" + e.getMessage()).errCode("").build());
			baseResponse.getStatus().setSuccess(false);
		}
		return baseResponse;
	}

	public void reload(Set<Class<? extends ESStackInitializer>> allIntializers) {
		StopWatch stopWatch = new StopWatch();
		allIntializers.forEach(intializer -> {
			String className = intializer.getName();
			try {
				stopWatch.start();
				log.info("Intializing {}", className);
				Object bean = context.getBean(Class.forName(intializer.getName()));
				Method method = bean.getClass().getMethod("initialize", String.class);
				method.invoke(bean, intializer.getName());
				log.info("Total time took to intialize {} , is {} ", className, stopWatch.getTime());
			} catch (Exception e) {
				log.error("Unable to intialize {}", className, e);
			} finally {
				stopWatch.stop();
				stopWatch.reset();
			}
		});
	}

	private boolean isReloadAllowed() {
		LocalDateTime lastReloadTime = getLastReloadedAt();
		if (ObjectUtils.isEmpty(lastReloadTime)) {
			return true;
		} else {
			throw new CustomGeneralException("Last Reloaded At: " + lastReloadTime + ", Cache can be reloaded after "
					+ lastReloadTime.plusSeconds(TTL_SECONDS));
		}
	}

	private LocalDateTime getLastReloadedAt() {
		String helperGroup = getIntializerHelperGroupKey();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
				.set(CacheSetName.STATIC_RELOAD.getName()).keys(new String[] {helperGroup}).compress(false)
				.plainData(true).bins(new String[] {BinName.STOREAT.getName()}).build();

		Map<String, Map<String, String>> lastReloadedAt = cachingService.get(metaInfo, String.class);
		if (MapUtils.isEmpty(lastReloadedAt)) {
			return null;
		}
		for (String key : lastReloadedAt.keySet()) {
			Map<String, String> reloadInfo = lastReloadedAt.get(key);
			LocalDateTime lastProcessedAt = LocalDateTime.parse(reloadInfo.get(BinName.STOREAT.getName()));
			return lastProcessedAt;
		}
		return null;
	}

	public void setLastReloadedAt() {
		// 1 hour
		String helperGroup = getIntializerHelperGroupKey();
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.STOREAT.name(), LocalDateTime.now().toString());
		cachingService.store(
				CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
						.set(CacheSetName.STATIC_RELOAD.getName()).key(helperGroup).build(),
				binMap, false, true, TTL_SECONDS);
	}

	private String getIntializerHelperGroupKey() {
		//return org.apache.commons.lang3.ObjectUtils.firstNonNull(environment.getProperty("INITIALIZER_GROUP"),
		//		ES_STATIC_MAP_KEY);
		return ES_STATIC_MAP_KEY;
	}

}
