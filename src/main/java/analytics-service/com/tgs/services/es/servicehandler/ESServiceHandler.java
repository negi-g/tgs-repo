package com.tgs.services.es.servicehandler;

import java.io.IOException;

import javax.annotation.PreDestroy;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Service
public abstract class ESServiceHandler {

	@Autowired
	protected RestHighLevelClient client;

	@Value("${setPrefix}")
	private String setPrefix;

	@PreDestroy
	public void cleanup() {
		try {
			log.info("Closing the elasticsearch rest client");
			client.close();
		} catch (IOException ioe) {
			log.error("Some exception occur while closing elasticsearch rest client", ioe);
		}
	}

//    public void closeConnection(RestHighLevelClient client) {
//        esClient.closeConnection(client);
//    }
//
//    public RestHighLevelClient createConnection() throws IOException {
//        RestHighLevelClient client = esClient.createConnection();
//        try {
//            if (client != null && client.ping()) {
//                return client;
//            } else {
//                throw new IOException("Connection Refused");
//            }
//        } catch (IOException e) {
//        	e.printStackTrace();
//            log.error("Connection Not Established");
//            throw e;
//        }
//    }

}
