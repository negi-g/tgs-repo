package com.tgs.services.messagebroker.restcontroller;

import com.tgs.services.analytics.QueueData;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.messagebroker.servicehandler.KafkaHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RequestMapping("/analytics/v1")
@Slf4j
@RestController
public class AnalyticsController {

    @Autowired
    KafkaHandler kafkaHandler;

    @RequestMapping(value = "/kafka-produce", method = RequestMethod.POST)
    public BaseResponse addRecord(HttpServletRequest request, HttpServletResponse response
            , @RequestBody QueueData queueData) throws Exception {
        kafkaHandler.initData(queueData, new BaseResponse());
        return kafkaHandler.getResponse();
    }
}

