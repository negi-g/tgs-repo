package com.tgs.services.messagebroker.servicehandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KafkaHandler extends ServiceHandler<QueueData, BaseResponse> {

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@Autowired
	private KafkaAdmin kafkaAdmin;

	@Value("${setPrefix}")
	private String setPrefix;

	static Set<String> topics;

	static {
		topics = new HashSet<>();
	}

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {

		ListenableFuture<SendResult<String, String>> callbackResult = queue(QueueDataType.ELASTICSEARCH, request);

		callbackResult.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

			@Override
			public void onSuccess(SendResult<String, String> result) {
				log.info("Sent message=[" + request + "] with offset=[" + result.getRecordMetadata().offset() + "]");
			}

			@Override
			public void onFailure(Throwable ex) {
				log.error("Unable to send message=[" + request + "] due to : " + ex.getMessage());
			}
		});

	}

	@Override
	public void afterProcess() throws Exception {}

	public ListenableFuture<SendResult<String, String>> queue(QueueDataType queueType, QueueData queueData) {
		ListenableFuture<SendResult<String, String>> callbackResult = null;
		// queueData.key <- index
		queueData.setKey(StringUtils.join(setPrefix, "_", queueData.getKey()));
		if (isTopicExists(queueData.getKey())) {
			ProducerRecord record = new ProducerRecord(queueData.getKey(), 0, queueData.getKey(), queueData.getValue());
			callbackResult = kafkaTemplate.send(record);
		} else {
			log.error("Unable to add data in queue , topic not exists {}", queueData.getKey());
		}
		return callbackResult;
	}

	private boolean isTopicExists(String topic) {
		boolean isTopicExists = false;
		AdminClient client = null;
		try {

			if (topics.contains(topic)) {
				return true;
			}
			client = AdminClient.create(kafkaAdmin.getConfig());
			ListTopicsResult kafkaTopics = client.listTopics();
			topics = kafkaTopics.names().get();
			if (topics.contains(topic)) {
				return true;
			} else {
				log.error("Creating New Topic {}", topic);
				/**
				 * Based on Partition, retention period for each topic will be handled while creating topics
				 */
				Collection<NewTopic> newTopics = new ArrayList<>();
				NewTopic newTopic = new NewTopic(topic, 1, (short) 1);
				newTopics.add(newTopic);
				CreateTopicsResult topicsResult = client.createTopics(newTopics);
				if (topicsResult != null && MapUtils.isNotEmpty(topicsResult.values())) {
					try {
						topicsResult.values().get(newTopic.name()).get();
					} catch (ExecutionException ee) {
						log.info("Exeception in Adding new topic ", ee);
						if (ee.getMessage().contains("already exists.")) {
							topics.add(topic);
							isTopicExists = true;
						}
					}
				}
			}
		} catch (InterruptedException | ExecutionException e) {
			log.error("Error Occured on Kafka Getting cluster topics {}", e.getMessage());
		} catch (Exception e) {
			log.error("Error Occured on Kafka cluster", e);
		} finally {
			if (client != null) {
				client.close();
			}
		}
		return isTopicExists;
	}

}
