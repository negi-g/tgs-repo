package com.tgs.services.ums.helper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


import java.util.Set;

import com.google.gson.Gson;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.dbmodel.DbUserRelation;
import com.tgs.services.ums.jparepository.UserRelationService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@InitializerGroup(group = InitializerGroup.Group.GENERAL)
public class UserRelationshipHelper extends InMemoryInitializer {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	UserRelationService userService;

	public UserRelationshipHelper(GeneralCachingCommunicator cachingCommunicator) {
		super(null);
		UserRelationshipHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		storeUserRelationships(CacheSetName.USER_RELATION_USERID1.getName());
		storeUserRelationships(CacheSetName.USER_RELATION_USERID2.getName());
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.USER_RELATION_USERID1.getName())
				.namespace(CacheNameSpace.USERS.getName()).build());
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.USER_RELATION_USERID2.getName())
				.namespace(CacheNameSpace.USERS.getName()).build());
	}

	public void storeUserRelationships(String setName) {
		Runnable fetchUserRelationshipTask = () -> {
			List<DbUserRelation> userRelationshipList = new ArrayList<>();
			final int resultsPerPage = 1000;
			final int dataReadConcurrencyLevel = 1;
			final Object minAndMax[] = ((Object[]) userService.findMinAndMaxIdUserRelation()[0]);

			if (!ObjectUtils.isEmpty(minAndMax) && !ObjectUtils.isEmpty(minAndMax[0])
					&& !ObjectUtils.isEmpty(minAndMax[1])) {

				ThreadLocal<Long> from = new ThreadLocal<>();
				ThreadLocal<Long> to = new ThreadLocal<>();
				AtomicLong currFrom = new AtomicLong(0);
				AtomicLong currTo = new AtomicLong(0);
				AtomicInteger userRelationshipCount = new AtomicInteger();

				final long THRESHOLD = (long) minAndMax[1];
				log.info("Started fetching user relationship data from database with limit {}, From {} - To {}",
						resultsPerPage, minAndMax[0], (long) minAndMax[0] + resultsPerPage);

				ExecutorService storeSearchResultExecutor = Executors.newFixedThreadPool(dataReadConcurrencyLevel);
				List<Future<?>> futures = new ArrayList<>();

				for (int i = 0; i < 500; i++) {

					futures.add(storeSearchResultExecutor.submit(() -> {

						synchronized (this) {
							from.set((currFrom.get() == 0) ? (long) minAndMax[0] : currFrom.get());
							to.set((currTo.get() == 0) ? (long) minAndMax[0] + resultsPerPage : currTo.get());
							currFrom.set(to.get() + 1);
							currTo.set(to.get() + resultsPerPage);
						}

						if (from.get() > THRESHOLD) {
							log.info(
									"Finished fetching user relationship data from database as the threshold is reached."
											+ " Final User relationship fetch count {}, From {} - To {}",
									userRelationshipCount.get(), from.get(), to.get());

							return;
						}

						log.info(
								"Started fetching user relationship data from database. Total User relationship"
										+ " Count {}, From {} - To {}",
								userRelationshipCount.get(), from.get(), to.get());

						List<DbUserRelation> userList = userService.findByIdBetween(from.get(), to.get());

						log.info(
								"Finished fetching user relationship data from database. Current User"
										+ " relationship count {}" + "Total Count {}, From {} - To {}",
								userList.size(), userRelationshipCount.get(), from.get(), to.get());

						userRelationshipCount.getAndAdd(userList.size());
						userRelationshipList.addAll(userList);
					}));
				}
				for (Future<?> future : futures) {
					try {
						future.get();
					} catch (Exception e) {
						log.info("All threads have not comlpeted yet");
					}
				}
				storeSearchResultExecutor.shutdown();
			} else {
				log.info("Unable to fetch User relationship info from database as the table is empty");
			}

			Map<String, LinkedHashSet<User>> userMap = new HashMap<>();

			userRelationshipList.forEach(user -> {
				User temp1 = User.builder()
						.userId(CacheSetName.USER_RELATION_USERID1.getName().equals(setName) ? user.getUserId2()
								: user.getUserId1())
						.name(CacheSetName.USER_RELATION_USERID1.getName().equals(setName) ? user.getUserName2()
								: user.getUserName1())
						.build();

				updateMap(CacheSetName.USER_RELATION_USERID1.getName().equals(setName) ? user.getUserId1()
						: user.getUserId2(), temp1, userMap);
			});
			userMap.forEach((k, v) -> {
				saveInCache(k, v, setName);
			});
		};

		Thread fetchUserRelationshipThread = new Thread(fetchUserRelationshipTask);
		fetchUserRelationshipThread.start();

		try {
			fetchUserRelationshipThread.join();
		} catch (InterruptedException e) {
			log.error("Unable to join UserRelationshipHelper thread ", e);
		}
	}

	private static void updateMap(String key, User user, Map<String, LinkedHashSet<User>> userMap) {
		if (userMap.get(key) == null) {
			LinkedHashSet<User> userSet = new LinkedHashSet<>();
			userSet.add(user);
			userMap.put(key, userSet);
		} else {
			userMap.get(key).add(user);
		}
	}

	public static void deleteFromCacheForUserId1(DbUserRelation userRelation) {
		deleteFromCache(CacheSetName.USER_RELATION_USERID1.getName(), userRelation.getUserId1(),
				userRelation.getUserId2());
	}

	public static void deleteFromCacheForUserId2(DbUserRelation userRelation) {
		deleteFromCache(CacheSetName.USER_RELATION_USERID2.getName(), userRelation.getUserId2(),
				userRelation.getUserId1());
	}

	public static void deleteFromCache(String setName, String key, String value) {
		Map<String, Set<User>> existingRelations = getUserRelations(Arrays.asList(key), setName);
		Set<User> existingUsers = new HashSet<>();
		for (User existingUser : existingRelations.get(key)) {
			if (!existingUser.getUserId().equals(value)) {
				existingUsers.add(existingUser);
			}
		}
		if (!existingUsers.isEmpty())
			saveInCache(key, existingUsers, setName);
		else
			deleteEntryFromCache(key, setName);
	}


	public static void saveInCacheForUserId1(DbUserRelation userRelation) {
		saveInCache(CacheSetName.USER_RELATION_USERID1.getName(), userRelation);
	}

	public static void saveInCacheForUserId2(DbUserRelation userRelation) {
		saveInCache(CacheSetName.USER_RELATION_USERID2.getName(), userRelation);
	}

	public static void saveInCache(String setName, DbUserRelation userRelation) {
		String key = CacheSetName.USER_RELATION_USERID1.getName().equals(setName) ? userRelation.getUserId1()
				: userRelation.getUserId2();
		Map<String, Set<User>> existingRelations = getUserRelations(Arrays.asList(key), setName);
		Set<User> existingUsers = existingRelations.getOrDefault(key, new HashSet<>());
		String relatedUserId = CacheSetName.USER_RELATION_USERID1.getName().equals(setName) ? userRelation.getUserId2()
				: userRelation.getUserId1();
		User userExists =
				existingUsers.stream().filter(u -> u.getUserId().equals(relatedUserId)).findFirst().orElse(null);
		if (userExists == null) {
			User relatedUser = User.builder().userId(relatedUserId)
					.name(CacheSetName.USER_RELATION_USERID1.getName().equals(setName) ? userRelation.getUserName2()
							: userRelation.getUserName1())
					.build();
			existingUsers.add(relatedUser);
			saveInCache(key, existingUsers, setName);
		}
	}

	public static void saveInCache(String userId, Set<User> userRelations, String setName) {
		if (StringUtils.isBlank(userId)) {
			return;
		}
		if (CollectionUtils.isNotEmpty(userRelations)) {
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.USERRELATION.getName(), GsonUtils.getGson().toJson(userRelations));
			cachingCommunicator.store(
					CacheMetaInfo.builder().set(setName).namespace(CacheNameSpace.USERS.getName()).key(userId).build(),
					binMap, false, true, -1);
		}
	}

	public static void deleteEntryFromCache(String userId, String setName) {
		cachingCommunicator.delete(
				CacheMetaInfo.builder().set(setName).namespace(CacheNameSpace.USERS.getName()).key(userId).build());
	}

	public static Map<String, Set<User>> getUserRelationsForUserId1(List<String> userIds) {
		return getUserRelations(userIds, CacheSetName.USER_RELATION_USERID1.getName());
	}

	public static Map<String, Set<User>> getUserRelationsForUserId2(List<String> userIds) {
		return getUserRelations(userIds, CacheSetName.USER_RELATION_USERID2.getName());
	}

	public static Map<String, Set<User>> getUserRelations(List<String> userIds, String setName) {
		Map<String, Set<User>> superiors = new HashMap<>();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName()).set(setName)
				.keys(userIds.toArray(new String[0])).build();
		Map<String, Map<String, String>> userMap = cachingCommunicator.get(metaInfo, String.class);
		if (MapUtils.isNotEmpty(userMap)) {
			Gson gson = GsonUtils.getGson();
			Type type = new TypeToken<Set<User>>() {}.getType();
			for (Entry<String, Map<String, String>> entrySet : userMap.entrySet()) {
				if (entrySet.getValue().get(BinName.USERRELATION.getName()) != null) {
					Set<User> userRelations =
							gson.fromJson(entrySet.getValue().get(BinName.USERRELATION.getName()), type);
					superiors.put(entrySet.getKey(), userRelations);
				}
			}
		}
		return superiors;
	}
}
