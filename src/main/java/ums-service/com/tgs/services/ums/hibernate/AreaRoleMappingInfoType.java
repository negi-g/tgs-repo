package com.tgs.services.ums.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.ums.datamodel.AreaRoleMappingInfo;

public class AreaRoleMappingInfoType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return AreaRoleMappingInfo.class;
    }
}
