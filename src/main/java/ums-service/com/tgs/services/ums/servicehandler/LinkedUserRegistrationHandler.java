package com.tgs.services.ums.servicehandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.EnumTypeAdapterFactory;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.datamodel.WalletStatus;
import com.tgs.services.ums.datamodel.RegisterLinkedUserRequest;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserConfiguration;
import com.tgs.services.ums.datamodel.UserRelation;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.dbmodel.DbUserRelation;
import com.tgs.services.ums.helper.UserHelper;
import com.tgs.services.ums.helper.UserRelationshipHelper;
import com.tgs.services.ums.jparepository.UserRelationService;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.manager.AbstractRegistrationManager;
import com.tgs.services.ums.restmodel.RegisterRequest;
import com.tgs.services.ums.restmodel.UserResponse;
import com.tgs.utils.exception.ResourceNotFoundException;

@Service
public class LinkedUserRegistrationHandler extends ServiceHandler<RegisterLinkedUserRequest, UserResponse> {

	@Autowired
	private UserService userService;
	
	@Autowired
	UserRelationService relationService;

	@Autowired
	PaymentServiceCommunicator paymentCommunicator;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;

	DbUser dbUser;

	Set<String> linkedUserIds;
	
	@Autowired
	private UserHelper userHelper;

	@Override
	public void beforeProcess() throws Exception {
		dbUser = userService.findByUserId(request.getUserId());
		if (dbUser == null) {
			throw new ResourceNotFoundException(SystemError.INVALID_USERID);
		}
		ClientGeneralInfo clientGeneralInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		if (clientGeneralInfo.getAllowedLinkedUserCreationRoles() != null && !clientGeneralInfo
				.getAllowedLinkedUserCreationRoles().contains(UserRole.getUserRole(dbUser.getRole()))) {
			response.addError(SystemError.NOT_ALLOWED_TO_CREATE_LINKED_USER
					.getErrorDetail(UserRole.getUserRole(dbUser.getRole()).toString()));
		}
		linkedUserIds = dbUser.getAdditionalInfo().getLinkedUserIds();
		linkedUserIds.add(dbUser.getUserId());
		validateAllowedProducts(dbUser, linkedUserIds);
	}

	@Override
	public void process() throws Exception {
		Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build().buildGson();
		User user = dbUser.toDomain();
		String email = dbUser.getEmail().split("_")[0];
		String mobile = dbUser.getMobile().split("_")[0];
		Integer emailCount = getEmailCount(email);
		Integer mobileCount = getMobileCount(mobile);
		Integer count = Math.max(emailCount, mobileCount);
		long id = userService.getNextSeriesId();
		RegisterRequest registerRequest =
				new GsonMapper<>(dbUser, RegisterRequest.builder().build(), RegisterRequest.class).convert();
		registerRequest.setUserId(UserUtils.generateUserId(id, user.getRole()));
		registerRequest.setCreatedOn(LocalDateTime.now());
		registerRequest.setMobile(mobile.concat("_" + count));
		registerRequest.setEmail(email.concat("_" + count));
		registerRequest.setPassword(RandomStringUtils.randomAlphanumeric(8));
		registerRequest.getAdditionalInfo().setLinkedUserIds(linkedUserIds);
		DbUser dbLinkedUser = gson.fromJson(gson.toJson(registerRequest), DbUser.class);
		dbLinkedUser.setUserConf(UserConfiguration.builder().build());
		dbLinkedUser.getAdditionalInfo().setAllowedProducts(request.getAllowedProducts());
		dbLinkedUser.setStatus(user.getStatus().getCode());
		setDefaults(dbLinkedUser);
		response.setUsers(Arrays.asList(userService.save(dbLinkedUser).toDomain()));
		
		// saving linked userrelationship
		saveLinkedUserRelationShips(request.getUserId(), registerRequest.getUserId());
		
		LogUtils.log("linkedUserRegistation#save", LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				"linkedUserRegistation#start");
		paymentCommunicator.updateUserWallet(
				UserWallet.builder().status(WalletStatus.ACTIVE).userId(dbLinkedUser.getUserId()).build());
		LogUtils.log("linkedUserRegistation#walletregistration",
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), "linkedUserRegistation#start");
		addLinkedUsers(dbUser, dbLinkedUser.getUserId());
	}

	private void saveLinkedUserRelationShips(String userId, String linkedUserId) {
		List<DbUserRelation> userRelations = relationService.findByUserId1(request.getUserId());
		for(DbUserRelation relation: userRelations) {
			DbUserRelation linkedUserRelation = new GsonMapper<>(relation, DbUserRelation.builder().build(), DbUserRelation.class).convert();
			linkedUserRelation.setUserId1(linkedUserId);
			linkedUserRelation.setId(null);
			linkedUserRelation.setCreatedOn(LocalDateTime.now());
			linkedUserRelation.setProcessedOn(LocalDateTime.now());
			relationService.save(linkedUserRelation);
			UserRelationshipHelper.saveInCacheForUserId1(linkedUserRelation);
			UserRelationshipHelper.saveInCacheForUserId2(linkedUserRelation);
			userHelper.updateCache(relation.getUserId2());
		}
	}

	private void validateAllowedProducts(DbUser dbUser, Set<String> linkedUserIds) {
		List<Product> allowedProducts = request.getAllowedProducts();

		if (CollectionUtils.isNotEmpty(allowedProducts)
				&& CollectionUtils.isEmpty(dbUser.getAdditionalInfo().getAllowedProducts())) {
			response.addError(SystemError.PRODUCT_ALREADY_ASSIGNED.getErrorDetail(allowedProducts.get(0),
					dbUser.getUserId(), dbUser.getUserId()));
		} else if (allowedProducts.contains(Product.NA) && allowedProducts.size() > 1) {
			throw new CustomGeneralException(SystemError.WITH_NA_NO_OTHER_PRODUCTS_ALLOWED);
		} else {
			ServiceUtils.isProductsAllowed(allowedProducts, linkedUserIds, response);
		}
	}

	private void setDefaults(DbUser dbLinkedUser) {
		dbLinkedUser.getAdditionalInfo().setDepositBankAccounts(null);
		ClientGeneralInfo clientGeneralInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		if (StringUtils.isNotBlank(clientGeneralInfo.getRegistrationBean())) {
			AbstractRegistrationManager registrationManager = (AbstractRegistrationManager) SpringContext
					.getApplicationContext().getBean(clientGeneralInfo.getRegistrationBean());
			registrationManager.process(dbLinkedUser);
		}

	}

	private void addLinkedUsers(DbUser dbUser, String newUserId) {
		Set<String> linkedUserIds = dbUser.getAdditionalInfo().getLinkedUserIds();
		linkedUserIds.add(dbUser.getUserId());
		for (String userId : linkedUserIds) {
			DbUser user = userService.findByUserId(userId);
			Set<String> linkedUsers = user.getAdditionalInfo().getLinkedUserIds();
			linkedUsers.add(newUserId);
			user.getAdditionalInfo().setLinkedUserIds(linkedUsers);
			userService.save(user);
		}
	}

	protected Integer getEmailCount(String email) {
		return userService.findByEmailPattern(email).size();
	}

	protected Integer getMobileCount(String mobile) {
		return userService.findByMobilePattern(mobile).size();
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
