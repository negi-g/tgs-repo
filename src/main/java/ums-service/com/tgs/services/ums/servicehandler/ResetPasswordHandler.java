package com.tgs.services.ums.servicehandler;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.OTPCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.OtpToken;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.restmodel.ResetPasswordRequest;
import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class ResetPasswordHandler extends ServiceHandler<ResetPasswordRequest, BaseResponse> {

	@Autowired
	UserService service;

	@Autowired
	private BCryptPasswordEncoder passEncoder;

	@Autowired
	private OTPCommunicator otpCommunicator;

	@Autowired
	MsgServiceCommunicator msgServiceCommunicator;

	private DbUser dbUser;

	@Override
	public void beforeProcess() throws Exception {

		// Reset password functionality can be utilized either by email or mobile.
		// findByEmailOrMobile is used to retrieve user using both the constraint.
		String partnerId = SystemContextHolder.getContextData().getHttpHeaders().getPartnerId();
		dbUser = ObjectUtils.firstNonNull(
				service.findByPartnerIdAndEmailAndRole(partnerId, request.getUserName(), request.getRole()),
				service.findByPartnerIdAndMobileAndRole(partnerId, request.getUserName(), request.getRole()));
		if (Objects.isNull(dbUser) && !UserUtils.DEFAULT_PARTNERID.equals(partnerId)) {
			DbUser partnerUser = service.findByUserId(partnerId);
			if (partnerUser.getEmail().equals(request.getUserName())
					|| partnerUser.getMobile().equals(request.getUserName())) {
				dbUser = partnerUser;
			}
		}
		if (Objects.isNull(dbUser)) {
			throw new CustomGeneralException(SystemError.INVALID_EMAIL_OR_MOBILE);
		}
		if (isMatchesOldPassword()) {
			throw new CustomGeneralException(SystemError.SAME_PASSWORD);
		}
	}

	@Override
	public void process() throws Exception {
		if (isResetPasswordApplicable()) {
			dbUser.setPassword(passEncoder.encode(request.getNewPassword()));
			service.save(dbUser);
			sendSms(dbUser);
		}
	}

	private boolean isMatchesOldPassword() {
		return StringUtils.isNotBlank(request.getNewPassword())
				&& passEncoder.matches(request.getNewPassword(), dbUser.getPassword());
	}

	private boolean isResetPasswordApplicable() throws Exception {
		// Password can be reset through OTP or existing password.
		if (StringUtils.isNotBlank(request.getOtp())) {
			try {
				OtpToken otpToken = otpCommunicator.validateOtp(request.getRequestId(), request.getOtp(), 3);
				if (otpToken != null && (dbUser.getEmail().equals(otpToken.getEmail())
						|| dbUser.getMobile().equals(otpToken.getMobile()))) {
					return true;
				} else {
					throw new CustomGeneralException(SystemError.INVALID_OTP);
				}
			} catch (Exception e) {
				throw e;
			}
		} else if (StringUtils.isNotBlank(request.getPassword())) {
			boolean isOldPasswordMatched = passEncoder.matches(request.getPassword(), dbUser.getPassword());
			if (!isOldPasswordMatched)
				throw new CustomGeneralException(SystemError.UNAUTHORISED);
			return true;
		}
		return false;
	}

	@Override
	public void afterProcess() throws Exception {

	}

	private void sendSms(DbUser user) {
		AbstractMessageSupplier<SmsAttributes> smsAttributeSupplier = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				Map<String, String> attributes = new HashMap<>();
				attributes.put("userEmail", user.getEmail());
				SmsAttributes smsAttr = SmsAttributes.builder().key(SmsTemplateKey.RESET_PASSWORD_SMS.name())
						.recipientNumbers(Arrays.asList(user.getMobile())).attributes(attributes)
						.partnerId(user.getPartnerId()).role(UserRole.getEnumFromCode(user.getRole())).build();
				return smsAttr;
			}
		};
		msgServiceCommunicator.sendMessage(smsAttributeSupplier.getAttributes());
	}

}
