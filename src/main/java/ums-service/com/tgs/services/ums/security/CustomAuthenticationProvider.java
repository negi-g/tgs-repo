package com.tgs.services.ums.security;

import static java.util.Collections.emptyList;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import com.tgs.services.base.ruleengine.GeneralBasicFact;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.tgs.services.base.analytics.SignInAttempQueryMapper;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.CustomAuthenticationException;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.gms.datamodel.systemaudit.SystemAudit;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.ums.datamodel.UserAnalyticsType;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.helper.analytics.UserAnalyticsHelper;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.restmodel.SignInRequest;
import com.tgs.services.ums.restmodel.SignInResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	UserService userService;

	DbUser dbUser;

	@Autowired
	private BCryptPasswordEncoder passEncoder;

	@Autowired
	GeneralServiceCommunicator gmsComm;

	@Autowired
	GeneralCachingCommunicator cachingComm;

	@Autowired
	PaymentServiceCommunicator payService;

	@Autowired
	UserAnalyticsHelper userAnalyticsHelper;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		SignInRequest signInRequest = (SignInRequest) authentication.getDetails();
		String userName = authentication.getName();
		String password = authentication.getCredentials().toString().trim();
		String partnerId = SystemContextHolder.getContextData().getHttpHeaders().getPartnerId();
		UserRole role = signInRequest.getRole();
		log.info("Authentication user {} with partnerId {} ", userName, partnerId);
		LogUtils.log(LogTypes.USERDBSTART, LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		DbUser dbUser = userService.findByPartnerIdAndEmailAndRole(partnerId, userName.toLowerCase(), role);
		if (Objects.isNull(dbUser))
			dbUser = userService.findByPartnerIdAndMobileAndRole(partnerId, userName, role);
		// Allow WL Partner to login into his own domain
		if (Objects.isNull(dbUser) && !UserUtils.DEFAULT_PARTNERID.equals(partnerId)) {
			dbUser = userService.findByEmailOrMobile(userName.toLowerCase(), userName);
			if (dbUser != null && !dbUser.getUserId().equals(partnerId))
				dbUser = null;
		}
		if (Objects.isNull(dbUser)) {
			throw new CustomAuthenticationException(SystemError.LOGIN_UNSUCCESSFUL);
		} else if (!dbUser.getStatus().equals(UserStatus.ENABLED.getCode())) {
			throw new CustomAuthenticationException(SystemError.INACTIVE_ACCOUNT);
		} else if (isMaxSignInLimitCrossed(dbUser, password)) {
			throw new CustomAuthenticationException(SystemError.UNSUCCESSFUL_ATTEMPT_LIMIT);
		} else if (!passEncoder.matches(password, dbUser.getPassword())) {
			incrementUnsuccessAttemptCount(dbUser);
			throw new CustomAuthenticationException(SystemError.LOGIN_UNSUCCESSFUL);
		}

		User user = new User(userName, password, true, true, true, true, emptyList());

		dbUser.getAdditionalInfo().setLastLoginTime(LocalDateTime.now());
		userService.save(dbUser);
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, password, emptyList());

		SignInResponse res = new SignInResponse();
		res.setUser(dbUser.toDomain());
		res.setOtpValidateRequest(signInRequest.getOtpValidateRequest());

		com.tgs.services.ums.datamodel.User parentUser = null;
		if (dbUser.getParentUserId() != null) {
			parentUser = userService.findByUserId(dbUser.getParentUserId()).toDomain();
			if (!parentUser.getStatus().equals(UserStatus.ENABLED)) {
				throw new CustomAuthenticationException(SystemError.INACTIVE_PARENT_ACCOUNT);
			}
			res.getUser().setParentUser(parentUser);
			res.getUser().setParentConf(parentUser.getUserConf());
		}

		ServiceUtils.updateUserInfoforJWTToken(res.getUser(), payService, null);
		res.setStatus(new Status(HttpStatusCode.HTTP_200));
		auth.setDetails(res);
		SystemContextHolder.getContextData().setUser(res.getUser());
		LogUtils.log(LogTypes.USERDBEND, null, LogTypes.USERDBSTART);
		ContextData contextData = SystemContextHolder.getContextData();
		SystemAudit audit = SystemAudit.builder().auditType(AuditAction.LOGIN).userId(contextData.getUser().getUserId())
				.ip(contextData.getHttpHeaders().getIp())
				.loggedInUserId(contextData.getUser().getEmulateOrLoggedInUserId()).build();
		gmsComm.addToSystemAudit(audit);
		return auth;
	}

	private boolean isMaxSignInLimitCrossed(DbUser dbUser, String currentPassword) {
		boolean isLimitCrossed = false;
		GeneralBasicFact generalfact = GeneralBasicFact.builder().build();
		ClientGeneralInfo clientgeneralInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, generalfact);
		Integer attempts = null;
		if (Objects.nonNull(clientgeneralInfo)
				&& BooleanUtils.isNotTrue(clientgeneralInfo.getPasswordAttemptConfiguration().getDisabled())) {
			attempts = getNoOfAttemptFromCache(dbUser);
			if (Objects.nonNull(attempts)
					&& clientgeneralInfo.getPasswordAttemptConfiguration().getMaxAttempts() < (attempts + 1)) {
				isLimitCrossed = true;
			}
		}
		if (isLimitCrossed && Objects.nonNull(attempts)) {
			boolean isWrongPassword = !passEncoder.matches(currentPassword, dbUser.getPassword());
			SignInAttempQueryMapper mapper = SignInAttempQueryMapper.builder().attempts(attempts).limitCrossed(true)
					.contextData(SystemContextHolder.getContextData()).user(dbUser.toDomain())
					.wrongPassword(isWrongPassword).build();
			userAnalyticsHelper.pushToAnalytics(mapper, UserAnalyticsType.SIGNIN_ATTEMPT);
		}
		return isLimitCrossed;
	}


	private void incrementUnsuccessAttemptCount(DbUser dbUser) {
		GeneralBasicFact generalfact = GeneralBasicFact.builder().build();
		ClientGeneralInfo clientgeneralInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, generalfact);
		if (Objects.nonNull(clientgeneralInfo)
				&& BooleanUtils.isNotTrue(clientgeneralInfo.getPasswordAttemptConfiguration().getDisabled())) {
			Integer attempts = getNoOfAttemptFromCache(dbUser);
			Integer ttl = null;
			if (Objects.nonNull(attempts)) {
				attempts++;
				ttl = -2;
			} else {
				attempts = 1;
				ttl = (clientgeneralInfo.getPasswordAttemptConfiguration().getTtl() * 60);
			}
			saveInCache(ttl, dbUser, attempts);
			SignInAttempQueryMapper mapper = SignInAttempQueryMapper.builder().attempts(attempts).limitCrossed(false)
					.contextData(SystemContextHolder.getContextData()).user(dbUser.toDomain()).wrongPassword(true)
					.build();
			userAnalyticsHelper.pushToAnalytics(mapper, UserAnalyticsType.SIGNIN_ATTEMPT);
		}
	}

	/**
	 * @param expirationTime {-2 if don't want to update ttl, and actual if it's a first entry}
	 * @param dbUser
	 */
	public void saveInCache(int expirationTime, DbUser dbUser, Integer numberOfAttempt) {
		String noOfAttempt = String.valueOf(numberOfAttempt);
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.USERID.getName(), dbUser.getUserId());
		binMap.put(BinName.SIGNIN_ATTEMPT.getName(), noOfAttempt);
		cachingComm.store(CacheMetaInfo.builder().set(CacheSetName.USER_LOGIN.getName())
				.namespace(CacheNameSpace.USERS.getName()).key(dbUser.getUserId()).build(), binMap, false, true,
				expirationTime);
	}

	public Integer getNoOfAttemptFromCache(DbUser dbUser) {
		String attemptCount = null;
		CacheMetaInfo cacheMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
				.set(CacheSetName.USER_LOGIN.getName()).keys((dbUser.getUserId().split(" "))).build();
		Map<String, Map<String, String>> userLoginMap = cachingComm.get(cacheMetaInfo, String.class);
		if (MapUtils.isNotEmpty(userLoginMap)) {
			for (Entry<String, Map<String, String>> entrySet : userLoginMap.entrySet()) {
				attemptCount = entrySet.getValue().get(BinName.SIGNIN_ATTEMPT.getName());
			}
		}
		if (StringUtils.isNotBlank(attemptCount)) {
			return Integer.parseInt(attemptCount);
		}
		return null;
	}


	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}
}
