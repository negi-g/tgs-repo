package com.tgs.services.ums.servicehandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.restmodel.UserTempToken;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.SortByAttributes;
import com.tgs.services.base.datamodel.UserBalanceSummary;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.FieldTransform;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.WalletStatus;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.helper.UserHelper;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.restmodel.SSOUserDetailResponse;
import com.tgs.services.ums.restmodel.UserResponse;
import com.tgs.services.ums.validator.EmulateValidator;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserFetchHandler extends ServiceHandler<UserFilter, UserResponse> {

	@Autowired
	private UserService usrService;

	@Autowired
	private EmulateValidator emulateValidator;

	@Autowired
	private ToggleUserHandler linkedUserHandler;

	@Autowired
	private PaymentServiceCommunicator paymentService;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		User loggedInUser = contextData.getUser();
		log.debug("[UserListing] In UserFetchHandler ");
		List<DbUser> userList = new ArrayList<>();

		for (int i = 0; i < 500; i++) {

			PageAttributes pageAttr = new PageAttributes();
			pageAttr.setPageNumber(i);
			pageAttr.setSize(5000);
			SortByAttributes srtBy = new SortByAttributes();
			srtBy.setParams(Arrays.asList("id"));
			srtBy.setOrderBy("asc");
			pageAttr.setSortByAttr(Arrays.asList(srtBy));
			request.setPageAttr(pageAttr);
			List<DbUser> userChunk = usrService.search(request);
			if (CollectionUtils.isEmpty(userChunk))
				break;
			userList.addAll(userChunk);
		}

		List<User> users = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(userList)) {
			userList.forEach(dbuser -> {
				User user = dbuser.toDomain();
				user.setCanBeEmulated(canBeEmulated(loggedInUser, user));
				if (StringUtils.isNotBlank(request.getName())
						&& CollectionUtils.isNotEmpty(user.getAdditionalInfo().getLinkedUserIds())) {
					user.setLinkedUserDetails(linkedUserHandler.getUserDetails(user.getUserId()).userDetails);
				}
				/**
				 * No filtering on the basis of group if it is empty string list.
				 */
				if (TgsCollectionUtils.isEmptyStringCollection(request.getGroups()) || TgsCollectionUtils
						.haveNonNullIntersection(user.getAdditionalInfo().getGroups(), request.getGroups())) {
					users.add(FieldTransform.mask(user));
				}
			});
			response.setUsers(users);
		}
	}

	@Override
	public void afterProcess() throws Exception {}

	private boolean canBeEmulated(User loggedInUser, User emulateUser) {
		if (loggedInUser == null || !emulateUser.getStatus().equals(UserStatus.ENABLED)
				|| emulateUser.getRole().equals(loggedInUser.getRole()))
			return false;
		if (loggedInUser.getRole().equals(UserRole.AGENT) && !(emulateUser.getRole().equals(UserRole.AGENT_STAFF)
				|| emulateUser.getParentUserId().equals(loggedInUser.getUserId())))
			return false;
		return emulateValidator.validate(loggedInUser, emulateUser);
	}

	public SSOUserDetailResponse getUserDetails(String token) {
		UserTempToken userTempToken = UserUtils.decodeTempToken(token);
		if (userTempToken == null || userTempToken.getCreatedOn().isBefore(LocalDateTime.now().minusHours(4))) {
			log.error("Token is Expired or invalid {}", userTempToken);
			throw new CustomGeneralException(SystemError.TOKEN_EXPIRED);
		}

		User user = UserHelper.getUserFromCache(userTempToken.getUserId());
		if (user == null)
			throw new CustomGeneralException(SystemError.INVALID_USERID);
		SSOUserDetailResponse res = new SSOUserDetailResponse();
		UserBalanceSummary balanceSummary = paymentService.getUserBalanceSummary(user.getUserId());
		String walletOrCreditStatus = WalletStatus.BLOCKED.name();
		if (balanceSummary.getWalletStatus().equals(WalletStatus.ACTIVE.name()) && (balanceSummary.getStatus() == null
				|| balanceSummary.getStatus().equals(CreditStatus.ACTIVE.name()))) {
			walletOrCreditStatus = WalletStatus.ACTIVE.name();
		}

		User ssoUserDetails = User.builder().name(user.getName()).userId(user.getUserId()).role(user.getRole())
				.balance(balanceSummary.getTotalBalance().doubleValue()).walletOrCreditStatus(walletOrCreditStatus)
				.build();
		if (user.getRole().equals(UserRole.AGENT_STAFF)) {
			User parentUser = UserHelper.getUserFromCache(user.getParentUserId());
			walletOrCreditStatus = WalletStatus.BLOCKED.name();
			balanceSummary = paymentService.getUserBalanceSummary(parentUser.getUserId());
			if (balanceSummary.getWalletStatus().equals(WalletStatus.ACTIVE.name())
					&& (balanceSummary.getStatus() == null
							|| balanceSummary.getStatus().equals(CreditStatus.ACTIVE.name()))) {
				walletOrCreditStatus = WalletStatus.ACTIVE.name();
			}

			ssoUserDetails.setParentUser(User.builder().name(parentUser.getName()).userId(parentUser.getUserId())
					.balance(balanceSummary.getTotalBalance().doubleValue()).role(parentUser.getRole())
					.walletOrCreditStatus(walletOrCreditStatus).build());
		}
		res.setUserDetails(ssoUserDetails);
		return res;
	}


}

