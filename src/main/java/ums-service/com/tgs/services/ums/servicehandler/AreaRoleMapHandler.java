package com.tgs.services.ums.servicehandler;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.FetchType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.filters.AreaRoleMappingFilter;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.dbmodel.DbAreaRoleMapping;
import com.tgs.services.ums.helper.AreaRoleHelper;
import com.tgs.services.ums.jparepository.AreaRoleMapService;
import com.tgs.services.ums.restmodel.AreaRoleMapResponse;

@Service
public class AreaRoleMapHandler extends ServiceHandler<AreaRoleMapping, AreaRoleMapResponse> {

	@Autowired
	private AreaRoleMapService areaRoleMapService;

	@Override
	public void beforeProcess() throws Exception {
	}

	@Override
	public void process() throws Exception {
		DbAreaRoleMapping roleMapping = new DbAreaRoleMapping();
		if (request.getAreaRole() != null) {
			roleMapping = areaRoleMapService.getMappingsByAreaRole(request.getAreaRole());
		}
		roleMapping = new GsonMapper<>(request, roleMapping, DbAreaRoleMapping.class).convert();
		areaRoleMapService.save(roleMapping);
		AreaRoleHelper.updateAreaRoleMapping(roleMapping.toDomain());
		response.getAreaRoleMapping().add(roleMapping.toDomain());
	}

	@Override
	public void afterProcess() {
	}

	public AreaRoleMapResponse fetchAreaRoleMap(AreaRoleMappingFilter filter) {
		AreaRoleMapResponse response = new AreaRoleMapResponse();
		List<AreaRoleMapping> roleMapping = new ArrayList<AreaRoleMapping>();
		if (FetchType.LIVE.equals(filter.getFetchType())) {
			roleMapping = areaRoleMapService.getMappingsByAreaRoleIn(filter.getAreaRoles());
		} else {
			roleMapping = AreaRoleHelper.getMappingsByAreaRoleIn(filter.getAreaRoles());
		}
		roleMapping.forEach(role -> {
			response.getAreaRoleMapping().add(role);
		});
		return response;
	}

	public BaseResponse deleteAreaRoleMap(AreaRole areaRole) {
		BaseResponse baseResponse = new BaseResponse();
		DbAreaRoleMapping roleMapping = areaRoleMapService.getMappingsByAreaRole(areaRole);
		if(roleMapping != null) {
			areaRoleMapService.deleteMappingsById(roleMapping.getId());
			AreaRoleHelper.deleteAreaRoleMappingFromCache(roleMapping.toDomain());
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_AREA_ROLE));
		}
		return baseResponse;
	}

}
