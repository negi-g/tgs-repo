package com.tgs.services.ums.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.AirlineResponseProcessor;

import com.tgs.services.ums.datamodel.fee.UserFee;
import com.tgs.services.ums.restmodel.UserFeeResponse;
import com.tgs.services.ums.servicehandler.UserFeeHandler;

@RestController
@RequestMapping("/ums/v1")
public class UserFeeController {

	@Autowired
	UserFeeHandler userFeeHandler;

	@Autowired
	UserFeeRequestValidator userFeeRequestValidator;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(userFeeRequestValidator);
	}

	@RequestMapping(value = "/addfee", method = RequestMethod.POST)
	protected UserFeeResponse addFee(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid UserFee bookingFeeRequest) throws Exception {
		userFeeHandler.initData(bookingFeeRequest, new UserFeeResponse());
		return userFeeHandler.getResponse();
	}

	@CustomRequestMapping(responseProcessors = { AirlineResponseProcessor.class })
	@RequestMapping(value = "/list-fee/{type}", method = RequestMethod.GET)
	protected UserFeeResponse getFeeList(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("type") String type) throws Exception {
		if (BooleanUtils
				.isTrue(SystemContextHolder.getContextData().getUser().getAdditionalInfo().getDisableMarkup())) {
			throw new CustomGeneralException(SystemError.PERMISSION_DENIED);
		}
		return userFeeHandler.getUserFeeList(type);
	}

	@RequestMapping(value = "/delete-fee/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteUserFee(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") String id) throws Exception {
		return userFeeHandler.deleteFee(id);
	}
}
