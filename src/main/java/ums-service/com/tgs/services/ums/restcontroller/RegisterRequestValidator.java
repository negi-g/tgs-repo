package com.tgs.services.ums.restcontroller;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.base.TgsValidator;
import com.tgs.services.ums.restmodel.RegisterRequest;

@Service
public class RegisterRequestValidator extends TgsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return RegisterRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof RegisterRequest) {
			RegisterRequest request = (RegisterRequest) target;
			registerErrors(errors, null, request);
		}
	}

}
