package com.tgs.services.ums.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tgs.services.ums.dbmodel.DbUserRelation;

@Repository
public interface UserRelationRepository
		extends JpaRepository<DbUserRelation, Long>, JpaSpecificationExecutor<DbUserRelation> {

	public List<DbUserRelation> findByUserId1(String userId1);

	public List<DbUserRelation> findByUserId2(String userId2);

	public DbUserRelation findByUserId1AndDepthAndPriority(String userId1, int depth, int priority);

	public DbUserRelation findByUserId1AndPriority(String userId1, int priority);

	public DbUserRelation findByUserId1AndDepthAndUserId2(String userId1, int depth, String userId2);

	public DbUserRelation findByUserId1AndUserId2(String userId1, String userId2);
	
	public List<DbUserRelation> findByIdBetween(Long from, Long to);
	
	@Query(value = "select min(h.id), max(h.id) from DbUserRelation h")
	public Object[] findMinAndMaxId();

}
