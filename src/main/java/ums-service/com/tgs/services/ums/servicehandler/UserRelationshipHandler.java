package com.tgs.services.ums.servicehandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import com.tgs.services.ums.dbmodel.DbUserRelation;
import com.tgs.services.ums.helper.UserHelper;
import com.tgs.services.ums.helper.UserRelationshipHelper;
import com.tgs.services.ums.jparepository.UserRelationService;
import com.tgs.services.ums.restmodel.RelationshipsRequest;

@Service
public class UserRelationshipHandler extends ServiceHandler<RelationshipsRequest, BaseResponse> {

	@Autowired
	UserRelationService service;

	@Autowired
	private UserHelper userHelper;

	@Autowired
	MsgServiceCommunicator msgSrvCommunicator;

	private Map<String, User> userMap;

	@Override
	public void beforeProcess() throws Exception {
		List<String> userIdList = Lists.newArrayList(request.getRelatedUserIds());
		userIdList.add(request.getUserId());
		userMap = UserHelper.getUserMap(UserCacheFilter.builder().userIds(userIdList).build());
		List<String> invalidUserIds = new ArrayList<>();
		userIdList.forEach(userId -> {
			if (userMap.get(userId) == null)
				invalidUserIds.add(userId);
		});
		if (CollectionUtils.isNotEmpty(invalidUserIds)) {
			throw new CustomGeneralException(SystemError.INVALID_USERIDS,
					SystemError.INVALID_USERIDS.getMessage(invalidUserIds.toString()));
		}
		User user = userMap.get(request.getUserId());
		if (!(user.getRole().equals(UserRole.AGENT) || user.getRole().equals(UserRole.DISTRIBUTOR)
				|| user.getRole().equals(UserRole.CORPORATE))) {
			throw new CustomGeneralException(SystemError.SALES_RELATION_DENIED);
		}
		request.getRelatedUserIds().forEach(child -> {
			User relatedUser = userMap.get(child);
			if (!relatedUser.getRole().equals(UserRole.SALES)) {
				throw new CustomGeneralException(SystemError.SALES_RELATION_DENIED);
			}
		});
	}

	@Override
	public void process() throws Exception {
		int priority = 0;
		List<DbUserRelation> existingRelations = service.findByUserId1(request.getUserId());
		for (String relatedUser : request.getRelatedUserIds()) {
			DbUserRelation existingRelation =
					existingRelations.stream().filter(r -> r.getUserId2().equals(relatedUser)).findAny().orElse(null);
			// Updating just priority
			if (existingRelation != null) {
				existingRelations.remove(existingRelation);
				existingRelation.setPriority(++priority);
				service.save(existingRelation);
			}
			// Creating new relation
			else {
				DbUserRelation relation = DbUserRelation.builder().userId1(request.getUserId())
						.userName1(userMap.get(request.getUserId()).getName()).userId2(relatedUser)
						.userName2(userMap.get(relatedUser).getName()).depth(1).priority(++priority).build();
				service.save(relation);
				UserRelationshipHelper.saveInCacheForUserId1(relation);
				UserRelationshipHelper.saveInCacheForUserId2(relation);
				userHelper.updateCache(relatedUser);
				// Email content to be constructed again after receiving template from TJ team.
				// sendHierachyUpdateEmail(userMap.get(request.getUserId()), userMap.get(child));
			}
		}
		existingRelations.forEach(rel -> {
			service.delete(rel);
			userHelper.removeAllowedUserIdsFromCache(rel.getUserId2(), Collections.singleton(rel.getUserId1()));
		});

	}

	private void sendHierachyUpdateEmail(User user, User relatedUser) {
		AbstractMessageSupplier<EmailAttributes> emailAttributeSupplier =
				new AbstractMessageSupplier<EmailAttributes>() {
					@Override
					public EmailAttributes get() {
						EmailAttributes emailAttributes = EmailAttributes.builder().build();
						emailAttributes.setToEmailId(user.getEmail());
						emailAttributes.setKey(EmailTemplateKey.SALES_HIERACHY_EMAIL.name());
						emailAttributes.setSalesRepName(relatedUser.getName());
						emailAttributes.setSalesRepMobile(relatedUser.getMobile());
						emailAttributes.setSalesRepEmail(relatedUser.getEmail());
						emailAttributes.setRole(user.getRole());
						return emailAttributes;
					}
				};
		msgSrvCommunicator.sendMail(emailAttributeSupplier.getAttributes());
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
