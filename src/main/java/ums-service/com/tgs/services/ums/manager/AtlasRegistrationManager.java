package com.tgs.services.ums.manager;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.tgs.services.base.datamodel.BankAccountInfo;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.utils.AtlasUtils;
import com.tgs.services.ums.dbmodel.DbUser;

@Service
public class AtlasRegistrationManager extends AbstractRegistrationManager {

	@Override
	public void process(DbUser dbUser) {
		if (UserRole.AGENT.getCode().equals(dbUser.getRole())
				|| UserRole.DISTRIBUTOR.getCode().equals(dbUser.getRole())) {

			/**
			 * Generate Random DBS account number
			 */

			List<BankAccountInfo> bankAccountInfos = new ArrayList<BankAccountInfo>();
			bankAccountInfos.add(AtlasUtils.generateDBSAccountNumber(dbUser.getUserId()));
			dbUser.getAdditionalInfo().setDepositBankAccounts(bankAccountInfos);
		}
	}
}
