package com.tgs.services.ums.restcontroller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.CommercialResponseProcessor;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.PaymentMetaInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.restmodel.SSOUserDetailResponse;
import com.tgs.services.ums.restmodel.UserDetailsResponse;
import com.tgs.services.ums.restmodel.UserResponse;
import com.tgs.services.ums.servicehandler.ToggleUserHandler;
import com.tgs.services.ums.servicehandler.UserInfoSearchHandler;
import com.tgs.services.ums.servicehandler.UserFetchHandler;

@RestController
@RequestMapping("/ums/v1")
public class UserFetchController {

	// @Autowired
	// UserService userService;

	@Autowired
	private UserFetchHandler userFetchHandler;

	@Autowired
	private UserInfoSearchHandler userInfoSearchHandler;

	@Autowired
	private UserService userService;

	@Autowired
	private ToggleUserHandler linkedUserHandler;


	@Autowired
	private PaymentServiceCommunicator payService;

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class, CommercialResponseProcessor.class})
	protected UserResponse getUserList(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody UserFilter userFilter, BindingResult result) throws Exception {
		List<String> userIds = UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), userFilter.getUserIds());

		/**
		 * This is to avoid sending more than 1000 userIds in IN query list while searching user with name for SALES
		 * roles
		 */
		if (userFilter.getName() != null
				&& SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.SALES)) {
			if (CollectionUtils.isNotEmpty(userIds) && userIds.size() < 1000) {
				userFilter.setUserIds(userIds);
			}
		} else {
			userFilter.setUserIds(userIds);
		}
		// Right now mid office roles wont be able to see partner data. If they can, we need to change this function
		// accordingly
		userFilter.setPartnerIdIn(
				UserServiceHelper.returnPartnerIds(SystemContextHolder.getContextData().getUser().getLoggedInUserId(),
						false, userFilter.getPartnerIdIn()));
		FiltersValidator validator =
				(FiltersValidator) SpringContext.getApplicationContext().getBean("filtersValidator");
		validator.validate(userFilter, result);
		if (result.hasErrors()) {
			UserResponse userResponse = new UserResponse();
			userResponse.setErrors(validator.getErrorDetailFromBindingResult(result));
			return userResponse;
		} else {
			userFetchHandler.initData(userFilter, new UserResponse());
			return userFetchHandler.getResponse();
		}
	}

	@RequestMapping(value = "/user-list", method = RequestMethod.POST)
	protected UserResponse getUsers(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody UserFilter userFilter, BindingResult result) throws Exception {
		List<String> userIds = UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), userFilter.getUserIds());
		if (userFilter.getName() != null
				&& SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.SALES)) {
			if (CollectionUtils.isNotEmpty(userIds) && userIds.size() < 1000) {
				userFilter.setUserIds(userIds);
			}
		} else {
			userFilter.setUserIds(userIds);
		}

		userFilter.setPartnerIdIn(
				UserServiceHelper.returnPartnerIds(SystemContextHolder.getContextData().getUser().getLoggedInUserId(),
						false, userFilter.getPartnerIdIn()));
		FiltersValidator validator =
				(FiltersValidator) SpringContext.getApplicationContext().getBean("filtersValidator");
		validator.validate(userFilter, result);
		if (result.hasErrors()) {
			UserResponse userResponse = new UserResponse();
			userResponse.setErrors(validator.getErrorDetailFromBindingResult(result));
			return userResponse;
		} else {
			userInfoSearchHandler.initData(userFilter, new UserResponse());
			return userInfoSearchHandler.getResponse();
		}

	}


	@RequestMapping(value = "/users/cache", method = RequestMethod.POST)
	protected UserResponse getUserList(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody UserCacheFilter userCacheFilter) throws Exception {
		userCacheFilter.setUserIds(UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), userCacheFilter.getUserIds()));
		UserResponse userResponse = new UserResponse();
		userResponse.setUsers(new ArrayList<>());
		userResponse.setUsers(userService.find(userCacheFilter));

		if (BooleanUtils.isTrue(userCacheFilter.getFetchUserDues())
				&& CollectionUtils.isNotEmpty(userResponse.getUsers())) {
			for (User user : userResponse.getUsers()) {
				BigDecimal balance =
						payService
								.getUserBalanceFromCache(PaymentMetaInfo.builder()
										.userIds(Arrays.asList(user.getUserId())).fetchCreditLineBalance(false).build())
								.get(user.getUserId());
				/**
				 * Show only usage in case usage is configured otherwise show all type of wallet / creditline etc.
				 */
				if (user.getDailyUsageInfo() != null) {
					user.setTotalBalance(user.getDailyUsageInfo().getBalance());
				} else {
					List<CreditLine> creditLines = payService.fetchCreditLineFromCache(user.getParentUserId());

					if (CollectionUtils.isNotEmpty(creditLines)) {
						for (CreditLine cline : creditLines) {
							balance = balance.add(cline.getBalance());
						}
					}
					user.setTotalBalance(balance.doubleValue());
					user.setBalanceSummary(payService.getUserBalanceSummary(user.getParentUserId()));
					user.setDueSummary(payService.getUserDueSummary(user.getParentUserId()));
				}
				user.setBalance(balance.doubleValue());
			}
		}

		return userResponse;
	}

	@RequestMapping(value = "/user-details/{userId}", method = RequestMethod.GET)
	protected @ResponseBody UserDetailsResponse fetchUserDetails(HttpServletRequest request,
			HttpServletResponse response, @PathVariable("userId") String userId) throws Exception {
		return linkedUserHandler.getUserDetails(userId);
	}

	@RequestMapping(value = "/sso/user-details/", method = RequestMethod.POST)
	protected @ResponseBody SSOUserDetailResponse fetchUserDetailsForSSO(HttpServletRequest request,
			HttpServletResponse response, @RequestBody User user) throws Exception {
		return userFetchHandler.getUserDetails(user.getTempToken());
	}

}
