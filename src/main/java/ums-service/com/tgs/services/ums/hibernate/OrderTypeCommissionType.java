package com.tgs.services.ums.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.cms.datamodel.OrderTypeCommission;

public class OrderTypeCommissionType extends CustomUserType {
    @Override
    public Class returnedClass() {
        return OrderTypeCommission.class;
    }
}
