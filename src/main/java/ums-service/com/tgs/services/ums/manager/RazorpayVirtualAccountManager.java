package com.tgs.services.ums.manager;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang.StringUtils.left;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.razorpay.Customer;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.razorpay.VirtualAccount;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.BankAccountInfo;
import com.tgs.services.base.datamodel.RazorpayConfiguration;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RazorpayVirtualAccountManager {

	private static final String BANK_NAME = "Razorpay";

	@Autowired
	private UserService userService;

	@Autowired
	private GeneralServiceCommunicator gsComm;

	public void createVirtualAccounts(String razorpayId) {
		List<DbUser> users = usersWithoutRazorpayVA();
		log.info("Creating virtual accounts for {} users", users.size());

		RazorpayConfiguration razorpayConfiguration = getRazorpayConfiguration(razorpayId);
		RazorpayClient razorpayClient = createRazorpayClient(razorpayConfiguration);

		users.forEach(user -> {
			BankAccountInfo virtualAccountInfo = createVirtualAccount(user, razorpayClient);
			if (user.getAdditionalInfo().getDepositBankAccounts() == null) {
				user.getAdditionalInfo().setDepositBankAccounts(new ArrayList<>());
			}
			user.getAdditionalInfo().getDepositBankAccounts().add(virtualAccountInfo);
			userService.save(user);
		});
	}

	private List<DbUser> usersWithoutRazorpayVA() {
		List<DbUser> enabledUsers =
				userService.search(UserFilter.builder().statuses(Lists.newArrayList(UserStatus.ENABLED)).build());
		return enabledUsers.stream().filter(user -> {
			if (user.getAdditionalInfo().getDepositBankAccounts() == null) {
				return true;
			}
			boolean hasRazorpayBank = false;
			for (BankAccountInfo bankAccountInfo : user.getAdditionalInfo().getDepositBankAccounts()) {
				hasRazorpayBank |= BANK_NAME.equals(bankAccountInfo.getBankName());
			}
			return !hasRazorpayBank;
		}).collect(toList());
	}

	private RazorpayConfiguration getRazorpayConfiguration(String razorpayId) {
		ClientGeneralInfo clientGeneralInfo =
				(ClientGeneralInfo) gsComm.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO).getOutput();
		return clientGeneralInfo.getExternalPaymentNotificationConfiguration().getRazorpayConfigurations().stream()
				.filter(conf -> razorpayId.equals(conf.getRazorpayId())).findFirst().orElse(null);
	}

	private RazorpayClient createRazorpayClient(RazorpayConfiguration razorpayConfiguration) {
		try {
			return new RazorpayClient(razorpayConfiguration.getApiKey(), razorpayConfiguration.getApiSecret());
		} catch (RazorpayException e) {
			throw new RuntimeException(e);
		}
	}

	private BankAccountInfo createVirtualAccount(DbUser user, RazorpayClient razorpayClient) {
		try {
			log.debug("Creating virtual account for userId {}", user.getUserId());
			Customer customer = createCustomer(user, razorpayClient);
			String customerId = customer.toJson().getString("id");
			JSONObject request = new JSONObject();
			JSONObject receivers = new JSONObject();
			JSONArray receiverTypes = new JSONArray();
			receiverTypes.put("bank_account");
			receivers.put("types", receiverTypes);
			request.put("receivers", receivers);
			request.put("customer_id", customerId);
			VirtualAccount virtualAccount = razorpayClient.VirtualAccounts.create(request);

			JSONObject firstReceiver = virtualAccount.toJson().getJSONArray("receivers").getJSONObject(0);
			String name = firstReceiver.getString("name");
			String accountNumber = firstReceiver.getString("account_number");
			String ifsc = firstReceiver.getString("ifsc");
			return BankAccountInfo.builder().bankName(BANK_NAME).accountNumber(accountNumber).ifscCode(ifsc)
					.accountHolderName(name).build();
		} catch (RazorpayException e) {
			log.error("Failed to create virtual account for userId {} due to ", user.getUserId(), e);
			throw new RuntimeException(e);
		}
	}

	private Customer createCustomer(DbUser user, RazorpayClient razorpayClient) {
		try {
			JSONObject request = new JSONObject();
			request.put("name", left(user.getName(), 50));
			request.put("email", user.getEmail());
			request.put("contact", user.getMobile());
			/**
			 * If a customer with the same details already exists, fetch details of existing customer
			 */
			request.put("fail_existing", "0");
			return razorpayClient.Customers.create(request);
		} catch (RazorpayException e) {
			log.error("Failed to create customer for userId {} due to ", user.getUserId(), e);
			throw new RuntimeException(e);
		}
	}
}
