package com.tgs.services.ums.dbmodel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.ums.datamodel.fee.UserFeeAmount;

public class UserFeeAmountType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return UserFeeAmount.class;
    }
}
