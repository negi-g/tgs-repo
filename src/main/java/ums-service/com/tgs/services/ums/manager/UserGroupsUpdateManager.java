package com.tgs.services.ums.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.restmodel.UserResponse;
import com.tgs.services.ums.restmodel.bulkupload.UserUpdateRequest;
import com.tgs.services.ums.servicehandler.UserUpdateHandler;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserGroupsUpdateManager {
	

	public <T extends UserUpdateRequest> BulkUploadResponse updateUsers(T userUpdateRequest) throws Exception {
		if (userUpdateRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(userUpdateRequest.getUploadId());
		}
		String jobId = BulkUploadUtils.generateRandomJobId();
		final ContextData contextData = SystemContextHolder.getContextData();
		Runnable saveUserGroupsTask = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(1);
			userUpdateRequest.getUpdateQuery().forEach(updateQuery -> {
				Future<?> f = executor.submit(() -> {
					SystemContextHolder.setContextData(contextData);
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(updateQuery.getRowId());
					try {
						saveUser(updateQuery.getUserFromRequest(), userUpdateRequest);
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (Exception e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error("Unable to update user with userid {} ", updateQuery.getUserId(), e);
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			});
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, BulkUploadUtils.INITIALIZATION_NOT_REQUIRED);
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting user groups for {} ", jobId, e);
			}
		};
		return BulkUploadUtils.processBulkUploadRequest(saveUserGroupsTask, userUpdateRequest.getUploadType(),
				jobId);
	}
	
	private <T extends UserUpdateRequest> void saveUser(User user, T userUpdateRequest) throws Exception {
		try {
			UserUpdateHandler updateHandler =
					(UserUpdateHandler) SpringContext.getApplicationContext().getBean("userUpdateHandler");
			updateHandler.initData(user, new UserResponse());
			UserResponse response = updateHandler.getResponse();
			if(response.getErrors() != null) {
				String errorMessage = response.getErrors().stream().map(err -> err.getMessage()).collect(Collectors.toSet()).toString();
				throw new CustomGeneralException(errorMessage);
			}
		} catch(Exception e) {
			throw new CustomGeneralException(e.getMessage());
		}
	}
}
