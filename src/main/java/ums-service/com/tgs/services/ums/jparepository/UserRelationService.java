package com.tgs.services.ums.jparepository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.ums.dbmodel.DbUserRelation;
import com.tgs.services.ums.helper.UserRelationshipHelper;

@Service
public class UserRelationService extends SearchService<DbUserRelation> {

	@Autowired
	private UserRelationRepository relationsRepo;

	@PersistenceContext
	EntityManager em;

	public DbUserRelation save(DbUserRelation userRelation) {
		userRelation = relationsRepo.saveAndFlush(userRelation);
		return userRelation;
		// This code is not required in current scenario for any of the clients.
		/*
		 * String userId1 = relations.getUserId1(); List<DbUserRelation> existingRelations =
		 * relationsRepo.findByUserId2(userId1); if (!existingRelations.isEmpty()) { existingRelations.forEach(relation
		 * -> { DbUserRelation relationship =
		 * ObjectUtils.firstNonNull(relationsRepo.findByUserId1AndUserId2(relation.getUserId1(),
		 * relations.getUserId2()), new DbUserRelation()); relationship.setUserId2(relations.getUserId2());
		 * relationship.setUserName2(relations.getUserName2()); relationship.setDepth(relation.getDepth() + 1);
		 * relation.setPriority(relation.getPriority() + 1); relationship.setUserId1(relation.getUserId1());
		 * relationship.setUserName1(relation.getUserName1()); relationsRepo.saveAndFlush(relationship); }); }
		 * relationHelper.storeUserRelationships(UserRelationFilter.builder()
		 * .userId1In(Collections.singletonList(relations.getUserId1())).depth(1).build(),CacheSetName.
		 * USER_RELATION_USERID1.getName()); relationHelper.storeUserRelationships(UserRelationFilter.builder()
		 * .userId2In(Collections.singletonList(relations.getUserId2())).depth(1).build(),CacheSetName.
		 * USER_RELATION_USERID2.getName());
		 */
	}

	@Override
	public List<DbUserRelation> search(QueryFilter filter) {
		return super.search(filter, relationsRepo);
	}

	public DbUserRelation findByUserId1AndDepthAndPriority(String userId1, int depth, int priority) {
		// Assuming depth =1 in current scenario
		// return relationsRepo.findByUserId1AndDepthAndPriority(userId1, depth, priority);
		return relationsRepo.findByUserId1AndPriority(userId1, priority);
	}

	public DbUserRelation findByUserId1AndDepthAndUserId2(String userId1, int depth, String userId2) {
		// return relationsRepo.findByUserId1AndDepthAndUserId2(userId1, depth, userId2);
		return relationsRepo.findByUserId1AndUserId2(userId1, userId2);
	}

	public DbUserRelation findByUserId1AndUserId2(String userId1, String userId2) {
		return relationsRepo.findByUserId1AndUserId2(userId1, userId2);
	}

	public List<DbUserRelation> findByUserId1(String userId) {
		return relationsRepo.findByUserId1(userId);
	}
	
	public List<DbUserRelation> findByIdBetween(Long from, Long to){
		return relationsRepo.findByIdBetween(from, to);
	}
	
	public Object[] findMinAndMaxIdUserRelation(){
		return relationsRepo.findMinAndMaxId();
	}

	public void delete(DbUserRelation relation) {
		relationsRepo.delete(relation);
		UserRelationshipHelper.deleteFromCacheForUserId1(relation);
		UserRelationshipHelper.deleteFromCacheForUserId2(relation);
	}

}
