package com.tgs.services.ums.servicehandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.JWTHelper;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserDetails;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.restmodel.SignInResponse;
import com.tgs.services.ums.restmodel.UserDetailsResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Getter
@Setter
@Slf4j
public class ToggleUserHandler extends ServiceHandler<String, SignInResponse> {
	
	private DbUser dbuser;
	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder passEncoder;

	@Autowired
	PaymentServiceCommunicator payService;
	
	@Autowired
	UserServiceCommunicator serviceComm;
	
	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws Exception {
		dbuser = userService.findByUserId(request);
		if (dbuser == null) {
			throw new CustomGeneralException(SystemError.INVALID_USERID);
		}

		User loggedInUser = contextData.getUser();

		User user = dbuser.toDomain();

		if (user == null) {
			response.addError(new ErrorDetail(SystemError.INVALID_USERID));
		} else if (!user.getStatus().equals(UserStatus.ENABLED)) {
			response.addError(new ErrorDetail(SystemError.INACTIVE_ACCOUNT));
		}
		
		Set<String> userIds = loggedInUser.getAdditionalInfo().getLinkedUserIds();
		if(CollectionUtils.isEmpty(userIds) || !userIds.contains(dbuser.getUserId())) {
			response.addError(SystemError.INVALID_LINKED_USERID.getErrorDetail(dbuser.getUserId()));
		}
			
		if (CollectionUtils.isEmpty(response.getErrors())) {
			ServiceUtils.updateUserInfoforJWTToken(user, payService, null);
			user.setEmulateUser(loggedInUser.getEmulateUser());
			response.setUser(user);
			String accessToken = JWTHelper.generateAndStoreAccessToken(user,
					SystemContextHolder.getContextData().getHttpResponse());
			response.setAccessToken(accessToken);
		}		
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public UserDetailsResponse getUserDetails(String userId) {
		List<UserDetails> userDetailsList = new ArrayList<>();
		User user = serviceComm.getUserFromCache(userId);
		if(user != null) {
			Set<String> linkedUserIds = user.getAdditionalInfo().getLinkedUserIds();
			userDetailsList.add(buildUserDetails(user));
			for(String linkedUserId: linkedUserIds) {
				User linkedUser = serviceComm.getUserFromCache(linkedUserId);
				if(linkedUser != null) {
					userDetailsList.add(buildUserDetails(linkedUser));
				}
			}
		}
		return new UserDetailsResponse(userDetailsList);
	}

	public UserDetails buildUserDetails(User user) {
		UserDetails userDetails = UserDetails.builder().username(user.getName())
				.createdOn(user.getCreatedOn())
				.userId(user.getUserId())
				.allowedProducts(user.getAdditionalInfo().getAllowedProducts())
				.userBalanceSummary(payService.getUserBalanceSummary(user.getUserId()))
				.userDuesSummary(payService.getUserDueSummary(user.getUserId()))
				.build();
		return userDetails;
	}

}
