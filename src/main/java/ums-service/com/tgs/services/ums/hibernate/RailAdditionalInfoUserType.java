package com.tgs.services.ums.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.ums.datamodel.RailAdditionalInfo;

public class RailAdditionalInfoUserType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return RailAdditionalInfo.class;
	}

}
