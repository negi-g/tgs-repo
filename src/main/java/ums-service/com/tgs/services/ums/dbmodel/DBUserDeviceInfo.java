package com.tgs.services.ums.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.DeviceInfo;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.ums.datamodel.UserDeviceInfo;
import com.tgs.services.ums.hibernate.DeviceInfoType;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "userdeviceinfo")
@Table(name = "userdeviceinfo")
@TypeDefs({ @TypeDef(name = "DeviceInfoType", typeClass = DeviceInfoType.class) })
@Getter
@Setter
public class DBUserDeviceInfo extends BaseModel<DBUserDeviceInfo, UserDeviceInfo> {

	@Column
	private String userId;
	@Column
	@Type(type = "DeviceInfoType")
	private DeviceInfo deviceInfo;
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Override
	public UserDeviceInfo toDomain() {
		return new GsonMapper<>(this, UserDeviceInfo.class).convert();
	}

	@Override
	public DBUserDeviceInfo from(UserDeviceInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DBUserDeviceInfo.class).convert();
	}
}
