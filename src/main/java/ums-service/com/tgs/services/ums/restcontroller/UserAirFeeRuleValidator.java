package com.tgs.services.ums.restcontroller;

import java.util.List;

import com.tgs.services.base.ruleengine.UserAirFeeRuleCriteria;
import com.tgs.services.base.ruleengine.UserFeeRuleCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.validator.ListValidator;

@Component
public class UserAirFeeRuleValidator {
	
	@Autowired
	FMSCommunicator fmsCommunicator;
	
	@Autowired
	ListValidator listValidator;

	public void validateCriteria(Errors errors, String fieldName, UserFeeRuleCriteria criteria) {
		if (criteria instanceof UserAirFeeRuleCriteria) {
			UserAirFeeRuleCriteria airFeeRuleCriteria = (UserAirFeeRuleCriteria) criteria;
			if (airFeeRuleCriteria != null) {
				listValidator.validateAirlines(airFeeRuleCriteria.getAirLineList(), fieldName +".airLineList",
						errors, SystemError.INVALID_FBRC_AIRLINE);

				List<RouteInfo> routeInfos = airFeeRuleCriteria.getRouteInfoList();
				if (!TgsCollectionUtils.isEmpty(routeInfos)) {
					int i = 0;
					for (RouteInfo routeInfo : routeInfos) {
						String routeFieldName = fieldName + ".routeInfoList[" + i++ + "].";
						if (routeInfo != null) {
							if (routeInfo.getFromCityOrAirport() != null && fmsCommunicator
									.getAirportInfo(routeInfo.getFromCityOrAirport().getCode()) == null) {
								rejectValue(errors, routeFieldName + "fromCityOrAirport", SystemError.INVALID_AIRPORT);
							}
							if (routeInfo.getToCityOrAirport() != null && fmsCommunicator
									.getAirportInfo(routeInfo.getToCityOrAirport().getCode()) == null) {
								rejectValue(errors, routeFieldName + "toCityOrAirport", SystemError.INVALID_AIRPORT);
							}
						}
					}
				}
			}
		}
	}
	
	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
