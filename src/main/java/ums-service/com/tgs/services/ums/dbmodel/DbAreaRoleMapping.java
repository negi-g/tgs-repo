package com.tgs.services.ums.dbmodel;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.AreaRoleMappingInfo;
import com.tgs.services.ums.hibernate.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "arearolemap")
@Table(name = "arearolemap")
@TypeDefs({
        @TypeDef(name = "AreaRoleMappingInfoType", typeClass = AreaRoleMappingInfoType.class),
})
public class DbAreaRoleMapping extends BaseModel<DbAreaRoleMapping, AreaRoleMapping> {

    @Column
    private String areaRole;

    @Column
    @Type(type = "AreaRoleMappingInfoType")
    private AreaRoleMappingInfo mappingInfo;


    public AreaRoleMapping toDomain() {
        return new GsonMapper<>(this, AreaRoleMapping.class).convert();
    }

    protected DbAreaRoleMapping from(AreaRoleMapping dataModel) {
        return new GsonMapper<>(dataModel, this, DbAreaRoleMapping.class).convert();
    }
}
