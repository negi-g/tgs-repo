package com.tgs.services.ums.servicehandler;


import com.tgs.filters.UserFilter;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.SortByAttributes;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.restmodel.UserResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class UserInfoSearchHandler extends ServiceHandler<UserFilter, UserResponse> {

	@Autowired
	private UserService usrService;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		log.debug("[UserListing] In UserFetchHandler ");
		List<DbUser> userList = new ArrayList<>();

		for (int i = 0; i < 500; i++) {

			PageAttributes pageAttr = new PageAttributes();
			pageAttr.setPageNumber(i);
			pageAttr.setSize(5000);
			SortByAttributes srtBy = new SortByAttributes();
			srtBy.setParams(Arrays.asList("id"));
			srtBy.setOrderBy("asc");
			pageAttr.setSortByAttr(Arrays.asList(srtBy));
			request.setPageAttr(pageAttr);
			List<DbUser> userChunk = usrService.search(request);
			if (CollectionUtils.isEmpty(userChunk))
				break;
			userList.addAll(userChunk);
		}

		List<User> users = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(userList)) {
			userList.forEach(dbuser -> {
				User user = User.builder().userId(dbuser.getUserId()).parentUserId(dbuser.getParentUserId())
						.name(dbuser.getName()).partnerId(dbuser.getPartnerId()).build();
				users.add(user);
			});
			response.setUsers(users);
		}
	}

	@Override
	public void afterProcess() throws Exception {}

}
