package com.tgs.services.ums.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.ums.dbmodel.DBUserDeviceInfo;

@Repository
public interface UserDeviceInfoRepository
		extends JpaRepository<DBUserDeviceInfo, Long>, JpaSpecificationExecutor<DBUserDeviceInfo> {

}
