package com.tgs.services.ums.servicehandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.runtime.ActionChangeValidator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.FieldTransform;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.ums.datamodel.ApplicationArea;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.DailyUsageInfo;
import com.tgs.services.ums.datamodel.RailAdditionalInfo;
import com.tgs.services.ums.datamodel.RailApplicationStatus;
import com.tgs.services.ums.datamodel.RailApplicationSubStatus;
import com.tgs.services.ums.datamodel.RailFlow;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.helper.UserHelper;
import com.tgs.services.ums.helper.UserRelationshipHelper;
import com.tgs.services.ums.jparepository.AreaRoleMapService;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.manager.EmployeeUpdateManager;
import com.tgs.services.ums.restmodel.UserResponse;
import com.tgs.utils.exception.ResourceNotFoundException;

@Service
public class UserUpdateHandler extends ServiceHandler<User, UserResponse> {

	@Autowired
	private UserService service;

	@Autowired
	private UserHelper userHelper;

	@Autowired
	private AreaRoleMapService areaRoleMapService;

	@Autowired
	private MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	private GeneralServiceCommunicator gsCommunicator;

	@Autowired
	ActionChangeValidator entityValidator;

	DbUser dbUser;

	List<String> onBoardingCcEmailIds;

	List<String> savedGroupIds;

	boolean isApiKeyUpdate;

	@Override
	public void beforeProcess() throws Exception {
		dbUser = service.findByUserId(request.getUserId());
		if (dbUser == null) {
			throw new ResourceNotFoundException(SystemError.INVALID_USERID);
		}
		if (request.getRole() != null && !dbUser.getRole().equals(request.getRole().getCode())
				&& !isRoleChangeAllowed()) {
			throw new CustomGeneralException(SystemError.ROLE_CHANGE_NOT_ALLOWED);
		}

		if (request.getEmail() != null && !dbUser.getEmail().equals(request.getEmail())) {
			if (service.findByEmail(request.getEmail()) != null)
				throw new CustomGeneralException(SystemError.EMAIL_EXISTS);
		}

		if (request.getMobile() != null && !dbUser.getMobile().equals(request.getMobile())) {
			if (service.findByMobile(request.getMobile()) != null)
				throw new CustomGeneralException(SystemError.MOBILE_EXISTS);
		}

		if (request.getAdditionalInfo() != null && request.getAdditionalInfo().getDailyUsageLimit() != null) {
			if (StringUtils.isBlank(dbUser.getParentUserId())) {
				throw new CustomGeneralException(SystemError.INVALID_LIMIT_ACTION);
			}
		}

		if (UserUtils.isB2CUser(dbUser.toDomain()) && request.getUserConf() != null
				&& MapUtils.isNotEmpty(request.getUserConf().getCommPlanMap())) {
			throw new CustomGeneralException(SystemError.INVALID_COMMPLAN_ACTION);
		}
		savedGroupIds = dbUser.getAdditionalInfo().getGroups();
		if (request.getAdditionalInfo() != null
				&& CollectionUtils.isNotEmpty(request.getAdditionalInfo().getGroups())) {
			ClientGeneralInfo clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
			Integer groupLimit = clientInfo.getUserAssignedGroupLimit();
			Integer userLimit = clientInfo.getGroupAssignedUserLimit();
			Map<String, Integer> excludedGroupsFromLimit = clientInfo.getExcludedGroupFromLimit();
			List<String> userGroups = request.getAdditionalInfo().getGroups().stream()
					.filter(gp -> StringUtils.isNotBlank(gp)).distinct().collect(Collectors.toList());
			if (userGroups.size() > groupLimit) {
				response.addError(SystemError.GROUP_LIMIT_EXCEEDED.getErrorDetail(groupLimit));
			}
			for (String group : userGroups) {
				if (excludedGroupsFromLimit.get(group) != null) {
					userLimit = excludedGroupsFromLimit.get(group);
				}
				List<String> userIds = service.getGroupInfo(group);
				if (userIds.size() >= userLimit && !userIds.contains(dbUser.getUserId())) {
					response.addError(SystemError.USER_LIMIT_EXCEEDED.getErrorDetail(userLimit, group));
				}
			}
		}
		List<Product> allowedProducts = request.getAdditionalInfo().getAllowedProducts();
		if (request.getAdditionalInfo() != null && CollectionUtils.isNotEmpty(allowedProducts)
				|| (dbUser.getAdditionalInfo().getAllowedProducts() != null && allowedProducts != null)) {
			Set<String> userIds = dbUser.getAdditionalInfo().getLinkedUserIds();
			try {
				if (allowedProducts.contains(Product.NA) && allowedProducts.size() > 1) {
					throw new CustomGeneralException(SystemError.WITH_NA_NO_OTHER_PRODUCTS_ALLOWED);
				}
				ServiceUtils.isProductsAllowed(allowedProducts, userIds, response);
			} catch (CustomGeneralException e) {
				response.addError(e.getError().getErrorDetail());
			}
		}

		if (!dbUser.getRole().equals(UserRole.CORPORATE.getCode())
				&& StringUtils.isNotEmpty(request.getAdditionalInfo().getCcEmailIds())) {
			throw new CustomGeneralException(SystemError.ADDITIONAL_EMAIL_NOT_ALLOWED);
		}

		if (StringUtils.isNotEmpty(request.getUserConf().getDistributorId())) {
			User distributor = UserHelper.getUserFromCache(request.getUserConf().getDistributorId());
			if (distributor == null || !distributor.getRole().equals(UserRole.DISTRIBUTOR)) {
				throw new ResourceNotFoundException(SystemError.INVALID_DISTRIBUTIORID);
			}
		}
		UserRole role = SystemContextHolder.getContextData().getUser().getRole();
		List<AreaRoleMapping> mappingList =
				areaRoleMapService.getMappingsByAreaRoleIn(ApplicationArea.USER_FIELDS.getAreaRoles());
		Map<UserRole, Set<String>> userRoleMap = AreaRoleMapping.getMapOfFieldsByUserRole(mappingList);
		Set<String> allowedFields = userRoleMap.containsKey(role) ? userRoleMap.get(role) : null;
		List<String> fieldsInRequest = TgsObjectUtils.getNotNullFields(request);

		if (CollectionUtils.isNotEmpty(allowedFields)) {
			fieldsInRequest.removeAll(allowedFields);
			if (CollectionUtils.isNotEmpty(fieldsInRequest)) {
				response.addError(SystemError.INVALID_FIELDS.getErrorDetail(fieldsInRequest.toString()));
			}
		}

		if (dbUser.getRole().equals(UserRole.AGENT.getCode()) && request.getStatus() != null) {
			List<String> dbUserFields = TgsObjectUtils.getNotNullFields(dbUser);
			validateEntity(dbUserFields, dbUser.getStatus(), request.getStatus().getCode());
		}
	}

	private void validateEntity(List<String> fieldsInRequest, String fromStatus, String toStatus) {
		Map<String, Set<User>> relations =
				UserRelationshipHelper.getUserRelationsForUserId1(Arrays.asList(request.getUserId()));
		Set<User> userRelations = relations.get(request.getUserId());
		if (userRelations != null) {
			fieldsInRequest.add("relations");
		}
		List<String> fields = entityValidator.isValidRequest(fieldsInRequest, dbUser.getStatus(),
				request.getStatus().getCode(), request.getClass().getSimpleName());
		if (CollectionUtils.isNotEmpty(fields))
			response.addError(SystemError.ENABLE_ENTITY.getErrorDetail(fields.toString()));
	}

	private boolean isRoleChangeAllowed() {
		// Only admin is allowed to change role
		User loggedInUser = SystemContextHolder.getContextData().getUser();
		List<UserRole> midOfficeRolesList = UserRole.getMidOfficeRoles().stream().collect(Collectors.toList());
		midOfficeRolesList.remove(UserRole.SALES);
		midOfficeRolesList.remove(UserRole.SALES_SUPPORT);
		UserRole dbRole = UserRole.getEnumFromCode(dbUser.getRole());
		if (UserRole.ADMIN.equals(loggedInUser.getRole()) && midOfficeRolesList.contains(request.getRole())
				&& midOfficeRolesList.contains(dbRole))
			return true;
		return false;
	}

	@Override
	public void process() throws Exception {
		String previousStatus = dbUser.getStatus();
		String previousDistributor = StringUtils.defaultString(dbUser.getUserConf().getDistributorId(), "");
		String previousParent = StringUtils.defaultString(dbUser.getParentUserId(), "");
		dbUser = Optional.ofNullable(dbUser).orElseGet(() -> new DbUser());
		dbUser = dbUser.from(request);
		dbUser.setEmail(dbUser.getEmail().toLowerCase());
		if (StringUtils.isNotEmpty(previousParent) && !previousParent.equals(dbUser.getParentUserId())) {
			throw new CustomGeneralException(SystemError.PARENT_CHANGE_NOT_ALLOWED);
		}
		if (!previousDistributor.equals(dbUser.getUserConf().getDistributorId())) {
			if (StringUtils.isNotEmpty(dbUser.getUserConf().getDistributorId()))
				dbUser.getUserConf().setMigrationDate(LocalDateTime.now());
			else
				dbUser.getUserConf().setMigrationDate(null);
			// This user should not be visible to previousDistribuor
			if (StringUtils.isNotEmpty(previousDistributor))
				updateCacheOfPrevDistributor(previousDistributor);
		}
		updateProfileFields(dbUser);
		updateRailAgentProfileDetails(dbUser);
		isApiKeyUpdate = checkForApiUser(dbUser);
		service.save(dbUser);
		savedGroupIds.removeAll(dbUser.getAdditionalInfo().getGroups());
		if (CollectionUtils.isNotEmpty(savedGroupIds)) {
			service.removeUserIdFromGroup(dbUser.getUserId(), savedGroupIds);
		}
		updateDailyUsage(dbUser);
		updateLogo(dbUser);
		updateTdsRate(dbUser);

		if (request.getStatus() != null && request.getStatus() == UserStatus.ENABLED
				&& !request.getStatus().getStatus().equals(previousStatus)) {
			sendActivationEmail(dbUser);
			sendActivationSms(dbUser);
			sendSms(dbUser, SmsTemplateKey.USER_ACTIVATION_2_SMS);
		} else if (request.getStatus() != null && UserStatus.ENABLED.getCode().equals(previousStatus)
				&& !request.getStatus().getStatus().equals(previousStatus)) {
			// Removes users jwt from the system.
			UserHelper.removeUserJWTTokens(dbUser.getUserId());

			/**
			 * If any user is disable than remove all active child tokens as well
			 */
			List<DbUser> childUsers = service.search(UserFilter.builder().parentUserId(dbUser.getUserId()).build());
			if (CollectionUtils.isNotEmpty(childUsers)) {
				for (DbUser dbuser : childUsers) {
					UserHelper.removeUserJWTTokens(dbuser.getUserId());
				}
			}

			sendSms(dbUser, SmsTemplateKey.USER_DEACTIVATION_SMS);
		}
	}

	private void updateRailAgentProfileDetails(DbUser user) {
		/**
		 * Case 1: When the User is on screen 1, Application Status : NEW,
		 * 
		 * Case 2 : When the User is on screen 2 , Application Status : PAID,
		 * 
		 * Case 3 : When User is on screen 3 (Documents Upload) , Application Status : KYC_PENDING,
		 * 
		 * Case 4 : KYC verification done for all documents , Application Status : KYC_COMPLETE,
		 * 
		 * Case 5 : Agent DSC Details Submitted , Application Status : ACTIVATION_PENDING,
		 * 
		 * Case 6 : Agent MacId, IMEI No, submitted , Application Status : COMPLETE
		 */
		if (user.getRailAdditionalInfo() != null) {
			RailAdditionalInfo railAdditionalInfo = user.getRailAdditionalInfo();
			railAdditionalInfo.setApplicationStatus(RailApplicationStatus.getApplicationStatus(railAdditionalInfo,
					SystemContextHolder.getContextData().getUser()));

			if (railAdditionalInfo.getApplicationStatus() != null) {
				if (railAdditionalInfo.getOnboardingStartDate() == null) {
					railAdditionalInfo.setOnboardingStartDate(LocalDateTime.now());
					user.setRailOnboardingStartDate(LocalDateTime.now());
				}
				// user.setApplStatus(railAdditionalInfo.getApplicationStatus().getCode());
				if (UserUtils.isMidOfficeRole(SystemContextHolder.getContextData().getUser().getRole())) {
					updateApplicationStatus(user);
				}
			}
		}
	}

	/**
	 * This method will update the application status , once agent submitted his/her details, MidOffice will verify
	 * those details and will update the status accordingly, it can be Rejected/Verified/Pending
	 * 
	 * @param user
	 */
	private void updateApplicationStatus(DbUser user) {
		RailAdditionalInfo railAdditionalInfo = user.getRailAdditionalInfo();

		if (railAdditionalInfo != null) {
			RailApplicationStatus status = railAdditionalInfo.getApplicationStatus();
			if (RailApplicationSubStatus.REJECTED.equals(railAdditionalInfo.getKycStatus())
					|| RailApplicationSubStatus.REJECTED.equals(railAdditionalInfo.getDscStatus())
					|| RailApplicationSubStatus.REJECTED.equals(railAdditionalInfo.getMrStatus())) {
				status = RailApplicationStatus.REJECTED;
			} else if (CollectionUtils.isNotEmpty(railAdditionalInfo.getAllowedRailFlows())
					&& railAdditionalInfo.getAllowedRailFlows().contains(RailFlow.DSC)) {
				if (RailApplicationSubStatus.KYC_VERIFIED.equals(railAdditionalInfo.getKycStatus())
						&& RailApplicationSubStatus.DSC_APPROVED.equals(railAdditionalInfo.getDscStatus())
						&& RailApplicationSubStatus.MR_APPROVED.equals(railAdditionalInfo.getMrStatus())) {
					status = RailApplicationStatus.COMPLETE;
				}
			} else {
				if (RailApplicationSubStatus.KYC_VERIFIED.equals(railAdditionalInfo.getKycStatus())
						&& RailApplicationSubStatus.MR_APPROVED.equals(railAdditionalInfo.getMrStatus())) {
					status = RailApplicationStatus.COMPLETE;
				}
			}
			railAdditionalInfo.setApplicationStatus(status);

		}

	}

	private void updateProfileFields(DbUser user) {
		if (MapUtils.isNotEmpty(user.getUserProfile().getData())) {
			String[] splits = user.getName().split(" ");
			String first = splits[0];
			String last = splits.length > 1 ? splits[1] : "";
			user.getUserProfile().getData().put(EmployeeUpdateManager.FIRST_NAME, first);
			user.getUserProfile().getData().put(EmployeeUpdateManager.LAST_NAME, last);
		}
	}

	private Boolean checkForApiUser(DbUser dbUser) {
		if (Objects.nonNull(dbUser.getUserConf()) && Objects.nonNull(dbUser.getUserConf().getApiConfiguration())
				&& BooleanUtils.isTrue(dbUser.getUserConf().getApiConfiguration().getIsNewKeyRequired())) {
			// generate new Key
			ClientGeneralInfo clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
					GeneralBasicFact.builder().applicableTime(LocalDateTime.now()).build());
			String key = StringUtils.join(dbUser.getUserId(), UUID.randomUUID());
			dbUser.getUserConf().getApiConfiguration().setApiKey(key);
			dbUser.getUserConf().getApiConfiguration().setIsNewKeyRequired(null);
			if (clientInfo != null) {
				dbUser.getUserConf().getApiConfiguration()
						.setAirVersion(clientInfo.getPartnerApiVersionInfo().getAirVersion());
				dbUser.getUserConf().getApiConfiguration()
						.setHotelVersion(clientInfo.getPartnerApiVersionInfo().getHotelVersion());
			}
			return true;
		}
		return false;
	}

	private void updateCacheOfPrevDistributor(String previousDistribuor) {
		userHelper.removeAllowedUserIdsFromCache(previousDistribuor, Collections.singleton(dbUser.getUserId()));
	}

	/**
	 * Whenever there is a change in main agency logo then it should propagate to all of its staff
	 * 
	 * @param dbUser
	 */
	private void updateLogo(DbUser dbUser) {
		if (request.getAdditionalInfo() != null && request.getAdditionalInfo().getLogoURL() != null
				&& !request.getAdditionalInfo().getLogoURL().equals(dbUser.getAdditionalInfo().getLogoURL())) {
			UserFilter userFilter = UserFilter.builder().build();
			userFilter.setUserIds(UserServiceHelper.checkAndReturnAllowedUserId(dbUser.getUserId(), null));
			List<DbUser> childUsers = service.search(userFilter);
			for (DbUser cUser : childUsers) {
				cUser.getAdditionalInfo().setLogoURL(dbUser.getAdditionalInfo().getLogoURL());
				service.save(cUser);
			}

		}
	}

	/**
	 * Whenever there is a change in main agency tds-rate then it should propagate to all of its staff
	 * 
	 * @param dbUser
	 */
	private void updateTdsRate(DbUser dbUser) {
		if (request.getAdditionalInfo() != null && request.getAdditionalInfo().getTdsRate() != null
				&& !request.getAdditionalInfo().getTdsRate().equals(dbUser.getAdditionalInfo().getTdsRate())) {
			UserFilter userFilter = UserFilter.builder().build();
			userFilter.setUserIds(UserServiceHelper.checkAndReturnAllowedUserId(dbUser.getUserId(), null));
			List<DbUser> childUsers = service.search(userFilter);
			for (DbUser cUser : childUsers) {
				cUser.getAdditionalInfo().setTdsRate(dbUser.getAdditionalInfo().getTdsRate());
				service.save(cUser);
			}

		}
	}

	private void updateDailyUsage(DbUser dbUser) {
		if (dbUser.getAdditionalInfo().getDailyUsageLimit() != null) {
			DailyUsageInfo oldUsage = TgsObjectUtils.firstNonNull(() -> UserHelper.getUsageInfo(dbUser.getUserId()),
					() -> UserUtils.getUsageInfo(dbUser.toDomain()));
			double limitDiff = dbUser.getAdditionalInfo().getDailyUsageLimit() - oldUsage.getLimit();
			if (limitDiff != 0) {
				oldUsage.setBalance(oldUsage.getBalance() + limitDiff);
			}
			oldUsage.setLimit(dbUser.getAdditionalInfo().getDailyUsageLimit());
			UserHelper.updateDailyInfo(oldUsage);
		}
	}

	private void sendActivationEmail(DbUser dbUser) {
		AbstractMessageSupplier<EmailAttributes> emailAttributeSupplier =
				new AbstractMessageSupplier<EmailAttributes>() {
					@Override
					public EmailAttributes get() {
						EmailAttributes emailAttributes = EmailAttributes.builder().build();
						emailAttributes.setToEmailId(dbUser.getEmail());
						emailAttributes.setKey(EmailTemplateKey.ACCOUNT_ACTIVATION_EMAIL.name());
						String policy = ObjectUtils
								.firstNonNull(dbUser.getUserConf().getCreditPolicyMap().get(Product.AIR), "N/A");
						Map<String, Set<User>> relations =
								UserRelationshipHelper.getUserRelationsForUserId1(Arrays.asList(dbUser.getUserId()));
						Set<User> userRelations = relations.get(dbUser.getUserId());
						if (userRelations != null) {
							User u = UserHelper.getUserFromCache(userRelations.stream().findFirst().get().getUserId());
							emailAttributes.setSalesRepName(u.getName());
							emailAttributes.setSalesRepMobile(u.getMobile());
							emailAttributes.setSalesRepEmail(u.getEmail());
						}
						emailAttributes.setPolicyInfos(policy);
						emailAttributes.setPartnerId(dbUser.getPartnerId());
						emailAttributes.setRole(UserRole.getUserRole(dbUser.getRole()));
						List<String> onBoardingCcEmailIds = getOnBoardingCCEmails();
						if (BooleanUtils.isTrue(dbUser.getAdditionalInfo().isAutoOnBoarding)
								&& CollectionUtils.isNotEmpty(onBoardingCcEmailIds)) {
							onBoardingCcEmailIds.forEach(emailId -> emailAttributes.setCcEmailId(emailId));
						}
						return emailAttributes;
					}
				};
		msgSrvCommunicator.sendMail(emailAttributeSupplier.getAttributes());
	}

	@Override
	public void afterProcess() throws Exception {
		if (!isApiKeyUpdate)
			FieldTransform.mask(dbUser);
		response.setUsers(new ArrayList<>());
		response.getUsers().add(dbUser.toDomain());
	}

	private void sendActivationSms(DbUser user) {
		AbstractMessageSupplier<SmsAttributes> smsAttributeSupplier = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				Map<String, String> attributes = new HashMap<>();
				String policy =
						ObjectUtils.firstNonNull(user.getUserConf().getCreditPolicyMap().get(Product.AIR), "N/A");
				Map<String, Set<User>> relations =
						UserRelationshipHelper.getUserRelationsForUserId1(Arrays.asList(user.getUserId()));
				Set<User> userRelations = relations.get(user.getUserId());
				if (userRelations != null) {
					User u = UserHelper.getUserFromCache(userRelations.stream().findFirst().get().getUserId());
					attributes.put("salesRep", u.getName());
					attributes.put("contact", u.getMobile().concat(" / ").concat(u.getEmail()));
				}
				attributes.put("policy", policy);
				SmsAttributes smsAttr = SmsAttributes.builder().key(SmsTemplateKey.USER_ACTIVATION_SMS.name())
						.recipientNumbers(Arrays.asList(user.getMobile())).attributes(attributes)
						.partnerId(user.getPartnerId()).role(UserRole.getUserRole(dbUser.getRole())).build();
				return smsAttr;
			}
		};
		msgSrvCommunicator.sendMessage(smsAttributeSupplier.getAttributes());
	}

	private void sendSms(DbUser dbUser, SmsTemplateKey templateKey) {
		AbstractMessageSupplier<SmsAttributes> smsAttributeSupplier = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				SmsAttributes smsAttr = SmsAttributes.builder().key(templateKey.name())
						.recipientNumbers(Arrays.asList(dbUser.getMobile())).attributes(new HashMap<>())
						.partnerId(dbUser.getPartnerId()).role(UserRole.getEnumFromCode(dbUser.getRole())).build();
				return smsAttr;
			}
		};
		msgSrvCommunicator.sendMessage(smsAttributeSupplier.getAttributes());
	}

	public List<String> getOnBoardingCCEmails() {
		if (CollectionUtils.isEmpty(onBoardingCcEmailIds)) {
			ClientGeneralInfo clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
					GeneralBasicFact.builder().applicableTime(LocalDateTime.now()).build());
			if (clientInfo != null)
				onBoardingCcEmailIds = clientInfo.getOnboardingCcEmailds();
		}
		return onBoardingCcEmailIds;
	}
}
