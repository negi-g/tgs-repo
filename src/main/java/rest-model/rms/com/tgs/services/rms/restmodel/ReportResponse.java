package com.tgs.services.rms.restmodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

import com.tgs.services.base.restmodel.BaseResponse;


@Getter
@Setter
@Builder
public class ReportResponse  extends BaseResponse {

    private byte[] byteStream;

    private String url;

    private List<Map<String, Object>> restResponse;

    private Object response;
}
