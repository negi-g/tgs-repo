package com.tgs.services.gms.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import com.tgs.services.gms.datamodel.CollectionUpdateQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CollectionUpdateRequest extends BulkUploadRequest {
	
	private List<CollectionUpdateQuery> updateQuery;

}
