package com.tgs.services.gms.restmodel;

import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class BillingEntityResponse extends BaseResponse {

    private List<GstInfo> billingEntities;

    private List<GstInfo> gstInfo;

    public BillingEntityResponse(List<GstInfo> entityList) {
        billingEntities = new ArrayList<>();
        billingEntities.addAll(entityList);
    }

    public BillingEntityResponse(GstInfo billingEntity) {
        billingEntities = new ArrayList<>();
        billingEntities.add(billingEntity);
    }

    public BillingEntityResponse() { }
}
