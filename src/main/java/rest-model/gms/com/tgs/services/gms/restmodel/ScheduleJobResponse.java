package com.tgs.services.gms.restmodel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.ScheduleJob;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ScheduleJobResponse extends BaseResponse {

	private List<ScheduleJob> jobs;

}
