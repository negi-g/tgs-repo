package com.tgs.services.gms.restmodel;

import java.util.List;

import com.tgs.services.base.datamodel.CityInfo;
import com.tgs.services.base.restmodel.BaseResponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityResponse extends BaseResponse {
	private List<CityInfo> cityInfos;
}
