package com.tgs.services.gms.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.datamodel.TravellerInfo;

public class TravellerInfoResponse extends BaseResponse {

	private List<TravellerInfo> travellerInfos;

	public List<TravellerInfo> getTravellerInfos() {
		if (travellerInfos == null) {
			travellerInfos = new ArrayList<>();
		}
		return travellerInfos;
	}
}
