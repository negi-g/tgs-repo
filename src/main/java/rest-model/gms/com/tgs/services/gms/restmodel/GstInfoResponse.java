package com.tgs.services.gms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.restmodel.BaseResponse;

public class GstInfoResponse extends BaseResponse {

	private List<GstInfo> gstInfo;

	public List<GstInfo> getGstInfos() {
		if (gstInfo == null) {
			gstInfo = new ArrayList<>();
		}
		return gstInfo;
	}

}
