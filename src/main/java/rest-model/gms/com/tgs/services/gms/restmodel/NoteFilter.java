package com.tgs.services.gms.restmodel;

import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.gms.datamodel.NoteType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@Setter
@ApiModel(value = "This is being used to filter notes, You can pass either any one field or combination of fields depending upon your search criteria")
public class NoteFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch information about any particular bookingId ")
	private String bookingId;

	@ApiModelProperty(notes = "To fetch information about any particular bookingId ")
	private String amendmentId;

	@ApiModelProperty(notes = "To fetch all information with specific noteType ")
	private NoteType noteType;

	@ApiModelProperty(notes = "To fetch notes of any particular user ")
	private String userId;

	@ApiModelProperty(notes = "To fetch notes using id ")
	private Long id;

}
