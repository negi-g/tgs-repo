package com.tgs.services.gms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.OtpToken;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OtpTokenResponse extends BaseResponse {

	private OtpToken otpToken;
}
