package com.tgs.services.vms.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelVoucherValidateRequest extends VoucherValidateRequest {

	HotelInfo hotelInfo;

	public HotelVoucherValidateRequest(String bookingId, String voucherCode, Product product,
			List<PaymentMedium> mediums, List<VoucherConfiguration> configurations) {
		this.setBookingId(bookingId);
		this.setVoucherCode(voucherCode);
		this.setMediums(mediums);
		this.setProduct(product);
		this.setConfigurations(configurations);
	}

	public HotelVoucherValidateRequest() {}
}

