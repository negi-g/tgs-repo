package com.tgs.services.vms.restmodel;

import com.tgs.services.fms.datamodel.TripInfo;
import lombok.*;
import java.util.List;

@Getter
@Setter
public class FlightVoucherValidateResponse extends VoucherValidateResponse {

	List<TripInfo> tripInfos;

	public FlightVoucherValidateResponse(Double discountedAmount) {
		this.setDiscountedAmount(discountedAmount);
	}

	public FlightVoucherValidateResponse() {}

}

