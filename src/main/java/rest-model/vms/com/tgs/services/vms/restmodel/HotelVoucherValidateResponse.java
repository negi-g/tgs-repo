package com.tgs.services.vms.restmodel;

import com.tgs.services.hms.datamodel.HotelInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelVoucherValidateResponse extends VoucherValidateResponse {

	HotelInfo hotelInfo;

	public HotelVoucherValidateResponse(Double discountedAmount) {
		this.setDiscountedAmount(discountedAmount);
	}

	public HotelVoucherValidateResponse() {}
}

