package com.tgs.services.vms.restmodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.PaymentMedium;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import java.util.List;


@Getter
@Setter
@Builder
public class VoucherDiscountRequest {

	@NonNull
	private String bookingId;

	@SerializedName("vcode")
	@NonNull
	private String voucherCode;

	@SerializedName("med")
	private List<PaymentMedium> mediums;

}
