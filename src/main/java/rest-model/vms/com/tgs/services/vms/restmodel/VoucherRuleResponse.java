package com.tgs.services.vms.restmodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import lombok.Getter;
import lombok.Setter;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
public class VoucherRuleResponse extends BaseResponse {

	@SerializedName("vc")
	private List<VoucherConfiguration> voucherConfigurations;

	public List<VoucherConfiguration> getVoucherConfigurations() {
		if (voucherConfigurations == null) {
			voucherConfigurations = new ArrayList<>();
		}
		return voucherConfigurations;
	}
}
