package com.tgs.services.vms.restmodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VoucherValidateResponse extends BaseResponse {

	@SerializedName("da")
	Double discountedAmount;

	public Double getDiscountedAmount() {
		if (discountedAmount == null) {
			discountedAmount = Double.valueOf(0);
		}
		return discountedAmount;
	}

	public VoucherValidateResponse() {}


}
