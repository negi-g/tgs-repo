package com.tgs.services.vms.restmodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VoucherDiscountResponse extends BaseResponse {

	@SerializedName("td")
	private Double totalDiscount;
}
