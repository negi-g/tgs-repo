package com.tgs.services.ims.restmodel.air;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ims.datamodel.SeatAllocationInfo;

import lombok.Setter;

@Setter
public class AirSeatAllocationListResponse extends BaseResponse {

	private String inventoryId;

	private List<SeatAllocationInfo> allocationInfo;

	private Product product;

	public List<SeatAllocationInfo> getAllocationInfo() {
		if (allocationInfo == null)
			allocationInfo = new ArrayList<>();
		return allocationInfo;
	}

}
