package com.tgs.services.ims.restmodel.air;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;

public class AirInventoryListResponse extends BaseResponse{
	
	List<AirInventoryTrip> inventoryTrip;
	
	public List<AirInventoryTrip> getInventoryTrip(){
		if(inventoryTrip==null)
			 inventoryTrip = new ArrayList<>();
		return inventoryTrip;
	}

}
