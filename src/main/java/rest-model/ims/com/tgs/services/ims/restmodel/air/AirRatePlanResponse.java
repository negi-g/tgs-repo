package com.tgs.services.ims.restmodel.air;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ims.datamodel.air.AirRatePlan;

import lombok.Setter;

@Setter
public class AirRatePlanResponse extends BaseResponse{

	List<AirRatePlan> ratePlans;

	public List<AirRatePlan> getRatePlans(){
		if (ratePlans == null) {
			ratePlans = new ArrayList<>();
		}
		return ratePlans;
	}

}
