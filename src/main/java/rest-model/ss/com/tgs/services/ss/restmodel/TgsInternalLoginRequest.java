package com.tgs.services.ss.restmodel;

import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TgsInternalLoginRequest {

	@NotNull
	@ApiModelProperty(required = true)
	private String email;

	private String otp;

	private String requestId;
}
