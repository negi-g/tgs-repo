package com.tgs.services.fms.restmodel.alternateclass;


import com.tgs.services.fms.datamodel.alternateclass.AlternateClassFareSearch;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AlternateClassFareSearchRequest {

    List<AlternateClassFareSearch> fareClasses;
}
