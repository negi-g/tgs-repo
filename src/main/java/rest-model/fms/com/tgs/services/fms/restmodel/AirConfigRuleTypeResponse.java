package com.tgs.services.fms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import java.util.ArrayList;
import java.util.List;

public class AirConfigRuleTypeResponse extends BaseResponse {

	private List<AirConfiguratorInfo> airConfiguratorInfos;

	public List<AirConfiguratorInfo> getAirConfiguratorInfos() {
		if (airConfiguratorInfos == null) {
			airConfiguratorInfos = new ArrayList<>();
		}
		return airConfiguratorInfos;
	}

}
