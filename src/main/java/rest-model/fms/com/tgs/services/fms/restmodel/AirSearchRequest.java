package com.tgs.services.fms.restmodel;

import javax.validation.Valid;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@Valid
@NoArgsConstructor
public class AirSearchRequest {

	@Valid
	@NonNull
	private AirSearchQuery searchQuery;

	private String searchId;

	private Boolean isNewFlow;
}
