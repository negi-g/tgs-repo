package com.tgs.services.fms.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SourceRouteBulkUploadRequest extends BulkUploadRequest {
	private List<SourceRouteInfo> sourceRouteInfos;

}
