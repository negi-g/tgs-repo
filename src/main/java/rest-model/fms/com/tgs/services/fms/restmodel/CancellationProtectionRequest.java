package com.tgs.services.fms.restmodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CancellationProtectionRequest {

	private String bookingId;
	
	@SerializedName("ta")
	private Double totalAmount;
}
