package com.tgs.services.fms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SupplierInfoResponse extends BaseResponse {
	private List<SupplierInfo> supplierInfos;

	public List<SupplierInfo> getSupplierInfos() {
		if (supplierInfos == null) {
			supplierInfos = new ArrayList<>();
		}
		return supplierInfos;
	}

}
