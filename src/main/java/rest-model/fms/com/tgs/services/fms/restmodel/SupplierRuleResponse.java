package com.tgs.services.fms.restmodel;

import lombok.Setter;
import java.util.List;
import java.util.ArrayList;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;

@Setter
public class SupplierRuleResponse extends BaseResponse{

	private List<SupplierRule> supplierRules;
	
	public List<SupplierRule> getSupplierRules() {
		if (supplierRules == null) {
			supplierRules = new ArrayList<>();
		}
		return supplierRules;
	}
}
