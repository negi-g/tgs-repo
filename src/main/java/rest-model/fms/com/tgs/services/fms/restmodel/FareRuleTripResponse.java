package com.tgs.services.fms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;

import java.util.LinkedHashMap;
import java.util.Map;

public class FareRuleTripResponse extends BaseResponse {

    private Map<String, FareRuleInformation> fareRule;

    public Map<String, FareRuleInformation> getFareRule() {
        if (fareRule == null) {
            fareRule = new LinkedHashMap<>();
        }
        return fareRule;
    }
}