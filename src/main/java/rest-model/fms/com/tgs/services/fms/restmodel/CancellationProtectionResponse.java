package com.tgs.services.fms.restmodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CancellationProtectionResponse extends BaseResponse {

	private String bookingId;

	@SerializedName("cpp")
	private Double cancellationProtectionCharges;

	@SerializedName("cpt")
	private Double cancellationProtectionTax;

	@SerializedName("cpmf")
	private Double cancellationProtectionManagementFee;

	@SerializedName("cpmft")
	private Double cancellationProtectionManagementFeeTax;

	@SerializedName("cpac")
	private Double cancellationProtectionAgentCommission;

	@SerializedName("cpact")
	private Double cancellationProtectionAgentCommissionTds;

	@SerializedName("tcpc")
	private Double totalCancellationProtectionCharges;
}
