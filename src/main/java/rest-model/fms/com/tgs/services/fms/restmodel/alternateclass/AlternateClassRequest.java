package com.tgs.services.fms.restmodel.alternateclass;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AlternateClassRequest {

    private List<String> priceIds;
}
