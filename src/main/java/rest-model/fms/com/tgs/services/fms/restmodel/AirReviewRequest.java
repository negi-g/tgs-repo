package com.tgs.services.fms.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.fms.restmodel.ReviewAdditionalInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirReviewRequest {

	private String searchId;

	private List<String> priceIds;

	@ApiModelProperty(hidden = true)
	private Boolean sendPriceAlert;

	private String bookingId;

	private Boolean priceValidation;

	@SerializedName("fT")
	private String flowType;

	@SerializedName("spids")
	private List<String> similarPriceIds;

	@SerializedName("lpids")
	private List<String> lowestPriceIds;

	private String tripId;
	
	private ReviewAdditionalInfo additionalInfo;

	public ReviewAdditionalInfo getAdditionalInfo() {
		return additionalInfo == null ? new ReviewAdditionalInfo() : additionalInfo;
	}

	public List<String> getLowestPriceIds() {
		return lowestPriceIds == null ? new ArrayList<>() : lowestPriceIds;
	}
	public List<String> getSimilarPriceIds() {
		return similarPriceIds;
	}
}
