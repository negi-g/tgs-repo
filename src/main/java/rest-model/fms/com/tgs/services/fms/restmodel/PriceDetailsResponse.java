package com.tgs.services.fms.restmodel;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.TripInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceDetailsResponse extends BaseResponse {

	private List<TripInfo> tripInfos;
}
