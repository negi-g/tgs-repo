package com.tgs.services.fms.restmodel;


import com.tgs.services.base.msg.restmodel.MessageRequest;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;


@Getter
@Setter
@SuperBuilder
public class AirMessageRequest extends MessageRequest {

	private List<String> priceIds;

}
