package com.tgs.services.fms.restmodel;

import java.util.HashMap;
import java.util.Map;

import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.collections.MapUtils;

import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirReviewSeatResponse extends BaseResponse {

	private TripSeatMap tripSeatMap;
	private String bookingId;

	public void prepareSeatStructure() {
		tripSeatMap.getTripSeat().forEach((segmentKey, seats) -> {
			seats.prepareStructureData();
		});
	}

	public void addSeatMap(TripSeatMap seatMap) {

		if (tripSeatMap == null || MapUtils.isEmpty(tripSeatMap.getTripSeat())) {
			tripSeatMap = TripSeatMap.builder().build();
			tripSeatMap.setTripSeat(new HashMap<>());
		}

		for (Map.Entry<String, SeatInformation> entry : seatMap.getTripSeat().entrySet()) {
			tripSeatMap.getTripSeat().put(entry.getKey(), entry.getValue());
		}
	}

}
