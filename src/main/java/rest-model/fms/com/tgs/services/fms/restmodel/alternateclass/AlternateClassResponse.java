package com.tgs.services.fms.restmodel.alternateclass;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.TripInfo;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AlternateClassResponse extends BaseResponse {

    private List<TripInfo> tripInfos;

    public List<TripInfo> getTripInfos() {
        if (tripInfos == null) {
            tripInfos = new ArrayList<>();
        }
        return tripInfos;
    }
}
