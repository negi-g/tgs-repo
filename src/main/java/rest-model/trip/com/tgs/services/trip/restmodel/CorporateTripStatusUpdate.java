package com.tgs.services.trip.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.DataModel;
import lombok.Getter;

@Getter
public class CorporateTripStatusUpdate extends DataModel{
	
	public List<String> tripIdIn;
	

}
