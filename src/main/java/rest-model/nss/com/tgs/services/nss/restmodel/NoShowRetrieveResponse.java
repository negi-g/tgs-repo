package com.tgs.services.nss.restmodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoShowRetrieveResponse extends BaseResponse {

	private Map<String, Map<String, List<FlightTravellerInfo>>> travellerInfoMap;


	public Map<String, Map<String, List<FlightTravellerInfo>>> getTravellerInfoMap() {
		if (travellerInfoMap == null) {
			travellerInfoMap = new HashMap<>();
		}
		return travellerInfoMap;
	}


}
