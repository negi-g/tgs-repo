package com.tgs.services.nss.restmodel;

import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString(includeFieldNames = false)
public class NoShowRefundRequest {

	@NotNull
	private String pnr;

	@NotNull
	private String email;

	private String supplierId;

	@NotNull
	private String airline;


}
