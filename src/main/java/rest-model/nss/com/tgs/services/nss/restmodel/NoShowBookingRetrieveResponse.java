package com.tgs.services.nss.restmodel;


import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NoShowBookingRetrieveResponse extends BaseResponse {


}
