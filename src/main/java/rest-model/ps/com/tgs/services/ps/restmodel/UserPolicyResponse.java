package com.tgs.services.ps.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ps.datamodel.UserPolicy;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPolicyResponse extends BaseResponse {

	List<UserPolicy> userPolicies;
	
	public List<UserPolicy> getUserPolicies() {
		if (userPolicies == null) {
			userPolicies = new ArrayList<>();
		}
		return userPolicies;
	}
}
