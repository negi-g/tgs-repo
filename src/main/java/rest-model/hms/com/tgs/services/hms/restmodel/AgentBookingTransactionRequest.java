package com.tgs.services.hms.restmodel;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgentBookingTransactionRequest {

	private LocalDate bookingDate; 
}
