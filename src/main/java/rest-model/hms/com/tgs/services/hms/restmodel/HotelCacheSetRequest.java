package com.tgs.services.hms.restmodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelCacheSetRequest {

	private String namespace;
	private String set;
	private String keys[];
	private String bins[];
	private String classType;
	private Boolean compress;
	private Boolean plainData;
	private Boolean kyroCompress;

}
