package com.tgs.services.hms.restmodel;

import java.util.List;

import com.tgs.services.hms.datamodel.HotelRegionInfoMapping;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelDeleteMappingRequest {

	private List<HotelRegionInfoMapping> regionInfoMappingQuery;
}
