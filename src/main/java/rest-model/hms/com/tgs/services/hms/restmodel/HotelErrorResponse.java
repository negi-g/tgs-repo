package com.tgs.services.hms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelErrorResponse extends BaseResponse {

	private String Message;
	private String MessageInfo;
	private String StartTime;
	private String EndTime;
	
}