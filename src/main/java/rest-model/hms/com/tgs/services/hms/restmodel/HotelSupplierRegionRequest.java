package com.tgs.services.hms.restmodel;

import com.tgs.services.hms.datamodel.HotelSupplierRegionQuery;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelSupplierRegionRequest {

	private HotelSupplierRegionQuery supplierRegionQuery;
}
