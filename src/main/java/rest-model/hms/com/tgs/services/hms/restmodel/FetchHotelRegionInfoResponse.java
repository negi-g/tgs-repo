package com.tgs.services.hms.restmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FetchHotelRegionInfoResponse extends BaseResponse {

	@SerializedName("response")
	private List<HotelRegionInfo> regionInfoList;

	private String next;
}
