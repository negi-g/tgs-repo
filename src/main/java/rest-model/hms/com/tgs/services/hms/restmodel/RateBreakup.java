package com.tgs.services.hms.restmodel;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RateBreakup
{

    private String Date;
    private String Day;
    private Double DisplayNightlyRate;

}
