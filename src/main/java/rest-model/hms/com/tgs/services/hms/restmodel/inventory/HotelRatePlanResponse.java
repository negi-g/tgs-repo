package com.tgs.services.hms.restmodel.inventory;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import lombok.Setter;

@Setter
public class HotelRatePlanResponse extends BaseResponse {

	List<HotelRatePlan> ratePlans;

	public List<HotelRatePlan> getRatePlans() {
		if (ratePlans == null) {
			ratePlans = new ArrayList<>();
		}
		return ratePlans;
	}

}
