package com.tgs.services.hms.restmodel;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRegionInfoRequest {
	
	private List<HotelRegionInfoQuery> regionInfoQuery;
}
