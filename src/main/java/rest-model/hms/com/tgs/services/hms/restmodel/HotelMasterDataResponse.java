package com.tgs.services.hms.restmodel;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.ESHotel;
import com.tgs.services.hms.datamodel.HotelInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelMasterDataResponse extends BaseResponse {
	
	private HotelInfo hInfo;
	private List<ESHotel> esHotel;

}
