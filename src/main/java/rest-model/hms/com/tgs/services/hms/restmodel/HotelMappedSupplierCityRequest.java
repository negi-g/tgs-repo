package com.tgs.services.hms.restmodel;

import com.tgs.services.hms.datamodel.HotelRegionInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelMappedSupplierCityRequest {

	private HotelRegionInfo supplierRegionQuery;
}
