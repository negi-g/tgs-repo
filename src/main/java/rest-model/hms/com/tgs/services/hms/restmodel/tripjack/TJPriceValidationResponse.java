package com.tgs.services.hms.restmodel.tripjack;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TJPriceValidationResponse extends BaseResponse{
	
	private HotelInfo hInfo;
	private String bookingId;
	private List<Alert> alerts;
	private HotelSearchQuery query;
	private boolean isPriceChanged;
	private Double differentialPayment;

}
