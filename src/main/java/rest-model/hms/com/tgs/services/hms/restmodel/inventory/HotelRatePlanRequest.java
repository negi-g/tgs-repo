package com.tgs.services.hms.restmodel.inventory;

import java.util.List;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRatePlanRequest implements Validatable {

	private List<HotelRatePlan> ratePlans;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		int i = 0;
		for (HotelRatePlan ratePlan : ratePlans) {
			errors.putAll(ratePlan.validate(null).withPrefixToFieldNames("ratePlans[" + i++ + "]."));
			i++;
		}
		return errors;
	}
}
