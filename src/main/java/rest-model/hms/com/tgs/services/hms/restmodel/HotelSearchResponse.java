package com.tgs.services.hms.restmodel;

import java.util.List;
import java.util.Map;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.MethodExecutionInfo;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelSearchResponse extends BaseResponse {

	private HotelSearchResult searchResult;
	private HotelSearchQuery searchQuery;

	@Exclude
	private Map<String, Map<String, MethodExecutionInfo>> methodExecutionInfo;

	@Exclude
	private Map<String, List<CheckPointData>> checkPointInfoMap;

	@Exclude
	List<String> errorMessages;
}
