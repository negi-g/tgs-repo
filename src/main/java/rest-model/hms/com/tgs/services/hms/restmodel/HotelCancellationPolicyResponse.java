package com.tgs.services.hms.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelCancellationPolicyResponse extends BaseResponse {
	private String id;
	private List<Alert> alerts;
	private List<HotelCancellationPolicy> cancellationPolicyList;
	private HotelCancellationPolicy cancellationPolicy;

	public void addAlert(Alert alert) {
		if (alert == null) {
			return;
		}
		if (alerts == null) {
			alerts = new ArrayList<>();
		}
		alerts.add(alert);
	}
}
