package com.tgs.services.hms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.supplier.HotelSourceInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelSourceInfoResponse extends BaseResponse {

	private List<HotelSourceInfo> sourceInfos;

	public List<HotelSourceInfo> getSourceInfos() {
		if (sourceInfos == null) {
			sourceInfos = new ArrayList<>();
		}
		return sourceInfos;
	}

}
