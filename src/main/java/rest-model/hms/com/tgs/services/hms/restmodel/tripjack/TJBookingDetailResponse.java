package com.tgs.services.hms.restmodel.tripjack;

import java.util.Map;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TJBookingDetailResponse extends BaseResponse {

	public TjOrder order;
	private Map<String, HotelItemDetails> itemInfos;
	public GstInfo gstInfo;
}

