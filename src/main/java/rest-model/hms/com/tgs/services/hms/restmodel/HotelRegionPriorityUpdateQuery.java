package com.tgs.services.hms.restmodel;

import com.tgs.services.base.BulkUploadQuery;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRegionPriorityUpdateQuery extends BulkUploadQuery {

	private Long regionId;
	private Long priority;
}
