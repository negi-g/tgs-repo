package com.tgs.services.hms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;

public class HotelConfigRuleTypeResponse extends BaseResponse {

	private List<HotelConfiguratorInfo> hotelConfiguratorInfos;

	public List<HotelConfiguratorInfo> getHotelConfiguratorInfos() {
		if (hotelConfiguratorInfos == null) {
			hotelConfiguratorInfos = new ArrayList<>();
		}
		return hotelConfiguratorInfos;
	}
}
