package com.tgs.services.hms.restmodel.quotation;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelQuotationDeleteRequest {

	@SerializedName("id")
	private Long quotationId;
	private String optionId;
	private Boolean deleteAll;
}
