package com.tgs.services.hms.restmodel;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.inventory.HotelRoomCategory;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelFetchRoomCategoryResponse extends BaseResponse {
	
	private List<HotelRoomCategory> roomCategoryList;
	private List<HotelRoomTypeInfo> roomTypeInfoList;

}
