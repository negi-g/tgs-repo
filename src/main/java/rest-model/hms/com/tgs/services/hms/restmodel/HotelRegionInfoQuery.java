package com.tgs.services.hms.restmodel;

import com.tgs.services.hms.datamodel.HotelRegionAdditionalInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelRegionInfoQuery {

	private Long id;
	private String regionId;
	private String regionName;
	private String regionType;
	private String countryId;
	private String countryName;
	private String supplierName;
	private String stateId;
	private String stateName;
	private String fullRegionName;
	private Boolean enabled;
	private HotelRegionAdditionalInfo additionalInfo;
}
