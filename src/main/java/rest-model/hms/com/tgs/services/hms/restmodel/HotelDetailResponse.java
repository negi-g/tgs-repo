package com.tgs.services.hms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelDetailResponse extends BaseResponse {
	private String id;
	private HotelInfo hotel;
	private HotelSearchQuery searchQuery;
}
