package com.tgs.services.hms.restmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRegionInfoResponse extends BaseResponse {

	private List<HotelRegionInfo> regionInfos;

	public List<HotelRegionInfo> getRegionInfos() {
		if (Objects.isNull(regionInfos)) {
			regionInfos = new ArrayList<>();
		}
		return regionInfos;
	}
}
