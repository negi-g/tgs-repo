package com.tgs.services.hms.restmodel;

import java.util.List;
import com.tgs.services.hms.datamodel.HotelRegionInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRegionInfoUpdateRequest {

	private List<HotelRegionInfo> regionInfos;
}
