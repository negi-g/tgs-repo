package com.tgs.services.hms.restmodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReservationResponse {
	
	private String Message;
	private String BookingServiceType;
	private BookingDetail BookingDetail;
	private String StartTime;
	private String EndTime;

}
