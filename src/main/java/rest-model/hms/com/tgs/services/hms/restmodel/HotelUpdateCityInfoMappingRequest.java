package com.tgs.services.hms.restmodel;

import java.util.List;
import com.tgs.services.hms.datamodel.HotelUpdateCityInfoMappingQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelUpdateCityInfoMappingRequest {
	
	private String supplierName;
	List<HotelUpdateCityInfoMappingQuery> cityInfoMappingQuery;
}
