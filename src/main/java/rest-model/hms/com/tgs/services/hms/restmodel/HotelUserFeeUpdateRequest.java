package com.tgs.services.hms.restmodel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import com.tgs.services.hms.datamodel.HotelFlowType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelUserFeeUpdateRequest {

	@NotNull
	private String id;
	
	private String optionId;

	@NotNull
	private HotelFlowType flowType;

	private String feeType;

	@NotNull
	@PositiveOrZero
	private Double value;
	
}
