package com.tgs.services.hms.restmodel;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoomDetail {

	private String RoomTypeDescription;
	private String NumberOfRoom;
	private List<Passengers> Passengers;
	
	

}
