package com.tgs.services.hms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelStaticInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FetchHotelInfoResponse extends BaseResponse{

	private HotelStaticInfo response;
}
