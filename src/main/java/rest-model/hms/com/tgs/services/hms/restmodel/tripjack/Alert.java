package com.tgs.services.hms.restmodel.tripjack;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Alert {
	private String type;
	private String message;
}
