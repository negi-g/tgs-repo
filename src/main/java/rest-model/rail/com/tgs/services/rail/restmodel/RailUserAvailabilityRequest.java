package com.tgs.services.rail.restmodel;

import com.tgs.services.base.restmodel.BaseRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailUserAvailabilityRequest extends BaseRequest {


	private String mobileNo;

	private String agentPan;

	private String email;


}
