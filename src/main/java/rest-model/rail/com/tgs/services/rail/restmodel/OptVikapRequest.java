package com.tgs.services.rail.restmodel;

import com.tgs.services.base.restmodel.BaseRequest;
import lombok.Getter;

@Getter
public class OptVikapRequest extends BaseRequest {

	private String bookingId;

	// # separted train s.no
	private String token;

	private String trainNumbers;

	private Boolean specialTrainFlag;
}
