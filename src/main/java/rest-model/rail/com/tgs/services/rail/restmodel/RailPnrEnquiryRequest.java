package com.tgs.services.rail.restmodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailPnrEnquiryRequest {

	private String pnrNumber;

	private String supplierId;


}
