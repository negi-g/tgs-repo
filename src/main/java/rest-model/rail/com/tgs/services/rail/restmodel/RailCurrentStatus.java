package com.tgs.services.rail.restmodel;

import java.time.LocalDateTime;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.RailGstInfo;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class RailCurrentStatus extends BaseResponse {

	RailJourneyInfo journeyInfo;

	RailPriceInfo priceInfo;

	RailGstInfo gstInfo;

	LocalDateTime createdOn;

	String corporateName;

	String agentName;

	String agentEmail;

	String agentAddress;

	String agentMobile;

	GstInfo gstDetails;

}
