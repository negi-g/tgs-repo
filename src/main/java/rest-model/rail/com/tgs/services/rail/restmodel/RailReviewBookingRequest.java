package com.tgs.services.rail.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.rail.datamodel.RailCommAddress;
import com.tgs.services.rail.datamodel.RailFareEnquiry;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class RailReviewBookingRequest {

	@NonNull
	private RailFareEnquiryRequest fareEnquiryRequest;

	private RailFareEnquiry fareEnquiry;

	@NonNull
	private List<RailTravellerInfo> travellerInfos;

	private String bookingId;

	private Boolean moreThanOneDay;

	private Boolean travelInsuranceOpted;

	private String masterId;

	private String wsLoginUserId;

	private String ticketType;

	@NonNull
	private String reservationChoice;

	private boolean isThroughOTP = true;

	private String clientTransactionId;

	private String agentDeviceId;

	private Boolean ssQuotaSplitCoach;

	private Boolean ticketUpgradation;

	private String preffCoachId;

	@NonNull
	private String mobileNumber;

	private RailCommAddress tktAddress;

	private GstInfo gstInfo;


}
