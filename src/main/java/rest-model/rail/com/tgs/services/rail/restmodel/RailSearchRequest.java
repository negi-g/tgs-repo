package com.tgs.services.rail.restmodel;

import com.tgs.services.rail.datamodel.RailSearchQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailSearchRequest {

	private RailSearchQuery searchQuery;

	private String searchId;

}
