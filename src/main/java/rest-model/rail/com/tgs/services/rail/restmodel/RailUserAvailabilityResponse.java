package com.tgs.services.rail.restmodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Builder;
import lombok.Setter;
import lombok.Getter;

@Getter
@Setter
@Builder
public class RailUserAvailabilityResponse extends BaseResponse {

	@SerializedName("apa")
	private Boolean agentPanAvailable;

	@SerializedName("ea")
	private Boolean emailAvailable;

	@SerializedName("ma")
	private Boolean mobileAvailable;


}
