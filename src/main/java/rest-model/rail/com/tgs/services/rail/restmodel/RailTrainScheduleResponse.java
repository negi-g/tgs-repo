package com.tgs.services.rail.restmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.RailInfo;
import com.tgs.services.rail.datamodel.RailRouteInfo;
import com.tgs.services.rail.datamodel.RailRunningDays;
import com.tgs.services.rail.datamodel.RailStationInfo;
import lombok.Builder;

@Builder
public class RailTrainScheduleResponse extends BaseResponse {

	@SerializedName("ri")
	private RailInfo railInfo;

	@SerializedName("ds")
	private RailStationInfo departStationInfo;

	@SerializedName("as")
	private RailStationInfo arrivalStaionInfo;

	@SerializedName("roi")
	private List<RailRouteInfo> routeInfos;

	@SerializedName("rundays")
	private RailRunningDays runningDays;
}
