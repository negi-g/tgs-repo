package com.tgs.services.rail.restmodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailPostOfficeEnquiryRequest {

	private String pinCode;

	private String city;
}
