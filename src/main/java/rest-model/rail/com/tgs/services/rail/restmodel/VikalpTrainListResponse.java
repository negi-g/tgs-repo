package com.tgs.services.rail.restmodel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class VikalpTrainListResponse extends BaseResponse {

	List<RailJourneyInfo> journeyInfo;

	Boolean vikalpInSpecialTrains;

}
