package com.tgs.services.rail.restmodel;

import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.RailInfo;
import com.tgs.services.rail.datamodel.RailJourneyClass;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailPnrEnquiryResponse extends BaseResponse {

	private String pnrNumber;

	private LocalDateTime journeyDate;

	private RailInfo railInfo;

	private RailStationInfo sourceStation;

	private RailStationInfo destinationStation;

	private RailStationInfo reservationUptoStation;

	private RailStationInfo boardingStation;

	private RailJourneyClass journeyClass;

	private String quota;

	private Integer passengerCount;

	private String chartStatus;

	private List<RailTravellerInfo> travellerInfo;

	private String trainCancelStatus;

	private Double bookingFare;

	private Double ticketFare;

	private String trainStatus;


}
