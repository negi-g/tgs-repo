package com.tgs.services.rail.restmodel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailPostOfficeListResponse extends BaseResponse {

	private String city;

	private String state;

	private List<String> postOffices;
}
