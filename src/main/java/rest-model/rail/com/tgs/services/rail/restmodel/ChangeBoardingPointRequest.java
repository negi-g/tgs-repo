package com.tgs.services.rail.restmodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class ChangeBoardingPointRequest {

	private String bookingId;

	@SerializedName("nbs")
	private String newBoardingStation;

}
