package com.tgs.services.rail.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.RailStationInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailStationInfoResponse extends BaseResponse {

	private List<RailStationInfo> stationInfos;

	public List<RailStationInfo> getStationInfos() {
		if (stationInfos == null) {
			stationInfos = new ArrayList<RailStationInfo>();
		}
		return stationInfos;
	}


}
