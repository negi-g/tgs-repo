package com.tgs.services.rail.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailFareEnquiryResponse extends BaseResponse {

	private RailJourneyInfo journeyInfo;

	private String bookingId;

}
