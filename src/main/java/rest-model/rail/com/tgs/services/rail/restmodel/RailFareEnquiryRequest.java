package com.tgs.services.rail.restmodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class RailFareEnquiryRequest {

	@NonNull
	private String journeyId;

	@NonNull
	private String journeyClass;

	@NonNull
	private String quota;

	@NonNull
	private LocalDate travelDate;

	private String searchId;

	@SerializedName("nbs")
	private String newBoardingPoint;

	@SerializedName("nbt")
	private LocalDateTime newBoardingTime;

	@SerializedName("pef")
	private Boolean paymentEnqFlag;

}
