package com.tgs.services.rail.restmodel;

import java.util.List;
import com.tgs.services.base.msg.restmodel.MessageRequest;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
public class RailMessageRequest extends MessageRequest {

	List<String> journeyIds;

}
