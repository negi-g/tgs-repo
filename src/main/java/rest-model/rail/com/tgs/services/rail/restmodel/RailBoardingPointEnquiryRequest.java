package com.tgs.services.rail.restmodel;

import lombok.Getter;

@Getter
public class RailBoardingPointEnquiryRequest {

	private String journeyId;

	private String journeyClass;

	private String bookingId;

}
