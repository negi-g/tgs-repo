package com.tgs.services.rail.restmodel;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ChangeBoardingPointResponse {

	private String pnr;

	private String newBoardingPoint;

	private String oldBoardingPoint;

	private LocalDateTime newBoardingDate;
}
