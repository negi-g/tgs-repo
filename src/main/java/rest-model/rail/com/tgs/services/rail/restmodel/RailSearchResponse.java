package com.tgs.services.rail.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.RailSearchResult;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailSearchResponse extends BaseResponse {

	private RailSearchResult searchResult;

	private String searchId;


}
