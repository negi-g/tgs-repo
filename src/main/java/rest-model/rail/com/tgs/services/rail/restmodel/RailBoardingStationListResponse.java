package com.tgs.services.rail.restmodel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.RailStationInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailBoardingStationListResponse extends BaseResponse {

	private List<RailStationInfo> stations;

}
