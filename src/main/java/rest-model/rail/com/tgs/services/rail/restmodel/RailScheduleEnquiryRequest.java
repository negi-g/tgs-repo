package com.tgs.services.rail.restmodel;

import lombok.Getter;

@Getter
public class RailScheduleEnquiryRequest {

	private String journeyId;

	private String bookingId;

}
