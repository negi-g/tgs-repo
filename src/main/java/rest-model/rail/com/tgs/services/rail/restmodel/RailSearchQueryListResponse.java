package com.tgs.services.rail.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.RailSearchQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailSearchQueryListResponse extends BaseResponse {

	private List<RailSearchQuery> searchQueryList;

	private RailSearchQuery searchQuery;

	private List<String> searchIds;

	public void addSearchId(String searchId) {
		if (searchIds == null) {
			searchIds = new ArrayList<>();
		}
		searchIds.add(searchId);
	}
}
