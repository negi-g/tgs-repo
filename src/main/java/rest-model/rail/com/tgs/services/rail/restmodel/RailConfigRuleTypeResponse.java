package com.tgs.services.rail.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.config.RailConfiguratorInfo;

public class RailConfigRuleTypeResponse extends BaseResponse {


	private List<RailConfiguratorInfo> railConfiguratorInfos;

	public List<RailConfiguratorInfo> getRailConfiguratorInfos() {
		if (railConfiguratorInfos == null) {
			railConfiguratorInfos = new ArrayList<RailConfiguratorInfo>();
		}
		return railConfiguratorInfos;
	}


}
