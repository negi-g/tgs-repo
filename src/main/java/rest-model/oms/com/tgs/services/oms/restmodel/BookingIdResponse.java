package com.tgs.services.oms.restmodel;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

import com.tgs.services.base.restmodel.BaseResponse;

@Getter
@Setter
public class BookingIdResponse extends BaseResponse {

    private Set<String> bookingIds;

    public BookingIdResponse(Set<String> bookingIds) {
        this.bookingIds = bookingIds;
    }
}
