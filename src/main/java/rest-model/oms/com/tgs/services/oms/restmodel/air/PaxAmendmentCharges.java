package com.tgs.services.oms.restmodel.air;

import java.util.Objects;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class PaxAmendmentCharges {

	private String message;

	private Double amendmentCharges;

	private Double refundAmount;

	private Double totalFare;

	public double getAmendmentCharges() {
		if (Objects.isNull(amendmentCharges)) {
			amendmentCharges = new Double(0.0);
		}
		return amendmentCharges;
	}

	public double getRefundableamount() {
		if (Objects.isNull(refundAmount)) {
			refundAmount = new Double(0.0);
		}
		return refundAmount;
	}

	public double getTotalFare() {
		if (Objects.isNull(totalFare)) {
			totalFare = new Double(0.0);
		}
		return totalFare;
	}
}
