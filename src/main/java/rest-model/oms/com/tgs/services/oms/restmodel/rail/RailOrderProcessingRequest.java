package com.tgs.services.oms.restmodel.rail;

import java.time.LocalDateTime;
import com.tgs.services.oms.datamodel.OrderStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailOrderProcessingRequest {

	private LocalDateTime createdOnBefore;
	private LocalDateTime createdOnAfter;
	private OrderStatus orderStatus;


}
