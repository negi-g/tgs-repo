package com.tgs.services.oms.restmodel;

import javax.validation.Valid;

import com.tgs.services.oms.datamodel.InvoiceType;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;


@Getter
@Setter
public class InvoiceRequest {

	@Valid
	@NonNull
	private String id;

	@Valid
	@NonNull
	private InvoiceType type;
	
}
