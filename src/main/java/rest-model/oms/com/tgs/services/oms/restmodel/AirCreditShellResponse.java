package com.tgs.services.oms.restmodel;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class AirCreditShellResponse {

	Double creditBalance;

	LocalDateTime expiryDate;
}
