package com.tgs.services.oms.restmodel.hotel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelBookingInfoUpdateRequest {
	
	private String bookingId;
	private String hotelBookingReference;
	private String supplierBookingId;

}
