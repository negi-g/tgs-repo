package com.tgs.services.oms.restmodel.misc;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.oms.restmodel.InvoiceResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MiscInvoiceResponse extends InvoiceResponse {

	private String description;

	private double amount;

	@SerializedName("iid")
	private String invoiceId;

}
