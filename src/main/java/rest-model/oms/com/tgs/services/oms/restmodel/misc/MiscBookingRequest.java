package com.tgs.services.oms.restmodel.misc;

import com.tgs.services.oms.restmodel.BookingRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MiscBookingRequest extends BookingRequest {

	private String description;

	private String userId;

}
