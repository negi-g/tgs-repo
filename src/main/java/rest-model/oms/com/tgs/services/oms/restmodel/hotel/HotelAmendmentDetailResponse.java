package com.tgs.services.oms.restmodel.hotel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelAmendmentDetailResponse extends BaseResponse {

	private Amendment amendment;

	private String note;

}
