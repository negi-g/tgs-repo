package com.tgs.services.oms.restmodel.hotel;

import lombok.Getter;

@Getter
public class ProcessHotelAmendmentRequest {
	
	private String amendmentId;

	private HotelAmendOrderRequest modifiedInfo;

	private String note;

}
