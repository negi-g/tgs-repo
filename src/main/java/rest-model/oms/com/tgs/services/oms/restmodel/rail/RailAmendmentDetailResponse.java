package com.tgs.services.oms.restmodel.rail;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import com.tgs.services.rail.datamodel.RailTDRReason;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailAmendmentDetailResponse extends BaseResponse {

	private RailJourneyInfo journeyInfo;

	private RailPriceInfo priceInfo;

	private Amendment amendment;

	private String note;

	private List<RailTDRReason> tdrReasonList;

}
