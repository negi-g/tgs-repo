package com.tgs.services.oms.restmodel.rail;

import java.util.List;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.rail.datamodel.RailCommAddress;
import com.tgs.services.rail.datamodel.RailTravellerInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailBookingRequest extends BookingRequest {

	private List<RailTravellerInfo> travellerInfos;

	private RailCommAddress tktAddress;

	// private Boolean travelInsuranceOpted;

	private String reservationChoice;

}
