package com.tgs.services.oms.restmodel.air;

import com.tgs.services.oms.datamodel.air.AirOrderItem;
import lombok.Getter;
import lombok.Setter;
import lombok.NonNull;
import javax.validation.Valid;
import com.tgs.services.oms.datamodel.InvoiceType;

import java.util.List;

@Getter
@Setter
public class AirInvoiceRequest {

	@Valid
	@NonNull
	private String id;

	private String invoiceId;

	@Valid
	@NonNull
	private InvoiceType type;

	private List<AirOrderItem> airOrderItemList;

	public AirInvoiceRequest(String bookingId, InvoiceType type, List<AirOrderItem> airOrderItemList) {
		this.id = bookingId;
		this.type = type;
		this.airOrderItemList = airOrderItemList;
	}

	public AirInvoiceRequest() { }
}
