package com.tgs.services.oms.restmodel.rail;

import java.util.Set;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RailCancellationRequest {

	private Amendment amendment;

	private String bookingId;

	private String amendmentId;

	private String reservationId;

	private AmendmentType type;

	private Set<String> paxIds;

	private RailJourneyInfo journeyInfo;

	private RailPriceInfo priceInfo;

	private Boolean isOfflineCancellation;

	// This will be required in case of Offline Cancellation
	private String agentCanId;

}
