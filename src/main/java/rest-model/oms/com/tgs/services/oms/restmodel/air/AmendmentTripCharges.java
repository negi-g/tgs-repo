package com.tgs.services.oms.restmodel.air;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.HashSet;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

@Builder
@Getter
@Setter
public class AmendmentTripCharges {

	@SerializedName("src")
	private String source;

	@SerializedName("dest")
	private String destination;

	private LocalDateTime departureDate;

	private Set<String> flightNumbers;

	private Set<String> airlines;

	private Map<PaxType, PaxAmendmentCharges> amendmentInfo;

	private List<AmendmentTravellerDetail> travellers;

	public Set<String> getFlightNumbers() {
		if (flightNumbers == null) {
			flightNumbers = new HashSet<>();
		}
		return flightNumbers;
	}

	public Set<String> getAirlines() {
		if (airlines == null) {
			airlines = new HashSet<>();
		}
		return airlines;
	}

	public Map<PaxType, PaxAmendmentCharges> getAmendmentInfo() {
		if (amendmentInfo == null) {
			amendmentInfo = new HashMap<>();
		}
		return amendmentInfo;
	}

	public List<AmendmentTravellerDetail> getTravellers() {
		if (travellers == null) {
			travellers = new ArrayList<>();
		}
		return travellers;
	}

	public Double getTravellersRefundableAmount(Amendment amendment) {
		Double refundableAmount = Double.valueOf(0);
		for (AmendmentTravellerDetail travellerDetail : travellers) {
			refundableAmount += travellerDetail.getRefundableamount();
			/**
			 * Hack is added for TripJack Ixigo client because they can't process negative value at traveller level
			 */
			if (StringUtils.equalsIgnoreCase(amendment.getBookingUserId(), "331544")
					&& travellerDetail.getRefundableamount() < 0) {
				throw new CustomGeneralException(SystemError.AMENDMENT_CHARGES_NEGATIVE);
			}
		}
		return refundableAmount;
	}

}
