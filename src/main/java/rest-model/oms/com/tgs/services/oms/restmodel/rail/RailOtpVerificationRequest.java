package com.tgs.services.oms.restmodel.rail;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailOtpVerificationRequest {

	private String otpToken;

	private String amendmentId;

	private String cancellationId;

	private String pnrNo;

	// 1 for resend OTP and 2 for OTP verification
	private String requestType;

	private Boolean isResendOtp;

	private Boolean isWithoutOtp;

	private String bookingId;

}
