package com.tgs.services.oms.restmodel.hotel;

import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.PriceInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RoomCancellationDetail {

	private String id;

	@SerializedName("rc")
	private String roomCategory;

	@SerializedName("rt")
	private String roomType;

	@SerializedName("srn")
	private String standardRoomName;

	@SerializedName("tfcs")
	private Map<HotelFareComponent, Double> totalFareComponents;

	@SerializedName("tafcs")
	private Map<HotelFareComponent, Map<HotelFareComponent, Double>> totalAddlFareComponents;

	@SerializedName("pis")
	private List<PriceInfo> perNightPriceInfos;

}
