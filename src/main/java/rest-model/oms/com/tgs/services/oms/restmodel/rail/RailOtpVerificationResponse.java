package com.tgs.services.oms.restmodel.rail;

import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailOtpVerificationResponse extends BaseResponse {


	private String otpToken;

	private Boolean isOtpVerified;
}
