package com.tgs.services.oms.restmodel.hotel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelOrderStatusUpdateRequest {

	private String bookingId;
	
	private String status;
	
	private String remarks;
}
