package com.tgs.services.oms.restmodel.air;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirReleasePNRRequest {

	String bookingId;

	@SerializedName("pnrs")
	List<String> pnrs;

}
