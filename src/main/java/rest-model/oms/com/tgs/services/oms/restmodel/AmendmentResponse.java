package com.tgs.services.oms.restmodel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import com.tgs.services.oms.restmodel.hotel.HotelCancellationResponse;
import com.tgs.services.oms.restmodel.rail.RailCancellationResponse;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AmendmentResponse extends BaseResponse {

	private List<Amendment> amendmentItems;

	private Double differentialPayment;

	private AirCancellationResponse airCancellationReviewResponse;

	private RailCancellationResponse railCancellationResponse;

	private HotelCancellationResponse hotelCancellationResponse;
	
	private Boolean isCancelled;

}
