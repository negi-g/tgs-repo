package com.tgs.services.oms.restmodel.air;

import lombok.Getter;
import lombok.Setter;
import java.util.List;
import java.util.Set;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import com.tgs.services.oms.restmodel.InvoiceResponse;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.oms.datamodel.air.AirInvoiceDetail;

@Getter
@Setter
public class AirInvoiceResponse extends InvoiceResponse {

	private List<AirInvoiceDetail> travellerWiseDetail;

	@SerializedName("iid")
	private String invoiceId;
	
	@SerializedName("tid")
	private String tripId;

	@SerializedName("dd")
	private Set<LocalDateTime> departureDates;

	@SerializedName("ad")
	private Set<LocalDateTime> arrivalDates;
	
	@SerializedName("vcs")
	private Set<String> voucherCodes;

	public Set<LocalDateTime> getDepartureDates() {
		if (departureDates == null) {
			departureDates = new LinkedHashSet<>();
		}
		return departureDates;
	}

	public Set<LocalDateTime> getArrivalDates() {
		if (arrivalDates == null) {
			arrivalDates = new LinkedHashSet<>();
		}
		return arrivalDates;
	}

	public List<AirInvoiceDetail> getTravellerWiseDetail() {
		if (travellerWiseDetail == null) {
			return new ArrayList<>();
		}
		return travellerWiseDetail;
	}

}
