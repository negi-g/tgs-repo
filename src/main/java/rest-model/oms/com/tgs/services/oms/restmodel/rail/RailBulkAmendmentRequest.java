package com.tgs.services.oms.restmodel.rail;

import java.util.Set;
import com.tgs.services.base.BulkUploadQuery;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.rail.datamodel.RailTdrInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailBulkAmendmentRequest extends BulkUploadQuery {

	private String bookingId;

	private AmendmentType type;

	private String remarks;

	private Set<String> paxIds;

	private RailTdrInfo tdrInfo;

	// This will be required in case of Offline Cancellation
	private String agentCanId;

	private Boolean isWithoutOtp;
	
	private String pnr;
	

}
