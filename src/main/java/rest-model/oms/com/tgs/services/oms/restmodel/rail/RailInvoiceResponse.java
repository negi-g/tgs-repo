package com.tgs.services.oms.restmodel.rail;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.oms.restmodel.InvoiceResponse;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailInvoiceResponse extends InvoiceResponse {

	private RailJourneyInfo journeyInfo;

	@SerializedName("iid")
	private String invoiceId;

	@SerializedName("pi")
	private RailPriceInfo priceInfo;

	private String pnr;

	@SerializedName("at")
	private AmendmentType amendmentType;

	@SerializedName("refId")
	private String irctcTransactionId;

	@SerializedName("irctcid")
	private String irctcAgentId;


}
