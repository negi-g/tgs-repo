package com.tgs.services.oms.restmodel.air;

import lombok.Getter;
import lombok.Setter;
import java.util.List;
import javax.validation.constraints.NotNull;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;

@Getter
@Setter
public class AirItemDetailRequest {

	@NotNull
	private List<FlightTravellerInfo> travellers;

	@NotNull
	private String bookingId;

}
