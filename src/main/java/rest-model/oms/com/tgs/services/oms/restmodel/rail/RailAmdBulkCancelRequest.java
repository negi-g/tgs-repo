package com.tgs.services.oms.restmodel.rail;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RailAmdBulkCancelRequest extends BulkUploadRequest {

	private List<RailBulkAmendmentRequest> amendemntList;

}
