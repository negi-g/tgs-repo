package com.tgs.services.oms.restmodel.rail;


import java.util.Set;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.rail.datamodel.RailTdrInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RailAmendmentRequest {

	private String amendmentId;

	private String bookingId;

	private AmendmentType type;

	private String remarks;

	private Set<String> paxIds;

	private RailTdrInfo tdrInfo;

	// This will be required in case of Offline Cancellation
	private String agentCanId;

	private Boolean isWithoutOtp;

	private Boolean isTdrProcess;


}
