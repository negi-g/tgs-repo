package com.tgs.services.oms.restmodel.hotel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelBnplStatusUpdateRequest {
	
	private String bookingId;
	private String bnplStatus;

}
