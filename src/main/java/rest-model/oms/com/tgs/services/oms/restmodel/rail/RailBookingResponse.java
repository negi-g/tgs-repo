package com.tgs.services.oms.restmodel.rail;

import lombok.Setter;
import com.tgs.services.oms.BookingResponse;
import lombok.Getter;

@Getter
@Setter
public class RailBookingResponse extends BookingResponse {


	private String returnUrl;
	private String userLoginId;

}
