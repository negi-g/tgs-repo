package com.tgs.services.oms.restmodel.hotel;

import javax.validation.Valid;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.InvoiceType;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class HotelInvoiceRequest {

	@Valid
	@NonNull
	private String id;

	private String invoiceId;

	@Valid
	@NonNull
	private InvoiceType type;

	private HotelInfo hInfo;

	public HotelInvoiceRequest(String bookingId, InvoiceType type, HotelInfo hInfo) {
		this.id = bookingId;
		this.type = type;
		this.hInfo = hInfo;
	}

	public HotelInvoiceRequest() { }
}
