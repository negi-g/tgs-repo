package com.tgs.services.oms.restmodel.hotel;

import javax.validation.constraints.NotNull;

import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.oms.restmodel.BookingRequest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelImportBookingRequest extends BookingRequest {

	private String supplierBookingId;
	
	@NotNull
	private String supplierId;
	
	private String bookingUserId;
	
	private HotelImportBookingParams hotelImportBookingAdditionalInfo;

}
