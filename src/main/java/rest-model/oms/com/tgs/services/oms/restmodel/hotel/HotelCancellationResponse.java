package com.tgs.services.oms.restmodel.hotel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.APIUserExclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HotelCancellationResponse {

	@SerializedName("aca")
	@APIUserExclude
	private Boolean autoCancellationAllowed;

	@SerializedName("tcf")
	private Double totalCancellationFare;

	@SerializedName("tmr")
	private Double totalAmountToRefund;

	@SerializedName("rcd")
	List<RoomCancellationDetail> roomCancellationDetail;
}
