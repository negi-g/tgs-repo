package com.tgs.services.oms.restmodel.rail;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.rail.datamodel.RailJourneyInfo;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RailCancellationResponse extends BaseResponse {

	@SerializedName("pi")
	private RailPriceInfo priceInfo;

	private RailJourneyInfo journeyInfo;

	private Boolean isCancelSuccess;


}
