package com.tgs.services.oms.restmodel.air;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Setter;
import lombok.Getter;

@Getter
@Setter
public class RetrieveTicketStatusResponse extends BaseResponse {

	private String ticketStatus;

	private String ticketNumber;

}
