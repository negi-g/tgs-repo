package com.tgs.services.oms.restmodel.air;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class AmendmentTravellerDetail {


	@SerializedName("fn")
	private String firstName;

	@SerializedName("ln")
	private String lastName;

	private Double amendmentCharges;

	private Double refundableamount;

	private Double totalFare;

	public double getAmendmentCharges() {
		if (Objects.isNull(amendmentCharges)) {
			amendmentCharges = new Double(0.0);
		}
		return amendmentCharges;
	}

	public double getRefundableamount() {
		if (Objects.isNull(refundableamount)) {
			refundableamount = new Double(0.0);
		}
		return refundableamount;
	}

	public double getTotalFare() {
		if (Objects.isNull(totalFare)) {
			totalFare = new Double(0.0);
		}
		return totalFare;
	}
}
