package com.tgs.services.oms.restmodel.air;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentChecklist;

import com.tgs.services.oms.datamodel.amendments.PWSReason;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AmendmentDetailResponse extends BaseResponse {

	private List<SegmentInfo> segmentInfoList;

	private Amendment amendment;

	private String note;

	private List<AmendmentChecklist> checked;

	private List<AmendmentChecklist> processChecklist;

	private List<PWSReason> pwsReasons;
}
