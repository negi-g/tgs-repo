package com.tgs.services.oms.restmodel.air;


import com.google.gson.annotations.SerializedName;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.restmodel.BookingRequest;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class AirManualBookingRequest extends BookingRequest {

	private List<AirOrderItem> airOrderItems;

	private String userId;

	private Boolean isGroupBook;

	private boolean forReIssue;

	@SerializedName("cu")
	private Boolean isCommissionEdited;

	private List<FlightTravellerInfo> travellerProfiles;

}

