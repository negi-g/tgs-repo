package com.tgs.services.oms.restmodel.air;

import java.util.List;
import java.util.Set;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import lombok.Getter;

@Getter
public class ProcessAirAmendmentRequest {

	private String amendmentId;

	private List<AirOrderItem> modItems;

	private String note;

	private Set<Short> checked;

	private boolean recallCommission;

	@SerializedName("recp")
	private boolean recallCPCommission;
}
