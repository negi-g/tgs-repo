package com.tgs.services.oms.restmodel.air;

import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class RetrieveTicketStatusRequest {

	@NotNull
	private List<String> ticketNumbers;

	@NotNull
	private String supplierId;

}
