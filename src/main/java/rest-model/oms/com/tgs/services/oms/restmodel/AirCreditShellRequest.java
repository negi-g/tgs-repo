package com.tgs.services.oms.restmodel;


import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
@Builder
public class AirCreditShellRequest extends BookingRequest {

	List<FlightTravellerInfo> travellerInfos;

	String airline;

	SupplierConfiguration supplierConfig;

	String pnr;

	String lastName;

}
