package com.tgs.services.oms.restmodel.rail;

import javax.validation.Valid;
import com.tgs.services.oms.datamodel.InvoiceType;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class RailInvoiceRequest {

	@Valid
	@NonNull
	private String id;

	private String invoiceId;

	@Valid
	@NonNull
	private InvoiceType type;

	private RailOrderItem railOrderItem;


	public RailInvoiceRequest(String bookingId, InvoiceType type, RailOrderItem railOrderItem) {
		this.id = bookingId;
		this.type = type;
		this.railOrderItem = railOrderItem;
	}

	public RailInvoiceRequest() {}


}
