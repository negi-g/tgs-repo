package com.tgs.services.oms.restmodel.rail;

import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import lombok.Getter;

@Getter
public class ProcessRailAmendmentRequest {

	private String amendmentId;

	private String note;

	private RailOrderItem modifiedItem;

}
