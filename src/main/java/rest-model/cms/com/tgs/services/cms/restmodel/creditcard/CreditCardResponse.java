package com.tgs.services.cms.restmodel.creditcard;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CreditCardResponse extends BaseResponse {

    List<CreditCardInfo> cardInfoList;

    public List<CreditCardInfo> getCardInfoList() {
        if (cardInfoList == null) {
            cardInfoList = new ArrayList<>();
        }
        return cardInfoList;
    }
}