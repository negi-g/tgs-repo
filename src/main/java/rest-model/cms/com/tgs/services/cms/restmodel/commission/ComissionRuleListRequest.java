package com.tgs.services.cms.restmodel.commission;


import com.tgs.services.cms.datamodel.commission.CommissionFilter;
import lombok.Getter;

@Getter
public class ComissionRuleListRequest {

    private CommissionFilter commissionFilter;
}
