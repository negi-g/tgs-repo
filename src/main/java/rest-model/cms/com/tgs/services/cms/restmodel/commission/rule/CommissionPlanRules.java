package com.tgs.services.cms.restmodel.commission.rule;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.cms.datamodel.commission.CommissionRule;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommissionPlanRules extends BaseResponse {

	/**
	 * This will be all commissionRules associated to commission plan id
	 */
	private List<CommissionRule> includedRules;

	/**
	 * This will be all commissionRules & not associated to commission plan id
	 */
	private List<CommissionRule> excludedRules;

	private String planName;

	private String description;

	private Boolean enabled;

	private OrderType product;

	/**
	 * This is CommissionPlan Id
	 */
	private Long id;

	public List<CommissionRule> getExcludedRules() {
		if (excludedRules == null) {
			excludedRules = new ArrayList<>();
		}
		return excludedRules;
	}

	public List<CommissionRule> getIncludedRules() {
		if (includedRules == null) {
			includedRules = new ArrayList<>();
		}
		return includedRules;
	}

}
