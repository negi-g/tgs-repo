package com.tgs.services.cms.restmodel.tourcode;


import com.tgs.services.cms.datamodel.tourcode.TourCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TourCodeRequest {

    private TourCode tourCode;
}