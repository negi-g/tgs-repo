package com.tgs.services.cms.restmodel.commission;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionRule;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IataCommissionUploadRequest extends BulkUploadRequest {

	private List<AirCommissionRule> uploadQuery;

}
