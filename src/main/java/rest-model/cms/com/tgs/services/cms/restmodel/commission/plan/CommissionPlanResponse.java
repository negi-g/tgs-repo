package com.tgs.services.cms.restmodel.commission.plan;


import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.cms.datamodel.commission.CommissionPlan;

import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
public class CommissionPlanResponse extends BaseResponse {

    List<CommissionPlan> commissionPlans;

    /*
     * @return plan asscoiated to that id
     */
    public List<CommissionPlan> getCommissionPlans() {
        if (commissionPlans == null) {
            commissionPlans = new ArrayList<>();
        }
        return commissionPlans;
    }
}