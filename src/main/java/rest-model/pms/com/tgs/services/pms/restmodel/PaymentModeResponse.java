package com.tgs.services.pms.restmodel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.PaymentMode;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PaymentModeResponse extends BaseResponse {

	List<PaymentMode> paymentModes;

	private String bookingId;

	private Double markup;

	/**
	 * This amount is inclusive of markup
	 */
	private BigDecimal amount;

	public List<PaymentMode> getPaymentModes() {
		if (paymentModes == null) {
			paymentModes = new ArrayList<>();
		}
		return paymentModes;
	}
}