package com.tgs.services.pms.restmodel;

import com.tgs.services.base.helper.NotNull;
import com.tgs.services.pms.datamodel.CreditPolicy;

import lombok.Getter;

@Getter
public class CreditIssueRequest {

	@NotNull
	private String userId;
	@NotNull
	private CreditPolicy policy;
}
