package com.tgs.services.pms.restmodel;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;

import lombok.Builder;
import lombok.Setter;

@Builder
@Setter
public class MoneyExchangeInfoResponse extends BaseResponse{

	MoneyExchangeInfo moneyExchangeInfo;
	List<MoneyExchangeInfo> moneyExchangeInfoList;
	
}
