package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.ExternalPayment;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExternalPaymentDetailResponse extends BaseResponse {

	private ExternalPayment payment;

}
