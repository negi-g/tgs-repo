package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.CreditPolicy;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class CreditPolicyResponse extends BaseResponse {

    private List<CreditPolicy> creditPolicies;

    public CreditPolicyResponse(List<CreditPolicy> creditPolicies) {
        this.creditPolicies = creditPolicies;
    }

    public CreditPolicyResponse(CreditPolicy creditPolicy) {
        creditPolicies = new ArrayList<>();
        creditPolicies.add(creditPolicy);
    }
}
