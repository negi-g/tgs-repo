package com.tgs.services.pms.restmodel;

import com.tgs.services.base.datamodel.Product;
import com.tgs.services.pms.datamodel.CreditPolicy;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPolicyRequest {

	private CreditPolicy policy;
	private String userId;
	private Product product;
}
