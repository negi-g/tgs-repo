package com.tgs.services.pms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.PaymentDetail;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PaymentDetailResponse extends BaseResponse {

	List<PaymentDetail> paymentList;

	public PaymentDetailResponse(List<PaymentDetail> paymentList) {
		this.paymentList = paymentList;
	}

	public PaymentDetailResponse(PaymentDetail payment) {
		this.paymentList = new ArrayList<>();
		this.paymentList.add(payment);
	}
}
