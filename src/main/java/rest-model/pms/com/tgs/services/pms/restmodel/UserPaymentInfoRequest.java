package com.tgs.services.pms.restmodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPaymentInfoRequest {
	private String userId;
}
