package com.tgs.services.pms.restmodel;

import com.tgs.services.pms.datamodel.CreditLine;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class SaveCreditLineRequest {

	@NotNull
	private CreditLine creditLine;
}
