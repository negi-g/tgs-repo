package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;

import com.tgs.services.gms.datamodel.Incidence.Incidence;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class IncidenceResponse extends BaseResponse {

    private Set<Incidence> incidences;

    public IncidenceResponse() {
        incidences = new HashSet<>();
    }

    public IncidenceResponse(Incidence incidence) {
        incidences = new HashSet<>();
        incidences.add(incidence);
    }

    public IncidenceResponse(Collection<Incidence> incidence) {
        incidences = new HashSet<>();
        incidences.addAll(incidence);
    }

    public Set<Incidence> getIncidences() {
        if (incidences == null) incidences = new HashSet<>();
        return incidences;
    }
}
