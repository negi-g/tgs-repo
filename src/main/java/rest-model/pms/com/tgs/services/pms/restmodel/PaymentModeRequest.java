package com.tgs.services.pms.restmodel;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Product;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class PaymentModeRequest {

	private Product product;

	private Boolean isDomestic;

	private String bookingId;

	@NotNull
	private BigDecimal amount;

	private String payUserId;

	private Boolean onlyEnabledModes;

	private Boolean considerWalletBalance;

	private String accountName;

	private Long billingEntityId;

	@SerializedName("ipncl")
	private boolean isPayNowConfirmLater;

	private boolean skipProductCheck;

}
