package com.tgs.services.pms.restmodel;

import lombok.Getter;
import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;

@Getter
public class PaymentIdsResponse extends BaseResponse {

    private List<String> paymentRefIds;

    public PaymentIdsResponse(List<String> paymentRefIds) {
        this.paymentRefIds = paymentRefIds;
    }

    public PaymentIdsResponse() {
        this.paymentRefIds = new ArrayList<>();
    }
}
