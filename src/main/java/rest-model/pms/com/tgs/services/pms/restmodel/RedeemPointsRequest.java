package com.tgs.services.pms.restmodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.restmodel.BaseRequest;
import com.tgs.services.pms.datamodel.PaymentOpType;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Setter
@Getter
public class RedeemPointsRequest extends BaseRequest {

	@NonNull
	private String bookingId;

	private PointsType type;

	private PaymentOpType opType;

	@NonNull
	@SerializedName("pts")
	private Double points;

	public PointsType getType() {
		if (type == null) {
			type = PointsType.COIN;
		}
		return type;
	}

}
