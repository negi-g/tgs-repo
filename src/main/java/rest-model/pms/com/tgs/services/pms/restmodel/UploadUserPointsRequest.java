package com.tgs.services.pms.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadUserPointsRequest extends BulkUploadRequest {

	private List<UserPointsUploadRequest> pointsRequest;
}
