package com.tgs.services.pms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.CreditBill;

import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
public class CreditBillResponse extends BaseResponse {

    List<CreditBill> creditBills;

    public CreditBillResponse(List<CreditBill> creditBills) {
        this.creditBills = creditBills;
    }

    public CreditBillResponse(CreditBill creditBill) {
        this.creditBills = new ArrayList<>();
        this.creditBills.add(creditBill);
    }

}
