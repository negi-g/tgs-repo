package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PaymentConfigurationRuleResponse extends BaseResponse {

    private List<PaymentConfigurationRule> rules;

    public List<PaymentConfigurationRule> getRules() {
        if (rules == null) {
            rules = new ArrayList<>();
        }
        return rules;
    }
}
