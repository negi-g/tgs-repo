package com.tgs.services.pms.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.ruleengine.PaymentGatewayConfigInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentGatewayConfigResponse extends BaseResponse{

	 private List<PaymentGatewayConfigInfo> gatewayConfigs;

	    public List<PaymentGatewayConfigInfo> getGatewayConfigs() {
	        if (gatewayConfigs == null) {
	        	gatewayConfigs = new ArrayList<>();
	        }
	        return gatewayConfigs;
	    }
}
