package com.tgs.services.pms.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.UserWallet;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserWalletResponse extends BaseResponse {

	private List<UserWallet> userWallets;

	public UserWalletResponse(UserWallet userWallet) {
		this.userWallets = new ArrayList<>();
		if(userWallet != null) {
			userWallets.add(userWallet);
		}
	}
}
