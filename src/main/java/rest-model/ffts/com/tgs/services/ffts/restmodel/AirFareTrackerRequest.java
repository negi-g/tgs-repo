package com.tgs.services.ffts.restmodel;

import java.util.List;
import javax.validation.constraints.NotNull;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.BulkUploadQuery;
import com.tgs.services.ffts.datamodel.notification.AirFareUpdateEmailData;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class AirFareTrackerRequest extends BulkUploadQuery{

	@NotNull
	private String pnr;

	@NotNull
	private String supplierId;

	@SerializedName("liu")
	private String loggedinUser;

	@SerializedName("at")
	private Double actionThreshold;

	@SerializedName("ssids")
	private List<String> searchSupplierIds;

	@SerializedName("ed")
	private AirFareUpdateEmailData emailData;

	private int indexInQueue;
	
	private boolean isRequestDataUpdated;
}
