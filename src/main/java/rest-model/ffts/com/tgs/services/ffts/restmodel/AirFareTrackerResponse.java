package com.tgs.services.ffts.restmodel;

import java.util.List;
import java.util.Map;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.TripInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirFareTrackerResponse extends BaseResponse {

	private Map<String, List<TripInfo>> tripInfos;
}
