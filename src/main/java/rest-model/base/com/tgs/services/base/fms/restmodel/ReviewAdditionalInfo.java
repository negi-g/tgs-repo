package com.tgs.services.base.fms.restmodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewAdditionalInfo {

    private String reason;
}
