package com.tgs.services.base.msg.restmodel;

import javax.validation.constraints.NotNull;
import com.tgs.services.base.enums.MessageMedium;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;


@Getter
@Setter
@SuperBuilder
public class MessageRequest {


	@NotNull
	private MessageMedium mode;

	private String bookingId;

	// email or mobile number
	@NotNull
	private String toIds;

	private boolean isWithoutPrice;

	private boolean isWithoutGst;

	private boolean isOldPrintCopy;

	private boolean isNewTicketPrint;
}
