package com.tgs.services.base.fms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.AirlineInfo;

import java.util.ArrayList;
import java.util.List;

public class AirlineInfoResponse extends BaseResponse {

	private List<AirlineInfo> airlineInfos;

	public List<AirlineInfo> getAirportInfos() {
		if (airlineInfos == null) {
			airlineInfos = new ArrayList<>();
		}
		return airlineInfos;
	}
}
