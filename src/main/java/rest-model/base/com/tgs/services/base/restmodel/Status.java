package com.tgs.services.base.restmodel;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Status {
	private Boolean success;
	private Integer httpStatus;

	public Status(HttpStatusCode httpStatusCode) {
		this.httpStatus = httpStatusCode.getHttpStatus();
		this.success = httpStatusCode.getSuccess();
	}

}
