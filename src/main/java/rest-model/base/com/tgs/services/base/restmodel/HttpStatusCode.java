package com.tgs.services.base.restmodel;

import lombok.Getter;

@Getter
public enum HttpStatusCode {

	HTTP_200(200, true), HTTP_400(400, false), HTTP_404(404, false), HTTP_403(403, false);

	private int httpStatus;
	private Boolean success;

	private HttpStatusCode(int httpStatus, Boolean success) {
		this.httpStatus = httpStatus;
		this.success = success;
	}
}
