package com.tgs.services.ums.restmodel.bulkupload;

import java.util.List;
import com.tgs.services.ums.datamodel.bulkUpload.UserUpdateQuery;
import com.tgs.services.ums.restmodel.UserGroupsRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserGroupsUpdateRequest extends UserUpdateRequest {
	
	private List<UserGroupsRequest> updateQuery;

	@Override
	public List<? extends UserUpdateQuery> getUpdateQuery() {
		return updateQuery;
	}
	
}
