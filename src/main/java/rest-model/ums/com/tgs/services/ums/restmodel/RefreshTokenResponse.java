package com.tgs.services.ums.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefreshTokenResponse extends BaseResponse {

	private String accessToken;
	private String refreshToken;
	private String oldrefreshToken;
}
