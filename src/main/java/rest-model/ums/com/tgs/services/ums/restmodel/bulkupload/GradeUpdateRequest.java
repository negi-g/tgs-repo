package com.tgs.services.ums.restmodel.bulkupload;

import java.util.List;
import com.tgs.services.ums.datamodel.bulkUpload.GradeUpdateQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GradeUpdateRequest extends UserUpdateRequest{

	private List<GradeUpdateQuery> updateQuery;
	
	@Override
	public List<GradeUpdateQuery> getUpdateQuery() {
		return updateQuery;
	}
}
