package com.tgs.services.ums.restmodel.bulkupload;

import java.util.List;
import com.tgs.services.ums.datamodel.bulkUpload.CommissionPlanUpdateQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommissionPlanUpdateRequest extends UserUpdateRequest {

	private List<CommissionPlanUpdateQuery> updateQuery;

	@Override
	public List<CommissionPlanUpdateQuery> getUpdateQuery() {
		return updateQuery;
	}
}
