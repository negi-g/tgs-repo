package com.tgs.services.ums.restmodel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ums.datamodel.UserDetails;

public class UserDetailsResponse extends BaseResponse {
	
	public List<UserDetails> userDetails;
	
	public UserDetailsResponse(List<UserDetails> userDetails) {
		this.userDetails = userDetails;
	}
}
