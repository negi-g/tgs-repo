package com.tgs.services.ums.restmodel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.LogExclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ResetPasswordRequest {

	@NotNull
	@ApiModelProperty(required = true)
	private String userName;
	@NotNull
	@ApiModelProperty(required = true)
	private String requestId;

	@LogExclude
	@Size(min = 5, max = 20)
	@ApiModelProperty(required = true)
	private String newPassword;

	@LogExclude
	private String password;
	private String otp;

	private UserRole role;
}
