package com.tgs.services.ums.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeUploadRequest extends BulkUploadRequest {

	private List<Employee> request;
}
