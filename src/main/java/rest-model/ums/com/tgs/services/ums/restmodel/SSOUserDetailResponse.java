package com.tgs.services.ums.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SSOUserDetailResponse extends BaseResponse {

	private User userDetails;
}
