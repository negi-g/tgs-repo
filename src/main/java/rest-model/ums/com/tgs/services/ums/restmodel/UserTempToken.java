package com.tgs.services.ums.restmodel;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class UserTempToken {

	private String userId;
	private LocalDateTime createdOn;

}
