package com.tgs.services.ums.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ums.datamodel.AreaRoleMapping;

public class AreaRoleMapResponse extends BaseResponse {
	List<AreaRoleMapping> roleMapping;

	public List<AreaRoleMapping> getAreaRoleMapping(){
		if (roleMapping == null) {
			roleMapping = new ArrayList<>();
		}
		return roleMapping;
	}
}