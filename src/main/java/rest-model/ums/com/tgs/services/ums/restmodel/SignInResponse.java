package com.tgs.services.ums.restmodel;


import com.tgs.services.base.restmodel.BaseResponse;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.gms.OtpValidateRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class SignInResponse extends BaseResponse {
	private User user;
	private String accessToken;
	private String refreshToken;

	@SerializedName("2dreq")
	private Boolean TwoDAuthRequired;

	@Exclude
	private OtpValidateRequest otpValidateRequest;
}
