package com.tgs.services.ums.restmodel;

import java.util.List;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserAdditionalInfo;
import com.tgs.services.ums.datamodel.bulkUpload.UserUpdateQuery;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserGroupsRequest extends UserUpdateQuery {
	private List<String> groupIds;

	@Override
	public User getUserFromRequest() {
		return User.builder().userId(getUserId()).additionalInfo(UserAdditionalInfo.builder().groups(groupIds).build())
				.build();
	}
}
