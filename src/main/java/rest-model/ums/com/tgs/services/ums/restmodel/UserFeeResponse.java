package com.tgs.services.ums.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ums.datamodel.fee.UserFee;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserFeeResponse extends BaseResponse {
    private List<UserFee> userFees;

    public List<UserFee> getUserFees() {
        if (userFees == null) {
            userFees = new ArrayList<>();
        }
        return userFees;
    }
}
