package com.tgs.services.points.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tgs.services.points.dbmodel.DbPointsConfigurationRule;

public interface PointsConfigurationRepository
		extends JpaRepository<DbPointsConfigurationRule, Long>, JpaSpecificationExecutor<DbPointsConfigurationRule> {

}
