package com.tgs.services.ums.datamodel;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class UserPolicyCacheFilter {
	private String userId;
	private List<String> policyIds;

}