package com.tgs.services.ps.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tgs.services.ps.dbmodel.DbUserPolicy;

public interface UserPolicyRepository extends JpaRepository<DbUserPolicy, Long>, JpaSpecificationExecutor<DbUserPolicy> {

	DbUserPolicy findByUserId(String userId);
}
