package com.tgs.services.ps.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aerospike.client.query.Filter;
import com.aerospike.client.query.IndexCollectionType;
import com.tgs.filters.UserPolicyFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ps.datamodel.Policy;
import com.tgs.services.ps.datamodel.UserPolicy;
import com.tgs.services.ps.jparepository.UserPolicyService;
import com.tgs.services.ums.datamodel.UserPolicyCacheFilter;

@Service
@InitializerGroup(group = InitializerGroup.Group.GENERAL)
public class UserPolicyHelper extends InMemoryInitializer {

	private static final String FIELD = "user_policy";

	private static CustomInMemoryHashMap policyHashMap;

	@Autowired
	private UserPolicyService policyService;

	public UserPolicyHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap userPolicyHashMap) {
		super(configurationHashMap);
		UserPolicyHelper.policyHashMap = userPolicyHashMap;
	}

	@Override
	public void process() {
		List<UserPolicy> userPolicies = policyService.search(UserPolicyFilter.builder().build());
		if (!CollectionUtils.isEmpty(userPolicies)) {
			userPolicies.forEach(policy -> updateInCache(policy));
		}

	}

	@Override
	public void deleteExistingInitializer() {
		policyHashMap.truncate(CacheSetName.USER_POLICY.getName());
	}

	public void updateInCache(UserPolicy policy) {
		Map<String, Object> binMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(policy.getPolicyInfo().getPolicies())) {
			List<String> policies =
					policy.getPolicyInfo().getPolicies().stream().map(Policy::getPolicyId).collect(Collectors.toList());
			binMap.put(BinName.POLICYID.getName(), policies);
		}
		updateUserPolicyInCache(binMap, policy);

	}

	public void updateUserPolicyInCache(Map<String, Object> binMap, UserPolicy policy) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
				.expiration(InMemoryInitializer.NEVER_EXPIRE).set(CacheSetName.USER_POLICY.getName()).listData(true)
				.key(policy.getUserId()).binValues(binMap).compress(false).build();
		policyHashMap.put(policy.getUserId(), FIELD, policy, metaInfo);

	}

	public static Map<String, UserPolicy> getUserPolicyMap(UserPolicyCacheFilter filter) {
		Map<String, UserPolicy> policies = new HashMap<>();
		Map<String, Map<String, UserPolicy>> userPolicyMap = new HashMap<>();
		CacheMetaInfo metaInfo = null;
		if (filter.getUserId() != null) {
			policies.put(filter.getUserId(), getUserPolicy(filter.getUserId()));
		} else if (CollectionUtils.isNotEmpty(filter.getPolicyIds())) {
			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName()).bins(new String[] {FIELD})
					.set(CacheSetName.USER_POLICY.getName()).build();
			for (String policyId : filter.getPolicyIds()) {
				userPolicyMap.putAll(policyHashMap.getResultSet(metaInfo, UserPolicy.class,
						Filter.contains(BinName.POLICYID.getName(), IndexCollectionType.LIST, policyId)));
			}
		}

		if (MapUtils.isNotEmpty(userPolicyMap)) {
			for (Entry<String, Map<String, UserPolicy>> entrySet : userPolicyMap.entrySet()) {
				policies.put(entrySet.getKey(), entrySet.getValue().get(FIELD));
			}
		}

		return policies;
	}

	public static UserPolicy getUserPolicy(String userId) {
		return policyHashMap.get(userId, FIELD, UserPolicy.class, CacheMetaInfo.builder()
				.namespace(CacheNameSpace.USERS.getName()).set(CacheSetName.USER_POLICY.getName()).build());
	}

}
