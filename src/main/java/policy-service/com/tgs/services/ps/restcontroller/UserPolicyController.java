package com.tgs.services.ps.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ps.datamodel.UserPolicy;
import com.tgs.services.ps.datamodel.UserPolicyFilter;
import com.tgs.services.ps.restmodel.UserPolicyResponse;
import com.tgs.services.ps.servicehandler.PolicyHandler;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/ps/v1/policy")
public class UserPolicyController {

	@Autowired
	private PolicyHandler policyHandler;

	@RequestMapping(value = "/assign", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.POLICY_SERVICE, isMidOfficeAllowed = true)
	protected @ResponseBody UserPolicyResponse assignToUser(@RequestBody UserPolicy policy) throws Exception {
		policyHandler.initData(policy, new UserPolicyResponse());
		return policyHandler.getResponse();
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected @ResponseBody UserPolicyResponse list(@RequestBody UserPolicyFilter filter) throws Exception {
		UserPolicyResponse response = new UserPolicyResponse();
		response.getUserPolicies().addAll(policyHandler.list(filter));
		return response;
	}

	@RequestMapping(value = "/status/{userId}/{policyId}/{enabled}", method = RequestMethod.GET)
	@CustomRequestProcessor(areaRole = AreaRole.POLICY_SERVICE, isMidOfficeAllowed = true)
	protected @ResponseBody BaseResponse changeStatus(@PathVariable("userId") String userId,
			@PathVariable("policyId") String policyId, @PathVariable("enabled") Boolean enabled) throws Exception {
		return policyHandler.updatePolicyStatus(userId, policyId, enabled);
	}
}
