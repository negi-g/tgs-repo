package com.tgs.services.ps.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.ps.datamodel.UserPolicyInfo;

public class UserPolicyInfoType extends CustomUserType {

	@Override
	public Class<?> returnedClass() {
		return UserPolicyInfo.class;
	}

}
