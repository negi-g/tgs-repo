package com.tgs.services.ps.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.tgs.services.ps.datamodel.Policy;
import com.tgs.services.ps.datamodel.PolicyGroup;
import com.tgs.services.ps.datamodel.UserPolicy;
import com.tgs.services.ps.datamodel.UserPolicyInfo;
import com.tgs.services.ps.helper.PolicyGroupHelper;
import com.tgs.services.ps.helper.UserPolicyHelper;

@Service
public class UserPolicyManager {

	@Autowired
	private PolicyGroupHelper policyGroupHelper;

	public List<Policy> getUserPolicies(String userId, List<String> labels, Boolean enabled) {
		UserPolicy userpolicy = UserPolicyHelper.getUserPolicy(userId);
		if (userpolicy != null) {
			UserPolicyInfo policyInfo = userpolicy.getPolicyInfo();
			if (policyInfo != null) {
				return getUserPolicies(policyInfo, labels,enabled);
			}
		}
		return Collections.emptyList();
	}

	public List<Policy> getUserPolicies(UserPolicyInfo policyInfo, List<String> labels, Boolean enabled) {
		List<Policy> policies = new ArrayList<>();
		if (!CollectionUtils.isEmpty(policyInfo.getPolicies())) {
			policyInfo.getPolicies().forEach(policy -> {
				if(isApplicable(policy.getLabels(), labels) && enabled ==null || policy.isEnabled() == enabled)
					policies.add(policy);
			});
		}
		if (!CollectionUtils.isEmpty(policyInfo.getGroupIds())) {
			for (String groupId : policyInfo.getGroupIds()) {
				PolicyGroup policyGroup = policyGroupHelper.getPolicyGroup(groupId);
				if (policyGroup != null && !policyGroup.isDeleted() && policyGroup.isEnabled()) {
					if (!CollectionUtils.isEmpty(policyGroup.getPolicies())) {
						policyGroup.getPolicies().forEach(policy -> {
							if(isApplicable(policy.getLabels(), labels))  {
								policy.setGroupId(String.valueOf(policyGroup.getId()));
								policies.add(policy);
							}
						});
					}
				}
			}
		}

		return policies;
	}

	public Policy fetchPolicyById(String userId, String policyId) {
		UserPolicy userPolicy = UserPolicyHelper.getUserPolicy(userId);
		if (userPolicy != null) {
			UserPolicyInfo policyInfo = userPolicy.getPolicyInfo();
			if (policyInfo != null) {
				// first priority to user level policies
				Policy policy = policyInfo.getPolicies().stream().filter(p -> policyId.equals(p.getPolicyId()) && p.isEnabled())
						.findFirst().orElse(null);
				if (policy != null)
					return policy;
				// filter group level policies (assuming no conflicts)
				if (!CollectionUtils.isEmpty(policyInfo.getGroupIds())) {
					for (String groupId : policyInfo.getGroupIds()) {
						PolicyGroup policyGroup = policyGroupHelper.getPolicyGroup(groupId);
						if (policyGroup != null && policyGroup.isEnabled()) {
							if (!CollectionUtils.isEmpty(policyGroup.getPolicies())) {
								policy = policyInfo.getPolicies().stream().filter(p -> policyId.equals(p.getPolicyId()) && p.isEnabled())
										.findFirst().orElse(null);
								if (policy != null)
									return policy;
							}
						}
					}
				}
			}
		}
		// Policy is not assigned to the user
		return null;
	}

	private boolean isApplicable(List<String> policyLabels, List<String> labels) {
		//If labels are empty, send all the policies
		if(CollectionUtils.isEmpty(labels))
			return true;
		List<String> intersect = labels.stream().filter(policyLabels::contains)
                .collect(Collectors.toList());
		return !CollectionUtils.isEmpty(intersect);
	}
}
