package com.tgs.services.ps.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.annotations.UpdateTimestamp;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.ps.datamodel.UserPolicy;
import com.tgs.services.ps.datamodel.UserPolicyInfo;
import com.tgs.services.ps.hibernate.UserPolicyInfoType;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "userpolicy", uniqueConstraints = @UniqueConstraint(columnNames = "userId"))
@TypeDefs(@TypeDef(name = "UserPolicyInfoType", typeClass = UserPolicyInfoType.class))
@Setter
@Getter
public class DbUserPolicy extends BaseModel<DbUserPolicy, UserPolicy> {

	@Column
	private String userId;
	
	@Column
	@Type(type = "UserPolicyInfoType")
	private UserPolicyInfo policyInfo;
	
	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;
	
	@Column
	@UpdateTimestamp
	private LocalDateTime processedOn;
	
	@Override
	public UserPolicy toDomain() {
		return new GsonMapper<>(this, UserPolicy.class).convert();
	}

	@Override
	public DbUserPolicy from(UserPolicy dataModel) {
		return new GsonMapper<>(dataModel, this, DbUserPolicy.class).convert();
	}
	
}
