package com.tgs.services.ps.jparepository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.PolicyGroupFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.ps.dbmodel.DbPolicyGroup;
import com.tgs.services.ps.helper.PolicyGroupHelper;

@Service
public class PolicyGroupService extends SearchService<DbPolicyGroup> {

	@Autowired
	private PolicyGroupRepository repository;

	@Autowired
	private PolicyGroupHelper policyGroupHelper;

	public DbPolicyGroup save(DbPolicyGroup policyGroup) {
		policyGroup.setProcessedOn(LocalDateTime.now());
		policyGroup = repository.save(policyGroup);
		policyGroupHelper.update(policyGroup.toDomain());
		return policyGroup;
	}

	public DbPolicyGroup findById(Long id) {
		return repository.findById(id);
	}

	public List<DbPolicyGroup> findAllEnabledAndNotDeleted() {
		return repository.findByEnabledAndIsDeleted(true, false);
	}

	public List<DbPolicyGroup> findAllNotDeleted() {
		return repository.findByIsDeleted(false);
	}

	public List<DbPolicyGroup> findAll(PolicyGroupFilter filter) {
		return search(filter, repository);
	}
}
