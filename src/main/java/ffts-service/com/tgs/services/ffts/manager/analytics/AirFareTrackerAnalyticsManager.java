package com.tgs.services.ffts.manager.analytics;

import java.util.List;

import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.ffts.datamodel.ActionResult;
import com.tgs.services.ffts.datamodel.AirRetrievedBookingInfo;
import com.tgs.services.ffts.datamodel.analytics.AnalyticsAirFareTracker;
import com.tgs.services.ffts.mapper.analytics.AirSearchToAFTAnalyticsMapper;
import com.tgs.services.ffts.mapper.analytics.RequestsToAFTAnalyticsMapper;
import com.tgs.services.ffts.mapper.analytics.RetrievedBookingToAFTAnalyticsMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirFareTrackerAnalyticsManager {

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private KafkaServiceCommunicator kafkaService;

	/**
	 * 
	 * @param airRetrievedBookingInfo must be non-null and must contain non-null {@code AirFareTrackerRequest}
	 * @param actionResult
	 * @param comment
	 */
	public void pushSearchAndActionAnalytics(AirRetrievedBookingInfo airRetrievedBookingInfo, ActionResult actionResult,
			String comment) {
		AirSearchToAFTAnalyticsMapper aftAnalyticsMapper =
				AirSearchToAFTAnalyticsMapper.builder().actionResult(actionResult).build();
		aftAnalyticsMapper.setAirRetrievedBookingInfo(airRetrievedBookingInfo).setComment(comment);
		pushToAnalytics(aftAnalyticsMapper);
	}

	/**
	 * 
	 * @param airFareTrackerRequest must be non-null
	 * @param airRetrievedBookingInfo
	 * @param comment
	 */
	public void pushBookingRetrievalAnalytics(AirFareTrackerRequest airFareTrackerRequest,
			AirRetrievedBookingInfo airRetrievedBookingInfo, String comment) {
		RetrievedBookingToAFTAnalyticsMapper aftAnalyticsMapper =
				RetrievedBookingToAFTAnalyticsMapper.builder().fmsCommunicator(fmsCommunicator).build();
		aftAnalyticsMapper.setAirRetrievedBookingInfo(airRetrievedBookingInfo).setComment(comment)
				.setAirFareTrackerRequest(airFareTrackerRequest);
		pushToAnalytics(aftAnalyticsMapper);
	}

	public void pushRequestAnalytics(List<AirFareTrackerRequest> fareTrackerRequests, Boolean isUploadSuccessful) {
		RequestsToAFTAnalyticsMapper aftAnalyticsMapper =
				RequestsToAFTAnalyticsMapper.builder().requests(fareTrackerRequests).build();
		aftAnalyticsMapper.setContextData(SystemContextHolder.getContextData())
				.setUser(SystemContextHolder.getContextData().getUser());
		if (isUploadSuccessful == null) {
			aftAnalyticsMapper.setComment("Uploading PNRs");
		} else {
			aftAnalyticsMapper.setComment("Failed to upload PNRs");
		}
		pushToAnalytics(aftAnalyticsMapper);
	}

	private void pushToAnalytics(Mapper<AnalyticsAirFareTracker> mapper) {
		ExecutorUtils.getAnalyticsThreadPool().submit(() -> {
			AnalyticsAirFareTracker retrievedBooking = null;
			try {
				retrievedBooking = mapper.convert();
				QueueData queueData = QueueData.builder().key(ESMetaInfo.AIR_FARE_TRACK_INFO.getIndex())
						.value(GsonUtils.getGson().toJson(retrievedBooking)).build();
				kafkaService.queue(QueueDataType.ELASTICSEARCH, queueData);
			} catch (Exception e) {
				log.error("Failed to store analytics data {} ", retrievedBooking, e);
			}
		});
	}
}
