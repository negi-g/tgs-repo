package com.tgs.services.ffts.client.oms;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.tgs.services.base.security.SecurityConstants;
import com.tgs.services.ffts.client.AFTAbstractTgsClient;

@Service
public class AFTOMSClient extends AFTAbstractTgsClient {

	final private static String RELATIVE_OMS_PATH = "/oms/v1/air";

	private static String orderServiceUrl;

	/**
	 * Mapping of relative paths to absolute paths
	 */
	private static Map<OMSService, String> urlMapping;

	static {
		urlMapping = new HashMap<>();
	}

	private String getOrderServiceUrl() {
		if (orderServiceUrl == null) {
			String serverUrl = getServerUrl();
			orderServiceUrl = new StringBuilder(serverUrl).append(RELATIVE_OMS_PATH).toString();
		}
		return orderServiceUrl;
	}

	private String getAbsolutePath(OMSService service) {
		String absolutePath = urlMapping.get(service);
		if (absolutePath == null) {
			absolutePath = getOrderServiceUrl() + service.relativePath;
			urlMapping.put(service, absolutePath);
		}
		return absolutePath;
	}

	final public <V> V doPostForOms(OMSService omsService, Object postData, Class<? extends V> responseType, String jwt)
			throws IOException {
		String url = getAbsolutePath(omsService);
		Map<String, String> headerParams = new HashMap<>();
		String jwtTokenHeader = SecurityConstants.TOKEN_PREFIX + jwt;
		headerParams.put(SecurityConstants.HEADER_STRING, jwtTokenHeader);
		return doPost(url, postData, responseType, headerParams);
	}

	public static enum OMSService {
		IMPORT_PNR("/import-pnr-book"), IMPORT_PNR_REVIEW("/import-pnr-book/review");

		private String relativePath;

		private OMSService(String relativePath) {
			this.relativePath = relativePath;
		}
	}
}
