package com.tgs.services.ffts.manager;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import com.tgs.services.oms.restmodel.air.AirImportPnrReviewResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.JWTHelper;
import com.tgs.services.cacheservice.datamodel.AirFareTrackerMetaInfo;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ffts.client.oms.AFTOMSClient;
import com.tgs.services.ffts.client.oms.AFTOMSClient.OMSService;
import com.tgs.services.ffts.datamodel.AirRetrievedBookingInfo;
import com.tgs.services.ffts.util.AFTUtils;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.oms.restmodel.air.AirImportPnrBookingRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
class AirImportPnrManager {

	private final static int MIN_MINUTES_LAPSE = 60,
			/**
			 * Queue is expected to be processed completely in 6 hours. Should be dynamically configurable.
			 */
			FAILED_RETRIEVAL_TTL = 6 * 3600;

	private final static String RETRIEVED_PNR_QUEUE_KEY = "retrieved_pnr_queue";

	@Autowired
	private GeneralCachingCommunicator cachingService;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private AFTOMSClient aftomsClient;

	AirImportPnrBooking retrieveBooking(AirFareTrackerRequest airFareTrackerRequest) throws Exception {
		AirImportPnrBookingRequest airImportPnrBookingRequest = AirImportPnrBookingRequest.builder()
				.pnr(airFareTrackerRequest.getPnr()).supplierId(airFareTrackerRequest.getSupplierId()).build();
		User loggedinUser = loggedInUser(airFareTrackerRequest.getLoggedinUser());
		airImportPnrBookingRequest.setBookingUserId(loggedinUser.getUserId());
		String jwt = null;
		try {
			jwt = JWTHelper.generateAndStoreAccessToken(loggedinUser);
		} catch (Exception e) {
			log.error("Failed to generate jwt for {} due to ", loggedinUser, e);
			throw new CustomGeneralException("Failed to generate jwt");
		}
		AirImportPnrReviewResponse airImportPnrReviewResponse = aftomsClient.doPostForOms(OMSService.IMPORT_PNR_REVIEW,
				airImportPnrBookingRequest, AirImportPnrReviewResponse.class, jwt);
		toAirImportPnrBooking(airImportPnrReviewResponse);
		if (airImportPnrReviewResponse == null) {
			throw new CustomGeneralException("No response received");
		}
		if (CollectionUtils.isNotEmpty(airImportPnrReviewResponse.getErrors())) {
			throw new CustomGeneralException(airImportPnrReviewResponse.getErrors().get(0).getMessage());
		}
		if (airImportPnrReviewResponse.getSegmentInfos() == null) {
			throw new CustomGeneralException("No segments received");
		}
		AirImportPnrBooking airImportPnrBooking = toAirImportPnrBooking(airImportPnrReviewResponse);
		airImportPnrBooking.setPnr(airFareTrackerRequest.getPnr());
		airImportPnrBooking.setSupplierId(airFareTrackerRequest.getSupplierId());
		LocalDateTime travelDateTime = airImportPnrBooking.getTripInfos().get(0).getDepartureTime();
		if (LocalDateTime.now().isAfter(travelDateTime)) {
			throw new CustomGeneralException(SystemError.TRAVEL_DATE_EXPIRED);
		}
		return airImportPnrBooking;
	}

	private User loggedInUser(String loggedinUserId) {
		User loggedinUser = null;
		if (StringUtils.isNotBlank(loggedinUserId)) {
			loggedinUser = userService.getUserFromCache(loggedinUserId);
		}
		/**
		 * if loggedinUserId is invalid/empty, see if some user is actually logged in
		 */
		if (loggedinUser == null) {
			loggedinUser = SystemContextHolder.getContextData().getUser();
		}
		/**
		 * otherwise, try to find a valid user to use search service
		 */
		if (loggedinUser == null) {
			loggedinUser = tryToFindValidUser();
		}
		return loggedinUser;
	}

	/**
	 * To be removed
	 * 
	 * @return
	 */
	private User tryToFindValidUser() {
		User user = userService.getUserFromCache("5402");
		if (user == null) {
			user = userService.getUserFromCache("91");
		}
		if (user == null) {
			user = userService.getUserFromCache("2000282");
		}
		return user;
	}

	private AirImportPnrBooking toAirImportPnrBooking(AirImportPnrReviewResponse airImportPnrReviewResponse) {
		try {
			return AirImportPnrBooking.builder()
					.tripInfos(AFTUtils.createTripListFromSegmentList(airImportPnrReviewResponse.getSegmentInfos()))
					.gstInfo(airImportPnrReviewResponse.getGstInfo())
					.deliveryInfo(airImportPnrReviewResponse.getDeliveryInfo()).build();
		} catch (Exception e) {
			log.error("Failed to parse response {} due to ", airImportPnrReviewResponse, e);
			throw new CustomGeneralException("Failed to parse response");
		}
	}

	/**
	 * Update or store data from {@code airFareTrackerRequest} for given {@code pnr}. It also signifies that booking
	 * data for given {@code pnr} is either available or will be available.
	 * 
	 * @param airFareTrackerRequest
	 */
	void storeRequestData(AirFareTrackerRequest airFareTrackerRequest) {
		String pnr = airFareTrackerRequest.getPnr();

		AirRetrievedBookingInfo airRetrievedBookingInfo = fetchRetrievedBooking(pnr);
		if (airRetrievedBookingInfo == null) {
			airRetrievedBookingInfo = AirRetrievedBookingInfo.builder().build();
		}
		airRetrievedBookingInfo.setAirFareTrackerRequest(airFareTrackerRequest);
		storeBookingData(pnr, airRetrievedBookingInfo);
	}

	AirRetrievedBookingInfo storeRetrievedBookingForTracking(AirImportPnrBooking airImportPnrBooking) {
		AirRetrievedBookingInfo airRetrievedBookingInfo = AirRetrievedBookingInfo.builder()
				.importTime(LocalDateTime.now()).airImportPnrBooking(airImportPnrBooking).successStatus(true).build();
		storeBookingData(airImportPnrBooking.getPnr(), airRetrievedBookingInfo);

		try {
			/**
			 * set compress to false for debugging
			 */
			cachingService.storeInQueue(AirFareTrackerMetaInfo.builder().namespace(CacheNameSpace.FLIGHT.getName())
					.set(CacheSetName.FARE_TRACKER.getName()).key(RETRIEVED_PNR_QUEUE_KEY)
					.pnr(airImportPnrBooking.getPnr()).compress(false).build());
		} catch (Exception e) {
			log.error("Deleting booking data for {} because storing AirFareTrackerMetaInfo in {} failed due to",
					airImportPnrBooking.getPnr(), RETRIEVED_PNR_QUEUE_KEY, e);
			cachingService.delete(CacheMetaInfo.builder().namespace(CacheNameSpace.FLIGHT.getName())
					.set(CacheSetName.FARE_TRACKER.getName()).key(airImportPnrBooking.getPnr()).build());
		}
		return airRetrievedBookingInfo;
	}

	AirRetrievedBookingInfo storeFailedBooking(String pnr) {
		AirRetrievedBookingInfo airRetrievedBookingInfo =
				AirRetrievedBookingInfo.builder().successStatus(false).build();
		storeBookingData(pnr, airRetrievedBookingInfo);
		return airRetrievedBookingInfo;
	}

	private void storeBookingData(String pnr, AirRetrievedBookingInfo airRetrievedBookingInfo) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.FLIGHT.getName())
				.set(CacheSetName.FARE_TRACKER.getName()).key(pnr).build();
		Map<String, AirRetrievedBookingInfo> binMap = new HashMap<>();
		binMap.put(BinName.RETRIEVEDBOOK.getName(), airRetrievedBookingInfo);
		int ttl = calculcateTtl(airRetrievedBookingInfo);
		if (ttl > 0) {
			cachingService.store(metaInfo, binMap, false, false, ttl);
		}
	}

	/**
	 * Handling is internal to {@code AirImportPnrManager} so that Retrieved Booking and Retrieved PNR Queue are
	 * consistent.
	 * <p>
	 * PNRs with older processed-on time are expected at index 0 as PNRs of new or recently processed bookings are
	 * stored at the end of the queue.
	 * <p>
	 * <u>Implementation note:</u>
	 * <p>
	 * <li>Traverse through the queue to find next eligible retrieved booking info.
	 * <li>Remove meta infos with blank PNRs or which don't have any corresponding booking info from queue.
	 * <li>Store meta infos which have any corresponding booking info back to queue.
	 * <li>Return booking info which has never been processed or was processed more than {@code MIN_MINUTES_LAPSE}
	 * minutes ago and have request data for tracking and processing.
	 * <li>Keep track of first popped PNR which was stored back to queue.
	 * <li>Terminate and return with null if end of cycle (first popped PNR) is encountered.
	 * 
	 * @return next {@code AirRetrievedBookingInfo} to track fare for
	 */
	AirRetrievedBookingInfo popNextRetrievedBookingToTrack() {
		String firstPoppedPnr = null;
		String pnr;
		AirFareTrackerMetaInfo cacheMetaInfo;
		LocalDateTime now = LocalDateTime.now();
		while ((cacheMetaInfo = popAirFareTrackerMetaInfo()) != null) {
			if (StringUtils.isBlank(pnr = cacheMetaInfo.getPnr())) {
				continue;
			}
			if (pnr.equals(firstPoppedPnr)) {
				/**
				 * Cycle ends here. None found. Store head back to queue.
				 */
				cachingService.storeInQueue(cacheMetaInfo);
				return null;
			}
			if (cacheMetaInfo.getProcessedOn() == null
					|| now.minusMinutes(MIN_MINUTES_LAPSE).isAfter(cacheMetaInfo.getProcessedOn())) {
				AirRetrievedBookingInfo airRetrievedBookingInfo = fetchRetrievedBooking(pnr);
				if (airRetrievedBookingInfo != null) {
					/**
					 * Ignore airRetrievedBookingInfo which doesn't yet have any request data for tracking and
					 * processing.
					 */
					if (airRetrievedBookingInfo.getAirFareTrackerRequest() != null) {
						cacheMetaInfo.setProcessedOn(LocalDateTime.now());
						cachingService.storeInQueue(cacheMetaInfo);
						return airRetrievedBookingInfo;
					}
				} else {
					/**
					 * Else (airRetrievedBookingInfo == null) no need to store such meta info. No Retrieved Booking
					 * exists (anymore) for this PNR.
					 */
					continue;
				}
			}
			if (firstPoppedPnr == null) {
				firstPoppedPnr = pnr;
			}
			/**
			 * Store meta info back to queue.
			 */
			cachingService.storeInQueue(cacheMetaInfo);
		}
		return null;
	}

	private AirFareTrackerMetaInfo popAirFareTrackerMetaInfo() {
		return cachingService.fetchFromQueue(AirFareTrackerMetaInfo.builder().namespace(CacheNameSpace.FLIGHT.getName())
				.set(CacheSetName.FARE_TRACKER.getName()).key(RETRIEVED_PNR_QUEUE_KEY).index(0).compress(false)
				.build());
	}

	boolean wasRequestCompleted(String pnr) {
		AirRetrievedBookingInfo airRetrievedBooking = fetchRetrievedBooking(pnr);
		/**
		 * If airRetrievedBooking.getStatus() is null, task couldn't set status and hence was not able to terminate.
		 */
		return airRetrievedBooking != null && airRetrievedBooking.getSuccessStatus() != null;
	}

	// @SuppressWarnings("serial")
	private AirRetrievedBookingInfo fetchRetrievedBooking(String pnr) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.FLIGHT.getName())
				.set(CacheSetName.FARE_TRACKER.getName()).key(pnr)
				/*
				 * .typeOfT(new TypeToken<List<AirRetrievedBooking>>() { }.getType()).compress(false)
				 */.build();
		return cachingService.getBinValue(metaInfo, AirRetrievedBookingInfo.class, false, false,
				BinName.RETRIEVEDBOOK.getName());
	}

	private int calculcateTtl(AirRetrievedBookingInfo airRetrievedBooking) {
		if (airRetrievedBooking.getAirImportPnrBooking() == null) {
			/**
			 * Request is expected to be processed again within FAILED_RETRIEVAL_TTL seconds. Request will have either
			 * of the following information:
			 * <li>thread was interrupted, or
			 * <li>booking retrieval failed.
			 */
			return FAILED_RETRIEVAL_TTL;
		}
		LocalDateTime localDateTime =
				airRetrievedBooking.getAirImportPnrBooking().getTripInfos().get(0).getDepartureTime();
		return (int) LocalDateTime.now().until(localDateTime, ChronoUnit.SECONDS);
	}

	void deleteRetrievedBooking(String pnr) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.FLIGHT.getName())
				.set(CacheSetName.FARE_TRACKER.getName()).key(pnr).build();
		cachingService.delete(metaInfo);
	}
}
