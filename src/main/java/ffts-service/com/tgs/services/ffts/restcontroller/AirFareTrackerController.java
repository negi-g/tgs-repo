package com.tgs.services.ffts.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.ffts.manager.AirFareTrackerManager;
import com.tgs.services.ffts.manager.RegisterPnrManager;
import com.tgs.services.ffts.restcontroller.validator.AirFareTrackerRequestListValidator;
import com.tgs.services.ffts.restcontroller.validator.AirFareTrackerRequestValidator;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequestList;
import com.tgs.services.ffts.restmodel.AirFareTrackerResponse;
import com.tgs.services.ffts.servicehandler.AirFareTrackerQueueServiceHandler;
import com.tgs.services.ffts.servicehandler.AirFareTrackerServiceHandler;


@RestController
@RequestMapping("/ffts/v1/air")
public class AirFareTrackerController {

	@Autowired
	private AirFareTrackerQueueServiceHandler fareTrackerQueueServiceHandler;

	@Autowired
	private AirFareTrackerServiceHandler airFareTrackerServiceHandler;

	@Autowired
	private AirFareTrackerManager airFareTrackerManager;

	@Autowired
	private RegisterPnrManager pnrManager;

	@Autowired
	private AirFareTrackerRequestListValidator aftRequestListValidator;

	@Autowired
	private AirFareTrackerRequestValidator aftRequestValidator;

	@InitBinder("airFareTrackerRequestList")
	public void aftReqListInitBinder(WebDataBinder dataBinder) {
		dataBinder.setValidator(aftRequestListValidator);
	}

	@InitBinder("airFareTrackerRequest")
	public void aftReqInitBinder(WebDataBinder dataBinder) {
		dataBinder.setValidator(aftRequestValidator);
	}

	@RequestMapping(value = "/find-new-fare", method = RequestMethod.POST)
	protected @ResponseBody AirFareTrackerResponse findNewFare(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirFareTrackerRequest fareTrackerRequest) throws Exception {
		airFareTrackerServiceHandler.initData(fareTrackerRequest, new AirFareTrackerResponse());
		return airFareTrackerServiceHandler.getResponse();
	}

	@RequestMapping(value = "/register-pnrs", method = RequestMethod.POST)
	protected @ResponseBody BaseResponse registerPnrs(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirFareTrackerRequestList fareTrackerRequestList) throws Exception {
		fareTrackerQueueServiceHandler.initData(fareTrackerRequestList, new BaseResponse());
		return fareTrackerQueueServiceHandler.getResponse();
	}

	@RequestMapping(value = "/job/execute-fare-tracker-engine", method = RequestMethod.GET)
	protected void executeEngine(HttpServletRequest request, HttpServletResponse response) throws Exception {
		airFareTrackerManager.findNewFares();
	}

	@RequestMapping(value = "/job/retrieve-bookings", method = RequestMethod.GET)
	protected void retrieveBookings(HttpServletRequest request, HttpServletResponse response) throws Exception {
		airFareTrackerManager.retrieveBookings();
	}

	@RequestMapping(value = "/upload-pnrs", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse uploadUsers(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirFareTrackerRequestList fareTrackerRequest) throws Exception {
		return pnrManager.registerPnrs(fareTrackerRequest);
	}
}
