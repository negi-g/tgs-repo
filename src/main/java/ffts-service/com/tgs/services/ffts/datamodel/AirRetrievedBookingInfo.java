/**
 * Inside ffts-service because com.tgs.services.ffts.restmodel.* is not accessible from data-model
 */
package com.tgs.services.ffts.datamodel;

import java.time.LocalDateTime;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class AirRetrievedBookingInfo {

	@SerializedName("aipb")
	private AirImportPnrBooking airImportPnrBooking;

	private Boolean successStatus;

	private LocalDateTime importTime;

	@SerializedName("aftr")
	private AirFareTrackerRequest airFareTrackerRequest;

	/**
	 * support for managing tracking frequency based on score calculated using multiple criteria
	 */
	private int trackingScore;

}
