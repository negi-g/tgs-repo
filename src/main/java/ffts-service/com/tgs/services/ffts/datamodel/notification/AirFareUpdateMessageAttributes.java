package com.tgs.services.ffts.datamodel.notification;

import java.math.BigDecimal;
import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AirFareUpdateMessageAttributes extends EmailAttributes {

	private String flightDesc;
	
	private String pnr;
	
	private String difference;
	
	private String newFare;
	
	private String oldFare;
}
