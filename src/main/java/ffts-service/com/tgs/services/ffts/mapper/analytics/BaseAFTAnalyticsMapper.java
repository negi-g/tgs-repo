package com.tgs.services.ffts.mapper.analytics;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.ffts.datamodel.AirRetrievedBookingInfo;
import com.tgs.services.ffts.datamodel.analytics.AirFareTrackerAnalyticsType;
import com.tgs.services.ffts.datamodel.analytics.AnalyticsAirFareTracker;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Setter
public class BaseAFTAnalyticsMapper extends Mapper<AnalyticsAirFareTracker> {

	private User user;
	private ContextData contextData;
	private String comment;

	@Getter(lombok.AccessLevel.PROTECTED)
	private AirRetrievedBookingInfo airRetrievedBookingInfo;

	private AirFareTrackerRequest airFareTrackerRequest;

	@Override
	protected void execute() throws CustomGeneralException {
		if (output == null) {
			output = AnalyticsAirFareTracker.builder().build();
		}

		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();

		output.setAnalyticstype(AirFareTrackerAnalyticsType.FLIGHT_FARE_TRACKER.name());
		output.setComment(comment);
		output.setAirlines(getAirlines());
		airFareTrackerRequest = getAirFareTrackerRequest();

		if (airFareTrackerRequest != null) {
			output.setPnr(airFareTrackerRequest.getPnr());
			output.setSupplier(airFareTrackerRequest.getSupplierId());
		}
	}

	/**
	 * Fetch airlines from booking info
	 * 
	 * @return
	 */
	private String getAirlines() {
		if (airRetrievedBookingInfo != null && airRetrievedBookingInfo.getAirImportPnrBooking() != null
				&& airRetrievedBookingInfo.getAirImportPnrBooking().getTripInfos() != null) {
			List<TripInfo> trips = airRetrievedBookingInfo.getAirImportPnrBooking().getTripInfos();
			StringJoiner airlines = new StringJoiner(", ");
			Set<String> airlineSet = new HashSet<>();
			trips.forEach(trip -> airlineSet.addAll(trip.getAirlineInfo(false)));
			airlineSet.forEach(airline -> airlines.add(airline));
			return airlines.toString();
		}
		return null;
	}

	final protected AirFareTrackerRequest getAirFareTrackerRequest() {
		if (airFareTrackerRequest == null && airRetrievedBookingInfo != null) {
			airFareTrackerRequest = airRetrievedBookingInfo.getAirFareTrackerRequest();
		}
		return airFareTrackerRequest;
	}

}
