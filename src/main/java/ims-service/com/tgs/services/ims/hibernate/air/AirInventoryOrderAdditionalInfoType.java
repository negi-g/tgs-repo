package com.tgs.services.ims.hibernate.air;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.ims.datamodel.air.AirInventoryOrderInfo;

public class AirInventoryOrderAdditionalInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return AirInventoryOrderInfo.class;
	}

}
