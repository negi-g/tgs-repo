package com.tgs.services.ims.restcontroller.air;

import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.helper.SystemError;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.ims.datamodel.SeatAllocation;
import com.tgs.services.ims.datamodel.SeatAllocationInfo;
import com.tgs.services.ims.jparepository.RatePlanService;
import com.tgs.services.ims.jparepository.air.AirInventoryService;

@Component
public class SeatAllocationValidator extends TgsValidator implements Validator {

	@Autowired
	AirInventoryService inventoryService;

	@Autowired
	RatePlanService ratePlanService;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(target instanceof SeatAllocation) {
			SeatAllocation allocation = (SeatAllocation) target;

			if(allocation.getInventoryId() == null || inventoryService.findById(Long.valueOf(allocation.getInventoryId()))==null) {
				rejectValue(errors,"inventoryId", SystemError.INVALID_INVENTORY_ID, allocation.getInventoryId());	
			}

			if(allocation.getProduct()==null) {
				rejectValue(errors,"product", SystemError.INVALID_PRODUCT, allocation.getProduct());	
			}
			int index = 0;
			for(SeatAllocationInfo allocationInfo : allocation.getAllocationInfo()) {
				
				String allocationField = createSeatAllocationField(index); 
				
				if(allocationInfo.getRatePlanId() ==null || ratePlanService.findById(Long.valueOf(allocationInfo.getRatePlanId()))==null) {
					rejectValue(errors,StringUtils.join(allocationField, ".ratePlanId"), SystemError.INVALID_RATEPLAN_ID, allocationInfo.getRatePlanId());	
				}

				if(allocationInfo.getId()==null && allocationInfo.getStartDate().isAfter(allocationInfo.getEndDate())) {
					rejectValue(errors,StringUtils.join(allocationField, ".startDate"), SystemError.INVALID_DATE_COMBINATION);	
				}

				if(allocationInfo.getId()==null && allocationInfo.getTotalSeats() ==null || allocationInfo.getTotalSeats()<=0) {
					rejectValue(errors,StringUtils.join(allocationField, ".totalSeats"), SystemError.INVALID_TOTAL_SEATS);	
				}
				
				index++;
			}

		}

	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}

	private String createSeatAllocationField(int index) {
		return StringUtils.join("allocationInfo[", index, "]");
	}
}
