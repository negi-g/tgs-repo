package com.tgs.services.ims.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.aerospike.client.query.Filter;
import com.google.gson.Gson;
import com.tgs.services.base.EnumTypeAdapterFactory;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.ims.datamodel.air.AirInventory;
import com.tgs.services.ims.datamodel.air.AirInventoryInfo;
import com.tgs.services.ims.datamodel.air.AirSeatAllocationAdditionalInfo;
import com.tgs.services.ims.dbmodel.air.DbAirInventory;
import com.tgs.services.ims.dbmodel.air.DbSeatAllocation;
import com.tgs.services.ims.jparepository.air.AirInventoryService;
import com.tgs.services.ims.jparepository.air.SeatAllocationService;
import com.tgs.services.ims.restmodel.air.AirInventoryFilter;
import com.tgs.services.ims.restmodel.air.AirSeatInventoryFilter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@InitializerGroup(group = InitializerGroup.Group.GENERAL)
public class SeatAllocationHelper extends InMemoryInitializer {

	@Autowired
	public static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	SeatAllocationService seatAllocationService;

	@Autowired
	AirInventoryService airInventoryService;

	public SeatAllocationHelper(GeneralCachingCommunicator cachingCommunicator) {
		super(null);
		SeatAllocationHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		Runnable task = () -> {
			final int limit = 1000;

			final int concurrencyLevel = 2;
			final Object minAndMax[] = ((Object[]) seatAllocationService.findMinAndMaxIdSeatAllocationInfo()[0]);

			if (!ObjectUtils.isEmpty(minAndMax) && !ObjectUtils.isEmpty(minAndMax[0])
					&& !ObjectUtils.isEmpty(minAndMax[1])) {

				ThreadLocal<Long> from = new ThreadLocal<>();
				ThreadLocal<Long> to = new ThreadLocal<>();
				AtomicLong currFrom = new AtomicLong(0);
				AtomicLong currTo = new AtomicLong(0);
				AtomicInteger seatAllocationCount = new AtomicInteger();

				final long THRESHOLD = (long) minAndMax[1];
				log.info("Started fetching seat allocation static data from database with limit {}, From {} - To {}",
						limit, minAndMax[0], (long) minAndMax[0] + limit);
				ExecutorService storeSearchResultExecutor = Executors.newFixedThreadPool(concurrencyLevel);
				List<Future<?>> futures = new ArrayList<>();
				Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build()
						.buildGson();
				Map<String, AirInventory> airInventoryMap = new HashMap<>();
				AirInventoryFilter airInventoryFilter = AirInventoryFilter.builder().build();
				List<DbAirInventory> airInventoryList = airInventoryService.findAll(airInventoryFilter);
				airInventoryList.forEach(inventory -> {
					airInventoryMap.put(inventory.getId().toString(), inventory.toDomain());
				});
				log.info("AirInventory Allocation Size {}", airInventoryMap.size());
				for (int i = 0; i < 500; i++) {
					futures.add(storeSearchResultExecutor.submit(() -> {

						synchronized (this) {
							from.set((currFrom.get() == 0) ? (long) minAndMax[0] : currFrom.get());
							to.set((currTo.get() == 0) ? (long) minAndMax[0] + limit : currTo.get());
							currFrom.set(to.get() + 1);
							currTo.set(to.get() + limit);
						}

						if (from.get() > THRESHOLD) {
							log.info(
									"Finished fetching seat allocation static data from database as the threshold is reached. Final Seat Allocation fetch count {}, From {} - To {}",
									seatAllocationCount.get(), from.get(), to.get());
							return;
						}
						log.info(
								"Started fetching seat allocation static info from database. Total Seat Allocation Count {}, From {} - To {}",
								seatAllocationCount.get(), from.get(), to.get());
						List<DbSeatAllocation> seatAllocations =
								seatAllocationService.findByIdBetweenAndValidOnGreaterThanEqual(from.get(), to.get());
						log.info(
								"Finished fetching seat allocation static info from database. Curr Seat Allocation Count {}, Total Seat Allocation {}, From {} - To {}",
								seatAllocations.size(), seatAllocationCount.get(), from.get(), to.get());
						List<AirSeatAllocationAdditionalInfo> airSeatAllocationInfos = new ArrayList<>();
						saveInCache(gson, airInventoryMap, seatAllocations, airSeatAllocationInfos);
						seatAllocationCount.getAndAdd(airSeatAllocationInfos.size());
						log.info(
								"Persisted seat allocation static info into aerospike. Curr Seat Allocation Count {}, Total Seat Allocation Count {}, From {} - To {}",
								airSeatAllocationInfos.size(), seatAllocationCount.get(), from.get(), to.get());
					}));
				}
				for (Future<?> future : futures) {
					try {
						future.get();
					} catch (Exception e) {
						log.info("All threads have not comlpeted yet");
					}
				}
				storeSearchResultExecutor.shutdown();
			} else {
				log.info("Unable to fetch seat allocation static info from database as the table is empty");
			}
		};

		Thread dataThread = new Thread(task);
		dataThread.start();
		try {
			dataThread.join();
		} catch (InterruptedException e) {
			log.error("Unable to join UserSeatAllocation thread ", e);
		}
	}

	public void saveInCache(Gson gson, Map<String, AirInventory> airInventoryMap,
			List<DbSeatAllocation> seatAllocations, List<AirSeatAllocationAdditionalInfo> airSeatAllocationInfos) {
		seatAllocations.forEach(seatAllocation -> {
			AirInventory airInventory = airInventoryMap.get(seatAllocation.getInventoryId());
			try {
				AirSeatAllocationAdditionalInfo seatAllocationInfo =
						gson.fromJson(gson.toJson(seatAllocation), AirSeatAllocationAdditionalInfo.class);
				// To check enabled airInventory.
				if (airInventory != null) {
					seatAllocationInfo.setAirlinePnr(seatAllocation.getAdditionalInfo().getAirlinePnr());
					seatAllocationInfo.setDiscount(seatAllocation.getAdditionalInfo().getDiscount());
					seatAllocationInfo.setDoubleDiscount(seatAllocation.getAdditionalInfo().getDoubleDiscount());
					updateSeatAllocation(seatAllocationInfo, airInventory);
					airSeatAllocationInfos.add(seatAllocationInfo);
				}
			} catch (Exception e) {
				log.error("Error occurred while caching allocation for id {} inventory id {}", seatAllocation.getId(),
						airInventory.getId(), e);
			}
		});
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.SEAT_ALLOCATION.getName())
				.namespace(CacheNameSpace.FLIGHT.getName()).build());
	}

	// for async bulk write
	private void store(List<DbSeatAllocation> seatAllocationList, Map<String, AirInventory> airInventoryMap) {
		Map<String, Map<String, String>> keyValueMap = new HashMap<>();
		Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build().buildGson();
		seatAllocationList.forEach(seatAllocation -> {
			AirInventory airInventory = airInventoryMap.get(seatAllocation.getInventoryId());
			if (airInventory != null) {
				Map<String, String> binMap = new HashMap<>();
				AirSeatAllocationAdditionalInfo airSeatAllocation =
						gson.fromJson(gson.toJson(seatAllocation), AirSeatAllocationAdditionalInfo.class);
				binMap.put(BinName.AIRINVENTORYID.getName(), seatAllocation.getInventoryId());
				binMap.put(BinName.SEATALLOCATION.getName(), GsonUtils.getGson().toJson(airSeatAllocation));
				binMap.put(BinName.SEARCHKEY.getName(), getFlightSearchKey(airSeatAllocation, airInventory));
				binMap.put(BinName.INVENTORYKEY.getName(), getFlightKey(airSeatAllocation, airInventory));
				keyValueMap.put(seatAllocation.getId().toString(), binMap);
			}
		});
		cachingCommunicator.asyncStore(
				CacheMetaInfo.builder().set(CacheSetName.SEAT_ALLOCATION.getName())
						.namespace(CacheNameSpace.FLIGHT.getName()).compress(false).plainData(true).build(),
				keyValueMap);
	}


	public static Map<String, AirSeatAllocationAdditionalInfo> searchSeatAllocation(AirSeatInventoryFilter filter) {
		Map<String, AirSeatAllocationAdditionalInfo> seatAllocation = new HashMap<>();
		Map<String, Map<String, String>> seatAllocationMap = new HashMap<>();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.FLIGHT.getName())
				.set(CacheSetName.SEAT_ALLOCATION.getName()).build();
		if (filter.getInventoryId() != null) {
			// filter seat allocation with airInventoryId
			seatAllocationMap = cachingCommunicator.getResultSet(metaInfo, String.class,
					Filter.equal(BinName.AIRINVENTORYID.getName(), filter.getInventoryId()));
		} else {
			// flight search
			for (String supplierId : filter.getSupplierIds()) {
				String searchKey = getFlightSearchKey(filter, supplierId);
				seatAllocationMap.putAll(cachingCommunicator.getResultSet(metaInfo, String.class,
						Filter.equal(BinName.SEARCHKEY.getName(), searchKey)));
				log.debug("Search key is {} and seatAllocationMap size is {}", searchKey, seatAllocationMap.size());
			}
		}

		if (MapUtils.isNotEmpty(seatAllocationMap)) {
			for (Entry<String, Map<String, String>> entrySet : seatAllocationMap.entrySet()) {
				seatAllocation.put(entrySet.getKey(),
						GsonUtils.getGson().fromJson(entrySet.getValue().get(BinName.SEATALLOCATION.getName()),
								AirSeatAllocationAdditionalInfo.class));
			}
		}
		return seatAllocation;
	}

	public static void updateSeatAllocation(AirSeatAllocationAdditionalInfo seatAllocation) {
		Map<String, AirInventory> inventories =
				AirInventoryHelper.searchAirInventory(Arrays.asList(seatAllocation.getInventoryId()));
		AirInventory inventory = inventories.get(seatAllocation.getInventoryId());
		updateSeatAllocation(seatAllocation, inventory);
	}

	public static void updateSeatAllocation(AirSeatAllocationAdditionalInfo seatAllocation, AirInventory airInventory) {
		int expiration = -1;
		if (seatAllocation.isDeleted()) {
			expiration = 1;
		}
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.AIRINVENTORYID.getName(), seatAllocation.getInventoryId());
		binMap.put(BinName.SEATALLOCATION.getName(), GsonUtils.getGson().toJson(seatAllocation));
		binMap.put(BinName.SEARCHKEY.getName(), getFlightSearchKey(seatAllocation, airInventory));
		binMap.put(BinName.INVENTORYKEY.getName(), getFlightKey(seatAllocation, airInventory));
		cachingCommunicator.store(
				CacheMetaInfo.builder().set(CacheSetName.SEAT_ALLOCATION.getName())
						.namespace(CacheNameSpace.FLIGHT.getName()).key(seatAllocation.getId().toString()).build(),
				binMap, false, true, expiration);
	}

	public void updateSeatAllocationKey(AirInventory airInventory) {
		AirSeatInventoryFilter filter =
				AirSeatInventoryFilter.builder().inventoryId(airInventory.getId().toString()).build();
		Map<String, AirSeatAllocationAdditionalInfo> seatAllocationMap = searchSeatAllocation(filter);
		if (MapUtils.isNotEmpty(seatAllocationMap)) {
			for (Entry<String, AirSeatAllocationAdditionalInfo> entrySet : seatAllocationMap.entrySet()) {
				updateSeatAllocation(entrySet.getValue(), airInventory);
			}
		}
	}

	public static String getFlightKey(AirSeatAllocationAdditionalInfo seatAllocation, AirInventory airInventory) {
		return StringUtils.join(airInventory.getFromAirport(), airInventory.getToAirport(), "_",
				airInventory.getCarrier(), "_", seatAllocation.getValidOn());
	}

	public static String getFlightSearchKey(AirSeatAllocationAdditionalInfo seatAllocation, AirInventory airInventory) {
		return StringUtils.join(airInventory.getFromAirport(), airInventory.getToAirport(), "_",
				airInventory.getSupplierId(), "_", seatAllocation.getValidOn());
	}

	public static String getFlightSearchKey(AirSeatInventoryFilter filter, String supplierId) {
		return StringUtils.join(filter.getSource().toArray()[0], filter.getDestination().toArray()[0], "_", supplierId,
				"_", filter.getTravelDate().toString());
	}

	public static void updateTimingsFromCache(SegmentInfo segment, AirInventoryInfo orgSupplierTiming) {
		if (!segment.getDepartTime().equals(orgSupplierTiming.getDepartureTime())) {
			segment.setDepartTime(orgSupplierTiming.getDepartureTime());
			log.debug("Departure Mismatch between deal inventory & supplier timings {}", segment);
		}
		if (!segment.getArrivalTime().equals(orgSupplierTiming.getArrivalTime())) {
			segment.setArrivalTime(orgSupplierTiming.getArrivalTime());
			log.debug("Arrival Mismatch between deal inventory & supplier timings {}", segment);
		}
	}

}

