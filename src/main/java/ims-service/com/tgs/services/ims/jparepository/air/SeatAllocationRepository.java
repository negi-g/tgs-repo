package com.tgs.services.ims.jparepository.air;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.tgs.services.ims.dbmodel.air.DbSeatAllocation;

@Repository
public interface SeatAllocationRepository
		extends JpaRepository<DbSeatAllocation, Long>, JpaSpecificationExecutor<DbSeatAllocation> {

	public List<DbSeatAllocation> findByInventoryIdOrderByValidOn(String inventoryId);

	public DbSeatAllocation findByInventoryIdAndValidOn(String inventoryId, LocalDate validOn);

	@Query(value = "select min(id), max(id) from seatallocation")
	public Object[] findMinAndMaxId();

	public List<DbSeatAllocation> findByIdBetweenAndValidOnGreaterThanEqual(Long from, Long to, LocalDate validon);

	public List<DbSeatAllocation> findByIdBetween(Long from, Long to);

}
