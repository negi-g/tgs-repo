package com.tgs.services.ims.servicehandler.air;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.ims.datamodel.air.AirInventory;
import com.tgs.services.ims.dbmodel.air.DbAirInventory;
import com.tgs.services.ims.helper.AirInventoryHelper;
import com.tgs.services.ims.helper.SeatAllocationHelper;
import com.tgs.services.ims.jparepository.air.AirInventoryService;
import com.tgs.services.ims.restmodel.air.AirInventoryFilter;
import com.tgs.services.ims.restmodel.air.AirInventoryResponse;
import com.tgs.utils.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirInventoryHandler extends ServiceHandler<AirInventory, AirInventoryResponse> {

	@Autowired
	private AirInventoryService service;

	@Autowired
	private FMSCommunicator flightCommunicator;

	@Autowired
	private SeatAllocationHelper seatAllocationHelper;

	DbAirInventory inventory;

	@Override
	public void beforeProcess() throws Exception {
		if (request.getId() != null) {
			inventory = service.findById(request.getId());
			if (inventory == null) {
				throw new ResourceNotFoundException(SystemError.INVALID_ID);
			}
		}
		checkDuplicacy();
	}

	@Override
	public void process() throws Exception {
		boolean updateSeatAllocationKey = false;
		List<SegmentInfo> segmentInfos = request.getSegmentInfos();
		// Create Request
		if (inventory == null) {
			inventory = new GsonMapper<>(request, inventory, DbAirInventory.class).convert();
			inventory.setEnabled(Boolean.TRUE);
			inventory.setSupplierId(ObjectUtils.firstNonNull(request.getSupplierId(),
					SystemContextHolder.getContextData().getUser().getLoggedInUserId()));
		} else {
			inventory = new GsonMapper<>(request, inventory, DbAirInventory.class).convert();
			if (!(segmentInfos.get(0).getDepartAirportInfo().getCode().equals(inventory.getFromAirport())
					&& segmentInfos.get(segmentInfos.size() - 1).getArrivalAirportInfo().getCode()
							.equals(inventory.getToAirport()))) {
				updateSeatAllocationKey = true;
			}
		}
		int segmentNum = 0;
		for (SegmentInfo info : segmentInfos) {
			info.setSegmentNum(segmentNum++);
			AirlineInfo airlineInfo =
					flightCommunicator.getAirlineInfo(info.getFlightDesignator().getAirlineInfo().getCode());
			airlineInfo.setIsTkRequired(false);
			info.getFlightDesignator().setAirlineInfo(airlineInfo);
		}
		inventory.setSegmentInfos(segmentInfos);
		inventory.setFromAirport(segmentInfos.get(0).getDepartAirportInfo().getCode());
		inventory.setToAirport(segmentInfos.get(segmentInfos.size() - 1).getArrivalAirportInfo().getCode());
		inventory.setCarrier(segmentInfos.get(0).getAirlineCode(false));
		service.save(inventory);
		AirInventoryHelper.UpdateAirInventory(inventory.toDomain());
		if (updateSeatAllocationKey) {
			seatAllocationHelper.updateSeatAllocationKey(inventory.toDomain());
		}
		response.getAirInventories().add(inventory.toDomain());
	}

	private void checkDuplicacy() {
		AirInventoryFilter filter = AirInventoryFilter.builder()
				.source(new HashSet<>(Arrays.asList(request.getSegmentInfos().get(0).getDepartureAirportCode())))
				.destination(new HashSet<>(Arrays.asList(
						request.getSegmentInfos().get(request.getSegmentInfos().size() - 1).getArrivalAirportCode())))
				.supplierIds(Arrays.asList(SystemContextHolder.getContextData().getUser().getLoggedInUserId()))
				.tripType(request.getTripType()).deleted(false).build();
		List<DbAirInventory> inventories = service.findAll(filter);
		// This is done to ensure that if during update request same inventory is being updated.
		if (request.getId() != null)
			inventories =
					inventories.stream().filter(p -> !p.getId().equals(request.getId())).collect(Collectors.toList());
		List<Long> invent = new ArrayList<Long>();
		for (DbAirInventory inventory : inventories) {
			if (BaseUtils.matchSegmentInfos(inventory.getSegmentInfos(), request.getSegmentInfos())) {
				log.info(
						"Duplicate inventories found with id {} and request id {}, size of similar inventories found is {}",
						inventory.getId(), request.getId(), inventories.size());
				invent.add(inventory.getId());
			}
		}
		if (CollectionUtils.isNotEmpty(invent)) {
			response.addError(SystemError.DUPLICATE_INVENTORY.getErrorDetail(invent.toString()));
		}
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	public BaseResponse updateInventoryStatus(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbAirInventory dbInventory = service.findById(id);
		if (Objects.nonNull(dbInventory)) {
			dbInventory.setEnabled(status);
			service.save(dbInventory);
			AirInventoryHelper.UpdateAirInventory(dbInventory.toDomain());
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse deleteInventory(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbAirInventory dbInventory = service.findById(id);
		if (Objects.nonNull(dbInventory)) {
			dbInventory.setDeleted(true);
			service.save(dbInventory);
			AirInventoryHelper.UpdateAirInventory(dbInventory.toDomain());
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}
}
