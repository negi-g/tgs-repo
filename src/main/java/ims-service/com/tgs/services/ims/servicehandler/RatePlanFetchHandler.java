package com.tgs.services.ims.servicehandler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.ims.datamodel.ImsConstants;
import com.tgs.services.ims.datamodel.RatePlanFilter;
import com.tgs.services.ims.datamodel.air.AirRatePlan;
import com.tgs.services.ims.dbmodel.DbRatePlan;
import com.tgs.services.ims.jparepository.RatePlanService;
import com.tgs.services.ims.restmodel.air.AirRatePlanResponse;

@Service
public class RatePlanFetchHandler extends ServiceHandler<RatePlanFilter, AirRatePlanResponse> {

	@Autowired
	RatePlanService service;

	@Autowired
	UserServiceCommunicator userServiceComm;

	@Override
	public void beforeProcess() throws Exception {
	}

	@Override
	public void process() throws Exception {
		// To fetch undeleted plans only
		request.setIsDeleted(false);
		List<String> supplierIds = UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), request.getSupplierIds());
		if(supplierIds == null)
			supplierIds = new ArrayList<>();
		supplierIds.add(ImsConstants.DEFAULT_SUPPLIER);
		request.setSupplierIds(supplierIds);

		List<DbRatePlan> planList = service.findAll(request);
		if (CollectionUtils.isNotEmpty(planList)) {
			planList.forEach(dbplan -> {
				AirRatePlan plan = dbplan.toDomain();
				plan.setCabinClass(CabinClass.getEnumFromCode(dbplan.getRatePlanInfo().get(0).getCabinClass()));
				plan.setBookingClass(dbplan.getRatePlanInfo().get(0).getBookingClass());
				response.getRatePlans().add(plan);
			});
		}

	}

	@Override
	public void afterProcess() throws Exception {
	}

}
