package com.tgs.services.ims.jparepository.air;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.tgs.services.ims.dbmodel.DbRatePlan;
import com.tgs.services.ims.dbmodel.air.DbAirInventory;
import com.tgs.services.ims.dbmodel.air.DbSeatAllocation;
import com.tgs.services.ims.restmodel.air.AirInventoryFilter;
import com.tgs.services.ims.restmodel.air.AirSeatInventoryFilter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirInventoryService {

	@Autowired
	AirInventoryRepository repo;

	@PersistenceContext
	EntityManager em;

	public DbAirInventory save(DbAirInventory airInventory) {
		airInventory = repo.saveAndFlush(airInventory);
		return airInventory;
	}

	public DbAirInventory findById(Long id) {
		return repo.findOne(id);
	}

	public List<DbAirInventory> findAll(AirInventoryFilter filter) {
		Specification<DbAirInventory> specification = new Specification<DbAirInventory>() {
			@Override
			public Predicate toPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = AirInventorySearchPredicate.getPredicateListBasedOnAirInventoryFilter(root, query,
						criteriaBuilder, filter);
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
		Sort sort = null;
		if (CollectionUtils.isNotEmpty(filter.getSortByAttr())) {
			Direction direction = Direction.fromString(filter.getSortByAttr().get(0).getOrderBy());
			sort = new Sort(direction, filter.getSortByAttr().get(0).getParams());
		} else {
			sort = new Sort(Direction.DESC, Arrays.asList("createdOn"));
		}
		return repo.findAll(specification,sort);
	}
	
	public List<Object[]> findAllWithSeatAllocation(AirSeatInventoryFilter inventoryFilter) {
		StringBuilder query = new StringBuilder(
				"SELECT {a.*}, {b.*}, {c.*}  FROM airinventory a JOIN seatallocation b ON a.id = cast(b.inventoryid AS int) JOIN rateplan c ON c.id = cast(b.rateplanid AS int) WHERE a.isEnabled = true AND c.isdeleted = false ");
		List<String> queryParams = new ArrayList<>();
		
		if(inventoryFilter!=null) {
			if(inventoryFilter.getTravelDate()!=null) {
				queryParams.add("b.validon = '".concat(inventoryFilter.getTravelDate().toString()).concat("'"));
			}
			if(inventoryFilter.getDestination()!=null) {
				queryParams.add(
						"a.toAirport IN ('".concat(Joiner.on("','").join(inventoryFilter.getDestination())).concat("')"));
			}
			if(inventoryFilter.getSource()!=null) {
				queryParams.add(
						"a.fromAirport IN ('".concat(Joiner.on("','").join(inventoryFilter.getSource())).concat("')"));
			}
			if(inventoryFilter.getTripType()!=null) {
				queryParams.add("a.tripType = '".concat(inventoryFilter.getTripType().getCode()).concat("'"));
			}
			if(CollectionUtils.isNotEmpty(inventoryFilter.getSupplierIds())) {
				queryParams.add(
						"a.supplierId IN ('".concat(Joiner.on("','").join(inventoryFilter.getSupplierIds())).concat("')"));
			}
			if(CollectionUtils.isNotEmpty(inventoryFilter.getAirlines())) {
				queryParams.add(
						"a.carrier IN ('".concat(Joiner.on("','").join(inventoryFilter.getAirlines())).concat("')"));
			}
			if(inventoryFilter.getCabinClass()!=null) {
				queryParams.add("c.ratePlanInfo @> '[{\"cC\": \"".concat(inventoryFilter.getCabinClass().getCode()).concat("\"}]'"));
			}
			if(inventoryFilter.getTotalSeats()!=null) {
				queryParams.add("b.totalseats >= ".concat(inventoryFilter.getTotalSeats().toString()));
			}else {
				queryParams.add("b.totalseats >= 0");
			}
		}
		
		if(CollectionUtils.isNotEmpty(queryParams))
			query.append(" AND ");
		query.append(Joiner.on(" AND ").join(queryParams));
		
		query.append(" order by ").append("b.inventoryid, b.validOn");
		
		query.append(" limit ").append(inventoryFilter.getPageAttr() != null && inventoryFilter.getPageAttr().getSize() != null
				? inventoryFilter.getPageAttr().getSize(): 1000).append(" ;");

		log.debug("[DealInventory] Query formed is {} " , query.toString());
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		List<Object[]> results = entityManager.unwrap(Session.class).createNativeQuery(query.toString())
				.addEntity("a", DbAirInventory.class).addEntity("b", DbSeatAllocation.class).addEntity("c", DbRatePlan.class)
				.setFetchSize(inventoryFilter.getPageAttr() != null ? inventoryFilter.getPageAttr().getSize() : 5)
				.getResultList();
		entityManager.close();
		log.debug("[DealInventory] Results found {}" , results.size());
		return results;
	}
	
}
