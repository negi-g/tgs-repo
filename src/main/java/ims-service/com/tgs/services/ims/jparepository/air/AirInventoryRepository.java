package com.tgs.services.ims.jparepository.air;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.ims.dbmodel.air.DbAirInventory;

@Repository
public interface AirInventoryRepository extends JpaRepository<DbAirInventory, Long>, JpaSpecificationExecutor<DbAirInventory> {

}
