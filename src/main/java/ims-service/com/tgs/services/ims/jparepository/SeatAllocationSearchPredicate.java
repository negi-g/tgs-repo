package com.tgs.services.ims.jparepository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.BooleanUtils;

import com.tgs.services.ims.dbmodel.air.DbSeatAllocation;
import com.tgs.services.ims.restmodel.air.SeatAllocationFilter;

public enum SeatAllocationSearchPredicate {

	INVENTORYID {
		@Override
		public void addPredicate(Root<DbSeatAllocation> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				SeatAllocationFilter filter, List<Predicate> predicates) {
			if(filter.getInventoryId() !=null)
				predicates.add(criteriaBuilder.equal(root.get("inventoryId"),filter.getInventoryId()));	
		}
	},
	ISDELETED {
		@Override
		public void addPredicate(Root<DbSeatAllocation> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				SeatAllocationFilter filter, List<Predicate> predicates) {
			if (filter.getIsDeleted() != null) {
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), BooleanUtils.isTrue(filter.getIsDeleted())));
			}
		}
	},
	VALID_ON {
		@Override
		public void addPredicate(Root<DbSeatAllocation> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				SeatAllocationFilter filter, List<Predicate> predicates) {
			if (filter.getValidOnBeforeDate() != null) {
				predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("validOn"),filter.getValidOnBeforeDate()));
			}
			if (filter.getValidOnAfterDate() != null) {
				predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("validOn"),filter.getValidOnAfterDate()));
			}
			
		}
	};
	public abstract void addPredicate(Root<DbSeatAllocation> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
			SeatAllocationFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnSeatAllocationFilter(Root<DbSeatAllocation> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, SeatAllocationFilter filter) {
		List<Predicate> predicates = new ArrayList<>();
		for (SeatAllocationSearchPredicate inventory : SeatAllocationSearchPredicate.values()) {
			inventory.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}
