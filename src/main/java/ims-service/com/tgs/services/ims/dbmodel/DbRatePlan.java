package com.tgs.services.ims.dbmodel;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.ims.datamodel.air.AirRatePlan;
import com.tgs.services.ims.datamodel.air.AirRatePlanInfo;
import com.tgs.services.ims.hibernate.air.AirRatePlanInfoType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "rateplan")
@Table(name = "rateplan")
@Builder
@Audited
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({
	@TypeDef(name = "AirRatePlanInfoType", typeClass = AirRatePlanInfoType.class),
})
public class DbRatePlan extends BaseModel<DbRatePlan,AirRatePlan> {

	
	@Column
    @Nonnull
	private String name;

	@Column
	private String description;
	
	@Column
	private String supplierId;
	
	@Column
    @CreationTimestamp
	private LocalDateTime createdOn;
	
	@Column
	private String product;
	
	@Column
	@Type(type = "AirRatePlanInfoType")
	private List<AirRatePlanInfo> ratePlanInfo;
	
	@Column
	private boolean isDeleted;
	
	@Override
    public AirRatePlan toDomain() {
        return new GsonMapper<>(this, AirRatePlan.class).convert();
    }

    @Override
    public DbRatePlan from(AirRatePlan dataModel) {
        return new GsonMapper<>(dataModel, this, DbRatePlan.class).convert();
    }
}
