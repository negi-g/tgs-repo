package com.tgs.services.ims.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.ims.dbmodel.DbInventoryOrder;

@Repository
public interface InventoryOrderRepository extends JpaRepository<DbInventoryOrder, Long>, JpaSpecificationExecutor<DbInventoryOrder> {

}
