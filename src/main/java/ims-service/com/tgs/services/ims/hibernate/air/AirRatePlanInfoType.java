package com.tgs.services.ims.hibernate.air;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.ims.datamodel.air.AirRatePlanInfo;

public class AirRatePlanInfoType extends CustomUserType{
	
	@Override
	public Class returnedClass() {
		List<AirRatePlanInfo> travellerList = new ArrayList<>();
		return travellerList.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<AirRatePlanInfo>>() {
		}.getType();
	}

}
