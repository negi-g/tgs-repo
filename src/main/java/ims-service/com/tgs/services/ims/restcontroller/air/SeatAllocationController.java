package com.tgs.services.ims.restcontroller.air;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.ims.datamodel.SeatAllocation;
import com.tgs.services.ims.dbmodel.air.DbSeatAllocation;
import com.tgs.services.ims.restmodel.air.AirSeatAllocationListResponse;
import com.tgs.services.ims.restmodel.air.SeatAllocationFilter;
import com.tgs.services.ims.restmodel.air.SeatAllocationResponse;
import com.tgs.services.ims.servicehandler.air.AirSeatAllocationListingHandler;
import com.tgs.services.ims.servicehandler.air.SeatAllocationHandler;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/ims/v1/seatallocation")
@CustomRequestProcessor(areaRole = AreaRole.IMS_SERVICE,
		includedRoles = {UserRole.ADMIN, UserRole.CALLCENTER, UserRole.SUPPLIER_ADMIN, UserRole.SUPPLIER})
@Scope(value = "session")
public class SeatAllocationController {

	@Autowired
	SeatAllocationHandler allocationHandler;

	@Autowired
	AirSeatAllocationListingHandler allocationListHandler;

	@Autowired
	private AuditsHandler auditHandler;


	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected SeatAllocationResponse saveSeatAllocation(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody SeatAllocation seatAllocationRequest) throws Exception {
		allocationHandler.initData(seatAllocationRequest, new SeatAllocationResponse());
		return allocationHandler.getResponse();
	}


	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected AirSeatAllocationListResponse listSeatAllocation(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody SeatAllocationFilter seatAllocationRequest) throws Exception {
		allocationListHandler.initData(seatAllocationRequest, new AirSeatAllocationListResponse());
		return allocationListHandler.getResponse();
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse delete(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Long id)
			throws Exception {
		return allocationHandler.deleteSeatAllocation(id);
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbSeatAllocation.class, ""));
		return auditResponse;
	}

}
