package com.tgs.services.pms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.pms.datamodel.NRCreditAdditionalInfo;

public class NRCreditAdditionalInfoType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return NRCreditAdditionalInfo.class;
    }
}
