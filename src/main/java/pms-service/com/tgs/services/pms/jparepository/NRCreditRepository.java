package com.tgs.services.pms.jparepository;

import com.tgs.services.pms.dbmodel.DbNRCredit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NRCreditRepository extends JpaRepository<DbNRCredit, Long>, JpaSpecificationExecutor<DbNRCredit> {

    DbNRCredit getByCreditId(String creditId);

    List<DbNRCredit> getByUserIdAndStatusIn(String userId, List<String> statuses);

    @Query(value = "SELECT * FROM nonrevolvingcredit WHERE userId = :userId AND :product = ANY (products) AND status = 'A' "
            + " AND (utilized + :amount ) <= creditAmount ORDER BY id ASC", nativeQuery = true)
    List<DbNRCredit> getUsableCredits(@Param("userId") String userId, @Param("product") String product, @Param("amount") long amount);

    @Query(value = "Select nrc, p from nonrevolvingcredit nrc LEFT JOIN Payment p on p.walletId = nrc.id and p.payUserId = nrc.userId and p.createdOn >= nrc.createdOn and p.createdOn <= CURRENT_TIMESTAMP and p.status = 'S' " +
            " Where nrc.creditExpiry < CURRENT_TIMESTAMP and nrc.status != 'E' ")
    List<Object[]> findCreditsToBeBilled();

}
