package com.tgs.services.pms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.pms.manager.CreditLineBulkMigrateManager;
import com.tgs.services.pms.restmodel.CreditLineMigrationRequest;
import com.tgs.services.pms.restmodel.MigrateCreditLineRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/pms/v1/creditline")
public class CreditLineMigrationController {

	@Autowired
	CreditLineBulkMigrateManager creditLineManager;

	@RequestMapping(value = "/migrate", method = RequestMethod.POST)
	protected BaseResponse save(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid CreditLineMigrationRequest creditLineMigrationRequest) throws Exception {
		log.info("Request received for creditline migration is {}", new Gson().toJson(creditLineMigrationRequest));

		for (CreditLineMigrationRequest clRequest : creditLineMigrationRequest.getMigrationRequests()) {
			creditLineManager.migrateCreditLine(clRequest);

		}
		return new BaseResponse();
	}

	@RequestMapping(value = "/upload-migrations", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse bulkMigrateCreditLine(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody MigrateCreditLineRequest migrationRequest)
			throws Exception {
		return creditLineManager.bulkMigrateCreditLines(migrationRequest);
	}
}
