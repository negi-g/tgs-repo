package com.tgs.services.pms.servicehandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.filters.GstInfoFilter;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.pms.datamodel.ExternalPaymentMode;
import com.tgs.services.pms.datamodel.ExternalPaymentSubmediumMode;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.dbmodel.DbPaymentGatewayConfigInfo;
import com.tgs.services.pms.helper.PaymentConfigurationHelper;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.restmodel.PaymentModeRequest;
import com.tgs.services.pms.restmodel.PaymentModeResponse;
import com.tgs.services.pms.ruleengine.PaymentConfigOutput;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.ruleengine.PaymentGatewayConfigInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PaymentModeHandlerV1 extends PaymentModeHandler {

	@Autowired
	private UserServiceCommunicator userServiceComm;

	public PaymentModeResponse getResponse(PaymentModeRequest request) {
		log.info("Payment modes request received: " + new Gson().toJson(request));
		PaymentModeResponse response = new PaymentModeResponse();
		this.request = request;
		this.response = response;
		restrictMidOfficeRules();
		String bookingId = request.getBookingId();
		String payUserId = ObjectUtils.firstNonNull(request.getPayUserId(),
				UserUtils.getPayUserId(SystemContextHolder.getContextData().getUser()));
		SystemContextHolder.getContextData().getReqIds().add(bookingId);
		User payUser = userServiceComm.getUserFromCache(payUserId);
		Long billingEntityId = request.getBillingEntityId();
		PaymentFact paymentFact = PaymentFact.builder().build();
		setPaymentFact(request, response, bookingId, payUserId, payUser, paymentFact);
		GstInfoFilter gstFilter =
				GstInfoFilter.builder().idIn(Arrays.asList(billingEntityId)).userIdIn(Arrays.asList(payUserId)).build();

		if (Product.AIR.equals(request.getProduct())) {
			listVirtualPaymentMedium(paymentFact, payUser, gstFilter, bookingId);
		}
		// In case of Virtual Payment, we dont need to show other payment mediums
		if (CollectionUtils.isEmpty(response.getPaymentModes())) {
			listWalletPaymentMedium(paymentFact, payUserId);
			listCreditLines(paymentFact, payUserId);
			listNRCredits(paymentFact, payUserId);
			if (BooleanUtils.isFalse(request.isPayNowConfirmLater())) {
				listExternalPaymentMediums(paymentFact, payUserId);
			}
		}
		return response;
	}

	private void listExternalPaymentMediums(PaymentFact paymentFact, String payUserId) {
		List<PaymentConfigurationRule> availableMediums = new ArrayList<>();
		availableMediums.addAll(getExternalPaymentMediumList(PaymentMedium.CREDITCARD, paymentFact));
		availableMediums.addAll(getExternalPaymentMediumList(PaymentMedium.DEBITCARD, paymentFact));
		availableMediums.addAll(getExternalPaymentMediumList(PaymentMedium.NETBANKING, paymentFact));
		availableMediums = TgsCollectionUtils.getNonNullElements(availableMediums);
		if (availableMediums != null) {
			Map<PaymentMedium, List<PaymentConfigurationRule>> mediums =
					availableMediums.stream().collect(Collectors.groupingBy(medium -> medium.getMedium()));

			for (Map.Entry<PaymentMedium, List<PaymentConfigurationRule>> medium : mediums.entrySet()) {
				Map<String, List<ExternalPaymentSubmediumMode>> externalPaymentMode = new HashMap<>();

				for (PaymentConfigurationRule submedium : medium.getValue()) {
					BigDecimal paymentFee = PaymentUtils.getPaymentFee(SystemContextHolder.getContextData().getUser(),
							request.getAmount(), submedium, null);
					BigDecimal partialPaymentFee = getPartialPaymentFee(submedium);
					if (submedium.getExternalPgInfoId() != null) {
						PaymentConfigOutput mediumConfigOutput = (PaymentConfigOutput) submedium.getOutput();
						DbPaymentGatewayConfigInfo mediumConfig =
								getPaymentGatewayConfigs(submedium.getExternalPgInfoId());
						if (mediumConfig != null) {
							PaymentGatewayConfigInfo configInfo = mediumConfig.toDomain();
							ExternalPaymentSubmediumMode submediumsMode = ExternalPaymentSubmediumMode.builder()
									.subType(configInfo.getGatewayType()).paymentFee(paymentFee)
									.subTypeDn(mediumConfigOutput.getSubTypeDn()).priority(submedium.getPriority())
									.ruleId(submedium.getId()).partialpaymentFee(partialPaymentFee).build();

							if (mediumConfigOutput.getTermsAndConditions() == null) {
								submediumsMode.setTnc(configInfo.getTermsAndConditions());
							} else {
								submediumsMode.setTnc(mediumConfigOutput.getTermsAndConditions());
							}
							if (externalPaymentMode.get(mediumConfigOutput.getDisplayName()) != null) {
								List<ExternalPaymentSubmediumMode> paymentSubMediumsMode = new ArrayList<>();
								paymentSubMediumsMode
										.addAll(externalPaymentMode.get(mediumConfigOutput.getDisplayName()));
								paymentSubMediumsMode.add(submediumsMode);
								externalPaymentMode.put(mediumConfigOutput.getDisplayName(), paymentSubMediumsMode);
							} else {
								externalPaymentMode.put(mediumConfigOutput.getDisplayName(),
										Arrays.asList(submediumsMode));
							}
						}
					}
				}
				if (MapUtils.isNotEmpty(externalPaymentMode)) {
					for (Map.Entry<String, List<ExternalPaymentSubmediumMode>> paymentModes : externalPaymentMode
							.entrySet()) {
						ExternalPaymentMode paymentMode = new ExternalPaymentMode(medium.getKey(),
								paymentModes.getValue(), paymentModes.getKey());
						response.getPaymentModes().add(paymentMode);
					}
				}
			}
		}
	}

	private DbPaymentGatewayConfigInfo getPaymentGatewayConfigs(String id) {
		return PaymentConfigurationHelper.getPaymentGatewayConfigs(id);
	}

	private List<PaymentConfigurationRule> getExternalPaymentMediumList(PaymentMedium paymentMedium,
			PaymentFact paymentFact) {
		paymentFact.setMedium(paymentMedium);
		return PaymentConfigurationHelper.getPaymentRuleBasedOnRuleType(paymentFact,
				PaymentRuleType.EXTERNAL_PAYMENT_MEDIUM);
	}
}
