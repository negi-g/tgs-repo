package com.tgs.services.pms.servicehandler;

import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.ExternalPaymentFilter;
import com.tgs.filters.ExternalPaymentStatusInfoFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.ExternalPayment;
import com.tgs.services.pms.datamodel.ExternalPaymentStatusInfo;
import com.tgs.services.pms.dbmodel.DbExternalPayment;
import com.tgs.services.pms.dbmodel.DbExternalPaymentStatusInfo;
import com.tgs.services.pms.jparepository.ExternalPaymentService;
import com.tgs.services.pms.jparepository.ExternalPaymentStatusSearchService;
import com.tgs.services.pms.restmodel.ExternalPaymentDetailResponse;

@Service
public class ExternalPaymentDetailHandler extends ServiceHandler<String, ExternalPaymentDetailResponse> {

	@Autowired
	private ExternalPaymentService externalPaymentService;

	@Autowired
	private ExternalPaymentStatusSearchService externalPaymentStatusSearchService;

	private ExternalPayment payment;

	@Override
	public void beforeProcess() throws Exception {
		if (StringUtils.isBlank(request)) {
			throw new CustomGeneralException(SystemError.INVALID_REQUEST);
		}
	}

	@Override
	public void process() throws Exception {
		ExternalPaymentFilter filter = ExternalPaymentFilter.builder().refId(request).build();
		List<ExternalPayment> paymentList = DbExternalPayment.toDomainList(externalPaymentService.search(filter));
		if (CollectionUtils.isEmpty(paymentList)) {
			return;
		}
		payment = paymentList.get(0);

		ExternalPaymentStatusInfoFilter statusFilter = ExternalPaymentStatusInfoFilter.builder().refId(request).build();
		List<ExternalPaymentStatusInfo> statuses =
				DbExternalPaymentStatusInfo.toDomainList(externalPaymentStatusSearchService.search(statusFilter));
		if (CollectionUtils.isNotEmpty(statuses)) {
			statuses.sort((s1, s2) -> s1.getCreatedOn().compareTo(s2.getCreatedOn()));
		}
		payment.setTimeline(statuses);
	}

	@Override
	public void afterProcess() throws Exception {
		response.setPayment(payment);
	}

}
