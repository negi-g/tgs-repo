package com.tgs.services.pms.restcontroller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.DepositRequestFilter;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.AirlineResponseProcessor;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.pms.datamodel.DIPayment;
import com.tgs.services.pms.datamodel.DepositAction;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.SuccessfulDepositsRequest;
import com.tgs.services.pms.jparepository.DepositRequestService;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.manager.DepositManager;
import com.tgs.services.pms.restcontroller.requestValidator.DepositActionValidator;
import com.tgs.services.pms.restcontroller.requestValidator.DepositRequestValidator;
import com.tgs.services.pms.restmodel.DIPaymentResponse;
import com.tgs.services.pms.restmodel.DepositResponse;
import com.tgs.services.pms.servicehandler.DepositRequestHandler;
import com.tgs.services.pms.servicehandler.SaveSuccessfulDepositsHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/pms/v1")
public class DepositController {

	@Autowired
	private DepositRequestHandler depositRequestHandler;

	@Autowired
	private DepositRequestService depositService;

	@Autowired
	private AuditsHandler auditHandler;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private SaveSuccessfulDepositsHandler saveSuccessfulDepositHandler;

	@Autowired
	private DepositRequestValidator depositRequestValidator;

	@Autowired
	private DepositActionValidator actionValidator;

	@Autowired
	private DepositManager depositManager;

	@Autowired
	private FiltersValidator validator;

	@InitBinder("depositRequestFilter")
	public void initBinderForFilter(WebDataBinder binder) {
		binder.addValidators(validator);
	}

	@InitBinder("depositRequest")
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(depositRequestValidator);
	}

	@RequestMapping(value = "/deposit-req-save", method = RequestMethod.POST)
	protected DepositResponse saveOrUpdateDeposit(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid DepositRequest depositRequest) throws Exception {
		depositRequestHandler.initData(depositRequest, new DepositResponse());
		return depositRequestHandler.saveRequest();
	}

	@RequestMapping(value = "/deposit-req-listing", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected DepositResponse search(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid DepositRequestFilter filter, BindingResult result) throws Exception {
		DepositResponse res = new DepositResponse();
		filter.setPartnerIdIn(UserServiceHelper.returnPartnerIds(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), false, filter.getPartnerIdIn()));
		FiltersValidator validator =
				(FiltersValidator) SpringContext.getApplicationContext().getBean("filtersValidator");
		validator.validate(filter, result);
		if (result.hasErrors()) {
			res.setErrors(validator.getErrorDetailFromBindingResult(result));
		} else {
			res.setDepositRequests(depositService.findAll(filter));
			res.getDepositRequests().forEach(req -> req
					.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), req)));
		}
		return res;
	}

	@RequestMapping(value = "/update-deposit-req/{action}", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected DepositResponse updateDepositAction(HttpServletRequest request,
			@PathVariable @NotNull DepositAction action, @RequestBody @Valid DepositRequest depositRequest)
			throws Exception {
		depositRequestHandler.initData(depositRequest, new DepositResponse());
		depositRequestHandler.setAction(action);
		return depositRequestHandler.getResponse();

	}

	@RequestMapping(value = "/deposit-audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData,
				com.tgs.services.pms.dbmodel.DbDepositRequest.class, "reqId"));
		return auditResponse;
	}

	@RequestMapping(value = "/save-successful-deposits", method = RequestMethod.POST)
	protected BaseResponse saveSuccessfulDeposits(HttpServletRequest request, HttpServletResponse response,
			@RequestBody SuccessfulDepositsRequest depositRequests) throws Exception {
		saveSuccessfulDepositHandler.initData(depositRequests, new BaseResponse());
		return saveSuccessfulDepositHandler.getResponse();
	}

	@CustomRequestMapping(responseProcessors = {AirlineResponseProcessor.class, UserIdResponseProcessor.class})
	@RequestMapping(value = "di/payments", method = RequestMethod.POST)
	protected DIPaymentResponse getPaymentsForDI(@RequestBody @Valid PaymentFilter filter, BindingResult result) {
		filter.setPayUserIdIn(UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), filter.getPayUserIdIn()));
		filter.setPartnerIdIn(UserServiceHelper.returnPartnerIds(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), false, filter.getPartnerIdIn()));
		FiltersValidator validator =
				(FiltersValidator) SpringContext.getApplicationContext().getBean("filtersValidator");
		validator.validate(filter, result);
		if (result.hasErrors()) {
			DIPaymentResponse paymentResponse = new DIPaymentResponse();
			paymentResponse.setErrors(validator.getErrorDetailFromBindingResult(result));
			return paymentResponse;
		} else {
			List<Payment> payments = paymentService.search(filter);
			List<DIPayment> diPayments = depositRequestHandler.getDIPayments(payments);
			return new DIPaymentResponse(diPayments);
		}
	}

	@RequestMapping(value = "/check-accepted-dr/{reqId}", method = RequestMethod.POST)
	protected BaseResponse checkAcceptedDR(@PathVariable @NotNull String reqId) throws Exception {
		depositManager.checkAcceptedDepositRequest(reqId);
		return new BaseResponse();
	}
}
