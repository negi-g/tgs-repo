package com.tgs.services.pms.manager.paymentManagers;

import java.math.BigDecimal;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.manager.PaymentManagerFactory;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class VendorPaymentManager extends AbstractPaymentManager {

	/**
	 * Post Redirection from bank or gateway
	 */
	@Override
	public Payment debit(Payment payment) throws PaymentException {
		log.info("Checking payment status for bookingId {}", payment.getRefId());
		GateWayManager gtM =
				PaymentUtils.getGatewayManager(GateWayType.valueOf(payment.getAdditionalInfo().getGateway()));
		PaymentProcessingResult paymentVerificationResult =
				gtM.verifyPayment(payment, (PgResponse) SystemContextHolder.getContextData().getMetaInfo());
		DbPayment dbPayment = null;
		if (PaymentStatus.SUCCESS.equals(paymentVerificationResult.getStatus())) {
			/**
			 * A successful debit for topup must not be stored. It affects agent's accounting.
			 */
			if (payment.getType().equals(PaymentTransactionType.TOPUP)) {
				payment.setStatus(PaymentStatus.SUCCESS);
				return payment;
			}
			dbPayment = doPayment(payment);
			return dbPayment.toDomain();
		}
		payment.getAdditionalInfo().setTotalDepositAmount(payment.getAmount().subtract(payment.getPaymentFee()));
		throw new PaymentException(SystemError.PAYMENT_FAILED);
	}

	@Override
	public Payment credit(Payment payment) throws PaymentException {
		payment.setPaymentRefId(RandomStringUtils.random(10, false, true));
		payment = PaymentManagerFactory.processGatewayRefund(payment);
		payment.setAmount(payment.getAmount().multiply(BigDecimal.valueOf(-1)));
		DbPayment dbPayment = doPayment(payment);
		return dbPayment.toDomain();
	}

}
