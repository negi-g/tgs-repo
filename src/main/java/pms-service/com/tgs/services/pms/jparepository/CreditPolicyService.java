package com.tgs.services.pms.jparepository;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.IncidenceFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.Incidence.Incidence;
import com.tgs.services.gms.datamodel.Incidence.IncidenceType;
import com.tgs.services.pms.datamodel.CreditBillCycleType;
import com.tgs.services.pms.datamodel.CreditPolicy;
import com.tgs.services.pms.dbmodel.DbCreditPolicy;
import com.tgs.utils.exception.CreditModuleException;

@Service
public class CreditPolicyService {

	@Autowired
	private CreditPolicyRepository repository;

	@Autowired
	private GeneralServiceCommunicator generalServiceCommunicator;

	public CreditPolicy findByPolicyId(String policyId) {
		DbCreditPolicy dbCreditPolicy = repository.findByPolicyId(policyId);
		if (dbCreditPolicy == null)
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
		return dbCreditPolicy.toDomain();
	}

	public List<CreditPolicy> getAll() {
		List<CreditPolicy> creditPolicyList = BaseModel.toDomainList(repository.findAll());
		creditPolicyList.sort(Comparator.comparing(CreditPolicy::getCreatedOn));
		return creditPolicyList;
	}

	public CreditPolicy save(CreditPolicy creditPolicy) {
		DbCreditPolicy dbCreditPolicy = null;
		validatePolicy(creditPolicy);
		if (creditPolicy.getId() != null) {
			dbCreditPolicy = repository.findOne(creditPolicy.getId());
			dbCreditPolicy = new GsonMapper<>(creditPolicy, dbCreditPolicy, DbCreditPolicy.class).convert();
		} else {
			creditPolicy.setPolicyId(creditPolicy.getPolicyId().trim());
			validateDuplicate(creditPolicy);
			dbCreditPolicy = new DbCreditPolicy().from(creditPolicy);
		}
		return repository.saveAndFlush(dbCreditPolicy).toDomain();
	}

	private void validatePolicy(CreditPolicy creditPolicy) {
		validateCycle(creditPolicy);
		validateIncidences(creditPolicy);
	}

	private void validateDuplicate(CreditPolicy creditPolicy) {
		List<CreditPolicy> creditPolicyList = getAll();
		for (CreditPolicy c : creditPolicyList) {
			if (c.getPolicyId().toLowerCase().equals(creditPolicy.getPolicyId().toLowerCase())) {
				throw new CreditModuleException(SystemError.DUPLICATE_CREDIT_POLICY);
			}
		}
	}

	private void validateIncidences(CreditPolicy creditPolicy) {
		List<String> incidenceIds = creditPolicy.getAdditionalInfo().getIncidenceList();
		if (!CollectionUtils.isEmpty(incidenceIds)) {
			List<Incidence> incidenceList = generalServiceCommunicator
					.getIncidences(IncidenceFilter.builder().incidenceIdIn(incidenceIds).build());
			Set<IncidenceType> incidenceTypes =
					incidenceList.stream().map(Incidence::getIncidenceType).collect(Collectors.toSet());
			if (incidenceList.size() != incidenceTypes.size()) {
				throw new CustomGeneralException(SystemError.DUPLICATE_INCIDENCE_TYPE);
			}
		}
	}

	private void validateCycle(CreditPolicy creditPolicy) {
		if ((creditPolicy.getBillCycleType().equals(CreditBillCycleType.DYNAMIC)
				|| creditPolicy.getBillCycleType().equals(CreditBillCycleType.FIXED_NUM_OF_DAYS))
				&& (creditPolicy.getAdditionalInfo().getBillCycleDays() == null
						|| creditPolicy.getAdditionalInfo().getBillCycleDays() <= 0)) {
			throw new CustomGeneralException(SystemError.CP_INVALID_BILL_CYCLE_DAY);
		}

		if (creditPolicy.getAdditionalInfo() != null && (creditPolicy.getAdditionalInfo().getLockAfterHours() < 0
				|| creditPolicy.getAdditionalInfo().getPaymentDueHours() < 0)) {
			throw new CustomGeneralException(SystemError.CP_INVALID_PAYMENT_LOCK_HOUR);
		}

		if (creditPolicy.getBillCycleType().equals(CreditBillCycleType.FIXED_DAY_OF_WEEK)
				&& (creditPolicy.getAdditionalInfo() == null
						|| creditPolicy.getAdditionalInfo().getBillingDOW() == null)) {
			throw new CustomGeneralException(SystemError.CP_INVALID_BILL_CYCLE_DAY);
		}

		if (creditPolicy.getBillCycleType().equals(CreditBillCycleType.FIXED_DAY_OF_MONTH)
				&& (creditPolicy.getAdditionalInfo() == null
						|| creditPolicy.getAdditionalInfo().getBillingDOM() == null)) {
			throw new CustomGeneralException(SystemError.CP_INVALID_BILL_CYCLE_DAY);
		}
		
		if (creditPolicy.getBillCycleType().equals(CreditBillCycleType.CALENDAR)) {
			if (creditPolicy.getAdditionalInfo() == null
					|| StringUtils.isBlank(creditPolicy.getAdditionalInfo().getAssociatedCalendar()))
				throw new CustomGeneralException(SystemError.CP_INVALID_ASSOCIATED_CAL);
			List<Document> documents = generalServiceCommunicator.fetchDocument(CollectionServiceFilter.builder()
					.key(creditPolicy.getAdditionalInfo().getAssociatedCalendar() + "_" + LocalDate.now().getYear()).build());
			if (CollectionUtils.isEmpty(documents)) {
				throw new CustomGeneralException(SystemError.CP_INVALID_ASSOCIATED_CAL);
			}

		}
	}

	public void delete(Long id) {
		repository.delete(id);
	}
}
