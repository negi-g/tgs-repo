package com.tgs.services.pms.manager.paymentManagers;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.tgs.filters.CreditLineFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.CurrencyConverter;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.user.BasePaymentUtils;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.datamodel.WalletStatus;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.dbmodel.DbUserWallet;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.jparepository.UserWalletService;
import com.tgs.services.pms.manager.CreditLineManager;
import com.tgs.services.pms.manager.NRCreditManager;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class WalletPaymentManager extends AbstractPaymentManager {

	@Autowired
	private MsgServiceCommunicator msgServiceCommunicator;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private UserWalletService walletService;

	@Autowired
	private CreditLineService creditlineService;

	@Autowired
	private PaymentMessenger paymentMsg;

	@Autowired
	@Lazy
	protected NRCreditManager nrCreditManager;

	@Autowired
	@Lazy
	protected CreditLineManager clManager;

	@Override
	public Payment debit(Payment payment) throws PaymentException {
		payment.setCurrentBalance(updateWalletBalance(payment, false));
		setTotalClosingBalance(payment);
		DbPayment dbPayment = doPayment(payment);
		sendSms("debited from", payment);
		return dbPayment.toDomain();
	}

	@Override
	public Payment credit(Payment payment) throws PaymentException {
		payment.setAmount(payment.getAmount().multiply(BigDecimal.valueOf(-1)));
		payment.setCurrentBalance(updateWalletBalance(payment, true));
		setTotalClosingBalance(payment);
		DbPayment dbPayment = doPayment(payment);
		sendSms("credited to", payment);
		return dbPayment.toDomain();
	}


	protected void setTotalClosingBalance(Payment payment) {
		if (BasePaymentUtils.isStoreClosingBalance()) {
			List<CreditLine> creditLines = creditlineService
					.search(CreditLineFilter.builder().userIdIn(Arrays.asList(payment.getPayUserId())).build());
			BigDecimal totalDues = BigDecimal.ZERO;
			if (CollectionUtils.isNotEmpty(creditLines)) {
				for (CreditLine creditline : creditLines) {
					totalDues.add(creditline.getOutstandingBalance());
				}
			}
			payment.getAdditionalInfo().setTotalClosingBalance(payment.getCurrentBalance().subtract(totalDues));
		}

	}

	public BigDecimal updateWalletBalance(Payment payment, boolean isCredit) {
		BigDecimal amount = payment.getAmount().multiply(BigDecimal.valueOf(-1));
		DbUserWallet walletBalance = walletService.findByUserId(payment.getPayUserId());
		List<PaymentTransactionType> transTypes = Arrays.asList(PaymentTransactionType.USER_FUND_TRANSFER,
				PaymentTransactionType.TOPUP, PaymentTransactionType.REFUND, PaymentTransactionType.REVERSE,
				PaymentTransactionType.COMMISSION, PaymentTransactionType.DEPOSIT_INCENTIVE,
				PaymentTransactionType.INTERNAL_PAYMENT);
		if (WalletStatus.ACTIVE.getStatus().equals(walletBalance.getStatus())
				|| transTypes.contains(payment.getType())) {
			long balance = (CurrencyConverter.toSubUnit(amount) + walletBalance.getBalance());
			if (isCredit) {
				return updateWalletBalance(walletBalance, balance);
			} else {
				// In case of debit, balance can go negative only till minBalance
				long minBalance = BasePaymentUtils.getMinBalanceAllowed(payment.getOpType(), payment.getType());
				if (balance >= CurrencyConverter.toSubUnit(BigDecimal.valueOf(minBalance))) {
					return updateWalletBalance(walletBalance, balance);
				} else {
					log.debug("[Insufficient balance] Wallet balance {}, Min Balance {}, requested balance {} ",
							walletBalance.getBalance(), CurrencyConverter.toSubUnit(BigDecimal.valueOf(minBalance)),
							balance);
					throw new PaymentException(SystemError.INSUFFICIENT_BALANCE);
				}
			}
		} else {
			throw new PaymentException(SystemError.USER_WALLET_BLOCKED_ACCESS_DENIED);
		}

	}

	private BigDecimal updateWalletBalance(DbUserWallet walletBalance, long balance) {
		walletBalance.setBalance(balance);
		walletBalance = walletService.save(walletBalance);
		return CurrencyConverter.toUnit(walletBalance.getBalance());
	}

	private void sendSms(String messageKey, Payment payment) {
		if ((payment.getType().equals(PaymentTransactionType.COMMISSION)
				&& payment.getOpType().equals(PaymentOpType.CREDIT))
				|| payment.getType().equals(PaymentTransactionType.PAID_FOR_ORDER))
			return;
		AbstractMessageSupplier<SmsAttributes> msgAttributes = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				User user = userService.getUserFromCache(payment.getPayUserId());
				Map<String, String> attributes = new HashMap<>();
				attributes.put("key", messageKey);
				attributes.put("amount",
						TgsStringUtils.formatCurrency(payment.getAmount().abs(), BaseUtils.getNationality()));
				attributes.put("currBlc",
						TgsStringUtils.formatCurrency(payment.getCurrentBalance().abs(), BaseUtils.getNationality()));
				if (payment.getAdditionalInfo().getAgentId() != null && "debited from".equals(messageKey)) {
					User payUser = userService.getUserFromCache(payment.getAdditionalInfo().getAgentId());
					attributes.put("agentCredit", "and credited to userid " + payment.getAdditionalInfo().getAgentId()
							+ " - " + payUser.getName());
				}
				SmsAttributes smsAttr = SmsAttributes.builder().key(SmsTemplateKey.WALLET_UPDATE_SMS.name())
						.recipientNumbers(Arrays.asList(user.getMobile())).attributes(attributes)
						.partnerId(payment.getPartnerId()).role(user.getRole()).build();
				return smsAttr;
			}
		};
		msgServiceCommunicator.sendMessage(msgAttributes.getAttributes());
	}

	public void lockUsersWallet(Set<String> userIds) {
		for (String userId : userIds) {
			updateWalletStatus(userId, "", WalletStatus.BLOCKED);
		}
	}

	public void changeWalletStatus(UserWallet userWallet) {
		User user = userService.getUserFromCache(userWallet.getUserId());
		if (user == null) {
			throw new CustomGeneralException(SystemError.INVALID_USERID);
		}
		if (WalletStatus.BLOCKED.equals(userWallet.getStatus())) {
			locklinkedUserCredits(user);
		}
		updateWalletStatus(userWallet.getUserId(), userWallet.getComments(), userWallet.getStatus());
	}

	private void locklinkedUserCredits(User user) {
		Set<String> linkedUserIds = user.getAdditionalInfo().getLinkedUserIds();
		if (CollectionUtils.isNotEmpty(linkedUserIds)) {
			lockUsersWallet(linkedUserIds);
			linkedUserIds.add(user.getUserId());
			clManager.lockUsersCreditLine(linkedUserIds);
			nrCreditManager.lockUserNrCredit(linkedUserIds);
		}
	}

	public DbUserWallet getUserWallet(String userId) {
		User user = userService.getUserFromCache(userId);
		if (user == null) {
			throw new CustomGeneralException(SystemError.INVALID_USERID);
		}
		return walletService.findByUserId(userId);
	}

	public void updateWalletStatus(String userId, String comments, WalletStatus status) {
		DbUserWallet wallet = getUserWallet(userId);
		wallet.setComments(comments);
		wallet.setStatus(status.getCode());
		walletService.save(wallet);
		sendEmail(userId, status);
	}

	public void sendEmail(String userId, WalletStatus status) {
		EmailTemplateKey key = WalletStatus.ACTIVE.equals(status) ? EmailTemplateKey.WALLET_UNLOCK_EMAIL
				: EmailTemplateKey.WALLET_LOCK_EMAIL;
		paymentMsg.sendWalletEmail(userId, key);
	}

}
