package com.tgs.services.pms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.pms.datamodel.UserPoint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "userpoint")
@Table(name = "userpoint")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DbUserPoint extends BaseModel<DbUserPoint, UserPoint> {

	@Column
	private String userId;

	@Column
	private String type;

	@Column
	private Double balance;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Override
	public UserPoint toDomain() {
		return new GsonMapper<>(this, UserPoint.class).convert();
	}

	@Override
	public DbUserPoint from(UserPoint dataModel) {
		return new GsonMapper<>(dataModel, this, DbUserPoint.class).convert();
	}
}
