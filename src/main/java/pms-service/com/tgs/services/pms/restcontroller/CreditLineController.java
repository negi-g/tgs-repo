package com.tgs.services.pms.restcontroller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.CreditFilter;
import com.tgs.filters.CreditLineFilter;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.manager.CreditLineManager;
import com.tgs.services.pms.manager.CreditManager;
import com.tgs.services.pms.restmodel.CreditIssueRequest;
import com.tgs.services.pms.restmodel.CreditLineResponse;
import com.tgs.services.pms.restmodel.CreditResponse;
import com.tgs.services.pms.restmodel.SaveCreditLineRequest;
import com.tgs.services.ums.datamodel.AreaRole;

@RestController
@RequestMapping("/pms/v1/creditline")
public class CreditLineController {

	@Autowired
	private CreditLineService service;

	@Autowired
	private CreditLineManager creditLineManager;

	@Autowired
	private CreditManager creditManager;

	@Autowired
	private AuditsHandler auditHandler;

	@RequestMapping(value = "/{creditNumber}", method = RequestMethod.GET)
	protected CreditLineResponse getByCreditId(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("creditNumber") String creditNumber) throws Exception {
		CreditLine creditLine = service.findByCreditNumber(creditNumber);
		return new CreditLineResponse(creditLine);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.PMS_SERVICE, includedRoles = {UserRole.ADMIN, UserRole.ACCOUNTS})
	protected CreditLineResponse save(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid SaveCreditLineRequest creditLineRequest) throws Exception {
		CreditLine creditLine = creditLineManager.save(creditLineRequest);
		return new CreditLineResponse(creditLine);
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected CreditLineResponse search(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid CreditLineFilter filter) throws Exception {
		return new CreditLineResponse(service.search(filter));
	}

	@RequestMapping(value = "/issue", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.PMS_SERVICE, includedRoles = {UserRole.ADMIN, UserRole.ACCOUNTS})
	protected CreditLineResponse issue(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid CreditIssueRequest issueRequest) throws Exception {
		CreditLine creditLine = creditLineManager.issueCreditLine(issueRequest.getPolicy(), issueRequest.getUserId());
		return new CreditLineResponse(creditLine);
	}


	@RequestMapping(value = "/getAllCredits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected CreditResponse getAllCredits(@RequestBody @Valid CreditFilter filter) throws Exception {
		return new CreditResponse(creditManager.search(filter));
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData,
				com.tgs.services.pms.dbmodel.DbCreditLine.class, "creditNumber"));
		return auditResponse;
	}

	@RequestMapping(value = "/job/lockunused", method = RequestMethod.GET)
	protected BaseResponse lock(@QueryParam("scanHours") Short scanHours) throws Exception {
		List<String> creditIds = creditManager.lockUnusedCreditLines(scanHours);
		BaseResponse response = new BaseResponse();
		creditIds.forEach(cid -> response.getMetaInfo().put(cid, null));
		return response;
	}
}
