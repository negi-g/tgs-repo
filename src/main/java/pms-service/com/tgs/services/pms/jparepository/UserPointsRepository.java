package com.tgs.services.pms.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.tgs.services.pms.dbmodel.DbUserPoint;

@Repository
public interface UserPointsRepository
		extends JpaRepository<DbUserPoint, Long>, JpaSpecificationExecutor<DbUserPoint> {

	public List<DbUserPoint> findByUserId(String userId);

	public DbUserPoint findByUserIdAndType(String userId, String type);

}
