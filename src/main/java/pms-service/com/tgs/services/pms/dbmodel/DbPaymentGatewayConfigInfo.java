
package com.tgs.services.pms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.pms.ruleengine.PaymentGatewayConfigInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "paymentgatewayconfiginfo", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
@Audited
public class DbPaymentGatewayConfigInfo extends BaseModel<DbPaymentGatewayConfigInfo, PaymentGatewayConfigInfo>{
	
	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	@CreationTimestamp
	private LocalDateTime processedOn;
	
	@Column
	private String gatewayType;
	
	@Column
	private String name;
	
	@Column
	private boolean enabled;
	
	@Column
	private boolean isDeleted;
	
	@Column
	private boolean isHidden;
	
	@Column
	private String termsAndConditions;

	@Column
	@JsonAdapter(JsonStringSerializer.class)
	private String gateWayInfo;

	public static DbPaymentGatewayConfigInfo create(PaymentGatewayConfigInfo configs) {
		return new DbPaymentGatewayConfigInfo().from(configs);
	}

	@Override
	public PaymentGatewayConfigInfo toDomain() {
		return new GsonMapper<>(this, PaymentGatewayConfigInfo.class, true).convert();
	}

	public PaymentGatewayConfigInfo toDomain(PaymentGatewayConfigInfo configs) {
		return new GsonMapper<>(this, configs, PaymentGatewayConfigInfo.class).convert();
	}

	@Override
	public DbPaymentGatewayConfigInfo from(PaymentGatewayConfigInfo rule) {
		return new GsonMapper<>(rule, this, DbPaymentGatewayConfigInfo.class).convert();
	}

}
