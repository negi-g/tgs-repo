package com.tgs.services.pms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.pms.ruleengine.PaymentBasicRuleInclusionCriteria;

public class PaymentBasicRuleInclusionCriteriaType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return PaymentBasicRuleInclusionCriteria.class;
    }
}
