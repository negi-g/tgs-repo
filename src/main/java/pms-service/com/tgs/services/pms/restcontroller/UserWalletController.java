package com.tgs.services.pms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.dbmodel.DbUserWallet;
import com.tgs.services.pms.manager.paymentManagers.WalletPaymentManager;
import com.tgs.services.pms.restmodel.UserWalletResponse;
import com.tgs.services.ums.datamodel.AreaRole;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/pms/v1/wallet")
@Slf4j
public class UserWalletController {
	
	@Autowired
	AuditsHandler auditHandler;
	
	@Autowired
	private WalletPaymentManager walletManager;
	
	
	@RequestMapping(value = "/update/status", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.PMS_SERVICE, isMidOfficeAllowed = true)
	protected @ResponseBody BaseResponse updateUserWallet(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody UserWallet userWallet) throws Exception {
		walletManager.changeWalletStatus(userWallet);
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/list/{userId}", method = RequestMethod.GET)
	@CustomRequestProcessor(areaRole = AreaRole.PMS_SERVICE, isMidOfficeAllowed = true)
	protected @ResponseBody UserWalletResponse listUserWallet(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("userId") String userId) throws Exception {
		DbUserWallet userWallet = walletManager.getUserWallet(userId);
		UserWallet wallet = userWallet != null? userWallet.toDomain(): null;
		return new UserWalletResponse(wallet);
	}
	
	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.PMS_SERVICE, isMidOfficeAllowed = true)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchWalletAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData,
				com.tgs.services.pms.dbmodel.DbUserWallet.class, "userId"));
		return auditResponse;
	}

}
