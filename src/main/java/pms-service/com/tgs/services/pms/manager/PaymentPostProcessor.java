package com.tgs.services.pms.manager;

import com.tgs.services.pms.datamodel.*;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.jparepository.CreditBillingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaymentPostProcessor {

    @Autowired
    private CreditBillingService billingService;

    @Autowired
    private DepositManager depositManager;

    @Autowired
    @Qualifier("CreditBillingManager")
    private CreditBillingManager billingManager;

    public void doPaymentPostProcessing(List<Payment> paymentList) {
        List<Payment> billPayments = paymentList.stream().filter(PaymentUtils::forBillSettlement).collect(Collectors.toList());
        for (Payment payment : billPayments) {
            BigDecimal amount = payment.getAmount().abs();
            CreditBill bill = billingService.findBillByBillNumber(payment.getAdditionalInfo().getBillNumber());
            bill.getAdditionalInfo().getSettlementPaymentIds().add(payment.getId());
            bill.setSettledAmount(bill.getSettledAmount().add(amount));
            if (bill.getPendingAmount().compareTo(BigDecimal.valueOf(0.01)) <= 0) {
                bill.setIsSettled(true);
                bill.getAdditionalInfo().setSettledDate(LocalDateTime.now());
            } else if (bill.getPendingAmount().compareTo(BigDecimal.valueOf(500)) <= 0) {
                bill.getAdditionalInfo().setThresholdSettled(true);
            }
            billingService.save(bill);
        }

        Payment payment = paymentList.get(0);
        if (payment.getType().equals(PaymentTransactionType.TOPUP) && payment.getStatus().equals(PaymentStatus.SUCCESS)
                && payment.getPaymentMedium().isWalletPaymentMedium() && payment.getOpType().equals(PaymentOpType.CREDIT)) {
             depositManager.acceptDepositRequest(payment.getRefId(), Payment.sum(paymentList));
        }
    }

}