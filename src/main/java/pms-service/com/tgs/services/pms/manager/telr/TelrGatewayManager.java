package com.tgs.services.pms.manager.telr;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.datamodel.pg.telr.TelrMerchantInfo;
import com.tgs.services.pms.datamodel.pg.telr.TelrRefundInfo;
import com.tgs.services.pms.datamodel.pg.telr.TelrTransactionDetails;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.common.NumberUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TelrGatewayManager extends GateWayManager {

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		TelrMerchantInfo telrGatewayInfo = (TelrMerchantInfo) gatewayInfo;
		TelrMerchantInfo telrPgInfo = TelrMerchantInfo.builder().o(telrGatewayInfo.getO())
				.gatewayURL(telrGatewayInfo.getGatewayURL()).build();
		return super.convertToMap(telrPgInfo);
	}

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		PaymentProcessingResult processingResult = new PaymentProcessingResult();
		if (payment.getAdditionalInfo().getOriginalPaymentRefId() == null) {
			processingResult.setStatus(PaymentStatus.FAILURE);
		} else {
			processingResult.setStatus(PaymentStatus.SUCCESS);
		}
		return processingResult;
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		PaymentProcessingResult trackingResult = null;
		try {
			TelrMerchantInfo telrGatewayInfo = (TelrMerchantInfo) gatewayMerchantInfo;
			String postData = getCheckRequestData(telrGatewayInfo, payment);
			Map<String, String> headerParams = new HashMap<String, String>();
			headerParams.put("Content-Type", "application/json");
			HttpUtils httpUtils = HttpUtils.builder().headerParams(headerParams).postData(postData)
					.urlString(telrGatewayInfo.getUrl()).build();
			Map<String, Object> responseObj = httpUtils.getResponse(Map.class).orElse(new HashMap<>());
			log.info("Telr Track order for Telr payment with refId {} is {}", payment.getRefId(),
					GsonUtils.getGson().toJson(responseObj));
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now())
					.logData(httpUtils.getUrlString()).key(payment.getRefId()).type("Telr Track Order Request")
					.logType("AirSupplierAPILogs").build()));
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now())
					.logData(GsonUtils.getGson().toJson(responseObj)).key(payment.getRefId())
					.type("Telr Track order Response").logType("AirSupplierAPILogs").build()));
			if (responseObj.get("order") != null) {
				TelrTransactionDetails transactionDetails = GsonUtils.getGson()
						.fromJson(GsonUtils.getGson().toJson(responseObj.get("order")), TelrTransactionDetails.class);
				trackingResult = prepareTrackingResult(payment, transactionDetails);
			} else {
				trackingResult = PaymentProcessingResult.builder().status(PaymentStatus.FAILURE).build();
			}
		} catch (Exception e) {
			log.error("Fetching payment status failed for refId {} due to", payment.getRefId(), e);
			throw new PaymentException(SystemError.PAYMENT_TRACKING_FAILED);
		}
		return trackingResult;
	}

	private PaymentProcessingResult prepareTrackingResult(Payment payment, TelrTransactionDetails transactionDetails) {
		PaymentProcessingResult trackingResult = new PaymentProcessingResult();
		trackingResult.getExternalPaymentInfo().setGatewayStatusCode(transactionDetails.getStatus().get("code"));
		if ("Paid".equals(transactionDetails.getStatus().get("text"))) {
			if (NumberUtils.compareDoubleUpto2Decimal(Double.valueOf(transactionDetails.getAmount()),
					payment.getAdditionalInfo().getExchangedAmount().doubleValue()) == 0) {
				trackingResult.setStatus(PaymentStatus.SUCCESS);
			} else {
				trackingResult.setStatus(PaymentStatus.FAILURE);
				trackingResult.getExternalPaymentInfo().setGatewayComment("Amount mismatch");
			}
			trackingResult.getExternalPaymentInfo().setMerchantTxnId(transactionDetails.getTransaction().get("ref"));
			payment.setMerchantTxnId(transactionDetails.getTransaction().get("ref"));
		} else if ("Authorised".equals(transactionDetails.getStatus().get("text"))) {
			trackingResult.setStatus(PaymentStatus.PENDING);
		} else {
			trackingResult.setStatus(PaymentStatus.FAILURE);
		}
		return trackingResult;
	}

	private String getCheckRequestData(TelrMerchantInfo telrGatewayInfo, Payment payment) {
		Map<String, Object> params = new HashMap<>();
		Map<String, String> orderParams = new HashMap<>();
		params.put("method", "check");
		getAuthParams(telrGatewayInfo, params);
		orderParams.put("ref", payment.getAdditionalInfo().getOriginalPaymentRefId());
		params.put("order", orderParams);
		return GsonUtils.getGson().toJson(params);
	}

	public void getAuthParams(TelrMerchantInfo telrGatewayInfo, Map<String, Object> params) {
		params.put("store", telrGatewayInfo.getStoreId());
		params.put("authkey", telrGatewayInfo.getOrderAuthKey());
	}

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment,
			PaymentConfigurationRule rule) {
		TelrMerchantInfo telrGatewayInfo = (TelrMerchantInfo) gatewayInfo;
		BigDecimal exchangedAmount = getExchangedAmount(payment.getAmount(), telrGatewayInfo);
		payment.getAdditionalInfo().setExchangedAmount(exchangedAmount);
		payment.getAdditionalInfo().setExchangedCurrency(telrGatewayInfo.getCurrency());
		telrGatewayInfo.appendRefIdToCallBack(payment.getRefId());
		telrGatewayInfo.setAmount(exchangedAmount.toString());
		createOrderAndSetGatewayUrl(telrGatewayInfo, payment);
	}

	public BigDecimal getExchangedAmount(BigDecimal amount, TelrMerchantInfo telrGatewayInfo) {
		ClientGeneralInfo clientInfo = super.getClientInfo();
		BigDecimal exchangedAmount =
				super.getAmountBasedOnCurrency(amount, clientInfo.getCurrencyCode(), telrGatewayInfo.getCurrency());
		return exchangedAmount;
	}

	private void createOrderAndSetGatewayUrl(TelrMerchantInfo telrGatewayInfo, PaymentRequest payment) {
		String postData = getRequestData(telrGatewayInfo, payment);
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Content-Type", "application/json");
		HttpUtils httpUtils = HttpUtils.builder().headerParams(headerParams).postData(postData)
				.urlString(telrGatewayInfo.getUrl()).build();
		try {
			Map<String, Object> responseObj = httpUtils.getResponse(Map.class).orElse(new HashMap<>());
			log.info("Create order for Telr payment with refId {} is {}", payment.getRefId(),
					GsonUtils.getGson().toJson(responseObj));
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now())
					.logData(httpUtils.getUrlString()).key(payment.getRefId()).type("Telr Create Order Request")
					.logType("AirSupplierAPILogs").build()));
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now())
					.logData(GsonUtils.getGson().toJson(responseObj)).key(payment.getRefId())
					.type("Telr Create order Response").logType("AirSupplierAPILogs").build()));
			if (responseObj.get("order") != null) {
				String responseData = GsonUtils.getGson().toJson(responseObj.get("order"));
				Map<String, String> responseMap =
						GsonUtils.getGson().fromJson(responseData, new TypeToken<Map<String, Object>>() {}.getType());
				String[] urlParams = responseMap.get("url").split("[?=]");
				telrGatewayInfo.setGatewayURL(urlParams[0]);
				telrGatewayInfo.setO(urlParams[2]);
				payment.setOriginalPaymentRefId(responseMap.get("ref"));
			} else {
				throw new CustomGeneralException(SystemError.FAILED_TO_INITIALIZE_PAYMENT);
			}
		} catch (IOException e) {
			throw new CustomGeneralException(SystemError.FAILED_TO_INITIALIZE_PAYMENT);
		}
	}

	private String getRequestData(TelrMerchantInfo telrGatewayInfo, PaymentRequest payment) {
		Map<String, Object> params = new HashMap<>();
		Map<String, String> orderParams = new HashMap<>();
		Map<String, String> returnParams = new HashMap<>();
		params.put("method", "create");
		getAuthParams(telrGatewayInfo, params);
		orderParams.put("cartid", payment.getRefId());
		orderParams.put("test", telrGatewayInfo.getTest());
		orderParams.put("amount", telrGatewayInfo.getAmount());
		orderParams.put("currency", telrGatewayInfo.getCurrency());
		orderParams.put("description", telrGatewayInfo.getDescription());
		returnParams.put("authorised", telrGatewayInfo.getReturnUrl());
		returnParams.put("cancelled", telrGatewayInfo.getReturnUrl());
		returnParams.put("declined", telrGatewayInfo.getReturnUrl());
		params.put("order", orderParams);
		params.put("return", returnParams);
		return GsonUtils.getGson().toJson(params);
	}

	@Override
	protected RefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		TelrMerchantInfo telrGatewayInfo = (TelrMerchantInfo) gatewayInfo;
		return TelrRefundInfo.builder().ivp_test(telrGatewayInfo.getTest()).tran_ref(payment.getMerchantTxnId())
				.ivp_trantype("refund").ivp_tranclass(telrGatewayInfo.getTransClass())
				.ivp_store(telrGatewayInfo.getStoreId()).ivp_currency(telrGatewayInfo.getCurrency())
				.ivp_amount(getExchangedAmount(payment.getAmount(), telrGatewayInfo).toString())
				.ivp_authkey(telrGatewayInfo.getRemoteAuthKey()).build();
	}

	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo)
			throws Exception {
		boolean isRefundSuccessful = false;
		String refundId = null;
		TelrMerchantInfo telrGatewayInfo = (TelrMerchantInfo) gatewayInfo;
		TelrRefundInfo telrRefundInfo = (TelrRefundInfo) refundInfo;
		Map<String, String> queryParams = new Gson().fromJson(GsonUtils.getGson().toJson(telrRefundInfo),
				new TypeToken<HashMap<String, String>>() {}.getType());
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Content-Type", "text/html");
		HttpUtils httpUtils = HttpUtils.builder().headerParams(headerParams).postData(getPostData(queryParams))
				.urlString(telrGatewayInfo.getRefundURL()).build();
		String response = (String) httpUtils.getResponse(null).get();
		Map<String, String> mappedResp = getMappedData(response);
		if (mappedResp.get("auth_status").equals("A")) {
			isRefundSuccessful = true;
			refundId = mappedResp.get("auth_tranref");
		} else {
			isRefundSuccessful = false;
		}

		log.info("Request URL {}, Refund Response received from telr payment gateway for ref Id {} is {}",
				httpUtils.getUrlString(), payment.getRefId(), GsonUtils.getGson().toJson(mappedResp));
		LogUtils.store(
				Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(httpUtils.getUrlString())
						.key(payment.getRefId()).type("telr Refund Request").logType("AirSupplierAPILogs").build()));
		LogUtils.store(Arrays.asList(
				LogData.builder().generationTime(LocalDateTime.now()).logData(GsonUtils.getGson().toJson(mappedResp))
						.key(payment.getRefId()).type("telr Refund Response").logType("AirSupplierAPILogs").build()));
		return RefundResult.builder().isRefundSuccessful(isRefundSuccessful).merchantRefundId(refundId).build();
	}

	private Map<String, String> getMappedData(String decrptData) throws IOException {
		String propertiesFormat = decrptData.replaceAll("&", "\n");
		Properties properties = new Properties();
		properties.load(new StringReader(propertiesFormat));
		Map<String, String> data = new HashMap(properties);
		return data;
	}

	private String getPostData(Map<String, String> queryParams) {
		Set<String> keys = queryParams.keySet();
		StringBuilder query = new StringBuilder();
		for (String key : keys) {
			if (query.length() > 0)
				query.append("&");
			query.append(key).append("=").append(queryParams.get(key));
		}
		return query.toString();
	}

	@Override
	protected String getGatewayName() {
		return "Telr PG";
	}

}
