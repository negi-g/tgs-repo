package com.tgs.services.pms.jparepository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tgs.services.pms.dbmodel.DbDepositRequest;

@Repository
public interface DepositRequestRepository
		extends JpaRepository<DbDepositRequest, Long>, JpaSpecificationExecutor<DbDepositRequest> {

	@Query(value = "SELECT a.* FROM depositrequest a WHERE a.userid = :userId and a.createdon >= :timestamp", nativeQuery = true)
	public List<DbDepositRequest> getRequestsReceivedAfterTimeStamp(@Param("userId") String userId,
			@Param("timestamp") Timestamp timestamp);

	public DbDepositRequest findByReqId(String reqId);

	public List<DbDepositRequest> findByTransactionIdOrderByCreatedOn(String transactionId);
}
