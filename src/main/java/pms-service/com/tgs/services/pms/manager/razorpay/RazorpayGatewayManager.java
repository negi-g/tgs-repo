package com.tgs.services.pms.manager.razorpay;

import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.razorpay.Order;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.razorpay.Refund;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.CurrencyConverter;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RazorPayMerchantInfo;
import com.tgs.services.pms.datamodel.pg.RazorPayRefundInfo;
import com.tgs.services.pms.datamodel.pg.RazorpayPgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.datamodel.pg.razorpay.RazorpayTransactionalDetails;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.utils.encryption.EncryptionUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RazorpayGatewayManager extends GateWayManager {

	@Autowired
	private PaymentService paymentService;

	private PaymentProcessingResult verificationResult;

	private RazorpayPgResponse response;

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment,
			PaymentConfigurationRule rule) {
		RazorPayMerchantInfo razorpayGatewayInfo = (RazorPayMerchantInfo) gatewayInfo;
		String razorOrderId = createRazorPayOrderId(razorpayGatewayInfo, payment);
		razorpayGatewayInfo.setOrder_id(razorOrderId);
		razorpayGatewayInfo.setAmount(CurrencyConverter.toSubUnit(payment.getAmount()).toString());
		razorpayGatewayInfo.appendOrderIdToCallBack(payment.getRefId());
		razorpayGatewayInfo.setGatewayURL(null);
	}

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		RazorPayMerchantInfo merchantInfo = (RazorPayMerchantInfo) gatewayInfo;
		merchantInfo.setSecret_key(null);
		return super.convertToMap(merchantInfo);
	}

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		verificationResult = new PaymentProcessingResult();
		response = (RazorpayPgResponse) pgResponse;
		RazorPayMerchantInfo merchantInfo = (RazorPayMerchantInfo) gatewayMerchantInfo;
		if (verifyPaymentSignature(merchantInfo, payment)) {
			verificationResult.setStatus(PaymentStatus.SUCCESS);
		} else {
			verificationResult.setStatus(PaymentStatus.FAILURE);
		}
		verificationResult.getExternalPaymentInfo().setMerchantTxnId(getRazorpayOrderId(response));
		verificationResult.getExternalPaymentInfo().setGatewayComment(response.getErrorDesc());
		verificationResult.getExternalPaymentInfo().setGatewayComment(response.getErrorDesc());
		return verificationResult;
	}

	private String getRazorpayOrderId(RazorpayPgResponse razorpayPgResponse) {
		String razorpayOrderId = razorpayPgResponse.getRazorpay_order_id();
		if (razorpayOrderId == null) {
			Map<String, String> map = GsonUtils.getGson().fromJson(razorpayPgResponse.getErrorMetaData(),
					new TypeToken<Map<String, String>>() {}.getType());
			razorpayOrderId = map.get("order_id");
		}
		return razorpayOrderId;
	}

	public Boolean verifyPaymentSignature(RazorPayMerchantInfo merchantInfo, Payment payment) {
		if (response.getRazorpay_payment_id() != null) {
			persistPgData(payment);
			String calculatedHMAC;
			try {
				calculatedHMAC = EncryptionUtils.calculateRFC2104HMAC(
						response.getRazorpay_order_id() + "|" + response.getRazorpay_payment_id(),
						merchantInfo.getSecret_key());
				if (calculatedHMAC.equals(response.getRazorpay_signature())) {
					log.info("Razorpay:: Signature Verified");
					response.setSuccess(true);
					return true;
				} else {
					log.error(
							"RazorPay Signature mismatch for refId {}. Signature received is {}. Signature generated is {}",
							response.getRefId(), response.getRazorpay_signature(), calculatedHMAC);
					response.setSuccess(false);
					response.setErrorDesc("Signature not verified!");
					return false;

				}
			} catch (SignatureException e) {
				log.error("error occured while verifying signature for refId {}", response.getRefId());
				return false;
			}
		} else {
			return false;
		}
	}

	private void persistPgData(Payment payment) {
		payment.getAdditionalInfo().setExternalPayId(response.getRazorpay_payment_id());
		payment.getAdditionalInfo().setExternalSignature(response.getRazorpay_signature());
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		RazorPayMerchantInfo merchantInfo = (RazorPayMerchantInfo) gatewayMerchantInfo;
		try {
			RazorpayClient rp = getRazorPayClient(merchantInfo.getKey(), merchantInfo.getSecret_key());
			Order order = rp.Orders.fetch(payment.getMerchantTxnId());
			if (payment.getRefId().trim().compareTo(order.get("receipt")) != 0) {
				log.error("Mismatched refId {}. Stored RefId: {},", payment.getRefId(), order.get("receipt"));
				return null;
			}
			RazorpayTransactionalDetails transactionDetails =
					GsonUtils.getGson().fromJson(order.toString(), RazorpayTransactionalDetails.class);
			log.info("Verication response {} for Razor pay payment with refId {} is {}", order, payment.getRefId(),
					payment.toString());
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(order.toString())
					.key(payment.getRefId()).type("RazorPay Verification Response").logType("AirSupplierAPILogs")
					.build()));
			return prepareTrackingResult(transactionDetails, payment);
		} catch (RazorpayException e) {
		}
		return null;
	}

	private PaymentProcessingResult prepareTrackingResult(RazorpayTransactionalDetails transactionDetails,
			Payment payment) {
		PaymentProcessingResult trackingResult = new PaymentProcessingResult();
		if (response != null) {
			trackingResult.getExternalPaymentInfo().setGatewayComment(response.getErrorDesc());
		}
		trackingResult.getExternalPaymentInfo().setGatewayStatusCode(transactionDetails.getStatus());
		if (transactionDetails.getStatus().equalsIgnoreCase("paid")) {
			if (payment.getAmount().compareTo(CurrencyConverter.toUnit(transactionDetails.getAmount())) == 0) {
				trackingResult.setStatus(PaymentStatus.SUCCESS);
			} else {
				log.error("Mismatched Amount for refId {}. Amount stored: {}, amount received: {}", payment.getRefId(),
						payment.getAmount(), transactionDetails.getAmount());
				trackingResult.setStatus(PaymentStatus.FAILURE);
				trackingResult.getExternalPaymentInfo().setGatewayComment("Amount mismatch");
			}
		} else {
			trackingResult.setStatus(PaymentStatus.FAILURE);
		}
		return trackingResult;
	}

	@Override
	protected RefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		return new RazorPayRefundInfo();
	}

	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo) {
		Payment orgPayment = DbPayment.toDomainList(paymentService.findByRefId(payment.getRefId())).stream()
				.filter(p -> p.getType().equals(PaymentTransactionType.PAID_FOR_ORDER)
						&& p.getPaymentMedium().isExternalPaymentMedium()
						&& p.getAdditionalInfo().getGateway().equals(GateWayType.RAZOR_PAY.name()))
				.findFirst().get();
		RazorPayMerchantInfo razorPayInfo = (RazorPayMerchantInfo) gatewayInfo;
		Refund refund;
		try {
			RazorpayClient razorpayClient = getRazorPayClient(razorPayInfo.getKey(), razorPayInfo.getSecret_key());
			JSONObject refundRequest = new JSONObject();
			refundRequest.put("amount", CurrencyConverter.toSubUnit(payment.getAmount()).toString());
			refund = razorpayClient.Payments.refund(orgPayment.getAdditionalInfo().getExternalPayId(), refundRequest);
		} catch (RazorpayException ex) {
			log.error("Exception while refunding order for refId {}. Error: {}", payment.getRefId(), ex.getMessage());
			throw new CustomGeneralException(SystemError.RAZORPAY_REFUND);
		}
		log.info("RazorPay:: Response received from gateway for ref Id {} is {}", payment.getRefId(),
				refund.toString());
		LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(refund.toString())
				.key(payment.getRefId()).type("RazorPay Refund Response").logType("AirSupplierAPILogs").build()));
		return RefundResult.builder().isRefundSuccessful(true).merchantRefundId(refund.get("id")).build();
	}

	private String createRazorPayOrderId(RazorPayMerchantInfo razorPayInfo, PaymentRequest payment) {
		com.razorpay.Order razorOrder;
		try {
			RazorpayClient razorpayClient = getRazorPayClient(razorPayInfo.getKey(), razorPayInfo.getSecret_key());
			JSONObject orderRequest = new JSONObject();
			orderRequest.put("amount", CurrencyConverter.toSubUnit(payment.getAmount()));
			orderRequest.put("currency", razorPayInfo.getCurrency());
			orderRequest.put("receipt", payment.getRefId());
			orderRequest.put("payment_capture", true);
			razorOrder = razorpayClient.Orders.create(orderRequest);
		} catch (RazorpayException ex) {
			log.error("Exception while creating razorpay order for refId {}. Error: {}", payment.getRefId(),
					ex.getMessage());
			throw new CustomGeneralException(SystemError.RAZORPAY_ORDER);
		}
		// if (payment.getTransactionType().equals(PaymentTransactionType.TOPUP)) {
		// payment.setMerchantTxnId(razorOrder.get("id"));
		// }
		payment.setMerchantTxnId(razorOrder.get("id"));
		return razorOrder.get("id");
	}

	public RazorpayClient getRazorPayClient(String pk, String sk) throws RazorpayException {
		return new RazorpayClient(pk, sk);
	}

	@Override
	protected String getGatewayName() {
		return "RazorPay Gateway";
	}
}
