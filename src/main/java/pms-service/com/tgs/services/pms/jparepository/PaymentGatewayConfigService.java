package com.tgs.services.pms.jparepository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.filters.PaymentGatewayConfigFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.pms.dbmodel.DbPaymentGatewayConfigInfo;
import com.tgs.services.pms.helper.PaymentConfigurationHelper;

@Service
public class PaymentGatewayConfigService extends SearchService<DbPaymentGatewayConfigInfo>{

	@Autowired
	private PaymentGatewayConfigRepository configRepository;
	
	@Autowired
	private PaymentConfigurationHelper paymentHelper;

	public DbPaymentGatewayConfigInfo saveorUpdate(DbPaymentGatewayConfigInfo configInfo) {
		configInfo.setProcessedOn(LocalDateTime.now());
		DbPaymentGatewayConfigInfo gatewayConfig = configRepository.save(configInfo);
		paymentHelper.process();
		return gatewayConfig;
	}

	public DbPaymentGatewayConfigInfo fetchConfigInfoById(Integer id) {
		return configRepository.findOne(Long.valueOf(id));
	}

	public List<DbPaymentGatewayConfigInfo> findAllConfigsInfo() {
		Sort sort = new Sort(Direction.DESC, Arrays.asList("processedOn"));
		return configRepository.findAll(sort);
	}
	
	public List<DbPaymentGatewayConfigInfo> findAll(PaymentGatewayConfigFilter filter) {
		return super.search(filter, configRepository);
	}

}
