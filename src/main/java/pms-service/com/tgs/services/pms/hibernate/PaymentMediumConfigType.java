package com.tgs.services.pms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.pms.ruleengine.PaymentConfigOutput;

public class PaymentMediumConfigType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return PaymentConfigOutput.class;
    }
}
