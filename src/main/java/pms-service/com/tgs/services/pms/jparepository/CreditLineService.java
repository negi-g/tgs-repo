package com.tgs.services.pms.jparepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.OptimisticLockException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import com.tgs.filters.CreditLineFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.datamodel.CurrencyConverter;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.dbmodel.DbCreditLine;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.helper.CreditLineHelper;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CreditLineService extends SearchService<DbCreditLine> {

	@Autowired
	private CreditLineRepository creditLineRepository;

	@Autowired
	private CreditLineHelper cHelper;

	public Map<DbCreditLine, List<Payment>> getCreditLinesToBill() {
		LocalDateTime afterDate = LocalDateTime.now().minusMonths(2);
		List<Object[]> creditLinesWithPayments = creditLineRepository.findCreditsToBeBilled(afterDate);
		return getPaymentEntriesToBill(creditLinesWithPayments);
	}

	public Map<DbCreditLine, List<Payment>> getCreditLinesToBillForUser(String userId) {
		List<Object[]> creditLinesWithPayments = creditLineRepository.findCreditsToBeBilledOfUser(userId);
		return getPaymentEntriesToBill(creditLinesWithPayments);
	}

	public Map<DbCreditLine, List<Payment>> getPaymentEntriesToBill(List<Object[]> creditLinesWithPayments) {
		Map<DbCreditLine, List<Payment>> map = new HashMap<>();
		creditLinesWithPayments.forEach(creditLinesWpayments -> {
			DbCreditLine dbCreditLine = (DbCreditLine) creditLinesWpayments[0];
			DbPayment dbPayment = (DbPayment) creditLinesWpayments[1];
			if (map.containsKey(dbCreditLine)) {
				if (dbPayment != null)
					map.get(dbCreditLine).add(dbPayment.toDomain());
			} else {
				List<Payment> payments = new ArrayList<>();
				if (dbPayment != null)
					payments.add(dbPayment.toDomain());
				map.put(dbCreditLine, payments);
			}
		});
		return map;
	}

	public List<CreditLine> search(CreditLineFilter filter) {
		return BaseModel.toDomainList(super.search(filter, creditLineRepository));
	}

	@Override
	public List<DbCreditLine> search(QueryFilter filter) {
		return super.search(filter, creditLineRepository);
	}

	public CreditLine findByCreditNumber(String creditNumber) {
		DbCreditLine creditLine = creditLineRepository.findByCreditNumberOrId(creditNumber, 0);
		if (creditLine == null)
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
		return creditLine.toDomain();
	}

	public CreditLine findByCreditNumberOrId(String creditNumber, long id) {
		DbCreditLine creditLine;
		if (!StringUtils.isEmpty(creditNumber))
			creditLine = creditLineRepository.findByCreditNumberOrId(creditNumber, 0);
		else
			creditLine = creditLineRepository.findOne(id);
		if (creditLine == null)
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
		return creditLine.toDomain();
	}

	public List<CreditLine> findCreditLinesByUserIdAndProduct(String userId, String productCode, BigDecimal amount) {
		List<DbCreditLine> dbCreditLines = creditLineRepository.getCreditLinesByUserIdAndProduct(userId, productCode,
				CurrencyConverter.toSubUnit(amount));
		List<CreditLine> creditLineList = BaseModel.toDomainList(dbCreditLines);
		creditLineList.removeIf(c -> !c.getStatus().equals(CreditStatus.ACTIVE));
		return creditLineList;
	}

	public CreditLine save(CreditLine creditLine) {
		DbCreditLine dbCreditLine = DbCreditLine.create(creditLine);
		if (creditLine.getBillCycleStart() == null) {
			dbCreditLine.setBillCycleStart(creditLine.getBillCycleStart());
			dbCreditLine.setBillCycleEnd(creditLine.getBillCycleEnd());
		}
		try {
			creditLine = creditLineRepository.saveAndFlush(dbCreditLine).toDomain();
		} catch (ObjectOptimisticLockingFailureException | OptimisticLockException e) {
			log.info(
					"[Hibernate] Row has been updated by another transaction for creditLine {} and error message is {}",
					GsonUtils.getGson().toJson(creditLine), e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			creditLineRepository.flush();
			log.info("Exception occurred while saving  creditLine {} and error message is {}",
					GsonUtils.getGson().toJson(creditLine), e.getMessage(), e);
			throw e;
		}
		creditLine.setBalance(creditLine.getBalance());
		cHelper.updateCreditInCache(creditLine.getUserId());
		return creditLine;
	}

	public CreditLine transact(BigDecimal transactionAmount, CreditLine creditLine) {
		BigDecimal oldOustanding = creditLine.getOutstandingBalance();
		if (transactionAmount.compareTo(BigDecimal.ZERO) <= 0
				|| creditLine.getBalance().compareTo(transactionAmount) >= 0) {
			creditLine.setOutstandingBalance(creditLine.getOutstandingBalance().add(transactionAmount));
		} else {
			throw new PaymentException(SystemError.INSUFFICIENT_BALANCE);
		}
		BigDecimal newOutstanding = creditLine.getOutstandingBalance();
		String creditNumber = creditLine.getCreditNumber();
		try {
			creditLine = save(creditLine);
		} catch (Exception e) {
			log.info(
					"Concurrent update occured for creditNumber {}, transaction amount {}, old outstanding {} , new outstanding {}  hence retrying again ",
					creditNumber, transactionAmount, oldOustanding, newOutstanding);
			// Concurrent update, so retrying again
			creditLine = creditLineRepository.findByCreditNumberOrId(creditNumber, 0).toDomain();
			creditLine.setOutstandingBalance(creditLine.getOutstandingBalance().add(transactionAmount));
			try {
				creditLine = save(creditLine);
				log.info("After retrying again, updated creditline is {}", GsonUtils.getGson().toJson(creditLine));
			} catch (Exception e1) {
				log.info("CreditLine update failed after first retry for Credit number {}", creditNumber);
				throw e1;
			}
		}
		return creditLine;
	}

	public static String generateCreditNumber(String userId) {
		return new StringBuilder().append("CC").append(userId).append("-V")
				.append(RandomStringUtils.random(3, false, true)).toString();
	}

	public void delete(Long id) {
		creditLineRepository.delete(id);
	}
}
