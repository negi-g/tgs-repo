package com.tgs.services.pms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.dbmodel.DbPaymentGatewayConfigInfo;
import com.tgs.services.pms.jparepository.PaymentConfigurationService;
import com.tgs.services.pms.jparepository.PaymentGatewayConfigService;
import com.tgs.services.pms.ruleengine.PaymentBasicRuleField;
import com.tgs.services.pms.ruleengine.PaymentFact;

@Service
@InitializerGroup(group = InitializerGroup.Group.GENERAL)
public class PaymentConfigurationHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap paymentRules;
	private static CustomInMemoryHashMap paymentGatewayConfigs;
	
	Map<String, DbPaymentGatewayConfigInfo> gatewayInfoMap = new HashMap<>();

	private static final String FIELD = "payment_rules";
	private static final String FIELD1 = "gateway_info";

	@Autowired
	private PaymentConfigurationService mediumService;
	
	@Autowired
	private PaymentGatewayConfigService gatewayConfigService;

	public PaymentConfigurationHelper(CustomInMemoryHashMap gatewayCredentialsMap,
			CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap paymentMediums) {
		super(configurationHashMap);
		PaymentConfigurationHelper.paymentRules = paymentMediums;
		PaymentConfigurationHelper.paymentGatewayConfigs = gatewayCredentialsMap;
	}

	@Override
	public void process() {
		gatewayConfigService.findAllConfigsInfo().forEach(configs -> {
			gatewayInfoMap.put(configs.getName(), configs);
		});

		gatewayInfoMap.forEach((name, info) -> {
			paymentGatewayConfigs.put(name, FIELD1, info, CacheMetaInfo.builder().compress(true)
					.expiration(InMemoryInitializer.NEVER_EXPIRE).set(CacheSetName.PAYMENT_CONFIG.getName()).build());
		});
		
		List<PaymentConfigurationRule> mediumConfigurations =
				BaseModel.toDomainList(mediumService.findAllPaymentRules());

		mediumConfigurations.forEach(paymentRule -> {
			paymentRule.getInclusionCriteria()
					.setPaymentMediums(Optional.ofNullable(paymentRule.getInclusionCriteria().getPaymentMediums())
							.orElseGet(() -> new ArrayList<>()));
			paymentRule.getInclusionCriteria().getPaymentMediums().add(paymentRule.getMedium());
		});

		Map<PaymentRuleType, List<PaymentConfigurationRule>> ruleMap =
				mediumConfigurations.stream().collect(Collectors.groupingBy(PaymentConfigurationRule::getRuleType));
		ruleMap.forEach((key, value) -> paymentRules.put(key.name(), FIELD, value,
				CacheMetaInfo.builder().compress(true).expiration(InMemoryInitializer.NEVER_EXPIRE)
						.set(CacheSetName.PAYMENT_CONFIG.getName()).build()));
	}

	@Override
	public void deleteExistingInitializer() {
		paymentRules.truncate(CacheSetName.PAYMENT_CONFIG.getName());
	}

	@SuppressWarnings("unchecked")
	public static List<PaymentConfigurationRule> getPaymentRuleBasedOnRuleType(PaymentFact fact, PaymentRuleType type) {

		List<PaymentConfigurationRule> matchingRules = getRuleOnRuleType(type);

		Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(PaymentBasicRuleField.class);

		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(matchingRules, fact, fieldResolverMap);

		return (List<PaymentConfigurationRule>) ruleEngine.fireAllRules();
	}

	@SuppressWarnings("unchecked")
	public static DbPaymentGatewayConfigInfo getPaymentGatewayConfigs(String id) {
		DbPaymentGatewayConfigInfo infolist = new DbPaymentGatewayConfigInfo();
		return paymentGatewayConfigs.get(id, FIELD1, infolist.getClass(),
				CacheMetaInfo.builder().set(CacheSetName.PAYMENT_CONFIG.getName()).compress(true)
						.typeOfT(new TypeToken<List<DbPaymentGatewayConfigInfo>>() {}.getType()).build());
	}

	public static <T> T getPaymentRuleOutput(PaymentFact fact, PaymentRuleType type) {
		List<PaymentConfigurationRule> rules = getPaymentRuleBasedOnRuleType(fact, type);
		if (CollectionUtils.isNotEmpty(rules)) {
			return (T) rules.get(0).getOutput();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static List<PaymentConfigurationRule> getApplicableRules(PaymentFact fact, PaymentRuleType type,
			PaymentMedium medium) {

		List<PaymentConfigurationRule> matchingRules = getRuleOnRuleType(type);
		matchingRules = matchingRules.stream().filter(r -> r.getMedium().equals(medium)).collect(Collectors.toList());

		Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(PaymentBasicRuleField.class);

		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(matchingRules, fact, fieldResolverMap);

		return (List<PaymentConfigurationRule>) ruleEngine.fireAllRules();
	}

	/**
	 * Get matching rule with highest priority.
	 * 
	 * @param fact
	 * @param type
	 * @return
	 */
	public static PaymentConfigurationRule getPaymentRule(PaymentFact fact, PaymentRuleType type) {
		List<PaymentConfigurationRule> matchingRules = getPaymentRuleBasedOnRuleType(fact, type);
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return matchingRules.get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static List<PaymentConfigurationRule> getRuleOnRuleType(PaymentRuleType type) {
		List<PaymentConfigurationRule> ruleList = new ArrayList<>();
		return paymentRules.get(type.name(), FIELD, ruleList.getClass(),
				CacheMetaInfo.builder().set(CacheSetName.PAYMENT_CONFIG.getName()).compress(true)
						.typeOfT(new TypeToken<List<PaymentConfigurationRule>>() {}.getType()).build());
	}

	public static PaymentConfigurationRule getPaymentRule(int ruleId, PaymentRuleType type) {
		List<PaymentConfigurationRule> rules = getRuleOnRuleType(type);
		return rules.stream().filter(rule -> rule.getId() == ruleId).findFirst().orElse(null);

	}
}
