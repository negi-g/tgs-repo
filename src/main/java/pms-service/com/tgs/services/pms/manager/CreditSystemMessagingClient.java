package com.tgs.services.pms.manager;

import java.time.Duration;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.pms.datamodel.Credit;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.datamodel.CreditBillEmailAttributes;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.WalletEmailAttributes;
import com.tgs.services.ums.datamodel.User;

@Service
public class CreditSystemMessagingClient {

	@Autowired
	private MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	private UserServiceCommunicator userService;

	@Value("${envname}")
	private String envName;

	public void sendCreditBillMail(CreditBill bill, Credit credit, EmailTemplateKey emailTemplateKey,
			Duration extension) {
		if (extension == null)
			extension = Duration.ZERO;
		Duration finalExtension = extension;
		AbstractMessageSupplier<CreditBillEmailAttributes> attributeSupplier =
				new AbstractMessageSupplier<CreditBillEmailAttributes>() {
					@Override
					public CreditBillEmailAttributes get() {
						User user = userService.getUserFromCache(bill.getUserId());
						long hoursLeft = Duration.between(LocalDateTime.now(), bill.getPaymentDueDate()).toHours();
						long daysLeft = hoursLeft / 24;
						hoursLeft = hoursLeft % 24;
						DateFormatType dateFormat = DateFormatType.CREDIT_EMAIL_FORMAT;
						CreditBillEmailAttributes mailAttributes = CreditBillEmailAttributes.builder()
								.toEmailId(user.getEmail()).key(emailTemplateKey.name())
								.billAmount(
										TgsStringUtils.formatCurrency(bill.getBillAmount(), BaseUtils.getNationality()))
								.outStandingAmount(TgsStringUtils.formatCurrency(
										bill.getBillAmount().subtract(bill.getSettledAmount()),
										BaseUtils.getNationality()))
								.billNumber(bill.getBillNumber())
								.dueDate(DateFormatterHelper.formatDateTime(bill.getPaymentDueDate(), dateFormat))
								.cycleStartDate(credit == null ? null
										: DateFormatterHelper.formatDate(credit.periodStart(), dateFormat))
								.cycleEndDate(credit == null ? null
										: DateFormatterHelper.formatDate(credit.periodEnd(), dateFormat))
								.extendDays((short) finalExtension.toDays())
								.extendHours((short) (finalExtension.toHours() % 24))
								.lockDate(DateFormatterHelper.formatDateTime(bill.getLockDate(), dateFormat))
								.timeLeft(daysLeft + " Day(s) " + hoursLeft + "Hours").role(user.getRole()).build();
						return mailAttributes;
					}
				};
		msgSrvCommunicator.sendMail(attributeSupplier.getAttributes());
	}

	public void sendCreditLimitExtensionMail(double oldLimit, CreditLine nuw) {
		AbstractMessageSupplier<WalletEmailAttributes> supplier = new AbstractMessageSupplier<WalletEmailAttributes>() {
			@Override
			public WalletEmailAttributes get() {
				User user = userService.getUserFromCache(nuw.getUserId());
				return WalletEmailAttributes.builder().key(EmailTemplateKey.CREDIT_EXTENSION_EMAIL.name())
						.newExtensionLimit(
								TgsStringUtils.formatCurrency(nuw.getCreditLimit(), BaseUtils.getNationality()))
						.oldLimit(TgsStringUtils.formatCurrency(oldLimit, BaseUtils.getNationality()))
						.toEmailUserId(nuw.getUserId()).role(user.getRole()).build();
			}
		};
		msgSrvCommunicator.sendMail(supplier.getAttributes());
	}

	public void sendExemptedDocsMail(CreditLine creditLine) {
		AbstractMessageSupplier<EmailAttributes> attributeSupplier = new AbstractMessageSupplier<EmailAttributes>() {
			@Override
			public EmailAttributes get() {
				User user = userService.getUserFromCache(creditLine.getUserId());
				EmailAttributes msgAttr =
						EmailAttributes.builder().key(EmailTemplateKey.EXEMPTED_DOCUMENTS_EMAIL.name())
								.toEmailUserId(creditLine.getUserId()).role(user.getRole()).build();
				return msgAttr;
			}
		};
		msgSrvCommunicator.sendMail(attributeSupplier.getAttributes());
	}

	public void sendUserDeactivationEmail(String userid) {
		AbstractMessageSupplier<EmailAttributes> attributeSupplier = new AbstractMessageSupplier<EmailAttributes>() {
			@Override
			public EmailAttributes get() {
				User user = userService.getUserFromCache(userid);
				EmailAttributes msgAttr =
						EmailAttributes.builder().key(EmailTemplateKey.ACCOUNT_DEACTIVATION_EMAIL.name())
								.toEmailUserId(user.getUserId()).role(user.getRole()).build();
				return msgAttr;
			}
		};
		msgSrvCommunicator.sendMail(attributeSupplier.getAttributes());
	}

	public void sendCreditEmail(CreditLine credit, EmailTemplateKey key) {
		AbstractMessageSupplier<WalletEmailAttributes> msgAttributes =
				new AbstractMessageSupplier<WalletEmailAttributes>() {
					@Override
					public WalletEmailAttributes get() {
						DateFormatType dateFormat = DateFormatType.CREDIT_EMAIL_FORMAT;
						WalletEmailAttributes mailAttributes = WalletEmailAttributes.builder().build();
						mailAttributes.setUserId(credit.getUserId());
						mailAttributes.setToEmailUserId(credit.getUserId());
						if (credit != null) {
							mailAttributes.setCreditNumber(credit.getCreditNumber());
							mailAttributes.setOutStandingAmount(String.valueOf(credit.getOutstandingBalance()));

							/**
							 * In case of dynamic credit , billcycleend is not set
							 */
							if (credit.getBillCycleEnd() != null) {
								mailAttributes.setDueDate(
										DateFormatterHelper.formatDateTime(credit.getBillCycleEnd(), dateFormat));
							}
						}
						mailAttributes.setKey(key.name());
						return mailAttributes;
					}
				};

		msgSrvCommunicator.sendMail(msgAttributes.getAttributes());
	}

	public void sendNrCreditEmail(NRCredit credit, EmailTemplateKey key) {
		AbstractMessageSupplier<WalletEmailAttributes> msgAttributes =
				new AbstractMessageSupplier<WalletEmailAttributes>() {
					@Override
					public WalletEmailAttributes get() {
						DateFormatType dateFormat = DateFormatType.CREDIT_EMAIL_FORMAT;
						WalletEmailAttributes mailAttributes = WalletEmailAttributes.builder().build();
						mailAttributes.setUserId(credit.getUserId());
						mailAttributes.setToEmailUserId(credit.getUserId());
						if (credit != null) {
							mailAttributes.setCreditNumber(credit.getCreditId());
							mailAttributes.setOutStandingAmount(String.valueOf(credit.getCreditAmount()));
							mailAttributes.setDueDate(
									DateFormatterHelper.formatDateTime(credit.getCreditExpiry(), dateFormat));
						}
						mailAttributes.setKey(key.name());
						return mailAttributes;
					}
				};

		msgSrvCommunicator.sendMail(msgAttributes.getAttributes());
	}
}
