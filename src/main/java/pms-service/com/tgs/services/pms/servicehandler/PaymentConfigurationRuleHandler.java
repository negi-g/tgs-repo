package com.tgs.services.pms.servicehandler;

import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.PaymentConfigurationFilter;
import com.tgs.filters.PaymentGatewayConfigFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.dbmodel.DbPaymentConfigurationRule;
import com.tgs.services.pms.dbmodel.DbPaymentGatewayConfigInfo;
import com.tgs.services.pms.jparepository.PaymentConfigurationService;
import com.tgs.services.pms.jparepository.PaymentGatewayConfigService;
import com.tgs.services.pms.restmodel.PaymentConfigurationRuleResponse;
import com.tgs.services.pms.restmodel.PaymentGatewayConfigResponse;
import com.tgs.services.pms.ruleengine.PaymentGatewayConfigInfo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PaymentConfigurationRuleHandler
		extends ServiceHandler<PaymentConfigurationRule, PaymentConfigurationRuleResponse> {

	@Autowired
	private PaymentConfigurationService confService;

	@Autowired
	private PaymentGatewayConfigService configService;

	@Override
	public void beforeProcess() throws Exception {
		DbPaymentConfigurationRule confRule = null;
		try {
			if (request != null && Objects.nonNull(request.getId())) {
				confRule = confService.fetchRuleById(request.getId());
			}
			// confRule = new GsonMapper<>(request, confRule, DbPaymentConfigurationRule.class).convert();
			/**
			 * patch update will not work
			 */
			confRule = new DbPaymentConfigurationRule().from(request);
			confRule = confService.saveorUpdate(confRule);
			request.setId(confRule.getId().intValue());
		} catch (Exception e) {
			log.error("Unable to save rule ", e);
			throw new CustomGeneralException("Save Or Update Failed " + e);
		}
	}

	@Override
	public void process() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {
		response.getRules().add(request);
	}

	public PaymentGatewayConfigResponse saveGatewayConfigInfos(DbPaymentGatewayConfigInfo configInfo) {
		DbPaymentGatewayConfigInfo gatewayConfigDbModel = null;
		PaymentGatewayConfigResponse configResponse = new PaymentGatewayConfigResponse();
		try {
			if (configInfo != null && Objects.nonNull(configInfo.getId())) {
				gatewayConfigDbModel = configService.fetchConfigInfoById(configInfo.getId().intValue());
			}
			gatewayConfigDbModel =
					new GsonMapper<>(configInfo, gatewayConfigDbModel, DbPaymentGatewayConfigInfo.class).convert();
			gatewayConfigDbModel = configService.saveorUpdate(gatewayConfigDbModel);
			configResponse.getGatewayConfigs().add(gatewayConfigDbModel.toDomain());
		} catch (Exception e) {
			log.error("Unable to save gateway configs ", e);
			configResponse.addError(new ErrorDetail(SystemError.FAILED_TO_SAVE_GATEWAY_CONFIG));
		}
		return configResponse;
	}

	/**
	 * This will list the Medium which added in Configuration Based on Product
	 *
	 * @param product
	 * @param enabled
	 * @return Payment Medium Available for Product payment medium />
	 */
	public PaymentConfigurationRuleResponse getPaymentMediums(Product product, Boolean enabled) {
		PaymentConfigurationRuleResponse mediumResponse = new PaymentConfigurationRuleResponse();
		confService.findAllPaymentRules().forEach(medium -> {
			PaymentConfigurationRule mediumConfiguration =
					new GsonMapper<>(medium, PaymentConfigurationRule.class, true).convert();
			if (enabled != null && mediumConfiguration.getEnabled() == enabled)
				mediumResponse.getRules().add(mediumConfiguration);
		});
		return mediumResponse;
	}

	/**
	 * 
	 * @param id -> paymentConfigurator rule id
	 * @return
	 */
	public BaseResponse deletePaymentRule(String id) {
		BaseResponse baseResponse = new BaseResponse();
		if (StringUtils.isNotBlank(id)) {
			DbPaymentConfigurationRule dbPaymentRule = confService.fetchRuleById(NumberUtils.toInt(id));
			dbPaymentRule.setDeleted(true);
			dbPaymentRule.setEnabled(false);
			confService.saveorUpdate(dbPaymentRule);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse deletePaymentGatewayConfig(String id) {
		BaseResponse baseResponse = new BaseResponse();
		if (StringUtils.isNotBlank(id)) {
			DbPaymentGatewayConfigInfo dbPaymentconfig = configService.fetchConfigInfoById(NumberUtils.toInt(id));
			dbPaymentconfig.setDeleted(true);
			dbPaymentconfig.setEnabled(false);
			configService.saveorUpdate(dbPaymentconfig);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updatePaymentRuleStatus(String id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		if (StringUtils.isNotBlank(id)) {
			DbPaymentConfigurationRule dbPaymentRule = confService.fetchRuleById(NumberUtils.toInt(id));
			if (dbPaymentRule != null && BooleanUtils.isNotTrue(dbPaymentRule.isDeleted())) {
				dbPaymentRule.setEnabled(status);
				confService.saveorUpdate(dbPaymentRule);
			} else {
				baseResponse.addError(new ErrorDetail(SystemError.RESOURCE_NOT_FOUND));
			}
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public PaymentGatewayConfigResponse getGatewayConfigInfos(PaymentGatewayConfigFilter configFilter) {
		PaymentGatewayConfigResponse configResponse = new PaymentGatewayConfigResponse();
		getValidFilter(configFilter);
		configService.findAll(configFilter).forEach(config -> {
			configResponse.getGatewayConfigs().add(config.toDomain());
		});
		return configResponse;
	}

	private void getValidFilter(PaymentGatewayConfigFilter configFilter) {
		configFilter.setGatewayTypeIn(
				CollectionUtils.isNotEmpty(configFilter.getGatewayTypeIn()) ? configFilter.getGatewayTypeIn() : null);
		configFilter.setNameIn(CollectionUtils.isNotEmpty(configFilter.getNameIn()) ? configFilter.getNameIn() : null);
	}

	public PaymentConfigurationRuleResponse getPaymentConfigurationRule(PaymentConfigurationFilter configFilter) {
		PaymentConfigurationRuleResponse mediumResponse = new PaymentConfigurationRuleResponse();
		confService.findAll(configFilter).forEach(medium -> {
			PaymentConfigurationRule mediumConfiguration =
					new GsonMapper<>(medium, PaymentConfigurationRule.class, true).convert();
			mediumResponse.getRules().add(mediumConfiguration);
		});
		return mediumResponse;
	}

	public PaymentGatewayConfigResponse getPaymentGatewayConfig(Boolean enabled) {
		PaymentGatewayConfigResponse configResponse = new PaymentGatewayConfigResponse();
		configService.findAllConfigsInfo().forEach(config -> {
			PaymentGatewayConfigInfo configuration =
					new GsonMapper<>(config, PaymentGatewayConfigInfo.class, true).convert();
			if (enabled != null && configuration.getEnabled() == enabled)
				configResponse.getGatewayConfigs().add(configuration);
		});
		return configResponse;
	}

	public BaseResponse updatePaymentgatewayinfo(String id, boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		if (StringUtils.isNotBlank(id)) {
			DbPaymentGatewayConfigInfo dbPaymentconfig = configService.fetchConfigInfoById(NumberUtils.toInt(id));
			if (Objects.nonNull(dbPaymentconfig)) {
				dbPaymentconfig.setEnabled(status);
				configService.saveorUpdate(dbPaymentconfig);
			} else {
				baseResponse.addError(new ErrorDetail(SystemError.RESOURCE_NOT_FOUND));
			}
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}
}
