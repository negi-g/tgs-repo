package com.tgs.services.pms.jparepository;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.pms.dbmodel.PaymentLedger;

@Service
public class PaymentLedgerService {

	@Autowired
	PaymentDetailRepository paymentDetailRepo;

	public PaymentLedger save(PaymentLedger paymentLedger) {
		return paymentDetailRepo.saveAndFlush(paymentLedger);
	}

	public void save(EntityManager em, PaymentLedger paymentLedger) {
		em.persist(paymentLedger);
	}
}
