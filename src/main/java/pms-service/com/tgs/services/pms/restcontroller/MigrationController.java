package com.tgs.services.pms.restcontroller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.PaymentFilter;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.dbmodel.DbUserWallet;
import com.tgs.services.pms.helper.PaymentHelper;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.jparepository.UserWalletService;
import com.tgs.services.pms.manager.CreditLineBulkMigrateManager;
import com.tgs.services.pms.manager.paymentManagers.WalletPaymentManager;
import com.tgs.services.pms.mapper.PaymentRequestToPaymentMapper;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/pms/v1/migration/job/")
public class MigrationController {

	@Autowired
	CreditLineBulkMigrateManager creditLineManager;

	@Autowired
	private UserWalletService walletService;

	@Autowired
	WalletPaymentManager wPayManager;

	@Autowired
	private UserServiceCommunicator userComm;

	@Autowired
	PaymentService paymentService;

	@Autowired
	private PaymentRequestToPaymentMapper paymentMapper;

	@Autowired
	private PaymentHelper paymentHelper;

	@RequestMapping(value = "/balance/{userId}", method = RequestMethod.POST)
	protected BaseResponse save(HttpServletRequest request, HttpServletResponse response, @PathVariable String userId)
			throws Exception {
		if (userId != null && userId.equals("null")) {
			userId = null;
		}
		List<User> users = userComm
				.getUsers(UserFilter.builder().userId(userId).roles(java.util.Arrays.asList(UserRole.AGENT)).build());
		for (User user : users) {
			DbUserWallet wallet = walletService.findByUserId(user.getUserId());
			List<Payment> payments = paymentService.search(PaymentFilter.builder().paymentMedium(PaymentMedium.WALLET)
					.status(PaymentStatus.SUCCESS).payUserIdIn(java.util.Arrays.asList(user.getUserId())).build());
			log.error("Wallet Balance for userid {} is {} ", user.getUserId(), wallet.getBalance());
			BigDecimal balance = new BigDecimal((double) wallet.getBalance() / 100);
			log.error("Wallet Balance after conversion for userid {} is {}", user.getUserId(), balance);
			if (!CollectionUtils.isEmpty(payments)) {
				balance = payments.get(0).getAmount().add(payments.get(0).getCurrentBalance());
			}
			log.error("Final Balance for userid {} is {}", user.getUserId(), balance);
			String refId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.WALLET_TOPUP).build());
			PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder().build();
			additionalInfo.setTotalDepositAmount(balance);

			PaymentRequest paymentReq = PaymentRequest.builder().payUserId(user.getUserId()).refId(refId)
					.amount(balance.multiply(new BigDecimal(-1))).product(Product.WALLET_TOPUP)
					.transactionType(PaymentTransactionType.TOPUP).loggedInUserId("690325")
					.additionalInfo(additionalInfo).reason("Migration from old system").allowZeroAmount(true).build();


			paymentReq.setSubType(PaymentTransactionType.TOPUP);
			Payment payment = paymentMapper.toPayment(paymentReq);
			log.error("Payment Object is {}", payment.toString());
			payment.setCurrentBalance(balance);
			payment.setPaymentRefId(RandomStringUtils.random(10, false, true));
			DbPayment dbPayment = new DbPayment().from(payment);
			dbPayment.setStatus(PaymentStatus.SUCCESS.getCode());
			dbPayment.setCreatedOn(LocalDateTime.of(2020, 05, 18, 23, 59));
			dbPayment.setPaymentMedium(PaymentMedium.WALLET.getCode());
			paymentService.save(dbPayment);

		}


		return new BaseResponse();
	}

	@RequestMapping(value = "/closingbalance/{userId}", method = RequestMethod.POST)
	protected BaseResponse closingBalance(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String userId) throws Exception {
		if (userId != null && userId.equals("null")) {
			userId = null;
		}
		log.error("UserId is {}", userId);
		List<User> users =
				userComm.getUsers(UserFilter.builder().userId(userId).roles(Arrays.asList(UserRole.AGENT)).build());
		if (CollectionUtils.isEmpty(users)) {
			log.error("No of users found is zero");
		} else {
			paymentHelper.fixClosingBalance(users,
					PaymentFilter.builder().createdOnAfterDate(LocalDate.of(2020, 05, 01)).build());
		}

		return new BaseResponse();

	}
}
