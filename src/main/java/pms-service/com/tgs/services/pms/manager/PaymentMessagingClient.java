package com.tgs.services.pms.manager;

import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentReason;
import com.tgs.services.pms.datamodel.PaymentReason.PaymentReasonContainer;
import com.tgs.services.pms.datamodel.notification.PaymentMessageAttributes;
import com.tgs.utils.common.TgsEnumUtils;

@Service
final public class PaymentMessagingClient {

	@Autowired
	private MsgServiceCommunicator msgSrvCommunicator;

	public void sendPaymentEmail(Payment payment) {
		if (payment == null) {
			return;
		}
		AbstractMessageSupplier<PaymentMessageAttributes> messageSupplier =
				new AbstractMessageSupplier<PaymentMessageAttributes>() {
					@Override
					public PaymentMessageAttributes get() {
						String paidUsing, bank, processed, failureReason, paymentFee;
						paidUsing = bank = processed = failureReason = null;
						paymentFee = Objects.toString(payment.getPaymentFee(), null);
						if (StringUtils.isNotBlank(payment.getReason())) {
							PaymentReasonContainer reasonContainer =
									PaymentReasonContainer.getContainer(payment.getReason());
							paidUsing = reasonContainer.getValue(PaymentReason.PAID_USING);
							bank = reasonContainer.getValue(PaymentReason.BANK);
							processed = reasonContainer.getValue(PaymentReason.PROCESSED);
							failureReason = reasonContainer.getValue(PaymentReason.FAILURE_REASON);
						}
						return PaymentMessageAttributes.builder().key(EmailTemplateKey.PAYMENT_EMAIL.name())
								.userId(payment.getPayUserId()).merchantTxnId(payment.getMerchantTxnId())
								.status(TgsEnumUtils.toString(payment.getStatus())).referenceId(payment.getRefId())
								.amount(Objects.toString(payment.getAmount())).paymentFee(paymentFee).paidUsing(paidUsing)
								.processed(processed).failureReason(failureReason).bank(bank)
								.type(TgsEnumUtils.toString(payment.getType(), null)).build();
					}
				};
		msgSrvCommunicator.sendMail(messageSupplier);
	}
}
