package com.tgs.services.pms.manager.paymentManagers;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserWalletFilter;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.user.BasePaymentUtils;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.jparepository.NRCreditService;
import com.tgs.services.pms.jparepository.UserWalletService;
import com.tgs.services.pms.mapper.PaymentRequestToPaymentMapper;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NRCreditPaymentManager extends AbstractPaymentManager {

	@Autowired
	private NRCreditService service;

	@Autowired
	private PaymentRequestToPaymentMapper paymentMapper;

	private NRCredit credit;

	@Autowired
	private UserWalletService walletService;

	@Override
	public Payment debit(Payment payment) throws PaymentException {
		return processPayment(payment);
	}

	@Override
	public Payment credit(Payment payment) throws PaymentException {
		payment.setAmount(payment.getAmount().multiply(BigDecimal.valueOf(-1)));
		return processPayment(payment);
	}

	private Payment processPayment(Payment payment) {
		if (payment.getType().equals(PaymentTransactionType.CREDIT_ISSUED)
				|| payment.getType().equals(PaymentTransactionType.CREDIT_RECALLED)
				|| payment.getType().equals(PaymentTransactionType.CREDIT_EXPIRED)) {
			payment.setCurrentBalance(payment.getAdditionalInfo().getClosingBalance());
			return doPayment(payment).toDomain();
		}
		validateAndSetNRCredit(payment);
		boolean reduceLimit = reduceLimit(payment);
		credit = service.transact(payment.getAmount(), credit, reduceLimit);
		sanitizeNegativeOutstanding();
		BigDecimal balance = reduceLimit ? credit.getBalance().add(payment.getAmount().abs()) : credit.getBalance();
		payment.setCurrentBalance(balance);
		payment.getAdditionalInfo().setUtilizedBalance(credit.getUtilized());
		setTotalClosingBalance(payment);
		Payment out = doPayment(payment).toDomain();
		if (reduceLimit)
			creditLimitRecallPPB(payment.getAmount());
		return out;
	}

	protected void setTotalClosingBalance(Payment payment) {
		if (BasePaymentUtils.isStoreClosingBalance()) {
			List<UserWallet> userWallet = walletService
					.search(UserWalletFilter.builder().userIdIn(Arrays.asList(payment.getPayUserId())).build());
			payment.getAdditionalInfo()
					.setTotalClosingBalance(userWallet.get(0).getBalance().subtract(credit.getUtilized()));
		}
	}

	private void validateAndSetNRCredit(Payment payment) {
		if (StringUtils.isEmpty(payment.getWalletNumber()) && payment.getWalletId() == null) {
			List<NRCredit> usableCredits =
					service.getUsableCredits(payment.getPayUserId(), payment.getProduct(), payment.getAmount());
			if (usableCredits.size() == 0)
				throw new PaymentException(SystemError.NO_USABLE_CREDIT);
			credit = usableCredits.get(0);
		} else {
			validate(payment);
		}
		payment.setWalletNumber(credit.getCreditId());
		payment.setWalletId(credit.getId());
	}

	private void validate(Payment payment) {
		credit = service.getByIdOrNumber(payment.getWalletId(), payment.getWalletNumber());

		if (!payment.getPayUserId().equals(credit.getUserId())) {
			throw new PaymentException(SystemError.CREDITLINE_USERID_NOTMATCH);
		}
		if (payment.getOpType().equals(PaymentOpType.CREDIT))
			return;

		if ((!credit.getStatus().equals(CreditStatus.ACTIVE)
				&& !payment.getAdditionalInfo().isAllowForDisabledMediums())
				|| credit.getCreditExpiry().isBefore(LocalDateTime.now()))
			throw new PaymentException(SystemError.NRCREDIT_USERID_NOTMATCH);

		if (payment.getProduct() != null && payment.getProduct().hasOrder()
				&& credit.getProducts().stream().noneMatch(product -> product.equals(payment.getProduct())))
			throw new PaymentException(SystemError.NR_CREDIT_INVALID_PRODUCT);

		if (credit.getBalance().compareTo(payment.getAmount()) < 0) {
			throw new PaymentException(SystemError.INSUFFICIENT_BALANCE);
		}
	}

	private static boolean reduceLimit(Payment payment) {
		return payment.getType().equals(PaymentTransactionType.TOPUP) && PaymentTransactionType.UTILISATION_ADJUSTMENT
				.equals(payment.getAdditionalInfo().getActualTransactionType());
	}

	private void creditLimitRecallPPB(BigDecimal amount) {
		WalletPaymentRequest paymentRequest = WalletPaymentRequest.builder().amount(amount.abs())
				.walletNumber(credit.getCreditId()).walletId(credit.getId()).payUserId(credit.getUserId())
				.paymentMedium(PaymentMedium.CREDIT).partnerId(credit.getPartnerId()).opType(PaymentOpType.DEBIT)
				.transactionType(PaymentTransactionType.CREDIT_RECALLED).build();
		paymentRequest.getAdditionalInfo().setUtilizedBalance(credit.getUtilized());
		paymentRequest.getAdditionalInfo().setAllowForDisabledMediums(true);
		Payment payment = paymentMapper.toPayment(paymentRequest);
		payment.setCurrentBalance(credit.getBalance());
		doPayment(payment);
	}

	private void sanitizeNegativeOutstanding() {
		if (credit.getUtilized().compareTo(BigDecimal.ZERO) < 0) {
			if (credit.getUtilized().compareTo(BigDecimal.valueOf(-0.05)) >= 0) {
				log.error("SANITIZING negative utilisation for NRCredit: {}, utilisation = {}", credit.getCreditId(),
						credit.getUtilized());
				credit.setUtilized(BigDecimal.ZERO);
				service.save(credit);
			}
			log.error("Utilization went negative for NRCredit: {}, utilization = {}", credit.getCreditId(),
					credit.getUtilized());
		}
	}
}
