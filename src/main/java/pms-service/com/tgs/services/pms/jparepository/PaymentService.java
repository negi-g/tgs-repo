package com.tgs.services.pms.jparepository;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

import com.tgs.services.pms.helper.PaymentUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.utils.exception.PaymentException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PaymentService extends SearchService<DbPayment> {

	@Autowired
	private PaymentRepository paymentRepo;

	public DbPayment save(DbPayment payment) {
		validateDuplicateEntry(payment);
		payment.setProcessedOn(LocalDateTime.now());
		paymentRepo.saveAndFlush(payment);
		return payment;
	}

	private void validateDuplicateEntry(DbPayment newPayment) {
		if (StringUtils.isNotBlank(newPayment.getRefId()) && newPayment.getId() == null) {
			List<DbPayment> payments = super.search(PaymentFilter.builder().refId(newPayment.getRefId()).build(),
					paymentRepo);
			for (DbPayment existingPayment : payments) {
				if (existingPayment.getPaymentMedium().equals(newPayment.getPaymentMedium())
						&& existingPayment.getType().equals(newPayment.getType())
						&& existingPayment.getStatus().equals(newPayment.getStatus())
						&& existingPayment.getPayUserId().equals(newPayment.getPayUserId())
						&& sameBillIfExists(existingPayment, newPayment)
						&& sameAmendmentIfExists(existingPayment, newPayment)) {
					if (Math.abs(newPayment.getAmount() - existingPayment.getAmount()) <= 1) {
						log.error("Found duplicate payment entry for payment refId {} , payment {}", newPayment.getRefId(), newPayment);
						throw new PaymentException(SystemError.DUPLICATE_PAYMENT);
					}
				}
			}
		}
	}

	private boolean sameBillIfExists(DbPayment existing, DbPayment nuw) {
		return (!PaymentUtils.forBillSettlement(nuw.toDomain()) && !PaymentUtils.forBillSettlement(existing.toDomain()))
				|| StringUtils.equals(nuw.getAdditionalInfo().getBillNumber(), existing.getAdditionalInfo().getBillNumber());
	}

	private boolean sameAmendmentIfExists(DbPayment existing, DbPayment nuw) {
		return StringUtils.isBlank(nuw.getAmendmentId()) || nuw.getAmendmentId().equals(existing.getAmendmentId());
	}

	public Payment storeFailedPayment(Payment payment, SystemError error) {
		DbPayment dbPayment = new DbPayment().from(payment);
		dbPayment.setStatus(PaymentStatus.FAILURE.getPaymentStatus());
		dbPayment.setReason(error.getMessage() + ". " + dbPayment.getReason());
		if (StringUtils.isBlank(dbPayment.getPaymentRefId())) {
			dbPayment.setPaymentRefId(RandomStringUtils.random(10, false, true));
		}
		return paymentRepo.saveAndFlush(dbPayment).toDomain();
	}

	public List<DbPayment> findByRefId(String refId) {
		return paymentRepo.findByRefIdOrderByProcessedOnDesc(refId);
	}

	public List<DbPayment> findByIds(List<Long> ids) {
		return paymentRepo.findByIdIn(ids);
	}

	public List<Payment> search(PaymentFilter filter) {
		List<Payment> paymentList = BaseModel.toDomainList(super.search(filter, paymentRepo));
		paymentList.sort(Comparator.comparing(Payment::getCreatedOn));
		return paymentList;
	}

	@Transactional
	public void deleteFailedPaymentsByRefId(String refId) {
		paymentRepo.deleteFailedPaymentsByRefId(refId);
	}

}
