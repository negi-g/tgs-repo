package com.tgs.services.pms.jparepository;

import com.tgs.services.pms.dbmodel.DbCreditPolicy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CreditPolicyRepository extends JpaRepository<DbCreditPolicy, Long>, JpaSpecificationExecutor<DbCreditPolicy> {

    List<DbCreditPolicy> findByIdIn(List<Long> ids);

    List<DbCreditPolicy> findByPolicyIdIn(List<String> policyIds);

    DbCreditPolicy findByPolicyId(String policyId);

}
