package com.tgs.services.pms.validator;

import java.util.Objects;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;

@Component
public class MoneyExchangeValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(target instanceof MoneyExchangeInfo) {
			MoneyExchangeInfo moneyExchangeInfo = (MoneyExchangeInfo) target;
			if(Objects.isNull(moneyExchangeInfo.getFromCurrency()))
				errors.rejectValue("fromCurrency", SystemError.INVALID_FROM_CURRENCY.errorCode(),SystemError.INVALID_FROM_CURRENCY.getMessage());
			if(Objects.isNull(moneyExchangeInfo.getToCurrency()))
				errors.rejectValue("toCurrency", SystemError.INVALID_TO_CURRENCY.errorCode(),SystemError.INVALID_TO_CURRENCY.getMessage());
			if(moneyExchangeInfo.getFromCurrency().equals(moneyExchangeInfo.getToCurrency()))
				errors.rejectValue("toCurrency", SystemError.SAME_CURRENCY.errorCode(),SystemError.SAME_CURRENCY.getMessage());
			if(moneyExchangeInfo.getExchangeRate()==0)
				errors.rejectValue("exchangeRate", SystemError.INVALID_EXCHANGE_RATE.errorCode(),SystemError.INVALID_EXCHANGE_RATE.getMessage());

		}
	}

}
