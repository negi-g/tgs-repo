package com.tgs.services.pms.jparepository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.tgs.services.pms.dbmodel.DbCreditLine;

@Repository
public interface CreditLineRepository
		extends JpaRepository<DbCreditLine, Long>, JpaSpecificationExecutor<DbCreditLine> {

	DbCreditLine findByCreditNumberOrId(String creditNumber, long Id);

	List<DbCreditLine> findByUserId(String userId);

	List<DbCreditLine> findByIdIn(List<Long> ids);

	@Query(value = "SELECT * FROM creditLine WHERE userId = :userId AND :product = ANY (products) "
			+ " AND (outstandingBalance + :amount ) <= creditLimit + cast((additionalInfo->>'curTemporaryExt') as double precision) ORDER BY id ASC",
			nativeQuery = true)
	List<DbCreditLine> getCreditLinesByUserIdAndProduct(@Param("userId") String userId,
			@Param("product") String product, @Param("amount") long amount);

	// Interval operator is not recognized by HQL. hence passing after date as parameter
	@Query(value = "Select c,p from creditLine c LEFT JOIN Payment p on p.walletId = c.id and p.payUserId = c.userId and p.createdOn > c.billCycleStart and p.createdOn > :afterDate "
			+ "and p.createdOn < c.billCycleEnd and p.status = 'S' Where c.billCycleEnd < CURRENT_TIMESTAMP and c.billCycleStart > :afterDate and c.status != 'E' ")
	List<Object[]> findCreditsToBeBilled(@Param("afterDate") LocalDateTime afterDateTime);

	@Query(value = "Select c,p from creditLine c LEFT JOIN Payment p on p.walletId = c.id and p.payUserId = c.userId and p.createdOn > c.billCycleStart "
			+ "and p.createdOn < c.billCycleEnd and p.status = 'S' Where c.billCycleEnd < CURRENT_TIMESTAMP and c.status != 'E' and c.userId= :userId")
	List<Object[]> findCreditsToBeBilledOfUser(@Param("userId") String userId);
}
