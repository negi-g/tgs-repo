package com.tgs.services.pms.manager;

import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.ruleengine.PaymentGatewayConfigInfo;
import com.tgs.utils.exception.PaymentException;

public interface IGateWayManager {

	public PaymentRequest initializeGatewayData(PaymentRequest payment, PaymentGatewayConfigInfo mediumConfig,
			PaymentConfigurationRule rule);

	public Payment refund(Payment payment, PaymentGatewayConfigInfo mediumConfig) throws PaymentException;

}
