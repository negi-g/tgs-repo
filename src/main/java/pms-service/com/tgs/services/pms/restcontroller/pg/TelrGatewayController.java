package com.tgs.services.pms.restcontroller.pg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import com.google.gson.Gson;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.telr.TelrPgResponse;
import com.tgs.services.pms.manager.PaymentProcessor;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class TelrGatewayController extends AbstractExternalPaymentController {

    @Autowired
    private PaymentProcessor paymentProcessor;

    @SuppressWarnings({"unchecked", "serial"})
    @RequestMapping("/pg/v1/response_callback/telr/{refId}")
    public void responseCallBack(Model model, HttpServletRequest request, HttpServletResponse response
            , @PathVariable("refId") String refId) throws IOException {
        String reqParams = GsonUtils.getGson().toJson(HttpUtils.getRequestParams(request));
        List<Payment> outPayments = new ArrayList<>();
        log.info("Telr:: Request Params {}, refId {}", reqParams, refId);
        TelrPgResponse pgResponse = TelrPgResponse.builder().build();
        log.info("Telr:: Call back received. Response=> {}", new Gson().toJson(pgResponse));
        pgResponse.setRefId(refId);
        pgResponse.setGateWayType(GateWayType.TELR);
        try {
            SystemContextHolder.getContextData().setMetaInfo(pgResponse);
            List<PaymentRequest> result = getPaymentEntries(pgResponse.getRefId(), GsonUtils.getGson().toJson(pgResponse));
            setLoggedInUserIfAbsent(result.get(0).getPayUserId());
            outPayments.addAll(paymentProcessor.process(result));
        } catch (Exception e) {
            log.error("Exception while processing Telr PGResponse TGS_RefId: {}",
                    pgResponse.getRefId(), e);
        }
        processPayments(outPayments, pgResponse.getRefId(), DepositRequest.builder().bank(pgResponse.getGateWayType().name()).build());
        redirect(pgResponse.getRefId(), response);
    }
}
