package com.tgs.services.pms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.pms.ruleengine.PaymentBasicRuleExclusionCriteria;

public class PaymentBasicRuleExclusionCriteriaType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return PaymentBasicRuleExclusionCriteria.class;
    }
}
