package com.tgs.services.pms.mapper;

import org.springframework.stereotype.Service;

import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentRequest;

@Service
public class PaymentToPaymentRequestMapper {

	public PaymentRequest toPayment(Payment payment) {
		PaymentRequest paymentRequest = PaymentRequest.builder()
				.payUserId(TgsObjectUtils.firstNonNull(() -> payment.getPayUserId(), () -> SystemContextHolder.getContextData().getUser().getLoggedInUserId()))
				.paymentMedium(payment.getPaymentMedium())
				.amount(payment.getAmount().subtract(payment.getPaymentFee()))
				.merchantTxnId(payment.getMerchantTxnId())
				.transactionType(payment.getType())
				.refId(payment.getRefId())
				.tds(payment.getTds())
				.build();
		return paymentRequest;
	}
}
