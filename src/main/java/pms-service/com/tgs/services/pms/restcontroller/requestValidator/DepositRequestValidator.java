package com.tgs.services.pms.restcontroller.requestValidator;

import com.tgs.services.base.TgsValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.pms.datamodel.DepositRequest;

@Component
public class DepositRequestValidator extends TgsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return DepositRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		DepositRequest depositRequest = (DepositRequest) target;
		registerErrors(errors, "", depositRequest);
	}

}
