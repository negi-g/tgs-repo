package com.tgs.services.pms.jparepository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.filters.PaymentConfigurationFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.pms.dbmodel.DbPaymentConfigurationRule;
import com.tgs.services.pms.helper.PaymentConfigurationHelper;

@Service
public class PaymentConfigurationService extends SearchService<DbPaymentConfigurationRule>{

	@Autowired
	private PaymentConfigurationRepository confRepository;

	@Autowired
	private PaymentConfigurationHelper paymentHelper;

	public DbPaymentConfigurationRule saveorUpdate(DbPaymentConfigurationRule medium) {
		medium.setProcessedOn(LocalDateTime.now());
		DbPaymentConfigurationRule pc = confRepository.save(medium);
		paymentHelper.process();	
		return pc;
	}

	public DbPaymentConfigurationRule fetchRuleById(Integer id) {
		return confRepository.findOne(Long.valueOf(id));
	}

	public List<DbPaymentConfigurationRule> findAllPaymentRules() {
		Sort sort = new Sort(Direction.DESC, Arrays.asList("processedOn"));
		return confRepository.findAll(sort);
	}

	public List<DbPaymentConfigurationRule> findEnabledPaymentMediums() {
		return confRepository.findByEnabledOrderByProcessedOnDesc(Boolean.TRUE);
	}
	
	public List<DbPaymentConfigurationRule> findAll(PaymentConfigurationFilter filter) {
		return super.search(filter, confRepository);
	}
}
