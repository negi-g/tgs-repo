package com.tgs.services.pms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.tgs.services.pms.dbmodel.DbPaymentGatewayConfigInfo;

@Repository
public interface PaymentGatewayConfigRepository extends JpaRepository<DbPaymentGatewayConfigInfo, Long>, JpaSpecificationExecutor<DbPaymentGatewayConfigInfo> {

}