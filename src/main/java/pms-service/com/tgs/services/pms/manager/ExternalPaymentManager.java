package com.tgs.services.pms.manager;

import java.time.LocalDateTime;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.tgs.filters.ExternalPaymentAdditionalInfoFilter;
import com.tgs.filters.ExternalPaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.pms.datamodel.ExternalPaymentStatusInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentProcessingResult.ExternalPaymentInfo;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.dbmodel.DbExternalPayment;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.jparepository.ExternalPaymentService;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ExternalPaymentManager {

	private static final int DEFAULT_HOURS = 5;

	@Autowired
	private ExternalPaymentService externalPaymentService;

	public void trackExternalPayments(ExternalPaymentFilter filter) {
		sanitizeFilter(filter);
		List<DbExternalPayment> dbExternalPayments = externalPaymentService.search(filter);
		if (CollectionUtils.isNotEmpty(dbExternalPayments)) {
			log.info("Checking payment status for {} payments", dbExternalPayments.size());
			dbExternalPayments.forEach(externalPayment -> {
				try {
					trackExternalPayment(externalPayment);
				} catch (Exception e) {
					log.error("Failed to track payment for refId {} due to ", externalPayment.getRefId(), e);
				}
			});
		}
	}

	private void sanitizeFilter(ExternalPaymentFilter filter) {
		if (filter.getCreatedOnAfterDate() == null && filter.getCreatedOnAfterDateTime() == null) {
			filter.setCreatedOnAfterDateTime(LocalDateTime.now().minusHours(DEFAULT_HOURS));
		}
		filter.setStatusIn(TgsCollectionUtils.getNonNullElements(filter.getStatusIn()));
		ExternalPaymentAdditionalInfoFilter additionalInfoFilter = filter.getAdditionalInfo();
		if (additionalInfoFilter == null) {
			additionalInfoFilter = new ExternalPaymentAdditionalInfoFilter();
			filter.setAdditionalInfo(additionalInfoFilter);
		}
		if (additionalInfoFilter.getGatewayStatus() == null && additionalInfoFilter.getGatewayStatusIn() == null) {
			additionalInfoFilter.setGatewayStatus(PaymentStatus.PENDING);
		}
	}

	public ExternalPaymentStatusInfo trackExternalPayment(String refId) {
		List<DbExternalPayment> dbExternalPayments =
				externalPaymentService.search(ExternalPaymentFilter.builder().refId(refId).build());
		if (CollectionUtils.isEmpty(dbExternalPayments)) {
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
		}
		return trackExternalPayment(dbExternalPayments.get(0));
	}

	private ExternalPaymentStatusInfo trackExternalPayment(DbExternalPayment externalPayment) {
		try {
			log.debug("Checking payment status for refId {}", externalPayment.getRefId());
			ExternalPaymentStatusInfo paymentStatusInfo = new ExternalPaymentStatusInfo();
			paymentStatusInfo.setRefId(externalPayment.getRefId());
			GateWayType gateWayType = GateWayType.valueOf(externalPayment.getAdditionalInfo().getGatewayType());
			GateWayManager gateWayManager = PaymentUtils.getGatewayManager(gateWayType);
			Payment payment = PaymentUtils.toPayment(externalPayment);
			PaymentProcessingResult trackingResult = gateWayManager.trackPayment(payment);
			paymentStatusInfo.setStatus(trackingResult.getStatus());
			paymentStatusInfo.setComment(getExternalPaymentStatusComment(trackingResult.getExternalPaymentInfo()));
			externalPaymentService.savePaymentStatus(paymentStatusInfo);
			return paymentStatusInfo;
		} catch (Exception e) {
			log.error("Failed to check payment status for refId {} due to ", externalPayment.getRefId(), e);
			if (!(e instanceof CustomGeneralException)) {
				throw new CustomGeneralException(SystemError.PAYMENT_TRACKING_FAILED);
			}
			throw e;
		}
	}

	private String getExternalPaymentStatusComment(ExternalPaymentInfo externalPaymentInfo) {
		String trackBy = "Tracked by ".concat(getLoggedInUserDesc());
		String gatewayStausCode = externalPaymentInfo.getGatewayStatusCode();
		String gatewayComment = externalPaymentInfo.getGatewayComment();
		return Joiner.on("; ").useForNull("null").join(gatewayStausCode, gatewayComment, trackBy).toString();
	}

	private String getLoggedInUserDesc() {
		User loggedInUser = SystemContextHolder.getContextData().getUser();
		if (loggedInUser == null)
			return "null";
		return new StringBuilder(loggedInUser.getName()).append("(").append(loggedInUser.getUserId()).append(")")
				.toString();
	}

}
