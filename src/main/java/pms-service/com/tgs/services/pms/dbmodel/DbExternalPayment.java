package com.tgs.services.pms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.pms.datamodel.ExternalPayment;
import com.tgs.services.pms.datamodel.ExternalPaymentAdditionalInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "externalpayment")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DbExternalPayment extends BaseModel<DbExternalPayment, ExternalPayment> {

	@CreationTimestamp
	@Column
	private LocalDateTime createdOn;

	@Column
	private String refId;

	@Column
	private String status;

	@Column
	private String payUserId;

	@Type(type = "com.tgs.services.pms.hibernate.ExternalPaymentAdditionalInfoType")
	@Column
	private ExternalPaymentAdditionalInfo additionalInfo;

	public ExternalPaymentAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) {
			additionalInfo = ExternalPaymentAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}

	@Override
	public ExternalPayment toDomain() {
		return new GsonMapper<>(this, ExternalPayment.class).convert();
	}

	@Override
	public DbExternalPayment from(ExternalPayment dataModel) {
		return new GsonMapper<>(dataModel, this, DbExternalPayment.class).convert();
	}
}
