package com.tgs.services.pms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.pms.dbmodel.DbUserWallet;

@Repository
public interface UserWalletRepository extends JpaRepository<DbUserWallet, Long>, JpaSpecificationExecutor<DbUserWallet> {

	public DbUserWallet findByUserId(String userId);

}