package com.tgs.services.pms.manager.me;

import java.io.IOException;
import java.util.List;

import com.tgs.services.pms.datamodel.MoneyExchangeInfo;

public interface IMoneyExchangeManager {

	public List<MoneyExchangeInfo> getExchangeData(List<MoneyExchangeInfo> info) throws IOException;
	
}
