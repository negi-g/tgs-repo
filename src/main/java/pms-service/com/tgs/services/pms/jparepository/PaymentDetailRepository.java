package com.tgs.services.pms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.pms.dbmodel.PaymentLedger;

@Repository
public interface PaymentDetailRepository
		extends JpaRepository<PaymentLedger, Long>, JpaSpecificationExecutor<PaymentLedger> {

}
