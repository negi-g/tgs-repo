package com.tgs.services.pms.manager;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.messagingService.datamodel.DepositRequestAttribute;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.DepositRequestStatus;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.dbmodel.DbDepositRequest;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.jparepository.DepositRequestService;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DepositManager {

	@Autowired
	private DepositRequestService depositService;

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	protected MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	private UserServiceCommunicator userComm;

	public DepositRequest submit(DepositRequest depositRequest) {
		DbDepositRequest dbRequest = null;
		try {
			if (depositRequest.getReqId() != null) {
				dbRequest = depositService.fetchByReqId(depositRequest.getReqId());
				if (dbRequest != null) {
					dbRequest.setUpdateUserId(UserUtils.getUserId(SystemContextHolder.getContextData().getUser()));
				}
			}

			checkDuplicateRequest(new DbDepositRequest().from(depositRequest));
			dbRequest = Optional.ofNullable(dbRequest).orElse(new DbDepositRequest()).from(depositRequest);
			// Validate MDR Limit after DR has been raised.
			if (dbRequest.getId() != null)
				validateMDRLimit(dbRequest.toDomain());
			checkIfPaymentReceived(dbRequest);

			dbRequest = depositService.save(dbRequest);
			log.info("Succesfully saved Deposit request for userid {} for requested amount {}, status is {}",
					dbRequest.getUserId(), dbRequest.getRequestedAmount(), dbRequest.getStatus());
			DepositRequestStatus status = DepositRequestStatus.getEnumFromCode(dbRequest.getStatus());
			if (DepositRequestStatus.SUBMITTED.equals(status))
				sendMail(EmailTemplateKey.RAISE_DEPOSIT_REQUEST_EMAIL, depositRequest);
			depositRequest = dbRequest.toDomain();
		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			log.error("Error while adding/updating deposit request {} because of {}", depositRequest.getReqId(),
					e.getMessage(), e);
			if (Objects.nonNull(e.getCause()) && e.getCause().getCause() instanceof PSQLException) {
				throw new CustomGeneralException(SystemError.DUPLICATE_REQUEST_ID);
			}
			throw e;

		}
		return depositRequest;
	}

	public void sendMail(EmailTemplateKey templateKey, DepositRequest depositRequest) {
		AbstractMessageSupplier<DepositRequestAttribute> msgAttributes =
				new AbstractMessageSupplier<DepositRequestAttribute>() {
					@Override
					public DepositRequestAttribute get() {
						User depositorUser = userComm
								.getUserFromCache(UserUtils.getUserId(SystemContextHolder.getContextData().getUser()));
						DepositRequestAttribute mailAttributes = DepositRequestAttribute.builder().build();
						mailAttributes.setGenerationTime(LocalDateTime.now().toString());
						mailAttributes.setDepositAmount(depositRequest.getRequestedAmount().toString());
						mailAttributes.setAgentId(depositorUser.getUserId());
						mailAttributes.setAgentName(depositorUser.getName());
						mailAttributes.setDepositType(depositRequest.getType().name().replaceAll("_", " "));
						mailAttributes.setTransactionId(depositRequest.getTransactionId());
						mailAttributes.setKey(templateKey.name());
						mailAttributes.setRole(depositorUser.getRole());
						return mailAttributes;
					}
				};

		msgSrvCommunicator.sendMail(msgAttributes.getAttributes());

	}

	public boolean acceptDepositRequest(String reqId, BigDecimal processedAmount) {
		DbDepositRequest depositRequest = depositService.fetchByReqId(reqId);
		if (Objects.nonNull(depositRequest)) {
			if (DepositRequestStatus.ACCEPTED.getCode().equals(depositRequest.getStatus())) {
				throw new CustomGeneralException(SystemError.DUPLICATE_REQUEST_ID);
			}
			depositRequest.getAdditionalInfo().setProcessedAmount(processedAmount.abs().doubleValue());
			DepositRequestStatus status = DepositRequestStatus.getEnumFromCode(depositRequest.getStatus());
			if (status.nextStatusSet().contains(DepositRequestStatus.ACCEPTED)) {
				depositRequest.setStatus(DepositRequestStatus.ACCEPTED.getCode());
			} else {
				throw new CustomGeneralException(SystemError.DEPOSITREQUEST_INVALID_ACTION);
			}
			depositService.save(depositRequest);
			return true;
		}
		return false;
	}

	private void checkIfPaymentReceived(DbDepositRequest dbRequest) {
		if (StringUtils.isNotBlank(dbRequest.getTransactionId())) {
			DepositRequestStatus status = DepositRequestStatus.getEnumFromCode(dbRequest.getStatus());
			if (DepositRequestStatus.SUBMITTED.equals(status)
					|| DepositRequestStatus.PAYMENT_NOT_RECEIVED.equals(status)) {
				CacheMetaInfo cacheMetaInfo = CacheMetaInfo.builder().set(CacheSetName.DEPOSIT_REQUEST.name())
						.key(dbRequest.getTransactionId()).build();
				List<DepositRequest> depositRequests = cachingCommunicator
						.getList(cacheMetaInfo, DepositRequest.class, false, false).get(BinName.SUCCESSDEPOSIT.name());
				if (CollectionUtils.isNotEmpty(depositRequests)) {
					dbRequest.setStatus(DepositRequestStatus.PAYMENT_RECEIVED.getCode());
				}
				cachingCommunicator.delete(cacheMetaInfo);
			}
		}
	}

	private void validateMDRLimit(DepositRequest depositRequest) {
		String assignedUserId = depositRequest.getAdditionalInfo().getAssignedUserId();
		String assignee = SystemContextHolder.getContextData().getUser().getUserId();
		// Check if logged in user can perform operations or not
		if (StringUtils.isEmpty(assignedUserId) || assignedUserId.equals(assignee)) {
			User user = SystemContextHolder.getContextData().getUser();
			PaymentUtils.validateUserMDRLimit(depositRequest, user);
		} else {
			// logged in user (assignee) is trying to assign DR to assignedUserId
			// First check if logged in user can perform operations or not
			User user = SystemContextHolder.getContextData().getUser();
			PaymentUtils.validateUserMDRLimit(depositRequest, user);
			// Now check if new assigned user can perform operations or not
			user = userComm.getUserFromCache(assignedUserId);
			PaymentUtils.validateUserMDRLimit(depositRequest, user);
		}
	}

	private void checkDuplicateRequest(DbDepositRequest dbRequest) {
		/**
		 * E.g., when deposit request is being updated.
		 */
		if (StringUtils.isBlank(dbRequest.getUserId())) {
			return;
		}

		if (!StringUtils.isBlank(dbRequest.getTransactionId())) {
			for (DbDepositRequest dbRequestEntry : depositService
					.fetchByTransactionIdOrderByCreatedOn(dbRequest.getTransactionId())) {
				DepositRequestStatus status = DepositRequestStatus.getEnumFromCode(dbRequestEntry.getStatus());
				if (!PaymentUtils.isDepositRequestRejected(status)) {
					log.info(
							"Save Deposit request failed for userid {} for requested amount {}, status is {} because this is a duplicate request. Existing deposit request has id {} and status {}",
							dbRequest.getUserId(), dbRequest.getRequestedAmount(), dbRequest.getStatus(),
							dbRequestEntry.getId(), dbRequestEntry.getStatus());
					throw new CustomGeneralException(SystemError.DUPLICATE_REQUEST);
				}
			}
		}

		for (DbDepositRequest dbRequestEntry : depositService.getRequestsReceivedInLastMinutes(dbRequest.getUserId(),
				5)) {
			DepositRequestStatus status = DepositRequestStatus.getEnumFromCode(dbRequestEntry.getStatus());
			if (!PaymentUtils.isDepositRequestRejected(status) && dbRequestEntry.equals(dbRequest)) {
				log.info(
						"Save Deposit request failed for userid {} for requested amount {}, status is {} because this is a duplicate request within last 5 minutes",
						dbRequest.getUserId(), dbRequest.getRequestedAmount(), dbRequest.getStatus());
				throw new CustomGeneralException(SystemError.DUPLICATE_REQUEST);
			}
		}
	}

	public void checkAcceptedDepositRequest(String reqId) {
		DepositRequest depositRequest = depositService.fetchById(reqId);
		PaymentFilter filter = PaymentFilter.builder().refId(reqId).status(PaymentStatus.SUCCESS).build();
		List<Payment> paymentList = paymentService.search(filter);
		if (CollectionUtils.isNotEmpty(paymentList)) {
			BigDecimal processedAmount = Payment.sum(paymentList).abs();
			if (depositRequest.getAdditionalInfo().getProcessedAmount() == null) {
				depositRequest.getAdditionalInfo().setProcessedAmount(processedAmount.doubleValue());
			}
			depositRequest.setStatus(DepositRequestStatus.ACCEPTED);
			depositService.save(depositRequest);
		}
	}

}
