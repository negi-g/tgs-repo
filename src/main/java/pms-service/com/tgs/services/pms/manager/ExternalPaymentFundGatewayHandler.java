package com.tgs.services.pms.manager;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.DepositAdditionalInfo;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.DepositRequestStatus;
import com.tgs.services.pms.datamodel.DepositType;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentReason;
import com.tgs.services.pms.datamodel.PaymentReason.PaymentReasonContainer;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ExternalPaymentFundGatewayHandler {

	@Autowired
	private DepositManager dpManager;

	@Autowired
	private PaymentProcessor paymentProcessor;

	@Autowired
	protected UserServiceCommunicator userService;

	public void processPayment(List<Payment> payments, DepositRequest oldDepositRequest) {
		Payment payment = payments.get(0);
		User payUser = userService.getUserFromCache(payment.getPayUserId());
		if (!payUser.getRole().isDepositWalletAllowed()) {
			log.error("Depsoit wallet not allowed for this user {} , hence returning ", payment.getPayUserId());
			throw new CustomGeneralException(SystemError.DEPOSIT_NOT_ALLOWED);
		}
		if (!PaymentStatus.SUCCESS.equals(payment.getStatus())) {
			return;
		}
		PaymentReasonContainer reasonContainer = PaymentReasonContainer.getContainer(payment.getReason());
		DepositRequest depReq = DepositRequest.builder().reqId(payment.getRefId()).userId(payment.getPayUserId())
				.type(DepositType.ONLINE_TOPUP).requestedAmount(payment.getAmount().doubleValue())
				.paymentFee(payment.getPaymentFee().doubleValue()).transactionId(payment.getMerchantTxnId())
				.partnerId(payment.getPartnerId())
				.additionalInfo(DepositAdditionalInfo.builder()
						.processedAmount(payment.getAmount().doubleValue() - payment.getPaymentFee().doubleValue())
						.build())
				.status(DepositRequestStatus.SUBMITTED).build();

		/**
		 * Overriding some of the existing detail into new DR Object.
		 */
		if (oldDepositRequest != null) {
			PaymentReasonContainer container = new PaymentReasonContainer(reasonContainer);
			container.removeReason(PaymentReason.PAYMENT_FEE);
			depReq.setComments(container.getCombinedReasons());
			depReq.setBank(oldDepositRequest.getBank());
		}

		depReq = dpManager.submit(depReq);
		payment.setRefId(depReq.getReqId());

		// topup payment request

		PaymentAdditionalInfo additionalInfo =
				PaymentAdditionalInfo.builder().externalPayId(payment.getAdditionalInfo().getExternalPayId())
						.externalSignature(payment.getAdditionalInfo().getExternalSignature()).build();
		PaymentRequest topupRequest = PaymentRequest.builder()
				.amount(payment.getAmount().subtract(payment.getPaymentFee())).refId(depReq.getReqId())
				.opType(PaymentOpType.CREDIT).product(Product.WALLET_TOPUP).reason(reasonContainer.getCombinedReasons())
				.transactionType(PaymentTransactionType.TOPUP).payUserId(payment.getPayUserId())
				.merchantTxnId(payment.getMerchantTxnId()).additionalInfo(additionalInfo).build();
		log.info("Topup PR: " + new Gson().toJson(topupRequest));
		paymentProcessor.process(Arrays.asList(topupRequest));
	}
}
