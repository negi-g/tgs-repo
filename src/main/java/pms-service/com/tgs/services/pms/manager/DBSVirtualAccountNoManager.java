package com.tgs.services.pms.manager;

import org.apache.commons.lang3.StringUtils;

import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.pms.datamodel.pg.dbs.DBSVirtualAccountNoData;

public class DBSVirtualAccountNoManager {
	
	private static final int PREFIX_LEN = 8;

	public static String generateVirtualAccountNo(DBSVirtualAccountNoData data) {
		if (data == null) {
			return null;
		}

		/**
		 * Append random numbers to the prefix in {@code data}.
		 */
		String prefix = TgsStringUtils.generateRandomNumber(PREFIX_LEN, StringUtils.defaultString(data.getPrefix()));
		
		return new StringBuilder(prefix)
				.append(data.getUserId())
				.toString();
	}

	public static DBSVirtualAccountNoData getData(String virtualAccountNo) {
		String userId = virtualAccountNo.substring(PREFIX_LEN);
		return DBSVirtualAccountNoData.builder().userId(userId).build();
	}
}
