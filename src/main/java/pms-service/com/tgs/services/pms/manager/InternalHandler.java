package com.tgs.services.pms.manager;

import com.tgs.filters.*;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.pms.datamodel.*;
import com.tgs.services.pms.jparepository.CreditBillingService;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.jparepository.NRCreditService;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.restmodel.InternalResponse;
import com.tgs.services.pms.restmodel.UtilizationSum;
import com.tgs.services.ums.datamodel.User;
import com.tgs.filters.UserFilter;
import com.tgs.services.ums.datamodel.UserStatus;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class InternalHandler {

	 @Autowired
	    private UserServiceCommunicator userServiceCommunicator;

	    @Autowired
	    private CreditLineService creditLineService;

	    @Autowired
	    private CreditBillingService billingService;

	    @Autowired
	    private NRCreditService creditService;

	    @Autowired
	    private PaymentService paymentService;

	    public void multipleActiveCredits() {
	        // Select count(*) as ct, userid from creditline where status in ('A', 'B') group by userid having count(*) > 1;
	    }

	    public void negativeOutstanding() {
	        //select userid, outstandingbalance from creditline where outstandingbalance < 0;
	        //select userid, utilized from nonrevolvingcredit where utilized < 0;
	    }

	    public void duplicateBills() {
	        // select count(*) as ct,userid, billamount from creditbill group by userid, billamount having count(*) > 1;
	    }

	    public void wronglySettledBills() {
	        //select userid, billamount, settledamount  from creditbill where issettled = true and (billamount - settledamount) > 0;
	    }

	    public InternalResponse inconsistentOutstandingAndBillAmount() {
	        UserFilter filter = UserFilter.builder()
	                .roles(Arrays.asList(UserRole.AGENT, UserRole.DISTRIBUTOR))
	                .statuses(Collections.singletonList(UserStatus.ENABLED))
	                .build();
	        List<User> allUsers = userServiceCommunicator.getUsers(filter);
	        List<User> users = allUsers.stream().filter(user -> !MapUtils.isEmpty(user.getUserConf().getCreditPolicyMap())).collect(Collectors.toList());
	        Map<String, List<CreditLine>> creditLineMap = creditLineService.search(CreditLineFilter.builder()
	                .userIdIn(users.stream().map(User::getUserId).collect(Collectors.toList()))
	                .statusIn(Arrays.asList(CreditStatus.ACTIVE, CreditStatus.BLOCKED))
	                .build()).stream().collect(Collectors.groupingBy(CreditLine::getUserId));
	        Map<String, List<NRCredit>> nrCreditMap = creditService.search(NRCreditFilter.builder()
	                .userIdIn(users.stream().map(User::getUserId).collect(Collectors.toList()))
	                .statusIn(Arrays.asList(CreditStatus.ACTIVE, CreditStatus.BLOCKED))
	                .build()).stream().collect(Collectors.groupingBy(NRCredit::getUserId));
	        Map<String, List<CreditBill>> pendingBillMap = billingService.search(CreditBillFilter.builder()
	                .userIdIn(users.stream().map(User::getUserId).collect(Collectors.toList()))
	                .isSettled(false)
	                .build()).stream().collect(Collectors.groupingBy(CreditBill::getUserId));

	        InternalResponse response = new InternalResponse();
	        for (User user : users) {
	            String userId = user.getUserId();
	            boolean revolving = false;
	            boolean nonRevolving = false;
	            List<CreditLine> creditLines = creditLineMap.get(userId);
	            if (CollectionUtils.isNotEmpty(creditLines)) {
	                revolving = true;
	                List<CreditBill> creditBills = pendingBillMap.get(userId).stream().filter(b ->
	                        b.getAdditionalInfo().getCreditType().equals(CreditType.REVOLVING)).collect(Collectors.toList());
	                BigDecimal utilisationSum = CreditLine.outstandingSum(creditLines);
	                BigDecimal pendingBillSum = CreditBill.pendingSum(creditBills);
	                if (!utilisationSum.equals(pendingBillSum)) {
	                    response.getSummary().add(new UtilizationSum(userId, utilisationSum, pendingBillSum, CreditType.REVOLVING));
	                }
	            }

	            List<NRCredit> nrCreditList = nrCreditMap.get(userId);
	            if (CollectionUtils.isNotEmpty(nrCreditList)) {
	                nonRevolving = true;
	                List<CreditBill> creditBills = pendingBillMap.get(userId).stream().filter(b ->
	                        b.getAdditionalInfo().getCreditType().equals(CreditType.NON_REVOLVING)).collect(Collectors.toList());
	                BigDecimal utilisationSum = NRCredit.utilisedSum(nrCreditList);
	                BigDecimal pendingBillSum = CreditBill.pendingSum(creditBills);
	                if (!utilisationSum.equals(pendingBillSum)) {
	                    response.getSummary().add(new UtilizationSum(userId, utilisationSum, pendingBillSum, CreditType.NON_REVOLVING));
	                }
	            }
	            if (revolving && nonRevolving) {
	                response.getUnPolicedCreditUsers().add(userId);
	            }
	        }
	        return response;
	    }

	    /**
	     * This was written to fix when a non-revolving bill considered creditline(of a different user) and loan both while scanning payment entries
	     * and vice versa because their walletId was same.
	     * @param filter
	     * @return
	     */
	    public List<CreditBill> fixBills(CreditBillFilter filter) {
	        List<CreditBill> out = new ArrayList<>();
	        List<CreditBill> bills = billingService.search(filter);
	        bills.forEach(b -> {
	            if (b.getSettledAmount().equals(BigDecimal.ZERO)) {
	                BigDecimal oldAmount = b.getBillAmount();
	                PaymentMedium medium = b.getAdditionalInfo().getCreditType().equals(CreditType.REVOLVING) ? PaymentMedium.CREDIT_LINE : PaymentMedium.CREDIT;
	                PaymentFilter paymentFilter = PaymentFilter.builder().idIn(b.getAdditionalInfo().getChargedPaymentIds().stream().collect(Collectors.toList())).build();
	                List<Payment> payments = paymentService.search(paymentFilter);
	                payments = payments.stream().filter(p -> p.getPayUserId().equals(b.getUserId()) && p.getPaymentMedium().equals(medium))
	                        .collect(Collectors.toList());
	                BigDecimal billAmount = Payment.sum(payments);
	                if (!billAmount.equals(oldAmount)) out.add(b);
	                b.setBillAmount(billAmount);
	                b.getAdditionalInfo().setChargedPaymentIds(payments.stream().map(Payment::getId).collect(Collectors.toSet()));
	                if (billAmount.compareTo(BigDecimal.valueOf(500)) <= 0) b.getAdditionalInfo().setThresholdSettled(true);
	                if (billAmount.compareTo(BigDecimal.ZERO) <= 0) b.setIsSettled(true);
	                billingService.save(b);
	            }
	        });
	        return out;
	    }


	    public List<Payment> billUpdateFix(String payref) {
	        List<Payment> out = new ArrayList<>();
	        List<Payment> payments;
	        if (StringUtils.isEmpty(payref)) {
	            PaymentFilter paymentFilter = PaymentFilter.builder()
	                    .additionalInfo(PaymentAdditionalInfoFilter.builder().actualTransactionTypeIn(Collections.singletonList(PaymentTransactionType.BILL_SETTLE)).build())
	                    .createdOnGreaterThan(LocalDateTime.of(LocalDate.now(), LocalTime.MIN))
	                    .createdOnLessThan(LocalDateTime.of(LocalDate.now(), LocalTime.MAX))
	                    .status(PaymentStatus.SUCCESS)
	                    .build();
	            payments = paymentService.search(paymentFilter);
	        } else {
	            payments = paymentService.search(PaymentFilter.builder().paymentRefIdIn(Arrays.asList(payref)).build());
	        }
	        for (Payment p : payments) {
	            String billNum = p.getAdditionalInfo().getBillNumber();
	            CreditBill bill = billingService.findBillByBillNumber(billNum);
	            if (bill.getAdditionalInfo().getSettlementPaymentIds().add(p.getId())) {
	                bill.setSettledAmount(bill.getSettledAmount().add(p.getAmount().abs()));
	                BigDecimal pendingAmount = bill.getPendingAmount();
	                if (pendingAmount.compareTo(BigDecimal.valueOf(500)) <= 0) bill.getAdditionalInfo().setThresholdSettled(true);
	                if (pendingAmount.compareTo(BigDecimal.ZERO) <= 0) bill.setIsSettled(true);
	                billingService.save(bill);
	                out.add(p);
	            }
	        }
	        return out;
	    }
	}
