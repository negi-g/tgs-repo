package com.tgs.services.pms.restcontroller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.datamodel.CreditPolicy;
import com.tgs.services.pms.dbmodel.DbCreditPolicy;
import com.tgs.services.pms.jparepository.CreditPolicyService;
import com.tgs.services.pms.restmodel.CreditBillResponse;
import com.tgs.services.pms.restmodel.CreditPolicyResponse;
import com.tgs.services.pms.restmodel.UserPolicyRequest;
import com.tgs.services.pms.servicehandler.UserPolicyHandler;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.User;

@RestController
@RequestMapping("/pms/v1/creditpolicy")
@CustomRequestProcessor(areaRole = AreaRole.PMS_SERVICE, includedRoles = {UserRole.ADMIN, UserRole.ACCOUNTS})
public class CreditPolicyController {

	@Autowired
	private CreditPolicyService service;

	@Autowired
	private UserPolicyHandler userPolicyHandler;

	@Autowired
	private AuditsHandler auditHandler;

	@RequestMapping(value = "/{policyId}", method = RequestMethod.GET)
	protected CreditPolicyResponse get(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("policyId") String policyId) throws Exception {
		CreditPolicy creditPolicy = service.findByPolicyId(policyId);
		return new CreditPolicyResponse(creditPolicy);
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	protected CreditPolicyResponse getAll(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = SystemContextHolder.getContextData().getEmulateOrLoggedInUser();
		if (!UserRole.getMidOfficeRoles().contains(user.getRole()))
			throw new CustomGeneralException(SystemError.ACCESS_DENIED);
		List<CreditPolicy> creditPolicyList = service.getAll();
		return new CreditPolicyResponse(creditPolicyList);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CP_EDIT, includedRoles = {UserRole.ACCOUNTS, UserRole.ADMIN})
	protected CreditPolicyResponse save(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid CreditPolicy creditPolicy) throws Exception {
		CreditPolicy returnPolicy = service.save(creditPolicy);
		return new CreditPolicyResponse(returnPolicy);
	}

	@RequestMapping(value = "/updatePolicy", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CP_EDIT, includedRoles = {UserRole.ACCOUNTS, UserRole.ADMIN})
	protected CreditBillResponse updatePolicy(@RequestBody UserPolicyRequest policyRequest) throws Exception {
		CreditBill bill = userPolicyHandler.updatePolicy(policyRequest.getUserId(), policyRequest.getProduct(),
				policyRequest.getPolicy());
		return new CreditBillResponse(bill);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@CustomRequestProcessor(areaRole = AreaRole.CP_EDIT, includedRoles = {UserRole.ACCOUNTS, UserRole.ADMIN})
    protected BaseResponse delete(@PathVariable("id") Long id) throws Exception {
		service.delete(id);
		return new BaseResponse();
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbCreditPolicy.class, "policyId"));
		return auditResponse;
	}
}
