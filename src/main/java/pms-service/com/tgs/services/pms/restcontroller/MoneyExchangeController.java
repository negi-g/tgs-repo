package com.tgs.services.pms.restcontroller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;
import com.tgs.services.pms.dbmodel.DbMoneyExchangeInfo;
import com.tgs.services.pms.jparepository.MoneyExchangeService;
import com.tgs.services.pms.manager.me.MoneyExchangeManager;
import com.tgs.services.pms.restmodel.MoneyExchangeInfoResponse;
import com.tgs.services.pms.restmodel.MoneyExchangeListResponse;
import com.tgs.services.pms.restmodel.MoneyExchangeRequest;
import com.tgs.services.pms.validator.MoneyExchangeValidator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/pms/v1/moneyexchange")
public class MoneyExchangeController {

	@Autowired
	private MoneyExchangeService moneyExchangeService;

	@Autowired
	private AuditsHandler auditsHandler;

	@Autowired
	private MoneyExchangeValidator validator;
	
	@Autowired
	private MoneyExchangeManager moneyExchangeManager;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected @ResponseBody MoneyExchangeListResponse fetchExchangeRates(HttpServletRequest request,
			HttpServletResponse response, @RequestBody MoneyExchangeInfoFilter filter) {
		MoneyExchangeListResponse moneyExchangeList = new MoneyExchangeListResponse();
		moneyExchangeList.setMoneyExchangeList(moneyExchangeService.findAll(filter));
		return moneyExchangeList;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected @ResponseBody MoneyExchangeInfoResponse fetchExchangeRates(HttpServletRequest request,
			HttpServletResponse response, @RequestBody @Valid MoneyExchangeInfo moneyExchangeInfo) {
		MoneyExchangeInfo serviceResponse = moneyExchangeService.save(moneyExchangeInfo);
		MoneyExchangeInfoResponse moneyExchangeInfoResponse =
				MoneyExchangeInfoResponse.builder().moneyExchangeInfo(serviceResponse).build();
		return moneyExchangeInfoResponse;
	}
	
	@RequestMapping(value = "/job/update", method = RequestMethod.POST)
	protected @ResponseBody MoneyExchangeInfoResponse fetchExchangeRates(HttpServletRequest request,
			HttpServletResponse response, @RequestBody MoneyExchangeRequest moneyExchRequest) throws IOException {
		
		MoneyExchangeInfoResponse moneyExchangeInfoResponse = MoneyExchangeInfoResponse.builder().build();
		moneyExchangeInfoResponse.setMoneyExchangeInfoList(moneyExchangeManager.updateExchangeRate(moneyExchRequest.getMoneyExchangeInfo()));
		return moneyExchangeInfoResponse;
		
	}
	
	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse disableMoneyExchangeInfo(HttpServletRequest request,
			HttpServletResponse response, @PathVariable("id") Long id,
			@PathVariable("status") Boolean status) throws Exception {
		MoneyExchangeInfo moneyExchangeInfo = moneyExchangeService.updateStatus(id, status);
		BaseResponse baseResponse = new BaseResponse();
		if (moneyExchangeInfo == null) {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	protected @ResponseBody AuditResponse fetchMoneyExchangeAudits(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody AuditsRequest auditsRequestData)
			throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(
				auditsHandler.fetchAudits(auditsRequestData, DbMoneyExchangeInfo.class, ""));
		return auditResponse;
	}
}
