package com.tgs.services.pms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.pms.datamodel.ExternalPaymentStatusInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "externalpaymentstatus")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DbExternalPaymentStatusInfo extends BaseModel<DbExternalPaymentStatusInfo, ExternalPaymentStatusInfo> {

	@CreationTimestamp
	@Column
	private LocalDateTime createdOn;

	@Column
	private String refId;

	@Column
	private String status;

	@Column
	private String comment;

	@Override
	public ExternalPaymentStatusInfo toDomain() {
		return new GsonMapper<>(this, ExternalPaymentStatusInfo.class).convert();
	}

	@Override
	public DbExternalPaymentStatusInfo from(ExternalPaymentStatusInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbExternalPaymentStatusInfo.class).convert();
	}

}
