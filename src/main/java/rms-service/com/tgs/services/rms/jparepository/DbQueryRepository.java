package com.tgs.services.rms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.rms.dbmodel.DbReportsQuery;

@Repository
public interface DbQueryRepository extends JpaRepository<DbReportsQuery, Long>, JpaSpecificationExecutor<DbReportsQuery> {

}
