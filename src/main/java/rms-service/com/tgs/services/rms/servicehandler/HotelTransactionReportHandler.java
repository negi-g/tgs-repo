package com.tgs.services.rms.servicehandler;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.CityInfo;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelOrderSupplierInfo;
import com.tgs.services.hms.datamodel.ProcessedHotelInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.DbAmendment;
import com.tgs.services.oms.dbmodel.DbHotelOrder;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.rms.datamodel.HotelTransactionReportFilter;
import com.tgs.services.rms.datamodel.HotelTransactionResponse;
import com.tgs.services.rms.datamodel.HotelTransactionResponse.HotelTransactionResponseBuilder;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import helper.HotelReportOrderItem;
import helper.ProcessedPayment;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelTransactionReportHandler
		extends TransactionReportHandler<HotelTransactionReportFilter, ReportResponse> {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	UserServiceCommunicator userCommunicator;

	@Autowired
	HotelOrderItemManager orderItemManager;

	@SuppressWarnings("unchecked")
	@Override
	public List getResultsFromDb() {
		List<HotelTransactionResponse> finalResponse = new ArrayList<>();

		if (request.getCreatedOnAfterDateTime() != null && request.getCreatedOnBeforeDateTime() != null) {
			EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
			StringBuilder paymentQuery = constructPaymentQuery();

			List<Object[]> payments =
					entityManager.unwrap(Session.class).createNativeQuery(paymentQuery.toString()).getResultList();
			Set<String> users = new HashSet<>();
			entityManager.close();

			payments.forEach(payment -> {

				ProcessedPayment pPayment = ProcessedPayment.builder().paymentMedium((String) payment[2])
						.userId((String) payment[3]).merchantTxnId((String) payment[4]).build();
				if (payment[0] != null)
					constructPaymentMap((String) payment[0], pPayment, amendmentPaymentMap);
				String bookingId = (String) payment[1];
				if (bookingId != null && (bookingId.matches("\\D*20.*") || bookingId.matches("\\D*60.*"))) {
					constructPaymentMap(bookingId, pPayment, bookingPaymentMap);
				}
				users.add((String) payment[3]);
			});
			bookingUsers = userCommunicator
					.getUsersFromCache(UserCacheFilter.builder().userIds(Lists.newArrayList(users)).build());
			salesHierarchy = userCommunicator.getUserRelations(Lists.newArrayList(users));

			Set<String> totalBookingIds = new HashSet<>(bookingPaymentMap.keySet());
			log.debug("[HTR] Booking ids found {} ", totalBookingIds.toString());
			log.debug("[HTR] Booking userids found {} ", users.toString());
			log.debug("[HTR] Booking userids found from bookings map {} ", bookingPaymentMap.keySet().toString());


			if (MapUtils.isNotEmpty(bookingPaymentMap)) {

				if (StringUtils.isBlank(request.getSelectionType()) || request.getSelectionType().equals("booking")) {
					Set<String> nonNullBookingIds =
							totalBookingIds.parallelStream().filter(Objects::nonNull).collect(Collectors.toSet());

					log.debug("[HTR] Non null booking ids found {} ", nonNullBookingIds.toString());
					OrderFilter orderFilter = OrderFilter.builder().bookingIds(new ArrayList<String>(nonNullBookingIds))
							.products(Arrays.asList(OrderType.HOTEL)).statuses(Arrays.asList(OrderStatus.SUCCESS))
							.build();
					List<DbHotelOrder> orders = orderItemManager.getHotelOrderList(orderFilter);
					if (CollectionUtils.isNotEmpty(orders)) {
						log.debug("[HTR] No of orders found {} ", orders.size());
						Map<String, List<HotelReportOrderItem>> map = new HashMap<>();
						Map<String, DbOrder> orderMap = new HashMap<>();
						orders.stream().forEach(order -> {
							order.getOrderItems().stream().forEach(orderItem -> {
								if (BooleanUtils.isNotTrue(orderItem.getRoomInfo().getIsDeleted())) {
									DbOrder dbOrder = order.getOrder();
									orderMap.put(dbOrder.getBookingId(), dbOrder);
									constructHotelOrderRow(map, orderItem, orderItem.getBookingId());
								}
							});
						});

						AtomicLong runCount = new AtomicLong(0L);
						map.forEach((k, v) -> {
							v.forEach(row -> {
								String key = k.split("_")[0];
								HotelTransactionResponse response = constructRowForHotelTransaction(row, null,
										bookingPaymentMap.get(key), orderMap.get(key), bookingUsers, salesHierarchy);
								response.setSr(runCount.incrementAndGet());

								if (response != null)
									finalResponse.add(response);
							});
						});
						log.debug("HTR : Constructed booking rows with size {}", map.size());
					}
				}
			}
		}
		return finalResponse;
	}

	private void constructHotelOrderRow(Map<String, List<HotelReportOrderItem>> hotelReportOrderItems,
			DbHotelOrderItem orderItem, String idKey) {
		String id = idKey.concat("_").concat(String.valueOf(orderItem.getId()));
		log.debug("HTR : Constructing hotel order row for id {} , bookingid {} ", id, orderItem.getBookingId());
		HotelReportOrderItem reportItem = new GsonMapper<>(orderItem, null, HotelReportOrderItem.class).convert();
		AdditionalHotelOrderItemInfo additionalInfo = orderItem.getAdditionalInfo();
		if (!ObjectUtils.isEmpty(additionalInfo) && !ObjectUtils.isEmpty(additionalInfo.getHInfo())) {
			HotelInfo hInfo = new GsonMapper<>(additionalInfo.getHInfo(), HotelInfo.class).convert();
			// processingManager.applyPropertyType(hInfo);
			reportItem.setHotelCategory(hInfo.getPropertyType());
			ProcessedHotelInfo hotelInfo = additionalInfo.getHInfo();
			if (!ObjectUtils.isEmpty(hotelInfo.getAddress())) {
				Address address = additionalInfo.getHInfo().getAddress();
				reportItem.setBookingId(orderItem.getBookingId());
				if (!ObjectUtils.isEmpty(address.getCity()))
					reportItem.setCity(address.getCity().getName());
				if (!ObjectUtils.isEmpty(address.getCountry()))
					reportItem.setCountry(address.getCountry().getName());
			}
			if (Objects.nonNull(hotelInfo.getRating())) {
				reportItem.setRating(hotelInfo.getRating());
			}
		}
		Long noOfNights =
				Duration.between(orderItem.getCheckInDate().atStartOfDay(), orderItem.getCheckOutDate().atStartOfDay())
						.toDays();
		reportItem.setNoOfNights(noOfNights.intValue());
		reportItem.setTotalNoOfRooms(1);
		reportItem.setTotalRoomNights(orderItem.getRoomInfo().getPerNightPriceInfos().size());
		hotelReportOrderItems.put(id, Lists.newArrayList(reportItem));
	}

	private HotelTransactionResponse constructRowForHotelTransaction(HotelReportOrderItem orderItem,
			DbAmendment amendMent, ProcessedPayment payment, DbOrder order, Map<String, User> bookingUsers,
			Map<String, Set<User>> salesHierachy) {
		HotelTransactionResponseBuilder response = HotelTransactionResponse.builder();
		User user = null;
		if (payment != null) {
			log.debug("HTR : Constructing transaction rows for userid {} , bookingid {} ", payment.getUserId(),
					order.getBookingId());
			user = bookingUsers.get(payment.getUserId());
			if (user == null)
				log.debug("HTR : User not found with userid {} ", payment.getUserId());
			TravellerInfo travellerInfo = orderItem.getRoomInfo().getTravellerInfo().get(0);
			log.debug("HTR : salesHierarchy size {} ", salesHierachy.size());
			log.debug("HTR : bookingUsers size {} ", bookingUsers.size());
			log.debug("HTR : salesHierarchy keyset {} ", salesHierachy.keySet());
			Set<User> userRelations = salesHierachy.get(user.getUserId());
			AddressInfo addressInfo = user.getAddressInfo();
			if (!ObjectUtils.isEmpty(addressInfo)) {
				CityInfo cityInfo = addressInfo.getCityInfo();
				if (!ObjectUtils.isEmpty(cityInfo)) {
					response.agencyCity(cityInfo.getName()).agencyState(cityInfo.getState());
				}
			}
			RoomInfo roomInfo = orderItem.getRoomInfo();
			response.bookingAmount(orderItem.getAmount()).bookingCurrency("INR")
					.bookingDate(orderItem.getCreatedOn().toLocalDate()).bookingId(orderItem.getBookingId())
					.checkoutDate(orderItem.getCheckOutDate())
					.checkinDate(orderItem.getCheckInDate())
					.bookingDate(order.getProcessedOn().toLocalDate())
					.city(orderItem.getCity()).country(orderItem.getCountry()).hotelName(orderItem.getHotel())
					.noOfNights(orderItem.getNoOfNights()).status(OrderStatus.getOrderStatus(order.getStatus()).name())
					.supplier(orderItem.getSupplierId())
					.supplierBookingId(orderItem.getAdditionalInfo().getSupplierBookingReference())
					.supplierRefNo(orderItem.getAdditionalInfo().getSupplierBookingId())
					.hotelBookingReference(orderItem.getAdditionalInfo().getHotelBookingReference())
					.totalNoOfRooms(orderItem.getTotalNoOfRooms()).totalRoomNights(orderItem.getTotalRoomNights())
					.agentMarkup(orderItem.getMarkup()).agentId(user.getUserId())
					.serviceDate(orderItem.getCreatedOn().toLocalDate()).agent(user.getName())
					.flowType(order.getAdditionalInfo().getFlowType() != null
							? order.getAdditionalInfo().getFlowType().getName()
							: "ONLINE")
					.channel(ChannelType.getNameFromCode(order.getChannelType()))
					.hotelCategory(orderItem.getHotelCategory()).pan(getPanNumber(Arrays.asList(roomInfo)))
					.pNum(getPassportNumber(Arrays.asList(roomInfo)))
					.voucherCode(order.getAdditionalInfo().getVoucherCode())
					.leaderName(StringUtils.join(travellerInfo.getFirstName(), " ", travellerInfo.getMiddleName(), " ",
							travellerInfo.getLastName()))
					.mealBasis(roomInfo.getMealBasis())
					.rating(orderItem.getRating())
					.isQuarantinePackage(orderItem.getAdditionalInfo().getIsQuarantinePackage());
			setFareComponents(response, roomInfo, orderItem);
			setSupplierChangeInfo(response, orderItem);
			if (!ObjectUtils.isEmpty(roomInfo.getDeadlineDateTime())) {
				response.deadline(orderItem.getRoomInfo().getDeadlineDateTime().toLocalDate());
			}

			if (userRelations != null)
				response.salesInCharge(Joiner.on(';')
						.join(userRelations.stream().map(usr -> usr.getName()).collect(Collectors.toSet())));

		}
		return response.build();
	}

	private static void setFareComponents(HotelTransactionResponseBuilder transactionResponse, RoomInfo roomInfo,
			HotelReportOrderItem orderItem) {
		List<HotelOrderSupplierInfo> hotelOrderSupplierInfos =
				orderItem.getAdditionalInfo().getHotelOrderSupplierInfos();

		Map<HotelFareComponent, Double> fareComponents = roomInfo.getTotalFareComponents();
		transactionResponse.cmu(fareComponents.getOrDefault(HotelFareComponent.CMU, 0.0));
		transactionResponse.discount(fareComponents.getOrDefault(HotelFareComponent.DS, 0.0));
		transactionResponse.pointsDiscount(fareComponents.getOrDefault(HotelFareComponent.RP, 0.0));
		transactionResponse.voucherDiscount(fareComponents.getOrDefault(HotelFareComponent.VD, 0.0));
		transactionResponse.oldSgp(fareComponents.getOrDefault(HotelFareComponent.SGP, 0.0));
		transactionResponse.oldSnp(fareComponents.getOrDefault(HotelFareComponent.SNP, 0.0));

		if (CollectionUtils.isNotEmpty(hotelOrderSupplierInfos)) {
			HotelOrderSupplierInfo lastHotelOrderSupplierInfo =
					hotelOrderSupplierInfos.get(hotelOrderSupplierInfos.size() - 1);
			transactionResponse
					.sgp(lastHotelOrderSupplierInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.SGP, 0.0));
			transactionResponse
					.snp(lastHotelOrderSupplierInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.SNP, 0.0));
		}
	}

	private static void setSupplierChangeInfo(HotelTransactionResponseBuilder transactionResponse,
			HotelReportOrderItem orderItem) {
		List<HotelOrderSupplierInfo> hotelOrderSupplierInfos =
				orderItem.getAdditionalInfo().getHotelOrderSupplierInfos();
		if (CollectionUtils.isNotEmpty(hotelOrderSupplierInfos)) {
			HotelOrderSupplierInfo lastHotelOrderSupplierInfo =
					hotelOrderSupplierInfos.get(hotelOrderSupplierInfos.size() - 1);
			transactionResponse.updatedSupplier(lastHotelOrderSupplierInfo.getSupplierId());
			transactionResponse.updatedSupplierBookingId(lastHotelOrderSupplierInfo.getSupplierBookingId());
		}
	}

	public String getPanNumber(List<RoomInfo> rooms) {
		StringJoiner joiner = new StringJoiner(",");
		for (RoomInfo roomInfo : rooms) {
			joiner.add(roomInfo.getTravellerInfo().get(0).getPanNumber());
		}
		return joiner.toString();
	}

	public String getPassportNumber(List<RoomInfo> rooms) {
		StringJoiner joiner = new StringJoiner(",");
		for (RoomInfo roomInfo : rooms) {
			joiner.add(roomInfo.getTravellerInfo().get(0).getPassportNumber());
		}
		return joiner.toString();
	}

	@Override
	public String getValuePartForEmail() {
		return null;
	}

	@Override
	protected Class<?> getResponseClass() {
		return HotelTransactionResponse.class;
	}

	@Override
	protected String getCollDocTypeForFetchingFields() {
		return "reports_hotel_transaction";
	}

}
