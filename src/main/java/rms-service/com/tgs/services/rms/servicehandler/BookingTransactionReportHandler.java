package com.tgs.services.rms.servicehandler;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.DbAmendment;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.rms.datamodel.AirBookingTransactionReportFilter;
import com.tgs.services.rms.datamodel.AirLineTransactionResponse;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import helper.ProcessedPayment;
import helper.ReportOrderItem;
import lombok.extern.slf4j.Slf4j;

import static com.tgs.services.base.utils.LogTypes.*;


@Service
@Slf4j
public class BookingTransactionReportHandler
		extends TransactionReportHandler<AirBookingTransactionReportFilter, ReportResponse> {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	UserServiceCommunicator userCommunicator;

	@Autowired
	FMSCommunicator fmsCommunicator;

	private Set<String> users = new HashSet<>();

	@Override
	public List getResultsFromDb() {
		List<AirLineTransactionResponse> finalResponse = new ArrayList<>();
		if (request.getCreatedOnAfterDateTime() != null && request.getCreatedOnBeforeDateTime() != null
				|| request.getDepartedOnAfterDate() != null && request.getDepartedOnBeforeDate() != null
				|| request.getBookedOnAfterDateTime() != null && request.getBookedOnBeforeDateTime() != null) {

			// Query Optimization
			LocalDateTime createdOnAfterForAllDbQueries = request.getCreatedOnAfterDateTime();
			LocalDateTime createdOnBeforeForAllDbQueries = request.getCreatedOnBeforeDateTime();
			if (createdOnAfterForAllDbQueries == null) {
				if (request.getBookedOnAfterDateTime() != null)
					createdOnAfterForAllDbQueries = request.getBookedOnAfterDateTime().minusMonths(3);
				else
					createdOnAfterForAllDbQueries =
							LocalDateTime.of(request.getDepartedOnAfterDate().minusMonths(3), LocalTime.MIN);
			}
			if (createdOnBeforeForAllDbQueries == null) {
				if (request.getBookedOnBeforeDateTime() != null)
					createdOnBeforeForAllDbQueries = request.getBookedOnBeforeDateTime();
				else
					createdOnBeforeForAllDbQueries = LocalDateTime.of(request.getDepartedOnBeforeDate(), LocalTime.MAX);
			}

			LogUtils.log(REPORT_FETCH_ORDER_START, SystemContextHolder.getContextData().getLogMetaInfo(), null);

			EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
			StringBuilder orderQuery =
					constructOrderQuery(createdOnBeforeForAllDbQueries, createdOnAfterForAllDbQueries);
			log.debug("Order Query {}", orderQuery.toString());
			List<Object[]> orders = entityManager.unwrap(Session.class).createNativeQuery(orderQuery.toString())
					.addEntity("a", DbAirOrderItem.class).addEntity("b", DbOrder.class).getResultList();
			entityManager.close();

			LogUtils.log(REPORT_FETCH_ORDER_END,
					SystemContextHolder.getContextData().getLogMetaInfo().setNoOfEntries(CollectionUtils.size(orders)),
					REPORT_FETCH_ORDER_START);

			log.info("BTR : Fetched orders entries from db {}", CollectionUtils.size(orders));
			Map<String, DbOrder> orderMap = new HashMap<>();
			if (CollectionUtils.isNotEmpty(orders)) {
				orders.forEach(order -> {
					DbOrder dbOrder = (DbOrder) order[1];
					orderMap.put(dbOrder.getBookingId(), dbOrder);
					users.add(dbOrder.getBookingUserId());
				});
			}
			Set<String> bookingids = new HashSet<>(orderMap.keySet());

			log.info("BTR : Constructed order map {}", MapUtils.size(orderMap));

			// Query Optimization : CreatedOn should never be null
			LocalDateTime createdBfr =
					request.getCreatedOnBeforeDateTime() != null ? request.getCreatedOnBeforeDateTime()
							: request.getBookedOnBeforeDateTime() != null ? request.getBookedOnBeforeDateTime()
									: createdOnBeforeForAllDbQueries;
			LocalDateTime createdAftr =
					request.getCreatedOnAfterDateTime() != null ? request.getCreatedOnAfterDateTime()
							: request.getBookedOnAfterDateTime() != null ? request.getBookedOnAfterDateTime()
									: createdOnAfterForAllDbQueries;

			LogUtils.log(REPORT_FETCH_AMENDMENT_START, SystemContextHolder.getContextData().getLogMetaInfo(), null);
			// Fetch amendments and corresponding booking status from old snapshot of amendment
			entityManager = em.getEntityManagerFactory().createEntityManager();
			StringBuilder amendMentQuery =
					constructAmendmentQuery(CollectionUtils.isNotEmpty(bookingids) ? bookingids : null,
							createdBfr.plusMonths(getMonthsToScanForAmendments()), createdAftr);

			log.debug("Amendment Query {}", amendMentQuery.toString());

			List<DbAmendment> bookingAmendments = entityManager.unwrap(Session.class)
					.createNativeQuery(amendMentQuery.toString()).addEntity("a", DbAmendment.class).getResultList();
			entityManager.close();

			LogUtils.log(REPORT_FETCH_AMENDMENT_END, SystemContextHolder.getContextData().getLogMetaInfo()
					.setNoOfEntries(CollectionUtils.size(bookingAmendments)), REPORT_FETCH_AMENDMENT_START);

			log.info("BTR : Fetched amendment entries from db {}", CollectionUtils.size(bookingAmendments));

			Map<String, DbAmendment> amendmentWithBookings = new HashMap<>();
			Map<String, DbAmendment> onlyAmendments = new HashMap<>();
			Set<String> bookingIdsOfOnlyAmendments = new HashSet<>();

			bookingAmendments.forEach(bookingAmendment -> {
				if (bookingids.contains(bookingAmendment.getBookingId()))
					amendmentWithBookings.put(bookingAmendment.getAmendmentId(), bookingAmendment);
				else {
					onlyAmendments.put(bookingAmendment.getAmendmentId(), bookingAmendment);
					bookingIdsOfOnlyAmendments.add(bookingAmendment.getBookingId());
				}
			});

			bookingids.addAll(bookingIdsOfOnlyAmendments);

			LogUtils.log(REPORT_FETCH_AMENDMENTBOOKINGS_START, SystemContextHolder.getContextData().getLogMetaInfo(),
					null);

			orderMap.putAll(constructOrdersMap(bookingIdsOfOnlyAmendments, em, createdOnBeforeForAllDbQueries,
					createdOnAfterForAllDbQueries));

			LogUtils.log(REPORT_FETCH_AMENDMENTBOOKINGS_END,
					SystemContextHolder.getContextData().getLogMetaInfo().setNoOfEntries(MapUtils.size(orderMap)),
					REPORT_FETCH_AMENDMENTBOOKINGS_START);

			log.info("BTR : Amendments processed {}", MapUtils.size(orderMap));

			if (BooleanUtils.isNotFalse(request.getFetchPayments())) {
				LogUtils.log(REPORT_FETCH_AMENDMENT_PAYMENT_START,
						SystemContextHolder.getContextData().getLogMetaInfo(), null);

				processPayments(bookingids, createdOnBeforeForAllDbQueries, createdOnAfterForAllDbQueries);

				LogUtils.log(REPORT_FETCH_AMENDMENT_PAYMENT_END,
						SystemContextHolder.getContextData().getLogMetaInfo().setNoOfEntries(null),
						REPORT_FETCH_AMENDMENT_PAYMENT_START);
			}
			log.info("BTR : Fetched payment entries from db");

			// LogUtils.log(REPORT_FETCH_FROM_DB, SystemContextHolder.getContextData().getLogMetaInfo(), REPORT_START);

			LogUtils.log(USER_RELATION_START, SystemContextHolder.getContextData().getLogMetaInfo(), null);

			bookingUsers = userCommunicator
					.getUsersFromCache(UserCacheFilter.builder().userIds(Lists.newArrayList(users)).build());
			// salesHierarchy = new HashMap<>();
			salesHierarchy = userCommunicator.getUserRelations(Lists.newArrayList(users));

			LogUtils.log(USER_RELATION_END,
					SystemContextHolder.getContextData().getLogMetaInfo().setNoOfEntries(MapUtils.size(bookingUsers)),
					USER_RELATION_START);

			LogUtils.log(CONSTRUCT_AMENDMENTWB_START, SystemContextHolder.getContextData().getLogMetaInfo(), null);

			amendmentWithBookings.forEach((amendmentId, amendment) -> {
				Set<String> amendmentSet = new HashSet<>();
				amendmentSet.add(amendmentId);
				finalResponse.addAll(constructRowsforBookingsWithAmendments(
						BooleanUtils.isNotFalse(request.getFetchAmendments()), bookingids, amendmentSet, amendment,
						orderMap, request.getCreatedOnAfterDateTime(), request.getCreatedOnBeforeDateTime()));
			});

			LogUtils.log(CONSTRUCT_AMENDMENTWB_END, SystemContextHolder.getContextData().getLogMetaInfo()
					.setNoOfEntries(MapUtils.size(amendmentWithBookings)), CONSTRUCT_AMENDMENTWB_START);

			log.info("BTR : Constructed rows of booking ids with amendments {}",
					CollectionUtils.size(amendmentWithBookings));

			LogUtils.log(CONSTRUCT_AMENDMENTOA_START, SystemContextHolder.getContextData().getLogMetaInfo(), null);
			onlyAmendments.forEach((k, v) -> {
				Set<String> set = new HashSet<>();
				set.add(k);
				log.debug("BTR : Fetching amendment for amendment id {} , id {} ", v.getAmendmentId(), v.getId());
				List<AirOrderItem> airOrders = airAmendmentManager.getAmendmentSnapshot(v, null, false);
				if (CollectionUtils.isNotEmpty(airOrders) && orderMap.get(v.getBookingId()) != null)
					finalResponse
							.addAll(constructRowForAmendmentSnapshots(v, airOrders, k, orderMap.get(v.getBookingId()),
									request.getCreatedOnAfterDateTime(), request.getCreatedOnBeforeDateTime()));
			});
			log.info("BTR : Constructed rows of only amendments {}", CollectionUtils.size(onlyAmendments));
			LogUtils.log(CONSTRUCT_AMENDMENTOA_END, SystemContextHolder.getContextData().getLogMetaInfo(),
					CONSTRUCT_AMENDMENTOA_START);

			// Construct map with trip info for all passengers
			LogUtils.log(CONSTRUCT_TRIP_START, SystemContextHolder.getContextData().getLogMetaInfo(), null);
			Map<String, List<ReportOrderItem>> map = constructTripInfo(orders, bookingids);
			map.forEach((k, v) -> {
				v.forEach(row -> {
					String key = StringUtils.split(k, "_")[0];// k.split("_")[0];
					AirLineTransactionResponse response = constructRow(row, null, bookingPaymentMap.get(key),
							orderMap.get(key), bookingUsers, salesHierarchy);
					if (response != null)
						finalResponse.add(response);
				});
			});
			LogUtils.log(CONSTRUCT_TRIP_END, SystemContextHolder.getContextData().getLogMetaInfo(),
					CONSTRUCT_TRIP_START);

		} else {
			throw new CustomGeneralException(SystemError.INVALID_QUERY_FILTER);
		}

		LogUtils.log(CONSTRUCT_ROW_START, SystemContextHolder.getContextData().getLogMetaInfo(), null);

		log.info("BTR : Constructed trip info");
		List<String> allowedRoles = getUserRoles();

		// This is done to apply airline code/ depature date filter in amendment rows because while fetching amendments
		// , we can't make join on airorderitem since common fields like createdon will then have same value,
		// and also we need to take care while fetching airorder rows from amendments snapshots
		if (CollectionUtils.isNotEmpty(request.getAirlineCodes()) || request.getDepartedOnAfterDate() != null
				|| CollectionUtils.isNotEmpty(allowedRoles)) {

			List<AirLineTransactionResponse> processedRes = finalResponse.stream().filter(f -> {
				boolean isValid = true;
				if (CollectionUtils.isNotEmpty(request.getAirlineCodes()))
					isValid = isValid && request.getAirlineCodes().contains(f.getAirlineCode());
				if (request.getDepartedOnAfterDate() != null && request.getDepartedOnBeforeDate() != null)
					isValid = isValid
							&& (LocalDate.parse(f.getDepartureDate()).isAfter(request.getDepartedOnAfterDate())
									|| LocalDate.parse(f.getDepartureDate()).isEqual(request.getDepartedOnAfterDate()))
							&& (LocalDate.parse(f.getDepartureDate()).isBefore(request.getDepartedOnBeforeDate())
									|| LocalDate.parse(f.getDepartureDate())
											.isEqual(request.getDepartedOnBeforeDate()));
				if (CollectionUtils.isNotEmpty(allowedRoles)) {
					String bookingUserId = f.getBookingUserId();
					UserRole bookingUserRole =
							bookingUsers.getOrDefault(bookingUserId, User.builder().build()).getRole();
					if (StringUtils.isNotBlank(bookingUserId)) {
						isValid = isValid && allowedRoles.contains(
								bookingUserRole != null ? bookingUserRole.getRoleDisplayName() : StringUtils.EMPTY);
					}
				}
				return isValid;
			}).sorted(getComparatorForSorting()).collect(Collectors.toList());
			LogUtils.log(CONSTRUCT_ROW_END, SystemContextHolder.getContextData().getLogMetaInfo(), CONSTRUCT_ROW_START);
			return processedRes;
		} else {
			LogUtils.log(CONSTRUCT_ROW_END, SystemContextHolder.getContextData().getLogMetaInfo(), CONSTRUCT_ROW_START);
			return sortResponse(finalResponse);
		}
		// return finalResponse;
	}


	private void processPayments(Set<String> bookingids, LocalDateTime createdOnBeforeDate,
			LocalDateTime createdOnAfterDateTime) {
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		StringBuilder paymentQuery = constructPaymentQuery(bookingids, createdOnBeforeDate, createdOnAfterDateTime);
		List<Object[]> payments =
				entityManager.unwrap(Session.class).createNativeQuery(paymentQuery.toString()).getResultList();
		payments.forEach(payment -> {
			PaymentConfigurationRule rule =
					payment[6] != null ? getPaymentRule(Integer.valueOf(payment[6].toString())) : null;
			ProcessedPayment pPayment = ProcessedPayment.builder().paymentMedium((String) payment[1])
					.userId((String) payment[2]).merchantTxnId((String) payment[3]).gatewaytype((String) payment[5])
					.ruleId((String) payment[6]).paymentSupplierId(rule != null ? rule.getExternalPgInfoId() : null)
					.build();
			constructPaymentMap((String) payment[0], pPayment, bookingPaymentMap);
			if (payment[4] != null)
				constructPaymentMap((String) payment[4], pPayment, amendmentPaymentMap);
			users.add((String) payment[2]);
		});
		entityManager.close();
	}


	private StringBuilder constructPaymentQuery(Set<String> bookingids, LocalDateTime createdOnBeforeDateTime,
			LocalDateTime createdOnAfterDateTime) {
		StringBuilder paymentQuery = new StringBuilder(
				"SELECT refid,paymentmedium,payUserId,merchantTxnId,amendmentid,additionalInfo->>'gw' as gw,additionalinfo->>'ruleId' as ruleId FROM payment where status = 'S' and refid IN ('")
						.append(Joiner.on("','").join(bookingids)).append("')").append(" and createdon >= '")
						.append(createdOnAfterDateTime.toString()).append("'").append(" and createdon <= '")
						.append(createdOnBeforeDateTime.toString()).append("';");
		return paymentQuery;
	}

	private Map<String, List<ReportOrderItem>> constructTripInfo(List<Object[]> orders, Set<String> bookingids) {
		Map<String, List<ReportOrderItem>> map = new HashMap<>();
		for (Object[] order : orders) {
			DbAirOrderItem orderItem = (DbAirOrderItem) order[0];
			DbOrder dbOrder = (DbOrder) order[1];
			users.add(dbOrder.getBookingUserId());
			users.add(dbOrder.getLoggedInUserId());
			if (dbOrder.getAdditionalInfo().getAssignedUserId() != null)
				users.add(dbOrder.getAdditionalInfo().getAssignedUserId());
			if (bookingids.contains(dbOrder.getBookingId()))
				constructAirOrderRow(map, orderItem, orderItem.getBookingId());
		}
		return map;
	}

	private StringBuilder constructOrderQuery(LocalDateTime createdOnBeforeDateTime,
			LocalDateTime createdOnAfterDateTime) {
		StringBuilder orderQuery = new StringBuilder(
				"SELECT {a.*}, {b.*} FROM airorderitem a JOIN orders b ON a.bookingid = b.bookingid WHERE ordertype ='A' and b.status IN ('S','C') ");
		orderQuery.append(" and b.createdOn>=' ").append(createdOnAfterDateTime.toString()).append("' ");
		orderQuery.append(" and b.createdOn<='").append(createdOnBeforeDateTime.toString()).append("' ");
		orderQuery.append(" and a.createdOn>=' ").append(createdOnAfterDateTime.toString()).append("' ");
		orderQuery.append(" and a.createdOn<='").append(createdOnBeforeDateTime.toString()).append("' ");
		if (request.getDepartedOnAfterDate() != null)
			orderQuery.append(" and a.departureTime >= '")
					.append(LocalDateTime.of(request.getDepartedOnAfterDate(), LocalTime.MIN).toString()).append("' ");
		if (request.getDepartedOnBeforeDate() != null)
			orderQuery.append(" and a.departureTime <= '")
					.append(LocalDateTime.of(request.getDepartedOnBeforeDate(), LocalTime.MAX).toString()).append("' ");
		if (request.getBookedOnAfterDateTime() != null)
			orderQuery.append(" and b.additionalinfo#>>'{fut,S}' >= '")
					.append(request.getBookedOnAfterDateTime().toString()).append("' ");
		if (request.getBookedOnBeforeDateTime() != null)
			orderQuery.append(" and b.additionalinfo#>>'{fut,S}' <= '")
					.append(request.getBookedOnBeforeDateTime().toString()).append("' ");
		if (CollectionUtils.isNotEmpty(request.getAirlineCodes()))
			orderQuery.append(" and a.airlinecode IN ('").append(Joiner.on("','").join(request.getAirlineCodes()))
					.append("') ");
		if (request.getCabinClass() != null)
			orderQuery.append(
					" and EXISTS ( SELECT FROM jsonb_array_elements(a.travellerinfo) tinfo WHERE tinfo -> 'fd' @> '{\"cc\":\"")
					.append(request.getCabinClass().getCode()).append("\"}')");
		List<String> allowedUserIds = getUserIds();
		if (CollectionUtils.isNotEmpty(allowedUserIds)) {
			orderQuery.append("and b.bookingUserId IN ('").append(Joiner.on("','").join(allowedUserIds)).append("')");
			orderQuery.append("and a.bookingUserId IN ('").append(Joiner.on("','").join(allowedUserIds)).append("')");
		}

		orderQuery.append(" order by a.bookingid,a.id;");
		return orderQuery;
	}

	protected Map<String, DbOrder> constructOrdersMap(Set<String> totalBookingIds, EntityManager em,
			LocalDateTime createdOnBeforeDateTime, LocalDateTime createdOnAfterDateTime) {
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		StringBuilder orderQuery = new StringBuilder(
				"SELECT {o.*}, {a.*} from orders o join airorderitem a on a.bookingid = o.bookingid where o.bookingId IN ('")
						.append(Joiner.on("','").join(totalBookingIds)).append("')");
		orderQuery.append(" and o.createdOn>=' ").append(createdOnAfterDateTime.toString()).append("' ");
		orderQuery.append(" and o.createdOn<='").append(createdOnBeforeDateTime.toString()).append("' ");
		orderQuery.append(" and a.createdOn>=' ").append(createdOnAfterDateTime.toString()).append("' ");
		orderQuery.append(" and a.createdOn<='").append(createdOnBeforeDateTime.toString()).append("' ");
		if (request.getDepartedOnAfterDate() != null)
			orderQuery.append(" and a.departureTime >= '").append(request.getDepartedOnAfterDate().toString())
					.append("' ");
		if (request.getDepartedOnBeforeDate() != null)
			orderQuery.append(" and a.departureTime <= '").append(request.getDepartedOnBeforeDate().toString())
					.append("' ");
		if (CollectionUtils.isNotEmpty(request.getAirlineCodes()))
			orderQuery.append(" and a.airlinecode IN ('").append(Joiner.on("','").join(request.getAirlineCodes()))
					.append("') ");
		if (request.getCabinClass() != null)
			orderQuery.append(
					" and EXISTS ( SELECT FROM jsonb_array_elements(a.travellerinfo) tinfo WHERE tinfo -> 'fd' @> '{\"cc\":\"")
					.append(request.getCabinClass().getCode()).append("\"}')");
		List<String> allowedUserIds = getUserIds();
		if (CollectionUtils.isNotEmpty(allowedUserIds)) {
			orderQuery.append("and o.bookingUserId IN ('").append(Joiner.on("','").join(allowedUserIds)).append("')");
			orderQuery.append("and a.bookingUserId IN ('").append(Joiner.on("','").join(allowedUserIds)).append("')");
		}

		orderQuery.append(" order by a.bookingid,a.id;");

		List<Object[]> orders = entityManager.unwrap(Session.class).createNativeQuery(orderQuery.toString())
				.addEntity("o", DbOrder.class).addEntity("a", DbAirOrderItem.class).getResultList();

		log.debug("Order with amendment {}", orderQuery.toString());

		entityManager.close();
		return orders.stream().map(orderItem -> {
			DbOrder order = (DbOrder) orderItem[0];
			return new SimpleEntry<>(order.getBookingId(), order);
		}).collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1));
	}

	@Override
	public String getValuePartForEmail() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getCollDocTypeForFetchingFields() {
		return "reports_booking_transaction";
	}

}
