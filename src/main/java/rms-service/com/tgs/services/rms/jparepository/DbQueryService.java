package com.tgs.services.rms.jparepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.rms.dbmodel.DbReportsQuery;

@Service
public class DbQueryService {

	@Autowired
	private DbQueryRepository queryRepo;
	
	public DbReportsQuery findById(Long id) {
		return queryRepo.findOne(id);
	}
}
