package com.tgs.services.rms.servicehandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.rms.datamodel.ReportFilter;
import com.tgs.services.rms.datamodel.ReportFormat;
import com.tgs.services.rms.dbmodel.DbReportsQuery;
import com.tgs.services.rms.jparepository.DbQueryService;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.string.StringTemplateUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DynamicReportsHandler extends ReportHandler<ReportFilter,ReportResponse> {

	@Autowired
	private DbQueryService queryService;

	@PersistenceContext
	private EntityManager em;

	DbReportsQuery dbQuery;
	
	@Override
	public List getResultsFromDb() {
		dbQuery = queryService.findById(request.getQueryId());
		if(dbQuery!=null) {
			Map<String, String> columnsMap = new HashMap<>();
			List<String> columnsInRequest = request.getOutputFields();
			Map<String,String> allowedFieldsMap = dbQuery.getColumnMap();
			List<String> allowedFields = Lists.newArrayList(allowedFieldsMap.keySet());
			List<String> displayColumns = new ArrayList<>();
			if(CollectionUtils.isNotEmpty(columnsInRequest)) {
				List<String> queryFields = new ArrayList<>();
				columnsInRequest.forEach(colReq -> {
					for(String allowedField : allowedFields) {
						if(allowedField.toLowerCase().contains(colReq.toLowerCase())) {
							queryFields.add(allowedField);
							displayColumns.add(allowedFieldsMap.get(allowedField));
							break;
						}
					}
				});
				columnsMap.put("columns", String.join(",", queryFields));
				request.setOutputFields(displayColumns);
			}
			else {
				columnsMap.put("columns", String.join(",", allowedFields));
				request.setOutputFields(Lists.newArrayList(allowedFieldsMap.values()));
			}
			Map<String, Object> queryFields = ObjectUtils.firstNonNull(request.getQueryFields(),new HashMap<>());
			queryFields.put("createdOnAfterDate", request.getCreatedOnAfterDate());
			queryFields.put("createdOnBeforeDate", request.getCreatedOnBeforeDate());
			queryFields.put("createdOnAfterDateTime", request.getCreatedOnAfterDateTime());
			queryFields.put("createdOnBeforeDateTime", request.getCreatedOnBeforeDateTime());
			List<String> userIds = (List)queryFields.get("userIds");
			User loggedInUser = SystemContextHolder.getContextData().getUser();
			String loggedInUserId = loggedInUser == null ? UserUtils.EMPTY_USERID : loggedInUser.getUserId();
			List<String> allowedUserIds = UserServiceHelper.checkAndReturnAllowedUserId(
					loggedInUserId, userIds);
			if (CollectionUtils.isNotEmpty(allowedUserIds))
				queryFields.put("userIds", allowedUserIds);
			queryFields.forEach((k,v) -> {
				if(v instanceof List) {
					if(CollectionUtils.isNotEmpty((Collection) v)) {
						columnsMap.put(k,"('".concat(Joiner.on("','").join((Iterable<?>) v).concat("')")));
					}else {
						columnsMap.put(k,"''");
					}
				}else {
					columnsMap.put(k, String.valueOf(v));
				}
			});
			String query = StringTemplateUtils.replaceAttributes(dbQuery.getQuery(), columnsMap);
			EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
			List results = entityManager.unwrap(Session.class).createNativeQuery(constructQuery(query)).getResultList();
			entityManager.close();
			return results;
		}
		return new ArrayList<>();
	}

	/**
	 * Replaces the unknown parameters in the query.
	 * Example, Input : Select * from users where id = 1 or name = null
	 * Output : Select * from users where id = 1
	 */
	private String constructQuery(String str) {
		String regex = "[AND|OR]*\\s[A-z]*[.]*[A-z>'\\-]+[1-9]* (=|!=|>|>=|<|<=|IN){1} (''|'null'|\\s)";
		Pattern pattern = Pattern.compile(regex,Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(str);
		StringBuilder query = new StringBuilder();
		int startIndex = 0;
		while (matcher.find()) { 
			query.append(str.substring(startIndex, matcher.start()));
			startIndex = matcher.end();
		}
		query.append(str.substring(startIndex,str.length()));
		String queryStr = query.toString().replaceAll(" +", " ").replaceAll("where;", ";").replaceAll("where group by", "group by");
		log.debug("[ReportsModule] Query formed is {} ", queryStr);
		return queryStr;
	}

	@Override
	public String getValuePartForEmail() {
		if(dbQuery!=null) {
			return dbQuery.getDescription();
		}
		return "";
	}

	@Override
	public void process() throws Exception {
		List queryResults = getResultsFromDb();
		List<String> outputFields = request.getOutputFields();
		if(ReportFormat.REST.equals(request.getOutputFormat())) {
			handleRestResponse(queryResults, outputFields);
		}
		else {
			writeToFile(queryResults,outputFields);
		}
	}
}
