package com.tgs.services.rms.servicehandler;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.tgs.services.base.communicator.RailCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.amendments.RailAdditionalInfo;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.oms.dbmodel.DbAmendment;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.rail.DbRailOrderItem;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailPaxType;
import com.tgs.services.rms.datamodel.RailTransactionReportFilter;
import com.tgs.services.rms.datamodel.RailTransactionResponse;
import com.tgs.services.rms.datamodel.RailTransactionResponse.RailTransactionResponseBuilder;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import helper.ProcessedPayment;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RailTransactionReportHandler
		extends TransactionReportHandler<RailTransactionReportFilter, ReportResponse> {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	UserServiceCommunicator userCommunicator;

	@Autowired
	RailCommunicator railCommunicator;

	@SuppressWarnings("unchecked")
	@Override
	public List<RailTransactionResponse> getResultsFromDb() {
		List<RailTransactionResponse> finalResponse = new ArrayList<>();
		if (request.getCreatedOnAfterDateTime() != null && request.getCreatedOnBeforeDateTime() != null) {
			EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
			StringBuilder paymentQuery = constructPaymentQuery();
			List<Object[]> payments =
					entityManager.unwrap(Session.class).createNativeQuery(paymentQuery.toString()).getResultList();
			log.debug("RTR : Fetched payment entries from db");
			entityManager.close();
			Set<String> users = new HashSet<>();

			LocalDateTime createdOnAfterForAllDbQueries = request.getCreatedOnAfterDateTime().minusMonths(3);
			LocalDateTime createdOnBeforeForAllDbQueries = request.getCreatedOnBeforeDateTime();
			payments.forEach(payment -> {
				ProcessedPayment pPayment = ProcessedPayment.builder().paymentMedium((String) payment[2])
						.userId((String) payment[3]).merchantTxnId((String) payment[4]).build();
				String bookingId = (String) payment[1];
				// Fetch amendments of rail bookings only
				if (bookingId != null && bookingId.matches("\\D*90.*")) {
					constructPaymentMap(bookingId, pPayment, bookingPaymentMap);
					if (payment[0] != null)
						constructPaymentMap((String) payment[0], pPayment, amendmentPaymentMap);
				}
				users.add((String) payment[3]);
			});

			bookingUsers = userCommunicator
					.getUsersFromCache(UserCacheFilter.builder().userIds(Lists.newArrayList(users)).build());
			filterPaymentByRoles(amendmentPaymentMap, bookingPaymentMap);
			salesHierarchy = userCommunicator.getUserRelations(Lists.newArrayList(users));
			Set<String> totalBookingIds = new HashSet<>(bookingPaymentMap.keySet());
			Set<String> totalAmendments = new HashSet<>(amendmentPaymentMap.keySet());

			log.debug("[Reports] Booking ids found {} with size {} ", totalBookingIds.toString(),
					totalBookingIds.size());
			log.debug("[Reports] Amendment ids found {} with size {}  ", totalAmendments.toString(),
					totalAmendments.size());
			if (MapUtils.isNotEmpty(bookingPaymentMap)) {

				// Booking ids with amendments.
				StringBuilder bookingWithAmendment = constructAmendmentQuery(totalBookingIds,
						createdOnBeforeForAllDbQueries.plusMonths(getMonthsToScanForAmendments()),
						createdOnAfterForAllDbQueries);
				entityManager = em.getEntityManagerFactory().createEntityManager();
				List<DbAmendment> bookingAmendments =
						entityManager.unwrap(Session.class).createNativeQuery(bookingWithAmendment.toString())
								.addEntity("a", DbAmendment.class).getResultList();
				log.debug("RTR : Fetched bookings with amendments from db with size{} ", bookingAmendments.size());
				Map<String, DbOrder> ordersMap = constructOrdersMap(totalBookingIds, em, createdOnBeforeForAllDbQueries,
						createdOnAfterForAllDbQueries.minusMonths(3));
				log.debug("RTR : Fetched orders from db with size {} ", ordersMap.size());
				bookingAmendments.forEach(bookingAmendment -> {
					finalResponse.addAll(constructRailRowsforBookingsWithAmendments(
							StringUtils.isBlank(request.getSelectionType())
									|| request.getSelectionType().equals("amendment"),
							totalBookingIds, totalAmendments, bookingAmendment, ordersMap,
							request.getCreatedOnAfterDateTime(), request.getCreatedOnBeforeDateTime()));
				});
				log.debug("ATR : Constructed booking rows with amendments");
				entityManager.close();

				// Booking ids without amendments
				if (StringUtils.isBlank(request.getSelectionType()) || request.getSelectionType().equals("booking")) {
					StringBuilder orderQuery = constructOrderQuery(totalBookingIds, createdOnBeforeForAllDbQueries,
							createdOnAfterForAllDbQueries);
					entityManager = em.getEntityManagerFactory().createEntityManager();
					List<Object[]> orders = entityManager.unwrap(Session.class).createNativeQuery(orderQuery.toString())
							.addEntity("a", DbRailOrderItem.class).addEntity("b", DbOrder.class).getResultList();
					entityManager.close();
					log.debug("[Reports] Booking ids without amendments {} ", totalBookingIds);
					// Construct map with trip info for all passengers
					Map<String, DbOrder> orderMap = new HashMap<>();
					for (Object[] order : orders) {
						DbRailOrderItem orderItem = (DbRailOrderItem) order[0];
						DbOrder dbOrder = (DbOrder) order[1];
						orderMap.put(dbOrder.getBookingId(), dbOrder);
						finalResponse.addAll(constructRailRow(orderItem, null,
								bookingPaymentMap.get(dbOrder.getBookingId()), orderMap.get(dbOrder.getBookingId())));
					}
					if (orderMap.size() != totalBookingIds.size()) {
						List<String> difference = totalBookingIds.stream().filter(e -> !orderMap.keySet().contains(e))
								.collect(Collectors.toList());
						log.info("RTR : Booking ids not in results are {} ", difference.toString());
					}
					log.debug("RTR : Constructed booking rows without amendments");
				}

				// Only amendments
				if (StringUtils.isBlank(request.getSelectionType()) || request.getSelectionType().equals("amendment")) {

					if (CollectionUtils.isNotEmpty(totalAmendments)) {
						entityManager = em.getEntityManagerFactory().createEntityManager();
						log.debug("[Reports] Amendment ids without bookings {} ", totalAmendments);
						StringBuilder amendMentQuery = constructAmendmentQuery(createdOnAfterForAllDbQueries,
								createdOnBeforeForAllDbQueries, totalAmendments);
						List<Object[]> amendMentsWithOrder = entityManager.unwrap(Session.class)
								.createNativeQuery(amendMentQuery.toString()).addEntity("a", DbAmendment.class)
								.addEntity("b", DbOrder.class).getResultList();
						log.debug("RTR : Fetched amendments from db");
						entityManager.close();

						List<String> amendmentIdsInResults = new ArrayList<>();
						amendMentsWithOrder.forEach(amendMentOrder -> {
							DbAmendment amendMent = (DbAmendment) amendMentOrder[0];
							DbOrder dbOrder = (DbOrder) amendMentOrder[1];
							amendmentIdsInResults.add(amendMent.getAmendmentId());
							// Fetch new state from amendment and put in results
							RailOrderItem railOrderNewSnapshot =
									amendMent.getAdditionalInfo().getRailAdditionalInfo().getOrderCurrentSnapshot();
							constructRowForAmendmentSnapshots(finalResponse, amendMent, railOrderNewSnapshot,
									amendMent.getAmendmentId(), dbOrder);
						});

						// Adding INFO log only to ensure that after adding 3 months check we are not missing any
						// amendment
						if (amendmentIdsInResults.size() != totalAmendments.size()) {
							List<String> difference = totalAmendments.stream()
									.filter(e -> !amendmentIdsInResults.contains(e)).collect(Collectors.toList());
							log.info("RTR : Amendment ids not in results are {} ", difference.toString());
						}
						log.debug("RTR : Constructed booking rows of only amendments");
					}
				}

			}

		}
		return finalResponse;

	}

	private StringBuilder constructOrderQuery(Set<String> totalBookingIds, LocalDateTime createdOnBeforeDateTime,
			LocalDateTime createdOnAfterDateTime) {
		StringBuilder orderQuery = new StringBuilder(
				"SELECT {a.*}, {b.*} FROM railorderitem a JOIN orders b ON a.bookingid = b.bookingid WHERE b.bookingid IN ('")
						.append(Joiner.on("','").join(totalBookingIds)).append("')").append(" and a.createdon >= '")
						.append(createdOnAfterDateTime.toString()).append("'").append(" and a.createdon <= '")
						.append(createdOnBeforeDateTime.toString()).append("'").append(" and b.createdon >= '")
						.append(createdOnAfterDateTime.toString()).append("'").append(" and b.createdon <= '")
						.append(createdOnBeforeDateTime.toString()).append("'")
						.append(" and ordertype ='R' and b.status IN ('S','C') order by a.bookingid,a.id;");
		return orderQuery;
	}

	protected List<RailTransactionResponse> constructRailRow(DbRailOrderItem orderItem, DbAmendment amendment,
			ProcessedPayment payment, DbOrder order) {
		List<RailTransactionResponse> paxWiseRows = new ArrayList<>();
		try {
			RailTransactionResponseBuilder response = RailTransactionResponse.builder();
			User user = null;
			if (payment != null) {
				user = bookingUsers.get(payment.getUserId());
				response.thirdPartyInvoiceId(payment.getMerchantTxnId()).thirdPartyInvoiceStatus("Success");
				if (StringUtils.isNotEmpty(payment.getPaymentMedium()))
					response.paymentMedium(StringUtils.capitalize(
							PaymentMedium.getPaymentMedium(payment.getPaymentMedium()).name().toLowerCase()));
			}
			if (order != null) {
				if (user == null)
					user = bookingUsers.get(order.getBookingUserId());
				constructOrderFields(order, bookingUsers, response);
			}
			if (user != null) {
				constructUserFields(salesHierarchy, response, user);

			}
			if (amendment != null) {
				response.amendmentId(amendment.getAmendmentId())
						.amendmentType(StringUtils
								.capitalize(AmendmentType.getEnumFromCode(amendment.getAmendmentType()).name()))
						.systemInvoiceId(amendment.getAdditionalInfo().getInvoiceId())
						.amendmentDate(amendment.getProcessedOn().toString()).cartStatus("SUCCESS");
			}
			paxWiseRows = constructRailOrderitemFIelds(orderItem, response);
		} catch (Exception e) {
			log.error("Error occurred while converting row of booking id  {} ",
					orderItem != null ? orderItem.getBookingId() : amendment.getAmendmentId(), e);
		}
		return paxWiseRows;
	}

	private void constructUserFields(Map<String, Set<User>> salesHierarchy, RailTransactionResponseBuilder response,
			User user) {
		Set<User> userRelations = salesHierarchy.get(user.getUserId());
		response.paymentStatus("Success").bookingUserId(user.getUserId()).bookingUserName(user.getName())
				.bookingUserAccountcode(user.getAdditionalInfo().getAccountingCode());
		if (user.getAddressInfo() != null && user.getAddressInfo().getCityInfo() != null) {
			response.bookingUserCity(user.getAddressInfo().getCityInfo().getName())
					.bookingUserState(user.getAddressInfo().getCityInfo().getState());
		}
		if (userRelations != null)
			response.salesHierachy(
					Joiner.on(';').join(userRelations.stream().map(usr -> usr.getName()).collect(Collectors.toSet())));
	}

	private void constructOrderFields(DbOrder order, Map<String, User> bookingUsers,
			RailTransactionResponseBuilder response) {
		String bookingType =
				order.getAdditionalInfo().getFlowType() != null ? order.getAdditionalInfo().getFlowType().getName()
						: "AUTOMATED";
		String invoiceStatus = order.getAdditionalInfo().getInvoiceId() != null ? "Success" : "Pending";
		response.bookingType(bookingType)
				.systemInvoiceId(response.build().getSystemInvoiceId() != null ? response.build().getSystemInvoiceId()
						: order.getAdditionalInfo().getInvoiceId())
				.invoiceStatus(invoiceStatus).cartGenerationDate(order.getCreatedOn().toLocalDate().toString())
				.cartGenerationTime(order.getCreatedOn().toLocalTime().toString())
				.assignedUserId(order.getAdditionalInfo().getAssignedUserId())
				.cartStatus(OrderStatus.getEnumFromCode(order.getStatus()).name());
		if (order.getAdditionalInfo().getFirstUpdateTime() != null) {
			LocalDateTime bookingTime = order.getAdditionalInfo().getFirstUpdateTime().get("S");
			if (bookingTime != null) {
				response.bookingDate(bookingTime.toLocalDate().toString())
						.bookingTime(bookingTime.toLocalTime().toString());

			} else {
				response.bookingDate(order.getCreatedOn().toLocalTime().toString())
						.bookingTime(order.getCreatedOn().toLocalTime().toString());
			}
		}
		if (order.getAdditionalInfo().getAssignedUserId() != null) {
			try {
				if (bookingUsers.get(order.getAdditionalInfo().getAssignedUserId()) != null) {
					response.assignedUserName(
							bookingUsers.get(order.getAdditionalInfo().getAssignedUserId()).getName());
					response.bookingEmployeeName(
							bookingUsers.get(order.getAdditionalInfo().getAssignedUserId()).getName());
				} else {
					User assignee = userCommunicator.getUserFromCache(order.getAdditionalInfo().getAssignedUserId());
					bookingUsers.put(order.getAdditionalInfo().getAssignedUserId(), assignee);
					response.assignedUserName(assignee.getName());
					response.bookingEmployeeName(assignee.getName());
				}
			} catch (Exception e) {
				log.error("Error occurred while fetching user name for id {}",
						order.getAdditionalInfo().getAssignedUserId(), e);
			}
		}
		try {
			if (bookingUsers.get(order.getLoggedInUserId()) != null) {
				response.loggedInUserName(bookingUsers.get(order.getLoggedInUserId()).getName());
			} else {
				User loggedInUser = userCommunicator.getUserFromCache(order.getLoggedInUserId());
				bookingUsers.put(order.getLoggedInUserId(), loggedInUser);
				response.loggedInUserName(loggedInUser.getName());
			}

		} catch (Exception e) {
			log.error("Error occurred while fetching user name for id {},bookingId {}",
					order.getAdditionalInfo().getAssignedUserId(), order.getBookingId(), e);
		}

	}

	private List<RailTransactionResponse> constructRailOrderitemFIelds(DbRailOrderItem orderItem,
			RailTransactionResponseBuilder response) {
		List<RailTransactionResponse> paxList = new ArrayList<>();
		String deptStation = StringUtils.join(railCommunicator.getRailStationInfo(orderItem.getFromStn()).getName(),
				"(", orderItem.getFromStn(), ")");
		String arrStation = StringUtils.join(railCommunicator.getRailStationInfo(orderItem.getToStn()).getName(), "(",
				orderItem.getToStn(), ")");
		String boardingStation = deptStation;
		if (StringUtils.isNotEmpty(orderItem.getAdditionalInfo().getBoardingStation()))
			boardingStation = StringUtils.join(
					railCommunicator.getRailStationInfo(orderItem.getAdditionalInfo().getBoardingStation()).getName(),
					"(", orderItem.getAdditionalInfo().getBoardingStation(), ")");
		String oldBoardingStation = "";
		if (StringUtils.isNotEmpty(orderItem.getAdditionalInfo().getOldboardingStation()))
			oldBoardingStation = StringUtils.join(railCommunicator
					.getRailStationInfo(orderItem.getAdditionalInfo().getOldboardingStation()).getName(), "(",
					orderItem.getAdditionalInfo().getOldboardingStation(), ")");
		response.bookingId(orderItem.getBookingId())
				.departureDate(orderItem.getDepartureTime().toLocalDate().toString())
				.departureTime(orderItem.getDepartureTime().toLocalTime().toString())
				.arrivalDate(orderItem.getArrivalTime().toLocalDate().toString())
				.arrivalTime(orderItem.getArrivalTime().toLocalTime().toString())
				.days(ChronoUnit.DAYS.between(LocalDateTime.now(), orderItem.getDepartureTime()))
				.bookingClass(orderItem.getAdditionalInfo().getJourneyClass().toString()).departureStation(deptStation)
				.bookingQuota(orderItem.getAdditionalInfo().getQuota()).arrivalStation(arrStation)
				.boardingStation(boardingStation).trainName(orderItem.getAdditionalInfo().getRailInfo().getName())
				.trainNo(orderItem.getAdditionalInfo().getRailInfo().getNumber())
				.description(String.join("-", boardingStation, arrStation)).oldBoardingStation(oldBoardingStation)
				.referenceNo(orderItem.getAdditionalInfo().getReservationId())
				.pnr(orderItem.getAdditionalInfo().getPnr());
		orderItem.getTravellerInfo().forEach(traveller -> {
			RailTransactionResponseBuilder travellerResponse = response.build().toBuilder();
			if (traveller.getId() == 1)
				constructFareComponents(travellerResponse,
						orderItem.getAdditionalInfo().getPriceInfo().getFareComponents());
			travellerResponse.name(traveller.getFullName())
					// Seat allocations can be null for CHILD
					.bookingStatus(traveller.getBookingSeatAllocation() != null
							? traveller.getBookingSeatAllocation().getBookingStatus()
							: "")
					.currentStatus(traveller.getCurrentSeatAllocation() != null
							? traveller.getCurrentSeatAllocation().getBookingStatus()
							: "")
					.paxType(traveller.getPaxType() != null ? traveller.getPaxType().toString()
							: RailPaxType.INFANT.toString())
					.build();
			paxList.add(travellerResponse.build());
		});
		return paxList;
	}

	private void constructFareComponents(RailTransactionResponseBuilder response,
			Map<RailFareComponent, Double> fareComponents) {
		// gross - non refundable - rcf -> refundable amount
		// This is done to find correct TF. in case of amendment snapshots, tf is not being stored correctly.
		Double salesTF = fareComponents.getOrDefault(RailFareComponent.BF, 0.0)
				+ fareComponents.get(RailFareComponent.WP) + fareComponents.getOrDefault(RailFareComponent.WPT, 0.0)
				+ fareComponents.getOrDefault(RailFareComponent.TI, 0.0)
				+ fareComponents.getOrDefault(RailFareComponent.TIT, 0.0);
		// Amendment TF - non refundabale components
		Double amendmentNetFare = fareComponents.getOrDefault(RailFareComponent.AAR, 0.0);
		if (amendmentNetFare > 0) {
			amendmentNetFare = -1 * amendmentNetFare;
		}
		Double salesNetFare = salesTF + fareComponents.getOrDefault(RailFareComponent.MF, 0.0)
				+ fareComponents.getOrDefault(RailFareComponent.PC, 0.0);
		Double salesGrossFare = salesTF + fareComponents.getOrDefault(RailFareComponent.DMF, 0.0)
				+ fareComponents.getOrDefault(RailFareComponent.DPC, 0.0);
		/*
		 * Double netFare = fareComponents.getOrDefault(RailFareComponent.BF, 0.0) > 1 ? salesNetFare :
		 * amendmentNetFare; Double customerGrossFare = fareComponents.getOrDefault(RailFareComponent.BF, 0.0) > 1 ?
		 * salesGrossFare : amendmentNetFare;
		 */
		Double netFare = amendmentNetFare < 0 ? amendmentNetFare : salesNetFare;
		Double customerGrossFare = amendmentNetFare < 0 ? amendmentNetFare : salesGrossFare;
		response.trainFare(fareComponents.getOrDefault(RailFareComponent.BF, 0.0))
				.irctcCharges(fareComponents.getOrDefault(RailFareComponent.WP, 0.0)
						+ fareComponents.getOrDefault(RailFareComponent.WPT, 0.0))
				.mf(fareComponents.getOrDefault(RailFareComponent.MF, 0.0))
				.pgCharges(fareComponents.getOrDefault(RailFareComponent.PC, 0.0))
				.dispalyedMf(fareComponents.getOrDefault(RailFareComponent.DMF, 0.0))
				.displayedPgCharges(fareComponents.getOrDefault(RailFareComponent.DPC, 0.0))
				.grossFare(customerGrossFare).rcf(fareComponents.getOrDefault(RailFareComponent.RCF, 0.0))
				.raf(fareComponents.getOrDefault(RailFareComponent.RAF, 0.0))
				.ti(fareComponents.getOrDefault(RailFareComponent.TI, 0.0)
						+ fareComponents.getOrDefault(RailFareComponent.TIT, 0.0))
				.netFare(netFare);
	}

	private void constructRowForAmendmentSnapshots(List<RailTransactionResponse> finalResponse, DbAmendment amendMent,
			RailOrderItem railOrderItem, String id, DbOrder order) {
		DbRailOrderItem dbRailOrderItem = new DbRailOrderItem().from(railOrderItem);
		finalResponse.addAll(constructRailRow(dbRailOrderItem, amendMent,
				amendMent != null ? amendmentPaymentMap.get(amendMent.getAmendmentId())
						: bookingPaymentMap.get(order.getBookingId()),
				order));
	}

	public List<RailTransactionResponse> constructRailRowsforBookingsWithAmendments(boolean fetchAmendments,
			Set<String> totalBookingIds, Set<String> totalAmendments, DbAmendment amendMent,
			Map<String, DbOrder> orderMap, LocalDateTime createdOnAfterDateTime,
			LocalDateTime createdOnBeforeDateTime) {
		List<RailTransactionResponse> finalResponse = new ArrayList<>();
		DbOrder order = orderMap.get(amendMent.getBookingId());
		log.debug("[Reports] Booking id {} with amendment id {} ", amendMent.getBookingId(), amendMent.getId());
		totalBookingIds.remove(amendMent.getBookingId());
		RailAdditionalInfo railAdditionalInfo = amendMent.getAdditionalInfo().getRailAdditionalInfo();
		if (railAdditionalInfo != null) {
			RailOrderItem railOrderPreviousSnapshot = railAdditionalInfo.getOrderPreviousSnapshot();
			if (railOrderPreviousSnapshot != null) {
				LocalDateTime createdDate = railOrderPreviousSnapshot.getCreatedOn();
				DbRailOrderItem dbRailOrderItem = new DbRailOrderItem().from(railOrderPreviousSnapshot);
				if (createdOnAfterDateTime != null) {
					if (createdDate.equals(createdOnAfterDateTime) || createdDate.equals(createdOnBeforeDateTime)
							|| createdDate.isAfter(createdOnAfterDateTime)
									&& createdDate.isBefore(createdOnBeforeDateTime)) {
						finalResponse.addAll(constructRailRow(dbRailOrderItem, null,
								bookingPaymentMap.get(amendMent.getBookingId()), order));
					}
				} else {
					finalResponse.addAll(constructRailRow(dbRailOrderItem, null,
							bookingPaymentMap.get(amendMent.getBookingId()), order));
				}
			}
			log.debug("[Reports] Fetch amendments {} and total amendments are {} ", fetchAmendments,
					totalAmendments.toString());
			if (fetchAmendments && totalAmendments.contains(amendMent.getAmendmentId())) {
				totalAmendments.remove(amendMent.getAmendmentId());
				log.debug("[Reports] Processing amendment id {} ", amendMent.getAmendmentId());
				RailOrderItem railOrderCurrentSnapshot = railAdditionalInfo.getOrderCurrentSnapshot();
				if (railOrderCurrentSnapshot != null) {
					DbRailOrderItem dbRailOrderItem = new DbRailOrderItem().from(railOrderCurrentSnapshot);
					finalResponse.addAll(constructRailRow(dbRailOrderItem, amendMent,
							amendMent != null && !amendmentPaymentMap.isEmpty()
									? amendmentPaymentMap.get(amendMent.getAmendmentId())
									: bookingPaymentMap.get(amendMent.getBookingId()),
							order));
				}
			}
		}
		return sortRailResponse(finalResponse);
	}

	@Override
	public String getValuePartForEmail() {
		// TODO Auto-generated method stub
		return null;
	}

	private List<RailTransactionResponse> sortRailResponse(List<RailTransactionResponse> response) {
		Comparator<RailTransactionResponse> comparator = Comparator
				.comparing(RailTransactionResponse::getCartGenerationDate,
						Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(RailTransactionResponse::getCartGenerationTime,
						Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(RailTransactionResponse::getBookingId, Comparator.nullsLast(Comparator.naturalOrder()));
		return response.stream().sorted(comparator).collect(Collectors.toList());
	}

	protected StringBuilder constructAmendmentQuery(LocalDateTime createdOnAfterForAllDbQueries,
			LocalDateTime createdOnBeforeForAllDbQueries, Set<String> totalAmendments) {
		StringBuilder amendMentQuery = new StringBuilder(
				"SELECT {a.*},{b.*} from amendment a JOIN orders b ON a.bookingid = b.bookingid where a.status = 'S' and b.ordertype ='R' and a.amendmentId IN ('")
						.append(Joiner.on("','").join(totalAmendments)).append("')").append(" and a.createdon >= '")
						.append(createdOnAfterForAllDbQueries.toString()).append("'").append(" and a.createdon <= '")
						.append(createdOnBeforeForAllDbQueries.toString()).append("'").append(" and b.createdon >= '")
						.append(createdOnAfterForAllDbQueries.minusMonths(3).toString()).append("'")
						.append(" and b.createdon <= '").append(createdOnBeforeForAllDbQueries.toString()).append("';");
		return amendMentQuery;
	}

	@Override
	protected String getCollDocTypeForFetchingFields() {
		return "reports_rail_transaction";
	}

}
