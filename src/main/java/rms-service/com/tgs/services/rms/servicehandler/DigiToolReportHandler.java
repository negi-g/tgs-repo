package com.tgs.services.rms.servicehandler;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.DbAmendment;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.rms.datamodel.DigiToolReportFilter;
import com.tgs.services.rms.datamodel.DigiToolResponse;
import com.tgs.services.rms.datamodel.DigiToolResponse.DigiToolResponseBuilder;
import com.tgs.services.rms.restmodel.ReportResponse;
import helper.ReportOrderItem;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DigiToolReportHandler extends TransactionReportHandler<DigiToolReportFilter, ReportResponse> {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	FMSCommunicator fmsCommunicator;

	@Autowired
	private AirOrderItemService itemService;

	@SuppressWarnings("unchecked")
	@Override
	public List getResultsFromDb() {
		List<DigiToolResponse> finalResponse = new ArrayList<>();
		if (request.getCreatedOnAfterDateTime() != null && request.getCreatedOnBeforeDateTime() != null) {
			EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
			StringBuilder amendmentQuery = constructRowForAmendment();
			List<DbAmendment> amendments = entityManager.unwrap(Session.class)
					.createNativeQuery(amendmentQuery.toString()).addEntity("a", DbAmendment.class).getResultList();
			log.debug("Digi Tool : Fetched amendment entries from db");
			entityManager.close();
			amendments.forEach(amendment -> {
				try {
					List<DbAirOrderItem> dbairOrders = itemService.findByBookingId(amendment.getBookingId());
					if (CollectionUtils.isNotEmpty(dbairOrders))
						constructRowForDigiTool(finalResponse, amendment, dbairOrders);
				} catch (Exception e) {
					log.error("Not able to construct digitool for amendmentId: {} because of {}",
							amendment.getAmendmentId(), e.getMessage());
				}
			});
			if (CollectionUtils.isNotEmpty(request.getAirlineCodes())) {
				return finalResponse.stream().filter(f -> {
					boolean isValid = true;
					if (CollectionUtils.isNotEmpty(request.getAirlineCodes()))
						isValid = request.getAirlineCodes().contains(f.getAirlineCode());
					return isValid;
				}).sorted(getComparatorForSortingDigiTool()).collect(Collectors.toList());
			}
		}
		return sortDigiToolResponse(finalResponse);
	}

	protected List<DigiToolResponse> sortDigiToolResponse(List<DigiToolResponse> response) {
		Comparator<DigiToolResponse> comparator = getComparatorForSortingDigiTool();
		return response.stream().sorted(comparator).collect(Collectors.toList());
	}

	protected Comparator<DigiToolResponse> getComparatorForSortingDigiTool() {
		return Comparator.comparing(DigiToolResponse::getAmendmentDate, Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(DigiToolResponse::getBookingId, Comparator.nullsLast(Comparator.naturalOrder()));
	}

	private void constructRowForDigiTool(List<DigiToolResponse> finalResponse, DbAmendment amendMent,
			List<DbAirOrderItem> dbairOrders) {
		Map<String, List<ReportOrderItem>> ordersMap = new HashMap<>();
		dbairOrders.forEach(orderItem -> {
			constructAirOrderRow(ordersMap, orderItem, amendMent.getAmendmentId());
		});
		ordersMap.forEach((k, v) -> {
			v.forEach(orderItem -> {
				DigiToolResponse response = contructAmendmentRows(amendMent, orderItem);
				if (response != null)
					finalResponse.add(response);
			});
		});
	}

	private DigiToolResponse contructAmendmentRows(DbAmendment amendMent, ReportOrderItem orderItem) {
		DigiToolResponseBuilder response = DigiToolResponse.builder();
		response.amendmentId(amendMent.getAmendmentId())
				.amendmentType(AmendmentType.getEnumFromCode(amendMent.getAmendmentType()).name())
				.amendmentDate(amendMent.getCreatedOn().toLocalDate().toString())
				.amendmentStatus(AmendmentStatus.getEnumFromCode(amendMent.getStatus()).name())
				.bookingId(amendMent.getBookingId()).bookingUserId(amendMent.getBookingUserId())
				.airline(fmsCommunicator.getAirlineInfo(orderItem.getAirlinecode()).getName())
				.name(orderItem.getFlightTravellerInfo().getFullName()).airlineCode(orderItem.getAirlinecode())
				.pnr(orderItem.getFlightTravellerInfo().getPnr()).supplier(orderItem.getSupplierId())
				.ticketNumber(orderItem.getFlightTravellerInfo().getTicketNumber())
				.tripType(AirType.getEnumFromCode(orderItem.getAdditionalInfo().getType()).name());
		return response.build();

	}

	private StringBuilder constructRowForAmendment() {
		StringBuilder amendmentQuery = new StringBuilder(
				"SELECT * FROM amendment a where (a.status = 'REQ' or a.status = 'ASN' or a.status = 'PRC'  or a.status = 'PEN' or a.status = 'PRASN') and createdOn>='")
						.append(request.getCreatedOnAfterDateTime().toString()).append("' and createdOn<='")
						.append(request.getCreatedOnBeforeDateTime().toString()).append("'");
		log.info("Amendment query is {} ", amendmentQuery.toString());
		return amendmentQuery;
	}

	@Override
	public String getValuePartForEmail() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getCollDocTypeForFetchingFields() {
		return "reports_digi_tool";
	}

}
