package com.tgs.services.rms.common;

import com.tgs.services.rms.servicehandler.AirLineTransactionReportHandler;
import com.tgs.services.rms.servicehandler.AmexReportHandler;
import com.tgs.services.rms.servicehandler.BookingTransactionReportHandler;
import com.tgs.services.rms.servicehandler.DSRHandler;
import com.tgs.services.rms.servicehandler.DigiToolReportHandler;
import com.tgs.services.rms.servicehandler.DynamicReportsHandler;
import com.tgs.services.rms.servicehandler.HotelTransactionReportHandler;
import com.tgs.services.rms.servicehandler.RailTransactionReportHandler;
import lombok.Getter;


@Getter
public enum ReportBean {

	DATABASEQUERY(DynamicReportsHandler.class),
	AIRLINE_TRANSACTION(AirLineTransactionReportHandler.class),
	BOOKING_TRANSACTION(BookingTransactionReportHandler.class),
	HOTEL_TRANSACTION(HotelTransactionReportHandler.class),
	RAIL_TRANSACTION(RailTransactionReportHandler.class),
	DSR(DSRHandler.class),
	DIGI_TOOL(DigiToolReportHandler.class),
	AMEX_REPORT(AmexReportHandler.class);

	private Class classType;

	private ReportBean(Class classType) {
		this.classType = classType;
	}


}
