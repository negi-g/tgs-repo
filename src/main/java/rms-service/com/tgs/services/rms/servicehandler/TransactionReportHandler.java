package com.tgs.services.rms.servicehandler;

import static com.tgs.services.base.utils.LogTypes.*;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.DefaultReportField;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.oms.Amendments.AirAmendmentManager;
import com.tgs.services.oms.datamodel.AirItemDetail;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.DbAmendment;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirItemDetail;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.helper.PaymentConfigurationHelper;
import com.tgs.services.rms.datamodel.AirLineTransactionResponse;
import com.tgs.services.rms.datamodel.AirLineTransactionResponse.AirLineTransactionResponseBuilder;
import com.tgs.services.rms.datamodel.ReportFilter;
import com.tgs.services.rms.datamodel.ReportFormat;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserProfileFields;
import com.tgs.utils.encryption.KryoUtils;
import helper.ProcessedPayment;
import helper.ReportOrderItem;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class TransactionReportHandler<T extends ReportFilter, RT extends ReportResponse>
		extends ReportHandler<T, ReportResponse> {

	private String clientGst;

	private String clientVat;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;

	@Autowired
	FMSCommunicator fmsCommunicator;

	@Autowired
	UserServiceCommunicator userCommunicator;

	@Autowired
	AirAmendmentManager airAmendmentManager;

	protected Map<String, User> bookingUsers = new HashMap<>();
	protected Map<String, Set<User>> salesHierarchy = new HashMap<>();
	protected Map<String, ProcessedPayment> bookingPaymentMap = new HashMap<>();
	protected Map<String, ProcessedPayment> amendmentPaymentMap = new HashMap<>();
	protected User parentUser;
	protected Map<String, AirItemDetail> itemDetailMap = new HashMap<>();
	protected UserProfileFields profileMeta;
	protected Map<String, String> visibleCustomFields = new HashMap<>();
	protected Map<String, List<GstInfo>> gstInfoMap = new HashMap<>();

	// temp used map for loggedin & assigned user to avoid re fetch from respective communicator
	protected Map<String, User> usersMap = new HashMap<>();

	/**
	 * Intentionally making it static so that Aerospike call can be avoided, Because server are getting restarted
	 * everyday so next day it will be fine if user state has been changed
	 */
	protected static Map<String, Boolean> igstMap = new HashMap<>();
	protected ClientGeneralInfo clientInfo;

	@Override
	public void process() throws Exception {

		List<String> outputFields = request.getOutputFields();
		if (CollectionUtils.isEmpty(outputFields))
			throw new CustomGeneralException(SystemError.INVALID_OUTPUT_FIELDS);

		clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		List queryResults = getResultsFromDb();
		Map<String, String> allowedFieldsMap = fetchRoleSpecificKeys();

		List<String> columnDisplayNames = new ArrayList<>();
		outputFields.retainAll(allowedFieldsMap.keySet());
		outputFields.forEach(field -> {
			columnDisplayNames.add(allowedFieldsMap.get(field));
		});

		if (MapUtils.isNotEmpty(visibleCustomFields)) {
			outputFields.addAll(visibleCustomFields.values());
			columnDisplayNames.addAll(visibleCustomFields.values());
		}
		List<Map<String, Object>> outputData = getListOfRows(queryResults);
		if (!ReportFormat.REST.equals(request.getOutputFormat())) {
			List<Object[]> outputRows = constructRows(outputData, outputFields);
			writeToFile(outputRows, columnDisplayNames);
		}
	}

	protected Map<String, String> fetchRoleSpecificKeys() {
		return getTransactionFields(getResponseClass(), getCollDocTypeForFetchingFields());
	};

	// Map with id and display names
	public Map<String, String> getTransactionFields(Class<?> reportResponse, String type) {
		List<Document> docList = gsCommunicator.fetchGeneralRoleSpecificDocument(
				CollectionServiceFilter.builder().isConsiderHierarchy(true).types(Arrays.asList(type)).build());
		User user = SystemContextHolder.getContextData().getUser();
		if (docList == null) {
			throw new CustomGeneralException(SystemError.DOCUMENT_IS_EMPTY);
		} else if (CollectionUtils.isNotEmpty(docList) && docList.get(0).getKey().contains(user.getRole().toString())) {
			return getFieldstoShowInCollDocs(docList);
		} else {
			List<Field> fields = Arrays.asList(reportResponse.getDeclaredFields());
			Map<String, String> reportFields = new HashMap<>();
			for (Field field : fields) {
				field.setAccessible(true);
				DefaultReportField reportField = field.getAnnotation(DefaultReportField.class);
				Boolean flag = false;
				if (reportField != null) {
					for (UserRole role : reportField.includedRoles()) {
						if (role.equals(user.getRole())) {
							flag = true;
							break;
						}
					}
					for (UserRole role : reportField.excludedRoles()) {
						if (role.equals(user.getRole())) {
							break;
						}
					}
					if (flag || (UserUtils.isMidOfficeRole(user.getRole()) && reportField.midOfficeRole())
							|| (!UserUtils.isMidOfficeRole(user.getRole()) && reportField.nonMidOfficeRole())) {
						SerializedName serializedName = field.getAnnotation(SerializedName.class);
						reportFields.put(serializedName.value(), serializedName.value());
					}
				}
			}
			return reportFields;
		}
	}

	public Map<String, String> getFieldstoShowInCollDocs(List<Document> docList) {
		Map<String, List<Map<String, String>>> data = new Gson().fromJson(docList.get(0).getData(),
				new TypeToken<HashMap<String, List<Map<String, String>>>>() {}.getType());
		List<Map<String, String>> list = data.get("data");
		Map<String, String> columnMappings = list.stream().collect(Collectors.toMap(row -> row.get("id"),
				row -> row.get("description"), (oldValue, newValue) -> newValue));
		return columnMappings;
	}

	public void constructAirOrderRow(Map<String, List<ReportOrderItem>> map, DbAirOrderItem orderItem, String idKey) {
		String id = idKey.concat("_").concat(orderItem.getSource()).concat("-").concat(orderItem.getDest());
		log.debug(" ID is " + id);
		ReportOrderItem reportItem = new GsonMapper<>(orderItem, ReportOrderItem.class).convert();
		reportItem.setDestination(orderItem.getDest());
		reportItem.setTripKey(String.join("-", orderItem.getSource(), orderItem.getDest()));
		reportItem.setArrivalDateTime(orderItem.getArrivalTime());
		reportItem.setFlightNumbers(orderItem.getFlightNumber());
		constructRowForSegment(map, orderItem.getTravellerInfo(), id, reportItem);
	}

	public void constructAirOrderRow(Map<String, List<ReportOrderItem>> map, AirOrderItem orderItem, String idKey) {
		String id = idKey.concat("_").concat(orderItem.getSource()).concat("-").concat(orderItem.getDest());
		log.debug(" ID is " + id);
		ReportOrderItem reportItem = new GsonMapper<>(orderItem, ReportOrderItem.class).convert();
		reportItem.setDestination(orderItem.getDest());
		reportItem.setTripKey(String.join("-", orderItem.getSource(), orderItem.getDest()));
		reportItem.setArrivalDateTime(orderItem.getArrivalTime());
		reportItem.setFlightNumbers(orderItem.getFlightNumber());
		constructRowForSegment(map, orderItem.getTravellerInfo(), id, reportItem);
	}

	private void constructRowForSegment(Map<String, List<ReportOrderItem>> map,
			List<FlightTravellerInfo> travellerInfos, String id, ReportOrderItem reportItem) {
		if (CollectionUtils.isNotEmpty(travellerInfos)) {
			// if (orderItem.getTravellerInfo().size() > 1) {
			travellerInfos.forEach(pax -> {
				ReportOrderItem airOrder = KryoUtils.copy(reportItem);
				airOrder.setFlightTravellerInfo(pax);
				if (map.get(id) != null) {
					List<ReportOrderItem> existingTravellers = map.get(id);
					existingTravellers.add(airOrder);
					map.put(id, existingTravellers);
				} else
					map.put(id, Lists.newArrayList(airOrder));
			});
		}
	}

	public void constructPaymentMap(String key, ProcessedPayment pPayment, Map<String, ProcessedPayment> paymentMap) {
		ProcessedPayment existingPayment = paymentMap.get(key);
		if (existingPayment == null) {
			paymentMap.put(key, pPayment);
		}

	}

	public List<AirLineTransactionResponse> constructRowsforBookingsWithAmendments(boolean fetchAmendments,
			Set<String> totalBookingIds, Set<String> totalAmendments, DbAmendment amendMent,
			Map<String, DbOrder> orderMap, LocalDateTime createdOnAfterDateTime,
			LocalDateTime createdOnBeforeDateTime) {
		List<AirLineTransactionResponse> finalResponse = new ArrayList<>();
		DbOrder order = orderMap.get(amendMent.getBookingId());
		log.debug("[Reports] Booking id {} with amendment id {} ", amendMent.getBookingId(), amendMent.getId());
		totalBookingIds.remove(amendMent.getBookingId());
		if (amendMent.getAdditionalInfo().getAirAdditionalInfo() != null) {
			List<AirOrderItem> airOrders =
					amendMent.getAdditionalInfo().getAirAdditionalInfo().getOrderPreviousSnapshot();
			if (CollectionUtils.isNotEmpty(airOrders)) {
				LocalDateTime createdDate = airOrders.get(0).getCreatedOn();
				if (createdOnAfterDateTime != null) {
					if (createdDate.equals(createdOnAfterDateTime) || createdDate.equals(createdOnBeforeDateTime)
							|| createdDate.isAfter(createdOnAfterDateTime)
									&& createdDate.isBefore(createdOnBeforeDateTime)) {
						finalResponse.addAll(constructRowForAmendmentSnapshots(null, airOrders,
								amendMent.getBookingId(), order, createdOnAfterDateTime, createdOnBeforeDateTime));
					}
				} else {
					finalResponse.addAll(constructRowForAmendmentSnapshots(null, airOrders, amendMent.getBookingId(),
							order, createdOnAfterDateTime, createdOnBeforeDateTime));
				}
			}
			log.debug("[Reports] Fetch amendments {} and total amendments are {} ", fetchAmendments,
					totalAmendments.toString());
			if (fetchAmendments && totalAmendments.contains(amendMent.getAmendmentId())) {
				totalAmendments.remove(amendMent.getAmendmentId());
				log.debug("[Reports] Processing amendment id {} ", amendMent.getAmendmentId());
				List<AirOrderItem> amendmentSnapshot = airAmendmentManager.getAmendmentSnapshot(amendMent, null, false);
				if (CollectionUtils.isNotEmpty(amendmentSnapshot))
					finalResponse.addAll(constructRowForAmendmentSnapshots(amendMent, amendmentSnapshot,
							amendMent.getAmendmentId(), order, createdOnAfterDateTime, createdOnBeforeDateTime));
			}
		}
		return finalResponse;
	}

	public List<AirLineTransactionResponse> constructRowForAmendmentSnapshots(DbAmendment amendMent,
			List<AirOrderItem> airOrders, String id, DbOrder order, LocalDateTime createdOnAfterDateTime,
			LocalDateTime createdOnBeforeDateTime) {
		Map<String, List<ReportOrderItem>> ordersMap = new HashMap<>();
		List<AirLineTransactionResponse> finalResponse = new ArrayList<>();
		// it has impact on system , for every iteration converting to new list gson conversion
		// List<DbAirOrderItem> dbairOrders = new DbAirOrderItem().toDbList(airOrders);
		// dbairOrders.forEach(orderItem -> {
		// constructAirOrderRow(ordersMap, orderItem, id);
		// });
		airOrders.forEach(airOrder -> {
			constructAirOrderRow(ordersMap, airOrder, id);
		});


		ordersMap.forEach((k, v) -> {
			v.forEach(row -> {
				AirLineTransactionResponse response = null;
				if (isBTR()
						&& (amendMent != null && createdOnAfterDateTime != null && createdOnBeforeDateTime != null)) {
					LocalDateTime processedDate = amendMent.getProcessedOn();
					if (processedDate.equals(createdOnAfterDateTime) || processedDate.equals(createdOnBeforeDateTime)
							|| processedDate.isAfter(createdOnAfterDateTime)
									&& processedDate.isBefore(createdOnBeforeDateTime)) {
						String key = StringUtils.split(k, "_")[0];
						response = constructRow(row, amendMent,
								amendMent != null && !amendmentPaymentMap.isEmpty() ? amendmentPaymentMap.get(key)
										: bookingPaymentMap.get(key),
								order, bookingUsers, salesHierarchy);
					}
				} else {
					String key = StringUtils.split(k, "_")[0];
					response = constructRow(row, amendMent,
							amendMent != null && !amendmentPaymentMap.isEmpty() ? amendmentPaymentMap.get(key)
									: bookingPaymentMap.get(key),
							order, bookingUsers, salesHierarchy);
				}
				if (response != null)
					finalResponse.add(response);
			});
		});
		return finalResponse;
	}

	protected AirLineTransactionResponse constructRow(ReportOrderItem orderItem, DbAmendment amendment,
			ProcessedPayment payment, DbOrder order, Map<String, User> bookingUsers,
			Map<String, Set<User>> salesHierarchy) {
		try {
			AirLineTransactionResponseBuilder response = AirLineTransactionResponse.builder();
			User user = null;
			if (payment != null) {
				user = bookingUsers.get(payment.getUserId());
				response.thirdPartyInvoiceId(payment.getMerchantTxnId()).thirdPartyInvoiceStatus("Success");
				if (StringUtils.isNotEmpty(payment.getPaymentMedium())) {
					response.paymentMedium(
							StringUtils.capitalize(PaymentMedium.getPaymentMedium(payment.getPaymentMedium()).name()));
				}
				if (StringUtils.isNotEmpty(payment.getPaymentMedium()))
					response.gatewayType(payment.getGatewaytype());
				if (StringUtils.isNotEmpty(payment.getPaymentMedium()))
					response.ruleId(payment.getRuleId());
				if (StringUtils.isNotEmpty(payment.getPaymentSupplierId()))
					response.paymentSupplierId(payment.getPaymentSupplierId());
			}
			if (orderItem != null) {
				if (user == null)
					user = bookingUsers.get(order.getBookingUserId());
				constructAirOrderitemFields(orderItem, response, user);
			}
			if (order != null) {
				if (user == null)
					user = bookingUsers.get(order.getBookingUserId());
				constructOrderFields(order, bookingUsers, response);
			}
			if (user != null) {
				Set<User> userRelations = salesHierarchy.get(user.getUserId());
				response.paymentStatus("Success").bookingUserId(user.getUserId()).bookingUserName(user.getName())
						.bookingUserEmail(user.getEmail()).grade(user.getAdditionalInfo().getGrade())
						.bookingUserAccountcode(user.getAdditionalInfo().getAccountingCode())
						.toVat(user.getAdditionalInfo().getVatNumber())
						.toGst(user.getGstInfo() != null ? user.getGstInfo().getGstNumber() : "");
				if (user.getAddressInfo() != null && user.getAddressInfo().getCityInfo() != null) {
					response.bookingUserCity(user.getAddressInfo().getCityInfo().getName())
							.bookingUserState(user.getAddressInfo().getCityInfo().getState())
							.bookingUserCountry(user.getAddressInfo().getCityInfo().getCountry());
				}
				if (userRelations != null)
					response.salesHierachy(Joiner.on(';')
							.join(userRelations.stream().map(usr -> usr.getName()).collect(Collectors.toSet())));

			}
			if (amendment != null) {
				String invoiceId = null;
				if (UserRole.corporate(user.getRole())) {
					invoiceId = airAmendmentManager.getPaxWiseInvoiceId(amendment.toDomain(),
							orderItem.getFlightTravellerInfo());
				} else {
					invoiceId = amendment.getAdditionalInfo().getInvoiceId();
				}

				response.amendmentId(amendment.getAmendmentId())
						.amendmentType(StringUtils
								.capitalize(AmendmentType.getEnumFromCode(amendment.getAmendmentType()).name()))
						.amendmentDate(amendment.getProcessedOn().toString()).cartStatus("SUCCESS")
						.systemInvoiceId(invoiceId);
			}
			return response.build();
		} catch (Exception e) {
			log.info("Error occurred while converting row of booking id  {} ",
					orderItem != null ? orderItem.getBookingId() : amendment.getAmendmentId(), e);
		}
		return null;
	}

	private void constructAirOrderitemFields(ReportOrderItem orderItem, AirLineTransactionResponseBuilder response,
			User bookingUser) {
		AirportInfo destAirport = fmsCommunicator.getAirportInfo(orderItem.getDestination());
		AirportInfo sourceAirport = fmsCommunicator.getAirportInfo(orderItem.getSource());
		SupplierInfo supplierInfo = fmsCommunicator.getSupplierInfo(orderItem.getSupplierId());
		response.bookingId(orderItem.getBookingId()).flightNumber(orderItem.getFlightNumbers())
				.departureAirport(sourceAirport.getName()).departureCity(sourceAirport.getCity())
				.departureDate(orderItem.getDepartureTime().toLocalDate().toString())
				.departureTime(orderItem.getDepartureTime().toLocalTime().toString())
				.arrivalAirport(destAirport.getName()).arrivalCity(destAirport.getCity())
				.destCountry(destAirport.getCountry())
				.arrivalDate(orderItem.getArrivalDateTime().toLocalDate().toString())
				.arrivalTime(orderItem.getArrivalDateTime().toLocalTime().toString())
				.days(ChronoUnit.DAYS.between(LocalDateTime.now(), orderItem.getDepartureTime()))
				.supplier(orderItem.getSupplierId()).supplierCode(supplierInfo.getCredentialInfo().getAccountingCode())
				.airline(fmsCommunicator.getAirlineInfo(orderItem.getAirlinecode()).getName())
				.airlineCode(orderItem.getAirlinecode()).description(orderItem.getTripKey())
				.name(orderItem.getFlightTravellerInfo().getFullName())
				.ticketNumber(orderItem.getFlightTravellerInfo().getTicketNumber())
				.paxType(StringUtils.capitalize(orderItem.getFlightTravellerInfo().getPaxType().name().toLowerCase()))
				.gdsPnr(orderItem.getFlightTravellerInfo().getSupplierBookingId())
				.bookingClass(orderItem.getFlightTravellerInfo().getFareDetail().getClassOfBooking())
				.pnr(orderItem.getFlightTravellerInfo().getPnr()).tourCode(orderItem.getAdditionalInfo().getTourCode())
				.tourCode(orderItem.getAdditionalInfo().getTourCode())
				.passThrough(orderItem.getAdditionalInfo().getCcId())
				.farebasis(orderItem.getFlightTravellerInfo().getFareDetail().getFareBasis())
				.corporateCode(orderItem.getAdditionalInfo().getAccountCode())
				.protectionInvoiceNo(orderItem.getBookingId())
				.predefinedSource(orderItem.getAdditionalInfo().getOrgSupplierName() != null
						? orderItem.getAdditionalInfo().getOrgSupplierName()
						: orderItem.getSupplierId());
		if (orderItem.getAdditionalInfo().getType() != null) {
			response.tripType(AirType.getEnumFromCode(orderItem.getAdditionalInfo().getType()).name());
		} else {
			ProductMetaInfo productMetaInfo = Product.getProductMetaInfoFromId(orderItem.getBookingId());
			response.tripType(AirType.getEnumFromCode(productMetaInfo.getSubProduct()).name());
		}
		if (orderItem.getFlightTravellerInfo().getFareDetail().getCabinClass() != null)
			response.cabinClass(StringUtils
					.capitalize(orderItem.getFlightTravellerInfo().getFareDetail().getCabinClass().getName()));
		// ClientGeneralInfo clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		if (clientInfo.getProtectGroupConfiguration().getPremiumRate() != null) {
			response.protectionPremiumRate(clientInfo.getProtectGroupConfiguration().getPremiumRate());
		}
		Map<FareComponent, Double> fareComponents =
				orderItem.getFlightTravellerInfo().getFareDetail().getFareComponents();
		constuctFareComponentFields(response, fareComponents, orderItem.getFlightTravellerInfo().getStatus(),
				bookingUser);
		if (isATR() || isBTR()) {
			response.panNumber(orderItem.getFlightTravellerInfo().getPanNumber());
		}
		if (isDSR()) {
			try {
				response.systemInvoiceId(orderItem.getFlightTravellerInfo().getInvoice());
				AirItemDetail itemDetail = itemDetailMap.get(orderItem.getBookingId());
				String paxKey = orderItem.getFlightTravellerInfo().getPaxKey();
				response.employeeName(paxKey);
				List<GstInfo> gstInfos = gstInfoMap.get(orderItem.getBookingId());
				if (CollectionUtils.isNotEmpty(gstInfos)) {
					GstInfo gstInfo = gstInfos.get(0);
					response.beCompanyName(gstInfo.getRegisteredName());
					response.beGstNumber(gstInfo.getGstNumber());
					if (StringUtils.isNotEmpty(gstInfo.getAddress()))
						response.beAddress(gstInfo.getAddress().replace(",", ";"));
					response.beEmail(gstInfo.getEmail());
					response.bePhone(gstInfo.getMobile());
					response.accountingCode(gstInfo.getAccountCode());
				}
				response.userType(UserRole.CORPORATE.name());
				response.personalBooking("NO");
				PaxType paxType = orderItem.getFlightTravellerInfo().getPaxType();
				if (itemDetail == null)
					return;
				response.cfMap(setReportCustomFields(itemDetail.getInfo().getProfileData().get(paxKey)));
				List<SegmentInfo> lff = itemDetail.getInfo().getLff().get(orderItem.getId());
				response.reason(itemDetail.getInfo().getReason());
				if (CollectionUtils.isNotEmpty(lff)) {
					if (lff.size() > 0) {
						SegmentInfo segment = lff.get(0);
						FareDetail fareDetail = segment.getPriceInfo(0).getFareDetail(paxType);
						Double tf = fareDetail != null ? fareDetail.getFareComponents().get(FareComponent.TF) : 0D;
						response.lf1Fare(tf).lf1Number(segment.getAirlineCode(false) + "-" + segment.getFlightNumber())
								.lf1Date(segment.getDepartTime().toLocalDate().toString())
								.lf1Time(segment.getDepartTime().toLocalTime().toString());
						if (tf != null)
							response.lf1dev(fareComponents.getOrDefault(FareComponent.TF, 0D) - tf);
						if (tf != null)
							response.wasted(fareComponents.getOrDefault(FareComponent.TF, 0D) - tf);
					}
					if (lff.size() > 1) {
						SegmentInfo segment = lff.get(1);
						FareDetail fareDetail = segment.getPriceInfo(0).getFareDetail(paxType);
						Double tf = fareDetail != null ? fareDetail.getFareComponents().get(FareComponent.TF) : null;
						response.lf2Fare(tf).lf2Number(segment.getAirlineCode(false) + "-" + segment.getFlightNumber())
								.lf2Date(segment.getDepartTime().toLocalDate().toString())
								.lf2Time(segment.getDepartTime().toLocalTime().toString());
						if (tf != null)
							response.lf2dev(fareComponents.getOrDefault(FareComponent.TF, 0D) - tf);
					}
					if (lff.size() > 2) {
						SegmentInfo segment = lff.get(2);
						FareDetail fareDetail = segment.getPriceInfo(0).getFareDetail(paxType);
						Double tf = fareDetail != null ? fareDetail.getFareComponents().get(FareComponent.TF) : null;
						response.lf3Fare(tf).lf3Number(segment.getAirlineCode(false) + "-" + segment.getFlightNumber())
								.lf3Date(segment.getDepartTime().toLocalDate().toString())
								.lf3Time(segment.getDepartTime().toLocalTime().toString());
						if (tf != null)
							response.lf3dev(fareComponents.getOrDefault(FareComponent.TF, 0D) - tf);
					}
				}
			} catch (Exception ex) {
				log.info(
						"DSR Exception: {} => Error while setting customField/LFF for booking id {} and airorderitem id {} ",
						parentUser.getUserId(), orderItem.getBookingId(), orderItem.getId(), ex);
			}
		}
	}


	public PaymentConfigurationRule getPaymentRule(Integer ruleId) {
		PaymentConfigurationRule rule = null;
		if (ruleId != null) {
			rule = PaymentConfigurationHelper.getPaymentRule(ruleId, PaymentRuleType.PAYMENT_MEDIUM);
			if (rule == null) {
				rule = PaymentConfigurationHelper.getPaymentRule(ruleId, PaymentRuleType.EXTERNAL_PAYMENT_MEDIUM);
			}
		}
		return rule;
	}

	private void constuctFareComponentFields(AirLineTransactionResponseBuilder response,
			Map<FareComponent, Double> fareComponents, TravellerStatus paxStatus, User bookingUser) {
		User user = SystemContextHolder.getContextData().getUser();
		// in case of admin roles they will not see bifurcation of partner components
		boolean isParentOffice = user != null && UserRole.getMidOfficeRoles().contains(user.getRole());
		Double taxableFare =
				fareComponents.getOrDefault(FareComponent.BF, 0.0) + fareComponents.getOrDefault(FareComponent.YQ, 0.0)
						+ fareComponents.getOrDefault(FareComponent.YR, 0.0);
		double totalCommission = 0;
		for (Entry<FareComponent, Double> entry : fareComponents.entrySet()) {
			if (FareComponent.getCommissionComponents().contains(entry.getKey())) {
				totalCommission += entry.getValue();
			}
			if (isParentOffice && FareComponent.getPartnerCommissionComponents().contains(entry.getKey())) {
				totalCommission += entry.getValue();
			}
		}
		constructClientFeesAndTaxes(fareComponents, bookingUser, response);
		constructCancelledFareFields(response, fareComponents, paxStatus);
		response.yQ(fareComponents.get(FareComponent.YQ)).yR(fareComponents.get(FareComponent.YR))
				.taf(fareComponents.get(FareComponent.TAF)).taxable(taxableFare)
				.rcf(fareComponents.get(FareComponent.RCF)).psf(fareComponents.get(FareComponent.PSF))
				.udf(fareComponents.get(FareComponent.UDF)).ob(fareComponents.get(FareComponent.OB))
				.oc(fareComponents.get(FareComponent.OC)).tds(fareComponents.get(FareComponent.TDS))
				.partnerTds(fareComponents.getOrDefault(FareComponent.PCTDS, 0.0)
						+ fareComponents.getOrDefault(FareComponent.PMTDS, 0.0))
				.grossDiscount(totalCommission)
				.acf(fareComponents.getOrDefault(FareComponent.ACF, 0.0)
						+ fareComponents.getOrDefault(FareComponent.ARF, 0.0)
						+ fareComponents.getOrDefault(FareComponent.CAMU, 0.0))
				.airlineGst(fareComponents.get(FareComponent.AGST))
				.netFare(fareComponents.getOrDefault(FareComponent.TF, 0.0) - totalCommission
						+ fareComponents.getOrDefault(FareComponent.TDS, 0.0))
				.netFareWithMarkup(fareComponents.getOrDefault(FareComponent.TF, 0.0)
						+ fareComponents.getOrDefault(FareComponent.MU, 0.0))
				.netDiscount(getNetDiscount(isParentOffice, totalCommission, fareComponents))
				.cmu(fareComponents.getOrDefault(FareComponent.CMU, 0.0)
						+ fareComponents.getOrDefault(FareComponent.XT, 0.0))
				.grossFare(fareComponents.get(FareComponent.TF)).fromGst(getClientGst())
				.amendMentMarkup(fareComponents.get(FareComponent.CAMU)).markup(fareComponents.get(FareComponent.MU))
				.fromVat(getClientVat()).otherTaxes(getOtherFareComponents(fareComponents))
				.baseFare(fareComponents.get(FareComponent.BF)).totalFare(fareComponents.get(FareComponent.TF))
				.paymentFee(fareComponents.get(FareComponent.PF))
				.creditShellAmount(fareComponents.getOrDefault(FareComponent.CS, 0.0))
				.partnerMarkup(getPartnerMarkUp(isParentOffice, fareComponents))
				.partnerCommission(getPartnerCommission(isParentOffice, fareComponents))
				.pointsDiscount(fareComponents.getOrDefault(FareComponent.RP, 0.0))
				.voucherDiscount(fareComponents.getOrDefault(FareComponent.VD, 0.0))
				.ugst(fareComponents.getOrDefault(FareComponent.UGST, 0.0))
				.cancellationProtectionAmount(fareComponents.getOrDefault(FareComponent.CPA, 0.0))
				.protectionCharges(fareComponents.getOrDefault(FareComponent.CPP, 0.0))
				.protectionGSTCharges(fareComponents.getOrDefault(FareComponent.CPT, 0.0))
				.protectionManagementFee(fareComponents.getOrDefault(FareComponent.CPMF, 0.0))
				.protectionManagementFeeTax(fareComponents.getOrDefault(FareComponent.CPMFT, 0.0))
				.protectionCommission(fareComponents.getOrDefault(FareComponent.CPAC, 0.0))
				.protectionCommissionTds(fareComponents.getOrDefault(FareComponent.CPACT, 0.0))
				.xt(fareComponents.getOrDefault(FareComponent.XT, 0.0))
				.partedxt(fareComponents.getOrDefault(FareComponent.PFCM, 0.0));

		double totalProtectionCharges = fareComponents.getOrDefault(FareComponent.CPP, 0.0)
				+ fareComponents.getOrDefault(FareComponent.CPT, 0.0)
				+ fareComponents.getOrDefault(FareComponent.CPMF, 0.0)
				+ fareComponents.getOrDefault(FareComponent.CPMFT, 0.0)
				+ fareComponents.getOrDefault(FareComponent.CPAC, 0.0);
		response.totalProtectionCharges(totalProtectionCharges);
		boolean igstApplicable = igstMap.get(bookingUser.getUserId());
		double cpmft = fareComponents.getOrDefault(FareComponent.CPMFT, 0.0);
		if (igstApplicable)
			response.protectionManagementFeeTaxIgst(cpmft);
		else {
			response.protectionManagementFeeTaxCgst(cpmft / 2);
			response.protectionManagementFeeTaxSgst(cpmft / 2);
		}
		if (isParentOffice) {
			// override previous values
			Double totalTds = fareComponents.getOrDefault(FareComponent.TDS, 0d)
					+ fareComponents.getOrDefault(FareComponent.PCTDS, 0d)
					+ fareComponents.getOrDefault(FareComponent.PMTDS, 0d);
			response.tds(totalTds);
			response.netFare(fareComponents.getOrDefault(FareComponent.TF, 0.0)
					- fareComponents.getOrDefault(FareComponent.CPAC, 0.0)
					+ fareComponents.getOrDefault(FareComponent.CPACT, 0.0) - totalCommission + totalTds);
			response.netDiscount(totalCommission - totalTds);
		}
	}

	// constructing client fees and taxes. if taxes (ccft, crft, caft) are zero then calculating it from ccf, crf or caf
	// using (ccf * 0.18) / 1.18

	private void constructClientFeesAndTaxes(Map<FareComponent, Double> fareComponents, User bookingUser,
			AirLineTransactionResponseBuilder response) {
		boolean igstApplicable = true;
		if (igstMap.get(bookingUser.getUserId()) == null) {
			igstMap.put(bookingUser.getUserId(), OmsHelper.isIgstApplicable(bookingUser));
		}
		igstApplicable = igstMap.get(bookingUser.getUserId());
		double igst = fareComponents.getOrDefault(FareComponent.IGST, 0.0);
		double sgst = fareComponents.getOrDefault(FareComponent.SGST, 0.0);
		double cgst = fareComponents.getOrDefault(FareComponent.CGST, 0.0);
		double totalGST = fareComponents.getOrDefault(FareComponent.CCFT, 0.0)
				+ fareComponents.getOrDefault(FareComponent.CAFT, 0.0)
				+ fareComponents.getOrDefault(FareComponent.CRFT, 0.0);
		if (igst == 0.0 && cgst == 0.0 && sgst == 0.0) {
			if (totalGST == 0) {
				double ccf = fareComponents.getOrDefault(FareComponent.CCF, 0.0);
				double crf = fareComponents.getOrDefault(FareComponent.CRF, 0.0);
				double caf = fareComponents.getOrDefault(FareComponent.CAF, 0.0);
				double ccft = fareComponents.getOrDefault(FareComponent.CCF, 0.0) == 0 ? 0 : (ccf * 0.18) / 1.18;
				double crft = fareComponents.getOrDefault(FareComponent.CRF, 0.0) == 0 ? 0 : (crf * 0.18) / 1.18;
				double caft = fareComponents.getOrDefault(FareComponent.CAF, 0.0) == 0 ? 0 : (caf * 0.18) / 1.18;
				totalGST = ccft + crft + caft;
				fareComponents.put(FareComponent.CCF, ccf - ccft);
				fareComponents.put(FareComponent.CAF, caf - caft);
				fareComponents.put(FareComponent.CRF, crf - crft);
			}
			if (igstApplicable) {
				igst = totalGST;
			} else {
				sgst = totalGST / 2;
				cgst = totalGST / 2;
			}
		}
		response.ccf(fareComponents.getOrDefault(FareComponent.CCF, 0.0)
				+ fareComponents.getOrDefault(FareComponent.CRF, 0.0))
				.caf(fareComponents.getOrDefault(FareComponent.CAF, 0.0)).cgst(cgst).igst(igst).sgst(sgst)
				.amendmentGst(totalGST);
	}

	private void constructCancelledFareFields(AirLineTransactionResponseBuilder response,
			Map<FareComponent, Double> fareComponents, TravellerStatus paxStatus) {
		Double managementFee =
				paxStatus != null && paxStatus.isCancelled() ? 0.0 : fareComponents.getOrDefault(FareComponent.MF, 0.0);
		Double managementGstFee = paxStatus != null && paxStatus.isCancelled() ? 0.0
				: fareComponents.getOrDefault(FareComponent.MFT, 0.0);
		response.managementFee(managementFee).managementGst(managementGstFee);
	}

	private void constructCancelledFareComponentFelds(AirLineTransactionResponseBuilder response,
			Map<FareComponent, Double> fareComponents, TravellerStatus paxStatus) {
		Double managementFee =
				paxStatus != null && paxStatus.isCancelled() ? 0.0 : fareComponents.getOrDefault(FareComponent.MF, 0.0);
		Double managementGstFee = paxStatus != null && paxStatus.isCancelled() ? 0.0
				: fareComponents.getOrDefault(FareComponent.MFT, 0.0);
		Double cgst = paxStatus != null && paxStatus.isCancelled() ? 0.0
				: fareComponents.getOrDefault(FareComponent.CGST, 0.0);
		Double sgst = paxStatus != null && paxStatus.isCancelled() ? 0.0
				: fareComponents.getOrDefault(FareComponent.SGST, 0.0);
		Double igst = paxStatus != null && paxStatus.isCancelled() ? 0.0
				: fareComponents.getOrDefault(FareComponent.IGST, 0.0);
		Double ugst = paxStatus != null && paxStatus.isCancelled() ? 0.0
				: fareComponents.getOrDefault(FareComponent.UGST, 0.0);

		response.managementFee(managementFee).managementGst(managementGstFee).cgst(cgst).sgst(sgst).igst(igst)
				.ugst(ugst);
	}

	protected Double getNetDiscount(boolean isParentOffice, Double totalCommission,
			Map<FareComponent, Double> fareComponents) {

		totalCommission = totalCommission - (fareComponents.getOrDefault(FareComponent.TDS, 0.0)
				+ fareComponents.getOrDefault(FareComponent.PCTDS, 0.0)
				+ fareComponents.getOrDefault(FareComponent.PMTDS, 0.0));
		return totalCommission;
	}

	private Double getPartnerCommission(boolean isPartnerCommReq, Map<FareComponent, Double> fareComponents) {
		Double partnerComm = new Double(0);
		if (!isPartnerCommReq) {
			for (FareComponent component : fareComponents.keySet()) {
				if (FareComponent.getPartnerCommissionComponents().contains(component)) {
					partnerComm += component.getAmount(fareComponents.get(component));
				}
			}
		}
		return partnerComm;
	}

	private Double getPartnerMarkUp(boolean isParentOffice, Map<FareComponent, Double> fareComponents) {
		return isParentOffice ? 0d : fareComponents.getOrDefault(FareComponent.PMU, 0.0);
	}

	private void constructOrderFields(DbOrder order, Map<String, User> bookingUsers,
			AirLineTransactionResponseBuilder response) {
		String bookingType =
				order.getAdditionalInfo().getFlowType() != null ? order.getAdditionalInfo().getFlowType().getName()
						: "AUTOMATED";
		String invoiceStatus = order.getAdditionalInfo().getInvoiceId() != null ? "Success" : "Pending";
		response.bookingType(bookingType)
				.systemInvoiceId(response.build().getSystemInvoiceId() != null ? response.build().getSystemInvoiceId()
						: order.getAdditionalInfo().getInvoiceId())
				.invoiceStatus(invoiceStatus).cartGenerationDate(order.getCreatedOn().toLocalDate().toString())
				.tripId(order.getAdditionalInfo().getTripId())
				.cartGenerationTime(order.getCreatedOn().toLocalTime().toString())
				.assignedUserId(order.getAdditionalInfo().getAssignedUserId())
				.channel(ChannelType.getNameFromCode(order.getChannelType()))
				.voucherCode(order.getAdditionalInfo().getVoucherCode())
				.subProductInvoiceIds(order.getAdditionalInfo().getProtectGroupInvoiceInfo().getInvoiceId())
				.subProductInvoiceStatus(
						order.getAdditionalInfo().getProtectGroupInvoiceInfo().isInvoicePushStatus() + "");

		OrderStatus orderStatus = OrderStatus.getEnumFromCode(order.getStatus());
		if (orderStatus != null)
			response.cartStatus(orderStatus.name());

		if (order.getAdditionalInfo().getFirstUpdateTime() != null) {
			LocalDateTime bookingTime = order.getAdditionalInfo().getFirstUpdateTime().get("S");
			if (bookingTime != null) {
				response.bookingDate(bookingTime.toLocalDate().toString())
						.bookingTime(bookingTime.toLocalTime().toString());

			} else {
				response.bookingDate(order.getCreatedOn().toLocalTime().toString())
						.bookingTime(order.getCreatedOn().toLocalTime().toString());
			}
		}
		if (order.getAdditionalInfo().getAssignedUserId() != null) {
			try {
				if (bookingUsers.get(order.getAdditionalInfo().getAssignedUserId()) != null) {
					response.assignedUserName(
							bookingUsers.get(order.getAdditionalInfo().getAssignedUserId()).getName());
					response.bookingEmployeeName(
							bookingUsers.get(order.getAdditionalInfo().getAssignedUserId()).getName());
				} else {
					User assignee = getUserFromMap(order.getAdditionalInfo().getAssignedUserId());
					if (assignee != null) {
						bookingUsers.put(order.getAdditionalInfo().getAssignedUserId(), assignee);
						response.assignedUserName(assignee.getName());
						response.bookingEmployeeName(assignee.getName());
					}
				}
			} catch (Exception e) {
				log.info("Error occurred while fetching user name for id {}",
						order.getAdditionalInfo().getAssignedUserId(), e);
			}
		}
		try {
			if (bookingUsers.get(order.getLoggedInUserId()) != null) {
				response.loggedInUserName(bookingUsers.get(order.getLoggedInUserId()).getName());
			} else {
				User loggedInUser = getUserFromMap(order.getLoggedInUserId());
				bookingUsers.put(order.getLoggedInUserId(), loggedInUser);
				response.loggedInUserName(loggedInUser.getName());
			}

		} catch (Exception e) {
			log.info("Error occurred while fetching user name for id {},bookingId {}",
					order.getAdditionalInfo().getAssignedUserId(), order.getBookingId(), e);
		}
	}

	private double getOtherFareComponents(Map<FareComponent, Double> fareComponents) {
		return fareComponents.getOrDefault(FareComponent.WO, 0.0) + fareComponents.getOrDefault(FareComponent.WC, 0.0)
				+ fareComponents.getOrDefault(FareComponent.YM, 0.0)
				+ fareComponents.getOrDefault(FareComponent.XT, 0.0)
				+ fareComponents.getOrDefault(FareComponent.CMU, 0.0)
				+ fareComponents.getOrDefault(FareComponent.AT, 0.0)
				+ fareComponents.getOrDefault(FareComponent.SP, 0.0)
				+ fareComponents.getOrDefault(FareComponent.BP, 0.0)
				+ fareComponents.getOrDefault(FareComponent.MP, 0.0)
				+ fareComponents.getOrDefault(FareComponent.SBC, 0.0);
	}

	public String getClientGst() {
		if (StringUtils.isEmpty(clientGst)) {
			ClientGeneralInfo clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
					GeneralBasicFact.builder().applicableTime(request.getCreatedOnBeforeDateTime()).build());
			if (clientInfo != null && clientInfo.getGstInfo() != null)
				clientGst = clientInfo.getGstInfo().getGstNumber();
		}
		return clientGst;
	}

	public String getClientVat() {
		if (StringUtils.isEmpty(clientVat)) {
			ClientGeneralInfo clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
					GeneralBasicFact.builder().applicableTime(request.getCreatedOnBeforeDateTime()).build());
			if (clientInfo != null)
				clientVat = clientInfo.getVatNumber() == null ? "NA" : clientInfo.getVatNumber();
		}
		return clientVat;
	}

	protected List<String> getUserIds() {
		List<String> userIds = request.getQueryFields() != null ? (List) request.getQueryFields().get("userIds")
				: Collections.emptyList();
		List<String> allowedUserIds = UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), userIds);
		return allowedUserIds;
	}

	protected int getMonthsToScanForAmendments() {
		List<String> userIds = getUserIds();
		if (CollectionUtils.isEmpty(userIds)) {
			userIds = new ArrayList<>();
			userIds.add(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
		}
		int months = CollectionUtils.isNotEmpty(userIds)
				&& (clientInfo.getReportUsersAllowedforBiggerScan().stream().anyMatch(userIds::contains)) ? 12 : 3;
		log.info("Months to scan is {} ", months);
		return months;
	}

	protected List<String> getUserRoles() {
		return request.getQueryFields() != null ? (List<String>) request.getQueryFields().get("roles")
				: new ArrayList<>();
	}

	protected StringBuilder constructAmendmentQuery(Set<String> bookingids, LocalDateTime createdBeforeDateTime,
			LocalDateTime createdAfterDateTime) {
		StringBuilder amendmentQuery = new StringBuilder(
				"SELECT * from  (select *, rank() OVER (PARTITION BY bookingid ORDER BY processedon ASC) AS pos "
						+ "FROM amendment where status = 'S' ");
		if (CollectionUtils.isNotEmpty(bookingids))
			amendmentQuery.append("and bookingid in ('").append(Joiner.on("','").join(bookingids)).append("')");
		amendmentQuery.append(" and createdon >= '").append(createdAfterDateTime.toString()).append("'");
		amendmentQuery.append(" and createdon <= '").append(createdBeforeDateTime.toString()).append("'");
		amendmentQuery.append(" order by processedon asc) a WHERE pos= 1;");
		return amendmentQuery;
	}

	protected StringBuilder constructAirDetailQuery(Set<String> bookingIds, String createdBefore, String createdAfter) {
		StringBuilder query = new StringBuilder("SELECT * FROM airitemdetail WHERE ");
		query.append("bookingid in ('").append(Joiner.on("','").join(bookingIds)).append("')");
		if (createdAfter != null)
			query.append(" and createdon >= '").append(createdAfter).append("'");
		if (createdBefore != null)
			query.append(" and createdon <= '").append(createdBefore).append("';");
		return query;
	}

	protected StringBuilder constructAmendmentQuery(LocalDateTime createdOnAfterForAllDbQueries,
			LocalDateTime createdOnBeforeForAllDbQueries, Set<String> totalAmendments) {
		StringBuilder amendMentQuery = new StringBuilder(
				"SELECT {a.*},{b.*} from amendment a LEFT JOIN orders b ON a.bookingid = b.bookingid ")
						.append(" and a.createdon >= '").append(createdOnAfterForAllDbQueries.toString()).append("'")
						.append(" and a.createdon <= '").append(createdOnBeforeForAllDbQueries.toString()).append("'")
						.append(" and b.createdon >= '")
						.append(createdOnAfterForAllDbQueries.minusMonths(getMonthsToScanForAmendments()).toString())
						.append("'").append(" and b.createdon <= '").append(createdOnBeforeForAllDbQueries.toString())
						.append("' where a.status = 'S' and b.ordertype ='A' and a.amendmentId IN ('")
						.append(Joiner.on("','").join(totalAmendments)).append("');");
		log.debug("Amendment query is {}", amendMentQuery);
		return amendMentQuery;
	}

	protected Map<String, DbOrder> constructOrdersMap(Set<String> totalBookingIds, EntityManager em,
			LocalDateTime createdOnBeforeDateTime, LocalDateTime createdOnAfterDateTime) {
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		StringBuilder orderQuery = new StringBuilder("SELECT * from orders where bookingId IN ('")
				.append(Joiner.on("','").join(totalBookingIds)).append("') ").append(" and createdon >= '")
				.append(createdOnAfterDateTime.toString()).append("'").append(" and createdon <= '")
				.append(createdOnBeforeDateTime.toString()).append("';");
		List<DbOrder> orders = entityManager.unwrap(Session.class).createNativeQuery(orderQuery.toString())
				.addEntity(DbOrder.class).getResultList();
		entityManager.close();
		return orders.stream().collect(Collectors.toMap(DbOrder::getBookingId, o -> o));
	}

	protected List<AirLineTransactionResponse> sortResponse(List<AirLineTransactionResponse> response) {
		LogUtils.log(REPORT_SORT_START, SystemContextHolder.getContextData().getLogMetaInfo(), null);
		Comparator<AirLineTransactionResponse> comparator = getComparatorForSorting();
		response = response.stream().sorted(comparator).collect(Collectors.toList());
		LogUtils.log(REPORT_SORT_END,
				SystemContextHolder.getContextData().getLogMetaInfo().setNoOfEntries(CollectionUtils.size(response)),
				REPORT_SORT_START);
		return response;
	}

	protected Comparator<AirLineTransactionResponse> getComparatorForSorting() {
		return Comparator
				.comparing(AirLineTransactionResponse::getCartGenerationDate,
						Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(AirLineTransactionResponse::getCartGenerationTime,
						Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(AirLineTransactionResponse::getBookingId,
						Comparator.nullsLast(Comparator.naturalOrder()));
	}

	protected Map<String, Object> setReportCustomFields(Map<String, Object> customFieldsMap) {
		Map<String, Object> reportFields = new LinkedTreeMap<>();
		if (MapUtils.isNotEmpty(visibleCustomFields) && MapUtils.isNotEmpty(customFieldsMap)) {
			customFieldsMap.forEach((key, value) -> {
				if (visibleCustomFields.containsKey(key)) {
					reportFields.put(visibleCustomFields.get(key), value);
				}
			});
		}
		return reportFields;
	}

	protected Map<String, String> getVisibleFields() {
		Map<String, String> map = new HashMap<>();
		if (profileMeta != null) {
			profileMeta.getFields().forEach(f -> {
				if (f.isVReports())
					map.put(f.getName(), StringUtils.isBlank(f.getDisplayLabel()) ? f.getName() : f.getDisplayLabel());
			});
		}
		return map;
	}

	protected abstract String getCollDocTypeForFetchingFields();

	protected Class<?> getResponseClass() {
		return AirLineTransactionResponse.class;
	}

	protected void setItemDetailMap(Set<String> bookingids, LocalDateTime createdBeforeDateTime,
			LocalDateTime createdAfterDateTime, EntityManager em) {
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		StringBuilder airItemDetailQuery = new StringBuilder("SELECT * from airitemdetail where bookingId IN ('")
				.append(Joiner.on("','").join(bookingids)).append("') ").append(" and createdon >= '")
				.append(createdAfterDateTime.toString()).append("'").append(" and createdon <= '")
				.append(createdBeforeDateTime.toString()).append("';");
		List<DbAirItemDetail> airItemDetail = entityManager.unwrap(Session.class)
				.createNativeQuery(airItemDetailQuery.toString()).addEntity(DbAirItemDetail.class).getResultList();
		entityManager.close();
		airItemDetail.forEach(itemDetail -> {
			AirItemDetail item = itemDetail.toDomain();
			itemDetailMap.put(item.getBookingId(), item);
		});
	}

	protected StringBuilder constructPaymentQuery() {
		StringBuilder paymentQuery = new StringBuilder(
				"SELECT amendmentid,refid,paymentmedium,payUserId,merchantTxnId FROM payment where status = 'S' and createdOn>='")
						.append(request.getCreatedOnAfterDateTime().toString()).append("' and createdOn<='")
						.append(request.getCreatedOnBeforeDateTime().toString()).append("'");
		List<String> allowedUserIds = getUserIds();
		if (CollectionUtils.isNotEmpty(allowedUserIds))
			paymentQuery.append("and payUserId IN ('").append(Joiner.on("','").join(allowedUserIds)).append("')");
		paymentQuery.append(";");
		return paymentQuery;
	}

	protected void filterPaymentByRoles(Map<String, ProcessedPayment> amendmentPaymentMap,
			Map<String, ProcessedPayment> bookingPaymentMap) {
		List<String> allowedRoles = getUserRoles();
		if (CollectionUtils.isNotEmpty(allowedRoles)) {

			if (MapUtils.isNotEmpty(amendmentPaymentMap)) {
				amendmentPaymentMap.entrySet().removeIf(
						entry -> (!allowedRoles.contains(bookingUsers.get(entry.getValue().getUserId()) != null
								? bookingUsers.get(entry.getValue().getUserId()).getRole().getRoleDisplayName()
								: StringUtils.EMPTY)));
			}

			if (MapUtils.isNotEmpty(bookingPaymentMap)) {
				bookingPaymentMap.entrySet().removeIf(
						entry -> (!allowedRoles.contains(bookingUsers.get(entry.getValue().getUserId()) != null
								? bookingUsers.get(entry.getValue().getUserId()).getRole().getRoleDisplayName()
								: StringUtils.EMPTY)));
			}
		}
	}

	private User getUserFromMap(String userId) {
		User user = usersMap.get(userId);
		if (user == null) {
			user = userCommunicator.getUserFromCache(userId);
			usersMap.put(user.getUserId(), user);
		}
		return user;
	}
}
