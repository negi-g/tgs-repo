package helper;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import com.tgs.services.base.helper.ReportParam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AmexReportUtil {

	public static String getFieldValue(Object response) {
		return getFieldValue(response,818);
	}
	
	public static String getFieldValue(Object response, int length) {
		StringBuilder rowData = new StringBuilder(length);
		List<Field> fields = Arrays.asList(response.getClass().getDeclaredFields());
		for (Field field : fields) {
			field.setAccessible(true);
			ReportParam reportParam = field.getAnnotation(ReportParam.class);
			String value;
			try {
				if(field.get(response)!=null)
					value = String.valueOf(field.get(response));
				else
					value = null;
				if (reportParam != null) {
					String strFinal = getStringToBeInserted(value, reportParam);
					rowData.insert(reportParam.beginIndex() - 1, strFinal);
				}
			} catch (Exception e) {
				log.error("Error occured while fetching value of field {} in class {}", field.getName(),
						response.getClass().getName());
			}
		}
		return rowData.toString();
	}

	public static String getStringToBeInserted(String str, ReportParam reportParam) {
		str = str == null ? new String("") : str;
		if (reportParam.isFiller()) {
			return getFillerString(reportParam.length(), reportParam.isNumeric());
		} else if (str.length() != reportParam.length()) {
			int difference = reportParam.length() - str.length();
			if (difference > 0) {
				String filler = getFillerString(difference, reportParam.isNumeric());
				if (reportParam.isNumeric())
					str = filler.concat(str);
				else
					str = str.concat(filler);
			} else {
				str = str.substring(0, reportParam.length());
			}
		}
		return str;
	}

	public static String getFillerString(int length, boolean isNumeric) {
		char[] fillers = new char[length];
		if (isNumeric) {
			Arrays.fill(fillers, '0');
		} else {
			Arrays.fill(fillers, ' ');
		}
		return new String(fillers);
	}
}

