package helper;

import java.time.LocalDateTime;

import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportOrderItem extends DbAirOrderItem {

	private LocalDateTime arrivalDateTime;

	private String destination;
	
	private String tripKey;
	
	private FlightTravellerInfo flightTravellerInfo;
	
	private String flightNumbers;
	
}
