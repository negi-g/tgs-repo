package com.tgs.services.base.runtime.database.CustomTypes;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.datamodel.TravellerInfo;

public class TravellerInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		List<TravellerInfo> travellerList = new ArrayList<>();
		return travellerList.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<TravellerInfo>>() {}.getType();
	}

}
