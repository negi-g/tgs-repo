package com.tgs.services.base.utils;

public class LogTypes {

	public static final String SYSTEM_FILTER = "system#filter";

	public static final String USERDBSTART = "user#db#start";
	public static final String USERDBEND = "user#db#end";
	public static final String AIRSEARCH = "airsearch";

	public static final String AIRSEARCH_RESPONSE_START = "airsearch#parseresponse#start";
	public static final String AIRSEARCH_RESPONSE_END = "airsearch#parseresponse#end";


	public static final String AIRSEARCH_DOSEARCH = "airsearch#dosearch";
	public static final String AIRSEARCH_DOSEARCH_END = "airsearch#dosearch#end";
	public static final String AIRSEARCH_PROCESS = "airsearch#process";
	public static final String AIRSEARCH_STORETRIPKEY = "airsearch#storetripkey";
	public static final String AIRSEARCH_CACHEFLIGHTLISTING = "airsearch#cacheflightlisting";
	public static final String AIRSEARCH_STORE = "airsearch#store";
	public static final String AIR_SUPPLIER_SEARCH_START = "air#supplier#search#start";
	public static final String AIR_SUPPLIER_SEARCH_END = "air#supplier#search#end";

	public static final String AIR_REVIEW_REVIEWTRIP = "airreview#reviewtrip";
	public static final String AIR_REVIEW_SUPPLIERBEGIN = "airreview#supplier#begin";
	public static final String AIR_REVIEW_SUPPLIEREND = "airreview#supplier#end";

	public static final String AIR_SUPPLIER_CHANGE_CLASS_SEARCH_START = "air#supplier#changeclasssearch#start";
	public static final String AIR_SUPPLIER_CHANGE_CLASS_END_START = "air#supplier#changeclassend#end";

	public static final String AIR_SUPPLIER_CHANGE_CLASS_FARE_SEARCH_START = "air#supplier#changeclasssearch#start";
	public static final String AIR_SUPPLIER_CHANGE_CLASS_FARE_END_START = "air#supplier#changeclassend#end";

	/*
	 * Hotel Logs
	 */

	public static final String HOTEL_SUPPLIER_SEARCH_START = "hotel#supplier#search#start";
	public static final String HOTEL_SUPPLIER_SEARCH_END = "hotel#supplier#search#end";
	public static final String HOTELSEARCH_PROCESS_START = "hotelsearch#process#start";
	public static final String HOTELSEARCH_PROCESS_END = "hotelsearch#process#end";


	public static final String HOTEL_SINGLE_SUPPLIER_PROCESS_START = "process#singlesupplier#start";
	public static final String HOTEL_SINGLE_SUPPLIER_PROCESS_END = "process#singlesupplier#end";

	public static final String HOTEL_PROCESS_OPTIONS_PROCESS_START = "process#singlesupplier#options#start";
	public static final String HOTEL_PROCESS_OPTIONS_PROCESS_END = "process#singlesupplier#options#end";

	public static final String HOTEL_APPLY_PROPERTY_TYPE_PROCESS_START =
			"process#singlesupplier#applypropertytype#start";
	public static final String HOTEL_APPLY_PROPERTY_TYPE_PROCESS_END = "process#singlesupplier#applypropertytype#end";

	public static final String HOTEL_APPLY_PROMOTION_PROCESS_START = "process#singlesupplier#applypromotion#start";
	public static final String HOTEL_APPLY_PROMOTION_PROCESS_END = "process#singlesupplier#applypromotion#end";

	public static final String HOTEL_COMBINE_PREVIOUS_RESULT_PROCESS_START =
			"process#singlesupplier#combinepreviousresult#start";
	public static final String HOTEL_COMBINE_PREVIOUS_RESULT_PROCESS_END =
			"process#singlesupplier#combinepreviousresult#end";

	public static final String HOTEL_AGGREGATE_HOTEL_PROCESS_START = "process#singlesupplier#aggregatehotel#start";
	public static final String HOTEL_AGGREGATE_HOTEL_PROCESS_END = "process#singlesupplier#aggregatehotel#end";

	public static final String HOTEL_AGGREGATE_BY_UNIQUE_KEY_HOTEL_PROCESS_START =
			"process#singlesupplier#aggregatebyuniqueykey#start";
	public static final String HOTEL_AGGREGATE_BY_UNIQUE_KEY_HOTEL_PROCESS_END =
			"process#singlesupplier#aggregatebyuniqueykey#end";

	public static final String HOTEL_AGGREGATE_BY_CONSTRAINT_HOTEL_PROCESS_START =
			"process#singlesupplier#aggregatebyconstraint#start";
	public static final String HOTEL_AGGREGATE_BY_CONSTRAINT_HOTEL_PROCESS_END =
			"process#singlesupplier#aggregatebyconstraint#end";

	public static final String HOTEL_MERGE_OPTIONS_PROCESS_START = "process#singlesupplier#mergeoptions#start";
	public static final String HOTEL_MERGE_OPTIONS_PROCESS_END = "process#singlesupplier#mergeoptions#end";

	public static final String HOTEL_MERGE_SORTED_OPTIONS_PROCESS_START =
			"process#singlesupplier#mergesortedoptions#start";
	public static final String HOTEL_MERGE_SORTED_OPTIONS_PROCESS_END = "process#singlesupplier#mergesortedoptions#end";

	public static final String HOTEL_BUILD_PROCESSED_OPTIONS_PROCESS_START =
			"process#singlesupplier#buildprocessedoptions#start";
	public static final String HOTEL_BUILD_PROCESSED_OPTIONS_PROCESS_END =
			"process#singlesupplier#buildprocessedoptions#end";

	public static final String HOTEL_APPLY_COMMERCIAL_PROCESS_START = "process#singlesupplier#applycommercial#start";
	public static final String HOTEL_APPLY_COMMERCIAL_PROCESS_END = "process#singlesupplier#applycommercial#end";

	public static final String HOTEL_POST_APPLY_COMMERCIAL_PROCESS_START =
			"process#singlesupplier#postapplycommercial#start";
	public static final String HOTEL_POST_APPLY_COMMERCIAL_PROCESS_END =
			"process#singlesupplier#postapplycommercial#end";

	/*
	 * Hotel Cache Related Logs
	 */

	public static final String HOTEL_PROCESS_AND_STORE_IN_CACHE_PROCESS_START =
			"process#cache#processandstore#singlesupplier#start";
	public static final String HOTEL_PROCESS_AND_STORE_IN_CACHE_PROCESS_END =
			"process#cache#processandstore#singlesupplier#end";

	public static final String HOTEL_FETCH_SEARCH_RESULT_IN_CACHE_PROCESS_START =
			"process#cache#fetchsearchresult#singlesupplier#start";
	public static final String HOTEL_FETCH_SEARCH_RESULT_IN_CACHE_PROCESS_END =
			"process#cache#fetchsearchresult#singlesupplier#end";

	public static final String HOTEL_STORE_SEARCH_RESULT_IN_CACHE_PROCESS_START =
			"process#cache#storesearchresult#singlesupplier#start";
	public static final String HOTEL_STORE_SEARCH_RESULT_IN_CACHE_PROCESS_END =
			"process#cache#storesearchresult#singlesupplier#end";

	public static final String HOTEL_FETCH_MEAL_SUPPLIER_PROCESS_START = "process#cache#meal#singlesupplier#start";
	public static final String HOTEL_FETCH_MEAL_SUPPLIER_PROCESS_END = "process#cache#meal#singlesupplier#end";

	public static final String HOTEL_FETCH_STATIC_DATA_SUPPLIER_PROCESS_START =
			"process#cache#staticdata#singlesupplier#start";
	public static final String HOTEL_FETCH_STATIC_DATA_SUPPLIER_PROCESS_END =
			"process#cache#staticdata#singlesupplier#end";

	public static final String HOTEL_FETCH_STATIC_DATA_SUPPLIER_MAPPING_PROCESS_START =
			"process#cache#staticdata#suppliermapping#singlesupplier#start";
	public static final String HOTEL_FETCH_STATIC_DATA_SUPPLIER_MAPPING_PROCESS_END =
			"process#cache#staticdata#suppliermapping#singlesupplier#end";

	public static final String HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_START =
			"process#cache#staticdata#hotelmapping#singlesupplier#start";
	public static final String HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_END =
			"process#cache#staticdata#hotelmapping#singlesupplier#end";

	public static final String MISSING_INFO = "missinginfo";

	public static final String RAILSEARCH = "railsearch";

	public static final String RAILSEARCH_RESPONSE_START = "railsearch#parseresponse#start";
	public static final String RAILSEARCH_RESPONSE_END = "railsearch#parseresponse#end";


	public static final String RAILSEARCH_DOSEARCH = "railsearch#dosearch";
	public static final String RAILSEARCH_PROCESS = "railsearch#process";
	public static final String RAILSEARCH_STORE = "railsearch#store";
	public static final String RAIL_SUPPLIER_SEARCH_START = "rail#supplier#search#start";
	public static final String RAIL_SUPPLIER_SEARCH_END = "rail#supplier#search#end";

	public static final String RAIL_REVIEW_REVIEWJOURNEY = "railreview#reviewjourney";
	public static final String RAIL_REVIEW_SUPPLIERBEGIN = "railreview#supplier#begin";
	public static final String RAIL_REVIEW_SUPPLIEREND = "railreview#supplier#end";

	public static final String RAIL_SUPPLIER_CHANGE_CLASS_SEARCH_START = "rail#supplier#changeclasssearch#start";
	public static final String RAIL_SUPPLIER_CHANGE_CLASS_END_START = "rail#supplier#changeclassend#end";

	public static final String RAIL_SUPPLIER_CHANGE_CLASS_FARE_SEARCH_START = "rail#supplier#changeclasssearch#start";
	public static final String RAIL_SUPPLIER_CHANGE_CLASS_FARE_END_START = "rail#supplier#changeclassend#end";

	public static final String RAILBOOKING_PROCESS = "rail#booking#process";

	public static final String REPORT_START = "report#start";

	public static final String REPORT_END = "report#end";

	public static final String REPORT_STREAM_END = "report#stream#end";
	// public static final String REPORT_FETCH_FROM_DB = "report#from#db";

	public static final String REPORT_FETCH_ORDER_START = "report#order#start";
	public static final String REPORT_FETCH_ORDER_END = "report#order#end";

	public static final String REPORT_FETCH_AMENDMENT_START = "report#amendment#start";
	public static final String REPORT_FETCH_AMENDMENT_END = "report#amendment#end";


	public static final String REPORT_FETCH_AMENDMENTBOOKINGS_START = "report#amendmentbooking#start";
	public static final String REPORT_FETCH_AMENDMENTBOOKINGS_END = "report#amendmentbooking#end";

	public static final String REPORT_FETCH_AMENDMENT_PAYMENT_START = "report#amendmentpayemnt#start";
	public static final String REPORT_FETCH_AMENDMENT_PAYMENT_END = "report#amendmentpayemnt#end";

	public static final String USER_RELATION_START = "report#userrelation#start";
	public static final String USER_RELATION_END = "report#userrelation#end";

	public static final String CONSTRUCT_ROW_START = "report#constructrow#start";
	public static final String CONSTRUCT_ROW_END = "report#constructrow#end";

	public static final String CONSTRUCT_AMENDMENTWB_START = "report#amendmentwithbookings#start";
	public static final String CONSTRUCT_AMENDMENTWB_END = "report#amendmentwithbookings#end";

	public static final String CONSTRUCT_AMENDMENTOA_START = "report#onlyamendments#start";
	public static final String CONSTRUCT_AMENDMENTOA_END = "report#onlyamendments#end";

	public static final String CONSTRUCT_TRIP_START = "report#constructtripinfo#start";
	public static final String CONSTRUCT_TRIP_END = "report#constructtripinfo#end";

	public static final String REPORT_SORT_START = "report#sorting#start";
	public static final String REPORT_SORT_END = "report#sorting#end";


}
