package com.tgs.services.base.security;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.SecurityFilterConfiguration;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SecurityFilter extends BaseFilter {

	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;

	private static Boolean isSecurityFilterEnabled;

	private static List<String> securityFilterDisabledUrls;

	private static String securityKeywords;

	private static Pattern pattern;

	private static SecurityFilterConfiguration securityFilterConfiguration;

	@Value("${enableSecurityFilter}")
	public void setEnableSecurityFilter(String enableSecurityFilter) {
		isSecurityFilterEnabled = Boolean.parseBoolean(enableSecurityFilter);
	}

	@Value("${securityDisabledUrls}")
	public void setBlockedUrls(String disabledUrls) {
		SecurityFilter.securityFilterDisabledUrls =
				StringUtils.isNotEmpty(disabledUrls) ? Arrays.asList(disabledUrls.split(",")) : Collections.emptyList();
	}

	@Value("${securityKeywords}")
	public void setSecurityKeywords(String securityKeywords) {
		SecurityFilter.securityKeywords = securityKeywords;
	}

	@PostConstruct
	private void init() {
		setDefaults();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String requestUrl = httpRequest.getRequestURL().toString();
		log.debug("Request URL for security filter is : {} ", requestUrl);
		List<Map<String, Object>> parametersMapList = new ArrayList<Map<String, Object>>();
		String faultyValue = null;
		RequestWrapper requestWrapper = null;

		try {
			Boolean isFilterEnabled = securityFilterConfiguration.getIsSecurityFilterEnabled();
			// Fetch list of urls disabled for security filter
			List<String> disabledUrls = securityFilterConfiguration.getSecurityDisabledUrls() != null
					? securityFilterConfiguration.getSecurityDisabledUrls()
					: securityFilterDisabledUrls;

			if (isSecurityFilterEnabled && BooleanUtils.isNotFalse(isFilterEnabled)) {
				// Check if request url is one of the disabled urls
				boolean skipFilterValidation = CollectionUtils.isNotEmpty(disabledUrls)
						&& ServiceUtils.isUrlMatchesRegex(disabledUrls, requestUrl);

				if (!skipFilterValidation) {
					if ("POST".equalsIgnoreCase(httpRequest.getMethod())) {
						// Reading request body with the help of request wrapper
						requestWrapper = new RequestWrapper(httpRequest);
						JSONObject requestBody = requestWrapper.getRequestBody();
						JSONArray requestBodyArray = requestWrapper.getRequestArray();
						// Build Parameters Map
						buildParametersMapList(parametersMapList, requestBody, requestBodyArray);
					} else if ("GET".equalsIgnoreCase(httpRequest.getMethod())) {
						// Reading parameters for GET request
						parametersMapList.add(HttpUtils.getRequestParams(httpRequest));
					}
					if (!parametersMapList.isEmpty()) {
						LogUtils.log("doFilter#validateKeywordsInMap#start",
								LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

						// Validating key-value pairs in map
						faultyValue = validateKeywordsInMap(parametersMapList);

						LogUtils.log("doFilter#validateKeywordsInMap#end",
								LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
								"doFilter#validateKeywordsInMap#start");

						if (faultyValue != null) {
							log.info(
									"[SecurityFilter] The request url has been blocked by SecurityFilter. Url is {} with request body {} has value {} which has faulty keywords in it.",
									requestUrl, parametersMapList, faultyValue);
							accessDeniedResponse(response, requestUrl, faultyValue);
						}
					}
				} else
					log.debug("[SecurityFilter] The request url is blocked for security filter {}", requestUrl);
			} else
				log.debug(
						"[SecurityFilter] Security Filter is disabled. Security filter from enviroment is {} and from cache is {}.",
						isSecurityFilterEnabled, isFilterEnabled);

			if (requestWrapper == null)
				chain.doFilter(request, response);
			else
				chain.doFilter(requestWrapper, response);

		} catch (CustomGeneralException e) {

		} catch (Exception e) {
			log.error("Error occurred while checking for security breach with request url {} due to {} ",
					httpRequest.getRequestURL().toString(), e.getMessage(), e);
			if (requestWrapper == null)
				chain.doFilter(request, response);
			else
				chain.doFilter(requestWrapper, response);
		}
	}

	private void buildParametersMapList(List<Map<String, Object>> parametersMapList, JSONObject requestBody,
			JSONArray requestBodyArray) {
		if (requestBody != null) {
			parametersMapList.add(GsonUtils.getGson().fromJson(requestBody.toString(),
					new TypeToken<Map<String, Object>>() {}.getType()));
		} else if (requestBodyArray != null) {
			for (Object requestBodyObject : requestBodyArray) {
				if (requestBodyObject instanceof JSONObject) {
					parametersMapList.add(GsonUtils.getGson().fromJson(requestBodyObject.toString(),
							new TypeToken<Map<String, Object>>() {}.getType()));
				} else if (requestBodyObject instanceof JSONArray) {
					buildParametersMapList(parametersMapList, null, (JSONArray) requestBodyObject);
				}
			}
		}
	}

	private String validateKeywordsInMap(List<Map<String, Object>> parametersMapList) {

		LocalDateTime from = null;
		LocalDateTime to = null;
		if (pattern != null) {

			for (Map<String, Object> parametersMap : parametersMapList) {
				Matcher matcher = pattern.matcher("");
				for (String key : parametersMap.keySet()) {
					if (parametersMap.get(key) == null)
						continue;
					String value = parametersMap.get(key).toString();
					matcher.reset(value);
					from = LocalDateTime.now();
					log.info("match.find() started at {}", from);
					if (matcher.find()) {
						to = LocalDateTime.now();
						log.info("match.find() ended at{}", to);
						long milliseconds = ChronoUnit.MILLIS.between(from, to);
						log.info("total time taken by match.find to finish is {}", milliseconds);
						return value;


					}

				}
			}
		}
		to = LocalDateTime.now();
		log.info("match.find() ended at{}", to);
		long milliseconds = ChronoUnit.MILLIS.between(from, to);
		log.info("total time taken by match.find to finish is {}", milliseconds);
		return null;
	}


	private String convertToRegex(String keywords) {
		// Converting keywords to regular expressions for pattern matching
		if (StringUtils.isEmpty(keywords))
			return null;
		keywords = "(?:" + keywords + ")";
		keywords = keywords.replace(",", "|");
		return keywords;
	}

	@Scheduled(initialDelay = 5 * 1000, fixedDelay = 60 * 1000)
	public void setDefaults() {
		SystemContextHolder.setContextData(null);
		ClientGeneralInfo clientInfo = gmsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		securityFilterConfiguration = clientInfo.getSecurityFilterConfiguration();
		String securityRegex = convertToRegex(securityFilterConfiguration.getSecurityKeywords());
		String regex = securityRegex != null ? securityRegex : convertToRegex(securityKeywords);
		if (StringUtils.isNotEmpty(regex) && pattern == null) {
			pattern = Pattern.compile(regex);
		}
	}

	private void accessDeniedResponse(ServletResponse response, String requestUrl, String value) throws IOException {
		log.debug("Security breach detected for requestUrl {} and value {} ", requestUrl, value);
		addToAnalytics(requestUrl, "SecurityBreach" + value, SystemError.ACCESS_DENIED);
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_403));
		res.addError(new ErrorDetail(SystemError.ACCESS_DENIED.errorCode(), SystemError.ACCESS_DENIED.getMessage()));
		response.getWriter().write(GsonUtils.getGsonBuilder().setPrettyPrinting().create().toJson(res));
		throw new CustomGeneralException(SystemError.ACCESS_DENIED);
	}


}
