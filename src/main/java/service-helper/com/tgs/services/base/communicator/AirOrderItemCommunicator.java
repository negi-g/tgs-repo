package com.tgs.services.base.communicator;

import java.util.List;

import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import org.springframework.stereotype.Service;

@Service
public interface AirOrderItemCommunicator {

	List<SegmentInfo> updateOrderAndItem(List<SegmentInfo> segmentInfos, Order order, AirItemStatus itemStatus);

	List<TripInfo> findTripByBookingId(String bookingId);
	
	HotelSearchQuery getHotelSearchQuery(String bookingId);

	List<AirOrderItem> findItems(String bookingId);

	List<AirOrderItem> getAirOrderItems(List<String> bookingId);

	List<SegmentInfo>  checkAndUpdateOrderStatus(List<SegmentInfo> segmentInfos, Order order);

	Order getOrderByBookingId(String bookingId);
}
