package com.tgs.services.base.communicator;

import java.util.List;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;

public interface AmendmentCommunicator {

	List<AirOrderItem> filterItemsNPax(Amendment amendment, List<AirOrderItem> airOrderItems);

	List<Amendment> fetchAmendments(List<String> amendmentIds);
}
