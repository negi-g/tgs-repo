package com.tgs.services.base;

import java.util.Arrays;

import com.google.gson.Gson;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cms.datamodel.commission.CommissionRule;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

public class GsonPoly {

	public static void main(String[] args) {
		CommissionRule rule = CommissionRule.builder().build();
		// rule.setCode("AI");
		rule.setProduct(Product.AIR);
		FlightBasicRuleCriteria ruleCriteria = new FlightBasicRuleCriteria();
		// ruleCriteria.setProduct(OrderType.AIR.name());
		ruleCriteria.setAirlineList(Arrays.asList("AI"));
		rule.setInclusionCriteria(ruleCriteria);

		Gson gson = new Gson();
		System.out.println(gson.toJson(rule));
		String str = gson.toJson(rule);
		gson = GsonUtils.builder().build().buildGson(str, CommissionRule.class);
		CommissionRule rule1 = gson.fromJson(str, CommissionRule.class);
		System.out.println(rule1);
	}
}
