
package com.tgs.services.base.security;

import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import com.tgs.utils.encryption.EncryptionUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ConfigurationProperties
@Component
public class SecurityConstants {
	public static final String LOGIN_URL = "/ums/v1/sign-in";

	public static String KEYID;

	public static final String HEADER_STRING = "Authorization";
	public static final String RT_STRING = "RT";
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String API_HEADER_KEY = "apikey";

	// Plain Text Key is tech@sol#123
	private static String SECRET;
	// This value is defined in millisecond
	public static long ACCESSTOKEN_EXPIRATION_TIME;

	// This value is defined in millisecond
	public static long ACCESSTOKEN_REFRESH_TIME;

	private static String DATA_SECURITY_KEY;

	@Autowired
	private Environment env;

	@PostConstruct
	public void readProperty() {
		KEYID = env.getProperty("jwt.keyid");
		String encryptedKey = env.getProperty("jwt.encryptedKey");
		String encryptedDataSecurityKey = env.getProperty("encryptedDataSecurityKey");
		if (StringUtils.isNotEmpty(encryptedDataSecurityKey)) {
			encryptedDataSecurityKey = encryptedDataSecurityKey.trim();
		}
		ACCESSTOKEN_EXPIRATION_TIME = env.getProperty("jwt.accessTokenExpirationTime", Long.class);
		ACCESSTOKEN_REFRESH_TIME = env.getProperty("jwt.accessTokenRefreshTime", Long.class);
		if (StringUtils.isNotBlank(KEYID)) {
			SECRET = EncryptionUtils.decryptUsingKMS(encryptedKey, KEYID);
			if (StringUtils.isNotBlank(encryptedDataSecurityKey)) {
				DATA_SECURITY_KEY = EncryptionUtils.decryptUsingKMS(encryptedDataSecurityKey, KEYID);
			}
		} else {
			SECRET = encryptedKey;
			DATA_SECURITY_KEY = encryptedDataSecurityKey;
		}
	}

	public static String getSecretKey() {
		return SECRET;
	}

	public static String getDataSecurityKey() {
		return DATA_SECURITY_KEY;
	}

}
