package com.tgs.services.base.manager;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.jparepository.BaseService;
import com.tgs.services.gms.datamodel.ReferenceIdConfiguration;

@Service
public class DefaultReferenceIdGenerator extends AbstractReferenceIdGenerator {

	@Autowired
	private BaseService baseService;

	protected String getPrefix(ProductMetaInfo productMetaInfo, ReferenceIdConfiguration idConfiguration) {
		String clientPrefix = StringUtils.defaultString(idConfiguration.getPrefix());
		return clientPrefix.concat(productMetaInfo.getProduct().getPrefix(productMetaInfo));
	}

	protected String getSuffix(ProductMetaInfo productMetaInfo, ReferenceIdConfiguration idConfiguration) {
		return null;
	}

	protected String getNextSequence() {
		return baseService.getNextReferenceIdSequence().toString();
	}
}
