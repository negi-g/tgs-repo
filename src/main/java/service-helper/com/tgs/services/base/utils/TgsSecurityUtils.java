package com.tgs.services.base.utils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import com.tgs.services.base.datamodel.EncryptionData;
import com.tgs.services.base.security.SecurityConstants;
import com.tgs.utils.encryption.EncryptionUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * {@code TgsSecurityUtils} is different from {@link EncryptionUtils} due to dependency on {@link SecurityConstants}.
 */
@Slf4j
public class TgsSecurityUtils {

	/**
	 * Encrypt data if system has security key.
	 * 
	 * @param data
	 * @return
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public static String encryptData(String data) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		log.debug("Encrypting {}", data);
		return EncryptionUtils.encryptData(data,
				EncryptionData.builder().aesKey(SecurityConstants.getDataSecurityKey()).build());
	}

	/**
	 * Decrypt data if system has security key.
	 * 
	 * @param data
	 * @return
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public static String decryptData(String data) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		log.debug("Decrypting {}", data);
		return EncryptionUtils.decryptData(data,
				EncryptionData.builder().aesKey(SecurityConstants.getDataSecurityKey()).build());
	}
}
