package com.tgs.services.base.runtime.database;

import java.sql.Types;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;

public enum PsqlFunction {

	JSONB_EXTRACT_PATH("jsonb_extract_path", Types.JAVA_OBJECT),
	JSONB_EXTRACT_PATH_TEXT("jsonb_extract_path_text", Types.LONGVARCHAR), TO_JSONB("to_jsonb", Types.JAVA_OBJECT);

	final String functionName;
	final int returnType;

	private PsqlFunction(String functionName, int returnJdbcType) {
		this.functionName = functionName;
		this.returnType = returnJdbcType;
	}

	/**
	 * 
	 * @return JDBC Type of value returned by this function
	 * @see Types
	 */
	public int returnType() {
		return returnType;
	}

	/**
	 * Create an expression for this function.
	 * 
	 * @param name function name
	 * @param type result/return type
	 * @param args function arguments
	 * @return expression
	 */
	public <T> Expression<T> getExpression(CriteriaBuilder criteriaBuilder, Class<T> type, Expression<?>... args) {
		return criteriaBuilder.function(functionName, type, args);
	}
}
