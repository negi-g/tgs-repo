package com.tgs.services.base.utils;

import java.util.Base64;

public class HibernateUtils {
	
	private static final String DELIMITER = " ";
	
	public static String getNextDataSetCursorInfo(Long limit , Long cursor) {
		
		String str = String.join(DELIMITER , String.valueOf(limit), String.valueOf(cursor));
		return Base64.getEncoder().encodeToString(str.getBytes());
		
	}
	
	public static String getDecodedString(String str) {
		byte[] decodedBytes = Base64.getDecoder().decode(str);
		return new String(decodedBytes);
	}
	
	public static String getDelimiter() {
		return DELIMITER;
	}

	public static long[] getDataFromRequest(String request) {
		
		String str = getDecodedString(request);
		String[] data = str.split(DELIMITER);
		return new long[] {Long.valueOf(data[0]) , Long.valueOf(data[1])};
	}

}
