package com.tgs.services.base;

import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.axiom.om.DeferredParsingException;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.context.ServiceContext;
import org.apache.axis2.description.MessageContextListener;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import com.ctc.wstx.exc.WstxIOException;
import com.ctc.wstx.exc.WstxLazyException;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class SoapRequestResponseListner implements MessageContextListener {

	private String key;
	private String type;
	private String supplier;
	private Boolean storeLogs;
	private StopWatch stopWatch = new StopWatch();

	private List<String> visibilityGroups;

	private Long responseTime;

	public SoapRequestResponseListner(String bookingId) {
		this.key = bookingId;
		this.visibilityGroups = new ArrayList<>();
	}

	public SoapRequestResponseListner(String bookingId, String type) {
		this.key = bookingId;
		this.type = ObjectUtils.firstNonNull(type, "");
		this.visibilityGroups = new ArrayList<>();
	}

	public SoapRequestResponseListner(String bookingId, String type, String supplier) {
		this.key = bookingId;
		this.type = ObjectUtils.firstNonNull(type, "");
		this.supplier = supplier;
		this.visibilityGroups = new ArrayList<>();
	}

	@Override
	public void attachServiceContextEvent(ServiceContext sc, MessageContext mc) {
		stopWatch.reset();
		stopWatch.start();
		extractMessage(mc, type + "Request");
	}

	/**
	 * Synchronously send the request and receive a response. This relies on the transport correctly connecting the
	 * response InputStream! - For Each It will be called during that time MC will be null
	 * 
	 * @param mc the request MessageContext to send.
	 * @throws AxisFault Sends the message using a two way transport and waits for a response
	 */
	public void extractMessage(MessageContext mc, String type) {
		if (BooleanUtils.isNotFalse(storeLogs)) {
			try {
				if (mc != null && mc.getEnvelope() != null) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					mc.getEnvelope().cloneOMElement().serialize(baos);
					String requestXml = baos.toString();
					User user = SystemContextHolder.getContextData().getUser();
					LogData logData = LogData.builder().generationTime(LocalDateTime.now())
							.logData(requestXml.toString()).key(key).type(type)
							.userRole(UserUtils.getEmulatedUserRoleOrUserRole(user)).visibilityGroups(visibilityGroups)
							.userId(UserUtils.getUserId(user)).logType("AirSupplierAPILogs").build();
					if (isResponse(type)) {
						log.debug("Time taken to get response for key {} , type {} : {}  ", key, type, responseTime);
						logData.setResponseTime(responseTime);
					}
					LogUtils.store(Arrays.asList(logData));
					baos.close();
				}
			} catch (DeferredParsingException | WstxIOException | WstxLazyException e) {
				// httpclient connection closed while reading before stream
				log.info("Error Occured while adding to log for key {} cause {}", key, e.getMessage());
			} catch (Exception e) {
				log.error("Error Occured while adding to log for key {}", key, e);
			} finally {

			}
		}
	}

	private boolean isResponse(String type) {
		return StringUtils.containsIgnoreCase(type, "Response") || StringUtils.containsIgnoreCase(type, "Rs");
	}

	@Override
	public void attachEnvelopeEvent(MessageContext mc) {
		if (!stopWatch.isStopped()) {
			// in case of same listener is used in multi threading will give you IllegalStateException
			// cause stopwatch might be closed by any other thread
			stopWatch.stop();
			responseTime = stopWatch.getTime(TimeUnit.MILLISECONDS);
		}
		extractMessage(mc, type + "Response");
	}

	public void extractMessage(String message, String type) {
		if (BooleanUtils.isNotFalse(storeLogs)) {
			try {

				if (message != null) {
					LogData logData = LogData.builder().generationTime(LocalDateTime.now()).logData(message).key(key)
							.visibilityGroups(visibilityGroups).type(type).logType("AirSupplierAPILogs").build();
					if (isResponse(type)) {
						log.debug("Time taken to get response for key {} , type {} : {}  ", key, type, responseTime);
						logData.setResponseTime(responseTime);
					}
					LogUtils.store(Arrays.asList(logData));
				}
			} catch (Exception e) {
				log.error("Error Occured while adding to log for key {}", key, e);
			} finally {
				responseTime = null;
			}
		}
	}

}
