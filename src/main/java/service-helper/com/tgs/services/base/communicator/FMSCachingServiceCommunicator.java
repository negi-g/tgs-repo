package com.tgs.services.base.communicator;

import java.util.List;
import java.util.Map;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.fms.datamodel.AirCacheSearchResult;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirReviewResponse;

public interface FMSCachingServiceCommunicator {

	/**
	 * expiration will in seconds
	 */
	<V> boolean store(String key, String bin, V value, CacheMetaInfo metaInfo);

	/**
	 * expiration will in seconds
	 *
	 * @param trips
	 * @param key
	 * @param metaInfo
	 * @return
	 */
	boolean cacheFlightListingOnSearchId(List<TripInfo> trips, String key, CacheMetaInfo metaInfo);

	boolean cacheSearchResult(List<TripInfo> trips, String key, CacheMetaInfo metaInfo);

	<V> List<V> fetchValues(String key, Class<V> classofV, CacheMetaInfo metaInfo);

	<V> List<V> fetchPartionValues(String key, Class<V> classofV, CacheMetaInfo metaInfo);

	/**
	 * If you want to fetch value corresponding to a particular bin
	 *
	 * @param key
	 * @param classofV
	 * @param binValue
	 * @return
	 */
	<V> V fetchValue(String key, Class<V> classofV, String set, String binValue);

	<V> V fetchValue(Class<V> classofV, CacheMetaInfo metaInfo);

	/**
	 * If you want to fetch value corresponding to list of bins
	 *
	 * @param key
	 * @param classofV
	 * @param binValue
	 * @return
	 */
	<V> Map<String, V> fetchValue(String key, Class<V> classofV, String set, String... binValue);

	boolean deleteRecord(CacheMetaInfo metaInfo);

	/**
	 * Fetch {@code TripInfo} for given priceId.
	 *
	 * @param priceId
	 * @return
	 */
	TripInfo getTripInfo(String priceId);

	/**
	 * Fetch {@code AirSearchQuery} for given searchId.
	 *
	 * @param searchId
	 * @return
	 */
	AirSearchQuery getSearchQuery(String searchId);

	/**
	 * Fetch {@code AirRevieResponse} for given bookingId.
	 *
	 * @param bookingId
	 * @return
	 */
	AirReviewResponse getAirReviewResponse(String bookingId);

	void scanAndDelete(CacheMetaInfo metaInfo);

	AirCacheSearchResult fetchAirSearchResult(String key, CacheMetaInfo metaInfo);

	boolean deleteCacheRecord(CacheMetaInfo metaInfo);

}
