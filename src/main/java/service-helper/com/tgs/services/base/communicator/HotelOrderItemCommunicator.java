package com.tgs.services.base.communicator;

import java.util.List;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.restmodel.hotel.HotelBnplStatusUpdateRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingInfoUpdateRequest;

@Service
public interface HotelOrderItemCommunicator {

	void updateOrderItem(HotelInfo hInfo, Order order, HotelItemStatus itemStatus);

	HotelInfo getHotelInfo(String bookingId);

	List<HotelOrderItem> getHotelOrderItems(List<String> bookingIds);

	void updateOrderItemStatus(Order order, HotelItemStatus itemStatus);

	void updateHotelReferenceNumber(HotelBookingInfoUpdateRequest request);

	void updateHotelBnplStatus(HotelBnplStatusUpdateRequest request);

}
