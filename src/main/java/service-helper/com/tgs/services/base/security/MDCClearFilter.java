package com.tgs.services.base.security;

import org.apache.log4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import javax.servlet.FilterConfig;
import javax.servlet.Filter;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import java.io.IOException;


/**
 * A Filter that needs to be filtered for every requests which are incoming to tomcat spring thread pool.<br>
 * when thread borrowed from Spring thread pool,it may have some thread context(mdc) which is not cleared after its
 * execution to handle such scenarious, MDCClearFilter will be the first filter registered for chaining to other filter
 * and its clear MDC context before returning response to caller.
 *
 * @author Prabhu
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MDCClearFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		chain.doFilter(request, response);
		// this will handle even if any Filter throwing exception directly
		MDC.clear();
	}

	@Override
	public void destroy() {

	}


}
