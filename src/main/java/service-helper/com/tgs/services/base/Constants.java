package com.tgs.services.base;

import java.time.LocalDateTime;

public final class Constants {

    public static final LocalDateTime maxDateTime = LocalDateTime.of(9999, 1,1,0,0);
    
}
