package com.tgs.services.base;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.cxf.binding.soap.interceptor.SoapPreProtocolOutInterceptor;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.datamodel.User;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class CxfResponseLogInterceptor extends AbstractPhaseInterceptor<Message> {

	private LogData responseLogData;
	private String key;
	private String type;
	private boolean storeLog;
	private List<String> visibilityGroups;

	public CxfResponseLogInterceptor() {
		super(Phase.PRE_STREAM);
		addBefore(SoapPreProtocolOutInterceptor.class.getName());
	}

	public CxfResponseLogInterceptor(String type, String key, boolean storeLog,List<String> visibilityGroups) {
		super(Phase.PRE_STREAM);
		addBefore(SoapPreProtocolOutInterceptor.class.getName());
		this.key = key;
		this.type = type;
		this.storeLog = true;
		this.visibilityGroups = CollectionUtils.isNotEmpty(visibilityGroups) ? visibilityGroups : new ArrayList<>();;
	}

	@Override
	public void handleMessage(Message message) {
		boolean isOutbound;
		isOutbound = message == message.getExchange().getOutMessage()
				|| message == message.getExchange().getOutFaultMessage();

		if (!isOutbound && this.storeLog) {
			try {
				InputStream is = message.getContent(InputStream.class);
				CachedOutputStream bos = new CachedOutputStream();
				IOUtils.copy(is, bos);
				bos.flush();
				is.close();
				handleInboundXmlMessage(new String(bos.getBytes()));
				message.setContent(InputStream.class, bos.getInputStream());
				bos.close();
			} catch (Exception ioe) {
				log.info("Not able to fetch response from webservice ", ioe);
			}
		}

	}

	@Override
	public void handleFault(Message message) {
	}

	protected void handleInboundXmlMessage(String xml) {
		User user = SystemContextHolder.getContextData().getUser();
		responseLogData = LogData.builder().generationTime(LocalDateTime.now()).logData(xml).key(key)
				.type(this.type + " Response").userRole(UserUtils.getEmulatedUserRoleOrUserRole(user))
				.userId(UserUtils.getUserId(user)).logType("AirSupplierAPILogs").visibilityGroups(visibilityGroups).build();
		log.debug("Response for type {} , key {} ", responseLogData.getType(), responseLogData.getKey());

		log.debug("Total time took to process key {} is {}", key,
				SystemContextHolder.getContextData().getSearchWatch().getTime());

		LogUtils.store(Arrays.asList(responseLogData));
	}

}
