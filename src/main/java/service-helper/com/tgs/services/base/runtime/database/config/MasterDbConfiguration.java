package com.tgs.services.base.runtime.database.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.tgs.utils.encryption.EncryptionUtils;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory", transactionManagerRef = "transactionManager",
		basePackages = {"com.tgs.services"})
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class MasterDbConfiguration {

	@Value(("${master.datasource.url}"))
	private String url;

	@Value(("${master.datasource.username}"))
	private String username;

	@Value(("${master.datasource.driverClassName}"))
	private String driverClassName;

	@Value(("${master.datasource.keyid}"))
	private String keyid;

	@Value(("${master.datasource.encrypted-password}"))
	private String encryptedPassword;

	private String password;

	@Primary
	@Bean
	@ConfigurationProperties(prefix = "master.datasource")
	public DataSource dataSource() {
		if (StringUtils.isNotBlank(keyid)) {
			password = EncryptionUtils.decryptUsingKMS(encryptedPassword, keyid);
		} else {
			/**
			 * password can't be left null because it is re-used.
			 */
			password = encryptedPassword;
		}
		return DataSourceBuilder.create().password(password).build();
	}

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("dataSource") DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean factoryBean =
				builder.dataSource(DataSourceBuilder.create().username(username).password(password).url(url)
						.driverClassName(driverClassName).build()).packages("com.tgs.services").build();
		return factoryBean;
	}

	@Bean(name = "transactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("entityManagerFactory") EntityManagerFactory EntityManagerFactory) {
		JpaTransactionManager jpaManager = new JpaTransactionManager(EntityManagerFactory);
		jpaManager.setDataSource(dataSource());
		return jpaManager;
	}
	
	/**
	 * To be used for queries which doesn't query on any entity.
	 */
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource)
	{
	    return new JdbcTemplate(dataSource);
	}
}
