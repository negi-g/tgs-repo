package com.tgs.services.base.validator.response;

import static com.tgs.services.base.enums.UserRole.AGENT;
import static com.tgs.services.base.enums.UserRole.AGENT_STAFF;
import static com.tgs.services.base.enums.UserRole.CORPORATE;
import static com.tgs.services.base.enums.UserRole.CORPORATE_EMPLOYEE;
import static java.util.stream.Collectors.toList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;

@Service
public class AgentResponseUserRolesValidator extends ResponseUserRolesValidator {

	final static private List<UserRole> TARGET_ROLES = Lists.newArrayList(AGENT, AGENT_STAFF);
	final static private List<UserRole> FORBIDDEN_ROLES = Lists.newArrayList(CORPORATE, CORPORATE_EMPLOYEE);

	@Override
	protected void validateUserRoles(User targetUser, Map<UserRole, List<User>> roleWiseUsers,
			ResponseValidationResult result) {
		List<User> agentList = roleWiseUsers.getOrDefault(AGENT, Collections.emptyList());
		long agentCount = agentList.size();
		User agentUser = agentCount > 0 ? agentList.get(0) : null;

		List<String> parentUserIdList = roleWiseUsers.getOrDefault(AGENT_STAFF, Collections.emptyList()).stream()
				.map(User::getParentUserId).distinct().collect(toList());
		long parentUserCount = parentUserIdList.size();
		String parentUserId = parentUserCount > 0 ? parentUserIdList.get(0) : null;

		if ((agentCount + parentUserCount) == 0) {
			return;
		}

		Validator.builder().agentCount(agentCount).agentUser(agentUser).parentUserCount(parentUserCount)
				.parentUserId(parentUserId).targetUser(targetUser).build().validate(result);
	}

	@Override
	public boolean supports(User targetUser) {
		return targetUser != null
				&& (TARGET_ROLES.contains(targetUser.getRole()) || FORBIDDEN_ROLES.contains(targetUser.getRole()));
	}

	@Builder
	@Getter
	private static class Validator {

		private final long agentCount;
		private final long parentUserCount;
		private User agentUser;
		private String parentUserId;
		private User targetUser;

		private void validate(ResponseValidationResult result) {
			if (FORBIDDEN_ROLES.contains(targetUser.getRole())) {
				result.addError(new StringBuilder(userToString(targetUser))
						.append(" is able to see data of AGENT/AGENT_STAFF").toString());
				return;
			}
			if (agentCount > 1 || parentUserCount > 1) {
				result.addError(new StringBuilder(userToString(targetUser))
						.append(" is able to see data of more than 1 AGENTs").toString());
			}
			if (agentUser != null && !agentUser.getUserId().equals(targetUser.getParentUserId())) {
				result.addError(new StringBuilder(userToString(targetUser)).append(" is able to see data of user ")
						.append(userToString(agentUser)).toString());
			}
			if (parentUserId != null && !parentUserId.equals(targetUser.getParentUserId())) {
				result.addError(new StringBuilder(userToString(targetUser)).append(" is able to see data of user ")
						.append(parentUserId).toString());
			}
		}

		private String userToString(User user) {
			return new StringBuilder(user.getUserId()).append('(').append(user.getRole()).append(')').toString();
		}
	}

}
