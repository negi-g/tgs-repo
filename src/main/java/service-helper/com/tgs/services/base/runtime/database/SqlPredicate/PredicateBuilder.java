package com.tgs.services.base.runtime.database.SqlPredicate;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls.EqualsPredicate;
import com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls.GTEPredicate;
import com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls.GTPredicate;
import com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls.InPredicate;
import com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls.LTEPredicate;
import com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls.LTPredicate;
import com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls.LikePredicate;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.utils.exception.UnAuthorizedException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PredicateBuilder {

	public List<Predicate> buildPredicates(Root<? extends BaseModel<?, ?>> root, Class<?> filterClass,
			CriteriaBuilder criteriaBuilder, QueryFilter filter, List<Predicate> predicates) {
		try {
			buildPredicates(root, filterClass, criteriaBuilder, filter, predicates, new ArrayList<>());
		} catch (UnAuthorizedException e) {
			throw new UnAuthorizedException(SystemError.UNAUTHORIZED_ACCESS);
		} catch (Exception e) {
			log.error("Unable to build predicate list because of {}", e.getMessage(), e);
			throw new CustomGeneralException("Exception occurred during search. Check logs.");
		}
		return predicates;
	}

	/**
	 * Support for jsonb
	 * 
	 * @param root
	 * @param filterClass
	 * @param criteriaBuilder
	 * @param filter
	 * @param predicates
	 * @param path
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	private void buildPredicates(Root<?> root, Class<?> filterClass, CriteriaBuilder criteriaBuilder,
			QueryFilter filter, List<Predicate> predicates, List<String> path)
			throws IllegalArgumentException, IllegalAccessException {
		boolean pathEmpty = path.isEmpty();
		List<Field> searchFields = new ArrayList<>(Arrays.asList(filterClass.getFields()));
		List<Field> parentFilterFields = new ArrayList<>(Arrays.asList(QueryFilter.class.getDeclaredFields()));
		searchFields.addAll(parentFilterFields);
		for (Field field : searchFields) {
			field.setAccessible(true);
			SearchPredicate predicate = field.getAnnotation(SearchPredicate.class);
			if (predicate != null && predicate.isUserIdentifier())
				enforceUserVisibilityPermissions(field, filter);
			Object value = field.get(filter);
			if (predicate != null && value != null && StringUtils.isNotEmpty(predicate.convertToDateTime())) {
				value = LocalDateTime.of((LocalDate) value,
						predicate.convertToDateTime().equals("FROM") ? LocalTime.MIN : LocalTime.MAX);
			}
			if (value != null && predicate != null) {
				String attribute = predicate.dbAttribute();
				PredicateType predicateType = predicate.type();
				boolean isNegated = predicate.isNegated();
				value = handleEnums(field, value);
				path.addAll(getPath(attribute));
				if (PredicateType.OBJECT.equals(predicateType)) {
					buildPredicates(root, field.getType(), criteriaBuilder, (QueryFilter) value, predicates, path);
				} else {
					getPredicate(predicateType).createAndAdd(root, path, criteriaBuilder, predicates, value, isNegated);
				}
				log.debug("Filter {}, Field {}, value {}, path {}", filterClass.getSimpleName(), field.getName(), value,
						GsonUtils.getGson().toJson(path));
				if (pathEmpty)
					path.clear();
				else
					path.remove(path.size() - 1);
			}
		}
	}

	private List<String> getPath(String str) {
		return Stream.of(str.split("\\.")).collect(Collectors.toList());
	}

	private Object handleEnums(Field field, Object value) {

		Class<?> typeArgClass;
		try {
			if (field.getType().isEnum())
				value = field.getType().getMethod("getCode").invoke(value).toString();

			if (Collection.class.isAssignableFrom(field.getType())
					&& field.getGenericType() instanceof ParameterizedType) {
				typeArgClass = ((Class<?>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0]);
				if (typeArgClass.isEnum()) {
					List<String> strings = new ArrayList<>();
					for (Object v : (List<?>) value) {
						strings.add(typeArgClass.getMethod("getCode").invoke(v).toString());
					}
					value = strings;
				}
			}
		} catch (Exception ex) {
			log.error(ex.getMessage());
			throw new CustomGeneralException("Exception occurred during search. Check logs.");
		}
		return value;
	}


	private static IPredicate getPredicate(PredicateType predicateType) {

		switch (predicateType) {
			case GT:
				return new GTPredicate();
			case LT:
				return new LTPredicate();
			case GTE:
				return new GTEPredicate();
			case LTE:
				return new LTEPredicate();
			case LIKE:
				return new LikePredicate();
			case EQUAL:
				return new EqualsPredicate();
			case IN:
				return new InPredicate();
			default:
				return new EqualsPredicate();
		}
	}

	@SuppressWarnings("unchecked")
	private void enforceUserVisibilityPermissions(Field field, QueryFilter filter)
			throws IllegalArgumentException, IllegalAccessException {
		field.setAccessible(true);
		if (SystemContextHolder.getContextData() != null && BooleanUtils.isNotTrue(filter.getIsInternalQuery()))
			field.set(filter,
					UserServiceHelper.checkAndReturnAllowedUserId(
							UserUtils.getUserId(SystemContextHolder.getContextData().getUser()),
							(List<String>) field.get(filter)));
	}
}
