package com.tgs.services.base;

import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.Arrays;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.datamodel.User;
import org.apache.axis.Message;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import com.tgs.services.base.utils.LogUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AxisRequestResponseListener {

	String key;
	String type;
	String supplier;
	private Boolean storeLogs;

	public AxisRequestResponseListener(String bookingId) {
		this.key = bookingId;
	}

	public AxisRequestResponseListener(String bookingId, String type) {
		this.key = bookingId;
		this.type = ObjectUtils.firstNonNull(type, "");
	}

	public AxisRequestResponseListener(String bookingId, String type, String supplier) {
		this.key = bookingId;
		this.type = ObjectUtils.firstNonNull(type, "");
		this.supplier = supplier;
	}

	public void extractMessage(Message mc, String type) {
		if (BooleanUtils.isNotFalse(storeLogs)) {
			try {
				User user = SystemContextHolder.getContextData().getUser();
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				String requestXml = mc.getSOAPPartAsString();
				LogData logData =
						LogData.builder().generationTime(LocalDateTime.now()).logData(requestXml).key(key).type(type)
								.logType("AirSupplierAPILogs").userRole(UserUtils.getEmulatedUserRoleOrUserRole(user))
								.userId(UserUtils.getUserId(user)).build();
				LogUtils.store(Arrays.asList(logData));
				baos.close();
			} catch (Exception e) {
			}
		}
	}

	public void extractMessage(String message, String type) {
		try {
			User user = SystemContextHolder.getContextData().getUser();
			LogData logData = LogData.builder().generationTime(LocalDateTime.now()).logData(message).key(key).type(type)
					.logType("AirSupplierAPILogs").userRole(UserUtils.getEmulatedUserRoleOrUserRole(user))
					.userId(UserUtils.getUserId(user)).build();
			LogUtils.store(Arrays.asList(logData));
		} catch (Exception e) {
		}
	}

}
