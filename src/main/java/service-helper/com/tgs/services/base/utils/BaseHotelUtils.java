package com.tgs.services.base.utils;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.Gson;
import com.tgs.services.base.MethodExecutionInfo;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.datamodel.BaseHotelConstants;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.enums.CountryInfoType;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.analytics.HotelMethodExecutionInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelOrderSupplierInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelClientFeeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BaseHotelUtils {

	private final static Set<HotelFareComponent> nonComputableFareComponents;

	static {
		nonComputableFareComponents = new HashSet<>();
		nonComputableFareComponents.addAll(HotelFareComponent.getGstComponents());
		nonComputableFareComponents.addAll(HotelFareComponent.getNonComputableFareComponents());
	}

	private static HMSCommunicator hmsComm;

	private static GeneralServiceCommunicator gmsComm;

	public static void init(HMSCommunicator comm, GeneralServiceCommunicator gmsComm) {
		BaseHotelUtils.hmsComm = comm;
		BaseHotelUtils.gmsComm = gmsComm;
	}

	public static Map<String, Integer> generateTravellerKeyWithTTL(HotelInfo hInfo, ContextData contextData,
			DeliveryInfo deliveryInfo, List<RoomTravellerInfo> roomTravellerInfo, User bookingUser) {
		Map<String, Integer> keysWithTtl = new HashMap<>();
		String key = generateTravellerKey(hInfo, contextData, deliveryInfo, roomTravellerInfo, bookingUser);
		keysWithTtl.put(key,
				(int) LocalDateTime.now().until(
						hInfo.getOptions().get(0).getRoomInfos().get(0).getCheckOutDate().atTime(12, 00),
						ChronoUnit.SECONDS));
		log.info("Keys generated for {} hotel and {} travellers are {}", hInfo.getName(), keysWithTtl.keySet());
		return keysWithTtl;
	}

	public static String generateTravellerKey(HotelInfo hInfo, ContextData contextData, DeliveryInfo deliveryInfo,
			List<RoomTravellerInfo> roomTravellerInfos, User bookingUser) {
		if (hInfo == null)
			return null;

		StringBuilder stringBuilder = new StringBuilder(bookingUser.getUserId());
		RoomInfo roomInfo = hInfo.getOptions().get(0).getRoomInfos().get(0);
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String contacts = "";
		String emails = "";
		if (deliveryInfo != null) {
			if (CollectionUtils.isNotEmpty(deliveryInfo.getContacts()))
				contacts = deliveryInfo.getContacts().stream().collect(Collectors.joining("-"));
			if (CollectionUtils.isNotEmpty(deliveryInfo.getEmails()))
				emails = deliveryInfo.getEmails().stream().collect(Collectors.joining("-"));
		}
		String roomTravellers = "";
		if (CollectionUtils.isNotEmpty(roomTravellerInfos)) {
			roomTravellers = StringUtils.lowerCase(roomTravellerInfos.get(0).getTravellerInfo().get(0).getFirstName());
		}
		return stringBuilder.append(hInfo.getName().replaceAll("[^a-zA-Z]", "").toLowerCase())
				.append(roomInfo.getCheckInDate().format(dateTimeFormatter))
				.append(roomInfo.getCheckOutDate().format(dateTimeFormatter)).append(contextData.getUser().getUserId())
				.append(contacts).append(roomTravellers).append(emails).toString();
	}

	public static double totalFareComponentsAmount(Map<HotelFareComponent, Double> fareComponents,
			Set<HotelFareComponent> gstFareComponents) {
		AtomicDouble totalAmount = new AtomicDouble(0);
		if (CollectionUtils.isNotEmpty(gstFareComponents) && !fareComponents.isEmpty()) {
			gstFareComponents.forEach(fc -> totalAmount.addAndGet(fareComponents.getOrDefault(fc, 0.0)));
		}
		return totalAmount.doubleValue();
	}

	public static BigDecimal getTotalSupplierBookingAmount(Option option) {

		BigDecimal amount = BigDecimal.ZERO;
		List<RoomInfo> roomInfoList = option.getRoomInfos();
		for (RoomInfo roomInfo : roomInfoList) {
			BigDecimal roomAmount =
					BigDecimal.valueOf(roomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
							+ roomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.SP, 0.0));
			amount = amount.add(roomAmount);
		}
		amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		return amount;
	}

	public static void updateManagementFee(HotelInfo hotelInfo, HotelSearchQuery searchQuery, Boolean isSearch) {
		long startTime = System.currentTimeMillis();
		try {
			HotelClientFeeOutput clientFeeOutput = getClientFeeOutput(searchQuery);
			for (Option option : hotelInfo.getOptions()) {
				for (RoomInfo roomInfo : option.getRoomInfos()) {
					if (Objects.nonNull(clientFeeOutput)) {
						roomInfo.getPerNightPriceInfos().forEach(priceInfo -> {
							priceInfo.getFareComponents().put(HotelFareComponent.MF,
									clientFeeOutput.getManagementFee());
							priceInfo.getFareComponents().put(HotelFareComponent.MFT,
									clientFeeOutput.getManagementFee() * clientFeeOutput.getManagementFeeTax());
						});
					}
				}
			}
			BaseHotelUtils.updateTotalFareComponents(hotelInfo, isSearch);
		} finally {
			BaseHotelUtils.setMethodExecutionInfo(Thread.currentThread().getStackTrace()[1].getMethodName(),
					searchQuery, Arrays.asList(hotelInfo), System.currentTimeMillis() - startTime);
		}
	}

	public static void updateManagementFee(RoomInfo roomInfo, HotelSearchQuery searchQuery) {
		HotelClientFeeOutput clientFeeOutput = getClientFeeOutput(searchQuery);
		if (Objects.nonNull(clientFeeOutput)) {
			roomInfo.getPerNightPriceInfos().forEach(priceInfo -> {
				priceInfo.getFareComponents().put(HotelFareComponent.MF, clientFeeOutput.getManagementFee());
				priceInfo.getFareComponents().put(HotelFareComponent.MFT,
						clientFeeOutput.getManagementFee() * clientFeeOutput.getManagementFeeTax());
			});
			BaseHotelUtils.updateRoomTotalFareComponents(roomInfo);
		}
	}

	/**
	 * expression which given for Commission type will be evaluated
	 */
	public static Double evaluateHotelExpression(String commExpression, PriceInfo priceInfo,
			List<HotelFareComponent> hotelFareComponents) {
		ExpressionParser expressionParser = new SpelExpressionParser();
		String commercialComponent = getHotelFareComponentExpression(commExpression, priceInfo, hotelFareComponents);
		Expression expression = expressionParser.parseExpression(commercialComponent);
		double value = expression.getValue(Double.class);
		if (commExpression.contains("*"))
			value = value / 100;
		return value;
	}

	public static String getHotelFareComponentExpression(String expression, PriceInfo priceInfo,
			List<HotelFareComponent> hotelFareComponents) {
		for (HotelFareComponent fareComponent : hotelFareComponents) {
			Double amount = 0.0d;
			if (priceInfo.getFareComponents().containsKey(fareComponent)) {
				amount = priceInfo.getFareComponents().getOrDefault(fareComponent, 0.0);
			} else if (priceInfo.getAddlFareComponents().containsKey(HotelFareComponent.TAF)) {
				amount = priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF).getOrDefault(fareComponent, 0.0);
			}
			expression = expression.replace(fareComponent.name(), String.valueOf(amount));
		}
		return expression;
	}

	public static String getAlertCcMails(AlertType alertType) {
		HotelConfiguratorInfo configInfo = hmsComm.getHotelConfigRule(HotelConfiguratorRuleType.GNPURPOSE, null);
		HotelGeneralPurposeOutput hotelGeneralPurposeOutput = (HotelGeneralPurposeOutput) configInfo.getOutput();
		Map<String, String> alertEmailMap = hotelGeneralPurposeOutput.getAlertMails();
		StringJoiner ccEmails = new StringJoiner(",");
		if (MapUtils.isNotEmpty(alertEmailMap)) {
			if (StringUtils.isNotBlank(alertEmailMap.get(alertType.name()))) {
				ccEmails.add(alertEmailMap.get(alertType.name()));
			}
		}
		return ccEmails.toString();
	}

	public static void updateCancellationPolicyFromDeadlineDatetimeInOption(Option option,
			HotelSearchQuery searchQuery) {

		LocalDateTime deadlineDateTime = option.getDeadlineDateTime();
		if (deadlineDateTime == null)
			return;
		Double totalPrice = getTotalSupplierBookingAmount(option).doubleValue();

		LocalDateTime fromDate = LocalDateTime.now();
		List<PenaltyDetails> pds = new ArrayList<>();
		if (deadlineDateTime.toLocalDate().isAfter(LocalDate.now())) {
			PenaltyDetails pd1 = PenaltyDetails.builder().fromDate(LocalDateTime.now()).toDate(deadlineDateTime)
					.penaltyAmount(0.0).build();
			pds.add(pd1);
			fromDate = deadlineDateTime;
		}
		PenaltyDetails pd2 = PenaltyDetails.builder().fromDate(fromDate)
				.toDate(searchQuery.getCheckinDate().atTime(LocalTime.NOON)).penaltyAmount(totalPrice).build();
		pds.add(pd2);
		HotelCancellationPolicy cp = option.getCancellationPolicy();
		if (cp == null)
			cp = HotelCancellationPolicy.builder().build();
		cp.setPenalyDetails(pds);
	}


	public static boolean isPerNightSupplierPricePresent(RoomInfo roomInfo) {

		List<PriceInfo> priceInfoList = roomInfo.getPerNightPriceInfos();
		if (CollectionUtils.isNotEmpty(priceInfoList)) {
			PriceInfo priceInfo = priceInfoList.get(0);
			return priceInfo.getFareComponents().containsKey(HotelFareComponent.BF)
					|| priceInfo.getFareComponents().containsKey(HotelFareComponent.SP);
		}
		return false;
	}


	public static boolean isPerNightManagementFeePresent(RoomInfo roomInfo) {

		List<PriceInfo> priceInfoList = roomInfo.getPerNightPriceInfos();
		if (CollectionUtils.isNotEmpty(priceInfoList)) {
			PriceInfo priceInfo = priceInfoList.get(0);
			if (priceInfo.getAddlFareComponents().containsKey(HotelFareComponent.TAF)) {
				Map<HotelFareComponent, Double> afcs = priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF);
				return afcs.containsKey(HotelFareComponent.MF);
			}
		}
		return false;
	}

	public static HotelClientFeeOutput getClientFeeOutput(HotelSearchQuery searchQuery) {
		String userId = SystemContextHolder.getContextData().getUser().getUserId();
		HotelBasicFact basicFact =
				HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery).generateFactFromUserId(userId);
		HotelConfiguratorInfo clientConfiguratorInfo =
				hmsComm.getHotelConfigRule(HotelConfiguratorRuleType.CLIENTFEE, basicFact);
		return Objects.isNull(clientConfiguratorInfo) ? null
				: (HotelClientFeeOutput) clientConfiguratorInfo.getOutput();
	}

	public static void updateManagementFeeTax(RoomInfo roomInfo, HotelSearchQuery searchQuery) {
		HotelClientFeeOutput clientFeeOutput = getClientFeeOutput(searchQuery);
		if (Objects.nonNull(clientFeeOutput)) {
			roomInfo.getPerNightPriceInfos().forEach(priceInfo -> {
				Map<HotelFareComponent, Double> afcs = priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF);
				if (afcs != null) {
					Double mf = afcs.get(HotelFareComponent.MF);
					afcs.put(HotelFareComponent.MFT, mf * clientFeeOutput.getManagementFeeTax());
				}
			});
			BaseHotelUtils.updateRoomTotalFareComponents(roomInfo);
		}
	}

	public static void updateTotalFareComponents(HotelInfo hotelInfo, Boolean isSearch) {
		hotelInfo.getOptions().forEach(option -> {
			double totalPrice = 0.0;
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				if (BooleanUtils.isTrue(isSearch)) {
					updateRoomTotalFareComponentsForSearch(roomInfo);
				} else {
					updateRoomTotalFareComponents(roomInfo);
				}
				totalPrice += roomInfo.getTotalPrice();
			}
			option.setTotalPrice(totalPrice);
		});
	}

	public static void updateTotalFareComponents(Option option, Boolean isSearch) {

		double totalPrice = 0.0;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			if (BooleanUtils.isTrue(isSearch)) {
				updateRoomTotalFareComponentsForSearch(roomInfo);
			} else {
				updateRoomTotalFareComponents(roomInfo);
			}
			totalPrice += roomInfo.getTotalPrice();
		}
		option.setTotalPrice(totalPrice);
	}

	public static void updateRoomTotalFareComponentsForSearch(RoomInfo roomInfo) {

		updatePerNightTotalFareComponentsForSearch(roomInfo);
		Map<HotelFareComponent, Double> roomLevelFareComponents = new HashMap<>();
		roomInfo.getPerNightPriceInfos().forEach(priceInfo -> {
			Iterator<Entry<HotelFareComponent, Double>> priceLevelFareComponentItr =
					priceInfo.getFareComponents().entrySet().iterator();

			while (priceLevelFareComponentItr.hasNext()) {
				HotelFareComponent fareComponent = priceLevelFareComponentItr.next().getKey();
				roomLevelFareComponents.put(fareComponent, roomLevelFareComponents.getOrDefault(fareComponent, 0.0)
						+ priceInfo.getFareComponents().get(fareComponent));
			}
		});
		roomInfo.setTotalPrice(roomLevelFareComponents.get(HotelFareComponent.TF));
	}

	public static void updatePerNightTotalFareComponentsForSearch(RoomInfo roomInfo) {
		for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
			final double totalPrice[] = new double[1];
			final double totalMarkup[] = new double[1];

			priceInfo.getFareComponents().forEach((fareComponent, value) -> {

				if (fareComponent.equals(HotelFareComponent.MU)) {
					totalMarkup[0] += priceInfo.getFareComponents().get(fareComponent);
				}

				if (!nonComputableFareComponents.contains(fareComponent)) {
					totalPrice[0] += fareComponent.getAmount(priceInfo.getFareComponents().get(fareComponent));
				}
			});

			if (!ObjectUtils.isEmpty(priceInfo.getFareComponents().get(HotelFareComponent.CMU))) {
				if (ObjectUtils.isEmpty(priceInfo.getMiscInfo().getSupplierPrice())) {
					priceInfo.getMiscInfo()
							.setSupplierPrice((priceInfo.getFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
									+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.SP, 0.0)));
				}
				priceInfo.getFareComponents().put(HotelFareComponent.BF, priceInfo.getMiscInfo().getSupplierPrice()
						+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.CMU, 0.0));
			}

			Double totalFare = totalPrice[0] + priceInfo.getFareComponents().getOrDefault(HotelFareComponent.BF, 0.0);
			priceInfo.getFareComponents().put(HotelFareComponent.TF, totalFare);
			priceInfo.getFareComponents().put(HotelFareComponent.NF, totalFare - totalMarkup[0]);
		}
	}


	public static void updateRoomTotalFareComponents(RoomInfo roomInfo) {
		updatePerNightTotalFareComponents(roomInfo);
		Map<HotelFareComponent, Double> roomLevelFareComponents = new HashMap<>();
		Map<HotelFareComponent, Map<HotelFareComponent, Double>> roomLevelAddlFareParentComponents = new HashMap<>();

		roomInfo.getPerNightPriceInfos().forEach(priceInfo -> {
			Map<HotelFareComponent, Map<HotelFareComponent, Double>> priceLevelAdditionalFareComponentsMap =
					priceInfo.getAddlFareComponents();

			priceLevelAdditionalFareComponentsMap
					.forEach((priceLevelAdditionalParentComponent, priceLevelAdditionalChildComponentsMap) -> {
						priceLevelAdditionalChildComponentsMap
								.forEach((priceLevelAdditionalChildComponent, priceLevelChildComponentValue) -> {
									roomLevelAddlFareParentComponents
											.computeIfAbsent(priceLevelAdditionalParentComponent,
													val -> new HashMap<>())
											.put(priceLevelAdditionalChildComponent,
													roomLevelAddlFareParentComponents
															.get(priceLevelAdditionalParentComponent)
															.getOrDefault(priceLevelAdditionalChildComponent, 0.0)
															+ priceLevelAdditionalChildComponentsMap.getOrDefault(
																	priceLevelAdditionalChildComponent, 0.0));
								});
					});

			Iterator<Entry<HotelFareComponent, Double>> priceLevelFareComponentItr =
					priceInfo.getFareComponents().entrySet().iterator();

			while (priceLevelFareComponentItr.hasNext()) {
				HotelFareComponent fareComponent = priceLevelFareComponentItr.next().getKey();
				roomLevelFareComponents.put(fareComponent, roomLevelFareComponents.getOrDefault(fareComponent, 0.0)
						+ priceInfo.getFareComponents().get(fareComponent));
			}
		});
		roomInfo.setTotalPrice(roomLevelFareComponents.get(HotelFareComponent.TF));
		roomInfo.setTotalFareComponents(roomLevelFareComponents);
		roomInfo.setTotalAddlFareComponents(roomLevelAddlFareParentComponents);

	}

	public static void updateSupplierTotalAddlFareComponents(List<HotelOrderSupplierInfo> hotelOrderSupplierInfos) {
		if (hotelOrderSupplierInfos != null) {
			mapAddlFareComponents(hotelOrderSupplierInfos);
			for (HotelOrderSupplierInfo hotelOrderSupplierInfo : hotelOrderSupplierInfos) {
				Map<HotelFareComponent, Double> fareComponents = new HashMap<>();
				Map<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareParentComponents = new HashMap<>();

				hotelOrderSupplierInfo.getPerNightPriceInfos().forEach(priceInfo -> {
					Map<HotelFareComponent, Map<HotelFareComponent, Double>> additionalFareComponentsMap =
							priceInfo.getAddlFareComponents();

					additionalFareComponentsMap.forEach((addlFareParentComponent, addlFareComponentMap) -> {
						addlFareComponentMap.forEach((addlFareComponent, value) -> {
							addlFareParentComponents.computeIfAbsent(addlFareParentComponent, val -> new HashMap<>())
									.put(addlFareComponent,
											addlFareParentComponents.get(addlFareParentComponent)
													.getOrDefault(addlFareComponent, 0.0)
													+ addlFareComponentMap.getOrDefault(addlFareComponent, 0.0));
						});
					});
					Iterator<Entry<HotelFareComponent, Double>> fareComponentIter =
							priceInfo.getFareComponents().entrySet().iterator();

					while (fareComponentIter.hasNext()) {
						HotelFareComponent fareComponent = fareComponentIter.next().getKey();
						fareComponents.put(fareComponent, fareComponents.getOrDefault(fareComponent, 0.0)
								+ priceInfo.getFareComponents().get(fareComponent));
					}
				});
				hotelOrderSupplierInfo.setTotalFareComponents(fareComponents);
				hotelOrderSupplierInfo.setTotalAddlFareComponents(addlFareParentComponents);
			}
		}
	}

	public static void updatePerNightTotalFareComponents(RoomInfo roomInfo) {
		mapAddlFareComponents(roomInfo);
		for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
			Map<HotelFareComponent, Map<HotelFareComponent, Double>> additionalFareComponentsMap =
					priceInfo.getAddlFareComponents();
			final double totalPrice[] = new double[1];
			final double totalMarkup[] = new double[1];
			final double taxAndAddlFees[] = new double[1];

			additionalFareComponentsMap.forEach((addlFareParentComponent, addlFareChildComponent) -> {
				addlFareChildComponent.forEach((fareComponent, value) -> {

					HotelFareComponent mapComponent = fareComponent.mapComponent();

					if (fareComponent.equals(HotelFareComponent.MU)) {
						totalMarkup[0] +=
								priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent);
					}
					if (fareComponent.keepCurrentComponent() && !nonComputableFareComponents.contains(fareComponent)) {
						taxAndAddlFees[0] +=
								priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent);
						totalPrice[0] += fareComponent.getAmount(
								priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent));
					}
					if (priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent) != null
							&& mapComponent != null) {
						double fareComponentValue = fareComponent.getAmount(
								priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent));
						double mapComponentValue = priceInfo.getFareComponents().getOrDefault(mapComponent, 0.0);
						priceInfo.getFareComponents().put(mapComponent, mapComponentValue + fareComponentValue);
					}
				});
			});

			updateClientMarkup(priceInfo);

			Double totalFare = totalPrice[0] + (priceInfo.getFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
					- priceInfo.getFareComponents().getOrDefault(HotelFareComponent.AAR, 0.0));
			priceInfo.getFareComponents().put(HotelFareComponent.TAF, taxAndAddlFees[0]);
			priceInfo.getFareComponents().put(HotelFareComponent.TF, totalFare);
			priceInfo.getFareComponents().put(HotelFareComponent.NF, totalFare - totalMarkup[0]);
		}
	}

	private static void updateClientMarkup(PriceInfo priceInfo) {
		ContextData contextData = SystemContextHolder.getContextData();
		if (Objects.isNull(contextData.getValue("FLOW_TYPE"))
				|| !contextData.getValue("FLOW_TYPE").get().toString().equals(HotelFlowType.AMENDMENT.name())) {
			if (!ObjectUtils.isEmpty(priceInfo.getFareComponents().get(HotelFareComponent.CMU))) {
				if (ObjectUtils.isEmpty(priceInfo.getMiscInfo().getSupplierPrice())) {
					priceInfo.getMiscInfo()
							.setSupplierPrice((priceInfo.getFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
									+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.SP, 0.0)));
				}
				priceInfo.getFareComponents().put(HotelFareComponent.BF, priceInfo.getMiscInfo().getSupplierPrice()
						+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.CMU, 0.0));
			}
		}
	}

	private static void mapAddlFareComponents(List<HotelOrderSupplierInfo> hotelOrderSupplierInfos) {
		for (HotelOrderSupplierInfo hotelOrderSupplierInfo : hotelOrderSupplierInfos) {
			for (PriceInfo priceInfo : hotelOrderSupplierInfo.getPerNightPriceInfos()) {
				Map<HotelFareComponent, Double> fareComponentsMap = priceInfo.getFareComponents();
				Set<HotelFareComponent> mapComponentKeys = new HashSet<>();

				// Map all the fare components into their respective additional fare components key.

				for (Map.Entry<HotelFareComponent, Double> fareComponents : fareComponentsMap.entrySet()) {
					HotelFareComponent fareComponent = fareComponents.getKey();
					HotelFareComponent mapComponent = fareComponent.mapComponent();

					if (!ObjectUtils.isEmpty(mapComponent)) {
						if (MapUtils.isNotEmpty(priceInfo.getAddlFareComponents().get(mapComponent))) {
							priceInfo.getAddlFareComponents().get(mapComponent).put(fareComponent,
									fareComponents.getValue());
						} else {
							Map<HotelFareComponent, Double> map = new HashMap<>();
							map.put(fareComponent, fareComponents.getValue());
							priceInfo.getAddlFareComponents().put(mapComponent, map);
						}
						mapComponentKeys.add(fareComponent);
					}
				}

				for (HotelFareComponent fareComponent : mapComponentKeys) {
					priceInfo.getFareComponents().remove(fareComponent);
				}
			}
		}
	}

	private static void mapAddlFareComponents(RoomInfo roomInfo) {

		for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
			// Map<HotelFareComponent, Map<HotelFareComponent, Double>> filteredAddlParentFareComponents = new
			// HashMap<>();
			Map<HotelFareComponent, Double> fareComponentsMap = priceInfo.getFareComponents();
			Set<HotelFareComponent> mapComponentKeys = new HashSet<>();

			// Map all the fare components into their respective additional fare components
			// key.

			for (Map.Entry<HotelFareComponent, Double> fareComponents : fareComponentsMap.entrySet()) {
				HotelFareComponent fareComponent = fareComponents.getKey();
				HotelFareComponent mapComponent = fareComponent.mapComponent();

				if (!ObjectUtils.isEmpty(mapComponent)) {
					if (MapUtils.isNotEmpty(priceInfo.getAddlFareComponents().get(mapComponent))) {
						priceInfo.getAddlFareComponents().get(mapComponent).put(fareComponent,
								fareComponents.getValue());
					} else {
						Map<HotelFareComponent, Double> map = new HashMap<>();
						map.put(fareComponent, fareComponents.getValue());
						priceInfo.getAddlFareComponents().put(mapComponent, map);
					}
					mapComponentKeys.add(fareComponent);
				}
			}

			for (HotelFareComponent fareComponent : mapComponentKeys) {
				priceInfo.getFareComponents().remove(fareComponent);
			}

			// // Filter additional fare component based on keep current element.
			// Map<HotelFareComponent, Map<HotelFareComponent, Double>> additionalFareComponentsMap =
			// priceInfo.getAddlFareComponents();
			// for (Map.Entry<HotelFareComponent, Map<HotelFareComponent, Double>> additionalFareComponents :
			// additionalFareComponentsMap
			// .entrySet()) {
			// Map<HotelFareComponent, Double> filteredAddlFareComponents = new HashMap<>();
			// HotelFareComponent addlFareParentComponent = additionalFareComponents.getKey();
			// Iterator<Entry<HotelFareComponent, Double>> addlFareComponentIter =
			// additionalFareComponents.getValue().entrySet().iterator();
			// while (addlFareComponentIter.hasNext()) {
			// HotelFareComponent fareComponent = addlFareComponentIter.next().getKey();
			// if (!fareComponent.keepCurrentComponent())
			// filteredAddlFareComponents.put(HotelFareComponent.OT,
			// priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent));
			// filteredAddlFareComponents.put(fareComponent,
			// priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent));
			// }
			// filteredAddlParentFareComponents.put(addlFareParentComponent, filteredAddlFareComponents);
			// priceInfo.setAddlFareComponents(filteredAddlParentFareComponents);
			// }
		}
	}

	@Deprecated
	public static void updateTotalFareComponents(RoomInfo roomInfo) {
		Map<HotelFareComponent, Double> totalFareComponents = new HashMap<>();
		for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
			for (Map.Entry<HotelFareComponent, Double> dayWiseFareComponent : priceInfo.getFareComponents()
					.entrySet()) {
				if (dayWiseFareComponent.getValue() == null)
					continue;
				totalFareComponents.put(dayWiseFareComponent.getKey(),
						totalFareComponents.getOrDefault(dayWiseFareComponent.getKey(), 0.0)
								+ dayWiseFareComponent.getValue());
			}
		}
		Double totalPrice = 0.0;
		for (HotelFareComponent hotelFareComponent : HotelFareComponent.getTotalFareComponents()) {
			totalPrice += totalFareComponents.getOrDefault(hotelFareComponent, 0d);
		}
		roomInfo.setTotalPrice(totalPrice);
		totalFareComponents.put(HotelFareComponent.TF, roomInfo.getTotalPrice());
		roomInfo.setTotalFareComponents(totalFareComponents);
	}

	public static AirType getTripType(HotelInfo hotelInfo) {
		ClientGeneralInfo generalInfo = null;
		if (MapUtils.getObject(SystemContextHolder.getContextData().getValueMap(),
				ConfiguratorRuleType.CLIENTINFO.name()) == null) {
			generalInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
			SystemContextHolder.getContextData().setValue(ConfiguratorRuleType.CLIENTINFO.name(), generalInfo);
		} else {
			generalInfo = (ClientGeneralInfo) SystemContextHolder.getContextData().getValueMap()
					.get(ConfiguratorRuleType.CLIENTINFO.name());
		}

		if (hotelInfo.isDomesticTrip(generalInfo.getCountry())) {
			return AirType.DOMESTIC;
		} else if (!hotelInfo.isDomesticTrip(generalInfo.getCountry())) {
			return AirType.INTERNATIONAL;
		}
		return AirType.ALL;
	}

	public static AirType getTripType(HotelSearchQuery searchQuery) {
		ClientGeneralInfo generalInfo = null;
		if (MapUtils.getObject(SystemContextHolder.getContextData().getValueMap(),
				ConfiguratorRuleType.CLIENTINFO.name()) == null) {
			generalInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
			SystemContextHolder.getContextData().setValue(ConfiguratorRuleType.CLIENTINFO.name(), generalInfo);
		} else {
			generalInfo = (ClientGeneralInfo) SystemContextHolder.getContextData().getValueMap()
					.get(ConfiguratorRuleType.CLIENTINFO.name());
		}

		if (searchQuery.getSearchCriteria().isDomesticTrip(generalInfo.getCountry())) {
			return AirType.DOMESTIC;
		} else if (!searchQuery.getSearchCriteria().isDomesticTrip(generalInfo.getCountry())) {
			return AirType.INTERNATIONAL;
		}
		return AirType.ALL;
	}

	public static void flattenFareComponents(RoomInfo roomInfo) {
		Map<HotelFareComponent, Double> fareComponent = roomInfo.getTotalFareComponents();
		fareComponent.remove(HotelFareComponent.TAF);
		Map<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareComponents =
				roomInfo.getTotalAddlFareComponents();

		for (Map.Entry<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareComponent : addlFareComponents
				.entrySet()) {
			fareComponent.putAll(addlFareComponent.getValue());
		}
		roomInfo.setTotalAddlFareComponents(null);

		for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
			flattenFareComponents(priceInfo);
		}

		for (HotelOrderSupplierInfo hotelOrderSupplierInfo : roomInfo.getHotelOrderSupplierInfos()) {
			flattenFareComponents(hotelOrderSupplierInfo);
		}
	}

	private static void flattenFareComponents(HotelOrderSupplierInfo hotelOrderSupplierInfo) {
		Map<HotelFareComponent, Double> hotelOrderSupplierFareComponent =
				hotelOrderSupplierInfo.getTotalFareComponents();
		hotelOrderSupplierFareComponent.remove(HotelFareComponent.TAF);
		Map<HotelFareComponent, Map<HotelFareComponent, Double>> hotelOrderSupplierAddlFareComponents =
				hotelOrderSupplierInfo.getTotalAddlFareComponents();

		for (Map.Entry<HotelFareComponent, Map<HotelFareComponent, Double>> hotelOrderSupplierAddlFareComponent : hotelOrderSupplierAddlFareComponents
				.entrySet()) {
			hotelOrderSupplierFareComponent.putAll(hotelOrderSupplierAddlFareComponent.getValue());
		}
		hotelOrderSupplierInfo.setTotalAddlFareComponents(null);

		for (PriceInfo priceInfo : hotelOrderSupplierInfo.getPerNightPriceInfos()) {
			flattenFareComponents(priceInfo);
		}

	}

	private static void flattenFareComponents(PriceInfo priceInfo) {
		Map<HotelFareComponent, Double> fareComponent = priceInfo.getFareComponents();
		fareComponent.remove(HotelFareComponent.TAF);
		Map<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareComponents = priceInfo.getAddlFareComponents();

		for (Map.Entry<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareComponent : addlFareComponents
				.entrySet()) {
			fareComponent.putAll(addlFareComponent.getValue());
		}
		priceInfo.setAddlFareComponents(null);
	}

	public static double getTotalOptionValue(Option option, double perRooomNight) {

		double totalOptionValue = 0.0;
		int roomNights = option.getRoomInfos().get(0).getPerNightPriceInfos().size();
		int roomCount = option.getRoomInfos().size();
		totalOptionValue = perRooomNight * (roomNights * roomCount);
		return totalOptionValue;
	}

	public static double getPerRoomNightFareComponent(Option option, double totalAmount) {

		double perRoomNight = 0.0;
		int roomNights = option.getRoomInfos().get(0).getPerNightPriceInfos().size();
		int roomCount = option.getRoomInfos().size();
		perRoomNight = totalAmount / (roomNights * roomCount);
		return perRoomNight;
	}

	public static double getFareComponentPriceAtOptionLevel(Option option, HotelFareComponent fareComponent) {

		return option.getRoomInfos().stream().mapToDouble(ri -> {
			return ri.getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
				return priceInfo.getFareComponents().getOrDefault(fareComponent, 0.0);
			}).sum();
		}).sum();
	}

	public static String getSupplierBinName(String supplierName) {
		/**
		 * Due to the limitation of aerospike binName
		 */

		if (supplierName.length() > 9) {
			supplierName = supplierName.substring(0, 9);
		}
		return "S_" + supplierName + "_ID";
	}

	public static String getSupplierSetName(String supplierName) {

		return supplierName + "_" + CacheSetName.HOTEL_SEARCH_INFO;
	}

	public static String getCurrency() {
		return "INR";
	}

	public static int getTotalRoomNights(Option option) {

		int totalRoomNights = 0;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			totalRoomNights += roomInfo.getPerNightPriceInfos().size();
		}
		return totalRoomNights;
	}

	public static List<Map<String, String>> getCollectionDocument(String key) {
		List<Document> docList = gmsComm.fetchGeneralRoleSpecificDocument(
				CollectionServiceFilter.builder().isConsiderHierarchy(true).key(key).build());
		if (CollectionUtils.isNotEmpty(docList)) {
			Map<String, List<Map<String, String>>> data = new Gson().fromJson(docList.get(0).getData(),
					new TypeToken<HashMap<String, List<Map<String, String>>>>() {}.getType());
			return data.getOrDefault("data", null);
		}
		return null;
	}


	public static String getSpecificFieldFromId(String countryid, String fieldName) {
		List<Map<String, String>> doc = BaseHotelUtils.getCollectionDocument("HOTEL_COUNTRY");
		return CountryInfoType.getSpecificFieldFromCountryId(countryid, fieldName, doc);
	}

	public static boolean equalLists(List<String> one, List<String> two) {
		if (one == null && two == null) {
			return true;
		}

		if ((one == null && two != null) || one != null && two == null || one.size() != two.size()) {
			return false;
		}


		one = new ArrayList<String>(one);
		two = new ArrayList<String>(two);

		Collections.sort(one);
		Collections.sort(two);
		return one.equals(two);
	}

	public static int getNumberOfOptionsInSearchResult(List<HotelInfo> hotelInfos) {

		if (CollectionUtils.isNotEmpty(hotelInfos)) {
			return hotelInfos.stream().mapToInt(hotelInfo -> {
				return hotelInfo.getOptions().size();
			}).sum();
		}
		return 0;
	}

	public static List<String> getHotelSupplierIds(HotelSearchQuery searchQuery, String type) {

		List<String> supplierIds = new ArrayList<>();
		if (BaseHotelConstants.SINGLE_SUPPLIER.equals(type)) {
			supplierIds.add(searchQuery.getMiscInfo().getSupplierId());
		} else {
			supplierIds = CollectionUtils.isEmpty(searchQuery.getMiscInfo().getSupplierIds())
					? Arrays.asList(searchQuery.getMiscInfo().getSupplierId())
					: searchQuery.getMiscInfo().getSupplierIds();
		}
		return supplierIds;
	}

	public static void setMethodExecutionInfo(String methodName, HotelSearchQuery searchQuery,
			List<HotelInfo> hotelInfos, long totalTimeInMs) {
		try {
			final ContextData contextData = SystemContextHolder.getContextData();
			final String methodExecutionType = contextData.getValueMap()
					.getOrDefault(BaseHotelConstants.METHOD_EXECUTION_TYPE, "UNKNOWN").toString();
			final String methodSubExecutionType = contextData.getValueMap()
					.getOrDefault(BaseHotelConstants.METHOD_SUB_EXECUTION_TYPE, "UNKNOWN").toString();
			final String supplierName = getHotelSupplierIds(searchQuery, methodExecutionType).toString();
			final int roomSearched =
					CollectionUtils.isNotEmpty(searchQuery.getRoomInfo()) ? searchQuery.getRoomInfo().size() : 0;
			final int daysSearched = (int) Duration
					.between(searchQuery.getCheckinDate().atStartOfDay(), searchQuery.getCheckoutDate().atStartOfDay())
					.toDays();
			final int numberOfHotels = CollectionUtils.isEmpty(hotelInfos) ? 0 : hotelInfos.size();
			final int numberOfOptions =
					CollectionUtils.isEmpty(hotelInfos) ? 0 : getNumberOfOptionsInSearchResult(hotelInfos);
			final int numberOfRoomNights = numberOfOptions * roomSearched * daysSearched;

			HotelMethodExecutionInfo methodExecutionInfo =
					HotelMethodExecutionInfo.builder().hotelCount(numberOfHotels).optionCount(numberOfOptions)
							.roomNight(numberOfRoomNights).timeInMs(totalTimeInMs).type(methodExecutionType)
							.subtype(methodSubExecutionType).build();
			if (MapUtils.isEmpty(contextData.getMethodExecutionInfo().get(supplierName))) {
				contextData.getMethodExecutionInfo().put(supplierName, new LinkedHashMap<>());
			} else {
				Map<String, MethodExecutionInfo> executionInfo = contextData.getMethodExecutionInfo().get(supplierName);
				if (Objects.nonNull(executionInfo.get(methodName))) {
					HotelMethodExecutionInfo oldMethodExecutionInfo =
							(HotelMethodExecutionInfo) executionInfo.get(methodName);
					oldMethodExecutionInfo.setHotelCount(
							methodExecutionInfo.getHotelCount() + oldMethodExecutionInfo.getHotelCount());
					oldMethodExecutionInfo.setOptionCount(
							methodExecutionInfo.getOptionCount() + oldMethodExecutionInfo.getOptionCount());
					oldMethodExecutionInfo
							.setRoomNight(methodExecutionInfo.getRoomNight() + oldMethodExecutionInfo.getRoomNight());
					oldMethodExecutionInfo
							.setTimeInMs(methodExecutionInfo.getTimeInMs() + oldMethodExecutionInfo.getTimeInMs());
					methodExecutionInfo = oldMethodExecutionInfo;
				}
			}
			contextData.getMethodExecutionInfo().get(supplierName).put(methodName, methodExecutionInfo);
		} catch (Exception e) {
			log.info("Unable to set method execution info for search query {} and method name {}", searchQuery,
					methodName, e);
		}
	}

	public static ContextData getNewContextDataForMethodExecutionInfo(ContextData contextData) {
		Map<String, Map<String, MethodExecutionInfo>> methodExecutionInfo = contextData.getMethodExecutionInfo();
		contextData.setMethodExecutionInfo(null);
		ContextData methodContextData = contextData.deepCopy();
		methodContextData.setCheckPointInfo(null);
		methodContextData.setValueMap(contextData.getValueMap());
		methodContextData.setMethodExecutionInfo(methodExecutionInfo);
		return methodContextData;
	}

	public static Double getTargetFareValue(PriceInfo priceInfo, HotelFareComponent fareComponent) {
		if (priceInfo.getFareComponents().containsKey(fareComponent)) {
			return priceInfo.getFareComponents().get(fareComponent);
		} else if (priceInfo.getAddlFareComponents().containsKey(HotelFareComponent.TAF)) {
			return priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF).getOrDefault(fareComponent, 0.0d);
		}
		return 0.0d;
	}

	public static void updateClientCommission(Option option) {
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				Double perNightSAgentCommssion = getTargetFareValue(priceInfo, HotelFareComponent.SAC);
				Double perNightSupplierDiscount = getTargetFareValue(priceInfo, HotelFareComponent.SDS);
				if (perNightSAgentCommssion > 0.0) {
					double perNightGST = getTargetFareValue(priceInfo, HotelFareComponent.GST);
					double perNightTCS = getTargetFareValue(priceInfo, HotelFareComponent.TCS);
					Double totalTax = getTargetFareValue(priceInfo, HotelFareComponent.TTSF);
					Double supplierBasePrice = getTargetFareValue(priceInfo, HotelFareComponent.SBP);
					Double supplierNetPrice =
							(supplierBasePrice - perNightSupplierDiscount + totalTax + perNightGST + perNightTCS)
									- perNightSAgentCommssion;

					priceInfo.getFareComponents().put(HotelFareComponent.SGP,
							(supplierNetPrice + perNightSAgentCommssion));
					priceInfo.getFareComponents().put(HotelFareComponent.SNP, supplierNetPrice);
					priceInfo.getFareComponents().put(HotelFareComponent.BF,
							(supplierNetPrice + perNightSAgentCommssion));
					priceInfo.getMiscInfo().setSupplierPrice(priceInfo.getFareComponents().get(HotelFareComponent.BF));
				}
			}
		}
	}
}
