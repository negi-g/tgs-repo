package com.tgs.services.base.runtime.database;

import org.hibernate.envers.RevisionListener;

import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.ums.datamodel.User;

public class CustomRevisionListener implements RevisionListener {

	@Override
	public void newRevision(Object revisionEntity) {
		CustomRevision rev = (CustomRevision) revisionEntity;
		User user = null;
		String ip = null;
		if (SystemContextHolder.getContextData() != null) {
			user = SystemContextHolder.getContextData().getEmulateOrLoggedInUser();
			ip = SystemContextHolder.getContextData().getHttpHeaders()!=null ? SystemContextHolder.getContextData().getHttpHeaders().getIp()
					:null;
			ip = (ip!=null && ip.contains(",")) ? ip.substring(0, ip.indexOf(",")) : ip;
		}
		if (user != null) {
			rev.setUserName(user.getName());
			rev.setUserId(user.getUserId());
		}
		// ip may contains more than 1 IP , extract first one.
		rev.setUserIp((ip));
	}

}
