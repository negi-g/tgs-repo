package com.tgs.services.base.runtime.database.CustomTypes;

import com.tgs.services.base.datamodel.BillingEntityInfo;

public class BillingEntityInfoType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return BillingEntityInfo.class;
    }
}
