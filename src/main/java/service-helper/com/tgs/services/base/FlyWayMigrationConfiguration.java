package com.tgs.services.base;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class FlyWayMigrationConfiguration {

	@Value("${flyway.client.prefix}")
	private String prefix;

	@Value("${flyway.client.locations}")
	private String locations;

	/**
	 * Override default flyway initializer to do nothing
	 */
	@Bean
	FlywayMigrationInitializer flywayInitializer(Flyway flyway) {
		return new FlywayMigrationInitializer(flyway, null);
	}


	@Bean
	@DependsOn("flywayInitializer")
	FlywayMigrationInitializer clientFlywayInitializer(Flyway flyway) {
		flyway.setLocations(locations.split(","));
		flyway.setSqlMigrationPrefix(prefix);
		flyway.setTable("schema_version_client");
		return new FlywayMigrationInitializer(flyway, null);
	}

}
