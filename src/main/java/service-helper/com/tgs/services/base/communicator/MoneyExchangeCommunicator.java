package com.tgs.services.base.communicator;

import com.tgs.filters.MoneyExchangeInfoFilter;

public interface MoneyExchangeCommunicator {
	
	double getExchangeValue(Double amount, MoneyExchangeInfoFilter moneyExchangeInfoFilter, boolean isFallBackReq);

}
