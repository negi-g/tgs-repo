package com.tgs.services.base.runtime.spring;

import java.lang.reflect.Method;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerMethod;
import com.tgs.services.base.runtime.SystemContextHolder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SpringUtils {

	public static void setEndPointInContextData(HandlerMethod obj) {
		String endPoint = null;
		String basePath = "";
		String httpRequestMethod = null;
		try {
			Method method = obj.getMethod();
			if (method.getDeclaringClass().isAnnotationPresent(RestController.class)) {
				SystemContextHolder.getContextData().getRequestContextHolder()
						.setDefinedAnnotations(method.getAnnotations());
				RequestMapping[] requestAnnot = method.getDeclaredAnnotationsByType(RequestMapping.class);
				if (ArrayUtils.isNotEmpty(requestAnnot)) {
					try {
						endPoint = requestAnnot[0].value()[0];
						if (endPoint.startsWith("/"))
							endPoint = endPoint.substring(1);

						if (requestAnnot[0].value()[0].indexOf("{") != -1)
							endPoint = endPoint.substring(0, requestAnnot[0].value()[0].indexOf("{") - 2);
						RequestMapping[] requestMapping =
								method.getDeclaringClass().getDeclaredAnnotationsByType(RequestMapping.class);
						if (requestMapping != null && requestMapping.length > 0) {
							if (requestMapping[0].value()[0].startsWith("/"))
								basePath = requestMapping[0].value()[0].substring(1);
							else
								basePath = requestMapping[0].value()[0];
							if (basePath.endsWith("/")) {
								basePath = basePath.substring(0, basePath.length() - 1);
							}
							if (requestMapping[0].method().length > 0) {
								httpRequestMethod = requestMapping[0].method()[0].name();
							}
						}
						if (StringUtils.isBlank(httpRequestMethod) && requestAnnot[0].method().length > 0) {
							httpRequestMethod = requestAnnot[0].method()[0].name();
						}
					} catch (Exception e) {
						endPoint = "";
						basePath = "";
					}
				}
			}
			if (SystemContextHolder.getContextData().getRequestContextHolder() != null) {
				SystemContextHolder.getContextData().getRequestContextHolder().setBaseURL(basePath);
				SystemContextHolder.getContextData().getRequestContextHolder()
						.setRequestURI(SystemContextHolder.getContextData().getHttpRequest().getRequestURI());
				SystemContextHolder.getContextData().getRequestContextHolder().setEndPoint(basePath + "/" + endPoint);
				SystemContextHolder.getContextData().getRequestContextHolder().setHttpRequestMethod(httpRequestMethod);
			}
		} catch (Exception e) {
			log.error("Unable to retrieve event from request for endPoint {}", endPoint, e);
		}
	}
}
