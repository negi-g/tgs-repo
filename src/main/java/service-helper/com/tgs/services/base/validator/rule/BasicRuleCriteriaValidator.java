package com.tgs.services.base.validator.rule;

import java.util.function.Predicate;

import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.BasicRuleCriteria;
import com.tgs.services.base.validator.ListValidator;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

@Service
public class BasicRuleCriteriaValidator {

	@Autowired
	UserServiceCommunicator usCommunicator;

	@Autowired
	ListValidator listValidator;

	public void validateCriteria(Errors errors, String fieldName, BasicRuleCriteria criteria) {
		if (criteria == null) {
			return;
		}

		if (criteria instanceof BasicRuleCriteria) {

			GeneralBasicRuleCriteria generalCriteria = (GeneralBasicRuleCriteria) criteria;

			listValidator.validateUserIds(generalCriteria.getUserIds(), fieldName + ".userIds", errors,
					SystemError.INVALID_FBRC_USERID);

			listValidator.validateListForNullElements(generalCriteria.getRoles(), fieldName + ".roles", errors,
					SystemError.INVALID_USER_ROLE);

			listValidator.validateListForNullElements(generalCriteria.getChannelTypes(), fieldName + ".channelTypes",
					errors, SystemError.INVALID_CHANNEL_TYPE);

			listValidator.validateListForNullElements(generalCriteria.getPaymentMediums(),
					fieldName + ".paymentMediums", errors, SystemError.INVALID_PAYMENT_MEDIUM);

			Predicate<String> isNonEmpty = str -> StringUtils.isNotBlank(str);

			listValidator.validateStringList(generalCriteria.getBankNames(), fieldName + ".bankNames", errors,
					SystemError.INVALID_ELEMENT, isNonEmpty);

			if (generalCriteria.getPaxCount() != null && generalCriteria.getPaxCount() <= 0) {
				errors.rejectValue(fieldName + ".noOfPax", SystemError.INVALID_PAX_COUNT.errorCode(),
						SystemError.INVALID_PAX_COUNT.getMessage(generalCriteria.getPaxCount()));

			}

		}
	}
}
