package com.tgs.services.base.communicator;

import java.util.List;
import com.tgs.services.base.LogData;

public interface LogServiceCommunicator {

	public void store(List<LogData> logData);

	public void store(LogData logData);

	public List<LogData> getLogs(LogData logData);
}
