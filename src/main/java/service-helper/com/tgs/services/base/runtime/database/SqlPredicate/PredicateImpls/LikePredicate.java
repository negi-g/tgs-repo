package com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls;

import java.util.function.BiFunction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

import com.tgs.services.base.runtime.database.SqlPredicate.IPredicate;

public class LikePredicate extends IPredicate {

	@SuppressWarnings("unchecked")
	@Override
	protected BiFunction<Expression<String>, Expression<String>, Predicate> getFunctionForPredicateWithExpression(
			CriteriaBuilder criteriaBuilder) {
		return criteriaBuilder::like;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected BiFunction<Expression<String>, String, Predicate> getFunctionForPredicateWithObject(
			CriteriaBuilder criteriaBuilder) {
		return (exp, value) -> criteriaBuilder.like(criteriaBuilder.lower(exp),
				"%" + value.toString().toLowerCase() + "%");
	}

	@Override
	protected Class<?> targetValueType() {
		return String.class;
	}
}
