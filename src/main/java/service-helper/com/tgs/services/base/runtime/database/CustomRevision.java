package com.tgs.services.base.runtime.database;

import javax.persistence.Entity;

import org.hibernate.envers.RevisionEntity;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "customrev")
@RevisionEntity(CustomRevisionListener.class)
@Getter
@Setter
public class CustomRevision extends CustomRevisionEntity {

	private static final long serialVersionUID = 1L;
	private String userName;
	private String userId;
	private String userIp;
	

}
