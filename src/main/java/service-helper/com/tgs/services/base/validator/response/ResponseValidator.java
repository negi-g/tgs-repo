package com.tgs.services.base.validator.response;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ResponseValidator {

	final public void validateResponse(BaseResponse response) {
		ResponseValidationResult result = new ResponseValidationResult();
		validateResponse(response, result);
		if (!result.errors.isEmpty()) {
			String endoint = SystemContextHolder.getContextData().getRequestContextHolder().getEndPoint();
			String source = this.getClass().getName();
			log.error("Anomaly in response: endpoint={}, source={}, errors={}, response={}", endoint, source,
					result.errors, GsonUtils.getGson().toJson(response));
			throw new CustomGeneralException(SystemError.ACCESS_DENIED);
		}
	}

	protected abstract void validateResponse(BaseResponse response, ResponseValidationResult result);

	protected static class ResponseValidationResult {
		private List<String> errors = new ArrayList<>();

		protected void addError(String error) {
			errors.add(error);
		}
	}

}
