package com.tgs.services.base.validator.response;

import java.util.Map;
import org.springframework.stereotype.Service;
import com.tgs.services.ums.datamodel.User;

@Service
public class NullUserResponseUserMetaInfoValidator extends ResponseUserMetaInfoValidator {

	@Override
	protected void validateUserMetaInfo(User targetUser, Map<String, User> usersMetaInfo,
			ResponseValidationResult result) {
		if (targetUser == null) {
			result.addError("Null user is able to see user data.");
		} else {
			result.addError(targetUser.getUserId() + "(no role) is able to see user data.");
		}
	}

	@Override
	public boolean supports(User targetUser) {
		return targetUser == null || targetUser.getRole() == null;
	}

}
