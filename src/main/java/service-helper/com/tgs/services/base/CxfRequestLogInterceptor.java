package com.tgs.services.base;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.cxf.binding.soap.interceptor.SoapPreProtocolOutInterceptor;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.datamodel.User;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class CxfRequestLogInterceptor extends AbstractPhaseInterceptor<Message> {

	private LogData requestLogData;
	private String key;
	private String type;
	private boolean storeLog;

	private List<String> visibilityGroups;

	public CxfRequestLogInterceptor() {
		super(Phase.PRE_STREAM);
		addBefore(SoapPreProtocolOutInterceptor.class.getName());
	}
	
	public CxfRequestLogInterceptor(String type, String key, boolean storeLog, List<String> visibilityGroups) {
		super(Phase.PRE_STREAM);
		addBefore(SoapPreProtocolOutInterceptor.class.getName());
		this.key = key;
		this.type = type;
		this.storeLog = true;
		this.visibilityGroups = CollectionUtils.isNotEmpty(visibilityGroups) ? visibilityGroups : new ArrayList<>();
	}

	@Override
	public void handleMessage(Message message) {
		boolean isOutbound;
		isOutbound = message == message.getExchange().getOutMessage()
				|| message == message.getExchange().getOutFaultMessage();

		if (isOutbound && this.storeLog) {
			OutputStream os = message.getContent(OutputStream.class);
			CachedStream cs = new CachedStream();
			message.setContent(OutputStream.class, cs);
			message.getInterceptorChain().doIntercept(message);
			try {
				cs.flush();
				CachedOutputStream csnew = (CachedOutputStream) message.getContent(OutputStream.class);
				String soapMessage = IOUtils.toString(csnew.getInputStream());
				handleOutboundXmlMessage(soapMessage);
				cs.close();
				IOUtils.copy(new ByteArrayInputStream(soapMessage.getBytes("UTF-8")), os, 1024);
				os.flush();
				message.setContent(OutputStream.class, os);
				os.close();
				csnew.close();
			} catch (IOException ioe) {
				log.info("Not able to send request to webservice ", ioe);
			}
		}
	}

	@Override
	public void handleFault(Message message) {
	}

	protected void handleOutboundXmlMessage(String xml) {
		User user = SystemContextHolder.getContextData().getUser();
		requestLogData = LogData.builder().generationTime(LocalDateTime.now()).logData(xml).type(type + " Request")
				.key(key).userRole(UserUtils.getEmulatedUserRoleOrUserRole(user)).userId(UserUtils.getUserId(user))
				.logType("AirSupplierAPILogs").visibilityGroups(visibilityGroups).build();
		log.debug("logging request for type {} , key {} ", requestLogData.getType(), requestLogData.getKey());
		SystemContextHolder.getContextData().setSearchWatch(new StopWatch());
		SystemContextHolder.getContextData().getSearchWatch().start();

		LogUtils.store(Arrays.asList(requestLogData));
	}

	private class CachedStream extends CachedOutputStream {
		public CachedStream() {
			super();
		}

		@Override
		protected void doFlush() throws IOException {
			currentStream.flush();
		}

		@Override
		protected void doClose() throws IOException {
		}

		@Override
		protected void onWrite() throws IOException {
		}
	}

}
