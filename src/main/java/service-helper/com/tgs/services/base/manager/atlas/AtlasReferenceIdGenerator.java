package com.tgs.services.base.manager.atlas;

import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.manager.DefaultReferenceIdGenerator;
import com.tgs.services.gms.datamodel.ReferenceIdConfiguration;

@Service
public class AtlasReferenceIdGenerator extends DefaultReferenceIdGenerator {

	protected String getPrefix(ProductMetaInfo productMetaInfo, ReferenceIdConfiguration idConfiguration) {
		if (Product.WALLET_TOPUP.equals(productMetaInfo.getProduct())) {
			return Product.WALLET_TOPUP.getPrefix();
		}
		if (Product.AMENDMENT.equals(productMetaInfo.getProduct())) {
			return idConfiguration.getAmdIdPrefix();
		}
		return super.getPrefix(productMetaInfo, idConfiguration);
	}
}
