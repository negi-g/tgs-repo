package com.tgs.services.base.runtime;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.tgs.services.base.ActionChangeCriterion;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.FetchType;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;

@Component
public class ActionChangeValidator {

	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;

	public List<String> isValidRequest(List<String> fieldsInRequest, String fromValue, String toValue, String key) {
		List<Document> docList = gmsCommunicator.fetchDocument(CollectionServiceFilter.builder().fetchType(FetchType.CACHE).key(key).enabled(true).types(Arrays.asList("action_change_fields")).build());
		if(CollectionUtils.isNotEmpty(docList)) {
			String data = docList.get(0).getData();
			ActionChangeCriterion actionChangeCriteria = new Gson().fromJson(data, ActionChangeCriterion.class);
			List<String> fields = actionChangeCriteria.getFields();
			if(StringUtils.equals(fromValue, actionChangeCriteria.getFromAction())
					&& StringUtils.equals(toValue, actionChangeCriteria.getToAction())) {
				fields.removeAll(fieldsInRequest);
				if (CollectionUtils.isNotEmpty(fields)) {
					return fields;
				}
			}

		}
		return Collections.emptyList();
	}
	
}
