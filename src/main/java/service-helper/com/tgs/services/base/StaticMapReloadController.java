package com.tgs.services.base;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.ReloadRequest;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;

@EnableScheduling
@RequestMapping("/base/v1")
@RestController
@ApiIgnore
@Slf4j
public class StaticMapReloadController {

	@Autowired
	ApplicationContext context;

	@Value("${env}")
	private String env;

	@Autowired
	Environment environment;

	@Autowired
	GeneralCachingCommunicator cachingService;

	@Autowired
	SyncService syncService;

	private static final String STATIC_MAP_KEY = "STATIC_MAP";
	private static final int ttl = 10;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reload/{operation_type}/{type}", method = RequestMethod.GET)
	protected BaseResponse reloadAll(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("type") String type, @PathVariable("operation_type") String operationType) throws Exception {
		log.info("Doing manual reload by user {}", SystemContextHolder.getContextData().getUser());
		Set<Class<? extends InMemoryInitializer>> allIntializers = new HashSet<>();
		if ("all".equalsIgnoreCase(type)) {
			allIntializers =
					TgsObjectUtils.findAllMatchingInheritedTypes(InMemoryInitializer.class, "com.tgs.services");
			removeHeavyIntialize(allIntializers);
			reload(allIntializers, true, true);
		} else {
			allIntializers.add((Class<? extends InMemoryInitializer>) Class.forName(type));
			reload(allIntializers, true, false);
		}

		return new BaseResponse();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/reload", method = RequestMethod.POST)
	protected BaseResponse reloadType(HttpServletRequest request, HttpServletResponse response,
			@RequestBody ReloadRequest reloadRequestType) throws Exception {
		log.info("Doing manual reload by user {}", SystemContextHolder.getContextData().getUser());
		Set<Class<? extends InMemoryInitializer>> allIntializers = new HashSet<>();
		ContextData orgContextData = SystemContextHolder.getContextData();
		allIntializers.add((Class<? extends InMemoryInitializer>) Class.forName(reloadRequestType.getType()));
		reload(allIntializers, true, false);
		if (reloadRequestType.isReloadInmemory()) {
			SystemContextHolder.setContextData(orgContextData);
			syncService.sync("gms", reloadRequestType);
		}
		return new BaseResponse();
	}

	@Scheduled(initialDelay = 5 * 1000, fixedDelay = 7 * 24 * 60 * 60 * 1000)
	public void reloadAll() throws Exception {
		reload(null, false, true);
	}

	private boolean isReloadAllowed() {
		LocalDateTime lastReloadTime = getLastReloadedAt();
		if (ObjectUtils.isEmpty(lastReloadTime)) {
			return true;
		} else {
			throw new CustomGeneralException("Last Reloaded At: " + lastReloadTime + ", Cache can be reloaded after "
					+ lastReloadTime.plusMinutes(ttl).plusHours(1));
		}
	}

	public void reload(Set<Class<? extends InMemoryInitializer>> allIntializers, boolean manualReload,
			boolean isLastReloadCheckRequired) throws Exception {

		if (isLastReloadCheckRequired && isReloadAllowed()) {
			setLastReloadedAt();
		}

		if (allIntializers == null) {
			allIntializers = new HashSet<>();
			allIntializers =
					TgsObjectUtils.findAllMatchingInheritedTypes(InMemoryInitializer.class, "com.tgs.services");
			removeNonEnvironmentIntializers(allIntializers);
			removeHeavyIntialize(allIntializers);
		}
		StopWatch stopWatch = new StopWatch();
		ContextData contextData = ContextData.builder().build();
		SystemContextHolder.setContextData(contextData);
		allIntializers.forEach(intializer -> {
			try {
				if (isValidInitialization(intializer) || manualReload) {
					stopWatch.start();
					log.debug("Intializing {}", intializer.getName());
					Object bean = context.getBean(Class.forName(intializer.getName()));
					Method method = bean.getClass().getMethod("initialize", String.class);
					method.invoke(bean, intializer.getName());
					log.debug(" Total time took to intialize {} , is {} messagingService", intializer.getName(),
							stopWatch.getTime());
				}
			} catch (Exception e) {
				log.error("Unable to intialize {}", intializer.getName(), e);
			} finally {
				stopWatch.stop();
				stopWatch.reset();
			}
		});
		log.info("Reload completed !!!");
	}

	private void removeNonEnvironmentIntializers(Set<Class<? extends InMemoryInitializer>> allIntializers) {
		String helperGroup = environment.getProperty("INITIALIZER_GROUP");
		log.info("Value of helper initializerGroup string = {} ", StringUtils.upperCase(helperGroup));
		if (StringUtils.isBlank(helperGroup)) {
			log.info("No Helper initializer group provided");
			return;
		}
		for (Iterator<Class<? extends InMemoryInitializer>> itr = allIntializers.iterator(); itr.hasNext();) {
			Class<? extends InMemoryInitializer> intializer = itr.next();
			InitializerGroup group = intializer.getDeclaredAnnotation(InitializerGroup.class);
			if (Objects.isNull(group)) {
				continue;
			}
			InitializerGroup.Group gs = group.group();
			if (!StringUtils.upperCase(helperGroup).contains(gs.name())) {
				log.info("Removing helper {} of helper initializer group {} ", intializer, gs.name());
				itr.remove();
			}
		}
	}

	public boolean isValidInitialization(Class<? extends InMemoryInitializer> intializer) {
		if ("AirportHelper".contains(intializer.getName()) || "AirlineHelper".contains(intializer.getName())) {
			return false;
		}
		return true;
	}

	private LocalDateTime getLastReloadedAt() {
		String helperGroup = getIntializerHelperGroupKey();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
				.set(CacheSetName.STATIC_RELOAD.getName()).keys(new String[] {helperGroup}).compress(false)
				.plainData(true).bins(new String[] {BinName.STOREAT.getName()}).build();

		Map<String, Map<String, String>> lastReloadedAt = cachingService.get(metaInfo, String.class);
		if (MapUtils.isEmpty(lastReloadedAt)) {
			return null;
		}
		for (String key : lastReloadedAt.keySet()) {
			Map<String, String> reloadInfo = lastReloadedAt.get(key);
			LocalDateTime lastProcessedAt = LocalDateTime.parse(reloadInfo.get(BinName.STOREAT.getName()));
			return lastProcessedAt;
		}
		return null;
	}

	public void setLastReloadedAt() {
		String helperGroup = getIntializerHelperGroupKey();
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.STOREAT.name(), LocalDateTime.now().toString());
		// store for 3 hours
		cachingService.store(
				CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
						.set(CacheSetName.STATIC_RELOAD.getName()).key(helperGroup).build(),
				binMap, false, true, 10800);
	}

	@Deprecated
	public void removeHeavyIntialize(Set<Class<? extends InMemoryInitializer>> allIntializers) throws Exception {
		allIntializers.remove(Class.forName("com.tgs.services.hms.helper.HotelStaticDataPersistenceHelper"));
		allIntializers.remove(Class.forName("com.tgs.services.hms.helper.HotelSupplierMappingPersistenceHelper"));
		allIntializers.remove(Class.forName("com.tgs.services.hms.helper.SupplierHotelIdMappingHelper"));
		// allIntializers.remove(Class.forName("com.tgs.services.hms.helper.HotelRegionStaticDataPersistenceHelper"));
		// allIntializers.remove(Class.forName("com.tgs.services.hms.helper.HotelMealPersistenceHelper"));
		// allIntializers.remove(Class.forName("com.tgs.services.fms.helper.SourceRouteInfoHelper"));
	}

	private String getIntializerHelperGroupKey() {
		return org.apache.commons.lang3.ObjectUtils.firstNonNull(environment.getProperty("INITIALIZER_GROUP"),
				STATIC_MAP_KEY);
	}

}
