package com.tgs.services.base.runtime.envers;

import java.util.Set;

import org.hibernate.event.spi.PostUpdateEvent;
import org.springframework.stereotype.Component;

import com.tgs.services.base.runtime.PostUpdateEnversListener;

import lombok.extern.slf4j.Slf4j;

/**
 * If there is no change between two revisions of entity this listener will skip the audit entry of 2nd event
 */
@Component
@Slf4j
public class NoChangeDetectionEnversListener implements PostUpdateEnversListener {

	@Override
	public boolean isValidEventForAudit(PostUpdateEvent event) {
		Object[] oldState = event.getOldState();
		if(oldState!=null) {
			Set<String> differentFields = getDifferentFields(event);
			log.debug("[NoChangeDetectionEnversListener] Different fields are : {}", differentFields.toString());
			if(differentFields.isEmpty() || (differentFields.size()==1 && differentFields.contains("processedOn"))) {
				return false;
			}
		}
		return true;	
	}
}
