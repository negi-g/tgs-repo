package com.tgs.services.base;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.restmodel.AirSearchRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("beforeCreateSearchQueryValidator")
public class SearchQueryValidator implements Validator {

	@Autowired
	private OrderServiceCommunicator orderCommunicator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof AirSearchRequest) {
			ServiceUtils.isProductEnabled(Product.AIR);
			AirSearchRequest searchRequest = (AirSearchRequest) target;
			try {
				AirSearchQuery searchQuery = searchRequest.getSearchQuery();
				User user = SystemContextHolder.getContextData().getUser();

				if (Objects.isNull(searchQuery) && StringUtils.isBlank(searchRequest.getSearchId())) {
					errors.rejectValue("searchQuery", SystemError.INVALID_SEARCH_REQUST.errorCode(),
							SystemError.INVALID_SEARCH_REQUST.getMessage());
					return;
				}

				if (Objects.isNull(searchQuery.getCabinClass()))
					errors.rejectValue("searchQuery.cabinClass", SystemError.INVALID_CABIN_CLASS.errorCode(),
							SystemError.INVALID_CABIN_CLASS.getMessage());

				if (MapUtils.isEmpty(searchQuery.getPaxInfo()) || Optional
						.ofNullable(searchQuery.getPaxInfo().getOrDefault(PaxType.ADULT, 0)).orElse(0) == 0) {
					errors.rejectValue("searchQuery.paxInfo", SystemError.ADULT_VALIDATION.errorCode(),
							SystemError.ADULT_VALIDATION.getMessage());
					return;
				}

				if (Optional.ofNullable(searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0))
						.orElse(0) > searchQuery.getPaxInfo().getOrDefault(PaxType.ADULT, 0)) {
					errors.rejectValue("searchQuery.paxInfo", SystemError.INFANT_VALIDATION.errorCode(),
							SystemError.INFANT_VALIDATION.getMessage());
					return;
				}

				if (UserUtils.isCorporateEmployee(user) && (searchQuery.getPaxInfo().getOrDefault(PaxType.ADULT, 0) > 1
						|| searchQuery.getPaxInfo().getOrDefault(PaxType.CHILD, 0) > 0
						|| searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0) > 0)) {
					errors.rejectValue("searchQuery.paxInfo",
							SystemError.CORPORATE_EMPLOYEE_SEARCH_VALIDATION.errorCode(),
							SystemError.CORPORATE_EMPLOYEE_SEARCH_VALIDATION.getMessage());
					return;
				}

				// if (searchQuery.getPaxInfo().get(PaxType.CHILD) >
				// searchQuery.getPaxInfo().get(PaxType.ADULT)) {
				// errors.rejectValue("searchQuery.paxInfo",
				// SystemError.CHILD_VALIDATION.errorCode(),
				// SystemError.CHILD_VALIDATION.getMessage());
				// }

				if (CollectionUtils.isEmpty(searchQuery.getRouteInfos())) {
					errors.rejectValue("searchQuery.routeInfos", SystemError.ROUTES_EMPTY.errorCode(),
							SystemError.ROUTES_EMPTY.getMessage());
					return;
				}

				if (CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())
						&& searchQuery.getPreferredAirline().size() > 10) {
					errors.rejectValue("searchQuery.preferredAirline", SystemError.PREFERREDAIRLINE_SIZE.errorCode(),
							SystemError.PREFERREDAIRLINE_SIZE.getMessage());
					return;
				}

				if (CollectionUtils.isNotEmpty(searchQuery.getRouteInfos())) {
					LocalDate previousDate = LocalDate.now();
					LocalDate yearDate = LocalDate.now().plusYears(1);
					Map<String, Boolean> routeMap = new HashMap<>();
					for (RouteInfo routeInfo : searchQuery.getRouteInfos()) {
						if (Objects.isNull(routeInfo.getTravelDate())
								|| !(routeInfo.getTravelDate().isAfter(previousDate)
										|| routeInfo.getTravelDate().isEqual(previousDate))) {
							errors.rejectValue("searchQuery.routeInfos", SystemError.TRAVEL_DATE_VALIDATION.errorCode(),
									SystemError.TRAVEL_DATE_VALIDATION.getMessage());
							break;
						}

						if (routeInfo.getTravelDate().isAfter(yearDate)) {
							errors.rejectValue("searchQuery.routeInfos",
									SystemError.TRAVEL_DATE_YEAR_VALIDATION.errorCode(),
									SystemError.TRAVEL_DATE_YEAR_VALIDATION.getMessage());
							break;
						}

						if (routeInfo.getFromCityOrAirport() == null
								|| routeInfo.getFromCityOrAirport().getCode() == null
								|| routeInfo.getFromCityOrAirport().getCode()
										.equals(routeInfo.getToCityOrAirport().getCode())) {
							errors.rejectValue("searchQuery.routeInfos",
									SystemError.ORIGIN_DESTINATION_VALIDATION.errorCode(),
									SystemError.ORIGIN_DESTINATION_VALIDATION.getMessage());
							break;
						}
						String key = routeInfo.getFromCityOrAirport().getCode()
								+ routeInfo.getFromCityOrAirport().getCode() + routeInfo.getTravelDate();
						Boolean previousValue = routeMap.put(key, true);
						if (previousValue != null) {
							errors.rejectValue("searchQuery.routeInfos",
									SystemError.SAME_SECTOR_SAME_DAY_NOT_ALLOWED.errorCode(),
									SystemError.SAME_SECTOR_SAME_DAY_NOT_ALLOWED.getMessage());
							break;
						}

						previousDate = routeInfo.getTravelDate();
					}
				}

				if ((Optional.ofNullable(searchQuery.getPaxInfo().getOrDefault(PaxType.ADULT, 0)).orElse(0)
						+ Optional.ofNullable(searchQuery.getPaxInfo().getOrDefault(PaxType.CHILD, 0)).orElse(0)) > 9) {
					errors.rejectValue("searchQuery.paxInfo", SystemError.PASSENGER_VALIDATION.errorCode(),
							SystemError.PASSENGER_VALIDATION.getMessage());
				}

				if (searchQuery.isPNRCreditSearch()) {
					try {
						orderCommunicator.validateSearchQueryOnPNR(searchQuery);
						LocalDateTime csExpiryDate =
								searchQuery.getSearchModifiers().getPnrCreditInfo().getCSExpiryDate();
						if (LocalDateTime.now().isAfter(csExpiryDate)) {
							errors.rejectValue("searchQuery", "Credit Shell Expired Already");
						}
					} catch (CustomGeneralException e) {
						errors.rejectValue("searchQuery", e.getError().errorCode(), e.getError().getMessage());
					}
				}
			} catch (Exception e) {
				log.error("Not able to parse air search Request {}", GsonUtils.getGson().toJson(searchRequest), e);
				throw new CustomGeneralException(SystemError.INVALID_REQUEST);
			}
		}
	}
}
