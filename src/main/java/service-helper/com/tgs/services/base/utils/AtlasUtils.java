package com.tgs.services.base.utils;

import java.util.Random;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.BankAccountInfo;
import com.tgs.services.base.enums.UserRole;

public class AtlasUtils {

	public static String generateUserId(long id, UserRole role) {
		int idLength = String.valueOf(id).length();
		int num = generateRandomInt(idLength < 6 ? 6 - idLength : 1);
		String clientPrefix = UserRole.WHITELABEL_PARTNER.equals(role) ? "60" : "";
		if (StringUtils.isNotBlank(clientPrefix)) {
			return clientPrefix + role.getPrefix() + num + id;
		}
		return num + "" + id;
	}

	/**
	 * Generates a random integer of specific length
	 */
	public static int generateRandomInt(int length) {
		return new Random().ints((int) Math.pow(10, length - 1), ((int) Math.pow(10, length)) - 1).findFirst()
				.getAsInt();
	}

	public static BankAccountInfo generateDBSAccountNumber(String userId) {
		return BankAccountInfo.builder().accountNumber("9TJK" + AtlasUtils.generateRandomInt(4) + userId)
				.bankName("DBS Bank Pvt Ltd, Mumbai").ifscCode("DBSS0IN0811").accountHolderName("Tripjack pvt ltd")
				.build();

	}

}
