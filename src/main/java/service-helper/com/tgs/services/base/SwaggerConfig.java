package com.tgs.services.base;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

import springfox.documentation.RequestHandler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket umsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("ums").select()
				.apis(exactPackage("com.tgs.services.ums")).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo("Ums Service", "Information related to Ums Service")).enable(true)
				.securityContexts(Lists.newArrayList(securityContext())).securitySchemes(Lists.newArrayList(apiKey()));
	}

	@Bean
	public Docket gmsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("gms").select()
				.apis(exactPackage("com.tgs.services.gms")).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo("General Service", "Information related to General Service")).enable(true)
				.securityContexts(Lists.newArrayList(securityContext())).securitySchemes(Lists.newArrayList(apiKey()));
	}

	@Bean
	public Docket cmsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("cms").select()
				.apis(exactPackage("com.tgs.services.cms")).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo("Commission Service", "Information related to Commission Service")).enable(true)
				.securityContexts(Lists.newArrayList(securityContext())).securitySchemes(Lists.newArrayList(apiKey()));
	}

	@Bean
	public Docket fmsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("fms").select()
				.apis(exactPackage("com.tgs.services.fms")).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo("Flight Service", "Information related to flight product")).enable(true)
				.securityContexts(Lists.newArrayList(securityContext())).securitySchemes(Lists.newArrayList(apiKey()));
	}

	@Bean
	public Docket omsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("oms").select()
				.apis(exactPackage("com.tgs.services.oms")).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo("Order Service", "Information related to order service")).enable(true)
				.securityContexts(Lists.newArrayList(securityContext())).securitySchemes(Lists.newArrayList(apiKey()));
	}

	@Bean
	public Docket pmsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("pms").select()
				.apis(exactPackage("com.tgs.services.pms")).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo("Payment Service", "Information related to payment service")).enable(true)
				.securityContexts(Lists.newArrayList(securityContext())).securitySchemes(Lists.newArrayList(apiKey()));
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/anyPath.*"))
				.build();
	}

	List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return Lists.newArrayList(new SecurityReference("AUTHORIZATION", authorizationScopes));
	}

	/**
	 * Creates an object containing API information including author name, email,
	 * version, license, etc.
	 *
	 * @param title
	 *            API title
	 * @param description
	 *            API description
	 * @return API information
	 */
	private ApiInfo apiInfo(String title, String description) {
		return new ApiInfo(title, description, "1.0", "terms of controller url", "Ashu Gupta", "license",
				"license url");
	}

	private static Predicate<RequestHandler> exactPackage(final String pkg) {
		return input -> input.declaringClass().getPackage().getName().contains(pkg);
	}

	private ApiKey apiKey() {
		return new ApiKey("Authorization", "Authorization", "header");
	}

	@Bean
	SecurityConfiguration security() {
		return new SecurityConfiguration(null, null, null, // realm Needed for authenticate button to work
				null, // appName Needed for authenticate button to work
				"", // apiKeyValue
				ApiKeyVehicle.HEADER, "Authorization", // apiKeyName
				null);
	}
}
