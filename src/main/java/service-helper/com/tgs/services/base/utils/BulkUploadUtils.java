package com.tgs.services.base.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tgs.services.base.restmodel.BulkUploadResponse;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadType;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BulkUploadUtils {

	private static GeneralCachingCommunicator generalCachingComm;
	
	private static int ttl = 24 * 60 * 60;
	
	public static String INITIALIZATION_NOT_REQUIRED = "Initialization_Not_Required";

	public static void init(GeneralCachingCommunicator generalCachingCommunicator) {
		generalCachingComm = generalCachingCommunicator;
	}

	public static void cacheBulkInfoResponse(BulkUploadInfo uploadInfo, String jobId) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(jobId).namespace(CacheNameSpace.GENERAL_PURPOSE.name())
				.compress(true).set(CacheSetName.BULK_UPLOAD.getName()).build();
		generalCachingComm.store(metaInfo, BinName.UPLOADRESPONSE.getName(), Arrays.asList(uploadInfo), true, false,
				ttl);		
	}

	public static BulkUploadResponse getCachedBulkUploadResponse(String jobId) {
		BulkUploadResponse bulkResponse = new BulkUploadResponse();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(jobId).namespace(CacheNameSpace.GENERAL_PURPOSE.name())
				.compress(true).set(CacheSetName.BULK_UPLOAD.getName()).build();
		log.info("getCachedBulkUploadResponse: fetching cachedData for jobId {}", jobId);
		Map<String, List<BulkUploadInfo>> cachedBulkInfoMap =
				generalCachingComm.getList(metaInfo, BulkUploadInfo.class, true, false, new String[0]);
		log.info("getCachedBulkUploadResponse: successfully fetched cachedData for jobId {}", jobId);
		List<BulkUploadInfo> cachedBulkInfoList = new ArrayList<>();
		for (String cachedKey : cachedBulkInfoMap.keySet()) {
			List<BulkUploadInfo> cachedBulkInfo = cachedBulkInfoMap.get(cachedKey);
			cachedBulkInfoList.addAll(cachedBulkInfo);
		}
		bulkResponse.setUploadId(jobId);
		bulkResponse.setUploadInfo(cachedBulkInfoList);

		log.info("getCachedBulkUploadResponse: check completion for jobId {}", jobId);
		String beanName = BulkUploadUtils.isBulkInfoResponseCompletelySaved(jobId);
		log.info("getCachedBulkUploadResponse: completed for jobId {}, beanName = {}", jobId, beanName);

		if (StringUtils.isEmpty(beanName)) {
			bulkResponse.setRetryInSecond(3);
		} else if (!INITIALIZATION_NOT_REQUIRED.equals(beanName)){
			InMemoryInitializer inMemoryInitializer =
					(InMemoryInitializer) SpringContext.getApplicationContext().getBean(beanName);
			inMemoryInitializer.initialize(beanName);
		}
		return bulkResponse;
	}

	public static String isBulkInfoResponseCompletelySaved(String jobId) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
				.set(CacheSetName.BULK_UPLOAD.getName()).key(jobId).build();
		Map<String, String> cachedCompleteAtMap = generalCachingComm.get(metaInfo, String.class, false, true, new String[] {BinName.COMPLETEAT.getName(), BinName.BEANNAME.getName()});
		if (MapUtils.isEmpty(cachedCompleteAtMap))
			return null;
		return cachedCompleteAtMap.get(BinName.BEANNAME.getName());
	}

	public static void storeBulkInfoResponseCompleteAt(String jobId, String beanName) {
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.COMPLETEAT.getName(), LocalDateTime.now().toString());
		binMap.put(BinName.BEANNAME.getName(), beanName);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.name())
				.set(CacheSetName.BULK_UPLOAD.getName()).key(jobId).build();

		generalCachingComm.store(metaInfo, binMap, false, true, ttl);
	}

	public static BulkUploadResponse processBulkUploadRequest(Runnable bulkUploadTask, UploadType uploadType,
			String jobId) throws InterruptedException {
		BulkUploadResponse bulkResponse = new BulkUploadResponse();
		Thread bulkUploadThread = new Thread(bulkUploadTask);
		if (uploadType.equals(UploadType.SYNCHRONOUS)) {
			bulkUploadThread.start();
			bulkUploadThread.join();
			bulkResponse = getCachedBulkUploadResponse(jobId);
		} else if (uploadType.equals(UploadType.ASYNCHRONOUS)) {
			bulkUploadThread.start();
			bulkResponse.setUploadId(jobId);
			bulkResponse.setRetryInSecond(3);
		}
		return bulkResponse;
	}

	public static String generateRandomJobId() {
		String randomNumber = RandomStringUtils.random(8, false, true);
		return StringUtils.join("BU_", randomNumber);
	}

}
