package com.tgs.services.base.runtime.database.SqlPredicate;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.tgs.services.base.runtime.database.PsqlFunction;

public abstract class IPredicate {

	final public void createAndAdd(Root<?> root, List<String> path, CriteriaBuilder criteriaBuilder,
			List<Predicate> predicates, Object value, boolean isNegated) {
		if (!supports(value)) {
			return;
		}

		// attribute of the root
		String attributeName = path.get(0);

		Expression<?> leftHandExpression = evaluateLeftHandExpression(criteriaBuilder, root.get(attributeName), path);
		Object rightHandExpression = evaluateRightHandExpression(criteriaBuilder, value, isLefHandExpessionJsonb(path));

		Predicate predicate = createPredicate(criteriaBuilder, leftHandExpression, rightHandExpression);
		if (isNegated) {
			predicate = predicate.not();
		}
		predicates.add(predicate);
	}

	/**
	 * 
	 * @param value
	 * @return {@code false} if {@code value} is not an instance of {@code targetValueType()}, otherwise {@code true}.
	 *         Returns {@code true} if {@code targetValueType()} is {@code null}.
	 */
	private boolean supports(Object value) {
		Class<?> targetType = targetValueType();
		if (targetType == null) {
			return value != null;
		}
		return targetType.isInstance(value);
	}

	/**
	 * Create and return jsonb extract expression if {@code path.size() > 0}, otherwise return {@code attributePath}.
	 * 
	 * @param criteriaBuilder
	 * @param attributePath
	 * @param path
	 * @return
	 */
	private Expression<?> evaluateLeftHandExpression(CriteriaBuilder criteriaBuilder, Path<?> attributePath,
			List<String> path) {
		if (!isJsonAttribute(path)) {
			return attributePath;
		}

		return createJsonbExtractExpression(criteriaBuilder, attributePath, path);
	}

	/**
	 * First element of {@code path} is ignored while using jsonb_extract_path function.
	 * <p>
	 * E.g., if attribute is "person" and path is "[person, name]", "person" must be ignored.
	 * 
	 * @param criteriaBuilder
	 * @param attributePath jsonb attribute
	 * @param path
	 * @return
	 */
	private Expression<?> createJsonbExtractExpression(CriteriaBuilder criteriaBuilder, Path<?> attributePath,
			List<String> path) {
		Expression<?>[] args = new Expression[path.size()];

		// first param is attribute
		args[0] = attributePath;

		// path params
		for (int i = 1; i < args.length; i++) {
			args[i] = criteriaBuilder.literal(path.get(i));
		}

		return jsonbExtractPathFunction().getExpression(criteriaBuilder, String.class, args);
	}

	/**
	 * If left hand expression of this predicate is jsonb, convert value (or each element of value if it is a
	 * collection) to jsonb. Otherwise return value as is.
	 * 
	 * @param criteriaBuilder
	 * @param value
	 * @param isLefHandExpessionJsonb
	 * @return
	 */
	private Object evaluateRightHandExpression(CriteriaBuilder criteriaBuilder, Object value,
			boolean isLefHandExpessionJsonb) {
		if (!isLefHandExpessionJsonb) {
			return value;
		}

		if (value instanceof Collection) {
			Collection<?> coll = (Collection<?>) value;
			List<Expression<?>> jsonbCollection = new ArrayList<>();
			for (Object val : coll) {
				jsonbCollection.add(PsqlFunction.TO_JSONB.getExpression(criteriaBuilder, String.class,
						criteriaBuilder.literal(val.toString()).as(val.getClass())));
			}
			return jsonbCollection.get(0);
		}

		return PsqlFunction.TO_JSONB.getExpression(criteriaBuilder, String.class,
				criteriaBuilder.literal(value.toString()).as(value.getClass()));
	}

	private boolean isJsonAttribute(List<String> path) {
		return path.size() > 1;
	}

	/**
	 * 
	 * @param path
	 * @return {@code true} if we are inside jsonb attribute and {@code jsonbExtractPathFunction()} used returns jsonb,
	 *         otherwise {@code false}
	 */
	private boolean isLefHandExpessionJsonb(List<String> path) {
		return isJsonAttribute(path) && jsonbExtractPathFunction().returnType() == Types.JAVA_OBJECT;
	}

	private PsqlFunction jsonbExtractPathFunction() {
		Class<?> targetType = targetValueType();
		if (targetType != null
				&& (String.class.isAssignableFrom(targetType) || Collection.class.isAssignableFrom(targetType))) {
			return PsqlFunction.JSONB_EXTRACT_PATH_TEXT;
		}

		return PsqlFunction.JSONB_EXTRACT_PATH;
	}

	/**
	 * Respective {@code BiFunction} is used for creating {@code Predicate}.
	 * 
	 * @param criteriaBuilder
	 * @param exp
	 * @param value
	 * @return
	 */
	@SuppressWarnings("unchecked")
	final protected <T> Predicate createPredicate(CriteriaBuilder criteriaBuilder, Expression<? extends T> exp,
			Object value) {
		if (value instanceof Expression) {
			return getFunctionForPredicateWithExpression(criteriaBuilder).apply(exp, (Expression<? extends T>) value);
		}

		return getFunctionForPredicateWithObject(criteriaBuilder).apply(exp, value);
	}

	protected abstract Class<?> targetValueType();

	protected abstract <T> BiFunction<Expression<? extends T>, Expression<? extends T>, Predicate> getFunctionForPredicateWithExpression(
			CriteriaBuilder criteriaBuilder);

	protected abstract <T> BiFunction<Expression<?>, T, Predicate> getFunctionForPredicateWithObject(
			CriteriaBuilder criteriaBuilder);
}
