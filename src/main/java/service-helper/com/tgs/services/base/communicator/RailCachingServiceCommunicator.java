package com.tgs.services.base.communicator;

import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;

public interface RailCachingServiceCommunicator {

	/**
	 * expiration will in seconds
	 */
	public <V> boolean store(String key, String bin, V value, CacheMetaInfo metaInfo);

	/**
	 * If you want to fetch value corresponding to a particular bin
	 * 
	 * @param key
	 * @param classofV
	 * @param binValue
	 * @return
	 */
	public <V> V fetchValue(String key, Class<V> classofV, String set, String binValue);

}
