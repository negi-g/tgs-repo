package com.tgs.services.base.runtime.database.CustomTypes;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.tgs.services.ps.datamodel.Policy;

public class UserPolicyListType extends CustomUserType {

	@Override
	public Class<?> returnedClass() {
		return new ArrayList<Policy>().getClass();
	}

	@SuppressWarnings("serial")
	@Override
	public Type returnedType() {
		return new TypeToken<List<Policy>>() {
		}.getType();
	}

}
