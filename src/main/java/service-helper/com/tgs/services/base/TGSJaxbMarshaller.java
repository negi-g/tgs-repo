package com.tgs.services.base;

import java.util.HashMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingDetailsRequestV2;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingDetailsResponseV2;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingRequestV3;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingResponseV3;
import com.tgs.services.hms.datamodel.agoda.precheck.CancellationRequestV2;
import com.tgs.services.hms.datamodel.agoda.precheck.CancellationResponseV2;
import com.tgs.services.hms.datamodel.agoda.precheck.ConfirmCancellationRequestV2;
import com.tgs.services.hms.datamodel.agoda.precheck.ConfirmCancellationResponseV2;
import com.tgs.services.hms.datamodel.agoda.precheck.PrecheckRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.PrecheckResponse;
import com.tgs.services.hms.datamodel.agoda.search.AgodaSearchRequest;
import com.tgs.services.hms.datamodel.agoda.search.AgodaSearchResponse;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotelFeed;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotelFeedFull;
import com.tgs.services.hms.datamodel.agoda.statc.CityFeed;
import com.tgs.services.hms.datamodel.agoda.statc.CountryFeed;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.Results;

@Configuration
public class TGSJaxbMarshaller {

	@Bean
	public Jaxb2Marshaller jaxb2Marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setClassesToBeBound(new Class[] {AgodaSearchRequest.class, AgodaSearchResponse.class,
				CountryFeed.class, CityFeed.class, AgodaHotelFeed.class, AgodaHotelFeedFull.class,
				PrecheckRequest.class, PrecheckResponse.class, BookingRequestV3.class, BookingResponseV3.class,
				BookingDetailsResponseV2.class, BookingDetailsRequestV2.class, CancellationRequestV2.class,
				CancellationResponseV2.class, ConfirmCancellationRequestV2.class, ConfirmCancellationResponseV2.class,Customer.class,Results.class

		});
		marshaller.setMarshallerProperties(new HashMap<String, Object>() {
			{
				put(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
			}
		});
		return marshaller;
	}

}
