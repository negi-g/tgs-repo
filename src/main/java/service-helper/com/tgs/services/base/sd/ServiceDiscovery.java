package com.tgs.services.base.sd;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;
import javax.jmdns.ServiceTypeListener;
import javax.naming.NamingException;

public class ServiceDiscovery {

	// private DirContext dirContext;

	private JmDNS jmdns;

	private static String type = "_app._tcp.local.";

	void init() throws NamingException, UnknownHostException, IOException {
		// Hashtable env = new Hashtable();
		// env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.dns.DnsContextFactory");
		// env.put("com.sun.jndi.dns.timeout.initial", "20000");
		// env.put(Context.AUTHORITATIVE, "true");
		// env.put(Context.SECURITY_PROTOCOL, "ssl");
		// ns-1692.awsdns-19.co.uk
		// ns-1229.awsdns-25.org
		// ns-64.awsdns-08.com
		// ns-954.awsdns-55.net
		// env.put(Context.PROVIDER_URL,
		// "dns://ns-64.awsdns-08.com. dns://ns-1229.awsdns-25.org. dns://ns-1692.awsdns-19.co.uk.
		// dns://ns-954.awsdns-55.net.");
		// env.put(Context.DNS_URL,
		// "dns://ns-64.awsdns-08.com dns://ns-1229.awsdns-25.org dns://ns-1692.awsdns-19.co.uk
		// dns://ns-954.awsdns-55.net");
		// dirContext = new InitialDirContext(env);
		jmdns = JmDNS.create();
	}

	void registerService() throws Exception {
		// Attributes
		// dirContext.bind("serve.tj.com", localAddr());
		// dirContext.addToEnvironment(propName, propVal)
		// System.out.println(dirContext.getAttributes("tripjack.com", new String[] {"A"}));
		// NamingEnumeration<Binding> bindings = dirContext.listBindings("www.tripjack.com");
		// while (bindings.hasMoreElements()) {
		// Binding binding = bindings.next();
		// System.out.println(binding);
		// }
		ServerSocket server = new ServerSocket(1234);
		System.out.println("Server port: " + server.getLocalPort());
		ServiceInfo serviceInfo = ServiceInfo.create(type, "LogManager", server.getLocalPort(), "");
		jmdns.addServiceTypeListener(new ServiceTypeListener() {

			@Override
			public void subTypeForServiceTypeAdded(ServiceEvent event) {
				System.out.println("service event" + event);
			}

			@Override
			public void serviceTypeAdded(ServiceEvent event) {
				System.out.println("service event" + event);
			}
		});
		jmdns.registerService(serviceInfo);
	}

	void discoverService() throws UnknownHostException, IOException {
		System.out.println("Service discovery: " + type);
		// jmdns.addServiceTypeListener(new ServiceTypeListener() {
		//
		// @Override
		// public void subTypeForServiceTypeAdded(ServiceEvent event) {
		// System.out.println("service event" + event);
		//
		// }
		//
		// @Override
		// public void serviceTypeAdded(ServiceEvent event) {
		// System.out.println("service event" + event);
		//
		// }
		// });
		jmdns.addServiceListener(type, new ServiceListener() {

			@Override
			public void serviceResolved(ServiceEvent event) {
				System.out.println("event" + event);
				// System.out.println(event.getInfo());
			}

			@Override
			public void serviceRemoved(ServiceEvent event) {
				System.out.println("event" + event);
			}

			@Override
			public void serviceAdded(ServiceEvent event) {
				System.out.println("event" + event);
			}
		});
	}

	public static void main(String a[]) throws Exception {
		ServiceDiscovery serviceDiscovery = new ServiceDiscovery();
		serviceDiscovery.init();
		serviceDiscovery.registerService();
		serviceDiscovery.discoverService();
		Thread.sleep(10000);
		// serviceDiscovery.jmdns.unregisterAllServices();
		System.out.println("closing");
		serviceDiscovery.jmdns.close();
		System.out.println("exiting");
		System.exit(0);
	}

	// static String localAddr() throws SocketException {
	// String addr;
	// for (Enumeration<NetworkInterface> nifs = NetworkInterface.getNetworkInterfaces(); nifs.hasMoreElements();) {
	// NetworkInterface nif = nifs.nextElement();
	// for (Enumeration<InetAddress> iaenum = nif.getInetAddresses(); iaenum.hasMoreElements();) {
	// InetAddress interfaceAddress = iaenum.nextElement();
	// if (useInetAddress(nif, interfaceAddress)) {
	// addr = interfaceAddress.getHostAddress();
	// if (addr.startsWith("192.168"))
	// return addr;
	// }
	// }
	// }
	// return null;
	// }
	//
	// static boolean useInetAddress(NetworkInterface networkInterface, InetAddress interfaceAddress) {
	// try {
	// return networkInterface.isUp() && networkInterface.supportsMulticast() && !networkInterface.isLoopback();
	// } catch (Exception exception) {
	// return false;
	// }
	// }
}
