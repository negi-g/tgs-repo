package com.tgs.services.base.runtime.spring;

import java.lang.reflect.Method;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;

@Order(1003)
@ControllerAdvice
public class CustomizedAPIHandlerInterceptor implements ResponseBodyAdvice<Object>, HandlerInterceptor {

	@Autowired
	AccessControlProcessor requestProcessor;

	// before the actual handlers will be executed
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		SpringUtils.setEndPointInContextData((HandlerMethod) handler);
		doPreProcessing((HandlerMethod) handler);
		return true;
	}

	private void doPreProcessing(HandlerMethod handler) {
		Method method = handler.getMethod();
		if (method != null) {
			CustomRequestProcessor annot = null;
			if (method.isAnnotationPresent(CustomRequestProcessor.class)) {
				annot = method.getAnnotation((CustomRequestProcessor.class));
			} else {
				annot = method.getDeclaringClass().getDeclaredAnnotation(CustomRequestProcessor.class);
			}
			if (annot != null) {
				requestProcessor.process(annot.areaRole(), annot.includedRoles(), annot.excludedRoles(),
						annot.emulatedAllowedRoles(), annot.isMidOfficeAllowed());
			}
		}
	}

	// after the handlers is executed
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {}

	public static void main(String[] args) {

	}

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

	@Override
	public Object beforeBodyWrite(Object responseObject, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse httpResponse) {
		if (responseObject instanceof BaseResponse) {
			BaseResponse response = (BaseResponse) responseObject;
			if (Objects.isNull(response.getErrors())) {
				response.setStatus(new Status(HttpStatusCode.HTTP_200));
			} else if (Objects.isNull(response.getStatus())) {
				response.setStatus(new Status(HttpStatusCode.HTTP_400));
			}
		}
		return responseObject;
	}

}
