package com.tgs.services.base.helper;

import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.DateFormatInfo;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;

@Service
public class DateFormatterHelper {

	private static GeneralServiceCommunicator generalCommunicator;

	@Autowired
	public DateFormatterHelper(GeneralServiceCommunicator generalCommunicator) {
		DateFormatterHelper.generalCommunicator = generalCommunicator;
	}

	public static String format(TemporalAccessor dateTime, String format) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		return formatter.format(dateTime);
	}

	public static String formatDate(TemporalAccessor dateTime, DateFormatType dateFormat) {
		DateFormatInfo dateFormatInfo = getDateFormats(dateFormat);
		// default date format
		String format = "dd/MM/yyyy";
		if (dateFormatInfo != null && StringUtils.isNotEmpty(dateFormatInfo.getDateFormat())) {
			format = dateFormatInfo.getDateFormat();
		}
		return DateFormatterHelper.format(dateTime, format);
	}

	public static String formatDateTime(TemporalAccessor dateTime, DateFormatType dateFormat) {
		DateFormatInfo dateFormatInfo = getDateFormats(dateFormat);
		// default date time format
		String format = "dd/MM/yyyy HH:mm:SS";
		if (dateFormatInfo != null && StringUtils.isNotEmpty(dateFormatInfo.getDateTimeFormat())) {
			format = dateFormatInfo.getDateTimeFormat();
		}
		return DateFormatterHelper.format(dateTime, format);
	}

	private static DateFormatInfo getDateFormats(DateFormatType dateFormat) {
		ClientGeneralInfo clientInfo = (ClientGeneralInfo) generalCommunicator
				.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		DateFormatInfo dateFormatInfo = null;
		if (clientInfo != null && clientInfo.getDateFormats() != null) {
			if (clientInfo.getDateFormats().get(dateFormat) != null) {
				dateFormatInfo = clientInfo.getDateFormats().get(dateFormat);
			} else {
				dateFormatInfo = clientInfo.getDateFormats().get(DateFormatType.DEFAULT_FORMAT);
			}
		}
		return dateFormatInfo;
	}

}
