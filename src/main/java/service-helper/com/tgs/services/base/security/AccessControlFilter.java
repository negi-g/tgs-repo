package com.tgs.services.base.security;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.HttpHeader;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.AccessControlInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.ruleengine.AccessControlOutput;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AccessControlFilter extends BaseFilter {

	@Getter
	public static class AccessControlCombinationData {
		private String url;
		private List<String> allowedIps;
	}

	@Autowired
	GeneralServiceCommunicator gmsComm;

	@Autowired
	UserServiceCommunicator userService;

	private static String env;

	private static List<String> BLOCKED_URLS;

	private static List<String> API_ALLOWED_URLS;

	private static String allowedUrlAndIpCombinations;

	@Value("${env}")
	public void setEnv(String e) {
		env = e;
	}

	@Value("${apiallowedurls}")
	public void setApiAllowedUrls(String[] urls) {
		API_ALLOWED_URLS = urls != null ? Arrays.asList(urls) : Collections.emptyList();
	}

	@Value("${blockedurls}")
	public void setBlockedUrls(String[] urls) {
		BLOCKED_URLS = urls != null ? Arrays.asList(urls) : Collections.emptyList();
	}

	@Value("${allowedCombinations}")
	public void setAllowedCombinations(String allowedCombinations) {
		allowedUrlAndIpCombinations = allowedCombinations;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		boolean authenticate = true;
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		try {
			User user = SystemContextHolder.getContextData().getUser();

			String requestUrl = httpRequest.getRequestURL().toString();


			/**
			 * Don't validate in case it is an API User, Because they don't need to send currenv in Httpheaders
			 */
			if ("prod".equals(env) && !UserUtils.isApiUserRequest(user)) {
				if (StringUtils
						.isNotEmpty(BLOCKED_URLS.stream().filter(u -> requestUrl.matches(u)).findAny().orElse(null))) {
					HttpHeader headers = HttpUtils.buildHttpHeader(httpRequest);
					String currEnv = headers.getCurrEnv();
					if (!"prod".equals(currEnv))
						accessDeniedResponse(response, requestUrl, "Env : " + currEnv);
				}
			}
			/**
			 * 
			 * Allow access to API User only for the allowed API Urls and only from list of white listed Ip(s)
			 */

			if (UserUtils.isApiUserRequest(user)) {
				if (CollectionUtils.isNotEmpty(API_ALLOWED_URLS)
						&& !ServiceUtils.isUrlMatchesRegex(API_ALLOWED_URLS, requestUrl)) {
					accessDeniedResponse(response, requestUrl, "Api user : " + UserUtils.getUserId(user));
				}
				if (Objects.nonNull(user.getUserConf()) && Objects.nonNull(user.getUserConf().getApiConfiguration())
						&& CollectionUtils.isNotEmpty(user.getUserConf().getApiConfiguration().getWhitelistedIps())
						&& !isIpAllowed(user.getUserConf().getApiConfiguration().getWhitelistedIps(),
								SystemContextHolder.getContextData().getHttpHeaders().getIp())) {
					accessDeniedResponse(response, requestUrl, "Api user : " + UserUtils.getUserId(user));
				}

			}
			if (user == null) {
				List<AccessControlCombinationData> confList = GsonUtils.getGson().fromJson(allowedUrlAndIpCombinations,
						new TypeToken<List<AccessControlCombinationData>>() {
							private static final long serialVersionUID = 1L;
						}.getType());
				for (AccessControlCombinationData conf : confList) {
					if (requestUrl.matches(conf.getUrl())) {
						authenticate = isIpAllowed(conf.getAllowedIps(),
								SystemContextHolder.getContextData().getHttpHeaders().getIp());
					}
				}
				if (!authenticate) {
					accessDeniedResponse(response, requestUrl,
							SystemContextHolder.getContextData().getHttpHeaders().getIp());
				}
			}
			List<ConfiguratorInfo> configInfoList = gmsComm.getConfiguratorInfos(ConfiguratorRuleType.ACCESSCONTROL);
			if (CollectionUtils.isNotEmpty(configInfoList)) {

				for (AccessControlInfo accessControl : ((AccessControlOutput) configInfoList.get(0).getOutput())
						.getAccessControlInformation()) {
					if (user != null && requestUrl.matches(accessControl.getEndPointUrl())
							&& (Objects.isNull(accessControl.getMethodType())
									|| accessControl.getMethodType().trim().equals(httpRequest.getMethod()))) {
						authenticate = false;
						if ((Objects.nonNull(accessControl.getAllowedUserIds())
								&& accessControl.getAllowedUserIds().contains(UserUtils.getUserId(user)))
								|| (user.getEmulateUser() != null
										&& Objects.nonNull(accessControl.getEmulatedAllowedRoles())
										&& isRoleAllowed(accessControl.getEmulatedAllowedRoles(),
												user.getEmulateUser().getRole().getRoleDisplayName()))
								|| (Objects.nonNull(accessControl.getAllowedRoles()) && isRoleAllowed(
										accessControl.getAllowedRoles(), user.getRole().getRoleDisplayName()))
								|| (Objects.nonNull(accessControl.getAllowedIps())
										&& isIpAllowed(accessControl.getAllowedIps(),
												SystemContextHolder.getContextData().getHttpHeaders().getIp()))) {
							authenticate = true;
						}

						break;
					}
				}
				if (!authenticate) {
					accessDeniedResponse(response, requestUrl, UserUtils.getUserId(user));
				}

			}
			chain.doFilter(httpRequest, response);
		} catch (CustomGeneralException e) {

		} catch (Exception e) {
			log.error("Error occurred while checking access contorl {}  ", e.getMessage(), e);
			chain.doFilter(httpRequest, response);
		} finally {
		}
	}

	private void accessDeniedResponse(ServletResponse response, String requestUrl, String combination)
			throws IOException {
		log.info("Authentication failed for requestUrl {} and combination {} ", requestUrl, combination);
		addToAnalytics(requestUrl, combination, SystemError.ACCESS_DENIED);
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_403));
		res.addError(new ErrorDetail(SystemError.ACCESS_DENIED.errorCode(), SystemError.ACCESS_DENIED.getMessage()));
		response.getWriter().write(GsonUtils.getGsonBuilder().setPrettyPrinting().create().toJson(res));
		throw new CustomGeneralException(SystemError.ACCESS_DENIED);
	}

	private boolean isIpAllowed(List<String> allowedIps, String remoteAddr) {
		for (String ip : allowedIps) {
			if (remoteAddr.matches(ip)) {
				return true;
			}
		}
		log.debug("IP match failed , allowedIps {}, remoteAddr {}", allowedIps, remoteAddr);
		return false;
	}

	private boolean isRoleAllowed(List<String> allowedRoles, String userRole) {
		for (String role : allowedRoles) {
			if (role.equals(userRole))
				return true;
		}
		log.debug("Role match failed , allowedRoles {}, userRole {}", allowedRoles, userRole);
		return false;
	}

}
