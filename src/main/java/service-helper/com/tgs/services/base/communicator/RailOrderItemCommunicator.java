package com.tgs.services.base.communicator;

import java.time.LocalDateTime;
import com.tgs.services.oms.datamodel.rail.RailItemStatus;
import com.tgs.services.oms.datamodel.rail.RailOrderItem;
import com.tgs.services.rail.datamodel.RailJourneyInfo;

public interface RailOrderItemCommunicator {

	RailOrderItem getRailOrderItem(String bookingId);

	RailJourneyInfo getJourneyInfo(String bookingId);

	void updateOrderItem(RailItemStatus success, RailJourneyInfo journeyInfo, String bookingId);

	RailOrderItem updateOrderItem(String bookingId, String newBoardingStation, LocalDateTime newBoardingDate);

	void updateOrderItem(String bookingId, String optedTrainNumbers);

	void deleteFromCache(String key);

	void abortBooking(String bookingId);
}
