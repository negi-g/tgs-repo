package com.tgs.services.base;

import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public abstract class ESStackInitializer {

	public void initialize(String name) {
		this.process();
	}


	public abstract void process();


}
