package com.tgs.services.base.security;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class RequestWrapper extends HttpServletRequestWrapper {

	private JSONObject requestBody;
	private JSONArray requestBodyArray;

	public RequestWrapper(HttpServletRequest request) throws IOException {
		super(request);
		BufferedReader requestBody = request.getReader();
		Object requestBodyJson = new JSONTokener(requestBody).nextValue();
		if (requestBodyJson instanceof JSONObject)
			this.requestBody = (JSONObject) requestBodyJson;
		else if (requestBodyJson instanceof JSONArray) {
			this.requestBodyArray = (JSONArray) requestBodyJson;
		}
	}

	public JSONObject getRequestBody() {
		return this.requestBody;
	}

	public JSONArray getRequestArray() {
		return this.requestBodyArray;
	}

	@Override
	public BufferedReader getReader() throws IOException {
		return new BufferedReader(new InputStreamReader(getInputStream()));
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
		final ByteArrayInputStream bais = new ByteArrayInputStream(
				requestBody != null ? requestBody.toString().getBytes() : requestBodyArray.toString().getBytes());
		return new ServletInputStream() {

			@Override
			public boolean isFinished() {
				return false;
			}

			@Override
			public boolean isReady() {
				return false;
			}

			@Override
			public void setReadListener(ReadListener readListener) {

			}

			@Override
			public int read() {
				return bais.read();
			}
		};
	}

}
