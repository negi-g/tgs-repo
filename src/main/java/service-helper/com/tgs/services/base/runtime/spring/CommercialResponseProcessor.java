package com.tgs.services.base.runtime.spring;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.helper.CommercialPlanId;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.cms.datamodel.commission.CommissionPlan;


@Service
public class CommercialResponseProcessor implements ResponseProcessor {

	private static final String META_KEY = "Commercial_Plan";

	@Autowired
	CommercialCommunicator commercialComm;

	@Override
	public void process(BaseResponse obj, String[] metaInfo) {
		Set<Integer> planIds = TgsObjectUtils.findMethodAnnotationValues(obj, CommercialPlanId.class);
		if(planIds.isEmpty())
			planIds = TgsObjectUtils.findAnnotationValues(obj, CommercialPlanId.class);
		Map<Integer, CommissionPlan> map = new HashMap<>();
		planIds.forEach(planId-> {
			if(planId!=null) {
				CommissionPlan plan = commercialComm.getCommissionPlanFromCache(String.valueOf(planId));
				if(plan!=null)
					map.put(planId, CommissionPlan.builder().name(plan.getName()).build());
			}
		});
		if(MapUtils.isNotEmpty(map))
			obj.getMetaInfo().put(META_KEY, map);

	}
}
