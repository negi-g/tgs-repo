package com.tgs.services.base.runtime.database.config;

public enum DatabaseEnvironment {
	MASTER, REPLICA;
}
