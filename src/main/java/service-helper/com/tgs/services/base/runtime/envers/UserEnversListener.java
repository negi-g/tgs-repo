package com.tgs.services.base.runtime.envers;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.event.spi.PostUpdateEvent;
import org.springframework.stereotype.Component;

import com.tgs.services.base.runtime.PostUpdateEnversListener;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserEnversListener implements PostUpdateEnversListener {
	
	List<String> fields = Arrays.asList("processedOn","additionalInfo.lastLoginTime","additionalInfo.lastTxnTime.AIR","additionalInfo.lastTxnTime.HOTEL");

	@Override
	public boolean isValidEventForAudit(PostUpdateEvent event) {
		Object[] oldState = event.getOldState();
		if (oldState != null) {
			Set<String> differentFields = getDifferentFields(event);
			log.debug("[User] Different fields are : {}", differentFields.toString());
			differentFields.removeAll(fields);
			if (CollectionUtils.isEmpty(differentFields)) {
				return false;
			}
		}
		return true;
	}

}
