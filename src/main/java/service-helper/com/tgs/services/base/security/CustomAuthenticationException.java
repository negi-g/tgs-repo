package com.tgs.services.base.security;

import org.springframework.security.core.AuthenticationException;

import com.tgs.services.base.helper.SystemError;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomAuthenticationException extends AuthenticationException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private SystemError error;

	public CustomAuthenticationException(SystemError error) {
		super(null);
		this.error = error;
	}

	public CustomAuthenticationException(SystemError error, String message) {
		super(message);
		this.error = error;
	}

	public CustomAuthenticationException(String message) {
		super(message);
	}
}
