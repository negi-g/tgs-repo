package com.tgs.services.base.runtime.envers;

import java.util.Set;
import org.hibernate.event.spi.PostUpdateEvent;
import org.springframework.stereotype.Component;
import com.tgs.services.base.runtime.PostUpdateEnversListener;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UserWalletEnversListener implements PostUpdateEnversListener{
	
	@Override
	public boolean isValidEventForAudit(PostUpdateEvent event) {
		Object[] oldState = event.getOldState();
		if (oldState != null) {
			Set<String> differentFields = getDifferentFields(event);
			log.debug("[UserWallet] Different fields are : {}", differentFields.toString());
			if (differentFields.isEmpty() || differentFields.contains("balance")) {
				return false;
			}
		}
		return true;
	}

}
