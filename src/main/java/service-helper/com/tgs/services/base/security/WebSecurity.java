package com.tgs.services.base.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.spring.CustomAccessDeniedHandler;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

	@Autowired
	private AuthenticationProvider authProvider;

	protected static String[] JWT_WAIVER_URLS;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
				.accessDeniedHandler(accessDeniedHandler()).and().authorizeRequests().antMatchers(JWT_WAIVER_URLS)
				.permitAll().anyRequest().authenticated().and()
				.addFilter(new JWTAuthenticationFilter(authenticationManager()))
				.addFilter(new APIKeyAuthorizationFilter(authenticationManager()))
				.addFilter(new JWTAuthorizationFilter(authenticationManager()))
				.addFilterAfter(new SystemContextFilter(), JWTAuthorizationFilter.class).sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		// http.cors().and().csrf().disable().authorizeRequests().antMatchers(SecurityConstants.JWT_WAIVER_URLS)
		// .permitAll().anyRequest().authenticated().and()
		// .addFilter(new JWTAuthenticationFilter(authenticationManager()))
		// .addFilter(new JWTAuthorizationFilter(authenticationManager()))
		// .addFilterAfter(new SystemContextFilter(),
		// JWTAuthorizationFilter.class).sessionManagement()
		// .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider);
	}

	public AuthenticationEntryPoint authenticationEntryPoint() {
		return new AuthenticationEntryPoint() {

			@Override
			public void commence(HttpServletRequest request, HttpServletResponse response,
					AuthenticationException authException) throws IOException, ServletException {
				BaseResponse res = new BaseResponse();
				res.setStatus(new Status(HttpStatusCode.HTTP_403));
				res.addError(new ErrorDetail(SystemError.FORBIDDEN));
				response.getWriter().write(GsonUtils.getGsonBuilder().setPrettyPrinting().create().toJson(res));

			}
		};
	}

	@Bean
	public AccessDeniedHandler accessDeniedHandler() {
		return new CustomAccessDeniedHandler();
	}

	@Value("${jwt.jwtdissurls}")
	public void setDisabledUrls(String[] disabledUrls) {
		JWT_WAIVER_URLS = disabledUrls;
	}
}
