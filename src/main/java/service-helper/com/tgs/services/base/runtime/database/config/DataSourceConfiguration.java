package com.tgs.services.base.runtime.database.config;

//@Configuration
//@EnableJpaRepositories(basePackages = {
//		"com.tgs.services.common.crudrepository" }, entityManagerFactoryRef = "entityManager", transactionManagerRef = "transactionManager")
//@EnableTransactionManagement
public class DataSourceConfiguration {

	// @Autowired(required = false)
	// private PersistenceUnitManager persistenceUnitManager;
	//
	// @Bean
	// @Primary
	// @ConfigurationProperties("spring.jpa")
	// public JpaProperties customerJpaProperties() {
	// return new JpaProperties();
	// }
	//
	// @Bean
	// @ConfigurationProperties(prefix = "replica.datasource")
	// public DataSource customerDevelopmentDataSource() {
	// return DataSourceBuilder.create().build();
	// }
	//
	// @Bean
	// @ConfigurationProperties(prefix = "master.datasource")
	// public DataSource customerProductionDataSource() {
	// return DataSourceBuilder.create().build();
	// }
	//
	// /**
	// * Adds all available datasources to datasource map.
	// *
	// * @return datasource of current context
	// */
	// @Bean
	// @Primary
	// public DataSource customerDataSource() {
	// DataSourceRouter router = new DataSourceRouter();
	//
	// final HashMap<Object, Object> map = new HashMap<>(2);
	// map.put(DatabaseEnvironment.REPLICA, customerDevelopmentDataSource());
	// map.put(DatabaseEnvironment.MASTER, customerProductionDataSource());
	// router.setDefaultTargetDataSource(customerProductionDataSource());
	// router.setTargetDataSources(map);
	// return router;
	// }
	//
	// @Bean
	// @Primary
	// public LocalContainerEntityManagerFactoryBean entityManager(final
	// JpaProperties customerJpaProperties) {
	// EntityManagerFactoryBuilder builder =
	// createEntityManagerFactoryBuilder(customerJpaProperties);
	//
	// return
	// builder.dataSource(customerDataSource()).packages("com.tgs.services.common.crudrepository")
	// .persistenceUnit("entityManager").build();
	// }
	//
	// @Bean
	// @Primary
	// public JpaTransactionManager transactionManager(@Qualifier("entityManager")
	// final EntityManagerFactory factory) {
	// return new JpaTransactionManager(factory);
	// }
	//
	// private EntityManagerFactoryBuilder
	// createEntityManagerFactoryBuilder(JpaProperties customerJpaProperties) {
	// JpaVendorAdapter jpaVendorAdapter =
	// createJpaVendorAdapter(customerJpaProperties);
	// return new EntityManagerFactoryBuilder(jpaVendorAdapter,
	// customerJpaProperties.getProperties(),
	// this.persistenceUnitManager);
	// }
	//
	// private JpaVendorAdapter createJpaVendorAdapter(JpaProperties jpaProperties)
	// {
	// AbstractJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
	// adapter.setShowSql(jpaProperties.isShowSql());
	// adapter.setDatabase(jpaProperties.getDatabase());
	// adapter.setDatabasePlatform(jpaProperties.getDatabasePlatform());
	// adapter.setGenerateDdl(jpaProperties.isGenerateDdl());
	// return adapter;
	// }
}
