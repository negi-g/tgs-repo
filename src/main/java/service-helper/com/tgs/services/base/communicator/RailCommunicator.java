package com.tgs.services.base.communicator;

import java.util.List;
import java.util.Set;
import com.tgs.services.oms.restmodel.rail.RailCancellationRequest;
import com.tgs.services.oms.restmodel.rail.RailCancellationResponse;
import com.tgs.services.oms.restmodel.rail.RailOtpVerificationRequest;
import com.tgs.services.rail.datamodel.RailStationInfo;
import com.tgs.services.rail.datamodel.RailTDRReason;
import com.tgs.services.rail.datamodel.RailTdrInfo;
import com.tgs.services.rail.datamodel.config.RailSourceConfigurationOutput;
import com.tgs.services.rail.restmodel.RailCurrentStatus;
import com.tgs.services.rail.restmodel.RailFareEnquiryResponse;

public interface RailCommunicator {

	RailFareEnquiryResponse getRailFareEnquiryResponse(String bookingId);

	RailStationInfo getRailStationInfo(String code);

	RailCancellationResponse cancelPassengers(RailCancellationRequest request);

	boolean submitOtpRequest(RailOtpVerificationRequest railOtpRequest);

	boolean resendOtp(RailOtpVerificationRequest railOtpRequest);

	List<RailTDRReason> getReasonsList(String transactionId, String bookingId);

	boolean submitTDRRequest(RailTdrInfo tdrInfo, String transactionId, Set<String> paxIds, String bookingId);

	RailCurrentStatus getLiveBookingDetails(String bookingId);

	RailSourceConfigurationOutput getRailSourceConfiguration();


}
