package com.tgs.services.base.runtime.database.CustomTypes;

import com.tgs.services.cms.datamodel.commission.air.AirCommercialRuleCriteria;

public class AirCommercialRuleCriteriaType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return AirCommercialRuleCriteria.class;
    }
}
