package com.tgs.services.base.runtime.database.CustomTypes;

import com.tgs.services.cms.datamodel.commission.air.AirCommercialComponent;

public class AirCommercialComponentType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return AirCommercialComponent.class;
    }
}

