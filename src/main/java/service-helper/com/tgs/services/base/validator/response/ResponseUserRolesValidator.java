package com.tgs.services.base.validator.response;

import static java.util.stream.Collectors.groupingBy;
import java.util.List;
import java.util.Map;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.ums.datamodel.User;

public abstract class ResponseUserRolesValidator extends ResponseUserMetaInfoValidator {

	@Override
	final protected void validateUserMetaInfo(User targetUser, Map<String, User> usersMetaInfo,
			ResponseValidationResult result) {
		Map<UserRole, List<User>> roleWiseUsers = RoleWiseUserMetaInfoCache.get();
		if (roleWiseUsers == null) {
			roleWiseUsers = usersMetaInfo.values().stream().collect(groupingBy(User::getRole));
			RoleWiseUserMetaInfoCache.set(roleWiseUsers);
		}
		validateUserRoles(targetUser, roleWiseUsers, result);
	}

	protected abstract void validateUserRoles(User targetUser, Map<UserRole, List<User>> roleWiseUsers,
			ResponseValidationResult result);

}
