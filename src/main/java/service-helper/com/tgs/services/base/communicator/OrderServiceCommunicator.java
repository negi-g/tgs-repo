package com.tgs.services.base.communicator;

import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.ums.datamodel.User;
import java.util.List;
import java.util.Map;

public interface OrderServiceCommunicator {

	GstInfo getGstInfo(String bookingId);

	void processBooking(String bookingId);

	Order findByBookingId(String bookingId);

	List<Order> getOrders(List<String> bookingIds);

	void updateOrder(Order order);

	double getGrossCommission(Map<FareComponent, Double> fareComponents, User user, boolean includePartnerCommission);

	void validateSearchQueryOnPNR(AirSearchQuery searchQuery);

	List<TravellerInfo> getFlightTravellers(String pnr);
}
