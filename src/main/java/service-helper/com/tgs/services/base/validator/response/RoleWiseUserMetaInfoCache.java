package com.tgs.services.base.validator.response;

import static java.util.stream.Collectors.groupingBy;
import java.util.List;
import java.util.Map;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.ums.datamodel.User;

final public class RoleWiseUserMetaInfoCache {

	private final static ThreadLocal<Map<UserRole, List<User>>> ROLE_WISE_USER_METAINFO = new ThreadLocal<>();

	public static void clearCache() {
		ROLE_WISE_USER_METAINFO.set(null);
	}

	public static void set(Map<UserRole, List<User>> roleWiseUsers) {
		ROLE_WISE_USER_METAINFO.set(roleWiseUsers);
	}

	public static Map<UserRole, List<User>> processIfAbsentAndGet(Map<String, User> usersMetaInfo) {
		Map<UserRole, List<User>> roleWiseUsers = get();
		if (roleWiseUsers == null) {
			roleWiseUsers = usersMetaInfo.values().stream().collect(groupingBy(User::getRole));
			ROLE_WISE_USER_METAINFO.set(roleWiseUsers);
		}
		return roleWiseUsers;
	}

	public static Map<UserRole, List<User>> get() {
		return ROLE_WISE_USER_METAINFO.get();
	}
}
