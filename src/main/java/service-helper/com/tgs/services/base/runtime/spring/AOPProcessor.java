
package com.tgs.services.base.runtime.spring;

import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.tgs.services.base.gson.GsonUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
@Configurable
public class AOPProcessor {

	@Autowired
	AccessControlProcessor requestProcessor;

	// public List<AccessControlProcessor> getCheckPermissionList() {
	// return Arrays.asList(requestProcessor);
	// }

	// @Before("@annotation(annot)")
	// public void validateCustomProcessor(CustomRequestProcessor annot) {
	// for (AccessControlProcessor processor : getCheckPermissionList())
	// processor.process(annot.areaRole(), annot.includedRoles(),
	// annot.excludedRoles(),
	// annot.isMidOfficeAllowed());
	// }

	@AfterThrowing(pointcut = "execution(* com.tgs.services..restcontroller..* (..))", throwing = "exception")
	public void handleException(JoinPoint jp, Throwable exception) {
		HttpServletRequest request =
				((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

		String requestUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
				+ request.getContextPath() + request.getRequestURI() + "?" + request.getQueryString();
		Object[] args = jp.getArgs();
		String reqParams = null;
		if (args.length > 2) {
			reqParams = GsonUtils.getGson().toJson(args[2]);
		}
		log.info("Exception happened on url {} with reqbody {} with error {} ", requestUrl, reqParams,
				exception.getMessage());
	}

	@Around("execution(* com.tgs.services..restcontroller..* (..)) && target(controller)")
	public Object handleException(ProceedingJoinPoint jp, Object controller) throws Throwable {
		long startTime = System.currentTimeMillis();
		Object result = jp.proceed();
		long totalTime = System.currentTimeMillis() - startTime;
		if (totalTime >= 60000) {
			log.info("{}|Invocation time {}ms ", controller.getClass().getSimpleName(), totalTime);
		}
		return result;
	}

	// @AfterReturning(pointcut = "execution(*
	// com.tgs.services.gms.restcontroller.ConfiguratorController.saveOrUpdateConfigRule
	// (..))", returning = "retVal")
	// public void handleReturning(JoinPoint jp, Object retVal) {
	// System.out.println("returning method:" + jp.getSignature().getName() +
	// retVal);
	// }

	// @After("beforeAdvice()")
	// public void beforeAdvic(JoinPoint joinPoint) {
	// System.out.println("Before method:" + joinPoint.getSignature().getName());
	// }

	// @Around("execution(*
	// com.tgs.services.gms.restcontroller.ConfiguratorController.saveOrUpdateConfigRule
	// (..))")
	// public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable {
	// Object retVal = pjp.proceed();
	// return retVal;
	// }

}
