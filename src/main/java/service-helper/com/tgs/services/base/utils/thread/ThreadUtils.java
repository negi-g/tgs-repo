package com.tgs.services.base.utils.thread;

import org.apache.commons.lang3.ArrayUtils;

public class ThreadUtils {

	public static String getThreadStackTrace() {
		return ThreadUtils.getThreadStackTrace(Thread.currentThread().getStackTrace(), -1, -1);
	}

	public static String getThreadStackTrace(int fromIndex, int toIndex) {
		return ThreadUtils.getThreadStackTrace(Thread.currentThread().getStackTrace(), fromIndex, toIndex);
	}

	public static String getThreadStackTrace(StackTraceElement[] stackTraceElement, int fromIndex, int toIndex) {
		StringBuilder stackTrace = new StringBuilder();
		fromIndex = fromIndex == -1 ? 1 : fromIndex;
		toIndex = toIndex == -1 ? stackTraceElement.length : Math.min(toIndex, stackTraceElement.length);
		if (ArrayUtils.isNotEmpty(stackTraceElement)) {
			for (int i = fromIndex; i < toIndex; i++) {
				stackTrace.append(stackTraceElement[i]).append("\n");
			}
		}
		return stackTrace.toString();
	}
}
