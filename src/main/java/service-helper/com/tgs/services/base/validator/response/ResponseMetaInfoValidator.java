package com.tgs.services.base.validator.response;

import java.util.Map;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.restmodel.BaseResponse;

public abstract class ResponseMetaInfoValidator extends ResponseValidator {

	@Override
	protected void validateResponse(BaseResponse response, ResponseValidationResult result) {
		if (MapUtils.isEmpty(response.getMetaInfo())) {
			return;
		}
		validateMetaInfo(response.getMetaInfo(), result);
	}

	protected abstract void validateMetaInfo(Map<String, Object> metaInfo, ResponseValidationResult result);

}
