package com.tgs.services.base.validator.response;

import static com.tgs.services.base.enums.UserRole.SUPPLIER;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;

@Service
public class SupplierResponseUserMetaInfoValidator extends ResponseUserRolesValidator {

	@Override
	protected void validateUserRoles(User targetUser, Map<UserRole, List<User>> roleWiseUsers,
			ResponseValidationResult result) {
		List<User> suppliersList = roleWiseUsers.getOrDefault(SUPPLIER, Collections.emptyList());
		int suppliersCount = suppliersList.size();
		User supplierUser = suppliersCount > 0 ? suppliersList.get(0) : null;

		if (suppliersCount == 0) {
			return;
		}

		Validator.builder().suppliersCount(suppliersCount).supplierUser(supplierUser).targetUser(targetUser).build()
				.validate(result);
	}

	@Override
	public boolean supports(User targetUser) {
		return targetUser != null && targetUser.getRole() != null && (SUPPLIER.equals(targetUser.getRole())
				|| !UserRole.getMidOfficeRoles().contains(targetUser.getRole()));
	}

	@Builder
	@Getter
	private static class Validator {

		private final int suppliersCount;
		private User supplierUser;
		private User targetUser;

		private void validate(ResponseValidationResult result) {
			/**
			 * if targetUser is neither a SUPPLIER nor a midOffice user
			 */
			if (!SUPPLIER.equals(targetUser.getRole())) {
				result.addError(
						new StringBuilder(userToString(targetUser)).append(" is able to see SUPPLIER data").toString());
				return;
			}
			if (suppliersCount > 1) {
				result.addError(new StringBuilder(userToString(targetUser))
						.append(" is able to see data of more than 1 DISTRIBUTORs").toString());
			}
			if (supplierUser != null && !supplierUser.getUserId().equals(targetUser.getParentUserId())) {
				result.addError(new StringBuilder(userToString(targetUser)).append(" is able to see data of user ")
						.append(userToString(supplierUser)).toString());
			}
		}
	}

	private static String userToString(User user) {
		return new StringBuilder(user.getUserId()).append('(').append(user.getRole()).append(')').toString();
	}

}
