package com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls;

import java.util.function.BiFunction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

import com.tgs.services.base.runtime.database.SqlPredicate.IPredicate;

public class LTPredicate extends IPredicate {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected BiFunction<Expression<? extends Comparable>, Expression<? extends Comparable>, Predicate> getFunctionForPredicateWithExpression(
			CriteriaBuilder criteriaBuilder) {
		return criteriaBuilder::lessThan;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected BiFunction<Expression, Comparable, Predicate> getFunctionForPredicateWithObject(
			CriteriaBuilder criteriaBuilder) {
		return criteriaBuilder::lessThan;
	}

	@Override
	protected Class<?> targetValueType() {
		return Comparable.class;
	}
}
