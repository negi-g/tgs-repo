package com.tgs.services.base;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapterFactory;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.CustomInstanceCreator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsObjectUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GsonMapper<RT> extends Mapper<RT> {

	private Object input;
	private Class<RT> classType;
	private boolean requirePolymorphismValidation;

	public GsonMapper(Class<RT> classType) {
		this.classType = classType;
	}

	public GsonMapper(Object input, Class<RT> classType) {
		this.input = input;
		this.classType = classType;
	}

	public GsonMapper(Object input, Class<RT> classType, boolean requirePolymorphismValidation) {
		this.input = input;
		this.classType = classType;
		this.requirePolymorphismValidation = requirePolymorphismValidation;
	}

	public GsonMapper(Object input, RT output, Class<RT> classType, boolean requirePolymorphismValidation) {
		this.input = input;
		this.output = output;
		this.classType = classType;
		this.requirePolymorphismValidation = requirePolymorphismValidation;
	}

	public GsonMapper(Object input, RT output, Class<RT> classType) {
		this.input = input;
		this.output = output;
		this.classType = classType;
	}

	public GsonBuilder getGsonBuilder() {
		GsonBuilder gsonBuilder = GsonUtils.getGsonBuilder();
		List<TypeAdapterFactory> factories = new ArrayList<>();
		factories.add(new EnumTypeAdapterFactory());
		gsonBuilder = GsonUtils.builder().typeFactories(factories).build().buildGsonBuilder();
		return gsonBuilder;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute() {
		if (this.input == null) {
			return;
		}
		Gson gson = null;
		List<TypeAdapterFactory> factories = new ArrayList<>();
		factories.add(new EnumTypeAdapterFactory());
		if (SystemContextHolder.getContextData() != null && SystemContextHolder.getContextData().getGson() != null) {
			gson = SystemContextHolder.getContextData().getGson();
		} else {
			gson = getGsonBuilder().create();
			if (SystemContextHolder.getContextData() != null) {
				SystemContextHolder.getContextData().setGson(gson);
			}
		}

		String json = gson.toJson(this.input);
		if (requirePolymorphismValidation) {
			gson = GsonUtils.builder().typeFactories(factories).build().buildGson(json, classType);
		}

		if (output != null) {
			GsonBuilder gsonBuilder = getGsonBuilder();
			TgsObjectUtils.getNotNullClassValueMap(output).forEach((k, v) -> {
				gsonBuilder.registerTypeAdapter(k, new CustomInstanceCreator(v, k));
			});

			gsonBuilder.registerTypeAdapter(classType, new CustomInstanceCreator(output, classType));
			output = GsonUtils.buildGson(json, classType, gsonBuilder)
					.fromJson(json, classType);
		} else {
			output = gson.fromJson(json, this.classType);
		}
	}
}
