package com.tgs.services.base.runtime.database;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

@MappedSuperclass
public class CustomRevisionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@RevisionNumber
	private long id;

	@RevisionTimestamp
	private long timestamp;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Transient
	public Date getRevisionDate() {
		return new Date(timestamp);
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof DefaultRevisionEntity)) {
			return false;
		}

		final CustomRevisionEntity that = (CustomRevisionEntity) o;
		return id == that.id && timestamp == that.timestamp;
	}

	@Override
	public int hashCode() {
		long result;
		result = id;
		result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
		return (int) result;
	}

	@Override
	public String toString() {
		return "DefaultRevisionEntity(id = " + id + ", revisionDate = "
				+ DateFormat.getDateTimeInstance().format(getRevisionDate()) + ")";
	}
}
