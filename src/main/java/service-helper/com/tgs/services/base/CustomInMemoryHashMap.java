package com.tgs.services.base;

import java.util.Map;
import com.aerospike.client.query.Filter;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;

public interface CustomInMemoryHashMap {

	public <V> V put(String key, String field, V value, CacheMetaInfo metaInfo);

	public <V> V get(String key, String field, Class<V> classofV, CacheMetaInfo metaInfo);

	public void delete(String key, CacheMetaInfo metaInfo);

	public void truncate(String set);

	public void modifyTtl(String set, int ttl);
	
	public <V> Map<String, Map<String, V>> getResultSet(CacheMetaInfo metaInfo, Class<V> classofV, Filter filter);

}
