package com.tgs.services.base.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class APIKeyHelper {

	public static GeneralCachingCommunicator cachingCommunicator;

	public static UserServiceCommunicator userService;

	@Autowired
	public APIKeyHelper(GeneralCachingCommunicator cachingCommunicator, UserServiceCommunicator userService) {
		APIKeyHelper.cachingCommunicator = cachingCommunicator;
		APIKeyHelper.userService = userService;
	}

	public static User getAPIUser(String apiKey) {
		return userService.getUser(apiKey);
	}

}
