package com.tgs.services.base.utils;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Service;
import com.tgs.services.rail.datamodel.RailFareComponent;
import com.tgs.services.rail.datamodel.RailPriceInfo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BaseRailUtils {

	public static Double evaluateRailExpression(String commExpression, RailPriceInfo priceInfo) {
		commExpression = commExpression.replaceAll("\\s+", "");
		ExpressionParser expressionParser = new SpelExpressionParser();
		String commercialComponent = getRailFareComponentExpression(commExpression, priceInfo);
		Expression expression = expressionParser.parseExpression(commercialComponent);
		double value = expression.getValue(Double.class);
		if (commExpression.matches(".*[a-zA-Z]+.*"))
			value = value / 100;
		return value;
	}

	public static String getRailFareComponentExpression(String expression, RailPriceInfo priceInfo) {
		for (RailFareComponent fareComponent : RailFareComponent.values()) {
			Double amount = priceInfo.getFareComponents().getOrDefault(fareComponent, 0.0);
			expression =
					expression.replaceAll("%\\*\\b" + fareComponent.name() + "\\b", "*" + String.valueOf(amount / 100));
			expression = expression.replaceAll("\\b" + fareComponent.name() + "\\b", String.valueOf(amount));
		}
		return expression;
	}

}
