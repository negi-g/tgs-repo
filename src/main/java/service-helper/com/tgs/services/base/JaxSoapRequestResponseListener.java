package com.tgs.services.base;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javax.xml.soap.SOAPException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class JaxSoapRequestResponseListener implements SOAPHandler<SOAPMessageContext> {

	private String key;
	private String type;
	private String supplier;
	private Boolean storeLogs;

	private List<String> visibilityGroups;

	private Long responseTime;

	private static final String _REQUEST_TIME_ = "RequestTime";

	public JaxSoapRequestResponseListener(String bookingId, String type, String supplier) {
		this.key = bookingId;
		this.type = ObjectUtils.firstNonNull(type, "");
		this.supplier = supplier;
		this.visibilityGroups = new ArrayList<>();
	}

	@Override
	public boolean handleMessage(SOAPMessageContext mc) {
		boolean isRequest = isRequest(mc);
		captureResponseTime(mc, isRequest);
		extractMessage(mc, (isRequest) ? type + "Request" : type + "Response");
		return true;
	}

	@Override
	public boolean handleFault(SOAPMessageContext mc) {
		boolean isRequest = isRequest(mc);
		captureResponseTime(mc, isRequest);
		extractMessage(mc, (isRequest) ? type + "Request" : type + "Response");
		return false;
	}

	private void extractMessage(SOAPMessageContext mc, String type) {
		if (BooleanUtils.isNotFalse(storeLogs)) {
			try {
				if (mc != null && mc.getMessage() != null) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					mc.getMessage().writeTo(baos);
					String requestXml = baos.toString();
					User user = SystemContextHolder.getContextData().getUser();
					LogData logData = LogData.builder().generationTime(LocalDateTime.now())
							.logData(requestXml.toString()).key(key).type(type)
							.userRole(UserUtils.getEmulatedUserRoleOrUserRole(user)).visibilityGroups(visibilityGroups)
							.userId(UserUtils.getUserId(user)).logType("AirSupplierAPILogs").build();
					if (!isRequest(mc)) {
						log.debug("Time taken to get response for key {} , type {} : {}  ", key, type, responseTime);
						logData.setResponseTime(responseTime);
					}
					LogUtils.store(Arrays.asList(logData));
					baos.close();
				}
			} catch (SOAPException | IOException e) {
				log.info("Error Occured while adding to log for key {} cause {}", key, e.getMessage());
			} catch (Exception e) {
				log.error("Error Occured while adding to log for key {}", key, e);
			}
		}
	}

	@Override
	public void close(MessageContext arg0) {

	}

	@Override
	public Set getHeaders() {
		return null;
	}

	private boolean isRequest(SOAPMessageContext mc) {
		return BooleanUtils.isTrue((Boolean) mc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY));
	}

	private void captureResponseTime(SOAPMessageContext mc, boolean isRequest) {
		if (isRequest) {
			mc.put(_REQUEST_TIME_, System.currentTimeMillis());
		} else {
			Long requestTimeInMilliSeconds = (Long) mc.get(_REQUEST_TIME_);
			responseTime = System.currentTimeMillis() - requestTimeInMilliSeconds;
		}
	}

}
