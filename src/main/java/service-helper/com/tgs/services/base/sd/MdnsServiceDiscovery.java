package com.tgs.services.base.sd;

import java.io.IOException;
import javax.jmdns.JmDNS;
import org.springframework.stereotype.Service;

@Service
public class MdnsServiceDiscovery {

	private JmDNS jmdns;

	void init() throws IOException {
		jmdns = JmDNS.create();
	}

	public void registerService(ServiceInfo serviceInfo) throws Exception {
		javax.jmdns.ServiceInfo jmdnsServiceInfo = toJmdnsServiceInfo(serviceInfo);
		jmdns.registerService(jmdnsServiceInfo);
	}

	private javax.jmdns.ServiceInfo toJmdnsServiceInfo(ServiceInfo serviceInfo) {
		return javax.jmdns.ServiceInfo.create(serviceInfo.getType(), serviceInfo.getName(), serviceInfo.getPort(), "");
	}

}
