package com.tgs.services.trip.servicehandler;

import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.CorporateTripFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.trip.datamodel.CorporateTrip;
import com.tgs.services.trip.dbmodel.DbCorporateTrip;
import com.tgs.services.trip.hibernate.CorporateTripService;
import com.tgs.services.trip.restmodel.CorporateTripResponse;

@Service
public class CorporateTripHandler extends ServiceHandler<CorporateTrip, CorporateTripResponse> {

	@Autowired
	CorporateTripService service;

	@Autowired
	UserServiceCommunicator userService;

	@Override
	public void beforeProcess() throws Exception {
		if ("NEW".equals(request.getModificationType())) {
			DbCorporateTrip existingTrip = service.findByTripId(request.getTripId());
			if (existingTrip != null)
				throw new CustomGeneralException(SystemError.DUPLICATE_TRIPID);
		}
		/*
		 * //We will save trips only for users which have valid email in our database.
		 * request.getTripSearchQuery().getTravellerInfos().forEach(pax -> { Object email =
		 * pax.getUserProfile().getData().get("email"); if (email != null) { User user =
		 * userService.getUser(UserFilter.builder().email(email.toString()).build()); if (user == null) throw new
		 * CustomGeneralException(SystemError.INVALID_EMAIL); pax.setUserId(user.getUserId()); } else { throw new
		 * CustomGeneralException(SystemError.INVALID_EMAIL); } });
		 */
	}

	@Override
	public void process() throws Exception {
		DbCorporateTrip dbTripRequest = new DbCorporateTrip();
		if ("UPDATE".equals(request.getModificationType())) {
			dbTripRequest = service.findByTripId(request.getTripId());
			if (dbTripRequest == null)
				throw new CustomGeneralException(SystemError.INVALID_TRIPID);
		}
		dbTripRequest = dbTripRequest.from(request);
		dbTripRequest.setUserId(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
		dbTripRequest.setStatus("Requested");
		dbTripRequest.setIsPushed(false);
		dbTripRequest = service.save(dbTripRequest);
		response.getTripRequests().add(dbTripRequest.toDomain());

	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	public CorporateTripResponse list(CorporateTripFilter filter) {
		if (response == null) {
			response = new CorporateTripResponse();
		}
		response.getTripRequests().addAll(DbCorporateTrip.toDomainList(service.findAll(filter)));
		return response;
	}


	public BaseResponse updateTripStatus(List<String> tripIds) {
		BaseResponse baseResponse = new BaseResponse();
		if (CollectionUtils.isEmpty(tripIds))
			return baseResponse;
		try {
			List<DbCorporateTrip> trips = service.findAll(CorporateTripFilter.builder().tripIdIn(tripIds).build());
			trips.forEach(trip -> {
				if (tripIds.contains(trip.getTripId())) {
					trip.setIsPushed(true);
					service.save(trip);
				}
			});
		} catch (Exception e) {
			baseResponse.addError(
					ErrorDetail.builder().message("Updating trip status in the database failed").errCode("").build());
			baseResponse.getStatus().setSuccess(false);
		}
		return baseResponse;
	}

}
