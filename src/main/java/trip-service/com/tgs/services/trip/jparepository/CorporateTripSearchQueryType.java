package com.tgs.services.trip.jparepository;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.trip.datamodel.CorporateTripSearchQuery;

public class CorporateTripSearchQueryType extends CustomUserType {

	@Override
	public Class<?> returnedClass() {
		return CorporateTripSearchQuery.class;
	}

	


}
