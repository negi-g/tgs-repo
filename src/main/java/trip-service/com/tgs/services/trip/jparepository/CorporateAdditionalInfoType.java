package com.tgs.services.trip.jparepository;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.trip.datamodel.CorporateAdditionalInfo;

public class CorporateAdditionalInfoType extends CustomUserType {

	@Override
	public Class<?> returnedClass() {
		return CorporateAdditionalInfo.class;
	}


}
