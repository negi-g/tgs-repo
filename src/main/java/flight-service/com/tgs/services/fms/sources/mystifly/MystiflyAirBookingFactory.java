package com.tgs.services.fms.sources.mystifly;

import com.tgs.services.base.SoapRequestResponseListner;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.OnePoint;
import onepoint.mystifly.OnePointStub;

@Slf4j
@Service
public class MystiflyAirBookingFactory extends AbstractAirBookingFactory {

	protected OnePointStub onePointStub;

	protected String fareSourceCode;

	protected String sessionId;

	protected MystiflyBindingService bindingService;

	protected SoapRequestResponseListner listener = null;

	public MystiflyAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);

	}

	public void initialize() {
		onePointStub = bindingService.getOnePointStub();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());

	}

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;
		bindingService = MystiflyBindingService.builder().configuration(supplierConf).build();
		initialize();
		sessionId = supplierSession.getSupplierSessionInfo().getSessionToken();
		fareSourceCode = bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getJourneyKey();
		MystiflyBookingManager bookingManager = MystiflyBookingManager.builder().onePointStub(onePointStub)
				.sessionId(sessionId).fareSourceCode(fareSourceCode).bookingId(bookingId).isHoldBooking(isHoldBooking)
				.criticalMessageLogger(criticalMessageLogger).order(order).deliveryInfo(deliveryInfo)
				.segmentInfos(bookingSegments.getSegmentInfos()).listener(listener).configuration(supplierConf)
				.bookingUser(bookingUser).build();

		MystiflyBookingRetreiveManager retreiveManager = MystiflyBookingRetreiveManager.builder()
				.criticalMessageLogger(criticalMessageLogger).onePointStub(onePointStub).sessionId(sessionId)
				.bookingId(bookingId).listener(listener).configuration(supplierConf).bookingUser(bookingUser).build();

		MystiflyReviewManager reviewManager = MystiflyReviewManager.builder().onePointStub(onePointStub)
				.bookingId(bookingId).criticalMessageLogger(criticalMessageLogger).sessionId(sessionId)
				.fareSourceCode(fareSourceCode).listener(listener).configuration(supplierConf).bookingUser(bookingUser)
				.build();

		double amountToBePaidToAirline = reviewManager.priceQuote(bookingSegments.getSegmentInfos());
		if (!isFareDiff(amountToBePaidToAirline)) {
			/**
			 * Now update the FSC to the new One that we received in re-validate response
			 */
			fareSourceCode = bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getJourneyKey();
			bookingManager.setFareSourceCode(fareSourceCode);
			pnr = bookingManager.createPNR(retreiveManager);
			if (isHoldBooking) {
				updateTimeLimit(TgsDateUtils.getCalendar(bookingManager.getTicketTimeLimit()));
			}
		}
		/**
		 * If we get MF(Supplier reference Number then it means it is Booked) Only in this case set isSuccess as true
		 */
		if (bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0)
				.getSupplierBookingId() != null) {
			isSuccess = true;
		}
		return isSuccess;
	}


	/**
	 * This need to be Call only in case of LCC Hold , We have only MF Number and status is HOLD, We need to call Ticket
	 * Order API , after its success we can call TripDetails for fetching Ticket Number and PNR
	 */
	@Override
	public boolean doConfirmBooking() {
		bindingService = MystiflyBindingService.builder().configuration(supplierConf).build();
		boolean isSuccess = false;
		MystiflySessionManager sessionManager = null;
		initialize();
		try {
			sessionManager = MystiflySessionManager.builder().configuration(supplierConf)
					.criticalMessageLogger(criticalMessageLogger).listener(listener).onePointStub(onePointStub)
					.bookingId(bookingId).build();
			if (supplierSession != null) {
				sessionId = supplierSession.getSupplierSessionInfo().getSessionToken();
			} else {
				sessionId = sessionManager.createSessionId();
			}
			MystiflyBookingManager bookingManager = MystiflyBookingManager.builder().onePointStub(onePointStub)
					.sessionId(sessionId).fareSourceCode(fareSourceCode).bookingId(bookingId).order(order)
					.deliveryInfo(deliveryInfo).segmentInfos(bookingSegments.getSegmentInfos())
					.configuration(supplierConf).listener(listener).criticalMessageLogger(criticalMessageLogger)
					.bookingUser(bookingUser).build();

			String supplierReference = bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo()
					.getTravellerInfo().get(0).getSupplierBookingId();
			MystiflyBookingRetreiveManager retreiveManager =
					MystiflyBookingRetreiveManager.builder().onePointStub(onePointStub).listener(listener)
							.sessionId(sessionId).bookingId(bookingId).configuration(supplierConf)
							.criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).build();
			if (StringUtils.isBlank(pnr)) {
				return false;
			}
			bookingManager.fetchTicketNumbersOrPnr(retreiveManager, supplierReference);
			pnr = bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getPnr();
			if (BookingUtils.hasTicketNumberInAllSegments(bookingSegments.getSegmentInfos())
					|| !pnr.equalsIgnoreCase("MYSPNR")) {
				isSuccess = true;
			} else {
				isSuccess = false;
			}
		} finally {
			// sessionManager.closeSession(sessionId);
		}
		return isSuccess;
	}
}
