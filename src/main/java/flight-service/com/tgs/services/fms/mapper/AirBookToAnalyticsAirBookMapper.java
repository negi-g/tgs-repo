package com.tgs.services.fms.mapper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import com.tgs.services.fms.analytics.AnalyticsAirQuery;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirAnalyticsUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AirBookToAnalyticsAirBookMapper extends Mapper<AnalyticsAirQuery> {

	private String bookingId;
	private User user;
	private ContextData contextData;
	private List<TripInfo> tripInfos;
	private AirSearchQuery searchQuery;
	private List<PaymentRequest> paymentRequests;
	private Order order;
	private String errorMessage;
	private AirAnalyticsType analyticsType;

	@Override
	protected void execute() throws CustomGeneralException {

		List<SegmentInfo> segmentInfos = AirUtils.getSegmentInfos(tripInfos);
		output = output != null ? output : AnalyticsAirQuery.builder().build();
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		AirReviewToAnalyticsAirReviewMapper.builder().searchQuery(searchQuery).tripInfos(tripInfos).bookingId(bookingId)
				.build().setOutput(output).convert();

		output.setFlowtype(getFlowType());
		output.setErrormsg(getErrorMessage());
		output.setBaggages(AirAnalyticsUtils.getBaggages(segmentInfos));
		output.setMeals(AirAnalyticsUtils.getMeal(segmentInfos));
		output.setSeats(AirAnalyticsUtils.getSeat(segmentInfos));
		output.setFflier(AirAnalyticsUtils.getFFlier(segmentInfos));
		output.setPaymentmedium(getPaymentMediums());
		output.setOrderassigneduserid(getOrderAssignedId());
		output.setItemstatus(AirAnalyticsUtils.getItemStatus(segmentInfos));

	}


	private String getPaymentMediums() {
		Set<String> paymentModes = new HashSet<>();
		if (CollectionUtils.isNotEmpty(paymentRequests)) {
			paymentRequests.forEach(paymentRequest -> {
				if (paymentRequest.getPaymentMedium() != null)
					paymentModes.add(paymentRequest.getPaymentMedium().name());
			});
			if (CollectionUtils.isNotEmpty(paymentModes)) {
				return paymentModes.stream().distinct().collect(Collectors.toList()).toString();
			}
		}
		return null;
	}

	private String getFlowType() {
		if (order.getAdditionalInfo().getFlowType() != null) {
			return order.getAdditionalInfo().getFlowType().getName();
		}
		return null;
	}

	private String getErrorMessage() {
		if (StringUtils.isNotBlank(errorMessage)) {
			return errorMessage;
		}
		return null;
	}

	private String getOrderAssignedId() {
		return StringUtils.isNotBlank(order.getAdditionalInfo().getAssignedUserId())
				? order.getAdditionalInfo().getAssignedUserId()
				: null;
	}

}
