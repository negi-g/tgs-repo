package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;

@Service
public class InternationalReturnSplitReviewEngine extends AbstractAirReviewEngine {

	@Override
	public List<TripInfo> reviewTrips(List<TripInfo> tripInfos, List<AirSearchQuery> searchQueries,
			ContextData contextData, String booking) {
		List<TripInfo> newTripInfos = new ArrayList<>();
		for (int i = 0; i < tripInfos.size(); i++) {
			newTripInfos.add(this.reviewTrip(tripInfos.get(i), searchQueries.get(0), false, contextData, booking));
		}
		return newTripInfos;
	}

}
