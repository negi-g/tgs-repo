package com.tgs.services.fms.sources.amadeusndc;

import lombok.Getter;
import com.tgs.services.base.enums.PaxType;

@Getter
public enum AmadeusNdcPaxType {

	ADT(PaxType.ADULT), CHD(PaxType.CHILD), INF(PaxType.INFANT);

	private PaxType paxType;

	AmadeusNdcPaxType(PaxType paxType) {
		this.paxType = paxType;
	}

	public static AmadeusNdcPaxType getEnumFromPaxType(PaxType paxType) {
		for (AmadeusNdcPaxType amadeusPaxType : AmadeusNdcPaxType.values()) {
			if (amadeusPaxType.getPaxType().equals(paxType)) {
				return amadeusPaxType;
			}
		}
		return null;
	}

}
