package com.tgs.services.fms.sources.amadeus;

import com.amadeus.xml.AmadeusCancelWebServicesStub;
import com.tgs.services.oms.datamodel.Order;
import org.springframework.stereotype.Service;
import com.amadeus.xml.AmadeusBookingWebServicesStub;
import com.amadeus.xml.AmadeusWebServicesStub;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;

@Service
public class AmadeusAirCancellationFactory extends AbstractAirCancellationFactory {

	protected SoapRequestResponseListner listener;

	protected AmadeusBindingService bindingService;

	protected AmadeusWebServicesStub webServicesStub;

	protected AmadeusBookingWebServicesStub bookingStub;

	protected AmadeusCancelWebServicesStub cancelStub;

	public AmadeusAirCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		super(bookingSegments, order, supplierConf);
	}

	private void initialize() {
		bindingService = AmadeusBindingService.builder().configuration(supplierConf).cacheCommunicator(cachingComm)
				.user(bookingUser).build();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		webServicesStub = bindingService.getWebServiceStub(null);
		cancelStub = bindingService.getCancelWebServiceStub(null);
	}

	@Override
	public boolean releaseHoldPNR() {
		boolean isCancelSuccess = false;
		AmadeusSessionManager sessionManager = AmadeusSessionManager.builder().listener(listener)
				.supplierConfiguration(supplierConf).bookingUser(bookingUser).build();
		pnr = bookingSegments.getSegmentInfos().get(0).getTravellerInfo().get(0).getSupplierBookingId();
		initialize();
		sessionManager.setWebServicesStub(webServicesStub);
		sessionManager.setListener(listener);
		try {
			sessionManager.openSession();
			AmadeusCancellationManager cancellationManager = AmadeusCancellationManager.builder().listener(listener)
					.supplierConfiguration(supplierConf).bookingId(bookingId).bookingUser(bookingUser).build();
			cancellationManager.setSession(sessionManager.getSessionSchema());
			cancellationManager.setCancelStub(cancelStub);
			cancellationManager.setWebServicesStub(webServicesStub);
			cancellationManager.setCriticalMessageLogger(criticalMessageLogger);
			isCancelSuccess = cancellationManager.cancelPnr(pnr);
		} finally {
			sessionManager.closeSession();
		}
		return isCancelSuccess;
	}


}
