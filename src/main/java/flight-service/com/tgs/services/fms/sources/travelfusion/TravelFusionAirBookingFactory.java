package com.tgs.services.fms.sources.travelfusion;

import com.tgs.services.base.utils.BookingUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.oms.datamodel.Order;
import TravelFusionRequests.CommonPrice;
import lombok.extern.slf4j.Slf4j;
import java.math.BigDecimal;

@Slf4j
@Service
public class TravelFusionAirBookingFactory extends AbstractAirBookingFactory {

	RestAPIListener listener = null;
	protected TravelFusionBindingService bindingService = null;
	protected String loginId = null;
	protected FlightAPIURLRuleCriteria apiURLS;
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public TravelFusionAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments,
			Order order) throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;
		String supplierReference = null;
		CommonPrice reqPrice = new CommonPrice();
		try {
			bindingService = TravelFusionBindingService.builder().configuration(supplierConf).build();
			CreditCardInfo card = getSupplierBookingCreditCard();
			listener = new RestAPIListener(bookingId);
			TravelFusionBookingManager bookingManager =
					TravelFusionBookingManager.builder().supplierConfiguration(supplierConf).bookingId(bookingId)
							.segmentInfos(bookingSegments.getSegmentInfos()).order(order).bindingService(bindingService)
							.deliveryInfo(order.getDeliveryInfo()).listener(listener).cachingComm(cachingComm)
							.bookingUser(bookingUser).build();
			bookingManager.initLoginId(true, bookingId);
			bookingManager.createPNR(isHoldBooking, card);
			supplierReference = bookingManager.getBookingOrderNo();
			TravelFusionResponses.CommonPrice price = null;
			if (bookingManager.processTerms.getTFBookingReference() != null
					&& bookingManager.processTerms.getRouter().getGroupList().getGroup().get(0).getPrice() != null) {
				price = bookingManager.processTerms.getRouter().getGroupList().getGroup().get(0).getPrice();
				double supplierAmount = bookingManager.getSupplierChargeableAmount(card, price);
				if (!isFareDiff(supplierAmount) && StringUtils.isNotBlank(supplierReference)) {
					reqPrice.setCurrency(price.getCurrency());
					reqPrice.setAmount(bookingManager.supplierChargeableAmount);
					supplierReference = bookingManager.startBook(reqPrice);
					String bookingPnr = bookingManager.checkBookingStatus(supplierReference);
					isTkRequired = bookingManager.isTicketRequired;
					pnr = bookingPnr;
				}
			}
		} finally {
			if (StringUtils.isNotBlank(pnr) && isTkRequired) {
				isSuccess = BookingUtils.hasPNRInAllSegments(bookingSegments.getSegmentInfos())
						&& BookingUtils.hasTicketNumberInAllSegments(bookingSegments.getSegmentInfos());
			} else if (StringUtils.isNotBlank(pnr)) {
				isSuccess = BookingUtils.hasPNRInAllSegments(bookingSegments.getSegmentInfos());
			}
			isTkRequired = false;
			log.info("TravelFusion Booking status for {} PNR {} and supplier reference {}", bookingId, pnr,
					supplierReference);
		}
		return isSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		// right now its not supported
		return false;
	}

	public double getAmountBasedOnCurrency(double amount, String fromCurrency) {
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (fromCurrency.equalsIgnoreCase(toCurrency)) {
			return amount;
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.type(AirSourceType.TRAVELFUSION.name().toUpperCase()).toCurrency(toCurrency).build();
		return moneyExchnageComm.getExchangeValue(amount, filter, true);
	}

}
