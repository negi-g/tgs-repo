package com.tgs.services.fms.sources.travelport.session;

import com.tgs.utils.exception.air.SupplierSessionException;
import com.travelport.www.schema.sharedbooking_v47_0.BookingStartReq;
import com.travelport.www.schema.sharedbooking_v47_0.BookingStartRsp;
import com.travelport.www.schema.sharedbooking_v47_0.SessionActivity_type1;
import com.travelport.www.schema.sharedbooking_v47_0.BookingEndReq;
import com.travelport.www.schema.sharedbooking_v47_0.BookingEndRsp;
import com.travelport.www.service.sharedbooking_v47_0.SharedBookingServiceStub;
import com.travelport.www.service.sharedbooking_v47_0.SystemFaultMessage;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.rmi.RemoteException;
import java.util.*;

@Slf4j
@SuperBuilder
final class TravelPortSessionManager extends TravelPortServiceManager {

	public String createSession() throws SupplierSessionException {
		String sessionKey = StringUtils.EMPTY;
		SharedBookingServiceStub serviceStub = bindingService.getSharedBookingService();
		listener.setType("Session");
		BookingStartRsp bookingBaseRsp = null;
		try {
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			BookingStartReq bookingStartReq = new BookingStartReq();
			bookingStartReq.setAuthorizedBy(getAuthorizedBy());
			bookingStartReq
					.setTargetBranch(getTypeBranchCode(configuration.getSupplierCredential().getOrganisationCode()));
			bookingStartReq.setTraceId(traceId);
			bookingStartReq.setBillingPointOfSaleInfo(buildBillingPointOfSale());
			if (StringUtils.isNotBlank(providerCode)) {
				bookingStartReq.setProviderCode(getProviderCode(providerCode));
			}
			bindingService.setProxyAndAuthentication(serviceStub, "CreateSession");
			bookingBaseRsp = serviceStub.service(bookingStartReq);
			if (bookingBaseRsp != null && !checkAnyErrors(bookingBaseRsp)) {
				sessionKey = bookingBaseRsp.getBookingStartRsp().getSessionKey().getTypeRef();
			}
		} catch (RemoteException | SystemFaultMessage fault) {
			logCriticalMessage(fault.getMessage());
			throw new SupplierSessionException(fault.getMessage());
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return sessionKey;
	}

	public void endSession() {
		if (StringUtils.isNotBlank(sessionkey)) {
			SharedBookingServiceStub serviceStub = bindingService.getSharedBookingService();
			BookingEndReq bookingEndReq = new BookingEndReq();
			buildBaseCoreRequest(bookingEndReq);
			bookingEndReq.setSessionKey(getTypeSessionKey(sessionkey));
			bookingEndReq.setSessionActivity(SessionActivity_type1.Ignore);
			listener.setType("EndSession");
			try {
				serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
				bindingService.setProxyAndAuthentication(serviceStub, "EndSession");
				BookingEndRsp bookingEndRsp = serviceStub.service(bookingEndReq, getSessionContext(true));
				if (!Objects.isNull(bookingEndRsp.getBookingEndRsp().getResponseMessage())) {
					log.info(getList(bookingEndRsp.getBookingEndRsp().getResponseMessage()).get(0).getString());
				}
			} catch (SystemFaultMessage systemFaultMessage) {
				logCriticalMessage(systemFaultMessage.toString());
			} catch (Exception e) {
				logCriticalMessage(e.getMessage());
			} finally {
				serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			}
		}
	}
}
