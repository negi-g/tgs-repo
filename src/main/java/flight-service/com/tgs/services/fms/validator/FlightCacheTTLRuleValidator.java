package com.tgs.services.fms.validator;

import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.CacheFilterOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirCacheFilter;

import java.util.Objects;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;


@Service
public class FlightCacheTTLRuleValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T output) {

		CacheFilterOutput ttlConfiguration = (CacheFilterOutput) output;

		if (ttlConfiguration == null || ttlConfiguration.getTtlConfig() == null) {
			errors.rejectValue(fieldName, SystemError.NULL_VALUE.errorCode(), SystemError.NULL_VALUE.getMessage());
			return;
		}

		for (AirCacheFilter params : ttlConfiguration.getTtlConfig()) {
			if (params.getStartPeriod() < 0 || Objects.isNull(params.getDiffTime()) || params.getDiffTime() < 0
					|| params.getEndPeriod() < 0) {
				errors.rejectValue(fieldName, SystemError.NEGATIVE_VALUE.errorCode(),
						SystemError.NEGATIVE_VALUE.getMessage());
				return;
			}
			if (params.getEndPeriod() < params.getStartPeriod()) {
				errors.rejectValue(fieldName, SystemError.INVALID_PERIOD.errorCode(),
						SystemError.INVALID_PERIOD.getMessage());
				return;
			}
		}


	}
}
