package com.tgs.services.fms.helper;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.manager.AirReviewEngine;
import com.tgs.services.fms.manager.MultiCityReviewEngine;
import com.tgs.services.fms.manager.DomReturnReviewEngine;
import com.tgs.services.fms.manager.InternationalReturnSplitReviewEngine;

@Service
public class AirReviewManager {

	@Autowired
	private AirReviewEngine reviewEngine;

	@Autowired
	private DomReturnReviewEngine domReturnReviewEngine;

	@Autowired
	private MultiCityReviewEngine multicityReviewEngine;

	@Autowired
	private InternationalReturnSplitReviewEngine returnSplitReviewEngine;

	public void storeSearchLogs(List<AirSearchQuery> searchQueries, String bookingId) {
		if (CollectionUtils.isNotEmpty(searchQueries)) {
			SystemContextHolder.getContextData().addReqIds(bookingId, searchQueries.get(0).getSearchId());
			ExecutorUtils.getGeneralPurposeThreadPool().submit(() -> {
				for (AirSearchQuery searchQuery : searchQueries) {
					LogUtils.copyLog(searchQuery.getSearchId(), bookingId);
				}
			});
		}
	}

	public List<TripInfo> reviewTrips(List<AirSearchQuery> searchQueries, List<TripInfo> tripInfos, String bookingId) {
		LogUtils.log(LogTypes.AIR_REVIEW_REVIEWTRIP, null, LogTypes.SYSTEM_FILTER);

		AirSearchQuery searchQuery = searchQueries.get(0);
		SystemContextHolder.getContextData().setBookingId(bookingId);
		if (searchQueries.size() == 1 && (searchQuery.isOneWay() || (!searchQuery.isSplitSearch()
				&& searchQuery.isIntl() && (searchQuery.isMultiCity() || searchQuery.isReturn())))) {

			// 1. International/Domestic Oneway 2. International return/ multicity combined view
			return reviewEngine.reviewTrips(tripInfos, searchQueries, SystemContextHolder.getContextData(), bookingId);

		} else if (searchQuery.isDomesticReturn()) {

			return domReturnReviewEngine.reviewTrips(tripInfos, searchQueries, SystemContextHolder.getContextData(),
					bookingId);

		} else if (searchQuery.isSplitSearch() && searchQuery.isIntlReturn()) {

			return returnSplitReviewEngine.reviewTrips(tripInfos, searchQueries, SystemContextHolder.getContextData(),
					bookingId);

		} else {

			// 1.Domestic Multicity 2.International Multicity split view.
			return multicityReviewEngine.reviewTrips(tripInfos, searchQueries, SystemContextHolder.getContextData(),
					bookingId);

		}
	}
}
