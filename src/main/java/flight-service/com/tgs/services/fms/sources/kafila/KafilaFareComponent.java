package com.tgs.services.fms.sources.kafila;

import com.tgs.services.base.enums.FareComponent;

public enum KafilaFareComponent {

	ab("adult base") {
		public FareComponent getFareComponent() {
			return FareComponent.BF;
		}
	},
	ay("adult yq") {
		public FareComponent getFareComponent() {
			return FareComponent.YQ;
		}
	},
	at("adult tax") {
		public FareComponent getFareComponent() {
			return FareComponent.AT;
		}
	},
	cb("child base") {
		public FareComponent getFareComponent() {
			return FareComponent.BF;
		}
	},
	cy("child yq") {
		public FareComponent getFareComponent() {
			return FareComponent.YQ;
		}
	},
	ct("child tax") {
		public FareComponent getFareComponent() {
			return FareComponent.AT;
		}
	},
	ib("infant base") {
		public FareComponent getFareComponent() {
			return FareComponent.BF;
		}
	},
	iy("infant yq") {
		public FareComponent getFareComponent() {
			return FareComponent.YQ;
		}
	},
	it("infant tax") {
		public FareComponent getFareComponent() {
			return FareComponent.AT;
		}
	};

	private String desc;

	KafilaFareComponent(String desc) {
		this.desc = desc;
	}
}
