package com.tgs.services.fms.sources.kafila;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.SegmentMiscInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.manager.TripPriceEngine;
import com.tgs.services.kafila.searchRequest.SearchDetails;
import com.tgs.services.kafila.searchRequest.SearchRequest;
import com.tgs.services.kafila.searchresponse.ConnectingFlightDetails;
import com.tgs.services.kafila.searchresponse.FlightDetails;
import com.tgs.services.kafila.searchresponse.SearchResponse;
import com.tgs.utils.exception.air.NoSearchResultException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class KafilaSearchManager extends KafilaServiceManager {

	private String tripString;

	private String uuid;

	public AirSearchResult doSearch() {
		uuid = getUUID();
		SearchRequest searchRequest = buildSearchRequest();
		SearchResponse searchResponse = bindingService.doSearch(searchRequest);
		if (searchResponse.isError()) {
			throw new NoSearchResultException(searchResponse.errorMessage());
		}
		AirSearchResult searchResult = buildSearchResult(searchResponse);
		return searchResult;
	}

	private AirSearchResult buildSearchResult(SearchResponse searchResponse) {
		AirSearchResult searchResult = new AirSearchResult();
		tripString = GsonUtils.getGson().toJson(searchResponse.getPARAM().get(0));

		if (searchQuery.isIntlReturn()) {
			List<TripInfo> onwardTripInfos = parseSearchResponse(searchResponse, "FLIGHTOW");
			List<TripInfo> returnTripInfos = parseSearchResponse(searchResponse, "FLIGHTRT");
			List<TripInfo> combinedTrips = setCombo(onwardTripInfos, returnTripInfos);
			searchResult.getTripInfos().put(TripInfoType.COMBO.getName(), combinedTrips);
		} else if (searchQuery.isDomesticReturn()) {
			List<TripInfo> onwardTripInfos = parseSearchResponse(searchResponse, "FLIGHTOW");
			searchResult.getTripInfos().put(TripInfoType.ONWARD.getName(), onwardTripInfos);
			List<TripInfo> returnTripInfos = parseSearchResponse(searchResponse, "FLIGHTRT");
			searchResult.getTripInfos().put(TripInfoType.RETURN.getName(), returnTripInfos);
		} else {
			List<TripInfo> tripInfos = parseSearchResponse(searchResponse, "FLIGHT");
			searchResult.getTripInfos().put(TripInfoType.ONWARD.getName(), tripInfos);
		}
		return searchResult;
	}

	private SearchRequest buildSearchRequest() {
		SearchRequest searchRequest = SearchRequest.builder().build();
		List<SearchDetails> searchDetailsList = new ArrayList<>();
		SearchDetails searchDetail = SearchDetails.builder().build();
		searchDetail.setAUTH_TOKEN(getToken());
		searchDetail.setSESSION_ID(uuid);
		searchDetail.setADT(Integer.toString(getAdults()));
		searchDetail.setCHD(Integer.toString(getChildren()));
		searchDetail.setINF(Integer.toString(getInfants()));
		searchDetail.setPC(KafilaCabinClass.getCabinClass(searchQuery));
		searchDetail.setSRC(searchQuery.getRouteInfos().get(0).getFromCityAirportCode());
		searchDetail.setDES(searchQuery.getRouteInfos().get(0).getToCityAirportCode());
		searchDetail.setDEP_DATE(searchQuery.getRouteInfos().get(0).getTravelDate().toString());
		if (searchQuery.isReturn()) {
			searchDetail.setRET_DATE(searchQuery.getRouteInfos().get(1).getTravelDate().toString());
		}
		searchDetail.setHS(getHitSrc());
		searchDetail.setTRIP(getTripType());
		searchDetail.setSECTOR(getSector());
		if (supplierConfiguration.isTestCredential() && CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())) {
			searchDetail.setPF(searchQuery.getPrefferedAirline());
		}
		searchDetailsList.add(searchDetail);
		searchRequest.setSTR(searchDetailsList);
		return searchRequest;
	}


	public List<TripInfo> parseSearchResponse(SearchResponse responseBody, String key) {
		List<TripInfo> tripInfos = new ArrayList<>();
		List<FlightDetails> jounrneys = null;
		boolean isReturnTrips = false;

		if (StringUtils.equalsIgnoreCase("FLIGHTOW", key)) {
			jounrneys = responseBody.getFLIGHTOW();
		} else if (StringUtils.equalsIgnoreCase("FLIGHTRT", key)) {
			jounrneys = responseBody.getFLIGHTRT();
			isReturnTrips = true;
		} else if (StringUtils.equalsIgnoreCase("FLIGHT", key)) {
			jounrneys = responseBody.getFLIGHT();
		}

		for (FlightDetails trip : jounrneys) {
			try {
				TripInfo tripInfo = new TripInfo();
				List<SegmentInfo> segmentInfos = new ArrayList<>();
				SegmentInfo segmentInfo;
				if (trip.getCON_DETAILS() == null) {
					Map<String, String> additionalInfo =
							buildFareProperties(trip.getFLIGHT_INFO(), trip.getSeat(), trip.getFareType());
					segmentInfo = populateDirectSegmentInfo(trip);
					segmentInfo.getDepartAirportInfo().setTerminal(additionalInfo.getOrDefault(DEPART_TERMINAL, null));
					segmentInfo.getArrivalAirportInfo()
							.setTerminal(additionalInfo.getOrDefault(ARRIVAL_TERMINAL, null));
					segmentInfo.setIsReturnSegment(isReturnTrips);
					segmentInfo = populatePriceInfo(trip, segmentInfo, additionalInfo);
					segmentInfos.add(segmentInfo);
				} else {
					int segmentNum = 0;
					for (int i = 0; i <= Integer.valueOf(trip.getSTOP()); i++) {
						ConnectingFlightDetails connectingLegs = trip.getCON_DETAILS().get(i);
						if (i < Integer.valueOf(trip.getSTOP())
								&& trip.getCON_DETAILS().get(i + 1).getFLIGHT_NO().equals(connectingLegs.getFLIGHT_NO())
								&& connectingLegs.getARRV_TIME()
										.equals(trip.getCON_DETAILS().get(i + 1).getDEP_TIME())) {
							int flyThroughStops = 0;
							List<AirportInfo> stopAirports = new ArrayList<AirportInfo>();
							ConnectingFlightDetails lastLeg = trip.getCON_DETAILS().get(i + 1);
							while (trip.getCON_DETAILS().get(i + 1).getFLIGHT_NO()
									.equals(connectingLegs.getFLIGHT_NO())) {
								flyThroughStops++;
								lastLeg = trip.getCON_DETAILS().get(i + 1);
								stopAirports.add(AirportHelper.getAirport(lastLeg.getORG_CODE()));
								i++;
								if (i == Integer.valueOf(trip.getSTOP())) {
									break;
								}
							}
							segmentInfo =
									populateFlyThroughSegment(connectingLegs, flyThroughStops, stopAirports, lastLeg);
						} else {
							segmentInfo = populateSegmentInfo(connectingLegs);
						}
						segmentInfo.setIsReturnSegment(isReturnTrips);
						segmentInfo.setSegmentNum(segmentNum++);
						segmentInfo = populatePriceInfo(trip, segmentInfo, null);
						segmentInfos.add(segmentInfo);
					}
				}
				tripInfo.getSegmentInfos().addAll(segmentInfos);
				SegmentMiscInfo misc = SegmentMiscInfo.builder().combinationId(trip.getID()).build();
				tripInfo.getSegmentInfos().get(0).setMiscInfo(misc);
				tripInfos.add(tripInfo);
			} catch (Exception e) {
				log.error("Unable to parse trip {}", searchQuery.getSearchId(), e);
			}
		}
		return tripInfos;
	}


	public SegmentInfo populatePriceInfo(FlightDetails trip, SegmentInfo segmentInfo,
			Map<String, String> additionalInfo) {

		Map<KafilaFareComponent, Double> fareMap = buildFareMap(trip.getTAX_BREAKUP());

		if (MapUtils.isEmpty(additionalInfo)) {
			additionalInfo = buildFareProperties(trip.getFLIGHT_INFO(), trip.getSeat(), trip.getFareType());
		}

		List<PriceInfo> priceinfos = new ArrayList<PriceInfo>();
		PriceInfo priceinfo = PriceInfo.builder(searchQuery.getRequestId()).build();
		priceinfo.setSupplierBasicInfo(supplierConfiguration.getBasicInfo());
		priceinfo.setFareIdentifier(KafilaUtils.getFareTypeOnPCC(trip));

		Map<PaxType, FareDetail> priceInfoMap = new HashMap<>();

		FareDetail adultFareDetail = new FareDetail();
		Double baseFare = fareMap.getOrDefault(KafilaFareComponent.ab, 0D);
		Double tax = fareMap.getOrDefault(KafilaFareComponent.at, 0D);
		Double fuelCharge = fareMap.getOrDefault(KafilaFareComponent.ay, 0D);
		adultFareDetail.setFareComponents(getFareComponents(segmentInfo, baseFare, tax, fuelCharge));
		setCabinClass(adultFareDetail, trip.getF_CLASS());
		setFareProperties(additionalInfo, adultFareDetail, PaxType.ADULT);
		priceInfoMap.put(PaxType.ADULT, adultFareDetail);


		if (getChildren() > 0) {
			FareDetail childFareDetail = new FareDetail();
			baseFare = fareMap.getOrDefault(KafilaFareComponent.cb, 0D);
			tax = fareMap.getOrDefault(KafilaFareComponent.ct, 0D);
			fuelCharge = fareMap.getOrDefault(KafilaFareComponent.cy, 0D);
			setCabinClass(childFareDetail, trip.getF_CLASS());
			childFareDetail.setFareComponents(getFareComponents(segmentInfo, baseFare, tax, fuelCharge));
			setFareProperties(additionalInfo, childFareDetail, PaxType.CHILD);
			priceInfoMap.put(PaxType.CHILD, childFareDetail);
		}


		if (getInfants() > 0) {
			FareDetail infantFareDetail = new FareDetail();
			baseFare = fareMap.getOrDefault(KafilaFareComponent.ib, 0D);
			tax = fareMap.getOrDefault(KafilaFareComponent.it, 0D);
			fuelCharge = fareMap.getOrDefault(KafilaFareComponent.iy, 0D);
			setCabinClass(infantFareDetail, trip.getF_CLASS());
			infantFareDetail.setFareComponents(getFareComponents(segmentInfo, baseFare, tax, fuelCharge));
			setFareProperties(additionalInfo, infantFareDetail, PaxType.INFANT);
			priceInfoMap.put(PaxType.INFANT, infantFareDetail);
		}

		priceinfo.setFareDetails(priceInfoMap);

		PriceMiscInfo miscInfo = priceinfo.getMiscInfo();
		miscInfo.setJourneyKey(trip.getUID());
		miscInfo.setSegmentKey(trip.getID());
		miscInfo.setAirPriceSolutionKey(trip.getTID());
		miscInfo.setTokenId(tripString);
		miscInfo.setLegNum(0);
		miscInfo.setTraceId(uuid);
		priceinfo.setMiscInfo(miscInfo);
		priceinfos.add(priceinfo);
		segmentInfo.setPriceInfoList(priceinfos);
		return segmentInfo;
	}

	private Map<FareComponent, Double> getFareComponents(SegmentInfo segmentInfo, Double baseFare, Double tax,
			Double fuelCharge) {
		Map<FareComponent, Double> fareMap = new HashMap<>();
		if (segmentInfo.getSegmentNum() == 0
				&& !(searchQuery.isIntlReturn() && BooleanUtils.isTrue(segmentInfo.getIsReturnSegment()))) {
			fareMap.put(FareComponent.BF, baseFare);
			fareMap.put(FareComponent.YQ, fuelCharge);
			fareMap.put(FareComponent.AT, tax);
			fareMap.put(FareComponent.TF, baseFare + tax + fuelCharge);
		} else {
			fareMap.put(FareComponent.BF, 0D);
			fareMap.put(FareComponent.YQ, 0D);
			fareMap.put(FareComponent.AT, 0D);
			fareMap.put(FareComponent.TF, 0D);
		}
		return fareMap;
	}


	private void setFareProperties(Map<String, String> additionalInfo, FareDetail fareDetail, PaxType paxType) {
		Integer seatCount = Integer.valueOf(additionalInfo.get(SEAT));
		if (seatCount != null && !PaxType.INFANT.equals(paxType)) {
			fareDetail.setSeatRemaining(seatCount);
		}

		String fareBasis = additionalInfo.getOrDefault(FAREBASIS, null);
		if (StringUtils.isBlank(fareDetail.getFareBasis()) && StringUtils.isNotBlank(fareBasis)) {
			fareDetail.setFareBasis(fareBasis);
		}

		String classofBook = additionalInfo.getOrDefault(CLASSOFBOOK, null);
		if (StringUtils.isBlank(fareDetail.getClassOfBooking()) && StringUtils.isNotBlank(classofBook)) {
			fareDetail.setClassOfBooking(classofBook);
		}

		String cabinBaggage = additionalInfo.getOrDefault(CABIN_BAGGAGE, null);
		String checkInBaggage = additionalInfo.getOrDefault(CHECKIN_BAGGAGE, null);
		BaggageInfo baggageInfo = fareDetail.getBaggageInfo();
		if (StringUtils.isBlank(baggageInfo.getCabinBaggage()) && StringUtils.isNotBlank(cabinBaggage)) {
			baggageInfo.setCabinBaggage(cabinBaggage);
		}
		if (StringUtils.isBlank(baggageInfo.getAllowance()) && StringUtils.isNotBlank(checkInBaggage)) {
			baggageInfo.setAllowance(checkInBaggage);
		}

		String refundable = additionalInfo.getOrDefault(REFUNDABLE, null);
		if (StringUtils.equalsIgnoreCase(refundable, "REFUNDABLE")) {
			fareDetail.setRefundableType(RefundableType.REFUNDABLE.getRefundableType());
		} else {
			fareDetail.setRefundableType(RefundableType.NON_REFUNDABLE.getRefundableType());
		}

	}

	private Map<String, String> buildFareProperties(String flight_info, String seatCount, String fType) {

		Map<String, String> properties = new HashMap<>();
		String[] infos = StringUtils.split(flight_info, "|");
		properties.put(REFUNDABLE, infos[0]);
		String terminal = infos[1];
		properties.put(DEPART_TERMINAL, StringUtils.split(terminal, ",")[0]);
		properties.put(ARRIVAL_TERMINAL, StringUtils.split(terminal, ",")[1]);
		properties.put(SEAT, seatCount);
		String baggageText = infos[2];
		if (StringUtils.isNotBlank(baggageText)) {
			baggageText = StringUtils.replaceOnceIgnoreCase(baggageText, "Baggage:", "");
			if (StringUtils.containsIgnoreCase(baggageText, "Cabin")) {
				String cabinBaggage = StringUtils.split(baggageText, ",")[0];
				if (StringUtils.isNotBlank(cabinBaggage)) {
					properties.put(CABIN_BAGGAGE, StringUtils.replaceOnceIgnoreCase(cabinBaggage, "Cabin=", ""));
				}
			}
			if (StringUtils.containsIgnoreCase(baggageText, "Checkin")) {
				String checkInBaggage = StringUtils.split(baggageText, ",")[1];
				if (StringUtils.isNotBlank(checkInBaggage)) {
					properties.put(CHECKIN_BAGGAGE, StringUtils.replaceOnceIgnoreCase(checkInBaggage, "Checkin=", ""));
				}
			}
		}
		if (infos.length >= 5) {
			String fareBasis = infos[4];
			if (StringUtils.length(fareBasis) <= 10) {
				properties.put(FAREBASIS, StringUtils.replaceOnceIgnoreCase(fareBasis, "CLASS=", ""));
			}
		}
		if (StringUtils.isNotBlank(fType)) {
			properties.put(CLASSOFBOOK, fType);
		}
		return properties;
	}

	private void setCabinClass(FareDetail fareDetail, String f_class) {
		if (StringUtils.equalsIgnoreCase(f_class, "Economy")) {
			fareDetail.setCabinClass(CabinClass.ECONOMY);
		} else if (StringUtils.equalsIgnoreCase(f_class, "Business")) {
			fareDetail.setCabinClass(CabinClass.BUSINESS);
		} else if (StringUtils.equalsIgnoreCase(f_class, "PremiumEconomy")) {
			fareDetail.setCabinClass(CabinClass.PREMIUM_ECONOMY);
		} else {
			fareDetail.setCabinClass(CabinClass.ECONOMY);
		}
	}

	private Map<KafilaFareComponent, Double> buildFareMap(String taxesStr) {
		String[] taxBreakUpWithFareRule = StringUtils.split(taxesStr, "|");
		Map<KafilaFareComponent, Double> fareComponentMap = new HashMap<>();
		if (ArrayUtils.isNotEmpty(taxBreakUpWithFareRule)) {
			String taxes = taxBreakUpWithFareRule[0];
			String[] listOfFares = null;
			if (searchQuery.isIntlReturn()) {
				String[] split = taxes.split("\\|");
				String[] parts1 = split[0].split(",");
				String[] parts2 = split[1].split(",");

				String[] parts = new String[parts1.length + parts2.length];
				System.arraycopy(parts1, 0, parts, 0, parts1.length);
				System.arraycopy(parts2, 0, parts, parts1.length, parts2.length);
				listOfFares = parts;
			} else {
				listOfFares = StringUtils.split(taxes, ",");
			}

			for (String fareComp : listOfFares) {
				String component = StringUtils.split(fareComp, "=")[0];
				Double amount = Double.valueOf(StringUtils.split(fareComp, "=")[1]);
				KafilaFareComponent fareComponent = KafilaFareComponent.valueOf(component);
				if (fareComponentMap.containsKey(fareComponent) && searchQuery.isIntlReturn()) {
					fareComponentMap.put(fareComponent, fareComponentMap.get(fareComponent) + amount);
				} else {
					fareComponentMap.put(fareComponent, amount);
				}
			}
		}
		return fareComponentMap;
	}


	public SegmentInfo populateDirectSegmentInfo(FlightDetails trip) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(trip.getACode()));
		segmentInfo.setArrivalTime(KafilaUtils.convertStringToDate(trip.getADate(), trip.getATime()));
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(trip.getD_CODE()));
		segmentInfo.setDepartTime(KafilaUtils.convertStringToDate(trip.getDDate(), trip.getDTime()));
		segmentInfo.setFlightDesignator(createFlightDesignator(trip.getFCode(), trip.getFlightNo()));
		segmentInfo.setStops(0);
		segmentInfo.setSegmentNum(0);
		segmentInfo.setIsReturnSegment(false);

		if (StringUtils.isNotBlank(trip.getDUR())) {
			segmentInfo.setDuration(KafilaUtils.toMinutes(trip.getDUR()));
		} else {
			segmentInfo.calculateDuration();
		}

		return segmentInfo;
	}

	public SegmentInfo populateFlyThroughSegment(ConnectingFlightDetails flight1, int flyThroughStops,
			List<AirportInfo> airports, ConnectingFlightDetails flight2) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(flight2.getDES_CODE()));
		segmentInfo.setArrivalTime(KafilaUtils.convertStringToDate(flight2.getARRV_DATE(), flight2.getARRV_TIME()));
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(flight1.getORG_CODE()));
		segmentInfo.setDepartTime(KafilaUtils.convertStringToDate(flight1.getDEP_DATE(), flight1.getDEP_TIME()));
		segmentInfo.setFlightDesignator(createFlightDesignator(flight1.getFLIGHT_CODE(), flight1.getFLIGHT_NO()));
		segmentInfo.setStops(Integer.valueOf(flyThroughStops));
		segmentInfo.setStopOverAirports(airports);
		segmentInfo.setDuration(segmentInfo.calculateDuration());

		return segmentInfo;
	}


	public SegmentInfo populateSegmentInfo(ConnectingFlightDetails connectingLeg) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(connectingLeg.getDES_CODE()));
		segmentInfo.setArrivalTime(
				KafilaUtils.convertStringToDate(connectingLeg.getARRV_DATE(), connectingLeg.getARRV_TIME()));
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(connectingLeg.getORG_CODE()));
		segmentInfo.setDepartTime(
				KafilaUtils.convertStringToDate(connectingLeg.getDEP_DATE(), connectingLeg.getDEP_TIME()));
		segmentInfo.setFlightDesignator(
				createFlightDesignator(connectingLeg.getFLIGHT_CODE(), connectingLeg.getFLIGHT_NO()));
		segmentInfo.setStops(0);

		if (StringUtils.isNotBlank(connectingLeg.getDURATION())) {
			segmentInfo.setDuration(KafilaUtils.toMinutes(connectingLeg.getDURATION()));
		} else {
			segmentInfo.setDuration(segmentInfo.calculateDuration());
		}

		return segmentInfo;

	}

	private FlightDesignator createFlightDesignator(String flightCode, String flightNo) {
		FlightDesignator flightDesign = new FlightDesignator();
		flightDesign.setAirlineInfo(AirlineHelper.getAirlineInfo(flightCode));
		flightDesign.setFlightNumber(flightNo.trim());
		return flightDesign;
	}

	private List<TripInfo> setCombo(List<TripInfo> onwardTrips, List<TripInfo> returnTrips) {
		List<TripInfo> result = new ArrayList<TripInfo>();
		for (TripInfo onwardTrip : onwardTrips) {
			for (TripInfo returnTrip : returnTrips) {
				if (onwardTrip.getSegmentInfos().get(0).getMiscInfo().getCombinationId()
						.equals(returnTrip.getSegmentInfos().get(0).getMiscInfo().getCombinationId())) {
					TripInfo copyOnwardTrip = new GsonMapper<>(onwardTrip, TripInfo.class).convert();
					TripInfo copyReturnTrip = new GsonMapper<>(returnTrip, TripInfo.class).convert();
					TripPriceEngine.filterComboPriceInfos(copyOnwardTrip, copyReturnTrip);
					if (copyOnwardTrip.isPriceInfosNotEmpty() && copyReturnTrip.isPriceInfosNotEmpty()) {
						copyReturnTrip.getSegmentInfos().get(0).setIsReturnSegment(true);
						copyOnwardTrip.getSegmentInfos().addAll(copyReturnTrip.getSegmentInfos());
						copyOnwardTrip.getSegmentInfos().forEach(segmentInfo -> {
							segmentInfo.setMiscInfo(null);
						});
						result.add(copyOnwardTrip);
					}
				}
			}
		}
		return result;
	}
}
