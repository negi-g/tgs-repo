package com.tgs.services.fms.sources.kafila;

import java.time.LocalDateTime;
import java.util.UUID;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
public abstract class KafilaServiceManager {

	protected static final String REFUNDABLE = "REFUNDABLE";
	protected static final String SEAT = "SEAT";
	protected static final String CABIN_BAGGAGE = "CABIN_BAGGAGE";
	protected static final String CHECKIN_BAGGAGE = "CHECKIN_BAGGAGE";
	protected static final String CLASSOFBOOK = "CLASS";
	protected static final String FAREBASIS = "F_TYPE";
	protected static final String DEPART_TERMINAL = "D_TERMINAL";
	protected static final String ARRIVAL_TERMINAL = "A_TERMINAL";

	private static final char DEVELOPMENT = 'D';
	private static final char PRODUCTION = 'P';

	protected String token;
	protected String pnr;

	protected User user;
	protected RestAPIListener listener;
	protected AirSearchQuery searchQuery;
	protected SupplierConfiguration supplierConfiguration;

	protected int adults;
	protected int children;
	protected int infants;

	private static final String ONE_WAY = "1";
	private static final String ROUND_TRIP = "2";

	private static char DOMESTIC = 'D';
	private static char INTL = 'I';

	protected KafilaBindingService bindingService;

	protected String bookingId;

	public void init() {
		if (searchQuery != null) {
			adults = AirUtils.getParticularPaxCount(searchQuery, PaxType.ADULT);
			children = AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD);
			infants = AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT);
		}
	}

	public String formatRQRS(String message, String type) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(message)) {
			buffer.append(type + " : " + LocalDateTime.now().toString()).append("\n\n").append(message).append("\n\n");
		}
		return buffer.toString();
	}


	public String getDomain() {
		if (StringUtils.isBlank(supplierConfiguration.getSupplierCredential().getDomain())) {
			return "B2B";
		}
		return supplierConfiguration.getSupplierCredential().getDomain();
	}

	public char getSector() {
		return BooleanUtils.isTrue(searchQuery.isIntl()) ? INTL : DOMESTIC;
	}

	public String getTripType() {
		if (searchQuery != null) {
			if (searchQuery.isOneWay()) {
				return ONE_WAY;
			} else {
				return ROUND_TRIP;
			}
		}
		return ONE_WAY;
	}

	public char getHitSrc() {
		return supplierConfiguration.isTestCredential() ? DEVELOPMENT : PRODUCTION;
	}

	protected String getUUID() {
		if (StringUtils.isNotBlank(supplierConfiguration.getSupplierCredential().getProviderCode())) {
			return supplierConfiguration.getSupplierCredential().getProviderCode();
		}
		return UUID.randomUUID().toString();
	}
}
