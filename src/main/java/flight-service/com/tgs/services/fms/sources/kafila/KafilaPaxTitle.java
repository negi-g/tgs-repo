package com.tgs.services.fms.sources.kafila;

import lombok.Getter;

@Getter
public enum KafilaPaxTitle {

	Mstr("MASTER"), Miss("MS"), Mr("MR"), Ms("MS"), Mrs("MRS");

	String code;

	KafilaPaxTitle(String code) {
		this.code = code;
	}

	public String getCode() {
		return this.code;
	}

	public static KafilaPaxTitle getEnumFromCode(String code) {
		for (KafilaPaxTitle enumCode : KafilaPaxTitle.values()) {
			if (enumCode.code.equals(code)) {
				return enumCode;
			}
		}
		return null;
	}
}
