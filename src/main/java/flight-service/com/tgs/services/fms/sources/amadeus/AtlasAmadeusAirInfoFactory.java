package com.tgs.services.fms.sources.amadeus;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AtlasAmadeusAirInfoFactory extends AmadeusAirInfoFactory {

	public AtlasAmadeusAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public AtlasAmadeusAirInfoFactory(SupplierConfiguration supplierConf) {
		super(supplierConf);
	}

	@Override
	public void initNoOfRecommendations() {

		Map<CabinClass, Integer> fareFamily = new LinkedHashMap<>();
		Integer noOfRecommndation = AmadeusUtils.getItinCount(sourceConfiguration, searchQuery).intValue();

		if (searchQuery.isIntl() && !searchQuery.getCabinClass().equals(CabinClass.ECONOMY)) {
			searchManager.isCabinRequest = true;
		}

		if (!searchManager.isCabinRequest && searchQuery.getCabinClass() != null && !searchQuery.isIntl()
				&& !isIntlMulticity()) {
			List<CabinClass> cabinClasses = new ArrayList<>(searchQuery.getCabinClass().getAllowedCabinClass());
			cabinClasses.remove(CabinClass.PREMIUM_BUSINESS);
			cabinClasses.remove(CabinClass.PREMIUMFIRST);
			if (cabinClasses.size() > 6) {
				// Fare Family Not Supported More Than 6
				cabinClasses.removeAll(cabinClasses.subList(6, cabinClasses.size()));
			}
			if (searchQuery.isIntl()) {
				cabinClasses = Arrays.asList(searchQuery.getCabinClass());
			}

			Integer eachClassCount = noOfRecommndation / cabinClasses.size();
			cabinClasses.forEach(cabinClass -> {
				fareFamily.put(cabinClass, eachClassCount);
			});
		}
		searchManager.setNO_OF_REC(new BigInteger(String.valueOf(noOfRecommndation)));
		searchManager.setFareFamily(fareFamily);
	}

	protected boolean isIntlMulticity() {
		return searchQuery.isMultiCity() && searchQuery.isIntl();
	}
}
