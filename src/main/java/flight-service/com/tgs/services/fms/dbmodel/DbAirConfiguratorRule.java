package com.tgs.services.fms.dbmodel;

import java.time.LocalDateTime;
import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.base.runtime.database.CustomTypes.FlightBasicRuleCriteriaType;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import lombok.Getter;
import lombok.Setter;

@Audited
@Getter
@Setter
@Entity
@Table(name = "airconfiguratorrule")
@TypeDefs({@TypeDef(name = "FlightBasicRuleCriteriaType", typeClass = FlightBasicRuleCriteriaType.class)})
public class DbAirConfiguratorRule extends BaseModel<DbAirConfiguratorRule, AirConfiguratorInfo> {

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private boolean enabled;

	@Column
	@Nonnull
	private String ruleType;

	@Column
	@Type(type = "FlightBasicRuleCriteriaType")
	private FlightBasicRuleCriteria inclusionCriteria;

	@Column
	@Type(type = "FlightBasicRuleCriteriaType")
	private FlightBasicRuleCriteria exclusionCriteria;

	@Column
	@Nonnull
	@JsonAdapter(JsonStringSerializer.class)
	private String output;

	@Column
	private double priority;

	@Column
	private Boolean exitOnMatch;

	@Column
	private boolean isDeleted;

	@Column
	private String description;

	@Override
	public AirConfiguratorInfo toDomain() {
		return new GsonMapper<>(this, AirConfiguratorInfo.class).convert();
	}

	@Override
	public DbAirConfiguratorRule from(AirConfiguratorInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbAirConfiguratorRule.class).convert();
	}

}
