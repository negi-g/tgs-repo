package com.tgs.services.fms.helper;

import java.util.*;
import java.util.stream.Collectors;
import com.tgs.filters.SourceRouteInfoFilter;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.utils.exception.CachingLayerException;
import java.util.concurrent.TimeUnit;
import net.logstash.logback.encoder.org.apache.commons.lang.BooleanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.aerospike.client.Bin;
import com.aerospike.client.Operation;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.NonOperatingSectorInfo;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.jparepository.SourceRouteInfoService;
import com.tgs.services.fms.mapper.NoResultSectorAirQueryMapper;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@InitializerGroup(group = InitializerGroup.Group.FLIGHT)
public class SourceRouteInfoHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap sourceRouteMap;

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	SourceRouteInfoService service;

	@Autowired
	private static AirAnalyticsHelper airanalyticsHelper;

	private static Map<String, List<SourceRouteInfo>> sourceRouteInfoInMemoryMap = new HashMap<>();

	private static CustomInMemoryHashMap sourceRouteConfig;

	@Autowired
	public SourceRouteInfoHelper(CustomInMemoryHashMap airportHashMap, GeneralCachingCommunicator cachingCommunicator,
			AirAnalyticsHelper airanalyticsHelper, CustomInMemoryHashMap sourceRouteConfig) {
		super(configurationHashMap);
		SourceRouteInfoHelper.cachingCommunicator = cachingCommunicator;
		SourceRouteInfoHelper.airanalyticsHelper = airanalyticsHelper;
		SourceRouteInfoHelper.sourceRouteConfig = sourceRouteConfig;
	}

	private static final String FIELD = "route_list";

	@Override
	public void process() {
		Runnable fetchSourceRouteInfoTask = () -> {
			processInMemory();
			log.info("Fetching source route data from database");
			StopWatch watch = StopWatch.createStarted();
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
				List<SourceRouteInfo> routeList = DbSourceRouteInfo.toDomainList(service.findAll(page));
				if (CollectionUtils.isEmpty(routeList))
					break;
				log.debug("Fetched source route info from database, info list size is {}", routeList.size());
				Map<String, List<SourceRouteInfo>> sectorWiseMapping = routeList.stream()
						.collect(Collectors.groupingBy(routeInfo -> getKey(routeInfo.getSrc(), routeInfo.getDest())));
				sectorWiseMapping.forEach((key, infoList) -> {
					sourceRouteConfig.put(key, FIELD, infoList,
							CacheMetaInfo.builder().set(CacheSetName.ROUTE_INFO.getName())
									.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
				});
			}
			watch.stop();
			log.info("Time Taken to update total {} Source RouteInfo : {} milliseconds",
					sourceRouteInfoInMemoryMap.keySet().size(), watch.getTime(TimeUnit.MILLISECONDS));
		};

		Thread fetchRouteInfoThread = new Thread(fetchSourceRouteInfoTask);
		fetchRouteInfoThread.start();
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	public void processInMemory() {
		Map<String, List<SourceRouteInfo>> tempInMemoryRules = new HashMap<>();
		log.info("[SourceRouteInfoHelper] Fetching source route data from database");
		StopWatch watch = StopWatch.createStarted();
		for (int i = 0; i < 500; i++) {
			Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
			List<SourceRouteInfo> routeList = DbSourceRouteInfo.toDomainList(service.findAll(page));
			if (CollectionUtils.isEmpty(routeList))
				break;
			log.debug("Fetched source route info from database, info list size is {}", routeList.size());
			Map<String, List<SourceRouteInfo>> sectorWiseMapping = routeList.stream()
					.collect(Collectors.groupingBy(routeInfo -> getKey(routeInfo.getSrc(), routeInfo.getDest())));
			sectorWiseMapping.forEach((key, infoList) -> {
				List<SourceRouteInfo> exisitingLists = tempInMemoryRules.getOrDefault(key, new ArrayList<>());
				exisitingLists.addAll(infoList);
				tempInMemoryRules.put(key, exisitingLists);
			});
		}
		sourceRouteInfoInMemoryMap = tempInMemoryRules;
		watch.stop();
		log.info("[SourceRouteInfoHelper] Time Taken to update total {} Source RouteInfo : {} milliseconds",
				sourceRouteInfoInMemoryMap.keySet().size(), watch.getTime(TimeUnit.MILLISECONDS));
	}

	public static String getKey(String src, String dest) {
		return String.join("-", src, dest);
	}

	@Override
	public void deleteExistingInitializer() {
		if (sourceRouteInfoInMemoryMap != null) {
			sourceRouteInfoInMemoryMap.clear();
		}

		sourceRouteConfig.truncate(CacheSetName.ROUTE_INFO.getName());
	}

	@SuppressWarnings("serial")
	public static boolean isValidRoute(AirSearchQuery searchQuery, NonOperatingSectorInfo sourceRouteInfo) {

		/**
		 * To check whether the sourceId has faced any NOT_OPERATING_SECTOR in any previous searches.
		 */
		/*
		 * if (!SourceRouteInfoHelper.isOperatingSector(sourceRouteInfo)) {
		 * log.info("Not Operating sector {} src {} dest {} ", sourceRouteInfo.getSourceId(),
		 * sourceRouteInfo.getSource(), sourceRouteInfo.getDestination()); return false; }
		 */

		List<String> keys =
				Arrays.asList(getKey(sourceRouteInfo.getSource(), sourceRouteInfo.getDestination()), getKey("*", "*"),
						getKey(sourceRouteInfo.getSource(), "*"), getKey("*", sourceRouteInfo.getDestination()));

		if (MapUtils.isNotEmpty(sourceRouteInfoInMemoryMap)) {
			for (String key : keys) {
				List<SourceRouteInfo> routesList = sourceRouteInfoInMemoryMap.get(key);
				if (CollectionUtils.isNotEmpty(routesList)) {
					for (SourceRouteInfo info : routesList) {
						if (info.getSourceId().equals(sourceRouteInfo.getSourceId())
								&& BooleanUtils.isTrue(info.isEnabled()))
							return true;
					}
				}
			}
		}
		log.debug("No valid Route found for search request searchid {} - {}", searchQuery.getSearchId(),
				sourceRouteInfo);
		return false;
	}

	/**
	 * @implSpec :Caching NonOperating sectors, So we can avoid same sector search for the particular sourceId for
	 *           particular days (be default specidfied to two days)
	 */
	public static void addNonOperatingSector(NonOperatingSectorInfo sectorInfo) {

		String key = getNonOperatingSectorKey(sectorInfo);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.NON_OPERATIONAL_SECTOR.getName())
				.namespace(CacheNameSpace.STATIC_MAP.getName()).expiration(CacheType.NON_OPERATIONAL_SECTORS.getTtl())
				.key(key).build();
		Map<String, String> binMap = new HashMap<>();

		/**
		 * @param isOperational used to determine operating status for the searching sectors.
		 */

		binMap.put(BinName.ROUTEINFO.getName(), GsonUtils.getGson().toJson(sectorInfo));
		cachingCommunicator.store(metaInfo, binMap, false, true, CacheType.NON_OPERATIONAL_SECTORS.getTtl());
	}

	/**
	 * @implSpec : generate the key from sourceRouteInfo (vice versa for onward, return)
	 */
	public static boolean isOperatingSector(NonOperatingSectorInfo sourceRouteInfo) {
		String key = getNonOperatingSectorKey(sourceRouteInfo);
		NonOperatingSectorInfo returnRouteInfo =
				new GsonMapper<>(sourceRouteInfo, NonOperatingSectorInfo.class).convert();
		returnRouteInfo.setSource(sourceRouteInfo.getDestination());
		returnRouteInfo.setDestination(sourceRouteInfo.getSource());
		String returnKey = getNonOperatingSectorKey(returnRouteInfo);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.STATIC_MAP.getName())
				.set(CacheSetName.NON_OPERATIONAL_SECTOR.getName())
				.keys(Arrays.asList(key, returnKey).toArray(new String[0])).build();
		Map<String, Map<String, String>> resultSet = cachingCommunicator.get(metaInfo, String.class);
		if (MapUtils.isNotEmpty(resultSet)
				&& ((resultSet.get(key) != null && resultSet.get(key).get(BinName.ROUTEINFO.getName()) != null)
						|| (resultSet.get(returnKey) != null
								&& resultSet.get(returnKey).get(BinName.ROUTEINFO.getName()) != null))) {
			return false;
		}
		return true;
	}

	public static String getNonOperatingSectorKey(NonOperatingSectorInfo sourceRouteInfo) {
		if (sourceRouteInfo != null && ObjectUtils.allNotNull(sourceRouteInfo.getSource(),
				sourceRouteInfo.getDestination(), sourceRouteInfo.getSourceId())) {
			StringJoiner joiner = new StringJoiner("_");
			joiner.add(sourceRouteInfo.getSource());
			joiner.add(sourceRouteInfo.getDestination());
			joiner.add(sourceRouteInfo.getSourceId().toString());
			return StringUtils.join(joiner.toString());
		}
		return null;
	}

	/**
	 * 1.This function will increment @param noResultCount for every search request. <br>
	 * 2.If the count is greater than @param noSearchResultTreshold it will be treated as NoResultSector. <br>
	 * 3.The next search will be triggered only if @param noResultCount is greater than @param noSearchResultTreshold
	 * and (@param noResultCount % @param noSearchResultBufferCount) is equal to 0.
	 * 
	 * @param searchQuery
	 * @param sourceId
	 * @return
	 */
	public static boolean isNoResultSector(AirSearchQuery searchQuery, Integer sourceId, User bookingUser) {
		String key = SourceRouteInfoHelper.getNoResultSectorKey(searchQuery.getRouteInfos(), sourceId);
		boolean isNoResultSector = false;
		AirGeneralPurposeOutput gnPurpose =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));

		// In case of deal inventory no result sector check is not required.
		if (sourceId == null || (gnPurpose.getNoResultSectorDisabledSource() != null
				&& gnPurpose.getNoResultSectorDisabledSource().contains(sourceId))) {
			return isNoResultSector;
		}

		Long noResultCount = SourceRouteInfoHelper.fetchNoResultSearchCount(searchQuery, key);

		Integer noSearchResultThreshold = gnPurpose.getNoSearchResultThreshold();
		Integer noSearchResultBufferCount = gnPurpose.getNoSearchResultBufferCount();
		log.debug("NoSearch {} currentcount {}  Threshold {} Buffer {}", searchQuery.getSearchId(), noResultCount,
				noSearchResultThreshold, noSearchResultBufferCount);
		if (noSearchResultThreshold != null && noResultCount > noSearchResultThreshold) {
			if (noResultCount % noSearchResultBufferCount != 0) {
				isNoResultSector = true;
			}
		}

		if (isNoResultSector) {
			addNoResultInfoToAnalytics(sourceId, searchQuery, noResultCount);
		}

		return isNoResultSector;
	}

	/**
	 * @implNote : This will do atomic increment operation on count bin and return the value.
	 */
	public static Long fetchNoResultSearchCount(AirSearchQuery searchQuery, String key) {
		Long noResultCount = Long.valueOf(0);
		try {
			CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(key).namespace(CacheNameSpace.STATIC_MAP.name())
					.plainData(true).set(CacheSetName.NO_RESULTS_SECTOR.getName()).build();
			noResultCount = cachingCommunicator.performOperationsAndFetchBinValue(metaInfo, BinName.COUNT.getName(),
					Long.class, -1, Operation.add(new Bin(BinName.COUNT.getName(), 1)),
					Operation.get(BinName.COUNT.getName()));
			return noResultCount == null ? 0 : noResultCount;
		} catch (CachingLayerException e) {
			log.error("Unable to fetch {} noresultsector {}", searchQuery.getSearchId(), key, e);
		}
		return noResultCount;
	}

	public static void deleteNoResultKey(List<RouteInfo> routes, Integer sourceId) {
		String key = SourceRouteInfoHelper.getNoResultSectorKey(routes, sourceId);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(key).namespace(CacheNameSpace.STATIC_MAP.name())
				.plainData(true).set(CacheSetName.NO_RESULTS_SECTOR.getName()).build();
		cachingCommunicator.delete(metaInfo);
	}

	private static String getNoResultSectorKey(List<RouteInfo> routes, Integer sourceId) {
		StringJoiner joiner = new StringJoiner("_");
		for (RouteInfo route : routes) {
			joiner.add(route.getFromCityAirportCode());
			joiner.add(route.getToCityAirportCode());
		}
		joiner.add(sourceId.toString());
		return joiner.toString();
	}

	private static void addNoResultInfoToAnalytics(Integer sourceId, AirSearchQuery searchQuery, Long noResultCount) {
		NoResultSectorAirQueryMapper queryMapper = NoResultSectorAirQueryMapper.builder().searchquery(searchQuery)
				.sourceId(sourceId).noResultHitCount(noResultCount).user(SystemContextHolder.getContextData().getUser())
				.contextData(SystemContextHolder.getContextData()).build();
		airanalyticsHelper.pushToAnalytics(queryMapper, AirAnalyticsType.NO_RESULT_SECTOR);
	}

	public void updateCacheAndInMemory(SourceRouteInfo request) {
		String key = getKey(request.getSrc(), request.getDest());
		SourceRouteInfoFilter filter =
				SourceRouteInfoFilter.builder().src(request.getSrc()).dest(request.getDest()).build();
		List<SourceRouteInfo> infoList = org.apache.commons.collections4.CollectionUtils
				.emptyIfNull(service.findAll(filter)).stream().collect(Collectors.toList());
		log.info("Updating SourceRouteInfo {} and size {}", key, CollectionUtils.size(infoList));
		if (CollectionUtils.isNotEmpty(infoList)) {
			sourceRouteInfoInMemoryMap.put(key, infoList);
			sourceRouteConfig.put(key, FIELD, infoList, CacheMetaInfo.builder().set(CacheSetName.ROUTE_INFO.getName())
					.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
		} else if (sourceRouteInfoInMemoryMap.containsKey(key)) {
			sourceRouteInfoInMemoryMap.remove(key);
			sourceRouteConfig.delete(key, CacheMetaInfo.builder().set(CacheSetName.ROUTE_INFO.getName()).build());
		}
	}
}
