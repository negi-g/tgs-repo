package com.tgs.services.fms.sources.sqiva;


import java.io.IOException;
import java.net.Proxy;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import com.tgs.services.base.LogData;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.supplier.SupplierCredential;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class SqivaBindingService {

	// bindingservice
	private static final String GET_SCHEDULE_V2 = "get_schedule_v2";
	private static final String GET_FARE_V2 = "get_fare_v2_new";
	private static final String GET_PREPAID_BAGGAGE = "get_prepaid_baggage";
	private static final String GET_ANCILLARY_FEE = "get_ancillary_fee_v2";

	private static final String GET_BOOK_V2 = "booking_v2";
	private static final String APP = "app";
	private static final String INFORMATION = "information";
	private static final String AIRLINE_CODE = "airline_code";
	private static final String TRANSACTION = "transaction";
	private static final String GET_ALL_BOOK_INFO_2 = "get_all_book_info_2";
	private static final String ADD_ANCILLARY_V2 = "add_ancillary_v2";
	private static final String UPDATE_PREPAID_BAGGAGE = "update_prepaid_baggage";
	private static final String PAYMENT = "payment";
	private static final String ACTION = "action";
	private static final String RQID = "rqid";
	private static final String GET_BOOK_PRICE_DETAIL_INFO_2 = "get_book_price_detail_info_2";

	private User user;

	private RestAPIListener listener;

	private SupplierCredential credential;

	private SqivaAirline airline;

	public Map<String, String> getHeader() {
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Content-Type", "application/json");
		headerParams.put("User-Agent", getUserAgent());
		return headerParams;
	}


	private String getUserAgent() {
		if (StringUtils.isNotBlank(credential.getUserName())) {
			return credential.getUserName();
		}
		return "WS-AWAN";
	}

	public JSONObject getSearchResult(Map<String, String> params, HttpUtils httpUtils) throws IOException {
		JSONObject responseObj = null;
		Proxy proxy = AirSupplierUtils.getProxy(user);
		String response = null;
		try {
			if (httpUtils == null) {
				httpUtils = HttpUtils.builder().build();
			}
			httpUtils.setUrlString(credential.getUrl());
			params = addCommonParams(params, GET_SCHEDULE_V2, INFORMATION);
			httpUtils.setQueryParams(params);
			httpUtils.setHeaders(getHeader());
			if (proxy != null) {
				httpUtils.setProxy(proxy);
			}
			response = httpUtils.getResponse(null).orElse(null).toString();
			if (StringUtils.isNotBlank(response)) {
				responseObj = new JSONObject(response);
			}
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getUrlString(), "SearchRQ"),
					formatRQRS(response, "SearchRS"));
			LogData logData = LogData.builder().logData(endPointRQRS).type("1-SearchAvailable").build();
			logData.setKey(listener.getKey());
			listener.addLog(logData);
		}
		return responseObj;
	}

	public Map<String, String> addCommonParams(Map<String, String> params, String action, String app) {
		if (params == null) {
			params = new HashMap<>();
		}
		params.put(APP, app);
		params.put(ACTION, action);
		params.put(AIRLINE_CODE, airline.getCode());
		params.put(RQID, credential.getPassword());
		return params;
	}


	public JSONObject getFareQuote(Map<String, String> params, HttpUtils httpUtils) throws IOException {
		JSONObject response = null;
		String strRes = null;
		Proxy proxy = AirSupplierUtils.getProxy(user);
		try {
			if (httpUtils == null) {
				httpUtils = HttpUtils.builder().build();
			}
			httpUtils.setUrlString(credential.getUrl());
			params = addCommonParams(params, GET_FARE_V2, INFORMATION);
			httpUtils.setQueryParams(params);
			httpUtils.setHeaders(getHeader());
			if (proxy != null) {
				httpUtils.setProxy(proxy);
			}
			strRes = httpUtils.getResponse(null).orElse(null).toString();
			if (StringUtils.isNotBlank(strRes)) {
				response = new JSONObject(strRes);
			}

		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getUrlString(), "FareQuoteRQ"),
					formatRQRS(strRes, "FareQuoteRS"));
			LogData logData = LogData.builder().logData(endPointRQRS).type("2-FareQuote").build();
			logData.setKey(listener.getKey());
			listener.addLog(logData);
		}
		return response;
	}


	public String formatRQRS(String message, String type) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(message)) {
			buffer.append(type + " : " + LocalDateTime.now().toString()).append("\n\n").append(message).append("\n\n");
		}
		return buffer.toString();
	}

	public JSONObject getPrePaidBaggage(Map<String, String> bagaggeParams, HttpUtils httpUtils) throws IOException {
		JSONObject response = null;
		String strRes = null;
		Proxy proxy = AirSupplierUtils.getProxy(user);
		try {
			if (httpUtils == null) {
				httpUtils = HttpUtils.builder().build();
			}
			httpUtils.setUrlString(credential.getUrl());
			bagaggeParams = addCommonParams(bagaggeParams, GET_PREPAID_BAGGAGE, INFORMATION);
			httpUtils.setQueryParams(bagaggeParams);
			httpUtils.setHeaders(getHeader());
			if (proxy != null) {
				httpUtils.setProxy(proxy);
			}
			strRes = httpUtils.getResponse(null).orElse(null).toString();
			if (StringUtils.isNotBlank(strRes)) {
				response = new JSONObject(strRes);
			}
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getUrlString(), "PrepaidBaggageRQ"),
					formatRQRS(strRes, "PrepaidBaggageRS"));
			LogData logData = LogData.builder().logData(endPointRQRS).type("3-PrepaidBaggage").build();
			logData.setKey(listener.getKey());
			listener.addLog(logData);
		}
		return response;
	}

	public JSONObject getAncillary(Map<String, String> params, HttpUtils httpUtils) throws IOException {
		JSONObject response = null;
		String strRes = null;
		Proxy proxy = AirSupplierUtils.getProxy(user);
		try {
			if (httpUtils == null) {
				httpUtils = HttpUtils.builder().build();
			}
			httpUtils.setUrlString(credential.getUrl());
			params = addCommonParams(params, GET_ANCILLARY_FEE, INFORMATION);
			httpUtils.setQueryParams(params);
			httpUtils.setHeaders(getHeader());
			if (proxy != null) {
				httpUtils.setProxy(proxy);
			}
			strRes = httpUtils.getResponse(null).orElse(null).toString();
			if (StringUtils.isNotBlank(strRes)) {
				response = new JSONObject(strRes);
			}
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getUrlString(), "AncillaryRQ"),
					formatRQRS(strRes, "AncillaryRS"));
			LogData logData = LogData.builder().logData(endPointRQRS).type("4-Ancillary").build();
			logData.setKey(listener.getKey());
			listener.addLog(logData);
		}
		return response;
	}


	public JSONObject getBookingResponse(Map<String, String> params, HttpUtils httpUtils) throws IOException {
		JSONObject response = null;
		String strRes = null;
		Proxy proxy = AirSupplierUtils.getProxy(user);
		try {
			if (httpUtils == null) {
				httpUtils = HttpUtils.builder().build();
			}
			httpUtils.setUrlString(credential.getUrl());
			params = addCommonParams(params, GET_BOOK_V2, TRANSACTION);
			httpUtils.setQueryParams(params);
			httpUtils.setHeaders(getHeader());
			if (proxy != null) {
				httpUtils.setProxy(proxy);
			}
			strRes = httpUtils.getResponse(null).orElse(null).toString();
			if (StringUtils.isNotBlank(strRes)) {
				response = new JSONObject(strRes);
			}

		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getUrlString(), "BOOKING_V2-RQ"),
					formatRQRS(strRes, "BOOKING_V2-RS"));
			LogData logData = LogData.builder().logData(endPointRQRS).type("BOOKING_V2").build();
			logData.setKey(listener.getKey());
			listener.addLog(logData);
		}
		return response;
	}

	public JSONObject getALLBookInfo_V2(Map<String, String> params, HttpUtils httpUtils) throws IOException {
		JSONObject response = null;
		String strRes = null;
		Proxy proxy = AirSupplierUtils.getProxy(user);
		try {
			if (httpUtils == null) {
				httpUtils = HttpUtils.builder().build();
			}
			httpUtils.setUrlString(credential.getUrl());
			params = addCommonParams(params, GET_ALL_BOOK_INFO_2, INFORMATION);
			httpUtils.setQueryParams(params);
			httpUtils.setHeaders(getHeader());
			if (proxy != null) {
				httpUtils.setProxy(proxy);
			}
			strRes = httpUtils.getResponse(null).orElse(null).toString();
			if (StringUtils.isNotBlank(strRes)) {
				response = new JSONObject(strRes);
			}

		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getUrlString(), "GET_ALL_BOOK_INFO_2-RQ"),
					formatRQRS(strRes, "GET_ALL_BOOK_INFO_2-RS"));
			LogData logData = LogData.builder().logData(endPointRQRS).type("GET_ALL_BOOK_INFO_2").build();
			logData.setKey(listener.getKey());
			listener.addLog(logData);
		}
		return response;
	}

	public JSONObject getBookPriceDetailInfo_V2(Map<String, String> params, HttpUtils httpUtils) throws IOException {
		JSONObject response = null;
		String strRes = null;
		Proxy proxy = AirSupplierUtils.getProxy(user);
		try {
			if (httpUtils == null) {
				httpUtils = HttpUtils.builder().build();
			}
			httpUtils.setUrlString(credential.getUrl());
			params = addCommonParams(params, GET_BOOK_PRICE_DETAIL_INFO_2, INFORMATION);
			httpUtils.setQueryParams(params);
			httpUtils.setHeaders(getHeader());
			if (proxy != null) {
				httpUtils.setProxy(proxy);
			}
			strRes = httpUtils.getResponse(null).orElse(null).toString();
			if (StringUtils.isNotBlank(strRes)) {
				response = new JSONObject(strRes);
			}

		} finally {
			String endPointRQRS =
					StringUtils.join(formatRQRS(httpUtils.getUrlString(), "GET_BOOK_PRICE_DETAIL_INFO_2-RQ"),
							formatRQRS(strRes, "GET_BOOK_PRICE_DETAIL_INFO_2-RS"));
			LogData logData = LogData.builder().logData(endPointRQRS).type("GET_BOOK_PRICE_DETAIL_INFO_2").build();
			logData.setKey(listener.getKey());
			listener.addLog(logData);
		}
		return response;
	}

	public JSONObject addAnillary_V2(Map<String, String> params, HttpUtils httpUtils) throws IOException {
		JSONObject response = null;
		String strRes = null;
		Proxy proxy = AirSupplierUtils.getProxy(user);
		try {
			if (httpUtils == null) {
				httpUtils = HttpUtils.builder().build();
			}
			params = addCommonParams(params, ADD_ANCILLARY_V2, TRANSACTION);
			httpUtils.setUrlString(credential.getUrl());
			httpUtils.setQueryParams(params);
			httpUtils.setHeaders(getHeader());
			if (proxy != null) {
				httpUtils.setProxy(proxy);
			}
			strRes = httpUtils.getResponse(null).orElse(null).toString();
			if (StringUtils.isNotBlank(strRes)) {
				response = new JSONObject(strRes);
			}

		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getUrlString(), "ADD_ANCILLARY_V2-RQ"),
					formatRQRS(strRes, "ADD_ANCILLARY_V2-RS"));
			LogData logData = LogData.builder().logData(endPointRQRS).type("ADD_ANCILLARY_V2").build();
			logData.setKey(listener.getKey());
			listener.addLog(logData);
		}
		return response;
	}

	public JSONObject updatePrepaidBaggage(Map<String, String> params, HttpUtils httpUtils) throws IOException {
		JSONObject response = null;
		String strRes = null;
		Proxy proxy = AirSupplierUtils.getProxy(user);
		try {
			if (httpUtils == null) {
				httpUtils = HttpUtils.builder().build();
			}
			httpUtils.setUrlString(credential.getUrl());
			params = addCommonParams(params, UPDATE_PREPAID_BAGGAGE, TRANSACTION);
			httpUtils.setQueryParams(params);
			httpUtils.setHeaders(getHeader());
			if (proxy != null) {
				httpUtils.setProxy(proxy);
			}
			strRes = httpUtils.getResponse(null).orElse(null).toString();
			if (StringUtils.isNotBlank(strRes)) {
				response = new JSONObject(strRes);
			}

		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getUrlString(), "UPDATE_PREPAID_BAGGAGE-RQ"),
					formatRQRS(strRes, "UPDATE_PREPAID_BAGGAGE-RS"));
			LogData logData = LogData.builder().logData(endPointRQRS).type("UPDATE_PREPAID_BAGGAGE").build();
			logData.setKey(listener.getKey());
			listener.addLog(logData);
		}
		return response;
	}

	public JSONObject payment(Map<String, String> params, HttpUtils httpUtils) throws IOException {
		JSONObject response = null;
		String strRes = null;
		Proxy proxy = AirSupplierUtils.getProxy(user);
		try {
			if (httpUtils == null) {
				httpUtils = HttpUtils.builder().build();
			}
			httpUtils.setUrlString(credential.getUrl());
			params = addCommonParams(params, PAYMENT, TRANSACTION);
			httpUtils.setQueryParams(params);
			httpUtils.setHeaders(getHeader());
			if (proxy != null) {
				httpUtils.setProxy(proxy);
			}
			strRes = httpUtils.getResponse(null).orElse(null).toString();
			if (StringUtils.isNotBlank(strRes)) {
				response = new JSONObject(strRes);
			}

		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getUrlString(), "PAYMENT-RQ"),
					formatRQRS(strRes, "PAYMENT-RS"));
			LogData logData = LogData.builder().logData(endPointRQRS).type("PAYMENT").build();
			logData.setKey(listener.getKey());
			listener.addLog(logData);
		}
		return response;
	}

}

