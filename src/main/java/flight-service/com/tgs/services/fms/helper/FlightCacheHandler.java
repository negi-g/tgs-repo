package com.tgs.services.fms.helper;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.CacheFilterOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirCacheFilter;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirCacheSearchResult;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.LowFareInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import java.time.temporal.ChronoUnit;
import java.time.LocalDate;

@Slf4j
@Service
public class FlightCacheHandler {

	public static final String DELIM = "_";

	@Autowired
	FMSCachingServiceCommunicator cachingService;

	public static Integer getFlightListingCachingExpiration(User user, AirSearchQuery searchQuery) {
		return CacheType.FLIGHTSEARCHCACHING.getTtl(user, searchQuery);
	}


	public void cacheAirSearchResults(AirSearchResult searchResult, AirSearchQuery searchQuery,
			SupplierConfiguration supplierConf, User bookingUser) {
		if (searchResult != null && !AirUtils.isCacheDisabled(searchQuery, supplierConf.getBasicInfo(), bookingUser)) {
			log.debug("Storing searchResult in cache for searchQuery {}", searchQuery);
			searchResult.getTripInfos().forEach((type, trips) -> {
				log.debug("Total no of trips for type {} is {} for searchQuery {}", type, trips.size(), searchQuery);
				if (CollectionUtils.isNotEmpty(trips)) {
					try {
						int expiration = getFlightListingCachingExpiration(bookingUser, searchQuery);
						SupplierRule supplierRule = SupplierConfigurationHelper
								.getSupplierRule(supplierConf.getSourceId(), supplierConf.getBasicInfo().getRuleId());
						String key = generateKey(type, searchQuery, supplierRule);
						CacheMetaInfo metaInfo = CacheMetaInfo.builder().compress(true).plainData(false)
								.expiration(expiration).namespace(CacheNameSpace.FLIGHT_CACHE.getName())
								.set(CacheSetName.FLIGHT_SEARCH.getName()).kyroCompress(true).build();
						log.debug("Storing SearchResult for key {} and ttl {}", key, expiration);
						cachingService.cacheSearchResult(trips, key, metaInfo);
					} catch (Exception e) {
						log.error("Unable to store flight listing into cache for searchQuery {} ", searchQuery, e);
					}
					if (searchQuery.getIsDomestic() || (searchQuery.isIntl() && searchQuery.isOneWay())) {
						storeLowFare(trips, searchQuery, type);
					}
				}
			});
		}
	}

	public String generateKey(String type, AirSearchQuery searchQuery, SupplierRule supplierRule) {
		String sectorKey = "";

		for (RouteInfo routInfo : searchQuery.getRouteInfos()) {
			sectorKey = StringUtils.join(sectorKey, routInfo.getFromCityOrAirport().getCode(),
					routInfo.getToCityOrAirport().getCode(), routInfo.getTravelDate().toString());
		}

		String paxKey = StringUtils.join(searchQuery.getPaxInfo().get(PaxType.ADULT).intValue(),
				searchQuery.getPaxInfo().getOrDefault(PaxType.CHILD, 0).intValue(),
				searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0).intValue(),
				searchQuery.getCabinClass().getCode());

		String miscKey = StringUtils.join(searchQuery.getPrefferedAirline(), supplierRule.getId(),
				supplierRule.getProcessedOn().toString());
		return type + sectorKey + paxKey + miscKey;
	}

	public AirSearchResult getCacheResult(AirSearchQuery searchQuery, SupplierConfiguration supplierConf, User user) {
		AirSearchResult searchResult = null;
		try {
			SupplierRule supplierRule = SupplierConfigurationHelper.getSupplierRule(supplierConf.getSourceId(),
					supplierConf.getBasicInfo().getRuleId());
			for (TripInfoType type : searchQuery.getTripInfoTypes()) {
				String key = generateKey(type.name(), searchQuery, supplierRule);
				CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.FLIGHT_CACHE.getName())
						.set(CacheSetName.FLIGHT_SEARCH.getName()).kyroCompress(true).build();
				AirCacheSearchResult cacheResult = cachingService.fetchAirSearchResult(key, metaInfo);
				if (Objects.isNull(cacheResult)) {
					log.debug("Cache is empty for sq {}", searchQuery.getSearchId());
					return null;
				}

				if (isCacheValid(cacheResult, user, searchQuery, supplierConf.getBasicInfo())) {
					List<TripInfo> trips = cacheResult.getTripInfos();
					log.debug("{} Trips found {} from cache for Supplier {}, supplier rule {}", type, trips.size(),
							supplierConf.getSupplierId(), supplierConf.getBasicInfo().getRuleId());
					if (searchResult == null) {
						searchResult = new AirSearchResult();
					}
					searchResult.getTripInfos().put(type.name(), trips);
				} else {
					// if any triptype is invalid , other type also should be ignored
					return null;
				}
			}
			if (searchQuery.isDomesticReturn() && searchResult != null) {
				if (Collections.isEmpty(searchResult.getTripInfos().get(TripInfoType.ONWARD.getName()))
						|| Collections.isEmpty(searchResult.getTripInfos().get(TripInfoType.RETURN.getName()))) {
					return null;
				}
			}
			return searchResult;
		} catch (Exception e) {
			log.error("Unable to store flight listing into cache for searchQuery {} ", searchQuery, e);
		} finally {
			if (searchResult == null) {
				log.debug("Unable to find searchresult in cache for searchQuery{}", searchQuery);
			}
		}
		return searchResult;
	}

	// This functionality will remove special fare trip from the entire search result.
	@Deprecated
	private List<TripInfo> processSpecialFare(List<TripInfo> trips) {
		List<TripInfo> newTrips = new ArrayList<>();
		for (TripInfo tripInfo : trips) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				List<PriceInfo> priceInfoList = segmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
					return priceInfo.getFareIdentifier() == null
							|| !priceInfo.getFareIdentifier().equals(FareType.SPECIAL_RETURN);
				}).collect(Collectors.toList());
				segmentInfo.setPriceInfoList(priceInfoList);
			}
			if (CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos().get(0).getPriceInfoList())) {
				newTrips.add(tripInfo);
			}
		}
		return newTrips;
	}

	public boolean deleteCache(AirSearchQuery searchQuery, SupplierBasicInfo supplierInfo, String bookingId) {
		String key = null;
		try {
			SupplierRule supplierRule =
					SupplierConfigurationHelper.getSupplierRule(supplierInfo.getSourceId(), supplierInfo.getRuleId());
			for (TripInfoType type : TripInfoType.values()) {
				key = generateKey(type.getName(), searchQuery, supplierRule);
				cachingService.deleteCacheRecord(CacheMetaInfo.builder().compress(true).plainData(false)
						.namespace(CacheNameSpace.FLIGHT_CACHE.getName()).set(CacheSetName.FLIGHT_SEARCH.getName())
						.kyroCompress(true).key(key).build());
				if (StringUtils.isNotEmpty(key)) {
					log.info("Deleted flight cache for key - {} bookingId {} searchquery {} ", key, bookingId,
							searchQuery);
				}
			}
		} catch (Exception e) {
			log.error("Unable to delete cache for key {}, bookingId {} searchQuery {}", key, bookingId, searchQuery, e);
		}
		return true;
	}

	public static Integer getSearchPriceIdExpiration(AirSearchQuery searchQuery) {
		return CacheType.PRICEIDKEYATSEARCH.getTtl(SystemContextHolder.getContextData().getUser(), searchQuery);
	}

	public void storeTripKey(AirSearchResult searchResult, AirSearchQuery searchQuery) {
		if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			Integer expiration = getSearchPriceIdExpiration(searchQuery);
			searchResult.getTripInfos().forEach((type, trips) -> {
				if (CollectionUtils.isNotEmpty(trips)) {
					try {
						String searchKey = searchQuery.getSearchId();
						cachingService.store(searchKey, BinName.SEARCHQUERYBIN.getName(), searchQuery,
								CacheMetaInfo.builder().set(CacheSetName.SEARCHQUERY.name()).compress(true)
										.plainData(false).expiration(expiration).build());
						// kryo compressed priceId vs trip
						processAndStore(trips, searchKey, type, searchQuery, expiration, false);
					} catch (Exception e) {
						log.error("Unable to store tripInfo List for searchQuery {} ", searchQuery, e);
					}
				}
			});
		}

		LogUtils.log(LogTypes.AIRSEARCH_STORETRIPKEY,
				SystemContextHolder.getContextData().getLogMetaInfo().setSearchId(searchQuery.getSearchId()),
				LogTypes.AIRSEARCH_PROCESS);
	}

	public void storeLowFare(List<TripInfo> tripInfoList, AirSearchQuery searchQuery, String tripType) {
		try {
			int routeInfos = 0;
			if (tripType.equals(TripInfoType.RETURN.getTripType())) {
				routeInfos = 1;
			}

			String binName = searchQuery.getRouteInfos().get(routeInfos).getTravelDate().toString();
			String fromAirportCode = searchQuery.getRouteInfos().get(routeInfos).getFromCityOrAirport().getCode();
			String toAirportCode = searchQuery.getRouteInfos().get(routeInfos).getToCityOrAirport().getCode();
			String key = fromAirportCode + "-" + toAirportCode;
			LowFareInfo existingLowFare =
					cachingService.fetchValue(key, LowFareInfo.class, CacheSetName.LOW_FARE.getName(), binName);
			LowFareInfo newLowFare = null;
			for (TripInfo trip : tripInfoList) {
				LowFareInfo tempLowFare = getLowestFareFromSegments(trip.getSegmentInfos());
				if (newLowFare == null || newLowFare.getFare() > tempLowFare.getFare()) {
					newLowFare = tempLowFare;
				}
			}
			if (Objects.isNull(existingLowFare) || newLowFare.getCode().equals(existingLowFare.getCode())
					|| newLowFare.getFare() < existingLowFare.getFare()) {
				cachingService.store(key, binName, newLowFare, CacheMetaInfo.builder().set(CacheSetName.LOW_FARE.name())
						.compress(true).plainData(false).expiration(CacheType.LOWAIRFARE.getTtl()).build());
			}
		} catch (Exception e) {
			log.error("Unable to store lowFare for searchQuery {}", searchQuery, e);
		}
	}

	private LowFareInfo getLowestFareFromSegments(List<SegmentInfo> segmentInfoList) {
		LowFareInfo lowFareInfo = new LowFareInfo();
		Double lowestFare = new Double(0);
		int priceInfos = segmentInfoList.get(0).getPriceInfoList().size();
		for (int priceIndex = 0; priceIndex < priceInfos; priceIndex++) {
			Double segmentPrice = new Double(0);
			for (SegmentInfo segment : segmentInfoList) {
				PriceInfo priceInfo = segment.getPriceInfo(priceIndex);
				if (!FareType.SPECIAL_RETURN.getName().equals(priceInfo.getFareIdentifier())
						&& MapUtils.isNotEmpty(priceInfo.getFareDetail(PaxType.ADULT).getFareComponents())) {
					FareDetail fareDetail = priceInfo.getFareDetail(PaxType.ADULT);
					segmentPrice += fareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0.0);
				}
			}
			if (lowestFare.doubleValue() == 0d || lowestFare > segmentPrice) {
				lowestFare = segmentPrice;
			}
		}
		lowFareInfo.setFare(lowestFare);
		lowFareInfo.setCode(segmentInfoList.get(0).getFlightDesignator().getAirlineInfo().getCode());
		return lowFareInfo;
	}

	public String getSegmentKey(List<SegmentInfo> segmentInfos) {
		String key = "";
		for (SegmentInfo segmentInfo : segmentInfos) {
			key = StringUtils.join(key, segmentInfo.getDepartAirportInfo().getCode().trim(),
					segmentInfo.getArrivalAirportInfo().getCode().trim(),
					segmentInfo.getFlightDesignator().getAirlineInfo().getCode().trim(),
					segmentInfo.getFlightDesignator().getFlightNumber().trim());
		}
		return key;
	}

	public String generateUniqueId(TripInfo tripInfo, String searchId, int tripIndex) {
		return searchId + DELIM + tripIndex + getSegmentKey(tripInfo.getSegmentInfos());
	}

	public void storeSearchQueryForCombiSearch(AirSearchQuery searchQuery, TripInfo tripInfo) {
		cachingService.store(getSegmentKey(tripInfo.getSegmentInfos()), BinName.SEARCHQUERYBIN.getName(), searchQuery,
				CacheMetaInfo.builder().set(CacheSetName.SEARCHQUERY.name()).compress(true).plainData(false)
						.expiration(getSearchPriceIdExpiration(searchQuery)).build());
	}

	public AirSearchQuery fetchSearchQueryForCombiSearch(TripInfo tripInfo) {
		return cachingService.fetchValue(getSegmentKey(tripInfo.getSegmentInfos()), AirSearchQuery.class,
				CacheSetName.SEARCHQUERY.getName(), BinName.SEARCHQUERYBIN.getName());
	}

	public void processAndStore(List<TripInfo> trips, String searchId, String tripType, AirSearchQuery searchQuery) {
		processAndStore(trips, searchId, tripType, searchQuery, null, true);
	}

	public void processAndStore(List<TripInfo> trips, String searchId, String tripType, AirSearchQuery searchQuery,
			Integer ttl, boolean isStorePrice) {
		int tripIndex = 0;
		Integer expiration = ttl != null ? ttl : getSearchPriceIdExpiration(searchQuery);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.PRICE_INFO.name()).kyroCompress(true)
				.plainData(false).expiration(expiration).build();
		log.debug("Flight processAndStore expiration {} for search {} ", expiration, searchQuery.getSearchId());
		for (TripInfo tripInfo : trips) {
			try {
				// it should be unique even if same flight numbers/airline in trips available
				// this uniqueTripId is used in frontend to remove duplicates for Dealinventory and normal fare after
				// merging at frontend
				String uniqueTripId = generateUniqueId(tripInfo, searchId, tripIndex++);
				tripInfo.setId(uniqueTripId);
				for (int i = 0; i < tripInfo.getSegmentInfos().size(); i++) {
					ListIterator<PriceInfo> iter = tripInfo.getSegmentInfos().get(i).getPriceInfoList().listIterator();
					int j = 0;
					while (iter.hasNext()) {
						iter.next();
						String id = uniqueTripId + DELIM + System.nanoTime();
						if (j == tripInfo.getSegmentInfos().get(0).getPriceInfoList().size()) {
							iter.remove();
							continue;
						}

						if (i >= 1) {
							id = tripInfo.getSegmentInfos().get(0).getPriceInfoList().get(j).getId();
						}
						tripInfo.getSegmentInfos().get(i).getPriceInfoList().get(j).setId(id);
						j++;
					}
				}

				if (isStorePrice) {
					tripInfo.getSegmentInfos().get(0).getPriceInfoList().forEach(priceInfo -> {
						cachingService.store(priceInfo.getId(), BinName.PRICEINFOBIN.getName(), tripInfo, metaInfo);
					});
				}
			} catch (Exception e) {
				log.error("Unable to persist tripInfo {}", tripInfo, e);
			}
		}
	}

	public void storeTripPrice(AirSearchResult searchResult, AirSearchQuery searchQuery) {
		ExecutorUtils.getFlightSearchThreadPool().submit(() -> {
			log.debug("Storing trip price for search {}", searchQuery.getSearchId());
			Integer expiration = getSearchPriceIdExpiration(searchQuery);
			CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.PRICE_INFO.name()).kyroCompress(true)
					.plainData(false).expiration(expiration).build();
			AtomicInteger priceCounter = new AtomicInteger(0);
			for (Entry<String, List<TripInfo>> entrySet : searchResult.getTripInfos().entrySet()) {
				for (TripInfo tripInfo : entrySet.getValue()) {
					tripInfo.getSegmentInfos().get(0).getPriceInfoList().forEach(priceInfo -> {
						cachingService.store(priceInfo.getId(), BinName.PRICEINFOBIN.getName(), tripInfo, metaInfo);
						priceCounter.getAndIncrement();
					});
				}
			}
			log.debug("Stored price options for trip for {} trip count {} price count {} ttl is {}",
					searchQuery.getSearchId(), searchResult.getTotalTrips(), priceCounter.get(), expiration);
		});
	}

	public static String getSearchQueryKey(String key) {
		return key.substring(0, key.indexOf(DELIM));
	}

	public boolean isCacheValid(AirCacheSearchResult cacheResult, User user, AirSearchQuery searchQuery,
			SupplierBasicInfo supplierInfo) {
		long expiration = getSearchQueryValidTTL(user, searchQuery, supplierInfo);
		if (expiration == 0) {
			log.debug("Expiration 0 for sq {} and cache created {}", searchQuery.getSearchId(),
					cacheResult.getCreationTime());
			return false;
		}
		long diffMinutes = Duration.between(cacheResult.getCreationTime(), java.time.LocalDateTime.now()).toMinutes();
		log.debug("Cache {} expiration {} current time diff {}", searchQuery.getSearchId(), expiration, diffMinutes);
		return diffMinutes <= expiration;
	}

	// return in minutes
	public long getSearchQueryValidTTL(User user, AirSearchQuery searchQuery, SupplierBasicInfo supplierInfo) {
		FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(searchQuery).generateFact(supplierInfo);
		BaseUtils.createFactOnUser(flightFact, user);
		List<CacheFilterOutput> outputs =
				AirConfiguratorHelper.getAllAirConfigRuleOutputs(flightFact, AirConfiguratorRuleType.CACHE_FILTER);
		int daysBetweenTravel =
				(int) ChronoUnit.DAYS.between(LocalDate.now(), searchQuery.getRouteInfos().get(0).getTravelDate());

		if (CollectionUtils.isNotEmpty(outputs)) {
			for (CacheFilterOutput output : outputs) {
				for (AirCacheFilter params : output.getTtlConfig()) {
					Range<Integer> range = Range.between(params.getStartPeriod(), params.getEndPeriod());
					if (range.contains(daysBetweenTravel)) {
						return params.getDiffTime();
					}
				}
			}
		}
		log.debug("Default cache not applicable for {} CacheType.getTtl is 0", searchQuery.getSearchId());
		return 0;
	}
}
