package com.tgs.services.fms.jparepository;

import com.tgs.services.fms.dbmodel.DbFareRuleInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface FareRuleRepository
		extends JpaRepository<DbFareRuleInfo, Long>, JpaSpecificationExecutor<DbFareRuleInfo> {

	// public List<DbFareRuleInfo> findByEnabled(Boolean enabled);
}
