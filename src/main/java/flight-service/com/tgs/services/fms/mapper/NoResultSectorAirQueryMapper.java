package com.tgs.services.fms.mapper;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.analytics.NoResultSectorAirQuery;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NoResultSectorAirQueryMapper extends Mapper<NoResultSectorAirQuery> {

	private Boolean isNoResultSector;
	private Long noResultHitCount;
	private Integer sourceId;
	private User user;
	private AirSearchQuery searchquery;
	private ContextData contextData;

	@Override
	protected void execute() throws CustomGeneralException {

		output = output != null ? output : NoResultSectorAirQuery.builder().build();
		AnalyticsAirQueryMapper.builder().searchQuery(searchquery).user(user).contextData(contextData).build()
				.setOutput(output).convert();
		output.setNoresulthitcount(noResultHitCount);
		output.setSourcename(AirSourceType.getAirSourceType(sourceId).name());

	}


}
