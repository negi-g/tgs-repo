package com.tgs.services.fms.sources.otaradixx;

import java.util.Arrays;
import com.tgs.services.base.enums.PaxType;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RadixxPaxType {
	ADULT(PaxType.ADULT, 1), CHILD(PaxType.CHILD, 6), INFANT(PaxType.INFANT, 5);

	private PaxType paxType;
	private int paxId;

	public static PaxType getPaxType(int paxId) {
		return Arrays.stream(RadixxPaxType.values()).filter(rdPaxType -> {
			return rdPaxType.getPaxId() == paxId;
		}).findFirst().get().getPaxType();
	}

	public static int getPaxId(PaxType paxType) {
		return Arrays.stream(RadixxPaxType.values()).filter(rdPaxType -> {
			return rdPaxType.getPaxType() == paxType;
		}).findFirst().get().getPaxId();
	}
}
