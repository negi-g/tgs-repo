package com.tgs.services.fms.sources.travelport.session;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
@Slf4j
public class TravelPortAirInfoFactory extends AbstractAirInfoFactory {

	protected TravelPortBindingService bindingService;

	protected SoapRequestResponseListner listener;

	public TravelPortAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public TravelPortAirInfoFactory(SupplierConfiguration supplierConf) {
		super(null, supplierConf);
	}

	public void initialize() {
		bindingService = TravelPortBindingService.builder().configuration(supplierConf).build();
		bindingService.setSupplierAnalyticsInfos(supplierAnalyticsInfos);
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize();
		Map<AirSearchQuery, Future<AirSearchResult>> futureTaskMap = new LinkedHashMap<>();

		if (searchQuery.isDomesticReturn()
				&& BooleanUtils.isTrue(supplierConf.getSupplierCredential().getIsSplitTicketDisabled())) {
			List<AirSearchQuery> splitQueries = AirUtils.splitSearchQueriesForDomesticReturn(searchQuery);

			// Onward one way search
			createSearchTask(splitQueries.get(0), futureTaskMap);

			// Return one way search
			createSearchTask(splitQueries.get(1), futureTaskMap);

			// complete search
			createSearchTask(searchQuery, futureTaskMap);
		} else {
			TravelPortSearchManager searchManager = buildSearchManager(searchQuery);
			searchResult = searchManager.doSearch();
		}

		for (AirSearchQuery sQuery : futureTaskMap.keySet()) {
			try {
				AirSearchResult newSResult = futureTaskMap.get(sQuery).get(30, TimeUnit.SECONDS);
				if (searchResult == null || MapUtils.isEmpty(searchResult.getTripInfos())) {
					searchResult = newSResult;
				} else {
					searchResult = AirUtils.mergeSearchResult(searchResult, newSResult, sQuery);
				}
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				if (CollectionUtils.isNotEmpty(criticalMessageLogger)) {
					criticalMessageLogger.add(e.getMessage());
				} else {
					log.info("Error Occured On supplier Search {} sq {} and cause {}",
							supplierConf.getBasicInfo().getDescription(), searchQuery.getSearchId(), e.getMessage());
				}
			}
		}
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		initialize();
		boolean isReviewSuccess = false;
		String sessionId = null;
		SupplierSession session = null;
		boolean isNewToken = false;
		TripInfo reviewedTrip = null;
		TravelPortSessionManager sessionManager = null;
		String traceId = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		String providerCode = TravelPortUtils.getProviderCode(selectedTrip, supplierConf);
		try {
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			TravelPortReviewManager reviewManager = TravelPortReviewManager.builder().searchQuery(searchQuery)
					.sourceConfiguration(sourceConfiguration).configuration(supplierConf).bindingService(bindingService)
					.traceId(traceId).moneyExchnageComm(moneyExchnageComm).bookingId(bookingId)
					.providerCode(providerCode).listener(listener).bookingUser(user).build();
			reviewManager.init();
			reviewedTrip = reviewManager.reviewFlight(selectedTrip, false);
			if (reviewedTrip != null) {
				SupplierSessionInfo sessionInfo =
						SupplierSessionInfo.builder().tripKey(selectedTrip.getSegmentsBookingKey()).build();
				session = getSessionIfExists(bookingId, sessionInfo);
				sessionManager =
						TravelPortSessionManager.builder().bindingService(bindingService).searchQuery(searchQuery)
								.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
								.configuration(supplierConf).providerCode(providerCode).traceId(traceId)
								.bookingId(bookingId).listener(listener).bookingUser(user).build();
				sessionManager.init();
				if (Objects.isNull(session)) {
					isNewToken = true;
					sessionId = sessionManager.createSession();
				} else {
					sessionId = session.getSupplierSessionInfo().getSessionToken();
				}
				reviewManager.setSessionkey(sessionId);
				if (reviewManager.sellSegment(reviewedTrip, sessionId)) {
					isReviewSuccess = true;
					reviewManager.setSSRInfos(reviewedTrip);
				}
			}
		} finally {
			if (isNewToken && reviewedTrip != null && isReviewSuccess) {
				SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().sessionToken(sessionId).build();
				storeBookingSession(bookingId, sessionInfo, session, selectedTrip);
			} else {
				if (sessionManager != null) {
					sessionManager.setSessionkey(sessionId);
					sessionManager.endSession();
				}
			}
		}
		return reviewedTrip;
	}


	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		initialize();
		TripFareRule tripFareRule = null;
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		TravelPortFareRuleManager ruleManager =
				TravelPortFareRuleManager.builder().configuration(supplierConf).bookingId(bookingId).listener(listener)
						.bindingService(bindingService).bookingUser(user).moneyExchnageComm(moneyExchnageComm).build();
		ruleManager.init();
		if (isDetailed) {
			tripFareRule = ruleManager.getFareRule(selectedTrip, bookingId);
		} else {
			if (BooleanUtils.isTrue(sourceConfiguration.getIsFareRuleFromITQ())) {
				SupplierConfiguration supplierConfiguration =
						SupplierConfigurationHelper.getSupplierConfiguration("Travelport-FareRule");
				TravelPortMiniRuleManager miniRuleManager =
						TravelPortMiniRuleManager.builder().configuration(supplierConfiguration).bookingId(bookingId)
								.bookingId(bookingId).bookingUser(user).moneyExchnageComm(moneyExchnageComm).build();
				miniRuleManager.init();
				tripFareRule = miniRuleManager.getFareRule(selectedTrip, bookingId);
			} else {
				boolean isFareDisplayFlow = false;
				if (!ruleManager.isFareRuleKeyAvailable(selectedTrip)) {
					ruleManager.airFareDisplay(selectedTrip);
					isFareDisplayFlow = true;
				}
				tripFareRule = ruleManager.getStructuredFareRule(selectedTrip, isFareDisplayFlow);
			}
		}
		return tripFareRule;
	}

	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		initialize();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		TravelPortSSRManager ssrManager = null;
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		String providerCode = TravelPortUtils.getProviderCode(tripInfo, supplierConf);
		String traceId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		ssrManager = TravelPortSSRManager.builder().bindingService(bindingService).searchQuery(searchQuery)
				.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
				.configuration(supplierConf).listener(listener).providerCode(providerCode).traceId(traceId)
				.bookingId(bookingId).segmentInfos(tripInfo.getSegmentInfos()).bookingUser(user).build();
		ssrManager.init();
		tripSeatMap = ssrManager.getSeatMap(tripInfo);
		return tripSeatMap;
	}

	@Override
	public TripInfo getChangeClass(TripInfo tripInfo, String searchId) {
		initialize();
		listener = new SoapRequestResponseListner(searchId, null, supplierConf.getBasicInfo().getSupplierName());
		String traceId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		TravelPortAlternateClassSearchManager alternateClassSearchManager = null;
		alternateClassSearchManager = TravelPortAlternateClassSearchManager.builder().searchQuery(searchQuery)
				.moneyExchnageComm(moneyExchnageComm).sourceConfiguration(sourceConfiguration)
				.configuration(supplierConf).listener(listener).bindingService(bindingService).traceId(traceId)
				.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
		alternateClassSearchManager.init();
		alternateClassSearchManager.searchAlternateClass(tripInfo, searchId);
		return tripInfo;
	}

	@Override
	public TripInfo getChangeClassFare(TripInfo tripInfo, String searchId) {
		initialize();
		listener = new SoapRequestResponseListner(searchId, null, supplierConf.getBasicInfo().getSupplierName());
		String traceId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		TravelPortReviewManager reviewManager = TravelPortReviewManager.builder().searchQuery(searchQuery)
				.searchId(searchId).sourceConfiguration(sourceConfiguration).configuration(supplierConf)
				.bindingService(bindingService).listener(listener).moneyExchnageComm(moneyExchnageComm).traceId(traceId)
				.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
		reviewManager.init();
		tripInfo = reviewManager.reviewFlight(tripInfo, true);
		return tripInfo;
	}

	private TravelPortSearchManager buildSearchManager(AirSearchQuery searchQuery) {
		boolean isSplitTicketingSearch = searchQuery.isDomesticReturn()
				&& BooleanUtils.isNotTrue(supplierConf.getSupplierCredential().getIsSplitTicketDisabled());
		TravelPortSearchManager searchManager = TravelPortSearchManager.builder().searchQuery(searchQuery)
				.moneyExchnageComm(moneyExchnageComm).sourceConfiguration(sourceConfiguration)
				.configuration(supplierConf).bindingService(bindingService).criticalMessageLogger(criticalMessageLogger)
				.bookingUser(user).isSplitTicketingSearch(isSplitTicketingSearch).build();
		SoapRequestResponseListner listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
				supplierConf.getBasicInfo().getSupplierName());
		listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
				supplierConf.getBasicInfo().getSupplierName());
		searchManager.setListener(listener);
		searchManager.setTraceId(TravelPortUtils.keyGenerator());
		searchManager.init();
		searchManager.setListener(listener);
		return searchManager;
	}

	private void createSearchTask(AirSearchQuery searchQuery,
			Map<AirSearchQuery, Future<AirSearchResult>> futureTaskMap) {
		TravelPortSearchManager searchManager = buildSearchManager(searchQuery);
		Future<AirSearchResult> futureTask =
				ExecutorUtils.getFlightSearchThreadPool().submit(() -> searchManager.doSearch());
		futureTaskMap.put(searchQuery, futureTask);
	}

}
