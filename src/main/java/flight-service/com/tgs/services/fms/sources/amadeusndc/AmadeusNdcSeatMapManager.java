package com.tgs.services.fms.sources.amadeusndc;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import javax.xml.ws.BindingProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.CarrierType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.CoreRequestType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.CountryType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.IATAPayloadStandardAttributesType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.OfferType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.PartyType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.PartyType.Sender;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.PointofSaleType2;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.PricingParameterType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.RecipientType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.RequestType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.ResponseParametersType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.SeatAvailabilityRQ;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.TravelAgencyType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrq.TravelAgencyTypeCodeContentType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrs.ALaCarteOfferItemType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrs.CabinCompartmentType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrs.ErrorType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrs.PaxSegmentType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrs.PaxType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrs.ResponseType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrs.SeatAvailabilityRS;
import org.iata.iata._2015._00._2018_1.seatavailabilityrs.SeatMapType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrs.SeatRowType;
import org.iata.iata._2015._00._2018_1.seatavailabilityrs.SeatType;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatPosition;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@SuperBuilder
public class AmadeusNdcSeatMapManager extends AmadeusNdcServiceManager {

	protected TripInfo selectedTrip;

	private TripSeatMap tripSeatMap;

	public void seatAvailability() {
		AlteaNDC181PT servicePort = service.getAlteaNDC181Port();
		BindingProvider bp = (BindingProvider) servicePort;
		addJAXHandlers(bp, bookingId, AmadeusNdcHeaderHandler._SEAT_AVAILABILITY_);
		SeatAvailabilityRS seatAvailabilityResponse =
				servicePort.ndcSeatAvailability(createSeatAvailability(), null, null, getSecurityHostedUser());
		if (!isAnyError(seatAvailabilityResponse)) {
			parseSeatMap(seatAvailabilityResponse);
		}
	}

	private void parseSeatMap(SeatAvailabilityRS seatAvailabilityResponse) {
		if (isValidResponse(seatAvailabilityResponse)) {

			Map<String, SeatInformation> tripSeat = new HashMap<>();

			String adultPaxId = "";
			for (PaxType pax : seatAvailabilityResponse.getResponse().getDataLists().getPaxList().getPax()) {
				if (AmadeusNdcPaxType.ADT.name().equals(pax.getPTC())) {
					adultPaxId = pax.getPaxID();
					break;
				}
			}

			for (SegmentInfo segment : selectedTrip.getSegmentInfos()) {

				List<SeatSSRInformation> seatsInfo = new ArrayList<>();

				String paxSegmentId = getPaxSegmentId(segment,
						seatAvailabilityResponse.getResponse().getDataLists().getPaxSegmentList().getPaxSegment());
				List<ALaCarteOfferItemType> matchedAlaCarteOfferItems =
						matchOffersBySegmentAndPaxID(paxSegmentId, adultPaxId, seatAvailabilityResponse.getResponse());

				if (CollectionUtils.isNotEmpty(matchedAlaCarteOfferItems)) {
					for (ALaCarteOfferItemType alaCarteOfferItem : matchedAlaCarteOfferItems) {
						for (SeatMapType seatMap : seatAvailabilityResponse.getResponse().getSeatMap()) {
							for (CabinCompartmentType cabinCompartment : seatMap.getCabinCompartment()) {
								for (SeatRowType seatRow : cabinCompartment.getSeatRow()) {
									int row = seatRow.getRowNumber().intValue();
									for (SeatType seat : seatRow.getSeat()) {
										if (isSeatAllowed(seat) && Arrays.asList(seat.getOfferItemRefID().split(" "))
												.contains(alaCarteOfferItem.getOfferItemID())) {

											int column = seat.getCabinColumnID().toUpperCase().charAt(0) - 64;
											SeatSSRInformation seatSSRInfo = new SeatSSRInformation();
											seatSSRInfo.setSeatNo(row + seat.getCabinColumnID());
											seatSSRInfo.setCode(row + seat.getCabinColumnID());
											SeatPosition seatPosition =
													SeatPosition.builder().row(row).column(column).build();
											seatSSRInfo.setSeatPosition(seatPosition);
											if ("F".equals(seat.getOccupationStatusCode())) {
												seatSSRInfo.setIsBooked(false);
											} else {
												seatSSRInfo.setIsBooked(true);
											}
											seatSSRInfo.setAmount(getAmountBasedOnCurrency(
													alaCarteOfferItem.getUnitPrice().getTotalAmount().getValue(),
													alaCarteOfferItem.getUnitPrice().getTotalAmount().getCurCode()));
											seatSSRInfo.getMiscInfo().setPaxSsrKeys(
													setPaxWiseSSRKeyInMisc(seat, paxSegmentId, seatAvailabilityResponse
															.getResponse().getALaCarteOffer().getALaCarteOfferItem()));
											seatsInfo.add(seatSSRInfo);

										}
									}
								}
							}
						}

					}
				}

				if (CollectionUtils.isNotEmpty(seatsInfo)) {
					SeatInformation seatInformation = SeatInformation.builder().seatsInfo(seatsInfo).build();
					setNotAvailabeSeats(seatInformation, seatAvailabilityResponse.getResponse().getSeatMap());
					seatInformation.prepareStructureData();

					tripSeat.put(segment.getId(), seatInformation);
				}
			}
			tripSeatMap.setTripSeat(tripSeat);
		}
	}

	private boolean isSeatAllowed(SeatType seat) {
		for (String restrictedCode : AmadeusNDCConstants.RESTRICTED_SEAT_CHARACTERISTIC_CODE) {
			if (seat.getCharacteristicCode().contains(restrictedCode)) {
				return false;
			}
		}
		return true;
	}

	private void setNotAvailabeSeats(SeatInformation seatInformation, List<SeatMapType> seatMapList) {
		SeatMapType seatMap = seatMapList.get(0);
		List<String> columnIds = seatMap.getCabinCompartment().get(0).getColumnID();
		// 0 .. to available-1 will be marked as booked
		Boolean bookingStatus = true;
		int notAvailableEnd = seatInformation.getSeatsInfo().get(0).getSeatPosition().getRow() - 1;
		int totalColumns = columnIds.size();
		List<SeatSSRInformation> availableSeatsList = seatInformation.getSeatsInfo();
		List<SeatSSRInformation> notAvailableSeatsList = new ArrayList<>();
		log.info("NDC Not available rows:{} and totalColums{}: for bookingID: {}", notAvailableEnd, columnIds,
				bookingId);

		for (int row = 1; row <= notAvailableEnd; row++) {
			for (int column = 0; column < totalColumns; column++) {
				SeatSSRInformation notAvailableSeatInfo = new SeatSSRInformation();
				String seatCode = String.valueOf(row) + String.valueOf(columnIds.get(column));
				notAvailableSeatInfo.setCode(seatCode);
				notAvailableSeatInfo.setIsBooked(bookingStatus);
				SeatPosition seatPosition = SeatPosition.builder().row(row).column(column).build();
				notAvailableSeatInfo.setSeatPosition(seatPosition);
				notAvailableSeatInfo.setIsAisle(false);
				notAvailableSeatInfo.setIsLegroom(false);
				notAvailableSeatInfo.setSeatNo(seatCode);
				notAvailableSeatInfo.setMiscInfo(availableSeatsList.get(0).getMiscInfo());
				notAvailableSeatsList.add(notAvailableSeatInfo);
			}
		}
		notAvailableSeatsList.addAll(availableSeatsList);
		seatInformation.setSeatsInfo(notAvailableSeatsList);
	}

	private Map<String, String> setPaxWiseSSRKeyInMisc(SeatType seat, String paxSegmentId,
			List<ALaCarteOfferItemType> aLaCarteOfferItemTypeList) {
		Map<String, String> paxWiseSSRKey = new HashMap<>();
		List<String> seatApplicableOfferItems = Arrays.asList(seat.getOfferItemRefID().split(" "));
		for (ALaCarteOfferItemType aLaCarteOfferItem : aLaCarteOfferItemTypeList) {
			if (aLaCarteOfferItem.getEligibility().getFlightAssociations().getPaxSegmentRefID().contains(paxSegmentId)
					&& seatApplicableOfferItems.contains(aLaCarteOfferItem.getOfferItemID())) {
				StringJoiner joiner = new StringJoiner("/");
				for (String paxRefId : aLaCarteOfferItem.getEligibility().getPaxRefID()) {
					joiner.add(paxSegmentId);
					joiner.add(aLaCarteOfferItem.getOfferItemID());
					// paxSegmentId / OfferItemId will be stored paxwise and will be consumed separately in the booking.
					paxWiseSSRKey.put(paxRefId, joiner.toString());
				}
			}
		}
		return paxWiseSSRKey;
	}

	private boolean isValidResponse(SeatAvailabilityRS seatAvailabilityResponse) {
		if (seatAvailabilityResponse.getResponse() != null
				&& seatAvailabilityResponse.getResponse().getALaCarteOffer() != null
				&& CollectionUtils
						.isNotEmpty(seatAvailabilityResponse.getResponse().getALaCarteOffer().getALaCarteOfferItem())
				&& seatAvailabilityResponse.getResponse().getDataLists() != null
				&& seatAvailabilityResponse.getResponse().getDataLists().getPaxList() != null
				&& CollectionUtils
						.isNotEmpty(seatAvailabilityResponse.getResponse().getDataLists().getPaxList().getPax())
				&& CollectionUtils.isNotEmpty(seatAvailabilityResponse.getResponse().getSeatMap())
				&& isAllSeatMapHasSameDeck(seatAvailabilityResponse.getResponse().getSeatMap())
				&& seatAvailabilityResponse.getResponse().getDataLists().getServiceDefinitionList() != null
				&& CollectionUtils.isNotEmpty(seatAvailabilityResponse.getResponse().getDataLists()
						.getServiceDefinitionList().getServiceDefinition())) {
			return true;
		}
		return false;
	}

	private String getPaxSegmentId(SegmentInfo segment, List<PaxSegmentType> paxSegmentTypes) {
		for (PaxSegmentType paxSegment : paxSegmentTypes) {
			LocalDateTime departureTime =
					TgsDateUtils.getLocalDateTime(paxSegment.getDep().getAircraftScheduledDateTime().getValue());
			if (segment.getDepartureAirportCode().equals(paxSegment.getDep().getIATALocationCode())
					&& segment.getArrivalAirportCode().equals(paxSegment.getArrival().getIATALocationCode())
					&& segment.getDepartTime().isEqual(departureTime)) {
				return paxSegment.getPaxSegmentID();
			}
		}
		return null;
	}

	private boolean isAllSeatMapHasSameDeck(List<SeatMapType> seatMapList) {
		String deckCode = seatMapList.get(0).getCabinCompartment().get(0).getDeckCode();
		for (SeatMapType seatMap : seatMapList) {
			for (CabinCompartmentType cabinCompartment : seatMap.getCabinCompartment()) {
				if (!deckCode.equalsIgnoreCase(cabinCompartment.getDeckCode())) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean isAnyError(SeatAvailabilityRS seatAvailabilityResponse) {
		boolean isAnyError = false;
		StringJoiner errorMessage = new StringJoiner("");
		if (seatAvailabilityResponse != null && CollectionUtils.isNotEmpty(seatAvailabilityResponse.getErrors())) {
			for (ErrorType error : seatAvailabilityResponse.getErrors()) {
				errorMessage.add(StringUtils.join(error.getCode(), "  ", error.getDescText()));
				isAnyError = true;
			}
			log.info("Error occured on booking {} error message {}", bookingId, errorMessage.toString());
		}
		logCriticalMessage(errorMessage.toString());
		return isAnyError;
	}

	private SeatAvailabilityRQ createSeatAvailability() {
		SeatAvailabilityRQ seatAvailabilityRQ = new SeatAvailabilityRQ();
		RequestType requestType = new RequestType();
		CoreRequestType coreRequestType = new CoreRequestType();
		OfferType offerType = new OfferType();
		PriceMiscInfo miscInfo = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo();
		offerType.setOfferItemID(miscInfo.getFareLevel());
		offerType.setOwnerCode("SQ");
		coreRequestType.setOffer(offerType);
		requestType.setCoreRequest(coreRequestType);
		ResponseParametersType responseParameters = new ResponseParametersType();
		PricingParameterType pricingParameters = new PricingParameterType();
		pricingParameters.setOverrideCurCode(getCurrencyCode());
		responseParameters.setPricingParameter(pricingParameters);
		requestType.setResponseParameters(responseParameters);
		seatAvailabilityRQ.setRequest(requestType);
		seatAvailabilityRQ.setParty(getPartyType());
		seatAvailabilityRQ.setPayloadAttributes(getPayloadAttributes());
		seatAvailabilityRQ.setPointOfSale(getPointOfSale());
		return seatAvailabilityRQ;
	}

	private PointofSaleType2 getPointOfSale() {
		PointofSaleType2 pointOfSale=new PointofSaleType2();
		CountryType countryType=new CountryType();
		countryType.setCountryCode(AmadeusNDCConstants.COUNTRY_CODE);
		pointOfSale.setCountry(countryType);
		return pointOfSale;
	}

	private IATAPayloadStandardAttributesType getPayloadAttributes() {
		IATAPayloadStandardAttributesType payloadAttributesType=new IATAPayloadStandardAttributesType();
		payloadAttributesType.setVersion(AmadeusNDCConstants.SERVICE_VERSION);
		return payloadAttributesType;
	}

	private PartyType getPartyType() {
		PartyType partyType = new PartyType();
		Sender sender = new Sender();
		TravelAgencyType travelAgencyType = new TravelAgencyType();
		travelAgencyType.setAgencyID(supplierConf.getSupplierCredential().getIataNumber());
		travelAgencyType.setIATANumber(new BigDecimal(supplierConf.getSupplierCredential().getIataNumber()));
		travelAgencyType.setName(supplierConf.getSupplierCredential().getAgentUserName());
		travelAgencyType.setTypeCode(TravelAgencyTypeCodeContentType.ONLINE_TRAVEL_AGENCY);
		sender.setTravelAgency(travelAgencyType);

		RecipientType recipientType = new RecipientType();
		CarrierType carrierType=new CarrierType();
		carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
		recipientType.setORA(carrierType);
		partyType.setRecipient(recipientType);
		partyType.setSender(sender);
		return partyType;
	}

	private List<ALaCarteOfferItemType> matchOffersBySegmentAndPaxID(String paxSegmentId, String adultPaxId,
			ResponseType responseType) {
		List<ALaCarteOfferItemType> matchedAlaCarteOfferItems = new ArrayList<>();
		for (ALaCarteOfferItemType alaCarteOfferItem : responseType.getALaCarteOffer().getALaCarteOfferItem()) {
			if (alaCarteOfferItem.getEligibility().getPaxRefID().contains(adultPaxId) && alaCarteOfferItem
					.getEligibility().getFlightAssociations().getPaxSegmentRefID().contains(paxSegmentId)) {
				matchedAlaCarteOfferItems.add(alaCarteOfferItem);
			}
		}
		return matchedAlaCarteOfferItems;
	}

}
