package com.tgs.services.fms.sources.travelport.session;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import com.travelport.www.schema.universal_v47_0.ProviderReservationStatus_type0;
import com.travelport.www.schema.universal_v47_0.UniversalRecordCancelReq;
import com.travelport.www.schema.universal_v47_0.UniversalRecordCancelRsp;
import com.travelport.www.service.universal_v47_0.UniversalRecordFaultMessage;
import com.travelport.www.service.universal_v47_0.UniversalRecordCancelServiceStub;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import java.rmi.RemoteException;

@SuperBuilder
@Getter
@Setter
final class TravelPortCancellationManager extends TravelPortServiceManager {

	protected static final String ALREADY_CANCELED = "already been cancelled";

	public boolean cancelPNR(String supplierBookingId) {
		boolean isCancelSuccess = false;
		UniversalRecordCancelServiceStub cancelService = bindingService.getUniversalRecordCancelService();
		listener.setType(AirUtils.getLogType("UniversalRecordCancel", configuration));
		UniversalRecordCancelReq universalRecordCancelReq = new UniversalRecordCancelReq();
		try {
			buildBaseCoreRequest(universalRecordCancelReq);
			universalRecordCancelReq.setVersion(getTypeURversion(version));
			universalRecordCancelReq.setUniversalRecordLocatorCode(getTypeLocatorCode(universalLocatorCode));
			cancelService._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(cancelService, "ReleasePNR");
			UniversalRecordCancelRsp universalRecordCancelRsp = cancelService.service(universalRecordCancelReq);
			if (checkAnyErrors(universalRecordCancelRsp)) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
			ProviderReservationStatus_type0 reservationStatus =
					getList(universalRecordCancelRsp.getProviderReservationStatus()).get(0);
			if (reservationStatus.getCancelInfo() != null && reservationStatus.getCancelInfo().getType() != null
					&& "Error".equals(reservationStatus.getCancelInfo().getType().getValue())) {
				logCriticalMessage(reservationStatus.getCancelInfo().getString());
			} else {
				isCancelSuccess = reservationStatus.getCancelled();
			}
		} catch (UniversalRecordFaultMessage fault) {
			if (StringUtils.isNotBlank(fault.getMessage())
					&& fault.getMessage().toLowerCase().contains(ALREADY_CANCELED)) {
				isCancelSuccess = true;
			} else {
				throw new SupplierUnHandledFaultException(fault.getMessage());
			}
		} catch (RemoteException e) {
			throw new CustomGeneralException(e.getMessage());
		} finally {
			cancelService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isCancelSuccess;
	}

}
