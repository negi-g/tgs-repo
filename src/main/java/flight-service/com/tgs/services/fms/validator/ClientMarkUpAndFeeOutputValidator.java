package com.tgs.services.fms.validator;

import java.util.Objects;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.AirClientFeeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirClientMarkupOutput;

@Service
public class ClientMarkUpAndFeeOutputValidator extends AbstractAirConfigRuleValidator {

	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {

		if (iRuleOutput == null) {
			return;
		}

		if (iRuleOutput instanceof AirClientMarkupOutput) {
			AirClientMarkupOutput output = (AirClientMarkupOutput) iRuleOutput;
			if (output.getAirClientMarkUp() == null || output.getAirClientMarkUp().doubleValue() < 0) {
				rejectValue(errors, fieldName + ".airClientMarkUp", SystemError.NEGATIVE_VALUE);
			}
			if (Objects.isNull(output.getConditions())) {
				rejectValue(errors, fieldName + ".conditions", SystemError.NULL_VALUE);
			}
		}

		if (iRuleOutput instanceof AirClientFeeOutput) {
			AirClientFeeOutput output = (AirClientFeeOutput) iRuleOutput;

			if (output.getManagementFee() != null && output.getManagementFee().doubleValue() < 0) {
				rejectValue(errors, fieldName + ".managementFee", SystemError.NEGATIVE_VALUE);
			}

			if (output.getClientRescheduleFee() != null && output.getClientRescheduleFee().doubleValue() < 0) {
				rejectValue(errors, fieldName + ".clientRescheduleFee", SystemError.NEGATIVE_VALUE);
			}

			if (output.getClientCancellationFee() != null && output.getClientCancellationFee().doubleValue() < 0) {
				rejectValue(errors, fieldName + ".clientCancellationFee", SystemError.NEGATIVE_VALUE);
			}

			if (MapUtils.isNotEmpty(output.getAirTypeClientCancellationFee())) {
				output.getAirTypeClientCancellationFee().forEach((airType, fee) -> {
					if (airType == null) {
						rejectValue(errors, fieldName + ".airTypeClientCancellationFee", SystemError.NULL_VALUE);
					}
					if (fee == null || fee.doubleValue() < 0) {
						rejectValue(errors, fieldName + ".airTypeClientCancellationFee", SystemError.NEGATIVE_VALUE);
					}
				});
			}

			if (MapUtils.isNotEmpty(output.getAmendmentFeeMap())) {
				output.getAmendmentFeeMap().forEach((key, airAmendmentClientFee) -> {
					if (StringUtils.isBlank(key) || Objects.isNull(airAmendmentClientFee)
							|| MapUtils.isEmpty(airAmendmentClientFee.getAmendmentFee())) {
						rejectValue(errors, fieldName + ".amendmentFeeMap", SystemError.NULL_VALUE);
					} else {
						airAmendmentClientFee.getAmendmentFee().forEach((airType, fee) -> {
							if (Objects.isNull(airType)) {
								rejectValue(errors, fieldName + ".amendmentFeeMap", SystemError.NULL_VALUE);
							}
							if (fee == null || fee.doubleValue() < 0) {
								rejectValue(errors, fieldName + ".amendmentFeeMap", SystemError.NEGATIVE_VALUE);
							}
						});
					}
				});
			}
		}
	}
}
