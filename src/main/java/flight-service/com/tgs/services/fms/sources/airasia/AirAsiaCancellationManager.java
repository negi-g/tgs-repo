package com.tgs.services.fms.sources.airasia;

import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.tempuri.*;
import java.rmi.RemoteException;

@Setter
@Getter
@Slf4j
@SuperBuilder
final class AirAsiaCancellationManager extends AirAsiaServiceManager {


	public boolean cancelAll() {
		boolean isCancelSuccess = false;
		CancelResponse cancelResponse = null;
		try {
			listener.setType("CANCEL");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			cancelResponse = bookingStub.cancel(buildCancelRequest());
			if (cancelResponse != null && isAnyError(cancelResponse.getCancelResult())) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
			if (cancelResponse != null && cancelResponse.getCancelResult() != null
					&& cancelResponse.getCancelResult().getSuccess() != null) {
				isCancelSuccess = true;
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isCancelSuccess;
	}

	private Cancel buildCancelRequest() {
		Cancel cancel = new Cancel();
		CancelRequestData cancelRequest = new CancelRequestData();
		cancelRequest.setCancelBy(CancelBy.All);
		cancelRequest.setCancelFee(null);
		cancelRequest.setCancelJourney(null);
		cancelRequest.setCancelSSR(null);
		cancel.setObjCancelRequestData(cancelRequest);
		cancel.setStrSessionID(sessionSignature);
		return cancel;
	}

	public boolean checkCancelStatus(Booking bookingResponse) {
		boolean isCancelled = false;
		ArrayOfJourney journey = bookingResponse.getJourneys();
		if (bookingResponse != null && bookingResponse.getBookingInfo() != null) {
			BookingStatus bookingStatus = bookingResponse.getBookingInfo().getBookingStatus();
			if (bookingStatus.equals(BookingStatus.Closed) || bookingStatus.equals(BookingStatus.HoldCanceled)) {
				isCancelled = true;
			}
		}
		if (!isCancelled && (journey == null || (journey != null && ArrayUtils.isEmpty(journey.getJourney())))) {
			isCancelled = true;
		}
		return isCancelled;
	}
}
