package com.tgs.services.fms.sources.travelport.sessionless;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TravelPortSessionLessAirInfoFactory extends AbstractAirInfoFactory {

	protected TravelPortSessionLessBindingService bindingService;

	protected SoapRequestResponseListner listener;

	public TravelPortSessionLessAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public TravelPortSessionLessAirInfoFactory(SupplierConfiguration supplierConf) {
		super(null, supplierConf);
	}

	public void initialize() {
		bindingService = TravelPortSessionLessBindingService.builder().configuration(supplierConf).build();
		bindingService.setSupplierAnalyticsInfos(supplierAnalyticsInfos);
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize();
		Map<AirSearchQuery, Future<AirSearchResult>> futureTaskMap = new LinkedHashMap<>();

		if (searchQuery.isDomesticReturn()
				&& BooleanUtils.isTrue(supplierConf.getSupplierCredential().getIsSplitTicketDisabled())) {
			List<AirSearchQuery> splitQueries = AirUtils.splitSearchQueriesForDomesticReturn(searchQuery);

			// Onward one way search
			createSearchTask(splitQueries.get(0), futureTaskMap);

			// Return one way search
			createSearchTask(splitQueries.get(1), futureTaskMap);

			// complete search
			createSearchTask(searchQuery, futureTaskMap);
		} else {
			TravelPortSessionLessSearchManager searchManager = buildSearchManager(searchQuery);
			searchResult = searchManager.doSearch();
		}

		for (AirSearchQuery sQuery : futureTaskMap.keySet()) {
			try {
				AirSearchResult newSResult = futureTaskMap.get(sQuery).get(30, TimeUnit.SECONDS);
				if (searchResult == null || MapUtils.isEmpty(searchResult.getTripInfos())) {
					searchResult = newSResult;
				} else {
					searchResult = AirUtils.mergeSearchResult(searchResult, newSResult, sQuery);
				}
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				if (CollectionUtils.isNotEmpty(criticalMessageLogger)) {
					criticalMessageLogger.add(e.getMessage());
				} else {
					log.error(AirSourceConstants.AIR_SUPPLIER_SEARCH, supplierConf.getBasicInfo().getDescription(),
							searchQuery.getSearchId(), e);
				}
			}
		}
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		initialize();
		TripInfo reviewedTrip = null;
		try {
			String traceId = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
			String providerCode = TravelPortSessionLessUtils.getProviderCode(selectedTrip, supplierConf);
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			TravelPortSessionLessReviewManager reviewManager = TravelPortSessionLessReviewManager.builder()
					.searchQuery(searchQuery).sourceConfiguration(sourceConfiguration).configuration(supplierConf)
					.bindingService(bindingService).traceId(traceId).moneyExchnageComm(moneyExchnageComm)
					.bookingId(bookingId).providerCode(providerCode).listener(listener).bookingUser(user).build();
			reviewManager.init();
			reviewedTrip = reviewManager.reviewFlight(selectedTrip, true);
		} finally {
			storeBookingSession(bookingId, null, null, reviewedTrip);
		}
		return reviewedTrip;
	}

	@Override
	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		initialize();
		TripFareRule tripFareRule = null;
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		TravelPortSessionLessFareRuleManager ruleManager = TravelPortSessionLessFareRuleManager.builder()
				.configuration(supplierConf).bookingId(bookingId).listener(listener).bindingService(bindingService)
				.moneyExchnageComm(moneyExchnageComm).bookingUser(user).build();
		ruleManager.init();
		if (isDetailed) {
			tripFareRule = ruleManager.getFareRule(selectedTrip, bookingId);
		} else {
			if (BooleanUtils.isTrue(sourceConfiguration.getIsFareRuleFromITQ())) {
				SupplierConfiguration supplierConfiguration =
						SupplierConfigurationHelper.getSupplierConfiguration("Travelport-FareRule");
				TravelPortSessionLessMiniRuleManager miniRuleManager = TravelPortSessionLessMiniRuleManager.builder()
						.configuration(supplierConfiguration).bookingId(bookingId).bookingId(bookingId)
						.moneyExchnageComm(moneyExchnageComm).bookingUser(user).build();
				miniRuleManager.init();
				tripFareRule = miniRuleManager.getFareRule(selectedTrip, bookingId);
			} else {
				boolean isFareDisplayFlow = false;
				if (!ruleManager.isFareRuleKeyAvailable(selectedTrip)) {
					ruleManager.airFareDisplay(selectedTrip);
					isFareDisplayFlow = true;
				}
				tripFareRule = ruleManager.getStructuredFareRule(selectedTrip, isFareDisplayFlow);
			}
		}
		return tripFareRule;
	}

	private TravelPortSessionLessSearchManager buildSearchManager(AirSearchQuery searchQuery) {
		SoapRequestResponseListner listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
				supplierConf.getBasicInfo().getSupplierName());
		boolean isSplitTicketingSearch = searchQuery.isDomesticReturn()
				&& BooleanUtils.isNotTrue(supplierConf.getSupplierCredential().getIsSplitTicketDisabled());
		TravelPortSessionLessSearchManager searchManager = TravelPortSessionLessSearchManager.builder()
				.searchQuery(searchQuery).moneyExchnageComm(moneyExchnageComm).sourceConfiguration(sourceConfiguration)
				.configuration(supplierConf).bindingService(bindingService).criticalMessageLogger(criticalMessageLogger)
				.bookingUser(user).isSplitTicketingSearch(isSplitTicketingSearch).build();
		searchManager.setListener(listener);
		searchManager.setTraceId(TravelPortSessionLessUtils.keyGenerator());
		searchManager.init();
		return searchManager;
	}

	private void createSearchTask(AirSearchQuery searchQuery,
			Map<AirSearchQuery, Future<AirSearchResult>> futureTaskMap) {
		TravelPortSessionLessSearchManager searchManager = buildSearchManager(searchQuery);
		Future<AirSearchResult> futureTask =
				ExecutorUtils.getFlightSearchThreadPool().submit(() -> searchManager.doSearch());
		futureTaskMap.put(searchQuery, futureTask);
	}

	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		initialize();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		String traceId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		String providerCode = TravelPortSessionLessUtils.getProviderCode(tripInfo, supplierConf);
		TravelPortSessionLessSeatManager seatManager = TravelPortSessionLessSeatManager.builder()
				.searchQuery(searchQuery).sourceConfiguration(sourceConfiguration).configuration(supplierConf)
				.bindingService(bindingService).traceId(traceId).moneyExchnageComm(moneyExchnageComm)
				.bookingId(bookingId).providerCode(providerCode).listener(listener).bookingUser(user).build();
		seatManager.init();

		// For time being not showing seat map in case infant booking
		if (searchQuery.getPaxInfo().get(PaxType.INFANT) > 0) {
			return null;
		}
		return seatManager.getSeatMap(tripInfo, seatManager.getSeatMapRsp(seatManager.getSeatMapRequest(tripInfo)));
	}

}
