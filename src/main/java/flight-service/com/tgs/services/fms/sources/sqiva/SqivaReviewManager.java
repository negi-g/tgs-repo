package com.tgs.services.fms.sources.sqiva;


import com.tgs.services.base.GsonMapper;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import java.util.Iterator;
import java.util.List;

@SuperBuilder
@Getter
@Setter
final class SqivaReviewManager extends SqivaServiceManager {

	public TripInfo doReview(TripInfo tripInfo) {
		TripInfo orginalTripInfo = new GsonMapper<>(tripInfo, TripInfo.class).convert();
		List<TripInfo> tripInfos = AirUtils.splitTripInfo(tripInfo, false);
		AirSearchQuery searchQuery = AirUtils.combineSearchQuery(AirUtils.getSearchQueryFromTripInfos(tripInfos));
		boolean isSpecialReturn = searchQuery.isReturn() && SqivaUtils.isAllPriceOptionsAreSR(tripInfo);
		SqivaFareSearchManager fareManager = SqivaFareSearchManager.builder().searchQuery(searchQuery)
				.configuration(configuration).criticalMessageLogger(criticalMessageLogger).bookignId(bookignId)
				.listener(listener).sqivaAirline(sqivaAirline).bindingService(bindingService).user(user)
				.isReviewFlow(true).build();
		fareManager.setTripInfo(tripInfo);
		fareManager.setSpecialReturn(isSpecialReturn);
		TripInfo reviewRedTrip = fareManager.doFareQuote();
		doMatchAndFilterFares(orginalTripInfo, reviewRedTrip);
		if (!reviewRedTrip.isPriceInfosNotEmpty()) {
			throw new NoSeatAvailableException("NoFares Available");
		}
		return reviewRedTrip;
	}


	private void doMatchAndFilterFares(TripInfo orginalTripInfo, TripInfo reviewRedTrip) {
		for (int segmentNum = 0; segmentNum < orginalTripInfo.getSegmentInfos().size(); segmentNum++) {
			SegmentInfo orgSegmentInfo = orginalTripInfo.getSegmentInfos().get(segmentNum);
			SegmentInfo reviewedSegment = reviewRedTrip.getSegmentInfos().get(segmentNum);
			PriceInfo orginalPrice = orgSegmentInfo.getPriceInfo(0);
			filterMatchedPrices(reviewedSegment, orginalPrice);
		}
	}

	private void filterMatchedPrices(SegmentInfo reviewedSegment, PriceInfo orginalPrice) {
		for (Iterator<PriceInfo> iter = reviewedSegment.getPriceInfoList().iterator(); iter.hasNext();) {
			PriceInfo priceInfo = iter.next();
			if (!AirUtils.isSamePriceInfo(priceInfo, orginalPrice)) {
				iter.remove();
			}
		}
	}

}
