package com.tgs.services.fms.sources.travelport.session;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.oms.datamodel.Order;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Slf4j
@Service
public class AtlasTravelPortAirBookingFactory extends TravelPortAirBookingFactory {

	public AtlasTravelPortAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments,
			Order order) throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	@Override
	protected void additionalAncillaries() {
		Integer noOfDaysForPNRActive = TravelPortUtils.retainPNRDays(sourceConfiguration);
		LocalDate pnrDaysToBeActive = null;
		if (noOfDaysForPNRActive != null) {
			pnrDaysToBeActive = LocalDate.now().plusDays(noOfDaysForPNRActive);
		} else {
			int segmentSize = CollectionUtils.size(getBookingSegments().getSegmentInfos());
			pnrDaysToBeActive = getBookingSegments().getSegmentInfos().get(segmentSize - 1).getArrivalTime()
					.toLocalDate().plusDays(180);
		}
		try {
			bookingManager.addContactDetails("VIEWTRIPITIN");
			bookingManager.pnrRetention(pnrDaysToBeActive);
		} finally {
			bookingManager.resetKeys();
		}

	}

	@Override
	protected void airPNRElement() {
		bookingManager.bookingAirPnrElement(true);
	}

}
