package com.tgs.services.fms.sources.travelport.session;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.manager.TripPriceEngine;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import com.travelport.www.schema.air_v47_0.AirLegModifiers_type0;
import com.travelport.www.schema.air_v47_0.AirPricePoint_type0;
import com.travelport.www.schema.air_v47_0.AirPricingInfo_type0;
import com.travelport.www.schema.air_v47_0.AirPricingModifiers_type0;
import com.travelport.www.schema.air_v47_0.AirSearchModifiers_type0;
import com.travelport.www.schema.air_v47_0.BookingInfo_type0;
import com.travelport.www.schema.air_v47_0.FareInfo_type0;
import com.travelport.www.schema.air_v47_0.FlightDetails_type0;
import com.travelport.www.schema.air_v47_0.FlightOption_type0;
import com.travelport.www.schema.air_v47_0.LowFareSearchReq;
import com.travelport.www.schema.air_v47_0.LowFareSearchRsp;
import com.travelport.www.schema.air_v47_0.Option_type0;
import com.travelport.www.schema.air_v47_0.PassengerType_type0;
import com.travelport.www.schema.air_v47_0.PermittedCabins_type0;
import com.travelport.www.schema.air_v47_0.PreferredCabins_type0;
import com.travelport.www.schema.air_v47_0.PreferredCarriers_type0;
import com.travelport.www.schema.air_v47_0.PreferredProviders_type0;
import com.travelport.www.schema.air_v47_0.SearchAirLegChoice_type1;
import com.travelport.www.schema.air_v47_0.SearchAirLeg_type0;
import com.travelport.www.schema.air_v47_0.SplitTicketingSearch_type0;
import com.travelport.www.schema.air_v47_0.TypeBaseAirSegment;
import com.travelport.www.schema.air_v47_0.TypeEticketability;
import com.travelport.www.schema.air_v47_0.TypeFaresIndicator;
import com.travelport.www.schema.common_v47_0.BaseCoreSearchReqChoice_type0;
import com.travelport.www.schema.common_v47_0.CabinClass_type0;
import com.travelport.www.schema.common_v47_0.Carrier_type0;
import com.travelport.www.schema.common_v47_0.CityOrAirport_type0;
import com.travelport.www.schema.common_v47_0.Provider_type0;
import com.travelport.www.schema.common_v47_0.SearchPassenger_type0;
import com.travelport.www.schema.common_v47_0.TypeCarrier;
import com.travelport.www.schema.common_v47_0.TypeFlexibleTimeSpec;
import com.travelport.www.schema.common_v47_0.TypePTC;
import com.travelport.www.schema.common_v47_0.TypeSearchLocation;
import com.travelport.www.schema.common_v47_0.TypeSearchLocationChoice_type0;
import com.travelport.www.service.air_v47_0.AirFaultMessage;
import com.travelport.www.service.air_v47_0.AirServiceStub;
import com.travelport.www.schema.air_v47_0.PassengerType_type0;
import lombok.Builder;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class TravelPortSearchManager extends TravelPortServiceManager {

	protected AirSearchResult searchResult;
	private Map<String, TypeBaseAirSegment> airSegementReferenceMap;
	private Map<String, FlightDetails_type0> flightDetailsMap;
	private Map<String, FareInfo_type0> fareInfoMap;
	private boolean isSplitTicketingSearch;


	public AirSearchResult doSearch() {
		log.debug("Intialization completed AirService for search Query {} ", searchQuery);
		AirServiceStub serviceBinding = bindingService.getAirService();
		listener.setType(AirUtils.getLogType("Low Fare Search", configuration));
		LowFareSearchReq lowFareSearchRequest = buildLowFareSearchReq();
		LowFareSearchRsp searchResponse = null;
		try {
			SystemContextHolder.getContextData().setSearchWatch(new StopWatch());
			SystemContextHolder.getContextData().getSearchWatch().start();
			serviceBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(serviceBinding, "Search");
			searchResponse = serviceBinding.service(lowFareSearchRequest, getSessionContext(false));
			log.debug("Total time took to process key {} is {}", searchQuery.getSearchId(),
					SystemContextHolder.getContextData().getSearchWatch().getTime());
			if (!checkAnyErrors(searchResponse)) {
				parseSearchResponse(searchResponse);
			} else {
				throw new NoSearchResultException(String.join(",", criticalMessageLogger));
			}
		} catch (AirFaultMessage airFaultMessage) {
			if (UNAUTHORIZED_CRED.equalsIgnoreCase(airFaultMessage.getMessage())) {
				throw new SupplierSessionException(airFaultMessage.getMessage());
			}
			throw new NoSearchResultException(airFaultMessage.getMessage());
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			serviceBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return searchResult;
	}

	protected LowFareSearchReq buildLowFareSearchReq() {
		LowFareSearchReq lowFareSearchRequest = new LowFareSearchReq();
		BaseCoreSearchReqChoice_type0 baseCore = new BaseCoreSearchReqChoice_type0();
		buildBaseCoreRequest(lowFareSearchRequest);
		for (RouteInfo routeInfo : searchQuery.getRouteInfos()) {
			baseCore.addSearchAirLeg(buildAirSearchReqProperty(lowFareSearchRequest, routeInfo));
		}
		buildAirSearchModifiers(lowFareSearchRequest);
		if (isSplitTicketingSearch) {
			lowFareSearchRequest.setSplitTicketingSearch(new SplitTicketingSearch_type0());
		}
		buildPassengerDetails(lowFareSearchRequest);
		buildAirPricingModifiers(lowFareSearchRequest);
		lowFareSearchRequest.setBaseCoreSearchReqChoice_type0(baseCore);
		lowFareSearchRequest.setSolutionResult(false);
		return lowFareSearchRequest;
	}

	protected void buildAirSearchModifiers(LowFareSearchReq lowFareSearchRequest) {
		AirSearchModifiers_type0 airSearchModifiers = new AirSearchModifiers_type0();
		airSearchModifiers.setMaxSolutions(new BigInteger(getItinCount()));
		if (StringUtils.isNotBlank(configuration.getSupplierCredential().getProviderCode())) {
			PreferredProviders_type0 preferredProviders = new PreferredProviders_type0();
			Provider_type0 provider = new Provider_type0();
			provider.setCode(getTypeProviderCode(configuration.getSupplierCredential().getProviderCode()));
			preferredProviders.addProvider(provider);
			airSearchModifiers.setPreferredProviders(preferredProviders);
		}
		if (CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())) {
			PreferredCarriers_type0 preferredCarriers = new PreferredCarriers_type0();
			airSearchModifiers.setPreferredCarriers(preferredCarriers);
			Carrier_type0[] carrier_type0s = new Carrier_type0[searchQuery.getPreferredAirline().size()];
			int index = 0;
			for (AirlineInfo airlineInfo : searchQuery.getPreferredAirline()) {
				Carrier_type0 carrier_type0 = new Carrier_type0();
				TypeCarrier carrier = getTypeCarrierCode(airlineInfo.getCode());
				carrier_type0.setCode(carrier);
				carrier_type0s[index++] = carrier_type0;
			}
			airSearchModifiers.getPreferredCarriers().setCarrier(carrier_type0s);
		}
		lowFareSearchRequest.setAirSearchModifiers(airSearchModifiers);
	}

	protected void buildPassengerDetails(LowFareSearchReq lowFareSearchRequest) {
		List<SearchPassenger_type0> searchPassenger_type0s = new ArrayList<>();
		for (Map.Entry<PaxType, Integer> paxInfo : searchQuery.getPaxInfo().entrySet()) {
			int paxCount = paxInfo.getValue();
			while (paxCount > 0) {
				SearchPassenger_type0 type0 = new SearchPassenger_type0();
				PaxType paxType = paxInfo.getKey();
				TypePTC type = new TypePTC();
				if (!isSpecialAccCodeApplied) {
					type.setTypePTC(paxType.getType());
				} else {
					type.setTypePTC(getPaxType(paxType));
				}
				if (isSpecialAccCodeApplied) {
					type0.setPricePTCOnly(true);
				}
				type0.setCode(type);
				type0.setBookingTravelerRef(TravelPortUtils.keyGenerator());
				if (PaxType.CHILD.getType().equals(paxType.getType())) {
					type0.setAge(new BigInteger(TravelPortConstants.CHILD_AGE));
				}
				if (PaxType.INFANT.getType().equals(paxType.getType())) {
					type0.setPricePTCOnly(Boolean.TRUE);
					type0.setAge(new BigInteger(TravelPortConstants.INFANT_AGE));
				}
				searchPassenger_type0s.add(type0);
				paxCount--;
			}
		}
		lowFareSearchRequest.setSearchPassenger(searchPassenger_type0s.toArray(new SearchPassenger_type0[0]));
	}

	public String getItinCount() {
		int flightCount = 100;
		if (sourceConfiguration != null) {
			if (searchQuery.getIsDomestic()) {
				flightCount = org.apache.commons.lang3.ObjectUtils.firstNonNull(
						sourceConfiguration.getDomflightsCount(), sourceConfiguration.getFlightsCount(), flightCount);
			} else {
				flightCount = org.apache.commons.lang3.ObjectUtils.firstNonNull(sourceConfiguration.getFlightsCount(),
						sourceConfiguration.getDomflightsCount(), flightCount);
			}
		}
		return String.valueOf(flightCount);
	}

	protected void buildAirPricingModifiers(LowFareSearchReq lowFareSearchRequest) {
		AirPricingModifiers_type0 airPricingModifiers = new AirPricingModifiers_type0();
		if (CollectionUtils.isNotEmpty(configuration.getSupplierAdditionalInfo().getAccountCodes())) {
			airPricingModifiers.setFaresIndicator(TypeFaresIndicator.PrivateFaresOnly);
			airPricingModifiers.setAccountCodeFaresOnly(Boolean.TRUE);
			setAccountCodesInReq(configuration.getSupplierAdditionalInfo().getAccountCodes(), airPricingModifiers);
		} else {
			airPricingModifiers.setFaresIndicator(TypeFaresIndicator.AllFares);
		}
		airPricingModifiers.setETicketability(TypeEticketability.Yes);
		lowFareSearchRequest.setAirPricingModifiers(airPricingModifiers);
	}

	protected SearchAirLeg_type0 buildAirSearchReqProperty(LowFareSearchReq lowFareSearchRequest, RouteInfo routeInfo) {
		SearchAirLeg_type0 searchAirLeg = new SearchAirLeg_type0();
		SearchAirLegChoice_type1 legChoice = new SearchAirLegChoice_type1();
		TypeSearchLocationChoice_type0 originLocation = new TypeSearchLocationChoice_type0();
		TypeSearchLocationChoice_type0 destLocation = new TypeSearchLocationChoice_type0();
		TypeFlexibleTimeSpec[] specTimes = new TypeFlexibleTimeSpec[1];
		TypeSearchLocation[] origin = new TypeSearchLocation[1];
		TypeSearchLocation[] dest = new TypeSearchLocation[1];
		TypeSearchLocation searchOrigin = new TypeSearchLocation();
		CityOrAirport_type0 originAirport = new CityOrAirport_type0();
		originAirport.setCode(getIATACode(routeInfo.getFromCityAirportCode()));
		originLocation.setCityOrAirport(originAirport);
		searchOrigin.setTypeSearchLocationChoice_type0(originLocation);
		TypeSearchLocation searchDestination = new TypeSearchLocation();
		CityOrAirport_type0 destinationAirport = new CityOrAirport_type0();
		destinationAirport.setCode(getIATACode(routeInfo.getToCityAirportCode()));
		destLocation.setCityOrAirport(destinationAirport);
		searchDestination.setTypeSearchLocationChoice_type0(destLocation);
		searchDestination.getTypeSearchLocationChoice_type0().setCityOrAirport(destinationAirport);
		TypeFlexibleTimeSpec departureTime = new TypeFlexibleTimeSpec();
		departureTime.setPreferredTime(routeInfo.getTravelDate().toString());
		origin[0] = searchOrigin;
		dest[0] = searchDestination;
		specTimes[0] = departureTime;
		legChoice.setSearchDepTime(specTimes);
		searchAirLeg.setSearchOrigin(origin);
		searchAirLeg.setSearchDestination(dest);
		searchAirLeg.setSearchAirLegChoice_type1(legChoice);
		buildAirLegModifiers(searchAirLeg);
		return searchAirLeg;
	}

	private void parseSearchResponse(LowFareSearchRsp searchResponse) {
		searchResult = new AirSearchResult();
		airSegementReferenceMap = new HashMap<>();
		flightDetailsMap = new HashMap<>();
		fareInfoMap = new HashMap<>();

		long startTime = System.currentTimeMillis();
		long sumOfAllIteration = 0L;
		if (searchResponse != null && searchResponse.getBaseAvailabilitySearchRspChoice_type0() != null
				&& searchResponse.getBaseAvailabilitySearchRspChoice_type0().getAirPricePointList() != null
				&& ArrayUtils.isNotEmpty(searchResponse.getBaseAvailabilitySearchRspChoice_type0()
						.getAirPricePointList().getAirPricePoint())) {

			for (TypeBaseAirSegment typeBaseAirSegment : searchResponse.getAirSegmentList().getAirSegment()) {
				airSegementReferenceMap.put(typeBaseAirSegment.getKey().getTypeRef(), typeBaseAirSegment);
			}

			for (FlightDetails_type0 flightDetails : searchResponse.getFlightDetailsList().getFlightDetails()) {
				flightDetailsMap.put(flightDetails.getKey().getTypeRef(), flightDetails);
			}

			for (FareInfo_type0 fareInfo : searchResponse.getFareInfoList().getFareInfo()) {
				fareInfoMap.put(fareInfo.getKey().getTypeRef(), fareInfo);
			}


			// Map<String, List<TripInfo>> intlcompleteItenaryFalseTripInfoMap = new LinkedHashMap<>();
			int index = 1;
			for (AirPricePoint_type0 airPricePoint : searchResponse.getBaseAvailabilitySearchRspChoice_type0()
					.getAirPricePointList().getAirPricePoint()) {
				long startTime1 = System.currentTimeMillis();
				buildTripsOnAirPricePoint(airPricePoint);
				long endTime1 = System.currentTimeMillis();
				log.debug("Airpirce Point Create Trips {} for {} and took {}", index, searchQuery.getSearchId(),
						endTime1 - startTime1);
				sumOfAllIteration += (endTime1 - startTime1);
				index++;
			}
			// if (!intlcompleteItenaryFalseTripInfoMap.isEmpty()) {
			// make Manual Commbinations For International CompleteItenary False List for intl split Screen
			// setTripInfo(tripInfos, processIntlTripInfoList(intlcompleteItenaryFalseTripInfoMap),
			// TripInfoType.COMBO.name());
			// searchResult.setTripInfos(tripInfos);
			// }
		}
		long endTime = System.currentTimeMillis();
		log.debug("Total SearchResult Creating {} and took {} and SumOfAll {}", searchQuery.getSearchId(),
				endTime - startTime, sumOfAllIteration);
	}

	private void buildTripsOnAirPricePoint(AirPricePoint_type0 airPricePoint) {
		AtomicBoolean isReturnSegment = new AtomicBoolean(false);
		List<TripInfo> onwardTripInfoList = new ArrayList<>();
		List<TripInfo> returnTripInfoList = new ArrayList<>();
		Map<String, List<TripInfo>> segregateTripInfoMap = new LinkedHashMap<>();
		List<TripInfo> completeItenaryFalseTripInfoList = new ArrayList<>();
		for (FlightOption_type0 flightOption : airPricePoint.getAirPricingInfo()[0].getFlightOptionsList()
				.getFlightOption()) {
			List<TripInfo> intlTripInfoList = new ArrayList<>();
			String flightOptionKey = flightOption.getOrigin() + "_" + flightOption.getDestination();
			for (Option_type0 option : flightOption.getOption()) {
				List<SegmentInfo> segmentInfos = buildSegments(option, airPricePoint);
				TripInfo tripInfo = null;
				if (CollectionUtils.isNotEmpty(segmentInfos)) {
					tripInfo = new TripInfo();
					tripInfo.setSegmentInfos(segmentInfos);
					if (airPricePoint.getCompleteItinerary() && searchQuery.isDomesticReturn()) {
						if (isReturnSegment.get()) {
							returnTripInfoList.add(tripInfo);
						} else {
							onwardTripInfoList.add(tripInfo);
						}
					} else if (searchQuery.isOneWay() || searchQuery.isDomesticMultiCity()) {
						onwardTripInfoList.add(tripInfo);
					} else if ((airPricePoint.getCompleteItinerary()
							&& (searchQuery.isIntlReturn() || (searchQuery.isIntl() && searchQuery.isMultiCity())))) {
						intlTripInfoList.add(tripInfo);
					} else if (searchQuery.isDomesticReturn()) {
						completeItenaryFalseTripInfoList.add(tripInfo);
					}
					// else {
					// buildIntlCompleteItenaryFalseTripInfoMap(flightOptionKey, intlcompleteItenaryFalseTripInfoMap,
					// tripInfo);
					// }
				}
			}
			if (airPricePoint.getCompleteItinerary() && searchQuery.isDomesticReturn())
				isReturnSegment.set(true);
			if (!intlTripInfoList.isEmpty()) {
				segregateTripInfoMap.put(flightOptionKey, intlTripInfoList);
			}
		}

		if (CollectionUtils.isNotEmpty(onwardTripInfoList) && CollectionUtils.isNotEmpty(returnTripInfoList)
				&& searchQuery.isDomesticReturn()) {
			for (TripInfo onwardTripInfo : onwardTripInfoList) {
				for (TripInfo returnTripInfo : returnTripInfoList) {
					TripPriceEngine.setSpecialReturnIdentifier(onwardTripInfo.getSegmentInfos().get(0),
							onwardTripInfo.getSegmentInfos().get(0).getPriceInfo(0),
							returnTripInfo.getSegmentInfos().get(0),
							returnTripInfo.getSegmentInfos().get(0).getPriceInfo(0), onwardTripInfo, returnTripInfo);
				}
			}
			setTripInfo(onwardTripInfoList, TripInfoType.ONWARD.name());
			setTripInfo(returnTripInfoList, TripInfoType.RETURN.name());
		}

		if (searchQuery.isOneWay() || searchQuery.isDomesticMultiCity()) {
			setTripInfo(onwardTripInfoList, TripInfoType.ONWARD.name());
		} else if ((searchQuery.isIntl() && searchQuery.isMultiCity())) {
			setTripInfo(processIntlTripInfoList(segregateTripInfoMap), TripInfoType.COMBO.name());
		} else if (searchQuery.isIntlReturn() && segregateTripInfoMap.size() != 0) {
			splitSROnSearchQuery(processIntlTripInfoList(segregateTripInfoMap));
		} else if (searchQuery.isDomesticReturn() && CollectionUtils.isNotEmpty(completeItenaryFalseTripInfoList)) {
			splitSROnSearchQuery(completeItenaryFalseTripInfoList);
		}
	}

	private void buildIntlCompleteItenaryFalseTripInfoMap(String flightOptionKey,
			Map<String, List<TripInfo>> intlcompleteItenaryFalseTripInfoMap, TripInfo tripInfo) {
		List<TripInfo> tripInfoList = null;
		if (intlcompleteItenaryFalseTripInfoMap.containsKey(flightOptionKey)) {
			tripInfoList = intlcompleteItenaryFalseTripInfoMap.get(flightOptionKey);
		} else {
			tripInfoList = new ArrayList<>();
		}
		tripInfoList.add(tripInfo);
		intlcompleteItenaryFalseTripInfoMap.put(flightOptionKey, tripInfoList);
	}

	private List<TripInfo> processIntlTripInfoList(Map<String, List<TripInfo>> segregateTripInfoMap) {
		List<TripInfo> consolidatedTripInfoList = new ArrayList<>();
		if (!segregateTripInfoMap.isEmpty())
			makeCombinations(segregateTripInfoMap, 0, new ArrayList<>(), consolidatedTripInfoList);
		return consolidatedTripInfoList;
	}

	private void makeCombinations(Map<String, List<TripInfo>> segregateTripInfoMap, int index,
			ArrayList<TripInfo> tempArrayList, List<TripInfo> consolidatedTripInfoList) {
		if (index >= segregateTripInfoMap.size()) {
			if (tempArrayList.size() == 1 || combinationsPossible(tempArrayList)) {
				consolidatedTripInfoList.add(makeCombinations(new ArrayList<>(tempArrayList)));
			}
			return;
		}
		List<TripInfo> tripInfoList = (List<TripInfo>) segregateTripInfoMap.values().toArray()[index];
		if (CollectionUtils.isNotEmpty(tripInfoList)) {
			for (int i = 0; i < tripInfoList.size(); i++) {
				tempArrayList.add(tripInfoList.get(i));
				makeCombinations(segregateTripInfoMap, index + 1, tempArrayList, consolidatedTripInfoList);
				tempArrayList.remove(tempArrayList.size() - 1);
			}
		}
	}

	private TripInfo makeCombinations(ArrayList<TripInfo> tempArrayList) {
		TripInfo tripInfo = new TripInfo();
		for (int index = 0; index < tempArrayList.size(); index++) {
			TripInfo tripInfoItem = new GsonMapper<>(tempArrayList.get(index), TripInfo.class).convert();
			if (index != 0) {
				// setting base fares, tax in segment zero only
				tripInfoItem.getSegmentInfos().forEach(segmentInfo -> {
					if (segmentInfo.getSegmentNum() == 0) {
						resetFareComponents(segmentInfo);
					}
				});
			}
			tripInfo.getSegmentInfos().addAll(tripInfoItem.getSegmentInfos());
		}
		return tripInfo;
	}

	private void resetFareComponents(SegmentInfo segmentInfo) {
		Map<PaxType, FareDetail> fareDetailsForPriceInfo = segmentInfo.getFareDetailsForPriceInfo(0);
		for (Map.Entry<PaxType, FareDetail> entry : fareDetailsForPriceInfo.entrySet()) {
			FareDetail fareDetail = entry.getValue();
			fareDetail.setFareComponents(new HashMap<FareComponent, Double>());
		}
	}

	private boolean combinationsPossible(ArrayList<TripInfo> tempArrayList) {
		for (int index = 0; index < tempArrayList.size() - 1; index++) {
			TripInfo currentTripInfo = tempArrayList.get(index);
			TripInfo nextTripInfo = tempArrayList.get(index + 1);
			if (!(nextTripInfo.getDepartureTime().compareTo(currentTripInfo.getArrivalTime()) > 0)) {
				return false;
			}
		}
		if (!checkBookingCounts(tempArrayList)) {
			return false;
		}

		// if (!checkBaggageAllowance(tempArrayList)) {
		// return false;
		// }

		return true;
	}

	private boolean checkBaggageAllowance(ArrayList<TripInfo> tempArrayList) {
		SegmentInfo segment = null;
		try {
			BaggageInfo baggageInfo = tempArrayList.get(0).getSegmentInfos().get(0).getPriceInfoList().get(0)
					.getFareDetail(PaxType.ADULT).getBaggageInfo();
			if (StringUtils.isNotBlank(baggageInfo.getAllowance())) {
				int baggageSize = Integer.valueOf(baggageInfo.getAllowance().split(" ")[0]);
				for (int index = 1; index < tempArrayList.size(); index++) {
					for (int segmentIndex = 0; segmentIndex < tempArrayList.get(index).getSegmentInfos()
							.size(); segmentIndex++) {
						segment = tempArrayList.get(index).getSegmentInfos().get(segmentIndex);
						baggageInfo = segment.getPriceInfo(0).getFareDetail(PaxType.ADULT).getBaggageInfo();
						if (Integer.valueOf(baggageInfo.getAllowance().split(" ")[0]) < baggageSize) {
							return false;
						}
					}
				}
			}
		} catch (Exception e) {
			log.debug("Bagagge allowance failed {} for the segment{} for segmentKey {} excep ",
					searchQuery.getSearchId(), segment, segment.getPriceInfo(0).getMiscInfo().getSegmentKey(), e);
		}
		return true;
	}

	private boolean checkBookingCounts(ArrayList<TripInfo> tempArrayList) {
		for (TripInfo tripInfo : tempArrayList) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				for (Map.Entry<PaxType, FareDetail> entry : segmentInfo.getPriceInfoList().get(0).getFareDetails()
						.entrySet()) {
					FareDetail fareDetail = entry.getValue();
					if (fareDetail.getSeatRemaining() <= 0) {
						return false;
					}
				}
			}
		}
		return true;
	}

	private void splitSROnSearchQuery(List<TripInfo> tripInfoList) {
		List<TripInfo> onwardList = new ArrayList<>();
		List<TripInfo> returnList = new ArrayList<>();
		tripInfoList.forEach(trip -> {
			boolean isReturnSegment = false;
			for (SegmentInfo segmentInfo : trip.getSegmentInfos()) {
				if (BooleanUtils.isTrue(segmentInfo.isReturnSegment)) {
					isReturnSegment = true;
				}
			}
			if (isReturnSegment) {
				returnList.add(trip);
			} else {
				onwardList.add(trip);
			}
		});

		if (searchQuery.isIntlReturn()) {
			setTripInfo(tripInfoList, TripInfoType.COMBO.name());
		} else {
			setTripInfo(onwardList, TripInfoType.ONWARD.name());
			setTripInfo(returnList, TripInfoType.RETURN.name());
		}
	}

	private void setTripInfo(List<TripInfo> tripInfoList, String tripType) {
		/*
		 * In domestic return split search, search query will have ONEWAY as searchtype and RETURN as original search
		 * type. In such case trips should be placed under RETURN tripType
		 */
		if (SearchType.ONEWAY.equals(searchQuery.getSearchType())
				&& SearchType.RETURN.equals(searchQuery.getOrigSearchType())) {
			tripType = TripInfoType.RETURN.name();
		}
		if (org.apache.commons.collections.CollectionUtils
				.isNotEmpty(searchResult.getTripInfos().getOrDefault(tripType, new ArrayList<>()))) {
			List<TripInfo> tempTripInfoList = searchResult.getTripInfos().get(tripType);
			tempTripInfoList.addAll(tripInfoList);
			searchResult.getTripInfos().put(tripType, tempTripInfoList);
		} else {
			searchResult.getTripInfos().put(tripType, tripInfoList);
		}
	}

	private List<SegmentInfo> buildSegments(Option_type0 option, AirPricePoint_type0 airPricePoint) {
		List<SegmentInfo> segmentInfos = new ArrayList<>();
		try {
			int segmentNumber = 0;
			for (BookingInfo_type0 bookingInfo : option.getBookingInfo()) {
				TypeBaseAirSegment airSegment = airSegementReferenceMap.get(bookingInfo.getSegmentRef().getTypeRef());
				for (int legIndex = 0; legIndex < airSegment.getFlightDetailsRef().length; legIndex++) {
					SegmentInfo segmentInfo = parseSegmentInfo(airPricePoint, segmentNumber, legIndex, bookingInfo);
					segmentInfos.add(segmentInfo);
					segmentNumber++;
				}
			}
		} catch (Exception e) {
			log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
		}
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			return segmentInfos;
		}
		return null;
	}

	private SegmentInfo parseSegmentInfo(AirPricePoint_type0 airPricePoint, int segmentNumber, int legIndex,
			BookingInfo_type0 bookingInfo) {
		TypeBaseAirSegment airSegment = airSegementReferenceMap.get(bookingInfo.getSegmentRef().getTypeRef());
		FlightDetails_type0 flightDetails =
				flightDetailsMap.get(airSegment.getFlightDetailsRef()[legIndex].getKey().getTypeRef());
		AirlineInfo marketingAirline = null;
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setSegmentNum(segmentNumber);
		segmentInfo.setFlightDesignator(buildFlightDesignator(airSegment));
		marketingAirline = segmentInfo.getFlightDesignator().getAirlineInfo();
		AirlineInfo operatingAirline = TravelPortUtils.getOperatingCarrier(airSegment.getCodeshareInfo());
		if (operatingAirline != null && !operatingAirline.getCode().equals(marketingAirline.getCode())) {
			segmentInfo.setOperatedByAirlineInfo(operatingAirline);
		}
		// segmentInfo.setStops(airSegment.getNumberOfStops());
		segmentInfo.setDuration(Long.valueOf(flightDetails.getFlightTime().toString()));
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(flightDetails.getOrigin().getTypeIATACode()));
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(flightDetails.getDestination().getTypeIATACode()));
		segmentInfo.setDepartTime(TravelPortUtils.getIsoDateTime(flightDetails.getDepartureTime()));
		segmentInfo.setArrivalTime(TravelPortUtils.getIsoDateTime(flightDetails.getArrivalTime()));
		if (flightDetails.getOriginTerminal() != null) {
			String departureTerminal = AirUtils.getTerminalInfo(flightDetails.getOriginTerminal());
			segmentInfo.getDepartAirportInfo().setTerminal(departureTerminal);
		}
		if (flightDetails.getDestinationTerminal() != null) {
			String arrivalTerminal = AirUtils.getTerminalInfo(flightDetails.getDestinationTerminal());
			segmentInfo.getArrivalAirportInfo().setTerminal(arrivalTerminal);
		}
		// no more stop over airport logic
		// segmentInfo.setStopOverAirports(getStopOverAirports(airSegment,
		// segmentInfo));
		if (searchQuery.isReturn() && airSegment.getGroup() == 1) {
			segmentInfo.setIsReturnSegment(true);
		}
		PriceInfo priceInfo = getPriceInfo(legIndex, segmentNumber, airPricePoint, bookingInfo);
		if (priceInfo != null) {
			segmentInfo.getPriceInfoList().add(priceInfo);
		}
		return segmentInfo;
	}

	private PriceInfo getPriceInfo(Integer legIndex, int segmentNumber, AirPricePoint_type0 pricePoint,
			BookingInfo_type0 bookingInfo) {
		TypeBaseAirSegment airSegment = airSegementReferenceMap.get(bookingInfo.getSegmentRef().getTypeRef());
		AirPricingInfo_type0 pricingInfo_type0 = pricePoint.getAirPricingInfo()[0];
		PriceInfo pInfo = PriceInfo.builder(searchQuery.getRequestId()).build();
		pInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		PriceMiscInfo miscInfo = pInfo.getMiscInfo();
		Map<PaxType, FareDetail> fareDetailMap = pInfo.getFareDetails();
		miscInfo.setTraceId(traceId);
		String plattingCarrier = pricingInfo_type0.getPlatingCarrier().getTypeCarrier();
		if (StringUtils.isNotBlank(providerCode)) {
			miscInfo.setProviderCode(providerCode);
		}
		if (StringUtils.isNotBlank(plattingCarrier)) {
			miscInfo.setPlatingCarrier(AirlineHelper.getAirlineInfo(plattingCarrier));
		}
		miscInfo.setJourneyKey(airSegment.getGroup() + "");
		miscInfo.setLegNum(legIndex);
		if (legIndex == 0) {
			miscInfo.setSegmentKey(airSegment.getKey().getTypeRef());
		}
		FareInfo_type0 fareInfo = fareInfoMap.get(bookingInfo.getFareInfoRef().getTypeRef());
		setFareRuleKeys(miscInfo, fareInfo);
		if (fareInfo.getPrivateFare() != null && StringUtils.isNotBlank(fareInfo.getPrivateFare().getValue())) {
			miscInfo.setFareIndicator(fareInfo.getPrivateFare().toString());
		}
		if (airSegment.getAvailabilitySource() != null) {
			miscInfo.setAvailablitySource(airSegment.getAvailabilitySource().getTypeAvailabilitySource());
		}
		miscInfo.setParticipationLevel(airSegment.getParticipantLevel());
		miscInfo.setPolledAvailabilityOption(airSegment.getPolledAvailabilityOption());
		miscInfo.setAvailabilityDisplayType(airSegment.getAvailabilityDisplayType());
		miscInfo.setLinkavailablity(airSegment.getLinkAvailability());
		setFareIdentifier(fareInfo, pInfo);
		for (AirPricingInfo_type0 airPricingInfo : pricePoint.getAirPricingInfo()) {
			PassengerType_type0 passengerType = airPricingInfo.getPassengerType()[0];
			PaxType paxType = TravelPortPaxType.getPaxType(passengerType.getCode().getTypePTC());
			FareDetail fareDetail = getFareDetails(airPricingInfo, bookingInfo, segmentNumber, pricePoint, pInfo);
			fareDetailMap.put(paxType, fareDetail);
		}
		pInfo.setFareDetails(fareDetailMap);
		pInfo.setMiscInfo(miscInfo);
		return pInfo;
	}

	private FareDetail getFareDetails(AirPricingInfo_type0 airPricingInfo, BookingInfo_type0 bookingInfo,
			int segmentNumber, AirPricePoint_type0 airPricePoint, PriceInfo pInfo) {
		FareInfo_type0 fareInfo = fareInfoMap.get(airPricingInfo.getFareInfoRef()[0].getKey().getTypeRef());
		double dividingFactor = 1.0;
		if (searchQuery.isDomesticReturn() && airPricePoint.getCompleteItinerary()) {
			dividingFactor = 2.0;
			if (isSpecialAccCodeApplied) {
				pInfo.setFareIdentifier(FareType.MARINE);
			} else {
				pInfo.setFareIdentifier(FareType.SPECIAL_RETURN);
			}
		}
		pInfo.getMiscInfo().setEffectiveDate(fareInfo.getEffectiveDate());
		FareDetail fareDetail = new FareDetail();
		fareDetail.setCabinClass(TravelPortUtils.getCabinClass(bookingInfo));
		fareDetail.setSeatRemaining(Integer.valueOf(bookingInfo.getBookingCount()));
		fareDetail.setClassOfBooking(bookingInfo.getBookingCode());
		// fareDetail.setFareType(TravelPortUtils.getFareType(fareInfo));
		fareDetail.setFareBasis(fareInfo.getFareBasis());
		fareDetail.setRefundableType(TravelPortUtils.getRefundableType(airPricingInfo));
		if (segmentNumber == 0) {
			Double baseFare = getEquivalentBaseFare(airPricingInfo, dividingFactor);
			Double totalFare = getEquivalentTotalfare(airPricingInfo, dividingFactor);
			fareDetail.getFareComponents().put(FareComponent.BF, baseFare);
			fareDetail.getFareComponents().put(FareComponent.TF, totalFare);
			if (Objects.nonNull(airPricingInfo.getTaxInfo())) {
				setTaxDetails(fareDetail, airPricingInfo.getTaxInfo(), dividingFactor);
			}
		}
		TravelPortUtils.setBaggageAllowance(fareDetail, fareInfo);
		return fareDetail;
	}

	private FlightDesignator buildFlightDesignator(TypeBaseAirSegment airSegment) {
		FlightDesignator flightDesignator = FlightDesignator.builder().build();
		flightDesignator.setFlightNumber(airSegment.getFlightNumber().getTypeFlightNumber().trim());
		flightDesignator.setEquipType(airSegment.getEquipment().getTypeEquipment());
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(airSegment.getCarrier().getTypeCarrier()));
		return flightDesignator;
	}

	protected void buildAirLegModifiers(SearchAirLeg_type0 searchAirLeg) {
		AirLegModifiers_type0 airLegModifiers = new AirLegModifiers_type0();
		String cabinCode = TravelPortUtils.getCabinCode(configuration, searchQuery);

		Boolean usePreffedCabin =
				sourceConfiguration != null ? BooleanUtils.isTrue(sourceConfiguration.getUsePrefferedCabin()) : false;

		if (BooleanUtils.isTrue(usePreffedCabin)) {

			if (!TravelPortConstants.ECONOMY.equals(cabinCode)) {
				PreferredCabins_type0 preferredCabins = new PreferredCabins_type0();
				CabinClass_type0 cabinClass = new CabinClass_type0();
				cabinClass.setType(cabinCode);
				preferredCabins.setCabinClass(cabinClass);
				/**
				 * Preferred Cabin class is used when a customer first preference is the cabin class passed in the
				 * request . If that cabin class is not available then by default behaviour is to downgrade the cabin
				 * class. For Tripjack , Travelport team has configured upgrade cabin class configuration.
				 */
				airLegModifiers.setPreferredCabins(preferredCabins);
			}

		} else {
			PermittedCabins_type0 permittedCabins = new PermittedCabins_type0();
			CabinClass_type0 cabinClass = new CabinClass_type0();
			cabinClass.setType(cabinCode);
			permittedCabins.addCabinClass(cabinClass);
			/*
			 * Permitted Cabin class should be used when only that cabin class is required. In that cabin class is not
			 * available then nothing will be returned
			 */
			airLegModifiers.setPermittedCabins(permittedCabins);

		}


		searchAirLeg.setAirLegModifiers(airLegModifiers);
	}

}
