package com.tgs.services.fms.utils;

import com.google.common.collect.Lists;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.CityInfo;
import com.tgs.services.base.datamodel.ContactInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.LogDataVisibilityGroup;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.AirSearchModifiers;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.SupplierFlowType;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceClientInfo;
import com.tgs.services.fms.datamodel.airconfigurator.NetRemittanceOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class AirSupplierUtils {

	private static UserServiceCommunicator userSrvComm;

	public static void init(UserServiceCommunicator usrSrvComm) {
		userSrvComm = usrSrvComm;
	}

	public static final String CONTACT_PREFIX = "+";

	private static final List<String> GST_MANDATORY_FARE_TYPES =
			Arrays.asList(FareType.CORPORATE_FLEX.getName(), FareType.SME.getName());

	public static String removeIllegalCharacters(String text) {
		return text.replaceAll("[^a-zA-Z0-9!#$%&@'*+-/=?^_`{|}~.]+", "");
	}

	public static String getEmailId(DeliveryInfo deliveryInfo) {
		if (deliveryInfo == null || CollectionUtils.isEmpty(deliveryInfo.getEmails())) {
			return null;
		}
		String emailId = deliveryInfo.getEmails().get(0);
		return removeIllegalCharacters(emailId);
	}

	public static String getEmailId(ContactInfo contactInfo) {
		if (contactInfo == null || CollectionUtils.isEmpty(contactInfo.getEmails())) {
			return null;
		}
		String emailId = contactInfo.getEmails().get(0);
		return removeIllegalCharacters(emailId);
	}

	public static String getContactNumber(DeliveryInfo deliveryInfo) {
		if (deliveryInfo == null || CollectionUtils.isEmpty(deliveryInfo.getContacts())) {
			return null;
		}
		String contactNumber = deliveryInfo.getContacts().get(0);
		return contactNumber.replaceAll("[^0-9]", "");
	}

	public static String getContactNumberWithCountryCode(String contactNumber) {
		String countryCode = ServiceCommunicatorHelper.getClientInfo().getCountryCode();
		if (contactNumber.startsWith(StringUtils.join(CONTACT_PREFIX, countryCode))) {
			// contact number has info with proper country code
			contactNumber = contactNumber.replace(CONTACT_PREFIX, "");
			return contactNumber.replaceAll("\\s+", "");
		}
		if (!contactNumber.startsWith(countryCode)) {
			contactNumber = StringUtils.join(countryCode, contactNumber);
		}
		if (contactNumber.startsWith(CONTACT_PREFIX)) {
			contactNumber = contactNumber.replace(CONTACT_PREFIX, "");
		}
		// if (!contactNumber.startsWith(CONTACT_PREFIX)) {
		// contactNumber = StringUtils.join(CONTACT_PREFIX, contactNumber);
		// }
		return contactNumber.replaceAll("\\s+", "");
	}

	public static String getContactNumberWithCountryCode(DeliveryInfo deliveryInfo) {
		if (deliveryInfo == null || CollectionUtils.isEmpty(deliveryInfo.getContacts())) {
			return null;
		}
		return getContactNumberWithCountryCode(deliveryInfo.getContacts().get(0));
	}

	public static String getContactNumberWithCountryCode(ContactInfo contactInfo) {
		if (contactInfo == null || CollectionUtils.isEmpty(contactInfo.getContacts())) {
			return null;
		}
		return getContactNumberWithCountryCode(contactInfo.getContacts().get(0));
	}


	public static String getCompanyName(User bookingUser, ClientGeneralInfo clientInfo,
			SupplierConfiguration configuration) {

		AirGeneralPurposeOutput generalPurposeOutput =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		AirSourceClientInfo sourceClientInfo =
				MapUtils.getObject(generalPurposeOutput.getSourceWiseClientInfo(), configuration.getSourceId(), null);

		if (sourceClientInfo != null) {
			return sourceClientInfo.getCompanyName();
		} else if (StringUtils.isNotBlank(bookingUser.getName())) {
			return bookingUser.getName();
		}
		return clientInfo.getCompanyName();
	}

	/**
	 * This will filter critical messages which we are getting frequently from the supplier
	 * 
	 * @param criticalMessages
	 * @return
	 */
	public static List<String> filterCriticalMessage(List<String> criticalMessages, boolean isSuccess, User user) {
		if (isSuccess && CollectionUtils.isNotEmpty(criticalMessages)) {
			AirGeneralPurposeOutput generalOutput =
					AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
			if (CollectionUtils.isNotEmpty(generalOutput.getCriticalLoggerExclusions())) {
				Iterator<String> criticalMessageIterator = criticalMessages.iterator();
				List<String> exclusions = generalOutput.getCriticalLoggerExclusions();
				while (criticalMessageIterator.hasNext()) {
					String criticalMessage = criticalMessageIterator.next();

					if (exclusions.contains(criticalMessage.trim())) {
						criticalMessageIterator.remove();
					}
				}
			}
		}
		return criticalMessages;
	}

	public static void addToAnalytics(List<SupplierAnalyticsQuery> analyticsInfos, long timeToCreate, String stubName) {
		if (analyticsInfos != null) {
			SupplierAnalyticsQuery stubQuery = SupplierAnalyticsQuery.builder()
					.flowtype(SupplierFlowType.STUB_CREATION.name())
					.searchtimeinsec(Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(timeToCreate)).intValue()).build();
			stubQuery.setStubname(stubName);
			stubQuery.setGenerationtime(LocalDateTime.now());
			stubQuery.setCreatedtime(LocalDateTime.now().toString());
			analyticsInfos.add(stubQuery);
		}
	}

	public static void isStoreLogRequired(AirSearchQuery searchQuery) {
		// Temp code to disable search log
		if (!UserUtils.isMidOfficeRole(
				UserUtils.getEmulatedUserRoleOrUserRoleCode(SystemContextHolder.getContextData().getUser()))) {
			if (searchQuery.getSearchModifiers() == null) {
				searchQuery.setSearchModifiers(new AirSearchModifiers());
			}
			searchQuery.getSearchModifiers().setStoreSearchLog(Boolean.FALSE);
		}
	}

	public static Proxy getProxy(User user) {
		Proxy proxy = null;
		AirGeneralPurposeOutput configuratorInfo =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
		if (configuratorInfo != null && StringUtils.isNotBlank(configuratorInfo.getProxyAddress())) {
			String proxyAddress = configuratorInfo.getProxyAddress();
			proxy = new java.net.Proxy(java.net.Proxy.Type.HTTP,
					new InetSocketAddress(proxyAddress.split(":")[0], Integer.valueOf(proxyAddress.split(":")[1])));
		}
		return proxy;
	}

	public static String getProxyAddress(User user, SupplierConfiguration configuration) {
		AirGeneralPurposeOutput configuratorInfo =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
		String proxy = null;
		if (configuratorInfo != null) {
			Map<Integer, String> sourceWiseProxy = configuratorInfo.getSourceWiseProxyAddress();
			if (org.apache.commons.collections.MapUtils.isNotEmpty(sourceWiseProxy))
				proxy = sourceWiseProxy.get(configuration.getSourceId());
			else
				proxy = configuratorInfo.getProxyAddress();
		}
		return proxy;
	}

	public static boolean isGSTMandatoryFareType(TripInfo trip) {
		Set<String> fareTypes = trip.getFareType(0);
		trip.getSegmentInfos().forEach(segmentInfo -> {
			fareTypes.add(segmentInfo.getPriceInfo(0).getOriginalFareType());
		});
		Optional<String> allowedTypes =
				fareTypes.stream().filter(ft -> GST_MANDATORY_FARE_TYPES.contains(ft.toUpperCase())).findFirst();
		return allowedTypes.isPresent();
	}

	public static DeliveryInfo getDeliveryDetails(DeliveryInfo customerDeliveryInfo) {
		ClientGeneralInfo clientInfo = ServiceCommunicatorHelper.getClientInfo();
		if (BooleanUtils.isTrue(clientInfo.getUseClientDeliveryInfo())) {
			DeliveryInfo deliveryInfo = new DeliveryInfo();
			deliveryInfo.setContacts(Arrays.asList(clientInfo.getWorkPhone()));
			deliveryInfo.setEmails(Arrays.asList(clientInfo.getEmail()));
			return deliveryInfo;
		}
		return customerDeliveryInfo;
	}

	public static List<String> getLogVisibility() {
		return new ArrayList<>(Lists.newArrayList(LogDataVisibilityGroup.DEVELOPER.name()));
	}

	public static String getAirlinePromotionCode(IFact fact) {
		NetRemittanceOutput configuratorRule =
				AirConfiguratorHelper.getAirConfigRule(fact, AirConfiguratorRuleType.NETREMITTANCE);
		if (Objects.nonNull(configuratorRule)) {
			return configuratorRule.getNetRemittanceCode();
		}
		return null;
	}

	public static AddressInfo getContactAddressInfo(String bookingUserId, ClientGeneralInfo clientInfo,
			SupplierConfiguration supplierConfiguration) {
		AddressInfo addressInfo = AddressInfo.builder().build();
		User bookingUser = userSrvComm.getUserFromCache(bookingUserId);
		AirGeneralPurposeOutput generalPurposeOutput =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		AirSourceClientInfo sourceClientInfo = MapUtils.getObject(generalPurposeOutput.getSourceWiseClientInfo(),
				supplierConfiguration.getSourceId(), null);
		if (!BooleanUtils.isTrue(clientInfo.getUseClientAddress()) && Objects.isNull(sourceClientInfo)
				&& bookingUser.getAddressInfo() != null
				&& StringUtils.isNotEmpty(bookingUser.getAddressInfo().getAddress())
				&& StringUtils.isNotEmpty(bookingUser.getAddressInfo().getPincode())
				&& bookingUser.getAddressInfo().getCityInfo() != null
				&& StringUtils.isNotEmpty(bookingUser.getAddressInfo().getCityInfo().getName())
				&& StringUtils.isNotEmpty(bookingUser.getAddressInfo().getCityInfo().getCountry())
				&& StringUtils.isNotEmpty(bookingUser.getAddressInfo().getCityInfo().getState())) {
			addressInfo = bookingUser.getAddressInfo();
		} else if (Objects.nonNull(sourceClientInfo)) {
			addressInfo.setAddress(sourceClientInfo.getAddress());
			CityInfo cityInfo = CityInfo.builder().name(sourceClientInfo.getCity())
					.country(sourceClientInfo.getCountry()).state(sourceClientInfo.getState()).build();
			addressInfo.setCityInfo(cityInfo);
			addressInfo.setPincode(sourceClientInfo.getPostalCode());
		} else {
			addressInfo.setAddress(StringUtils.join(clientInfo.getAddress1(), clientInfo.getAddress2()));
			CityInfo cityInfo = CityInfo.builder().name(clientInfo.getCity()).country(clientInfo.getCountry())
					.state(clientInfo.getState()).build();
			addressInfo.setCityInfo(cityInfo);
			addressInfo.setPincode(clientInfo.getPostalCode());
		}
		return addressInfo;
	}

	/**
	 * If connecting time between segments is more than 1440 minutes (24 hrs) then it is considered as new trip.
	 */
	public static List<TripInfo> getGDSImportTripInfos(List<SegmentInfo> segmentInfos, User bookingUser) {
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			List<TripInfo> tripInfos = new ArrayList<>();
			TripInfo tripInfo = new TripInfo();
			int segmentNum = 0;
			for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
				SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
				segmentInfo.setSegmentNum(segmentNum);
				if (segmentIndex + 1 == segmentInfos.size()
						|| AirUtils.getConnectingTime(segmentInfo, segmentInfos.get(segmentIndex + 1)) > 1440) {
					tripInfo.getSegmentInfos().add(segmentInfo);
					tripInfos.add(tripInfo);
					tripInfo = new TripInfo();
					segmentNum = 0;
				} else {
					tripInfo.getSegmentInfos().add(segmentInfo);
					segmentNum++;
				}
			}

			if (CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
				tripInfos.add(tripInfo);
			}

			// To set return segment
			if (setIsReturnSegment(tripInfos, bookingUser)) {
				tripInfos.get(1).getSegmentInfos().forEach(segment -> {
					segment.setIsReturnSegment(true);
				});
			}
			return tripInfos;
		}
		return null;
	}

	public static boolean setIsReturnSegment(List<TripInfo> tripInfos, User bookingUser) {
		if (CollectionUtils.isNotEmpty(tripInfos) && tripInfos.size() == 2) {
			TripInfo onwardTrip = tripInfos.get(0);
			TripInfo returnTrip = tripInfos.get(1);
			String onwardDepartureAirport = onwardTrip.getDepartureAirportCode();
			String onwardArrivalAirport = onwardTrip.getArrivalAirportCode();
			String returnDepartureAirport = returnTrip.getDepartureAirportCode();
			String retunrArrivalAirport = returnTrip.getArrivalAirportCode();
			if ((onwardDepartureAirport.equals(retunrArrivalAirport)
					|| AirUtils.isGNNearByAirport(onwardDepartureAirport, onwardTrip.getSupplierInfo().getSourceId(),
							null, bookingUser)
					|| AirUtils.isGNNearByAirport(retunrArrivalAirport, onwardTrip.getSupplierInfo().getSourceId(),
							null, bookingUser))
					&& (onwardArrivalAirport.equals(returnDepartureAirport)
							|| AirUtils.isGNNearByAirport(onwardArrivalAirport,
									onwardTrip.getSupplierInfo().getSourceId(), null, bookingUser)
							|| AirUtils.isGNNearByAirport(returnDepartureAirport,
									onwardTrip.getSupplierInfo().getSourceId(), null, bookingUser))) {
				return true;
			}
		}
		return false;
	}

}
