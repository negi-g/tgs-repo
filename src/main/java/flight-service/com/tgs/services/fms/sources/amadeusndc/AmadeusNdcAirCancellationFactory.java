package com.tgs.services.fms.sources.amadeusndc;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.lang3.StringUtils;
import org.iata.iata._2015._00._2018_1.orderviewrs.ErrorType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderItemType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderViewRS;
import org.iata.iata._2015._00._2018_1.orderviewrs.PaxListType;
import org.iata.iata._2015._00._2018_1.orderviewrs.PaxType;
import org.iata.iata._2015._00._2018_1.orderviewrs.ServiceType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderViewRS.Errors;
import org.springframework.stereotype.Service;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.amadeus.wsdl.altea_ndc_18_1_v1_v4.AlteaNDC181Service;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.exception.air.CancellationException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AmadeusNdcAirCancellationFactory extends AbstractAirCancellationFactory {

	protected AmadeusNdcBindingService bindingService;

	private static AlteaNDC181Service service = null;

	protected AmadeusNdcReviewManager reviewManager;

	private static AlteaNDC181PT servicePort;

	private String toCurrency;

	static {
		// keeping static block to load class before warm up
		log.debug("AmadeusNdcAirCancellationFactory Starting creating static port service .....");
		service = new AlteaNDC181Service();
		servicePort = service.getAlteaNDC181Port();
		log.debug("AmadeusNdcAirCancellationFactory completed creating static port service .....");
	}

	public AmadeusNdcAirCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		super(bookingSegments, order, supplierConf);
		toCurrency = AirSourceConstants.getClientCurrencyCode();
	}

	@Override
	public boolean releaseHoldPNR() {
		log.debug("NDC release Hold PNR started for bookingID {}", bookingId);
		boolean isAbortSuccess = false;
		boolean isOrderValid = false;
		boolean isPaymentDone = false;
		AmadeusNdcBookingRetrieveManager retrieveManager = AmadeusNdcBookingRetrieveManager.builder()
				.supplierConf(supplierConf).service(service).bookingId(bookingId).toCurrency(toCurrency)
				.moneyExchangeComm(moneyExchangeComm).supplierPNR(bookingSegments.getSupplierBookingId()).build();

		OrderViewRS orderViewRs = retrieveManager.orderRetrieve();
		if (!retrieveManager.isAnyError(orderViewRs)) {
			isOrderValid = true;
		}

		isPaymentDone = CollectionUtils.isNotEmpty(orderViewRs.getResponse().getOrder().get(0).getPaymentInfo());
		if (!isPaymentDone && checkPaxStatusCode(orderViewRs) && isOrderValid) {
			AmadeusNdcCancellationManager cancellationManager = AmadeusNdcCancellationManager.builder()
					.supplierConf(supplierConf).moneyExchangeComm(moneyExchangeComm).service(service)
					.bookingId(bookingId).supplierPNR(bookingSegments.getSupplierBookingId()).build();
			boolean isCancelSuccess = cancellationManager.orderCancel();
			boolean isOrderReleased = isOrderReleased(bookingSegments.getSupplierBookingId(), retrieveManager);
			if (isCancelSuccess && isOrderReleased) {
				log.debug("Release Hold PNR successful for bookingID {}", bookingId);
				isAbortSuccess = true;
			}
		}
		return isAbortSuccess;
	}


	@Override
	public AirCancellationDetail reviewBookingForCancellation(AirCancellationDetail cancellationDetail) {
		log.debug("Ndc OrderReshop started for bookingID {}", bookingId);
		try {
			AmadeusNdcBookingRetrieveManager retrieveManager = AmadeusNdcBookingRetrieveManager.builder()
					.supplierConf(supplierConf).service(service).bookingId(bookingId).toCurrency(toCurrency)
					.moneyExchangeComm(moneyExchangeComm).supplierPNR(bookingSegments.getSupplierBookingId()).build();
			OrderViewRS orderViewRs = retrieveManager.orderRetrieve();
			List<PaxType> paxList = orderViewRs.getResponse().getDataLists().getPaxList().getPax();

			BidiMap<String, String> bidiPaxMap = new DualHashBidiMap<>();
			populateOrderRetrievePaxMap(paxList, bidiPaxMap);
			OrderType orderType = orderViewRs.getResponse().getOrder().get(0);

			AmadeusNdcReshopManager reshopManager =
					AmadeusNdcReshopManager.builder().service(service).orderItemRefId(orderType.getOrderID())
							.orderItemAIRTypeId(orderType.getOrderItem().get(0).getOrderItemID()).bidiPaxMap(bidiPaxMap)
							.bookingSegments(bookingSegments).moneyExchangeComm(moneyExchangeComm).bookingId(bookingId)
							.supplierConf(supplierConf).build();
			reshopManager.orderReshop();
			cancellationDetail.setAutoCancellationAllowed(Boolean.TRUE);

		} finally {
			log.debug("Cancellation(OrderReshop) Review completed for bookingID {}", bookingId);
		}
		return cancellationDetail;
	}

	@Override
	public boolean confirmBookingForCancellation() {
		boolean isCancelled = false;
		String childPnr = null;
		try {
			String supplierPnr = bookingSegments.getSupplierBookingId();
			AmadeusNdcBookingRetrieveManager retrieveManager = AmadeusNdcBookingRetrieveManager.builder()
					.supplierConf(supplierConf).service(service).bookingId(bookingId).toCurrency(toCurrency)
					.moneyExchangeComm(moneyExchangeComm).supplierConf(supplierConf).supplierPNR(supplierPnr).build();

			OrderViewRS orderViewRs = retrieveManager.orderRetrieve();
			if (!retrieveManager.isAnyError(orderViewRs)) {
				PaxListType paxList = orderViewRs.getResponse().getDataLists().getPaxList();
				String airlinePNR = orderViewRs.getResponse().getOrder().get(0).getBookingRef().get(0).getBookingID();
				if (isCompleteCancellation()) {
					AmadeusNdcCancellationManager cancellationManager = AmadeusNdcCancellationManager.builder()
							.supplierConf(supplierConf).moneyExchangeComm(moneyExchangeComm).service(service)
							.bookingId(bookingId).supplierPNR(supplierPnr).build();
					isCancelled = cancellationManager.orderCancel();
				} else {
					AmadeusNdcOrderSplitManager orderSplitManager = AmadeusNdcOrderSplitManager.builder()
							.bookingSegments(bookingSegments).airlinePNR(airlinePNR).supplierConf(supplierConf)
							.supplierPNR(supplierPnr).service(service).paxList(paxList).build();
					orderSplitManager.orderChange();
					childPnr = orderSplitManager.getChildPNR();
					// child Pnr is in format of GDSPnr (SQ_djf)
					if (StringUtils.isNotBlank(childPnr)) {
						log.debug("Cancelling NDC ChildPnr {}", childPnr);
						AmadeusNdcCancellationManager cancellationManager = AmadeusNdcCancellationManager.builder()
								.bookingId(bookingId).supplierConf(supplierConf).moneyExchangeComm(moneyExchangeComm)
								.service(service).supplierPNR(childPnr).build();
						isCancelled = cancellationManager.orderCancel();
						orderSplitManager.updateCancelledPaxPnr(childPnr);
					}
				}
			}
		} finally {
			if (!isCancelled) {
				String message = null;
				if (isCancelled && StringUtils.isNotBlank(childPnr)) {
					message = StringUtils.join("Divide PNR ", childPnr, " For Cancellation Travellers");
					criticalMessageLogger.add(message);
				} else if (!isCancelled) {
					message = StringUtils.join("Cancellation Failed for " + getSupplierConf().getSupplierId());
					criticalMessageLogger.add(message);
				}
				log.error("{} for bookingid {}", message, bookingId);
			}

		}
		return isCancelled;
	}

	private boolean isOrderReleased(String orderRefId, AmadeusNdcBookingRetrieveManager retrieveManager) {
		boolean cancellationStatus = false;
		if (StringUtils.isNotBlank(orderRefId)) {
			OrderViewRS orderCancelledRs = retrieveManager.orderRetrieve();
			if (CollectionUtils.isNotEmpty(orderCancelledRs.getErrors())) {
				for (Errors errors : orderCancelledRs.getErrors()) {
					for (ErrorType error : errors.getError()) {
						if (StringUtils.equals(error.getCode(), AmadeusNDCConstants.PNR_CANCELLED_CODE)
								&& StringUtils.equals(error.getDescText(), AmadeusNDCConstants.PNR_CANCELLED_MESSAGE)) {
							cancellationStatus = true;
							log.debug("Cross Check completed Successfully for order id {}", orderRefId);
							return cancellationStatus;
						}
					}
				}

			} else {
				throw new CancellationException("Order not cancelled , still able to OrderRetrieveRs");
			}
		}
		return cancellationStatus;
	}

	private boolean checkPaxStatusCode(OrderViewRS orderViewRs) {
		boolean isPaxOnHold = true;
		OrderItemType orderItemType = orderViewRs.getResponse().getOrder().get(0).getOrderItem().get(0);
		for (ServiceType serviceType : orderItemType.getService()) {
			if (!(StringUtils.equalsIgnoreCase(serviceType.getStatusCode(),
					AmadeusNDCConstants.PNR_HOLD_STATUS_CODE))) {
				isPaxOnHold = false;
				break;
			}
		}
		return isPaxOnHold;
	}

	private boolean isCompleteCancellation() {
		List<SegmentInfo> originalSegments = bookingSegments.getSegmentInfos();
		List<SegmentInfo> cancellationSegments = bookingSegments.getCancelSegments();
		SegmentInfo cancelledSegment = bookingSegments.getCancelSegments().get(0);
		SegmentInfo originalSegment = bookingSegments.getSegmentInfos().get(0);
		return cancellationSegments.size() == originalSegments.size()
				&& cancelledSegment.getTravellerInfo().size() == originalSegment.getTravellerInfo().size();
	}

	private void populateOrderRetrievePaxMap(List<PaxType> paxList, BidiMap<String, String> bidiPaxMap) {

		for (PaxType paxType : paxList) {
			String paxId = paxType.getPaxID();
			// converting PAX2 --> P2 for OrderReshop
			List<String> splitPaxId = Arrays.asList(paxId.split("AX"));
			String reshopRefId = String.join("", splitPaxId);
			String nameCombination =
					paxType.getIndividual().getGivenName().get(0) + "-" + paxType.getIndividual().getSurname();
			bidiPaxMap.put(reshopRefId, nameCombination);
		}
	}

	@Override
	public boolean isTravellerCancellationFeeZero(AirCancellationDetail cancellationDetail) {
		for (SegmentInfo segmentInfo : bookingSegments.getCancelSegments()) {
			if (segmentInfo.getSegmentNum().equals(0) && segmentInfo.getIsReturnSegment() == false) {
				for (FlightTravellerInfo flightTravellerInfo : segmentInfo.getTravellerInfo()) {
					if (!flightTravellerInfo.getPaxType().equals(com.tgs.services.base.enums.PaxType.INFANT)
							&& (flightTravellerInfo.getFareDetail().getFareComponents().get(FareComponent.ACF) == null
									|| flightTravellerInfo.getFareDetail().getFareComponents()
											.get(FareComponent.ACF) <= 0)) {
						log.error("Air Cancellation Fees is zero for this bookingId {}  for this passenger {}",
								bookingId, flightTravellerInfo);
						cancellationDetail.buildMessageInfo(bookingId, bookingSegments,
								"AirCancellation Fee is zero for this Passenger " + flightTravellerInfo.getFullName());
						cancellationDetail.setAutoCancellationAllowed(Boolean.FALSE);
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	protected boolean isPassengerWiseCancellationAllowed(Integer sourceId) {
		return true;
	}

}
