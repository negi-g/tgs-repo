package com.tgs.services.fms.sources.amadeusndc;

import lombok.Getter;
import com.tgs.services.base.enums.CabinClass;

@Getter
public enum AmadeusNdcCabinClass {

	ECO(CabinClass.ECONOMY, "ECO"),
	ECOPREMIUM(CabinClass.PREMIUM_ECONOMY, "PRE"),
	BUSINESS(CabinClass.BUSINESS, "BUS"),
	FIRST(CabinClass.FIRST, "FIR");

	private CabinClass cabinClass;

	private String code;

	private AmadeusNdcCabinClass(CabinClass cabinClass, String code) {
		this.code = code;
		this.cabinClass = cabinClass;
	}

	public static CabinClass getCabinClassFromCode(String cabinClassCode) {
		for (AmadeusNdcCabinClass amadeusCabinClass : AmadeusNdcCabinClass.values()) {
			if (amadeusCabinClass.getCode().equals(cabinClassCode)) {
				return amadeusCabinClass.getCabinClass();
			}
		}
		return null;
	}

	public static AmadeusNdcCabinClass getEnumFromCabinClass(CabinClass cabinClass) {
		for (AmadeusNdcCabinClass amadeusCabinClass : AmadeusNdcCabinClass.values()) {
			if (amadeusCabinClass.getCabinClass().equals(cabinClass)) {
				return amadeusCabinClass;
			}
		}
		return null;
	}
}
