package com.tgs.services.fms.sources.travelport.sessionless;

import org.springframework.stereotype.Service;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import travelport.www.schema.air_v47_0.AirPriceRsp;
import travelport.www.schema.universal_v47_0.UniversalRecordRetrieveRsp;
import travelport.www.service.air_v47_0.AirFaultMessage;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

@Slf4j
@Service
public class TravelPortSessionLessAirBookingFactory extends AbstractAirBookingFactory {

	private TravelPortSessionLessBindingService bindingService;

	protected TravelPortSessionLessBookingManager bookingManager;

	protected SoapRequestResponseListner listener;

	public TravelPortSessionLessAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments,
			Order order) throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	public void initialize() {
		bindingService = TravelPortSessionLessBindingService.builder().configuration(supplierConf).build();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		bookingManager = TravelPortSessionLessBookingManager.builder().configuration(supplierConf).order(order)
				.deliveryInfo(deliveryInfo).segmentInfos(bookingSegments.getSegmentInfos()).bookingId(bookingId)
				.bindingService(bindingService).listener(listener).sourceConfiguration(sourceConfiguration)
				.criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).gstInfo(gstInfo).build();
		if (BaseUtils
				.getParticularPaxTravellerInfo(bookingSegments.getSegmentInfos().get(0).getTravellerInfo(),
						PaxType.INFANT)
				.size() > 0 && TravelPortSessionLessUtils.isSeatSSrSelected(bookingSegments.getSegmentInfos())) {
			bookingManager.isMerchandiseCallRequired = false;
		}
		String traceId = bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		bookingManager.setTraceId(traceId);
	}

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;
		try {
			initialize();
			bookingManager.init(bookingSegments);
			bookingManager.setKeyRef(bookingSegments);
			AirPriceRsp rePriceRsp = bookingManager.sellwithSSR();
			if (rePriceRsp != null && rePriceRsp.getAirPriceResult() != null
					&& rePriceRsp.getAirPriceResult()[0].getAirPricingSolution() != null) {
				String totalPrice =
						rePriceRsp.getAirPriceResult()[0].getAirPricingSolution()[0].getTotalPrice().getTypeMoney();

				double amountPaybleToSupplier =
						bookingManager.getAmountBasedOnCurrency(totalPrice, totalPrice.substring(0, 3), 1.0);
				if (!isFareDiff(amountPaybleToSupplier)) {
					pnr = bookingManager.createPNR(isHoldBooking, getSupplierBookingCreditCard());
					if (bookingManager.isSeatSSRAssignmentFailed()) {
						isSuccess = false;
						return isSuccess;
					}
					isSuccess = checkIsSuccess();
					if (isSuccess && bookingManager.isMerchandiseCallRequired()) {
						isSuccess = bookingManager.addSeatSSRForBookingWithInfants(isHoldBooking,
								getSupplierBookingCreditCard()) && checkIsSuccess();
					}
				}
			}
		} finally {
			log.info("TravelPort Indigo Booking status for {} PNR {} is {}", bookingId, pnr, isSuccess);
		}
		return isSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isSuccess = false;
		initialize();
		bookingManager.init(bookingSegments);
		log.info("Retrieving UniversalRecord for BookingId {}  using Universal Record Locator {}", bookingId,
				bookingManager.universalPnr);
		UniversalRecordRetrieveRsp retreiveBooking = bookingManager.getUnivRecordRetrieveRS();
		if (retreiveBooking.getUniversalRecord() != null
				&& retreiveBooking.getUniversalRecord().getAirReservation() != null) {
			bookingManager.setAirlineCharges(retreiveBooking.getUniversalRecord().getAirReservation()[0]);
		}
		if (!isFareDiff(bookingManager.totalAirlineCharges) && TravelPortSessionLessConstants.TYPE
				.equalsIgnoreCase(retreiveBooking.getUniversalRecord().getStatus().getTypeURStatus())) {
			bookingManager.modifyRecordWithFOP(retreiveBooking, getSupplierBookingCreditCard());
			isSuccess = checkIsSuccess();
		}
		return isSuccess;
	}

	private boolean checkIsSuccess() {
		UniversalRecordRetrieveRsp retreive = bookingManager.getUnivRecordRetrieveRS();
		if (retreive.getUniversalRecord() != null && retreive.getUniversalRecord().getAirReservation() != null) {
			if (isHoldBooking && bookingManager.isSegmentStatus(retreive, TravelPortSessionLessConstants.PN)
					&& !bookingManager.isTicketedAction(retreive, TravelPortSessionLessConstants.TICKETED)) {
				log.info("BookingId {} with pnr {} is on HOLD", bookingId, pnr);
				return true;
			} else if (bookingManager.isTicketedAction(retreive, TravelPortSessionLessConstants.TICKETED)
					&& bookingManager.isSegmentStatus(retreive, TravelPortSessionLessConstants.HK)) {
				log.info("Ticketing is success  for BookingId {} with pnr {}", bookingId, pnr);
				return true;
			}
		}
		return false;
	}
}
