package com.tgs.services.fms.servicehandler;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.ProtectGroupConfiguration;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirCancellationProtectionOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.CacheType;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.restmodel.CancellationProtectionRequest;
import com.tgs.services.fms.restmodel.CancellationProtectionResponse;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CancellationProtectionHandler
		extends ServiceHandler<CancellationProtectionRequest, CancellationProtectionResponse> {

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Override
	public void beforeProcess() throws Exception {
		if (StringUtils.isBlank(request.getBookingId())) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}
		if (request.getTotalAmount() < 0) {
			throw new CustomGeneralException(SystemError.CANNOT_BE_NEGATIVE);
		}
	}

	@Override
	public void process() throws Exception {
		try {
			AirReviewResponse reviewResponse = cachingService.getAirReviewResponse(request.getBookingId());
			if (reviewResponse == null) {
				throw new CustomGeneralException(SystemError.KEYS_EXPIRED);
			}

			FlightBasicFact fact = FlightBasicFact.createFact().generateFact(reviewResponse.getSearchQuery())
					.generateFact(reviewResponse.getTripInfos().get(0),
							AirUtils.getAirType(reviewResponse.getTripInfos().get(0)));
			User user = SystemContextHolder.getContextData().getUser();
			BaseUtils.createFactOnUser(fact, user);

			AirConfiguratorInfo rule =
					AirConfiguratorHelper.getAirConfigRuleInfo(fact, AirConfiguratorRuleType.CANCELLATION_PROTECTION);

			if (rule != null) {
				reviewResponse.setCancellationProtectionRuleId(rule.getId());
				updateReviewResponseOnCache(request.getBookingId(), reviewResponse);

				AirCancellationProtectionOutput ruleOutput = (AirCancellationProtectionOutput) rule.getOutput();

				String cancellationProtectionPremium = ruleOutput.getCancellationProtectionPremium();

				double totalAmount = request.getTotalAmount();
				double maxRedeemableAmount = new ProtectGroupConfiguration().getMaxRedeemableAmount();

				if (totalAmount > maxRedeemableAmount)
					throw new CustomGeneralException(SystemError.PROTECTION_NOT_APPLICABLE);

				int multiplyFactor = 0;
				for (TripInfo tripInfo : reviewResponse.getTripInfos()) {
					int totalSegments = tripInfo.getNumOfSegments();
					int paxCount = tripInfo.getPaxCount();
					multiplyFactor += paxCount * totalSegments;
				}

				double cancellationProtectionCharges =
						getPriceFromExpression(cancellationProtectionPremium, totalAmount, multiplyFactor);
				double gstCharges = getPriceFromExpression(ruleOutput.getCancellationProtectionTax(),
						cancellationProtectionCharges, multiplyFactor);
				double managementFee = getPriceFromExpression(ruleOutput.getCancellationProtectionManagementFee(),
						cancellationProtectionCharges, multiplyFactor);
				double managementFeeTax = getPriceFromExpression(ruleOutput.getCancellationProtectionManagementFeeTax(),
						managementFee, multiplyFactor);
				double cancellationProtectionAgentCommission = getPriceFromExpression(
						ruleOutput.getCancellationProtectionAgentCommission(), totalAmount, multiplyFactor);
				double tdsRateDouble = user.getAdditionalInfo() != null
						? ObjectUtils.defaultIfNull(user.getAdditionalInfo().getTdsRate(), 0.0)
						: 0d;
				double cancellationProtectionAgentCommissionTds =
						getPriceFromExpression(String.valueOf(tdsRateDouble).concat("%"),
								cancellationProtectionAgentCommission, multiplyFactor);
				log.info("[CancelProtection] Agent commission , Tds rate and tds charges are {} {} {}",
						cancellationProtectionAgentCommission, tdsRateDouble, cancellationProtectionAgentCommissionTds);
				double totalCancellationProtectionCharges = cancellationProtectionCharges + gstCharges + managementFee
						+ managementFeeTax + cancellationProtectionAgentCommission;

				response.setBookingId(request.getBookingId());
				response.setCancellationProtectionCharges(cancellationProtectionCharges);
				response.setCancellationProtectionTax(gstCharges);
				response.setCancellationProtectionManagementFee(managementFee);
				response.setCancellationProtectionManagementFeeTax(managementFeeTax);
				response.setCancellationProtectionAgentCommission(cancellationProtectionAgentCommission);
				response.setCancellationProtectionAgentCommissionTds(cancellationProtectionAgentCommissionTds);
				response.setTotalCancellationProtectionCharges(totalCancellationProtectionCharges);
			}
		} catch (CustomGeneralException exp) {
			log.error("Cancellation amount greater than max redeemable amount for booking id {}",
					request.getBookingId());
			throw exp;
		} catch (Exception exp) {
			log.error("No enabled cancellation protection rule found for bookingid {}", request.getBookingId(), exp);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}

	@Override
	public void afterProcess() throws Exception {}

	public void updateReviewResponseOnCache(String bookingId, AirReviewResponse reviewResponse) {
		cachingService.store(bookingId, BinName.AIRREVIEW.getName(), reviewResponse,
				CacheMetaInfo.builder().set(CacheSetName.AIR_REVIEW.name()).compress(true).plainData(false)
						.expiration(CacheType.REVIEWKEYEXPIRATION.getTtl()).build());
	}

	public double getPriceFromExpression(String expression, double amount, int multiplyFactor) {
		if (expression.indexOf('%') >= 0) {
			return BaseUtils.getPriceFromExpression(expression, amount);
		} else {
			return BaseUtils.getPriceFromExpression(expression, amount) * multiplyFactor;
		}
	}

}
