package com.tgs.services.fms.sources.sqiva;

import static com.tgs.services.fms.sources.sqiva.SqivaConstant.*;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.util.*;

@SuperBuilder
@Slf4j
@Getter
@Setter
final class SqivaSSRManager extends SqivaServiceManager {

	private static final String BAGGAE_KEY = "prepaid_baggage";
	private static final String FIX_CHARGE = "fix_charge";
	private static final int BAGGAGE_SSR_CODE_KEY = 0;
	private static final int BAGGAGE_SSR_DESC = 1;
	private static final int BAGGAGE_SSR_UNIT = 4;
	private static final int BAGGAGE_SSR_PRICE = 5;

	private static final int MEAL_CODE_INDEX = 0;
	private static final int MEAL_CODE_DESC = 1;
	private static final int MEAL_PRICE = 2;
	private static final List<String> MEAL_CODES = Arrays.asList("VGML");

	protected void doBaggage(TripInfo tripInfo) {
		HttpUtils httpUtils = HttpUtils.builder().build();
		try {
			Map<String, String> bagaggeParams = buildBagaggeParams(tripInfo);
			JSONObject baggageObject = bindingService.getPrePaidBaggage(bagaggeParams, httpUtils);
			if (!isAnyError(baggageObject)) {
				parseBagageResponse(tripInfo, baggageObject);
			}
		} catch (Exception e) {
			log.error("unable to fetch baggage for {}", bookignId, e);
		}
	}

	private void parseBagageResponse(TripInfo tripInfo, JSONObject baggageObject) {
		JSONArray bagaggeArray = baggageObject.getJSONArray(BAGGAE_KEY);
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			List<BaggageSSRInformation> baggageSSRInformations = new ArrayList<>();
			for (int bIndex = 0; bIndex < bagaggeArray.length(); bIndex++) {
				BaggageSSRInformation ssrInformation = new BaggageSSRInformation();
				JSONArray bagaggeSSR = bagaggeArray.getJSONArray(bIndex);
				String code = bagaggeSSR.getString(BAGGAGE_SSR_CODE_KEY);
				Double price = bagaggeSSR.getDouble(BAGGAGE_SSR_PRICE);
				String description = bagaggeSSR.getString(BAGGAGE_SSR_DESC);
				int unit = bagaggeSSR.getInt(BAGGAGE_SSR_UNIT);
				ssrInformation.setCode(code);
				ssrInformation.setQuantity(unit);
				ssrInformation.setDesc(description);
				if (segmentInfo.getSegmentNum() == 0 && segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
					ssrInformation.setAmount(price);
				} else {
					ssrInformation.setAmount(null);
				}
				baggageSSRInformations.add(ssrInformation);
			}
			if (MapUtils.isEmpty(segmentInfo.getSsrInfo())) {
				segmentInfo.setSsrInfo(new HashMap<>());
			}
			segmentInfo.getSsrInfo().put(SSRType.BAGGAGE, baggageSSRInformations);
		}
	}

	private Map<String, String> buildBagaggeParams(TripInfo tripInfo) {
		Map<String, String> baggageParams = new HashMap<String, String>();
		List<TripInfo> tripInfos = AirUtils.splitTripInfo(tripInfo, false);
		baggageParams.put("org", tripInfos.get(0).getDepartureAirportCode());
		baggageParams.put("des", tripInfos.get(0).getArrivalAirportCode());
		if (CollectionUtils.size(tripInfos) == 2) {
			baggageParams.put("des", tripInfos.get(1).getDepartureAirportCode());
		}
		return baggageParams;
	}

	protected void doAncillary(TripInfo tripInfo) {
		HttpUtils httpUtils = HttpUtils.builder().build();
		try {
			JSONObject ancillaryObj = bindingService.getAncillary(null, httpUtils);
			if (!isAnyError(ancillaryObj)) {
				parseAncillaryResponse(tripInfo, ancillaryObj);
			}
		} catch (Exception e) {
			log.error("unable to fetch meal for {}", bookignId, e);
		}
	}

	private void parseAncillaryResponse(TripInfo tripInfo, JSONObject ancillaryObj) {
		JSONArray fixCharges = ancillaryObj.getJSONArray(FIX_CHARGE);
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
				List<MealSSRInformation> mealSSRInformations = new ArrayList<>();
				for (int index = 0; index < fixCharges.length(); index++) {
					JSONArray mealSSR = fixCharges.getJSONArray(index);
					String code = mealSSR.getString(MEAL_CODE_INDEX);
					if (MEAL_CODES.contains(code)) {
						MealSSRInformation mealSSRInformation = new MealSSRInformation();
						String descritpion = mealSSR.getString(MEAL_CODE_DESC);
						Double price = mealSSR.getDouble(MEAL_PRICE);
						mealSSRInformation.setDesc(descritpion);
						mealSSRInformation.setAmount(price);
						mealSSRInformation.setCode(code);
						mealSSRInformations.add(mealSSRInformation);
					}
				}
				if (MapUtils.isEmpty(segmentInfo.getSsrInfo())) {
					segmentInfo.setSsrInfo(new HashMap<>());
				}
				segmentInfo.getSsrInfo().put(SSRType.MEAL, mealSSRInformations);
			}
		}
	}

	public double addAncillary_V2(BookingSegments bookingSegments, String pnr) {
		HttpUtils httpUtils = HttpUtils.builder().build();
		double totalAncillaryCharges = 0d;
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put(BOOK_CODE, pnr);
		String bookLegId = "";
		String ancillaryId = "";
		String weight = "";
		String paxId = "";
		boolean isMealAddedForTraveller = false;
		for (int segmentIndex = 0; segmentIndex < bookingSegments.getSegmentInfos().size();) {
			List<SegmentInfo> subSegments = getSubLegSegments(bookingSegments.getSegmentInfos(), segmentIndex);
			SegmentInfo firstSegment = subSegments.get(0);
			for (FlightTravellerInfo traveller : firstSegment.getTravellerInfo()) {
				if (traveller.getSsrMealInfo() != null
						&& StringUtils.isNotBlank(traveller.getSsrMealInfo().getCode())) {
					bookLegId =
							StringUtils.join(StringUtils.isNotEmpty(bookLegId) ? StringUtils.join(bookLegId, ",") : "",
									firstSegment.getPriceInfo(0).getMiscInfo().getBookLegId());
					ancillaryId = StringUtils.join(
							StringUtils.isNotEmpty(ancillaryId) ? StringUtils.join(ancillaryId, ",") : "",
							traveller.getSsrMealInfo().getCode());
					paxId = StringUtils.join(StringUtils.isNotEmpty(paxId) ? StringUtils.join(paxId, ",") : "",
							traveller.getMiscInfo().getPassengerKey());
					weight = StringUtils.join(StringUtils.isNotEmpty(weight) ? StringUtils.join(weight, ",") : "",
							DEFAULT_WEIGHT);
					isMealAddedForTraveller = true;
				}
			}
			segmentIndex += subSegments.size();
		}
		queryParams.put(BOOK_LEG_ID, bookLegId);
		queryParams.put(ANCILLARY_ID, ancillaryId);
		queryParams.put(WEIGHT, weight);
		queryParams.put(PAX_ID, paxId);
		if (isMealAddedForTraveller) {
			try {
				JSONObject addAncillaryResponse = bindingService.addAnillary_V2(queryParams, httpUtils);
				if (isAnyError(addAncillaryResponse)) {
					throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
				}
				String ancillaryAmountStr = addAncillaryResponse.get(TOTAL_ANCILLARY_AMOUNT).toString();
				totalAncillaryCharges = Double.valueOf(ancillaryAmountStr);
			} catch (IOException e) {
				throw new SupplierRemoteException(e);
			}
		}
		return totalAncillaryCharges;
	}

	public double updatePrepaidBaggage(BookingSegments bookingSegments, String pnr) {
		HttpUtils httpUtils = HttpUtils.builder().build();
		double totalPrerpaidBaggage = 0d;
		for (SegmentInfo segment : bookingSegments.getSegmentInfos()) {
			if (segment.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
				for (FlightTravellerInfo traveller : segment.getTravellerInfo()) {
					if (traveller.getSsrBaggageInfo() != null
							&& StringUtils.isNotBlank(traveller.getSsrBaggageInfo().getCode())) {
						try {
							Map<String, String> queryParams = buildUpdateBaggageParams(traveller, pnr,
									segment.getPriceInfo(0).getMiscInfo().getBookLegId());
							JSONObject updatePrepaidBaggageResponse =
									bindingService.updatePrepaidBaggage(queryParams, httpUtils);
							if (isAnyError(updatePrepaidBaggageResponse)) {
								throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
							}
							String baggageAmountStr = updatePrepaidBaggageResponse.get(TOTAL_BAGGAGE_AMOUNT).toString();
							totalPrerpaidBaggage = Double.valueOf(baggageAmountStr);
						} catch (IOException e) {
							throw new SupplierRemoteException(e);
						}
					}
				}
			}
		}
		return totalPrerpaidBaggage;
	}

	private Map<String, String> buildUpdateBaggageParams(FlightTravellerInfo traveller, String pnr, String bookLegId) {
		Map<String, String> updateBaggageParams = new HashMap<String, String>();
		updateBaggageParams.put(BOOK_CODE, pnr);
		updateBaggageParams.put(PAX_ID, traveller.getMiscInfo().getPassengerKey());
		updateBaggageParams.put(BOOK_LEG_ID, bookLegId);
		updateBaggageParams.put(BAGGAGE_ID, traveller.getSsrBaggageInfo().getCode());
		return updateBaggageParams;
	}

}
