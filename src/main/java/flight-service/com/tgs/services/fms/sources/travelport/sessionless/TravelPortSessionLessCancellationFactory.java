package com.tgs.services.fms.sources.travelport.sessionless;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.oms.datamodel.Order;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;

@Service
public class TravelPortSessionLessCancellationFactory extends AbstractAirCancellationFactory {

	protected TravelPortSessionLessBindingService bindingService;

	private SoapRequestResponseListner listener;

	public TravelPortSessionLessCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		super(bookingSegments, order, supplierConf);
	}

	@Override
	public boolean releaseHoldPNR() {
		boolean isAbortSuccess = false;
		bindingService = bindingService.builder().configuration(supplierConf).build();
		return isAbortSuccess;
	}
}
