package com.tgs.services.fms.sources.amadeusndc;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import com.tgs.services.base.enums.FareType;

public class AmadeusNdcUtils {

	public static String replaceSpecialCharacter(String input) {
		return input.replaceAll("[-.:\\\";@&()',/]", "");
	}

	public static FareType getFareType(String priceClassName) {
		 List<String> priceClassNames =Arrays.asList(priceClassName.split(" "));
		 priceClassName=priceClassNames.get(priceClassNames.size()-1).toUpperCase();
		FareType fareType=FareType.getEnumFromName(priceClassName);
		return Objects.nonNull(fareType)?fareType:FareType.PUBLISHED;
	}

}
