package com.tgs.services.fms.sources.ifly;


import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class IFlyRouteManager extends IflyServiceManager {

	public void addRouteInfoToSystem() {
		this.iFlyAirline.getOperatingAirportCodes();
	}
}
