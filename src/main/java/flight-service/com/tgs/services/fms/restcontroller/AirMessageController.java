package com.tgs.services.fms.restcontroller;


import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.restmodel.AirMessageRequest;
import com.tgs.services.fms.servicehandler.AirMessageHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RequestMapping("/fms/v1")
@RestController
@Slf4j
public class AirMessageController {

	@Autowired
	AirMessageHandler messageHandler;

	@RequestMapping(value = "/msg-itinerary", method = RequestMethod.POST)
	protected BaseResponse sendEticket(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid AirMessageRequest emailRequest) throws Exception {
		messageHandler.initData(emailRequest, new BaseResponse());
		return messageHandler.getResponse();
	}
}
