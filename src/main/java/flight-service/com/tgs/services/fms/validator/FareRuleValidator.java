package com.tgs.services.fms.validator;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.validator.rule.FlightBasicRuleCriteriaValidator;
import com.tgs.services.fms.datamodel.farerule.FareRuleAmendmentMarkupInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyContent;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyType;
import com.tgs.services.fms.datamodel.farerule.FareRuleTimeWindow;

@Service
public class FareRuleValidator implements Validator {

	@Autowired
	FlightBasicRuleCriteriaValidator criteriaValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}
		if (target instanceof FareRuleInfo) {
			FareRuleInfo rule = (FareRuleInfo) target;
			criteriaValidator.validateCriteria(errors, "inclusionCriteria", rule.getInclusionCriteria());
			criteriaValidator.validateCriteria(errors, "exclusionCriteria", rule.getExclusionCriteria());
			validatePolicyInfo(rule, errors);
			validateAmendmentMarkUp(rule, errors);
		}
	}

	private void validatePolicyInfo(FareRuleInfo rule, Errors errors) {
		FareRuleInformation fareRuleInformation = (FareRuleInformation) rule.getOutput();

		Map<FareRuleTimeWindow, FareRulePolicyContent> cancelPolicy =
				fareRuleInformation.getFareRuleInfo().get(FareRulePolicyType.CANCELLATION);
		Map<FareRuleTimeWindow, FareRulePolicyContent> reschedulePolicy =
				fareRuleInformation.getFareRuleInfo().get(FareRulePolicyType.DATECHANGE);

		if (cancelPolicy == null || MapUtils.isEmpty(cancelPolicy)
				|| cancelPolicy.get(FareRuleTimeWindow.DEFAULT) == null
				|| (cancelPolicy.get(FareRuleTimeWindow.DEFAULT).getAmount() == null
						&& StringUtils.isBlank(cancelPolicy.get(FareRuleTimeWindow.DEFAULT).getPolicyInfo()))) {
			rejectValue(errors, "fareRuleInformation.fareRuleInfo", SystemError.FARE_RULE_INVALID_POLICY_INFO,
					"cancellation");
		}

		if (reschedulePolicy == null || MapUtils.isEmpty(reschedulePolicy)
				|| reschedulePolicy.get(FareRuleTimeWindow.DEFAULT) == null
				|| (reschedulePolicy.get(FareRuleTimeWindow.DEFAULT).getAmount() == null
						&& StringUtils.isBlank(reschedulePolicy.get(FareRuleTimeWindow.DEFAULT).getPolicyInfo()))) {
			rejectValue(errors, "fareRuleInformation.fareRuleInfo", SystemError.FARE_RULE_INVALID_POLICY_INFO,
					"date change");
		}
	}

	private void validateAmendmentMarkUp(FareRuleInfo rule, Errors errors) {
		FareRuleInformation fareRuleInformation = (FareRuleInformation) rule.getOutput();

		List<FareRuleAmendmentMarkupInfo> amendmentMarkUp = fareRuleInformation.getAmendmentMarkup();
		Set<AmendmentType> allValues = new HashSet<AmendmentType>();
		for (int i = 0; i < amendmentMarkUp.size(); i++) {
			FareRuleAmendmentMarkupInfo markUp = amendmentMarkUp.get(i);
			if (CollectionUtils.isEmpty(markUp.getAmendmentTypes())) {
				rejectValue(errors, StringUtils.join("fareRuleInformation.amendmentMarkup[", i, "]"),
						SystemError.INVALID_FR_AMENDMENT_MARKUP);
				return;
			}
			for (AmendmentType amendmentType : markUp.getAmendmentTypes()) {
				if (allValues.contains(amendmentType)) {
					rejectValue(errors, StringUtils.join("fareRuleInformation.amendmentMarkup[", i, "]"),
							SystemError.DUPLICATE_FR_AMENDMENT_MARKUP);
				}
				allValues.add(amendmentType);
			}
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
