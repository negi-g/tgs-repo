package com.tgs.services.fms.validator;

import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.AirSSRFilterConfiguration;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import java.util.Map;


@Service
public class AirSSRFilterConfigurationValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T output) {


		AirSSRFilterConfiguration configuration = (AirSSRFilterConfiguration) output;

		if (output == null || MapUtils.isEmpty(configuration.getIsNotApplicable())) {
			errors.rejectValue(fieldName + ".isNotApplicable", SystemError.NULL_VALUE.errorCode(),
					SystemError.NULL_VALUE.getMessage());
			return;
		}

		Map<SSRType, Boolean> isSSRApplicableMap = configuration.getIsNotApplicable();


		if (MapUtils.isNotEmpty(isSSRApplicableMap)) {
			for (Map.Entry<SSRType, Boolean> entry : isSSRApplicableMap.entrySet()) {
				if (entry.getKey() == null) {
					errors.rejectValue(fieldName + ".isNotApplicable", SystemError.KEY_EMPTY.errorCode(),
							SystemError.KEY_EMPTY.getMessage());
					return;
				}
				if (entry.getValue() == null) {
					errors.rejectValue(fieldName + ".isNotApplicable", SystemError.NULL_VALUE.errorCode(),
							SystemError.NULL_VALUE.getMessage());
					return;
				}
			}
		}

	}
}
