package com.tgs.services.fms.sources.ifly;

import com.tgs.services.base.SoapRequestResponseListner;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class IflyAirBookingRetrieveFactroy extends AbstractAirBookingRetrieveFactory {

	protected IflyBindingService bindingService;

	protected SoapRequestResponseListner listener = null;

	public IflyAirBookingRetrieveFactroy(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}

	private void initialize() {
		bindingService = IflyBindingService.builder().configuration(supplierConf).user(bookingUser).build();
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		initialize();
		pnrBooking = AirImportPnrBooking.builder().build();
		listener = new SoapRequestResponseListner(pnr, null, supplierConf.getBasicInfo().getSupplierName());
		IflyBookingRetreiveManager retreiveManager = IflyBookingRetreiveManager.builder().bindingService(bindingService)
				.configuration(supplierConf).listener(listener).bookingUser(bookingUser).build();
		retreiveManager.init();
		retreiveManager.setPnr(pnr);
		pnrBooking = retreiveManager.retreiveBooking();
		return pnrBooking;
	}


}
