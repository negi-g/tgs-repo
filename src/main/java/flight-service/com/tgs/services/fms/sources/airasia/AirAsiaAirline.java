package com.tgs.services.fms.sources.airasia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.tempuri.ArrayOfBookingName;
import org.tempuri.ArrayOfPassengerTravelDocument;
import org.tempuri.BookingName;
import org.tempuri.MessageState;
import org.tempuri.PassengerTravelDocument;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;

public enum AirAsiaAirline {

	AIRASIA();

	public static final String PASS_DOC_TYPE_CODE = "P";

	public static final String FF_DOC_TYPE_CODE = "OAFF";

	public String getGSTTypeCode() {
		return "G";
	}

	public String getContactTypeCode() {
		return "P";
	}

	public String getTitle(FlightTravellerInfo travellerInfo) {
		if (travellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())) {
			return "CHD";
		} else if (travellerInfo.getPaxType().equals(PaxType.INFANT)) {
			return StringUtils.EMPTY;
		}
		return travellerInfo.getTitle().toUpperCase();
	}

	public ArrayOfBookingName getBookingPaxName(FlightTravellerInfo flightTravellerInfo) {
		ArrayOfBookingName bookingPaxName = new ArrayOfBookingName();
		BookingName[] bookingNames = new BookingName[1];
		bookingNames[0] = new BookingName();
		bookingNames[0].setFirstName(getFirstName(flightTravellerInfo));
		bookingNames[0].setLastName(flightTravellerInfo.getLastName());
		bookingNames[0].setState(MessageState.New);
		bookingNames[0].setTitle(getTitle(flightTravellerInfo));
		bookingPaxName.setBookingName(bookingNames);
		return bookingPaxName;
	}


	/**
	 * @implSpec : In case first name is blank we can use last name same for first name
	 * @implSpec : last name cannot be empty, for more contact ashu sir before making changes
	 */
	private String getFirstName(FlightTravellerInfo flightTravellerInfo) {
		if (StringUtils.isBlank(flightTravellerInfo.getFirstName())) {
			return flightTravellerInfo.getLastName();
		}
		return flightTravellerInfo.getFirstName();
	}

	public ArrayOfPassengerTravelDocument getPassengerTravelDocument(FlightTravellerInfo travellerInfo,
			FlightTravellerInfo infantTraveller, String carrierCode) {
		ArrayOfPassengerTravelDocument passengerTravelDocument = new ArrayOfPassengerTravelDocument();

		// adult passport
		PassengerTravelDocument travelDocument = getTravelDocument(travellerInfo, false, carrierCode);
		if (travelDocument != null) {
			passengerTravelDocument.addPassengerTravelDocument(travelDocument);
		}
		// Freq flier
		if (MapUtils.isNotEmpty(travellerInfo.getFrequentFlierMap())) {
			travelDocument = getTravelDocument(travellerInfo, true, carrierCode);
			if (travelDocument != null) {
				passengerTravelDocument.addPassengerTravelDocument(travelDocument);
			}
		}
		// Infant
		if (infantTraveller != null) {
			travelDocument = getTravelDocument(infantTraveller, false, carrierCode);
			if (travelDocument != null) {
				passengerTravelDocument.addPassengerTravelDocument(travelDocument);
			}
			if (MapUtils.isNotEmpty(infantTraveller.getFrequentFlierMap())) {
				travelDocument = getTravelDocument(infantTraveller, true, carrierCode);
				if (travelDocument != null) {
					passengerTravelDocument.addPassengerTravelDocument(travelDocument);
				}
			}
		}

		return passengerTravelDocument;
	}

	public PassengerTravelDocument getTravelDocument(FlightTravellerInfo travellerInfo, boolean isFF,
			String carrierCode) {
		PassengerTravelDocument travelDocument = null;
		if (StringUtils.isNotBlank(travellerInfo.getPassportNumber()) && !isFF) {
			travelDocument = new PassengerTravelDocument();
			travelDocument.setState(MessageState.New);
			travelDocument.setGender(AirAsiaUtils.getGender(travellerInfo.getTitle(), travellerInfo));
			travelDocument.setDocTypeCode(PASS_DOC_TYPE_CODE);// passport type
			travelDocument.setDOB(TgsDateUtils.getCalendar(travellerInfo.getDob()));
			travelDocument.setNationality(travellerInfo.getPassportNationality());
			travelDocument.setDocNumber(travellerInfo.getPassportNumber());
			travelDocument.setNames(getBookingPaxName(travellerInfo));
			travelDocument.setExpirationDate(TgsDateUtils.getCalendar(travellerInfo.getExpiryDate()));
			travelDocument.setIssuedDate(TgsDateUtils.getCalendar(travellerInfo.getPassportIssueDate()));
			travelDocument.setIssuedByCode(travellerInfo.getPassportNationality());
			if (travellerInfo.getPaxType().equals(PaxType.INFANT)) {
				travelDocument.setDocSuffix("I");
			}
		} else if (MapUtils.isNotEmpty(travellerInfo.getFrequentFlierMap())) {
			String ffNumber = travellerInfo.getFrequentFlierMap().getOrDefault(carrierCode, null);
			if (StringUtils.isNotBlank(ffNumber)) {
				travelDocument = new PassengerTravelDocument();
				travelDocument.setState(MessageState.New);
				travelDocument.setGender(AirAsiaUtils.getGender(travellerInfo.getTitle(), travellerInfo));
				travelDocument.setDocTypeCode(FF_DOC_TYPE_CODE);// ffliercode type
				travelDocument.setDOB(TgsDateUtils.getCalendar(travellerInfo.getDob()));
				travelDocument.setNationality(travellerInfo.getPassportNationality());
				travelDocument.setDocNumber(ffNumber);
				travelDocument.setNames(getBookingPaxName(travellerInfo));
				travelDocument.setIssuedByCode(travellerInfo.getPassportNationality());
			}
		}
		return travelDocument;
	}

	/* SSR Applicable on All segment */
	protected boolean isSSRApplicableOnAllSegment(SegmentInfo segmentInfo) {
		return true;
	}

	public boolean isAdditionalSSRRequired(List<SegmentInfo> segments) {
		return AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "HF")
				|| AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "CL")
				|| AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "FS");
	}

	public List<String> getAdditionalSSRCodes(List<SegmentInfo> segments) {
		List<String> additionalSSRList = new ArrayList<>();
		if (AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "HF"))
			additionalSSRList.add("HFL");
		if (AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "CL"))
			additionalSSRList.add("GCPL");
		if (AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "FS"))
			additionalSSRList.add("GCP");
		return additionalSSRList;
	}

	public void processPriceInfo(String productClass, PriceInfo priceInfo, SupplierConfiguration configuration) {
		if (productClass.equalsIgnoreCase("EC")) {
			priceInfo.setFareIdentifier(FareType.PUBLISHED);
		} else if (productClass.equalsIgnoreCase("HF")) {
			priceInfo.setFareIdentifier(FareType.PREMIUM_FLEX);
		} else if (productClass.equalsIgnoreCase("PM")) {
			priceInfo.setFareIdentifier(FareType.PREMIUM_FLATBED);
		} else if (productClass.equalsIgnoreCase("EP")) {
			priceInfo.setFareIdentifier(FareType.SALE);
		} else if (productClass.equalsIgnoreCase("CL")) {
			priceInfo.setFareIdentifier(FareType.CORPORATE_LITE);
		} else if (productClass.equalsIgnoreCase("FS")) {
			priceInfo.setFareIdentifier(FareType.CORPORATE_FLEX);
		}
	}

	public boolean isInfantFareOnSegmentWise(SegmentInfo segmentInfo) {
		return true;
	}

	public RouteInfo getCurrencyCode(AirSearchQuery searchQuery) {
		if (searchQuery != null) {
			return searchQuery.getRouteInfos().get(0);
		}
		return null;
	}

	public List<String> passThruLegCodes() {
		return Arrays.asList("PBPB");
	}

	public Double getSSRAmount(SegmentInfo segmentInfo, String code, Double amount) {
		if (passThruLegCodes().contains(code) && segmentInfo.getSegmentNum() > 0) {
			return new Double(0);
		}
		if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
			return amount;
		}
		return null;
	}

	public boolean isPassThruBaggageAdded(List<SegmentInfo> segmentInfos) {
		AtomicBoolean isAdded = new AtomicBoolean();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getTravellerInfo() != null) {
				segmentInfo.getTravellerInfo().forEach(travellerInfo -> {
					if (!isAdded.get() && travellerInfo.getSsrBaggageInfo() != null
							&& StringUtils.isNotBlank(travellerInfo.getSsrBaggageInfo().getCode())
							&& passThruLegCodes().contains(travellerInfo.getSsrBaggageInfo().getCode())) {
						isAdded.set(true);
					}
				});
			}
		});
		return isAdded.get();
	}
}
