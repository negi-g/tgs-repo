package com.tgs.services.fms.sources.flightroutes24.FR24Urls;

public final class FR24Urls {


	public static final String SEARCH_URL="/overseas/search.do";

	public static final String REVIEW_URL="/overseas/verify.do";

	public static final String FARERULE_URL = "http://120.79.211.225:8560/api/fareRule/overseas/query.do";

	public static final String BOOKING_URL="/v2/overseas/order.do";
    
	public static final String PAYVERIFY_URL="/v2/overseas/payVerify.do";
	
	public static final String APPLYTICKET_URL="/order/overseas/applyTicket.do";
	
	public static final String QUERYORDER_URL="/order/overseas/queryOrderDetail.do";
	
}
