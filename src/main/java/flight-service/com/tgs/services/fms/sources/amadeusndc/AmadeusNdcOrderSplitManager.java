package com.tgs.services.fms.sources.amadeusndc;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import javax.xml.ws.BindingProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.iata.iata._2015._00._2018_1.orderchangerq.AgencyTypeContentType;
import org.iata.iata._2015._00._2018_1.orderchangerq.AggregatorType;
import org.iata.iata._2015._00._2018_1.orderchangerq.BookingEntityType;
import org.iata.iata._2015._00._2018_1.orderchangerq.BookingRefType;
import org.iata.iata._2015._00._2018_1.orderchangerq.CarrierType;
import org.iata.iata._2015._00._2018_1.orderchangerq.ChangeOrderType;
import org.iata.iata._2015._00._2018_1.orderchangerq.CountryType;
import org.iata.iata._2015._00._2018_1.orderchangerq.IATAPayloadStandardAttributesType;
import org.iata.iata._2015._00._2018_1.orderchangerq.IndividualType;
import org.iata.iata._2015._00._2018_1.orderchangerq.OrdChangeParamsType;
import org.iata.iata._2015._00._2018_1.orderchangerq.OrderChangeRQ;
import org.iata.iata._2015._00._2018_1.orderchangerq.ParticipantType;
import org.iata.iata._2015._00._2018_1.orderchangerq.PointofSaleType2;
import org.iata.iata._2015._00._2018_1.orderchangerq.RecipientType;
import org.iata.iata._2015._00._2018_1.orderchangerq.RequestType;
import org.iata.iata._2015._00._2018_1.orderchangerq.UpdatePaxType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderViewRS;
import org.iata.iata._2015._00._2018_1.orderviewrs.PaxListType;
import org.iata.iata._2015._00._2018_1.orderviewrs.PaxType;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.amadeus.wsdl.altea_ndc_18_1_v1_v4.AlteaNDC181Service;
import com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.utils.exception.air.CancellationException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@SuperBuilder
final class AmadeusNdcOrderSplitManager extends AmadeusNdcServiceManager {


	private BookingSegments bookingSegments;

	private PaxListType paxList;

	private Map<String, PaxType> paxMap;

	private Map<String, PaxType> splitPaxMap;

	private String childPNR;


	public void orderChange() {
		String parentBookingRefId = null;
		paxMap = new HashMap<>();
		splitPaxMap = new HashMap<>();

		paxList.getPax().forEach(paxType -> {
			// firstname+lastname
			String key = paxType.getIndividual().getGivenName().get(0) + "-" + paxType.getIndividual().getSurname();
			paxMap.put(key, paxType);
		});

		OrderChangeRQ orderChangeRq = null;
		orderChangeRq = createOrderChangeRq();
		AMASecurityHostedUser hostedUser = getSecurityHostedUser();
		AlteaNDC181PT servicePort = service.getAlteaNDC181Port();
		if (Objects.isNull(servicePort)) {
			service = new AlteaNDC181Service();
			servicePort = service.getAlteaNDC181Port();
		}
		BindingProvider bp = (BindingProvider) servicePort;
		addJAXHandlers(bp, bookingId, AmadeusNdcHeaderHandler._ORDER_CHANGE_);
		OrderViewRS orderChangeRs = servicePort.ndcOrderChange(orderChangeRq, null, null, hostedUser);

		if (!isAnyError(orderChangeRs)) {
			OrderType orderType = orderChangeRs.getResponse().getOrder().get(0);
			orderChangeRs.getResponse().getDataLists().getPaxList().getPax().forEach(pax -> {
				splitPaxMap.put(pax.getPaxID(), pax);
			});

			childPNR = orderType.getOrderID();
			if (StringUtils.isNotBlank(childPNR)) {
				for (org.iata.iata._2015._00._2018_1.orderviewrs.BookingRefType bookingRef : orderType
						.getBookingRef()) {
					if (Objects.nonNull(bookingRef.getTypeCode())) {
						if (StringUtils.equals(AmadeusNDCConstants.PNR_SPLIT_CODE, bookingRef.getTypeCode()))
							parentBookingRefId = bookingRef.getBookingID();
					}
				}
			}
		}
	}

	public boolean isAnyError(OrderViewRS orderChangeRs) {
		if (CollectionUtils.isNotEmpty(orderChangeRs.getErrors())) {
			StringJoiner errorMessage = new StringJoiner("");
			orderChangeRs.getErrors().forEach(error -> {
				error.getError().forEach(errorType -> {
					errorMessage.add(errorType.getCode() + ":" + errorType.getDescText());
				});
			});
			log.error("Could not perform PNR split : {}", getSupplierPNR());
			throw new CancellationException(errorMessage.toString());
		}
		return false;
	}

	public void updateCancelledPaxPnr(String childPnr) {
		if (StringUtils.isNotBlank(childPnr)) {
			for (FlightTravellerInfo travellerInfo : bookingSegments.getCancelSegments().get(0).getTravellerInfo()) {
				travellerInfo.setPnr(childPnr);
			}
		}
	}

	private OrderChangeRQ createOrderChangeRq() {
		OrderChangeRQ orderChangeRq = new OrderChangeRQ();
		RequestType requestType = new RequestType();
		BookingRefType bookingRef = new BookingRefType();
		populateBookingRef(bookingRef);
		requestType.getBookingRef().add(bookingRef);
		ChangeOrderType changeOrderType = new ChangeOrderType();
		setTravellerInfoForSplit(changeOrderType);
		requestType.setChangeOrder(changeOrderType);

		OrdChangeParamsType orderChangeParameters = new OrdChangeParamsType();
		orderChangeParameters.setReason(AmadeusNDCConstants.ORDER_CHANGE_REASON);
		requestType.setOrderChangeParameters(orderChangeParameters);
		orderChangeRq.setPayloadAttributes(getPayloadAttributes());
		orderChangeRq.setPointOfSale(getPointOfSale());
		orderChangeRq.setParty(getParty());
		orderChangeRq.setRequest(requestType);

		return orderChangeRq;
	}


	private void populateBookingRef(BookingRefType bookingRef) {
		bookingRef.setBookingID(getAirlinePNR());
		BookingEntityType bookingEntityType = new BookingEntityType();
		CarrierType carrierType = new CarrierType();
		carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
		bookingEntityType.setCarrier(carrierType);
		bookingRef.setBookingEntity(bookingEntityType);
	}

	private void setTravellerInfoForSplit(ChangeOrderType changeOrderType) {
		List<FlightTravellerInfo> travellerInfoList = bookingSegments.getCancelSegments().get(0).getTravellerInfo();
		List<UpdatePaxType> updatePaxList = changeOrderType.getUpdatePax();
		for (FlightTravellerInfo travellerInfo : travellerInfoList) {
			matchAndSetPaxDetails(travellerInfo, updatePaxList);
		}
	}

	private void matchAndSetPaxDetails(FlightTravellerInfo travellerInfo, List<UpdatePaxType> updatePaxList) {
		String firstName = travellerInfo.getFirstName();
		String lastName = travellerInfo.getLastName();
		String key = firstName + "-" + lastName;

		PaxType paxType = paxMap.get(key);
		if (!travellerInfo.getPaxType().equals(com.tgs.services.base.enums.PaxType.INFANT)) {
			UpdatePaxType updatePaxType = new UpdatePaxType();
			org.iata.iata._2015._00._2018_1.orderchangerq.PaxType newPaxType =
					new org.iata.iata._2015._00._2018_1.orderchangerq.PaxType();
			newPaxType.setPaxID(paxType.getPaxID());
			IndividualType individualType = new IndividualType();
			individualType.setIndividualID(paxType.getPaxID());
			String givenName = paxType.getIndividual().getGivenName().get(0);
			individualType.setSurname(paxType.getIndividual().getSurname());
			individualType.getGivenName().add(givenName);

			newPaxType.setIndividual(individualType);
			updatePaxType.setNew(newPaxType);
			updatePaxList.add(updatePaxType);
		}

	}

	private IATAPayloadStandardAttributesType getPayloadAttributes() {
		IATAPayloadStandardAttributesType payloadAttributesType = new IATAPayloadStandardAttributesType();
		payloadAttributesType.setVersion(AmadeusNDCConstants.SERVICE_VERSION);
		return payloadAttributesType;
	}

	private org.iata.iata._2015._00._2018_1.orderchangerq.PartyType getParty() {
		org.iata.iata._2015._00._2018_1.orderchangerq.PartyType party =
				new org.iata.iata._2015._00._2018_1.orderchangerq.PartyType();
		org.iata.iata._2015._00._2018_1.orderchangerq.SenderType senderType =
				new org.iata.iata._2015._00._2018_1.orderchangerq.SenderType();
		org.iata.iata._2015._00._2018_1.orderchangerq.TravelAgencyType travelAgency =
				new org.iata.iata._2015._00._2018_1.orderchangerq.TravelAgencyType();
		travelAgency.setAgencyID(supplierConf.getSupplierCredential().getIataNumber());
		travelAgency.setIATANumber(new BigDecimal(supplierConf.getSupplierCredential().getIataNumber()));
		travelAgency.setName(supplierConf.getSupplierCredential().getAgentUserName());
		travelAgency.setTypeCode(AgencyTypeContentType.ONLINE_TRAVEL_AGENCY);
		senderType.setTravelAgency(travelAgency);
		party.setSender(senderType);
		party.setRecipient(getRecipient());
		return party;
	}

	private RecipientType getRecipient() {
		RecipientType recipientType = new RecipientType();
		CarrierType carrierType = new CarrierType();
		carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
		recipientType.setORA(carrierType);
		return recipientType;
	}


	private PointofSaleType2 getPointOfSale() {
		PointofSaleType2 pointOfSale = new PointofSaleType2();
		CountryType countryType = new CountryType();
		countryType.setCountryCode(AmadeusNDCConstants.COUNTRY_CODE);
		pointOfSale.setCountry(countryType);
		return pointOfSale;
	}
}
