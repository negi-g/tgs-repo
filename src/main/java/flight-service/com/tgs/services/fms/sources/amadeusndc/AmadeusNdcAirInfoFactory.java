package com.tgs.services.fms.sources.amadeusndc;

import org.springframework.stereotype.Service;
import com.amadeus.wsdl.altea_ndc_18_1_v1_v4.AlteaNDC181Service;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AmadeusNdcAirInfoFactory extends AbstractAirInfoFactory {

	protected AmadeusNdcSearchManager searchManager;

	protected AmadeusNdcBindingService bindingService;

	private static AlteaNDC181Service service = null;

	protected AmadeusNdcReviewManager reviewManager;

	private String toCurrency;

	private static boolean isServicePortCreated;

	static {
		// keeping static block to load class before warm up
		log.debug("AmadeusNdcAirInfoFactory Starting creating static port service .....");
		service = new AlteaNDC181Service();
		service.getAlteaNDC181Port();
		isServicePortCreated = true;
		log.debug("AmadeusNdcAirInfoFactory completed creating static port service .....");
	}

	public AmadeusNdcAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
		toCurrency = AirSourceConstants.getClientCurrencyCode();
	}

	@Override
	protected void searchAvailableSchedules() {
		bindingService = AmadeusNdcBindingService.builder().build();
		if (isServicePortCreated) {
			searchManager = AmadeusNdcSearchManager.builder().service(service).searchQuery(searchQuery)
					.supplierConf(supplierConf).moneyExchangeComm(moneyExchnageComm).toCurrency(toCurrency).build();
			searchResult = searchManager.airShopping();
		}
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		TripInfo reviewedTrip = null;
		try {
			bindingService = AmadeusNdcBindingService.builder().build();
			reviewManager = AmadeusNdcReviewManager.builder().supplierConf(supplierConf).service(service)
					.selectedTrip(selectedTrip).criticalMessageLogger(criticalMessageLogger).bookingUser(user)
					.moneyExchangeComm(moneyExchnageComm).bookingId(bookingId).toCurrency(toCurrency).build();
			reviewManager.offerPrice();
			reviewedTrip = reviewManager.getSelectedTrip();
			if (reviewedTrip != null) {
				AmadeusNdcSSRManager ssrManager = AmadeusNdcSSRManager.builder().supplierConf(supplierConf)
						.service(service).selectedTrip(selectedTrip).criticalMessageLogger(criticalMessageLogger)
						.bookingUser(user).moneyExchangeComm(moneyExchnageComm).bookingId(bookingId)
						.toCurrency(toCurrency).build();
				ssrManager.serviceList();
			}
		} finally {
			if (reviewedTrip != null) {
				storeBookingSession(bookingId, null, null, reviewedTrip);
			}
		}
		return selectedTrip;
	}

	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		try {
			AmadeusNdcSeatMapManager seatManager = AmadeusNdcSeatMapManager.builder().supplierConf(supplierConf)
					.service(service).tripSeatMap(tripSeatMap).selectedTrip(tripInfo)
					.criticalMessageLogger(criticalMessageLogger).bookingUser(user).moneyExchangeComm(moneyExchnageComm)
					.bookingId(bookingId).toCurrency(toCurrency).build();
			seatManager.seatAvailability();
		} catch (Exception e) {
			log.error("Unable to fetch seat map for trip {}", tripInfo.toString(), e);
		}
		return tripSeatMap;
	}
}
