package com.tgs.services.fms.sources.technogram;

import java.io.IOException;
import java.util.Map.Entry;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.restmodel.AirReviewSeatResponse;
import com.tgs.services.fms.restmodel.SeatMapRequest;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;

@SuperBuilder
final class TechnoGramSeatSSRManager extends TechnoGramServiceManager {

	public AirReviewSeatResponse getSeatMap(String refId) {
		SeatMapRequest seatMapRequest = buildSeatMapRequest(refId);
		AirReviewSeatResponse seatMapResponse = seatMap(seatMapRequest);
		return seatMapResponse;
	}

	private SeatMapRequest buildSeatMapRequest(String refId) {
		SeatMapRequest seatMapRequest = new SeatMapRequest();
		seatMapRequest.setBookingId(refId);
		return seatMapRequest;
	}

	public AirReviewSeatResponse seatMap(SeatMapRequest seatMapRequest) {
		AirReviewSeatResponse seatMapResponse = null;
		try {
			seatMapResponse = getResponseByRequest(GsonUtils.getGson().toJson(seatMapRequest), "SeatMap",
					bindingService.getSeatMapURL(), AirReviewSeatResponse.class);
			parseSeatMapResponse(seatMapResponse);

		} catch (IOException e) {
			throw new SupplierRemoteException(e.getMessage());
		}
		return seatMapResponse;
	}

	private void parseSeatMapResponse(AirReviewSeatResponse seatMapResponse) {
		for (Entry<String, SeatInformation> seatInfo : seatMapResponse.getTripSeatMap().getTripSeat().entrySet()) {
			for (SeatSSRInformation seatSsrInfo : seatInfo.getValue().getSeatsInfo()) {
				if (seatSsrInfo.getAmount() != null)
					seatSsrInfo.setAmount(getAmountBasedOnCurrency(seatSsrInfo.getAmount(), false));
			}
		}
	}

	public TripSeatMap getTripSeatMap(TripInfo tripInfo, AirReviewSeatResponse seatResponse) {
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			String segKey = segmentInfo.getPriceInfoList().get(0).getMiscInfo().getSegmentKey();
			tripSeatMap.getTripSeat().put(segmentInfo.getId(), seatResponse.getTripSeatMap().getTripSeat().get(segKey));
		});
		return tripSeatMap;
	}

}

