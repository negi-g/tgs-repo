package com.tgs.services.fms.validator;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.validator.ListValidator;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractAirConfigRuleValidator {

	@Autowired
	protected ListValidator listValidator;

	protected void validateAirlines(List<String> airlines, String fieldName, Errors errors, SystemError systemError) {
		if (CollectionUtils.isNotEmpty(airlines)) {
			List<String> validatingAirlines = new ArrayList<>();
			for (String airline : airlines) {
				if (!airline.equalsIgnoreCase("ALL"))
					validatingAirlines.add(airline);
			}
			if (CollectionUtils.isNotEmpty(validatingAirlines)) {
				listValidator.validateAirlines(validatingAirlines, fieldName, errors, systemError);
			}
		}
	}

	protected void validateAirports(List<String> airports, String fieldName, Errors errors, SystemError systemError) {
		listValidator.validateAirports(airports, fieldName, errors, systemError);
	}

	protected void validateSourceIds(List<Integer> sourceIds, String fieldName, Errors errors,
			SystemError systemError) {
		listValidator.validateSourceIds(sourceIds, fieldName, errors, systemError);
	}

	protected void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}

	public abstract <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput);

}
