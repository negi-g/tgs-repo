package com.tgs.services.fms.sources.travelport.sessionless;

import org.springframework.stereotype.Service;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import com.tgs.services.oms.restmodel.air.RetrieveTicketStatusRequest;
import com.tgs.services.oms.restmodel.air.RetrieveTicketStatusResponse;

@Service
public class TravelPortSessionLessAirBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {
	private TravelPortSessionLessBindingService bindingService;
	private TravelPortSessionLessBookingRetrieveManager retrieveManager;
	protected SoapRequestResponseListner listener;

	public void initialize() {
		listener = new SoapRequestResponseListner(pnr, null, supplierConf.getBasicInfo().getSupplierName());
		bindingService = TravelPortSessionLessBindingService.builder().configuration(supplierConf).build();
		retrieveManager = TravelPortSessionLessBookingRetrieveManager.builder().pnr(pnr).listener(listener)
				.bindingService(bindingService).configuration(supplierConf).bookingUser(bookingUser).build();
		String traceId = TravelPortSessionLessUtils.keyGenerator();
		retrieveManager.setTraceId(traceId);

	}

	public TravelPortSessionLessAirBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}


	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		initialize();
		retrieveManager.init();
		pnrBooking = retrieveManager.retrieveBooking(retrieveManager.getUnivRecordImportRS(pnr));
		return pnrBooking;
	}

	@Override
	public RetrieveTicketStatusResponse retrieveTicketStatus(RetrieveTicketStatusRequest ticketStatusRequest) {
		initialize();
		return retrieveManager.retrieveTicketStatus(ticketStatusRequest);
	}
}
