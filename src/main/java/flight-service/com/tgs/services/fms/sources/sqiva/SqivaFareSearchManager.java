package com.tgs.services.fms.sources.sqiva;

import java.io.IOException;
import java.util.*;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.fms.datamodel.*;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;


@SuperBuilder
@Getter
@Setter
final class SqivaFareSearchManager extends SqivaServiceManager {

	// Fare Array
	private static final int TOTAL_FARE = 0;
	private static final int BASE_FARE = 1;
	private static final int INSURANCE = 2;
	private static final int AIRPORT_TAX = 3;
	private static final int FUEL_SURCHARGE = 4;
	private static final int TERMINAL_FEE = 5;
	private static final int BOOKING_FEE = 6;
	private static final int VAT_GST = 7;

	private static final String YEASY = "YEASY";

	// Constants
	private static final int FARE_PK = 0;
	private static final int ADULT_FARE_INDEX = 1;
	private static final int CHILD_FARE_INDEX = 2;
	private static final int INFANT_FARE_INDEX = 3;

	private static final int ADT_ADDTIONAL_TAX = 20;
	private static final int CHD_ADDTIONAL_TAX = 21;
	private static final int INFT_ADDTIONAL_TAX = 22;

	private static final int SR_ADULT_FARE_INDEX = 4;
	private static final int SR_CHILD_FARE_INDEX = 5;
	private static final int SR_INFANT_FARE_INDEX = 6;


	private static final int ADULT_CHECKIN_BAGAGE = 18;
	private static final int CHILD_CHECKIN_BAGAGE = 19;

	private TripInfo tripInfo;

	private boolean isSpecialReturn;

	private boolean isReviewFlow;

	protected TripInfo doFareQuote() {
		HttpUtils httpUtils = HttpUtils.builder().build();
		TripInfo copyOfTrip = null;
		try {
			Map<String, String> fareQuoteParams = buildFareParams();
			JSONObject fareResultObject = bindingService.getFareQuote(fareQuoteParams, httpUtils);
			boolean isAnyError = isAnyError(fareResultObject);
			if (isAnyError) {
				throw new NoSeatAvailableException(String.join(",", criticalMessageLogger));
			}
			copyOfTrip = new GsonMapper<>(tripInfo, TripInfo.class).convert();
			copyOfTrip.getSegmentInfos().forEach(segmentInfo -> {
				segmentInfo.setPriceInfoList(null);
			});
			parseFareQuoteResponse(copyOfTrip, fareResultObject);
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		}
		return copyOfTrip;
	}

	private void parseFareQuoteResponse(TripInfo copyOfTrip, JSONObject fareResultObject) {
		List<TripInfo> trips = copyOfTrip.splitTripInfo(false);

		try {
			// update onward trip
			JSONArray arrayOfPriceInfo = fareResultObject.getJSONArray("fare_info");
			List<JSONArray> filterPrices = groupAndFilterFares(arrayOfPriceInfo);
			if (CollectionUtils.isNotEmpty(filterPrices)) {
				TripInfo tripInfo = trips.get(0);
				for (JSONArray priceOption : filterPrices) {
					buildFareInfoToPrice(tripInfo, priceOption, false);
				}
			} else {
				throw new NoSeatAvailableException("No option Available post filtering allowed fare types");
			}
			// update return trip
			if (CollectionUtils.size(trips) == 2) {
				arrayOfPriceInfo = fareResultObject.getJSONArray("ret_fare_info");
				filterPrices = groupAndFilterFares(arrayOfPriceInfo);
				if (CollectionUtils.isNotEmpty(filterPrices)) {
					TripInfo tripInfo = trips.get(1);
					for (JSONArray priceOption : filterPrices) {
						buildFareInfoToPrice(tripInfo, priceOption, true);
					}
				} else {
					throw new NoSeatAvailableException("No option Available post filtering allowed fare types");
				}
			}
			tripInfo = copyOfTrip;
		} finally {

		}
	}

	private List<JSONArray> groupAndFilterFares(JSONArray arrayOfPriceInfo) {
		Map<String, List<JSONArray>> groupedByFareTypes = new HashMap<>();
		SegmentInfo firstSegment = tripInfo.getSegmentInfos().get(0);
		Map<String, Integer> classWiseSeatCounts = firstSegment.getPriceInfo(0).getMiscInfo().getClassWiseSeatCounts();
		// group by class of book
		for (int priceIndex = 0; priceIndex < arrayOfPriceInfo.length(); priceIndex++) {
			JSONArray priceOption = arrayOfPriceInfo.getJSONArray(priceIndex);
			String fareType = priceOption.getString(7);
			String classOfBooking = priceOption.getString(0).split("/")[0];
			if (isReviewFlow || (classWiseSeatCounts != null && classWiseSeatCounts.get(classOfBooking) != null)) {
				List<JSONArray> existingList = groupedByFareTypes.getOrDefault(fareType, new ArrayList<>());
				existingList.add(priceOption);
				groupedByFareTypes.put(fareType, existingList);
			}
		}

		// remove unwanted filter and sort
		List<String> allowedfareTypes = sqivaAirline.getAllowedFareTypes();
		Set<String> fareTypes = groupedByFareTypes.keySet();
		fareTypes.retainAll(allowedfareTypes);
		List<JSONArray> finalPrices = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(allowedfareTypes) && MapUtils.isNotEmpty(groupedByFareTypes)) {
			for (Map.Entry<String, List<JSONArray>> entry : groupedByFareTypes.entrySet()) {
				filterNullFares(entry.getValue());
				sortPriceOnTotalFare(entry.getValue());
				if (CollectionUtils.isNotEmpty(entry.getValue())) {
					finalPrices.add(entry.getValue().get(0));
				}
			}
		}

		return finalPrices;
	}

	private void filterNullFares(List<JSONArray> value) {
		for (Iterator<JSONArray> priceArray = value.iterator(); priceArray.hasNext();) {
			JSONArray jsonArray = priceArray.next();
			JSONArray adultFareArray = isSpecialReturn ? jsonArray.getJSONArray(SR_ADULT_FARE_INDEX)
					: jsonArray.getJSONArray(ADULT_FARE_INDEX);
			if (adultFareArray.optDouble(TOTAL_FARE, 0d) == 0d || adultFareArray.optDouble(BASE_FARE, 0d) == 0d) {
				priceArray.remove();
				continue;
			}
			if (AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD) > 0) {
				JSONArray childFareArray = isSpecialReturn ? jsonArray.getJSONArray(SR_CHILD_FARE_INDEX)
						: jsonArray.getJSONArray(CHILD_FARE_INDEX);
				if (childFareArray.optDouble(TOTAL_FARE, 0d) == 0d || childFareArray.optDouble(BASE_FARE, 0d) == 0d) {
					priceArray.remove();
					continue;
				}
			}

			if (AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT) > 0) {
				JSONArray infantFareArray = isSpecialReturn ? jsonArray.getJSONArray(SR_INFANT_FARE_INDEX)
						: jsonArray.getJSONArray(INFANT_FARE_INDEX);
				if (infantFareArray.optDouble(TOTAL_FARE, 0d) == 0d || infantFareArray.optDouble(BASE_FARE, 0d) == 0d) {
					priceArray.remove();
					continue;
				}
			}

		}
	}

	private void sortPriceOnTotalFare(List<JSONArray> prices) {
		if (CollectionUtils.isNotEmpty(prices)) {
			Collections.sort(prices, new Comparator<JSONArray>() {
				@Override
				public int compare(JSONArray price1, JSONArray price2) {
					Double priceA_tf = getTotalFareOnJsonPriceArray(price1);
					Double priceB_tf = getTotalFareOnJsonPriceArray(price2);
					return Double.compare(priceA_tf, priceB_tf);
				}
			});
		}
	}

	private Double getTotalFareOnJsonPriceArray(JSONArray priceArray) {
		Double totalFare = 0D;

		// adult total fare
		totalFare += priceArray.getJSONArray(1).optDouble(0, 0d);
		if (AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD) > 0) {
			// child total fare
			totalFare += priceArray.getJSONArray(2).optDouble(0, 0d);
		}
		if (AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT) > 0) {
			// infant total fare
			totalFare += priceArray.getJSONArray(3).optDouble(0, 0d);
		}
		return totalFare;
	}

	private void buildFareInfoToPrice(TripInfo copyOfTrip, JSONArray jsonArray, boolean isRetunTrip) {
		List<TripInfo> splitTrips = tripInfo.splitTripInfo(false);
		TripInfo originalTripInfo = splitTrips.get(0);
		if (isRetunTrip) {
			originalTripInfo = splitTrips.get(1);
		}
		for (int segmentIndex = 0; segmentIndex < copyOfTrip.getSegmentInfos().size(); segmentIndex++) {
			SegmentInfo orgSegmentInfo = originalTripInfo.getSegmentInfos().get(segmentIndex);
			PriceMiscInfo orgMiscInfo = orgSegmentInfo.getPriceInfo(0).getMiscInfo();
			Integer orgSeatCount = MapUtils.isNotEmpty(orgSegmentInfo.getPriceInfo(0).getFareDetails())
					? orgSegmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT).getSeatRemaining()
					: null;
			SegmentInfo segmentInfo = copyOfTrip.getSegmentInfos().get(segmentIndex);
			Integer segmentNum = segmentInfo.getSegmentNum();
			List<PriceInfo> priceInfos = segmentInfo.getPriceInfoList();
			PriceInfo priceInfo = PriceInfo.builder().build();
			priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
			String[] classOfBookAndFareBasis = jsonArray.get(FARE_PK).toString().split("/");
			String classOfBook = classOfBookAndFareBasis[0];
			String fareBasis = classOfBookAndFareBasis[1];
			priceInfo.setMiscInfo(new GsonMapper<>(orgMiscInfo, PriceMiscInfo.class).convert());
			priceInfo.getMiscInfo().setClassWiseSeatCounts(null);
			Map<PaxType, FareDetail> fareDetailMap =
					buildFareDetail(priceInfo, segmentNum, classOfBook, fareBasis, jsonArray, orgMiscInfo);
			setSeatCountFromOrgPrice(orgSeatCount, fareDetailMap);
			sqivaAirline.setFareType(priceInfo, isSpecialReturn, jsonArray.getString(7));
			priceInfo.setFareDetails(fareDetailMap);
			priceInfos.add(priceInfo);
		}
	}

	private void setSeatCountFromOrgPrice(Integer seatCount, Map<PaxType, FareDetail> fareDetailMap) {
		if (seatCount != null) {
			for (Map.Entry<PaxType, FareDetail> entry : fareDetailMap.entrySet()) {
				PaxType paxType = entry.getKey();
				FareDetail fareDetail = entry.getValue();
				if (!PaxType.INFANT.equals(paxType)) {
					fareDetail.setSeatRemaining(seatCount);
				}
			}
		}
	}

	private Map<PaxType, FareDetail> buildFareDetail(PriceInfo priceInfo, int segmentNum, String classOfBook,
			String fareBasis, JSONArray priceArray, PriceMiscInfo miscInfo) {
		Map<PaxType, FareDetail> paxTypeFareDetailMap = new HashMap<>();
		JSONArray adultFareArray = isSpecialReturn ? priceArray.getJSONArray(SR_ADULT_FARE_INDEX)
				: priceArray.getJSONArray(ADULT_FARE_INDEX);
		if (!adultFareArray.isEmpty()) {
			JSONArray commonTax = priceArray.optJSONArray(ADT_ADDTIONAL_TAX);
			FareDetail adultFD = parseFareDetail(priceInfo.getMiscInfo(), segmentNum, adultFareArray, commonTax);
			setStaticFareDetail(adultFD, classOfBook, fareBasis, miscInfo);
			adultFD.setBaggageInfo(buildBaggageInfo(priceArray, ADULT_CHECKIN_BAGAGE));
			paxTypeFareDetailMap.put(PaxType.ADULT, adultFD);
		}
		if (AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD) > 0) {
			JSONArray commonTax = priceArray.optJSONArray(CHD_ADDTIONAL_TAX);
			JSONArray childFareArray = isSpecialReturn ? priceArray.getJSONArray(SR_CHILD_FARE_INDEX)
					: priceArray.getJSONArray(CHILD_FARE_INDEX);
			FareDetail childFd = parseFareDetail(priceInfo.getMiscInfo(), segmentNum, childFareArray, commonTax);
			setStaticFareDetail(childFd, classOfBook, fareBasis, miscInfo);
			childFd.setBaggageInfo(buildBaggageInfo(priceArray, CHILD_CHECKIN_BAGAGE));
			paxTypeFareDetailMap.put(PaxType.CHILD, childFd);
		}

		if (AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT) > 0) {
			JSONArray commonTax = priceArray.optJSONArray(INFT_ADDTIONAL_TAX);
			JSONArray infantFareArray = isSpecialReturn ? priceArray.getJSONArray(SR_INFANT_FARE_INDEX)
					: priceArray.getJSONArray(INFANT_FARE_INDEX);
			FareDetail infantFd = parseFareDetail(priceInfo.getMiscInfo(), segmentNum, infantFareArray, commonTax);
			setStaticFareDetail(infantFd, classOfBook, fareBasis, null);
			paxTypeFareDetailMap.put(PaxType.INFANT, infantFd);
		}
		return paxTypeFareDetailMap;
	}

	public void setStaticFareDetail(FareDetail fareDetail, String classOfBook, String fareBasis,
			PriceMiscInfo miscInfo) {
		fareDetail.setClassOfBooking(classOfBook);
		fareDetail.setFareBasis(fareBasis);
		fareDetail.setCabinClass(CabinClass.ECONOMY);
		fareDetail.setRefundableType(RefundableType.NON_REFUNDABLE.getRefundableType());
		fareDetail.setIsHandBaggage(false);
		fareDetail.setIsMealIncluded(false);
		if (miscInfo != null && MapUtils.isNotEmpty(miscInfo.getClassWiseSeatCounts())) {
			fareDetail.setSeatRemaining(miscInfo.getClassWiseSeatCounts().get(classOfBook));
		}
	}

	private BaggageInfo buildBaggageInfo(JSONArray priceArray, int checkInBagIndex) {
		BaggageInfo baggageInfo = new BaggageInfo();
		baggageInfo.setAllowance(StringUtils.join(priceArray.getInt(checkInBagIndex), " KG Included"));
		return baggageInfo;
	}


	public Map<String, String> buildFareParams() {
		Map<String, String> fareParams = new HashMap<String, String>();
		List<TripInfo> tripInfos = AirUtils.splitTripInfo(tripInfo, false);
		fareParams.put("org", tripInfos.get(0).getDepartureAirportCode());
		fareParams.put("des", tripInfos.get(0).getArrivalAirportCode());
		fareParams.put("flight_date", SqivaUtils.localDateTimeToSqivaFormat(tripInfos.get(0).getDepartureTime()));
		fareParams.put("flight_no", SqivaUtils.getFlightNumberWithCode(tripInfos.get(0)));

		if (CollectionUtils.size(tripInfos) == 2) {
			fareParams.put("return_flight", "1");
			fareParams.put("ret_flight_no", SqivaUtils.getFlightNumberWithCode(tripInfos.get(1)));
			fareParams.put("ret_flight_date",
					SqivaUtils.localDateTimeToSqivaFormat(tripInfos.get(1).getDepartureTime()));
			fareParams.put("ret_flight_no", SqivaUtils.getFlightNumberWithCode(tripInfos.get(1)));
		}
		return fareParams;
	}


	public FareDetail parseFareDetail(PriceMiscInfo miscInfo, int segmentNum, JSONArray fareArray,
			JSONArray commonTax) {
		FareDetail frDetail = new FareDetail();
		Map<FareComponent, Double> fComponent = new HashMap<FareComponent, Double>();
		if (miscInfo.getLegNum() == 0 && segmentNum == 0 && !fareArray.isEmpty()) {
			Double totalCharges = 0d;
			for (int i = 0; i < fareArray.length(); i++) {
				switch (i) {
					case TOTAL_FARE:
						// dont use this due to some time mismtach from api
						Double charges = fareArray.optDouble(TOTAL_FARE, 0d) - fareArray.optDouble(BOOKING_FEE, 0d);
						// fComponent.put(FareComponent.TF, charges);
						break;
					case BASE_FARE:
						Double basefare = fareArray.optDouble(BASE_FARE, 0d);
						totalCharges += basefare;
						fComponent.put(FareComponent.BF, basefare);
						break;
					case AIRPORT_TAX:
						Double airportTax = fareArray.optDouble(AIRPORT_TAX, 0d);
						totalCharges += airportTax;
						fComponent.put(FareComponent.WO, airportTax);
						break;
					case FUEL_SURCHARGE:
						Double fuelCharge = fareArray.optDouble(FUEL_SURCHARGE, 0d);
						totalCharges += fuelCharge;
						fComponent.put(FareComponent.YQ, fuelCharge);
						break;
					case VAT_GST:
						Double gstCharges = fareArray.optDouble(VAT_GST, 0d);
						totalCharges += gstCharges;
						fComponent.put(FareComponent.AGST, gstCharges);
						break;
					case TERMINAL_FEE:
						Double terminalFee = fareArray.optDouble(TERMINAL_FEE, 0d);
						totalCharges += terminalFee;
						fComponent.put(FareComponent.AT, terminalFee);
						break;
				}
			}
			fComponent.put(FareComponent.TF, totalCharges);
		} else {
			fComponent.put(FareComponent.BF, 0d);
			fComponent.put(FareComponent.TF, 0d);
		}
		if (commonTax != null && miscInfo.getLegNum() == 0 && segmentNum == 0) {
			for (int i = 0; i < commonTax.length(); i++) {
				JSONArray commonTaxBreakUp = commonTax.optJSONArray(i);
				if (commonTaxBreakUp != null) {
					String taxItemName = commonTaxBreakUp.optString(0, null);
					Double taxAmount = commonTaxBreakUp.optDouble(1);
					if (StringUtils.isNotBlank(taxItemName) && taxAmount != null) {
						taxItemName = StringUtils.replace(taxItemName, " ", "");
						if (StringUtils.equalsIgnoreCase("UserDevelopmentFee", taxItemName)) {
							Double udfCharges = fComponent.getOrDefault(FareComponent.UDF, 0d) + taxAmount;
							Double totalCharges = fComponent.getOrDefault(FareComponent.TF, 0d);
							fComponent.put(FareComponent.UDF, udfCharges);
							fComponent.put(FareComponent.TF, totalCharges + udfCharges);
						} else if (StringUtils.equalsIgnoreCase("CuteFee", taxItemName)) {
							// taxAmount = taxAmount * legCounts;
							Double taxes = fComponent.getOrDefault(FareComponent.AT, 0d) + taxAmount;
							Double totalCharges = fComponent.getOrDefault(FareComponent.TF, 0d);
							fComponent.put(FareComponent.AT, taxes);
							fComponent.put(FareComponent.TF, totalCharges + taxAmount);
						} else {
							Double taxes = fComponent.getOrDefault(FareComponent.AT, 0d) + taxAmount;
							Double totalCharges = fComponent.getOrDefault(FareComponent.TF, 0d);
							fComponent.put(FareComponent.AT, taxes);
							fComponent.put(FareComponent.TF, totalCharges + taxAmount);
						}
					}
				}
			}
		}
		frDetail.setFareComponents(fComponent);
		return frDetail;
	}


}
