package com.tgs.services.fms.sources.sabre;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import com.sabre.webservices.websvc.EnhancedAirBookServiceStub;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.ebxml.www.namespaces.messageheader.MessageHeader;
import com.google.common.base.Enums;
import com.google.common.util.concurrent.AtomicDouble;
import com.sabre.services.sp.eab.v3_10.AirItineraryPricingInfo_type0;
import com.sabre.services.sp.eab.v3_10.BaggageProvisions_type0;
import com.sabre.services.sp.eab.v3_10.EnhancedAirBookRQ;
import com.sabre.services.sp.eab.v3_10.EnhancedAirBookRS;
import com.sabre.services.sp.eab.v3_10.FlightSegment_type6;
import com.sabre.services.sp.eab.v3_10.HeaderInformation_type0;
import com.sabre.services.sp.eab.v3_10.Item_type1;
import com.sabre.services.sp.eab.v3_10.Meal_type0;
import com.sabre.services.sp.eab.v3_10.MiscInformation_type0;
import com.sabre.services.sp.eab.v3_10.PTC_FareBreakdown_type2;
import com.sabre.services.sp.eab.v3_10.PriceQuote_type0;
import com.sabre.services.sp.eab.v3_10.PrivateFare_type0;
import com.sabre.services.sp.eab.v3_10.Tax_type7;
import com.sabre.services.sp.eab.v3_10.TravelItineraryReadRS_type0;
import com.sabre.services.stl_messagecommon.v02_01.CompletionCodes;
import com.sabre.webservices.websvc.EnhancedAirBookService;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class SabreReviewManager extends SabreServiceManager {

	public TripInfo reviewFlight(TripInfo selectedTrip, AtomicBoolean isReviewSuccess) throws NoSeatAvailableException {
		TripInfo reviewedTrip = null;
		EnhancedAirBookServiceStub serviceStub = bindingService.getEab();
		try {
			listener.setType(AirUtils.getLogType("EnhancedAirBook", configuration));
			boolean isDomReturn =
					searchQuery.isDomesticReturn() && AirUtils.splitTripInfo(selectedTrip, false).size() == 2;
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.ENHANCED_AIR_BOOK, SabreConstants.CONVERSATION_ID);
			EnhancedAirBookRQ enhancedAirBookRQ = new EnhancedAirBookRQ();
			enhancedAirBookRQ.setHaltOnError(true);
			enhancedAirBookRQ.setVersion(getVesrionType0(SabreConstants.ENHANCED_RQ_VERSION));
			enhancedAirBookRQ.setOTA_AirBookRQ(SabreUtils.getOTAAirBookRQ(selectedTrip, searchQuery));
			enhancedAirBookRQ.setOTA_AirPriceRQ(getOTAAirPriceReq(selectedTrip, searchQuery, configuration));
			enhancedAirBookRQ.setPostProcessing(SabreUtils.getPostProcessing());
			EnhancedAirBookRS enhancedAirBookRS =
					serviceStub.enhancedAirBookRQ(enhancedAirBookRQ, messageHeader, getSecurity());

			isAnyWarning(enhancedAirBookRS.getApplicationResults());
			isAnyCritical(enhancedAirBookRS.getApplicationResults());
			if (enhancedAirBookRS.getApplicationResults().getStatus().equals(CompletionCodes.Complete)) {
				reviewedTrip = new GsonMapper<>(selectedTrip, TripInfo.class).convert();
				boolean isFareAvailable = checkFareAvailable(enhancedAirBookRS, reviewedTrip);
				isReviewSuccess.set(true);
				if (!isFareAvailable && isDomReturn) {
					log.info("Fare Difference on booking . search again and sell {} and isdomReturn {}", bookingId,
							isDomReturn);
					reviewedTrip = null;
					/**
					 * @implSpec :As Two Domestic Trips Involved , if sell available & fare change , let system do
					 *           search & sell again
					 */
					isReviewSuccess.set(false);
					return reviewedTrip;
				} else {
					log.info("Updating New price for booking id {} on sell ", bookingId);
					updateNewPriceOnTrip(enhancedAirBookRS, reviewedTrip);
				}
				updateTripInfo(selectedTrip, enhancedAirBookRS);
			} else {
				isReviewSuccess.set(false);
				throw new NoSeatAvailableException();
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return reviewedTrip;
	}


	private void updateTripInfo(TripInfo selectedTrip, EnhancedAirBookRS enhancedAirBookRS) {
		selectedTrip.getSegmentInfos().forEach(segment -> {
			FlightSegment_type6 flightSegment =
					getFlightSegmentFromItem(segment, enhancedAirBookRS.getTravelItineraryReadRS());
			if (flightSegment != null) {
				if (StringUtils.isNotEmpty(flightSegment.getUpdatedArrivalTime())) {
					segment.setArrivalTime(
							SabreUtils.convertToLocalTime(LocalDateTime.now(), flightSegment.getUpdatedArrivalTime()));
				}
				if (StringUtils.isNotEmpty(flightSegment.getUpdatedDepartureTime())) {
					segment.setDepartTime(SabreUtils.convertToLocalTime(LocalDateTime.now(),
							flightSegment.getUpdatedDepartureTime()));
				}
			}
		});
	}

	private FlightSegment_type6 getFlightSegmentFromItem(SegmentInfo segment,
			TravelItineraryReadRS_type0 travelItineraryReadRS) {
		if (travelItineraryReadRS != null && travelItineraryReadRS.getTravelItinerary() != null
				&& travelItineraryReadRS.getTravelItinerary().getItineraryInfo() != null
				&& travelItineraryReadRS.getTravelItinerary().getItineraryInfo().getReservationItems() != null
				&& ArrayUtils.isNotEmpty(travelItineraryReadRS.getTravelItinerary().getItineraryInfo()
						.getReservationItems().getItem())) {
			for (Item_type1 item : travelItineraryReadRS.getTravelItinerary().getItineraryInfo().getReservationItems()
					.getItem()) {
				for (FlightSegment_type6 flightSegment : item.getFlightSegment()) {
					if (segment.getDepartureAirportCode().equals(flightSegment.getOriginLocation().getLocationCode())
							&& segment.getArrivalAirportCode()
									.equals(flightSegment.getDestinationLocation().getLocationCode())
							&& DateFormatterHelper.format(segment.getDepartTime(), "yyyy-MM-dd hh:mm").replace(" ", "T")
									.equals(flightSegment.getDepartureDateTime())) {
						return flightSegment;
					}
				}
			}
		}
		return null;
	}

	private void updateNewPriceOnTrip(EnhancedAirBookRS enhancedAirBookRS, TripInfo selectedTrip) {
		FareType oldFareType = getOldFareType(selectedTrip);
		SegmentInfo segment = selectedTrip.getSegmentInfos().get(0);
		PriceMiscInfo priceMiscInfo = segment.getPriceInfo(0).getMiscInfo();
		resetSegmentPriceFares(selectedTrip);
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		Map<PaxType, FareDetail> fareDetails = new HashMap<>();
		AtomicInteger priceIndex = new AtomicInteger(0);
		priceInfo.setMiscInfo(priceMiscInfo);
		priceInfo.setFareIdentifier(oldFareType);
		PriceQuote_type0 priceQuote = enhancedAirBookRS.getOTA_AirPriceRS()[0].getPriceQuote();
		MiscInformation_type0 miscInfo = priceQuote.getMiscInformation();
		Map<String, AirlineInfo> validatingCarrier = new HashMap<>();
		if (miscInfo != null && miscInfo.getHeaderInformation() != null) {
			HeaderInformation_type0[] headers = miscInfo.getHeaderInformation();
			for (HeaderInformation_type0 headerInformation : headers) {
				if (headerInformation.getValidatingCarrier() != null) {
					String priceSolIndex = headerInformation.getSolutionSequenceNmbr();
					AirlineInfo validateCarrier =
							AirlineHelper.getAirlineInfo(headerInformation.getValidatingCarrier().getCode());
					validatingCarrier.put(priceSolIndex, validateCarrier);
				}
			}
		}
		for (AirItineraryPricingInfo_type0 airItineraryPricingInfo : priceQuote.getPricedItinerary()
				.getAirItineraryPricingInfo()) {
			String paxType = getPaxTypeCode(airItineraryPricingInfo.getPassengerTypeQuantity().getCode(), priceIndex);
			updatePriceAncillaries(paxType, airItineraryPricingInfo, selectedTrip, priceInfo);
			priceInfo = segment.getPriceInfo(0);
			fareDetails = priceInfo.getFareDetails();
			FareDetail fareDetail = fareDetails.getOrDefault(PaxType.getPaxType(paxType), new FareDetail());
			if (segment.getSegmentNum().intValue() == 0) {
				double baseFare = getConvertedBaseFare(airItineraryPricingInfo);
				double totalTax = getConvertedTotalFare(airItineraryPricingInfo);
				fareDetail.getFareComponents().put(FareComponent.BF, baseFare);
				fareDetail.getFareComponents().put(FareComponent.TF, totalTax);
				if (airItineraryPricingInfo.getItinTotalFare() != null
						&& airItineraryPricingInfo.getItinTotalFare().getTaxes() != null
						&& airItineraryPricingInfo.getItinTotalFare().getTaxes().getTax() != null) {
					for (Tax_type7 tax : airItineraryPricingInfo.getItinTotalFare().getTaxes().getTax()) {
						FareComponent fareComponent = Enums.getIfPresent(GDSFareComponentMapper.class, tax.getTaxCode())
								.or(GDSFareComponentMapper.TX).getFareComponent();
						String currencyCode = airItineraryPricingInfo.getItinTotalFare().getEquivFare() != null
								? airItineraryPricingInfo.getItinTotalFare().getEquivFare().getCurrencyCode()
								: airItineraryPricingInfo.getItinTotalFare().getBaseFare().getCurrencyCode();
						fareDetail.getFareComponents().put(fareComponent,
								fareDetail.getFareComponents().getOrDefault(fareComponent, 0.0)
										+ getAmountBasedOnCurrency(new BigDecimal(tax.getAmount()), currencyCode));
					}
				}
				PrivateFare_type0 privateFare = airItineraryPricingInfo.getItinTotalFare().getPrivateFare();
				if (privateFare != null && PRIVATE_FARE_INDICATOR.contains(privateFare.getInd())) {
					priceInfo.getMiscInfo().setIsPrivateFare(true);
				}
				RefundableType refundableType =
						isRefundable(airItineraryPricingInfo.getItinTotalFare().getNonRefundableInd());
				fareDetail.setRefundableType(refundableType.getRefundableType());
			}
			fareDetails.put(PaxType.getPaxType(paxType), fareDetail);
			if (MapUtils.isNotEmpty(validatingCarrier) && priceInfo.getMiscInfo().getPlatingCarrier() == null
					&& validatingCarrier.getOrDefault(airItineraryPricingInfo.getSolutionSequenceNmbr(),
							null) != null) {
				priceInfo.getMiscInfo()
						.setPlatingCarrier(validatingCarrier.get(airItineraryPricingInfo.getSolutionSequenceNmbr()));
			}
			priceIndex.getAndIncrement();
		}
		priceInfo.setFareDetails(fareDetails);
		segment.setPriceInfoList(Arrays.asList(priceInfo));
		updateMealInfo(enhancedAirBookRS.getTravelItineraryReadRS(), selectedTrip);
	}

	private void updateMealInfo(TravelItineraryReadRS_type0 travelItineraryReadRS, TripInfo selectedTrip) {

		if (travelItineraryReadRS != null && travelItineraryReadRS.getTravelItinerary() != null
				&& travelItineraryReadRS.getTravelItinerary().getItineraryInfo() != null
				&& travelItineraryReadRS.getTravelItinerary().getItineraryInfo().getReservationItems() != null
				&& ArrayUtils.isNotEmpty(travelItineraryReadRS.getTravelItinerary().getItineraryInfo()
						.getReservationItems().getItem())) {
			Item_type1[] items =
					travelItineraryReadRS.getTravelItinerary().getItineraryInfo().getReservationItems().getItem();
			boolean isFreeMeal = false;
			for (int index = 0; index < items.length && index < selectedTrip.getSegmentInfos().size(); index++) {
				if (ArrayUtils.isNotEmpty(items[index].getFlightSegment())
						&& ArrayUtils.isNotEmpty(items[index].getFlightSegment()[0].getMeal())) {
					Meal_type0[] meals = items[index].getFlightSegment()[0].getMeal();
					for (Meal_type0 meal : meals) {
						isFreeMeal = isFreeMeal || isFreeMeal(meal.getCode());
					}
				}
				Map<PaxType, FareDetail> fareDetails =
						selectedTrip.getSegmentInfos().get(index).getPriceInfo(0).getFareDetails();
				for (Entry<PaxType, FareDetail> entry : fareDetails.entrySet()) {
					FareDetail fareDetail = entry.getValue();
					fareDetail.setIsMealIncluded(isFreeMeal);
				}
			}
		}

	}

	private FareType getOldFareType(TripInfo selectedTrip) {
		return FareType.getEnumFromName(selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getFareType());
	}

	private void resetSegmentPriceFares(TripInfo selectedTrip) {
		selectedTrip.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.setPriceInfoList(null);
		});
	}

	public void updatePriceAncillaries(String paxType, AirItineraryPricingInfo_type0 airItineraryPricingInfo,
			TripInfo tripInfo, PriceInfo defaultPriceInfo) {
		AtomicInteger segmentIndex = new AtomicInteger(0);
		for (PTC_FareBreakdown_type2 ptcFareBreakdown : airItineraryPricingInfo.getPTC_FareBreakdown()) {
			if (ptcFareBreakdown.getFareBasis() != null
					&& StringUtils.isNotEmpty(ptcFareBreakdown.getFareBasis().getCode())
					&& StringUtils.isNotEmpty(ptcFareBreakdown.getFareBasis().getFilingCarrier())) {
				SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(segmentIndex.get());
				PriceInfo priceInfo1 = getPriceInfo(segmentInfo, defaultPriceInfo);
				PaxType paxType1 = PaxType.getPaxType(paxType);
				FareDetail fareDetail = priceInfo1.getFareDetail(paxType, new FareDetail());
				fareDetail.setCabinClass(CabinClass.getCabinClass(ptcFareBreakdown.getCabin()).orElse(null));
				fareDetail.setClassOfBooking(
						getClassOfBooking(airItineraryPricingInfo.getBaggageProvisions(), segmentInfo));
				fareDetail.setFareBasis(ptcFareBreakdown.getFareBasis().getCode());
				String baggageAllowance = ptcFareBreakdown.getFreeBaggageAllowance();
				if (StringUtils.isNotBlank(baggageAllowance) && baggageAllowance.contains("PC001")) {
					fareDetail.getBaggageInfo().setAllowance("1 PC");
				} else if (StringUtils.isNotBlank(baggageAllowance)) {
					fareDetail.getBaggageInfo().setAllowance(baggageAllowance);
				}
				priceInfo1.getFareDetails().put(paxType1, fareDetail);
				if (StringUtils.isNotEmpty(ptcFareBreakdown.getFareBasis().getCorporateID())) {
					String corporateAccountCode = ptcFareBreakdown.getFareBasis().getCorporateID();
					if (StringUtils.isNotEmpty(corporateAccountCode)) {
						priceInfo1.getMiscInfo().setIsPrivateFare(Boolean.TRUE);
						priceInfo1.getMiscInfo().setAccountCode(corporateAccountCode);
						log.info("Private fare applied on trip booking id {} and code {}", bookingId,
								corporateAccountCode);
					}
				}
				segmentInfo.setPriceInfoList(Arrays.asList(priceInfo1));
				segmentIndex.getAndIncrement();
			}
		}
	}

	public PriceInfo getPriceInfo(SegmentInfo segmentInfo, PriceInfo defaultPriceInfo) {
		if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
			return segmentInfo.getPriceInfo(0);
		}
		return new GsonMapper<>(defaultPriceInfo, PriceInfo.class).convert();
	}

	private boolean checkFareAvailable(EnhancedAirBookRS enhancedAirBookRS, TripInfo selectedTrip) {

		boolean isFareAvailable = true;
		AtomicDouble newFare = new AtomicDouble(0);

		AtomicDouble tripTotalFare = AirUtils.getAirlineTotalFare(selectedTrip);

		// doubt here , why it is array?
		PriceQuote_type0 priceQuote = enhancedAirBookRS.getOTA_AirPriceRS()[0].getPriceQuote();
		if (priceQuote != null) {
			AirItineraryPricingInfo_type0[] pricingInfos = priceQuote.getPricedItinerary().getAirItineraryPricingInfo();
			for (AirItineraryPricingInfo_type0 priceInfo : pricingInfos) {
				double pricingTotalFare = getConvertedTotalFare(priceInfo);
				newFare.getAndAdd(pricingTotalFare);
			}
		}
		double differenceFare = Math.abs(newFare.get() - tripTotalFare.get());
		if (newFare.get() >= 0
				&& (newFare.get() == tripTotalFare.get() || (differenceFare >= 0 && differenceFare <= 2))) {
			log.info("Sell fare Available for booking {} trip fare {}", bookingId, newFare.get());
		} else {
			log.info("Supplier Fare {} , Selected trip fare {} for bookingid {} sell found fare difference  ", newFare,
					tripTotalFare, bookingId);
			isFareAvailable = false;
		}
		return isFareAvailable;
	}

	public double getConvertedBaseFare(AirItineraryPricingInfo_type0 airItineraryPricingInfo) {
		if (airItineraryPricingInfo.getItinTotalFare() != null) {
			BigDecimal baseFare = new BigDecimal(airItineraryPricingInfo.getItinTotalFare().getEquivFare() != null
					? airItineraryPricingInfo.getItinTotalFare().getEquivFare().getAmount()
					: (airItineraryPricingInfo.getItinTotalFare().getBaseFare() != null)
							? airItineraryPricingInfo.getItinTotalFare().getBaseFare().getAmount()
							: null);
			String currencyCode = airItineraryPricingInfo.getItinTotalFare().getEquivFare() != null
					? airItineraryPricingInfo.getItinTotalFare().getEquivFare().getCurrencyCode()
					: (airItineraryPricingInfo.getItinTotalFare().getBaseFare() != null)
							? airItineraryPricingInfo.getItinTotalFare().getBaseFare().getCurrencyCode()
							: null;
			return getAmountBasedOnCurrency(baseFare, currencyCode);
		}
		return 0;
	}

	public double getConvertedTotalFare(AirItineraryPricingInfo_type0 priceInfo) {
		if (priceInfo.getItinTotalFare() != null && priceInfo.getItinTotalFare().getTotalFare() != null) {
			BigDecimal totalTax = new BigDecimal(priceInfo.getItinTotalFare().getTotalFare().getAmount());
			String currencyCode = priceInfo.getItinTotalFare().getTotalFare().getCurrencyCode();
			return getAmountBasedOnCurrency(totalTax, currencyCode);
		}
		return 0;
	}

	private RefundableType isRefundable(String nonRefundableInd) {
		if (("N").equalsIgnoreCase(nonRefundableInd)) {
			return RefundableType.NON_REFUNDABLE;
		} else {
			return RefundableType.REFUNDABLE;
		}
	}

	public void setSSRInfos(TripInfo tripInfo) {
		if (tripInfo != null) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				FlightBasicFact fact = FlightBasicFact.builder().build().generateFact(segmentInfo);
				BaseUtils.createFactOnUser(fact, bookingUser);
				Map<SSRType, List<? extends SSRInformation>> ssrInfos = AirUtils.getPreSSR(fact);
				if (MapUtils.isNotEmpty(ssrInfos)) {
					segmentInfo.setSsrInfo(ssrInfos);
				}
			}
		}
	}

	private String getClassOfBooking(BaggageProvisions_type0[] baggageProvisions, SegmentInfo segmentInfo) {
		if (ArrayUtils.isNotEmpty(baggageProvisions)) {
			for (BaggageProvisions_type0 baggageProvision : baggageProvisions) {
				if (baggageProvision.getAssociations() != null) {
					for (int index =
							0; index < baggageProvision.getAssociations().getResBookDesigCode().length; index++) {
						if (segmentInfo.getDepartureAirportCode()
								.equals(baggageProvision.getAssociations().getOriginLocation()[index].getLocationCode())
								&& segmentInfo.getArrivalAirportCode()
										.equals(baggageProvision.getAssociations().getDestinationLocation()[index]
												.getLocationCode())
								&& segmentInfo.getDepartTime().toLocalDate().toString().equals(
										baggageProvision.getAssociations().getDepartureDate()[index].getDate())) {
							return baggageProvision.getAssociations().getResBookDesigCode()[index].getString();
						}
					}
				}
			}
		}
		return null;
	}

}
