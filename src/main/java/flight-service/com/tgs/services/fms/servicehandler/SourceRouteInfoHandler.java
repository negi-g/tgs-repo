package com.tgs.services.fms.servicehandler;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.SourceRouteInfoFilter;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;
import com.tgs.services.fms.helper.SourceRouteInfoHelper;
import com.tgs.services.fms.jparepository.SourceRouteInfoService;
import com.tgs.services.fms.restmodel.SourceRouteInfoResponse;

@Slf4j
@Service
public class SourceRouteInfoHandler extends ServiceHandler<SourceRouteInfo, SourceRouteInfoResponse> {


	@Autowired
	SourceRouteInfoService routeInfoService;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		try {
			DbSourceRouteInfo dbSourceRouteInfo = null;
			if (Objects.nonNull(request.getId())) {
				dbSourceRouteInfo = routeInfoService.findOne(request.getId());
			}
			dbSourceRouteInfo = Optional.ofNullable(dbSourceRouteInfo).orElse(new DbSourceRouteInfo()).from(request);
			if (!dbSourceRouteInfo.toDomain().isSrcAndDestAreWild()
					&& StringUtils.equalsIgnoreCase(dbSourceRouteInfo.getSrc(), dbSourceRouteInfo.getDest())) {
				throw new CustomGeneralException(SystemError.SOURCE_ROUTE_SAME_SRC_DEST_NA);
			}
			dbSourceRouteInfo = routeInfoService.save(dbSourceRouteInfo);
			request.setId(dbSourceRouteInfo.getId());
			response.getSourceRouteInfos().add(dbSourceRouteInfo.toDomain());
		} catch (Exception e) {
			log.error("Unable to save Source route info {}", request, e);
			if (e.getCause() != null
					&& StringUtils.containsAny(e.getCause().getMessage(), "ConstraintViolationException")) {
				throw new CustomGeneralException(SystemError.SOURCE_ROUTE_ALREADY_EXISTS);
			}
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}


	public SourceRouteInfoResponse fetchSourceRoutes(SourceRouteInfoFilter filter) {
		SourceRouteInfoResponse routeInfoResponse = new SourceRouteInfoResponse();
		List<SourceRouteInfo> sourceRouteinfo = routeInfoService.findAll(filter);
		routeInfoResponse.setSourceRouteInfos(sourceRouteinfo);
		return routeInfoResponse;
	}

	public BaseResponse updateSourceRoute(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbSourceRouteInfo dbSourceRouteInfo = routeInfoService.findOne(id);
		if (Objects.nonNull(dbSourceRouteInfo)) {
			dbSourceRouteInfo.setEnabled(status);
			routeInfoService.save(dbSourceRouteInfo);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}
}
