package com.tgs.services.fms.mapper;

import java.time.LocalDateTime;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.analytics.AirSearchRestrictionQuery;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AirSearchRestrictionQueryMapper extends Mapper<AirSearchRestrictionQuery> {

	protected int sourceid;
	protected int threadcount;
	protected int maxallowedthread;
	protected int timeoutcount;
	protected int maxtimeoutallowed;
	protected String searchid;
	protected String analyticstype;
	protected User user;
	protected ContextData contextData;


	@Override
	protected void execute() throws CustomGeneralException {

		if (output == null) {
			output = AirSearchRestrictionQuery.builder().build();
		}
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		output.setThreadcount(threadcount);
		output.setMaxallowedthread(maxallowedthread);
		output.setMaxtimeoutallowed(maxtimeoutallowed);
		output.setTimeoutcount(timeoutcount);
		output.setSearchid(searchid);
		output.setAnalyticstype(analyticstype);
		output.setSourcename(AirSourceType.getAirSourceType(sourceid).name());
	}

}
