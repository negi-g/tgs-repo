package com.tgs.services.fms.sources.airasia;


import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.tempuri.Booking;
import org.tempuri.BookingServiceBasicHttpBinding_IBookingServiceStub;
import org.tempuri.SessionServiceBasicHttpBinding_ISessionServiceStub;

@Slf4j
@Service
public class AirAsiaCancellationFactory extends AbstractAirCancellationFactory {

	protected SoapRequestResponseListner listener = null;

	protected AirAsiaBindingService bindingService;

	protected SessionServiceBasicHttpBinding_ISessionServiceStub sessionStub;

	protected BookingServiceBasicHttpBinding_IBookingServiceStub bookingStub;

	public AirAsiaCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		super(bookingSegments, order, supplierConf);
	}

	public void initialize() {
		bindingService = AirAsiaBindingService.builder().cacheCommunicator(cachingComm).user(bookingUser).build();
		sessionStub = bindingService.getSessionStub(supplierConf, null);
		bookingStub = bindingService.getBookingStub(supplierConf, null);
	}

	@Override
	public boolean releaseHoldPNR() {
		boolean isCancelSuccess = false;
		AirAsiaSessionManager sessionManager = null;
		try {
			initialize();
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			sessionManager = AirAsiaSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionStub(sessionStub).bookingId(bookingId).bookingUser(bookingUser).build();
			sessionManager.init();
			sessionManager.openSession();
			AirAsiaBookingRetrieveManager retrieveManager = AirAsiaBookingRetrieveManager.builder()
					.configuration(supplierConf).sessionStub(sessionManager.sessionStub)
					.sessionSignature(sessionManager.sessionSignature).criticalMessageLogger(criticalMessageLogger)
					.bookingStub(bookingStub).listener(listener).bookingUser(bookingUser).build();
			retrieveManager.init();
			Booking booking = retrieveManager.getBookingResponse(pnr);
			isCancelSuccess = retrieveManager.isCancelled(booking);
			if (!isCancelSuccess && retrieveManager.isValidPNRBooking(booking, pnr)) {
				double totalFare = booking.getBookingSum().getTotalCost().doubleValue();
				AirAsiaCancellationManager cancellationManager = AirAsiaCancellationManager.builder()
						.configuration(supplierConf).sessionSignature(sessionManager.getSessionSignature())
						.bookingStub(bookingStub).listener(listener).bookingUser(bookingUser).build();
				cancellationManager.init();
				cancellationManager.cancelAll();
				AirAsiaBookingManager bookingManager = AirAsiaBookingManager.builder().configuration(supplierConf)
						.sessionSignature(sessionManager.getSessionSignature()).bookingId(bookingId)
						.sourceConfiguration(sourceConfiguration).listener(listener).bookingStub(bookingStub)
						.criticalMessageLogger(criticalMessageLogger).totalFare(totalFare).bookingUser(bookingUser)
						.build();
				bookingManager.initialize(bookingSegments.getSegmentInfos());
				bookingManager.initializeTravellerInfo(
						bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo());
				double amountToBeCalculated = booking.getBookingSum().getTotalCost().doubleValue();
				if (!isHoldBooking) {
					// Booking which done payment and do cancel for reverting payment
					// only if payment done
					bookingManager.setTotalFare(-1 * amountToBeCalculated);
					bookingManager.addPaymentToBooking();
				}
				bookingManager.commitBooking(false, pnr, null);
				booking = retrieveManager.getBookingResponse(pnr);
				isCancelSuccess = cancellationManager.checkCancelStatus(booking);
			}
		} finally {
			sessionManager.closeSession();
			storeSessionInfo();
		}
		return isCancelSuccess;
	}

	private void storeSessionInfo() {
		bindingService = AirAsiaBindingService.builder().cacheCommunicator(cachingComm).user(bookingUser).build();
		bindingService.storeSessionMetaInfo(supplierConf, SessionServiceBasicHttpBinding_ISessionServiceStub.class,
				sessionStub);
		bindingService.storeSessionMetaInfo(supplierConf, BookingServiceBasicHttpBinding_IBookingServiceStub.class,
				bookingStub);
	}
}
