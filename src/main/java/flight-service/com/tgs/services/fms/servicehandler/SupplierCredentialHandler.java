package com.tgs.services.fms.servicehandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.base.utils.FieldTransform;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.fms.datamodel.supplier.SupplierConfigurationFilter;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.datamodel.supplier.SupplierRuleFilter;
import com.tgs.services.fms.dbmodel.DbSupplierInfo;
import com.tgs.services.fms.dbmodel.DbSupplierRule;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.jparepository.SupplierInfoService;
import com.tgs.services.fms.jparepository.SupplierRuleService;
import com.tgs.services.fms.restmodel.SupplierInfoResponse;
import com.tgs.utils.common.UpdateMaskedField;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SupplierCredentialHandler extends ServiceHandler<SupplierInfo, SupplierInfoResponse> {

	@Autowired
	SupplierInfoService supplierService;

	@Autowired
	SupplierRuleService supplierRuleService;

	@Autowired
	SyncService syncService;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		DbSupplierInfo supplierDbModel = null;
		if (request.getId() != null) {
			supplierDbModel = supplierService.findById(request.getId());
			UpdateMaskedField.update(request);
		}
		supplierDbModel = Optional.ofNullable(supplierDbModel).orElseGet(() -> new DbSupplierInfo()).from(request);
		supplierDbModel = supplierService.save(supplierDbModel);
		request = supplierDbModel.toDomain();
		syncService.sync("fms", supplierDbModel.toDomain());
	}

	@Override
	public void afterProcess() throws Exception {
		FieldTransform.mask(request);
		response.getSupplierInfos().add(request);
	}

	public List<SupplierRule> getSupplierRules(SupplierRuleFilter ruleFilter) {
		log.info("Fetching supplier rule from database ");
		List<DbSupplierRule> supplierRules = supplierRuleService.findAll(ruleFilter);
		log.info("fetched supplier rule from database ");
		List<SupplierRule> rules = DbSupplierRule.toDomainList(supplierRules);
		log.info("converted from dbmodel to datamodel");
		return rules;
	}

	public SupplierRule saveSupplierRule(SupplierRule supplierRule) {
		DbSupplierRule supplierRuleDbModel = null;
		supplierRule.setExitOnMatch(false);
		if (Objects.nonNull(supplierRule.getId())) {
			supplierRuleDbModel = supplierRuleService.findById(supplierRule.getId());
		}
		supplierRuleDbModel =
				Optional.ofNullable(supplierRuleDbModel).orElseGet(() -> new DbSupplierRule()).from(supplierRule);
		Integer sourceId =
				SupplierConfigurationHelper.getSupplierInfo(supplierRuleDbModel.getSupplierId()).getSourceId();
		if (isGeneralInvRule(supplierRule, sourceId))
			validateInvGeneralRule(supplierRule.getId());
		supplierRuleDbModel.setSourceId(sourceId);
		supplierRule = supplierRuleService.save(supplierRuleDbModel).toDomain();
		syncService.sync("fms", supplierRule);
		return supplierRule;

	}

	public BaseResponse deleteSupplierRule(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbSupplierRule supplierRule = supplierRuleService.findById(id);
		if (Objects.nonNull(supplierRule)) {
			supplierRule.setDeleted(Boolean.TRUE);
			supplierRule.setEnabled(Boolean.FALSE);
			supplierRuleService.save(supplierRule);
			syncService.sync("fms", null, "DELETE");
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updateSupplierRule(Long id, boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbSupplierRule supplierRule = supplierRuleService.findById(id);
		if (Objects.nonNull(supplierRule)) {
			if (status && isGeneralInvRule(supplierRule.toDomain(), supplierRule.getSourceId())) {
				validateInvGeneralRule(supplierRule.getId());
			}
			supplierRule.setEnabled(status);
			supplierRuleService.save(supplierRule);
			syncService.sync("fms", supplierRule);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	private boolean isGeneralInvRule(SupplierRule supplierRule, Integer sourceId) {
		if (AirSourceType.DEALINVENTORY.getSourceId() == sourceId) {
			try {
				List<String> inclusionNonNullFields =
						TgsObjectUtils.getNotNullFields(supplierRule.getInclusionCriteria(), false, true);
				List<String> exclusionNonNullFields =
						TgsObjectUtils.getNotNullFields(supplierRule.getExclusionCriteria(), false, true);
				boolean isInclusionEmpty = CollectionUtils.isEmpty(inclusionNonNullFields);
				boolean isExclusionEmpty = CollectionUtils.isEmpty(exclusionNonNullFields);
				return isInclusionEmpty && isExclusionEmpty;
			} catch (Exception e) {
				log.error("Exception occurred while validating supplier rule {} with exception {}", supplierRule, e);
				throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
			}
		}
		return false;
	}

	public void validateInvGeneralRule(long supplierRuleId) {
		/**
		 * 	Validates if atleast one deal inventory supplier rule which is not deleted is present the error is thrown.
		 */
		List<DbSupplierRule> dealInventoryRules = new ArrayList<>();
		SupplierRuleFilter filter = SupplierRuleFilter.builder()
				.sourceIds(Arrays.asList(AirSourceType.DEALINVENTORY.getSourceId())).deleted(false).build();
		dealInventoryRules = supplierRuleService.findAll(filter);

		if (CollectionUtils.isNotEmpty(dealInventoryRules)) {
			for (DbSupplierRule rule : dealInventoryRules) {
				if (rule.getId() != supplierRuleId && isGeneralInvRule(rule.toDomain(), rule.getSourceId())) {
					throw new CustomGeneralException(SystemError.INVENTORY_SUPPLIER_ALREADY_EXISTS);
				}
			}
		}
	}

	public SupplierInfoResponse getSupplierInfo(SupplierConfigurationFilter configurationFilter) {
		SupplierInfoResponse supplierInfoResponse = new SupplierInfoResponse();
		supplierService.findAll(configurationFilter).forEach(supplier -> {
			supplierInfoResponse.getSupplierInfos().add(supplier.toDomain());
		});
		return FieldTransform.mask(supplierInfoResponse);
	}

	public BaseResponse deleteSupplierInfo(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbSupplierInfo supplierInfo = supplierService.findById(id);
		if (Objects.nonNull(supplierInfo)) {
			supplierInfo.setEnabled(Boolean.FALSE);
			supplierInfo.setDeleted(Boolean.TRUE);
			supplierService.save(supplierInfo);
			syncService.sync("fms", null, "DELETE");
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updateStatusSupplierInfo(Long id, boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbSupplierInfo supplierInfo = supplierService.findById(id);
		if (Objects.nonNull(supplierInfo)) {
			supplierInfo.setEnabled(status);
			supplierService.save(supplierInfo);
			syncService.sync("fms", null);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

}
