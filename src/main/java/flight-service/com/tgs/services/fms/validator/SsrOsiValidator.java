package com.tgs.services.fms.validator;

import com.tgs.services.fms.datamodel.SsrOsiConditions;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;

@Service
public class SsrOsiValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {

		if (iRuleOutput == null) {
			errors.rejectValue(fieldName, SystemError.NULL_VALUE.errorCode(), SystemError.NULL_VALUE.getMessage());
			return;
		}

		if (iRuleOutput instanceof SsrOsiConditions) {
			SsrOsiConditions ssrOsi = (SsrOsiConditions) iRuleOutput;

			if (BooleanUtils.isTrue(ssrOsi.getIsOsiMandatory())) {

				if (!(BooleanUtils.isTrue(ssrOsi.getIsOsiRequired())
						&& BooleanUtils.isTrue(ssrOsi.getIsSeaSsrRequired()))) {
					errors.rejectValue(fieldName, SystemError.CONFLICTING_POLICIES.errorCode(),
							SystemError.CONFLICTING_POLICIES.getMessage());
					return;
				}
			}

			if (BooleanUtils.isTrue(ssrOsi.getIsOsiMandatory()) && (BooleanUtils.isFalse(ssrOsi.getIsOsiRequired())
					|| BooleanUtils.isFalse(ssrOsi.getIsSeaSsrRequired()))) {
				errors.rejectValue(fieldName, SystemError.OSI_MANDATORY.errorCode(),
						SystemError.OSI_MANDATORY.getMessage());
				return;
			}

		}
	}

}
