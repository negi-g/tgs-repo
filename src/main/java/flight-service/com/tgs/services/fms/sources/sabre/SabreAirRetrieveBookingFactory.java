package com.tgs.services.fms.sources.sabre;

import com.tgs.services.base.SoapRequestResponseListner;
import org.springframework.stereotype.Service;
import com.sabre.services.res.tir.v3_9.TravelItineraryReadRS;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SabreAirRetrieveBookingFactory extends AbstractAirBookingRetrieveFactory {

	protected SabreBindingService bindingService;

	private SoapRequestResponseListner listener;

	SabreAirRetrieveBookingFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		listener = new SoapRequestResponseListner(pnr, "", supplierConf.getSupplierId());
		SabreTokenServiceManager tokenManager =
				SabreTokenServiceManager.builder().listener(listener).configuration(supplierConf).bookingId(pnr)
						.bindingService(bindingService).bookingUser(bookingUser).build();
		String binaryToken = tokenManager.generateSessionToken();
		SabreBookingManager bookingManager =
				SabreBookingManager.builder().binaryToken(binaryToken).configuration(supplierConf).listener(listener)
						.bindingService(bindingService).bookingUser(bookingUser).build();
		TravelItineraryReadRS travelItineraryResponse = bookingManager.travelItineraryReadLLS(pnr);
		SabreBookingRetrieveManager retrieveManager = SabreBookingRetrieveManager.builder().supplierRefId(pnr)
				.configuration(supplierConf).sourceConfiguration(sourceConfiguration).listener(listener)
				.moneyExchnageComm(moneyExchangeComm).bookingUser(getBookingUser()).bookingUser(bookingUser).build();
		pnrBooking = retrieveManager.retrieveBooking(travelItineraryResponse);
		return pnrBooking;
	}

}
