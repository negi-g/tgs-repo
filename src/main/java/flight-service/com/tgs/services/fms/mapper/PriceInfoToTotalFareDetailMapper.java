package com.tgs.services.fms.mapper;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.List;
import java.util.Map;

/**
 * This class uses for merging priceInfos<PaxType::FareDetail> to TotalFareDetail{final view level fare components to
 * consumer}
 **/
@Builder
@Getter
@Setter
public class PriceInfoToTotalFareDetailMapper extends Mapper<PriceInfo> {

	boolean addMarkupInTF;

	Map<PaxType, Integer> paxInfo;

	List<PriceInfo> priceInfos;

	@Override
	protected void execute() throws CustomGeneralException {
		output = PriceInfo.builder().build();
		output.setTotalFareDetail(new FareDetail());
		double totalMarkup = 0;
		for (PriceInfo priceInfo : priceInfos) {
			/**
			 * Combining fareComponents of all priceInfos
			 */
			for (Map.Entry<PaxType, FareDetail> paxFareDetail : priceInfo.getFareDetails().entrySet()) {
				for (Map.Entry<FareComponent, Double> fareComponent : paxFareDetail.getValue().getFareComponents()
						.entrySet()) {
					double existingValue =
							output.getTotalFareDetail().getFareComponents().getOrDefault(fareComponent.getKey(), 0.0);
					double newValue = fareComponent.getValue() * paxInfo.get(paxFareDetail.getKey());
					output.getTotalFareDetail().getFareComponents().put(fareComponent.getKey(),
							(existingValue + newValue));
				}

				// Combining AddlFareComponents of all priceInfos
				if (paxFareDetail.getValue().getAddlFareComponents() != null) {
					for (FareComponent parentFc : paxFareDetail.getValue().getAddlFareComponents().keySet()) {
						for (Map.Entry<FareComponent, Double> fareComponent : paxFareDetail.getValue()
								.getAddlFareComponents().get(parentFc).entrySet()) {
							double existingValue = output.getTotalFareDetail().getAddlFareComponents(parentFc)
									.getOrDefault(fareComponent.getKey(), 0.0);
							double newValue = fareComponent.getValue() * paxInfo.get(paxFareDetail.getKey());
							output.getTotalFareDetail().getAddlFareComponents(parentFc).put(fareComponent.getKey(),
									(existingValue + newValue));
							if (FareComponent.MU.equals(fareComponent.getKey())) {
								totalMarkup += output.getTotalFareDetail().getAddlFareComponents().get(parentFc)
										.getOrDefault(fareComponent.getKey(), 0d);
							}
						}
					}
				}
			}
		}
		if (addMarkupInTF) {
			output.getTotalFareDetail().getFareComponents().put(FareComponent.TF,
					output.getTotalFareDetail().getFareComponents().get(FareComponent.TF) + totalMarkup);
		}
	}
}
