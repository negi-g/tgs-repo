package com.tgs.services.fms.sources.flightroutes24;

import lombok.Getter;

@Getter
public enum FlightRoutes24CabinClass {

	ECONOMY("Y"), PREMIUM_ECONOMY("P"), BUSINESS("C"), FIRST("F");
	private String value;

	private FlightRoutes24CabinClass(String value) {
		this.value = value;
	}

}
