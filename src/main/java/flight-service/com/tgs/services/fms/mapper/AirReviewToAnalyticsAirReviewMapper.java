package com.tgs.services.fms.mapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import com.tgs.services.base.datamodel.FareChangeAlert;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.PriceInfo;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.analytics.AnalyticsAirQuery;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirAnalyticsUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@Builder
public class AirReviewToAnalyticsAirReviewMapper extends Mapper<AnalyticsAirQuery> {

	private User user;
	private ContextData contextData;
	private String bookingId;
	private List<TripInfo> tripInfos;
	private AirSearchQuery searchQuery;
	private List<Alert> alerts;
	private List<String> errorMessages;
	private Double totalbookingfare;
	private Double farediff;
	private String bookedfaretype;
	private List<TripInfo> oldTripInfos;
	private Long priceInfoCreatedMinutes;

	@Override
	protected void execute() throws CustomGeneralException {

		output = output != null ? output : AnalyticsAirQuery.builder().build();
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		output = AnalyticsAirQueryMapper.builder().searchQuery(searchQuery).build().setOutput(output).convert();

		output.setBookingid(bookingId);
		output.setTotalbookingfare(totalbookingfare);
		output.setBookedfaretype(bookedfaretype);
		output.setSearchCabinClass(AirAnalyticsUtils.getCabinClass(searchQuery));
		output.setFarediff(farediff);

		if (CollectionUtils.isNotEmpty(tripInfos)) {
			List<SegmentInfo> segmentInfos = AirUtils.getSegmentInfos(tripInfos);
			output.setAirlines(AirAnalyticsUtils.getAirlines(segmentInfos));
			output.setBookingclass(AirAnalyticsUtils.getBookingClass(segmentInfos));
			output.setFarebasis(AirAnalyticsUtils.getFareBasis(segmentInfos));
			output.setFaretype(AirAnalyticsUtils.getFareType(segmentInfos));
			output.setFlightnumbers(AirAnalyticsUtils.getFlightNumbers(segmentInfos));
			output.setCabinclass(AirAnalyticsUtils.getCabinClass(segmentInfos));
			output.setSourcename(AirAnalyticsUtils.getSourceName(segmentInfos));
			output.setSuppliername(AirAnalyticsUtils.getSupplierName(segmentInfos));
			output.setSupplierdesc(AirAnalyticsUtils.getSupplierDesc(segmentInfos));
			output.setTotalairlinefare(AirAnalyticsUtils.getTotalAirlineFare(segmentInfos));
			output.setCommission(AirAnalyticsUtils.getTotalCommission(segmentInfos));
			output.setCommissionid(AirAnalyticsUtils.getCommissionId(segmentInfos));
			output.setRefundabletype(AirAnalyticsUtils.getRefundableType(segmentInfos));
			output.setSuppliersessionid(AirAnalyticsUtils.getSupplierSession(bookingId, user));
			output.setOrgfaretype(AirAnalyticsUtils.getOriginalFareType(segmentInfos));
			output.setPriceinfocreatedon(AirAnalyticsUtils.getPriceCreatedOn(segmentInfos));
		}

		if (CollectionUtils.isNotEmpty(oldTripInfos)) {
			List<SegmentInfo> oldSegmentInfos = AirUtils.getSegmentInfos(oldTripInfos);
			output.setOldbookingclass(AirAnalyticsUtils.getBookingClass(oldSegmentInfos));
			output.setOldtotalairlinefare(AirAnalyticsUtils.getTotalAirlineFare(oldSegmentInfos));
		}

		if (CollectionUtils.isNotEmpty(alerts)) {
			output.setAlerts(getAlerts());
			if (isFareAlert(alerts) && CollectionUtils.isNotEmpty(oldTripInfos)) {
				List<SegmentInfo> oldSegments = AirUtils.getSegmentInfos(oldTripInfos);
				List<SegmentInfo> newSegments = AirUtils.getSegmentInfos(tripInfos);
				StringJoiner joiner = new StringJoiner(" & ");
				for (int i = 0; i < oldSegments.size(); i++) {
					PriceInfo oldPrice = oldSegments.get(i).getPriceInfo(0);
					if (StringUtils.isBlank(output.getDiffsearchid())) {
						output.setDiffsearchid(oldPrice.getRequestId());
					}
					PriceInfo newPrice = newSegments.get(i).getPriceInfo(0);
					oldPrice.getFareDetails().forEach((paxType, fd) -> {
						Map<FareComponent, Double> newPriceComp = newPrice.getFareDetail(paxType).getFareComponents();
						fd.getFareComponents().forEach((fareComponent, amount) -> {
							if (Double.compare(amount, newPriceComp.getOrDefault(fareComponent, 0D)) != 0) {
								joiner.add(StringUtils.join(fareComponent, amount, "-",
										newPriceComp.getOrDefault(fareComponent, 0D)));
							}
						});
					});
				}
				output.setFarediffcomponent(joiner.toString());
			}
		}

		if (CollectionUtils.isNotEmpty(errorMessages)) {
			output.setErrormsg(errorMessages.toString());
		}

		output.setPriceinfocreatedminutes(priceInfoCreatedMinutes);
	}

	private boolean isFareAlert(List<Alert> alerts) {
		return alerts.stream()
				.filter(alert -> StringUtils.equalsIgnoreCase(alert.getType(), AlertType.FAREALERT.name())).findFirst()
				.isPresent();
	}

	private String getAlerts() {
		List<String> alert = new ArrayList<>();
		alerts.forEach(alert1 -> {
			alert.add(alert1.getType());
		});
		return alert.toString();
	}
}
