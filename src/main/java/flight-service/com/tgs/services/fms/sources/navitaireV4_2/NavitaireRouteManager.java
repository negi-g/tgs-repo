package com.tgs.services.fms.sources.navitaireV4_2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import com.navitaire.schemas.webservices.datacontracts.utilities.ArrayOfMarket;
import com.navitaire.schemas.webservices.datacontracts.utilities.Market;
import com.navitaire.schemas.webservices.servicecontracts.utilitiesservice.GetMarketListRequest;
import com.navitaire.schemas.webservices.servicecontracts.utilitiesservice.GetMarketListResponse;
import com.tgs.services.base.SpringContext;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;
import com.tgs.services.fms.jparepository.SourceRouteInfoService;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@SuperBuilder
public class NavitaireRouteManager extends NavitaireServiceManager {

	SourceRouteInfoService routeInfoService;

	public void addRouteInfoToSystem(List<SourceRouteInfo> routes) {
		routeInfoService =
				(SourceRouteInfoService) SpringContext.getApplicationContext().getBean("sourceRouteInfoService");
		try {
			GetMarketListRequest marketListRq = new GetMarketListRequest();
			listener.setType("MarketList");
			utilityStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			GetMarketListResponse marketListResponse = utilityStub.getMarketList(marketListRq, getContractVersion(),
					getExceptionStackTrace(), getMessageContractversion(), getSignature());
			parseMarketList(marketListResponse, routes);
		} catch (Exception e) {
			log.error("Unable to Add Route into System {}", e);
		} finally {
			utilityStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}

	}

	private void parseMarketList(GetMarketListResponse marketListResponse, List<SourceRouteInfo> routes) {
		if (marketListResponse != null && marketListResponse.getMarketList() != null
				&& ArrayUtils.isNotEmpty(marketListResponse.getMarketList().getMarket())) {
			Map<String, SourceRouteInfo> mapDbSourceRoute = new HashMap<>();
			for (SourceRouteInfo routeInfo : DbSourceRouteInfo
					.toDomainList(routeInfoService.findAllBySourceId(navitaireAirline.getSourceId()))) {
				mapDbSourceRoute.put(getKey(routeInfo), routeInfo);
			}

			ArrayOfMarket marketList = marketListResponse.getMarketList();
			for (Market market : marketList.getMarket()) {
				SourceRouteInfo route = new SourceRouteInfo();
				route.setSrc(market.getLocationCode());
				route.setEnabled(true);
				route.setDest(market.getTravelLocationCode());
				route.setSourceId(navitaireAirline.getSourceId());
				if (!mapDbSourceRoute.containsKey(getKey(route))) {
					routes.add(route);
				}
			}
			log.info("Size of the new Airport List that is to be addeded for source {} is : {}",
					navitaireAirline.getSourceId(), routes.size());
		}
	}

	private String getKey(SourceRouteInfo route) {
		return StringUtils.join(route.getSrc(), "_", route.getDest());
	}
}
