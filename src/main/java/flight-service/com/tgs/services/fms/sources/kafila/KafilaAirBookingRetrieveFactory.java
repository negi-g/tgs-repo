package com.tgs.services.fms.sources.kafila;


import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;

@Service
public class KafilaAirBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	protected RestAPIListener listener = null;

	protected FlightAPIURLRuleCriteria apiUrls;

	protected String tokenId;

	protected KafilaBindingService bindingService;

	public KafilaAirBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}

	public void initialize() {
		if (listener == null) {
			listener = new RestAPIListener(pnr);
		}
		bindingService = KafilaBindingService.builder().configuration(supplierConf).build();
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		initialize();

		KafilaBookingRetrieveManager bookingRetrieveManager = KafilaBookingRetrieveManager.builder().listener(listener)
				.supplierConfiguration(supplierConf).bindingService(bindingService).pnr(pnr).build();
		pnrBooking = bookingRetrieveManager.getPnrDetails(bookingRetrieveManager.getBookingDetailsResponse());
		return pnrBooking;
	}


}

