package com.tgs.services.fms.sources.sqiva;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoPNRFoundException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class SqivaBookingRetrieveManager extends SqivaServiceManager {

	public JSONObject getAllBookInfo_V2(String pnr) {
		HttpUtils httpUtils = HttpUtils.builder().build();
		JSONObject bookInfoResponse = null;
		try {
			Map<String, String> bookInfoParams = getBookInfoParams(pnr);
			bookInfoResponse = bindingService.getALLBookInfo_V2(bookInfoParams, httpUtils);
			boolean isAnyError = isAnyError(bookInfoResponse);
			if (isAnyError) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		}
		return bookInfoResponse;
	}

	private Map<String, String> getBookInfoParams(String pnr) {
		Map<String, String> bookInfoParams = new HashMap<String, String>();
		bookInfoParams.put(SqivaConstant.BOOK_CODE, pnr);
		return bookInfoParams;
	}

	public AirImportPnrBooking parseRetrieveResponse(JSONObject retrieveResponse, String pnr) {
		AirImportPnrBooking pnrBooking = null;
		JSONObject bookPriceDetailInfo = getBookPriceDetailInfo_V2(pnr);
		if (retrieveResponse != null && bookPriceDetailInfo != null) {
			List<TripInfo> tripInfos = fetchTripInfo(retrieveResponse, bookPriceDetailInfo, pnr);
			// GstInfo gstInfo = fetchGSTInfo(retrieveResponse);
			DeliveryInfo deliveryInfo = fetchDeliveryInfo(retrieveResponse);
			pnrBooking = AirImportPnrBooking.builder().tripInfos(tripInfos).deliveryInfo(deliveryInfo).build();
		} else {
			throw new NoPNRFoundException(AirSourceConstants.AIR_PNR_JOURNEY_UNAVAILABLE + pnr);
		}
		return pnrBooking;
	}

	private List<TripInfo> fetchTripInfo(JSONObject retrieveResponse, JSONObject bookPriceDetailInfo, String pnr) {
		List<TripInfo> tripInfos = new ArrayList<>();
		JSONArray paxList = retrieveResponse.getJSONArray(SqivaConstant.PAX_LIST);
		List<FlightTravellerInfo> travellerInfos = getTravellerInfoFromPaxList(paxList, pnr);
		List<SegmentInfo> segmentInfos = getSegmentInfoFromRouteInfo(retrieveResponse, travellerInfos);
		updateTravellerFare(segmentInfos, bookPriceDetailInfo);
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(segmentInfos);
		tripInfos.add(tripInfo);
		return tripInfos;
	}

	private void updateTravellerFare(List<SegmentInfo> segmentInfos, JSONObject bookPriceDetailInfo) {
		JSONArray detailPriceArray = bookPriceDetailInfo.getJSONArray(SqivaConstant.DETAIL_PRICE);
		segmentInfos.forEach(segment -> {
			segment.getTravellerInfo().forEach(traveller -> {
				for (int index = 0; index < detailPriceArray.length(); index++) {
					JSONArray detailPrice = detailPriceArray.getJSONArray(index);
					String travellerName = detailPrice.getString(SqivaConstant.DETAIL_PRICE_TRAVELLER_NAME_INDEX);
					String departureAirportCode =
							detailPrice.getString(SqivaConstant.DETAIL_PRICE_DEPARTURE_CODE_INDEX);
					String arrivalAirportCode = detailPrice.getString(SqivaConstant.DETAIL_PRICE_ARRIVAL_CODE_INDEX);
					String segmentTravellerName =
							StringUtils.join(traveller.getFirstName(), " ", traveller.getLastName());
					if (segment.getDepartureAirportCode().equals(departureAirportCode)
							&& segment.getArrivalAirportCode().equals(arrivalAirportCode)
							&& segmentTravellerName.equalsIgnoreCase(travellerName)) {
						String fareComponent = detailPrice.getString(SqivaConstant.DETAIL_PRICE_FARE_COMPONENT_INDEX);
						Double fareAmount = detailPrice.getDouble(SqivaConstant.DETAIL_PRICE_FARE_AMOUNT_INDEX);
						switch (fareComponent) {
							case "Basic Fare":
								traveller.getFareDetail().getFareComponents().put(FareComponent.BF, fareAmount);
								break;
							case "Fare Vat":
								traveller.getFareDetail().getFareComponents().put(FareComponent.AGST, fareAmount);
								break;
							case "Prepaid Baggage":
								if (fareAmount != 0) {
									SSRInformation baggageSSR = new SSRInformation();
									baggageSSR.setCode(fareComponent);
									baggageSSR.setDesc(fareComponent);
									baggageSSR.setAmount(fareAmount);
									traveller.setSsrBaggageInfo(baggageSSR);
								}
								break;
							case "Meal / Beverage Fee":
							case "Meal":
								SSRInformation mealSSR = new SSRInformation();
								mealSSR.setCode(fareComponent);
								mealSSR.setDesc(fareComponent);
								mealSSR.setAmount(fareAmount);
								traveller.setSsrMealInfo(mealSSR);
								break;
							case "User Development Fee":
								traveller.getFareDetail().getFareComponents().put(FareComponent.UDF, fareAmount);
								break;
							default:
								Double oldValue = traveller.getFareDetail().getFareComponents()
										.getOrDefault(FareComponent.AT, 0d);
								traveller.getFareDetail().getFareComponents().put(FareComponent.AT,
										oldValue + fareAmount);
						}
					}
				}
				Double totalAirLineFare = traveller.getFareDetail().getAirlineFare();
				traveller.getFareDetail().getFareComponents().put(FareComponent.TF, totalAirLineFare);
			});
		});
	}

	private List<SegmentInfo> getSegmentInfoFromRouteInfo(JSONObject retrieveResponse,
			List<FlightTravellerInfo> travellerInfos) {

		JSONArray routeInfo = retrieveResponse.getJSONArray(SqivaConstant.ROUTE_INFO);
		JSONArray ssrDetailArray = retrieveResponse.getJSONArray(SqivaConstant.SSR_DETAIL);
		JSONArray bookAncillaryFeeArray = retrieveResponse.getJSONArray(SqivaConstant.BOOK_ANCILLARY_FEE);
		JSONArray routeDetailArray = retrieveResponse.getJSONArray(SqivaConstant.ROUTE_DETAIL);

		List<SegmentInfo> segmentInfos = new ArrayList<>();
		int segmentNum = 0;
		for (int index = 0; index < routeInfo.length(); index++) {
			JSONArray route = routeInfo.getJSONArray(index);

			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setDepartAirportInfo(
					AirportHelper.getAirportInfo(route.getString(SqivaConstant.ROUTE_INFO_DEPARTURE_CODE_INDEX)));
			segmentInfo.setArrivalAirportInfo(
					AirportHelper.getAirportInfo(route.getString(SqivaConstant.ROUTE_INFO_ARRIVAL_CODE_INDEX)));

			String flightNumber =
					route.getString(SqivaConstant.ROUTE_INFO_FLIGHT_NUMBER_INDEX).split("-")[1].substring(2);
			String airlineCode = route.getString(SqivaConstant.ROUTE_INFO_FLIGHT_NUMBER_INDEX).split("-")[0];
			FlightDesignator fd = FlightDesignator.builder().flightNumber(flightNumber)
					.airlineInfo(AirlineHelper.getAirlineInfo(airlineCode)).build();
			segmentInfo.setFlightDesignator(fd);

			String departureDate = route.getString(SqivaConstant.ROUTE_INFO_DEPARTURE_DATE_INDEX);
			String departureTime = route.getString(SqivaConstant.ROUTE_INFO_DEPARTURE_TIME_INDEX);
			LocalDateTime departureDateTime = SqivaUtils.convertToLocalDateTime("dd-MMM-yyHHmm",
					org.apache.commons.lang3.StringUtils.join(departureDate, departureTime));

			String arrivalDate = route.getString(SqivaConstant.ROUTE_INFO_ARRIVAL_DATE_INDEX);
			String arrivalTime = route.getString(SqivaConstant.ROUTE_INFO_ARRIVAL_TIME_INDEX);
			LocalDateTime arrivalDateTime = SqivaUtils.convertToLocalDateTime("dd-MMM-yyHHmm",
					org.apache.commons.lang3.StringUtils.join(arrivalDate, arrivalTime));
			segmentInfo.setDepartTime(departureDateTime);
			segmentInfo.setArrivalTime(arrivalDateTime);
			segmentInfo.setDuration(segmentInfo.calculateDuration());

			List<PriceInfo> priceInfos = new ArrayList<>();
			PriceInfo priceInfo = PriceInfo.builder().fareIdentifier(FareType.PUBLISHED.getName()).build();
			String bookLegId = route.getString(SqivaConstant.ROUTE_INFO_BOOK_LEG_ID_INDEX);
			priceInfo.getMiscInfo().setBookLegId(bookLegId);
			priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
			priceInfos.add(priceInfo);
			segmentInfo.setPriceInfoList(priceInfos);

			List<FlightTravellerInfo> copiedTravellers = new ArrayList<>();
			travellerInfos.forEach(traveller -> {
				FlightTravellerInfo copiedTraveller = new GsonMapper<>(traveller, FlightTravellerInfo.class).convert();
				setTravellerAncilary(copiedTraveller, bookLegId, ssrDetailArray, bookAncillaryFeeArray);
				setTravellerClassOfBooking(segmentInfo, copiedTraveller, routeDetailArray);
				copiedTravellers.add(copiedTraveller);
			});

			SegmentBookingRelatedInfo bookingRelatedInfo =
					SegmentBookingRelatedInfo.builder().travellerInfo(copiedTravellers).build();
			segmentInfo.setBookingRelatedInfo(bookingRelatedInfo);
			segmentInfo.setSegmentNum(segmentNum);
			segmentInfos.add(segmentInfo);
			segmentNum++;
		}
		return segmentInfos;
	}

	private void setTravellerClassOfBooking(SegmentInfo segmentInfo, FlightTravellerInfo copiedTraveller,
			JSONArray routeDetailArray) {
		for (int index = 0; index < routeDetailArray.length(); index++) {
			JSONArray routeDetail = routeDetailArray.getJSONArray(index);
			String classOfBooking = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_SUB_CLASS_INDEX);
			copiedTraveller.getFareDetail().setClassOfBooking(classOfBooking);
			// fare basis remains same of classofbook, in retrieve api not getting farebasis
			copiedTraveller.getFareDetail().setFareBasis(classOfBooking);
		}
	}

	private void setTravellerAncilary(FlightTravellerInfo traveller, String bookLegId, JSONArray ssrDetailArray,
			JSONArray bookAncillaryFeeArray) {
		for (int index = 0; index < ssrDetailArray.length(); index++) {
			JSONArray ssrDetail = ssrDetailArray.getJSONArray(index);
			for (int ssrDetailIndex = 0; ssrDetailIndex < ssrDetail.length(); ssrDetailIndex++) {
				String ssrDetailBookLegId = ssrDetail.getString(SqivaConstant.SSR_LIST_BOOK_LEG_ID_INDEX);
				String ssrDetailPaxId = ssrDetail.getString(SqivaConstant.SSR_LIST_PAX_ID_INDEX);
				String ssrCode = ssrDetail.getString(SqivaConstant.SSR_LIST_SSR_CODE_INDEX);
				String ssrDesc = ssrDetail.getString(SqivaConstant.SSR_LIST_SSR_DESC_INDEX);
				if (StringUtils.isNotBlank(ssrDetailBookLegId) && ssrDetailBookLegId.equals(bookLegId)
						&& StringUtils.isNotBlank(ssrDetailPaxId)
						&& ssrDetailPaxId.equals(traveller.getMiscInfo().getPassengerKey())
						&& StringUtils.isNotBlank(ssrCode)) {
					Double ssrAmount = getAmountBySSRCode(bookAncillaryFeeArray, ssrCode);
					SSRInformation mealSSR = new SSRInformation();
					mealSSR.setCode(ssrCode);
					mealSSR.setDesc(ssrDesc);
					mealSSR.setAmount(ssrAmount);
					traveller.setSsrMealInfo(mealSSR);
				}
			}
		}
	}

	private Double getAmountBySSRCode(JSONArray bookAncillaryFeeArray, String ssrCode) {
		Double ssrAmount = 0d;
		for (int index = 0; index < bookAncillaryFeeArray.length(); index++) {
			JSONArray bookAncillaryFee = bookAncillaryFeeArray.getJSONArray(index);
			String ancillaryCode = bookAncillaryFee.getString(SqivaConstant.BOOK_ANCILLARY_FEE_SSR_CODE);
			if (ssrCode.equals(ancillaryCode)) {
				ssrAmount = bookAncillaryFee.getDouble(SqivaConstant.BOOK_ANCILLARY_FEE_SSR_AMOUNT);
			}
		}
		return ssrAmount;
	}

	private List<FlightTravellerInfo> getTravellerInfoFromPaxList(JSONArray paxList, String pnr) {
		List<FlightTravellerInfo> flightTravellerInfos = new ArrayList<>();
		for (int index = 0; index < paxList.length(); index++) {
			JSONArray pax = paxList.getJSONArray(index);
			FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
			travellerInfo.setFirstName(pax.getString(SqivaConstant.PAX_LIST_FIRST_NAME_INDEX));
			travellerInfo.setLastName(pax.getString(SqivaConstant.PAX_LIST_LAST_NAME_INDEX));
			travellerInfo.setPaxType(
					PaxType.getPaxTypeFromCode(pax.getString(SqivaConstant.PAX_LIST_PAX_TYPE_INDEX).toUpperCase()));
			travellerInfo.setTicketNumber(pax.getString(SqivaConstant.PAX_LIST_TICKET_NUM_INDEX));
			travellerInfo.setPnr(pnr);
			String passportNumber = pax.getString(SqivaConstant.PAX_LIST_PASSPORT_INDEX);
			if (StringUtils.isNotBlank(passportNumber)) {
				travellerInfo.setPassportNumber(passportNumber);
			}
			String nationality = pax.getString(SqivaConstant.PAX_LIST_NATIONALITY_INDEX);
			if (StringUtils.isNotBlank(nationality)) {
				travellerInfo.setPassportNationality(nationality);
			}
			String title = pax.getString(SqivaConstant.PAX_LIST_TITLE_INDEX);
			travellerInfo.setTitle((PaxType.ADULT.equals(travellerInfo.getPaxType()) ? title
					: StringUtils.equalsIgnoreCase(title, "MSTR") ? "MASTER" : title));
			String paxId = pax.getString(SqivaConstant.PAX_LIST_PAX_ID_INDEX);
			travellerInfo.getMiscInfo().setPassengerKey(paxId);
			FareDetail fareDetail = new FareDetail();
			fareDetail.setCabinClass(CabinClass.ECONOMY);
			fareDetail.setRefundableType(RefundableType.NON_REFUNDABLE.getRefundableType());
			fareDetail.setIsHandBaggage(false);
			fareDetail.setIsMealIncluded(false);
			travellerInfo.setFareDetail(fareDetail);
			flightTravellerInfos.add(travellerInfo);
		}
		return flightTravellerInfos;
	}

	private DeliveryInfo fetchDeliveryInfo(JSONObject retrieveResponse) {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		String contact = retrieveResponse.getString(SqivaConstant.CONTACT);
		String email = retrieveResponse.getString(SqivaConstant.CONTACT_EMAIL);
		if (StringUtils.isNotBlank(email)) {
			List<String> emails = new ArrayList<>();
			emails.add(email);
			deliveryInfo.setEmails(emails);
		}
		if (StringUtils.isNotBlank(contact)) {
			List<String> contacts = new ArrayList<>();
			contacts.add(contact);
			deliveryInfo.setContacts(contacts);
		}
		return deliveryInfo;
	}

	public JSONObject getBookPriceDetailInfo_V2(String pnr) {
		HttpUtils httpUtils = HttpUtils.builder().build();
		JSONObject bookPriceDetailResponse = null;
		try {
			Map<String, String> bookPriceDetailParams = getBookPriceDetailParams(pnr);
			bookPriceDetailResponse = bindingService.getBookPriceDetailInfo_V2(bookPriceDetailParams, httpUtils);
			boolean isAnyError = isAnyError(bookPriceDetailResponse);
			if (isAnyError) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		}
		return bookPriceDetailResponse;
	}

	private Map<String, String> getBookPriceDetailParams(String pnr) {
		Map<String, String> bookInfoParams = new HashMap<String, String>();
		bookInfoParams.put(SqivaConstant.BOOK_CODE, pnr);
		return bookInfoParams;
	}

}
