package com.tgs.services.fms.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.HoldFeeChargeOutput;

@Service
public class HoldFeeRuleValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {
		if (iRuleOutput == null) {
			return;
		}

		if (iRuleOutput instanceof HoldFeeChargeOutput) {
			HoldFeeChargeOutput output = (HoldFeeChargeOutput) iRuleOutput;
			if (StringUtils.isBlank(output.getExpression())) {
				rejectValue(errors, fieldName + ".expression", SystemError.NULL_VALUE);
			}
		}

	}

}
