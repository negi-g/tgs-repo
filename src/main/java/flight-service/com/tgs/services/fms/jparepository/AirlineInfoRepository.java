package com.tgs.services.fms.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.tgs.services.fms.dbmodel.DbAirlineInfo;

public interface AirlineInfoRepository
		extends JpaRepository<DbAirlineInfo, Long>, JpaSpecificationExecutor<DbAirlineInfo> {

	@Query(value = "SELECT a.* FROM airlineinfo a WHERE a.code ilike %:searchQuery% or a.name ilike %:searchQuery%",
			nativeQuery = true)
	public List<DbAirlineInfo> fetchAirlines(@Param("searchQuery") String searchQuery);

	public DbAirlineInfo findByCode(String code);
}
