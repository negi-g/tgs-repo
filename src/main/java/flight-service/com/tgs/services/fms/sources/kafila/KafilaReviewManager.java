package com.tgs.services.fms.sources.kafila;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.kafila.reviewRequest.FlightDetails;
import com.tgs.services.kafila.reviewRequest.Param;
import com.tgs.services.kafila.reviewRequest.RequestDetails;
import com.tgs.services.kafila.reviewRequest.ReviewRequest;
import com.tgs.services.kafila.reviewResponse.ReviewResponse;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Service
public class KafilaReviewManager extends KafilaServiceManager {

	ReviewResponse reviewResponse;
	ReviewRequest reviewRequest;

	public TripInfo fareCheck(TripInfo selectedTrip) {
		reviewRequest = buildReviewRequest(selectedTrip);
		reviewResponse = bindingService.doReview(reviewRequest);
		if (!parseResponse(reviewResponse, selectedTrip)) {
			selectedTrip = null;
		}
		return selectedTrip;
	}

	public ReviewRequest buildReviewRequest(TripInfo trip) {

		ReviewRequest reviewRequest = new ReviewRequest();
		RequestDetails requestDetails = new RequestDetails();
		List<RequestDetails> detailsList = new ArrayList<>();
		FlightDetails flightDetails = new FlightDetails();
		FlightDetails flightDetails2 = new FlightDetails();
		flightDetails.setID(trip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getSegmentKey());
		flightDetails.setUID(trip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getJourneyKey());
		flightDetails.setTID(trip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getAirPriceSolutionKey());

		if (searchQuery.isOneWay()) {
			requestDetails.setFLIGHT(flightDetails);
		} else {
			requestDetails.setFLIGHTOW(flightDetails);
			List<TripInfo> splitTrips = trip.splitTripInfo(false);
			SegmentInfo returnSegment = splitTrips.get(1).getSegmentInfos().get(0);
			flightDetails2.setID(returnSegment.getPriceInfo(0).getMiscInfo().getSegmentKey());
			flightDetails2.setUID(returnSegment.getPriceInfo(0).getMiscInfo().getJourneyKey());
			flightDetails2.setTID(returnSegment.getPriceInfo(0).getMiscInfo().getAirPriceSolutionKey());
			requestDetails.setFLIGHTRT(flightDetails2);
		}

		String paramDetails = trip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTokenId();
		Param param = GsonUtils.getGson().fromJson(paramDetails, Param.class);
		requestDetails.setPARAM(param);
		detailsList.add(requestDetails);
		reviewRequest.setSTR(detailsList);
		return reviewRequest;

	}

	private boolean parseResponse(ReviewResponse reviewResponse, TripInfo selectedTrip) {

		if (BooleanUtils.isFalse(((searchQuery.isOneWay() || searchQuery.isIntlReturn())
				&& StringUtils.equals(reviewResponse.getSTATUS().get(0).getStatus(), "T"))
				|| (StringUtils.equals(reviewResponse.getSTATUSOW().get(0).getStatus(), "T")
						&& StringUtils.equals(reviewResponse.getSTATUSRT().get(0).getStatus(), "T")))) {
			return false;
		}
		return updateFareDetails(reviewResponse, selectedTrip);
	}

	public boolean updateFareDetails(ReviewResponse response, TripInfo selectedTrip) {

		List<TripInfo> splittedtrips = selectedTrip.splitTripInfo(false);

		if (searchQuery.isIntlReturn() || searchQuery.isDomesticReturn()) {
			com.tgs.services.kafila.reviewResponse.FlightDetails onwardFlight = response.getFLIGHTOW().get(0);
			com.tgs.services.kafila.reviewResponse.FlightDetails returnFlight = response.getFLIGHTRT().get(0);
			updateJourneyTimings(response, onwardFlight, splittedtrips.get(0), true);
			updateJourneyTimings(response, returnFlight, splittedtrips.get(1), false);
		} else {
			com.tgs.services.kafila.reviewResponse.FlightDetails flightInfo = response.getFLIGHT().get(0);
			updateJourneyTimings(response, flightInfo, splittedtrips.get(0), true);
		}

		for (SegmentInfo segment : selectedTrip.getSegmentInfos()) {

			Double basic, yq, at;
			basic = yq = at = 0D;

			PriceInfo priceinfo = segment.getPriceInfoList().get(0);
			Map<PaxType, FareDetail> priceInfoMap = priceinfo.getFareDetails();

			FareDetail adultFareDetail = priceinfo.getFareDetail(PaxType.ADULT);
			Map<FareComponent, Double> adultFD = adultFareDetail.getFareComponents();

			if (Objects.nonNull(response.getFAREOW()) && BooleanUtils.isFalse(segment.getIsReturnSegment())) {
				basic = response.getFAREOW().get(0).getBASIC_ADT();
				yq = response.getFAREOW().get(0).getYQ_ADT();
				at = Double.valueOf(response.getFAREOW().get(0).getTAX_ADT());
			} else if (Objects.nonNull(response.getFAREOW()) && BooleanUtils.isTrue(segment.getIsReturnSegment())) {
				basic = response.getFARERT().get(0).getBASIC_ADT();
				yq = response.getFARERT().get(0).getYQ_ADT();
				at = Double.valueOf(response.getFARERT().get(0).getTAX_ADT());
			} else if (segment.getSegmentNum() == 0 && BooleanUtils
					.isFalse(searchQuery.isIntlReturn() && BooleanUtils.isTrue(segment.getIsReturnSegment()))) {
				basic = response.getFARE().get(0).getBASIC_ADT();
				yq = response.getFARE().get(0).getYQ_ADT();
				at = Double.valueOf(response.getFARE().get(0).getTAX_ADT());
			}

			adultFD.put(FareComponent.BF, basic);
			adultFD.put(FareComponent.YQ, yq);
			adultFD.put(FareComponent.AT, at);
			adultFD.put(FareComponent.TF, basic + yq + at);
			adultFareDetail.setFareComponents(adultFD);
			priceInfoMap.put(PaxType.ADULT, adultFareDetail);

			if (Objects.nonNull(priceinfo.getFareDetail(PaxType.CHILD))) {
				FareDetail childFareDetail = priceinfo.getFareDetail(PaxType.CHILD);
				Map<FareComponent, Double> childFD = childFareDetail.getFareComponents();

				if (Objects.nonNull(response.getFAREOW()) && BooleanUtils.isFalse(segment.getIsReturnSegment())) {
					basic = response.getFAREOW().get(0).getBASIC_CHD();
					yq = response.getFAREOW().get(0).getYQ_CHD();
					at = Double.valueOf(response.getFAREOW().get(0).getTAX_CHD());
				} else if (Objects.nonNull(response.getFARERT())
						&& BooleanUtils.isTrue(segment.getIsReturnSegment() == true)) {
					basic = response.getFARERT().get(0).getBASIC_CHD();
					yq = response.getFARERT().get(0).getYQ_CHD();
					at = Double.valueOf(response.getFARERT().get(0).getTAX_CHD());
				} else if (segment.getSegmentNum() == 0 && BooleanUtils
						.isFalse(searchQuery.isIntlReturn() && BooleanUtils.isTrue(segment.getIsReturnSegment()))) {
					basic = response.getFARE().get(0).getBASIC_CHD();
					yq = response.getFARE().get(0).getYQ_CHD();
					at = Double.valueOf(response.getFARE().get(0).getTAX_CHD());
				}

				childFD.put(FareComponent.BF, basic);
				childFD.put(FareComponent.YQ, yq);
				childFD.put(FareComponent.AT, at);
				childFD.put(FareComponent.TF, basic + yq + at);
				childFareDetail.setFareComponents(childFD);
				priceInfoMap.put(PaxType.CHILD, childFareDetail);
			}

			if (priceinfo.getFareDetail(PaxType.INFANT) != null) {
				FareDetail infantFareDetail = priceinfo.getFareDetail(PaxType.INFANT);
				Map<FareComponent, Double> infantFD = infantFareDetail.getFareComponents();

				if (Objects.nonNull(response.getFAREOW()) && BooleanUtils.isFalse(segment.getIsReturnSegment())) {
					basic = response.getFAREOW().get(0).getBASIC_INF();
					at = Double.valueOf(response.getFAREOW().get(0).getTAX_INF());
					yq = response.getFAREOW().get(0).getYQ_INF();
				} else if (Objects.nonNull(response.getFARERT()) && BooleanUtils.isTrue(segment.getIsReturnSegment())) {
					basic = response.getFARERT().get(0).getBASIC_INF();
					at = Double.valueOf(response.getFARERT().get(0).getTAX_INF());
					yq = response.getFARERT().get(0).getYQ_INF();
				} else if (segment.getSegmentNum() == 0 && BooleanUtils
						.isFalse(searchQuery.isIntlReturn() && BooleanUtils.isTrue(segment.getIsReturnSegment()))) {
					basic = response.getFARE().get(0).getBASIC_INF();
					at = Double.valueOf(response.getFARE().get(0).getTAX_INF());
					yq = response.getFARE().get(0).getYQ_INF();
				}

				infantFD.put(FareComponent.BF, basic);
				infantFD.put(FareComponent.AT, at);
				infantFD.put(FareComponent.YQ, yq);
				infantFD.put(FareComponent.TF, basic + at + yq);
				infantFareDetail.setFareComponents(infantFD);
				priceInfoMap.put(PaxType.INFANT, infantFareDetail);
			}
			priceinfo.setFareDetails(priceInfoMap);
			priceinfo.setSupplierBasicInfo(supplierConfiguration.getBasicInfo());
		}

		return true;
	}

	private void updateJourneyTimings(ReviewResponse response,
			com.tgs.services.kafila.reviewResponse.FlightDetails flightDetail, TripInfo tripInfo, boolean isOutbound) {
		List<SegmentInfo> segmentInfos = tripInfo.getSegmentInfos();
		if (CollectionUtils.size(segmentInfos) > 1) {
			List<com.tgs.services.kafila.reviewResponse.FlightDetails> connectingDetails = null;
			if (Objects.isNull(response.getCON_FLIGHT())) {
				connectingDetails = isOutbound ? response.getCON_FLIGHTOW() : response.getCON_FLIGHTRT();
			} else {
				connectingDetails = response.getCON_FLIGHT();
			}
			for (com.tgs.services.kafila.reviewResponse.FlightDetails connectingDetail : connectingDetails) {
				String origin = StringUtils.trim(connectingDetail.getORG_CODE());
				String destination = StringUtils.trim(connectingDetail.getDES_CODE());
				SegmentInfo segmentInfo = segmentInfos.stream()
						.filter(seg -> StringUtils.equalsIgnoreCase(seg.getDepartureAirportCode(), origin)
								&& StringUtils.equalsIgnoreCase(seg.getArrivalAirportCode(), destination))
						.findFirst().get();
				if (segmentInfo != null) {
					segmentInfo.setDepartTime(KafilaUtils.toLocalDateTime(connectingDetail.getDEP_DATE(),
							connectingDetail.getDEP_TIME()));
					segmentInfo.setArrivalTime(KafilaUtils.toLocalDateTime(connectingDetail.getARRV_DATE(),
							connectingDetail.getARRV_TIME()));
					segmentInfo.setDuration(KafilaUtils.toMinutes(connectingDetail.getDURATION()));
				}
			}
		} else {
			SegmentInfo segmentInfo = segmentInfos.get(0);
			segmentInfo
					.setDepartTime(KafilaUtils.toLocalDateTime(flightDetail.getDEP_DATE(), flightDetail.getDEP_TIME()));
			segmentInfo.setArrivalTime(
					KafilaUtils.toLocalDateTime(flightDetail.getARRV_DATE(), flightDetail.getARRV_TIME()));
			segmentInfo.setDuration(KafilaUtils.toMinutes(flightDetail.getDURATION()));
		}
	}


	public TripInfo setBookingString(TripInfo tripInfo) {
		tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo()
				.setAvailabilityDisplayType(GsonUtils.getGson().toJson(reviewResponse));
		return tripInfo;

	}


}
