package com.tgs.services.fms.manager;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.DealInventoryCommunicator;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.enums.AirRules;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.airconfigurator.AirClientFeeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirClientMarkupOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirSSRFilterConfiguration;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.airconfigurator.ListOutput;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.FareComponentHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ims.datamodel.air.AirInventoryInfo;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirSearchResultProcessingManager {

	@Autowired
	private AirUserFeeManager userFeeManager;

	@Autowired
	private AirPartnerUserFeeManager partnerFeeManager;

	@Autowired
	private AirPartnerCommissionManager partnerCommissionManager;

	@Autowired
	private CommercialCommunicator commericialCommunicator;

	@Autowired
	private ShowPublicBookPrivateEngine bookPrivateEngine;

	@Autowired
	private ShowPublicBookPrivateDynamicEngine dynamicEngine;

	@Autowired
	private FareRuleManager fareRuleManager;

	@Autowired
	private DealInventoryCommunicator inventoryCommunicator;

	@Autowired
	private InventoryFareDropEngine fareDropEngine;

	public void processAirSearchResult(AirSearchResult searchResult, AirSearchQuery searchQuery, User bookingUser) {
		if (searchResult != null && searchResult.getTripInfos() != null) {
			searchResult.getTripInfos().forEach((key, trips) -> {
				// sortSimilarFlights(trips);
				if (CollectionUtils.isNotEmpty(trips)) {
					filterAndCombineFares(trips, searchQuery, bookingUser);
				}
			});
		}
	}

	private void filterSpecialReturn(AirSearchResult searchResult) {
		List<TripInfo> onwardTrips = searchResult.getTripInfos().get(TripInfoType.ONWARD.name());
		List<TripInfo> returnTrips = searchResult.getTripInfos().get(TripInfoType.RETURN.name());
		HashMap<String, TripInfo> specialReturnMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(returnTrips) && CollectionUtils.isNotEmpty(onwardTrips)) {
			// Filtering special return for domestic returns trips
			filterSpecialReturnOnTrips(onwardTrips, false);
			filterSpecialReturnOnTrips(returnTrips, false);
			getSpecialReturnMap(specialReturnMap, onwardTrips);
			getSpecialReturnMap(specialReturnMap, returnTrips);
			removeUnmatchedSplReturn(onwardTrips, specialReturnMap);
			removeUnmatchedSplReturn(returnTrips, specialReturnMap);
		} else if (CollectionUtils.isNotEmpty(onwardTrips)) {
			// Filtering special return for one way domestic and multi city domestic
			filterSpecialReturnOnTrips(onwardTrips, true);
		}
	}

	private void filterSpecialReturnOnTrips(List<TripInfo> trips, boolean isOneWayTrip) {
		for (Iterator<TripInfo> tripIterator = trips.iterator(); tripIterator.hasNext();) {
			TripInfo trip = tripIterator.next();
			Set<Integer> indexToBeRemoved = new HashSet<Integer>();
			for (int priceIndex = 0; priceIndex < trip.getSegmentInfos().get(0).getPriceInfoList()
					.size(); priceIndex++) {
				Set<String> fareTypeSet = trip.getFareType(priceIndex);
				if (fareTypeSet.contains(FareType.SPECIAL_RETURN.name()) && (fareTypeSet.size() > 1 || isOneWayTrip)) {
					indexToBeRemoved.add(priceIndex);
				}
			}
			if (CollectionUtils.isNotEmpty(indexToBeRemoved)) {
				log.debug("Removing price indexes {} for trip {}", indexToBeRemoved, trip.getTripKey());
				trip.getSegmentInfos().forEach(segment -> {
					int priceIndex = 0;
					for (Iterator<PriceInfo> iter = segment.getPriceInfoList().iterator(); iter.hasNext();) {
						iter.next();
						if (indexToBeRemoved.contains(priceIndex)) {
							iter.remove();
						}
						priceIndex++;
					}
				});
				if (CollectionUtils.isEmpty(trip.getSegmentInfos().get(0).getPriceInfoList())) {
					log.debug("Removing trip {} because of empty priceInfo", trip.getTripKey());
					tripIterator.remove();
				}
			}
		}
	}

	private void getSpecialReturnMap(HashMap<String, TripInfo> specialReturnMap, List<TripInfo> tripInfos) {
		for (TripInfo trip : tripInfos) {
			for (PriceInfo priceInfo : trip.getSegmentInfos().get(0).getPriceInfoList()) {
				if (StringUtils.isNotEmpty(priceInfo.getSpecialReturnIdentifier())) {
					specialReturnMap.put(priceInfo.getSpecialReturnIdentifier(), trip);
				}
			}
		}
	}

	private void removeUnmatchedSplReturn(List<TripInfo> tripInfos, HashMap<String, TripInfo> specialReturnMap) {
		for (Iterator<TripInfo> tripIterator = tripInfos.iterator(); tripIterator.hasNext();) {
			TripInfo trip = tripIterator.next();
			for (SegmentInfo segmentInfo : trip.getSegmentInfos()) {
				for (Iterator<PriceInfo> priceIterator = segmentInfo.getPriceInfoList().iterator(); priceIterator
						.hasNext();) {
					PriceInfo priceInfo = priceIterator.next();
					if (StringUtils.isNotEmpty(priceInfo.getSpecialReturnIdentifier())) {
						for (Iterator<String> stringIterator =
								priceInfo.getMatchedSpecialReturnIdentifier().iterator(); stringIterator.hasNext();) {
							String matchedReturnIdentifier = stringIterator.next();
							if (specialReturnMap.get(matchedReturnIdentifier) == null) {
								stringIterator.remove();
								log.debug("Removing msri {} from special fare {}", matchedReturnIdentifier,
										priceInfo.getSpecialReturnIdentifier());
							}
						}
						if (CollectionUtils.isEmpty(priceInfo.getMatchedSpecialReturnIdentifier())) {
							priceIterator.remove();
						}
					}
				}
			}
			if (CollectionUtils.isEmpty(trip.getSegmentInfos().get(0).getPriceInfoList())) {
				tripIterator.remove();
			}
		}
	}

	public static void sortSimilarFlights(List<TripInfo> trips) {
		Collections.sort(trips, new Comparator<TripInfo>() {
			@Override
			public int compare(TripInfo tripInfo1, TripInfo tripInfo2) {
				boolean isEqual = false;
				if (tripInfo1.getSegmentInfos().size() == tripInfo2.getSegmentInfos().size()) {
					isEqual = true;
					for (int i = 0; i < tripInfo1.getSegmentInfos().size(); i++) {
						if (!tripInfo1.getSegmentInfos().get(i).getFlightDesignator().getFlightNumber()
								.equals(tripInfo2.getSegmentInfos().get(i).getFlightDesignator().getFlightNumber())) {
							isEqual = false;
							break;
						}
					}
				}
				return isEqual ? 0 : -1;
			}
		});
	}

	public void filterAndCombineFares(List<TripInfo> trips, AirSearchQuery searchQuery, User bookingUser) {

		AirGeneralPurposeOutput gnOutput =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		// default always true (just for debugging purpose)
		boolean isMergeRequired = BooleanUtils.isNotTrue(gnOutput.getDisablePriceMerge());

		Map<String, TripInfo> keyMap = new HashMap<>();

		for (Iterator<TripInfo> iter = trips.iterator(); iter.hasNext();) {
			TripInfo trip = iter.next();

			/**
			 * As we are not supporting infant search for corporate fare. If the Search has infant paxType this will
			 * filter out the priceInfo which has corporate fare.
			 */
			trip = removeCorporateFareForInfantSearch(searchQuery, trip);

			if (trip != null) {
				logFareComponent(trip);
				String key = "";
				for (SegmentInfo segmentInfo : trip.getSegmentInfos()) {
					/**
					 * Key - FlightNumber + AirlineCode + Trip Departure Time
					 * 
					 * @implNote : LocalTime can't be used cause , in GoAir Same Flight number is operated in connecting
					 *           case.
					 */
					key = StringUtils.join(key, segmentInfo.getFlightDesignator().getFlightNumber(),
							segmentInfo.getAirlineCode(false), segmentInfo.getDepartTime().toString());
				}
				if (keyMap.get(key) == null) {
					keyMap.put(key, trip);
				} else {
					log.debug("Before Price Merge {}", trip);
					TripPriceEngine.filterAndMergePriceInfo(trip, keyMap, key, isMergeRequired);
					log.debug("Post Merge Trip price options {}", keyMap.get(key));
					iter.remove();
				}
			} else {
				iter.remove();
			}
		}
	}

	private void logFareComponent(TripInfo trip) {
		List<String> tripKeys = FareComponentHelper.getTripKeys(Arrays.asList(trip));
		if (CollectionUtils.isNotEmpty(tripKeys)) {
			// String tripKey = tripKeys.get(0);
			AtomicDouble tripYQFare = new AtomicDouble(0);
			for (SegmentInfo segmentInfo : trip.getSegmentInfos()) {
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					if (priceInfo.getFareDetail(PaxType.ADULT) != null) {
						if (MapUtils.isNotEmpty(priceInfo.getFareDetail(PaxType.ADULT).getFareComponents())) {
							tripYQFare.getAndAdd(priceInfo.getFareDetail(PaxType.ADULT).getFareComponents()
									.getOrDefault(FareComponent.YQ, 0d));
						}
					}
				});
			}
			// log.debug("YQ Fare {} -> {}", tripKey, tripYQFare.get());
		}
	}

	public static AirSearchResult processSearchTypeResult(AirSearchResult searchResult, Integer sourceId,
			AirSearchQuery searchQuery, User bookingUser) {
		if (!searchQuery.isSplitSearch() && searchQuery.isIntlReturn() && searchResult != null
				&& MapUtils.isNotEmpty(searchResult.getTripInfos())
				&& (searchResult.getTripInfos().get(TripInfoType.ONWARD.name()) != null
						|| searchResult.getTripInfos().get(TripInfoType.RETURN.name()) != null)) {
			List<TripInfo> tripInfos =
					AirUtils.buildCombination(searchResult.getTripInfos().get(TripInfoType.ONWARD.name()),
							searchResult.getTripInfos().get(TripInfoType.RETURN.name()), sourceId, bookingUser);
			searchResult = new AirSearchResult();
			if (CollectionUtils.isNotEmpty(tripInfos)) {
				searchResult.getTripInfos().put(TripInfoType.COMBO.getName(), tripInfos);
			}
		}
		return searchResult;
	}

	public void process(AirSearchResult searchResult, AirSearchQuery searchQuery, String bookingId, User user) {

		if (searchResult != null && searchResult.getTripInfos() != null) {

			AirGeneralPurposeOutput gnSourceRule =
					AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
			searchResult.getTripInfos().forEach((key, trips) -> {
				log.debug("Total Trip size is {} , for searchid {}", trips.size(), searchQuery.getSearchId());

				// CopyOnWriteArrayList<List<TripInfo>> chunkList = new CopyOnWriteArrayList<>(Lists.partition(trips,
				// 80));

				List<CopyOnWriteArrayList<TripInfo>> chunks = new ArrayList<>();
				ContextData copyContextData = SystemContextHolder.getContextData().deepCopy();

				for (List<TripInfo> chunk1 : Lists.partition(trips, 80)) {
					chunks.add(new CopyOnWriteArrayList<>(chunk1));
				}

				List<Future<List<TripInfo>>> futureTaskList = new ArrayList<>();
				for (List<TripInfo> tripList : chunks) {
					futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool().submit(() -> {
						return processTrips(tripList, searchQuery, user, gnSourceRule, copyContextData);
					}));
				}

				log.debug("Total no of threads created for processing is {} , for searchid {}", futureTaskList.size(),
						searchQuery.getSearchId());
				for (Future<List<TripInfo>> future : futureTaskList) {
					List<TripInfo> invalidTrips = new ArrayList<>();
					try {
						invalidTrips.addAll(future.get());
					} catch (Exception e) {
						log.error("Unable to get future#task result", e);
					} finally {
						if (CollectionUtils.isNotEmpty(invalidTrips)) {
							trips.removeAll(invalidTrips);
						}
					}
				}
				log.debug("Processing completed for all trips for searchid {}", searchQuery.getSearchId());

			});

			// To ensure that all the special returns has their pair.
			if (searchQuery.getIsDomestic() || (searchQuery.isIntl() && searchQuery.isOneWay())) {
				filterSpecialReturn(searchResult);
			}

			LogUtils.log(
					LogTypes.AIRSEARCH_PROCESS, SystemContextHolder.getContextData().getLogMetaInfo()
							.setSearchId(searchQuery.getSearchId()).setTrips(searchResult.getTotalTrips()),
					LogTypes.AIRSEARCH_DOSEARCH_END);
		}
	}

	private List<TripInfo> processTrips(List<TripInfo> tripList, AirSearchQuery searchQuery, User user,
			AirGeneralPurposeOutput gnSourceRule, ContextData contextData) {
		List<TripInfo> invalidTrips = new ArrayList<>();
		boolean isPartnerFlow = AirUtils.isPartnerFlow(user);
		for (Iterator<TripInfo> iter = tripList.iterator(); iter.hasNext();) {
			TripInfo tripInfo = null;
			try {
				tripInfo = iter.next();
				String supplierId = String.valueOf(tripInfo.getSupplierInfo().getSupplierId());
				LogUtils.log("process#singletrip#start",
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

				AirSourceConfigurationOutput airSourceConfig = null;

				if (MapUtils.getObject(contextData.getValueMap(), supplierId) == null) {
					AirSourceConfigurationOutput airSourceOutput =
							AirUtils.getAirSourceConfiguration(searchQuery, tripInfo.getSupplierInfo(), user);
					if (airSourceOutput == null) {
						airSourceOutput = new AirSourceConfigurationOutput();
					}
					contextData.setValue(supplierId, airSourceOutput);
				}
				airSourceConfig = (AirSourceConfigurationOutput) contextData.getValueMap().get(supplierId);

				processTripInfo(tripInfo, searchQuery, user, airSourceConfig, gnSourceRule, isPartnerFlow);
				TripPriceEngine.filterInValidPriceInfoFromTrip(tripInfo.getSegmentInfos(), searchQuery);
				/**
				 * We are validating tripInfo post processing because some parameters like segmeprocessTripInfontNum are
				 * set during processing, that's why we need to validate post processing
				 */

				if (!isValidTripInfo(tripInfo, searchQuery, airSourceConfig, gnSourceRule)) {
					invalidTrips.add(tripInfo);
					// iter.remove();
				}

				LogUtils.log("process#singletrip", AirUtils.getLogMetaInfo(searchQuery, tripInfo),
						"process#singletrip#start", 20L);

			} catch (Exception e) {
				log.error("Unable to process tripInfo for search Id {} for trip {} ", searchQuery.getSearchId(),
						tripInfo, e);
				invalidTrips.add(tripInfo);
			}
		}
		return invalidTrips;
	}

	private void setSegmentTiming(TripInfo tripInfo, Map<String, AirInventoryInfo> airlineTiming) {
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			if (CollectionUtils.isEmpty(segmentInfo.getStopOverAirports())) {
				AirInventoryInfo segmentTiming = AirInventoryInfo.builder().departureTime(segmentInfo.getDepartTime())
						.arrivalTime(segmentInfo.getArrivalTime()).build();
				airlineTiming.put(StringUtils.join(segmentInfo.getDepartureAirportCode(), "-",
						segmentInfo.getArrivalAirportCode(), "-", segmentInfo.getAirlineCode(false),
						segmentInfo.getFlightNumber()), segmentTiming);
			}
		});
	}

	public boolean isValidTripInfo(TripInfo tripInfo, AirSearchQuery searchQuery,
			AirSourceConfigurationOutput airSourceOutput, AirGeneralPurposeOutput gnSourceRule) {
		int noOfPriceOptions = tripInfo.getSegmentInfos().get(0).getPriceInfoList().size();
		boolean preferredAirlineExist = false;
		boolean isDirectFlight = true;


		AirType airType = AirUtils.getAirType(tripInfo);

		/**
		 * Removing flights on the basis of near by departure time.
		 */

		int flightRestrictionMinutes = AirUtils.getFlightRestrictionMinutes(tripInfo, airSourceOutput, gnSourceRule);
		if (Duration.between(LocalDateTime.now(), tripInfo.getDepartureTime())
				.toMinutes() <= flightRestrictionMinutes) {
			return false;
		}

		/**
		 * No of priceList in every segment should be Equal, otherwise consider that as in Valid trip
		 */
		Integer segmentIndex = 0;
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			if (segmentInfo.getSegmentNum() == 0 && isValidTripPrice(segmentInfo, searchQuery, segmentIndex)) {
				if (segmentInfo.getPriceInfoList().size() == 0) {
					log.debug("There doesn't exist any price info for trip {} , searchQuery {}", tripInfo, searchQuery);
					return false;
				}
				for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
					for (Entry<PaxType, FareDetail> entrySet : priceInfo.getFareDetails().entrySet()) {

						// Invalid Price When Airline fare is Zero
						if (MapUtils.isEmpty(entrySet.getValue().getFareComponents())
								|| (entrySet.getValue().getAirlineFare() <= 0
										&& !gnSourceRule.isZeroFareAllowed(airType, priceInfo.getSourceId()))) {
							log.error("TotalFare for paxType {} is zero for {} tripInfo {}", entrySet.getKey(),
									searchQuery.getSearchId(), tripInfo);
							return false;
						}
					}

					if (priceInfo.getFareDetails().size() != getPaxTypeCount(searchQuery)) {
						// Invalid Price FareDetail size When Requested count not match
						log.error("Price Fare Detail Mismatch for searchid {} trip {}", searchQuery.getSearchId(),
								tripInfo);
						return false;
					}
				}
			}

			// This is to filter result {we Only want result for those Airlines that we
			// passed in preferred Airlines}
			if (CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())) {
				if (!(searchQuery.getPrefferedAirline().contains(segmentInfo.getAirlineCode(false)))) {
					return false;
				}
			}

			if (segmentInfo.getSegmentNum() > 0) {
				isDirectFlight = false;
			}

			if (StringUtils.isBlank(searchQuery.getPrefferedAirline()) || searchQuery.getPrefferedAirline()
					.contains(segmentInfo.getFlightDesignator().getAirlineInfo().getCode())) {
				preferredAirlineExist = true;
			}

			if (segmentInfo.getPriceInfoList().size() != noOfPriceOptions || noOfPriceOptions == 0) {
				log.error("No of priceOptions doesn't match for tripInfo {}", tripInfo);
				return false;
			}
			segmentIndex++;
		}

		if (!preferredAirlineExist) {
			return false;
		}

		if ((!isDirectFlight && BooleanUtils.isTrue(searchQuery.getSearchModifiers().getIsDirectFlight()))
				|| (isDirectFlight && BooleanUtils.isTrue(searchQuery.getSearchModifiers().getIsConnectingFlight()))) {
			return false;
		}
		return true;
	}

	private boolean isValidTripPrice(SegmentInfo segmentInfo, AirSearchQuery searchQuery, Integer segmentIndex) {
		if ((searchQuery.isIntlReturn() && BooleanUtils.isTrue(segmentInfo.getIsReturnSegment()))
				|| (searchQuery.isIntl() && searchQuery.isMultiCity() && segmentIndex > 0)) {
			return false;
		}
		return true;
	}

	public void processTripInfo(TripInfo trip, AirSearchQuery searchQuery, User user,
			AirSourceConfigurationOutput sourceConfig, AirGeneralPurposeOutput gnSourceRule, boolean isPartnerFlow) {
		int segmentNum = 0;
		sourceConfig = Objects.isNull(sourceConfig) ? new AirSourceConfigurationOutput() : sourceConfig;
		ContextData contextData = SystemContextHolder.getContextData();
		SegmentInfo previousSegmentInfo = null;
		trip.setPaxInfo(searchQuery.getPaxInfo());
		LogUtils.log("segmentInfos#singletrip#start",
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		for (int i = 0; i < trip.getSegmentInfos().size(); i++) {
			SegmentInfo segmentInfo = trip.getSegmentInfos().get(i);

			/** Setting Segment Number **/
			if (segmentInfo.getSegmentNum() == null) {
				if (i > 0 && segmentInfo.getIsReturnSegment()
						&& !trip.getSegmentInfos().get(i - 1).getIsReturnSegment()) {
					segmentNum = 0;
				}
				segmentInfo.setSegmentNum(segmentNum++);
			}

			segmentInfo.setIsArrivalNextDay();

			// segmentInfo.getStops() is overrided in Segment level to return zero if null
			segmentInfo.setStops(segmentInfo.getStops());
			segmentInfo.setIsReturnSegment(segmentInfo.getIsReturnSegment());

			/** setting Cabin Class for priceInfo **/
			if (i > 0) {
				for (int priceInfoIndex = 0; priceInfoIndex < segmentInfo.getPriceInfoList().size(); priceInfoIndex++) {
					PriceInfo priceInfo = segmentInfo.getPriceInfoList().get(priceInfoIndex);
					if (MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
						FareDetail fd = priceInfo.getFareDetail(PaxType.ADULT);
						if (fd.getCabinClass() == null) {
							fd.setCabinClass(trip.getSegmentInfos().get(0).getPriceInfo(priceInfoIndex)
									.getFareDetail(PaxType.ADULT).getCabinClass());
						}
						if (fd.getRefundableType() == null) {
							fd.setRefundableType(trip.getSegmentInfos().get(0).getPriceInfo(priceInfoIndex)
									.getFareDetail(PaxType.ADULT).getRefundableType());
						}
					}
				}
			}

			if ((previousSegmentInfo != null) && (segmentInfo.getSegmentNum() > 0
					|| BooleanUtils.isFalse(segmentInfo.getIsCombinationFirstSegment()))) {
				long connectingTime = AirUtils.getConnectingTime(previousSegmentInfo, segmentInfo);
				trip.getSegmentInfos().get(i - 1).setConnectingTime(connectingTime);
			}
			previousSegmentInfo = segmentInfo;
		}

		LogUtils.log("segmentInfos#singletrip#end",
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId()).build(),
				"segmentInfos#singletrip#start", null);

		LogUtils.log("message#singletrip#start", LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				null);
		// set messages here
		if (BooleanUtils.isTrue(searchQuery.getSearchModifiers().getIncludeMessages())) {
			for (SegmentInfo segmentInfo : trip.getSegmentInfos()) {
				for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
					addMessages(segmentInfo, searchQuery, priceInfo, user);
				}
			}
		}

		LogUtils.log("message#singletrip#end", AirUtils.getLogMetaInfo(searchQuery, trip), "message#singletrip#start",
				null);

		LogUtils.log("pricing#singletrip#start",
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId()).build(),
				null, 5L);

		SystemContextHolder.getContextData().setGson(GsonUtils.getGson());
		trip.setTripPriceInfos(AirUtils.getTripTotalPriceInfoList(trip, user));

		sortSSRInfoOnPrice(trip);

		LogUtils.log("pricing#singletrip#end", AirUtils.getLogMetaInfo(searchQuery, trip), "pricing#singletrip#start",
				null);

		if (user != null) {

			if (BooleanUtils.isTrue(gnSourceRule.getIsInventoryFareDropEnabled())) {
				LogUtils.log("applyInventoryFareDrop#singletrip#start",
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

				fareDropEngine.applyInventoryFareDrop(trip, user);

				LogUtils.log(
						"applyInventoryFareDrop#singletrip#end", LogMetaInfo.builder()
								.timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId()).build(),
						"applyInventoryFareDrop#singletrip#start", 2L);
			}

			if (BooleanUtils.isTrue(gnSourceRule.getIsDynamicSPBP())) {
				LogUtils.log("processSystemDefinedFareType#singletrip#start",
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

				TripPriceEngine.processSystemDefinedFareType(contextData, trip, searchQuery, user);
				dynamicEngine.isUserShowPublicBookPrivate(trip, user);

				LogUtils.log("processSystemDefinedFareType#singletrip#end", AirUtils.getLogMetaInfo(searchQuery, trip),
						"processSystemDefinedFareType#singletrip#start", null);
			} else {
				LogUtils.log("showpublicbookprivate#singletrip#start",
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

				bookPrivateEngine.isUserShowPublicBookPrivate(trip, user, sourceConfig);

				LogUtils.log(
						"showpublicbookprivate#singletrip#end", LogMetaInfo.builder()
								.timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId()).build(),
						"showpublicbookprivate#singletrip#start", null);
			}

			if (trip.isPriceInfosNotEmpty()) {
				/* Setting Farerule details after the ShowPublicBookPrivate is completed */
				LogUtils.log("farerule#singletrip#start",
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

				AtomicReference tempSourceConfig = new AtomicReference<AirSourceConfigurationOutput>(sourceConfig);
				AirUtils.splitTripInfo(trip, true).forEach(tripInfo -> {
					FlightBasicFact flightFact =
							FlightBasicFact.createFact().generateFact(tripInfo, AirUtils.getAirType(tripInfo));
					BaseUtils.createFactOnUser(flightFact, user);
					fareRuleManager.setInclusions(tripInfo, flightFact, searchQuery, tempSourceConfig, user);
				});

				LogUtils.log("farerule#singletrip#end", AirUtils.getLogMetaInfo(searchQuery, trip),
						"farerule#singletrip#start", 5L);

				/* setting commission if any */

				LogUtils.log("commission#singletrip#start",
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
				commericialCommunicator.processUserCommission(trip, user, searchQuery);
				LogUtils.log("commission#singletrip#end", AirUtils.getLogMetaInfo(searchQuery, trip),
						"commission#singletrip#start", null);

				LogUtils.log("faredetail#singletrip#start",
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

				processFareDetail(trip, searchQuery, user, sourceConfig, gnSourceRule);
				LogUtils.log("faredetail#singletrip#end", AirUtils.getLogMetaInfo(searchQuery, trip),
						"faredetail#singletrip#start", null);


				/**
				 * setting mark up if any
				 *
				 * @implNote : This should be always after applying commission
				 */
				LogUtils.log("markup#singletrip#start",
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
				if (!(user != null && user.getAdditionalInfo() != null
						&& BooleanUtils.isTrue(user.getAdditionalInfo().getDisableMarkup())))

					userFeeManager.processUserFee(user, trip, UserFeeType.MARKUP);
				LogUtils.log("markup#singletrip#end", LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
						.searchId(searchQuery.getSearchId()).build(), "markup#singletrip#start", null);

				if (isPartnerFlow) {
					LogUtils.log("partnermarkup#singletrip#start",
							LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
					partnerFeeManager.processPartnerMarkUp(user, trip, UserFeeType.PARTNER_MARKUP,
							searchQuery.getSearchId());
					LogUtils.log("partnermarkup#singletrip#end",
							LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
									.searchId(searchQuery.getSearchId()).build(),
							"partnermarkup#singletrip#start", null);

					LogUtils.log("partnercommission#singletrip#start",
							LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
					partnerCommissionManager.processPartnerCommission(user, trip, searchQuery);
					LogUtils.log("partnercommission#singletrip#end",
							LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
									.searchId(searchQuery.getSearchId()).build(),
							"partnercommission#singletrip#start", null);
				}

			}
			processPNRCredit(trip, searchQuery);
		}
	}

	private void addMessages(SegmentInfo segmentInfo, AirSearchQuery searchQuery, PriceInfo priceInfo, User user) {
		priceInfo.getMessages().clear();
		FlightBasicFact fact = FlightBasicFact.createFact();
		BaseUtils.createFactOnUser(fact, user);
		AirUtils.generateMissingFactsOnSegmentInfo(segmentInfo, fact);
		fact.generateFact(priceInfo);
		fact.setRouteInfos(searchQuery.getRouteInfos());
		fact.setAirline(segmentInfo.getPlatingCarrier(priceInfo));
		fact.setBookingClasses(
				Stream.of(priceInfo.getBookingClass(PaxType.ADULT)).collect(Collectors.toCollection(HashSet::new)));
		List<AirConfiguratorInfo> rule =
				AirConfiguratorHelper.getAllAirConfigRule(AirConfiguratorRuleType.MESSAGE, fact);
		if (CollectionUtils.isNotEmpty(rule)) {
			rule.forEach(item -> {
				ListOutput<MessageInfo> messages = (ListOutput) item.getIRuleOutPut();
				priceInfo.getMessages().addAll(messages.getValues());
			});
		}
	}

	public void processTripInfoProperties(TripInfo trip, AirSearchQuery searchQuery, SupplierConfiguration supplierConf,
			AirSourceConfigurationOutput sourceConfiguration) {
		User user = SystemContextHolder.getContextData().getUser();
		ContextData contextData = SystemContextHolder.getContextData();
		String bookingId = contextData.getBookingId();
		PriceMiscInfo miscInfo = trip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo();
		if (miscInfo.getCcInfoId() == null) {
			CreditCardInfo cardInfo = commericialCommunicator.getCreditCardInfo(trip, user);
			if (cardInfo != null) {
				trip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().setCcInfoId(cardInfo.getId().intValue());
				trip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getRuleIdMap().put(AirRules.CCID,
						cardInfo.getId());
			}
			log.debug("Passthru for booking {} and info {}", bookingId, cardInfo);
		}

		if (miscInfo.getIata() == null) {
			commericialCommunicator.processAirCommission(trip, user);
			log.debug("IATA Commission for booking {} and trip {}", bookingId, trip);
		}

		if (StringUtils.isBlank(miscInfo.getTourCode())) {
			LogUtils.log("tourcode#singletrip#start",
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			/* Set Tour Code Commission If any */

			commericialCommunicator.processTourCode(trip, user, searchQuery);
			log.debug("TourCode for booking {} and trip {}", bookingId, trip);
			LogUtils.log(
					"singletrip", LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
							.searchId(searchQuery.getSearchId()).bookingId(contextData.getBookingId()).build(),
					"tourcode#singletrip#start", 10L);
		}
		if (sourceConfiguration != null && BooleanUtils.isTrue(sourceConfiguration.getIsSupplierBlockAllowed())) {
			TripPriceEngine.holdFeeCharges(trip, bookingId);
		}
		this.processBookingConditions(trip, supplierConf);
	}

	private void processFareDetail(TripInfo tripInfo, AirSearchQuery searchQuery, User user,
			AirSourceConfigurationOutput sourceConfig, AirGeneralPurposeOutput gnRule) {
		final FareComponent managementFeeFareComponent = FareComponent.MF;
		final FareComponent clientMarkUpFeeFareComponent = FareComponent.CMU;
		AtomicInteger tripIndex = new AtomicInteger();
		/**
		 * splitted trips are needed here because in case of Int'l round trip, We want to apply management fee on
		 * journey wise, i.e for onward journey and return journey separately, and fact(routeInfo) also need to created
		 * journey wise
		 */
		List<TripInfo> splittedTrips = AirUtils.splitTripInfo(tripInfo, false);
		/**
		 * In case of multicity , there will be total n number of trips, for each we have to apply management fee, so we
		 * will be using singleTrip
		 */
		AtomicReference<TripInfo> singleTrip = new AtomicReference<TripInfo>();
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			if (segmentInfo.getBookingRelatedInfo() != null
					&& CollectionUtils.isNotEmpty(segmentInfo.getTravellerInfo())) {
				segmentInfo.getTravellerInfo().forEach(travellerInfo -> {
					if (travellerInfo.getFareDetail() != null
							&& MapUtils.isNotEmpty(travellerInfo.getFareDetail().getFareComponents())) {
						setSeatRemaining(travellerInfo.getFareDetail(), sourceConfig.getMaxSeatCountAllowed());
						addManagementFee(travellerInfo.getFareDetail(), managementFeeFareComponent, searchQuery,
								travellerInfo.getPaxType(), segmentInfo, user, tripInfo, 0);
						addClientMarkup(travellerInfo.getFareDetail(), clientMarkUpFeeFareComponent, searchQuery,
								travellerInfo.getPaxType(), segmentInfo, user, tripInfo, 0);
					}
				});
			} else {
				if (segmentInfo.getSegmentNum() == 0) {
					singleTrip.set(splittedTrips.get(tripIndex.get()));
					tripIndex.getAndIncrement();
				}
				for (int priceIndex = 0; priceIndex < segmentInfo.getPriceInfoList().size(); priceIndex++) {
					PriceInfo priceInfo = segmentInfo.getPriceInfoList().get(priceIndex);
					boolean hideSeatEnabled = isHideSeatEnabled(sourceConfig, gnRule, priceInfo.getSupplierBasicInfo());

					for (Entry<PaxType, FareDetail> entry : priceInfo.getFareDetails().entrySet()) {
						setSeatRemaining(entry.getValue(), sourceConfig.getMaxSeatCountAllowed());
						if (hideSeatEnabled)
							entry.getValue().setSeatRemaining(null);
						addManagementFee(entry.getValue(), managementFeeFareComponent, searchQuery, entry.getKey(),
								segmentInfo, user, singleTrip.get(), priceIndex);
						addClientMarkup(entry.getValue(), clientMarkUpFeeFareComponent, searchQuery, entry.getKey(),
								segmentInfo, user, singleTrip.get(), priceIndex);
					}

					if (StringUtils.isNotBlank(priceInfo.getMiscInfo().getAccountCode())
							&& BooleanUtils.isTrue(priceInfo.getSupplierBasicInfo().getShowAccountCode())) {
						// SupplierRule supplierRule = AirUtils.getSupplierRule(priceInfo);
						priceInfo.setCode(priceInfo.getMiscInfo().getAccountCode());
					}
				}
			}
		});

	}

	private boolean isHideSeatEnabled(AirSourceConfigurationOutput sourceConfig, AirGeneralPurposeOutput gnRule,
			SupplierBasicInfo supplierInfo) {
		Boolean isHideSeatEnabled = sourceConfig.getIsHideSeatEnabled();
		if (isHideSeatEnabled != null) {
			return BooleanUtils.isTrue(isHideSeatEnabled);
		}
		if (CollectionUtils.isNotEmpty(gnRule.getIsHideSeatEnabled())) {
			return gnRule.getIsHideSeatEnabled().contains(supplierInfo.getSourceId());
		}
		return false;
	}

	private void setSeatRemaining(FareDetail fareDetail, Integer maxSeatCountAllowed) {
		if (Objects.nonNull(maxSeatCountAllowed) && maxSeatCountAllowed.intValue() > 0) {
			Integer seatRemaining = Objects.isNull(fareDetail.getSeatRemaining())
					|| fareDetail.getSeatRemaining() <= maxSeatCountAllowed ? fareDetail.getSeatRemaining()
							: maxSeatCountAllowed;
			fareDetail.setSeatRemaining(seatRemaining);
		}
	}

	private void addManagementFee(FareDetail fareDetail, FareComponent targetFareComponent, AirSearchQuery searchQuery,
			PaxType paxType, SegmentInfo segmentInfo, User user, TripInfo tripInfo, Integer priceIndex) {
		if (!fareDetail.getFareComponents().containsKey(targetFareComponent)) {
			String key =
					StringUtils.join(searchQuery.getAirType().getName(), "_", UserUtils.getUserId(user), "_ClientFee");
			AirClientFeeOutput clientFeeOutput =
					(AirClientFeeOutput) MapUtils.getObject(SystemContextHolder.getContextData().getValueMap(), key);
			if (clientFeeOutput == null) {
				clientFeeOutput =
						AirUtils.getClientFeeOutput(searchQuery, paxType, segmentInfo, user, tripInfo, priceIndex);
				/**
				 * This is to ensure that if none of the rule satisfy for a user then it won't attempt again.
				 */
				if (clientFeeOutput == null) {
					clientFeeOutput = new AirClientFeeOutput();
				}
				SystemContextHolder.getContextData().setValue(key, clientFeeOutput);
			}
			Double fee;
			if (Objects.nonNull(clientFeeOutput) && Objects.nonNull(fee = clientFeeOutput.getManagementFee())
					&& fee >= 0) {
				BaseUtils.updateFareComponent(fareDetail.getFareComponents(), targetFareComponent, fee, user);
			}
		}
	}

	private void addClientMarkup(FareDetail fareDetail, FareComponent targetFareComponent, AirSearchQuery searchQuery,
			PaxType paxType, SegmentInfo segmentInfo, User user, TripInfo tripInfo, Integer priceIndex) {

		if (!fareDetail.getFareComponents().containsKey(targetFareComponent)) {
			AirClientMarkupOutput clientMarkUpOutput =
					AirUtils.getClientMarkUpOutput(searchQuery, paxType, segmentInfo, user, tripInfo, priceIndex);
			Double fee;
			if (Objects.nonNull(clientMarkUpOutput) && Objects.nonNull(fee = clientMarkUpOutput.getAirClientMarkUp())
					&& fee >= 0) {
				BaseUtils.updateFareComponent(fareDetail.getFareComponents(), targetFareComponent, fee, user);
			}
		}
	}

	public static void filterSearchResultBasedUponSearchQueryFilters(AirSearchResult searchResult,
			AirSearchQuery searchQuery) {

	}

	public void resetCommercial(TripInfo tripInfo, User user) {
		commericialCommunicator.resetCommission(tripInfo, user);
		userFeeManager.resetMarkUp(tripInfo, user);
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
			PriceMiscInfo miscInfo = priceInfo.getMiscInfo();
			miscInfo.setFareRuleId(null);
		}
	}

	public void addXtraFare(TripInfo oldTripInfo, TripInfo newTripInfo, User user, AirGeneralPurposeOutput gnSourceRule,
			String bookingId) {
		if (BooleanUtils.isTrue(gnSourceRule.getIsDynamicSPBP())) {
			dynamicEngine.addXtraFareOnTrip(oldTripInfo, newTripInfo, user, bookingId);
		} else {
			bookPrivateEngine.addXtraFareOnTrip(oldTripInfo, newTripInfo, user, bookingId);
		}
	}

	private void sortSSRInfoOnPrice(TripInfo trip) {
		trip.getSegmentInfos().forEach(segmentInfo -> {
			if (MapUtils.isNotEmpty(segmentInfo.getSsrInfo())) {
				for (Entry<SSRType, List<? extends SSRInformation>> ssrTypeInfos : segmentInfo.getSsrInfo()
						.entrySet()) {
					List<? extends SSRInformation> ssrInfos = ssrTypeInfos.getValue();
					if (CollectionUtils.isNotEmpty(ssrInfos)) {
						Collections.sort(ssrInfos, new Comparator<SSRInformation>() {
							@Override
							public int compare(SSRInformation o1, SSRInformation o2) {
								if (o1.getAmount() != null && o2.getAmount() != null) {
									return Double.compare(o1.getAmount(), o2.getAmount());
								}
								return 0;
							}
						});
					}
				}
			}
		});
	}

	private static TripInfo removeCorporateFareForInfantSearch(AirSearchQuery searchQuery, TripInfo tripInfo) {
		if (AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT) > 0) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				List<PriceInfo> filteredPriceInfoList = segmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
					return !priceInfo.getFareType().equals(FareType.CORPORATE.getName())
							&& BooleanUtils.isNotTrue(priceInfo.getMiscInfo().getIsPrivateFare());
				}).collect(Collectors.toList());
				if (CollectionUtils.isEmpty(filteredPriceInfoList)) {
					return null;
				}
				segmentInfo.setPriceInfoList(filteredPriceInfoList);
			}
		}
		return tripInfo;
	}

	/**
	 * For LCC SSR cannot be selected before some hrs prior to departure. This method will validate the SSR's based on
	 * the restriction timings from @param sourceConfiguration
	 * 
	 * for eg: meal cannot be selected 6 hrs prior to the departure in 6E.
	 * 
	 * @param tripInfo
	 * @param sourceConfiguration
	 */
	public void processSSRInfoOnReview(TripInfo tripInfo, AirSourceConfigurationOutput sourceConfiguration,
			String bookingId) {

		if (sourceConfiguration == null) {
			sourceConfiguration = new AirSourceConfigurationOutput();
		}

		LocalDateTime time = LocalDateTime.now();
		LocalDateTime departureTime = tripInfo.getDepartureTime();

		/**
		 * @implNote :
		 * 
		 *           1. From SOURCECONFIG - we are picking API level Restriction , i.e : before departure SSR are not
		 *           allowed to book via API <br>
		 *           2. From SSR_FILTER - as client says Segment duration between config - Remove those SSR not allowed
		 *           for booking
		 * 
		 */
		// to handle in case of flythru case where having same flight number in connecting case
		List<TripInfo> tripInfos = tripInfo.splitTripInfo(true);

		for (TripInfo trip : tripInfos) {
			for (SegmentInfo segmentInfo : trip.getSegmentInfos()) {

				FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(trip, AirUtils.getAirType(trip))
						.generateFact(trip, 0).generateFact(segmentInfo).generateFact(segmentInfo.getPriceInfo(0));
				BaseUtils.createFactOnUser(flightFact, SystemContextHolder.getContextData().getUser());
				AirConfiguratorInfo configRule =
						AirConfiguratorHelper.getAirConfigRuleInfo(flightFact, AirConfiguratorRuleType.SSR_FILTER);
				AirSSRFilterConfiguration ssrFilter = null;
				if (configRule != null) {
					log.debug("Applying SSR Filter Config {} and id {}", bookingId, configRule.getId());
					ssrFilter = (AirSSRFilterConfiguration) configRule.getOutput();
				}

				Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
				if (MapUtils.isNotEmpty(segmentInfo.getSsrInfo()) && sourceConfiguration != null) {
					if (CollectionUtils.isNotEmpty(segmentInfo.getSsrInfo().get(SSRType.MEAL))) {
						// API Restrictions + SSR_Filter
						boolean isRemoveMeal =
								!AirUtils.isSSRValidForTrip(sourceConfiguration.getMealSSRRestrictionMinutes(),
										departureTime, time) || !AirUtils.isSSRApplicable(ssrFilter, SSRType.MEAL);
						if (isRemoveMeal) {
							log.debug("Removing Meal SSR due to restrictions {} and meals count {} and config {} ",
									bookingId, CollectionUtils.size(segmentInfo.getSsrInfo().get(SSRType.MEAL)),
									ssrFilter);
							removeSSRonLeg(trip, SSRType.MEAL, legNum, segmentInfo.getSegmentNum());
						}
					}
					if (CollectionUtils.isNotEmpty(segmentInfo.getSsrInfo().get(SSRType.BAGGAGE))) {
						// API Restrictions + SSR_Filter
						boolean isRemoveBaggage =
								!AirUtils.isSSRValidForTrip(sourceConfiguration.getBaggageSSRRestrictionMinutes(),
										departureTime, time) || !AirUtils.isSSRApplicable(ssrFilter, SSRType.BAGGAGE);
						if (isRemoveBaggage) {
							log.debug("Removing Baggage SSR due to restrictions {} and baggage count {} and config {}",
									bookingId, CollectionUtils.size(segmentInfo.getSsrInfo().get(SSRType.BAGGAGE)),
									ssrFilter);
							removeSSRonLeg(trip, SSRType.BAGGAGE, legNum, segmentInfo.getSegmentNum());
						}
					}
				}
			}
		}
	}

	private void removeSSRonLeg(TripInfo tripInfo, SSRType ssrType, Integer legNum, Integer segmentNum) {
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
			if (legNum != null && priceInfo.getMiscInfo().getLegNum() != null
					&& priceInfo.getMiscInfo().getLegNum() == legNum) {
				segmentInfo.getSsrInfo().remove(ssrType);
			} else if (segmentInfo.getSegmentNum() == segmentNum) {
				segmentInfo.getSsrInfo().remove(ssrType);
			}
		}
	}

	public static Integer getPaxTypeCount(AirSearchQuery searchQuery) {
		AtomicInteger paxTypeCount = new AtomicInteger(0);
		if (MapUtils.isNotEmpty(searchQuery.getPaxInfo())) {
			searchQuery.getPaxInfo().forEach(((paxType, count) -> {
				if (count > 0) {
					paxTypeCount.getAndIncrement();
				}
			}));
		}
		return paxTypeCount.get();
	}

	// This will set PNR credit for each passenger type in first segment of trip if applicable.
	public void processPNRCredit(TripInfo tripInfo, AirSearchQuery searchQuery) {
		if (searchQuery.isPNRCreditSearch()) {
			double creditBalance = searchQuery.getSearchModifiers().getPnrCreditInfo().getCreditBalance();
			int paxCount = tripInfo.getPaxCount();
			if (creditBalance > 0) {
				creditBalance = Math.floor(creditBalance / paxCount);
				for (PriceInfo priceInfo : tripInfo.getSegmentInfos().get(0).getPriceInfoList()) {
					for (Entry<PaxType, FareDetail> entry : priceInfo.getFareDetails().entrySet()) {
						if (entry.getValue() != null && MapUtils.isNotEmpty(entry.getValue().getFareComponents())) {
							entry.getValue().getFareComponents().put(FareComponent.CS, creditBalance);
						}
					}
					priceInfo.getMiscInfo()
							.setCreditShellPNR(searchQuery.getSearchModifiers().getPnrCreditInfo().getPnr());
				}
			}
		}
	}

	public void storeFlightTimings(AirSearchResult searchResult, SupplierConfiguration configuration, User user) {
		AirGeneralPurposeOutput gnSourceRule =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
		if (gnSourceRule != null && gnSourceRule.getTripTimingEnabledSources().contains(configuration.getSourceId())
				&& searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			searchResult.getTripInfos().forEach((tripType, trips) -> {
				/***
				 * @implNote : <br>
				 *           Mapping will be created based on airline wise from searchresults
				 */
				Map<String, List<TripInfo>> airlineWiseTrips =
						trips.stream().collect(Collectors.groupingBy(TripInfo::getAirlineCode));
				if (MapUtils.isNotEmpty(airlineWiseTrips)) {
					airlineWiseTrips.forEach((airline, tripInfo) -> {
						/**
						 * @implNote : <br>
						 *           1. In Case of Same Flight number through entire journey , we are not adding to
						 *           mapping to many cases<br>
						 *           &nbsp;&nbsp; 1.a : Direct Supplier can give 1 segment via stop<br>
						 *           &nbsp;&nbsp; 1.b : Inventory flight can be 2 segments without stop (in such cases
						 *           no proper timing)<br>
						 *           2. Under Every TripType Results , Unique Segment AirInventoryInfo will be added
						 *           <br>
						 *           Refer doc for More :
						 *           https://docs.google.com/document/d/1PnBxHlZW9jEynNgWpaUp_jRMV_AWhB2N07fxgZ4Lowk/edit
						 */
						Map<String, AirInventoryInfo> airlineTiming = new HashMap<>();
						String tripKey = StringUtils.join(tripInfo.get(0).getDepartureAirportCode(),
								tripInfo.get(0).getArrivalAirportCode(), "_", airline, "_",
								tripInfo.get(0).getDepartureTime().toLocalDate());
						for (Iterator<TripInfo> iter = tripInfo.iterator(); iter.hasNext();) {
							TripInfo trip = iter.next();
							setSegmentTiming(trip, airlineTiming);
						}
						if (MapUtils.isNotEmpty(airlineTiming)) {
							inventoryCommunicator.storeFlightTimings(tripKey, airlineTiming);
						}
					});
				}
			});
		}
	}

	public void resetCreditShellBalance(TripInfo selectedTrip, AirSearchQuery searchQuery) {
		if (searchQuery.isPNRCreditSearch()) {
			for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
				for (PriceInfo price : segmentInfo.getPriceInfoList()) {
					for (Entry<PaxType, FareDetail> entry : price.getFareDetails().entrySet()) {
						entry.getValue().getFareComponents().put(FareComponent.CS, 0d);
					}
				}
			}
		}
	}

	public void processBookingConditions(TripInfo tripInfo, SupplierConfiguration supplierConf) {
		if (BooleanUtils.isTrue(supplierConf.getSupplierAdditionalInfo().getIsPANRequired())) {
			tripInfo.getBookingConditions().setIsPANRequired(true);
		}
	}

}
