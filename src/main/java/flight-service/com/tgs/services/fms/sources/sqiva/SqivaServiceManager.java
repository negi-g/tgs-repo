package com.tgs.services.fms.sources.sqiva;

import static com.tgs.services.fms.sources.sqiva.SqivaConstant.ERR_CODE;
import static com.tgs.services.fms.sources.sqiva.SqivaConstant.ERR_MSG;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SuperBuilder
@Getter
@Setter
abstract class SqivaServiceManager {

	// serviceManager
	protected AirSearchQuery searchQuery;
	protected SqivaBindingService bindingService;
	protected SqivaAirline sqivaAirline;
	protected SupplierConfiguration configuration;
	protected User user;
	protected RestAPIListener listener;
	protected List<String> criticalMessageLogger;

	protected String bookignId;

	protected boolean isAnyError(JSONObject responseObj) {
		if (Objects.nonNull(responseObj) && responseObj.has(ERR_CODE) && responseObj.has(ERR_MSG)) {
			criticalMessageLogger.add(StringUtils.join(responseObj.get(ERR_CODE) + " -- " + responseObj.get(ERR_MSG)));
			return true;
		}
		return false;
	}

	protected List<SegmentInfo> getSubLegSegments(List<SegmentInfo> segmentInfos, int segmentIndex) {
		List<SegmentInfo> subLegSegments = new ArrayList<SegmentInfo>();
		for (; segmentIndex < segmentInfos.size(); segmentIndex++) {
			Integer nextSegmentLegNum = SqivaUtils.getNextLegNum(segmentInfos, segmentIndex);
			subLegSegments.add(segmentInfos.get(segmentIndex));
			if (nextSegmentLegNum == 0) {
				break;
			}
		}
		return subLegSegments;
	}

}
