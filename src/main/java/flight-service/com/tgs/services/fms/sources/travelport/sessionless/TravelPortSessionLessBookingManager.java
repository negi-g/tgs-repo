package com.tgs.services.fms.sources.travelport.sessionless;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.Builder;
import lombok.Getter;
import org.apache.axis2.databinding.types.YearMonth;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Pair;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.CardType;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import travelport.www.schema.air_v47_0.AirItinerary_type0;
import travelport.www.schema.air_v47_0.AirMerchandisingOfferAvailabilityReq;
import travelport.www.schema.air_v47_0.AirMerchandisingOfferAvailabilityRsp;
import travelport.www.schema.air_v47_0.AirPriceReq;
import travelport.www.schema.air_v47_0.AirPriceRsp;
import travelport.www.schema.air_v47_0.AirPricingInfoRef_type0;
import travelport.www.schema.air_v47_0.AirPricingInfo_type0;
import travelport.www.schema.air_v47_0.AirPricingPayment_type0;
import travelport.www.schema.air_v47_0.AirPricingSolution_type0;
import travelport.www.schema.air_v47_0.AirReservation_type0;
import travelport.www.schema.air_v47_0.AirSolution_type0;
import travelport.www.schema.air_v47_0.BaseAirPricingPaymentGroup;
import travelport.www.schema.air_v47_0.BaseAirPricingPaymentGroupChoice_type0;
import travelport.www.schema.air_v47_0.BookingInfo_type0;
import travelport.www.schema.air_v47_0.CodeshareInfo_type0;
import travelport.www.schema.air_v47_0.FareInfo_type0;
import travelport.www.schema.air_v47_0.HostReservation_type0;
import travelport.www.schema.air_v47_0.OptionalService_type0;
import travelport.www.schema.air_v47_0.OptionalServicesTotal_type0;
import travelport.www.schema.air_v47_0.OptionalServices_type0;
import travelport.www.schema.air_v47_0.PassengerType_type0;
import travelport.www.schema.air_v47_0.ProviderDefinedType_type1;
import travelport.www.schema.air_v47_0.SearchTraveler_type0;
import travelport.www.schema.air_v47_0.SeatMapReq;
import travelport.www.schema.air_v47_0.SeatMapRsp;
import travelport.www.schema.air_v47_0.TypeBaseAirSegment;
import travelport.www.schema.air_v47_0.TypePricingMethod;
import travelport.www.schema.common_v47_0.ActionStatus_type0;
import travelport.www.schema.common_v47_0.AddressName_type1;
import travelport.www.schema.common_v47_0.AgencyBillingIdentifier_type0;
import travelport.www.schema.common_v47_0.AgencyBillingPassword_type0;
import travelport.www.schema.common_v47_0.AgencyPayment;
import travelport.www.schema.common_v47_0.AgentAction_type0;
import travelport.www.schema.common_v47_0.BaseBookingTravelerInfoA;
import travelport.www.schema.common_v47_0.BaseBookingTravelerInfoB;
import travelport.www.schema.common_v47_0.BookingTravelerName_type0;
import travelport.www.schema.common_v47_0.BookingTraveler_type0;
import travelport.www.schema.common_v47_0.CVV_type0;
import travelport.www.schema.common_v47_0.Country_type1;
import travelport.www.schema.common_v47_0.CreditCard_type0;
import travelport.www.schema.common_v47_0.DeliveryInfo_type0;
import travelport.www.schema.common_v47_0.Email_type0;
import travelport.www.schema.common_v47_0.FormOfPaymentChoice_type1;
import travelport.www.schema.common_v47_0.FormOfPayment_type0;
import travelport.www.schema.common_v47_0.HostToken_type0;
import travelport.www.schema.common_v47_0.Location_type0;
import travelport.www.schema.common_v47_0.NameRemark_type0;
import travelport.www.schema.common_v47_0.Number_type0;
import travelport.www.schema.common_v47_0.Payment_type0;
import travelport.www.schema.common_v47_0.PhoneNumber_type0;
import travelport.www.schema.common_v47_0.SSR_type0;
import travelport.www.schema.common_v47_0.SearchPassenger_type0;
import travelport.www.schema.common_v47_0.ServiceData_type0;
import travelport.www.schema.common_v47_0.ServiceInfo_type0;
import travelport.www.schema.common_v47_0.ShippingAddress_type0;
import travelport.www.schema.common_v47_0.State_type0;
import travelport.www.schema.common_v47_0.Street_type1;
import travelport.www.schema.common_v47_0.TypeAgencyPayment;
import travelport.www.schema.common_v47_0.TypeGender;
import travelport.www.schema.common_v47_0.TypeMerchandisingService;
import travelport.www.schema.common_v47_0.TypeRef;
import travelport.www.schema.common_v47_0.TypeStructuredAddress;
import travelport.www.schema.common_v47_0.Type_type3;
import travelport.www.schema.common_v47_0.Type_type9;
import travelport.www.schema.universal_v47_0.AirAdd_type0;
import travelport.www.schema.universal_v47_0.AirCreateReservationReq;
import travelport.www.schema.universal_v47_0.AirCreateReservationRsp;
import travelport.www.schema.universal_v47_0.AirMerchandisingFulfillmentReq;
import travelport.www.schema.universal_v47_0.AirMerchandisingFulfillmentRsp;
import travelport.www.schema.universal_v47_0.RecordIdentifier_type0;
import travelport.www.schema.universal_v47_0.UniversalModifyCmd_type0;
import travelport.www.schema.universal_v47_0.UniversalRecordModifyReq;
import travelport.www.schema.universal_v47_0.UniversalRecordModifyRsp;
import travelport.www.schema.universal_v47_0.UniversalRecordRetrieveRsp;
import travelport.www.service.universal_v47_0.AirFaultMessage;
import travelport.www.service.universal_v47_0.AirServiceStub;
import travelport.www.service.universal_v47_0.AvailabilityFaultMessage;
import travelport.www.service.universal_v47_0.UniversalModifyFaultMessage;
import travelport.www.service.universal_v47_0.UniversalRecordFaultMessage;
import travelport.www.service.universal_v47_0.UniversalRecordModifyServiceStub;

@Slf4j
@SuperBuilder
@Getter
final class TravelPortSessionLessBookingManager extends TravelPortSessionLessServiceManager {

	protected List<FlightTravellerInfo> travellerInfos;

	protected String plattingCarrier;

	protected double totalAirlineCharges;

	protected ClientGeneralInfo clientInfo;

	protected AddressInfo contactAddress;

	protected GstInfo gstInfo;

	protected boolean isMerchandiseCallRequired;

	protected double seatSSRCharges;

	protected Map<String, Pair<String, String>> segHostRefMap;

	@Builder.Default
	protected boolean isSeatSSRAssignmentFailed = false;

	public void init(BookingSegments segments) {
		init();
		clientInfo = ServiceCommunicatorHelper.getClientInfo();
		contactAddress = AirSupplierUtils.getContactAddressInfo(bookingUser.getUserId(), clientInfo, configuration);
		travellerInfos = segments.getSegmentInfos().get(0).getTravellerInfo();
		segmentInfos = segments.getSegmentInfos();
		plattingCarrier = segments.getSegmentInfos().get(0).getPriceInfo(0).getPlattingCarrier();
		if (StringUtils.isNotBlank(travellerInfos.get(0).getPnr())
				&& StringUtils.isNotBlank(travellerInfos.get(0).getSupplierBookingId())) {
			pnr = travellerInfos.get(0).getPnr();
			universalPnr = travellerInfos.get(0).getSupplierBookingId();
			reservationPNR = travellerInfos.get(0).getReservationPNR();
		}
	}

	public void setKeyRef(BookingSegments segments) {
		Map<String, String> travellerKeyMap = new HashMap<>();
		for (PaxType paxType : PaxType.values()) {
			List<FlightTravellerInfo> travellers = AirUtils.getParticularPaxTravellerInfo(travellerInfos, paxType);
			List<String> travellerKeys = segments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo()
					.getBookingTravellerInfoRef().get(paxType);
			for (int index = 0; index < travellers.size(); index++) {
				travellerKeyMap.put(travellers.get(index).getFullName(), travellerKeys.get(index));
			}
		}
		segmentInfos.forEach(segment -> {
			segment.getTravellerInfo().forEach(traveller -> {
				traveller.getMiscInfo().setPassengerKey(travellerKeyMap.get(traveller.getFullName()));
			});
		});
	}

	public String createPNR(boolean isHoldBook, CreditCardInfo cardInfo) {
		AirServiceStub airServiceStub = bindingService.getAirCreateReservationStub();
		listener.setType(AirUtils.getLogType("AirCreateReservation", configuration));
		airServiceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
		AirCreateReservationReq createReservationRequest = createReservationRequest(isHoldBook, cardInfo);
		AirCreateReservationRsp createReservationResponse = null;
		try {
			bindingService.setProxyAndAuthentication(airServiceStub, "AirCreateReservation");
			createReservationResponse = airServiceStub.service(createReservationRequest, getSupportedVersion());
			if (!checkAnyErrors(createReservationResponse)) {
				// Used to retrieve Record.
				universalPnr = createReservationResponse.getUniversalRecord().getLocatorCode().getTypeLocatorCode();
				BookingUtils.updateSupplierBookingId(segmentInfos, universalPnr);
				if (ArrayUtils.isNotEmpty(createReservationResponse.getUniversalRecord().getAirReservation())
						&& createReservationResponse.getUniversalRecord().getAirReservation()[0] != null
						&& ArrayUtils.isNotEmpty(createReservationResponse.getUniversalRecord().getAirReservation()[0]
								.getSupplierLocator())
						&& createReservationResponse.getUniversalRecord().getAirReservation()[0]
								.getSupplierLocator()[0] != null) {
					reservationPNR = createReservationResponse.getUniversalRecord().getAirReservation()[0]
							.getLocatorCode().getTypeLocatorCode();
					setReservetionPNRForSegment(reservationPNR);
					pnr = createReservationResponse.getUniversalRecord().getAirReservation()[0].getSupplierLocator()[0]
							.getSupplierLocatorCode();
					BookingUtils.updateAirlinePnr(segmentInfos, pnr);
					AirReservation_type0 reservation =
							createReservationResponse.getUniversalRecord().getAirReservation()[0];
					setAirlineCharges(reservation);
					if (TravelPortSessionLessUtils.isSeatSSrSelected(getSegmentInfos())
							&& checkCriticalWarnings(createReservationResponse)) {
						isSeatSSRAssignmentFailed = true;
					}
				}
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} catch (AvailabilityFaultMessage | AirFaultMessage e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} finally {
			airServiceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}

		return pnr;

	}

	private AirCreateReservationReq createReservationRequest(boolean isHoldBook, CreditCardInfo creditCardInfo) {
		AirCreateReservationReq createReservationRequest = new AirCreateReservationReq();
		buildBaseCoreRequest(createReservationRequest);
		createReservationRequest.setBookingTraveler(buildBookingTraveller());
		createReservationRequest.setAirPricingSolution(buildAirPricing());
		createReservationRequest.setActionStatus(buildActionStatus());
		List<SSR_type0> ssrList = new ArrayList<>();
		AtomicInteger index = new AtomicInteger(1);
		buildGSTSSRDetail(ssrList, index);
		buildPassDetails(ssrList, index);
		createReservationRequest.setSSR(ssrList.toArray(new SSR_type0[0]));
		if (!isHoldBook) {
			createReservationRequest.setPayment(buildPayment());
			createReservationRequest.setFormOfPayment(
					buildFOP(creditCardInfo, createReservationRequest.getPayment()[0].getFormOfPaymentRef()));
		}
		return createReservationRequest;
	}

	private void buildBaseUniversalModifyRequest(UniversalRecordModifyReq universalRecordModifyReq) {
		buildBaseCoreRequest(universalRecordModifyReq);
		universalRecordModifyReq.setReturnRecord(Boolean.TRUE);
		universalRecordModifyReq.setVersion(getTypeURversion(version));
		buildRecordIdentifier(universalRecordModifyReq);
	}

	private void buildRecordIdentifier(UniversalRecordModifyReq universalRecordModifyReq) {
		RecordIdentifier_type0 recordIdentifier = new RecordIdentifier_type0();
		if (StringUtils.isNotBlank(providerCode)) {
			recordIdentifier.setProviderCode(getProviderCode(providerCode));
		}
		recordIdentifier.setProviderLocatorCode(getTypeProviderLocatorCode(pnr));
		recordIdentifier.setUniversalLocatorCode(getTypeLocatorCode(universalPnr));
		universalRecordModifyReq.setRecordIdentifier(recordIdentifier);
	}

	protected void modifyRecordWithFOP(UniversalRecordRetrieveRsp retreiveBooking, CreditCardInfo cardInfo) {
		UniversalRecordModifyReq universalRecordModifyReq = new UniversalRecordModifyReq();
		buildBaseUniversalModifyRequest(universalRecordModifyReq);
		UniversalModifyCmd_type0 universalModifyCmd = new UniversalModifyCmd_type0();
		universalModifyCmd.setKey(getTypeRef(TravelPortSessionLessUtils.keyGenerator()));
		AirAdd_type0 airAdd = new AirAdd_type0();
		airAdd.setReservationLocatorCode(getTypeLocatorCode(reservationPNR));
		airAdd.setAirPricingPayment(getAirPricingPayment(cardInfo,
				retreiveBooking.getUniversalRecord().getAirReservation()[0].getAirPricingInfo()));
		universalModifyCmd.setAirAdd(airAdd);
		universalRecordModifyReq.addUniversalModifyCmd(universalModifyCmd);
		modifyUniversalRecord(universalRecordModifyReq, "Add Payment");
	}

	private AirPricingPayment_type0 getAirPricingPayment(CreditCardInfo cardInfo,
			AirPricingInfo_type0[] airPricingInfos) {
		AirPricingPayment_type0 pricingPayment = new AirPricingPayment_type0();
		List<AirPricingInfoRef_type0> refList = new ArrayList<>();
		for (AirPricingInfo_type0 info : airPricingInfos) {
			AirPricingInfoRef_type0 ref = new AirPricingInfoRef_type0();
			ref.setKey(info.getKey());
			refList.add(ref);
		}
		pricingPayment.setPayment(buildPayment());
		pricingPayment.setFormOfPayment(buildFOP(cardInfo, pricingPayment.getPayment()[0].getFormOfPaymentRef()));
		pricingPayment.setAirPricingInfoRef(refList.toArray(new AirPricingInfoRef_type0[0]));
		BaseAirPricingPaymentGroup base = new BaseAirPricingPaymentGroup();
		BaseAirPricingPaymentGroupChoice_type0 basePrice = new BaseAirPricingPaymentGroupChoice_type0();
		base.setBaseAirPricingPaymentGroupChoice_type0(basePrice);
		pricingPayment.setBaseAirPricingPaymentGroup(base);
		return pricingPayment;
	}

	private UniversalRecordModifyRsp modifyUniversalRecord(UniversalRecordModifyReq modifyReq, String key) {
		UniversalRecordModifyServiceStub modifyServicde = bindingService.getUniversalRecordModifyService();
		UniversalRecordModifyRsp recordModifyRsp = null;
		try {
			listener.setType(AirUtils.getLogType(key, configuration));
			modifyServicde._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(modifyServicde, key);
			recordModifyRsp = modifyServicde.service(modifyReq, getSessionContext(false), getSupportedVersion());
			version = recordModifyRsp.getUniversalRecord().getVersion().getTypeURVersion();
			if (checkAnyErrors(recordModifyRsp)) {
				log.error("Universal Modify failed {} type {} cause {}", bookingId, key,
						String.join(",", criticalMessageLogger));
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			} else {
				log.info("Universal Modify is Success for bookingId {} Universal locator {} and Pnr {}", bookingId,
						universalPnr, pnr);
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} catch (AvailabilityFaultMessage | UniversalModifyFaultMessage | UniversalRecordFaultMessage fault) {
			throw new SupplierUnHandledFaultException(fault.getMessage());
		} finally {
			modifyServicde._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return recordModifyRsp;
	}


	private String getText(String code, String freeText) {
		return StringUtils.join(code, " ", freeText);
	}

	private void buildGSTSSRDetail(List<SSR_type0> ssrList, AtomicInteger index) {
		if (false && StringUtils.isNotBlank(gstInfo.getAddress())) {
			String freeText = "/IND/" + gstInfo.getAddress() + "/" + gstInfo.getCityName() + "/" + gstInfo.getState()
					+ "/" + gstInfo.getPincode();
			buildSSRComponents(ssrList, "GSTA", freeText, "", plattingCarrier, index);
		}
		if (gstInfo != null && StringUtils.isNotBlank(gstInfo.getGstNumber())
				&& StringUtils.isNotBlank(gstInfo.getRegisteredName())) {
			String registeredName = BaseUtils.getCleanGstName(gstInfo.getRegisteredName()).replaceAll("[^\\w\\s]", "")
					.replaceAll(" ", "");
			registeredName = TravelPortSessionLessUtils.getValidRegisteredName(registeredName);
			String freeText = "/IND/" + gstInfo.getGstNumber() + "/" + registeredName;
			buildSSRComponents(ssrList, "GSTN", freeText, "NN", plattingCarrier, index);
		}

		if (false && gstInfo != null && StringUtils.isNotBlank(gstInfo.getMobile())) {
			// Not Supported
			String freeText = "/IND/" + gstInfo.getMobile();
			buildSSRComponents(ssrList, "GSTP", freeText, "NN", plattingCarrier, index);
		}

		if (gstInfo != null && StringUtils.isNotBlank(gstInfo.getEmail())) {
			String gstEmail = AirSupplierUtils.removeIllegalCharacters(gstInfo.getEmail());
			gstEmail = gstInfo.getEmail().replaceAll("[@]", "//");
			gstEmail = gstEmail.replaceAll("[-]", "./");
			gstEmail = gstEmail.replaceAll("[_]", "..");
			String freeText = "/IND/" + gstEmail;
			buildSSRComponents(ssrList, "GSTE", freeText, "", plattingCarrier, index);
		}
	}

	public void buildPassDetails(List<SSR_type0> ssrList, AtomicInteger index) {
		Set<String> uniqueCarriers = TravelPortSessionLessUtils.getUniqueAirlines(segmentInfos);
		for (String carrier : uniqueCarriers) {
			for (FlightTravellerInfo travellerInfo : travellerInfos) {
				if (StringUtils.isNotBlank(travellerInfo.getPassportNumber()) && travellerInfo.getExpiryDate() != null
						&& travellerInfo.getMiscInfo() != null
						&& StringUtils.isNotBlank(travellerInfo.getMiscInfo().getPassengerKey())
						&& travellerInfo.getDob() != null) {
					buildSSRComponents(ssrList, TravelPortSessionLessConstants.TRAVEL_DOCUMENTS_INFORMATION,
							TravelPortSessionLessUtils.buildPassportFreeText(travellerInfo),
							TravelPortSessionLessConstants.PASSPORT_ACTON_CODE, carrier, index);
				}
			}
		}
	}

	private void buildSSRComponents(List<SSR_type0> ssrList, String type, String freeText, String status,
			String carrier, AtomicInteger index) {
		SSR_type0 ssr = new SSR_type0();
		ssr.setKey(getTypeRef(String.valueOf(index.get())));
		ssr.setType(getTypeSSrCode(type));
		ssr.setCarrier(getTypeCarrierCode(carrier));
		if (StringUtils.isNotBlank(status)) {
			ssr.setStatus(status);
		}
		ssr.setFreeText(getTypeSSrFreeText(freeText));
		ssrList.add(ssr);
		index.getAndIncrement();
	}

	protected void setAirlineCharges(AirReservation_type0 reservation) {
		AtomicDouble totalCharge = new AtomicDouble(0.0);
		Arrays.asList(reservation.getAirPricingInfo()).forEach(airPrice -> {
			int paxCount = airPrice.getPassengerType().length;
			totalCharge.addAndGet(getEquivalentTotalfare(airPrice, 1.0) * paxCount);
		});
		if (reservation.getOptionalServices() != null
				&& reservation.getOptionalServices().getOptionalServicesTotal() != null) {
			String ssrTotal =
					reservation.getOptionalServices().getOptionalServicesTotal().getApproximateTotalPrice() != null
							? reservation.getOptionalServices().getOptionalServicesTotal().getApproximateTotalPrice()
									.getTypeMoney()
							: reservation.getOptionalServices().getOptionalServicesTotal().getTotalPrice()
									.getTypeMoney();
			totalCharge.addAndGet(getAmountBasedOnCurrency(ssrTotal, ssrTotal.substring(0, 3), 1.0));
		}
		totalAirlineCharges = totalCharge.get();
	}

	private AirPricingSolution_type0[] buildAirPricing() {
		double totalFare, baseFare, tax, services;
		totalFare = baseFare = tax = services = 0;
		List<AirPricingSolution_type0> airPircingSolutions = new ArrayList<>();
		AirPricingSolution_type0 airPricingSolution = new AirPricingSolution_type0();
		List<HostToken_type0> tokenList = new ArrayList<>();
		airPricingSolution
				.setKey(getTypeRef(segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getAirPriceSolutionKey()));
		airPricingSolution.setAirSegment(getAirSegments(tokenList));
		airPricingSolution.setAirPricingInfo(getAirPricingInfo());
		airPricingSolution.setHostToken(tokenList.toArray(new HostToken_type0[0]));
		airPricingSolution.setOptionalServices(getOptionalService(false));
		for (SegmentInfo segment : segmentInfos) {
			if (segment.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
				for (FlightTravellerInfo traveller : segment.getTravellerInfo()) {
					String totalAirlineFare =
							TravelPortSessionLessUtils.getTotalAirlineFare(currencyCode, traveller.getFareDetail());
					String baseFareWithCurrencyCode =
							TravelPortSessionLessUtils.getBaseFare(currencyCode, traveller.getFareDetail());
					String taxFare = TravelPortSessionLessUtils.getTaxFare(currencyCode, traveller.getFareDetail());
					String optionalServiceCharges =
							TravelPortSessionLessUtils.getServiceFare(currencyCode, traveller.getFareDetail());
					totalFare += Double.valueOf(totalAirlineFare.substring(3));
					baseFare += Double.valueOf(baseFareWithCurrencyCode.substring(3));
					tax += Double.valueOf(taxFare.substring(3));
					services += Double.valueOf(optionalServiceCharges.substring(3));
					if (isMerchandiseCallRequired) {
						services -= traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.SP, 0.0);
						totalFare -= traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.SP, 0.0);
					}
				}
			}
		}
		airPricingSolution.setTotalPrice(getTypeMoney(StringUtils.join(currencyCode, totalFare)));
		airPricingSolution.setBasePrice(getTypeMoney(StringUtils.join(currencyCode, baseFare)));
		airPricingSolution.setTaxes(getTypeMoney(StringUtils.join(currencyCode, tax)));
		airPricingSolution.setApproximateTotalPrice(getTypeMoney(StringUtils.join(currencyCode, totalFare)));
		airPricingSolution.setApproximateBasePrice(getTypeMoney(StringUtils.join(currencyCode, baseFare)));
		airPricingSolution.setApproximateTaxes(getTypeMoney(StringUtils.join(currencyCode, tax)));
		airPricingSolution.setServices(getTypeMoney(StringUtils.join(currencyCode, services)));
		airPircingSolutions.add(airPricingSolution);
		return airPircingSolutions.toArray(new AirPricingSolution_type0[0]);
	}

	private OptionalServices_type0 getOptionalService(boolean addSSRPostPNRCreation) {
		AtomicDouble totalSSRAmount = new AtomicDouble();
		OptionalServices_type0 optionalServices = new OptionalServices_type0();
		optionalServices.setOptionalService(getServices(totalSSRAmount, addSSRPostPNRCreation));
		if (optionalServices.getOptionalService() != null) {
			optionalServices.setOptionalServicesTotal(getServicesTotal(totalSSRAmount.get()));
			return optionalServices;
		}
		return null;
	}

	private OptionalServicesTotal_type0 getServicesTotal(Double totalSSRAmount) {
		OptionalServicesTotal_type0 total = new OptionalServicesTotal_type0();
		String ssrAmount = StringUtils.join(currencyCode, String.valueOf(totalSSRAmount));
		total.setApproximateTotalPrice(getTypeMoney(ssrAmount));
		total.setApproximateBasePrice(getTypeMoney(ssrAmount));
		total.setTotalPrice(getTypeMoney(ssrAmount));
		total.setBasePrice(getTypeMoney(ssrAmount));
		return total;
	}

	private OptionalService_type0[] getServices(AtomicDouble totalSSRAmount, boolean addSSRPostPNRCreation) {
		List<OptionalService_type0> services = new ArrayList<>();
		for (SegmentInfo segment : segmentInfos) {
			for (FlightTravellerInfo traveller : segment.getTravellerInfo()) {

				// bagagge
				if (!addSSRPostPNRCreation && traveller.getSsrBaggageInfo() != null && segment.getSegmentNum() == 0) {
					OptionalService_type0 service =
							getService(traveller.getSsrBaggageInfo(), segment, traveller, totalSSRAmount, false);
					service.setType(getMerchandisingService(TravelPortSessionLessConstants.BAGGAGE));
					services.add(service);
				}

				// meal
				if (!addSSRPostPNRCreation && traveller.getSsrMealInfo() != null) {
					OptionalService_type0 service =
							getService(traveller.getSsrMealInfo(), segment, traveller, totalSSRAmount, false);
					service.setType(getMerchandisingService(TravelPortSessionLessConstants.MEAL_OR_BEVERAGE));
					services.add(service);
				}

				// seat
				if (traveller.getSsrSeatInfo() != null
						&& segment.getPriceInfoList().get(0).getMiscInfo().getLegNum() == 0) {
					if (addSSRPostPNRCreation || !isMerchandiseCallRequired) {
						OptionalService_type0 service =
								getService(traveller.getSsrSeatInfo(), segment, traveller, totalSSRAmount, true);
						service.setType(getMerchandisingService(TravelPortSessionLessConstants.SEAT));
						services.add(service);
					} else {
						seatSSRCharges = seatSSRCharges + traveller.getSsrSeatInfo().getAmount();
					}
				}
			}
		}
		if (CollectionUtils.isNotEmpty(services)) {
			return services.toArray(new OptionalService_type0[0]);
		}
		return null;
	}

	private OptionalService_type0 getService(SSRInformation ssrInfo, SegmentInfo segment, FlightTravellerInfo traveller,
			AtomicDouble totalSSRAmount, boolean isSeatSSR) {
		totalSSRAmount.addAndGet(ssrInfo.getAmount());
		List<ServiceData_type0> dataList = new ArrayList<>();
		ServiceData_type0 data = new ServiceData_type0();
		OptionalService_type0 service = new OptionalService_type0();
		ServiceInfo_type0 info = new ServiceInfo_type0();
		info.setDescription(new String[] {ssrInfo.getDesc()});
		if (isSeatSSR) {
			info.setDescription(new String[] {ssrInfo.getMiscInfo().getRemarks()});
			data.setData(traveller.getSsrSeatInfo().getKey());
		}
		data.setAirSegmentRef(getTypeRef(segment.getPriceInfo(0).getMiscInfo().getSegmentKey()));
		data.setBookingTravelerRef(getTypeRef(traveller.getMiscInfo().getPassengerKey()));
		dataList.add(data);
		service.setKey(getTypeRef(TravelPortSessionLessUtils.keyGenerator()));
		service.setServiceInfo(info);
		service.setServiceData(dataList.toArray(new ServiceData_type0[0]));
		String ssrCurreny = StringUtils.isNotBlank(ssrInfo.getMiscInfo().getPricingCurrency())
				? ssrInfo.getMiscInfo().getPricingCurrency()
				: currencyCode;
		String ssrAmount = StringUtils.join(ssrCurreny, String.valueOf(ssrInfo.getAmount()));
		service.setTotalPrice(getTypeMoney(ssrAmount));
		service.setBasePrice(getTypeMoney(ssrAmount));
		service.setApproximateBasePrice(getTypeMoney(ssrAmount));
		service.setApproximateTotalPrice(getTypeMoney(ssrAmount));
		if (StringUtils.isNotBlank(providerCode)) {
			service.setProviderCode(getTypeProviderCode(providerCode));
		}
		service.setProviderDefinedType(getProviderDefinedType(ssrInfo.getMiscInfo().getSeatCodeType()));
		service.setQuantity(BigInteger.valueOf(1));
		service.setSupplierCode(getTypeSupplierCode(segment.getAirlineCode(false)));
		service.setServiceStatus(TravelPortSessionLessConstants.SSR_STATUS);
		return service;
	}

	private ProviderDefinedType_type1 getProviderDefinedType(String key) {
		ProviderDefinedType_type1 type = new ProviderDefinedType_type1();
		type.setProviderDefinedType_type1(key);
		return type;
	}

	private TypeMerchandisingService getMerchandisingService(String serviceType) {
		TypeMerchandisingService type = new TypeMerchandisingService();
		type.setTypeMerchandisingService(serviceType);
		return type;
	}

	private AirPricingInfo_type0[] getAirPricingInfo() {
		List<AirPricingInfo_type0> pricingInfo = new ArrayList<>();
		for (PaxType paxType : PaxType.values()) {
			List<FlightTravellerInfo> travellers = AirUtils.getParticularPaxTravellerInfo(travellerInfos, paxType);
			if (CollectionUtils.isNotEmpty(travellers)) {
				AirPricingInfo_type0 priceInfo = new AirPricingInfo_type0();
				priceInfo.setKey(getTypeRef(
						segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getPaxPricingInfo().get(paxType).get(0)));
				priceInfo.setPricingMethod(TypePricingMethod.Auto);
				priceInfo.setProviderCode(getTypeProviderCode(configuration.getSupplierCredential().getProviderCode()));
				priceInfo.setPassengerType(getPassengerType(travellers));
				double totalFare = 0.0;
				double baseFare = 0.0;
				double totalTax = 0.0;
				double optionalServiceCharges = 0.0;
				List<FareInfo_type0> fareInfoList = null;
				List<BookingInfo_type0> bookingInfoList = null;
				for (int index = 0; index < segmentInfos.size(); index++) {
					SegmentInfo segment = segmentInfos.get(index);
					String fareInfoKey = segment.getPriceInfo(0).getMiscInfo().getFareInfoKeyMap().get(paxType);
					if (segment.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
						List<FlightTravellerInfo> passengers =
								AirUtils.getParticularPaxTravellerInfo(segment.getTravellerInfo(), paxType);
						FareDetail fd = passengers.get(0).getFareDetail();
						if (fareInfoList == null) {
							fareInfoList = new ArrayList<>();
						}
						FareInfo_type0 fareInfo = getFareInfo(segment, paxType);
						fareInfo.setKey(getTypeRef(fareInfoKey));
						fareInfo.setPassengerTypeCode(getTypePTC(TravelPortSessionLessUtils.getTypePTC(paxType)));
						fareInfoList.add(fareInfo);
						if (bookingInfoList == null) {
							bookingInfoList = new ArrayList<>();
						}
						BookingInfo_type0 bookingInfo = getBookingInfo(segment);
						bookingInfo.setFareInfoRef(getTypeRef(fareInfoKey));
						bookingInfoList.add(bookingInfo);
						totalFare = totalFare + fd.getAirlineFare();
						baseFare = baseFare + fd.getFareComponents().getOrDefault(FareComponent.BF, 0.0);
						totalTax = totalTax + TravelPortSessionLessUtils.getTaxFare(fd);
						optionalServiceCharges += TravelPortSessionLessUtils.getServiceFare(fd);
						if (isMerchandiseCallRequired) {
							optionalServiceCharges -= fd.getFareComponents().getOrDefault(FareComponent.SP, 0.0);
							totalFare -= fd.getFareComponents().getOrDefault(FareComponent.SP, 0.0);
						}
					}
				}
				priceInfo.setFareInfo(fareInfoList.toArray(new FareInfo_type0[0]));
				priceInfo.setBookingInfo(bookingInfoList.toArray(new BookingInfo_type0[0]));
				priceInfo.setTotalPrice(getTypeMoney(StringUtils.join(currencyCode, String.valueOf(totalFare))));
				priceInfo.setBasePrice(getTypeMoney(StringUtils.join(currencyCode, String.valueOf(baseFare))));
				priceInfo.setApproximateBasePrice(
						getTypeMoney(StringUtils.join(currencyCode, String.valueOf(baseFare))));
				priceInfo.setApproximateTotalPrice(
						getTypeMoney(StringUtils.join(currencyCode, String.valueOf(totalFare))));
				priceInfo.setTaxes(getTypeMoney(StringUtils.join(currencyCode, String.valueOf(totalTax))));
				priceInfo.setServices(
						getTypeMoney(StringUtils.join(currencyCode, String.valueOf(optionalServiceCharges))));


				pricingInfo.add(priceInfo);
			}
		}
		return pricingInfo.toArray(new AirPricingInfo_type0[0]);

	}

	private PassengerType_type0[] getPassengerType(List<FlightTravellerInfo> travellerInfo) {
		List<PassengerType_type0> passengerType = new ArrayList<>();
		for (FlightTravellerInfo traveller : travellerInfo) {
			PassengerType_type0 passenger = new PassengerType_type0();
			passenger.setCode(getTypePTC(TravelPortSessionLessUtils.getTypePTC(traveller.getPaxType())));
			passenger.setBookingTravelerRef(traveller.getMiscInfo().getPassengerKey());
			passengerType.add(passenger);
		}
		return passengerType.toArray(new PassengerType_type0[0]);
	}

	private TypeBaseAirSegment[] getAirSegments(List<HostToken_type0> tokenList) {
		List<TypeBaseAirSegment> airSegments = new ArrayList<>();
		TypeBaseAirSegment airSegment = null;
		int groupId = 0;
		for (int index = 0; index < segmentInfos.size(); index++) {
			SegmentInfo segment = segmentInfos.get(index);
			String hostTokenKey = segment.getPriceInfo(0).getMiscInfo().getTokenId().split(",")[0];
			String segmentKey = segment.getPriceInfo(0).getMiscInfo().getSegmentKey();
			int legNum = segment.getPriceInfo(0).getMiscInfo().getLegNum();
			int nextSegmentLegNum = index + 1 < segmentInfos.size()
					? segmentInfos.get(index + 1).getPriceInfo(0).getMiscInfo().getLegNum()
					: 0;
			if (legNum == 0) {
				airSegment = new TypeBaseAirSegment();
				HostToken_type0 hostToken = new HostToken_type0();
				airSegment.setCodeshareInfo(getCodeShare(segment));
				if (MapUtils.isNotEmpty(segHostRefMap) && segHostRefMap.get(segmentKey) != null) {
					hostToken.setKey(getTypeRef(segHostRefMap.get(segmentKey).getKey()));
					hostToken.setString(segHostRefMap.get(segmentKey).getValue());
					airSegment.setHostTokenRef(segHostRefMap.get(segmentKey).getKey());
				} else {
					hostToken.setKey(getTypeRef(hostTokenKey));
					hostToken.setString(segment.getPriceInfo(0).getMiscInfo().getTokenId().split(",")[1]);
					airSegment.setHostTokenRef(hostTokenKey);
				}
				tokenList.add(hostToken);
				airSegment.setKey(getTypeRef(segmentKey));
				airSegment.setGroup(groupId);
				airSegment.setCarrier(getTypeCarrierCode(segment.getAirlineCode(true)));
				airSegment.setFlightNumber(getTypeFlightNumber(segment.getFlightNumber()));
				airSegment.setOrigin(getIATACode(segment.getDepartureAirportCode()));
				airSegment.setDestination(getIATACode(segment.getArrivalAirportCode()));
				airSegment.setDepartureTime(segment.getDepartTime().toString());
				airSegment.setArrivalTime(segment.getArrivalTime().toString());
				if (StringUtils.isNotBlank(providerCode)) {
					airSegment.setProviderCode(getTypeProviderCode(providerCode));
				}
				airSegment.setSupplierCode(getTypeSupplierCode(segment.getAirlineCode(false)));
				airSegment.setClassOfService(getTypeClassOfService(
						segment.getPriceInfo(0).getFareDetail(PaxType.ADULT).getClassOfBooking()));
			}
			airSegment.setNumberOfStops(legNum);
			airSegment.setDestination(getIATACode(segment.getArrivalAirportCode()));
			airSegment.setArrivalTime(TravelPortSessionLessUtils.getStringDateTime(segment.getArrivalTime()));
			if (nextSegmentLegNum == 0) {
				if (index + 1 < segmentInfos.size()) {
					SegmentInfo nextSegmentInfo = segmentInfos.get(index + 1);
					if (nextSegmentInfo.getSegmentNum() == 0) {
						groupId++;
					}
				}
				airSegments.add(airSegment);
			}
		}
		return airSegments.toArray(new TypeBaseAirSegment[0]);
	}

	private BookingInfo_type0 getBookingInfo(SegmentInfo segment) {
		BookingInfo_type0 bookingInfo = new BookingInfo_type0();
		bookingInfo.setSegmentRef(getTypeRef(segment.getPriceInfo(0).getMiscInfo().getSegmentKey()));
		bookingInfo.setBookingCode(segment.getPriceInfo(0).getBookingClass(PaxType.ADULT));
		bookingInfo.setCabinClass(TravelPortSessionLessUtils
				.getCabinClass(segment.getPriceInfo(0).getCabinClass(PaxType.ADULT).getName()));
		bookingInfo.setHostTokenRef(getTypeRef(segment.getPriceInfo(0).getMiscInfo().getTokenId().split(",")[0]));
		return bookingInfo;
	}

	private FareInfo_type0 getFareInfo(SegmentInfo segment, PaxType paxType) {
		FareInfo_type0 fareInfo = new FareInfo_type0();
		fareInfo.setKey(getTypeRef(segment.getPriceInfo(0).getMiscInfo().getFareRuleInfoRef()));
		fareInfo.setOrigin(getIATACode(segment.getDepartureAirportCode()));
		fareInfo.setDestination(getIATACode(segment.getArrivalAirportCode()));
		fareInfo.setFareBasis(segment.getPriceInfo(0).getFareBasis(paxType));
		fareInfo.setEffectiveDate(LocalDateTime.now().toLocalDate().toString());
		fareInfo.setSupplierCode(getTypeSupplierCode(segment.getAirlineCode(false)));
		if (segment.getPriceInfo(0).getMiscInfo().getIsPromotionalFare() != null) {
			fareInfo.setPromotionalFare(segment.getPriceInfo(0).getMiscInfo().getIsPromotionalFare());
		}
		// fareInfo.setFareFamily(getTypeFareFamily(segment.getPriceInfo(0).getBookingClass(PaxType.ADULT)));
		return fareInfo;
	}

	private CodeshareInfo_type0 getCodeShare(SegmentInfo segment) {
		CodeshareInfo_type0 codeShare = new CodeshareInfo_type0();
		codeShare.setOperatingFlightNumber(getTypeFlightNumber(segment.getFlightNumber()));
		codeShare.setOperatingCarrier(getTypeCarrierCode(segment.getAirlineCode(true)));
		codeShare.setString(StringUtils.EMPTY);
		return codeShare;
	}

	private Payment_type0[] buildPayment() {
		List<Payment_type0> paymentList = new ArrayList<>();
		Payment_type0 payment = new Payment_type0();
		payment.setKey(getTypeRef(TravelPortSessionLessUtils.keyGenerator()));
		double paymentAmount = BookingUtils.getAmountChargedByAirline(segmentInfos);
		if (isMerchandiseCallRequired) {
			paymentAmount -= seatSSRCharges;
		}
		payment.setAmount(getTypeMoney(currencyCode.concat(String.valueOf(paymentAmount))));
		payment.setType(Type_type3.Itinerary);
		// The credit card that is will be used to make this payment.
		payment.setFormOfPaymentRef(getTypeRef(TravelPortSessionLessUtils.keyGenerator()));
		paymentList.add(payment);
		return paymentList.toArray(new Payment_type0[0]);
	}

	private ActionStatus_type0[] buildActionStatus() {
		List<ActionStatus_type0> action = new ArrayList<>();
		ActionStatus_type0 actionStatus = new ActionStatus_type0();
		if (StringUtils.isNotBlank(providerCode)) {
			actionStatus.setProviderCode(getProviderCode(providerCode));
		}
		actionStatus.setTicketDate(TravelPortSessionLessConstants.TICKET_DATE);
		actionStatus.setType(Type_type9.ACTIVE);
		action.add(actionStatus);
		return action.toArray(new ActionStatus_type0[0]);
	}

	private FormOfPayment_type0[] buildFOP(CreditCardInfo creditCardInfo, TypeRef paymentFOPRefKey) {
		FormOfPayment_type0[] fopList = new FormOfPayment_type0[1];
		FormOfPayment_type0 formOfPayment = new FormOfPayment_type0();
		FormOfPaymentChoice_type1 fopChoice = new FormOfPaymentChoice_type1();
		formOfPayment.setKey(paymentFOPRefKey);
		if (creditCardInfo != null && StringUtils.isNotBlank(creditCardInfo.getCardNumber())) {
			formOfPayment.setType(getType_Type6(TravelPortSessionLessConstants.CREDIT_CARD_MODE));
			CreditCard_type0 creditCard = getCreditCard(creditCardInfo);
			fopChoice.setCreditCard(creditCard);
		} else {
			formOfPayment.setType(getType_Type6(TravelPortSessionLessConstants.AGENCY_PAYMENT));
			AgencyPayment agencyPayment = new AgencyPayment();
			TypeAgencyPayment typeAgencyPayment = new TypeAgencyPayment();
			AgencyBillingIdentifier_type0 billingIdentifier = new AgencyBillingIdentifier_type0();
			billingIdentifier.setAgencyBillingIdentifier_type0(getCredential().getAgentUserName());
			typeAgencyPayment.setAgencyBillingIdentifier(billingIdentifier);
			AgencyBillingPassword_type0 password_type0 = new AgencyBillingPassword_type0();
			password_type0.setAgencyBillingPassword_type0(getCredential().getAgentPassword());
			typeAgencyPayment.setAgencyBillingPassword(password_type0);
			agencyPayment.setAgencyPayment(typeAgencyPayment);
			fopChoice.setAgencyPayment(agencyPayment);
		}
		formOfPayment.setFormOfPaymentChoice_type1(fopChoice);
		fopList[0] = formOfPayment;
		return fopList;
	}

	private CreditCard_type0 getCreditCard(CreditCardInfo creditCardInfo) {
		CreditCard_type0 creditCard = new CreditCard_type0();
		String cardNumber = creditCardInfo.getCardNumber();
		String cardHolderName = creditCardInfo.getHolderName();
		String cardType = creditCardInfo.getCardType().getVendorCode();
		creditCard.setKey(getTypeRef(TravelPortSessionLessUtils.keyGenerator()));
		creditCard.setExpDate(getExpiryDate(creditCardInfo));
		creditCard.setNumber(getTypeCreditCard(cardNumber));
		creditCard.setCVV(getCVV(creditCardInfo.getCvv()));
		if (StringUtils.isNotBlank(cardHolderName))
			creditCard.setName(getNameType(cardHolderName));
		creditCard.setType(getTypeMerchant(cardType));
		if (cardType.equals(CardType.MASTERCARD.getVendorCode()))
			creditCard.setType(getTypeMerchant("MC"));
		setBillingAddress(creditCard);
		return creditCard;
	}

	private CVV_type0 getCVV(String cvvNum) {
		CVV_type0 cvv = new CVV_type0();
		cvv.setCVV_type0(cvvNum);
		return cvv;
	}

	private YearMonth getExpiryDate(CreditCardInfo creditCardInfo) {
		YearMonth yearMonth = null;
		try {
			if (StringUtils.isNotBlank(creditCardInfo.getExpiry())) {
				String[] monthYear = creditCardInfo.getExpiry().split("/");
				yearMonth = new YearMonth(Integer.parseInt(monthYear[1]), Integer.parseInt(monthYear[0]));
			}
		} catch (Exception e) {
			log.error("Failed to parse credit card expiry date {}", bookingId, e);
		}
		return yearMonth;
	}

	private void setBillingAddress(CreditCard_type0 creditCard) {
		ClientGeneralInfo companyInfo = ServiceCommunicatorHelper.getClientInfo();
		if (companyInfo != null && StringUtils.isNotBlank(companyInfo.getNationality())) {
			TypeStructuredAddress billingAddress = new TypeStructuredAddress();
			billingAddress.setAddressName(getAddressName(companyInfo.getAddress1()));
			billingAddress.setStreet(getStreet());
			billingAddress.setCity(getCityType(companyInfo.getCity()));
			State_type0 state = new State_type0();
			state.setString(companyInfo.getState());
			billingAddress.setState(state);
			billingAddress.setPostalCode(getPostalCode(companyInfo.getPostalCode()));
			billingAddress.setCountry(getCountry(companyInfo.getNationality()));
			creditCard.setBillingAddress(billingAddress);
		}
	}

	private BookingTraveler_type0[] buildBookingTraveller() {
		List<BookingTraveler_type0> travellersList = new ArrayList<>();
		int adultIterated = 0;
		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			BaseBookingTravelerInfoA bookingTravelerInfo = new BaseBookingTravelerInfoA();
			BookingTraveler_type0 bookingTraveler = new BookingTraveler_type0();
			// pax key
			bookingTraveler.setKey(getTypeRef(travellerInfo.getMiscInfo().getPassengerKey()));
			bookingTraveler.setGender(getGender(TravelPortSessionLessUtils.getGender(travellerInfo)));
			if (travellerInfo.getDob() != null) {
				bookingTraveler.setDOB(travellerInfo.getDob().toString());
			}
			bookingTraveler.setAge(new BigInteger(TravelPortSessionLessUtils.getAge(travellerInfo)));
			bookingTraveler
					.setTravelerType(getTypePTC(TravelPortSessionLessUtils.getTypePTC(travellerInfo.getPaxType())));
			BookingTravelerName_type0 bookingTravelerName = new BookingTravelerName_type0();
			bookingTravelerName.setFirst(getFirst_type2(TravelPortSessionLessUtils.getFirstName(travellerInfo)));
			bookingTravelerName.setLast(getTravellerLastname(TravelPortSessionLessUtils.getLastName(travellerInfo)));
			bookingTravelerName.setPrefix(getPrefix(TravelPortSessionLessUtils.getPaxPrefix(travellerInfo)));
			bookingTravelerInfo.setBookingTravelerName(bookingTravelerName);

			PhoneNumber_type0 phoneNumber = new PhoneNumber_type0();
			phoneNumber.setNumber(getNumberType0(AirSupplierUtils.getContactNumber(deliveryInfo)));
			phoneNumber.setCountryCode(getCountryType(clientInfo.getCountryCode()));
			phoneNumber.setLocation(getLocation(clientInfo.getNationality()));
			bookingTravelerInfo.addPhoneNumber(phoneNumber);

			Email_type0 email = new Email_type0();
			email.setEmailID(AirSupplierUtils.getEmailId(deliveryInfo));
			bookingTravelerInfo.addEmail(email);
			if (travellerInfo.getPaxType() == PaxType.INFANT && travellerInfo.getDob() != null) {
				NameRemark_type0 dobRemark = new NameRemark_type0();
				dobRemark.setRemarkData(TravelPortSessionLessUtils.createDate(travellerInfo.getDob()));
				bookingTraveler.addNameRemark(dobRemark);
			}

			if (travellerInfo.getPaxType() == PaxType.CHILD) {
				NameRemark_type0 dobRemark = new NameRemark_type0();
				String age = TravelPortSessionLessUtils.getAge(travellerInfo);
				bookingTraveler.setAge(new BigInteger(age));
				dobRemark.setRemarkData(StringUtils.join("P-C", age));
				bookingTraveler.addNameRemark(dobRemark);
				if (TravelPortSessionLessUtils.isChildPaxRequiresSSR(plattingCarrier)) {
					SSR_type0 ssr = new SSR_type0();
					TypeRef key = new TypeRef();
					key.setTypeRef(TravelPortSessionLessConstants.CHD_SSR_KEY);
					ssr.setKey(key);
					ssr.setType(getTypeSSrCode(TravelPortSessionLessConstants.CHLD));
					ssr.setStatus(TravelPortSessionLessConstants.HK);
					ssr.setFreeText(getTypeSSrFreeText(
							TravelPortSessionLessUtils.createDate(getDob(travellerInfo.getDob(), age))));
					bookingTraveler.addSSR(ssr);
				}
			}
			if (PaxType.ADULT.equals(travellerInfo.getPaxType()) && adultIterated == 0) {
				bookingTravelerInfo.setDeliveryInfo(deliveryInfo(travellerInfo.getMiscInfo().getPassengerKey()));
				adultIterated++;
			}
			bookingTraveler.setBaseBookingTravelerInfoA(bookingTravelerInfo);
			bookingTraveler.setBaseBookingTravelerInfoB(getTravellerInfoB());
			travellersList.add(bookingTraveler);
		}
		return travellersList.toArray(new BookingTraveler_type0[0]);
	}

	private BaseBookingTravelerInfoB getTravellerInfoB() {
		BaseBookingTravelerInfoB info = new BaseBookingTravelerInfoB();
		info.setAddress(getAddress());
		return info;
	}

	private TypeStructuredAddress[] getAddress() {
		List<TypeStructuredAddress> addressList = new ArrayList<>();
		TypeStructuredAddress add = new TypeStructuredAddress();
		AddressName_type1 address = new AddressName_type1();
		address.setAddressName_type0(TravelPortSessionLessUtils.subStr(contactAddress.getAddress()));
		add.setAddressName(address);
		add.setStreet(getStreet());
		add.setCountry(getCountry(clientInfo.getNationality()));
		add.setPostalCode(getPostalCode(contactAddress.getPincode()));
		add.setState(getState(contactAddress.getCityInfo().getState()));
		add.setCity(getCityType(contactAddress.getCityInfo().getName()));
		addressList.add(add);
		return addressList.toArray(new TypeStructuredAddress[0]);
	}

	private Street_type1[] getStreet() {
		List<Street_type1> streetList = new ArrayList<>();
		Street_type1 street = new Street_type1();
		street.setStreet_type0(TravelPortSessionLessUtils.subStr(contactAddress.getAddress()));
		streetList.add(street);
		return streetList.toArray(new Street_type1[0]);
	}

	private TypeGender getGender(String gender) {
		TypeGender paxGender = new TypeGender();
		paxGender.setTypeGender(gender);
		return paxGender;
	}

	private Location_type0 getLocation(String nationality) {
		Location_type0 location = new Location_type0();
		location.setLocation_type0(nationality);
		return location;
	}

	private DeliveryInfo_type0[] deliveryInfo(String travellerKey) {
		List<DeliveryInfo_type0> infos = new ArrayList<>();
		DeliveryInfo_type0 info = new DeliveryInfo_type0();
		info.setShippingAddress(getShippingAdd(travellerKey));
		infos.add(info);
		return infos.toArray(new DeliveryInfo_type0[0]);
	}

	private ShippingAddress_type0 getShippingAdd(String travellerKey) {
		ShippingAddress_type0 add = new ShippingAddress_type0();
		add.setKey(getTypeRef(travellerKey));
		add.setCountry(getCountry(clientInfo.getNationality()));
		add.setPostalCode(getPostalCode(contactAddress.getPincode()));
		add.setState(getState(contactAddress.getCityInfo().getState()));
		add.setCity(getCityType(contactAddress.getCityInfo().getName()));
		add.setStreet(getStreet());
		return add;
	}

	private State_type0 getState(String state) {
		State_type0 stateType = new State_type0();
		stateType.setString(state);
		return stateType;
	}

	private Country_type1 getCountry(String nationality) {
		Country_type1 country = new Country_type1();
		country.setCountry_type0(nationality);
		return country;
	}

	protected Number_type0 getNumberType0(String contactNumber) {
		Number_type0 number = new Number_type0();
		number.setNumber_type0(contactNumber);
		return number;
	}

	protected boolean isTicketedAction(UniversalRecordRetrieveRsp retreiveBooking, String status) {
		for (AgentAction_type0 action : retreiveBooking.getUniversalRecord().getAgencyInfo().getAgentAction()) {
			if (status.equalsIgnoreCase(action.getActionType().getValue())) {
				return true;
			}
		}
		return false;
	}

	protected boolean isSegmentStatus(UniversalRecordRetrieveRsp retreiveBooking, String status) {
		for (TypeBaseAirSegment segment : retreiveBooking.getUniversalRecord().getAirReservation()[0].getAirSegment()) {
			if (!status.equals(segment.getStatus())) {
				return false;
			}
		}
		return true;
	}

	public AirPriceRsp sellwithSSR() {
		AirPriceRsp airPriceRsp = null;
		try {
			AirPriceReq airPriceReq = buildAirPriceRequest();
			airPriceRsp = getAirPriceResponse(airPriceReq, "Air RePrice");
		} catch (Exception e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		}
		return airPriceRsp;
	}

	private AirPriceReq buildAirPriceRequest() {
		AirPriceReq airPriceReq = new AirPriceReq();
		buildBaseCoreRequest(airPriceReq);
		buildAirItinerary(airPriceReq);
		return airPriceReq;
	}

	private void buildAirItinerary(AirPriceReq airPriceReq) {
		AirItinerary_type0 airItinerary = new AirItinerary_type0();
		List<HostToken_type0> tokenList = new ArrayList<>();
		airItinerary.setAirSegment(getAirSegments(tokenList));
		airItinerary.setHostToken(tokenList.toArray(new HostToken_type0[0]));
		airPriceReq.setAirItinerary(airItinerary);
		buildSearchPassengerValues(airPriceReq);
		buildAirPricingCommand(segmentInfos, airPriceReq, true);
		airPriceReq.setOptionalServices(getOptionalService(false));
	}

	private void buildSearchPassengerValues(AirPriceReq airPriceReq) {
		int tempChildAge = Integer.valueOf(TravelPortSessionLessConstants.CHILD_AGE);
		for (FlightTravellerInfo traveller : travellerInfos) {
			SearchPassenger_type0 passenger = new SearchPassenger_type0();
			passenger.setCode(getTypePTC(TravelPortSessionLessUtils.getTypePTC(traveller.getPaxType())));
			passenger.setBookingTravelerRef(traveller.getMiscInfo().getPassengerKey());
			if (PaxType.CHILD.equals(traveller.getPaxType())) {
				passenger.setAge(new BigInteger(String.valueOf(tempChildAge)));
				tempChildAge++;
			}
			if (PaxType.INFANT.equals(traveller.getPaxType())) {
				passenger.setAge(new BigInteger(TravelPortSessionLessConstants.INFANT_AGE));
				passenger.setPricePTCOnly(Boolean.FALSE);
			}
			airPriceReq.addSearchPassenger(passenger);
		}
	}

	private void setReservetionPNRForSegment(String reservationPNR) {
		segmentInfos.forEach(segmentInfo -> {
			segmentInfo.updateReservationPnr(reservationPNR);
		});
	}

	public boolean addSeatSSRForBookingWithInfants(boolean isHoldBook, CreditCardInfo cardInfo) {
		boolean isSuccess = false;
		getSeatMap();
		merchandiseOfferAvailability();
		travelport.www.service.universal_v47_0.MerchandiseService.AirServiceStub airServiceStub =
				bindingService.getAirMerchandiseFulfillmentStub();
		listener.setType(AirUtils.getLogType("AirMerchandisingFulfillment", configuration));
		airServiceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
		AirMerchandisingFulfillmentReq request = merchandisingFulfilmentRequest(isHoldBook, cardInfo);
		AirMerchandisingFulfillmentRsp response = null;
		try {
			bindingService.setProxyAndAuthentication(airServiceStub, "AirMerchandisingFulfillment");
			response = airServiceStub.service(request, getSupportedVersion());
			if (!checkAnyErrors(response)) {
				// The success message “One or more ancillary services saved successfully.” is returned in
				// ResponseMessage (Type=”Info” and Code “4436”).
				isSuccess = true;
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re.getMessage());
		} catch (AirFaultMessage e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} finally {
			airServiceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}

	private void getSeatMap() {
		// if (segHostRefMap == null) {
		// segHostRefMap = new HashMap<String, Pair<String, String>>();
		// }
		// listener = new SoapRequestResponseListner(bookingId, null, configuration.getBasicInfo().getSupplierName());
		// String traceId = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		// String providerCode = configuration.getSupplierCredential().getProviderCode();
		// TravelPortSessionLessSeatManager seatManager = TravelPortSessionLessSeatManager.builder()
		// .searchQuery(searchQuery).sourceConfiguration(sourceConfiguration).configuration(configuration)
		// .bindingService(bindingService).traceId(traceId).moneyExchnageComm(moneyExchnageComm)
		// .bookingId(bookingId).providerCode(providerCode).listener(listener).bookingUser(bookingUser).build();
		// seatManager.init();
		// TripInfo trip = new TripInfo();
		// trip.setSegmentInfos(segmentInfos);
		// SeatMapRsp response = seatManager.getSeatMapRsp(getSeatMapRequest(segmentInfos));
		// for (TypeBaseAirSegment segment : response.getAirSegment()) {
		// Pair<String, String> pair = new Pair<String, String>(segment.getHostTokenRef(),
		// getHostTokenValue(segment.getHostTokenRef(), response.getHostToken()));
		// segHostRefMap.put(segment.getKey().getTypeRef(), pair);
		// }
	}

	private void merchandiseOfferAvailability() {
		if (segHostRefMap == null) {
			segHostRefMap = new HashMap<String, Pair<String, String>>();
		}
		travelport.www.service.air_v47_0.MerchandiseOfferAvailability.AirServiceStub airServiceStub =
				bindingService.getAirMerchandiseOfferAvailabilityStub();
		listener.setType(AirUtils.getLogType("AirMerchandisingOfferAvailability", configuration));
		airServiceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
		AirMerchandisingOfferAvailabilityReq request = merchandisingOfferAvailRequest();
		AirMerchandisingOfferAvailabilityRsp response = null;
		try {
			bindingService.setProxyAndAuthentication(airServiceStub, "AirMerchandisingOfferAvailability");
			response = airServiceStub.service(request);
			if (!checkAnyErrors(response)) {
				for (TypeBaseAirSegment segment : response.getAirSolution().getAirSegment()) {
					Pair<String, String> pair = new Pair<String, String>(segment.getHostTokenRef(),
							getHostTokenValue(segment.getHostTokenRef(), response.getAirSolution().getHostToken()));
					segHostRefMap.put(segment.getKey().getTypeRef(), pair);
				}
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re.getMessage());
		} catch (travelport.www.service.air_v47_0.AirFaultMessage e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} finally {
			airServiceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private String getHostTokenValue(String hostTokenRef, HostToken_type0[] hostToken) {
		for (HostToken_type0 host : hostToken) {
			if (hostTokenRef.equals(host.getKey().getTypeRef())) {
				return host.getString();
			}
		}
		return null;
	}

	private AirMerchandisingOfferAvailabilityReq merchandisingOfferAvailRequest() {
		AirMerchandisingOfferAvailabilityReq req = new AirMerchandisingOfferAvailabilityReq();
		buildBaseCoreRequest(req);
		req.setHostReservation(getHostReservation());
		return req;
	}

	private SeatMapReq getSeatMapRequest(List<SegmentInfo> segments) {
		SeatMapReq req = new SeatMapReq();
		buildBaseCoreRequest(req);
		List<HostToken_type0> tokenList = new ArrayList<>();
		req.setAirSegment(getAirSegments(tokenList));
		// req.setHostReservation(getHostReservation()[0]);
		req.setReturnSeatPricing(true);
		req.setHostToken(tokenList.toArray(new HostToken_type0[0]));
		req.setSearchTraveler(getSearchTravellers());
		return req;
	}

	private AirMerchandisingFulfillmentReq merchandisingFulfilmentRequest(boolean isHoldBook, CreditCardInfo cardInfo) {
		AirMerchandisingFulfillmentReq req = new AirMerchandisingFulfillmentReq();
		buildBaseCoreRequest(req);
		req.setAirSolution(getAirSolution());
		req.setHostReservation(getHostReservation());
		req.setOptionalServices(getOptionalService(true));
		if (!isHoldBook) {
			req.setCreditCard(getCreditCard(cardInfo));
		}
		return req;
	}

	private AirSolution_type0 getAirSolution() {
		AirSolution_type0 airSolution = new AirSolution_type0();
		List<HostToken_type0> tokenList = new ArrayList<>();
		airSolution.setAirSegment(getAirSegments(tokenList));
		airSolution.setHostToken(tokenList.toArray(new HostToken_type0[0]));
		airSolution.setSearchTraveler(getSearchTravellers());
		return airSolution;
	}

	private SearchTraveler_type0[] getSearchTravellers() {
		List<SearchTraveler_type0> searchTravellersList = new ArrayList<SearchTraveler_type0>();
		for (FlightTravellerInfo traveller : travellerInfos) {
			searchTravellersList.add(getSearchPassenger(traveller.getMiscInfo().getPassengerKey(),
					traveller.getPaxType(), TravelPortSessionLessUtils.getPaxPrefix(traveller),
					traveller.getFirstName(), traveller.getLastName()));
		}
		return searchTravellersList.toArray(new SearchTraveler_type0[0]);
	}

	private HostReservation_type0[] getHostReservation() {
		List<HostReservation_type0> hostReservation = new ArrayList<HostReservation_type0>();
		HostReservation_type0 reservation = new HostReservation_type0();
		reservation.setUniversalLocatorCode(getTypeLocatorCode(universalPnr));
		reservation.setCarrierLocatorCode(getTypeLocatorCode(pnr));
		reservation.setProviderLocatorCode(getTypeProviderLocatorCode(pnr));
		reservation.setCarrier(getTypeCarrierCode(plattingCarrier));
		reservation.setProviderCode(getTypeProviderCode(providerCode));
		hostReservation.add(reservation);
		return hostReservation.toArray(new HostReservation_type0[0]);
	}
}
