package com.tgs.services.fms.validator;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.AirCancellationProtectionOutput;

@Service
public class AirCancellationProtectionOutputValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {
		AirCancellationProtectionOutput output = (AirCancellationProtectionOutput) iRuleOutput;
		if (output == null) {
			return;
		}
		validateField(errors, fieldName + ".CancellationProtectionPremium", output.getCancellationProtectionPremium());

		validateField(errors, fieldName + ".CancellationProtectionTax", output.getCancellationProtectionTax());

		validateField(errors, fieldName + ".CancellationProtectionManagementFee",
				output.getCancellationProtectionManagementFee());

		validateField(errors, fieldName + ".CancellationProtectionManagementFeeTax",
				output.getCancellationProtectionManagementFeeTax());

		validateField(errors, fieldName + ".AgentCommission", output.getCancellationProtectionAgentCommission());

	}

	private void validateField(Errors errors, String fieldName, String field) {
		try {
			field = field.replaceAll("\\s+", "");
			boolean isPercentageExpression = field.indexOf('%') >= 0;
			if (isPercentageExpression && !(field.indexOf('%') == field.length() - 1)) {
				rejectValue(errors, fieldName, SystemError.INVALID_COMPONENT_EXPRESSION, field);
			} else {
				field = field.replaceAll("%", "");
				float number = Float.parseFloat(field);
				boolean isInvalidPercentage = !(isPercentageExpression && (number >= 0.0 && number <= 100));
				boolean isInvalidFixedNumber = !(!isPercentageExpression && (number >= 0.0 && number <= 514332.50));

				if (isInvalidPercentage && isInvalidFixedNumber) {
					rejectValue(errors, fieldName, SystemError.INVALID_COMPONENT_EXPRESSION, field);
				}
			}
		} catch (Exception exp) {
			rejectValue(errors, fieldName, SystemError.INVALID_COMPONENT_EXPRESSION, field);
		}

	}
}
