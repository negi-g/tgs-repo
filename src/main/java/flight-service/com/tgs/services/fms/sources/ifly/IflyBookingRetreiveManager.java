package com.tgs.services.fms.sources.ifly;

import java.rmi.RemoteException;
import java.util.*;
import com.ibsplc.www.wsdl.ReservationsStub;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.reflect.TypeToken;
import com.ibsplc.www.ires.simpletypes.AddressType;
import com.ibsplc.www.ires.simpletypes.PnrContactType;
import com.ibsplc.www.ires.simpletypes.FareDetailsForGuestType;
import com.ibsplc.www.ires.simpletypes.FlightSegmentDetailsType;
import com.ibsplc.www.ires.simpletypes.GuestPriceBreakDownType;
import com.ibsplc.www.ires.simpletypes.GuestReponseDetailsType;
import com.ibsplc.www.ires.simpletypes.ItineraryDetailsType;
import com.ibsplc.www.ires.simpletypes.PriceBreakDownType;
import com.ibsplc.www.ires.simpletypes.RetrieveBookingRQ;
import com.ibsplc.www.ires.simpletypes.RetrieveBookingRS;
import com.ibsplc.www.ires.simpletypes.SurchargeType;
import com.ibsplc.www.ires.simpletypes.TaxType;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoPNRFoundException;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Setter
@Slf4j
@SuperBuilder
final class IflyBookingRetreiveManager extends IflyServiceManager {

	protected String pnr;

	public AirImportPnrBooking retreiveBooking() {
		AirImportPnrBooking pnrBooking = null;
		ReservationsStub reservationsStub = bindingService.getReservationsStub();
		try {
			listener.setType(AirUtils.getLogType("RetrieveBooking", getConfiguration()));
			reservationsStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			RetrieveBookingRS retreiveResponse = reservationsStub.retrieveBooking(createRetriveBookingRQ());
			if (!isValidBooking(retreiveResponse)) {
				List<TripInfo> tripInfos = fetchTripDetails(retreiveResponse);
				DeliveryInfo deliveryInfo = fetchDeliveryInfo(retreiveResponse);
				pnrBooking = AirImportPnrBooking.builder().tripInfos(tripInfos).deliveryInfo(deliveryInfo).build();
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			reservationsStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return pnrBooking;
	}

	private boolean isValidBooking(RetrieveBookingRS retreiveResponse) {
		if (retreiveResponse != null && StringUtils.isNotEmpty(retreiveResponse.getPNRNumber())
				&& (retreiveResponse.getItinerary() == null || ArrayUtils.isEmpty(retreiveResponse.getItinerary()))) {
			throw new NoPNRFoundException("No PNR/Journey Available " + retreiveResponse.getPNRNumber());
		} else if (retreiveResponse == null || StringUtils.isEmpty(retreiveResponse.getPNRNumber())) {
			throw new NoPNRFoundException("Invalid PNR " + pnr);
		}
		return false;
	}

	private DeliveryInfo fetchDeliveryInfo(RetrieveBookingRS retreiveResponse) {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		if (ArrayUtils.isNotEmpty(retreiveResponse.getPnrContact())) {
			List<String> emails = new ArrayList<>();
			PnrContactType contactType = retreiveResponse.getPnrContact()[0];
			AddressType addressType = contactType.getAddress();
			emails.add(addressType.getEmailAddress());
			deliveryInfo.setEmails(emails);
			List<String> contacts = new ArrayList<>();
			contacts.add(StringUtils.join(addressType.getPhoneNumberCountryCode(), addressType.getPhoneNumber()));
			deliveryInfo.setContacts(contacts);
		}
		return deliveryInfo;
	}

	private List<TripInfo> fetchTripDetails(RetrieveBookingRS retreiveResponse) {
		List<TripInfo> tripInfos = getTripDetils(retreiveResponse);
		List<FlightTravellerInfo> flightTravellerInfos = getTripTravaller(retreiveResponse);
		shiftFareDetailsToFlightTraveller(tripInfos, flightTravellerInfos);
		return tripInfos;
	}

	/**
	 * This method is duplicating for every suppliers
	 *
	 * @param tripInfos
	 * @param flightTravellerInfos
	 */

	private void shiftFareDetailsToFlightTraveller(List<TripInfo> tripInfos,
			List<FlightTravellerInfo> flightTravellerInfos) {
		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				SegmentBookingRelatedInfo bookingRelatedInfo = SegmentBookingRelatedInfo.builder().build();
				segmentInfo.setBookingRelatedInfo(bookingRelatedInfo);
				segmentInfo.getBookingRelatedInfo().setTravellerInfo(new ArrayList<>());
				segmentInfo.getPriceInfo(0).getFareDetails().forEach((paxType, fareDetail) -> {
					List<FlightTravellerInfo> travellerInfos1 =
							GsonUtils.getGson()
									.fromJson(
											GsonUtils.getGson()
													.toJson(AirUtils.getParticularPaxTravellerInfo(flightTravellerInfos,
															paxType)),
											new TypeToken<List<FlightTravellerInfo>>() {}.getType());
					travellerInfos1.forEach(travellerInfo -> {
						travellerInfo.setFareDetail(fareDetail);
						segmentInfo.getBookingRelatedInfo().getTravellerInfo().add(travellerInfo);
					});
				});
				// Finally Make Price Info Fare Detail to empty
				segmentInfo.getPriceInfo(0).setFareDetails(null);
			});
		});

	}

	private List<FlightTravellerInfo> getTripTravaller(RetrieveBookingRS retreiveResponse) {
		List<FlightTravellerInfo> travellerInfos = new ArrayList<>();
		for (GuestReponseDetailsType guestDetail : retreiveResponse.getGuestDetails()) {
			FlightTravellerInfo travellerInfo = new FlightTravellerInfo();

			travellerInfo.setPnr(retreiveResponse.getPNRNumber());
			if (Objects.nonNull(guestDetail.getDateOfBirth())) {
				travellerInfo.setDob(TgsDateUtils.calenderToLocalDate(guestDetail.getDateOfBirth()));
			}
			travellerInfo.setFirstName(guestDetail.getGivenName());
			travellerInfo.setLastName(guestDetail.getSurName());
			travellerInfo.setTitle(guestDetail.getNamePrefix().getValue());
			travellerInfo.setPaxType(PaxType.valueOf(guestDetail.getGuestType().getValue()));
			travellerInfos.add(travellerInfo);
		}
		return travellerInfos;
	}

	private List<TripInfo> getTripDetils(RetrieveBookingRS retreiveResponse) {
		List<TripInfo> tripInfos = new ArrayList<>();
		for (ItineraryDetailsType itineraryDetailsType : retreiveResponse.getItinerary()) {
			for (String segGroupId : getJourneyKeyList(retreiveResponse.getItinerary()[0])) {
				TripInfo tripInfo = new TripInfo();
				int segNum = 0;
				for (FlightSegmentDetailsType flightSegmentDetails : itineraryDetailsType.getFlightSegmentDetails()) {
					if (flightSegmentDetails.getFlightSegmentGroupID().equals(segGroupId)) {
						SegmentInfo segmentInfo = getSegmentInfo(flightSegmentDetails, segNum++);
						populatePriceInfo(segmentInfo, retreiveResponse, flightSegmentDetails.getSegmentId());
						tripInfo.getSegmentInfos().add(segmentInfo);
					}
				}
				tripInfo.setConnectionTime();
				tripInfos.add(tripInfo);
			}
		}
		return tripInfos;
	}

	private List<String> getJourneyKeyList(ItineraryDetailsType itineraryDetailsType) {
		List<String> journeyKeyList = new ArrayList<String>();
		for (FlightSegmentDetailsType flightSegmentDetails : itineraryDetailsType.getFlightSegmentDetails()) {
			if (!journeyKeyList.contains(flightSegmentDetails.getFlightSegmentGroupID())) {
				journeyKeyList.add(flightSegmentDetails.getFlightSegmentGroupID());
			}
		}
		return journeyKeyList;
	}

	private void populatePriceInfo(SegmentInfo segmentInfo, RetrieveBookingRS retreiveResponse, String segId) {
		String fareComponentId = StringUtils.EMPTY;
		List<PriceInfo> priceInfoList = segmentInfo.getPriceInfoList();
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(getConfiguration().getBasicInfo());
		Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();
		for (GuestPriceBreakDownType guestPriceBreakDownType : retreiveResponse.getItinPrice()[0]
				.getGuestPriceBreakDown()) {
			PriceBreakDownType priceBreakDown =
					getFareDetailsForGuestType(guestPriceBreakDownType.getPriceBreakDown(), segId);
			fareComponentId = priceBreakDown.getFareDetailsForGuestType().getFareComponentId();
			FareDetail fareDetail = createFareDetail(priceBreakDown.getFareDetailsForGuestType());
			fareDetails.put(PaxType.valueOf(guestPriceBreakDownType.getGuestType().getValue()), fareDetail);
			if (segmentInfo.getSegmentNum() == 0) {
				addFareComponenets(guestPriceBreakDownType, fareDetail, priceBreakDown, fareComponentId);
			}
		}
		priceInfoList.add(priceInfo);
	}

	/**
	 * There can be two breakdown ,one for onward trip and other for return
	 *
	 * @param priceBreakDownList
	 * @param segId
	 * @return priceBreakdown
	 */

	private PriceBreakDownType getFareDetailsForGuestType(PriceBreakDownType[] priceBreakDownList, String segId) {
		for (PriceBreakDownType priceBreakDown : priceBreakDownList) {
			List<Long> segmentIds = new ArrayList<>();
			for (long segmentId : priceBreakDown.getFareDetailsForGuestType().getSegmentId()) {
				segmentIds.add(Long.valueOf(segmentId));
			}
			if (segmentIds.contains(Long.valueOf(segId))) {
				return priceBreakDown;
			}
		}
		return null;
	}

	private void addFareComponenets(GuestPriceBreakDownType guestPriceBreakDownType, FareDetail fareDetail,
			PriceBreakDownType priceBreakDown, String fareComponentId) {
		Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
		fareComponents.put(FareComponent.BF, priceBreakDown.getAppliedFareDetailsType().getBaseFare());
		fareComponents.put(FareComponent.TF, priceBreakDown.getAppliedFareDetailsType().getDisplayFareAmount());
		if (priceBreakDown.getSurcharge() != null) {
			for (SurchargeType surchargeType : priceBreakDown.getSurcharge()) {
				fareComponents.put(FareComponent.AT,
						fareComponents.getOrDefault(FareComponent.AT, 0.0) + surchargeType.getAmount());
			}
		}
		if (guestPriceBreakDownType.getTax() != null) {
			for (TaxType taxType : guestPriceBreakDownType.getTax()) {
				if (taxType.getFareComponentId().equals(fareComponentId)) {
					// Here I am getting only gst componenets
					if (taxType.getCode().equals(FareComponent.CGST.name())
							|| taxType.getCode().equals(FareComponent.SGST.name())) {
						fareComponents.put(FareComponent.AGST,
								fareComponents.getOrDefault(FareComponent.AGST, 0.0) + taxType.getAmount());
					} else if (taxType.getCode().equals("IN")) {
						fareComponents.put(FareComponent.UDF,
								fareComponents.getOrDefault(FareComponent.UDF, 0.0) + taxType.getAmount());
					} else {
						fareComponents.put(FareComponent.OC,
								fareComponents.getOrDefault(FareComponent.OC, 0.0) + taxType.getAmount());
					}
				}
			}
		}
	}

	private FareDetail createFareDetail(FareDetailsForGuestType fareDetailsForGuestType) {
		FareDetail fareDetail = new FareDetail();
		fareDetail.setFareBasis(fareDetailsForGuestType.getFareBasisCode());
		fareDetail.setFareType(fareDetailsForGuestType.getFareType());

		fareDetail.setCabinClass(CabinClass.ECONOMY);
		fareDetail.setIsMealIncluded(false);
		// classofbooking
		// fareDetail.setFareComponents(new HashMap<>());
		fareDetail.setRefundableType(RefundableType.NON_REFUNDABLE.getRefundableType());

		return fareDetail;
	}

	private SegmentInfo getSegmentInfo(FlightSegmentDetailsType flightSegmentDetails, int segNum) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setDuration(IflyUtils.getJourneyMinutesFromHoursFormat(flightSegmentDetails.getJourneyTime()));
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirportInfo(flightSegmentDetails.getOffPoint()));
		segmentInfo.setArrivalTime(TgsDateUtils.toLocalDateTime(flightSegmentDetails.getScheduledArrivalTimeLTC()));
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirportInfo(flightSegmentDetails.getBoardPoint()));
		segmentInfo.setDepartTime(TgsDateUtils.toLocalDateTime(flightSegmentDetails.getScheduledDepartureTimeLTC()));
		segmentInfo.setSegmentNum(segNum);
		segmentInfo.setFlightDesignator(createFlightDesignator(flightSegmentDetails));
		segmentInfo.setIsReturnSegment(false);
		segmentInfo.setStops(flightSegmentDetails.getStops());
		segmentInfo.calculateDuration();
		segmentInfo.setStopOverAirports(null);
		return segmentInfo;
	}

	private FlightDesignator createFlightDesignator(FlightSegmentDetailsType flightSegmentDetails) {
		FlightDesignator flightDesignator = new FlightDesignator();
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(flightSegmentDetails.getCarrierCode()));
		flightDesignator.setEquipType(flightSegmentDetails.getAircraftType());
		flightDesignator.setFlightNumber(flightSegmentDetails.getFltNumber());
		return flightDesignator;
	}

	private RetrieveBookingRQ createRetriveBookingRQ() {
		RetrieveBookingRQ request = new RetrieveBookingRQ();
		request.setAirlineCode(getIFlyAirline().getAirlineCode());
		request.setAgencyCode(getAgencyCode());
		request.setBookingChannel(getBookingChannelType());
		request.setPnrNumber(pnr);
		return request;
	}


}
