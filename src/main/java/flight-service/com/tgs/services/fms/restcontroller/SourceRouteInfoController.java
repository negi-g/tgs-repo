package com.tgs.services.fms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.ums.datamodel.AreaRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.SourceRouteInfoFilter;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.jparepository.SourceRouteInfoService;
import com.tgs.services.fms.manager.SourceRouteUploadManager;
import com.tgs.services.fms.restmodel.SourceRouteBulkUploadRequest;
import com.tgs.services.fms.restmodel.SourceRouteInfoResponse;
import com.tgs.services.fms.servicehandler.SourceRouteInfoHandler;

@RestController
@RequestMapping("/fms/v1/source-route")
public class SourceRouteInfoController {

	@Autowired
	SourceRouteInfoHandler sourceRouteInfoHandler;

	@Autowired
	SourceRouteInfoService routeInfoService;

	@Autowired
	SourceRouteUploadManager sourceRoutemanager;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected SourceRouteInfoResponse saveSourceRouteInfo(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody SourceRouteInfo sourceRouteInfo) throws Exception {
		sourceRouteInfoHandler.initData(sourceRouteInfo, new SourceRouteInfoResponse());
		return sourceRouteInfoHandler.getResponse();
	}

	@RequestMapping(value = "/bulk-upload", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected @ResponseBody BulkUploadResponse saveSourceRouteInfoBulk(HttpServletRequest request,
			HttpServletResponse response, @RequestBody SourceRouteBulkUploadRequest sourceRouteBulkRequest)
			throws Exception {
		return sourceRoutemanager.bulkUpdateSourceRoute(sourceRouteBulkRequest);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected BaseResponse deleteSourceRoute(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return routeInfoService.deleteById(id);
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected SourceRouteInfoResponse getSourceRoute(HttpServletRequest request, HttpServletResponse response,
			@RequestBody SourceRouteInfoFilter sourceRouteInfoFilter) throws Exception {
		return sourceRouteInfoHandler.fetchSourceRoutes(sourceRouteInfoFilter);
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected BaseResponse updateConfigStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return sourceRouteInfoHandler.updateSourceRoute(id, status);
	}

}
