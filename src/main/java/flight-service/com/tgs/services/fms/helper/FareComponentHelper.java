package com.tgs.services.fms.helper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringJoiner;

import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareComponentCacheData;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FareComponentHelper {

	static GeneralCachingCommunicator communicator;

	@Autowired
	public FareComponentHelper(GeneralCachingCommunicator communicator) {
		FareComponentHelper.communicator = communicator;
	}

	public static void storeFareComponent(TripInfo tripInfo, List<FareComponent> fareComponentList,
			SupplierConfiguration configuration) {

		Map<String, FareComponentCacheData> fareComponentCacheData = new HashMap<>();
		for (SegmentInfo segment : tripInfo.getSegmentInfos()) {
			for (int priceIndex = 0; priceIndex < segment.getPriceInfoList().size(); priceIndex++) {
				String key = getTripKey(tripInfo, configuration, priceIndex);
				if (fareComponentCacheData.get(key) == null) {
					fareComponentCacheData.put(key, new FareComponentCacheData());
				}
				Map<FareComponent, Double> fareComponents = new HashMap<>();
				FareDetail adultFareDetail = segment.getFareDetailsForPriceInfo(priceIndex).get(PaxType.ADULT);
				for (FareComponent fc : fareComponentList) {
					fareComponents.put(fc, adultFareDetail.getFareComponents().getOrDefault(fc, 0.0));
				}
				fareComponentCacheData.get(key).getSegmentFares().add(fareComponents);
			}
		}

		for (Entry<String, FareComponentCacheData> entry : fareComponentCacheData.entrySet()) {
			Map<String, String> binMap = new HashMap<>();
			log.debug("Storing fare component for the trip {}", tripInfo.getTripKey());
			binMap.put(BinName.FARECOMPONENT.getName(), GsonUtils.getGson().toJson(entry.getValue()));
			binMap.put(BinName.SOURCEID.getName(), tripInfo.getSupplierInfo().getSourceId().toString());
			CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.FARE_INFO.getName())
					.namespace(CacheNameSpace.FLIGHT.getName()).key(entry.getKey())
					.keys(Arrays.asList(entry.getKey()).toArray(new String[0])).build();
			communicator.store(metaInfo, binMap, false, true, CacheType.FARECOMPONENT.getTtl());
		}

	}

	private static String getTripKey(TripInfo trip, SupplierConfiguration configuration, int priceIndex) {
		StringJoiner joiner = new StringJoiner("_");
		String airline = trip.getSegmentInfos().get(0).getAirlineCode(false);
		joiner.add(airline);
		String accountCode = "NA";
		if (StringUtils.isNotBlank(trip.getSegmentInfos().get(0).getPriceInfo(priceIndex).getAccountCode())) {
			accountCode = trip.getSegmentInfos().get(0).getPriceInfo(priceIndex).getAccountCode();
		}
		joiner.add(accountCode);
		String supplierId = configuration.getBasicInfo().getRuleId().toString();
		joiner.add(supplierId);
		for (SegmentInfo segment : trip.getSegmentInfos()) {
			PriceInfo priceInfo = segment.getPriceInfo(priceIndex);
			FareDetail adultFareDetail = priceInfo.getFareDetail(PaxType.ADULT);
			String classOfBooking = adultFareDetail.getClassOfBooking();
			Double adultBaseFare = adultFareDetail.getFareComponents().getOrDefault(FareComponent.BF, 0.0);
			joiner.add(segment.getDepartureAirportCode());
			joiner.add(segment.getArrivalAirportCode());
			joiner.add(classOfBooking);
			joiner.add(adultBaseFare.toString());
		}
		return joiner.toString();
	}

	public static void fetchFareComponents(List<TripInfo> tripInfos, List<FareComponent> fareComponentList,
			Map<FareComponent, List<FareComponent>> mappedComponents, SupplierConfiguration configuration) {
		Set<String> keys = new HashSet<String>();
		for (TripInfo tripInfo : tripInfos) {
			for (TripInfo trip : tripInfo.splitTripInfo(false)) {
				for (int priceIndex = 0; priceIndex < trip.getSegmentInfos().get(0).getPriceInfoList()
						.size(); priceIndex++) {
					keys.add(getTripKey(trip, configuration, priceIndex));
				}
			}
		}
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.FARE_INFO.getName())
				.namespace(CacheNameSpace.FLIGHT.getName()).keys(keys.toArray(new String[0])).build();
		Map<String, Map<String, String>> resultMap = communicator.get(metaInfo, String.class);
		if (MapUtils.isNotEmpty(resultMap)) {
			for (TripInfo tripInfo : tripInfos) {
				List<TripInfo> trips = tripInfo.splitTripInfo(false);
				for (TripInfo trip : trips) {
					for (int segmentIndex = 0; segmentIndex < trip.getSegmentInfos().size(); segmentIndex++) {
						SegmentInfo segmentInfo = trip.getSegmentInfos().get(segmentIndex);
						for (int priceIndex = 0; priceIndex < segmentInfo.getPriceInfoList().size(); priceIndex++) {
							String key = getTripKey(trip, configuration, priceIndex);
							if (resultMap.get(key) != null) {
								Type typeOfT = new TypeToken<FareComponentCacheData>() {}.getType();
								FareComponentCacheData fareComponentsData = GsonUtils.getGson()
										.fromJson(resultMap.get(key).get(BinName.FARECOMPONENT.getName()), typeOfT);
								if (fareComponentsData != null
										&& CollectionUtils.isNotEmpty(fareComponentsData.getSegmentFares())) {

									Map<FareComponent, Double> cachedFareComponents = FareComponentHelper
											.getSegmentFareComponents(fareComponentsData, segmentIndex);
									if (MapUtils.isNotEmpty(cachedFareComponents)) {
										for (Entry<PaxType, FareDetail> entrySet : segmentInfo
												.getFareDetailsForPriceInfo(priceIndex).entrySet()) {
											if (entrySet.getKey() != PaxType.INFANT) {
												FareComponentHelper.addFareComponent(cachedFareComponents,
														entrySet.getValue().getFareComponents(), fareComponentList,
														mappedComponents);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private static Map<FareComponent, Double> getSegmentFareComponents(FareComponentCacheData fareComponentData,
			int segmentIndex) {
		return fareComponentData.getSegmentFares().get(segmentIndex);
	}

	/**
	 * @param mappedComponents eg: { "YQ" : ["AT","OC"] }
	 *
	 * @implNote child component might get totalled with any parent component in the list. if child component fare is
	 *           not able to remove from one component then it will try with the successive components.
	 *
	 * @implSpec passing @param mappedComponents will remove the cached fares from the parent fare component. Passing
	 *           null @param mappedComponents will just add up the new component.
	 *
	 */
	public static void fetchTripFareComponents(List<TripInfo> tripInfos, List<FareComponent> components,
			Map<FareComponent, List<FareComponent>> mappedComponents, SupplierConfiguration configuration) {
		if (CollectionUtils.isNotEmpty(components)) {
			fetchFareComponents(tripInfos, components, mappedComponents, configuration);
		}
	}

	/**
	 * @implSpec <br>
	 *           while adding a new fare component, if that component is already added up in any parent component it
	 *           will be removed.
	 */
	private static void addFareComponent(Map<FareComponent, Double> cachedFareComponents,
			Map<FareComponent, Double> segmentFareComponents, List<FareComponent> fareComponentList,
			Map<FareComponent, List<FareComponent>> mappedComponents) {
		for (FareComponent fareComponent : fareComponentList) {
			if (cachedFareComponents.get(fareComponent) != null && segmentFareComponents.get(fareComponent) == null) {
				Double cachedFare = cachedFareComponents.get(fareComponent);
				// To remove new component fare from the parent component.
				if (MapUtils.isNotEmpty(mappedComponents)
						&& CollectionUtils.isNotEmpty(mappedComponents.get(fareComponent))) {
					List<FareComponent> parentComponents = mappedComponents.get(fareComponent);
					boolean isComponentReduced = false;
					for (FareComponent parentComponent : parentComponents) {
						Double parentComponentFare = segmentFareComponents.get(parentComponent);
						if (parentComponentFare != null && parentComponentFare >= cachedFare) {
							Double oldFare = segmentFareComponents.get(parentComponent);
							Double newFare = oldFare - cachedFare;
							segmentFareComponents.put(parentComponent, newFare);
							log.debug(
									"Removed fare component {} fare {} from parent component {} old fare -> {} updated fare -> {}",
									fareComponent, cachedFare, parentComponent, oldFare, newFare);
							isComponentReduced = true;
							break;
						}
					}
					if (isComponentReduced) {
						segmentFareComponents.put(fareComponent, cachedFare);
					}
				}
			}
		}
	}

	public static List<String> getTripKeys(List<TripInfo> tripInfos) {
		List<String> keys = new ArrayList<>();
		for (TripInfo tripInfo : tripInfos) {
			if (tripInfo.isPriceInfosNotEmpty()) {
				StringJoiner key = new StringJoiner("_");
				for (SegmentInfo segment : tripInfo.getSegmentInfos()) {
					key.add(segment.getDepartureAirportCode());
					key.add(segment.getArrivalAirportCode());
					key.add(segment.getAirlineCode(false));
				}
				keys.add(key.toString());
			}
		}
		return keys;
	}

	public static void cacheFareComponents(List<TripInfo> tripInfos, List<FareComponent> fareComponentList,
			SupplierConfiguration configuration) {
		if (CollectionUtils.isNotEmpty(fareComponentList)) {
			for (TripInfo tripInfo : tripInfos) {
				tripInfo.splitTripInfo(false).forEach(trip -> {
					FareComponentHelper.storeFareComponent(trip, fareComponentList, configuration);
				});
			}
		}
	}


	public static void storeInfantFare(AirSearchQuery searchQuery, int sourceId, FareDetail infantFare) {
		String key = getInfantFareKey(searchQuery, sourceId);
		Map<String, String> binMap = new HashMap<>();
		if (infantFare != null && MapUtils.isNotEmpty(infantFare.getFareComponents())) {
			CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.INFANT_FARE.getName())
					.namespace(CacheNameSpace.FLIGHT.getName()).key(key).build();
			binMap.put(BinName.INFANTFARE.getName(), GsonUtils.getGson().toJson(infantFare));
			communicator.store(metaInfo, binMap, false, true, -1);
		}
	}


	public static FareDetail getInfantfareFromCache(AirSearchQuery searchQuery, int sourceId) {
		String key = getInfantFareKey(searchQuery, sourceId);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.FLIGHT.getName())
				.set(CacheSetName.INFANT_FARE.getName()).key(key).build();
		return GsonUtils.getGson().fromJson(
				communicator.getBinValue(metaInfo, String.class, false, true, BinName.INFANTFARE.getName()),
				FareDetail.class);
	}

	private static String getInfantFareKey(AirSearchQuery searchQuery, int sourceId) {
		return StringUtils.join(sourceId + "_" + searchQuery.getAirType().getCode());
	}
}
