package com.tgs.services.fms.sources.ifly;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class IflyAirBookingFactory extends AbstractAirBookingFactory {

	protected IflyBindingService bindingService;

	protected SoapRequestResponseListner listener = null;

	public IflyAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	private void initialize() {
		bindingService = IflyBindingService.builder().configuration(supplierConf).user(bookingUser).build();
	}

	@Override
	public boolean doBooking() {
		initialize();
		try {
			String sessionToken = bookingSegments.getSupplierSession().getSupplierSessionInfo().getSessionToken();
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			IflyReviewManager reviewManager = IflyReviewManager.builder().bindingService(bindingService)
					.configuration(supplierConf).segmentInfos(bookingSegments.getSegmentInfos()).listener(listener)
					.bookingId(bookingId).criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).build();
			reviewManager.init();
			reviewManager.setClientSessionId(sessionToken);
			reviewManager.doPriceQuote();
			if (!isFareDiff(reviewManager.getTotalAmountToPaid())) {
				IflyBookingManager bookingManager = IflyBookingManager.builder().bindingService(bindingService)
						.bookingId(bookingId).configuration(supplierConf).order(order)
						.segmentInfos(bookingSegments.getSegmentInfos()).criticalMessageLogger(criticalMessageLogger)
						.deliveryInfo(deliveryInfo).listener(listener).bookingUser(bookingUser).build();
				bookingManager.init();
				bookingManager.setClientSessionId(sessionToken);
				pnr = bookingManager.createPNR();
			}
		} catch (NoSeatAvailableException e) {
			throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
		}
		return StringUtils.isNotBlank(pnr);
	}

	@Override
	public boolean doConfirmBooking() {
		// They do not support Hold and Confirm Functionality
		return false;
	}

}
