package com.tgs.services.fms.sources.airasiadotrez;

import java.io.IOException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import com.airasia.datamodel.Credentials;
import com.airasia.datamodel.TokenCreateRQ;
import com.airasia.datamodel.TokenCreateRS;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
@SuperBuilder
final class AirAsiaDotRezTokenManager extends AirAsiaDotRezServiceManager {

	private int sessionValidMinutes;

	public String createToken() {
		TokenCreateRS tokenCreateRS = null;
		HttpUtils httpUtils = null;
		try {
			TokenCreateRQ tokenRQ = TokenCreateRQ.builder().credentials(getCredentials()).build();
			httpUtils = HttpUtils.builder().urlString(bindingService.loginUrl())
					.postData(GsonUtils.getGson().toJson(tokenRQ)).headerParams(headerParams())
					.timeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS).proxy(AirSupplierUtils.getProxy(bookingUser))
					.build();
			tokenRQ.setApplicationName(StringUtils.EMPTY);
			tokenCreateRS = httpUtils.getResponse(TokenCreateRS.class).orElse(null);
			if (isAnyError(tokenCreateRS)) {
				throw new SupplierSessionException(StringUtils.join(",", criticalMessageLogger));
			}
			sessionValidMinutes = tokenCreateRS.getData().getIdleTimeoutInMinutes();
			return tokenCreateRS.getData().getToken();
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(getEndPoint(httpUtils.getUrlString()),
					formatRQRS(httpUtils.getPostData(), "TokenCreateRQ"),
					formatRQRS(httpUtils.getResponseString(), "TokenCreateRS"));
			listener.addLog(LogData.builder().key(getListnerKey()).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility())
					.type(AirUtils.getLogType("1-L-TokenCreate", supplierConfig))
					.responseTime(httpUtils.getResponseTime()).build());
		}
	}

	public Credentials getCredentials() {
		Credentials cred = Credentials.builder().username(getCredential().getUserName())
				.password(getCredential().getPassword()).domain(getCredential().getDomain()).location(null).build();
		return cred;
	}

}
