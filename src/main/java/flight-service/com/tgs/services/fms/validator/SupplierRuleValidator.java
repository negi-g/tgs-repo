package com.tgs.services.fms.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import javax.validation.constraints.Pattern;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.fms.helper.AirSourceType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.datamodel.KeyValue;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.validator.ListValidator;
import com.tgs.services.base.validator.rule.FlightBasicRuleCriteriaValidator;
import com.tgs.services.fms.datamodel.supplier.SupplierAdditionalInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;


@Service
public class SupplierRuleValidator implements org.springframework.validation.Validator {

	@Autowired
	FlightBasicRuleCriteriaValidator criteriaValidator;

	@Autowired
	ListValidator listValidator;

	private static List<Integer> NAVITAIRE_SUPPORTED_FARE_CLASS_SOURCES = Arrays.asList(
			AirSourceType.GOAIR.getSourceId(), AirSourceType.INDIGO.getSourceId(), AirSourceType.SPICEJET.getSourceId(),
			AirSourceType.SCOOT.getSourceId(), AirSourceType.AIRASIA.getSourceId(),
			AirSourceType.AIRASIADOTREZ.getSourceId(), AirSourceType.JAZEERA.getSourceId());

	private static List<String> NAVITAIRE_SUPPORTED_FARE_CLASS =
			Arrays.asList("CompressByProductClass", "LowestFareClass", "Default");

	private static List<Integer> TRAVELPORT_SUPPORTED_FARE_CLASS_SOURCES =
			Arrays.asList(AirSourceType.TRAVELPORT.getSourceId());

	private static List<String> TRAVELPORT_SUPPORTED_FARE_CLASS = getFareClass();

	private static final String FARE_TYPE_REGEX = "^[a-zA-Z_]+$";
	private static final java.util.regex.Pattern FARE_TYPE_PATTERN = java.util.regex.Pattern.compile(FARE_TYPE_REGEX);

	private static List<String> getFareClass() {
		List<String> fareClasses = new ArrayList<>();
		for (CabinClass cabinClass : CabinClass.values()) {
			fareClasses.add(cabinClass.getName());
		}
		return fareClasses;
	}


	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}

		if (target instanceof SupplierRule) {
			SupplierRule rule = (SupplierRule) target;
			if (SupplierConfigurationHelper.getSupplierInfo(rule.getSupplierId()) == null) {
				rejectValue(errors, "supplierId", SystemError.INVALID_FBRC_SUPPLIERID, rule.getSupplierId());
			}

			SupplierAdditionalInfo supplierAdditionalInfo = rule.getSupplierAdditionalInfo();

			if (StringUtils.isNotBlank(supplierAdditionalInfo.getSpecialFareType())) {
				if (!FARE_TYPE_PATTERN.matcher(supplierAdditionalInfo.getSpecialFareType()).matches()) {
					rejectValue(errors, "supplierAdditionalInfo.specialFareType", SystemError.INVALID_DATA_FORMAT,
							supplierAdditionalInfo.getSpecialFareType());
				}
			}

			listValidator.validateAirlines(supplierAdditionalInfo.getIncludedAirlines(),
					"supplierAdditionalInfo.includedAirlines", errors, SystemError.INVALID_FBRC_AIRLINE);

			listValidator.validateAirlines(supplierAdditionalInfo.getExcludedAirlines(),
					"supplierAdditionalInfo.excludedAirlines", errors, SystemError.INVALID_FBRC_AIRLINE);

			listValidator.validateAirlines(supplierAdditionalInfo.getAllowedAirlines(),
					"supplierAdditionalInfo.allowedAirlines", errors, SystemError.INVALID_FBRC_AIRLINE);

			listValidator.validateAirlines(supplierAdditionalInfo.getBspAllowedAirlines(),
					"supplierAdditionalInfo.bspAllowedAirlines", errors, SystemError.INVALID_FBRC_AIRLINE);

			if (StringUtils.isNotBlank(supplierAdditionalInfo.getFareClass())) {

				if (NAVITAIRE_SUPPORTED_FARE_CLASS_SOURCES.contains(rule.getSourceId())
						&& !NAVITAIRE_SUPPORTED_FARE_CLASS.contains(supplierAdditionalInfo.getFareClass())) {
					rejectValue(errors, "supplierAdditionalInfo.fareClass", SystemError.INVALID_FARE_CLASS,
							supplierAdditionalInfo.getFareClass());
				}

				if (TRAVELPORT_SUPPORTED_FARE_CLASS_SOURCES.contains(rule.getSourceId())
						&& !TRAVELPORT_SUPPORTED_FARE_CLASS.contains(supplierAdditionalInfo.getFareClass())) {
					rejectValue(errors, "supplierAdditionalInfo.fareClass", SystemError.INVALID_FARE_CLASS,
							supplierAdditionalInfo.getFareClass());
				}
			}

			Predicate<KeyValue> isKeySupplierIdValid = val -> StringUtils.isNotBlank(val.getKey())
					&& SupplierConfigurationHelper.getSupplierInfo(val.getValue()) != null;

			listValidator.validateList(supplierAdditionalInfo.getTicketingSupplierIds(),
					"supplierAdditionalInfo.ticketingSupplierIds", errors, SystemError.INVALID_TICKETING_SUPPLIER_ID,
					isKeySupplierIdValid);

			listValidator.validateList(supplierAdditionalInfo.getBookingSupplierIds(),
					"supplierAdditionalInfo.bookingSupplierIds", errors, SystemError.INVALID_BOOKING_SUPPLIER_ID,
					isKeySupplierIdValid);

			criteriaValidator.validateCriteria(errors, "inclusionCriteria", rule.getInclusionCriteria());
			criteriaValidator.validateCriteria(errors, "exclusionCriteria", rule.getExclusionCriteria());

		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}

}
