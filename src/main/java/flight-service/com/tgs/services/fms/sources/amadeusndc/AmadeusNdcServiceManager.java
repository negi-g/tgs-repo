package com.tgs.services.fms.sources.amadeusndc;

import com.tgs.services.fms.helper.AirSourceType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.List;
import java.util.StringJoiner;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.EndpointReference;
import javax.xml.ws.handler.Handler;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.iata.iata._2007._00.iata2010.CompanyNameType;
import org.iata.iata._2007._00.iata2010.UniqueIDType;
import org.iata.iata._2015._00._2018_1.airshoppingrq.AirShoppingRQ;
import org.iata.iata._2015._00._2018_1.orderviewrs.ErrorType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderViewRS;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderViewRS.Errors;
import com.amadeus.wsdl.altea_ndc_18_1_v1_v4.AlteaNDC181Service;
import com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser;
import com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser.UserID;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.JaxSoapRequestResponseListener;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.ums.datamodel.User;

@Slf4j
@Getter
@Setter
@SuperBuilder
abstract class AmadeusNdcServiceManager {

	protected Order order;

	protected SupplierConfiguration supplierConf;

	protected List<SegmentInfo> bookingSegmentInfos;

	protected AlteaNDC181Service service;

	protected String bookingId;

	protected List<String> criticalMessageLogger;

	protected MoneyExchangeCommunicator moneyExchangeComm;

	protected String toCurrency;

	protected User bookingUser;

	private BigDecimal airlineTotalPrice;

	private String airlinePNR;

	private String supplierPNR;

	public AMASecurityHostedUser getSecurityHostedUser() {

		AMASecurityHostedUser securityHostedUser = new AMASecurityHostedUser();
		UserID userIdInfo = new UserID();
		userIdInfo.setPOSType("1");
		userIdInfo.setRequestorType("U");
		userIdInfo.setPseudoCityCode(supplierConf.getSupplierCredential().getOrganisationCode());
		userIdInfo.setAgentDutyCode("SU");
		UniqueIDType requestorIdInfo = new UniqueIDType();
		CompanyNameType companyName = new CompanyNameType();
		companyName.setValue("SQ");
		requestorIdInfo.setCompanyName(companyName);
		userIdInfo.setRequestorID(requestorIdInfo);
		securityHostedUser.setUserID(userIdInfo);

		return securityHostedUser;
	}

	protected void addJAXHandlers(BindingProvider bp, String key, String action) {
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				supplierConf.getSupplierCredential().getUrl());
		List<Handler> handlerChain = bp.getBinding().getHandlerChain();
		AmadeusNdcHeaderHandler headerHandler = new AmadeusNdcHeaderHandler(supplierConf, action);
		handlerChain.add(headerHandler);
		JaxSoapRequestResponseListener loggingHandler = new JaxSoapRequestResponseListener(key,
				AirUtils.getLogType(action, supplierConf), supplierConf.getSupplierId());
		handlerChain.add(loggingHandler);
		bp.getBinding().setHandlerChain(handlerChain);
	}

	public String getCurrencyCode() {
		return supplierConf.getSupplierCredential().getCurrencyCode();
	}

	protected void logCriticalMessage(String message) {
		if (criticalMessageLogger != null && StringUtils.isNotBlank(message)) {
			criticalMessageLogger.add(message);
		}
	}

	protected double getAmountBasedOnCurrency(BigDecimal amount, String fromCurrency) {
		if (StringUtils.isBlank(toCurrency)) {
			toCurrency = AirSourceConstants.getClientCurrencyCode();
		}
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount.doubleValue();
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(getMoneyExchangeType(supplierConf)).build();
		return moneyExchangeComm.getExchangeValue(amount.doubleValue(), filter, true);
	}

	protected double getAmountBasedOnPreferredCurrency(BigDecimal amount, String fromCurrency,
			String preferredCurrency) {
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount.doubleValue();
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(preferredCurrency).type(getMoneyExchangeType(supplierConf)).build();
		return moneyExchangeComm.getExchangeValue(amount.doubleValue(), filter, true);
	}

	protected double getAmountBasedOnPreferredCurrencyBooking(BigDecimal amount, String fromCurrency,
			String preferredCurrency) {
		if (StringUtils.equalsIgnoreCase(fromCurrency, preferredCurrency)) {
			return amount.doubleValue();
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(preferredCurrency).type(getMoneyExchangeType(supplierConf)).build();
		return moneyExchangeComm.getExchangeValue(amount.doubleValue(), filter, true);
	}


	public String getMoneyExchangeType(SupplierConfiguration supplierConfiguration) {
		AirSourceType sourceType = AirSourceType.getAirSourceType(supplierConfiguration.getSourceId());
		return StringUtils
				.join(sourceType.name(), "_", supplierConfiguration.getSupplierCredential().getAgentUserName())
				.toUpperCase();
	}

	protected boolean isAnyError(OrderViewRS orderviewResponse) {
		boolean isAnyError = false;
		StringJoiner errorMessage = new StringJoiner("");
		if (orderviewResponse != null && CollectionUtils.isNotEmpty(orderviewResponse.getErrors())) {
			for (Errors error : orderviewResponse.getErrors()) {
				for (ErrorType errorType : error.getError()) {
					errorMessage.add(StringUtils.join(errorType.getCode(), "  ", errorType.getDescText()));
					isAnyError = true;
				}
			}
			log.info("Error occured on booking {} error message {}", bookingId, errorMessage.toString());
		}
		logCriticalMessage(errorMessage.toString());
		return isAnyError;
	}

	protected String getBaggageString(String value, String unit) {
		return StringUtils.join(value, " ", unit);
	}


}

