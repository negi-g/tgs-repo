package com.tgs.services.fms.sources.travelport.session;

import static com.tgs.services.fms.sources.travelport.session.TravelPortConstants.FARERULE_CATEGORY;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tgs.services.fms.utils.AirUtils;
import com.travelport.www.schema.air_v47_0.AirFareRuleCategory_type0;
import com.travelport.www.schema.air_v47_0.AirFareRulesModifier_type0;
import com.travelport.www.schema.air_v47_0.AirFareRulesReq;
import com.travelport.www.schema.air_v47_0.AirFareRulesRsp;
import com.travelport.www.schema.air_v47_0.FareRuleKey_type0;
import com.travelport.www.schema.air_v47_0.FareRule_type0;
import com.travelport.www.schema.air_v47_0.TypeFareRuleCategoryCode;
import com.travelport.www.schema.air_v50_0.AirFareDisplayModifiers_type0;
import com.travelport.www.schema.air_v50_0.AirFareDisplayReq;
import com.travelport.www.schema.air_v50_0.AirFareDisplayRsp;
import com.travelport.www.schema.air_v50_0.AirFareDisplayRuleKey_type0;
import com.travelport.www.schema.air_v50_0.FareDisplay_type0;
import com.travelport.www.schema.air_v50_0.FareRuleCategoryTypes;
import com.travelport.www.schema.air_v50_0.FareRuleLong_type0;
import com.travelport.www.schema.air_v50_0.FareRulesFilterCategory_type0;
import com.travelport.www.schema.air_v50_0.TypeFaresIndicator;
import com.travelport.www.schema.air_v50_0.ValueDetails;
import com.travelport.www.schema.air_v50_0.VariableCategoryType;
import com.travelport.www.schema.common_v47_0.BaseReqChoice_type0;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyContent;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyType;
import com.tgs.services.fms.datamodel.farerule.FareRuleTimeWindow;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.utils.exception.air.FareRuleUnHandledFaultException;
import com.travelport.www.service.air_v47_0.AirFaultMessage;
import com.travelport.www.service.air_v47_0.AirServiceStub;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class TravelPortFareRuleManager extends TravelPortServiceManager {

	public TripFareRule getFareRule(TripInfo selectedTrip, String bookingId) {
		AirServiceStub airService = bindingService.getAirService();
		listener.setType(AirUtils.getLogType("AirFareRules", configuration));
		AirFareRulesReq airFareRulesReq = null;
		AirFareRulesRsp airFareRulesRsp = null;
		try {
			airService._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(airService, "TripFareRule");
			airFareRulesReq = buildAirFareRulesReq(selectedTrip);
			airFareRulesRsp = airService.service(airFareRulesReq, getSessionContext(false));
			if (!checkAnyErrors(airFareRulesRsp)) {
				return parseFareRuleRsp(airFareRulesRsp, selectedTrip);
			}
		} catch (AirFaultMessage | RemoteException e) {
			throw new FareRuleUnHandledFaultException(e.getMessage());
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;
	}

	private TripFareRule parseFareRuleRsp(AirFareRulesRsp airFareRulesRsp, TripInfo selectedTrip) {
		TripFareRule tripfareRule = TripFareRule.builder().build();
		Map<String, FareRule_type0> fareRuleMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(getList(airFareRulesRsp.getFareRule()))) {
			getList(airFareRulesRsp.getFareRule()).forEach(fareRule -> {
				fareRuleMap.put(fareRule.getFareInfoRef(), fareRule);
			});
			FareRule_type0 fareRule = null;
			for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
				if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
					fareRule = fareRuleMap.get(segmentInfo.getPriceInfo(0).getMiscInfo().getFareRuleInfoRef());
				}
				Map<String, String> rules = new HashMap<String, String>();
				getList(fareRule.getFareRuleLong()).forEach(rule -> {
					if (rules.containsKey(rule.getCategory().toString())) {
						String value = rules.get(rule.getCategory().toString());
						value = value + " " + rule.getString();
						rules.put(FARERULE_CATEGORY.get(rule.getCategory().toString()), value);
					} else {
						rules.put(FARERULE_CATEGORY.get(rule.getCategory().toString()), rule.getString());
					}

				});
				FareRuleInformation information = new FareRuleInformation();
				information.setMiscInfo(rules);
				tripfareRule.getFareRule().put(segmentInfo.getSegmentKey(), information);
			}
		}
		return tripfareRule;
	}

	private AirFareRulesReq buildAirFareRulesReq(TripInfo selectedTrip) {
		AirFareRulesReq airFareRulesReq = new AirFareRulesReq();
		buildBaseCoreRequest(airFareRulesReq);
		BaseReqChoice_type0 baseReq = new BaseReqChoice_type0();
		List<AirFareRuleCategory_type0> airFareRuleCategoryList = new ArrayList<AirFareRuleCategory_type0>();
		AirFareRulesModifier_type0 airFareRulesModifier = new AirFareRulesModifier_type0();
		List<FareRuleKey_type0> fareRuleKey_type0s = new ArrayList<>();
		for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
			if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {

				FareRuleKey_type0 ruleKeyType0 = new FareRuleKey_type0();
				ruleKeyType0.setFareInfoRef(segmentInfo.getPriceInfo(0).getMiscInfo().getFareRuleInfoRef());
				if (StringUtils.isNotBlank(providerCode)) {
					ruleKeyType0.setProviderCode(getTypeProviderCode(providerCode));
				}
				ruleKeyType0.setTypeNonBlanks(segmentInfo.getPriceInfo(0).getMiscInfo().getFareRuleInfoValue());
				fareRuleKey_type0s.add(ruleKeyType0);

				AirFareRuleCategory_type0 airFareRuleCategory = new AirFareRuleCategory_type0();
				airFareRuleCategory
						.setFareInfoRef(getTypeRef(segmentInfo.getPriceInfo(0).getMiscInfo().getFareRuleInfoRef()));
				airFareRuleCategory.addCategoryCode(TypeFareRuleCategoryCode.CHG);
				airFareRuleCategoryList.add(airFareRuleCategory);
				airFareRulesModifier.addAirFareRuleCategory(airFareRuleCategory);

			}
		}

		baseReq.setFareRuleKey(fareRuleKey_type0s.toArray(new FareRuleKey_type0[0]));
		airFareRulesReq.setBaseReqChoice_type0(baseReq);
		airFareRulesReq.setAirFareRulesModifier(airFareRulesModifier);
		return airFareRulesReq;
	}

	public TripFareRule getStructuredFareRule(TripInfo selectedTrip, boolean isFareDisplayFlow) {
		TripFareRule tripFareRule = TripFareRule.builder().build();
		if (isFareDisplayFlow) {
			for (int index = 0; index < selectedTrip.getSegmentInfos().size(); index++) {
				List<SegmentInfo> subSegments =
						TravelPortUtils.getSubLegSegments(selectedTrip.getSegmentInfos(), index);
				airFareRule(subSegments, isFareDisplayFlow, tripFareRule);
			}
		} else {
			airFareRule(selectedTrip.getSegmentInfos(), isFareDisplayFlow, tripFareRule);
		}

		return tripFareRule;
	}

	private void airFareRule(List<SegmentInfo> segments, boolean isFareDisplayFlow, TripFareRule tripFareRule) {
		com.travelport.www.service.air_v50_0.AirServiceStub stub = bindingService.getAirService_V50();
		listener.setType(AirUtils.getLogType("AirFareRules_V50", configuration));
		stub._getServiceClient().getAxisService().addMessageContextListener(listener);
		bindingService.setProxyAndAuthentication(stub, "TripFareRule");
		try {
			com.travelport.www.schema.air_v50_0.AirFareRulesReq airFareRulesReq =
					buildAirFareRuleV50Request(segments, isFareDisplayFlow);
			com.travelport.www.schema.air_v50_0.AirFareRulesRsp airFareRulesRsp =
					stub.service(airFareRulesReq, getSessionContext(false));
			if (!checkAnyErrors(airFareRulesRsp)) {
				tripFareRule = parseAirFareRuleV50Rsp(airFareRulesRsp, segments, tripFareRule, isFareDisplayFlow);
			}
		} catch (RemoteException | com.travelport.www.service.air_v50_0.AirFaultMessage e) {
			throw new FareRuleUnHandledFaultException(e.getMessage());
		} finally {
			stub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private com.travelport.www.schema.air_v50_0.AirFareRulesReq buildAirFareRuleV50Request(List<SegmentInfo> segments,
			boolean isFareDisplayFlow) {
		com.travelport.www.schema.air_v50_0.AirFareRulesReq fareruleReq =
				new com.travelport.www.schema.air_v50_0.AirFareRulesReq();
		buildBaseCoreRequest_V50(fareruleReq);
		com.travelport.www.schema.common_v50_0.BaseReqChoice_type0 baseRequestChoice =
				new com.travelport.www.schema.common_v50_0.BaseReqChoice_type0();
		List<FareRulesFilterCategory_type0> fareRuleFilterCategories = new ArrayList<>();

		if (isFareDisplayFlow) {
			FareRulesFilterCategory_type0 fareRuleFilterCategory = new FareRulesFilterCategory_type0();
			fareRuleFilterCategory.setCategoryCode(getCategoryCode());
			fareRuleFilterCategories.add(fareRuleFilterCategory);
			AirFareDisplayRuleKey_type0 fareDisplayKey = new AirFareDisplayRuleKey_type0();
			fareDisplayKey.setTypeNonBlanks(segments.get(0).getPriceInfo(0).getMiscInfo().getFareRuleInfoValue());
			fareDisplayKey
					.setProviderCode(getTypeProviderCode_V50(configuration.getSupplierCredential().getProviderCode()));
			baseRequestChoice.setAirFareDisplayRuleKey(fareDisplayKey);
		} else {
			List<com.travelport.www.schema.air_v50_0.FareRuleKey_type0> fareRuleKeys = new ArrayList<>();

			for (SegmentInfo segment : segments) {
				if (segment.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {

					String fareRuleInfoRef = segment.getPriceInfo(0).getMiscInfo().getFareRuleInfoRef();
					FareRulesFilterCategory_type0 fareRuleFilterCategory = new FareRulesFilterCategory_type0();
					fareRuleFilterCategory.setFareInfoRef(fareRuleInfoRef);
					fareRuleFilterCategory.setCategoryCode(getCategoryCode());
					fareRuleFilterCategories.add(fareRuleFilterCategory);

					com.travelport.www.schema.air_v50_0.FareRuleKey_type0 fareRuleKey =
							new com.travelport.www.schema.air_v50_0.FareRuleKey_type0();
					fareRuleKey.setFareInfoRef(fareRuleInfoRef);
					fareRuleKey.setProviderCode(
							getTypeProviderCode_V50(configuration.getSupplierCredential().getProviderCode()));
					fareRuleKey.setTypeNonBlanks(segment.getPriceInfo(0).getMiscInfo().getFareRuleInfoValue());
					fareRuleKeys.add(fareRuleKey);
				}
			}
			fareruleReq.setRetrieveProviderReservationDetails(true);
			baseRequestChoice
					.setFareRuleKey(fareRuleKeys.toArray(new com.travelport.www.schema.air_v50_0.FareRuleKey_type0[0]));
		}
		fareruleReq.setFareRulesFilterCategory(fareRuleFilterCategories.toArray(new FareRulesFilterCategory_type0[0]));
		fareruleReq.setBaseReqChoice_type0(baseRequestChoice);
		return fareruleReq;
	}

	private OMElement getCategoryCode() {
		OMFactory omFactory = OMAbstractFactory.getOMFactory();
		OMElement categoryCode =
				omFactory.createOMElement("CategoryCode", "http://www.travelport.com/schema/air_v50_0", "");
		omFactory.createOMText(categoryCode, com.travelport.www.schema.air_v50_0.TypeFareRuleCategoryCode._CHG);
		return categoryCode;
	}

	private TripFareRule parseAirFareRuleV50Rsp(com.travelport.www.schema.air_v50_0.AirFareRulesRsp airFareRulesRsp,
			List<SegmentInfo> segments, TripFareRule tripFareRule, boolean isFareDisplayFlow) {
		if (isFareDisplayFlow) {
			FareRuleLong_type0 fareRuleLong = getCategory16FromLongRule(airFareRulesRsp);
			if (fareRuleLong != null) {
				Map<String, FareRuleInformation> fareRule = tripFareRule.getFareRule();
				FareRuleInformation fareRuleInfo = new FareRuleInformation();
				Map<String, String> miscInfo = new HashMap<>();
				miscInfo.put(TravelPortConstants.CAT_16, fareRuleLong.getString());
				fareRuleInfo.setMiscInfo(miscInfo);
				fareRule.put(segments.get(0).getDepartureAirportCode() + "_"
						+ segments.get(segments.size() - 1).getArrivalAirportCode(), fareRuleInfo);
				tripFareRule.setFareRule(fareRule);
			}
		} else {
			Map<String, com.travelport.www.schema.air_v50_0.FareRule_type0> fareRuleMap = new HashMap<>();
			if (ArrayUtils.isNotEmpty(airFareRulesRsp.getFareRule())) {
				for (com.travelport.www.schema.air_v50_0.FareRule_type0 fr : getList(airFareRulesRsp.getFareRule())) {
					fareRuleMap.put(fr.getFareInfoRef(), fr);
				}
				com.travelport.www.schema.air_v50_0.FareRule_type0 fareRule = null;
				Map<String, FareRuleInformation> fareBasisFareRuleMap = new HashMap<String, FareRuleInformation>();
				for (int segmentIndex = 0; segmentIndex < segments.size(); segmentIndex++) {
					SegmentInfo segmentInfo = segments.get(segmentIndex);
					if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
						fareRule = fareRuleMap.get(segmentInfo.getPriceInfo(0).getMiscInfo().getFareRuleInfoRef());

						if (fareRule != null && fareRule.getStructuredFareRules() != null
								&& ArrayUtils.isNotEmpty(fareRule.getStructuredFareRules().getFareRuleCategoryType())) {

							FareRuleCategoryTypes frCategoryType =
									fareRule.getStructuredFareRules().getFareRuleCategoryType()[0];

							VariableCategoryType category = getVariableCategoryTypeByName(
									frCategoryType.getVariableCategoryDetails(), TravelPortConstants.CHANGEITIN_CAT);
							setFareRuleAmount(TravelPortUtils.getSubLegSegments(segments, segmentIndex),
									fareBasisFareRuleMap, category, TravelPortConstants.CHANGEITIN_CAT);

							category = getVariableCategoryTypeByName(frCategoryType.getVariableCategoryDetails(),
									TravelPortConstants.CANCELLATION_CAT);
							setFareRuleAmount(TravelPortUtils.getSubLegSegments(segments, segmentIndex),
									fareBasisFareRuleMap, category, TravelPortConstants.CANCELLATION_CAT);

							category = getVariableCategoryTypeByName(frCategoryType.getVariableCategoryDetails(),
									TravelPortConstants.NOSHOW_CAT);
							setFareRuleAmount(TravelPortUtils.getSubLegSegments(segments, segmentIndex),
									fareBasisFareRuleMap, category, TravelPortConstants.NOSHOW_CAT);
						}
					}
				}

				if (MapUtils.isNotEmpty(fareBasisFareRuleMap)) {
					FareRuleInformation fareRuleInfo = new FareRuleInformation();
					fareBasisFareRuleMap.forEach((k, v) -> {
						v.getFareRuleInfo().forEach((policy, info) -> {
							info.forEach((timeWindow, policyContent) -> {
								Map<FareRuleTimeWindow, FareRulePolicyContent> newFareRuleInfo =
										fareRuleInfo.getFareRuleInfo().getOrDefault(policy,
												new HashMap<FareRuleTimeWindow, FareRulePolicyContent>());
								FareRulePolicyContent newPolicyContent =
										newFareRuleInfo.getOrDefault(timeWindow, new FareRulePolicyContent());
								newPolicyContent.setAmount(ObjectUtils.defaultIfNull(newPolicyContent.getAmount(), 0d)
										+ policyContent.getAmount());
								newPolicyContent.setPolicyInfo(policyContent.getPolicyInfo());
								newFareRuleInfo.put(timeWindow, newPolicyContent);
								fareRuleInfo.getFareRuleInfo().put(policy, newFareRuleInfo);
							});
						});
					});
					String segmentKey = StringUtils.join(segments.get(0).getDepartureAirportCode(), "-",
							segments.get(segments.size() - 1).getArrivalAirportCode());
					tripFareRule.getFareRule().put(segmentKey, fareRuleInfo);
				}
			}
		}
		return tripFareRule;
	}

	private FareRuleLong_type0 getCategory16FromLongRule(
			com.travelport.www.schema.air_v50_0.AirFareRulesRsp airFareRulesRsp) {
		if (ArrayUtils.isNotEmpty(airFareRulesRsp.getFareRule())
				&& ArrayUtils.isNotEmpty(airFareRulesRsp.getFareRule()[0].getFareRuleLong())) {
			for (FareRuleLong_type0 fareRuleLong : airFareRulesRsp.getFareRule()[0].getFareRuleLong()) {
				if (fareRuleLong.getCategory() != null && StringUtils.isNotBlank(fareRuleLong.getString())
						&& fareRuleLong.getCategory().intValue() == 16) {
					return fareRuleLong;
				}
			}
		}
		return null;
	}

	private VariableCategoryType getVariableCategoryTypeByName(VariableCategoryType[] varialbleCategoryDetails,
			String name) {

		VariableCategoryType category = null;

		if (ArrayUtils.isNotEmpty(varialbleCategoryDetails)) {
			for (VariableCategoryType variableCategory : varialbleCategoryDetails) {
				if (TravelPortConstants.CHG.equals(variableCategory.getValue())
						&& ArrayUtils.isNotEmpty(variableCategory.getCategoryDetails())
						&& validateCategoryDetails(name, getList(variableCategory.getCategoryDetails()))) {
					category = variableCategory;
					break;
				}
			}
		}
		return category;
	}

	private boolean validateCategoryDetails(String name, List<ValueDetails> categoryDetails) {
		boolean isValid = false;

		ValueDetails voluntaryDetail = getCategoryDetailByName(TravelPortConstants.VOLUNTARY, categoryDetails);
		if (voluntaryDetail == null || !TravelPortConstants.APPLIES.equals(voluntaryDetail.getValue())) {
			return isValid;
		}

		if (TravelPortConstants.CHANGEITIN_CAT.equals(name)) {
			ValueDetails categoryDetail = getCategoryDetailByName(TravelPortConstants.CHANGEITIN_CAT, categoryDetails);
			if (categoryDetail != null) {
				isValid = true;
				if (!TravelPortConstants.APPLIES.equals(categoryDetail.getValue())) {
					isValid = false;
				}
			}
		}
		if (TravelPortConstants.CANCELLATION_CAT.equals(name)) {
			ValueDetails categoryDetail =
					getCategoryDetailByName(TravelPortConstants.CANCELLATION_CAT, categoryDetails);
			if (categoryDetail != null) {
				isValid = true;
				if (!TravelPortConstants.APPLIES.equals(categoryDetail.getValue())) {
					isValid = false;
				}
				categoryDetail = getCategoryDetailByName(TravelPortConstants.CANCELLATIONREEFUNDS_CAT, categoryDetails);
				if (categoryDetail != null && !TravelPortConstants.APPLIES.equals(categoryDetail.getValue())) {
					isValid = false;
				}
			}
		}
		if (TravelPortConstants.NOSHOW_CAT.equals(name)) {
			ValueDetails categoryDetail = getCategoryDetailByName(TravelPortConstants.NOSHOW_CAT, categoryDetails);
			if (categoryDetail != null) {
				isValid = true;
				if (!TravelPortConstants.APPLIES.equals(categoryDetail.getValue())) {
					isValid = false;
				}
			}
		}
		return isValid;
	}

	private ValueDetails getCategoryDetailByName(String category, List<ValueDetails> categoryDetails) {
		if (CollectionUtils.isNotEmpty(categoryDetails)) {
			for (ValueDetails categoryDetail : categoryDetails) {
				if (category.equals(categoryDetail.getName())) {
					return categoryDetail;
				}
			}
		}
		return null;
	}

	private void setFareRuleAmount(List<SegmentInfo> segments, Map<String, FareRuleInformation> fareBasisFareRuleMap,
			VariableCategoryType categoryType, String categoryName) {
		if (categoryType != null) {
			FareRuleInformation fareRule = new FareRuleInformation();
			String fareBasis = segments.get(0).getPriceInfo(0).getFareBasis(PaxType.ADULT);
			if (fareBasisFareRuleMap.get(fareBasis) != null) {
				return;
			}

			FareRulePolicyType policyType = null;
			String policyInfo = null;
			Map<FareRuleTimeWindow, FareRulePolicyContent> policy = new HashMap<>();
			FareRulePolicyContent policyContent = new FareRulePolicyContent();
			ValueDetails amount1Cat = getCategoryDetailByName(TravelPortConstants.AMOUNT1_CAT,
					getList(categoryType.getCategoryDetails()));
			ValueDetails currency1Cat = getCategoryDetailByName(TravelPortConstants.CURRENCY1_CAT,
					getList(categoryType.getCategoryDetails()));
			if (categoryName.equals(TravelPortConstants.CANCELLATION_CAT)) {
				policyType = FareRulePolicyType.CANCELLATION;
				policyInfo = "Cancellation policy Info";
			}
			if (categoryName.equals(TravelPortConstants.NOSHOW_CAT)) {
				policyType = FareRulePolicyType.NO_SHOW;
				policyInfo = "No show policy Info";
			}
			if (categoryName.equals(TravelPortConstants.CHANGEITIN_CAT)) {
				policyType = FareRulePolicyType.DATECHANGE;
				policyInfo = "Date change policy Info";
			}

			ValueDetails penaltyCharge = getCategoryDetailByName(TravelPortConstants.PENALTY_CHARGES_CAT,
					getList(categoryType.getCategoryDetails()));

			FareRuleTimeWindow frWindow = getFareRuleTimeWindowPenaltyCharge(penaltyCharge);
			if (amount1Cat != null && currency1Cat != null) {
				Double amount = getAmountBasedOnCurrency(amount1Cat.getValue(), currency1Cat.getValue(), 1.0);
				policyContent.setAmount(amount);
				policyContent.setPolicyInfo(policyInfo);
				policy.put(frWindow, policyContent);
			}
			if (MapUtils.isNotEmpty(policy)) {
				fareRule.getFareRuleInfo().put(policyType, policy);
				fareBasisFareRuleMap.put(fareBasis, fareRule);
			}
		}
	}

	private FareRuleTimeWindow getFareRuleTimeWindowPenaltyCharge(ValueDetails penaltyCharge) {
		if (penaltyCharge == null || TravelPortConstants.PENALTY_CHARGES_DEFAULT.contains(penaltyCharge.getValue())) {
			return FareRuleTimeWindow.DEFAULT;
		}
		if (TravelPortConstants.PENALTY_CHARGES_BEFORE_DEPARTURE.contains(penaltyCharge.getValue())) {
			return FareRuleTimeWindow.BEFORE_DEPARTURE;
		}
		if (TravelPortConstants.PENALTY_CHARGES_AFTER_DEPARTURE.contains(penaltyCharge.getValue())) {
			return FareRuleTimeWindow.AFTER_DEPARTURE;
		}
		return FareRuleTimeWindow.DEFAULT;
	}

	protected void airFareDisplay(TripInfo selectedTrip) {
		AirFareDisplayReq fareDisplayReq = null;
		AirFareDisplayRsp fareDisplayRes = null;
		com.travelport.www.service.air_v50_0.AirServiceStub airService = bindingService.getAirService_V50();
		listener.setType(AirUtils.getLogType("AirFareDisplay", configuration));

		try {
			airService._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(airService, "TripFareRule");
			for (int index = 0; index < selectedTrip.getSegmentInfos().size(); index++) {
				if (selectedTrip.getSegmentInfos().get(index).getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
					List<SegmentInfo> subSegments =
							TravelPortUtils.getSubLegSegments(selectedTrip.getSegmentInfos(), index);
					fareDisplayReq = buildAirFareDisplayReq(subSegments);
					fareDisplayRes = airService.service(fareDisplayReq, getSessionContext(false));
					if (!checkAnyErrors(fareDisplayRes)) {
						parseFareDisplayRes(fareDisplayRes, subSegments);
					}
				}
			}
		} catch (RemoteException | com.travelport.www.service.air_v50_0.AirFaultMessage e) {
			throw new FareRuleUnHandledFaultException(e.getMessage());
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private void parseFareDisplayRes(AirFareDisplayRsp fareDisplayRes, List<SegmentInfo> subSegments) {
		FareDisplay_type0 fareDisplay = getFareDisplay(subSegments, fareDisplayRes.getFareDisplay());
		if (fareDisplay != null) {
			subSegments.get(0).getPriceInfo(0).getMiscInfo()
					.setFareRuleInfoValue(fareDisplay.getAirFareDisplayRuleKey().getTypeNonBlanks());
		}
	}

	private FareDisplay_type0 getFareDisplay(List<SegmentInfo> subLegSegments, FareDisplay_type0[] fareDisplays) {
		if (ArrayUtils.isNotEmpty(fareDisplays)) {
			for (FareDisplay_type0 fareDisplay : fareDisplays) {
				if (subLegSegments.get(0).getDepartureAirportCode().equals(fareDisplay.getOrigin().getTypeIATACode())
						&& subLegSegments.get(subLegSegments.size() - 1).getArrivalAirportCode()
								.equals(fareDisplay.getDestination().getTypeIATACode())
						&& subLegSegments.get(0).getPriceInfo(0).getBookingClass(PaxType.ADULT)
								.equals(fareDisplay.getBookingCode()[0].getCode().getTypeClassOfService())
						&& subLegSegments.get(0).getPriceInfo(0).getFareBasis(PaxType.ADULT)
								.equals(fareDisplay.getFareBasis())) {
					return fareDisplay;
				}
			}
		}
		return null;
	}

	private AirFareDisplayReq buildAirFareDisplayReq(List<SegmentInfo> subSegments) {
		AirFareDisplayReq fareDisplayReq = new AirFareDisplayReq();
		buildBaseCoreRequest_V50(fareDisplayReq);

		fareDisplayReq.setOrigin(getTypeIataCode(subSegments.get(0).getDepartureAirportCode()));

		fareDisplayReq.setDestination(getTypeIataCode(subSegments.get(subSegments.size() - 1).getArrivalAirportCode()));

		fareDisplayReq
				.setProviderCode(getTypeProviderCode_V50(configuration.getSupplierCredential().getProviderCode()));

		com.travelport.www.schema.common_v50_0.TypePassengerType[] passengerTypes =
				new com.travelport.www.schema.common_v50_0.TypePassengerType[1];
		com.travelport.www.schema.common_v50_0.TypePassengerType passengerType =
				new com.travelport.www.schema.common_v50_0.TypePassengerType();
		passengerType.setCode(getTypePTC_V50("ADT"));
		passengerTypes[0] = passengerType;
		fareDisplayReq.setPassengerType(passengerTypes);

		fareDisplayReq
				.setFareBasis(getFareBasisCode_V50(subSegments.get(0).getPriceInfo(0).getFareBasis(PaxType.ADULT)));

		com.travelport.www.schema.common_v50_0.Carrier_type0[] carriers =
				new com.travelport.www.schema.common_v50_0.Carrier_type0[1];
		com.travelport.www.schema.common_v50_0.Carrier_type0 carrier =
				new com.travelport.www.schema.common_v50_0.Carrier_type0();
		carrier.setCode(getTypeCarrierCode_V50(subSegments.get(0).getAirlineCode(false)));
		carriers[0] = carrier;
		fareDisplayReq.setCarrier(carriers);

		AirFareDisplayModifiers_type0 displayModifiers = new AirFareDisplayModifiers_type0();
		displayModifiers.setMaxResponses(BigInteger.valueOf(200));
		displayModifiers
				.setDepartureDate(TgsDateUtils.localDateTimeToDate(subSegments.get(0).getDepartTime()).getTime());
		displayModifiers.setFaresIndicator(TypeFaresIndicator.AllFares);
		displayModifiers.setProhibitTravelRestrictedFares(false);
		fareDisplayReq.setAirFareDisplayModifiers(displayModifiers);

		return fareDisplayReq;
	}

	protected boolean isFareRuleKeyAvailable(TripInfo selectedTrip) {
		return StringUtils
				.isNotEmpty(selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getFareRuleInfoValue());
	}
}
