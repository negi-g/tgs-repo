package com.tgs.services.fms.sources.amadeusndc;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import javax.xml.ws.BindingProvider;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyContent;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyType;
import com.tgs.services.fms.datamodel.farerule.FareRuleTimeWindow;
import org.apache.axis2.databinding.types.Token;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.google.common.base.Enums;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import org.iata.iata._2015._00._2018_1.offerpricerq.AggregatorType;
import org.iata.iata._2015._00._2018_1.offerpricerq.CarrierType;
import org.iata.iata._2015._00._2018_1.offerpricerq.CountryType;
import org.iata.iata._2015._00._2018_1.offerpricerq.CreateOrderType;
import org.iata.iata._2015._00._2018_1.offerpricerq.DataListsType;
import org.iata.iata._2015._00._2018_1.offerpricerq.IATAPayloadStandardAttributesType;
import org.iata.iata._2015._00._2018_1.offerpricerq.OfferPriceRQ;
import org.iata.iata._2015._00._2018_1.offerpricerq.ParticipantType;
import org.iata.iata._2015._00._2018_1.offerpricerq.PartyType;
import org.iata.iata._2015._00._2018_1.offerpricerq.PaxListType;
import org.iata.iata._2015._00._2018_1.offerpricerq.PointofSaleType2;
import org.iata.iata._2015._00._2018_1.offerpricerq.PricingParameterType;
import org.iata.iata._2015._00._2018_1.offerpricerq.RecipientType;
import org.iata.iata._2015._00._2018_1.offerpricerq.RequestType;
import org.iata.iata._2015._00._2018_1.offerpricerq.ResponseParametersType;
import org.iata.iata._2015._00._2018_1.offerpricerq.SelectedOfferItemType;
import org.iata.iata._2015._00._2018_1.offerpricerq.SelectedOfferType;
import org.iata.iata._2015._00._2018_1.offerpricerq.SenderType;
import org.iata.iata._2015._00._2018_1.offerpricerq.TravelAgencyType;
import org.iata.iata._2015._00._2018_1.offerpricers.*;
import org.iata.iata._2015._00._2018_1.offerpricers.FareDetailType.PassengerRefs;
import org.iata.iata._2015._00._2018_1.offerpricers.TaxDetailType.Breakdown.Tax;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import static com.tgs.services.fms.sources.amadeusndc.AmadeusNDCConstants.*;
import static com.tgs.services.fms.sources.amadeusndc.AmadeusNDCConstants.NOSHOW_BEFORE_TICKET;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class AmadeusNdcReviewManager extends AmadeusNdcServiceManager {

	protected TripInfo selectedTrip;
	protected SoapRequestResponseListner listner;
	private Map<String, PaxType> paxMap;
	private static final String ORA_DESIG_CODE = "SQ";
	protected Map<String, org.iata.iata._2015._00._2018_1.offerpricers.PenaltyType> penaltyMap;
	private Map<String, PriceClassType> priceClassMap;

	public void offerPrice() {
		log.debug("starting offerPrice Ndc...");
		penaltyMap = new HashMap<>();
		priceClassMap = new HashMap<>();
		AlteaNDC181PT servicePort = service.getAlteaNDC181Port();
		OfferPriceRQ offerPriceRQ = createOfferPriceRQ();
		BindingProvider bp = (BindingProvider) servicePort;
		addJAXHandlers(bp, bookingId, AmadeusNdcHeaderHandler._OFFER_PRICE_);
		OfferPriceRS offerPriceRS = servicePort.ndcOfferPrice(offerPriceRQ, null, null, getSecurityHostedUser());

		if (CollectionUtils.isNotEmpty(offerPriceRS.getError())) {
			StringJoiner errorMessage = new StringJoiner("");
			for (org.iata.iata._2015._00._2018_1.offerpricers.ErrorType error : offerPriceRS.getError()) {
				errorMessage.add(error.getCode() + ": " + error.getDescText());
			}
			throw new NoSeatAvailableException(errorMessage.toString());
		}
		org.iata.iata._2015._00._2018_1.offerpricers.PaxListType paxListType =
				offerPriceRS.getResponse().getDataLists().getPaxList();
		List<org.iata.iata._2015._00._2018_1.offerpricers.PaxType> paxTypes = paxListType.getPax();
		List<PriceClassType> priceClassList =
				offerPriceRS.getResponse().getDataLists().getPriceClassList().getPriceClass();
		paxMap = new HashMap<>();
		for (org.iata.iata._2015._00._2018_1.offerpricers.PaxType paxType : paxTypes) {
			String paxId = paxType.getPaxID();
			String ptc = paxType.getPTC().substring(0, 1);
			paxMap.put(paxId, PaxType.getEnumFromCode(ptc));
		}

		PenaltyListType penalties = offerPriceRS.getResponse().getDataLists().getPenaltyList();

		for (PenaltyType penalty : penalties.getPenalty()) {
			String penaltyId = penalty.getPenaltyID();
			penaltyMap.put(penaltyId, penalty);
		}

		for (PriceClassType priceClass : priceClassList) {
			priceClassMap.put(priceClass.getPriceClassID(), priceClass);
		}
		List<PriceInfo> priceInfoList = selectedTrip.getSegmentInfos().get(0).getPriceInfoList();
		if (CollectionUtils.isEmpty(priceInfoList)) {
			log.error("SelectedTrip has no priceInfo", selectedTrip);
		}
		PriceInfo priceInfo = priceInfoList.get(0);
		Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();

		OfferType offerType = offerPriceRS.getResponse().getPricedOffer().getOffer();
		setFareType(priceInfo, offerType);
		validateFareDetails(fareDetails, offerType);

		PriceMiscInfo miscInfo = priceInfo.getMiscInfo();
		setMiniFareRuleFromPenalty(offerType.getPenaltyRefID(), miscInfo);

	}

	private void setFareType(PriceInfo priceInfo, OfferType offerType) {
		FareComponentType fareComponent =
				offerType.getOfferItem().get(0).getFareDetail().get(0).getFareComponent().get(0);
		String priceClassRefId = fareComponent.getPriceClassRef();
		String priceClassName = priceClassMap.get(priceClassRefId).getName();
		priceInfo.setFareIdentifier(AmadeusNdcUtils.getFareType(priceClassName));
	}


	private void validateFareDetails(Map<PaxType, FareDetail> fareDetails, OfferType offerType) {
		OfferItemType offerItem = offerType.getOfferItem().get(0);
		List<FareDetailType> fareDetailTypeList = offerItem.getFareDetail();
		for (FareDetailType fareDetailType : fareDetailTypeList) {
			List<PassengerRefs> paxRefIdList = fareDetailType.getPassengerRefs();
			for (PassengerRefs paxRefIdString : paxRefIdList) {
				List<String> paxRefIds = Arrays.asList(paxRefIdString.getValue().split(" "));
				for (String paxRefId : paxRefIds) {
					PaxType paxType = paxMap.get(paxRefId);
					log.info("Updating fareComponent for PaxType {}", paxType);
					updateFareComponents(fareDetails.get(paxType).getFareComponents(), fareDetailType.getPrice());
				}
			}
		}
	}


	private void updateFareComponents(Map<FareComponent, Double> fareComponents,
			FarePriceDetailType farePriceDetailType) {
		fareComponents = new HashMap<>();
		double totalFare = getAmountBasedOnCurrency(
				farePriceDetailType.getTotalAmount().getDetailCurrencyPrice().getTotal().getValue(),
				farePriceDetailType.getTotalAmount().getDetailCurrencyPrice().getTotal().getCode());
		fareComponents.put(FareComponent.TF, totalFare);

		double baseFare = getAmountBasedOnCurrency(farePriceDetailType.getBaseAmount().getValue(),
				farePriceDetailType.getBaseAmount().getCode());
		fareComponents.put(FareComponent.BF, baseFare);

		if (farePriceDetailType.getTaxes().getBreakdown() != null
				&& CollectionUtils.isNotEmpty(farePriceDetailType.getTaxes().getBreakdown().getTax())) {
			for (Tax tax : farePriceDetailType.getTaxes().getBreakdown().getTax()) {
				if (tax.getAmount() != null && tax.getAmount().getValue() != null) {
					FareComponent fareComponent = Enums.getIfPresent(GDSFareComponentMapper.class, tax.getTaxCode())
							.or(GDSFareComponentMapper.TX).getFareComponent();
					double taxAmount = getAmountBasedOnCurrency(tax.getAmount().getValue(), tax.getAmount().getCode());
					double oldValue = fareComponents.getOrDefault(fareComponent, 0.0);
					fareComponents.put(fareComponent, oldValue + taxAmount);
				}
			}
		} else {
			double taxes = getAmountBasedOnCurrency(farePriceDetailType.getTaxes().getTotal().getValue(),
					farePriceDetailType.getTaxes().getTotal().getCode());
			fareComponents.put(FareComponent.AT, taxes);
		}
	}


	private OfferPriceRQ createOfferPriceRQ() {
		OfferPriceRQ offerPriceRQ = new OfferPriceRQ();

		RequestType requestType = new RequestType();
		DataListsType dataList = new DataListsType();
		PaxListType paxList = buildPaxList();
		dataList.setPaxList(paxList);
		requestType.setDataLists(dataList);

		CreateOrderType pricedOfferType = new CreateOrderType();
		List<SelectedOfferType> selectedOfferTypeList = pricedOfferType.getSelectedOffer();
		populateSelectedOfferList(selectedOfferTypeList, paxList);
		requestType.setPricedOffer(pricedOfferType);
		offerPriceRQ.setPayloadAttributes(getPayloadAttribute());
		offerPriceRQ.setPointOfSale(getPointOfScale());
		offerPriceRQ.setParty(getPartyType());
		offerPriceRQ.setRequest(requestType);

		return offerPriceRQ;
	}

	private void populateSelectedOfferList(List<SelectedOfferType> selectedOfferTypeList, PaxListType paxList) {
		if (CollectionUtils.isEmpty(selectedOfferTypeList)) {
			log.error("NDC selectOfferType list is empty");
		}
		PriceMiscInfo priceMiscInfo = selectedTrip.getSegmentInfos().get(0).getPriceInfoList().get(0).getMiscInfo();
		SelectedOfferType selectedOfferType = new SelectedOfferType();
		selectedOfferType.setOfferRefID(priceMiscInfo.getFareKey());
		selectedOfferType.setShoppingResponseRefID(priceMiscInfo.getJourneyKey());
		// owner code and ORA both are same string value
		selectedOfferType.setOwnerCode(supplierConf.getSupplierCredential().getProviderCode());

		List<SelectedOfferItemType> selectedOfferItemTypeList = selectedOfferType.getSelectedOfferItem();
		SelectedOfferItemType selectedOfferItemType = new SelectedOfferItemType();
		selectedOfferItemType.setOfferItemRefID(priceMiscInfo.getFareLevel());
		List<String> paxRefIds = selectedOfferItemType.getPaxRefID();
		for (org.iata.iata._2015._00._2018_1.offerpricerq.PaxType paxType : paxList.getPax()) {
			String paxId = paxType.getPaxID();
			paxRefIds.add(paxId);
		}
		selectedOfferItemTypeList.add(selectedOfferItemType);
		selectedOfferTypeList.add(selectedOfferType);

	}

	private PaxListType buildPaxList() {
		PaxListType paxList = new PaxListType();
		List<org.iata.iata._2015._00._2018_1.offerpricerq.PaxType> paxTypeList = paxList.getPax();
		Map<PaxType, Integer> paxMap = selectedTrip.getPaxInfo();
		for (PaxType pax : paxMap.keySet()) {
			Integer quota = paxMap.get(pax);
			while (quota != 0) {
				org.iata.iata._2015._00._2018_1.offerpricerq.PaxType paxType =
						new org.iata.iata._2015._00._2018_1.offerpricerq.PaxType();
				String paxTypeCode = com.tgs.services.base.enums.PaxType.getStringReprestation(pax);
				String calculatedId = paxTypeCode + quota.toString();

				if (StringUtils.equalsIgnoreCase(INFANT, paxTypeCode)) {
					Token parentIdToken = new Token();
					parentIdToken.setValue(StringUtils.join(ADULT, String.valueOf(quota)));
					paxType.setPaxRefID(calculatedId);
				}
				paxType.setPaxID(calculatedId);
				paxType.setPTC(paxTypeCode);
				paxTypeList.add(paxType);
				quota--;
			}
		}
		return paxList;
	}

	private IATAPayloadStandardAttributesType getPayloadAttribute() {
		IATAPayloadStandardAttributesType payloadAttribute = new IATAPayloadStandardAttributesType();
		payloadAttribute.setVersion(AmadeusNDCConstants.SERVICE_VERSION);
		return payloadAttribute;
	}

	private ResponseParametersType getResponseParametersType() {
		ResponseParametersType responseParametersType = new ResponseParametersType();
		PricingParameterType pricingParameterType = new PricingParameterType();
		pricingParameterType.setOverrideCurCode(getCurrencyCode());
		responseParametersType.setPricingParameter(pricingParameterType);
		return responseParametersType;
	}

	private PointofSaleType2 getPointOfScale() {
		PointofSaleType2 pointOfSale = new PointofSaleType2();
		CountryType countryCode = new CountryType();
		countryCode.setCountryCode(AmadeusNDCConstants.COUNTRY_CODE);
		pointOfSale.setCountry(countryCode);
		return pointOfSale;
	}

	private PartyType getPartyType() {
		PartyType partyType = new PartyType();

		RecipientType recipientType = new RecipientType();
		CarrierType carrierType = new CarrierType();
		carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
		recipientType.setORA(carrierType);
		partyType.setRecipient(recipientType);

		SenderType senderType = new SenderType();
		TravelAgencyType travelAgencyType = new TravelAgencyType();
		travelAgencyType.setAgencyID(supplierConf.getSupplierCredential().getIataNumber());
		travelAgencyType.setIATANumber(new BigDecimal(supplierConf.getSupplierCredential().getIataNumber()));
		travelAgencyType.setName(supplierConf.getSupplierCredential().getAgentUserName());
		travelAgencyType.setTypeCode(
				org.iata.iata._2015._00._2018_1.offerpricerq.TravelAgencyTypeCodeContentType.ONLINE_TRAVEL_AGENCY);
		senderType.setTravelAgency(travelAgencyType);
		partyType.setSender(senderType);
		return partyType;
	}

	protected void setMiniFareRuleFromPenalty(List<String> penaltyRefs, PriceMiscInfo miscInfo) {
		if (CollectionUtils.isNotEmpty(penaltyRefs)) {
			FareRuleInformation ruleInfo = miscInfo.getFareRuleInfo();
			Map<FareRulePolicyType, Map<FareRuleTimeWindow, FareRulePolicyContent>> fareRuleInfo = new HashMap<>();
			Map<FareRuleTimeWindow, FareRulePolicyContent> cancellationTimeWindow = new HashMap<>();
			Map<FareRuleTimeWindow, FareRulePolicyContent> dateChangeTimeWindow = new HashMap<>();
			Map<FareRuleTimeWindow, FareRulePolicyContent> noShowTimeWindow = new HashMap<>();
			for (String penaltyCode : penaltyRefs) {
				org.iata.iata._2015._00._2018_1.offerpricers.PenaltyType penaltyType = penaltyMap.get(penaltyCode);
				if (BooleanUtils.isTrue(penaltyType.isChangeFeeInd())
						&& REISSUE_BEFORE_TICKET.contains(penaltyType.getAppCode())) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					dateChangeTimeWindow.put(FareRuleTimeWindow.BEFORE_DEPARTURE, content);
				}
				if (BooleanUtils.isTrue(penaltyType.isCancelFeeInd())
						&& CANCELLATION_BEFORE_TICKET.contains(penaltyType.getAppCode())) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					cancellationTimeWindow.put(FareRuleTimeWindow.AFTER_DEPARTURE, content);
				}
				if (BooleanUtils.isTrue(penaltyType.isCancelFeeInd())
						&& CANCELLATION_AFTER_TICKET.contains(penaltyType.getAppCode())) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					cancellationTimeWindow.put(FareRuleTimeWindow.AFTER_DEPARTURE, content);
				}
				if (BooleanUtils.isTrue(penaltyType.isChangeFeeInd())
						&& REISSUE_AFTER_TICKET.contains(penaltyType.getAppCode())) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					dateChangeTimeWindow.put(FareRuleTimeWindow.BEFORE_DEPARTURE, content);
				}
				if (NOSHOW_AFTER_TICKET.contains(penaltyType)) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					noShowTimeWindow.put(FareRuleTimeWindow.AFTER_DEPARTURE, content);
				}
				if (NOSHOW_BEFORE_TICKET.contains(penaltyType)) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					noShowTimeWindow.put(FareRuleTimeWindow.BEFORE_DEPARTURE, content);
				}
			}
			fareRuleInfo.put(FareRulePolicyType.CANCELLATION, cancellationTimeWindow);
			fareRuleInfo.put(FareRulePolicyType.DATECHANGE, dateChangeTimeWindow);
			fareRuleInfo.put(FareRulePolicyType.NO_SHOW, noShowTimeWindow);
			ruleInfo.setFareRuleInfo(fareRuleInfo);
			miscInfo.setFareRuleInfo(ruleInfo);
		}
	}


	private boolean isValidAmount(org.iata.iata._2015._00._2018_1.offerpricers.PenaltyType penaltyType) {
		return penaltyType.getPenaltyAmount() != null && penaltyType.getPenaltyAmount().getValue() != null
				&& penaltyType.getPenaltyAmount().getValue().doubleValue() >= 0;
	}

}
