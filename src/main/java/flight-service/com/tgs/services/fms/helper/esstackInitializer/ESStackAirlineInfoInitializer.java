package com.tgs.services.fms.helper.esstackInitializer;

import java.util.List;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ESStackInitializer;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.fms.dbmodel.DbAirlineInfo;
import com.tgs.services.fms.jparepository.AirlineInfoService;

@Service
public class ESStackAirlineInfoInitializer extends ESStackInitializer {

	@Autowired
	AirlineInfoService service;

	@Autowired
	ElasticSearchCommunicator esCommunicator;

	@Override
	public void process() {
		List<DbAirlineInfo> airlineHashMap = service.findAll();
		if (CollectionUtils.isNotEmpty(airlineHashMap)) {
			this.deleteExistingData();
			esCommunicator.addBulkDocuments(airlineHashMap, ESMetaInfo.AIRLINE);
		}
	}

	public void deleteExistingData() {
		esCommunicator.deleteIndex(ESMetaInfo.AIRLINE);
	}

}
