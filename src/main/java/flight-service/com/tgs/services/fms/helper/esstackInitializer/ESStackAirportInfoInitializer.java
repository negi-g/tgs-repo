package com.tgs.services.fms.helper.esstackInitializer;

import java.util.List;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ESStackInitializer;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.fms.dbmodel.DbAirportInfo;
import com.tgs.services.fms.jparepository.AirportInfoService;

@Service
public class ESStackAirportInfoInitializer extends ESStackInitializer {

	@Autowired
	AirportInfoService service;

	@Autowired
	ElasticSearchCommunicator esCommunicator;


	@Override
	public void process() {
		List<DbAirportInfo> airportHashMap = service.findAll();
		if (CollectionUtils.isNotEmpty(airportHashMap)) {
			this.deleteExistingData();
			esCommunicator.addBulkDocuments(airportHashMap, ESMetaInfo.AIRPORT);
		}

	}

	public void reload(List<DbAirportInfo> list) {
		if (CollectionUtils.isNotEmpty(list)) {
			esCommunicator.addBulkDocument(list, ESMetaInfo.AIRPORT);
		}
	}

	public void delete(DbAirportInfo airportInfo) {
		if (airportInfo != null) {
			esCommunicator.deleteDocumentById(airportInfo, ESMetaInfo.AIRPORT);
		}
	}

	public void deleteExistingData() {
		esCommunicator.deleteIndex(ESMetaInfo.AIRPORT);
	}
}
