package com.tgs.services.fms.sources.kafila;

import java.util.List;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.kafila.reviewResponse.ReviewResponse;
import com.tgs.services.oms.datamodel.Order;

@Service
public class KafilaAirBookingFactory extends AbstractAirBookingFactory {

	protected KafilaBindingService bindingService;
	RestAPIListener listener;
	boolean isSuccess;

	public KafilaAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	@Override
	public boolean doBooking() {

		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(bookingSegments.getSegmentInfos());
		List<TripInfo> tripInfos = tripInfo.splitTripInfo(false);
		AirSearchQuery searchQuery = AirUtils.combineSearchQuery(AirUtils.getSearchQueryFromTripInfos(tripInfos));

		AirlineInfo airlineInfo = bookingSegments.getSegmentInfos().get(0).getFlightDesignator().getAirlineInfo();
		AirlineInfo returnAirlineInfo = null;
		if (searchQuery.isDomesticReturn()) {
			returnAirlineInfo = tripInfos.get(1).getSegmentInfos().get(0).getFlightDesignator().getAirlineInfo();
		}
		listener = new RestAPIListener(bookingId);
		bindingService = KafilaBindingService.builder().configuration(supplierConf).listener(listener).build();

		// double amount = bookingManager.fareCheckBeforeConfirmation();
		TripInfo fareCheckTrip = KafilaUtils.createTripFromSegmentList(bookingSegments.getSegmentInfos());

		AirSearchQuery searchquery = AirUtils.getSearchQueryFromTripInfo(fareCheckTrip);
		KafilaReviewManager reviewManager = KafilaReviewManager.builder().searchQuery(searchquery).build();
		ReviewResponse reviewResponse = bindingService.doReview(reviewManager.buildReviewRequest(fareCheckTrip));

		bookingSegments.getSegmentInfos().get(0).getPriceInfoList().get(0).getMiscInfo()
				.setAvailabilityDisplayType(GsonUtils.getGson().toJson(reviewResponse));

		KafilaBookingManager bookingManager = KafilaBookingManager.builder().bookingId(bookingId).order(order)
				.segmentInfos(bookingSegments.getSegmentInfos()).listener(listener).supplierConfiguration(supplierConf)
				.user(bookingUser).bindingService(bindingService).deliveryInfo(deliveryInfo).build();
		bookingManager.setUUID(bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId());

		double amountPayableToSupplier = reviewResponse.getFARE().get(0).getGRAND_TOTAL();
		if (!isFareDiff(amountPayableToSupplier)) {
			isSuccess = bookingManager.createPNR();
			if (isSuccess) {
				BookingUtils.updateSupplierBookingId(bookingSegments.getSegmentInfos(),
						bookingManager.getKafilaBookingId());
				bookingManager.checkAndUpdateBookingInfo(tripInfos, searchQuery);
			}
		}


		if (searchQuery.isOneWay() && BooleanUtils.isTrue(airlineInfo.getIsTkRequired())) {
			isSuccess = BookingUtils.hasPNRInAllSegments(bookingSegments.getSegmentInfos())
					&& BookingUtils.hasTicketNumberInAllSegments(bookingSegments.getSegmentInfos());
		} else if (searchQuery.isDomesticReturn() && BooleanUtils.isTrue(airlineInfo.getIsTkRequired())
				&& BooleanUtils.isTrue(returnAirlineInfo.getIsTkRequired())) {
			isSuccess = BookingUtils.hasPNRInAllSegments(bookingSegments.getSegmentInfos())
					&& BookingUtils.hasTicketNumberInAllSegments(bookingSegments.getSegmentInfos());
		} else if (searchQuery.isIntlReturn() && BooleanUtils.isTrue(airlineInfo.getIsTkRequired())) {
			isSuccess = BookingUtils.hasPNRInAllSegments(bookingSegments.getSegmentInfos())
					&& BookingUtils.hasTicketNumberInAllSegments(bookingSegments.getSegmentInfos());
		} else {
			isSuccess = BookingUtils.hasPNRInAllSegments(bookingSegments.getSegmentInfos());
		}

		pnr = bookingSegments.getSegmentInfos().get(0).getTravellerInfo().get(0).getPnr();
		return isSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		// not supprted
		return false;
	}
}
