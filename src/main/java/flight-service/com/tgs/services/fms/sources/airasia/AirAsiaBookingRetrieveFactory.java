package com.tgs.services.fms.sources.airasia;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.tempuri.BookingServiceBasicHttpBinding_IBookingServiceStub;
import org.tempuri.SessionServiceBasicHttpBinding_ISessionServiceStub;

@Slf4j
@Service
public class AirAsiaBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	protected SoapRequestResponseListner listener = null;

	protected AirAsiaBindingService bindingService;

	protected SessionServiceBasicHttpBinding_ISessionServiceStub sessionStub;

	protected BookingServiceBasicHttpBinding_IBookingServiceStub bookingStub;

	public AirAsiaBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}

	public void initialize() {
		bindingService = bindingService.builder().cacheCommunicator(cachingCommunicator).build();
		sessionStub = bindingService.getSessionStub(supplierConf, null);
		bookingStub = bindingService.getBookingStub(supplierConf, null);
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		AirAsiaSessionManager sessionManager = null;
		try {
			initialize();
			pnrBooking = AirImportPnrBooking.builder().build();
			listener = new SoapRequestResponseListner(pnr, "", supplierConf.getBasicInfo().getSupplierId());
			sessionManager = AirAsiaSessionManager.builder().configuration(supplierConf).sessionStub(sessionStub)
					.listener(listener).bookingUser(bookingUser).build();
			sessionManager.init();
			sessionManager.openSession();
			if (StringUtils.isNotEmpty(sessionManager.getSessionSignature())) {
				log.debug("Inside Retreieve Manager to fetch Trips {}", pnr);
				AirAsiaBookingRetrieveManager retrieveManager =
						AirAsiaBookingRetrieveManager.builder().bookingUser(bookingUser).configuration(supplierConf)
								.sessionSignature(sessionManager.sessionSignature).bookingStub(bookingStub)
								.listener(listener).moneyExchnageComm(moneyExchangeComm).build();
				retrieveManager.init();
				pnrBooking = retrieveManager.retrieveBooking(pnr);
			}
		} finally {
			sessionManager.closeSession();
			storeSessionInfo();
		}
		return pnrBooking;
	}

	private void storeSessionInfo() {
		bindingService =
				AirAsiaBindingService.builder().cacheCommunicator(cachingCommunicator).user(bookingUser).build();
		bindingService.storeSessionMetaInfo(supplierConf, SessionServiceBasicHttpBinding_ISessionServiceStub.class,
				sessionStub);
		bindingService.storeSessionMetaInfo(supplierConf, BookingServiceBasicHttpBinding_IBookingServiceStub.class,
				bookingStub);
	}
}
