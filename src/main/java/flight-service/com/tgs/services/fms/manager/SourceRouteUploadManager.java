package com.tgs.services.fms.manager;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.SourceRouteInfoFilter;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;
import com.tgs.services.fms.helper.SourceRouteInfoHelper;
import com.tgs.services.fms.jparepository.SourceRouteInfoService;
import com.tgs.services.fms.restmodel.SourceRouteBulkUploadRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;


@Slf4j
@Service
public class SourceRouteUploadManager {

	@Autowired
	SourceRouteInfoService routeInfoService;

	@Autowired
	private Validator validator;

	public BulkUploadResponse bulkUpdateSourceRoute(SourceRouteBulkUploadRequest sourceRouteUploadRequest)
			throws Exception {
		if (sourceRouteUploadRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(sourceRouteUploadRequest.getUploadId());
		}
		String jobId = BulkUploadUtils.generateRandomJobId();
		final ContextData contextData = SystemContextHolder.getContextData();
		Runnable sourceRoute = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			for (com.tgs.services.fms.datamodel.SourceRouteInfo sourceRouteRequest : sourceRouteUploadRequest
					.getSourceRouteInfos()) {
				Future<?> f = executor.submit(() -> {
					SystemContextHolder.setContextData(contextData);
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					Set<ConstraintViolation<SourceRouteInfo>> violations = validator.validate(sourceRouteRequest);
					if (!violations.isEmpty()) {
						StringJoiner sb = new StringJoiner(",");
						for (ConstraintViolation<SourceRouteInfo> constraintViolation : violations) {
							sb.add(constraintViolation.getMessage());
						}
						uploadInfo.setErrorMessage(sb.toString());
						uploadInfo.setStatus(UploadStatus.FAILED);
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.info("Unable to save source route with {} cause {}", sourceRouteRequest, sb.toString());
					} else {
						uploadInfo.setId(sourceRouteRequest.getRowId());
						try {
							saveSourceRoute(sourceRouteRequest);
							uploadInfo.setStatus(UploadStatus.SUCCESS);
						} catch (Exception e) {
							uploadInfo.setErrorMessage(e.getMessage());
							uploadInfo.setStatus(UploadStatus.FAILED);
							log.error("Unable to save source route with {} ", sourceRouteRequest, e);
						} finally {
							BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
							log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
						}
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, BulkUploadUtils.INITIALIZATION_NOT_REQUIRED);

			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting sourceroute for {} ", jobId, e);
			}
		};
		return BulkUploadUtils.processBulkUploadRequest(sourceRoute, sourceRouteUploadRequest.getUploadType(), jobId);
	}

	public void saveSourceRoute(SourceRouteInfo sourceRoute) {
		log.debug("Uploading SourceRoute {}", sourceRoute);
		DbSourceRouteInfo dbSourceRouteInfo = null;

		SourceRouteInfoFilter sourceRouteFilter = new SourceRouteInfoFilter();
		if (Objects.nonNull(sourceRoute.getId())) {
			dbSourceRouteInfo = routeInfoService.findOne(sourceRoute.getId());
		} else {
			sourceRouteFilter.setSrc(sourceRoute.getSrc());
			sourceRouteFilter.setDest(sourceRoute.getDest());
			sourceRouteFilter.setSourceId(sourceRoute.getSourceId());
			List<SourceRouteInfo> listSourceRoute = routeInfoService.findAll(sourceRouteFilter);
			if (CollectionUtils.isNotEmpty(listSourceRoute)) {
				dbSourceRouteInfo = new DbSourceRouteInfo().from(listSourceRoute.get(0));
			}
		}
		dbSourceRouteInfo = Optional.ofNullable(dbSourceRouteInfo).orElse(new DbSourceRouteInfo()).from(sourceRoute);
		routeInfoService.save(dbSourceRouteInfo);
	}


}
