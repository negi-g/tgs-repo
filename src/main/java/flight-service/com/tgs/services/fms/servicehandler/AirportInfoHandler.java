package com.tgs.services.fms.servicehandler;

import java.util.Objects;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.fms.dbmodel.DbAirportInfo;
import com.tgs.services.fms.jparepository.AirportInfoService;
import com.tgs.services.fms.restmodel.AirportInfoResponse;

@Service
public class AirportInfoHandler extends ServiceHandler<AirportInfo, AirportInfoResponse> {

	@Autowired
	AirportInfoService airportService;

	@Autowired
	SyncService syncService;

	@Override
	public void beforeProcess() throws Exception {
		if (StringUtils.isBlank(request.getCode()) || StringUtils.isBlank(request.getName())) {
			throw new CustomGeneralException("Code or Name cannot be blank");
		}
	}

	@Override
	public void process() throws Exception {
		com.tgs.services.fms.dbmodel.DbAirportInfo dbAirportInfo = null;
		if (Objects.nonNull(request.getId())) {
			dbAirportInfo = airportService.findById(request.getId());
		}
		if (Objects.nonNull(request.getCode()) && dbAirportInfo == null) {
			dbAirportInfo = airportService.findByCode(request.getCode());
			if (dbAirportInfo != null) {
				throw new CustomGeneralException(SystemError.DUPLICATE_AIRPORTCODE);
			}
		}

		dbAirportInfo = Optional.ofNullable(dbAirportInfo).orElse(new com.tgs.services.fms.dbmodel.DbAirportInfo())
				.from(request);
		dbAirportInfo = airportService.save(dbAirportInfo);
		request.setId(dbAirportInfo.getId());
		response.getAirportInfos().add(dbAirportInfo.toDomain());
		syncService.sync("fms", request);
	}

	@Override
	public void afterProcess() throws Exception {

	}


	public BaseResponse deleteAirport(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbAirportInfo dbAirportInfo = airportService.findById(id);

		if (Objects.nonNull(dbAirportInfo)) {
			airportService.deleteById(dbAirportInfo);
			syncService.sync("fms", null);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public AirportInfoResponse updateStatus(Long id, Boolean status) {

		DbAirportInfo dbAirportInfo = airportService.findById(id);
		AirportInfoResponse airportinfoResponse = new AirportInfoResponse();
		if (Objects.isNull(dbAirportInfo)) {
			airportinfoResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
			return airportinfoResponse;
		}
		dbAirportInfo.setEnabled(status);
		dbAirportInfo = airportService.save(dbAirportInfo);
		syncService.sync("fms", null);
		airportinfoResponse.getAirportInfos().add(dbAirportInfo.toDomain());

		return airportinfoResponse;
	}
}
