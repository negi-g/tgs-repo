package com.tgs.services.fms.sources.travelport.session;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class TravelPortConstants {

	public static final String SESSIONED = "Sessioned";

	public static final String TERMINAL_COMMAND = "RULA/JNTAX|R.P|ER";

	public static final String TICKET_DATE = "T*";

	public static final String TYPE = "ACTIVE";

	public static final String END_SESSION_COMMAND = "End";

	public static final String IGNORE_SESSION_COMMAND = "Ignore";

	public static final String ADULT = "ADT";

	public static final String CHILD = "CNN";

	public static final String INFANT = "INF";

	public static final String ORIGIN_APPLICATION = "UAPI";

	public static final String TRACE_ID = "trace";

	public static final String AIR_SERVICE = "AirService";

	public static final String UNIVERSAL_RECORD_SERVICE = "UniversalRecordService";

	public static final String SHARED_BOOKING_SERVICE = "SharedBookingService";

	public static final String PASSIVE_SERVICE = "PassiveService";

	public static final String OSI = "OSI";

	// Bank Settlement Process
	public static final String BSP = "Cash";

	public static final String CREDIT_CARD_MODE = "Credit";

	public static final String ECONOMY = "Economy";

	public static final String PREMIUM_ECONOMY = "PremiumEconomy";

	public static final String BUSINESS = "Business";

	public static final String FIRST = "First";

	public static final String ERROR = "error";

	public static final String WARNING = "warning";

	public static final String SEGMENT_TYPE = "Tour";

	public static final String PASSIVE_REMARK_TYPE = "Retention";

	public static final String AFTER_DEPARTURE = "After Departure";

	public static final String CHILD_AGE = "08";
	public static final String INFANT_AGE = "01";

	public static final int TKT_NUMBER_LENGTH = 13;

	public static final String DEFAULT = "DEFAULT";

	public static final String AGENCY = "AGENCY";

	public static final String CHLD = "CHLD";

	public static final String HK = "HK";

	// SSR key only for child
	public static final String CHD_SSR_KEY = "1B";

	public static final String CHG = "CHG";

	public static final String CANCELLATION_CAT = "Cancellation";

	public static final String NOSHOW_CAT = "NoShowPenalty";

	public static final String CHANGEITIN_CAT = "ChangeItinPenalty";

	public static final String AMOUNT1_CAT = "Amt1";

	public static final String CURRENCY1_CAT = "Currency1";

	public static final String CANCELLATIONREEFUNDS_CAT = "CancellationsRefunds";

	public static final String APPLIES = "X";

	public static final String PENALTY_CHARGES_CAT = "PenaltyCharges";

	public static final String VOLUNTARY = "Voluntary";

	// Penalty charges 1 and 4 is applicable by default
	public static final List<String> PENALTY_CHARGES_DEFAULT = Arrays.asList("1", "4");

	// Penalty charges 2 and 5 is applicable before departure
	public static final List<String> PENALTY_CHARGES_BEFORE_DEPARTURE = Arrays.asList("2", "5");

	// Penalty charges 3 and 6 is applicable after departure
	public static final List<String> PENALTY_CHARGES_AFTER_DEPARTURE = Arrays.asList("3", "6");

	public static final Map<String, String> SSRINFO = new HashMap<String, String>() {
		{
			put("SPML", "SPECIAL MEAL");
			put("AVIH", "ANIMAL IN HOLD");
			put("BULK", "BULKY BAGGAGE");
			put("CBBG", "CABIN BAGGAGE");
			put("DPNA", "DISABLE PAX NEED ASSIST INTELLECT/DEVELOP");
			put("EPAY", "GUARANTEED ELECTRONIC PAYMENT");
			put("EXST", "EXTRA SEAT");
			put("FRAG", "FRAGILE BAGGAGE");
			put("GPST", "GROUP SEAT");
			put("GRPF", "GROUP FARE");
			put("LANG", "LANGUAGE ASSISTANCE");
			put("MAAS", "MEET AND ASSIST");
			put("PETC", "PET IN CABIN");
			put("SEMN", "SHIPS CREW");
			put("SPEQ", "SPORTS EQUIPMENT");
			put("TKNM", "MANUALLY ENTER TICKET NUMBER");
			put("UMNR", "UNACCOMPANIED MINOR");
			put("WEAP", "WEAPON FIREARM OR AMMUNITION AS CHKD BAG");
			put("XBAG", "EXCESS BAGGAGE");
		}
	};


	public static HashMap<String, String> FARERULE_CATEGORY = new HashMap<String, String>() {
		{
			put("16", "CHANGES/CANCELLATIONS");
		}
	};

	public static final String AGENT_USER_NAME = "user";

	public static final String UAPI = "uAPI";

	public static final List<String> airlinesRequireChildSSR = Arrays.asList("LH");

	public static final String CAT_16 = "CAT 16";

	public static final Integer PNR_ACTIVE_DAYS = 180;

	public static final String SSR_TYPE = "CKIN";
	
	public static final String OSI_KEY_TYPE = "1";
}
