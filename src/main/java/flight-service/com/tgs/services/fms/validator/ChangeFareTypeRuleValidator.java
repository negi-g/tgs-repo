package com.tgs.services.fms.validator;

import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.UnmaskFareTypeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.ChangeFareTypeOutput;

@Service
public class ChangeFareTypeRuleValidator extends AbstractAirConfigRuleValidator {

	public static final String REGEX = "[a-zA-Z_]+";

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {
		if (iRuleOutput == null) {
			return;
		}

		if (iRuleOutput instanceof ChangeFareTypeOutput) {
			ChangeFareTypeOutput changeFareTypeOutput = (ChangeFareTypeOutput) iRuleOutput;
			if (StringUtils.isBlank(changeFareTypeOutput.getFareType())
					|| !changeFareTypeOutput.getFareType().matches(REGEX)) {
				errors.rejectValue(fieldName, SystemError.INVALID_VALUE.errorCode(),
						SystemError.INVALID_VALUE.getMessage());
				return;
			}

		}

	}

}
