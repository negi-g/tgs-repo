package com.tgs.services.fms.jparepository;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.helper.SourceRouteInfoHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.tgs.filters.SourceRouteInfoFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;
import javax.validation.Valid;

@Slf4j
@Service
public class SourceRouteInfoService extends SearchService<DbSourceRouteInfo> {

	@Autowired
	SourceRouteInfoRepository sourceRouteRepo;

	@Autowired
	SourceRouteInfoHelper srHelper;

	public List<DbSourceRouteInfo> findAll(Pageable pageable) {
		List<DbSourceRouteInfo> sourceRouteInfoList = new ArrayList<>();
		sourceRouteRepo.findAll(pageable).forEach(sourceRouteInfo -> sourceRouteInfoList.add(sourceRouteInfo));
		return sourceRouteInfoList;
	}

	public void addAll(List<DbSourceRouteInfo> sourceRouteInfos) {
		sourceRouteInfos.forEach(sourceRouteInfo -> {
			try {
				sourceRouteInfo.setSrc(StringUtils.upperCase(sourceRouteInfo.getSrc()));
				sourceRouteInfo.setDest(StringUtils.upperCase(sourceRouteInfo.getDest()));
				if (!sourceRouteInfo.toDomain().isSrcAndDestAreWild()
						&& StringUtils.equalsIgnoreCase(sourceRouteInfo.getSrc(), sourceRouteInfo.getDest())) {
					throw new CustomGeneralException(SystemError.SOURCE_ROUTE_SAME_SRC_DEST_NA);
				}
				sourceRouteRepo.saveAndFlush(sourceRouteInfo);
			} catch (Exception e) {
				log.error("Unable to save DbSourceRouteInfo {}", sourceRouteInfo);
			}
		});
	}

	public DbSourceRouteInfo findOne(Long id) {
		return sourceRouteRepo.findOne(id);
	}

	public DbSourceRouteInfo save(DbSourceRouteInfo dbSRoute) {
		dbSRoute.setSrc(StringUtils.upperCase(dbSRoute.getSrc()));
		dbSRoute.setDest(StringUtils.upperCase(dbSRoute.getDest()));
		if (!dbSRoute.toDomain().isSrcAndDestAreWild()
				&& StringUtils.equalsIgnoreCase(dbSRoute.getSrc(), dbSRoute.getDest())) {
			throw new CustomGeneralException(SystemError.SOURCE_ROUTE_SAME_SRC_DEST_NA);
		}
		dbSRoute = sourceRouteRepo.saveAndFlush(dbSRoute);
		srHelper.updateCacheAndInMemory(dbSRoute.toDomain());
		return dbSRoute;
	}


	public BaseResponse deleteById(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbSourceRouteInfo sourceRouteInfo = findOne(id);
		if (sourceRouteInfo == null) {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
			return baseResponse;
		}
		sourceRouteRepo.delete(sourceRouteInfo.getId());
		srHelper.updateCacheAndInMemory(sourceRouteInfo.toDomain());
		return baseResponse;
	}

	public List<DbSourceRouteInfo> findAllBySourceId(int sourceId) {
		return sourceRouteRepo.findBySourceId(sourceId);
	}

	public List<SourceRouteInfo> findAll(SourceRouteInfoFilter filter) {
		List<SourceRouteInfo> dbSourceRouteInfo = BaseModel.toDomainList(super.search(filter, sourceRouteRepo));
		return dbSourceRouteInfo;
	}


	public DbSourceRouteInfo saveWithoutReload(@Valid SourceRouteInfo sourceRouteInfo) {
		if (!sourceRouteInfo.isSrcAndDestAreWild()
				&& StringUtils.equalsIgnoreCase(sourceRouteInfo.getSrc(), sourceRouteInfo.getDest())) {
			throw new CustomGeneralException(SystemError.SOURCE_ROUTE_SAME_SRC_DEST_NA);
		}
		DbSourceRouteInfo sourceRoute = new DbSourceRouteInfo().from(sourceRouteInfo);
		sourceRoute = sourceRouteRepo.saveAndFlush(sourceRoute);
		return sourceRoute;
	}

}
