package com.tgs.services.fms.sources.travelport.sessionless;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSeatMapFaultException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import travelport.www.schema.air_v47_0.Characteristic_type1;
import travelport.www.schema.air_v47_0.Facility_type0;
import travelport.www.schema.air_v47_0.OptionalService_type0;
import travelport.www.schema.air_v47_0.Row_type0;
import travelport.www.schema.air_v47_0.Rows_type0;
import travelport.www.schema.air_v47_0.SearchTraveler_type0;
import travelport.www.schema.air_v47_0.SeatMapReq;
import travelport.www.schema.air_v47_0.SeatMapRsp;
import travelport.www.schema.air_v47_0.TypeBaseAirSegment;
import travelport.www.schema.air_v47_0.TypeSeatAvailability;
import travelport.www.schema.common_v47_0.First_type0;
import travelport.www.schema.common_v47_0.First_type2;
import travelport.www.schema.common_v47_0.HostToken_type0;
import travelport.www.schema.common_v47_0.Last_type0;
import travelport.www.schema.common_v47_0.Name_type0;
import travelport.www.schema.common_v47_0.Name_type4;
import travelport.www.schema.common_v47_0.Prefix_type0;
import travelport.www.service.air_v47_0.AirFaultMessage;
import travelport.www.service.air_v47_0.AirServiceStub;

@Slf4j
@Getter
@Setter
@SuperBuilder
final class TravelPortSessionLessSeatManager extends TravelPortSessionLessServiceManager {

	public SeatMapRsp getSeatMapRsp(SeatMapReq seatMapReq) {
		AirServiceStub airService = bindingService.getAirService();
		listener.setType(AirUtils.getLogType("Seat Map", configuration));
		airService._getServiceClient().getAxisService().addMessageContextListener(listener);
		SeatMapRsp seatMapRsp = null;
		try {
			bindingService.setProxyAndAuthentication(airService, "SeatMap");
			seatMapRsp = airService.service(seatMapReq, getSessionContext(false));
			if (checkAnyErrors(seatMapRsp)) {
				throw new NoSeatAvailableException(String.join(",", criticalMessageLogger));
			}
		} catch (AirFaultMessage airFaultMessage) {
			throw new SupplierSeatMapFaultException(airFaultMessage.getMessage());
		} catch (RemoteException remoteException) {
			throw new SupplierRemoteException(remoteException.getMessage());
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return seatMapRsp;
	}

	protected TripSeatMap getSeatMap(TripInfo tripInfo, SeatMapRsp seatMapRsp) {
		TripSeatMap seatMap = TripSeatMap.builder().build();
		parseSeatMap(seatMapRsp, tripInfo, seatMap);
		return seatMap;
	}


	protected SeatMapReq getSeatMapRequest(TripInfo tripInfo) {
		SeatMapReq req = new SeatMapReq();
		buildBaseCoreRequest(req);
		List<TypeBaseAirSegment> typeBaseAirSegmentList = new ArrayList<>();
		buildAirSegmentList(typeBaseAirSegmentList, tripInfo.getSegmentInfos(), true, false);
		req.setReturnSeatPricing(true);
		req.setAirSegment(typeBaseAirSegmentList.toArray(new TypeBaseAirSegment[0]));
		req.setHostToken(getHostTokenList(tripInfo.getSegmentInfos()).toArray(new HostToken_type0[0]));
		req.setSearchTraveler(getSearchTravellers(tripInfo).toArray(new SearchTraveler_type0[0]));
		return req;
	}

	private List<SearchTraveler_type0> getSearchTravellers(TripInfo tripInfo) {
		List<SearchTraveler_type0> searchTravellers = new ArrayList<>();
		for (Map.Entry<PaxType, List<String>> travellersRefMap : tripInfo.getSegmentInfos().get(0).getPriceInfo(0)
				.getMiscInfo().getBookingTravellerInfoRef().entrySet()) {
			if (!travellersRefMap.getKey().equals(PaxType.INFANT)) {
				for (String key : travellersRefMap.getValue()) {
					SearchTraveler_type0 passenger = getSearchPassenger(key, travellersRefMap.getKey(), "MR",
							RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
					passenger.setAge(new BigInteger(TravelPortSessionLessConstants.ADULT_AGE));
					if (PaxType.CHILD.getType().equals(travellersRefMap.getKey().getType())) {
						passenger.setAge(new BigInteger(TravelPortSessionLessConstants.CHILD_AGE));
					}
					searchTravellers.add(passenger);
				}
			}
		}
		return searchTravellers;
	}

	private void parseSeatMap(SeatMapRsp seatMapRsp, TripInfo tripInfo, TripSeatMap seatMap) {
		Map<String, OptionalService_type0> optionalServiceMap = new HashMap<>();
		String firstAdultKey = null;
		for (SearchTraveler_type0 traveller : seatMapRsp.getSearchTraveler()) {
			if (traveller.getCode().getTypePTC().equals(TravelPortSessionLessUtils.getTypePTC(PaxType.ADULT))) {
				firstAdultKey = traveller.getKey().getTypeRef();
				break;
			}
		}
		Map<String, List<Row_type0>> rowsMap = new HashMap<>();
		for (OptionalService_type0 service : seatMapRsp.getOptionalServices().getOptionalService()) {
			optionalServiceMap.put(service.getKey().getTypeRef(), service);
		}
		for (Rows_type0 rows : seatMapRsp.getRows()) {
			rowsMap.put(rows.getSegmentRef().getTypeRef(), getList(rows.getRow()));
		}
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
				List<Row_type0> rows = rowsMap.get(segmentInfo.getPriceInfo(0).getMiscInfo().getSegmentKey());
				List<SeatSSRInformation> seatInfos = new ArrayList<>();
				for (Row_type0 row : rows) {
					if (StringUtils.equals(firstAdultKey, row.getSearchTravelerRef().getTypeRef())) {
						for (Facility_type0 facility : row.getFacility()) {
							if (StringUtils.isNotEmpty(facility.getSeatCode())) {
								OptionalService_type0 optionalService =
										optionalServiceMap.get(facility.getOptionalServiceRef().getTypeRef());
								SeatSSRInformation seatSSRInformation = new SeatSSRInformation();
								seatSSRInformation.setKey(facility.getSeatCode());
								seatSSRInformation.setSeatNo(StringUtils.replace(facility.getSeatCode(), "-", ""));
								seatSSRInformation.setCode(StringUtils.replace(facility.getSeatCode(), "-", ""));
								seatSSRInformation.setAmount(getAmount(optionalService, optionalServiceMap));
								seatSSRInformation.setIsBooked(isSeatBooked(facility));
								if (CollectionUtils.isNotEmpty(getList(facility.getCharacteristic()))) {
									for (Characteristic_type1 characteristic : facility.getCharacteristic()) {
										if (StringUtils.equalsIgnoreCase(characteristic.getValue(), "AISLE")) {
											seatSSRInformation.setIsAisle(true);
										}
										if (StringUtils.equalsIgnoreCase(characteristic.getValue(), "ExtraLegRoom")) {
											seatSSRInformation.setIsLegroom(true);
										}
									}
								}
								seatSSRInformation.getMiscInfo().setSeatCodeType(
										optionalService.getProviderDefinedType().getProviderDefinedType_type1());
								seatSSRInformation.getMiscInfo().setRemarks(facility.getRemark()[0].getString());
								// seatSSRInformation.getMiscInfo().setSupplierKey(optionalService.getKey().getTypeRef());
								seatSSRInformation.setDesc(StringUtils.replace(facility.getSeatCode(), "-", ""));
								seatInfos.add(seatSSRInformation);
							}
						}
					}
				}
				if (CollectionUtils.isNotEmpty(seatInfos)) {
					SeatInformation seatInformation = SeatInformation.builder().seatsInfo(seatInfos).build();
					seatMap.getTripSeat().put(segmentInfo.getId(), seatInformation);
				}
			}
		}
	}

	private Boolean isSeatBooked(Facility_type0 facility) {
		TypeSeatAvailability availability = facility.getAvailability();
		if (availability.getValue() == TypeSeatAvailability._Available) {
			return false;
		}
		return true;
	}

	private Double getAmount(OptionalService_type0 optionalService,
			Map<String, OptionalService_type0> optionalServiceMap) {
		if (optionalService != null) {
			Double amount = getAmountBasedOnCurrency(optionalService.getTotalPrice().getTypeMoney(),
					optionalService.getTotalPrice().getTypeMoney().substring(0, 3), 1.0);
			return amount;
		}
		return 0.0;
	}

}
