package com.tgs.services.fms.sources.travelport.sessionless;

import java.util.Arrays;
import com.tgs.services.base.enums.FareType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@AllArgsConstructor
@Getter
enum TravelPortSessionLessFareFamily {

	LITE_FARE("Lite Fare", FareType.LITE),
	SME_FARE("SME Fare", FareType.SME),
	FLEXI_PLUS("Flexible Plus", FareType.FLEXI_PLUS),
	REGULAR_FARE("Regular Fare", FareType.PUBLISHED),
	RETURN_FARE("Return Fare", FareType.SPECIAL_RETURN),
	PROMO_FARE("Promo Fare", FareType.PROMO),
	CORPORATE("Corporate Fare", FareType.CORPORATE),
	SALE("Sale Fare", FareType.SALE),
	FLEXIBLE_PLUS("Flexible Plus Fare", FareType.FLEXI_PLUS),
	FAMILY_FARE("Family Fare", FareType.FAMILY);

	private String fareFamily;
	private FareType fareType;

	public static FareType getFareFamily(String fareFamily) {
		return Arrays.asList(TravelPortSessionLessFareFamily.values()).stream().filter(family -> {
			return StringUtils.equalsIgnoreCase(family.getFareFamily(), fareFamily);
		}).findFirst().orElse(REGULAR_FARE).getFareType();
	}

}
