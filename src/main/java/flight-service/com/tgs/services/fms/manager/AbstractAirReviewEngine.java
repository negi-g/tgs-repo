package com.tgs.services.fms.manager;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;

import com.tgs.services.fms.helper.AirlineHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Setter
@Slf4j
public abstract class AbstractAirReviewEngine {

	@Autowired
	private FlightCacheHandler cacheHandler;

	@Autowired
	private AirSearchManager searchManager;

	public TripInfo reviewTrip(TripInfo oldTripInfo, AirSearchQuery searchQuery, boolean isSingleSell,
			ContextData contextData, String bookingId) {

		// In domestic return separate sell pnr credit balance should be divided for onward and return separately.
		if (searchQuery.isDomesticReturn() && !isSingleSell && searchQuery.isPNRCreditSearch()) {
			searchQuery =
					new GsonMapper<AirSearchQuery>(searchQuery, new AirSearchQuery(), AirSearchQuery.class).convert();
			if (searchQuery.getSearchModifiers().getPnrCreditInfo().getCreditBalance() > 0) {
				searchQuery.getSearchModifiers().getPnrCreditInfo()
						.setCreditBalance(searchQuery.getSearchModifiers().getPnrCreditInfo().getCreditBalance() / 2);
			}
		}

		if (BooleanUtils.isTrue(oldTripInfo.getSegmentInfos().get(0).getIsCombinationFirstSegment())) {
			TripInfo newTripInfo =
					reviewCombinationTripSourceWise(oldTripInfo, searchQuery, isSingleSell, contextData, bookingId);
			return newTripInfo;
		}
		return reviewTripSourceWise(oldTripInfo, searchQuery, isSingleSell, contextData, bookingId);
	}

	private TripInfo reviewTripSourceWise(TripInfo oldTripInfo, AirSearchQuery searchQuery, boolean isSingleSell,
			ContextData contextData, String bookingId) {
		TripInfo newTripInfo = null;
		boolean reviewSuccessful = false;
		boolean isFoundFareDiff = false;
		PriceInfo priceInfo = oldTripInfo.getSegmentInfos().get(0).getPriceInfo(0);
		AirSourceType airSourceType = AirSourceType.getAirSourceType(priceInfo.getSupplierBasicInfo().getSourceId());
		SupplierConfiguration supplierConf = SupplierConfigurationHelper.getSupplierConfiguration(
				priceInfo.getSupplierBasicInfo().getSourceId(), priceInfo.getSupplierBasicInfo().getRuleId());
		searchQuery.setPreferredAirline(getPreferredAirline(supplierConf, searchQuery));
		AbstractAirInfoFactory factory = airSourceType.getFactoryInstance(searchQuery, supplierConf);
		try {
			factory.setContextData(contextData);
			newTripInfo = factory.reviewTrip(oldTripInfo, bookingId, 1);
			if (newTripInfo == null) {
				cacheHandler.deleteCache(searchQuery, supplierConf.getBasicInfo(), bookingId);
				searchQuery.setSearchId(bookingId);
				searchQuery.setIsLiveSearch(true);
				searchQuery.setSourceIds(Arrays.asList(priceInfo.getSourceId()));
				log.info("Review failed for booking Id {} , trying search again", bookingId);
				AirSearchResult searchResult = searchManager.doSearch(searchQuery, contextData);
				if (isSingleSell) {
					AirSearchResultProcessingManager.processSearchTypeResult(searchResult, airSourceType.getSourceId(),
							searchQuery, contextData.getUser());
				}
				TripInfo tempTripInfo = AirUtils.filterTripFromAirSearchResult(searchResult, oldTripInfo);
				if (tempTripInfo == null) {
					log.info("After search,no flight found for selected booking Id {}", bookingId);
					throw new NoSeatAvailableException("No seat available for selectedTrip " + oldTripInfo);
				}
				/**
				 * Creating factory again to avoid any previous data to conflict with new request
				 */
				priceInfo = tempTripInfo.getSegmentInfos().get(0).getPriceInfo(0);
				supplierConf = SupplierConfigurationHelper.getSupplierConfiguration(
						priceInfo.getSupplierBasicInfo().getSourceId(), priceInfo.getSupplierBasicInfo().getRuleId());
				factory = airSourceType.getFactoryInstance(searchQuery, supplierConf);
				factory.setContextData(contextData);
				newTripInfo = factory.reviewTrip(tempTripInfo, bookingId, 2);
				if (newTripInfo == null) {
					log.info("After review,no flight found for selected booking Id {}", bookingId);
					throw new NoSeatAvailableException("No seat available for selectedTrip " + oldTripInfo);
				}
			}

			isFoundFareDiff = AirUtils.logTripWiseFareChange(oldTripInfo, newTripInfo, bookingId);
			reviewSuccessful = true;
		} catch (NoSeatAvailableException e) {
			log.info(AirSourceConstants.REVIEW_FAILED, bookingId, oldTripInfo, e);
			throw new CustomGeneralException(SystemError.FLIGHT_SOLD_OUT);
		} catch (Exception e) {
			log.error("Unable to review itinerary for bookingId {}, searchQuery {}", bookingId, searchQuery, e);
			throw new CustomGeneralException(SystemError.FLIGHT_SOLD_OUT);
		} finally {
			if (!reviewSuccessful || isFoundFareDiff) {
				cacheHandler.deleteCache(searchQuery, supplierConf.getBasicInfo(), bookingId);
			}
		}
		return newTripInfo;
	}

	protected TripInfo reviewCombinationTripSourceWise(TripInfo oldTripInfo, AirSearchQuery searchQuery,
			boolean isSingleSell, ContextData contextData, String bookingId) {
		TripInfo newTripInfo = null;
		List<TripInfo> trips = AirUtils.splitTripInfo(oldTripInfo, true);
		for (TripInfo tripInfo : trips) {
			TripInfo edgeTripInfo = reviewTripSourceWise(tripInfo,
					cacheHandler.fetchSearchQueryForCombiSearch(tripInfo), isSingleSell, contextData, bookingId);
			if (newTripInfo == null) {
				newTripInfo = edgeTripInfo;
			} else {
				newTripInfo.getSegmentInfos().addAll(edgeTripInfo.getSegmentInfos());
			}
		}
		// calculate connecting time among different sources
		SegmentInfo previousSegmentInfo = null;
		for (int i = 0; i < newTripInfo.getSegmentInfos().size(); i++) {
			SegmentInfo segmentInfo = newTripInfo.getSegmentInfos().get(i);
			if (BooleanUtils.isTrue(segmentInfo.getIsReturnSegment()) && segmentInfo.getSegmentNum() == 0) {
				previousSegmentInfo = null;
			}
			if ((previousSegmentInfo != null) && (segmentInfo.getSegmentNum() > 0
					|| BooleanUtils.isFalse(segmentInfo.getIsCombinationFirstSegment()))) {
				long connectingTime = AirUtils.getConnectingTime(previousSegmentInfo, segmentInfo);
				newTripInfo.getSegmentInfos().get(i - 1).setConnectingTime(connectingTime);
			}
			previousSegmentInfo = segmentInfo;
		}
		return newTripInfo;
	}

	public TripInfo filterTripInfo(AirSearchResult airSearchResult, TripInfo selectedTrip) {
		return AirUtils.filterTripFromAirSearchResult(airSearchResult, selectedTrip);
	}

	public abstract List<TripInfo> reviewTrips(List<TripInfo> tripInfos, List<AirSearchQuery> searchQueries,
			ContextData contextData, String booking);

	private Set<AirlineInfo> getPreferredAirline(SupplierConfiguration supplierConf, AirSearchQuery searchQuery) {
		List<String> searchForAirline = new ArrayList<>();
		if (StringUtils.isNotEmpty(searchQuery.getPrefferedAirline())) {
			searchForAirline = Arrays.asList(searchQuery.getPrefferedAirline().split(","));
		}

		List<String> preferredAirline = AirUtils.getPreferredAirline(null, supplierConf, searchForAirline);
		Set<AirlineInfo> airlineList = new HashSet<>();
		if (CollectionUtils.isNotEmpty(preferredAirline)) {
			preferredAirline.stream().forEach(airline -> airlineList.add(AirlineHelper.getAirlineInfo(airline)));
		}
		return airlineList;
	}

}
