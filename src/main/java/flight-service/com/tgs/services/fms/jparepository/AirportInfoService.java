package com.tgs.services.fms.jparepository;

import java.util.Arrays;
import java.util.List;
import com.tgs.filters.AirportInfoFilter;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.esstackInitializer.ESStackAirportInfoInitializer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.fms.dbmodel.DbAirportInfo;

@Service
public class AirportInfoService extends SearchService<DbAirportInfo> {

	@Autowired
	AirportInfoRepository airportInfoRepo;

	@Autowired
	AirportHelper airportHelper;

	@Autowired
	ESStackAirportInfoInitializer airportInfoInitializer;

	public List<DbAirportInfo> fetchAirports(@Param("searchQuery") String searchQuery) {
		return airportInfoRepo.fetchAirports(searchQuery);
	}

	public List<DbAirportInfo> fetchByCity(@Param("searchQuery") String searchQuery) {
		return airportInfoRepo.fetchAirportsByCity(searchQuery);
	}

	public List<DbAirportInfo> findAll() {
		return airportInfoRepo.findAll();
	}

	public void addAll(List<DbAirportInfo> airPortInfos) {
		airPortInfos.forEach(airportInfo -> {
			airportInfoRepo.saveAndFlush(airportInfo);
		});
	}

	public DbAirportInfo findById(Long id) {
		return airportInfoRepo.findOne(id);
	}

	public DbAirportInfo save(DbAirportInfo dbAirportInfo) {
		dbAirportInfo.setCode(StringUtils.upperCase(dbAirportInfo.getCode()));
		DbAirportInfo airportInfo = airportInfoRepo.saveAndFlush(dbAirportInfo);
		airportHelper.process();
		airportInfoInitializer.reload(Arrays.asList(dbAirportInfo));
		return airportInfo;
	}

	public DbAirportInfo findByCode(String code) {
		return airportInfoRepo.findByCode(code);
	}

	public void deleteById(DbAirportInfo airportInfo) {
		airportInfoRepo.delete(airportInfo.getId());
		airportHelper.process();
		airportInfoInitializer.delete(airportInfo);
	}

	public List<AirportInfo> search(AirportInfoFilter filter) {
		List<DbAirportInfo> airportList = super.search(filter, airportInfoRepo);
		return BaseModel.toDomainList(airportList);
	}

}
