package com.tgs.services.fms.sources.airasia;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.*;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.FareComponentHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.sources.AirSourceConstants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.tempuri.*;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.KeyValue;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.tempuri.FlightDesignator;

@Slf4j
@Setter
@Getter
@SuperBuilder
abstract class AirAsiaServiceManager {

	protected String sessionSignature;
	protected AirSearchQuery searchQuery;
	protected SupplierConfiguration configuration;
	protected DeliveryInfo deliveryInfo;
	protected String bookingId;
	protected final static String PAX_TYPE_ADULT = "ADT";
	protected final static String PAX_TYPE_CHILD = "CHD";
	protected final static String PAX_TYPE_INFANT = "INFT";
	protected List<FlightTravellerInfo> infants;
	protected List<FlightTravellerInfo> adults;
	protected List<FlightTravellerInfo> child;
	protected int adultCount;
	protected int childCount;
	protected int infantCount;
	protected AirSourceConfigurationOutput sourceConfiguration;
	protected SoapRequestResponseListner listener;
	protected SessionServiceBasicHttpBinding_ISessionServiceStub sessionStub;
	protected BookingServiceBasicHttpBinding_IBookingServiceStub bookingStub;
	protected FareServiceBasicHttpBinding_IFareServiceStub searchStub;
	protected List<String> criticalMessageLogger;
	protected FareDetail infantFare;
	protected ClientGeneralInfo clientInfo;
	protected MoneyExchangeCommunicator moneyExchnageComm;
	protected User bookingUser;

	protected AtomicBoolean isPromoFare;

	protected List<SupplierAnalyticsQuery> supplierAnalyticInfos;

	protected static List<String> GST_TICKET_CODE = Arrays.asList("SST", "CST", "IST");

	protected AirAsiaAirline airAsiaAirline;

	protected static final String SESSION_NOT_FOUND = "session id not found";

	public boolean isStoreLog(AirSearchQuery searchQuery) {
		if (searchQuery.getSearchModifiers() != null
				&& BooleanUtils.isTrue(searchQuery.getSearchModifiers().getStoreSearchLog())) {
			return true;
		}
		return false;
	}

	public void init() {
		setPaxCount();
		airAsiaAirline = AirAsiaAirline.AIRASIA;
		isPromoFare = new AtomicBoolean(false);
	}

	public void setPaxCount() {
		if (Objects.nonNull(searchQuery)) {
			this.adultCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.ADULT);
			this.childCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD);
			this.infantCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT);
		}
	}

	public double getAmountBasedOnCurrency(BigDecimal amount, String fromCurrency) {
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount.doubleValue();
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(AirSourceType.AIRASIA.name().toUpperCase()).build();
		return moneyExchnageComm.getExchangeValue(amount.doubleValue(), filter, true);
	}

	public String getCurrencyCode() {
		String currencyCode = getCurrencyCodeFromAirPortInfo();
		if (StringUtils.isBlank(currencyCode)) {
			if (configuration != null
					&& StringUtils.isNotBlank(configuration.getSupplierCredential().getCurrencyCode())) {
				return configuration.getSupplierCredential().getCurrencyCode();
			}
			clientInfo = ServiceCommunicatorHelper.getClientInfo();
			if (clientInfo != null && StringUtils.isNotBlank(clientInfo.getCurrencyCode())) {
				return clientInfo.getCurrencyCode();
			}
		}
		return currencyCode;
	}

	private String getCurrencyCodeFromAirPortInfo() {
		RouteInfo sourceAirport = airAsiaAirline.getCurrencyCode(searchQuery);
		if (sourceAirport != null) {
			if (CollectionUtils.isNotEmpty(sourceConfiguration.getCurrencyCodes())) {
				List<KeyValue> currencyList = sourceConfiguration.getCurrencyCodes();
				Optional<KeyValue> currencyCode = currencyList.stream()
						.filter(currencyKey -> currencyKey.getKey()
								.equalsIgnoreCase(sourceAirport.getFromCityOrAirport().getCountryCode())
								|| currencyKey.getKey().contains(sourceAirport.getFromCityAirportCode()))
						.findFirst();
				if (currencyCode.isPresent()) {
					return currencyCode.get().getValue();
				}
			}
		}
		return null;
	}

	public ArrayOfPaxPriceType getPaxPriceType(boolean isPaxPrice) {
		ArrayOfPaxPriceType priceType = new ArrayOfPaxPriceType();
		for (int paxCount = 0; paxCount < adultCount; paxCount++) {
			priceType.addPaxPriceType(getPaxPriceType(adultCount, PAX_TYPE_ADULT, isPaxPrice));
		}

		for (int paxCount = 0; paxCount < childCount; paxCount++) {
			if (childCount > 0) {
				priceType.addPaxPriceType(getPaxPriceType(childCount, PAX_TYPE_CHILD, isPaxPrice));
			}
		}
		return priceType;
	}

	private PaxPriceType getPaxPriceType(int paxCount, String paxCode, boolean isPaxPrice) {
		PaxPriceType priceType = new PaxPriceType();
		priceType.setPaxType(paxCode);
		// To do set pax count.
		return priceType;
	}

	public SellJourneyByKeyRequestData getSellJourneyByKeyRequestData(List<TripInfo> tripInfos, boolean isPaxPrice,
			boolean isSearch) {
		SellJourneyByKeyRequestData sellKeyRequest = new SellJourneyByKeyRequestData();
		sellKeyRequest.setCurrencyCode(getCurrencyCode());
		sellKeyRequest.setPaxPriceType(getPaxPriceType(isPaxPrice));
		ArrayOfSellKeyList arrayOfSellKeyList = new ArrayOfSellKeyList();
		for (TripInfo tInfo : tripInfos) {
			// Split Trip info will handle Intl Return to sell as trip journey wise
			AirUtils.splitTripInfo(tInfo, true).forEach(tripInfo -> {
				List<StringJoiner> fareKeys = getTripPriceFareKey(tripInfo, isSearch);
				fareKeys.forEach(fareKey -> {
					SellKeyList sellKeyList = new SellKeyList();
					sellKeyList.setJourneySellKey(getJourneySellKey(tripInfo));
					sellKeyList.setFareSellKey(fareKey.toString());
					arrayOfSellKeyList.addSellKeyList(sellKeyList);
				});
			});
		}

		sellKeyRequest.setJourneySellKeys(arrayOfSellKeyList);
		sellKeyRequest.setPaxCount((short) (adultCount + childCount));
		sellKeyRequest.setLoyaltyFilter(LoyaltyFilter.MonetaryOnly);
		sellKeyRequest.setIsAllotmentMarketFare(false);
		sellKeyRequest.setSourcePOS(getPOS());
		sellKeyRequest.setActionStatusCode("NN");
		return sellKeyRequest;
	}

	public PointOfSale getPOS() {
		PointOfSale pos = new PointOfSale();
		pos.setState(MessageState.New);
		pos.setDomainCode(configuration.getSupplierCredential().getDomain());
		// To do set Agent code.
		// pos.setAgentCode();
		pos.setLocationCode(configuration.getSupplierCredential().getDomain());
		if (StringUtils.isNotBlank(configuration.getSupplierCredential().getOrganisationCode())) {
			pos.setOrganizationCode(configuration.getSupplierCredential().getOrganisationCode());
		}
		return pos;
	}

	private String getJourneySellKey(TripInfo tInfo) {
		StringJoiner joiner = new StringJoiner("^");
		SegmentInfo segmentInfo = tInfo.getSegmentInfos().get(0);
		if (segmentInfo.getMiscInfo() != null
				&& StringUtils.isNotBlank(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey())
				&& (segmentInfo.getSegmentNum() == 0 || segmentInfo.getIsReturnSegment()
						|| BooleanUtils.isTrue(segmentInfo.getIsCombinationFirstSegment()))) {
			joiner.add(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey());
		}
		return joiner.toString();
	}

	private List<StringJoiner> getTripPriceFareKey(TripInfo tInfo, boolean isSearch) {
		List<StringJoiner> joiners = new ArrayList<>();
		if (isSearch) {
			AtomicInteger totalSegmentCount = new AtomicInteger(tInfo.getSegmentInfos().size());
			PriceInfo firstSegmentPriceInfo = tInfo.getSegmentInfos().get(0).getPriceInfo(0);
			StringJoiner joiner = new StringJoiner("^");
			joiner.add(firstSegmentPriceInfo.getMiscInfo().getFareKey());
			if (totalSegmentCount.get() > 1) {
				for (int segmentIndex = 1; segmentIndex < totalSegmentCount.get(); segmentIndex++) {
					SegmentInfo segmentInfo = tInfo.getSegmentInfos().get(segmentIndex);
					PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
					if (priceInfo != null) {
						joiner.add(priceInfo.getMiscInfo().getFareKey());
					}
				}
			}
			joiners.add(joiner);
		} else {
			StringJoiner joiner = new StringJoiner("^");
			for (SegmentInfo segmentInfo : tInfo.getSegmentInfos()) {
				joiner.add(segmentInfo.getPriceInfo(0).getMiscInfo().getFareKey());
			}
			joiners.add(joiner);
		}
		return joiners;
	}

	public boolean isAnyError(BookingUpdateResponseData bookingUpdateResponseData) {
		boolean isError = false;
		StringJoiner message = new StringJoiner("");
		if (bookingUpdateResponseData != null
				&& StringUtils.isNotBlank(bookingUpdateResponseData.getExceptionMessage())) {
			isError = true;
			message.add(bookingUpdateResponseData.getExceptionMessage());
		}
		if (bookingUpdateResponseData != null) {
			if (bookingUpdateResponseData.getError() != null
					&& StringUtils.isNotEmpty(bookingUpdateResponseData.getError().getErrorText())) {
				message.add(bookingUpdateResponseData.getError().getErrorText());
				isError = true;
			}
			if (bookingUpdateResponseData.getWarning() != null
					&& StringUtils.isNotBlank(bookingUpdateResponseData.getWarning().getWarningText())) {
				message.add(bookingUpdateResponseData.getWarning().getWarningText());
			}
		}
		if (criticalMessageLogger != null && StringUtils.isNotBlank(message.toString())) {
			log.info("Sell Failed for BookingId {}", bookingId, message.toString());
			criticalMessageLogger.add(message.toString());
		}
		return isError;
	}

	public double getTotalSellfare(Success success) {
		if (success != null && success.getPNRAmount() != null) {
			return success.getPNRAmount().getTotalCost().doubleValue();
		}
		return 0;
	}

	public SSRAvailabilityForBookingRequest setCommonSSRRequest(SSRAvailabilityForBookingRequest ssrRequest) {
		ssrRequest.setCurrencyCode(getCurrencyCode());
		ssrRequest.setInventoryControlled(true);
		ssrRequest.setNonInventoryControlled(true);
		ssrRequest.setNonSeatDependent(true);
		ssrRequest.setSeatDependent(true);
		short[] passengerNumberList = new short[1];
		passengerNumberList[0] = 0;
		ArrayOfInt16 ofInt = new ArrayOfInt16();
		ofInt.set_short(passengerNumberList);
		ssrRequest.setPassengerNumberList(ofInt);
		return ssrRequest;
	}

	public boolean isAnyPaymentError(AddPaymentToBookingResponse response) {
		boolean isError = false;
		String message = null;
		if (response != null && response.getAddPaymentToBookingResult() != null
				&& StringUtils.isNotBlank(response.getAddPaymentToBookingResult().getExceptionMessage())) {
			isError = true;
			message = response.getAddPaymentToBookingResult().getExceptionMessage();
		}
		if (response != null && response.getAddPaymentToBookingResult() != null
				&& response.getAddPaymentToBookingResult().getValidationPayment() != null && response
						.getAddPaymentToBookingResult().getValidationPayment().getPaymentValidationErrors() != null) {
			PaymentValidationError[] paymentValidationError = response.getAddPaymentToBookingResult()
					.getValidationPayment().getPaymentValidationErrors().getPaymentValidationError();
			if (paymentValidationError != null && paymentValidationError.length > 0) {
				message = StringUtils.join(paymentValidationError[0].getErrorType(),
						paymentValidationError[0].getErrorDescription());
				isError = true;
			}
		}
		if (criticalMessageLogger != null && StringUtils.isNotBlank(message)) {
			log.info("Payment Error Type  {} Description {} for bookingId {}", message, bookingId);
			criticalMessageLogger.add(message);
		}
		return isError;
	}

	public FareDetail setPaxWiseFareBreakUp(FareDetail fareDetail, PaxFare paxFare, int paxCount,
			BookingServiceCharge[] serviceCharge) {
		for (BookingServiceCharge charge : serviceCharge) {
			String chargeType = charge.getChargeType().getValue(); // Charge Type
			AirAsiaCharges airAsiaCharges = AirAsiaCharges.getNavitaireChargeOnChargeType(chargeType);
			String chargeCode = charge.getChargeCode(); // FeeType
			double amount = getAmountBasedOnCurrency(charge.getAmount(), charge.getCurrencyCode());
			String ticketCode = charge.getTicketCode();
			if (amount > 0 && !chargeType.equalsIgnoreCase("IncludedTax")) {
				if (airAsiaCharges != null && StringUtils.isNotBlank(chargeCode)) {
					// Set Amount Based on Sub Type
					airAsiaCharges = AirAsiaCharges.getNavitaireChargeOnFeeType(chargeCode, airAsiaCharges);
				}
				airAsiaCharges.setFareComponent(fareDetail, paxFare.getPaxType(), amount, charge.getCurrencyCode());
			}
			if (airAsiaCharges.equals(AirAsiaCharges.DISCOUNT)
					|| airAsiaCharges.equals(AirAsiaCharges.PROMOTION_DISCOUNT)) {
				isPromoFare.set(true);
			}
			// To do set fare basis.
			// fareDetail.setFareBasis(paxFare.getTicketFareBasisCode());
		}
		return fareDetail;
	}

	public String getSupplierDesc() {
		return configuration.getBasicInfo().getDescription();
	}

	protected String getPromoCode(AirSearchQuery searchQuery) {
		FlightBasicFact fact =
				FlightBasicFact.createFact().generateFact(searchQuery).generateFact(configuration.getBasicInfo());
		BaseUtils.createFactOnUser(fact, bookingUser);
		String promoCode = AirUtils.getSupplierPromotionCode(fact);
		if (StringUtils.isEmpty(promoCode) && configuration != null && configuration.getSupplierAdditionalInfo() != null
				&& CollectionUtils.isNotEmpty(configuration.getSupplierAdditionalInfo().getAccountCodes())) {
			promoCode = configuration.getSupplierAdditionalInfo().getAccountCodes().get(0);
		}
		return promoCode;
	}

	public void setInfantFareToTrip(List<TripInfo> tripInfos, TripInfo reviewedTrip) {
		if (sourceConfiguration != null && BooleanUtils.isTrue(sourceConfiguration.getIsInfantFareFromCache())
				&& searchQuery.getIsDomestic()) {
			FareComponentHelper.storeInfantFare(searchQuery, AirSourceType.AIRASIA.getSourceId(),
					reviewedTrip.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.INFANT));
		}
		for (TripInfo newTripInfo : tripInfos) {
			AtomicInteger segmentNum = new AtomicInteger(0);
			boolean isFlyThru = isFlyThruFLight(newTripInfo.getSegmentInfos());
			newTripInfo.getSegmentInfos().forEach(segmentInfo -> {
				FareDetail infantFareDetail = getInfantFareBasedOnSourceType(reviewedTrip, segmentNum, segmentInfo);
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					FareDetail fareDetail = new FareDetail();
					fareDetail.setFareComponents(new HashMap<>());
					if (MapUtils.isNotEmpty(infantFareDetail.getFareComponents())) {
						if (isFlyThru) {
							if (priceInfo.getMiscInfo().getLegNum() == 0) {
								infantFareDetail.getFareComponents().forEach((fc, amount) -> {
									fareDetail.getFareComponents().put(fc, amount);
								});
							}
						} else if (airAsiaAirline.isInfantFareOnSegmentWise(segmentInfo)) {
							infantFareDetail.getFareComponents().forEach((fc, amount) -> {
								fareDetail.getFareComponents().put(fc, amount);
							});
						}
					}
					fareDetail.setCabinClass(searchQuery.getCabinClass());
					priceInfo.getFareDetail(PaxType.INFANT, new FareDetail())
							.setFareComponents(fareDetail.getFareComponents());
				});
				segmentNum.getAndIncrement();
			});
			AirAsiaUtils.setTotalFareOnTripInfo(Arrays.asList(newTripInfo));
		}
		// AirAsiaUtils.setTotalFareOnTripInfo(tripInfos);
	}

	/**
	 * Infant fare applicable on Journey wise
	 */
	public SegmentInfo setInfantFareToTrip(SegmentInfo segmentInfo, FareDetail oldfareDetail) {
		if (oldfareDetail != null && MapUtils.isNotEmpty(oldfareDetail.getFareComponents())) {
			segmentInfo.getPriceInfoList().forEach(priceInfo -> {
				FareDetail fareDetail = priceInfo.getFareDetail(PaxType.INFANT, new FareDetail());
				if (MapUtils.isEmpty(fareDetail.getFareComponents())) {
					fareDetail.setFareComponents(new HashMap<>());
					oldfareDetail.getFareComponents().forEach((fc, amount) -> {
						fareDetail.getFareComponents().put(fc, amount);
					});
					fareDetail.setCabinClass(searchQuery.getCabinClass());
					priceInfo.getFareDetail(PaxType.INFANT, fareDetail)
							.setFareComponents(fareDetail.getFareComponents());
				}
			});
		}
		return segmentInfo;
	}

	private FareDetail getInfantFareBasedOnSourceType(TripInfo reviewedTrip, AtomicInteger segmentNum,
			SegmentInfo segmentInfo) {
		// Considering infant fare will be same for all the segments.
		// Need to confirm.
		if (airAsiaAirline.isInfantFareOnSegmentWise(segmentInfo)) {
			return reviewedTrip.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.INFANT);
		}
		return infantFare;
	}


	public ArrayOfPassenger getPassengers() {
		ArrayOfPassenger passenger = new ArrayOfPassenger();
		for (int paxCount = 0; paxCount < adultCount; paxCount++) {
			passenger.addPassenger(getPaxInfo(PAX_TYPE_ADULT));
		}

		for (int paxCount = 0; paxCount < childCount; paxCount++) {
			if (childCount > 0) {
				passenger.addPassenger(getPaxInfo(PAX_TYPE_CHILD));
			}
		}
		return passenger;
	}

	public Passenger getPaxInfo(String paxType) {
		Passenger passenger = new Passenger();
		PassengerTypeInfo passengerType = new PassengerTypeInfo();
		passengerType.setState(MessageState.New);
		passengerType.setPaxType(paxType);
		passenger.setPassengerTypeInfo(passengerType);
		return passenger;
	}

	public FlightDesignator getFlightDesignator(SegmentInfo segmentInfo) {
		FlightDesignator designator = new FlightDesignator();
		designator.setCarrierCode(segmentInfo.getAirlineCode(false));
		designator.setFlightNumber(segmentInfo.getFlightNumber());
		return designator;
	}


	public SellFare getSellFare(SegmentInfo segmentInfo, Integer priceIndex) {
		SellFare sellFare = new SellFare();
		sellFare.setState(MessageState.New);
		PriceInfo priceInfo = segmentInfo.getPriceInfo(priceIndex);
		Integer sequence = Integer.valueOf(priceInfo.getMiscInfo().getFareSequence());
		sellFare.setFareSequence(sequence.shortValue());
		sellFare.setProductClass(priceInfo.getBookingClass(PaxType.ADULT));
		sellFare.setRuleNumber(priceInfo.getMiscInfo().getRuleNumber());
		sellFare.setCarrierCode(priceInfo.getPlattingCarrier());
		sellFare.setFareBasisCode(priceInfo.getFareBasis(PaxType.ADULT));
		sellFare.setFareClassOfService(priceInfo.getMiscInfo().getFareClassOfService());
		sellFare.setClassOfService(priceInfo.getMiscInfo().getClassOfService());
		sellFare.setFareApplicationType(getFareApplicationName(priceInfo.getMiscInfo().getFareApplicationName()));
		sellFare.setIsAllotmentMarketFare(false);
		return sellFare;
	}

	protected FareApplicationType getFareApplicationName(String fareApplicationName) {
		FareApplicationType applicationType = null;
		if (FareApplicationType.Governing.getValue().equals(fareApplicationName)) {
			applicationType = FareApplicationType.Governing;
		} else if (FareApplicationType.Sector.getValue().equals(fareApplicationName)) {
			applicationType = FareApplicationType.Sector;
		} else if (FareApplicationType.Route.getValue().equals(fareApplicationName)) {
			applicationType = FareApplicationType.Route;
		}
		return applicationType;
	}

	public boolean isFlyThruFLight(List<SegmentInfo> segmentInfos) {
		boolean isFlyThru = false;
		for (SegmentInfo segment : segmentInfos) {
			if (segment.getPriceInfo(0).getMiscInfo().getLegNum() > 0) {
				isFlyThru = true;
				break;
			}
		}
		return isFlyThru;
	}

	private Set<String> getAirlineCode(List<SegmentInfo> segmentInfos) {
		Set<String> airlineCode = new HashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			airlineCode.add(segmentInfo.getFlightDesignator().getAirlineCode());
		});
		return airlineCode;
	}

	private Set<String> getFlightNumber(List<SegmentInfo> segmentInfos) {
		Set<String> flightNumbers = new HashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getFlightDesignator() != null)
				flightNumbers.add(segmentInfo.getFlightDesignator().getFlightNumber());
		});
		return flightNumbers;
	}

	public void addExceptionToLogger(Exception e) {
		if (criticalMessageLogger != null && e != null && StringUtils.isNotBlank(e.getMessage())) {
			criticalMessageLogger.add(e.getMessage());
		}
	}

	public String getAirline(TripInfo tripInfo) {
		String airline = tripInfo.getAirlineCode();
		if (sourceConfiguration != null && CollectionUtils.isNotEmpty(sourceConfiguration.getIncludedAirlines())) {
			return sourceConfiguration.getIncludedAirlines().stream().findFirst().get();
		}
		return airline;
	}

	protected List<SegmentInfo> getSubLegSegments(List<SegmentInfo> segmentInfos, int segmentIndex) {
		List<SegmentInfo> subLegSegments = new ArrayList<SegmentInfo>();
		for (; segmentIndex < segmentInfos.size(); segmentIndex++) {
			Integer nextSegmentLegNum = AirAsiaUtils.getNextLegNum(segmentInfos, segmentIndex);
			subLegSegments.add(segmentInfos.get(segmentIndex));
			if (nextSegmentLegNum == 0) {
				break;
			}
		}
		return subLegSegments;
	}

	protected boolean isCancelled(Booking bookingResponse) {
		BookingStatus bookingStatus = bookingResponse.getBookingInfo().getBookingStatus();
		return BookingStatus._HoldCanceled.equals(bookingStatus.getValue());
	}

	protected com.tgs.services.fms.datamodel.FlightDesignator getFlightDesignator(Leg leg) {
		com.tgs.services.fms.datamodel.FlightDesignator flightDesignator =
				com.tgs.services.fms.datamodel.FlightDesignator.builder().build();
		flightDesignator.setFlightNumber(leg.getFlightDesignator().getFlightNumber().trim());
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(leg.getFlightDesignator().getCarrierCode()));
		if (leg.getLegInfo() != null) {
			flightDesignator.setEquipType(leg.getLegInfo().getEquipmentType());
		}
		return flightDesignator;
	}

	public FareDetail setPaxWiseFareBreakUpV4(FareDetail fareDetail, PaxFare paxFare, int paxCount,
			BookingServiceCharge[] serviceCharge) {
		for (BookingServiceCharge charge : serviceCharge) {
			String chargeType = charge.getChargeType().getValue();
			AirAsiaCharges airAsiaCharges = AirAsiaCharges.getNavitaireChargeOnChargeType(chargeType);
			String chargeCode = charge.getChargeCode(); // FeeType
			double amount = getAmountBasedOnCurrency(charge.getAmount(), charge.getCurrencyCode());
			if (amount > 0 && !chargeType.equalsIgnoreCase("IncludedTax")) {
				if (airAsiaCharges != null && StringUtils.isNotBlank(chargeCode)) {
					// Set Amount Based on Sub Type
					airAsiaCharges = AirAsiaCharges.getNavitaireChargeOnFeeType(chargeCode, airAsiaCharges);
					airAsiaCharges.setFareComponent(fareDetail, paxFare.getPaxType(), amount, charge.getCurrencyCode());
				} else {
					// Set Directly to Other or Chargetype Fare component
					airAsiaCharges.setFareComponent(fareDetail, paxFare.getPaxType(), amount, charge.getCurrencyCode());
				}
			}
		}
		return fareDetail;
	}

	public Map<PaxType, FareDetail> getDefaultFareDetail(Fare fare, int seatCount) {
		Map<PaxType, FareDetail> fareDetailMap = new HashMap<>();
		searchQuery.getPaxInfo().forEach((paxType, count) -> {
			if (count > 0) {
				FareDetail fareDetail = new FareDetail();
				fareDetail.setFareComponents(new HashMap<>());
				fareDetail.setRefundableType(RefundableType.PARTIAL_REFUNDABLE.getRefundableType());
				fareDetail.setClassOfBooking(fare.getProductClass());
				fareDetail.setSeatRemaining(seatCount);
				fareDetail.setFareBasis(fare.getFareBasisCode());
				fareDetail.setCabinClass(searchQuery.getCabinClass());
				fareDetailMap.put(paxType, fareDetail);
			}
		});
		return fareDetailMap;
	}

	public FareDetail parsePaxWiseFareBreakUp(FareDetail fareDetail, PaxFare paxFare, String paxType, int paxCount,
			BookingServiceCharge[] serviceCharge) {
		if (Objects.nonNull(paxFare) && paxFare.getPaxType().equalsIgnoreCase(paxType)) {
			return setPaxWiseFareBreakUp(fareDetail, paxFare, paxCount, serviceCharge);
		}
		return fareDetail;
	}

	public PaxType getPaxType(String paxCode) {
		PaxType paxType = PaxType.ADULT;
		if (paxCode.equals(PAX_TYPE_CHILD)) {
			paxType = PaxType.CHILD;
		} else if (paxCode.equals(PAX_TYPE_INFANT)) {
			paxType = PaxType.INFANT;
		}
		return paxType;
	}

	protected BookingPointOfSale getSourceBookingPOS() {
		BookingPointOfSale pos = new BookingPointOfSale();
		pos.setState(MessageState.New);
		pos.setOrganizationCode(configuration.getSupplierCredential().getOrganisationCode());
		return pos;
	}
}
