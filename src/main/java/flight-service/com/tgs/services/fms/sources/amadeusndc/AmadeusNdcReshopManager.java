package com.tgs.services.fms.sources.amadeusndc;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import javax.xml.ws.BindingProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.BidiMap;
import org.iata.iata._2015._00._2018_1.orderreshoprq.CarrierType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.CountryType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.DeleteOrderItemType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.IATAPayloadStandardAttributesType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.OrderReshopRQ;
import org.iata.iata._2015._00._2018_1.orderreshoprq.PartyType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.PointofSaleType2;
import org.iata.iata._2015._00._2018_1.orderreshoprq.RecipientType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.RequestType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.ReshopOrderType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.SenderType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.ServiceOrderType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.TravelAgencyType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.TravelAgencyTypeCodeContentType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.TravelAgentType;
import org.iata.iata._2015._00._2018_1.orderreshoprq.UpdateOrderType;
import org.iata.iata._2015._00._2018_1.orderreshoprs.OfferType;
import org.iata.iata._2015._00._2018_1.orderreshoprs.OrderReshopRS;
import org.iata.iata._2015._00._2018_1.orderreshoprs.ResponseType;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.utils.exception.air.CancellationException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class AmadeusNdcReshopManager extends AmadeusNdcServiceManager {

	private String orderItemRefId;

	private String orderItemAIRTypeId;

	private BookingSegments bookingSegments;

	private BidiMap<String, String> bidiPaxMap;


	public void orderReshop() {
		AMASecurityHostedUser securityHostedUser = getSecurityHostedUser();
		AlteaNDC181PT servicePort = service.getAlteaNDC181Port();
		BindingProvider bp = (BindingProvider) servicePort;
		addJAXHandlers(bp, bookingId, AmadeusNdcHeaderHandler._ORDER_RESHOP_);
		OrderReshopRS orderReshopRs = servicePort.ndcOrderReshop(createOrderReshopRq(), null, null, securityHostedUser);

		if (!isAnyError(orderReshopRs)) {
			ResponseType responseType = orderReshopRs.getResponse();
			populateCancellationFee(responseType.getReshopResults().getReshopOffers().getReshopOffer());
		}
	}

	private boolean isAnyError(OrderReshopRS orderReshopRs) {
		if (CollectionUtils.isNotEmpty(orderReshopRs.getErrors())) {
			StringJoiner errorMessage = new StringJoiner("");
			for (org.iata.iata._2015._00._2018_1.orderreshoprs.ErrorType error : orderReshopRs.getErrors()) {
				errorMessage.add(error.getCode() + ": " + error.getDescText());
			}
			throw new CancellationException(errorMessage.toString());
		}
		return false;
	}

	private void populateCancellationFee(List<OfferType> reshopOffers) {
		Map<String, OfferType> paxCancellationFeeMap = new HashMap<>();
		for (OfferType reshopOffer : reshopOffers) {
			// pax ref = P2, in orderViewRs is also converted to P2 from PAX2
			String paxReference = Arrays.asList(reshopOffer.getOfferID().split("-")).get(1);
			paxCancellationFeeMap.put(paxReference, reshopOffer);
		}
		matchPaxAndSetCancellationFee(paxCancellationFeeMap);
	}

	private void matchPaxAndSetCancellationFee(Map<String, OfferType> paxCancellationFeeMap) {
		List<SegmentInfo> cancelSegments = bookingSegments.getCancelSegments();
		SegmentInfo cancelSegment = cancelSegments.stream()
				.filter(Segment -> Segment.getSegmentNum().equals(0)
						&& org.apache.commons.lang.BooleanUtils.isNotTrue(Segment.getIsReturnSegment()))
				.collect(Collectors.toList()).get(0);
		List<FlightTravellerInfo> travellerInfos = cancelSegment.getTravellerInfo();

		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			String firstAndLastName = travellerInfo.getFirstName() + "-" + travellerInfo.getLastName();
			String reshopRefId = bidiPaxMap.getKey(firstAndLastName);
			OfferType reshopOffer = paxCancellationFeeMap.get(reshopRefId);
			setCancellationCharges(travellerInfo, reshopOffer);
		}
	}

	private void setCancellationCharges(FlightTravellerInfo travellerInfo, OfferType reshopOffer) {
		Map<FareComponent, Double> fareComponent = travellerInfo.getFareDetail().getFareComponents();
		if (!travellerInfo.getPaxType().equals(com.tgs.services.base.enums.PaxType.INFANT)) {
			Double cancellationCharges = getAmountBasedOnPreferredCurrencyBooking(
					reshopOffer.getDeleteOrderItem().get(0).getPenaltyDifferential().getAmount().getValue(),
					getCurrencyCode(), AirSourceConstants.getClientCurrencyCode());
			fareComponent.put(FareComponent.ACF, cancellationCharges);
		}
	}

	private OrderReshopRQ createOrderReshopRq() {
		OrderReshopRQ orderReshopRq = new OrderReshopRQ();
		orderReshopRq.setPayloadAttributes(getPayloadAttributes());
		orderReshopRq.setPointOfSale(getPointOfSale());
		orderReshopRq.setParty(getPartyType());
		orderReshopRq.setRequest(getRequestType());
		return orderReshopRq;
	}

	private PointofSaleType2 getPointOfSale() {
		PointofSaleType2 pointofSale = new PointofSaleType2();
		CountryType countryType = new CountryType();
		countryType.setCountryCode(AmadeusNDCConstants.COUNTRY_CODE);
		pointofSale.setCountry(countryType);
		return pointofSale;
	}

	private RequestType getRequestType() {
		RequestType requestType = new RequestType();
		requestType.setOrderItemRefID(orderItemRefId);
		requestType.setUpdateOrder(setUpdateOrderType());
		return requestType;
	}

	private UpdateOrderType setUpdateOrderType() {
		UpdateOrderType updateOrderType = new UpdateOrderType();
		ReshopOrderType reshopOrderType = new ReshopOrderType();
		reshopOrderType.setServiceOrder(setServiceOrder());
		updateOrderType.setReshopOrder(reshopOrderType);
		return updateOrderType;
	}

	private ServiceOrderType setServiceOrder() {
		ServiceOrderType serviceOrderType = new ServiceOrderType();
		DeleteOrderItemType deleteOrderItemType = new DeleteOrderItemType();
		deleteOrderItemType.setOrderItemRefID(orderItemAIRTypeId);
		serviceOrderType.getDeleteOrderItem().add(deleteOrderItemType);
		return serviceOrderType;
	}

	private IATAPayloadStandardAttributesType getPayloadAttributes() {
		IATAPayloadStandardAttributesType payloadAttributeType = new IATAPayloadStandardAttributesType();
		payloadAttributeType.setVersion(AmadeusNDCConstants.SERVICE_VERSION);
		return payloadAttributeType;
	}

	private PartyType getPartyType() {
		PartyType partyType = new PartyType();
		partyType.setSender(getSenderType());

		RecipientType recipientType = new RecipientType();
		CarrierType carrierType = new CarrierType();
		carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
		recipientType.setORA(carrierType);
		partyType.setRecipient(recipientType);
		return partyType;
	}

	private SenderType getSenderType() {
		SenderType senderType = new SenderType();
		senderType.setTravelAgency(getTravelAgency());
		return senderType;
	}

	private TravelAgencyType getTravelAgency() {
		TravelAgencyType travelAgencyType = new TravelAgencyType();
		travelAgencyType.setIATANumber(new BigDecimal(supplierConf.getSupplierCredential().getIataNumber()));
		travelAgencyType.setAgencyID(supplierConf.getSupplierCredential().getIataNumber());
		travelAgencyType.setName(supplierConf.getSupplierCredential().getAgentUserName());
		travelAgencyType.setTypeCode(TravelAgencyTypeCodeContentType.ONLINE_TRAVEL_AGENCY);
		return travelAgencyType;
	}
}
