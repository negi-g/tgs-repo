package com.tgs.services.fms.sources.otaradixx;

import java.rmi.RemoteException;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.CancelPNRActionTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.Airline;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.AirlinePerson;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.Charge;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.Customer;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.LogicalFlight;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.PhysicalFlight;
import org.springframework.util.ObjectUtils;
import org.tempuri.CancelPNR;
import org.tempuri.CancelPNRResponse;
import org.tempuri.ConnectPoint_ReservationStub;
import org.tempuri.RetrievePNRResponse;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.utils.AirCancelUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
final class RadixxAirCancellationManager extends RadixxServiceManager {

	public boolean cancelPNR(String pnr) {
		boolean isCancelSuccess = false;
		ConnectPoint_ReservationStub stub = null;
		CancelPNRResponse cancelResponse = null;
		try {
			stub = bindingService.getBookReservationStub(configuration);
			cancelResponse = stub.cancelPNR(buildCancelRequest(pnr));
			if (isCriticalException(cancelResponse.getCancelPNRResult().getExceptions())) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			} else {
				isCancelSuccess = true;
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		}
		return isCancelSuccess;
	}

	public CancelPNR buildCancelRequest(String pnr) {
		CancelPNR cancelPNR = new CancelPNR();
		org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.CancelPNR cancelPNRRq =
				new org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.CancelPNR();
		cancelPNRRq.setSecurityGUID(binaryToken);
		cancelPNRRq.setCarrierCodes(getCarrierCode());
		cancelPNRRq.setActionType(CancelPNRActionTypes.CancelReservation);
		cancelPNRRq.setCarrierCodes(getCarrierCode());
		cancelPNRRq.setReservationInfo(RadixxUtils.getReservationInfo(pnr));
		cancelPNR.setCancelPnrRequest(cancelPNRRq);
		return cancelPNR;
	}

	protected void setAirlineCancellationFee(RetrievePNRResponse retrieveResponse, BookingSegments bookingSegments,
			boolean isConfirm) {
		Airline[] airlines = retrieveResponse.getRetrievePNRResult().getAirlines().getAirline();
		for (Airline airline : airlines) {
			LogicalFlight[] logicalFlights = airline.getLogicalFlight().getLogicalFlight();
			for (LogicalFlight logicalFlight : logicalFlights) {
				PhysicalFlight[] physicalFlights = logicalFlight.getPhysicalFlights().getPhysicalFlight();
				for (PhysicalFlight physicalFlight : physicalFlights) {
					SegmentInfo segmentInfo =
							RadixxUtils.getSegmentInfo(physicalFlight, bookingSegments.getCancelSegments());
					if (!ObjectUtils.isEmpty(segmentInfo))
						buildFareDetail(physicalFlight.getCustomers().getCustomer(), segmentInfo.getTravellerInfo(),
								isConfirm);
				}
			}
		}
	}

	protected void buildFareDetail(Customer[] customer, List<FlightTravellerInfo> travellerInfo, boolean isConfirm) {
		for (FlightTravellerInfo traveller : travellerInfo) {
			if (AirCancelUtils.isTravellerAllowed(traveller)) {
				if (ArrayUtils.isNotEmpty(customer)) {
					for (Customer personList : customer) {
						if (!ObjectUtils.isEmpty(personList.getAirlinePersons())
								&& ArrayUtils.isNotEmpty(personList.getAirlinePersons().getAirlinePerson())) {
							AirlinePerson[] airlinePerson = personList.getAirlinePersons().getAirlinePerson();
							for (AirlinePerson person : airlinePerson) {
								if (person.getFirstName().equals(traveller.getFirstName())
										&& person.getLastName().equals(traveller.getLastName())
										&& traveller.getDob().toString()
												.equals(TgsDateUtils.calenderToLocalDate(person.getDOB()).toString())) {
									setTravellerCancelFee(person.getCharges().getCharge(), traveller.getFareDetail());
								}
							}
						}
					}
				}
//				if (isConfirm)
//					traveller.setStatus(TravellerStatus.CANCELLED);
			}
		}
	}

	protected void setTravellerCancelFee(Charge[] charge, FareDetail fareDetail) {
		double initialAmount = RadixxUtils.getInitialAmount(fareDetail, FareComponent.ACF);
		double amount = initialAmount;
		if (ArrayUtils.isNotEmpty(charge)) {
			for (Charge amountPaid : charge) {
				if (!amountPaid.getTaxIsRefundable() && amountPaid.getAmount().doubleValue() > 0) {
					amount = amount + getAmountBasedOnCurrency(amountPaid.getAmount(), amountPaid.getCurrencyCode());
				}
			}
		}
		if (amount > 0.0)
			fareDetail.getFareComponents().put(FareComponent.ACF, amount);
	}
}
