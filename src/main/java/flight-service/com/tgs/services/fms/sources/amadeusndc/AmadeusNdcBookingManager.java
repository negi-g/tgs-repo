package com.tgs.services.fms.sources.amadeusndc;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import javax.xml.ws.BindingProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.iata.iata._2015._00._2018_1.airshoppingrq.TravelAgencyTypeCodeContentType;
import org.iata.iata._2015._00._2018_1.orderchangerq.AmountType;
import org.iata.iata._2015._00._2018_1.orderchangerq.CashType;
import org.iata.iata._2015._00._2018_1.orderchangerq.OrderChangeRQ;
import org.iata.iata._2015._00._2018_1.orderchangerq.OrderType;
import org.iata.iata._2015._00._2018_1.orderchangerq.PaymentCardType;
import org.iata.iata._2015._00._2018_1.orderchangerq.PaymentInfoType;
import org.iata.iata._2015._00._2018_1.orderchangerq.PaymentMethodType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.AgencyTypeContentType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.AggregatorType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.CarrierType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.ContactInfoListType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.ContactInfoType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.CountryCodeType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.CountryDialingCodeType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.CountryType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.CreateOrderType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.DataListsType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.EmailAddressType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.FreeFormInstructionsType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.IATAPayloadStandardAttributesType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.IdentityDocType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.IdentityDocTypeCodeType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.IndividualType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.ItemIDType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.ListOfOfferInstructionsType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.ListOfOfferInstructionsType.Instruction;
import org.iata.iata._2015._00._2018_1.ordercreaterq.LoyaltyProgramAccountType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.OrderCreateRQ;
import org.iata.iata._2015._00._2018_1.ordercreaterq.OrderItemAssociationType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.OrderItemAssociationType.Flight;
import org.iata.iata._2015._00._2018_1.ordercreaterq.OrderItemAssociationType.Passengers;
import org.iata.iata._2015._00._2018_1.ordercreaterq.OrderOfferItemType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.OrderOfferItemType.OfferItemType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.OtherAddressType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.ParticipantType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.PartyType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.PaxListType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.PhoneType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.PointofSaleType2;
import org.iata.iata._2015._00._2018_1.ordercreaterq.PostalAddressType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.ProperNameType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.RecipientType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.RequestType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.SeatItem;
import org.iata.iata._2015._00._2018_1.ordercreaterq.SeatLocationType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.SeatLocationType.Row;
import org.iata.iata._2015._00._2018_1.ordercreaterq.SeatMapRowNbrType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.SegmentReferences;
import org.iata.iata._2015._00._2018_1.ordercreaterq.SelectedBundleServicesType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.SelectedOfferItemType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.SelectedOfferType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.SenderType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.ServiceDefinitionListType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.ServiceDefinitionType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.SupplementNameType;
import org.iata.iata._2015._00._2018_1.ordercreaterq.TravelAgencyType;
import org.iata.iata._2015._00._2018_1.orderviewrs.DateTimeType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderViewRS;
import org.iata.iata._2015._00._2018_1.orderviewrs.ResponseType;
import org.iata.iata._2015._00._2018_1.orderviewrs.ResponseType.TicketDocInfos.TicketDocInfo;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@SuperBuilder
public class AmadeusNdcBookingManager extends AmadeusNdcServiceManager {

	private GstInfo gstInfo;

	private CreditCardInfo creditCard;

	private Calendar ticketingTimelimit;

	private String supplierBookingId;

	public boolean createOrder(boolean isHoldBooking) {
		boolean isSuccess = false;
		AlteaNDC181PT servicePort = service.getAlteaNDC181Port();
		BindingProvider bp = (BindingProvider) servicePort;
		addJAXHandlers(bp, bookingId, AmadeusNdcHeaderHandler._ORDER_CREATE_);
		OrderViewRS orderviewResponse =
				servicePort.ndcOrderCreate(createOrderReq(), null, null, getSecurityHostedUser());
		if (!isAnyError(orderviewResponse)) {
			updatePNR(orderviewResponse);
			if (isHoldBooking) {
				setTicketingTimeLimit(orderviewResponse);
			}
			isSuccess = true;
		}
		return isSuccess;
	}

	private void updatePNR(OrderViewRS orderviewResponse) {
		ResponseType responseType = orderviewResponse.getResponse();
		if (CollectionUtils.isNotEmpty(responseType.getOrder())) {
			if (StringUtils.isNotBlank(responseType.getOrder().get(0).getOrderID())) {
				supplierBookingId = responseType.getOrder().get(0).getOrderID();
			}
			if (CollectionUtils.isNotEmpty(responseType.getOrder().get(0).getBookingRef())
					&& StringUtils.isNotBlank(responseType.getOrder().get(0).getBookingRef().get(0).getBookingID())) {
				setAirlinePNR(responseType.getOrder().get(0).getBookingRef().get(0).getBookingID());
			}
		}
	}

	private void setTicketingTimeLimit(OrderViewRS orderviewResponse) {
		DateTimeType timeLimitDateTime =
				orderviewResponse.getResponse().getOrder().get(0).getPaymentTimeLimitDateTime();
		if (timeLimitDateTime != null && timeLimitDateTime.getValue() != null) {
			ticketingTimelimit = timeLimitDateTime.getValue().toGregorianCalendar();
		}
	}

	public OrderCreateRQ createOrderReq() {
		OrderCreateRQ orderCreateRQ = new OrderCreateRQ();
		RequestType requestType = new RequestType();

		setCreateOrder(requestType);
		setContactInfo(requestType);
		setInstructionsList(requestType);
		setPaxList(requestType);
		setServiceDefinitionList(requestType);

		orderCreateRQ.setRequest(requestType);
		orderCreateRQ.setParty(getPartyType());
		orderCreateRQ.setPointOfSale(getPointOfSale());
		orderCreateRQ.setPayloadAttributes(getPayloadAttributesType());
		return orderCreateRQ;
	}

	private void setServiceDefinitionList(RequestType requestType) {
		ServiceDefinitionListType serviceDefinitionListType = new ServiceDefinitionListType();
		for (SegmentInfo segment : bookingSegmentInfos) {
			int paxId = 1;
			for (FlightTravellerInfo traveller : segment.getBookingRelatedInfo().getTravellerInfo()) {
				String paxRefId = "PAX" + paxId;
				if (segment.getSegmentNum() == 0 && traveller.getSsrBaggageInfo() != null) {
					SSRInformation baggageInfo = traveller.getSsrBaggageInfo();
					String[] paxSsrKey = baggageInfo.getMiscInfo().getPaxSsrKeys().get(paxRefId).split("/");
					String serviceDefinitionId = paxSsrKey[0];
					ServiceDefinitionType serviceDefinition = new ServiceDefinitionType();
					serviceDefinition.setServiceDefinitionID(serviceDefinitionId);
					serviceDefinition.setName(baggageInfo.getDesc());
					serviceDefinitionListType.getServiceDefinition().add(serviceDefinition);

					SelectedOfferItemType selectedOfferItem = new SelectedOfferItemType();
					selectedOfferItem.setOfferItemID(paxSsrKey[1]);
					selectedOfferItem.getPaxRefID().add(paxRefId);
					SelectedBundleServicesType bundleServiceType = new SelectedBundleServicesType();
					bundleServiceType.setSelectedServiceID(serviceDefinitionId);
					selectedOfferItem.setSelectedBundleServices(bundleServiceType);
					requestType.getCreateOrder().get(0).getSelectedOffer().get(0).getSelectedOfferItem()
							.add(selectedOfferItem);
				}

				if (traveller.getSsrMealInfo() != null) {
					SSRInformation mealInfo = traveller.getSsrMealInfo();
					String[] paxSsrKey = mealInfo.getMiscInfo().getPaxSsrKeys().get(paxRefId).split("/");
					String serviceDefinitionId = paxSsrKey[0];
//					ServiceDefinitionType serviceDefinition = new ServiceDefinitionType();
//					serviceDefinition.setServiceDefinitionID(serviceDefinitionId);
//					serviceDefinition.setName(mealInfo.getDesc());
//					serviceDefinitionListType.getServiceDefinition().add(serviceDefinition);

					SelectedOfferItemType selectedOfferItem = new SelectedOfferItemType();
					selectedOfferItem.setOfferItemID(paxSsrKey[1]);
					selectedOfferItem.getPaxRefID().add(paxRefId);
					// SelectedBundleServicesType bundleServiceType = new SelectedBundleServicesType();
					// bundleServiceType.setSelectedServiceID(serviceDefinitionId);
					// selectedOfferItem.setSelectedBundleServices(bundleServiceType);
					requestType.getCreateOrder().get(0).getSelectedOffer().get(0).getSelectedOfferItem()
							.add(selectedOfferItem);
				}

				if (traveller.getSsrSeatInfo() != null) {
					SSRInformation seatInfo = traveller.getSsrSeatInfo();
					OrderOfferItemType createdOrderItem = new OrderOfferItemType();

					String[] ssrKeys = seatInfo.getMiscInfo().getPaxSsrKeys().get(paxRefId).split("/");

					ItemIDType itemIdType = new ItemIDType();
					itemIdType.setOwner("SQ");
					itemIdType.setValue(ssrKeys[1]);
					createdOrderItem.setOfferItemID(itemIdType);

					OfferItemType offerItemType = new OfferItemType();
					SeatItem seatItem = new SeatItem();

					SeatLocationType locationType = new SeatLocationType();

					char column = seatInfo.getCode().charAt(seatInfo.getCode().length() - 1);
					locationType.setColumn(String.valueOf(column));

					Row rowType = new Row();

					SeatMapRowNbrType numberType = new SeatMapRowNbrType();
					String row = seatInfo.getCode().substring(0, seatInfo.getCode().length() - 1);
					numberType.setValue(row);

					rowType.setNumber(numberType);
					locationType.setRow(rowType);

					OrderItemAssociationType associationType = new OrderItemAssociationType();
					Passengers passenger = new Passengers();
					passenger.setPassengerReferences(paxRefId);
					associationType.getPassengers().add(passenger);

					Flight flight = new Flight();

					SegmentReferences segmentReferences = new SegmentReferences();
					segmentReferences.setValue(ssrKeys[0]);
					flight.getOriginDestinationReferencesOrSegmentReferences().add(segmentReferences);

					associationType.setFlight(flight);
					locationType.setAssociations(associationType);

					seatItem.setLocation(locationType);
					offerItemType.getSeatItem().add(seatItem);
					createdOrderItem.setOfferItemType(offerItemType);

					requestType.getCreateOrder().get(0).getCreateOrderItem().add(createdOrderItem);
				}

				paxId++;
			}
		}
		if(CollectionUtils.isNotEmpty(serviceDefinitionListType.getServiceDefinition()))
		{
			requestType.getDataLists().setServiceDefinitionList(serviceDefinitionListType);
		}
		
	}

	private void setCreateOrder(RequestType requestType) {
		CreateOrderType createOrderType = new CreateOrderType();
		SelectedOfferType selectedOfferType = new SelectedOfferType();
		PriceMiscInfo priceMiscInfo = bookingSegmentInfos.get(0).getPriceInfo(0).getMiscInfo();
		selectedOfferType.setOfferID(priceMiscInfo.getFareKey());
		selectedOfferType.setOwnerCode(bookingSegmentInfos.get(0).getPriceInfo(0).getPlattingCarrier());
		selectedOfferType.setShoppingResponseRefID(priceMiscInfo.getJourneyKey());

		SelectedOfferItemType selectedOfferItem = new SelectedOfferItemType();
		selectedOfferItem.setOfferItemID(priceMiscInfo.getFareLevel());
		int nonInfantTravelers =
				AirUtils.getPaxCountFromTravellerInfo(bookingSegmentInfos.get(0).getTravellerInfo(), false);
		for (int id = 1; id <= nonInfantTravelers; id++) {
			selectedOfferItem.getPaxRefID().add("PAX" + id);
		}
		int infantTravellers = bookingSegmentInfos.get(0).getTravellerInfo().size() - nonInfantTravelers;
		for (int id = 1; id <= infantTravellers; id++) {
			selectedOfferItem.getPaxRefID().add("PAX" + id + "1");
		}
		selectedOfferType.getSelectedOfferItem().add(selectedOfferItem);
		createOrderType.getSelectedOffer().add(selectedOfferType);
		requestType.getCreateOrder().add(createOrderType);
	}

	private void setPaxList(RequestType requestType) {
		List<FlightTravellerInfo> nonInfantTravellers =
				AirUtils.getParticularPaxTravellerInfo(bookingSegmentInfos.get(0).getTravellerInfo(), PaxType.ADULT);
		nonInfantTravellers.addAll(
				AirUtils.getParticularPaxTravellerInfo(bookingSegmentInfos.get(0).getTravellerInfo(), PaxType.CHILD));
		createPaxList(requestType, nonInfantTravellers);
		List<FlightTravellerInfo> infantTravellers =
				AirUtils.getParticularPaxTravellerInfo(bookingSegmentInfos.get(0).getTravellerInfo(), PaxType.INFANT);
		createPaxList(requestType, infantTravellers);
	}

	private void createPaxList(RequestType requestType, List<FlightTravellerInfo> travellers) {
		if (CollectionUtils.isNotEmpty(travellers)) {
			PaxListType paxListType =
					requestType.getDataLists().getPaxList() != null ? requestType.getDataLists().getPaxList()
							: new PaxListType();
			for (int paxIndex = 0; paxIndex < travellers.size(); paxIndex++) {
				PaxType paxType = travellers.get(paxIndex).getPaxType();
				FlightTravellerInfo traveller = travellers.get(paxIndex);
				IndividualType individual = new IndividualType();
				org.iata.iata._2015._00._2018_1.ordercreaterq.PaxType pax =
						new org.iata.iata._2015._00._2018_1.ordercreaterq.PaxType();

				ProperNameType firstName = new ProperNameType();
				firstName.setValue(traveller.getFirstName());
				individual.getGivenName().add(firstName);

				ProperNameType lastName = new ProperNameType();
				lastName.setValue(traveller.getLastName());
				individual.setSurname(lastName);

				individual.setBirthdate(TgsDateUtils.toXMLGregorianCalendar(traveller.getDob()));

				String paxIdString = "";
				int paxId = paxIndex + 1;
				if (PaxType.INFANT.equals(paxType)) {
					paxIdString = "PAX" + paxId + "1";
					pax.setPaxID(paxIdString);
					pax.setPTC(AmadeusNdcPaxType.INF.name());
					pax.setPaxRefID("PAX" + paxId);
				} else if (PaxType.CHILD.equals(paxType)) {
					paxIdString = "PAX" + paxId;
					pax.setPaxID(paxIdString);
					pax.setPTC(AmadeusNdcPaxType.CHD.name());
				} else {
					paxIdString = "PAX" + paxId;
					pax.setPaxID(paxIdString);
					pax.setPTC(AmadeusNdcPaxType.ADT.name());
					SupplementNameType title = new SupplementNameType();
					title.setValue(traveller.getTitle());
					individual.setTitleName(title);
				}
				individual.setIndividualID(paxIdString);
				pax.setIndividual(individual);
				updatePaxPassport(traveller, pax);
				updateFrequentFlyerInfo(traveller, pax);
				paxListType.getPax().add(pax);
				requestType.getDataLists().setPaxList(paxListType);
			}
		}
	}

	private void updatePaxPassport(FlightTravellerInfo traveller,
			org.iata.iata._2015._00._2018_1.ordercreaterq.PaxType pax) {
		if (StringUtils.isNotBlank(traveller.getPassportNumber())) {
			IdentityDocType identityDoc = new IdentityDocType();
			IdentityDocTypeCodeType docTypeCode = new IdentityDocTypeCodeType();
			docTypeCode.setValue("PT");
			identityDoc.setIdentityDocTypeCode(docTypeCode);
			identityDoc.setIdentityDocID(traveller.getPassportNumber());
			CountryCodeType residenceCountry = new CountryCodeType();
			residenceCountry.setValue(traveller.getPassportNationality());
			identityDoc.setResidenceCountryCode(residenceCountry);
			identityDoc.setIssueDate(TgsDateUtils.toXMLGregorianCalendar(traveller.getPassportIssueDate()));
			identityDoc.setExpiryDate(TgsDateUtils.toXMLGregorianCalendar(traveller.getExpiryDate()));
			// set birth date
			identityDoc.setIssuingCountryCode(residenceCountry);
			pax.getIdentityDoc().add(identityDoc);
		}
	}

	private void updateFrequentFlyerInfo(FlightTravellerInfo traveller,
			org.iata.iata._2015._00._2018_1.ordercreaterq.PaxType pax) {
		String carrier = bookingSegmentInfos.get(0).getPriceInfo(0).getPlattingCarrier();
		if (MapUtils.isNotEmpty(traveller.getFrequentFlierMap())
				&& traveller.getFrequentFlierMap().get(carrier) != null) {
			LoyaltyProgramAccountType loyaltyProgramAccountType = new LoyaltyProgramAccountType();
			CarrierType carrierType = new CarrierType();
			carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
			loyaltyProgramAccountType.setCarrier(carrierType);
			loyaltyProgramAccountType.setAccountNumber(traveller.getFrequentFlierMap().get(carrier));
			pax.getLoyaltyProgramAccount().add(loyaltyProgramAccountType);
		}
	}

	private void setContactInfo(RequestType requestType) {
		DataListsType dataListType = new DataListsType();
		ContactInfoListType contactListType = new ContactInfoListType();

		PhoneType phoneType = new PhoneType();
		phoneType.setLabelText("MOBILE");
		CountryDialingCodeType countryDialingCodeType = new CountryDialingCodeType();
		countryDialingCodeType.setValue("91");
		phoneType.setCountryDialingCode(countryDialingCodeType);
		phoneType.setPhoneNumber(new BigDecimal(AirSupplierUtils.getContactNumber(order.getDeliveryInfo())));

		EmailAddressType emailAddressType = new EmailAddressType();
		emailAddressType.setEmailAddressText(AirSupplierUtils.getEmailId(order.getDeliveryInfo()));

		for (int travellerIndex = 0; travellerIndex < bookingSegmentInfos.get(0).getTravellerInfo()
				.size(); travellerIndex++) {
			FlightTravellerInfo traveller = bookingSegmentInfos.get(0).getTravellerInfo().get(travellerIndex);

			if (!PaxType.INFANT.equals(traveller.getPaxType())) {
				ContactInfoType contactInfoType = new ContactInfoType();
				String individualRefID = "PAX" + (travellerIndex + 1);
				contactInfoType.setIndividualRefID(individualRefID);
				contactInfoType.getPhone().add(phoneType);

				contactInfoType.getEmailAddress().add(emailAddressType);
				contactListType.getContactInfo().add(contactInfoType);
			}
		}

		dataListType.setContactInfoList(contactListType);
		requestType.setDataLists(dataListType);
		updateGSTInfo(requestType);
	}

	private void updateGSTInfo(RequestType requestType) {
		if (gstInfo != null && StringUtils.isNotBlank(gstInfo.getGstNumber())) {
			ContactInfoType contactInfoType = new ContactInfoType();
			contactInfoType.setContactTypeText("GST");

			OtherAddressType gstIN = new OtherAddressType();
			gstIN.setLabelText("GSTIN");
			gstIN.setOtherAddressText(gstInfo.getGstNumber());
			contactInfoType.getOtherAddress().add(gstIN);

			OtherAddressType company = new OtherAddressType();
			company.setLabelText("COMPANY");
			company.setOtherAddressText(AmadeusNdcUtils.replaceSpecialCharacter(gstInfo.getRegisteredName()));
			contactInfoType.getOtherAddress().add(company);

			EmailAddressType emailAddressType = new EmailAddressType();
			emailAddressType.setEmailAddressText(gstInfo.getEmail());
			contactInfoType.getEmailAddress().add(emailAddressType);

			PhoneType phoneType = new PhoneType();
			CountryDialingCodeType countryDialingCode = new CountryDialingCodeType();
			countryDialingCode.setValue("91");
			phoneType.setCountryDialingCode(countryDialingCode);
			phoneType.setPhoneNumber(new BigDecimal(gstInfo.getMobile()));
			phoneType.setLabelText("MOBILE");
			contactInfoType.getPhone().add(phoneType);

			PostalAddressType postalAddressType = new PostalAddressType();
			postalAddressType.getStreetText().add(AmadeusNdcUtils.replaceSpecialCharacter(gstInfo.getAddress()));
			postalAddressType.setPostalCode(AmadeusNDCConstants.DEFAULT_POSTAL_CODE);
			CountryCodeType countryCodeType = new CountryCodeType();
			countryCodeType.setValue(AmadeusNDCConstants.COUNTRY_CODE);
			postalAddressType.setCountryCode(countryCodeType);
			contactInfoType.getPostalAddress().add(postalAddressType);

			requestType.getDataLists().getContactInfoList().getContactInfo().add(contactInfoType);
		}
	}

	private void setInstructionsList(RequestType requestType) {
		ListOfOfferInstructionsType listOfOfferInstructionsType = new ListOfOfferInstructionsType();
		Instruction ctcm = new Instruction();
		ctcm.setListKey("CTCM");
		FreeFormInstructionsType ffti = new FreeFormInstructionsType();
		ffti.setRefs(StringUtils.join("CTCE/"));
		ctcm.setFreeFormTextInstruction(ffti);

		Instruction ctce = new Instruction();
		ctce.setListKey("CTCE");
		// requestType.getDataLists().getInstructionsList().getInstruction().add(ctce);

		listOfOfferInstructionsType.getInstruction().add(ctcm);
		// requestType.getDataLists().setInstructionsList(listOfOfferInstructionsType);
	}

	private PartyType getPartyType() {
		PartyType party = new PartyType();

		SenderType senderType = new SenderType();
		TravelAgencyType travelAgency = new TravelAgencyType();
		travelAgency.setAgencyID(supplierConf.getSupplierCredential().getIataNumber());
		travelAgency.setIATANumber(new BigDecimal(supplierConf.getSupplierCredential().getIataNumber()));
		travelAgency.setTypeCode(AgencyTypeContentType.ONLINE_TRAVEL_AGENCY);
		ProperNameType nameType = new ProperNameType();
		nameType.setValue(supplierConf.getSupplierCredential().getAgentUserName());
		travelAgency.setName(nameType);
		senderType.setTravelAgency(travelAgency);
		party.setSender(senderType);

		RecipientType recipientType = new RecipientType();
		CarrierType oraCarrierType = new CarrierType();
		oraCarrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
		recipientType.setORA(oraCarrierType);
		party.setRecipient(recipientType);

		return party;
	}

	private IATAPayloadStandardAttributesType getPayloadAttributesType() {
		IATAPayloadStandardAttributesType payloadAttributeType = new IATAPayloadStandardAttributesType();
		payloadAttributeType.setVersion(AmadeusNDCConstants.SERVICE_VERSION);
		return payloadAttributeType;
	}

	private PointofSaleType2 getPointOfSale() {
		PointofSaleType2 pointOfSale = new PointofSaleType2();
		CountryType country = new CountryType();
		CountryCodeType countryCode = new CountryCodeType();
		countryCode.setValue(AmadeusNDCConstants.COUNTRY_CODE);
		country.setCountryCode(countryCode);
		pointOfSale.setCountry(country);
		return pointOfSale;
	}

	public boolean doPayment() {
		boolean isSuccess = false;
		AlteaNDC181PT servicePort = service.getAlteaNDC181Port();
		BindingProvider bp = (BindingProvider) servicePort;
		addJAXHandlers(bp, bookingId, AmadeusNdcHeaderHandler._ORDER_CHANGE_);
		OrderViewRS orderviewResponse =
				servicePort.ndcOrderChange(orderChangeReq(), null, null, getSecurityHostedUser());
		if (!isAnyError(orderviewResponse)) {
			updatePNR(orderviewResponse);
			updateTicketNumber(orderviewResponse);
			isSuccess = true;
		}
		return isSuccess;
	}

	private OrderChangeRQ orderChangeReq() {
		OrderChangeRQ orderChangeRq = new OrderChangeRQ();
		org.iata.iata._2015._00._2018_1.orderchangerq.RequestType requestType =
				new org.iata.iata._2015._00._2018_1.orderchangerq.RequestType();
		requestType.setActionContextCode("UPDATE_PAYMENT_METHOD_AND_PAY");
		OrderType orderType = new OrderType();
		orderType.setOrderID(supplierBookingId);
		orderType.setOwnerCode(bookingSegmentInfos.get(0).getPriceInfo(0).getPlattingCarrier());
		requestType.setOrder(orderType);

		createPaymentInfo(requestType);
		orderChangeRq.setParty(getParty());
		orderChangeRq.setPayloadAttributes(getOrderChangepayload());
		orderChangeRq.setPointOfSale(getOrderChangePos());
		orderChangeRq.setRequest(requestType);
		return orderChangeRq;
	}

	private org.iata.iata._2015._00._2018_1.orderchangerq.PointofSaleType2 getOrderChangePos() {
		org.iata.iata._2015._00._2018_1.orderchangerq.PointofSaleType2 pointOfSale =
				new org.iata.iata._2015._00._2018_1.orderchangerq.PointofSaleType2();
		org.iata.iata._2015._00._2018_1.orderchangerq.CountryType countryType =
				new org.iata.iata._2015._00._2018_1.orderchangerq.CountryType();
		countryType.setCountryCode(AmadeusNDCConstants.COUNTRY_CODE);
		pointOfSale.setCountry(countryType);
		return pointOfSale;
	}

	private org.iata.iata._2015._00._2018_1.orderchangerq.IATAPayloadStandardAttributesType getOrderChangepayload() {
		org.iata.iata._2015._00._2018_1.orderchangerq.IATAPayloadStandardAttributesType payloadAttributes =
				new org.iata.iata._2015._00._2018_1.orderchangerq.IATAPayloadStandardAttributesType();
		payloadAttributes.setVersion(AmadeusNDCConstants.SERVICE_VERSION);
		return payloadAttributes;
	}

	private org.iata.iata._2015._00._2018_1.orderchangerq.PartyType getParty() {
		org.iata.iata._2015._00._2018_1.orderchangerq.PartyType party =
				new org.iata.iata._2015._00._2018_1.orderchangerq.PartyType();
		org.iata.iata._2015._00._2018_1.orderchangerq.SenderType senderType =
				new org.iata.iata._2015._00._2018_1.orderchangerq.SenderType();
		org.iata.iata._2015._00._2018_1.orderchangerq.TravelAgencyType travelAgency =
				new org.iata.iata._2015._00._2018_1.orderchangerq.TravelAgencyType();
		travelAgency.setAgencyID(supplierConf.getSupplierCredential().getIataNumber());
		travelAgency.setIATANumber(new BigDecimal(supplierConf.getSupplierCredential().getIataNumber()));
		travelAgency.setName(supplierConf.getSupplierCredential().getAgentUserName());
		travelAgency
				.setTypeCode(org.iata.iata._2015._00._2018_1.orderchangerq.AgencyTypeContentType.ONLINE_TRAVEL_AGENCY);
		senderType.setTravelAgency(travelAgency);

		org.iata.iata._2015._00._2018_1.orderchangerq.RecipientType recipientType =
				new org.iata.iata._2015._00._2018_1.orderchangerq.RecipientType();
		org.iata.iata._2015._00._2018_1.orderchangerq.CarrierType carrierType =
				new org.iata.iata._2015._00._2018_1.orderchangerq.CarrierType();
		carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());

		recipientType.setORA(carrierType);
		party.setRecipient(recipientType);
		party.setSender(senderType);
		return party;
	}

	private void createPaymentInfo(org.iata.iata._2015._00._2018_1.orderchangerq.RequestType requestType) {
		PaymentInfoType paymentInfoType = new PaymentInfoType();
		AmountType amountType = new AmountType();
		amountType.setCurCode(getCurrencyCode());

		BigDecimal airlineAmount = getAirlineTotalPrice().setScale(2, BigDecimal.ROUND_HALF_UP);
		amountType.setValue(airlineAmount);

		paymentInfoType.setAmount(amountType);
		if (creditCard != null) {
			paymentInfoType.setTypeCode(AmadeusNDCConstants.PAYMENT_TYPE_CREDIT_CARD);
			PaymentMethodType paymentMethodType = new PaymentMethodType();
			PaymentCardType paymentCardType = new PaymentCardType();
			paymentCardType.setCardNumber(creditCard.getCardNumber());
			paymentCardType.setSeriesCode(creditCard.getCvv());
			paymentCardType.setCreditCardVendorCode(creditCard.getCardType().getVendorCode());
			if (StringUtils.isNotBlank(creditCard.getExpiry())) {
				String expiry = creditCard.getExpiry();
				String expirationDate = expiry.substring(0, 2).concat(expiry.substring(expiry.length() - 2));
				paymentCardType.setExpirationDate(expirationDate);
			}
			paymentMethodType.setPaymentCard(paymentCardType);
			paymentInfoType.setPaymentMethod(paymentMethodType);
		} else {
			paymentInfoType.setTypeCode(AmadeusNDCConstants.PAYMENT_TYPE_CASH);
			PaymentMethodType paymentMethodType = new PaymentMethodType();
			CashType cashType = new CashType();
			cashType.setCashInd(true);
			paymentMethodType.setCash(cashType);
			paymentInfoType.setPaymentMethod(paymentMethodType);
		}
		requestType.getPaymentInfo().add(paymentInfoType);
	}

	private void updateTicketNumber(OrderViewRS orderviewResponse) {
		if (orderviewResponse.getResponse().getTicketDocInfos() != null
				&& CollectionUtils.isNotEmpty(orderviewResponse.getResponse().getTicketDocInfos().getTicketDocInfo())) {
			for (TicketDocInfo ticketDocInfo : orderviewResponse.getResponse().getTicketDocInfos().getTicketDocInfo()) {
				String paxReference = ticketDocInfo.getPassengerReference();
				String paxName = getPaxNameByReference(
						orderviewResponse.getResponse().getDataLists().getPaxList().getPax(), paxReference);
				setTicketNumberToTravellers(paxName, ticketDocInfo.getTicketDocument().get(0).getTicketDocNbr());
			}
		}
	}

	private void setTicketNumberToTravellers(String paxName, String ticketDocNbr) {
		for (FlightTravellerInfo traveller : bookingSegmentInfos.get(0).getTravellerInfo()) {
			if (traveller.getPaxKey().equalsIgnoreCase(paxName)) {
				BookingUtils.updateTicketNumber(bookingSegmentInfos, ticketDocNbr, traveller);
			}
		}
	}

	private String getPaxNameByReference(List<org.iata.iata._2015._00._2018_1.orderviewrs.PaxType> paxList,
			String paxReference) {
		String paxName = "";
		for (org.iata.iata._2015._00._2018_1.orderviewrs.PaxType pax : paxList) {
			if (paxReference.equals(pax.getPaxID())) {
				paxName = StringUtils.join(pax.getIndividual().getGivenName().get(0), " ",
						pax.getIndividual().getSurname());
			}
		}
		return paxName;
	}

}
