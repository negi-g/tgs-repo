package com.tgs.services.fms.helper;

import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;

@Service
public class AirSearch extends AbstractAirSearch {

	@Override
	public AirSearchResult search(AirSearchQuery searchQuery, ContextData contextData) {
		return super.search(searchQuery, contextData);
	}
}
