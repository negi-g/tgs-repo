package com.tgs.services.fms.sources.sqiva;

import static com.tgs.services.fms.sources.sqiva.SqivaConstant.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

@SuperBuilder
@Getter
@Setter
public class SqivaBookingManager extends SqivaServiceManager {

	private BookingSegments bookingSegments;

	private DeliveryInfo deliveryInfo;

	public String booking_V2(GstInfo gstInfo) {
		String bookCode = null;
		HttpUtils httpUtils = HttpUtils.builder().build();
		Map<String, String> bookingParams = buildBookingParams(gstInfo);
		addGSTDetails(bookingParams, gstInfo);
		try {
			JSONObject bookingResultObject = bindingService.getBookingResponse(bookingParams, httpUtils);
			boolean isAnyError = isAnyError(bookingResultObject);
			if (isAnyError) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
			bookCode = bookingResultObject.get(BOOK_CODE).toString();
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		}
		return bookCode;
	}

	private Map<String, String> buildBookingParams(GstInfo gstInfo) {
		Map<String, String> bookingParams = new HashMap<String, String>();
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(bookingSegments.getSegmentInfos());
		List<TripInfo> bookingTrips = tripInfo.splitTripInfo(false);
		TripInfo onwardTrip = bookingTrips.get(0);
		TripInfo returnTrip = null;
		if (CollectionUtils.size(bookingTrips) == 2) {
			returnTrip = bookingTrips.get(1);
		}

		List<SegmentInfo> onwardSegments = onwardTrip.getSegmentInfos();
		SegmentInfo onwardFirstSegment = onwardSegments.get(0);
		SegmentInfo onwardLastSegment = onwardSegments.get(onwardSegments.size() - 1);
		List<FlightTravellerInfo> travllerInfoList = onwardFirstSegment.getTravellerInfo();

		bookingParams.put(ORG, onwardFirstSegment.getDepartureAirportCode());
		bookingParams.put(DES, onwardLastSegment.getArrivalAirportCode());
		bookingParams.put(ROUND_TRIP, (bookingTrips.size() == 2) ? "1" : "0");
		bookingParams.put(DEP_FLIGHT_NO, SqivaUtils.getFlightNumberWithCode(onwardTrip));
		bookingParams.put(DEP_DATE, SqivaUtils.splitDateAndTime(String.valueOf(onwardFirstSegment.getDepartTime())));
		bookingParams.put(SUBCLASS_DEP, SqivaUtils.getSubClass(onwardTrip));

		List<FlightTravellerInfo> adultTravllerInfoList = new ArrayList<>();
		List<FlightTravellerInfo> childTravllerInfoList = new ArrayList<>();
		List<FlightTravellerInfo> infantTravllerInfoList = new ArrayList<>();
		adultTravllerInfoList = AirUtils.getParticularPaxTravellerInfo(travllerInfoList, PaxType.ADULT);
		childTravllerInfoList = AirUtils.getParticularPaxTravellerInfo(travllerInfoList, PaxType.CHILD);
		infantTravllerInfoList = AirUtils.getParticularPaxTravellerInfo(travllerInfoList, PaxType.INFANT);

		// Contact will be updated first and contact will be copied to the adult contact.
		updateContactDetails(bookingParams, adultTravllerInfoList.get(0));

		SqivaUtils.populateTravellerInfo(adultTravllerInfoList, bookingParams, "a");
		SqivaUtils.populateTravellerInfo(childTravllerInfoList, bookingParams, "c");
		SqivaUtils.populateTravellerInfo(infantTravllerInfoList, bookingParams, "i");

		if (bookingTrips.size() == 2) {
			List<SegmentInfo> returnSegments = returnTrip.getSegmentInfos();
			SegmentInfo returnFirstSegment = returnSegments.get(0);
			bookingParams.put(RET_FLIGHT_NO, SqivaUtils.getFlightNumberWithCode(returnTrip));
			bookingParams.put(RET_DATE,
					SqivaUtils.splitDateAndTime(String.valueOf(returnFirstSegment.getDepartTime())));
			bookingParams.put(SUBCLASS_RET, SqivaUtils.getSubClass(returnTrip));
		}

		return bookingParams;
	}

	private void updateContactDetails(Map<String, String> bookingParams, FlightTravellerInfo firstAdultTraveller) {
		String contanctEmail = AirSupplierUtils.getEmailId(deliveryInfo);
		String contactPhoneNumber = AirSupplierUtils.getContactNumber(deliveryInfo);
		bookingParams.put(CALLER, firstAdultTraveller.getPaxKey());
		bookingParams.put(EMAIL, contanctEmail);
		bookingParams.put(CONTACT_1, contactPhoneNumber);
	}

	private void addGSTDetails(Map<String, String> bookingParams, GstInfo gstInfo) {
		if (gstInfo != null && StringUtils.isNotBlank(gstInfo.getGstNumber())) {
			bookingParams.put(COMPANY_GST_NO, gstInfo.getGstNumber());
			bookingParams.put(COMPANY_GST_NAME, gstInfo.getRegisteredName());
			bookingParams.put(COMPANY_GST_LOCATION, gstInfo.getAddress());
		}
	}

	public double payment(String pnr, double amountPaidToAirline) {
		double supplierPaymentDiff = 0d;
		HttpUtils httpUtils = HttpUtils.builder().build();
		try {
			Map<String, String> paymentParams = buildPaymentParams(pnr, amountPaidToAirline);
			JSONObject paymentResponse = bindingService.payment(paymentParams, httpUtils);
			boolean isAnyError = isAnyError(paymentResponse);
			if (isAnyError) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
			updateTicketNumber(paymentResponse, bookingSegments);
			supplierPaymentDiff = paymentResponse.getDouble(SqivaConstant.BOOK_BALANCE);
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		}
		return supplierPaymentDiff;
	}

	private Map<String, String> buildPaymentParams(String pnr, double amountPaidToAirline) {
		Map<String, String> paymentParams = new HashMap<String, String>();
		paymentParams.put(BOOK_CODE, pnr);
		paymentParams.put(AMOUNT, String.valueOf(amountPaidToAirline));
		return paymentParams;
	}

	public void updateSupplierAdditionalInfo(JSONObject retrieveResponse, BookingSegments bookingSegments) {
		JSONArray routeDetailArray = retrieveResponse.getJSONArray(SqivaConstant.ROUTE_DETAIL);
		for (int index = 0; index < routeDetailArray.length(); index++) {
			JSONArray routeDetail = routeDetailArray.getJSONArray(index);

			String departureAirportCode =
					routeDetail.getString(SqivaConstant.ROUTE_DETAIL_DEPARTURE_AIRPORT_CODE_INDEX);
			String departureDate = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_DEPARTURE_DATE_INDEX);
			String departureTime = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_DEPARTURE_TIME_INDEX);
			LocalDateTime departureDateTime = SqivaUtils.convertToLocalDateTime("dd-MMM-yyHHmm",
					org.apache.commons.lang3.StringUtils.join(departureDate, departureTime));

			String arrivalAirportCode = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_ARRIVAL_AIRPORT_CODE_INDEX);
			String arrivalDate = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_ARRIVAL_DATE_INDEX);
			String arrivalTime = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_ARRIVAL_TIME_INDEX);
			LocalDateTime arrivalDateTime = SqivaUtils.convertToLocalDateTime("dd-MMM-yyHHmm",
					org.apache.commons.lang3.StringUtils.join(arrivalDate, arrivalTime));

			String flightNumber = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_FLIGHT_NUMBER);
			String travellerFirstName = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_TRAVELLER_FIRST_NAME_INDEX);
			String travellerLastName = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_TRAVELLER_LAST_NAME_INDEX);
			String paxId = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_PAX_ID_INDEX);
			String bookLegId = routeDetail.getString(SqivaConstant.ROUTE_DETAIL_BOOK_LEG_ID_INDEX);

			// paxId and bookLegId will be set in the first leg of every segments
			for (int segmentIndex = 0; segmentIndex < bookingSegments.getSegmentInfos().size();) {
				List<SegmentInfo> subSegments = getSubLegSegments(bookingSegments.getSegmentInfos(), segmentIndex);
				SegmentInfo firstSegment = subSegments.get(0);
				SegmentInfo lastSegment = subSegments.get(subSegments.size() - 1);
				String segmentDeparture = firstSegment.getDepartureAirportCode();
				String segmentArrivalCode = lastSegment.getArrivalAirportCode();
				LocalDateTime segmentDepartureTime = firstSegment.getDepartTime();
				LocalDateTime segmentArrivalTime = lastSegment.getArrivalTime();
				if (segmentDeparture.equals(departureAirportCode) && segmentArrivalCode.equals(arrivalAirportCode)
						&& segmentDepartureTime.isEqual(departureDateTime)
						&& segmentArrivalTime.isEqual(arrivalDateTime)
						&& org.apache.commons.lang3.StringUtils.join(firstSegment.getAirlineCode(false), "-",
								firstSegment.getAirlineCode(false), firstSegment.getFlightNumber())
								.equals(flightNumber)) {
					firstSegment.getPriceInfo(0).getMiscInfo().setBookLegId(bookLegId);
					for (FlightTravellerInfo traveller : firstSegment.getTravellerInfo()) {
						if (traveller.getFirstName().equalsIgnoreCase(travellerFirstName)
								&& traveller.getLastName().equalsIgnoreCase(travellerLastName)) {
							traveller.getMiscInfo().setPassengerKey(paxId);
						}
					}
				}
				segmentIndex += subSegments.size();
			}
		}
	}

	public void updateTicketNumber(JSONObject paymentResponse, BookingSegments bookingSegments) {
		Map<String, String> travellerTicketNumberMap = new HashMap<String, String>();
		JSONArray ticektUnitArray = paymentResponse.getJSONArray(SqivaConstant.TICKET_UNIT);
		for (int index = 0; index < ticektUnitArray.length(); index++) {
			JSONArray ticketUnit = ticektUnitArray.getJSONArray(index);
			String travellerName = ticketUnit.getString(SqivaConstant.TICKET_UNIT_TRAVELLER_NAME_INDEX);
			String ticektNumber = ticketUnit.getString(SqivaConstant.TICKET_UNIT_TICKET_NUMBER_INDEX);
			travellerTicketNumberMap.put(travellerName.trim(), ticektNumber.trim());
		}

		bookingSegments.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.getTravellerInfo().forEach(traveller -> {
				String travellerName = traveller.getPaxKey().toUpperCase().trim();
				if (travellerTicketNumberMap.get(travellerName) != null) {
					traveller.setTicketNumber(travellerTicketNumberMap.get(travellerName));
				}
			});
		});
	}


}
