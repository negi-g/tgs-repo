package com.tgs.services.fms.sources.amadeusndc;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import javax.xml.ws.BindingProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.iata.iata._2015._00._2018_1.servicelistrq.CarrierType;
import org.iata.iata._2015._00._2018_1.servicelistrq.CoreRequestType;
import org.iata.iata._2015._00._2018_1.servicelistrq.CountryType;
import org.iata.iata._2015._00._2018_1.servicelistrq.IATAPayloadStandardAttributesType;
import org.iata.iata._2015._00._2018_1.servicelistrq.OfferItemType;
import org.iata.iata._2015._00._2018_1.servicelistrq.OfferType;
import org.iata.iata._2015._00._2018_1.servicelistrq.PartyType;
import org.iata.iata._2015._00._2018_1.servicelistrq.PointofSaleType2;
import org.iata.iata._2015._00._2018_1.servicelistrq.PrefLevelType;
import org.iata.iata._2015._00._2018_1.servicelistrq.RecipientType;
import org.iata.iata._2015._00._2018_1.servicelistrq.RequestType;
import org.iata.iata._2015._00._2018_1.servicelistrq.SenderType;
import org.iata.iata._2015._00._2018_1.servicelistrq.ServiceCriteriaType;
import org.iata.iata._2015._00._2018_1.servicelistrq.ServiceListRQ;
import org.iata.iata._2015._00._2018_1.servicelistrq.ServiceType;
import org.iata.iata._2015._00._2018_1.servicelistrq.ShoppingCriteriaType;
import org.iata.iata._2015._00._2018_1.servicelistrq.TravelAgencyType;
import org.iata.iata._2015._00._2018_1.servicelistrq.TravelAgencyTypeCodeContentType;
import org.iata.iata._2015._00._2018_1.servicelistrs.ALaCarteOfferItemType;
import org.iata.iata._2015._00._2018_1.servicelistrs.ErrorType;
import org.iata.iata._2015._00._2018_1.servicelistrs.PaxSegmentType;
import org.iata.iata._2015._00._2018_1.servicelistrs.PaxType;
import org.iata.iata._2015._00._2018_1.servicelistrs.ResponseType;
import org.iata.iata._2015._00._2018_1.servicelistrs.ServiceDefinitionType;
import org.iata.iata._2015._00._2018_1.servicelistrs.ServiceListRS;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class AmadeusNdcSSRManager extends AmadeusNdcServiceManager {

	protected TripInfo selectedTrip;

	public void serviceList() {
		AlteaNDC181PT servicePort = service.getAlteaNDC181Port();
		BindingProvider bp = (BindingProvider) servicePort;
		addJAXHandlers(bp, bookingId, AmadeusNdcHeaderHandler._SERVICE_LIST_);
		ServiceListRS serviceListResponse =
				servicePort.ndcServiceList(createServiceListReq(), null, null, getSecurityHostedUser());
		if (!isAnyError(serviceListResponse)) {
			updateSSRToTrip(serviceListResponse);

		}
	}

	private ServiceListRQ createServiceListReq() {
		ServiceListRQ serviceListRQ = new ServiceListRQ();
		RequestType request = new RequestType();
		CoreRequestType coreRequest = new CoreRequestType();
		OfferType offerType = new OfferType();
		PriceMiscInfo miscInfo = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo();
		offerType.setOfferID(miscInfo.getFareKey());
		OfferItemType offerItemType = new OfferItemType();
		offerItemType.setOwnerCode("SQ");
		offerItemType.setOfferItemID(miscInfo.getFareLevel());
		ServiceType serviceType = new ServiceType();
		serviceType.setServiceID("1");
		offerItemType.getService().add(serviceType);
		offerType.getOfferItem().add(offerItemType);
		coreRequest.getOffer().add(offerType);
		setPrefContextType(request);
		request.setCoreRequest(coreRequest);
		serviceListRQ.setRequest(request);
		serviceListRQ.setParty(getParty());
		serviceListRQ.setPayloadAttributes(getPayloadAttribute());
		serviceListRQ.setPointOfSale(getPointOfSale());
		return serviceListRQ;
	}

	private PointofSaleType2 getPointOfSale() {
		PointofSaleType2 pointOfSale = new PointofSaleType2();
		CountryType countryType = new CountryType();
		countryType.setCountryCode(AmadeusNDCConstants.COUNTRY_CODE);
		pointOfSale.setCountry(countryType);
		return pointOfSale;
	}

	private IATAPayloadStandardAttributesType getPayloadAttribute() {
		IATAPayloadStandardAttributesType payloadAttributes = new IATAPayloadStandardAttributesType();
		payloadAttributes.setVersion(AmadeusNDCConstants.SERVICE_VERSION);
		return payloadAttributes;
	}

	private void setPrefContextType(RequestType request) {
		ShoppingCriteriaType shoppingCriteria = new ShoppingCriteriaType();
		ServiceCriteriaType serviceCriteria = new ServiceCriteriaType();
		PrefLevelType prefLevel = new PrefLevelType();
		prefLevel.setPrefContextText(AmadeusNDCConstants.PREF_LEVEL_TEXT);
		serviceCriteria.setPrefLevel(prefLevel);
		shoppingCriteria.setServiceCriteria(serviceCriteria);
		request.setShoppingCriteria(shoppingCriteria);
	}

	private PartyType getParty() {
		PartyType party = new PartyType();
		SenderType sender = new SenderType();
		TravelAgencyType travelAgency = new TravelAgencyType();
		travelAgency.setAgencyID(supplierConf.getSupplierCredential().getIataNumber());
		travelAgency.setIATANumber(new BigDecimal(supplierConf.getSupplierCredential().getIataNumber()));
		travelAgency.setName(supplierConf.getSupplierCredential().getAgentUserName());
		travelAgency.setTypeCode(TravelAgencyTypeCodeContentType.ONLINE_TRAVEL_AGENCY);
		sender.setTravelAgency(travelAgency);


		RecipientType recipient = new RecipientType();
		CarrierType carrierType = new CarrierType();
		carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
		recipient.setORA(carrierType);
		party.setRecipient(recipient);
		party.setSender(sender);
		return party;
	}

	private void updateSSRToTrip(ServiceListRS serviceListResponse) {
		if (serviceListResponse.getResponse() != null && serviceListResponse.getResponse().getALaCarteOffer() != null
				&& CollectionUtils
						.isNotEmpty(serviceListResponse.getResponse().getALaCarteOffer().getALaCarteOfferItem())
				&& serviceListResponse.getResponse().getDataLists() != null
				&& serviceListResponse.getResponse().getDataLists().getPaxSegmentList() != null
				&& CollectionUtils.isNotEmpty(
						serviceListResponse.getResponse().getDataLists().getPaxSegmentList().getPaxSegment())
				&& serviceListResponse.getResponse().getDataLists().getServiceDefinitionList() != null
				&& CollectionUtils.isNotEmpty(serviceListResponse.getResponse().getDataLists()
						.getServiceDefinitionList().getServiceDefinition())) {

			String adultPaxId = "";
			for (PaxType pax : serviceListResponse.getResponse().getDataLists().getPaxList().getPax()) {
				if (AmadeusNdcPaxType.ADT.name().equals(pax.getPTC())) {
					adultPaxId = pax.getPaxID();
					break;
				}
			}

			Map<String, ServiceDefinitionType> serviceDefinitionMap = new HashMap<>();
			for (ServiceDefinitionType serviceDefinition : serviceListResponse.getResponse().getDataLists()
					.getServiceDefinitionList().getServiceDefinition()) {
				serviceDefinitionMap.put(serviceDefinition.getServiceDefinitionID(), serviceDefinition);
			}

			for (TripInfo tripInfo : selectedTrip.splitTripInfo(false, true)) {

				FlightBasicFact fact =
						FlightBasicFact.createFact().generateFact(tripInfo, AirUtils.getAirType(tripInfo));
				BaseUtils.createFactOnUser(fact, bookingUser);
				Map<SSRType, List<? extends SSRInformation>> preSSR = AirUtils.getPreSSR(fact);

				if (MapUtils.isNotEmpty(preSSR)) {

					List<String> paxSegmentIds = getPaxSegmentId(tripInfo.getSegmentInfos(),
							serviceListResponse.getResponse().getDataLists().getPaxSegmentList().getPaxSegment());
					List<ALaCarteOfferItemType> matchedAlaCarteOfferItems =
							matchOffersBySegmentAndPaxID(paxSegmentIds, adultPaxId, serviceListResponse.getResponse());
					if (CollectionUtils.isNotEmpty(matchedAlaCarteOfferItems)) {

						List<BaggageSSRInformation> baggageSSrList = new ArrayList<>();
						List<MealSSRInformation> mealSSRList = new ArrayList<>();

						for (ALaCarteOfferItemType alaCarteOfferItem : matchedAlaCarteOfferItems) {
							ServiceDefinitionType serviceDefinition = serviceDefinitionMap
									.get(alaCarteOfferItem.getService().getServiceDefinitionRefID());
							if (serviceDefinition != null) {

								SSRInformation baggageSSRInfo = AirUtils.getSSRInfo(preSSR.get(SSRType.BAGGAGE),
										serviceDefinition.getServiceCode());
								SSRInformation mealSSRInfo = AirUtils.getSSRInfo(preSSR.get(SSRType.MEAL),
										serviceDefinition.getServiceCode());

								if (baggageSSRInfo != null) {
									BaggageSSRInformation baggageSSr = new BaggageSSRInformation();
									double amount = getAmountBasedOnCurrency(
											alaCarteOfferItem.getUnitPrice().getTotalAmount().getValue(),
											alaCarteOfferItem.getUnitPrice().getTotalAmount().getCurCode());
									baggageSSr.setCode(serviceDefinition.getServiceCode());
									baggageSSr.setDesc(serviceDefinition.getName());
									baggageSSr.setAmount(amount);
									if (serviceDefinition.getDetail() != null
											&& serviceDefinition.getDetail().getServiceItemQuantityRules() != null
											&& serviceDefinition.getDetail().getServiceItemQuantityRules()
													.getMaximumQuantity() != null) {
										baggageSSr.setQuantity(serviceDefinition.getDetail()
												.getServiceItemQuantityRules().getMaximumQuantity().intValue());
									}
									baggageSSr.getMiscInfo()
											.setPaxSsrKeys(setPaxWiseSSRKeyInMisc(serviceDefinition.getName(),
													serviceListResponse.getResponse().getALaCarteOffer()
															.getALaCarteOfferItem(),
													serviceDefinitionMap, paxSegmentIds));
									baggageSSrList.add(baggageSSr);
								} else if (mealSSRInfo != null) {
									MealSSRInformation mealSSR = new MealSSRInformation();
									double amount = getAmountBasedOnCurrency(
											alaCarteOfferItem.getUnitPrice().getTotalAmount().getValue(),
											alaCarteOfferItem.getUnitPrice().getTotalAmount().getCurCode());
									mealSSR.setCode(serviceDefinition.getServiceCode());
									mealSSR.setDesc(mealSSRInfo.getDesc());
									mealSSR.setAmount(amount);
									mealSSR.getMiscInfo()
											.setPaxSsrKeys(setPaxWiseSSRKeyInMisc(serviceDefinition.getName(),
													serviceListResponse.getResponse().getALaCarteOffer()
															.getALaCarteOfferItem(),
													serviceDefinitionMap, paxSegmentIds));
									mealSSRList.add(mealSSR);
								}
							}
						}
						if (CollectionUtils.isNotEmpty(baggageSSrList) || CollectionUtils.isNotEmpty(mealSSRList)) {
							for (SegmentInfo segment : tripInfo.getSegmentInfos()) {
								Map<SSRType, List<? extends SSRInformation>> ssrInfo = new HashMap<>();
								if (CollectionUtils.isNotEmpty(baggageSSrList)) {
									List<BaggageSSRInformation> baggageSSRListCopy =
											GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(baggageSSrList),
													new TypeToken<List<BaggageSSRInformation>>() {}.getType());
									if (segment.getSegmentNum() != 0) {
										for (BaggageSSRInformation baggageSSR : baggageSSRListCopy) {
											baggageSSR.setAmount(null);
										}
									}
									ssrInfo.put(SSRType.BAGGAGE, baggageSSRListCopy);
								}
								if (CollectionUtils.isNotEmpty(mealSSRList)) {
									List<MealSSRInformation> mealSSRListCopy =
											GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(mealSSRList),
													new TypeToken<List<BaggageSSRInformation>>() {}.getType());
									ssrInfo.put(SSRType.MEAL, mealSSRListCopy);
								}
								segment.setSsrInfo(ssrInfo);
							}
						}

					}
				}
			}
		}
	}

	private Map<String, String> setPaxWiseSSRKeyInMisc(String serviceName,
			List<ALaCarteOfferItemType> alaCarteOfferItems, Map<String, ServiceDefinitionType> serviceDefinitionMap,
			List<String> paxSegmentIds) {
		Map<String, String> paxServiceDefinitionIdMap = new HashMap<>();
		for (ALaCarteOfferItemType alaCarteOfferItem : alaCarteOfferItems) {
			String serviceRefId = alaCarteOfferItem.getService().getServiceDefinitionRefID();
			ServiceDefinitionType serviceDefinition = serviceDefinitionMap.get(serviceRefId);
			if (serviceDefinition != null && serviceName.equals(serviceDefinition.getName()) && paxSegmentIds
					.containsAll(alaCarteOfferItem.getEligibility().getFlightAssociations().getPaxSegmentRefID())) {
				for (String paxRefId : alaCarteOfferItem.getEligibility().getPaxRefID()) {
					// serviceRefId / OfferItemId will be stored paxwise and will be consumed separately in the booking.
					paxServiceDefinitionIdMap.put(paxRefId,
							StringUtils.join(serviceRefId, "/", alaCarteOfferItem.getOfferItemID()));
				}
			}
		}
		return paxServiceDefinitionIdMap;
	}

	private List<ALaCarteOfferItemType> matchOffersBySegmentAndPaxID(List<String> paxSegmentIds, String adultPaxId,
			ResponseType response) {
		List<ALaCarteOfferItemType> matchedAlaCarteOfferItems = new ArrayList<>();
		for (ALaCarteOfferItemType alaCarteOfferItem : response.getALaCarteOffer().getALaCarteOfferItem()) {
			if (alaCarteOfferItem.getEligibility().getPaxRefID().contains(adultPaxId) && paxSegmentIds
					.containsAll(alaCarteOfferItem.getEligibility().getFlightAssociations().getPaxSegmentRefID())) {
				matchedAlaCarteOfferItems.add(alaCarteOfferItem);
			}
		}
		return matchedAlaCarteOfferItems;
	}

	private List<String> getPaxSegmentId(List<SegmentInfo> segmentInfos, List<PaxSegmentType> paxSegmentTypes) {
		List<String> paxSegmentIds = new ArrayList<>();
		for (SegmentInfo segmentInfo : segmentInfos) {
			for (PaxSegmentType paxSegment : paxSegmentTypes) {
				LocalDateTime departureTime =
						TgsDateUtils.getLocalDateTime(paxSegment.getDep().getAircraftScheduledDateTime().getValue());
				LocalDateTime arrivalTime = TgsDateUtils
						.getLocalDateTime(paxSegment.getArrival().getAircraftScheduledDateTime().getValue());
				if (segmentInfo.getDepartureAirportCode().equals(paxSegment.getDep().getIATALocationCode())
						&& segmentInfo.getArrivalAirportCode().equals(paxSegment.getArrival().getIATALocationCode())
						&& segmentInfo.getDepartTime().isEqual(departureTime)
						&& segmentInfo.getArrivalTime().isEqual(arrivalTime)) {
					paxSegmentIds.add(paxSegment.getPaxSegmentID());
				}
			}
		}
		return paxSegmentIds;
	}

	private boolean isAnyError(ServiceListRS serviceListResponse) {
		boolean isAnyError = false;
		StringJoiner errorMessage = new StringJoiner("");
		if (serviceListResponse != null && CollectionUtils.isNotEmpty(serviceListResponse.getError())) {
			for (ErrorType error : serviceListResponse.getError()) {
				errorMessage.add(StringUtils.join(error.getCode(), "  ", error.getDescText()));
				isAnyError = true;
			}
			log.info("Error occured on booking {} error message {}", bookingId, errorMessage.toString());
		}
		logCriticalMessage(errorMessage.toString());
		return isAnyError;
	}

}
