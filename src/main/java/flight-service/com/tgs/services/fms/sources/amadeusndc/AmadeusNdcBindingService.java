package com.tgs.services.fms.sources.amadeusndc;

import com.tgs.services.fms.sources.AirSourceConstants;
import org.apache.axis2.AxisFault;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.amadeus.wsdl.altea_ndc_18_1_v1_v4.AlteaNDC181Service;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;

@Builder
@Slf4j
public class AmadeusNdcBindingService {

	public AlteaNDC181PT getServicePort() throws AxisFault {
		AlteaNDC181PT servicePort = null;
		try {
			AlteaNDC181Service serviceStub = new AlteaNDC181Service();
			servicePort = serviceStub.getAlteaNDC181Port();
		} catch (Exception e) {
			log.error("Unable to generate Stub {} :", e);
		}
		return servicePort;
	}

	private void setSerivceProperties(Stub stub, String action) {
		ServiceClient client = stub._getServiceClient();
		Options options = client.getOptions();
		if (options != null) {
			options.setProperty(HTTPConstants.SO_TIMEOUT, AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			options.setProperty(HTTPConstants.CONNECTION_TIMEOUT, AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			options.setTimeOutInMilliSeconds(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			setConnectionManager(stub);
			stub._getServiceClient().setOptions(options);
		}
	}

	public void setConnectionManager(Stub stub) {
		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(30);
		conmgr.getParams().setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		conmgr.getParams().setSoTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		HttpClient client = new HttpClient(conmgr);
		stub._getServiceClient().getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
	}
}
