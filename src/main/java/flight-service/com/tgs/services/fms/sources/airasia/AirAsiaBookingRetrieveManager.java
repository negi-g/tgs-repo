package com.tgs.services.fms.sources.airasia;

import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.*;
import com.tgs.services.fms.datamodel.ssr.*;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.manager.TripPriceEngine;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.ImportPNRNotAllowedException;
import com.tgs.utils.exception.air.NoPNRFoundException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.tempuri.*;
import java.rmi.RemoteException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@Setter
@Getter
@Slf4j
@SuperBuilder
final class AirAsiaBookingRetrieveManager extends AirAsiaServiceManager {

	protected String carrierCode;

	public AirImportPnrBooking retrieveBooking(String pnr) {
		AirImportPnrBooking pnrBooking = null;
		List<TripInfo> tripInfos = null;
		AirItemStatus itemStatus = null;
		GstInfo gstInfo = null;
		DeliveryInfo deliveryInfo = null;
		Booking booking = getBookingResponse(pnr);
		try {
			if (isValidPNRBooking(booking, pnr)) {
				tripInfos = fetchTripDetails(booking);
				itemStatus = fetchItemStatus(booking.getBookingInfo());
				gstInfo = fetchGSTInfo(booking);
				deliveryInfo = fetchDeliveryInfo(booking);
			}
		} finally {
			if (CollectionUtils.isNotEmpty(tripInfos)) {
				resetTravellerId(tripInfos);
				pnrBooking = AirImportPnrBooking.builder().tripInfos(tripInfos).gstInfo(gstInfo)
						.deliveryInfo(deliveryInfo).itemStatus(itemStatus).build();
			}
		}
		return pnrBooking;
	}

	public Booking getBookingResponse(String pnr) {
		GetBooking bookingRequest = new GetBooking();
		Booking booking = null;
		try {
			bookingRequest.setStrSessionID(sessionSignature);
			bookingRequest.setObjGetBookingRequestData(getBookingRequestData(pnr));
			listener.setType("P-RETRIEVE");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			GetBookingResponse bookedResponse = bookingStub.getBooking(bookingRequest);
			if (!checkIsValidResponse(bookedResponse)) {
				booking = bookedResponse.getGetBookingResult();
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return booking;
	}

	private boolean checkIsValidResponse(GetBookingResponse bookedResponse) {
		if (bookedResponse != null && bookedResponse.getGetBookingResult() != null
				&& StringUtils.isNotBlank(bookedResponse.getGetBookingResult().getExceptionMessage())) {
			String message = bookedResponse.getGetBookingResult().getExceptionMessage();
			if (criticalMessageLogger != null) {
				criticalMessageLogger.add(message);
			}
			throw new ImportPNRNotAllowedException(message);
		}
		return false;
	}

	private GetBookingRequestData getBookingRequestData(String pnr) {
		GetBookingRequestData requestData = new GetBookingRequestData();
		requestData.setGetByRecordLocator(getRecordLocator(pnr));
		requestData.setGetBookingBy(GetBookingBy.RecordLocator);
		return requestData;
	}


	private GetByRecordLocator getRecordLocator(String pnr) {
		GetByRecordLocator recordLocator = new GetByRecordLocator();
		recordLocator.setRecordLocator(pnr);
		return recordLocator;
	}

	public boolean isValidPNRBooking(Booking booking, String pnr) {
		if (booking != null && StringUtils.isNotEmpty(booking.getRecordLocator())
				&& (booking.getJourneys() == null || ArrayUtils.isEmpty(booking.getJourneys().getJourney()))) {
			throw new NoPNRFoundException("No PNR/Journey Available " + booking.getRecordLocator());
		} else if (booking == null || StringUtils.isEmpty(booking.getRecordLocator())) {
			throw new NoPNRFoundException("Invalid PNR " + pnr);
		}
		return true;
	}

	private GstInfo fetchGSTInfo(Booking booking) {
		GstInfo gstInfo = null;
		BookingContact gstContactInfo = getBookingContact(booking, airAsiaAirline.getGSTTypeCode());
		if (Objects.nonNull(gstContactInfo)) {
			gstInfo = new GstInfo();
			gstInfo.setGstNumber(gstContactInfo.getCustomerNumber());
			gstInfo.setEmail(gstContactInfo.getEmailAddress());
			gstInfo.setRegisteredName(gstContactInfo.getCompanyName());
			gstInfo.setAddress(StringUtils.join(gstContactInfo.getAddressLine1(), gstContactInfo.getAddressLine2(),
					gstContactInfo.getAddressLine3()));
			gstInfo.setMobile(gstContactInfo.getHomePhone());
			gstInfo.setCityName(gstContactInfo.getCity());
			gstInfo.setPincode(gstContactInfo.getPostalCode());
			gstInfo.setState(gstContactInfo.getProvinceState());
		}
		return gstInfo;
	}

	public BookingContact getBookingContact(Booking booking, String contactTypeCode) {
		BookingContact[] bookingContacts = booking.getBookingContacts().getBookingContact();
		List<BookingContact> contacts = Arrays.stream(bookingContacts).filter(bookingContact -> {
			return (StringUtils.isNotEmpty(bookingContact.getTypeCode())
					&& bookingContact.getTypeCode().equals(contactTypeCode));
		}).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(contacts)) {
			return contacts.get(0);
		}
		return null;
	}

	private DeliveryInfo fetchDeliveryInfo(Booking booking) {
		DeliveryInfo deliveryInfo = null;
		BookingContact bookingContact = getBookingContact(booking, airAsiaAirline.getContactTypeCode());
		if (Objects.nonNull(bookingContact)) {
			deliveryInfo = new DeliveryInfo();
			deliveryInfo.setContacts(Arrays.asList(bookingContact.getOtherPhone()));
			deliveryInfo.setEmails(Arrays.asList(bookingContact.getEmailAddress()));
		}
		return deliveryInfo;
	}


	private AirItemStatus fetchItemStatus(BookingInfo bookingInfo) {
		AirItemStatus itemStatus = AirItemStatus.IN_PROGRESS;
		if (bookingInfo.getBookingStatus().equals(BookingStatus.Confirmed)) {
			itemStatus = AirItemStatus.SUCCESS;
		} else if (bookingInfo.getBookingStatus().equals(BookingStatus.Hold)) {
			itemStatus = AirItemStatus.ON_HOLD;
		} else if (bookingInfo.getBookingStatus().equals(BookingStatus.HoldCanceled)) {
			itemStatus = AirItemStatus.ABORTED;
		}
		return itemStatus;
	}

	private List<TripInfo> fetchTripDetails(Booking bookingDetails) {
		String confirmationCode = bookingDetails.getRecordLocator();
		List<TripInfo> tripInfos = getTripDetails(bookingDetails);
		List<FlightTravellerInfo> flightTravellerInfos =
				getTripTravaller(bookingDetails.getPassengers().getPassenger(), confirmationCode, tripInfos);
		TripPriceEngine.shiftFareDetailsToFlightTraveller(tripInfos, flightTravellerInfos);
		AirAsiaUtils.setTotalFareOnTripInfo(tripInfos);
		removeExtraSSRFromPax(tripInfos);
		setSeatInfosToTravellers(bookingDetails, tripInfos, confirmationCode);
		unsetSSRinfos(tripInfos);
		setTravellerBoardingStatus(bookingDetails.getJourneys().getJourney(), tripInfos);
		return tripInfos;
	}

	private void setTravellerBoardingStatus(Journey[] journeys, List<TripInfo> tripInfos) {
		for (Journey journey : journeys) {
			Segment[] segments = journey.getSegments().getSegment();
			for (Segment segment : segments) {
				SegmentInfo segmentInfo = AirAsiaUtils.findSegmentFromTripInfos(tripInfos, segment);
				if (segmentInfo != null) {
					PaxSegment[] paxSegments = segment.getPaxSegments().getPaxSegment();
					for (PaxSegment paxSegment : paxSegments) {
						FlightTravellerInfo travellerInfo = segmentInfo.getBookingRelatedInfo().getTravellerInfo()
								.get(paxSegment.getPassengerNumber());
						LiftStatus liftStatus = paxSegment.getLiftStatus();
						if (liftStatus.equals(LiftStatus.NoShow)) {
							travellerInfo.setStatus(TravellerStatus.NO_SHOW);
						} else if (liftStatus.equals(LiftStatus.Boarded)) {
							travellerInfo.setStatus(TravellerStatus.BOARDED);
						}
					}
				}
			}
		}
	}

	private List<TripInfo> getTripDetails(Booking booking) {
		int journeyIndex = 0;
		List<TripInfo> tripInfos = new ArrayList<>();
		if (ArrayUtils.isNotEmpty(booking.getJourneys().getJourney())) {
			for (Journey journey : booking.getJourneys().getJourney()) {
				AtomicInteger segmentNum = new AtomicInteger(0);
				TripInfo tripInfo = new TripInfo();
				Map<PaxType, Integer> paxCountInBooking = getPaxCountInBooking(booking);
				for (Segment segment : journey.getSegments().getSegment()) {
					List<SegmentInfo> segmentInfos =
							getSegmentDetails(segment, segmentNum, journey.getJourneySellKey(), paxCountInBooking);
					if (CollectionUtils.isNotEmpty(segmentInfos) && StringUtils.isNotBlank(carrierCode)) {
						carrierCode = segmentInfos.get(0).getAirlineCode(false);
					}
					if (journeyIndex == 1 && segmentNum.get() == 0) {
						segmentInfos.forEach(segmentInfo -> {
							segmentInfo.setIsReturnSegment(true);
						});
					}
					tripInfo.getSegmentInfos().addAll(segmentInfos);
				}
				tripInfo.setConnectionTime();
				tripInfos.add(tripInfo);
				journeyIndex++;
			}
		}
		return tripInfos;
	}

	private List<SegmentInfo> getSegmentDetails(Segment segment, AtomicInteger segmentNum, String journeyKey,
			Map<PaxType, Integer> paxCountInBooking) {
		Leg[] legs = segment.getLegs().getLeg();
		List<SegmentInfo> segmentInfos =
				parseSegmentInfoFromLeg(legs, segmentNum, segment.getFares(), journeyKey, paxCountInBooking);
		return segmentInfos;
	}

	private List<SegmentInfo> parseSegmentInfoFromLeg(Leg[] legs, AtomicInteger segmentIndex, ArrayOfFare arrayOfFare,
			String journeyKey, Map<PaxType, Integer> paxCountInBooking) {
		List<SegmentInfo> segmentInfos = new ArrayList<>();
		Integer legIndex = 0;
		for (Leg leg : legs) {
			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setSegmentNum(segmentIndex.intValue());
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirportInfo(leg.getDepartureStation()));
			segmentInfo.setArrivalAirportInfo(AirportHelper.getAirportInfo(leg.getArrivalStation()));
			segmentInfo.setDepartTime(TgsDateUtils.toLocalDateTime(leg.getSTD()));
			segmentInfo.setArrivalTime(TgsDateUtils.toLocalDateTime(leg.getSTA()));
			segmentInfo.setFlightDesignator(getFlightDesignator(leg));
			segmentInfo.setDuration(segmentInfo.calculateDuration());
			segmentInfo.setIsReturnSegment(Boolean.FALSE);
			if (leg.getLegInfo() != null && StringUtils.isNotBlank(leg.getLegInfo().getOperatingCarrier())) {
				AirlineInfo operatingAirline = AirlineHelper.getAirlineInfo(leg.getLegInfo().getOperatingCarrier());
				segmentInfo.setOperatedByAirlineInfo(operatingAirline);
			}
			List<PriceInfo> priceInfos = getPriceInfos(arrayOfFare, journeyKey, legIndex, paxCountInBooking);
			segmentInfo.setPriceInfoList(priceInfos);
			segmentInfo.setPriceInfoList(priceInfos);
			AirAsiaUtils.setTerminal(leg, segmentInfo);
			segmentInfos.add(segmentInfo);
			segmentIndex.getAndIncrement();
			legIndex++;
		}
		return segmentInfos;
	}

	public List<PriceInfo> getPriceInfos(ArrayOfFare arrayOfFare, String journeyKey, Integer legIndex,
			Map<PaxType, Integer> paxCountInBooking) {
		List<PriceInfo> priceInfos = new ArrayList<>();
		if (arrayOfFare != null && ArrayUtils.isNotEmpty(arrayOfFare.getFare())) {
			Fare fare = arrayOfFare.getFare()[0];
			PriceInfo priceInfo = PriceInfo.builder().build();
			priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
			PriceMiscInfo miscInfo = PriceMiscInfo.builder().build();
			miscInfo.setFareKey(fare.getFareSellKey());
			miscInfo.setFareClassOfService(fare.getFareClassOfService());
			miscInfo.setClassOfService(fare.getClassOfService());
			miscInfo.setRuleNumber(fare.getRuleNumber());
			short sequence = fare.getFareSequence();
			miscInfo.setFareSequence(Integer.valueOf(sequence).toString());
			miscInfo.setFareApplicationName(fare.getFareApplicationType().getValue());
			miscInfo.setPlatingCarrier(AirlineHelper.getAirlineInfo(fare.getCarrierCode()));
			miscInfo.setLegNum(legIndex);
			priceInfo.setMiscInfo(PriceMiscInfo.builder().fareKey(fare.getFareSellKey()).build());
			Map<PaxType, FareDetail> fareDetailMap =
					getPaxWiseFareDetail(fare, fare.getAvailableCount(), paxCountInBooking);
			if (legIndex.intValue() == 0) {
				// Set price in First Leg
				priceInfo.setFareDetails(fareDetailMap);
			} else {
				fareDetailMap.forEach(((paxType, fareDetail) -> {
					fareDetail.setFareComponents(new HashMap<>());
				}));
				priceInfo.setFareDetails(fareDetailMap);
			}
			miscInfo.setJourneyKey(journeyKey);
			priceInfo.setMiscInfo(miscInfo);
			airAsiaAirline.processPriceInfo(fare.getProductClass(), priceInfo, configuration);
			priceInfos.add(priceInfo);
		}
		return priceInfos;
	}

	private Map<PaxType, FareDetail> getPaxWiseFareDetail(Fare fare, int seatCount,
			Map<PaxType, Integer> paxCountInBooking) {
		Map<PaxType, FareDetail> fareDetailMap = new HashMap<>();
		if (fare != null && fare.getPaxFares() != null && ArrayUtils.isNotEmpty(fare.getPaxFares().getPaxFare())
				&& ArrayUtils.isNotEmpty(fare.getPaxFares().getPaxFare())) {
			PaxFare[] paxFares = fare.getPaxFares().getPaxFare();
			Arrays.stream(paxFares).forEach(paxFare -> {
				FareDetail fareDetail = new FareDetail();
				String paxType = paxFare.getPaxType();
				if (paxType.equalsIgnoreCase(PAX_TYPE_CHILD)) {
					paxType = PaxType.CHILD.getType();
				}
				fareDetail.setRefundableType(RefundableType.PARTIAL_REFUNDABLE.getRefundableType());
				fareDetail.setClassOfBooking(fare.getProductClass());
				fareDetail.setSeatRemaining(seatCount);
				fareDetail.setFareBasis(fare.getFareBasisCode());
				fareDetail.setCabinClass(CabinClass.ECONOMY);
				BookingServiceCharge[] serviceCharges = paxFare.getServiceCharges().getBookingServiceCharge();
				setPaxWiseFareBreakUpV4(fareDetail, paxFare, 1, serviceCharges);
				fareDetailMap.put(PaxType.getPaxType(paxType), fareDetail);
			});

		} else {
			fareDetailMap.putAll(getDefaultFareDetail(fare, seatCount, paxCountInBooking));
		}
		return fareDetailMap;
	}

	private Map<PaxType, FareDetail> getDefaultFareDetail(Fare fare, int seatCount,
			Map<PaxType, Integer> paxCountInBooking) {
		Map<PaxType, FareDetail> fareDetailMap = new HashMap<>();
		if (MapUtils.isNotEmpty(paxCountInBooking)) {
			for (Entry<PaxType, Integer> entry : paxCountInBooking.entrySet()) {
				PaxType paxType = entry.getKey();
				Integer count = entry.getValue();
				if (count > 0) {
					FareDetail fareDetail = new FareDetail();
					fareDetail.setFareComponents(new HashMap<>());
					fareDetail.setClassOfBooking(fare.getProductClass());
					fareDetail.setSeatRemaining(seatCount);
					fareDetail.setFareBasis(fare.getFareBasisCode());
					fareDetail.setCabinClass(CabinClass.getEnumFromCode(fare.getTravelClassCode()));
					fareDetailMap.put(paxType, fareDetail);
				}
			}
		}
		return fareDetailMap;
	}

	private void removeExtraSSRFromPax(List<TripInfo> tripInfos) {
		tripInfos.forEach(tripInfo -> {
			for (int segmentIndex = 0; segmentIndex < tripInfo.getSegmentInfos().size(); segmentIndex++) {
				SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(segmentIndex);
				List<FlightTravellerInfo> flightTravellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
				for (FlightTravellerInfo travellerInfo : flightTravellerInfos) {
					List<SegmentInfo> subLegSegments = getSubLegSegments(tripInfo.getSegmentInfos(), segmentIndex);
					String segmentKey = StringUtils.join(subLegSegments.get(0).getDepartureAirportCode(),
							subLegSegments.get(subLegSegments.size() - 1).getArrivalAirportCode());
					if (CollectionUtils.isNotEmpty(travellerInfo.getSsrBaggageInfos())) {
						List<SSRInformation> filteredBaggage =
								travellerInfo.getSsrBaggageInfos().stream().filter(baggageInfo -> {
									return baggageInfo.getKey().contains(segmentKey);
								}).collect(Collectors.toList());
						if (CollectionUtils.isNotEmpty(filteredBaggage)) {
							filteredBaggage.get(0).setKey(null);
							if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() != 0) {
								filteredBaggage.get(0).setAmount(0.0);
							}
							travellerInfo.setSsrBaggageInfo(filteredBaggage.get(0));
						}
					}

					if (CollectionUtils.isNotEmpty(travellerInfo.getSsrSeatInfos())) {
						if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
							List<SSRInformation> filteredSeat =
									travellerInfo.getSsrSeatInfos().stream().filter(seatInfo -> {
										return seatInfo.getKey().contains(segmentKey);
									}).collect(Collectors.toList());
							if (CollectionUtils.isNotEmpty(filteredSeat)) {
								filteredSeat.get(0).setKey(null);
								travellerInfo.setSsrSeatInfo(filteredSeat.get(0));
							}
						}
					}

					if (CollectionUtils.isNotEmpty(travellerInfo.getSsrMealInfos())) {
						String legKey = StringUtils.join(segmentInfo.getDepartureAirportCode(),
								segmentInfo.getArrivalAirportCode());
						List<SSRInformation> filteredMeal =
								travellerInfo.getSsrMealInfos().stream().filter(mealInfo -> {
									return mealInfo.getKey().contains(legKey);
								}).collect(Collectors.toList());
						if (CollectionUtils.isNotEmpty(filteredMeal)) {
							filteredMeal.get(0).setKey(null);
							travellerInfo.setSsrMealInfo(filteredMeal.get(0));
						}
					}
				}
			}
		});
	}

	private void unsetSSRinfos(List<TripInfo> tripInfos) {
		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (segmentInfo.getBookingRelatedInfo() != null
						&& CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
					List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
					travellerInfos.forEach(travellerInfo -> {
						travellerInfo.setSsrSeatInfos(null);
						travellerInfo.setSsrMealInfos(null);
						travellerInfo.setSsrBaggageInfos(null);
					});
				}
			});
		});
	}

	private void setSeatInfosToTravellers(Booking bookingDetails, List<TripInfo> tripInfos, String confirmationCode) {
		try {
			Journey[] journeys = bookingDetails.getJourneys().getJourney();
			AtomicInteger tripIndex = new AtomicInteger(0);
			Arrays.stream(journeys).forEach(journey -> {
				AtomicInteger segmentIndex = new AtomicInteger(0);
				Segment[] segments = journey.getSegments().getSegment();
				Arrays.stream(segments).forEach(segment -> {
					SegmentInfo segmentInfo = tripInfos.get(tripIndex.get()).getSegmentInfos().get(segmentIndex.get());
					List<SegmentInfo> subLegSegments =
							getSubLegSegments(tripInfos.get(tripIndex.get()).getSegmentInfos(), segmentIndex.get());
					ArrayOfPaxSeat paxSeat = segment.getPaxSeats();
					if (paxSeat != null && ArrayUtils.isNotEmpty(paxSeat.getPaxSeat())) {
						List<FlightTravellerInfo> travellerInfos = segmentInfo.getTravellerInfo();
						Arrays.stream(paxSeat.getPaxSeat()).forEach(paxSeat1 -> {
							if (paxSeat1.getDepartureStation()
									.equalsIgnoreCase(subLegSegments.get(0).getDepartureAirportCode())
									&& paxSeat1.getArrivalStation().equalsIgnoreCase(
											subLegSegments.get(subLegSegments.size() - 1).getArrivalAirportCode())) {
								Integer paxID = Integer.valueOf(paxSeat1.getPassengerNumber());
								for (FlightTravellerInfo travellerInfo : travellerInfos) {
									if (travellerInfo.getId() != null && travellerInfo.getPaxType() != PaxType.INFANT
											&& travellerInfo.getSsrSeatInfo() != null
											&& travellerInfo.getId().intValue() == paxID) {
										SSRInformation ssrSeatInfo = travellerInfo.getSsrSeatInfo();
										ssrSeatInfo.setCode(paxSeat1.getUnitDesignator());
										ssrSeatInfo.setDesc(paxSeat1.getUnitDesignator());
										travellerInfo.setSsrSeatInfo(ssrSeatInfo);
									}
								}
							}
						});
					}
					segmentIndex.getAndIncrement();
				});
				tripIndex.getAndIncrement();
			});
		} catch (Exception e) {
			log.error("Unable to add seat to pax {}", confirmationCode, e);
		}
	}

	public List<FlightTravellerInfo> getTripTravaller(Passenger[] passengers, String confirmationCode,
			List<TripInfo> tripInfos) {
		List<FlightTravellerInfo> travellerInfos = new ArrayList<>();
		if (ArrayUtils.isNotEmpty(passengers)) {
			long paxIndex = 0;
			long infantIndex = 0;
			for (Passenger passenger : passengers) {
				FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
				travellerInfo.setPnr(confirmationCode);
				travellerInfo.setId(paxIndex++);
				travellerInfo.setFirstName(passenger.getNames().getBookingName()[0].getFirstName());
				travellerInfo.setLastName(passenger.getNames().getBookingName()[0].getLastName());
				travellerInfo.setTitle(passenger.getNames().getBookingName()[0].getTitle());
				travellerInfo.setPaxType(getPaxType(passenger.getPassengerTypeInfo().getPaxType()));
				travellerInfo.setDob(AirAsiaUtils.getRetreieveDOB(passenger));
				setTravelDocumentDetails(passenger, travellerInfo);
				if (passenger.getPassengerFees() != null
						&& ArrayUtils.isNotEmpty(passenger.getPassengerFees().getPassengerFee())) {
					// SSR Functionality
					PassengerFee[] passengerFees = passenger.getPassengerFees().getPassengerFee();
					Arrays.stream(passengerFees).forEach(passengerFee -> {
						SegmentInfo segmentInfo = getSegmentByKey(tripInfos, passengerFee.getFlightReference());
						FlightBasicFact fact = AirAsiaUtils.getSSRFlightFact(segmentInfo, false, bookingUser);
						BaseUtils.createFactOnUser(fact, bookingUser);
						Map<SSRType, List<? extends SSRInformation>> preSSR = AirUtils.getPreSSR(fact);
						SSRInformation baggageSSRInfo =
								AirUtils.getSSRInfo(preSSR.get(SSRType.BAGGAGE), passengerFee.getSSRCode());
						SSRInformation mealSSRInfo =
								AirUtils.getSSRInfo(preSSR.get(SSRType.MEAL), passengerFee.getSSRCode());

						BaggageSSRInformation baggInformation = new BaggageSSRInformation();
						MealSSRInformation mealInformation = new MealSSRInformation();
						SeatSSRInformation seatInformation = new SeatSSRInformation();
						if (baggageSSRInfo != null) {
							baggInformation.setAmount(getSSRAmount(passengerFee.getServiceCharges()));
							baggInformation.setDesc(baggageSSRInfo.getDesc());
							baggInformation.setCode(passengerFee.getSSRCode());
							baggInformation.setKey(passengerFee.getFlightReference());
							travellerInfo.getSsrBaggageInfos().add(baggInformation);
						} else if (mealSSRInfo != null) {
							mealInformation.setCode(passengerFee.getSSRCode());
							mealInformation.setDesc(mealSSRInfo.getDesc());
							mealInformation.setAmount(getSSRAmount(passengerFee.getServiceCharges()));
							mealInformation.setKey(passengerFee.getFlightReference());
							travellerInfo.getSsrMealInfos().add(mealInformation);
						} else if ("SEAT".equals(passengerFee.getFeeCode())) {
							seatInformation.setAmount(getSSRAmount(passengerFee.getServiceCharges()));
							seatInformation.setKey(passengerFee.getFlightReference());
							travellerInfo.getSsrSeatInfos().add(seatInformation);
						}
					});
				}
				if (Objects.nonNull(passenger.getInfant())) {
					FlightTravellerInfo infantTraveller = new FlightTravellerInfo();
					infantTraveller = parsePaxInfo(passenger.getInfant(), confirmationCode);
					infantTraveller.setId(passengers.length + infantIndex);
					setTravelDocumentDetails(passenger, infantTraveller);
					travellerInfos.add(infantTraveller);
					tripInfos.forEach(tripInfo -> {
						for (int segmentIndex = 0; segmentIndex < tripInfo.getSegmentInfos().size(); segmentIndex++) {
							SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(segmentIndex);
							if (airAsiaAirline.isInfantFareOnSegmentWise(segmentInfo)) {
								List<SegmentInfo> subSegments =
										getSubLegSegments(tripInfo.getSegmentInfos(), segmentIndex);
								String segmentKey = StringUtils.join(subSegments.get(0).getDepartureAirportCode(),
										subSegments.get(subSegments.size() - 1).getArrivalAirportCode());
								FareDetail infantFare = getInfantFareFromAdult(passenger, segmentKey);
								Map<PaxType, FareDetail> infantPax = new HashMap<>();
								// Infant fare segment wise.
								if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
									infantPax.put(PaxType.INFANT, infantFare);
								} else {
									infantPax.put(PaxType.INFANT, new FareDetail());
								}
								segmentInfo.getPriceInfo(0).getFareDetails().put(PaxType.INFANT, infantFare);
							} else {
								String tripKey = StringUtils.join(tripInfo.getDepartureAirportCode(),
										tripInfo.getArrivalAirportCode());
								FareDetail infantFare = getInfantFareFromAdult(passenger, tripKey);
								Map<PaxType, FareDetail> infantPax = new HashMap<>();
								infantPax.put(PaxType.INFANT, infantFare);
								segmentInfo.getPriceInfo(0).getFareDetails().put(PaxType.INFANT, infantFare);
							}
						}
					});
					infantIndex++;
				}
				travellerInfos.add(travellerInfo);
			}
		}
		return travellerInfos;
	}

	private void setTravelDocumentDetails(Passenger passenger, FlightTravellerInfo travellerInfo) {
		if (passenger.getPassengerTravelDocuments() != null
				&& ArrayUtils.isNotEmpty(passenger.getPassengerTravelDocuments().getPassengerTravelDocument())) {
			PassengerTravelDocument travelDoc = getFilteredDocument(passenger.getPassengerTravelDocuments(),
					travellerInfo, AirAsiaAirline.PASS_DOC_TYPE_CODE);
			if (travelDoc != null) {
				travellerInfo.setPassportNumber(travelDoc.getDocNumber());
				travellerInfo.setPassportNationality(travelDoc.getNationality());
				travellerInfo.setPassportIssueDate(TgsDateUtils.calenderToLocalDate(travelDoc.getIssuedDate()));
				travellerInfo.setExpiryDate(TgsDateUtils.calenderToLocalDate(travelDoc.getExpirationDate()));
				travellerInfo.setPassportNationality(passenger.getPassengerInfo().getNationality());
			}
			PassengerTravelDocument ffTravelDoc = getFilteredDocument(passenger.getPassengerTravelDocuments(),
					travellerInfo, AirAsiaAirline.FF_DOC_TYPE_CODE);
			if (ffTravelDoc != null) {
				if (travellerInfo.getFrequentFlierMap() == null
						|| MapUtils.isEmpty(travellerInfo.getFrequentFlierMap())) {
					travellerInfo.setFrequentFlierMap(new HashMap<>());
				}
				travellerInfo.getFrequentFlierMap().put(carrierCode, ffTravelDoc.getDocNumber());
			}
		}
	}

	private PassengerTravelDocument getFilteredDocument(ArrayOfPassengerTravelDocument passengerTravelDocuments,
			FlightTravellerInfo travellerInfo, String doctTypeCode) {
		for (PassengerTravelDocument travelDocument : passengerTravelDocuments.getPassengerTravelDocument()) {
			String passengerName = AirAsiaUtils.getPassengerName(travelDocument.getNames().getBookingName()[0]);
			String travellerName = StringUtils.join(travellerInfo.getFirstName(), travellerInfo.getLastName());
			if (StringUtils.isNotEmpty(travelDocument.getDocTypeCode())
					&& travelDocument.getDocTypeCode().equalsIgnoreCase(doctTypeCode)
					&& passengerName.equalsIgnoreCase(travellerName)) {
				return travelDocument;
			}
		}
		return null;
	}


	private FareDetail getInfantFareFromAdult(Passenger pax, String keyToMatch) {
		FareDetail infantFare = new FareDetail();
		if (pax.getPassengerFees() != null && ArrayUtils.isNotEmpty(pax.getPassengerFees().getPassengerFee())) {
			for (PassengerFee paxFee : pax.getPassengerFees().getPassengerFee()) {
				if (paxFee.getFeeCode().equalsIgnoreCase(PAX_TYPE_INFANT)
						&& paxFee.getFlightReference().contains(keyToMatch)) {
					PaxFare paxFare = new PaxFare();
					paxFare.setPaxType(PAX_TYPE_INFANT);
					infantFare = parsePaxWiseFareBreakUp(infantFare, paxFare, PAX_TYPE_INFANT, 1,
							paxFee.getServiceCharges().getBookingServiceCharge());
				}
			}
		}
		return infantFare;
	}

	private FlightTravellerInfo parsePaxInfo(PassengerInfant infant, String confirmationCode) {
		FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
		travellerInfo.setFirstName(infant.getNames().getBookingName()[0].getFirstName());
		travellerInfo.setLastName(infant.getNames().getBookingName()[0].getLastName());
		travellerInfo.setTitle(infant.getNames().getBookingName()[0].getTitle());
		travellerInfo.setDob(TgsDateUtils.calenderToLocalDate(infant.getDOB()));
		travellerInfo.setPaxType(PaxType.INFANT);
		travellerInfo.setPnr(confirmationCode);
		return travellerInfo;
	}

	public double getSSRAmount(ArrayOfBookingServiceCharge serviceCharges) {
		double amount = 0;
		if (serviceCharges != null && ArrayUtils.isNotEmpty(serviceCharges.getBookingServiceCharge())) {
			for (BookingServiceCharge serviceCharge : serviceCharges.getBookingServiceCharge()) {
				if (!serviceCharge.getChargeType().toString().toLowerCase().contains("includedtax")) {
					if (serviceCharge.getChargeType().toString().toLowerCase().contains("discount")
							&& serviceCharge.getAmount().doubleValue() < 0) {
						serviceCharge.setAmount(serviceCharge.getAmount().abs());
					}
					amount += getAmountBasedOnCurrency(serviceCharge.getAmount(), serviceCharge.getCurrencyCode());
				}
			}
		}
		return amount;
	}

	private Map<PaxType, Integer> getPaxCountInBooking(Booking bookingInfo) {
		Map<PaxType, Integer> paxTypes = new HashMap<>();
		if (bookingInfo.getPassengers() != null && ArrayUtils.isNotEmpty(bookingInfo.getPassengers().getPassenger())) {
			Passenger[] passengers = bookingInfo.getPassengers().getPassenger();
			for (Passenger passenger : passengers) {
				PaxType paxType = getPaxType(passenger.getPassengerTypeInfo().getPaxType());
				Integer paxCount = paxTypes.getOrDefault(paxType, 0);
				paxTypes.put(paxType, paxCount + 1);
				if (Objects.nonNull(passenger.getInfant())) {
					PaxType infantType = PaxType.INFANT;
					Integer infantCount = paxTypes.getOrDefault(infantType, 0);
					paxTypes.put(infantType, infantCount + 1);
				}
			}
		}
		return paxTypes;
	}

	private static void resetTravellerId(List<TripInfo> tripInfos) {
		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				for (FlightTravellerInfo traveller : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					traveller.setId(null);
				}
			}
		}
	}

	private SegmentInfo getSegmentByKey(List<TripInfo> tripInfos, String key) {
		for (TripInfo tripInfo : tripInfos) {
			for (int segmentIndex = 0; segmentIndex < tripInfo.getSegmentInfos().size(); segmentIndex++) {
				List<SegmentInfo> subLegSegments = getSubLegSegments(tripInfo.getSegmentInfos(), segmentIndex);
				String segmentKey = StringUtils.join(subLegSegments.get(0).getDepartureAirportCode(),
						subLegSegments.get(subLegSegments.size() - 1).getArrivalAirportCode());
				if (key.equals(segmentKey)) {
					return tripInfo.getSegmentInfos().get(segmentIndex);
				}
			}
		}
		return null;
	}

}
