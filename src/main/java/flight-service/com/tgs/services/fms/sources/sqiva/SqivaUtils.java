package com.tgs.services.fms.sources.sqiva;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import java.time.Duration;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class SqivaUtils {

	public static LocalDateTime convertIntoLocalDateTime(String date, String time) {
		String finalDate = date + " " + time;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HHmm");
		LocalDateTime dateTime = LocalDateTime.parse(finalDate, formatter);
		return dateTime;
	}

	public static LocalDateTime convertToLocalDateTime(String format, final String datwWithTime) {
		DateTimeFormatter formatter =
				new DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern(format).toFormatter();
		LocalDateTime dateTime = LocalDateTime.parse(datwWithTime, formatter);
		return dateTime;
	}

	public static String localDateTimeToSqivaFormat(LocalDateTime localDateTime) {
		return localDateTime.toLocalDate().toString().replace("-", "");
	}

	public static String splitDateAndTime(String dateWithTime) {
		dateWithTime = dateWithTime.replace("-", "");
		String[] finalDepDate = dateWithTime.split("T");
		return finalDepDate[0];
	}

	public static String getFlightNumberWithCode(TripInfo tripInfo) {
		List<String> flightNumbers = new ArrayList<>();
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
				flightNumbers.add(segmentInfo.getFlightDesignator().getFlightNumber());
			}
		});
		String airlinceCode = tripInfo.getAirlineCode();
		StringJoiner joiner = new StringJoiner(",");
		flightNumbers.stream().forEach(ac -> joiner.add(StringUtils.join(airlinceCode, ac)));
		return joiner.toString();
	}

	public static void populateTravellerInfo(List<FlightTravellerInfo> travellers, Map<String, String> bookingParams,
			String prefix) {
		int paxCount = 1;
		if (CollectionUtils.isNotEmpty(travellers)) {
			bookingParams.put(StringUtils.join("num_pax_", travellers.get(0).getPaxType().name().toLowerCase()),
					String.valueOf(travellers.size()));
			for (FlightTravellerInfo traveller : travellers) {

				bookingParams.put(getTravellerKey(prefix, "_first_name_", paxCount), traveller.getFirstName());
				bookingParams.put(getTravellerKey(prefix, "_last_name_", paxCount), traveller.getLastName());
				bookingParams.put(getTravellerKey(prefix, "_salutation_", paxCount), getSalutaion(traveller));

				if (StringUtils.isNotBlank(traveller.getPassportNumber())) {
					bookingParams.put(getTravellerKey(prefix, "_passport_", paxCount), traveller.getPassportNumber());
					if (traveller.getExpiryDate() != null) {
						bookingParams.put(getTravellerKey(prefix, "_passport_exp_", paxCount),
								traveller.getExpiryDate().toString().replace("-", ""));
					}
					bookingParams.put(getTravellerKey(prefix, "_nationality_", paxCount),
							traveller.getPassportNationality());
				}

				if (PaxType.ADULT.equals(traveller.getPaxType())) {
					if (paxCount == 1) {
						bookingParams.put(getTravellerKey(prefix, "_mobile_", paxCount),
								bookingParams.get(SqivaConstant.CONTACT_1));
					}
					if (traveller.getDob() != null) {
						bookingParams.put(getTravellerKey(prefix, "_birthdate_", paxCount),
								traveller.getDob().toString().replace("-", ""));
					}
				} else if (PaxType.CHILD.equals(traveller.getPaxType())) {
					LocalDate dob = traveller.getDob();
					if (dob == null) {
						dob = LocalDate.now().minusYears(10);
					}
					bookingParams.put(getTravellerKey(prefix, "_birthdate_", paxCount),
							dob.toString().replace("-", ""));
				} else {
					if (traveller.getDob() != null) {
						bookingParams.put(getTravellerKey(prefix, "_birthdate_", paxCount),
								traveller.getDob().toString().replace("-", ""));
					}
					bookingParams.put(getTravellerKey(prefix, "_parent_", paxCount), String.valueOf(paxCount));
				}
				paxCount++;
			}
		}
	}

	private static String getTravellerKey(String prefix, String param, int paxCount) {
		return StringUtils.join(prefix, param, paxCount);
	}

	public static String getSalutaion(FlightTravellerInfo paxInfo) {
		return PaxType.ADULT.equals(paxInfo.getPaxType()) ? paxInfo.getTitle().toUpperCase()
				: StringUtils.equalsIgnoreCase(paxInfo.getTitle(), "MASTER") ? "MSTR" : "MISS";
	}

	public static String getSubClass(TripInfo tripInfo) {
		StringJoiner stringJoiner = new StringJoiner(",");
		tripInfo.getSegmentInfos().forEach(segment -> {
			if (segment.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
				String classOfBooking = segment.getTravellerInfo().get(0).getFareDetail().getClassOfBooking();
				String fareBasis = segment.getTravellerInfo().get(0).getFareDetail().getFareBasis();
				String subClass = StringUtils.join(classOfBooking, "/", fareBasis);
				stringJoiner.add(subClass);
			}
		});
		return stringJoiner.toString();
	}

	public static List<TripInfo> buildCombination(List<TripInfo> onwardTrips, List<TripInfo> returnTrips,
			Integer sourceId) {
		List<TripInfo> combinedTrips = new ArrayList<>();
		for (TripInfo onwardTrip : onwardTrips) {
			for (TripInfo returnTrip : returnTrips) {
				LocalDateTime onwardArrivalTime = onwardTrip.getArrivalTime();
				LocalDateTime returnDepartureTime = returnTrip.getDepartureTime();
				User user = SystemContextHolder.getContextData().getUser();
				if (Duration.between(onwardArrivalTime, returnDepartureTime).toMinutes() > AirUtils
						.getCombinationAllowedMinutes(sourceId, user)) {
					TripInfo copyOnwardTrip = new GsonMapper<>(onwardTrip, TripInfo.class).convert();
					TripInfo copyReturnTrip = new GsonMapper<>(returnTrip, TripInfo.class).convert();
					TripInfo originalTrip = new TripInfo();
					originalTrip.getSegmentInfos().addAll(copyOnwardTrip.getSegmentInfos());
					originalTrip.getSegmentInfos().addAll(copyReturnTrip.getSegmentInfos());
					combinedTrips.add(originalTrip);
				}
			}
		}
		return combinedTrips;
	}

	public static boolean isAllPriceOptionsAreSR(TripInfo tripInfo) {
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
			if (!FareType.SPECIAL_RETURN.getName().equals(priceInfo.getFareIdentifier())) {
				return false;
			}
		}
		return true;
	}

	public static int getNextLegNum(List<SegmentInfo> segmentInfos, int segmentIndex) {
		return (segmentIndex + 1 < segmentInfos.size())
				? segmentInfos.get(segmentIndex + 1).getPriceInfo(0).getMiscInfo().getLegNum()
				: 0;
	}

	public static TripInfo filterResults(AirSearchResult searchResult, TripInfo oldTrip) {
		TripInfo newTrip = filterTripFromAirSearchResult(searchResult, oldTrip);
		if (newTrip != null) {
			AtomicInteger segmentIndex = new AtomicInteger(0);
			oldTrip.getSegmentInfos().forEach(oldSegmentInfo -> {
				String fareIdentifier = oldSegmentInfo.getPriceInfo(0).getFareType();
				String oldFBA = oldSegmentInfo.getPriceInfo(0).getFareBasis(PaxType.ADULT);
				CabinClass cabinClass = oldSegmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT).getCabinClass();
				SegmentInfo newSegmentInfo = newTrip.getSegmentInfos().get(segmentIndex.get());
				List<PriceInfo> filteredPriceList = newSegmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
					return (StringUtils.isNotEmpty(priceInfo.getFareType())
							&& priceInfo.getFareIdentifier().equals(fareIdentifier))
							&& (priceInfo.getFareDetail(PaxType.ADULT).getCabinClass().equals(cabinClass)
									&& priceInfo.getFareBasis(PaxType.ADULT).equalsIgnoreCase(oldFBA));
				}).collect(Collectors.toList());
				if (CollectionUtils.isNotEmpty(filteredPriceList)
						&& AirUtils.matchSegmentInfo(newSegmentInfo, oldSegmentInfo)) {
					List<PriceInfo> newPriceInfos = new ArrayList<>();
					newPriceInfos.add(filteredPriceList.get(0));
					newSegmentInfo.setPriceInfoList(newPriceInfos);
				}
				segmentIndex.getAndIncrement();
			});
		}
		return newTrip;
	}

	public static List<TripInfo> buildCombination(List<TripInfo> onwardTrips, List<TripInfo> returnTrips,
			Integer sourceId, User bookingUser) {
		List<TripInfo> combinedTrips = new ArrayList<>();
		for (TripInfo onwardTrip : onwardTrips) {
			for (TripInfo returnTrip : returnTrips) {
				LocalDateTime onwardArrivalTime = onwardTrip.getArrivalTime();
				LocalDateTime returnDepartureTime = returnTrip.getDepartureTime();
				if (Duration.between(onwardArrivalTime, returnDepartureTime).toMinutes() > AirUtils
						.getCombinationAllowedMinutes(sourceId, bookingUser)) {
					TripInfo copyOnwardTrip = new GsonMapper<>(onwardTrip, TripInfo.class).convert();
					TripInfo copyReturnTrip = new GsonMapper<>(returnTrip, TripInfo.class).convert();
					if (onwardTrip.isPriceInfosNotEmpty() && returnTrip.isPriceInfosNotEmpty()) {
						int onwardPriceSize = onwardTrip.getSegmentInfos().get(0).getPriceInfoList().size();
						int returnPriceSize = returnTrip.getSegmentInfos().get(0).getPriceInfoList().size();
						for (int segmentIndex = 0; segmentIndex < copyOnwardTrip.getSegmentInfos()
								.size(); segmentIndex++) {
							copyOnwardTrip.getSegmentInfos().get(segmentIndex).setPriceInfoList(new ArrayList<>());
							for (int priceIndex = 0; priceIndex < onwardTrip.getSegmentInfos().get(segmentIndex)
									.getPriceInfoList().size(); priceIndex++) {
								for (int index = 0; index < returnPriceSize; index++) {
									PriceInfo priceInfo =
											new GsonMapper<PriceInfo>(onwardTrip.getSegmentInfos().get(segmentIndex)
													.getPriceInfoList().get(priceIndex), PriceInfo.class).convert();
									copyOnwardTrip.getSegmentInfos().get(segmentIndex).getPriceInfoList()
											.add(priceInfo);
								}
							}
						}
						for (int index = 0; index < onwardPriceSize - 1; index++) {
							for (int segmentIndex = 0; segmentIndex < copyReturnTrip.getSegmentInfos()
									.size(); segmentIndex++) {
								copyReturnTrip.getSegmentInfos().get(segmentIndex).getPriceInfoList()
										.addAll(GsonUtils.getGson().fromJson(
												GsonUtils.getGson()
														.toJson(returnTrip.getSegmentInfos().get(segmentIndex)
																.getPriceInfoList()),
												new TypeToken<List<PriceInfo>>() {}.getType()));
							}
						}
						copyReturnTrip.getSegmentInfos().get(0).setIsReturnSegment(true);
						copyOnwardTrip.getSegmentInfos().addAll(copyReturnTrip.getSegmentInfos());
						combinedTrips.add(copyOnwardTrip);
					}
				}
			}
		}
		return combinedTrips;
	}

	private static TripInfo filterTripFromAirSearchResult(AirSearchResult searchResult, TripInfo oldTrip) {
		if (searchResult == null || MapUtils.isEmpty(searchResult.getTripInfos())) {
			return null;
		}
		for (Entry<String, List<TripInfo>> entry : searchResult.getTripInfos().entrySet()) {
			for (TripInfo newTripInfo : entry.getValue()) {
				if (newTripInfo.getSegmentInfos().size() == oldTrip.getSegmentInfos().size()) {
					int i = 0;
					for (; i < newTripInfo.getSegmentInfos().size(); i++) {
						if (!AirUtils.matchSegmentInfo(newTripInfo.getSegmentInfos().get(i),
								oldTrip.getSegmentInfos().get(i))) {
							break;
						}
					}
					if (i == newTripInfo.getSegmentInfos().size()) {
						TripInfo trip = unsetRedudantPrice(newTripInfo, oldTrip);
						if (trip != null) {
							return trip;
						}
					}
				}
			}
		}
		return null;
	}

	private static TripInfo unsetRedudantPrice(TripInfo newTrip, TripInfo oldTrip) {
		for (int i = 0; i < newTrip.getSegmentInfos().size(); i++) {
			SegmentInfo newSegmentInfo = newTrip.getSegmentInfos().get(i);
			SegmentInfo oldSegmentInfo = oldTrip.getSegmentInfos().get(i);
			if (AirUtils.matchSegmentInfo(newSegmentInfo, oldSegmentInfo)) {
				String fareIdentifier = oldSegmentInfo.getPriceInfo(0).getFareType();
				CabinClass cabinClass = oldSegmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT).getCabinClass();
				Integer sourceId = oldSegmentInfo.getPriceInfo(0).getSourceId();
				List<PriceInfo> filteredPriceList = newSegmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
					return (StringUtils.isNotEmpty(priceInfo.getFareType())
							&& priceInfo.getFareIdentifier().equals(fareIdentifier))
							&& (priceInfo.getFareDetail(PaxType.ADULT).getCabinClass().equals(cabinClass)
									&& priceInfo.getSupplierBasicInfo().getSourceId() == sourceId);
				}).collect(Collectors.toList());
				if (CollectionUtils.isNotEmpty(filteredPriceList)) {
					newSegmentInfo.setPriceInfoList(filteredPriceList);
				} else {
					return null;
				}
			} else {
				return null;
			}
		}
		return newTrip;
	}

}
