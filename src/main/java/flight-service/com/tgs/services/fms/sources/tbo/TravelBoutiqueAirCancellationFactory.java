package com.tgs.services.fms.sources.tbo;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.service.tbo.datamodel.getBookingDetails.BookingDetailsResBody;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TravelBoutiqueAirCancellationFactory extends AbstractAirCancellationFactory {


	protected RestAPIListener listener = null;

	protected String tokenId;

	protected FlightAPIURLRuleCriteria apiURLS;


	public TravelBoutiqueAirCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		super(bookingSegments, order, supplierConf);
	}

	public void initialize() {
		apiURLS = getEndPointURL();
	}

	@Override
	public boolean releaseHoldPNR() {
		initialize();
		boolean isCancelSuccess = false;
		listener = new RestAPIListener(bookingId);

		try {
			TravelBoutiqueAuthenticationManager loginServiceManager =
					TravelBoutiqueAuthenticationManager.builder().supplierConfiguration(supplierConf).apiURLS(apiURLS)
							.uniqueId(bookingId).listener(listener).bookingUser(bookingUser).build();
			tokenId = loginServiceManager.login().getTokenId();
			TravelBoutiqueBookingDetailManager bookingDetailManager =
					TravelBoutiqueBookingDetailManager.builder().supplierConfiguration(supplierConf).pnr(pnr)
							.bookingId(bookingId).supplierBookingId(bookingSegments.getSupplierBookingId())
							.tokenId(tokenId).apiURLS(apiURLS).listener(listener).bookingUser(bookingUser).build();
			BookingDetailsResBody bookingDetails = bookingDetailManager.getBookingDetails();
			if (bookingDetails != null
					&& !bookingDetailManager.isCriticalException(bookingDetails.getResponse().getError(),
							bookingDetails.getResponse().getResponseStatus())) {
				TravelBoutiqueAirCancellationManager cancellationManager =
						TravelBoutiqueAirCancellationManager.builder().bookingId(bookingId)
								.source(bookingDetails.getResponse().getFlightItinerary().getSource()).tokenId(tokenId)
								.pnr(bookingSegments.getSupplierBookingId()).apiURLS(apiURLS).listener(listener)
								.criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).build();
				isCancelSuccess = cancellationManager.cancelPnr();
			}
		} catch (IOException e) {
			log.info("SupplierRemote Exception {}", bookingSegments.getSupplierBookingId());
			throw new SupplierRemoteException(e.getMessage());
		} finally {

		}
		return isCancelSuccess;

	}

}
