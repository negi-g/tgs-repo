package com.tgs.services.fms.sources.airarabia;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.utils.AirSupplierUtils;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.opentravel.www.OTA._2003._05.AAResWebServicesHttpBindingStub;
import org.opentravel.www.OTA._2003._05.AAResWebServices_ServiceLocator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.CacheType;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Setter
@Slf4j
@Builder
public class AirArabiaBindingService {

	private GeneralCachingCommunicator cacheCommunicator;

	protected List<SupplierAnalyticsQuery> supplierAnalyticsInfos;

	public AAResWebServicesHttpBindingStub getAAResWebServiceStub(SupplierConfiguration configuration) {
		AAResWebServicesHttpBindingStub bindingStub = null;
		try {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, AAResWebServicesHttpBindingStub.class, null);
			metaInfo = cacheCommunicator.fetchFromQueue(metaInfo);
			if (metaInfo != null) {
				bindingStub = (AAResWebServicesHttpBindingStub) metaInfo.getValue();
			}

			if (bindingStub == null) {
				URL url = new URL(configuration.getSupplierCredential().getUrl());
				bindingStub = (AAResWebServicesHttpBindingStub) new AAResWebServices_ServiceLocator()
						.getAAResWebServicesHttpPort(url);
			}
		} catch (Exception e) {
			log.error("Binding Initilaized failed ", e);
		}
		return bindingStub;
	}

	private static String getKey(SupplierConfiguration configuration, String type) {
		SupplierBasicInfo basicInfo = configuration.getBasicInfo();
		return StringUtils.join("-", basicInfo.getRuleId(), type);
	}

	private static SessionMetaInfo buildMetaInfo(SupplierConfiguration configuration, Class classx, Object value) {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(getKey(configuration, classx.toString())).value(value)
				.expiryTime(LocalDateTime.now().plusMinutes(240)).cacheType(CacheType.INMEMORY).build();
		metaInfo.setIndex(0);
		return metaInfo;
	}

	@SuppressWarnings("rawtypes")
	public void storeSessionMetaInfo(SupplierConfiguration configuration, Class classx, Object value) {
		if (value != null) {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, classx, value);
			cacheCommunicator.storeInQueue(metaInfo);
		}
	}

}
