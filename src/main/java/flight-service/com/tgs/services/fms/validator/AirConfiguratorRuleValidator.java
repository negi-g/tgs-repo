package com.tgs.services.fms.validator;

import org.springframework.validation.Errors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Validator;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.BasicRuleCriteria;
import com.tgs.services.base.validator.rule.FlightBasicRuleCriteriaValidator;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;

@Service
public class AirConfiguratorRuleValidator implements Validator {

	@Autowired
	FlightBasicRuleCriteriaValidator flightBasicRuleCriteriaValidator;

	@Autowired
	AirGeneralPurposeOutputValidator generalPurposeValidator;

	@Autowired
	AirSourceConfigurationOutputValidator sourceConfigValidator;

	@Autowired
	AirSSRInfoOutputValidator ssrInfoValidator;

	@Autowired
	AirFareTypeConfiguratorValidator ftConfigValidator;

	@Autowired
	AirCancellationProtectionOutputValidator cancellationValidator;

	@Autowired
	ClientMarkUpAndFeeOutputValidator clientMarkUpAndFeeValidator;

	@Autowired
	HoldFeeRuleValidator holdFeeRuleValidator;

	@Autowired
	AirSSRFilterConfigurationValidator ssrFilterConfigurationValidator;

	@Autowired
	AirCacheTTLConfigurationValidator ttlConfigurationValidator;

	@Autowired
	AirSupplierExclusionValidator supplierExclusionValidator;

	@Autowired
	FlightCacheTTLRuleValidator flightCacheTTLRuleValidator;

	@Autowired
	RestrictiveFareTypeValidator restrictiveFareTypeValidator;

	@Autowired
	AirHoldConfigValidator holdConfigValidator;

	@Autowired
	UnmaskFareRuleValidator unmaskFareRuleValidator;
	
	@Autowired
	SsrOsiValidator ssrOsiValidator;

	@Autowired
	ChangeFareTypeRuleValidator changeFareTypeRuleValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		AirConfiguratorInfo airconfiguratorInfo = (AirConfiguratorInfo) target;

		if (airconfiguratorInfo.getRuleType() == null) {
			rejectValue(errors, "ruleType", SystemError.INVALID_RULE);

		} else {

			if (airconfiguratorInfo.getOutput() == null) {
				rejectValue(errors, "output", SystemError.EMPTY_CONFIGURATOR_OUTPUT);
			}

			if (AirConfiguratorRuleType.GNPUPROSE.equals(airconfiguratorInfo.getRuleType())) {
				generalPurposeValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.SOURCECONFIG.equals(airconfiguratorInfo.getRuleType())) {
				sourceConfigValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.SSR_INFO.equals(airconfiguratorInfo.getRuleType())) {
				ssrInfoValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.FARE_TYPE.equals(airconfiguratorInfo.getRuleType())) {
				ftConfigValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.CANCELLATION_PROTECTION.equals(airconfiguratorInfo.getRuleType())) {
				cancellationValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.CLIENTMARKUP.equals(airconfiguratorInfo.getRuleType())
					|| AirConfiguratorRuleType.CLIENTFEE.equals(airconfiguratorInfo.getRuleType())) {
				clientMarkUpAndFeeValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.SSR_FILTER.equals(airconfiguratorInfo.getRuleType())) {
				ssrFilterConfigurationValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.HOLD_FEE.equals(airconfiguratorInfo.getRuleType())) {
				holdFeeRuleValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.CACHE_TTL.equals(airconfiguratorInfo.getRuleType())) {
				ttlConfigurationValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.SUPPLIER_EXCLUSION.equals(airconfiguratorInfo.getRuleType())) {
				supplierExclusionValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.CACHE_FILTER.equals(airconfiguratorInfo.getRuleType())) {
				flightCacheTTLRuleValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.RESTRICTIVE_FARE_TYPE.equals(airconfiguratorInfo.getRuleType())) {
				restrictiveFareTypeValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.HOLD_CONFIG.equals(airconfiguratorInfo.getRuleType())) {
				holdConfigValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.UNMASK_FARE.equals(airconfiguratorInfo.getRuleType())) {
				unmaskFareRuleValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			}else if (AirConfiguratorRuleType.SSROSI_CONFIG.equals(airconfiguratorInfo.getRuleType())) {
				ssrOsiValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());
			} else if (AirConfiguratorRuleType.CHANGE_FARE_TYPE.equals(airconfiguratorInfo.getRuleType())) {
				changeFareTypeRuleValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());

			}


		}
		flightBasicRuleCriteriaValidator.validateCriteria(errors, "inclusionCriteria",
				(BasicRuleCriteria) airconfiguratorInfo.getInclusionCriteria());
		flightBasicRuleCriteriaValidator.validateCriteria(errors, "exclusionCriteria",
				(BasicRuleCriteria) airconfiguratorInfo.getExclusionCriteria());

	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}

}
