package com.tgs.services.fms.sources;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.manager.AirSearchResultProcessingManager;
import com.tgs.services.fms.manager.TripPriceEngine;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.oms.restmodel.AirCreditShellRequest;
import com.tgs.services.oms.restmodel.AirCreditShellResponse;
import com.tgs.services.oms.restmodel.air.AirImportPnrBookingRequest;
import com.tgs.services.oms.restmodel.air.RetrieveTicketStatusRequest;
import com.tgs.services.oms.restmodel.air.RetrieveTicketStatusResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.air.ImportPNRNotAllowedException;
import com.tgs.utils.exception.air.NoPNRFoundException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Service
@NoArgsConstructor
public abstract class AbstractAirBookingRetrieveFactory {

	@Autowired
	AirSearchResultProcessingManager processingManager;

	protected SupplierConfiguration supplierConf;

	protected AirImportPnrBooking pnrBooking;

	protected String pnr;

	protected String accountCode;

	protected AirFlowType flowType;

	protected User bookingUser;

	@Autowired
	protected GeneralCachingCommunicator cachingCommunicator;

	protected AirSourceConfigurationOutput sourceConfiguration;

	@Autowired
	protected UserServiceCommunicator userServiceCommunicator;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchangeComm;

	public AbstractAirBookingRetrieveFactory(SupplierConfiguration supplierConf, String pnr) {
		this.supplierConf = supplierConf;
		this.pnr = pnr;
	}

	public AirImportPnrBooking retrieveBooking(ContextData contextData, AirImportPnrBookingRequest pnrRequest) {
		try {
			bookingUser = userServiceCommunicator.getUserFromCache(pnrRequest.getBookingUserId());
			sourceConfiguration = AirUtils.getAirSourceConfiguration(null, supplierConf.getBasicInfo(), bookingUser);
			AirGeneralPurposeOutput gnSourceRule =
					AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
			accountCode = pnrRequest.getAccountCode();
			pnrBooking = this.retrievePNRBooking();

			if (pnrBooking != null && CollectionUtils.isNotEmpty(pnrBooking.getTripInfos())) {
				updateBookingStatus();
				AirSearchQuery searchQuery =
						AirUtils.combineSearchQuery(AirUtils.getSearchQueryFromTripInfos(pnrBooking.getTripInfos()));
				if (searchQuery != null && CollectionUtils.isNotEmpty(pnrBooking.getTripInfos())) {
					pnrBooking.getTripInfos().forEach(trip -> {
						TripPriceEngine.updateCustomFareType(trip, searchQuery);
					});
				}
				if (searchQuery != null
						&& (searchQuery.isIntlReturn() || (searchQuery.isIntl() && searchQuery.isMultiCity()))) {
					// Combine Trips Into Trip for User Commission, MarkUp..
					for (TripInfo tripInfo : pnrBooking.getTripInfos()) {
						sortSegmentInfosOnDepatureTime(tripInfo.getSegmentInfos());
					}
					sortTripInfosOnDepatureTime(pnrBooking.getTripInfos());
					pnrBooking.setTripInfos(combineTripInfos(pnrBooking.getTripInfos()));
				}
				pnrBooking.getTripInfos().forEach(tripInfo -> {
					sortTravellers(tripInfo, searchQuery);
					processingManager.processTripInfo(tripInfo, searchQuery, bookingUser, sourceConfiguration,
							gnSourceRule, AirUtils.isPartnerFlow(bookingUser));
				});
			}
		} catch (NoPNRFoundException | ImportPNRNotAllowedException e) {
			log.info(AirSourceConstants.IMPORT_PNR_FAILED, pnrRequest.getPnr(), pnrRequest.getSupplierId(),
					e.getMessage());
			LogUtils.log(pnrRequest.getPnr(), "ImportPNR", e);
			throw e;
		} catch (SupplierSessionException se) {
			log.error(AirSourceConstants.IMPORT_PNR_FAILED, pnrRequest.getPnr(), pnrRequest.getSupplierId(),
					se.getMessage());
			LogUtils.log(pnrRequest.getPnr(), "ImportPNR", se);
			throw se;
		} catch (SupplierRemoteException se) {
			log.error(AirSourceConstants.LOG_SUPPLIER_REMOTE_FAILED, supplierConf.getSourceId(), pnrRequest.getPnr(),
					se.getMessage());
		} catch (CustomGeneralException cge) {
			log.info(AirSourceConstants.IMPORT_PNR_FAILED, pnrRequest.getPnr(), pnrRequest.getSupplierId(),
					cge.getMessage());
			LogUtils.log(pnrRequest.getPnr(), "ImportPNR", cge);
			throw cge;
		} finally {
			LogUtils.clearLogList();
			SystemContextHolder.getContextData().setGson(null);
			if (pnrBooking != null && CollectionUtils.isNotEmpty(pnrBooking.getTripInfos())) {
				String keyWithClientPrefix = getKeyWithClientPrefix(pnr);
				SystemContextHolder.getContextData().addReqIds(keyWithClientPrefix, pnr);
				LogUtils.copyLogsfromAllPartitions(pnr, keyWithClientPrefix);
			}
		}
		return pnrBooking;
	}

	private String getKeyWithClientPrefix(String pnr) {
		ClientGeneralInfo clientInfo = ServiceCommunicatorHelper.getClientInfo();
		String prefix = clientInfo.getRefIdConfiguration().getPrefix();
		return prefix.concat(pnr);
	}

	protected void sortTravellers(TripInfo tripInfo, AirSearchQuery searchQuery) {
		Map<PaxType, Integer> paxInfo = searchQuery.getPaxInfo();
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			List<FlightTravellerInfo> travellerInfos = new ArrayList<>();
			for (PaxType paxType : PaxType.values()) {
				travellerInfos.addAll(segmentInfo.getTravellerInfo().stream()
						.filter(travellerInfo -> travellerInfo.getPaxType().equals(paxType))
						.collect(Collectors.toList()));
			}
			segmentInfo.getBookingRelatedInfo().setTravellerInfo(travellerInfos);
		});
	}

	private void sortSegmentInfosOnDepatureTime(List<SegmentInfo> segmentInfos) {
		Collections.sort(segmentInfos, new Comparator<SegmentInfo>() {
			public int compare(SegmentInfo s1, SegmentInfo s2) {
				return s1.getDepartTime().compareTo(s2.getDepartTime());
			}
		});
	}

	private void sortTripInfosOnDepatureTime(List<TripInfo> tripInfos) {
		Collections.sort(tripInfos, new Comparator<TripInfo>() {
			public int compare(TripInfo t1, TripInfo t2) {
				return t1.getDepartureTime().compareTo(t2.getDepartureTime());
			}
		});
	}

	protected List<TripInfo> combineTripInfos(List<TripInfo> tripInfos) {
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(AirUtils.getSegmentInfos(tripInfos));
		return Arrays.asList(tripInfo);
	}

	private void updateBookingStatus() {
		String airline = pnrBooking.getTripInfos().get(0).getAirlineCode(false, 0);
		AirlineInfo airlineInfo = AirlineHelper.getAirlineInfo(airline);
		boolean hasTicketNumber = BookingUtils.hasTicketNumberInAllTrip(pnrBooking.getTripInfos());
		boolean hasPNR = BookingUtils.hasPNRInAllTrip(pnrBooking.getTripInfos());
		if (hasPNR && hasTicketNumber && airlineInfo.getIsTkRequired()) {
			pnrBooking.setItemStatus(AirItemStatus.ON_HOLD);
		} else if (hasPNR && !airlineInfo.getIsTkRequired()) {
			// Has To handle where supplier not provide tkt number
			pnrBooking.setItemStatus(AirItemStatus.ON_HOLD);
		} else if (hasPNR || hasTicketNumber) {
			pnrBooking.setItemStatus(AirItemStatus.ON_HOLD);
		} else {
			pnrBooking.setItemStatus(AirItemStatus.IN_PROGRESS);
		}
	}

	public abstract AirImportPnrBooking retrievePNRBooking();

	public void updateTimeLimit(Calendar holdTimeLimit) {
		if (Objects.nonNull(holdTimeLimit)) {
			LocalDateTime holdTime = TgsDateUtils.calenderToLocalDateTime(holdTimeLimit);
			if (pnrBooking != null) {
				pnrBooking.getTripInfos().forEach(trip -> {
					trip.getSegmentInfos().forEach(segmentInfo -> {
						segmentInfo.setTimeLimit(holdTime);
					});
				});
			}
		}
	}

	public AirCreditShellResponse fetchCreditShellBalance(AirCreditShellRequest request) {
		this.bookingUser = userServiceCommunicator
				.getUserFromCache(SystemContextHolder.getContextData().getUser().getParentUserId());
		sourceConfiguration = AirUtils.getAirSourceConfiguration(null, supplierConf.getBasicInfo(), bookingUser);
		return fetchBalance(request);
	}

	public AirCreditShellResponse fetchBalance(AirCreditShellRequest request) {
		return null;
	}

	public RetrieveTicketStatusResponse retrieveTicketStatus(RetrieveTicketStatusRequest ticketStatusRequest) {
		return new RetrieveTicketStatusResponse();
	}

}
