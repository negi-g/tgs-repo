package com.tgs.services.fms.sources.otaradixx;

import com.tgs.services.base.SpringContext;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;
import com.tgs.services.fms.jparepository.SourceRouteInfoService;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.datacontract.schemas._2004._07.radixx_connectpoint_flight_response.ViewAirportRoutes;
import org.tempuri.ConnectPoint_FlightStub;
import org.tempuri.RetrieveAirportRoutes;
import org.tempuri.RetrieveAirportRoutesResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Slf4j
@SuperBuilder
final class RadixxRouteManager extends RadixxServiceManager {

	SourceRouteInfoService routeInfoService;

	public void addRouteInfoToSystem() {
		routeInfoService =
				(SourceRouteInfoService) SpringContext.getApplicationContext().getBean("sourceRouteInfoService");
		try {
			ConnectPoint_FlightStub flightStub = bindingService.getFlightStub(configuration);
			RetrieveAirportRoutesResponse response = flightStub.retrieveAirportRoutes(getRetrieveAirportRoutesRQ());
			parseOperatingRoutes(response);
		} catch (Exception e) {
			log.error("Unable to Add Route into System {}", e);
		}
	}

	private void parseOperatingRoutes(RetrieveAirportRoutesResponse response) {
		List<DbSourceRouteInfo> routes = new ArrayList<>();
		ViewAirportRoutes airportRoutes = response.getRetrieveAirportRoutesResult();
		Arrays.asList(airportRoutes.getRoutes().getRoute()).forEach(routeInfo -> {
			DbSourceRouteInfo sourceRouteInfo = new DbSourceRouteInfo();
			sourceRouteInfo.setSourceId(airline.getSourceId());
			sourceRouteInfo.setEnabled(true);
			sourceRouteInfo.setSrc(routeInfo.getOriginAirportCode().trim());
			sourceRouteInfo.setDest(routeInfo.getDestinationAirportCode().trim());
			routes.add(sourceRouteInfo);
		});
		log.error("Total Size of Operating Airports {} & Supplier {} ", routes.size(), airline.name());
		routeInfoService.addAll(routes);
	}

	private RetrieveAirportRoutes getRetrieveAirportRoutesRQ() {
		RetrieveAirportRoutes routes = new RetrieveAirportRoutes();
		org.datacontract.schemas._2004._07.radixx_connectpoint_flight_request.RetrieveAirportRoutes airportRoutes =
				new org.datacontract.schemas._2004._07.radixx_connectpoint_flight_request.RetrieveAirportRoutes();
		airportRoutes.setLanguageCode("en");
		airportRoutes.setCarrierCodes(getCarrierCode());
		airportRoutes.setSecurityGUID(binaryToken);
		routes.setRetrieveAirportRoutesRequest(airportRoutes);
		return routes;
	}
}
