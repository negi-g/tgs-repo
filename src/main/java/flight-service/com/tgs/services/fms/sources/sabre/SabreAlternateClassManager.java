package com.tgs.services.fms.sources.sabre;

import com.sabre.webservices.sabrexml._2011._10.*;
import com.sabre.webservices.websvc.OTA_AirAvailServiceStub;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.sabre.services.stl_header.v120.CompletionCodes;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.alternateclass.AlternateClass;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.ebxml.www.namespaces.messageheader.MessageHeader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;


@SuperBuilder
@Slf4j
final class SabreAlternateClassManager extends SabreServiceManager {

	protected void searchAlternateClass(TripInfo tripInfo) {
		OTA_AirAvailRQ availaibilityRq = null;
		OTA_AirAvailServiceStub serviceStub = bindingService.getClassService();
		try {
			availaibilityRq = buildAirAvailRq(tripInfo);
			listener.setType(AirUtils.getLogType("OTA_AirAvail", configuration));
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			MessageHeader header = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.AIR_AVAIL_SERVICE, SabreConstants.CONVERSATION_ID);
			OTA_AirAvailRS availabilityRS = serviceStub.oTA_AirAvailRQ(availaibilityRq, header, getSecurity());
			buildAvailableClass(availabilityRS, tripInfo);
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private OTA_AirAvailRQ buildAirAvailRq(TripInfo tripInfo) {
		OTA_AirAvailRQ request = new OTA_AirAvailRQ();
		request.setVersion(SabreConstants.AIR_AVAIL_VERSION);
		request.setOriginDestinationInformation(buildOriginDestinationInfo(tripInfo));
		request.setOptionalQualifiers(buildOptionalQualifier(tripInfo));
		return request;
	}

	private OptionalQualifiers_type0 buildOptionalQualifier(TripInfo tripInfo) {
		OptionalQualifiers_type0 optionalQualifiers = new OptionalQualifiers_type0();
		FlightQualifiers_type0 flightQualifiers = new FlightQualifiers_type0();
		VendorPrefs_type0 vendorPref = new VendorPrefs_type0();
		Airline_type0 airline = new Airline_type0();
		airline.setCode(tripInfo.getAirlineCode());
		vendorPref.setAirline(airline);
		flightQualifiers.setVendorPrefs(vendorPref);
		optionalQualifiers.setFlightQualifiers(flightQualifiers);
		return optionalQualifiers;
	}


	private OriginDestinationInformation_type0 buildOriginDestinationInfo(TripInfo tripInfo) {
		OriginDestinationInformation_type0 od = new OriginDestinationInformation_type0();
		FlightSegment_type0[] flightSegment = new FlightSegment_type0[1];
		FlightSegment_type0 segment_type0 = new FlightSegment_type0();
		OriginLocation_type0 origin = new OriginLocation_type0();
		origin.setLocationCode(tripInfo.getDepartureAirportCode());
		DestinationLocation_type0 destination = new DestinationLocation_type0();
		destination.setLocationCode(tripInfo.getArrivalAirportCode());

		segment_type0.setOriginLocation(origin);
		segment_type0.setDestinationLocation(destination);
		// if we set flight number it will affect in connecting flight
		// flightSegment.setFlightNumber(tripInfo.getSegmentInfos().get(0).getFlightNumber());
		// flightSegment.setResBookDesigCode(getBookingClass(tripInfo));
		ConnectionLocations_type0 connectionLocation = new ConnectionLocations_type0();
		List<ConnectionLocation_type0> connections = new ArrayList<>();
		for (int segmentIndex = 0; segmentIndex < tripInfo.getSegmentInfos().size() - 1; segmentIndex++) {
			ConnectionLocation_type0 connectionLocationCode = new ConnectionLocation_type0();
			connectionLocationCode
					.setLocationCode(tripInfo.getSegmentInfos().get(segmentIndex).getArrivalAirportCode());
			connections.add(connectionLocationCode);
		}
		if (CollectionUtils.isNotEmpty(connections)) {
			connectionLocation.setConnectionLocation(connections.toArray(new ConnectionLocation_type0[0]));
			segment_type0.setConnectionLocations_type0(connectionLocation);
		}
		String departureTime =
				DateFormatterHelper.format(tripInfo.getSegmentInfos().get(0).getDepartTime(), "MM-dd hh:mm");
		departureTime = departureTime.replace(" ", "T");
		DateTime dateTime = new DateTime();
		dateTime.setDateTime(departureTime);
		segment_type0.setDepartureDateTime(dateTime);
		flightSegment[0] = segment_type0;
		od.setFlightSegment(flightSegment);
		return od;
	}

	private void buildAvailableClass(OTA_AirAvailRS airAvailabilityRS, TripInfo tripInfo) {
		int totalTraveller = AirUtils.getPaxCountFromPaxInfo(tripInfo.getPaxInfo(), false);
		if (airAvailabilityRS != null && airAvailabilityRS.getApplicationResults() != null
				&& airAvailabilityRS.getApplicationResults().getStatus().equals(CompletionCodes.Complete)) {
			OriginDestinationOptions_type0 originDestinationOptions = airAvailabilityRS.getOriginDestinationOptions();
			for (int tripIndex =
					0; tripIndex < originDestinationOptions.getOriginDestinationOption().length; tripIndex++) {
				OriginDestinationOption_type0 originDestinationOption =
						originDestinationOptions.getOriginDestinationOption()[tripIndex];
				FlightSegment_type1[] segmentList = originDestinationOption.getFlightSegment();
				int segmentListSize = segmentList.length;
				for (Integer segmentNum = 0; segmentNum < segmentListSize
						&& segmentListSize <= tripInfo.getSegmentInfos().size(); segmentNum++) {
					SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(segmentNum);
					FlightSegment_type1 flightSegment = segmentList[segmentNum];
					if (isSameFlightSegment(flightSegment, segmentInfo)
							&& CollectionUtils.isEmpty(segmentInfo.getAlternateClass())) {
						List<AlternateClass> classAvailable = new ArrayList<>();

						for (BookingClassAvail_type0 classAvailType0 : flightSegment.getBookingClassAvail()) {
							int availableSeats = Integer.parseInt(classAvailType0.getAvailability());
							if (availableSeats >= 1) {
								AlternateClass alternateClass =
										AlternateClass.builder().classOfBook(classAvailType0.getResBookDesigCode())
												.seatCount(availableSeats).build();
								classAvailable.add(alternateClass);
							}
						}

						segmentInfo.getAlternateClass().addAll(classAvailable);
					}
				}
			}
		}
	}


	private boolean isSameFlightSegment(FlightSegment_type1 flightSegment, SegmentInfo segmentInfo) {
		boolean isSame = false;
		if (flightSegment.getOriginLocation().getLocationCode().equals(segmentInfo.getDepartureAirportCode())
				&& flightSegment.getDestinationLocation().getLocationCode().equals(segmentInfo.getArrivalAirportCode())
				&& flightSegment.getFlightNumber().equals(segmentInfo.getFlightNumber())
				&& flightSegment.getMarketingAirline().getCode().equals(segmentInfo.getAirlineCode(false))) {
			isSame = true;
		}
		return isSame;
	}

}
