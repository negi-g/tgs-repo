package com.tgs.services.fms.sources.ifly;

import com.ibsplc.www.ires.simpletypes.Payment_Type;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.util.Arrays;
import java.util.List;

@Getter
public enum IflyAirline {

	TRUJET(AirSourceType.TRUJET.getSourceId(), "2T") {
		@Override
		public Integer getAllowedPriceIndex() {
			return null;
		}

		@Override
		public void getOperatingAirportCodes() {
			List<String> codes = Arrays.asList("Ahmedabad%20(AMD)", "Aurangabad%20(IXU)", "Bangalore%20(BLR)",
					"Chennai%20(MAA)", "Cuddapah%20(CDP)", "Goa%20(GOI)", "Hyderabad%20(HYD)", "Indore%20(IDR)",
					"Jaisalmer%20(JSA)", "Nanded%20(NDC)", "Nasik%20(ISK)", "Mumbai%20(BOM)", "Mysore%20(MYQ)",
					"Porbandar%20(PBD)", "Rajahmundry%20(RJA)", "Salem%20(SXV)", "Tirupati%20(TIR)",
					"Vidyanagar%20(VDY)", "Vijayawada%20(VGA)");
			for (String code : codes) {
				try {
					String url = "https://trujet-api.azurewebsites.net/api/Destinations/GetDestinationsByOrigin?Origin="
							+ code;
					Connection connection =
							Jsoup.connect(url).timeout(60000).ignoreContentType(true).ignoreHttpErrors(true);
					connection.header("Accept",
							"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
					connection.header("Accept-Encoding", "gzip,deflate,sdch");
					connection.header("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6,zh-TW;q=0.4");
					connection.header("Cache-Control", "no-cache");
					connection.header("Connection", "keep-alive");
					Document title = connection.get();
					code = code.replaceAll("%20", "");
					String operatingCode = code.split("[(]")[1].replaceAll("[)]", "");
					String text = title.text().replaceAll(
							"<ArrayOfstring xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">\n"
									+ " <string>",
							"");
					text = text.replaceAll("[)]", ")@");
					List<String> airports = Arrays.asList(text.split("@"));
					for (String airport : airports) {
						String marketingCode = airport.split("[(]")[1].replaceAll("[)]", "");
						System.out.println("insert into sourcerouteinfo(sourceid,enabled,src,dest) values('13',true,'"
								+ operatingCode + "','" + marketingCode + "');");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};


	private Integer sourceId;

	private String airlineCode;

	IflyAirline(Integer sourceId, String airlineCode) {
		this.airlineCode = airlineCode;
		this.sourceId = sourceId;
	}

	private static final String LOCALE = "en_IN";

	private static final String API = "API";

	private static final String DEFAULT_POS_COUNTRY = "IN";

	private static final Double EXCHANGE_RATE = 1.0;

	public static IflyAirline getIFlyAirline(Integer sourceId) {
		for (IflyAirline airline : IflyAirline.values()) {
			if (airline.getSourceId().equals(sourceId)) {
				return airline;
			}
		}
		return null;
	}


	public String getLocale() {
		return LOCALE;
	}

	public String getChannelType() {
		return API;
	}

	public Double getExchangeRate() {
		return EXCHANGE_RATE;
	}

	public String getPointOfSale(ClientGeneralInfo generalInfo) {
		if (StringUtils.isBlank(generalInfo.getNationality())) {
			return DEFAULT_POS_COUNTRY;
		}
		return generalInfo.getNationality();
	}

	public Payment_Type getAgencyPaymentType() {
		return Payment_Type.AG;
	}

	public Integer getAllowedPriceIndex() {
		return null;
	}

	public boolean isAllClassPriceRequired() {
		/**
		 * @implSpec : if this param return true will return all available class with fare
		 */
		return false;
	}

	public String getCurrencyCode() {
		return "INR";
	}

	public abstract void getOperatingAirportCodes();
}
