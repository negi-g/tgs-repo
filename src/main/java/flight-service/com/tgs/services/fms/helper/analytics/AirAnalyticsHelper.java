package com.tgs.services.fms.helper.analytics;

import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;
import com.tgs.services.base.BaseAnalyticsQuery;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import lombok.extern.slf4j.Slf4j;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class AirAnalyticsHelper {

	@Autowired
	ElasticSearchCommunicator esStackCommunicator;

	@Autowired
	KafkaServiceCommunicator kafkaService;

	public <T extends BaseAnalyticsQuery> void pushToAnalytics(Mapper<T> mapper, AirAnalyticsType airAnalyticsType) {
		pushToAnalytics(mapper, airAnalyticsType, ESMetaInfo.AIR);
	}

	public <T extends BaseAnalyticsQuery> void pushToAnalytics(Mapper<T> mapper, AirAnalyticsType airAnalyticsType,
			ESMetaInfo metaInfo) {
		ExecutorUtils.getAnalyticsThreadPool().submit(() -> {
			T tripQuery = null;
			try {
				tripQuery = mapper.convert();
				tripQuery.setAnalyticstype(airAnalyticsType.name());
				QueueData queueData = QueueData.builder().key(metaInfo.getIndex())
						.value(GsonUtils.getGson().toJson(tripQuery)).build();
				List<QueueDataType> queueTypes = Arrays.asList(QueueDataType.ELASTICSEARCH);
				kafkaService.queue(queueTypes, queueData);
			} catch (Exception e) {
				log.error("Failed to Push Analytics type {} ", airAnalyticsType.name(), e);
			}
		});
	}

}
