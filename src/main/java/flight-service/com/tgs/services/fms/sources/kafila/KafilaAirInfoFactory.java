package com.tgs.services.fms.sources.kafila;

import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class KafilaAirInfoFactory extends AbstractAirInfoFactory {

	protected RestAPIListener listener = null;
	protected String tokenId;

	public KafilaAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableSchedules() {
		KafilaSearchManager searchManager = null;
		listener = new RestAPIListener(searchQuery.getSearchId());
		KafilaBindingService bindingService =
				KafilaBindingService.builder().user(user).configuration(supplierConf).listener(listener).build();
		KafilaTokenManager tokenManager = KafilaTokenManager.builder().searchQuery(searchQuery).listener(listener)
				.bindingService(bindingService).supplierConfiguration(supplierConf).build();
		String token = tokenManager.login();
		searchManager = KafilaSearchManager.builder().supplierConfiguration(supplierConf).searchQuery(searchQuery)
				.listener(listener).user(user).bindingService(bindingService).token(token).build();
		searchManager.init();
		searchResult = searchManager.doSearch();
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		KafilaReviewManager reviewManager = null;
		TripInfo tripInfo = null;
		boolean isReviewSuccess = false;
		try {
			listener = new RestAPIListener(bookingId);
			listener.setKey(bookingId);
			KafilaBindingService bindingService =
					KafilaBindingService.builder().user(user).configuration(supplierConf).listener(listener).build();
			reviewManager = KafilaReviewManager.builder().supplierConfiguration(supplierConf).searchQuery(searchQuery)
					.bookingId(bookingId).listener(listener).bindingService(bindingService).user(user).build();
			tripInfo = reviewManager.fareCheck(selectedTrip);
			isReviewSuccess = true;

		} finally {
			if (isReviewSuccess) {
				tripInfo = reviewManager.setBookingString(tripInfo);
				storeBookingSession(bookingId, null, null, tripInfo);
			}
		}
		return tripInfo;
	}
}
