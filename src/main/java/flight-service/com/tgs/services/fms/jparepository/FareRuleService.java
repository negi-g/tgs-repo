package com.tgs.services.fms.jparepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.farerule.FareRuleFilter;
import com.tgs.services.fms.dbmodel.DbFareRuleInfo;
import com.tgs.services.fms.helper.FareRuleHelper;
import com.tgs.utils.springframework.data.SpringDataUtils;

@Service
public class FareRuleService {

	@Autowired
	FareRuleRepository fareRuleRepository;

	@Autowired
	FareRuleHelper fareRuleHelper;

	public DbFareRuleInfo save(DbFareRuleInfo fareRule) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			fareRule.cleanData();
			fareRule = fareRuleRepository.saveAndFlush(fareRule);
		}
		fareRuleHelper.process();
		return fareRule;
	}

	public List<DbFareRuleInfo> findAll() {
		Sort sort = new Sort(Direction.DESC, Arrays.asList("airline", "priority", "processedOn"));
		return fareRuleRepository.findAll(sort);
	}

	/*
	 * public List<DbFareRuleInfo> findAllByEnabled(Boolean enabled) { return fareRuleRepository.findByEnabled(enabled);
	 * }
	 */

	public List<DbFareRuleInfo> findAll(FareRuleFilter queryFilter) {
		PageRequest request = SpringDataUtils.getPageRequestFromFilter(queryFilter);
		Specification<DbFareRuleInfo> specification = new Specification<DbFareRuleInfo>() {
			@Override
			public Predicate toPredicate(Root<DbFareRuleInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				predicates =
						(FareRuleSearchPredicate.getPredicateListBasedOnFareRuleFilter(root, query, cb, queryFilter));
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};

		return fareRuleRepository.findAll(specification, request).getContent();
	}

	public DbFareRuleInfo findById(long id) {
		return fareRuleRepository.findOne(id);
	}

}
