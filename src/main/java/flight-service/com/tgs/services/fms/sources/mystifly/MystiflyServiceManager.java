package com.tgs.services.fms.sources.mystifly;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.JaxWsClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirTripType;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfError;
import org.datacontract.schemas._2004._07.mystifly_onepoint.CabinBaggageInfo;
import org.datacontract.schemas._2004._07.mystifly_onepoint.CabinType;
import org.datacontract.schemas._2004._07.mystifly_onepoint.Error;
import org.datacontract.schemas._2004._07.mystifly_onepoint.Gender;
import org.datacontract.schemas._2004._07.mystifly_onepoint.IsRefundable;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PTC_FareBreakdown;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PassengerTitle;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PassengerType;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PricedItinerary;
import org.datacontract.schemas._2004._07.mystifly_onepoint.Target;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.tgs.services.base.CxfRequestLogInterceptor;
import com.tgs.services.base.CxfResponseLogInterceptor;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.OnePointStub;


@Slf4j
@Getter
@Setter
@SuperBuilder
abstract class MystiflyServiceManager {

	protected OnePointStub onePointStub;

	protected String sessionId;

	protected Client client;

	protected SupplierConfiguration configuration;

	protected AirSearchQuery searchQuery;

	protected String fareSourceCode;

	protected Object port;
	
	protected User bookingUser;

	protected String bookingId;

	protected boolean isHoldBooking;

	protected DeliveryInfo deliveryInfo;

	protected List<String> criticalMessageLogger;

	protected SoapRequestResponseListner listener;

	protected static List<String> mealCodes = Arrays.asList("B", "L", "D", "K", "M", "O", "H", "V");

	/**
	 * when going for prduction, we can change Target here
	 *
	 * @return Target
	 */
	public Target getTarget() {
		if (BooleanUtils.isTrue(getConfiguration().getSupplierCredential().getIsTestCredential())) {
			return Target.Test;
		}
		// return Target.PRODUCTION;
		return null;
	}

	/**
	 * Mystifly is giving combination for return jorney . So in case of return we need to set the trip type as COMBO
	 *
	 * @param pricedItenary
	 * @return
	 */
	public String getTripType(PricedItinerary pricedItenary) {
		AirTripType tripType = pricedItenary.getDirectionInd();
		if (tripType.getValue().equalsIgnoreCase("RETURN")) {
			return "COMBO";
		}
		return "ONWARD";
	}

	/**
	 * This method return gender from title
	 *
	 * @param title
	 * @return
	 */

	public Gender getGenderFromTitle(String title) {
		if (title.compareToIgnoreCase("Mr") == 0 || title.compareToIgnoreCase("Mstr") == 0
				|| title.compareToIgnoreCase("Master") == 0) {
			return Gender.M;
		} else if (title.compareToIgnoreCase("Ms") == 0 || title.compareToIgnoreCase("Mrs") == 0
				|| title.compareToIgnoreCase("Miss") == 0) {
			return Gender.F;
		}
		return Gender.U;
	}

	public PassengerTitle getPassengerTitle(String title) {

		switch (title) {
			case "Mr":
				return PassengerTitle.MR;
			case "Mstr":
				return PassengerTitle.MSTR;
			case "Master":
				return PassengerTitle.MSTR;
			case "Ms":
				return PassengerTitle.MS;
			case "Mrs":
				return PassengerTitle.MRS;
			case "Miss":
				return PassengerTitle.MISS;
			default:
				return PassengerTitle.Default;
		}
	}

	public PassengerType getPassengerCode(PaxType paxType) {
		if (paxType.getType().equals("CNN"))
			return PassengerType.CHD;
		else if (paxType.getType().equals("ADT"))
			return PassengerType.ADT;
		else
			return PassengerType.INF;
	}


	public AirlineInfo getPlatingCarrier(String code) {
		return AirlineHelper.getAirlineInfo(code);
	}

	public Integer getRefundableType(IsRefundable isRefundable) {
		if (IsRefundable.Yes.equals(isRefundable)) {
			return RefundableType.REFUNDABLE.getRefundableType();
		} else if (IsRefundable.No.equals(isRefundable)) {
			return RefundableType.NON_REFUNDABLE.getRefundableType();
		}
		return null;
	}

	public CabinType getCabinType(CabinClass cabinClass) {
		if (cabinClass.getCode().equals("Y")) {
			return CabinType.Y;
		} else if (cabinClass.getCode().equals("S")) {
			return CabinType.S;
		} else if (cabinClass.getCode().equals("C")) {
			return CabinType.C;
		} else if (cabinClass.getCode().equals("J")) {
			return CabinType.J;
		} else if (cabinClass.getCode().equals("F")) {
			return CabinType.F;
		} else if (cabinClass.getCode().equals("P")) {
			return CabinType.P;
		}
		return CabinType.Default;
	}

	public String getFareBasis(PTC_FareBreakdown fareBreakDown, int index) {
		ArrayOfstring fareBasisArray = null;
		try {
			fareBasisArray = fareBreakDown.getFareBasisCodes();
			String[] array = fareBasisArray.getString();
			return array[index];
		} catch (Exception e) {
			// Forcufully we have to comment this error issue from mystifly side
			// log.error("Fare Basis is Empty for {} and fare basis {} and cause ", searchQuery.getSearchId(),
			// fareBasisArray.getString(), e);
		}
		return StringUtils.EMPTY;
	}

	public BaggageInfo createBaggaeInfo(PTC_FareBreakdown fareBreakDown, int index) {
		BaggageInfo bagInfo = new BaggageInfo();
		try {
			org.datacontract.schemas._2004._07.mystifly_onepoint.BaggageInfo baggageInfo =
					fareBreakDown.getBaggageInfo();
			if (ArrayUtils.isNotEmpty(baggageInfo.getBaggage())) {
				bagInfo.setAllowance(baggageInfo.getBaggage()[index]);
			}
			CabinBaggageInfo cabinBaggage = fareBreakDown.getCabinBaggageInfo();
			if (Objects.nonNull(cabinBaggage) && ArrayUtils.isNotEmpty(cabinBaggage.getCabinBaggage())) {
				bagInfo.setCabinBaggage(cabinBaggage.getCabinBaggage()[index]);
			}
		} catch (Exception e) {
			log.debug("Unable to get Baggage info for segmentnum  {}", index);
		}
		return bagInfo;
	}

	protected Boolean getIsMealIncluded(String value) {
		if (StringUtils.isNotBlank(value) && mealCodes.contains(value)) {
			return true;
		}
		return null;
	}


	protected void logCriticalMessage(String message) {
		if (StringUtils.isNotBlank(message) && criticalMessageLogger != null) {
			criticalMessageLogger.add(message);
		}
	}

	protected boolean isAnyCriticalException(ArrayOfError error) {
		AtomicBoolean isAnyError = new AtomicBoolean();
		StringJoiner message = new StringJoiner("");
		if (error != null && error.getError() != null) {
			for (Error excep : error.getError()) {
				if (excep.getCode() != null && excep.getMessage() != null) {
					isAnyError.set(true);
					message.add("Code :").add(excep.getCode()).add(" Descp :").add(excep.getMessage());
					log.info("Error Occured on booking id {} message {} ", bookingId, message);
				}
			}
		}
		logCriticalMessage(message.toString());
		return isAnyError.get();
	}

}
