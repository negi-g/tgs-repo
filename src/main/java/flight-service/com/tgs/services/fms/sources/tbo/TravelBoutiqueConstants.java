package com.tgs.services.fms.sources.tbo;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class TravelBoutiqueConstants {


	public static final String IP = "192.168.3.4";
	public static final int MALE = 1, FEMALE = 2;


	public static final Integer AISLE = 2;
	public static final Integer AISLE_RECLINE = 10;
	public static final Integer AISLE_WING = 11;
	public static final Integer AISLE_EXIT_ROW = 12;
	public static final Integer AISLE_RECLINE_WING = 13;
	public static final Integer AISLE_RECLINE_EXIT_ROW = 14;
	public static final Integer AISLE_WING_EXIT_ROW = 15;
	public static final Integer AISLE_RECLINE_WING_EXIT_ROW = 23;
	public static final Integer AISLE_BULK_HEAD = 31;
	public static final Integer AISLE_QUIET = 32;
	public static final Integer AISLE_BULK_HEAD_QUIET = 33;
	public static final Integer CENTRE_AISLE = 34;
	public static final Integer CENTRE_AISLE_BULK_HEAD = 36;
	public static final Integer CENTRE_AISLE_QUIET = 37;
	public static final Integer CENTRE_AISLE_BULK_HEAD_QUIET = 38;
	public static final Integer AISLE_BULK_HEAD_WING = 46;
	public static final Integer AISLE_BULK_HEAD_EXIT_ROW = 47;
	public static final Integer WINDOW_AISLE = 49;
	public static final Integer WINDOW_AISLE_RECLINE = 54;
	public static final Integer WINDOW_AISLE_WING = 55;
	public static final Integer WINDOW_AISLE_EXIT_ROW = 56;
	public static final Integer WINDOW_AISLE_RECLINE_WING = 57;
	public static final Integer WINDOW_AISLE_RECLINE_EXIT_ROW = 58;
	public static final Integer WINDOW_AISLE_WING_EXIT_ROW = 59;
	public static final Integer WINDOW_AISLE_BULK_HEAD = 60;
	public static final Integer WINDOW_AISLE_BULK_HEAD_WING = 61;

	public static final List<Integer> AISLE_SEAT_TYPE_CODES = Collections.unmodifiableList(Arrays.asList(AISLE,
			AISLE_RECLINE, AISLE_WING, AISLE_EXIT_ROW, AISLE_RECLINE_WING, AISLE_RECLINE_EXIT_ROW, AISLE_WING_EXIT_ROW,
			AISLE_RECLINE_WING_EXIT_ROW, AISLE_BULK_HEAD, AISLE_QUIET, AISLE_BULK_HEAD_QUIET, CENTRE_AISLE,
			CENTRE_AISLE_BULK_HEAD, CENTRE_AISLE_QUIET, CENTRE_AISLE_BULK_HEAD_QUIET, AISLE_BULK_HEAD_WING,
			AISLE_BULK_HEAD_EXIT_ROW, WINDOW_AISLE, WINDOW_AISLE_RECLINE, WINDOW_AISLE_WING, WINDOW_AISLE_EXIT_ROW,
			WINDOW_AISLE_RECLINE_WING, WINDOW_AISLE_RECLINE_EXIT_ROW, WINDOW_AISLE_WING_EXIT_ROW,
			WINDOW_AISLE_BULK_HEAD, WINDOW_AISLE_BULK_HEAD_WING));

	public static final List<String> BAGGAGE_CODES =
			Collections.unmodifiableList(Arrays.asList("none", "no baggage", "no bag"));
	public static final List<String> MEAL_CODES = Collections.unmodifiableList(Arrays.asList("none", "no meal"));
	public static final List<String> SEAT_CODES = Collections.unmodifiableList(Arrays.asList("none", "noseat"));


}
