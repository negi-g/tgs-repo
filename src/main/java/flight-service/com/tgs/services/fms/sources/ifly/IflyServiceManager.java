package com.tgs.services.fms.sources.ifly;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import com.ibsplc.www.ires.simpletypes.NameTitle_Type;
import com.ibsplc.www.ires.simpletypes.BookerDetailsType;
import com.ibsplc.www.ires.simpletypes.SsrFieldNameDetails_Type;
import com.ibsplc.www.ires.simpletypes.SsrFieldDetailsType;
import com.ibsplc.www.ires.simpletypes.SSRInformationType;
import com.ibsplc.www.ires.simpletypes.GuestRequestDetailsType;
import com.ibsplc.www.ires.simpletypes.FareDetailsForGuestType;
import com.ibsplc.www.ires.simpletypes.FareInfoType;
import com.ibsplc.www.ires.simpletypes.PaxDetails_Type;
import com.ibsplc.www.ires.simpletypes.DateOnlyDetailsType;
import com.ibsplc.www.ires.simpletypes.BookingChannelKeyType;
import com.ibsplc.www.ires.simpletypes.FlightSegmentDetailsType;
import com.ibsplc.www.ires.simpletypes.BookingChannelType;
import com.ibsplc.www.ires.simpletypes.ItineraryDetailsType;
import com.ibsplc.www.ires.simpletypes.ErrorType;
import com.ibsplc.www.ires.simpletypes.TripType;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.endpoint.Client;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
abstract class IflyServiceManager {

	private SupplierConfiguration configuration;

	private AirSearchQuery searchQuery;

	private String bookingId;

	protected IflyBindingService bindingService;

	private List<SegmentInfo> segmentInfos;

	private Order order;

	protected IflyAirline iFlyAirline;

	private List<String> criticalMessageLogger;

	private Client client;

	private String clientSessionId;

	private User bookingUser;

	private ClientGeneralInfo clientInfo;

	private DeliveryInfo deliveryInfo;

	protected SoapRequestResponseListner listener;

	private AirSourceConfigurationOutput sourceConfiguration;

	private MoneyExchangeCommunicator moneyExchnageComm;

	private List<SupplierAnalyticsQuery> supplierAnalyticInfos;

	private static String TO_CURRENCY;

	private AirSourceType sourceType;

	private static final String FLT_SUFFIX = "*";

	private Integer paxCountWithoutInfant;

	private Integer paxCountWithInfant;

	public void init() {
		this.iFlyAirline = IflyAirline.getIFlyAirline(configuration.getSourceId());
		clientInfo = ServiceCommunicatorHelper.getClientInfo();
		TO_CURRENCY = AirSourceConstants.getClientCurrencyCode();
		sourceType = AirSourceType.getAirSourceType(configuration.getSourceId());
		paxCountWithoutInfant = getPaxCountWithOutInfant();
		paxCountWithInfant = getPaxCountInfant();
	}

	public void setClientSessionId(String clientId) {
		bindingService.setClientToken(clientId);
	}

	protected TripType getTripType() {
		TripType tripType = TripType.OW;
		if (searchQuery.isMultiCity()) {
			tripType = TripType.MC;
		} else if (searchQuery.isDomesticReturn() || searchQuery.isIntlReturn()) {
			tripType = TripType.RT;
		}
		return tripType;
	}

	public String getTripType(String tripDirection) {
		if (tripDirection.equals(TripType._OW)) {
			return TripInfoType.ONWARD.getName();
		} else if (tripDirection.equals(TripType._RT)) {
			return TripInfoType.RETURN.getName();
		}
		return TripInfoType.ONWARD.getName();
	}

	protected BookingChannelKeyType getBookingChannel() {
		BookingChannelKeyType bookingChannel = new BookingChannelKeyType();
		bookingChannel.setChannel(configuration.getSupplierCredential().getUserName().toUpperCase());
		bookingChannel.setChannelType(iFlyAirline.getChannelType());
		bookingChannel.setLocale(iFlyAirline.getLocale());
		return bookingChannel;
	}

	protected int getPaxTypeCount() {
		int paxTypeCount = 0;
		for (PaxType paxType : getSearchQuery().getPaxInfo().keySet()) {
			if (getSearchQuery().getPaxInfo().get(paxType) > 0) {
				paxTypeCount++;
			}
		}
		return paxTypeCount;
	}

	public String getPointOfSale() {
		return iFlyAirline.getPointOfSale(clientInfo);
	}

	protected String getErrorMessage() {
		return String.join(",", criticalMessageLogger);
	}


	public double getAmountBasedOnCurrency(BigDecimal amount, String currencyCode) {
		// Currently we are not using currency but in future we might need to set amount
		// based on current , so keeping scope for future
		// MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(currencyCode)
		// .toCurrency(TO_CURRENCY).type(sourceType.name().toUpperCase()).build();
		// return moneyExchnageComm.getExchangeValue(amount, filter, true);
		return amount.doubleValue();
	}

	public double getAmountBasedOnCurrency(Double amount, String currencyCode) {
		// Currently we are not using currency but in future we might need to set amount
		// based on current , so keeping scope for future
		// MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(currencyCode)
		// .toCurrency(TO_CURRENCY).type(sourceType.name().toUpperCase()).build();
		// return moneyExchnageComm.getExchangeValue(amount, filter, true);
		return amount;
	}


	public boolean isErrorResponse(ErrorType errorType) {
		boolean isError = false;
		if (errorType != null) {
			String message = StringUtils.join(errorType.getErrorCode(), " ", errorType.getErrorValue());
			log.info("Error Occured on booking {} message {}", bookingId, message);
			isError = true;
			if (criticalMessageLogger != null) {
				criticalMessageLogger.add(message);
			}
		}
		return isError;
	}

	public String getCurrencyCode() {
		if (StringUtils.isNotBlank(configuration.getSupplierCredential().getCurrencyCode())) {
			return configuration.getSupplierCredential().getCurrencyCode();
		}
		clientInfo = ServiceCommunicatorHelper.getClientInfo();
		return clientInfo.getCurrencyCode();
	}

	public BookingChannelType getBookingChannelType() {
		BookingChannelType channelType = new BookingChannelType();
		channelType.setChannel(configuration.getSupplierCredential().getUserName().toUpperCase());
		channelType.setChannelType(iFlyAirline.getChannelType());
		channelType.setLocale(iFlyAirline.getLocale());
		return channelType;
	}

	public FareInfoType getFareInfoType() {
		FareInfoType fareInfoType = new FareInfoType();
		fareInfoType.setFareDetailsForGuestType(getFareDetailsPaxWise());
		return fareInfoType;
	}

	private FareDetailsForGuestType[] getFareDetailsPaxWise() {
		List<FareDetailsForGuestType> fareDetailsForGuestTypes = new ArrayList<>();
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (segmentInfo.getSegmentNum() == 0) {
				PriceInfo priceInfo = segmentInfo.getPriceInfoList().get(0);
				AtomicReference<String> priceComponentIndex = new AtomicReference<>();

				if (StringUtils.isNotBlank(priceInfo.getMiscInfo().getFareKey())) {
					priceComponentIndex.set(priceInfo.getMiscInfo().getFareKey());
				} else {
					priceComponentIndex.set(segmentInfo.getPriceInfo(0).getMiscInfo().getTraceId());
				}

				for (PaxType paxType : priceInfo.getFareDetails().keySet()) {
					FareDetail fareDetail = priceInfo.getFareDetail(paxType);
					FareDetailsForGuestType fareGuestType = new FareDetailsForGuestType();
					fareGuestType.setSegmentId(getSegmentId(segmentInfos.size(), segmentInfo.getIsReturnSegment()));
					fareGuestType.setCurrency(getCurrencyCode());
					fareGuestType.setBaseFare(fareDetail.getFareComponents().get(FareComponent.BF));
					fareGuestType.setFareBasisCode(fareDetail.getFareBasis());
					fareGuestType.setFareClass(fareDetail.getClassOfBooking());
					fareGuestType.setGuestType(getPaxDetailsType(paxType));
					fareGuestType.setFareLevel(priceInfo.getMiscInfo().getFareLevel());
					fareGuestType.setFareComponentId(priceComponentIndex.get());
					fareGuestType.setPricingUnitID(Integer.valueOf(priceInfo.getMiscInfo().getJourneyKey()));
					fareGuestType.setFareTransactionID(priceInfo.getMiscInfo().getFareTypeId().longValue());
					fareDetailsForGuestTypes.add(fareGuestType);
				}
			}
		}
		return fareDetailsForGuestTypes.toArray(new FareDetailsForGuestType[0]);
	}


	public PaxDetails_Type getPaxDetailsType(PaxType paxType) {
		if (PaxType.ADULT.equals(paxType)) {
			return PaxDetails_Type.ADULT;
		}
		if (PaxType.CHILD.equals(paxType)) {
			return PaxDetails_Type.CHILD;
		}
		return PaxDetails_Type.INFANT;
	}

	protected Integer getPaxCountWithOutInfant() {
		if (getSearchQuery() != null) {
			return AirUtils.getPaxCount(getSearchQuery(), false);
		} else if (CollectionUtils.isNotEmpty(segmentInfos)
				&& getSegmentInfos().get(0).getBookingRelatedInfo() != null) {
			return AirUtils.getPaxCountFromTravellerInfo(
					getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo(), false);
		}
		return 0;
	}

	protected Integer getPaxCountInfant() {
		if (getSearchQuery() != null) {
			return AirUtils.getPaxCount(getSearchQuery(), true);
		} else if (CollectionUtils.isNotEmpty(segmentInfos)
				&& getSegmentInfos().get(0).getBookingRelatedInfo() != null) {
			return AirUtils.getPaxCountFromTravellerInfo(
					getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo(), false);
		}
		return 0;
	}

	public long[] getSegmentId(int size, Boolean isReturnSegment) {
		List<Long> segIdList = getSegmentIdFromSegment(size, isReturnSegment);
		long[] longs1 = new long[segIdList.size()];
		int index = 0;
		for (Long segmentId : segIdList) {
			longs1[index++] = segmentId;
		}
		return longs1;
	}

	private List<Long> getSegmentIdFromSegment(int size, Boolean isReturnSegment) {
		List<Long> segIdList = new ArrayList<>();
		if (isReturnSegment) {
			for (int i = 0; i < size; i++) {
				if (segmentInfos.get(i).getIsReturnSegment()) {
					segIdList.add(segmentInfos.get(i).getPriceInfo(0).getMiscInfo().getLogicalFlightId());
				}
			}
		} else {
			for (int i = 0; i < size; i++) {
				if (!segmentInfos.get(i).getIsReturnSegment()) {
					segIdList.add(segmentInfos.get(i).getPriceInfo(0).getMiscInfo().getLogicalFlightId());
				}
			}
		}
		return segIdList;
	}

	public String[] getSegmentLogicalId(int size, Boolean isReturnSegment) {
		List<Long> segIdList = getSegmentIdFromSegment(size, isReturnSegment);
		String[] longs1 = new String[segIdList.size()];
		int index = 0;
		for (Long segmentId : segIdList) {
			longs1[index++] = String.valueOf(segmentId);
		}
		return longs1;
	}

	public boolean isBookingFlow() {
		return CollectionUtils.isNotEmpty(segmentInfos) && segmentInfos.get(0).getBookingRelatedInfo() != null
				&& CollectionUtils.isNotEmpty(segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo());

	}

	public ItineraryDetailsType buildItnearyDetailsType() {
		ItineraryDetailsType itineraryDetailsType = new ItineraryDetailsType();
		itineraryDetailsType.setFlightSegmentDetails(buildFlightDetails());
		return itineraryDetailsType;
	}

	public DateOnlyDetailsType getFlightDate(SegmentInfo segmentInfo) {
		DateOnlyDetailsType departureDate = new DateOnlyDetailsType();
		departureDate.setDate(TgsDateUtils.getCalendarWithoutTimeZone(segmentInfo.getDepartTime()).getTime());
		return departureDate;
	}

	public FlightSegmentDetailsType[] buildFlightDetails() {
		FlightSegmentDetailsType[] flightDetails = new FlightSegmentDetailsType[segmentInfos.size()];
		int index = 0;
		for (SegmentInfo segmentInfo : segmentInfos) {
			FlightSegmentDetailsType flightSegmentDetail = new FlightSegmentDetailsType();
			flightSegmentDetail.setBoardPoint(segmentInfo.getDepartureAirportCode());
			flightSegmentDetail.setOffPoint(segmentInfo.getArrivalAirportCode());
			flightSegmentDetail.setFltNumber(segmentInfo.getFlightNumber());
			flightSegmentDetail.setFltSuffix(FLT_SUFFIX);
			flightSegmentDetail.setFlightSegmentGroupID(segmentInfo.getPriceInfo(0).getMiscInfo().getTokenId());
			flightSegmentDetail.setCarrierCode(iFlyAirline.getAirlineCode());
			flightSegmentDetail
					.setSegmentId(String.valueOf(segmentInfo.getPriceInfo(0).getMiscInfo().getLogicalFlightId()));
			flightSegmentDetail.setFlightDate(getFlightDate(segmentInfo));
			flightSegmentDetail.setCabinClass(CabinClass.ECONOMY.getName());
			flightSegmentDetail.setFareClass(segmentInfo.getPriceInfoList().get(0).getBookingClass(PaxType.ADULT));
			flightDetails[index++] = flightSegmentDetail;
		}
		return flightDetails;
	}

	public GuestRequestDetailsType[] buildGuestdetails() {
		List<GuestRequestDetailsType> guestRequestDetailsTypes = new ArrayList<>();
		int index = 0;
		int infantId = 0;
		for (FlightTravellerInfo travellerInfo : segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo()) {
			GuestRequestDetailsType guestDetail = new GuestRequestDetailsType();
			guestDetail.setGender(IflyUtils.getGenderFromTitle(travellerInfo.getTitle()));
			guestDetail.setGivenName(travellerInfo.getFirstName());
			if (PaxType.INFANT.equals(travellerInfo.getPaxType())) {
				guestDetail.setParentGuestID(String.valueOf(infantId++));
			}
			guestDetail.setGuestId(String.valueOf(index));
			guestDetail.setSurName(travellerInfo.getLastName());
			guestDetail.setNamePrefix(IflyUtils.getPaxTitle(travellerInfo.getTitle()));
			guestDetail.setGuestType(IflyUtils.getPaxTypeDetail(travellerInfo));
			guestDetail.setDateOfBirth(IflyUtils.getDOB(travellerInfo));
			guestRequestDetailsTypes.add(guestDetail);
			index++;
		}
		return guestRequestDetailsTypes.toArray(new GuestRequestDetailsType[0]);
	}

	public SSRInformationType[] buildbaggageSSR() {
		List<SSRInformationType> ssrList = new ArrayList<>();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getSegmentNum() == 0) {
				List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
				long guestId = 0;
				for (FlightTravellerInfo travellerInfo : travellerInfos) {
					SSRInformation ssrBaggageInfo = travellerInfo.getSsrBaggageInfo();
					if (ssrBaggageInfo != null) {
						SSRInformationType ssrType = new SSRInformationType();
						ssrType.setGuestId(guestId);
						ssrType.setNumberOfRequests(1);
						ssrType.setSsrAirlineCode(getIFlyAirline().getAirlineCode());
						ssrType.setSsrCode(ssrBaggageInfo.getCode().split(" ")[1]);
						ssrType.setSsrFieldDetailsType(buildSsrDetailsType(ssrBaggageInfo));
						ssrType.setSegmentId(
								getSegmentLogicalId(segmentInfos.size(), segmentInfo.getIsReturnSegment()));
						ssrList.add(ssrType);
					}
					guestId++;
				}
			}
		});
		return ssrList.toArray(new SSRInformationType[0]);
	}

	public SsrFieldDetailsType[] buildSsrDetailsType(SSRInformation ssrBaggageInfo) {
		SsrFieldDetailsType[] ssrFieldDetailsTypes = new SsrFieldDetailsType[2];
		SsrFieldDetailsType fieldType1 = new SsrFieldDetailsType();
		// Weight
		fieldType1.setSsrFieldName(SsrFieldNameDetails_Type.value2);
		fieldType1.setSsrFieldValue(ssrBaggageInfo.getDesc().split("K")[0]);
		SsrFieldDetailsType fieldType2 = new SsrFieldDetailsType();
		// UNITS [KG/LB]
		fieldType2.setSsrFieldName(SsrFieldNameDetails_Type.value3);
		fieldType2.setSsrFieldValue(String.valueOf("KG"));
		ssrFieldDetailsTypes[0] = fieldType1;
		ssrFieldDetailsTypes[1] = fieldType2;
		return ssrFieldDetailsTypes;
	}

	public String getAgencyCode() {
		return configuration.getSupplierCredential().getOrganisationCode();
	}

	public BookerDetailsType buildBookerDetails() {
		BookerDetailsType bookerDetails = new BookerDetailsType();
		bookerDetails.setNamePrefix(NameTitle_Type.MR);
		bookerDetails.setGivenName(clientInfo.getName());
		return bookerDetails;
	}


}
