package com.tgs.services.fms.sources.otaradixx;

import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Pair;
import org.springframework.stereotype.Service;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RadixxSessionLessAirInfoFactory extends RadixxAirInfoFactory {

	public RadixxSessionLessAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize();
		if (listener == null) {
			listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
					supplierConf.getBasicInfo().getSupplierName());
		}
		RadixxSearchManager searchManager =
				RadixxSearchManager.builder().configuration(supplierConf).airline(airline).searchQuery(searchQuery)
						.bindingService(bindingService).listener(listener).sourceConfiguration(sourceConfiguration)
						.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
		searchManager.init();
		searchResult = searchManager.doFareQuoteShopSearch();
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		TripInfo newTripInfo = null;
		SupplierSession session = null;
		boolean isReviewSuccess = true;
		try {
			initialize();
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().tripKey(selectedTrip.getSegmentsBookingKey()).build();
			session = getSessionIfExists(bookingId, sessionInfo);
			if (Objects.nonNull(session)) {
				binaryToken = session.getSupplierSessionInfo().getSessionToken();
			}

			// We need to pass the Logical flight Id and Fare basis code in RetireveFareQuote Request
			List<TripInfo> trips = AirUtils.splitTripInfo(selectedTrip, false);
			for (int index = 0; index < searchQuery.getRouteInfos().size(); index++) {
				RouteInfo route = searchQuery.getRouteInfos().get(index);
				PriceInfo price = trips.get(index).getSegmentInfos().get(0).getPriceInfo(0);
				Pair<Integer, String> lfidFBCPair = new Pair<Integer, String>(
						price.getMiscInfo().getLogicalFlightId().intValue(), price.getFareBasis(PaxType.ADULT));
				lfidFBCMap.put(route.getRouteKey(), lfidFBCPair);
			}
			super.searchAvailableSchedules();
			boolean isDomReturn =
					searchQuery.isDomesticReturn() && AirUtils.splitTripInfo(selectedTrip, false).size() == 2;

			searchResult = combineResults(searchResult, isDomReturn);

			newTripInfo = RadixxUtils.filterResults(searchResult, selectedTrip);
			if (newTripInfo == null) {
				throw new NoSeatAvailableException();
			}
			RadixxSSRManager ssrManager =
					RadixxSSRManager.builder().bindingService(bindingService).configuration(supplierConf)
							.airline(airline).searchQuery(searchQuery).binaryToken(binaryToken).bookingId(bookingId)
							.listener(listener).criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			ssrManager.init();
			newTripInfo = ssrManager.getAARSpecialServices(newTripInfo, searchQuery);
		} catch (NoSeatAvailableException e) {
			isReviewSuccess = false;
			throw e;
		} finally {
			if (Objects.isNull(session) && isReviewSuccess) {
				SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().sessionToken(binaryToken).build();
				storeBookingSession(bookingId, sessionInfo, session, newTripInfo);
			}
		}
		return newTripInfo;
	}

}
