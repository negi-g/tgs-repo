package com.tgs.services.fms.sources.travelport.sessionless;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirSSRInfoOutput;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.restmodel.air.RetrieveTicketStatusRequest;
import com.tgs.services.oms.restmodel.air.RetrieveTicketStatusResponse;
import com.tgs.utils.exception.air.NoPNRFoundException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import travelport.www.schema.air_v47_0.AirPricingInfo_type0;
import travelport.www.schema.air_v47_0.AirRetrieveDocumentReq;
import travelport.www.schema.air_v47_0.AirRetrieveDocumentRsp;
import travelport.www.schema.air_v47_0.BookingInfo_type0;
import travelport.www.schema.air_v47_0.FareInfo_type0;
import travelport.www.schema.air_v47_0.FlightDetails_type0;
import travelport.www.schema.air_v47_0.PassengerType_type0;
import travelport.www.schema.air_v47_0.TicketInfo_type0;
import travelport.www.schema.air_v47_0.TypeBaseAirSegment;
import travelport.www.schema.common_v47_0.BookingTraveler_type0;
import travelport.www.schema.common_v47_0.LoyaltyCard_type0;
import travelport.www.schema.common_v47_0.SSR_type0;
import travelport.www.schema.common_v47_0.StringLength1To13;
import travelport.www.schema.common_v47_0.SupplierLocator_type0;
import travelport.www.schema.common_v47_0.TypeTicketStatus;
import travelport.www.schema.universal_v47_0.SupportedVersions;
import travelport.www.schema.universal_v47_0.UniversalRecordChoice_type7;
import travelport.www.schema.universal_v47_0.UniversalRecordImportReq;
import travelport.www.schema.universal_v47_0.UniversalRecordImportRsp;
import travelport.www.service.air_v47_0.AirServiceStub;
import travelport.www.service.universal_v47_0.UniversalRecordFaultMessage;
import travelport.www.service.universal_v47_0.UniversalRecordImportServiceStub;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class TravelPortSessionLessBookingRetrieveManager extends TravelPortSessionLessServiceManager {

	private Map<String, TypeBaseAirSegment> airSegmentReferenceMap;
	private Map<String, AirPricingInfo_type0> airPricingInfoMap;
	private Map<String, BookingTraveler_type0> bookingTravelerMap;
	private Map<String, TicketInfo_type0> ticketInfoMap;
	private AirSSRInfoOutput airSSRInfoOutput;

	private static final String GST_P = "GSTP";
	private static final String GST_A = "GSTA";
	private static final String GST_N = "GSTN";


	public UniversalRecordImportRsp getUnivRecordImportRS(String pnr) {
		UniversalRecordImportServiceStub importService = bindingService.getUniversalRecordImportService();
		listener.setType(AirUtils.getLogType("UniversalRecordImport", configuration));
		importService._getServiceClient().getAxisService().addMessageContextListener(listener);
		UniversalRecordImportReq universalRecordImportReq = buildUniversalImportPnrReq(pnr);
		UniversalRecordImportRsp universalRecordImportRsp = null;
		try {
			SupportedVersions version = new SupportedVersions();
			bindingService.setProxyAndAuthentication(importService, "UnivRecordImport");
			universalRecordImportRsp = importService.service(universalRecordImportReq, version);
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e.getMessage());
		} catch (UniversalRecordFaultMessage universalRecordFaultMessage) {
			throw new NoPNRFoundException(AirSourceConstants.AIR_PNR_JOURNEY_UNAVAILABLE + pnr);
		} finally {
			importService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return universalRecordImportRsp;
	}

	public UniversalRecordImportReq buildUniversalImportPnrReq(String pnr) {
		UniversalRecordImportReq universalRecordImportReq = new UniversalRecordImportReq();
		buildBaseCoreRequest(universalRecordImportReq);
		universalRecordImportReq.setProviderLocatorCode(getTypeProviderLocatorCode(pnr));
		if (StringUtils.isNotBlank(configuration.getSupplierCredential().getProviderCode())) {
			universalRecordImportReq
					.setProviderCode(getProviderCode(configuration.getSupplierCredential().getProviderCode()));
		}
		return universalRecordImportReq;
	}

	public AirImportPnrBooking retrieveBooking(UniversalRecordImportRsp universalRecordImportRsp) {
		AirImportPnrBooking pnrBooking = null;
		if (universalRecordImportRsp != null) {
			List<TripInfo> tripInfos = fetchTripInfo(universalRecordImportRsp);
			GstInfo gstInfo = fetchGSTInfo(universalRecordImportRsp);
			DeliveryInfo deliveryInfo = fetchDeliveryInfo(universalRecordImportRsp);
			pnrBooking = AirImportPnrBooking.builder().tripInfos(tripInfos).deliveryInfo(deliveryInfo).gstInfo(gstInfo)
					.build();
		} else {
			throw new NoPNRFoundException(AirSourceConstants.AIR_PNR_JOURNEY_UNAVAILABLE + pnr);
		}
		return pnrBooking;
	}


	private List<TripInfo> fetchTripInfo(UniversalRecordImportRsp importRsp) {
		List<TripInfo> tripInfos = new ArrayList<>();
		if (ArrayUtils.isEmpty(importRsp.getUniversalRecord().getUniversalRecordChoice_type7().getAirReservation()[0]
				.getAirPricingInfo())) {
			throw new NoPNRFoundException(AirSourceConstants.AIR_PNR_JOURNEY_UNAVAILABLE + pnr);
		}
		airSegmentReferenceMap = new HashMap<>();
		airPricingInfoMap = new HashMap<>();
		bookingTravelerMap = new HashMap<>();
		ticketInfoMap = new HashMap<>();
		TripInfo tripInfo = new TripInfo();
		List<SegmentInfo> segmentInfoList = new ArrayList<>();

		UniversalRecordChoice_type7 airReservation = importRsp.getUniversalRecord().getUniversalRecordChoice_type7();

		for (TypeBaseAirSegment airSegment : airReservation.getAirReservation()[0].getAirSegment()) {
			airSegmentReferenceMap.put(airSegment.getKey().getTypeRef(), airSegment);
		}

		for (AirPricingInfo_type0 pricingInfo_type0 : airReservation.getAirReservation()[0].getAirPricingInfo()) {
			airPricingInfoMap.put(pricingInfo_type0.getKey().getTypeRef(), pricingInfo_type0);
		}

		for (BookingTraveler_type0 traveler_type0 : importRsp.getUniversalRecord().getBookingTraveler()) {
			bookingTravelerMap.put(traveler_type0.getKey().getTypeRef(), traveler_type0);
		}


		if (Objects.nonNull(airReservation.getAirReservation()[0].getDocumentInfo())
				&& Objects.nonNull(airReservation.getAirReservation()[0].getDocumentInfo().getTicketInfo())) {
			for (TicketInfo_type0 ticketInfo_type0 : airReservation.getAirReservation()[0].getDocumentInfo()
					.getTicketInfo()) {
				ticketInfoMap.put(ticketInfo_type0.getBookingTravelerRef().getTypeRef(), ticketInfo_type0);
			}
		}

		AtomicInteger segmentNumber = new AtomicInteger(0);

		for (FareInfo_type0 fareInfo : airReservation.getAirReservation()[0].getAirPricingInfo()[0].getFareInfo()) {
			List<SegmentInfo> segmentInfos = fetchSegments(fareInfo,
					airReservation.getAirReservation()[0].getAirSegment(),
					airReservation.getAirReservation()[0].getAirPricingInfo()[0].getBookingInfo(), segmentNumber);
			segmentInfoList.addAll(segmentInfos);
		}

		segmentInfos = segmentInfoList;
		updateAirinePnr(airReservation.getAirReservation()[0].getSupplierLocator(), segmentInfoList);
		if (CollectionUtils.isNotEmpty(segmentInfoList)) {
			tripInfo.setSegmentInfos(segmentInfoList);
		}
		tripInfos.add(tripInfo);
		return tripInfos;
	}

	private void updateAirinePnr(SupplierLocator_type0[] supplierLocatorList, List<SegmentInfo> segmentInfoList) {
		for (SupplierLocator_type0 supplierLocator : supplierLocatorList) {
			setAirlinePnrForSegment(supplierLocator.getSupplierCode().getTypeCarrier(),
					supplierLocator.getSupplierLocatorCode());
		}
	}

	private List<SegmentInfo> fetchSegments(FareInfo_type0 fareInfo, TypeBaseAirSegment[] airSegments,
			BookingInfo_type0[] bookingInfoList, AtomicInteger segmentNumber) {
		TypeBaseAirSegment airSegment = null;
		BookingInfo_type0 bookingInfo = null;
		for (BookingInfo_type0 bookingInfoItem : bookingInfoList) {
			if (bookingInfoItem.getFareInfoRef().getTypeRef().equals(fareInfo.getKey().getTypeRef())) {
				airSegment = airSegmentReferenceMap.get(bookingInfoItem.getSegmentRef().getTypeRef());
				bookingInfo = bookingInfoItem;
			}
		}
		FlightBasicFact flightFact = FlightBasicFact.builder().build();
		BaseUtils.createFactOnUser(flightFact, bookingUser);
		flightFact.setSourceId(configuration.getSourceId());
		AirConfiguratorInfo airConfigRuleInfo =
				AirConfiguratorHelper.getAirConfigRuleInfo(flightFact, AirConfiguratorRuleType.SSR_INFO);
		if (Objects.nonNull(airConfigRuleInfo))
			airSSRInfoOutput = (AirSSRInfoOutput) airConfigRuleInfo.getOutput();
		List<SegmentInfo> segmentInfoList = new ArrayList<>();
		for (FlightDetails_type0 flightDetails : airSegment.getFlightDetails()) {
			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setSegmentNum(segmentNumber.get());
			FlightDesignator flightDesignator = new FlightDesignator();
			flightDesignator.setFlightNumber(airSegment.getFlightNumber().getTypeFlightNumber());
			flightDesignator.setEquipType(airSegment.getEquipment().getTypeEquipment());
			flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(airSegment.getCarrier().getTypeCarrier()));
			segmentInfo.setFlightDesignator(flightDesignator);
			segmentInfo.setIsReturnSegment(false);

			AirlineInfo operatingCarrier = getOperatingCarrier(airSegment.getCodeshareInfo());
			if (operatingCarrier != null
					&& !flightDesignator.getAirlineCode().equalsIgnoreCase(operatingCarrier.getCode())) {
				segmentInfo.setOperatedByAirlineInfo(operatingCarrier);
			}
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirportInfo(flightDetails.getOrigin().getTypeIATACode()));
			segmentInfo.setArrivalAirportInfo(
					AirportHelper.getAirportInfo(flightDetails.getDestination().getTypeIATACode()));
			if (flightDetails.getOriginTerminal() != null) {
				String departureTerminal = AirUtils.getTerminalInfo(flightDetails.getOriginTerminal());
				segmentInfo.getDepartAirportInfo().setTerminal(departureTerminal);
			}
			if (flightDetails.getDestinationTerminal() != null) {
				String arrivalTerminal = AirUtils.getTerminalInfo(flightDetails.getDestinationTerminal());
				segmentInfo.getArrivalAirportInfo().setTerminal(arrivalTerminal);
			}
			segmentInfo.setDepartTime(TravelPortSessionLessUtils.getIsoDateTime(flightDetails.getDepartureTime()));
			segmentInfo.setArrivalTime(TravelPortSessionLessUtils.getIsoDateTime(flightDetails.getArrivalTime()));
			if (flightDetails.getFlightTime() != null) {
				segmentInfo.setDuration(Long.valueOf(flightDetails.getFlightTime().toString()));
			} else if (airSegment.getTravelTime() != null) {
				segmentInfo.setDuration(Long.valueOf(airSegment.getTravelTime().toString()));
			} else {
				segmentInfo.setDuration(segmentInfo.calculateDuration());
			}
			List<PriceInfo> priceInfos = new ArrayList<>();
			PriceInfo priceInfo = PriceInfo.builder().build();
			priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
			priceInfo.getMiscInfo().setIsPrivateFare(TravelPortSessionLessUtils.isPrivateFare(fareInfo));
			AirlineInfo platingCarrier = AirlineHelper.getAirlineInfo(airSegment.getCarrier().getTypeCarrier());
			priceInfo.getMiscInfo().setPlatingCarrier(platingCarrier);
			priceInfos.add(priceInfo);
			segmentInfo.setPriceInfoList(priceInfos);
			segmentInfo.setBookingRelatedInfo(getBookingRelatedInfo(fareInfo, bookingInfo, segmentNumber));
			segmentInfoList.add(segmentInfo);
			segmentNumber.incrementAndGet();
		}
		return segmentInfoList;
	}

	private SegmentBookingRelatedInfo getBookingRelatedInfo(FareInfo_type0 fareInfo, BookingInfo_type0 bookingInfo,
			AtomicInteger segmentNumber) {
		List<FlightTravellerInfo> travellerInfoList = new ArrayList<>();
		for (Map.Entry<String, AirPricingInfo_type0> entry : airPricingInfoMap.entrySet()) {
			for (PassengerType_type0 passengerType : entry.getValue().getPassengerType()) {
				FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
				BookingTraveler_type0 bookingTraveler = bookingTravelerMap.get(passengerType.getBookingTravelerRef());
				travellerInfo.setTitle(TravelPortSessionLessUtils.getTitle(bookingTraveler));
				travellerInfo.setPaxType(TravelPortSessionLessUtils.getPaxType(bookingTraveler));
				travellerInfo.setFirstName(TravelPortSessionLessUtils.getPaxFirstName(bookingTraveler));
				travellerInfo.setLastName(TravelPortSessionLessUtils.getPaxLastName(bookingTraveler));
				travellerInfo.setSupplierBookingId(pnr);
				TypeBaseAirSegment airSegment = airSegmentReferenceMap.get(bookingInfo.getSegmentRef().getTypeRef());
				travellerInfo.setFareDetail(getFareDetail(entry.getValue(), fareInfo, bookingInfo, segmentNumber));
				String platingCarrier = airSegment.getCarrier().getTypeCarrier();
				if (ArrayUtils.isNotEmpty(bookingTraveler.getBaseBookingTravelerInfoA().getLoyaltyCard()))
					updateFreqFlierInfo(bookingTraveler.getBaseBookingTravelerInfoA().getLoyaltyCard(), travellerInfo,
							platingCarrier);
				updateSpecialServicesAndTicketNumbers(travellerInfo, bookingTraveler,
						bookingInfo.getSegmentRef().getTypeRef());
				updatePassportInformation(getList(bookingTraveler.getSSR()), travellerInfo);
				travellerInfoList.add(travellerInfo);
			}
		}
		SegmentBookingRelatedInfo bookingRelatedInfo =
				SegmentBookingRelatedInfo.builder().travellerInfo(travellerInfoList).build();
		return bookingRelatedInfo;
	}

	private void updatePassportInformation(List<SSR_type0> ssrlist, FlightTravellerInfo travellerInfo) {
		String freeText = null;
		for (SSR_type0 ssr : ssrlist) {
			if (ssr.getType().getTypeSSRCode().equals("DOCS")) {
				freeText = ssr.getFreeText().getTypeSSRFreeText();
				break;
			}
		}
		if (freeText != null) {
			String[] passportInformation = freeText.split("/");
			travellerInfo.setPassportNationality(passportInformation[1]);
			travellerInfo.setPassportNumber(passportInformation[2]);
			travellerInfo.setExpiryDate(TravelPortSessionLessUtils.createDate(passportInformation[6]));
		}
	}

	private void updateSpecialServicesAndTicketNumbers(FlightTravellerInfo travellerInfo,
			BookingTraveler_type0 bookingTraveler, String airSegmentKey) {
		List<SSR_type0> ssrlist = getList(bookingTraveler.getSSR());
		Map<String, String> mealCodes = null;
		Map<String, String> extraServiceCodes = null;
		if (Objects.nonNull(airSSRInfoOutput)) {
			mealCodes = airSSRInfoOutput.getMealSsrList().stream()
					.collect(Collectors.toMap(SSRInformation::getCode, SSRInformation::getDesc));
			extraServiceCodes = airSSRInfoOutput.getExtraSsrList().stream()
					.collect(Collectors.toMap(SSRInformation::getCode, SSRInformation::getDesc));
		}
		if (CollectionUtils.isNotEmpty(ssrlist)) {
			for (SSR_type0 ssr : ssrlist) {
				if (ssr.getSegmentRef() != null && StringUtils.isNotBlank(ssr.getSegmentRef().getTypeRef())
						&& ssr.getSegmentRef().getTypeRef().equals(airSegmentKey)) {
					SSRInformation ssrInfo = new SSRInformation();
					if (MapUtils.isNotEmpty(mealCodes) && ssr.getType() != null
							&& mealCodes.containsKey(ssr.getType().getTypeSSRCode().toUpperCase())) {
						ssrInfo.setCode(ssr.getType().getTypeSSRCode().toUpperCase());
						ssrInfo.setDesc(mealCodes.get(ssr.getType().getTypeSSRCode().toUpperCase()));
						travellerInfo.setSsrMealInfo(ssrInfo);
					} else if (MapUtils.isNotEmpty(extraServiceCodes) && ssr.getType() != null
							&& extraServiceCodes.containsKey(ssr.getType().getTypeSSRCode().toUpperCase())) {
						ssrInfo.setCode(ssr.getType().getTypeSSRCode().toUpperCase());
						ssrInfo.setDesc(extraServiceCodes.get(ssr.getType().getTypeSSRCode().toUpperCase()));
						List<SSRInformation> extraServices =
								(CollectionUtils.isEmpty(travellerInfo.getExtraServices())) ? new ArrayList<>()
										: travellerInfo.getExtraServices();
						extraServices.add(ssrInfo);
						travellerInfo.setExtraServices(extraServices);
					} else if (ssr.getType() != null && ssr.getType().getTypeSSRCode().toUpperCase().equals("TKNE")) {
						travellerInfo.setTicketNumber(ssr.getFreeText().getTypeSSRFreeText());
					}
				}
			}
		}
	}

	private void updateFreqFlierInfo(LoyaltyCard_type0[] loyaltyCard, FlightTravellerInfo traveller,
			String platingCarrier) {
		if (traveller.getFrequentFlierMap() == null) {
			traveller.setFrequentFlierMap(new HashMap<>());
		}
		traveller.getFrequentFlierMap().put(platingCarrier, loyaltyCard[0].getCardNumber().getTypeCardNumber());
	}

	private FareDetail getFareDetail(AirPricingInfo_type0 airPricingInfo, FareInfo_type0 fareInfo,
			BookingInfo_type0 bookingInfo, AtomicInteger segmentNumber) {
		FareDetail fareDetail = new FareDetail();
		Map<FareComponent, Double> fareComponents = new HashMap<>();
		fareDetail.setFareBasis(fareInfo.getFareBasis());
		fareDetail.setClassOfBooking(bookingInfo.getBookingCode());
		fareDetail.setCabinClass(TravelPortSessionLessUtils.getCabinClass(bookingInfo));
		fareDetail.setRefundableType(TravelPortSessionLessUtils.getRefundableType(airPricingInfo));
		if (segmentNumber.get() == 0) {
			fareComponents.put(FareComponent.BF, getEquivalentBaseFare(airPricingInfo, 1));
			fareComponents.put(FareComponent.TF, getEquivalentTotalfare(airPricingInfo, 1));
			// fareComponents.put(FareComponent.AT, getAmountBasedOnCurrency(airPricingInfo.getTaxes(),
			// airPricingInfo.getTaxes().substring(0, 3), 1.0));
			fareDetail.setFareComponents(fareComponents);
			if (Objects.nonNull(airPricingInfo.getTaxInfo())) {
				setTaxDetails(fareDetail, getList(airPricingInfo.getTaxInfo()), 1);
			}
		}
		setBaggageAllowance(fareDetail, fareInfo);
		fareDetail.setFareType(airPricingInfo.getPricingType().getPricingType_type0());
		return fareDetail;
	}

	private DeliveryInfo fetchDeliveryInfo(UniversalRecordImportRsp universalRecordImportRsp) {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		List<BookingTraveler_type0> bookingTravelersList =
				getList(universalRecordImportRsp.getUniversalRecord().getBookingTraveler());
		List<String> emailList = new ArrayList<>();
		List<String> contactList = new ArrayList<>();

		for (BookingTraveler_type0 bookingTraveler : bookingTravelersList) {
			if (bookingTraveler.getBaseBookingTravelerInfoA() != null
					&& bookingTraveler.getBaseBookingTravelerInfoA().getEmail() != null
					&& bookingTraveler.getBaseBookingTravelerInfoA().getEmail().length != 0) {
				String email = getList(bookingTraveler.getBaseBookingTravelerInfoA().getEmail()).get(0).getEmailID();
				emailList.add(email);
			}
			if (bookingTraveler.getBaseBookingTravelerInfoA() != null
					&& bookingTraveler.getBaseBookingTravelerInfoA().getPhoneNumber() != null
					&& bookingTraveler.getBaseBookingTravelerInfoA().getPhoneNumber().length != 0) {
				String contact = getList(bookingTraveler.getBaseBookingTravelerInfoA().getPhoneNumber()).get(0)
						.getNumber().getNumber_type0();
				contactList.add(contact);
			}
		}

		if (CollectionUtils.isNotEmpty(emailList)) {
			deliveryInfo.setEmails(emailList);
		}
		if (CollectionUtils.isNotEmpty(contactList)) {
			deliveryInfo.setContacts(contactList);
		}
		return deliveryInfo;
	}

	private GstInfo fetchGSTInfo(UniversalRecordImportRsp universalRecordImportRsp) {
		GstInfo gstInfo = null;
		if (universalRecordImportRsp != null && universalRecordImportRsp.getUniversalRecord() != null
				&& ArrayUtils.isNotEmpty(universalRecordImportRsp.getUniversalRecord().getSSR())) {
			gstInfo = new GstInfo();
			SSR_type0[] ssrList = universalRecordImportRsp.getUniversalRecord().getSSR();
			for (SSR_type0 ssr : ssrList) {
				if (ssr.getType() != null && StringUtils.isNotEmpty(ssr.getType().getTypeSSRCode())) {
					if (StringUtils.equalsIgnoreCase(GST_N, ssr.getType().getTypeSSRCode())) {
						String[] gstData = ssr.getFreeText().getTypeSSRFreeText().split("/");
						if (gstData.length >= 4) {
							gstInfo.setGstNumber(gstData[2]);
							gstInfo.setRegisteredName(gstData[3]);
						}
					} else if (StringUtils.equalsIgnoreCase(GST_A, ssr.getType().getTypeSSRCode())) {
						String[] gstData = ssr.getFreeText().getTypeSSRFreeText().split("/");
						if (gstData.length >= 7) {
							gstInfo.setAddress(gstData[2]);
							gstInfo.setCityName(gstData[4]);
							gstInfo.setState(gstData[5]);
							gstInfo.setPincode(gstData[6]);
						}
					} else if (StringUtils.equalsIgnoreCase(GST_P, ssr.getType().getTypeSSRCode())) {
						String[] gstData = ssr.getFreeText().getTypeSSRFreeText().split("/");
						if (gstData.length >= 3) {
							gstInfo.setMobile(gstData[2]);
						}
					}
				}
			}
		}
		return gstInfo;
	}

	public RetrieveTicketStatusResponse retrieveTicketStatus(RetrieveTicketStatusRequest ticketStatusRequest) {
		RetrieveTicketStatusResponse ticketStatusResponse = null;
		AirServiceStub airService = bindingService.getAirService();
		listener.setType(AirUtils.getLogType("AirRetrieveDocument", configuration));
		listener.setKey(ticketStatusRequest.getTicketNumbers().get(0));
		AirRetrieveDocumentReq airRetrieveDocumentReq = createAirRetrieveDocumentReq(ticketStatusRequest);
		AirRetrieveDocumentRsp airRetrieveDocumentResponse = null;
		try {
			airService._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(airService, "AirRetrieveDocument");
			airRetrieveDocumentResponse = airService.service(airRetrieveDocumentReq, null);
			if (airRetrieveDocumentResponse != null) {
				ticketStatusResponse = parseAirRetrieveDocumentRsp(airRetrieveDocumentResponse);
			}
		} catch (Exception e) {
			log.error("Exception occurred while retrieving ticket status for ticket Number {} exception {} ",
					ticketStatusRequest.getTicketNumbers().get(0), e.getMessage());
			throw new CustomGeneralException(SystemError.INVALID_TICKET_STATUS,
					SystemError.INVALID_TICKET_STATUS.getMessage(ticketStatusRequest.getTicketNumbers()));
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return ticketStatusResponse;
	}

	private AirRetrieveDocumentReq createAirRetrieveDocumentReq(RetrieveTicketStatusRequest ticketStatusRequest) {
		AirRetrieveDocumentReq retrieveDocumentRequest = new AirRetrieveDocumentReq();
		buildBaseCoreRequest(retrieveDocumentRequest);
		retrieveDocumentRequest
				.setProviderCode(getTypeProviderCode(configuration.getSupplierCredential().getProviderCode()));
		StringLength1To13[] ticketNumber = new StringLength1To13[1];
		StringLength1To13 str = new StringLength1To13();
		str.setStringLength1To13(ticketStatusRequest.getTicketNumbers().get(0));
		ticketNumber[0] = str;
		retrieveDocumentRequest.setTicketNumber(ticketNumber);
		return retrieveDocumentRequest;
	}

	private RetrieveTicketStatusResponse parseAirRetrieveDocumentRsp(AirRetrieveDocumentRsp airRetrieveDocumentRsp) {
		RetrieveTicketStatusResponse ticketStatusResponse = null;
		if (ArrayUtils.isNotEmpty(airRetrieveDocumentRsp.getETR())
				&& ArrayUtils.isNotEmpty(airRetrieveDocumentRsp.getETR()[0].getTicket())
				&& airRetrieveDocumentRsp.getETR()[0].getTicket()[0].getTicketStatus() != null) {
			TypeTicketStatus status = airRetrieveDocumentRsp.getETR()[0].getTicket()[0].getTicketStatus();
			String ticketNumber =
					airRetrieveDocumentRsp.getETR()[0].getTicket()[0].getTicketNumber().getTypeTicketNumber();
			ticketStatusResponse = new RetrieveTicketStatusResponse();
			ticketStatusResponse.setTicketStatus(status.getValue());
			ticketStatusResponse.setTicketNumber(ticketNumber);
			log.info("TicketStatus for {} and Status {}", ticketNumber, status.getValue());
		} else {
			throw new CustomGeneralException("Ticket status not present in the supplier response");
		}
		return ticketStatusResponse;
	}


}
