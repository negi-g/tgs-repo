package com.tgs.services.fms.validator;

import com.tgs.services.base.enums.FareType;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.AirFareTypeOutput;

@Service
public class AirFareTypeConfiguratorValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {
		AirFareTypeOutput output = (AirFareTypeOutput) iRuleOutput;
		if (output == null) {
			return;
		}
		if (output.getFareType() == null) {
			rejectValue(errors, ".fareType", SystemError.INVALID_FARE_TYPE);
		}

		if (FareType.SPECIAL_PRIVATE_FARE.getName().equals(output.getFareType())
				&& CollectionUtils.isEmpty(output.getComparativeFareTypes())) {
			rejectValue(errors, ".comparativeFareTypes", SystemError.INVALID_COMPARITIVE_FARE_TYPES);
		}
	}

}
