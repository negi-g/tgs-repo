package com.tgs.services.fms.validator;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.AirSupplierExclusionOutput;

@Service
public class AirSupplierExclusionValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {

		AirSupplierExclusionOutput ruleOutput = (AirSupplierExclusionOutput) iRuleOutput;
		if (ruleOutput == null) {
			errors.rejectValue(fieldName, SystemError.NULL_VALUE.errorCode(), SystemError.NULL_VALUE.getMessage());
			return;
		}
		List<String> excludedSuppliers = ruleOutput.getExcludedSupplierIds();
		if (CollectionUtils.isNotEmpty(excludedSuppliers)) {
			listValidator.validateUserIds(excludedSuppliers, fieldName + ".excludedSupplierIds", errors,
					SystemError.INVALID_FBRC_SUPPLIERID);
		}
	}


}
