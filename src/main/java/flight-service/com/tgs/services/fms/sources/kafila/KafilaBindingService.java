package com.tgs.services.fms.sources.kafila;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.Gson;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.kafila.login.LoginRequest;
import com.tgs.services.kafila.login.LoginResponse;
import com.tgs.services.kafila.pnrCreationRequest.PnrCreationRequest;
import com.tgs.services.kafila.pnrCreationResponse.PnrCreationResponse;
import com.tgs.services.kafila.pnrRetrieveRequest.PnrRetrieveRequest;
import com.tgs.services.kafila.pnrRetrieveResponse.PnrRetrieveResponse;
import com.tgs.services.kafila.reviewRequest.ReviewRequest;
import com.tgs.services.kafila.reviewResponse.ReviewResponse;
import com.tgs.services.kafila.searchRequest.SearchRequest;
import com.tgs.services.kafila.searchresponse.FlightDetails;
import com.tgs.services.kafila.searchresponse.SearchResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Builder
@Slf4j
public class KafilaBindingService {

	private static final String AUTH_URL = "http://nauth.ksofttechnology.com";
	private static final String AUTH_REQ_MAP = "/API/AUTH";

	private static final String BASE_URL = "http://stageapi.ksofttechnology.com";
	private static final String SEARCH_RWQ_MAP = "/API/FLIGHT";

	private static final String REVIEW_REQ_MAP = "/API/AVLT";

	private static final String PNR_REQ_MAP = "/API/FLIGHT";

	protected User user;

	protected RestAPIListener listener;
	protected SupplierConfiguration configuration;

	private static final String AUTH_NAME = "GET_AUTH_TOKEN";
	private static final String AUTH = "AUTH";

	public Map<String, String> getHeaderParams() {
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("content-type", "application/json");
		headerParams.put("Accept-Encoding", "gzip, deflate");
		return headerParams;
	}

	public LoginResponse authenticate(LoginRequest loginRequest) {
		loginRequest.setNAME(AUTH_NAME);
		loginRequest.setTYPE(AUTH);
		String requestData = GsonUtils.getGson().toJson(loginRequest);
		LoginResponse responseBody = null;
		HttpUtils httpUtils = null;
		String urlString = getURL(true, false, false, false);
		try {
			httpUtils = HttpUtils.builder().urlString(urlString).postData(requestData).headerParams(getHeaderParams())
					.timeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS).proxy(AirSupplierUtils.getProxy(user))
					.build();
			responseBody = httpUtils.getResponse(LoginResponse.class).orElse(null);
		} catch (IOException ioe) {
			throw new SupplierRemoteException(ioe);
		} finally {
			String responseData = GsonUtils.getGson().toJson(responseBody);
			String endPointRQRS = StringUtils.join(urlString, "\n\n", "Authentication Request", "\n\n", requestData,
					"\n\n", "Authentication Response", "\n\n", responseData);
			listener.addLog(LogData.builder().key(listener.getKey()).logData(endPointRQRS)
					.type(AirUtils.getLogType("Authentication", configuration))
					.responseTime(httpUtils.getResponseTime()).build());
		}
		return responseBody;

	}

	public SearchResponse doSearch(SearchRequest searchRequest) {
		searchRequest.setNAME("GET_FLIGHT");
		searchRequest.setTYPE("AIR");
		String requestData = GsonUtils.getGson().toJson(searchRequest);
		SearchResponse responseBody = null;
		HttpUtils httpUtils = null;
		String urlString = getURL(false, true, false, false);
		try {
			httpUtils = HttpUtils.builder().urlString(urlString).postData(requestData).headerParams(getHeaderParams())
					.timeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS).proxy(AirSupplierUtils.getProxy(user))
					.build();
			Gson gson = GsonUtils.getGsonBuilder()
					.registerTypeAdapter(FlightDetails.class, new FlightDetails.FlightDetailSerializer()).create();
			httpUtils.setGsonSeriliazer(gson);
			responseBody = httpUtils.getResponse(SearchResponse.class).orElse(null);
		} catch (IOException ioe) {
			throw new SupplierRemoteException(ioe);
		} finally {
			String responseData = GsonUtils.getGson().toJson(responseBody);
			String endPointRQRS = StringUtils.join(urlString, "\n\n", "Search Request", "\n\n", requestData, "\n\n",
					"Search Response", "\n\n", responseData);
			listener.addLog(LogData.builder().key(listener.getKey()).logData(endPointRQRS)
					.type(AirUtils.getLogType("Search", configuration)).responseTime(httpUtils.getResponseTime())
					.build());
		}
		return responseBody;
	}

	public ReviewResponse doReview(ReviewRequest reviewRequest) {
		reviewRequest.setNAME("FARE_CHECK");
		reviewRequest.setTYPE("AIR");

		String requestData = GsonUtils.getGson().toJson(reviewRequest);
		ReviewResponse responseBody = null;
		HttpUtils httpUtils = null;
		String urlString = getURL(false, false, true, false);
		try {
			httpUtils = HttpUtils.builder().urlString(urlString).postData(requestData).headerParams(getHeaderParams())
					.timeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS).proxy(AirSupplierUtils.getProxy(user))
					.build();
			responseBody = httpUtils.getResponse(ReviewResponse.class).orElse(null);
			if (responseBody.isError()) {
				throw new NoSeatAvailableException(responseBody.errorMessage());
			}
		} catch (IOException ioe) {
			throw new SupplierRemoteException(ioe);
		} finally {
			String responseData = GsonUtils.getGson().toJson(responseBody);
			String endPointRQRS = StringUtils.join(urlString, "\n\n", "FareCheck Request", "\n\n", requestData, "\n\n",
					"FareCheck Response", "\n\n", responseData);
			listener.addLog(LogData.builder().key(listener.getKey()).logData(endPointRQRS)
					.type(AirUtils.getLogType("FareCheck", configuration)).responseTime(httpUtils.getResponseTime())
					.build());
		}
		return responseBody;
	}

	public PnrCreationResponse doBooking(PnrCreationRequest request) {
		request.setTYPE("DC");
		request.setNAME("PNR_CREATION");

		String requestData = GsonUtils.getGson().toJson(request);
		PnrCreationResponse responseBody = null;
		HttpUtils httpUtils = null;
		String urlString = getURL(false, false, false, true);
		try {
			httpUtils = HttpUtils.builder().urlString(urlString).postData(requestData).headerParams(getHeaderParams())
					.timeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS).proxy(AirSupplierUtils.getProxy(user))
					.build();
			Gson gson = GsonUtils.getGsonBuilder().serializeNulls().create();
			httpUtils.setGsonSeriliazer(gson);
			responseBody = httpUtils.getResponse(PnrCreationResponse.class).orElse(null);
		} catch (IOException ioe) {
			throw new SupplierRemoteException(ioe);
		} finally {
			String responseData = GsonUtils.getGson().toJson(responseBody);
			String endPointRQRS = StringUtils.join(urlString, "\n\n", "PnrCreation Request", "\n\n", requestData,
					"\n\n", "PnrCreation Response", "\n\n", responseData);
			listener.addLog(LogData.builder().key(listener.getKey()).logData(endPointRQRS)
					.type(AirUtils.getLogType("PnrCreation", configuration)).responseTime(httpUtils.getResponseTime())
					.build());
		}
		return responseBody;
	}

	public PnrRetrieveResponse getPnr(PnrRetrieveRequest request) {

		request.setTYPE("DC");
		request.setNAME("PNR_RETRIVE");

		String requestData = GsonUtils.getGson().toJson(request);
		PnrRetrieveResponse responseBody = null;
		HttpUtils httpUtils = null;
		String urlString = getURL(false, false, false, true);
		try {

			httpUtils = HttpUtils.builder().urlString(urlString).postData(requestData).headerParams(getHeaderParams())
					.timeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS).proxy(AirSupplierUtils.getProxy(user))
					.build();
			responseBody = httpUtils.getResponse(PnrRetrieveResponse.class).orElse(null);
		} catch (IOException ioe) {
			throw new SupplierRemoteException(ioe.getMessage());
		} finally {
			String responseData = GsonUtils.getGson().toJson(responseBody);
			String endPointRQRS = StringUtils.join(urlString, "\n\n", "PnrRetreieve Request", "\n\n", requestData,
					"\n\n", "PnrRetreieve Response", "\n\n", responseData);
			listener.addLog(LogData.builder().key(listener.getKey()).logData(endPointRQRS)
					.type(AirUtils.getLogType("PnrRetreieve", configuration)).responseTime(httpUtils.getResponseTime())
					.build());
		}
		return responseBody;
	}

	private String getURL(boolean isAuthenticate, boolean isSearch, boolean isFareCheck, boolean isBook) {
		String apiURL = geEndPoint(isAuthenticate);
		if (isAuthenticate) {
			return StringUtils.join(apiURL + AUTH_REQ_MAP);
		} else if (isSearch) {
			return StringUtils.join(apiURL + SEARCH_RWQ_MAP);
		} else if (isFareCheck) {
			return StringUtils.join(apiURL + REVIEW_REQ_MAP);
		} else if (isBook) {
			return StringUtils.join(apiURL + PNR_REQ_MAP);
		}
		return apiURL;
	}

	private String geEndPoint(boolean isAuth) {
		if (StringUtils.isNotBlank(configuration.getSupplierCredential().getUrl())) {
			String[] urls = StringUtils.split(configuration.getSupplierCredential().getUrl(), ";");
			return isAuth ? urls[0] : urls[1];
		}
		return isAuth ? AUTH_URL : BASE_URL;
	}


}
