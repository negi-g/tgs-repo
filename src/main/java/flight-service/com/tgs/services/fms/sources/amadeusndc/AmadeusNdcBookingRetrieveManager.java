package com.tgs.services.fms.sources.amadeusndc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.ws.BindingProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.iata.iata._2015._00._2018_1.orderretrieverq.AgencyTypeCodeContentType;
import org.iata.iata._2015._00._2018_1.orderretrieverq.CarrierType;
import org.iata.iata._2015._00._2018_1.orderretrieverq.CountryType;
import org.iata.iata._2015._00._2018_1.orderretrieverq.IATAPayloadStandardAttributesType;
import org.iata.iata._2015._00._2018_1.orderretrieverq.OrderFilterCriteriaType;
import org.iata.iata._2015._00._2018_1.orderretrieverq.OrderRetrieveRQ;
import org.iata.iata._2015._00._2018_1.orderretrieverq.PartyType;
import org.iata.iata._2015._00._2018_1.orderretrieverq.PointofSaleType2;
import org.iata.iata._2015._00._2018_1.orderretrieverq.RecipientType;
import org.iata.iata._2015._00._2018_1.orderretrieverq.RequestType;
import org.iata.iata._2015._00._2018_1.orderretrieverq.SenderType;
import org.iata.iata._2015._00._2018_1.orderretrieverq.TravelAgencyType;
import org.iata.iata._2015._00._2018_1.orderviewrs.BaggageAllowanceType;
import org.iata.iata._2015._00._2018_1.orderviewrs.ContactInfoListType;
import org.iata.iata._2015._00._2018_1.orderviewrs.ContactInfoType;
import org.iata.iata._2015._00._2018_1.orderviewrs.DatedMarketingSegmentType;
import org.iata.iata._2015._00._2018_1.orderviewrs.DatedOperatingSegmentType;
import org.iata.iata._2015._00._2018_1.orderviewrs.EmailAddressType;
import org.iata.iata._2015._00._2018_1.orderviewrs.FareComponentType;
import org.iata.iata._2015._00._2018_1.orderviewrs.FareComponentType.SegmentRefs;
import org.iata.iata._2015._00._2018_1.orderviewrs.FareDetailType;
import org.iata.iata._2015._00._2018_1.orderviewrs.FareDetailType.PassengerRefs;
import org.iata.iata._2015._00._2018_1.orderviewrs.FarePriceDetailType;
import org.iata.iata._2015._00._2018_1.orderviewrs.IdentityDocType;
import org.iata.iata._2015._00._2018_1.orderviewrs.JourneyListType;
import org.iata.iata._2015._00._2018_1.orderviewrs.LoyaltyProgramAccountType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderItemType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderType;
import org.iata.iata._2015._00._2018_1.orderviewrs.OrderViewRS;
import org.iata.iata._2015._00._2018_1.orderviewrs.PaxJourneyType;
import org.iata.iata._2015._00._2018_1.orderviewrs.PaxSegmentType;
import org.iata.iata._2015._00._2018_1.orderviewrs.PaxType;
import org.iata.iata._2015._00._2018_1.orderviewrs.PhoneType;
import org.iata.iata._2015._00._2018_1.orderviewrs.ResponseType;
import org.iata.iata._2015._00._2018_1.orderviewrs.ResponseType.TicketDocInfos.TicketDocInfo;
import org.iata.iata._2015._00._2018_1.orderviewrs.ServiceDefinitionType;
import org.iata.iata._2015._00._2018_1.orderviewrs.ServiceType;
import org.iata.iata._2015._00._2018_1.orderviewrs.TaxDetailType.Breakdown.Tax;
import org.iata.iata._2015._00._2018_1.servicelistrq.TravelAgencyTypeCodeContentType;
import org.iata.iata._2015._00._2018_1.orderviewrs.TicketDocument;
import org.iata.iata._2015._00._2018_1.orderviewrs.TransportArrivalType;
import org.iata.iata._2015._00._2018_1.orderviewrs.TransportDepType;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.google.common.base.Enums;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.utils.AirUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
public class AmadeusNdcBookingRetrieveManager extends AmadeusNdcServiceManager {

	private Map<String, PaxSegmentType> paxSegmentMap;

	private Map<String, BaggageAllowanceType> baggageInfoMap;

	public OrderViewRS orderRetrieve() {
		AlteaNDC181PT servicePort = service.getAlteaNDC181Port();
		BindingProvider bp = (BindingProvider) servicePort;
		addJAXHandlers(bp, bookingId, AmadeusNdcHeaderHandler._ORDER_RETRIEVE_);
		boolean isRetry = true;
		int maxRetry = 4;
		int count = 0;
		OrderViewRS orderviewResponse = null;
		while (isRetry && count < maxRetry) {
			orderviewResponse =
					servicePort.ndcOrderRetrieve(createOrderRetrieveRq(), null, null, getSecurityHostedUser());
			if (isAnyError(orderviewResponse))
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					log.error("Could not retrieve booking {}", bookingId);
					break;
				}
			else
				isRetry = false;
			count++;
		}
		return orderviewResponse;
	}

	private OrderRetrieveRQ createOrderRetrieveRq() {
		OrderRetrieveRQ orderRetrieveRq = new OrderRetrieveRQ();
		RequestType requestType = new RequestType();
		OrderFilterCriteriaType orderFilterCriteriaType = new OrderFilterCriteriaType();
		org.iata.iata._2015._00._2018_1.orderretrieverq.OrderType orderType =
				new org.iata.iata._2015._00._2018_1.orderretrieverq.OrderType();
		orderType.setOrderID(getSupplierPNR());
		orderType.setOwnerCode("SQ");
		orderFilterCriteriaType.setOrder(orderType);
		requestType.setOrderFilterCriteria(orderFilterCriteriaType);
		orderRetrieveRq.setRequest(requestType);
		orderRetrieveRq.setPayloadAttributes(getPayloadAttributes());
		orderRetrieveRq.setPointOfSale(getPointOfSale());
		orderRetrieveRq.setParty(getParty());
		return orderRetrieveRq;
	}

	private PointofSaleType2 getPointOfSale() {
		PointofSaleType2 pointOfSale = new PointofSaleType2();
		CountryType countryType = new CountryType();
		countryType.setCountryCode(AmadeusNDCConstants.COUNTRY_CODE);
		pointOfSale.setCountry(countryType);
		return pointOfSale;
	}

	private PartyType getParty() {
		PartyType partyType = new PartyType();
		RecipientType recipientType = new RecipientType();
		CarrierType carrierType = new CarrierType();
		carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
		recipientType.setORA(carrierType);
		partyType.setRecipient(recipientType);


		SenderType senderType = new SenderType();
		TravelAgencyType travelAgencyType = new TravelAgencyType();
		travelAgencyType.setAgencyID(supplierConf.getSupplierCredential().getIataNumber());
		travelAgencyType.setIATANumber(new BigDecimal(supplierConf.getSupplierCredential().getIataNumber()));
		travelAgencyType.setName(supplierConf.getSupplierCredential().getAgentUserName());
		travelAgencyType.setTypeCode(AgencyTypeCodeContentType.ONLINE_TRAVEL_AGENCY);
		senderType.setTravelAgency(travelAgencyType);
		partyType.setSender(senderType);
		return partyType;
	}

	private IATAPayloadStandardAttributesType getPayloadAttributes() {
		IATAPayloadStandardAttributesType payloadAttributes = new IATAPayloadStandardAttributesType();
		payloadAttributes.setVersion(AmadeusNDCConstants.SERVICE_VERSION);
		return payloadAttributes;
	}

	public double getAmountTobePaidToAirline(OrderViewRS orderViewRs) {
		BigDecimal amountToBePaidToAirline = new BigDecimal(0);
		String currencyCode = null;
		if (orderViewRs != null && orderViewRs.getResponse() != null
				&& CollectionUtils.isNotEmpty(orderViewRs.getResponse().getOrder())) {
			for (OrderType order : orderViewRs.getResponse().getOrder()) {
				amountToBePaidToAirline =
						amountToBePaidToAirline.add(order.getTotalPrice().getTotalAmount().getValue());
				currencyCode = order.getTotalPrice().getTotalAmount().getCurCode();
			}
		}
		setAirlineTotalPrice(amountToBePaidToAirline);
		return getAmountBasedOnCurrency(amountToBePaidToAirline, currencyCode);
	}

	public boolean isSegmentsAndPNRActive(OrderViewRS orderViewRs) {
		boolean isSegmentsActive = false;
		if (orderViewRs != null && orderViewRs.getResponse() != null && orderViewRs.getResponse().getDataLists() != null
				&& orderViewRs.getResponse().getDataLists().getPaxList() != null
				&& CollectionUtils.isNotEmpty(orderViewRs.getResponse().getDataLists().getPaxList().getPax())
				&& orderViewRs.getResponse().getDataLists().getPaxJourneyList() != null
				&& CollectionUtils
						.isNotEmpty(orderViewRs.getResponse().getDataLists().getPaxJourneyList().getPaxJourney())
				&& orderViewRs.getResponse().getDataLists().getPaxSegmentList() != null
				&& CollectionUtils
						.isNotEmpty(orderViewRs.getResponse().getDataLists().getPaxSegmentList().getPaxSegment())
				&& CollectionUtils.isNotEmpty(orderViewRs.getResponse().getOrder())
				&& CollectionUtils.isNotEmpty(orderViewRs.getResponse().getOrder().get(0).getOrderItem())
				&& "NOT ENTITLED"
						.equals(orderViewRs.getResponse().getOrder().get(0).getOrderItem().get(0).getStatusCode())) {
			isSegmentsActive = true;
		}
		return isSegmentsActive;
	}

	public AirImportPnrBooking parseTripInfos(OrderViewRS orderViewRs) {
		AirImportPnrBooking pnrBooking = null;
		if (orderViewRs != null && CollectionUtils.isEmpty(orderViewRs.getErrors())
				&& orderViewRs.getResponse() != null) {
			ResponseType reseponseType = orderViewRs.getResponse();
			JourneyListType paxJourneyList = reseponseType.getDataLists().getPaxJourneyList();

			paxSegmentMap = new HashMap<>();
			for (PaxSegmentType paxSegment : reseponseType.getDataLists().getPaxSegmentList().getPaxSegment()) {
				paxSegmentMap.put(paxSegment.getPaxSegmentID(), paxSegment);
			}

			baggageInfoMap = new HashMap<>();
			if (reseponseType.getDataLists().getServiceDefinitionList() != null
					&& CollectionUtils
							.isNotEmpty(reseponseType.getDataLists().getServiceDefinitionList().getServiceDefinition())
					&& reseponseType.getDataLists().getBaggageAllowanceList() != null && CollectionUtils
							.isNotEmpty(reseponseType.getDataLists().getBaggageAllowanceList().getBaggageAllowance())) {
				for (ServiceDefinitionType serviceDefinition : reseponseType.getDataLists().getServiceDefinitionList()
						.getServiceDefinition()) {
					if ("Bag allowances".equals(serviceDefinition.getName())) {
						String baggageRefId =
								serviceDefinition.getServiceDefinitionAssociation().getBaggageAllowanceRefID();
						for (BaggageAllowanceType baggageAllowance : reseponseType.getDataLists()
								.getBaggageAllowanceList().getBaggageAllowance()) {
							if (baggageRefId.equals(baggageAllowance.getBaggageAllowanceID())) {
								baggageInfoMap.put(serviceDefinition.getServiceDefinitionID(), baggageAllowance);
							}
						}
					}
				}
			}

			List<TripInfo> tripInfos = new ArrayList<>();
			int journeyNum = 0;
			for (PaxJourneyType paxJourney : paxJourneyList.getPaxJourney()) {
				TripInfo tripInfo = new TripInfo();
				parseSegments(tripInfo, paxJourney, reseponseType, journeyNum);
				tripInfos.add(tripInfo);
				journeyNum++;
			}
			GstInfo gstInfo = null;
			DeliveryInfo deliveryInfo = parseDeliveryInfo(reseponseType);
			pnrBooking = AirImportPnrBooking.builder().tripInfos(tripInfos).gstInfo(gstInfo).deliveryInfo(deliveryInfo)
					.build();
		}
		return pnrBooking;
	}

	private void parseSegments(TripInfo tripInfo, PaxJourneyType paxJourney, ResponseType reseponseType,
			int journeyNum) {
		int segmentNum = 0;
		List<SegmentInfo> segmentInfos = new ArrayList<>();
		for (String paxSegmentRefId : paxJourney.getPaxSegmentRefID()) {
			PaxSegmentType paxSegment = paxSegmentMap.get(paxSegmentRefId);
			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setSegmentNum(segmentNum);

			TransportDepType departure = paxSegment.getDep();
			TransportArrivalType arrival = paxSegment.getArrival();
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(departure.getIATALocationCode()));
			segmentInfo.getDepartAirportInfo().setTerminal(AirUtils.getTerminalInfo(departure.getTerminalName()));
			segmentInfo
					.setDepartTime(TgsDateUtils.getLocalDateTime(departure.getAircraftScheduledDateTime().getValue()));

			segmentInfo.setArrivalAirportInfo(AirportHelper.getAirportInfo(arrival.getIATALocationCode()));
			segmentInfo.getArrivalAirportInfo().setTerminal(AirUtils.getTerminalInfo(arrival.getTerminalName()));
			segmentInfo
					.setArrivalTime(TgsDateUtils.getLocalDateTime(arrival.getAircraftScheduledDateTime().getValue()));

			if (paxSegment.getDuration() != null) {
				segmentInfo.setDuration(
						(paxSegment.getDuration().getHours() * 60) + paxSegment.getDuration().getMinutes());
			} else {
				segmentInfo.setDuration(segmentInfo.calculateDuration());
			}

			DatedMarketingSegmentType carrierFlightType = paxSegment.getMarketingCarrierInfo();
			FlightDesignator designator = FlightDesignator.builder().build();
			if (AirlineHelper.getAirlineInfo(carrierFlightType.getCarrierDesigCode()) == null) {
				carrierFlightType.getCarrierDesigCode();
			}
			designator.setAirlineInfo(AirlineHelper.getAirlineInfo(carrierFlightType.getCarrierDesigCode()));
			designator.setFlightNumber(StringUtils.trim(carrierFlightType.getMarketingCarrierFlightNumberText()));

			DatedOperatingSegmentType operatingCarrier = paxSegment.getOperatingCarrierInfo();
			if (operatingCarrier != null && !StringUtils.equalsIgnoreCase(designator.getAirlineCode(),
					operatingCarrier.getCarrierDesigCode())) {
				segmentInfo
						.setOperatedByAirlineInfo(AirlineHelper.getAirlineInfo(operatingCarrier.getCarrierDesigCode()));
			}

			segmentInfo.setFlightDesignator(designator);
			segmentInfo.setStops(0);
			List<PriceInfo> priceInfoList = new ArrayList<>();
			PriceInfo priceInfo = PriceInfo.builder().supplierBasicInfo(supplierConf.getBasicInfo()).build();
			priceInfoList.add(priceInfo);
			segmentInfo.setPriceInfoList(priceInfoList);
			List<FlightTravellerInfo> travellers =
					parseTravellers(segmentInfo, reseponseType, paxSegment, journeyNum, segmentNum);
			SegmentBookingRelatedInfo bookingRelatedInfo =
					SegmentBookingRelatedInfo.builder().travellerInfo(travellers).build();
			segmentInfo.setBookingRelatedInfo(bookingRelatedInfo);
			segmentInfos.add(segmentInfo);
			segmentNum++;
		}
		tripInfo.setSegmentInfos(segmentInfos);
	}

	private List<FlightTravellerInfo> parseTravellers(SegmentInfo segmentInfo, ResponseType reseponseType,
			PaxSegmentType paxSegment, int journeyNum, int segmentNum) {
		List<FlightTravellerInfo> travellers = new ArrayList<>();
		Map<String, TicketDocument> ticketDocumentMap = new HashMap<>();

		String pnr = reseponseType.getOrder().get(0).getBookingRef().get(0).getBookingID();
		List<PaxType> paxes = reseponseType.getDataLists().getPaxList().getPax();

		if (reseponseType.getTicketDocInfos() != null
				&& CollectionUtils.isNotEmpty(reseponseType.getTicketDocInfos().getTicketDocInfo())) {
			for (TicketDocInfo ticketDocInfo : reseponseType.getTicketDocInfos().getTicketDocInfo()) {
				if (CollectionUtils.isNotEmpty(ticketDocInfo.getTicketDocument())) {
					for (TicketDocument ticketDocument : ticketDocInfo.getTicketDocument()) {
						if ("T".equals(ticketDocument.getType())) {
							ticketDocumentMap.put(ticketDocInfo.getPassengerReference(), ticketDocument);
							break;
						}
					}
				}
			}
		}

		for (PaxType pax : paxes) {
			com.tgs.services.base.enums.PaxType paxType = AmadeusNdcPaxType.valueOf(pax.getPTC()).getPaxType();
			FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
			travellerInfo.setFirstName(pax.getIndividual().getGivenName().get(0));
			travellerInfo.setLastName(pax.getIndividual().getSurname());
			travellerInfo.setTitle(ObjectUtils.firstNonNull(pax.getIndividual().getTitleName(), "MASTER"));
			if (pax.getIndividual().getBirthdate() != null) {
				travellerInfo.setDob(TgsDateUtils.getLocalDateTime(pax.getIndividual().getBirthdate()).toLocalDate());
			}
			travellerInfo.setPaxType(paxType);
			if (CollectionUtils.isNotEmpty(pax.getIdentityDoc())) {
				for (IdentityDocType identityDoc : pax.getIdentityDoc()) {
					if ("PT".equals(identityDoc.getIdentityDocTypeCode())) {
						travellerInfo.setPassportNumber(identityDoc.getIdentityDocID());
						travellerInfo.setPassportNationality(identityDoc.getIssuingCountryCode());
						if (identityDoc.getIssueDate() != null) {
							travellerInfo.setPassportIssueDate(
									TgsDateUtils.getLocalDateTime(identityDoc.getIssueDate()).toLocalDate());
						}
						if (identityDoc.getExpiryDate() != null) {
							travellerInfo.setExpiryDate(
									TgsDateUtils.getLocalDateTime(identityDoc.getExpiryDate()).toLocalDate());
						}
						break;
					}
				}
			}
			if (CollectionUtils.isNotEmpty(pax.getLoyaltyProgramAccount())) {
				for (LoyaltyProgramAccountType lpa : pax.getLoyaltyProgramAccount()) {
					travellerInfo.getFrequentFlierMap().put(lpa.getCarrier().getAirlineDesigCode(),
							lpa.getAccountNumber());
				}
			}
			if (ticketDocumentMap.get(pax.getPaxID()) != null) {
				travellerInfo.setTicketNumber(ticketDocumentMap.get(pax.getPaxID()).getTicketDocNbr());
			}
			travellerInfo.setPnr(pnr);
			travellerInfo.setSupplierBookingId(getSupplierPNR());
			boolean isToSetFares = (journeyNum == 0 && segmentNum == 0);

			travellerInfo.setFareDetail(
					getFareDetail(reseponseType, paxSegment, pax.getPaxID(), reseponseType.getOrder(), isToSetFares));
			travellers.add(travellerInfo);
		}
		return travellers;
	}

	private FareDetail getFareDetail(ResponseType reseponseType, PaxSegmentType paxSegment, String paxID,
			List<OrderType> order, boolean isToSetFares) {
		FareDetail fd = new FareDetail();
		String segmentId = paxSegment.getPaxSegmentID();
		OrderItemType orderItemType = order.get(0).getOrderItem().get(0);
		for (FareDetailType fareDetailtype : orderItemType.getFareDetail()) {
			if (isPaxIdMatches(paxID, fareDetailtype.getPassengerRefs())) {
				FarePriceDetailType priceType = fareDetailtype.getPrice();
				FareComponentType fareComponentType =
						getFareComponentBySegmentId(fareDetailtype.getFareComponent(), segmentId);
				if (fareComponentType != null) {
					fd.setFareBasis(fareComponentType.getFareBasis().getFareBasisCode().getCode().split("/")[0]);
				}
				fd.setFareComponents(getFareComponentMap(priceType, isToSetFares));
			}
		}

		fd.setCabinClass(AmadeusNdcCabinClass.valueOf(paxSegment.getCabinType().getCabinTypeCode()).getCabinClass());
		fd.setClassOfBooking(paxSegment.getMarketingCarrierInfo().getRBDCode());

		if (CollectionUtils.isNotEmpty(reseponseType.getOrder().get(0).getOrderItem().get(0).getService())) {
			for (ServiceType service : reseponseType.getOrder().get(0).getOrderItem().get(0).getService()) {
				String serviceId = segmentId + "_" + paxID;
				if (serviceId.equals(service.getServiceID()) && service.getServiceAssociations() != null
						&& service.getServiceAssociations().getServiceDefinitionRef() != null
						&& baggageInfoMap.get(service.getServiceAssociations().getServiceDefinitionRef()
								.getServiceDefinitionRefID()) != null) {
					BaggageAllowanceType baggageallowanceType = baggageInfoMap.get(
							service.getServiceAssociations().getServiceDefinitionRef().getServiceDefinitionRefID());
					if (CollectionUtils.isNotEmpty(baggageallowanceType.getWeightAllowance())
							&& baggageallowanceType.getWeightAllowance().get(0).getMaximumWeightMeasure() != null) {
						BaggageInfo baggageInfo = new BaggageInfo();
						baggageInfo.setAllowance(getBaggageString(
								baggageallowanceType.getWeightAllowance().get(0).getMaximumWeightMeasure().getValue()
										.toString(),
								baggageallowanceType.getWeightAllowance().get(0).getMaximumWeightMeasure()
										.getUnitCode()));
						fd.setBaggageInfo(baggageInfo);
					}
				}
			}
		}
		// fd.setRefundableType(null);
		return fd;
	}

	private FareComponentType getFareComponentBySegmentId(List<FareComponentType> fareComponentTypes,
			String segmentId) {
		for (FareComponentType fareComponentType : fareComponentTypes) {
			for (SegmentRefs segmentRef : fareComponentType.getSegmentRefs()) {
				if (segmentRef.getValue().equals(segmentId)) {
					return fareComponentType;
				}
			}
		}
		return null;
	}

	private Map<FareComponent, Double> getFareComponentMap(FarePriceDetailType farePriceDetailType,
			boolean isToSetFares) {
		Map<FareComponent, Double> fareComponents = new HashMap<>();

		if (isToSetFares) {
			double totalFare = getAmountBasedOnCurrency(
					farePriceDetailType.getTotalAmount().getDetailCurrencyPrice().getTotal().getValue(),
					getCurrencyCode());
			fareComponents.put(FareComponent.TF, totalFare);

			double baseFare =
					getAmountBasedOnCurrency(farePriceDetailType.getBaseAmount().getValue(), getCurrencyCode());
			fareComponents.put(FareComponent.BF, baseFare);

			if (farePriceDetailType.getTaxes().getBreakdown() != null
					&& CollectionUtils.isNotEmpty(farePriceDetailType.getTaxes().getBreakdown().getTax())) {
				for (Tax tax : farePriceDetailType.getTaxes().getBreakdown().getTax()) {
					if (tax.getAmount() != null && tax.getAmount().getValue() != null) {
						FareComponent fareComponent = Enums.getIfPresent(GDSFareComponentMapper.class, tax.getTaxCode())
								.or(GDSFareComponentMapper.TX).getFareComponent();
						double taxAmount = getAmountBasedOnCurrency(tax.getAmount().getValue(), getCurrencyCode());
						double oldValue = fareComponents.getOrDefault(fareComponent, 0.0);
						fareComponents.put(fareComponent, oldValue + taxAmount);
					}
				}
			} else {
				double taxes = getAmountBasedOnCurrency(farePriceDetailType.getTaxes().getTotal().getValue(),
						getCurrencyCode());
				fareComponents.put(FareComponent.AT, taxes);
			}
		} else {
			fareComponents.put(FareComponent.TF, 0.0d);
			fareComponents.put(FareComponent.BF, 0.0d);
			fareComponents.put(FareComponent.AT, 0.0d);
		}
		return fareComponents;
	}

	private boolean isPaxIdMatches(String paxID, List<PassengerRefs> passengerRefs) {
		for (PassengerRefs passengerRef : passengerRefs) {
			if (Arrays.asList(passengerRef.getValue().split(" ")).contains(paxID)) {
				return true;
			}
		}
		return false;
	}

	private DeliveryInfo parseDeliveryInfo(ResponseType reseponseType) {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		ContactInfoListType contactInfoType = reseponseType.getDataLists().getContactInfoList();
		Set<String> emails = new HashSet<>();
		Set<String> contacts = new HashSet<>();
		if (CollectionUtils.isNotEmpty(contactInfoType.getContactInfo())) {
			for (ContactInfoType contactInfo : contactInfoType.getContactInfo()) {
				for (PhoneType phone : contactInfo.getPhone()) {
					contacts.add(phone.getPhoneNumber().toString());
				}
				for (EmailAddressType email : contactInfo.getEmailAddress()) {
					emails.add(email.getEmailAddressText());
				}
			}
		}
		deliveryInfo.setEmails(new ArrayList<>(emails));
		deliveryInfo.setContacts(new ArrayList<>(contacts));
		return deliveryInfo;
	}

}
