package com.tgs.services.fms.sources.sqiva;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.*;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Getter
@Setter
final class SqivaSearchManger extends SqivaServiceManager {

	// searchManager
	private static final int DURATION = 7;
	private static final int FLIGHT_NO = 0;
	private static final int SRC = 1;
	private static final int DES = 2;
	private static final int AIRCRAFT = 8;
	private static final int TRANSIT = 9;
	private static final int SEAT = 10;
	private static final int ROUTE = 11;
	private static final int LEG_INFO = 12;
	private static final int LEG_INFO_FOR_CONNECTING = 11;
	private static final int LEG_INFO_FOR_CONNECTING_DETAILS = 12;

	private static final int DEPARTUREDATE = 3;
	private static final int DEPARTURETIME = 5;
	private static final int ARRIVALDATE = 4;
	private static final int ARRIVALTIME = 6;

	private static final String ONWARD_SCHEDULE = "schedule";
	private static final String RETURN_SCHEDULE = "ret_schedule";


	public AirSearchResult doSearch() {
		AirSearchResult finalSearchResult = null;
		HttpUtils httpUtils = HttpUtils.builder().build();
		try {
			AirSearchResult searchResult = null;
			Map<String, String> searchParams = buildSearchParams();
			JSONObject searchResultObject = bindingService.getSearchResult(searchParams, httpUtils);
			boolean isAnyError = isAnyError(searchResultObject);

			if (isAnyError) {
				throw new NoSearchResultException(String.join(",", criticalMessageLogger));
			}

			// No Result
			if (searchQuery.isOneWay() && searchResultObject.getJSONArray(ONWARD_SCHEDULE) == null) {
				log.error("No search result found for oneway");
				throw new NoSearchResultException(AirSourceConstants.NO_SEARCH_RESULT);
			} else if (searchQuery.isReturn() && (searchResultObject.getJSONArray(ONWARD_SCHEDULE) == null
					&& searchResultObject.getJSONArray(RETURN_SCHEDULE) == null)) {
				log.error("No search result found for round trip");
				throw new NoSearchResultException(AirSourceConstants.NO_SEARCH_RESULT);
			}
			searchResult = new AirSearchResult();
			org.json.JSONArray arrayOfSchedule = searchResultObject.getJSONArray(ONWARD_SCHEDULE);
			List<TripInfo> onwardTrips = populateSearchResult(arrayOfSchedule);
			log.info("Total trips for onward {} size {}", searchQuery.getSearchId(), onwardTrips.size());
			searchResult.getTripInfos().put(TripInfoType.ONWARD.getName(), onwardTrips);
			if (searchQuery.isReturn()) {
				org.json.JSONArray arrayOfReturnSchedule = searchResultObject.getJSONArray(RETURN_SCHEDULE);
				List<TripInfo> returnTrips = populateSearchResult(arrayOfReturnSchedule);
				setReturnSegment(returnTrips);
				log.info("Total trips for return {} size {}", searchQuery.getSearchId(), returnTrips.size());
				searchResult.getTripInfos().put(TripInfoType.RETURN.getName(), returnTrips);
			}


			if (searchResult != null && searchQuery.isReturn() && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
				if (CollectionUtils.isNotEmpty(searchResult.getTripInfos().get(TripInfoType.ONWARD.name()))
						&& CollectionUtils.isNotEmpty(searchResult.getTripInfos().get(TripInfoType.RETURN.name()))) {
					List<TripInfo> onwards = searchResult.getTripInfos().get(TripInfoType.ONWARD.name());
					List<TripInfo> returns = searchResult.getTripInfos().get(TripInfoType.RETURN.name());
					List<TripInfo> combinedTrips =
							SqivaUtils.buildCombination(onwards, returns, configuration.getSourceId());
					if (CollectionUtils.isNotEmpty(combinedTrips)) {
						sqivaAirline.setCombinedTripsFareType(combinedTrips);
						searchResult.getTripInfos().put(TripInfoType.COMBO.name(), combinedTrips);
					}
				}
			}

			Map<String, List<Future<TripInfo>>> tasks = new LinkedHashMap<>();

			if (MapUtils.isNotEmpty(searchResult.getTripInfos())) {
				searchResult.getTripInfos().forEach((type, trips) -> {
					List<Future<TripInfo>> futureTaskList = new ArrayList<>();
					if (CollectionUtils.isNotEmpty(trips)) {
						trips.forEach(tripInfo -> {
							try {
								Future<TripInfo> task = ExecutorUtils.getFlightSearchThreadPool().submit(() -> {
									SqivaFareSearchManager fareManager = SqivaFareSearchManager.builder()
											.searchQuery(searchQuery).configuration(configuration)
											.sqivaAirline(sqivaAirline).bindingService(bindingService)
											.criticalMessageLogger(criticalMessageLogger).listener(listener).user(user)
											.build();
									fareManager.setTripInfo(tripInfo);
									boolean isSpecialReturn =
											searchQuery.isReturn() && SqivaUtils.isAllPriceOptionsAreSR(tripInfo);
									fareManager.setSpecialReturn(isSpecialReturn);
									return fareManager.doFareQuote();
								});
								futureTaskList.add(task);
							} catch (NoSeatAvailableException | SupplierRemoteException e) {
								log.error("Unable to price fare quote for {} trip {} cause {}",
										searchQuery.getSearchId(), tripInfo.getTripKey(), e.getMessage());
							}
						});
					}
					tasks.put(type, futureTaskList);
				});
			}

			Map<String, List<TripInfo>> fareSearchTrips = new HashMap<>();
			for (Map.Entry<String, List<Future<TripInfo>>> futureTasks : tasks.entrySet()) {
				String tripType = futureTasks.getKey();
				List<TripInfo> tripInfos = fareSearchTrips.getOrDefault(tripType, new ArrayList<>());
				fareSearchTrips.put(tripType, tripInfos);
				for (Future<TripInfo> tripInfoFuture : futureTasks.getValue()) {
					try {
						tripInfos.add(tripInfoFuture.get(20, TimeUnit.SECONDS));
					} catch (InterruptedException | ExecutionException | TimeoutException e) {
						log.info("Thread interuppted for Fare Search {}", searchQuery.getSearchId(), e);
					}
				}
				if (searchQuery.isReturn() && CollectionUtils.isNotEmpty(tripInfos)
						&& StringUtils.equalsIgnoreCase(TripInfoType.COMBO.name(), tripType)) {
					tripInfos.forEach(tripInfo -> {
						List<TripInfo> splitTrips = tripInfo.splitTripInfo(false);
						sortTripInfosOnDepatureTime(splitTrips);
						fareSearchTrips.put(TripInfoType.ONWARD.name(),
								fareSearchTrips.getOrDefault(TripInfoType.ONWARD.name(), new ArrayList<>()));
						fareSearchTrips.put(TripInfoType.RETURN.name(),
								fareSearchTrips.getOrDefault(TripInfoType.RETURN.name(), new ArrayList<>()));
						fareSearchTrips.get(TripInfoType.ONWARD.name()).add(splitTrips.get(0));
						fareSearchTrips.get(TripInfoType.RETURN.name()).add(splitTrips.get(1));
					});
				}
			}

			if (MapUtils.isNotEmpty(fareSearchTrips)) {
				finalSearchResult = new AirSearchResult();
				if (searchQuery.isReturn() && fareSearchTrips.containsKey(TripInfoType.COMBO.name())) {
					fareSearchTrips.remove(TripInfoType.COMBO.name());
				}
				finalSearchResult.setTripInfos(fareSearchTrips);
			}

		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		}
		return finalSearchResult;
	}

	private void sortTripInfosOnDepatureTime(List<TripInfo> tripInfos) {
		Collections.sort(tripInfos, new Comparator<TripInfo>() {
			public int compare(TripInfo t1, TripInfo t2) {
				return t1.getDepartureTime().compareTo(t2.getDepartureTime());
			}
		});
	}


	private void setReturnSegment(List<TripInfo> returnTrips) {
		for (TripInfo tripInfo : returnTrips) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				segmentInfo.setIsReturnSegment(true);
			}
		}
	}


	public Map<String, String> buildSearchParams() {
		Map<String, String> searchParams = new HashMap<String, String>();
		RouteInfo ownwardRouteInfo = searchQuery.getRouteInfos().get(0);
		searchParams.put("org", ownwardRouteInfo.getFromCityAirportCode());
		searchParams.put("des", ownwardRouteInfo.getToCityAirportCode());
		searchParams.put("flight_date", ownwardRouteInfo.getTravelDate().toString().replace("-", ""));
		if (searchQuery.isReturn()) {
			RouteInfo returnRouteInfo = searchQuery.getRouteInfos().get(1);
			searchParams.put("return_flight", "1");
			searchParams.put("ret_flight_date", returnRouteInfo.getTravelDate().toString().replace("-", ""));
		}
		return searchParams;
	}

	public List<TripInfo> populateSearchResult(JSONArray scheduleObject) throws JSONException {
		List<TripInfo> tripInfos = new ArrayList<>();
		for (int scheduleIndex = 0; scheduleIndex < scheduleObject.length(); scheduleIndex++) {
			JSONArray journeys = scheduleObject.getJSONArray(scheduleIndex);
			List<TripInfo> flights = null;

			if (scheduleIndex == 0) {
				// Direct or via flights
				flights = parseDirectOrViaFlights(journeys);
			} else if (scheduleIndex == 1) {
				// connecting flights
				flights = parseConnectingFlights(journeys);
			}

			if (CollectionUtils.isNotEmpty(flights)) {
				tripInfos.addAll(flights);
			}
		}
		return tripInfos;
	}

	public List<TripInfo> parseDirectOrViaFlights(JSONArray journeys) {
		List<TripInfo> tripInfos = new ArrayList<>();
		for (int legIndex = 0; legIndex < journeys.length(); legIndex++) {
			List<SegmentInfo> segments = new ArrayList<>();
			try {
				JSONArray scheduleLegs = journeys.getJSONArray(legIndex);
				if (!scheduleLegs.isEmpty()) {
					segments.addAll(populateDirectSegmentInfo(scheduleLegs));
				}
			} catch (Exception e) {
				log.error("[Sqiva] Unable to create segment info {}", searchQuery.getSearchId(), e);
			} finally {
				if (CollectionUtils.isNotEmpty(segments)) {
					TripInfo tripInfo = new TripInfo();
					setSegmentNum(segments);
					tripInfo.setSegmentInfos(segments);
					tripInfos.add(tripInfo);
				}
			}
		}
		return tripInfos;
	}

	public List<TripInfo> parseConnectingFlights(JSONArray journeys) {
		List<TripInfo> tripInfos = new ArrayList<>();
		for (int legIndex = 0; legIndex < journeys.length(); legIndex++) {
			List<SegmentInfo> segments = new ArrayList<>();
			try {
				JSONArray scheduleLegs = journeys.getJSONArray(legIndex);
				for (int segmentNum = 0; segmentNum < scheduleLegs.length(); segmentNum++) {
					JSONArray firstSec = scheduleLegs.getJSONArray(segmentNum);
					if (!firstSec.isEmpty()) {
						segments.addAll(populateConnectingSegmentInfo(firstSec, segmentNum));
					}
				}
			} catch (Exception e) {
				log.error("[Sqiva] Unable to create segment info {}", searchQuery.getSearchId(), e);
			} finally {
				if (CollectionUtils.isNotEmpty(segments)) {
					TripInfo tripInfo = new TripInfo();
					setSegmentNum(segments);
					tripInfo.setSegmentInfos(segments);
					tripInfo.setConnectionTime();
					tripInfos.add(tripInfo);
				}
			}
		}
		return tripInfos;
	}

	private void setSegmentNum(List<SegmentInfo> segments) {
		int segmentNum = 0;
		for (SegmentInfo segmentInfo : segments) {
			segmentInfo.setSegmentNum(segmentNum++);
		}
	}

	private List<SegmentInfo> populateDirectSegmentInfo(JSONArray segmentObj) throws JSONException {
		List<SegmentInfo> segments = new ArrayList<>();
		// Integer noOfStops = segmentObj.getInt(TRANSIT);
		String[] legsInfo = ((String) segmentObj.get(LEG_INFO)).split("/");
		int legNum = 0;
		for (String leg : legsInfo) {
			SegmentInfo segmentInfo = buildDirectFlight(leg, segmentObj, legNum);
			legNum++;
			segments.add(segmentInfo);
		}
		return segments;
	}

	private SegmentInfo buildDirectFlight(String leg, JSONArray segmentObj, int legNum) {
		SegmentInfo segmentInfo = new SegmentInfo();
		String[] legInfo = leg.split("-");
		String departureCode = legInfo[0];
		String[] arrivalCodeWithDepartureTime = legInfo[1].split(" ");
		String arrivalCode = arrivalCodeWithDepartureTime[0];
		String departureTime = StringUtils.replace(arrivalCodeWithDepartureTime[1], "_", "");
		String arrivalTime = StringUtils.replace(legInfo[2], "_", "");
		String durationStr = legInfo[3];
		FlightDesignator flightDesignator = new FlightDesignator();
		flightDesignator.setEquipType(segmentObj.get(AIRCRAFT).toString());
		String[] codeWithFlightNum = ((String) segmentObj.get(FLIGHT_NO)).split("-");
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(codeWithFlightNum[0]));
		flightDesignator.setFlightNumber(StringUtils.replace(codeWithFlightNum[1], codeWithFlightNum[0], ""));
		if (!segmentObj.isEmpty() && segmentObj != null) {
			String hoursWithMin[] = durationStr.split("h");
			Integer hours = Integer.valueOf(hoursWithMin[0].replace("h", ""));
			Integer minutes = Integer.valueOf(hoursWithMin[1].replace("m", ""));
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(departureCode));
			segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(arrivalCode));
			segmentInfo.setDuration((hours * 60) + minutes);
			segmentInfo.setFlightDesignator(flightDesignator);
			segmentInfo.setDepartTime(SqivaUtils.convertToLocalDateTime("yyyyMMddHHmm", departureTime));
			segmentInfo.setArrivalTime(SqivaUtils.convertToLocalDateTime("yyyyMMddHHmm", arrivalTime));
		}
		segmentInfo.setStops(0);
		List<PriceInfo> priceInfos = new ArrayList<>();
		PriceInfo priceInfo = PriceInfo.builder(searchQuery.getRequestId()).build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		PriceMiscInfo miscInfo = PriceMiscInfo.builder().build();
		miscInfo.setClassWiseSeatCounts(buildSeatMap(segmentObj.optJSONArray(SEAT)));
		miscInfo.setLegNum(legNum);
		priceInfo.setMiscInfo(miscInfo);
		priceInfos.add(priceInfo);
		segmentInfo.setPriceInfoList(priceInfos);
		return segmentInfo;
	}

	private Map<String, Integer> buildSeatMap(JSONArray optJSONArray) {
		Map<String, Integer> seatCounts = new HashMap<>();
		int totalPaxCount = AirUtils.getPaxCount(searchQuery, false);
		if (optJSONArray != null) {
			for (int i = 0; i < optJSONArray.length(); i++) {
				JSONArray classMap = optJSONArray.optJSONArray(i);
				String classOfBooking = classMap.optString(0);
				int seatsAvailable = classMap.optInt(1);
				if (seatsAvailable >= totalPaxCount) {
					seatCounts.put(classOfBooking, seatsAvailable);
				}
			}
		}
		return seatCounts;
	}

	private List<SegmentInfo> populateConnectingSegmentInfo(JSONArray segmentObj, int legIndex) throws JSONException {
		List<SegmentInfo> segments = new ArrayList<>();
		String connectingLegsInfo = segmentObj.optString(LEG_INFO_FOR_CONNECTING_DETAILS, null);
		String[] legDetails = connectingLegsInfo.split("/");
		if (CollectionUtils.size(legDetails) > 1) {
			int legNum = 0;
			for (String legInfo : legDetails) {
				SegmentInfo segmentInfo = buildDirectFlight(legInfo, segmentObj, legNum);
				legNum++;
				segments.add(segmentInfo);
			}
		} else {
			segments.add(buildConnectingSegmentInfo(segmentObj));
		}
		return segments;
	}


	private SegmentInfo buildConnectingSegmentInfo(JSONArray segmentObj) {
		String[] org_des = segmentObj.getString(LEG_INFO_FOR_CONNECTING).split("-");
		String depDate = segmentObj.getString(3);
		String arrDate = segmentObj.getString(4);
		String depTime = segmentObj.getString(5);
		String arrTime = segmentObj.getString(6);
		String hoursWithMin[] = segmentObj.getString(7).split("h");
		SegmentInfo segmentInfo = new SegmentInfo();
		FlightDesignator flightDesignator = new FlightDesignator();
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(org_des[0]));
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(org_des[org_des.length - 1]));
		segmentInfo.setDepartTime(SqivaUtils.convertIntoLocalDateTime(depDate, depTime));
		segmentInfo.setArrivalTime(SqivaUtils.convertIntoLocalDateTime(arrDate, arrTime));
		Integer hours = Integer.valueOf(hoursWithMin[0].replace("h", ""));
		Integer minutes = Integer.valueOf(hoursWithMin[1].replace("m", ""));
		segmentInfo.setDuration((hours * 60) + minutes);
		flightDesignator.setEquipType(segmentObj.get(AIRCRAFT).toString());
		String[] codeWithFlightNum = ((String) segmentObj.get(FLIGHT_NO)).split("-");
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(codeWithFlightNum[0]));
		flightDesignator.setFlightNumber(StringUtils.replace(codeWithFlightNum[1], codeWithFlightNum[0], ""));
		segmentInfo.setFlightDesignator(flightDesignator);
		List<PriceInfo> priceInfos = new ArrayList<>();
		PriceInfo priceInfo = PriceInfo.builder(searchQuery.getRequestId()).build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		PriceMiscInfo miscInfo = PriceMiscInfo.builder().build();
		miscInfo.setClassWiseSeatCounts(buildSeatMap(segmentObj.optJSONArray(SEAT)));
		miscInfo.setLegNum(0);
		priceInfo.setMiscInfo(miscInfo);
		priceInfos.add(priceInfo);
		segmentInfo.setPriceInfoList(priceInfos);
		segmentInfo.setIsReturnSegment(false);
		if (org_des.length > 2) {
			Integer stops = org_des.length - 2;
			List<AirportInfo> stopAirports = new ArrayList<>();
			for (int stopIndex = 1; stopIndex <= org_des.length - 2; stopIndex++) {
				stopAirports.add(AirportHelper.getAirport(org_des[stopIndex]));
			}
			if (CollectionUtils.isNotEmpty(stopAirports)) {
				segmentInfo.setStopOverAirports(stopAirports);
				segmentInfo.setStops(stops);
			}
		}
		return segmentInfo;
	}

}
