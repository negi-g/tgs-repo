package com.tgs.services.fms.sources.mystifly;

import java.rmi.RemoteException;
import com.tgs.services.fms.utils.AirUtils;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirCancelRQ;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirCancelRS;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.CancelBooking;
import onepoint.mystifly.CancelBookingResponse;

@Slf4j
@SuperBuilder
@Getter
@Setter
final class MystiflyCancellationManager extends MystiflyServiceManager {

	protected String supplierPNR;

	public boolean cancelPnr() {
		boolean isSuccess = false;
		try {
			listener.setType(AirUtils.getLogType("CancelBooking", configuration));
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			CancelBooking cancelBooking = new CancelBooking();
			cancelBooking.setRq(getPnrCancelRq());
			CancelBookingResponse cancelBookingResponse = onePointStub.cancelBooking(cancelBooking);
			AirCancelRS pnrCancelRs = cancelBookingResponse.getCancelBookingResult();
			if (!isAnyCriticalException(pnrCancelRs.getErrors())) {
				isSuccess = pnrCancelRs.isSuccessSpecified();
			}
		} catch (RemoteException e) {
			logCriticalMessage(e.getMessage());
			throw new SupplierRemoteException(e.getMessage());
		} finally {
			onePointStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}

	private AirCancelRQ getPnrCancelRq() {
		AirCancelRQ pnrCancelRq = new AirCancelRQ();
		pnrCancelRq.setSessionId(sessionId);
		pnrCancelRq.setTarget(getTarget());
		pnrCancelRq.setUniqueID(supplierPNR);
		return pnrCancelRq;
	}


}
