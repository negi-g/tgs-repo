package com.tgs.services.fms.helper.analytics;

import com.tgs.services.fms.analytics.AnalyticsAirQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AirSearchRestrictionQuery extends AnalyticsAirQuery {

	protected int threadcount;
	protected int maxallowedthread;
	protected int timeoutcount;
	protected int maxtimeoutallowed;


}
