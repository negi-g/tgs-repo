package com.tgs.services.fms.sources.navitaireV4_2;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.UnmaskFareTypeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.navitaire.schemas.webservices.BookingManagerStub;
import com.navitaire.schemas.webservices.SessionManagerStub;
import com.navitaire.schemas.webservices.datacontracts.booking.Booking;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@NoArgsConstructor
public class NavitaireAirBookingFactory extends AbstractAirBookingFactory {

	protected SoapRequestResponseListner listener = null;

	protected SessionManagerStub sessionStub;

	protected BookingManagerStub bookingStub;

	protected NavitaireBindingService bindingService;

	public NavitaireAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	public void initialize() {
		bindingService =
				NavitaireBindingService.builder().cacheCommunicator(cachingComm).bookingUser(bookingUser).build();
		sessionStub = bindingService.getSessionManagerStub(supplierConf, null);
		bookingStub = bindingService.getBookingManagerStub(supplierConf, null);
	}

	@Override
	public boolean doBooking() {
		boolean isBookingSuccess = false;
		List<SegmentInfo> segmentInfos = null;
		NavitaireSessionManager sessionManager = null;
		try {
			initialize();
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			segmentInfos = bookingSegments.getSegmentInfos();
			sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionBinding(sessionStub).bookingId(bookingId).bookingUser(bookingUser).build();
			sessionManager.init();
			if (supplierSession != null
					&& StringUtils.isNotBlank(supplierSession.getSupplierSessionInfo().getSessionToken())) {
				sessionManager.setSessionSignature(supplierSession.getSupplierSessionInfo().getSessionToken());
				NavitaireBookingManager bookingManager = NavitaireBookingManager.builder().configuration(supplierConf)
						.sessionSignature(sessionManager.sessionSignature).deliveryInfo(deliveryInfo)
						.searchQuery(bookingSegments.getSearchQuery()).bookingId(bookingId)
						.sourceConfiguration(sourceConfiguration).listener(listener).bookingBinding(bookingStub)
						.bookingUser(bookingUser).criticalMessageLogger(criticalMessageLogger)
						.segmentInfos(bookingSegments.getSegmentInfos()).isMaskingEnabled(checkMaskingEnabled())
						.build();
				bookingManager.initialize(segmentInfos);
				bookingManager.setSearchQuery(bookingSegments.getSearchQuery());
				bookingManager.setGmsCommunicator(gmsCommunicator);
				bookingManager.setMoneyExchnageComm(moneyExchangeComm);
				bookingManager.initializeTravellerInfo(segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo());
				double amountToBePaidToAirline = 0;
				bookingManager.sendGSTAndContactInfo(gstInfo, order.getBookingUserId());
				NavitaireAirline navitaireAirLine = bookingManager.getNavitaireAirline();
				TripInfo tripInfo = new TripInfo();
				tripInfo.setSegmentInfos(segmentInfos);
				ArrayOfstring preMeal =
						navitaireAirLine.getServiceBundleCodes(sourceConfiguration, tripInfo, SSRType.MEAL);
				if (AirUtils.isSSRAddedInTrip(segmentInfos)
						|| AirUtils.getParticularPaxCount(bookingSegments.getSearchQuery(), PaxType.INFANT) > 0
						|| navitaireAirLine.isAdditionalSSRRequired(bookingSegments.getSegmentInfos())) {
					bookingManager.sendSellSSRRequest(segmentInfos, true, preMeal);
				}
				bookingManager.getBookingState();
				amountToBePaidToAirline = bookingManager.sendSellPassengerinfo();

				// To validate passengers in the credit shell PNR.
				if (StringUtils.isNotBlank(segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getCreditShellPNR())) {
					NavitaireServiceManager csManager = navitaireAirLine.getCreditShellManager(bookingUser);
					csManager.setListener(listener);
					csManager.setConfiguration(supplierConf);
					csManager.setSourceConfiguration(sourceConfiguration);
					csManager.init();
					csManager.validateCreditShellPNR(
							segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getCreditShellPNR(),
							bookingManager.sessionSignature);
				}

				if (NavitaireUtils.isSeatAddedInTrip(segmentInfos)) {
					amountToBePaidToAirline = bookingManager.sendSeatSellRequest(segmentInfos);
				}
				String promoCode = bookingManager.getPromoCode(bookingSegments.getSearchQuery());
				if (StringUtils.isNotBlank(promoCode)) {
					amountToBePaidToAirline = bookingManager.applyPromoCode(promoCode);
				}
				if (!isHoldBooking && amountToBePaidToAirline != 0) {
					amountToBePaidToAirline = bookingManager.doPayment();
				}
				if (!isFareDiff(amountToBePaidToAirline)) {
					pnr = bookingManager.commitBooking(isHoldBooking,
							segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo(), StringUtils.EMPTY, true);
					NavitaireBookingRetrieveManager retrieveManager = NavitaireBookingRetrieveManager.builder()
							.configuration(supplierConf).sessionSignature(sessionManager.sessionSignature)
							.bookingBinding(bookingStub).listener(listener).bookingUser(bookingUser).build();
					retrieveManager.init();
					Booking retrieveBookingRS = retrieveManager.getBookingResponse(pnr);
					if (isHoldBooking) {
						Calendar holdTime = Calendar.getInstance();
						holdTime.setTimeInMillis(
								retrieveBookingRS.getBookingHold().getHoldDateTime().getTimeInMillis());
						updateTimeLimit(holdTime);
						bookingManager.updateSupplierReference(retrieveBookingRS, segmentInfos);
						isBookingSuccess = true;
					} else {
						if (StringUtils
								.isNotBlank(segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getCreditShellPNR())
								&& !isHoldBooking && navitaireAirLine.isSeperateAPIForCSPayment()) {
							double pnrCreditBalance = AirUtils.calculateAvailablePNRCredit(segmentInfos);
							NavitaireServiceManager csManager = navitaireAirLine.getCreditShellManager(bookingUser);
							csManager.setListener(listener);
							csManager.setSourceConfiguration(sourceConfiguration);
							csManager.setConfiguration(supplierConf);
							csManager.addCreditShellPaymentToBooking(pnrCreditBalance, pnr,
									segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getCreditShellPNR());
							retrieveBookingRS = retrieveManager.getBookingResponse(pnr);
						}
						if (bookingManager.isTicketNumberRequired()) {
							BookingUtils.updateAirlinePnr(segmentInfos, pnr);
							bookingManager.updateSupplierReference(retrieveBookingRS, segmentInfos);
							isBookingSuccess = bookingManager.updateTicketNumber(retrieveBookingRS, segmentInfos);
						} else {
							isBookingSuccess = bookingManager.isBookingSuccess(retrieveBookingRS.getBookingInfo());
						}
					}
				}
			}
		} finally {
			sessionManager.closeSessionWithoutSupplierException();
			bindingService =
					NavitaireBindingService.builder().cacheCommunicator(cachingComm).bookingUser(bookingUser).build();
			bindingService.storeSessionMetaInfo(supplierConf, SessionManagerStub.class, sessionStub);
			bindingService.storeSessionMetaInfo(supplierConf, BookingManagerStub.class, bookingStub);
		}
		return isBookingSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isBookingSuccess = false;
		NavitaireSessionManager sessionManager = null;
		try {
			initialize();
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionBinding(sessionStub).bookingId(bookingId).bookingUser(bookingUser).build();
			sessionManager.init();
			sessionManager.openSession();
			if (StringUtils.isNotBlank(sessionManager.getSessionSignature())) {
				List<SegmentInfo> segmentInfos = bookingSegments.getSegmentInfos();
				NavitaireBookingRetrieveManager retrieveManager = NavitaireBookingRetrieveManager.builder()
						.configuration(supplierConf).sessionBinding(sessionManager.sessionBinding)
						.sessionSignature(sessionManager.sessionSignature).bookingBinding(bookingStub)
						.listener(listener).bookingUser(bookingUser).build();
				retrieveManager.init();
				Booking booking = retrieveManager.getBookingResponse(pnr);
				NavitaireBookingManager bookingManager = NavitaireBookingManager.builder().configuration(supplierConf)
						.sessionSignature(sessionManager.sessionSignature).deliveryInfo(deliveryInfo)
						.bookingId(bookingId).bookingBinding(bookingStub).listener(listener).bookingUser(bookingUser)
						.criticalMessageLogger(criticalMessageLogger).sourceConfiguration(sourceConfiguration)
						.bookingUser(bookingUser).isMaskingEnabled(checkMaskingEnabled()).build();
				bookingManager.init();
				bookingManager.initializeTravellerInfo(segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo());
				bookingManager.getBookingState();
				bookingManager.setSearchQuery(bookingSegments.getSearchQuery());
				bookingManager.setSegmentInfos(segmentInfos);
				bookingManager.setMoneyExchnageComm(moneyExchangeComm);
				bookingManager.totalFare = booking.getBookingSum().getTotalCost().doubleValue();
				NavitaireAirline navitaireAirLine = bookingManager.getNavitaireAirline();
				double amountToBePaidToAirline = bookingManager.doPayment();
				if (!isFareDiff(amountToBePaidToAirline)) {
					pnr = bookingManager.commitBooking(false,
							segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo(), pnr, true);
					if (StringUtils.isNotBlank(segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getCreditShellPNR())
							&& navitaireAirLine.isSeperateAPIForCSPayment()) {
						double pnrCreditBalance = AirUtils.calculateAvailablePNRCredit(segmentInfos);
						NavitaireServiceManager csManager = navitaireAirLine.getCreditShellManager(bookingUser);
						csManager.setListener(listener);
						csManager.setConfiguration(supplierConf);
						csManager.setSourceConfiguration(sourceConfiguration);
						csManager.addCreditShellPaymentToBooking(pnrCreditBalance, pnr,
								segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getCreditShellPNR());
					}
					NavitaireBookingRetrieveManager retrieveBooking = NavitaireBookingRetrieveManager.builder()
							.configuration(supplierConf).sessionSignature(sessionManager.sessionSignature)
							.bookingBinding(bookingStub).listener(listener).bookingUser(bookingUser).build();
					retrieveBooking.init();
					Booking retrieveBookingRS = retrieveBooking.getBookingResponse(pnr);
					if (bookingManager.isTicketNumberRequired()) {
						isBookingSuccess = bookingManager.updateTicketNumber(retrieveBookingRS, segmentInfos);
					} else {
						isBookingSuccess = bookingManager.isBookingSuccess(retrieveBookingRS.getBookingInfo());
					}
				}
			}
		} finally {
			sessionManager.closeSessionWithoutSupplierException();
			bindingService =
					NavitaireBindingService.builder().cacheCommunicator(cachingComm).bookingUser(bookingUser).build();
			bindingService.storeSessionMetaInfo(supplierConf, SessionManagerStub.class, sessionStub);
			bindingService.storeSessionMetaInfo(supplierConf, BookingManagerStub.class, bookingStub);
		}
		return isBookingSuccess;
	}

	private boolean checkMaskingEnabled() {
		FlightBasicFact flightBasicFact = FlightBasicFact.createFact().generateFact(supplierConf.getBasicInfo());
		flightBasicFact.setProductClass(
				bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getBookingClass(PaxType.ADULT));
		BaseUtils.createFactOnUser(flightBasicFact, bookingUser);
		UnmaskFareTypeOutput unmaskFareOutputType =
				AirConfiguratorHelper.getAirConfigRule(flightBasicFact, AirConfiguratorRuleType.UNMASK_FARE);

		if (unmaskFareOutputType != null) {
			return BooleanUtils.isFalse(unmaskFareOutputType.getUnmaskFare());
		}
		// isMaskingAllowed true means masking allowed and only masking is only applicable for Indigo
		return Objects.equals(this.supplierConf.getSourceId(), AirSourceType.INDIGO.getSourceId());
	}

}
