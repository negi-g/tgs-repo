package com.tgs.services.fms.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.tgs.services.fms.dbmodel.DbAirportInfo;

public interface AirportInfoRepository extends JpaRepository<DbAirportInfo, Long>, JpaSpecificationExecutor<DbAirportInfo> {

	@Query(value = "SELECT a.* FROM airportinfo a WHERE a.code ilike %:searchQuery% or a.name ilike %:searchQuery%",
			nativeQuery = true)
	public List<DbAirportInfo> fetchAirports(@Param("searchQuery") String searchQuery);

	@Query(value = "SELECT a.* FROM airportinfo a WHERE a.city ilike %:searchQuery%", nativeQuery = true)
	public List<DbAirportInfo> fetchAirportsByCity(@Param("searchQuery") String searchQuery);

	public DbAirportInfo findByCode(String code);

	public DbAirportInfo findByCityContainingIgnoreCase(String city);

}
