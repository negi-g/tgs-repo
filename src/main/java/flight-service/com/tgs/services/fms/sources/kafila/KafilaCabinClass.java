package com.tgs.services.fms.sources.kafila;

import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import lombok.Getter;

@Getter
public enum KafilaCabinClass {

	DEFAULT(""), ECONOMY("EC"), BUSINESS("BU");

	private String value;

	private KafilaCabinClass(String value) {
		this.value = value;
	}

	public static String getCabinClass(AirSearchQuery searchQuery) {
		if (searchQuery.getCabinClass() == null) {
			return DEFAULT.getValue();
		} else if (CabinClass.ECONOMY.equals(searchQuery.getCabinClass())) {
			return ECONOMY.getValue();
		} else if (CabinClass.BUSINESS.equals(searchQuery.getCabinClass())) {
			return BUSINESS.getValue();
		}
		return ECONOMY.getValue();
	}
}
