package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.AirRules;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirFareTypeOutput;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ShowPublicBookPrivateDynamicEngine {

	private static String FT_RULE_KEY = "FTR_";

	public void isUserShowPublicBookPrivate(TripInfo tripInfo, User user) {

		List<PriceInfo> tripPrices = tripInfo.getTripPriceInfos();
		String userId = UserUtils.getParentUserId(user);

		try {
			// Iterating over all the cabin class
			for (CabinClass cabinClass : CabinClass.values()) {

				List<PriceInfo> specialFares = tripPrices.stream().filter(priceInfo -> {
					return isSystemDefinedFare(priceInfo)
							&& !priceInfo.getFareDetail(PaxType.ADULT).getFareComponents().containsKey(FareComponent.XT)
							&& priceInfo.getCabinClass(PaxType.ADULT).equals(cabinClass);
				}).collect(Collectors.toList());

				// If there is no special Fare with defined cabin class then return
				if (CollectionUtils.isEmpty(specialFares)) {
					continue;
				}

				log.debug("DSPBP:Special fares before filter is {} for flight key {}", specialFares.size(),
						tripInfo.getFlightNumberSet());

				// There can be more than one ruleId present in sameTripInfo
				specialFares = findLowestSpecialPrice(specialFares);

				log.debug("DSPBP:Special fares after filter is {} for flight key {}",
						CollectionUtils.isEmpty(specialFares) ? 0 : specialFares.size(), tripInfo.getFlightNumberSet());

				for (PriceInfo specialFare : specialFares) {
					AirFareTypeOutput output =
							getAirFareTypeOutputFromContextData(specialFare.getMiscInfo().getFareTypeRuleId());
					List<String> comparableFareTypes = output.getComparativeFareTypes();

					List<PriceInfo> applicablePriceInfos =
							findPublicFares(cabinClass, comparableFareTypes, tripInfo.getTripPriceInfos(), specialFare);

					log.debug("DSPBP:Number of Public fares matched is {} for fare type {} for flight key {}",
							CollectionUtils.isEmpty(applicablePriceInfos) ? 0 : applicablePriceInfos.size(),
							specialFare.getOriginalFareType(), tripInfo.getFlightNumberSet());

					if (CollectionUtils.isNotEmpty(applicablePriceInfos)) {
						// segment wise special fare will be removed
						List<PriceInfo> segmentsSpecialFare = getAndFilterSpecialFareFromTrips(tripInfo, specialFare);

						for (PriceInfo applicablePriceInfo : applicablePriceInfos) {
							List<PriceInfo> copyOfSegmentsSpecialFare = AirUtils.deepCopyPriceList(segmentsSpecialFare);
							copySpecialFareToSegments(tripInfo, copyOfSegmentsSpecialFare);
							Double diffOnPublicfare = Math
									.abs(applicablePriceInfo.getTotalAirlineFare() - specialFare.getTotalAirlineFare());
							log.debug("DSPBP:Difference on Public fare is {} for fare type {} for flight key {}",
									diffOnPublicfare, specialFare.getOriginalFareType(), tripInfo.getFlightNumberSet());
							updateXTFareOnPrivateSegment(tripInfo, diffOnPublicfare, user, specialFare,
									applicablePriceInfo.getFareIdentifier());
							log.debug(
									"DSPBP:Filtering Cheapest Fare for user {} cause SPBP logic applied from trip {} flightkey {}",
									userId, specialFare.getOriginalFareType(), tripInfo.getFlightNumberSet());

							removeRestrictedFare(applicablePriceInfo, tripInfo);
						}
					} else {
						log.debug("DSPBP:No Comparative fares available for fare type {} flightkey {}",
								specialFare.getOriginalFareType(), tripInfo.getFlightNumberSet());
					}
				}
			}
		} finally {
			filterUnAllowedSpecialFare(tripInfo);
			tripInfo.setTripPriceInfos(null);
			tripInfo.setTripPriceInfos(AirUtils.getTripTotalPriceInfoList(tripInfo, user));
		}
	}

	private void filterUnAllowedSpecialFare(TripInfo tripInfo) {

		List<PriceInfo> unAllowedSpecialFares = tripInfo.getSegmentInfos().get(0).getPriceInfoList().stream()
				.filter(priceInfo -> isSystemDefinedFare(priceInfo)
						&& !priceInfo.getFareDetail(PaxType.ADULT).getFareComponents().containsKey(FareComponent.XT))
				.collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(unAllowedSpecialFares)) {
			for (PriceInfo unallowedFare : unAllowedSpecialFares) {
				log.debug("DSPBP:Filtering fare for fare type {} flightkey {}", unallowedFare.getOriginalFareType(),
						tripInfo.getFlightNumberSet());
				int index = getPriceIndex(tripInfo, unallowedFare);
				tripInfo.getSegmentInfos().forEach(segment -> {
					segment.getPriceInfoList().remove(index);
				});
			}
		}
	}

	private List<PriceInfo> getAndFilterSpecialFareFromTrips(TripInfo tripInfo, PriceInfo specialFare) {
		int matchedIndex = getPriceIndex(tripInfo, specialFare);
		List<PriceInfo> segmentsSpecialFare = new ArrayList<>();

		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			if (matchedIndex < segmentInfo.getPriceInfoList().size()) {
				segmentsSpecialFare.add(segmentInfo.getPriceInfoList().get(matchedIndex));
				segmentInfo.getPriceInfoList().remove(matchedIndex);
			}
			if (matchedIndex < tripInfo.getTripPriceInfos().size()) {
				tripInfo.getTripPriceInfos().remove(matchedIndex);
			}
		});
		return segmentsSpecialFare;
	}

	public List<PriceInfo> findLowestSpecialPrice(List<PriceInfo> specialFares) {
		Map<Long, PriceInfo> ruleWisePriceInfo = new HashMap<Long, PriceInfo>();
		for (PriceInfo priceInfo : specialFares) {
			long ruleId = priceInfo.getMiscInfo().getFareTypeRuleId();
			if (ruleWisePriceInfo.get(ruleId) == null
					|| priceInfo.getTotalAirlineFare() < ruleWisePriceInfo.get(ruleId).getTotalAirlineFare()) {
				ruleWisePriceInfo.put(ruleId, priceInfo);
			}
		}
		specialFares = new ArrayList<>(ruleWisePriceInfo.values());
		specialFares.sort(Comparator.comparingDouble(PriceInfo::getTotalAirlineFare));
		return specialFares;
	}

	private List<PriceInfo> findPublicFares(CabinClass cabinClass, List<String> comparableFareTypes,
			List<PriceInfo> priceInfos, PriceInfo specialFarePriceInfo) {
		return priceInfos.stream()
				.filter(priceInfo -> priceInfo.getCabinClass(PaxType.ADULT).equals(cabinClass)
						&& comparableFareTypes.contains(priceInfo.getFareIdentifier())
						&& priceInfo.getTotalAirlineFare() >= specialFarePriceInfo.getTotalAirlineFare()
						&& !priceInfo.getFareDetail(PaxType.ADULT).getFareComponents().containsKey(FareComponent.XT))
				.collect(Collectors.toList());
	}

	public boolean isSystemDefinedFare(PriceInfo priceInfo) {
		return StringUtils.equalsIgnoreCase(FareType.SPECIAL_PRIVATE_FARE.getName(), priceInfo.getSystemDefinedType());
	}

	public boolean isXTExists(FareDetail fareDetail) {
		return fareDetail.getFareComponents().containsKey(FareComponent.XT);
	}

	public void updateXTFareOnPrivateSegment(TripInfo tripInfo, Double diffOnPublicFare, User user,
			PriceInfo specialFarePriceInfo, String applicableFareType) {
		Integer paxCount = specialFarePriceInfo.getTotalPaxCountOnPaxTypeWise(true);
		double amountOnSegment = Math.abs(diffOnPublicFare);
		double xtFare = amountOnSegment / paxCount;
		SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(0);
		AtomicInteger priceIndex = new AtomicInteger(0);
		segmentInfo.getPriceInfoList().forEach(priceInfo -> {
			if (AirUtils.isSamePriceInfo(specialFarePriceInfo, priceInfo)) {
				for (PaxType paxType : priceInfo.getFareDetails().keySet()) {
					FareDetail fareDetail = priceInfo.getFareDetail(paxType);
					if (!isXTExists(fareDetail)) {
						tripInfo.getSegmentInfos().forEach(segment -> {
							segment.getPriceInfo(priceIndex.get()).setFareIdentifier(applicableFareType);
						});
						priceInfo.setFareIdentifier(applicableFareType);
						BaseUtils.updateFareComponent(fareDetail.getFareComponents(), FareComponent.XT, xtFare, user);
					}
				}
			}
			priceIndex.getAndIncrement();
		});
	}

	public void copySpecialFareToSegments(TripInfo tripInfo, List<PriceInfo> segmentsPriceList) {
		for (int segmentIndex = 0; segmentIndex < segmentsPriceList.size(); segmentIndex++) {
			PriceInfo priceInfo = new GsonMapper<>(segmentsPriceList.get(segmentIndex), PriceInfo.class).convert();
			tripInfo.getSegmentInfos().get(segmentIndex).getPriceInfoList().add(priceInfo);
		}
	}

	private void removeRestrictedFare(PriceInfo priceInfo, TripInfo tripInfo) {
		int indexToBeRemoved = getPriceIndex(tripInfo, priceInfo);
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			if (indexToBeRemoved < segmentInfo.getPriceInfoList().size())
				segmentInfo.getPriceInfoList().remove(indexToBeRemoved);
		});
		if (indexToBeRemoved < tripInfo.getTripPriceInfos().size()) {
			tripInfo.getTripPriceInfos().remove(indexToBeRemoved);
		}
	}

	private int getPriceIndex(TripInfo tripInfo, PriceInfo existPriceInfo) {
		int priceIndex = 0;
		List<PriceInfo> priceList = tripInfo.getSegmentInfos().get(0).getPriceInfoList();
		for (priceIndex = 0; priceIndex < priceList.size(); priceIndex++) {
			if (AirUtils.isSamePriceInfo(priceList.get(priceIndex), existPriceInfo)) {
				break;
			}
		}
		return priceIndex;
	}

	/**
	 * @param oldTripInfo
	 * @param newTripInfo
	 * @implSpec : This Method Blindly Consider that oldTrip has all details
	 * @implNote : This Will add XT Fare Component to new TripInfo (which is copied from oldTrip) before applying will
	 *           check new trip is private fare
	 */
	public void addXtraFareOnTrip(TripInfo oldTripInfo, TripInfo newTripInfo, User user, String bookingId) {

		List<TripInfo> oldTripInfos = oldTripInfo.splitTripInfo(false);
		List<TripInfo> newTripInfos = newTripInfo.splitTripInfo(false);

		int tripIndex = 0;
		for (TripInfo oldTrip : oldTripInfos) {
			TripInfo newTrip = newTripInfos.get(tripIndex);
			if (newTrip != null && CollectionUtils.isNotEmpty(newTrip.getSegmentInfos())
					&& CollectionUtils.isNotEmpty(newTrip.getSegmentInfos().get(0).getPriceInfoList())) {
				addXtraFare(oldTrip, newTrip, user);
				updateFareIdentifier(newTrip.getSegmentInfos(), oldTrip.getSegmentInfos());
				log.debug("Added Xtra Fare to trip oldtrip {},newtrip {} for user {} booking {}", oldTripInfo,
						newTripInfo, UserUtils.getParentUserId(user), bookingId);
			}
			tripIndex++;
		}
	}

	private void updateFareIdentifier(List<SegmentInfo> newSegments, List<SegmentInfo> oldSegments) {
		for (int index = 0; index < newSegments.size(); index++) {
			newSegments.get(index).getPriceInfo(0)
					.setFareIdentifier(oldSegments.get(index).getPriceInfo(0).getFareIdentifier());
			newSegments.get(index).getPriceInfo(0).getMiscInfo()
					.setFareTypeRuleId(oldSegments.get(index).getPriceInfo(0).getMiscInfo().getFareTypeRuleId());
			if (Objects.nonNull(oldSegments.get(index).getPriceInfo(0).getMiscInfo().getRuleIdMap().get(AirRules.FT)))
				newSegments.get(index).getPriceInfo(0).getMiscInfo().getRuleIdMap().put(AirRules.FT,
						(oldSegments.get(index).getPriceInfo(0).getMiscInfo().getRuleIdMap().get(AirRules.FT)));
		}
	}

	/**
	 * @param oldTrip
	 * @param newTrip
	 * @param user
	 * @implNote : This Method will copy oldPriceinfo XT fare component to newPriceInfo
	 */
	public void addXtraFare(TripInfo oldTrip, TripInfo newTrip, User user) {
		SegmentInfo oldSegmentInfo = oldTrip.getSegmentInfos().get(0);
		SegmentInfo newSegmentInfo = newTrip.getSegmentInfos().get(0);
		FareDetail oldFareInfo = oldSegmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT);
		PriceInfo newPriceInfo = newSegmentInfo.getPriceInfo(0);
		PriceInfo oldPriceInfo = oldSegmentInfo.getPriceInfo(0);
		if (oldFareInfo.getFareComponents().containsKey(FareComponent.XT)) {
			newPriceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
				if (!isXTExists(fareDetail)) {
					FareDetail oldFareDetail = oldPriceInfo.getFareDetail(paxType);
					double xtraFare = oldFareDetail.getFareComponents().getOrDefault(FareComponent.XT, 0.0);
					BaseUtils.updateFareComponent(fareDetail.getFareComponents(), FareComponent.XT, xtraFare, user);
					AirUtils.copyStaticInfo(fareDetail, oldFareDetail);
				}
			}));
			updateFareIdentifier(newTrip.getSegmentInfos(), oldTrip.getSegmentInfos());
		}
	}

	public static AirFareTypeOutput getAirFareTypeOutputFromContextData(Long fareTypeRuleId) {
		String fareTypeRuleIdKey = FT_RULE_KEY + fareTypeRuleId;
		ContextData contextData = SystemContextHolder.getContextData();
		AirFareTypeOutput output = (AirFareTypeOutput) MapUtils.getObject(contextData.getValueMap(), fareTypeRuleIdKey);
		if (output == null) {
			AirConfiguratorInfo configuratorInfo =
					AirConfiguratorHelper.getAirConfigRuleById(AirConfiguratorRuleType.FARE_TYPE, fareTypeRuleId);
			output = (AirFareTypeOutput) configuratorInfo.getIRuleOutPut();
			contextData.setValue(fareTypeRuleIdKey, output);
		}
		return output;
	}

}
