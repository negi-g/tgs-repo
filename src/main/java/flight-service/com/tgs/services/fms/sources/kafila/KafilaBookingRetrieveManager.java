package com.tgs.services.fms.sources.kafila;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.kafila.pnrRetrieveRequest.Details;
import com.tgs.services.kafila.pnrRetrieveRequest.PnrRetrieveRequest;
import com.tgs.services.kafila.pnrRetrieveResponse.Fare;
import com.tgs.services.kafila.pnrRetrieveResponse.FlightDetails;
import com.tgs.services.kafila.pnrRetrieveResponse.OtherPnrDetails;
import com.tgs.services.kafila.pnrRetrieveResponse.Pax;
import com.tgs.services.kafila.pnrRetrieveResponse.PnrRetrieveResponse;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
public class KafilaBookingRetrieveManager extends KafilaServiceManager {

	List<Pax> passengers;

	String bookingClass;

	public AirImportPnrBooking getPnrDetails(PnrRetrieveResponse responseBody) {
		AirImportPnrBooking pnrBooking = null;

		if (responseBody != null && responseBody.getPARAM() != null) {
			pnrBooking = AirImportPnrBooking.builder().deliveryInfo(getDeliveryInfo(responseBody.getOthers().get(0)))
					.tripInfos(getTripInfos(responseBody)).pnr(getPnr(responseBody))
					.supplierId(supplierConfiguration.getSupplierId()).build();
		}
		return pnrBooking;
	}

	public PnrRetrieveResponse getBookingDetailsResponse() {

		PnrRetrieveRequest pnrRequest = buildPnrRetrieveRequest(pnr);
		PnrRetrieveResponse pnrResponse = bindingService.getPnr(pnrRequest);
		return pnrResponse;
	}

	public PnrRetrieveRequest buildPnrRetrieveRequest(String bookingID) {
		PnrRetrieveRequest request = new PnrRetrieveRequest();
		Details details = new Details();
		List<Details> detailList = new ArrayList<Details>();
		details.setBOOKINGID(bookingID);
		details.setCLIENT_SESSIONID(getUUID());
		details.setHS(String.valueOf(getHitSrc()));
		detailList.add(details);
		request.setSTR(detailList);
		return request;
	}

	private DeliveryInfo getDeliveryInfo(OtherPnrDetails details) {

		ArrayList<String> email = new ArrayList<>();
		ArrayList<String> contacts = new ArrayList<>();

		email.add(details.getCustEmail());
		if (!details.getAltEmail().equals("NA")) {
			email.add(details.getAltEmail());
		}

		contacts.add(details.getCustMobile());
		if (!details.getAltMobile().equals("NA")) {
			email.add(details.getAltMobile());
		}

		DeliveryInfo deliveryInfo = new DeliveryInfo();
		deliveryInfo.setContacts(contacts);
		deliveryInfo.setEmails(email);
		return deliveryInfo;
	}

	private String getPnr(PnrRetrieveResponse response) {
		if (response.getPAX() != null) {
			return response.getPAX().get(0).getApnr();
		} else {
			return response.getPAXOW().get(0).getApnr();
		}
	}

	private List<TripInfo> getTripInfos(PnrRetrieveResponse responseBody) {
		ArrayList<TripInfo> trips = null;
		if (Objects.nonNull(responseBody.getPAX())) {
			passengers = responseBody.getPAX();
		} else {
			passengers = responseBody.getPAXOW();
		}


		trips = new ArrayList<TripInfo>();
		if (Objects.nonNull(responseBody.getPAXOW())) {
			trips.add(createTripInfo(responseBody, "FlightOw"));
			trips.add(createTripInfo(responseBody, "FlightRt"));
		} else {
			trips.add(createTripInfo(responseBody, "Flight"));
		}
		return trips;
	}

	private TripInfo createTripInfo(PnrRetrieveResponse responseBody, String key) {
		FlightDetails trip = null;
		List<FlightDetails> con_flight = null;
		Fare fare = null;

		if (StringUtils.equalsIgnoreCase("FLIGHTOW", key)) {
			trip = responseBody.getFLIGHTOW().get(0);
			con_flight = responseBody.getCON_FLIGHTOW();
			fare = responseBody.getFAREOW().get(0);
		} else if (StringUtils.equalsIgnoreCase("FLIGHTRT", key)) {
			trip = responseBody.getFLIGHTRT().get(0);
			con_flight = responseBody.getCON_FLIGHTRT();
			fare = responseBody.getFARERT().get(0);
		} else if (StringUtils.equalsIgnoreCase("FLIGHT", key)) {
			trip = responseBody.getFLIGHT().get(0);
			con_flight = responseBody.getCON_FLIGHT();
			fare = responseBody.getFARE().get(0);
		}
		TripInfo tripInfo = new TripInfo();
		try {
			List<SegmentInfo> segmentInfos = new ArrayList<>();
			Map<String, String> additionalInfo = buildFareProperties(responseBody, key);
			SegmentInfo segmentInfo;
			if (con_flight == null) {
				segmentInfo = populateSegmentInfo(trip);
				segmentInfo.getDepartAirportInfo().setTerminal(additionalInfo.getOrDefault(DEPART_TERMINAL, null));
				segmentInfo.getArrivalAirportInfo().setTerminal(additionalInfo.getOrDefault(ARRIVAL_TERMINAL, null));
				segmentInfo = populatePriceInfo(fare, segmentInfo, additionalInfo, 0);
				segmentInfo.setBookingRelatedInfo(getBookingRelatedInfo(segmentInfo, responseBody));
				if (StringUtils.equalsIgnoreCase("FLIGHTRT", key)) {
					segmentInfo.setIsReturnSegment(true);
				}
				segmentInfos.add(segmentInfo);
			} else

			{
				int segmentNum = 0;
				for (FlightDetails flightDetailss : con_flight) {
					segmentInfo = populateSegmentInfo(flightDetailss);
					if (segmentNum == 0) {
						segmentInfo = populatePriceInfo(fare, segmentInfo, null, segmentNum);
					}
					segmentInfo.setSegmentNum(segmentNum++);
					if (StringUtils.equalsIgnoreCase("FLIGHTRT", key)) {
						segmentInfo.setIsReturnSegment(true);
					}

					segmentInfos.add(segmentInfo);
				}
			}
			tripInfo.getSegmentInfos().addAll(segmentInfos);
		} catch (Exception e) {
			log.error("Unable to parse trip {}", searchQuery.getSearchId(), e);
		}
		return tripInfo;
	}


	private SegmentBookingRelatedInfo getBookingRelatedInfo(SegmentInfo segmentInfo, PnrRetrieveResponse response) {
		List<FlightTravellerInfo> travellerInfoList = new ArrayList<FlightTravellerInfo>();
		int passengerCount = 0;
		for (Pax pax : passengers) {
			FlightTravellerInfo traveller = new FlightTravellerInfo();

			traveller.setFirstName(pax.getFn());
			traveller.setLastName(pax.getLn());
			traveller.setPaxType(KafilaUtils.setPaxType(pax.getType()));
			traveller.setTitle(KafilaPaxTitle.valueOf(pax.getTtl()).getCode());

			if (Objects.nonNull(pax.getDob())) {
				traveller.setDob(KafilaUtils.stringToDob(pax.getDob()));
			}
			// traveller.setFareDetail(getFareDetails(segment.getFares().get(0), passengerDetail.getPassengerTypeCode(),
			// null, legNum, segmentInfo.getSegmentNum()));

			travellerInfoList.add(traveller);
			// if (passenger.hasInfant) {
			// travellerInfoList.add(addInfantTraveller(segKey, passengerDetail.getInfant(), ++passengerCount, segment,
			// legNum, segmentInfo.getSegmentNum()));
			// }
			passengerCount++;

		}

		SegmentBookingRelatedInfo bookingRelatedInfo =
				SegmentBookingRelatedInfo.builder().travellerInfo(travellerInfoList).build();
		return bookingRelatedInfo;
	}


	public SegmentInfo populateSegmentInfo(FlightDetails flightDetails) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(flightDetails.getDES_CODE()));
		segmentInfo.setArrivalTime(
				KafilaUtils.convertStringToDate(flightDetails.getARRV_DATE(), flightDetails.getARRV_TIME()));
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(flightDetails.getORG_CODE()));
		segmentInfo.setDepartTime(
				KafilaUtils.convertStringToDate(flightDetails.getDEP_DATE(), flightDetails.getDEP_TIME()));
		segmentInfo.setFlightDesignator(
				createFlightDesignator(flightDetails.getFLIGHT_CODE(), flightDetails.getFLIGHT_NO().toString()));
		segmentInfo.setStops(Integer.valueOf(flightDetails.getSTOP()));
		if (StringUtils.isNotBlank(flightDetails.getLAYOVER_INFO())) {
			segmentInfo.setConnectingTime(KafilaUtils.toMinutes(flightDetails.getLAYOVER_INFO().split(" ")[3]));
		}

		if (StringUtils.isNotBlank(flightDetails.getDURATION())) {
			segmentInfo.setDuration(KafilaUtils.toMinutes(flightDetails.getDURATION()));
		} else {
			segmentInfo.calculateDuration();
		}

		return segmentInfo;

	}

	private FlightDesignator createFlightDesignator(String flightCode, String flightNo) {
		FlightDesignator flightDesign = new FlightDesignator();
		flightDesign.setAirlineInfo(AirlineHelper.getAirlineInfo(flightCode));
		flightDesign.setFlightNumber(flightNo.trim());
		return flightDesign;
	}

	private Map<String, String> buildFareProperties(PnrRetrieveResponse response, String key) {

		Map<String, String> properties = new HashMap<>();
		String baggageText = null;
		FlightDetails flight = null;
		if (StringUtils.equalsIgnoreCase("FLIGHTOW", key)) {
			properties.put(REFUNDABLE, response.getFAREOW().get(0).getREFUNDABLE().toUpperCase());
			flight = response.getFLIGHTOW().get(0);
		} else if (StringUtils.equalsIgnoreCase("FLIGHTRT", key)) {
			properties.put(REFUNDABLE, response.getFARERT().get(0).getREFUNDABLE().toUpperCase());
			flight = response.getFLIGHTRT().get(0);
		} else if (StringUtils.equalsIgnoreCase("FLIGHT", key)) {
			properties.put(REFUNDABLE, response.getFARE().get(0).getREFUNDABLE().toUpperCase());
			flight = response.getFLIGHT().get(0);
		}

		properties.put(DEPART_TERMINAL, flight.getORG_TRML().toString());
		properties.put(ARRIVAL_TERMINAL, flight.getDES_TRML().toString());
		properties.put(SEAT, flight.getSEAT().toString());

		baggageText = flight.getSERVICE();
		if (StringUtils.isNotBlank(baggageText)) {
			baggageText = StringUtils.replaceOnceIgnoreCase(baggageText, "Baggage:", "");
			if (StringUtils.containsIgnoreCase(baggageText, "Cabin")) {
				String cabinBaggage = StringUtils.split(baggageText, ",")[0];
				if (StringUtils.isNotBlank(cabinBaggage)) {
					properties.put(CABIN_BAGGAGE, StringUtils.replaceOnceIgnoreCase(cabinBaggage, "Cabin=", ""));
				}
			}
		}

		properties.put(FAREBASIS, flight.getFARE_BASIS());
		String[] trip_string = response.getPARAM().get(0).getTrip_String().split("*");
		bookingClass = trip_string[-5];
		properties.put(CLASSOFBOOK, bookingClass);

		return properties;

	}

	public SegmentInfo populatePriceInfo(Fare fare, SegmentInfo segmentInfo, Map<String, String> additionalInfo,
			int segmentNo) {

		List<PriceInfo> priceinfos = new ArrayList<PriceInfo>();
		PriceInfo priceinfo = PriceInfo.builder().build();
		Map<PaxType, FareDetail> priceInfoMap = new HashMap<>();

		Map<FareComponent, Double> adultFD = new HashMap<>();
		FareDetail adultFareDetail = new FareDetail();

		adultFD.put(FareComponent.BF, fare.getBASIC_ADT());
		adultFD.put(FareComponent.YQ, fare.getYQ_ADT());
		adultFD.put(FareComponent.AT, Double.valueOf(fare.getTAX_ADT()));
		adultFD.put(FareComponent.TF, fare.getTOTAL_ADT());

		adultFareDetail.setFareComponents(adultFD);
		setCabinClass(adultFareDetail, bookingClass);
		setFareProperties(additionalInfo, adultFareDetail, PaxType.ADULT);
		priceInfoMap.put(PaxType.ADULT, adultFareDetail);

		FareDetail childFareDetail = new FareDetail();
		if (fare.getBASIC_CHD() != null) {
			Map<FareComponent, Double> childFD = new HashMap<>();
			childFD.put(FareComponent.BF, fare.getBASIC_CHD());
			childFD.put(FareComponent.YQ, fare.getYQ_CHD());
			childFD.put(FareComponent.AT, Double.valueOf(fare.getTAX_CHD()));
			childFD.put(FareComponent.TF, fare.getTOTAL_CHD());

			setCabinClass(childFareDetail, bookingClass);
			childFareDetail.setFareComponents(childFD);
			setFareProperties(additionalInfo, childFareDetail, PaxType.CHILD);
			priceInfoMap.put(PaxType.CHILD, childFareDetail);
		}


		FareDetail infantFareDetail = new FareDetail();
		if (fare.getBASIC_INF() != null) {
			Map<FareComponent, Double> infantFD = new HashMap<>();
			infantFD.put(FareComponent.BF, fare.getBASIC_INF());
			infantFD.put(FareComponent.TF, fare.getTOTAL_INF());
			setCabinClass(infantFareDetail, bookingClass);
			infantFareDetail.setFareComponents(infantFD);
			setFareProperties(additionalInfo, infantFareDetail, PaxType.INFANT);
			priceInfoMap.put(PaxType.INFANT, infantFareDetail);
		}

		priceinfo.setFareDetails(priceInfoMap);
		priceinfo.setSupplierBasicInfo(supplierConfiguration.getBasicInfo());
		priceinfos.add(priceinfo);
		segmentInfo.setPriceInfoList(priceinfos);
		return segmentInfo;
	}

	private void setFareProperties(Map<String, String> additionalInfo, FareDetail fareDetail, PaxType paxType) {
		Integer seatCount = Integer.valueOf(additionalInfo.get(SEAT));
		if (seatCount != null && !PaxType.INFANT.equals(paxType)) {
			fareDetail.setSeatRemaining(seatCount);
		}

		String fareBasis = additionalInfo.getOrDefault(FAREBASIS, null);
		if (StringUtils.isBlank(fareDetail.getFareBasis()) && StringUtils.isNotBlank(fareBasis)) {
			fareDetail.setFareBasis(fareBasis);
		}

		String classofBook = additionalInfo.getOrDefault(CLASSOFBOOK, null);
		if (StringUtils.isBlank(fareDetail.getClassOfBooking()) && StringUtils.isNotBlank(classofBook)) {
			fareDetail.setClassOfBooking(classofBook);
		}

		String cabinBaggage = additionalInfo.getOrDefault(CABIN_BAGGAGE, null);
		String checkInBaggage = additionalInfo.getOrDefault(CHECKIN_BAGGAGE, null);
		BaggageInfo baggageInfo = fareDetail.getBaggageInfo();
		if (StringUtils.isBlank(baggageInfo.getCabinBaggage()) && StringUtils.isNotBlank(cabinBaggage)) {
			baggageInfo.setCabinBaggage(cabinBaggage);
		}
		if (StringUtils.isBlank(baggageInfo.getAllowance()) && StringUtils.isNotBlank(checkInBaggage)) {
			baggageInfo.setAllowance(checkInBaggage);
		}

		String refundable = additionalInfo.getOrDefault(REFUNDABLE, null);
		if (StringUtils.equalsIgnoreCase(refundable, "REFUNDABLE")) {
			fareDetail.setRefundableType(RefundableType.REFUNDABLE.getRefundableType());
		} else {
			fareDetail.setRefundableType(RefundableType.NON_REFUNDABLE.getRefundableType());
		}
	}

	private void setCabinClass(FareDetail fareDetail, String f_class) {
		if (StringUtils.equalsIgnoreCase(f_class, "Economy")) {
			fareDetail.setCabinClass(CabinClass.ECONOMY);
		} else if (StringUtils.equalsIgnoreCase(f_class, "Business")) {
			fareDetail.setCabinClass(CabinClass.BUSINESS);
		} else if (StringUtils.equalsIgnoreCase(f_class, "PremiumEconomy")) {
			fareDetail.setCabinClass(CabinClass.PREMIUM_ECONOMY);
		} else {
			fareDetail.setCabinClass(CabinClass.ECONOMY);
		}
	}
}
