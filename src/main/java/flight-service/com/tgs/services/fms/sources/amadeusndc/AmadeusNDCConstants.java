package com.tgs.services.fms.sources.amadeusndc;


import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;

@Getter
public class AmadeusNDCConstants {

	public static final String BEST_PRICING_OPTION = "CHJ";

	public static final String INFANT = "INF";

	public static final String CHILDREN = "CHD";

	public static final String ADULT = "ADT";

	public static final BigDecimal SERVICE_VERSION = new BigDecimal("18.1");

	public static final String COUNTRY_CODE = "IN";

	public static final String AGGREGATOR_ID = "IWN";

	public static final String AIRLINE_DESIG_CODE = "SQ";

	public static final String PAYMENT_TYPE_CREDIT_CARD = "CC";

	public static final String PAYMENT_TYPE_CASH = "CA";

	public static final String PREF_LEVEL_TEXT = "CFS";
	
	public static final String CHECKED_BAGGAGE_STRING = "BAGGAGEALLOWANCE_CARRYON";


	// FareRules Messages
	public static final String NOT_REFUNDABLE = "Cancel not refunded";
	public static final String CANCEL_PERMITTED = "Cancel permitted";
	public static final String REISSUE_PERMITTED = "Reissue permitted";

	public static final String DEFAULT_POSTAL_CODE = "000000";

	public static final List<String> RESTRICTED_SEAT_CHARACTERISTIC_CODE = Arrays.asList("1", "8", "28", "BA", "BK",
			"C", "CL", "CS", "D", "E", "EX", "GN", "LA", "LG", "PC", "SO", "ST", "TA", "V", "701", "702");

	// maximum penalty of ticket
	public static final List<String> REISSUE_AFTER_TICKET = Arrays.asList("ADX");
	public static final List<String> REISSUE_BEFORE_TICKET = Arrays.asList("BDX");
	public static final List<String> CANCELLATION_AFTER_TICKET = Arrays.asList("ADT");
	public static final List<String> CANCELLATION_BEFORE_TICKET = Arrays.asList("BDT");
	public static final List<String> NOSHOW_BEFORE_TICKET = Arrays.asList("BNX");
	public static final List<String> NOSHOW_AFTER_TICKET = Arrays.asList("ANX");


	// pnr cancellation/hold messages
	public static final String PNR_HOLD_STATUS_CODE = "HK";
	public static final String PNR_CANCELLED_MESSAGE = "RESERVATION PREVIOUSLY CANCELLED";
	public static final String PNR_CANCELLED_CODE = "911";
	public static final String PNR_SPLIT_CODE = "ASSOCIATED_BOOKING";
	public static final String ORDER_CHANGE_REASON = "DIV";


}
