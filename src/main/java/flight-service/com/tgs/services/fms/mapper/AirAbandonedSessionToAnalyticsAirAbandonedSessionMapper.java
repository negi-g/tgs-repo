package com.tgs.services.fms.mapper;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.analytics.AbandonedSessionAirQuery;
import com.tgs.services.fms.datamodel.supplier.SupplierAbandonedSessionInfo;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AirAbandonedSessionToAnalyticsAirAbandonedSessionMapper extends Mapper<AbandonedSessionAirQuery> {

	private User user;
	private ContextData contextData;
	private SupplierAbandonedSessionInfo sessionData;

	@Override
	protected void execute() throws CustomGeneralException {

		output = output != null ? output : AbandonedSessionAirQuery.builder().build();
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		output.setSourcename(AirSourceType.getAirSourceType(Integer.valueOf(sessionData.getSourceId())).name());
		output.setSuppliername(sessionData.getSupplierId());
		output.setJobtriggeredtime(sessionData.getJobTriggeredTime());
		output.setExpirytime(sessionData.getExpiryTime());
		output.setCountofsessions(sessionData.getCountOfSessions());
	}

}
