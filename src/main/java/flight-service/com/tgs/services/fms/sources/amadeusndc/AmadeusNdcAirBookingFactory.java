package com.tgs.services.fms.sources.amadeusndc;

import org.iata.iata._2015._00._2018_1.orderviewrs.OrderViewRS;
import org.springframework.stereotype.Service;
import com.amadeus.wsdl.altea_ndc_18_1_v1_v4.AlteaNDC181Service;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.oms.datamodel.Order;

@Service
public class AmadeusNdcAirBookingFactory extends AbstractAirBookingFactory {

	private static AlteaNDC181Service service = null;

	private String toCurrency;

	public AmadeusNdcAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
		if (service == null) {
			service = new AlteaNDC181Service();
		}
		toCurrency = AirSourceConstants.getClientCurrencyCode();
	}

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;
		AmadeusNdcBookingManager bookingManager =
				AmadeusNdcBookingManager.builder().bookingSegmentInfos(bookingSegments.getSegmentInfos())
						.bookingId(bookingId).supplierConf(supplierConf).order(order).gstInfo(gstInfo).service(service)
						.creditCard(getSupplierBookingCreditCard()).criticalMessageLogger(criticalMessageLogger)
						.moneyExchangeComm(moneyExchangeComm).toCurrency(toCurrency).build();
		if (bookingManager.createOrder(isHoldBooking)) {
			pnr = bookingManager.getAirlinePNR();
			String supplierPNR = bookingManager.getSupplierBookingId();
			bookingSegments.setSupplierBookingId(supplierPNR);
			BookingUtils.updateSupplierBookingId(bookingSegments.getSegmentInfos(), supplierPNR);
			if (isHoldBooking) {
				updateTimeLimit(bookingManager.getTicketingTimelimit());
			}
			isSuccess = true;
		}
		return isSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isSuccess = false;
		AmadeusNdcBookingRetrieveManager retrieveManager = AmadeusNdcBookingRetrieveManager.builder()
				.bookingId(bookingId).supplierConf(supplierConf).service(service)
				.criticalMessageLogger(criticalMessageLogger).toCurrency(toCurrency)
				.moneyExchangeComm(moneyExchangeComm).supplierPNR(bookingSegments.getSupplierBookingId()).build();
		OrderViewRS orderViewRs = retrieveManager.orderRetrieve();

		// client currency converted amount
		double amountToBePaidToAirline = retrieveManager.getAmountTobePaidToAirline(orderViewRs);
		if (!isFareDiff(amountToBePaidToAirline)) {
			AmadeusNdcBookingManager bookingManager =
					AmadeusNdcBookingManager.builder().bookingSegmentInfos(bookingSegments.getSegmentInfos())
							.bookingId(bookingId).supplierConf(supplierConf).order(order).gstInfo(gstInfo)
							.service(service).creditCard(getSupplierBookingCreditCard()).airlinePNR(pnr)
							.supplierBookingId(bookingSegments.getSupplierBookingId()).toCurrency(toCurrency)
							.criticalMessageLogger(criticalMessageLogger).moneyExchangeComm(moneyExchangeComm).build();
			bookingManager.setAirlineTotalPrice(retrieveManager.getAirlineTotalPrice());
			if (bookingManager.doPayment()) {
				isSuccess = true;
			}
		}
		return isSuccess;
	}

	@Override
	public boolean confirmBeforeTicket() {
		boolean isFareDiff = true;
		AmadeusNdcBookingRetrieveManager retrieveManager = AmadeusNdcBookingRetrieveManager.builder()
				.bookingId(bookingId).supplierConf(supplierConf).service(service).toCurrency(toCurrency)
				.criticalMessageLogger(criticalMessageLogger).moneyExchangeComm(moneyExchangeComm)
				.supplierPNR(bookingSegments.getSupplierBookingId()).build();
		OrderViewRS orderViewRs = retrieveManager.orderRetrieve();
		boolean isSegmentsActive = retrieveManager.isSegmentsAndPNRActive(orderViewRs);
		double amountToBePaidToAirline = retrieveManager.getAmountTobePaidToAirline(orderViewRs);
		if (isSegmentsActive && !isSameDayTicketing()) {
			AmadeusNdcBookingManager bookingManager =
					AmadeusNdcBookingManager.builder().bookingSegmentInfos(bookingSegments.getSegmentInfos())
							.bookingId(bookingId).supplierConf(supplierConf).order(order).service(service)
							.criticalMessageLogger(criticalMessageLogger).moneyExchangeComm(moneyExchangeComm)
							.toCurrency(toCurrency).build();

			// Reprice
			orderViewRs = retrieveManager.orderRetrieve();
			amountToBePaidToAirline = retrieveManager.getAmountTobePaidToAirline(orderViewRs);
		}

		if (isSegmentsActive && !isFareDiff(amountToBePaidToAirline)) {
			isFareDiff = false;
		}
		return isFareDiff;
	}

}
