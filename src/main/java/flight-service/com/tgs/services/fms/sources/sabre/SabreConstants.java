package com.tgs.services.fms.sources.sabre;

public class SabreConstants {

	public static final String CONVERSATION_ID = "20111123T121325638";

	public static final String SESSIONLESS_SERVICE = "TokenCreateRQ";

	public static final String SESSION_SERVICE = "SessionCreateRQ";

	public static final String BFM_SEARCH = "BargainFinderMaxRQ";

	public static final String TRAVEL_ITINERARY = "TravelItineraryReadRQ";

	public static final String AIR_PRICE = "OTA_AirPriceLLSRQ";

	public static final String END_TRANSACTION = "EndTransactionLLSRQ";

	public static final String SABRE_COMMAND = "SabreCommandLLSRQ";

	public static final String SESSION_CLOSE = "SessionCloseRQ";

	public static final String ENHANCED_AIR_BOOK = "EnhancedAirBookRQ";

	public static final String PASSENGER_DETAILS = "PassengerDetailsRQ";

	public static final String DESIGNATE_PRINTER = "DesignatePrinterLLSRQ";

	public static final String STRUCTURE_FARE_RULE = "StructureFareRulesRQ";

	public static final String AIR_TICKET = "AirTicketLLSRQ";

	public static final String AIR_FARE_RULES = "OTA_AirRulesLLSRQ";

	public static final String CONTEXT_CHANGE = "ContextChangeLLSRQ";

	public static final String SPECIAL_SERVICE = "SpecialServiceLLSRQ";

	public static final String DELETE_PRICE_QUOTE = "DeletePriceQuoteLLSRQ";

	public static final String TICKET_TYPE = "7TAW";

	public static final int MAX_NUM_ATTEMPTS = 5;

	public static final int WAIT_INTERVAL = 5000;

	public static final String END_TRANSACTION_VERSION = "2.0.8";

	public static final String RECEIVED_FROM = "SWS TEST";

	public static final String ECHO_TOKEN = "String";

	public static final String SABRE_CMD_VERSION = "1.8.1";

	public static final String SABRE_CMD_OUTPUT = "SCREEN";

	public static final String PASSENGER_DETAILS_VERSION = "3.4.0";

	public static final String ENHANCED_RQ_VERSION = "3.10.0";

	public static final String DESIGNATE_PRINTER_VERSION = "2.0.2";

	public static final String TRAVEL_ITINARY_READLLS_VERSION = "3.9.0";

	public static final String AIR_PRICE_VERSION = "2.17.0";

	public static final String AIR_TICKET_VERSION = "2.11.0";

	public static final String IGNORE_COMMAND = "I";

	public static final String OK_STATUS = "OK";

	public static final String AIR_RULES_VERSION = "2.3.0";

	public static final String CONTEXT_CHANGE_VERSION = "2.0.3";

	public static final String SPECIAL_SERVICE_VERSION = "2.3.0";

	public static final String IGNORE_RETRY_COMMAND = "IR";

	public static final String AIR_AVAIL_VERSION = "2.4.0";

	public static final String AIR_AVAIL_SERVICE = "OTA_AirAvailLLSRQ";

	public static final String DELETE_PRICEQUOTE_VERSION = "2.1.0";

	public static final String SEGMENT_CANCELLED_STATUS = "HX";

	public static final String OTA_CANCEL_SERVICE = "OTA_CancelLLSRQ";

	public static final String OTA_CANCEL_VERSION = "2.0.2";

	public static final String BFM_VERSION = "6.1.0";

	public static final String ARUNK_SERVICE = "ARUNK_LLSRQ";

	public static final String ARUNK_VERSION = "2.0.2";


}
