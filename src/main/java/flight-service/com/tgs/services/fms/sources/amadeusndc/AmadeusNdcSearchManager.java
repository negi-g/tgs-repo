package com.tgs.services.fms.sources.amadeusndc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.ws.BindingProvider;
import com.tgs.services.fms.datamodel.*;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyContent;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyType;
import com.tgs.services.fms.datamodel.farerule.FareRuleTimeWindow;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import io.jsonwebtoken.lang.Collections;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.axis2.databinding.types.Token;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.xerces.dom.ElementNSImpl;
import org.iata.iata._2015._00._2018_1.airshoppingrq.*;
import org.iata.iata._2015._00._2018_1.airshoppingrq.CabinTypeType;
import org.iata.iata._2015._00._2018_1.airshoppingrq.CarrierType;
import org.iata.iata._2015._00._2018_1.airshoppingrq.CountryType;
import org.iata.iata._2015._00._2018_1.airshoppingrq.IATAPayloadStandardAttributesType;
import org.iata.iata._2015._00._2018_1.airshoppingrq.OriginDestType;
import org.iata.iata._2015._00._2018_1.airshoppingrq.PaxType;
import org.iata.iata._2015._00._2018_1.airshoppingrq.ProgramCriteriaType.ProgramOwner;
import org.iata.iata._2015._00._2018_1.airshoppingrs.*;
import org.iata.iata._2015._00._2018_1.airshoppingrs.DatedMarketingSegmentType;
import org.iata.iata._2015._00._2018_1.airshoppingrs.DatedOperatingSegmentType;
import org.iata.iata._2015._00._2018_1.airshoppingrs.FareComponentType;
import org.iata.iata._2015._00._2018_1.airshoppingrs.FareDetailType;
import org.iata.iata._2015._00._2018_1.airshoppingrs.FareDetailType.PassengerRefs;
import org.iata.iata._2015._00._2018_1.airshoppingrs.FarePriceDetailType;
import org.iata.iata._2015._00._2018_1.airshoppingrs.FlightInfoAssocType;
import org.iata.iata._2015._00._2018_1.airshoppingrs.FlightSegmentReference;
import org.iata.iata._2015._00._2018_1.airshoppingrs.OfferItemType;
import org.iata.iata._2015._00._2018_1.airshoppingrs.PaxJourneyType;
import org.iata.iata._2015._00._2018_1.airshoppingrs.PaxSegmentType;
import org.iata.iata._2015._00._2018_1.airshoppingrs.TaxDetailType.Breakdown.Tax;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser;
import com.google.common.base.Enums;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import lombok.extern.slf4j.Slf4j;
import org.iata.iata._2015._00._2018_1.airshoppingrs.TransportArrivalType;
import org.iata.iata._2015._00._2018_1.airshoppingrs.TransportDepType;
import static com.tgs.services.fms.sources.amadeusndc.AmadeusNDCConstants.*;
import static com.tgs.services.fms.sources.amadeusndc.AmadeusNDCConstants.BEST_PRICING_OPTION;

@Slf4j
@Getter
@Setter
@SuperBuilder
final class AmadeusNdcSearchManager extends AmadeusNdcServiceManager {

	protected AirSearchQuery searchQuery;

	private Map<String, PaxJourneyType> paxJourneyList;
	private Map<String, PaxSegmentType> paxSegmentMap;
	private Map<String, String> paxMap;
	private Map<String, FlightSegmentReference> seatsLeftMap;
	private Map<String, BaggageAllowanceType> baggageAllowanceMap;
	private Map<String, PriceClassType> priceClassMap;
	private String shoppingResponseId;

	private Map<String, PenaltyType> penaltyMap;

	public AirSearchResult airShopping() {
		AirShoppingRQ airShoppingRQ = null;
		AirSearchResult searchResult = new AirSearchResult();
		log.debug("Starting creating port service .....");
		AlteaNDC181PT servicePort = service.getAlteaNDC181Port();
		log.debug("completed creating port service .....");
		airShoppingRQ = createAirShoppingRQ();
		AMASecurityHostedUser securityHostedUser = getSecurityHostedUser();

		BindingProvider bp = (BindingProvider) servicePort;
		addJAXHandlers(bp, searchQuery.getSearchId(), AmadeusNdcHeaderHandler._AIR_SHOPPING_);

		AirShoppingRS airShoppingRS = servicePort.ndcAirShopping(airShoppingRQ, null, null, securityHostedUser);

		if (CollectionUtils.isNotEmpty(airShoppingRS.getError())) {
			StringJoiner errorMessage = new StringJoiner("");
			for (ErrorType error : airShoppingRS.getError()) {
				errorMessage.add(error.getCode() + ": " + error.getDescText());
			}
			throw new NoSearchResultException(errorMessage.toString());
		}

		shoppingResponseId = airShoppingRS.getResponse().getShoppingResponse().getShoppingResponseID();
		paxJourneyList = new HashMap<>();
		paxSegmentMap = new HashMap<>();
		paxMap = new HashMap<>();
		priceClassMap = new HashMap<>();
		seatsLeftMap = new HashMap<>();
		baggageAllowanceMap = new HashMap<>();
		penaltyMap = new HashMap<>();
		FlightInfoAssocType flightSegmentReferenceType = airShoppingRS.getResponse().getAirShoppingProcessing()
				.getMarketingMessages().getMarketMessage().get(0).getAssociations().getOfferAssociations().getFlight();
		List<FlightSegmentReference> flightReferences = flightSegmentReferenceType.getFlightSegmentReference();
		BaggageAllowanceListType baggageAllowanceList =
				airShoppingRS.getResponse().getDataLists().getBaggageAllowanceList();
		List<PenaltyType> penaltyList = airShoppingRS.getResponse().getDataLists().getPenaltyList().getPenalty();
		List<PriceClassType> priceClassList =
				airShoppingRS.getResponse().getDataLists().getPriceClassList().getPriceClass();
		// map holds trip reference of segments , i.e FLT11 -> SEG2,SEG4
		for (PaxJourneyType paxJourney : airShoppingRS.getResponse().getDataLists().getPaxJourneyList()
				.getPaxJourney()) {
			paxJourneyList.put(paxJourney.getPaxJourneyID(), paxJourney);
		}

		for (PaxSegmentType segmentType : airShoppingRS.getResponse().getDataLists().getPaxSegmentList()
				.getPaxSegment()) {
			paxSegmentMap.put(segmentType.getPaxSegmentID(), segmentType);
		}

		for (org.iata.iata._2015._00._2018_1.airshoppingrs.PaxType paxType : airShoppingRS.getResponse().getDataLists()
				.getPaxList().getPax()) {
			String paxId = paxType.getPaxID();
			String ptc = paxType.getPTC();
			paxMap.put(ptc, paxId);

		}
		log.debug("NDC paxMap {}", paxMap);

		for (FlightSegmentReference flightReference : flightReferences) {
			String flightSegmentId = flightReference.getRef();
			seatsLeftMap.put(flightSegmentId, flightReference);
		}

		for (BaggageAllowanceType baggageAllowance : baggageAllowanceList.getBaggageAllowance()) {
			String bagAllowanceId = baggageAllowance.getBaggageAllowanceID();
			baggageAllowanceMap.put(bagAllowanceId, baggageAllowance);
		}

		for (PenaltyType penalty : penaltyList) {
			String penaltyId = penalty.getPenaltyID();
			penaltyMap.put(penaltyId, penalty);
		}
		for (PriceClassType priceClass : priceClassList) {
			priceClassMap.put(priceClass.getPriceClassID(), priceClass);
		}

		searchResult = buildJourneys(airShoppingRS);

		return searchResult;
	}

	private AirSearchResult buildJourneys(AirShoppingRS airShoppingRS) {
		AirSearchResult searchResult = new AirSearchResult();
		// parsing offer
		ResponseType.OffersGroup offersGroup = airShoppingRS.getResponse().getOffersGroup();
		List<AirlineOffersType> offersTypeList = offersGroup.getCarrierOffers();

		List<TripInfo> tripInfos = new ArrayList<>();

		for (AirlineOffersType offerTypeFare : offersTypeList) {
			List<OfferType> offerTypes = offerTypeFare.getOffer();
			for (OfferType offerType : offerTypes) {
				if (isValidCabinClass(offerType)) {
					try {
						TripInfo tripInfo = null;
						// complete journey static info (segment)
						List<String> penaltyRefs = offerType.getPenaltyRefID();
						JourneyOverviewType journeyOverview = offerType.getJourneyOverview();
						List<SegmentInfo> segmentInfos = new ArrayList<>();
						int journeyNo = 0;
						// journey price class -->flt1,flt2.....
						for (JourneyOverviewType.JourneyPriceClass priceClass : journeyOverview
								.getJourneyPriceClass()) {
							segmentInfos.addAll(buildSegmentInfos(priceClass, offerType, journeyNo, penaltyRefs));
							journeyNo++;
						}
						tripInfo = new TripInfo();
						tripInfo.setSegmentInfos(segmentInfos);
						List<TripInfo> tempTrips = tripInfo.splitTripInfo(false);
						for (TripInfo trip : tempTrips) {
							trip.setConnectionTime();
						}
						tripInfos.add(tripInfo);
					} catch (Exception e) {
						log.error("Unable to create trip {}", searchQuery.getSearchId(), e);
					}
				}
			}
		}
		if (searchQuery.isOneWay()) {
			searchResult.getTripInfos().put(TripInfoType.ONWARD.name(), tripInfos);
		} else {
			searchResult.getTripInfos().put(TripInfoType.COMBO.name(), tripInfos);
		}
		return searchResult;
	}

	private boolean isValidCabinClass(OfferType offerType) {
		OfferItemType offerItemType = offerType.getOfferItem().get(0);
		List<FareDetailType> nonEmptyFareDetailType=offerItemType.getFareDetail().stream()
				.filter(fareDetail -> CollectionUtils.isNotEmpty(fareDetail.getFareComponent()))
				.collect(Collectors.toList());
		FareDetailType fareDetail=nonEmptyFareDetailType.get(0);
		
		if (CollectionUtils.isNotEmpty(offerType.getOfferItem())
				&& CollectionUtils.isNotEmpty(offerType.getOfferItem().get(0).getFareDetail()) && CollectionUtils
						.isNotEmpty(fareDetail.getFareComponent())) {
			FareDetailType fareDetailType = offerType.getOfferItem().get(0).getFareDetail().get(0);
			org.iata.iata._2015._00._2018_1.airshoppingrs.FareComponentType.FareBasis.CabinType cabinType =
					fareDetail.getFareComponent().get(0).getFareBasis().getCabinType();
			ElementNSImpl cabinTypeName = (ElementNSImpl) cabinType.getCabinTypeName();
			CabinClass cabinClass = AmadeusNdcCabinClass.valueOf(cabinTypeName.getTextContent()).getCabinClass();
			if (CabinClass.ECONOMY.equals(searchQuery.getCabinClass())
					|| searchQuery.getCabinClass().equals(cabinClass)) {
				return true;
			}
		}
		return false;
	}

	private List<SegmentInfo> buildSegmentInfos(JourneyOverviewType.JourneyPriceClass priceClass, OfferType offerType,
			int journeyNo, List<String> penaltyRefs) {
		boolean isReturnSegment = searchQuery.isReturn() && journeyNo == 1;
		String paxJourneyRef = priceClass.getPaxJourneyRefID();
		PaxJourneyType paxJourneyType = paxJourneyList.get(paxJourneyRef);
		List<SegmentInfo> segmentInfos = new ArrayList<>();
		// each flightrefdid/paxjourneyrefid will have 1..m segment ref ids
		AtomicInteger segmentNumber = new AtomicInteger(0);
		for (String segmentRef : paxJourneyType.getPaxSegmentRefID()) {
			PaxSegmentType paxSegmentType = paxSegmentMap.get(segmentRef);
			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setSegmentNum(segmentNumber.get());
			TransportDepType departure = paxSegmentType.getDep();
			TransportArrivalType arrival = paxSegmentType.getArrival();
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(departure.getIATALocationCode()));
			segmentInfo.getDepartAirportInfo().setTerminal(AirUtils.getTerminalInfo(departure.getTerminalName()));
			segmentInfo
					.setDepartTime(TgsDateUtils.getLocalDateTime(departure.getAircraftScheduledDateTime().getValue()));

			segmentInfo.setArrivalAirportInfo(AirportHelper.getAirportInfo(arrival.getIATALocationCode()));
			segmentInfo.getArrivalAirportInfo().setTerminal(AirUtils.getTerminalInfo(arrival.getTerminalName()));
			segmentInfo
					.setArrivalTime(TgsDateUtils.getLocalDateTime(arrival.getAircraftScheduledDateTime().getValue()));

			if (paxSegmentType.getDuration() != null) {
				segmentInfo.setDuration(
						(paxSegmentType.getDuration().getHours() * 60) + paxSegmentType.getDuration().getMinutes());
			} else {
				segmentInfo.setDuration(segmentInfo.calculateDuration());
			}

			DatedMarketingSegmentType carrierFlightType = paxSegmentType.getMarketingCarrierInfo();
			FlightDesignator designator = FlightDesignator.builder().build();
			if (AirlineHelper.getAirlineInfo(carrierFlightType.getCarrierDesigCode()) == null) {
				carrierFlightType.getCarrierDesigCode();
			}
			designator.setAirlineInfo(AirlineHelper.getAirlineInfo(carrierFlightType.getCarrierDesigCode()));
			designator.setFlightNumber(StringUtils.trim(carrierFlightType.getMarketingCarrierFlightNumberText()));

			DatedOperatingSegmentType operatingCarrier = paxSegmentType.getOperatingCarrierInfo();
			if (operatingCarrier != null && !StringUtils.equalsIgnoreCase(designator.getAirlineCode(),
					operatingCarrier.getCarrierDesigCode())) {
				segmentInfo
						.setOperatedByAirlineInfo(AirlineHelper.getAirlineInfo(operatingCarrier.getCarrierDesigCode()));
			}

			segmentInfo.setFlightDesignator(designator);
			segmentInfo.setStops(0);
			segmentInfo.setIsReturnSegment(isReturnSegment);
			String bagAllowanceRefID = offerType.getBaggageAllowance().get(0).getBaggageAllowanceRefID();
			List<PriceInfo> priceInfoList =
					buildPriceInfo(segmentInfo, paxSegmentType, offerType, bagAllowanceRefID, journeyNo, penaltyRefs);
			segmentInfo.setPriceInfoList(priceInfoList);
			segmentInfos.add(segmentInfo);
			segmentNumber.getAndIncrement();
		}
		return segmentInfos;
	}


	public AirShoppingRQ createAirShoppingRQ() {
		AirShoppingRQ airShoppingRequest = new AirShoppingRQ();
		RequestType requestType = new RequestType();
		FlightRequestType flightRequestType = new FlightRequestType();

		searchQuery.getRouteInfos().forEach(routeInfo -> {
			OriginDestType originDestType = new OriginDestType();

			OriginDepRequestType originDepRequestType = new OriginDepRequestType();
			originDepRequestType.setIATALocationCode(routeInfo.getFromCityAirportCode());
			originDepRequestType.setDate(TgsDateUtils.toXMLGregorianCalendar(routeInfo.getTravelDate()));
			originDestType.setOriginDepRequest(originDepRequestType);

			DestArrivalRequestType destArrivalRequestType = new DestArrivalRequestType();
			destArrivalRequestType.setIATALocationCode(routeInfo.getToCityAirportCode());
			originDestType.setDestArrivalRequest(destArrivalRequestType);

			flightRequestType.getOriginDestRequest().add(originDestType);
		});
		requestType.setFlightRequest(flightRequestType);

		requestType.setPaxs(buildPaxInfos());

		ShoppingCriteriaType shoppingCriteria = new ShoppingCriteriaType();
		shoppingCriteria.getCabinTypeCriteria().addAll(createCabinCriteria());

		setBestPricingOption(shoppingCriteria);
		setCorporateCode(shoppingCriteria);
		setPromoCode(shoppingCriteria);
		requestType.setShoppingCriteria(shoppingCriteria);
		airShoppingRequest.setRequest(requestType);
		airShoppingRequest.setParty(getParty());
		airShoppingRequest.setPointOfSale(getPointofSale());
		airShoppingRequest.setPayloadAttributes(getPayloadAttributes());
		return airShoppingRequest;
	}

	private void setBestPricingOption(ShoppingCriteriaType shoppingCriteria) {
		if (BooleanUtils.isNotTrue(supplierConf.getSupplierAdditionalInfo().getIsBestPricingDisAllowed())) {
			org.iata.iata._2015._00._2018_1.airshoppingrq.BestPricingPreferencesType bestPricingPreferences =
					new org.iata.iata._2015._00._2018_1.airshoppingrq.BestPricingPreferencesType();
			bestPricingPreferences.setBestPricingOption(BEST_PRICING_OPTION);
			shoppingCriteria.setPricingMethodCriteria(bestPricingPreferences);
		}
	}

	private PaxsType buildPaxInfos() {
		PaxsType paxsType = new PaxsType();
		for (com.tgs.services.base.enums.PaxType paxRequest : searchQuery.getPaxInfo().keySet()) {
			Integer quota = searchQuery.getPaxInfo().get(paxRequest);
			while (quota != 0) {
				String paxTypeCode = com.tgs.services.base.enums.PaxType.getStringReprestation(paxRequest);
				String calculatedId = paxTypeCode + quota.toString();
				PaxType pax = new PaxType();
				pax.setPTC(paxTypeCode);

				if (StringUtils.equalsIgnoreCase(INFANT, paxTypeCode)) {
					Token parentIdToken = new Token();
					parentIdToken.setValue(StringUtils.join(ADULT, String.valueOf(quota)));
					pax.setPaxRefID(calculatedId);
				}
				Token idToken = new Token();
				idToken.setValue(calculatedId);
				pax.setPaxID(calculatedId);
				paxsType.getPax().add(pax);
				quota--;
			}
		}
		return paxsType;
	}

	private List<PriceInfo> buildPriceInfo(SegmentInfo segmentInfo, PaxSegmentType paxSegmentType, OfferType offerType,
			String bagAllowanceRefID, int journeyNo, List<String> penaltyRefs) {
		String offerId = offerType.getOfferID();
		OfferItemType offerItemType = offerType.getOfferItem().get(0);

		List<PriceInfo> priceInfoList = new ArrayList<>();
		PriceInfo priceInfo = PriceInfo.builder(searchQuery.getRequestId()).build();
		priceInfo.setSupplierBasicInfo(supplierConf.getBasicInfo());
		Map<com.tgs.services.base.enums.PaxType, FareDetail> fareDetailsMap = priceInfo.getFareDetails();
		List<FareDetailType> nonEmptyFareDetailType = offerItemType.getFareDetail().stream()
				.filter(fareDetail -> CollectionUtils.isNotEmpty(fareDetail.getFareComponent()))
				.collect(Collectors.toList());
		List<FareComponentType> fareComponentType = nonEmptyFareDetailType.get(0).getFareComponent();
		String fareBasisCode = fareComponentType.get(0).getFareBasis().getFareBasisCode().getCode();
		String rbdCode = fareComponentType.get(0).getFareBasis().getRBD();
		String offerItemId = offerItemType.getOfferItemID();

		for (Entry<com.tgs.services.base.enums.PaxType, Integer> paxInfo : searchQuery.getPaxInfo().entrySet()) {
			if (paxInfo.getValue() > 0) {
				FareDetail fareDetail = new FareDetail();
				com.tgs.services.base.enums.PaxType paxType = paxInfo.getKey();
				String amadeusPaxType = AmadeusNdcPaxType.getEnumFromPaxType(paxType).name();
				String paxId = paxMap.get(amadeusPaxType);
				FareDetailType fareDetailType = getFareDetailTypeByPaxId(paxId, offerItemType.getFareDetail());
				if (segmentInfo.getSegmentNum() == 0 && journeyNo == 0) {
					FarePriceDetailType farePriceDetailType = fareDetailType.getPrice();
					fareDetail.setFareComponents(getFareComponentMap(farePriceDetailType));
				} else {
					Map<FareComponent, Double> fareComponentMap = new HashMap<>();
					fareComponentMap.put(FareComponent.TF, 0.0d);
					fareComponentMap.put(FareComponent.BF, 0.0d);
					fareComponentMap.put(FareComponent.AT, 0.0d);
					fareDetail.setFareComponents(fareComponentMap);
				}
				buildFareDetails(fareDetail, fareDetailType, offerType, paxType);
				fareDetail.setFareBasis(fareBasisCode);
				fareDetail.setClassOfBooking(rbdCode);
				org.iata.iata._2015._00._2018_1.airshoppingrs.FareComponentType.FareBasis.CabinType cabinType =
						fareDetailType.getFareComponent().get(0).getFareBasis().getCabinType();
				ElementNSImpl cabinTypeName = (ElementNSImpl) cabinType.getCabinTypeName();
				fareDetail.setCabinClass(AmadeusNdcCabinClass.valueOf(cabinTypeName.getTextContent()).getCabinClass());
				fareDetailsMap.put(paxType, fareDetail);
			}
		}

		priceInfo.setFareDetails(fareDetailsMap);
		setFareType(priceInfo, fareComponentType.get(0));
		PriceMiscInfo miscInfo =
				PriceMiscInfo.builder().journeyKey(shoppingResponseId).fareKey(offerId).fareLevel(offerItemId).build();
		setMiniFareRuleFromPenalty(penaltyRefs, miscInfo);
		priceInfo.setMiscInfo(miscInfo);
		priceInfoList.add(priceInfo);
		return priceInfoList;
	}


	private FareDetailType getFareDetailTypeByPaxId(String paxId, List<FareDetailType> fareDetailTypes) {
		for (FareDetailType fareDetailType : fareDetailTypes) {
			for (PassengerRefs paxRef : fareDetailType.getPassengerRefs()) {
				if (Arrays.asList(paxRef.getValue().split(" ")).contains(paxId)) {
					return fareDetailType;
				}
			}
		}
		return null;
	}

	private void buildFareDetails(FareDetail fareDetail, FareDetailType fareDetailType, OfferType offerType,
			com.tgs.services.base.enums.PaxType paxType) {
		String bagAllowanceRefId = offerType.getBaggageAllowance().get(0).getBaggageAllowanceRefID();
		BaggageAllowanceType bagAllowanceType = baggageAllowanceMap.get(bagAllowanceRefId);
		String weightAllowance = null;
		String pieceAllowance = null;
		if (CollectionUtils.isNotEmpty(bagAllowanceType.getWeightAllowance())) {
			weightAllowance = getBaggageString(
					bagAllowanceType.getWeightAllowance().get(0).getMaximumWeightMeasure().getValue().toString(),
					bagAllowanceType.getWeightAllowance().get(0).getMaximumWeightMeasure().getUnitCode());
		} else if (CollectionUtils.isNotEmpty(bagAllowanceType.getPieceAllowance())) {
			pieceAllowance = bagAllowanceType.getPieceAllowance().get(0).getTotalQty().toString();
			log.debug("Setting peice allowance {} ,weight not available", pieceAllowance);
		}

		List<String> penaltyRefIds = offerType.getPenaltyRefID();
		BaggageInfo baggageInfo = new BaggageInfo();
		if (!(com.tgs.services.base.enums.PaxType.INFANT.equals(paxType))) {
			if (StringUtils.isNotBlank(weightAllowance)) {
				baggageInfo.setAllowance(weightAllowance);
			} else if (StringUtils.isNotBlank(pieceAllowance)) {
				baggageInfo.setAllowance(pieceAllowance + " " + " Piece");
			}
			String priceClassRefId = offerType.getJourneyOverview().getPriceClassRefID();
			PriceClassType priceClassType = priceClassMap.get(priceClassRefId);
			priceClassType.getDesc().forEach(description -> {
				if (StringUtils.equals(description.getDescID(), AmadeusNDCConstants.CHECKED_BAGGAGE_STRING)) {
					baggageInfo.setCabinBaggage(description.getDescText());
				}
			});
		}

		fareDetail.setBaggageInfo(baggageInfo);
		setRefundableType(penaltyRefIds, fareDetail);
		setSeatsRemaining(fareDetail, offerType);
		fareDetail.setIsMealIncluded(true);
	}

	private void setRefundableType(List<String> penaltyRefIds, FareDetail fareDetail) {
		for (String penaltyRefId : penaltyRefIds) {
			String message = penaltyMap.get(penaltyRefId).getDescText().get(0);
			if (StringUtils.equalsIgnoreCase(message, AmadeusNDCConstants.NOT_REFUNDABLE)) {
				fareDetail.setRefundableType(RefundableType.NON_REFUNDABLE.getRefundableType());
			} else {
				fareDetail.setRefundableType(RefundableType.REFUNDABLE.getRefundableType());
			}
		}
	}

	private void setFareType(PriceInfo priceInfo, FareComponentType fareComponentType) {
		String priceClassRefId = fareComponentType.getPriceClassRef();
		String priceClassName = priceClassMap.get(priceClassRefId).getName();
		priceInfo.setFareIdentifier(AmadeusNdcUtils.getFareType(priceClassName));
	}

	@Deprecated
	private void setFareTypeFromCabinCode(PriceInfo priceInfo, FareComponentType fareComponentType) {
		CabinClass cabinClass = priceInfo.getCabinClass(com.tgs.services.base.enums.PaxType.ADULT);
		ElementNSImpl cabinTypeCode =
				(ElementNSImpl) fareComponentType.getFareBasis().getCabinType().getCabinTypeCode();
		priceInfo.setFareIdentifier(AmadeusNdcFareType.getFareTypeFromCode(cabinTypeCode.getTextContent()));

	}

	private void setSeatsRemaining(FareDetail fareDetail, OfferType offerType) {
		String journeyRefId = offerType.getJourneyOverview().getJourneyPriceClass().get(0).getPaxJourneyRefID();
		PaxJourneyType paxJourneyType = paxJourneyList.get(journeyRefId);
		List<String> segmentRefIds = paxJourneyType.getPaxSegmentRefID();
		for (String segmentId : segmentRefIds) {
			Integer count = seatsLeftMap.get(segmentId).getClassOfService().getCode().getSeatsLeft();
			fareDetail.setSeatRemaining(count);
		}
	}

	private Map<FareComponent, Double> getFareComponentMap(FarePriceDetailType farePriceDetailType) {
		Map<FareComponent, Double> fareComponents = new HashMap<>();

		double totalFare = getAmountBasedOnCurrency(
				farePriceDetailType.getTotalAmount().getDetailCurrencyPrice().getTotal().getValue(), getCurrencyCode());
		fareComponents.put(FareComponent.TF, totalFare);

		double baseFare = getAmountBasedOnCurrency(farePriceDetailType.getBaseAmount().getValue(), getCurrencyCode());
		fareComponents.put(FareComponent.BF, baseFare);

		if (farePriceDetailType.getTaxes().getBreakdown() != null
				&& CollectionUtils.isNotEmpty(farePriceDetailType.getTaxes().getBreakdown().getTax())) {
			for (Tax tax : farePriceDetailType.getTaxes().getBreakdown().getTax()) {
				if (tax.getAmount() != null && tax.getAmount().getValue() != null) {
					FareComponent fareComponent = Enums.getIfPresent(GDSFareComponentMapper.class, tax.getTaxCode())
							.or(GDSFareComponentMapper.TX).getFareComponent();
					double taxAmount = getAmountBasedOnCurrency(tax.getAmount().getValue(), getCurrencyCode());
					double oldValue = fareComponents.getOrDefault(fareComponent, 0.0);
					fareComponents.put(fareComponent, oldValue + taxAmount);
				}
			}
		} else {
			double taxes =
					getAmountBasedOnCurrency(farePriceDetailType.getTaxes().getTotal().getValue(), getCurrencyCode());
			fareComponents.put(FareComponent.AT, taxes);
		}
		return fareComponents;
	}

	private List<CabinTypeType> createCabinCriteria() {
		List<CabinTypeType> cabinTypes = new ArrayList<>();
		if (CabinClass.ECONOMY.equals(searchQuery.getCabinClass())) {
			for (AmadeusNdcCabinClass cabin : AmadeusNdcCabinClass.values()) {
				CabinTypeType cabinTypeType = new CabinTypeType();
				cabinTypeType.setCabinTypeName(cabin.getCode());
				cabinTypes.add(cabinTypeType);
			}
		} else {
			CabinTypeType cabinTypeType = new CabinTypeType();
			cabinTypeType.setCabinTypeName(
					AmadeusNdcCabinClass.getEnumFromCabinClass(searchQuery.getCabinClass()).getCode());
			cabinTypes.add(cabinTypeType);
		}

		return cabinTypes;
	}

	private void setPromoCode(ShoppingCriteriaType shoppingCriteria) {
		if (StringUtils.isNotBlank(supplierConf.getSupplierAdditionalInfo().getPromoCode())) {
			org.iata.iata._2015._00._2018_1.airshoppingrq.PromotionType promotionType =
					new org.iata.iata._2015._00._2018_1.airshoppingrq.PromotionType();
			promotionType.setPromotionID(supplierConf.getSupplierAdditionalInfo().getPromoCode());
			PromotionIssuerType promotionIssuerType = new PromotionIssuerType();
			CarrierType carrierType = new CarrierType();
			carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
			promotionIssuerType.setCarrier(carrierType);
			promotionType.setPromotionIssuer(promotionIssuerType);
			shoppingCriteria.setPromotionCriteria(promotionType);
		}

	}

	private void setCorporateCode(ShoppingCriteriaType shoppingCriteria) {
		if (CollectionUtils.isNotEmpty(supplierConf.getSupplierAdditionalInfo().getAccountCodes())) {
			ProgramCriteriaType programCriteria = new ProgramCriteriaType();
			List<String> corporateCodes = supplierConf.getSupplierAdditionalInfo().getAccountCodes().size() > 6
					? supplierConf.getSupplierAdditionalInfo().getAccountCodes().subList(0, 6)
					: supplierConf.getSupplierAdditionalInfo().getAccountCodes();
			for (String corporateCode : corporateCodes) {
				ProgramAccountType programAccount = new ProgramAccountType();
				programAccount.setAccountID(corporateCode);
				programCriteria.getProgramAccount().add(programAccount);
			}
			ProgramOwner programOwner = new ProgramOwner();
			CarrierType carrierType = new CarrierType();
			carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
			programOwner.setCarrier(carrierType);
			programCriteria.setProgramOwner(programOwner);
			shoppingCriteria.getProgramCriteria().add(programCriteria);
		}
	}

	private PartyType getParty() {
		PartyType party = new PartyType();
		SenderType sender = new SenderType();
		TravelAgencyType travelAgency = new TravelAgencyType();
		travelAgency.setAgencyID(supplierConf.getSupplierCredential().getIataNumber());
		travelAgency.setIATANumber(new BigDecimal(supplierConf.getSupplierCredential().getIataNumber()));
		travelAgency.setName(supplierConf.getSupplierCredential().getAgentUserName());
		travelAgency.setTypeCode(TravelAgencyTypeCodeContentType.ONLINE_TRAVEL_AGENCY);
		sender.setTravelAgency(travelAgency);
		RecipientType recipient = new RecipientType();
		CarrierType carrierType = new CarrierType();
		carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
		recipient.setORA(carrierType);
		party.setRecipient(recipient);
		party.setSender(sender);

		return party;
	}

	private IATAPayloadStandardAttributesType getPayloadAttributes() {
		IATAPayloadStandardAttributesType payloadAttribute = new IATAPayloadStandardAttributesType();
		payloadAttribute.setVersion(AmadeusNDCConstants.SERVICE_VERSION);
		return payloadAttribute;
	}

	private ResponseParametersType getResponseParametersType() {
		ResponseParametersType responseParametersType = new ResponseParametersType();
		return responseParametersType;
	}

	protected void setMiniFareRuleFromPenalty(List<String> penaltyRefs, PriceMiscInfo miscInfo) {
		if (CollectionUtils.isNotEmpty(penaltyRefs)) {
			FareRuleInformation ruleInfo = miscInfo.getFareRuleInfo();
			Map<FareRulePolicyType, Map<FareRuleTimeWindow, FareRulePolicyContent>> fareRuleInfo = new HashMap<>();
			Map<FareRuleTimeWindow, FareRulePolicyContent> cancellationTimeWindow = new HashMap<>();
			Map<FareRuleTimeWindow, FareRulePolicyContent> dateChangeTimeWindow = new HashMap<>();
			Map<FareRuleTimeWindow, FareRulePolicyContent> noShowTimeWindow = new HashMap<>();
			for (String penaltyCode : penaltyRefs) {
				PenaltyType penaltyType = penaltyMap.get(penaltyCode);
				if (BooleanUtils.isTrue(penaltyType.isChangeFeeInd())
						&& REISSUE_BEFORE_TICKET.contains(penaltyType.getAppCode())
						&& StringUtils.equals(REISSUE_PERMITTED, penaltyType.getDescText().get(0))) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					dateChangeTimeWindow.put(FareRuleTimeWindow.BEFORE_DEPARTURE, content);
				}
				if (BooleanUtils.isTrue(penaltyType.isCancelFeeInd())
						&& CANCELLATION_BEFORE_TICKET.contains(penaltyType.getAppCode())
						&& StringUtils.equals(CANCEL_PERMITTED, penaltyType.getDescText().get(0))) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					cancellationTimeWindow.put(FareRuleTimeWindow.BEFORE_DEPARTURE, content);
				}
				if (BooleanUtils.isTrue(penaltyType.isCancelFeeInd())
						&& CANCELLATION_AFTER_TICKET.contains(penaltyType.getAppCode())) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					cancellationTimeWindow.put(FareRuleTimeWindow.AFTER_DEPARTURE, content);
				}
				if (BooleanUtils.isTrue(penaltyType.isChangeFeeInd())
						&& REISSUE_AFTER_TICKET.contains(penaltyType.getAppCode())) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					dateChangeTimeWindow.put(FareRuleTimeWindow.BEFORE_DEPARTURE, content);
				}
				if (NOSHOW_AFTER_TICKET.contains(penaltyType)) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					noShowTimeWindow.put(FareRuleTimeWindow.AFTER_DEPARTURE, content);
				}
				if (NOSHOW_BEFORE_TICKET.contains(penaltyType)) {
					FareRulePolicyContent content = new FareRulePolicyContent();
					if (isValidAmount(penaltyType)) {
						content.setAmount(penaltyType.getPenaltyAmount().getValue().doubleValue());
					}
					content.setPolicyInfo(String.join(",", penaltyType.getDescText()));
					noShowTimeWindow.put(FareRuleTimeWindow.BEFORE_DEPARTURE, content);
				}
			}
			fareRuleInfo.put(FareRulePolicyType.CANCELLATION, cancellationTimeWindow);
			fareRuleInfo.put(FareRulePolicyType.DATECHANGE, dateChangeTimeWindow);
			fareRuleInfo.put(FareRulePolicyType.NO_SHOW, noShowTimeWindow);
			ruleInfo.setFareRuleInfo(fareRuleInfo);
			miscInfo.setFareRuleInfo(ruleInfo);
		}
	}


	private boolean isValidAmount(PenaltyType penaltyType) {
		return penaltyType.getPenaltyAmount() != null && penaltyType.getPenaltyAmount().getValue() != null
				&& penaltyType.getPenaltyAmount().getValue().doubleValue() >= 0;
	}

	private PointofSaleType2 getPointofSale() {
		PointofSaleType2 pointOfSale = new PointofSaleType2();
		CountryType countyType = new CountryType();
		countyType.setCountryCode(COUNTRY_CODE);
		pointOfSale.setCountry(countyType);
		return pointOfSale;
	}
}
