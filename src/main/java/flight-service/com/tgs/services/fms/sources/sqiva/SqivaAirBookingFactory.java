package com.tgs.services.fms.sources.sqiva;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;

@Service
public class SqivaAirBookingFactory extends AbstractAirBookingFactory {

	SqivaAirline airline = null;

	RestAPIListener listener = null;

	SqivaBindingService bindingService = null;

	public SqivaAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
		listener = new RestAPIListener(bookingId);
		airline = SqivaAirline.getSqivaAirline(supplierConf.getSourceId());
		bindingService = SqivaBindingService.builder().credential(supplierConf.getSupplierCredential())
				.listener(listener).user(bookingUser).airline(airline).build();
	}

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;

		double amountToBePaidToAirline = 0d;
		SqivaBookingManager bookingManager =
				SqivaBookingManager.builder().configuration(supplierConf).listener(listener)
						.criticalMessageLogger(criticalMessageLogger).bindingService(bindingService).user(bookingUser)
						.bookignId(bookingId).bookingSegments(bookingSegments).deliveryInfo(deliveryInfo).build();
		SqivaBookingRetrieveManager retrieveManager = SqivaBookingRetrieveManager.builder().configuration(supplierConf)
				.listener(listener).criticalMessageLogger(criticalMessageLogger).bindingService(bindingService)
				.user(bookingUser).bookignId(bookingId).build();
		String airlinePNR = bookingManager.booking_V2(gstInfo);
		if (StringUtils.isNotBlank(airlinePNR)) {
			pnr = airlinePNR;
			JSONObject retrieveResponse = retrieveManager.getAllBookInfo_V2(airlinePNR);
			retrieveManager.getBookPriceDetailInfo_V2(airlinePNR);
			bookingManager.updateSupplierAdditionalInfo(retrieveResponse, bookingSegments);
			SqivaSSRManager ssrManager = SqivaSSRManager.builder().configuration(supplierConf).listener(listener)
					.criticalMessageLogger(criticalMessageLogger).bindingService(bindingService).user(bookingUser)
					.bookignId(bookingId).build();
			if (AirUtils.isSSRAddedInTrip(bookingSegments.getSegmentInfos())) {
				ssrManager.addAncillary_V2(bookingSegments, airlinePNR);
				ssrManager.updatePrepaidBaggage(bookingSegments, airlinePNR);
				retrieveResponse = retrieveManager.getAllBookInfo_V2(airlinePNR);
			}
			amountToBePaidToAirline = retrieveResponse.getDouble(SqivaConstant.BOOK_BALANCE);
			retrieveManager.getBookPriceDetailInfo_V2(airlinePNR);
			if (!isFareDiff(amountToBePaidToAirline)) {
				pnr = airlinePNR;
				isSuccess = true;
			}
		}
		return isSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isTicketingSuccess = false;
		SqivaBookingRetrieveManager retrieveManager = SqivaBookingRetrieveManager.builder().configuration(supplierConf)
				.listener(listener).criticalMessageLogger(criticalMessageLogger).bindingService(bindingService)
				.user(bookingUser).bookignId(bookingId).build();
		SqivaBookingManager bookingManager = SqivaBookingManager.builder().configuration(supplierConf)
				.listener(listener).criticalMessageLogger(criticalMessageLogger).bindingService(bindingService)
				.user(bookingUser).bookignId(bookingId).bookingSegments(bookingSegments).build();
		JSONObject retrieveResponse = retrieveManager.getAllBookInfo_V2(pnr);
		double amountToBePaidToAirline = retrieveResponse.getDouble(SqivaConstant.BOOK_BALANCE);
		if (!isFareDiff(amountToBePaidToAirline)) {
			bookingManager.payment(pnr, amountToBePaidToAirline);
			isTicketingSuccess = true;
		}
		return isTicketingSuccess;
	}

}
