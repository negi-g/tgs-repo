package com.tgs.services.fms.sources.sqiva;

class SqivaConstant {

	// General
	public static final String ORG = "org";
	public static final String DES = "des";
	public static final String EMAIL = "email";
	public static final String PAX_ID = "pax_id";
	public static final String WEIGHT = "weight";
	public static final String AMOUNT = "amount";
	public static final String CALLER = "caller";
	public static final String ERR_MSG = "err_msg";
	public static final String ERR_CODE = "err_code";
	public static final String DEP_DATE = "dep_date";
	public static final String DEFAULT_WEIGHT = "0";
	public static final String RET_DATE = "ret_date";
	public static final String BOOK_CODE = "book_code";
	public static final String CONTACT_1 = "contact_1";
	public static final String BAGGAGE_ID = "baggage_id";
	public static final String ROUND_TRIP = "round_trip";
	public static final String BOOK_LEG_ID = "book_leg_id";
	public static final String TICKET_UNIT = "ticket_unit";
	public static final String SUBCLASS_DEP = "subclass_dep";
	public static final String ANCILLARY_ID = "ancillary_id";
	public static final String BOOK_BALANCE = "book_balance";
	public static final String ROUTE_DETAIL = "route_detail";
	public static final String SUBCLASS_RET = "subclass_ret";
	public static final String DEP_FLIGHT_NO = "dep_flight_no";
	public static final String COMPANY_GST_NO = "companyGstNo";
	public static final String RET_FLIGHT_NO = "ret_flight_no";
	public static final String COMPANY_GST_NAME = "companyName";
	public static final String COMPANY_GST_LOCATION = "companyLocation";
	public static final String TOTAL_BAGGAGE_AMOUNT = "total_baggage_amount";
	public static final String TOTAL_ANCILLARY_AMOUNT = "total_ancillary_amount";
	public static final String PAX_LIST = "pax_list";
	public static final String ROUTE_INFO = "route_info";
	public static final String CONTACT = "contact";
	public static final String CONTACT_EMAIL = "contact_email";
	public static final String SSR_DETAIL = "ssr_detail";
	public static final String BOOK_ANCILLARY_FEE = "book_ancillary_fee";
	public static final String DETAIL_PRICE = "detail_price";

	// Route Detail
	public static final int ROUTE_DETAIL_PAX_ID_INDEX = 18;
	public static final int ROUTE_DETAIL_FLIGHT_NUMBER = 12;
	public static final int ROUTE_DETAIL_ARRIVAL_DATE_INDEX = 9;
	public static final int ROUTE_DETAIL_BOOK_LEG_ID_INDEX = 19;
	public static final int ROUTE_DETAIL_ARRIVAL_TIME_INDEX = 11;
	public static final int ROUTE_DETAIL_DEPARTURE_DATE_INDEX = 8;
	public static final int ROUTE_DETAIL_DEPARTURE_TIME_INDEX = 10;
	public static final int ROUTE_DETAIL_TRAVELLER_LAST_NAME_INDEX = 1;
	public static final int ROUTE_DETAIL_TRAVELLER_FIRST_NAME_INDEX = 0;
	public static final int ROUTE_DETAIL_ARRIVAL_AIRPORT_CODE_INDEX = 7;
	public static final int ROUTE_DETAIL_DEPARTURE_AIRPORT_CODE_INDEX = 6;
	public static final int ROUTE_DETAIL_SUB_CLASS_INDEX = 13;

	// Pax List
	public static final int PAX_LIST_FIRST_NAME_INDEX = 0;
	public static final int PAX_LIST_LAST_NAME_INDEX = 1;
	public static final int PAX_LIST_MOBILE_INDEX = 2;
	public static final int PAX_LIST_PASSPORT_INDEX = 3;
	public static final int PAX_LIST_NATIONALITY_INDEX = 4;
	public static final int PAX_LIST_PAX_TYPE_INDEX = 5;
	public static final int PAX_LIST_TICKET_NUM_INDEX = 6;
	public static final int PAX_LIST_TITLE_INDEX = 10;
	public static final int PAX_LIST_PAX_ID_INDEX = 11;

	// Route Info
	public static final int ROUTE_INFO_DEPARTURE_CODE_INDEX = 0;
	public static final int ROUTE_INFO_ARRIVAL_CODE_INDEX = 1;
	public static final int ROUTE_INFO_DEPARTURE_DATE_INDEX = 2;
	public static final int ROUTE_INFO_ARRIVAL_DATE_INDEX = 3;
	public static final int ROUTE_INFO_DEPARTURE_TIME_INDEX = 4;
	public static final int ROUTE_INFO_ARRIVAL_TIME_INDEX = 5;
	public static final int ROUTE_INFO_SUBCLASS_INDEX = 6;
	public static final int ROUTE_INFO_FLIGHT_NUMBER_INDEX = 7;
	public static final int ROUTE_INFO_BOOK_LEG_ID_INDEX = 11;

	// SSR List
	public static final int SSR_LIST_SSR_CODE_INDEX = 8;
	public static final int SSR_LIST_SSR_DESC_INDEX = 9;
	public static final int SSR_LIST_PAX_ID_INDEX = 10;
	public static final int SSR_LIST_BOOK_LEG_ID_INDEX = 11;

	// Ticket Unit
	public static final int TICKET_UNIT_TICKET_NUMBER_INDEX = 1;
	public static final int TICKET_UNIT_TRAVELLER_NAME_INDEX = 0;

	// Book Ancillary Fee
	public static final int BOOK_ANCILLARY_FEE_SSR_CODE = 0;
	public static final int BOOK_ANCILLARY_FEE_SSR_AMOUNT = 1;

	// Detail Price
	public static final int DETAIL_PRICE_TRAVELLER_NAME_INDEX = 1;
	public static final int DETAIL_PRICE_DEPARTURE_CODE_INDEX = 2;
	public static final int DETAIL_PRICE_ARRIVAL_CODE_INDEX = 3;
	public static final int DETAIL_PRICE_FARE_COMPONENT_INDEX = 4;
	public static final int DETAIL_PRICE_FARE_AMOUNT_INDEX = 5;

}
