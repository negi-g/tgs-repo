package com.tgs.services.fms.sources.mystifly;

import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.OnePointStub;

@Setter
@Getter
@Slf4j
@Builder
public class MystiflyBindingService {

	protected SupplierConfiguration configuration;

	public OnePointStub getOnePointStub() {
		OnePointStub onePointStub = null;
		String url = null;
		try {
			if (onePointStub == null) {
				url = configuration.getSupplierCredential().getUrl();
				onePointStub = new OnePointStub(configuration.getSupplierCredential().getUrl());
			}
		} catch (Exception e) {
			log.error("Unable to initialize mystifly one point stub {} cause ", url, e);
		}
		return onePointStub;
	}

}
