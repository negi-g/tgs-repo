package com.tgs.services.fms.sources.travelport.sessionless;

import static com.tgs.services.fms.sources.travelport.sessionless.TravelPortSessionLessConstants.AGENT_USER_NAME;
import static com.tgs.services.fms.sources.travelport.sessionless.TravelPortSessionLessConstants.ORIGIN_APPLICATION;
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.GZIPInputStream;
import javax.xml.ws.BindingProvider;
import org.apache.axis2.databinding.types.PositiveInteger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.stereotype.Service;
import com.google.common.base.Enums;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.CxfRequestLogInterceptor;
import com.tgs.services.base.CxfResponseLogInterceptor;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierCredential;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import com.travelport.www.schema.air_v50_0.FareBasis_type0;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import travelport.www.schema.air_v47_0.AirPriceReq;
import travelport.www.schema.air_v47_0.AirPriceRsp;
import travelport.www.schema.air_v47_0.AirPricingCommand_type0;
import travelport.www.schema.air_v47_0.AirPricingInfo_type0;
import travelport.www.schema.air_v47_0.AirPricingModifiers_type0;
import travelport.www.schema.air_v47_0.AirPricingSolution_type0;
import travelport.www.schema.air_v47_0.AirSegmentPricingModifiers_type0;
import travelport.www.schema.air_v47_0.BaggageAllowance_type0;
import travelport.www.schema.air_v47_0.BookingCode_type0;
import travelport.www.schema.air_v47_0.Code_type5;
import travelport.www.schema.air_v47_0.CodeshareInfo_type0;
import travelport.www.schema.air_v47_0.Connection_type0;
import travelport.www.schema.air_v47_0.FareInfo_type0;
import travelport.www.schema.air_v47_0.NumberInParty_type0;
import travelport.www.schema.air_v47_0.PermittedBookingCodes_type0;
import travelport.www.schema.air_v47_0.PromoCode_type0;
import travelport.www.schema.air_v47_0.PromoCodes_type0;
import travelport.www.schema.air_v47_0.SearchTraveler_type0;
import travelport.www.schema.air_v47_0.TypeAvailabilitySource;
import travelport.www.schema.air_v47_0.TypeBaseAirSegment;
import travelport.www.schema.air_v47_0.TypeEquipment;
import travelport.www.schema.air_v47_0.TypeFaresIndicator;
import travelport.www.schema.common_v47_0.AddressName_type1;
import travelport.www.schema.common_v47_0.BaseCoreReq;
import travelport.www.schema.common_v47_0.BaseRsp;
import travelport.www.schema.common_v47_0.BillingPointOfSaleInfo_type0;
import travelport.www.schema.common_v47_0.City_type1;
import travelport.www.schema.common_v47_0.CountryCode_type0;
import travelport.www.schema.common_v47_0.First_type0;
import travelport.www.schema.common_v47_0.First_type2;
import travelport.www.schema.common_v47_0.HostToken_type0;
import travelport.www.schema.common_v47_0.Last_type0;
import travelport.www.schema.common_v47_0.Name_type0;
import travelport.www.schema.common_v47_0.Name_type4;
import travelport.www.schema.common_v47_0.PostalCode_type1;
import travelport.www.schema.common_v47_0.Prefix_type0;
import travelport.www.schema.common_v47_0.Prefix_type1;
import travelport.www.schema.common_v47_0.ResponseMessage_type0;
import travelport.www.schema.common_v47_0.Text_type1;
import travelport.www.schema.common_v47_0.TypeBranchCode;
import travelport.www.schema.common_v47_0.TypeCardMerchantType;
import travelport.www.schema.common_v47_0.TypeCardNumber;
import travelport.www.schema.common_v47_0.TypeCarrier;
import travelport.www.schema.common_v47_0.TypeClassOfService;
import travelport.www.schema.common_v47_0.TypeCreditCardNumber;
import travelport.www.schema.common_v47_0.TypeFlightNumber;
import travelport.www.schema.common_v47_0.TypeGdsRemark;
import travelport.www.schema.common_v47_0.TypeIATACode;
import travelport.www.schema.common_v47_0.TypeLocatorCode;
import travelport.www.schema.common_v47_0.TypeMoney;
import travelport.www.schema.common_v47_0.TypePTC;
import travelport.www.schema.common_v47_0.TypePercentageWithDecimal;
import travelport.www.schema.common_v47_0.TypeProviderCode;
import travelport.www.schema.common_v47_0.TypeProviderLocatorCode;
import travelport.www.schema.common_v47_0.TypeRef;
import travelport.www.schema.common_v47_0.TypeSSRCode;
import travelport.www.schema.common_v47_0.TypeSSRFreeText;
import travelport.www.schema.common_v47_0.TypeSupplierCode;
import travelport.www.schema.common_v47_0.TypeTaxInfo;
import travelport.www.schema.common_v47_0.TypeTravelerLastName;
import travelport.www.schema.common_v47_0.TypeURVersion;
import travelport.www.schema.common_v47_0.Type_type6;
import travelport.www.schema.sharedbooking_v47_0.BookingStartRsp;
import travelport.www.schema.sharedbooking_v47_0.TypeSessionKey;
import travelport.www.schema.universal_v47_0.SupportedVersions;
import travelport.www.schema.universal_v47_0.UniversalRecordRetrieveReq;
import travelport.www.schema.universal_v47_0.UniversalRecordRetrieveRsp;
import travelport.www.service.air_v47_0.AirFaultMessage;
import travelport.www.service.air_v47_0.AirServiceStub;
import travelport.www.service.universal_v47_0.UniversalRecordArchivedFaultMessage;
import travelport.www.service.universal_v47_0.UniversalRecordFaultMessage;
import travelport.www.service.universal_v47_0.UniversalRecordServiceStub;
import travelport.www.soa.common.security.sessioncontext_v1.SessProp_type0;
import travelport.www.soa.common.security.sessioncontext_v1.SessTok_type0;
import travelport.www.soa.common.security.sessioncontext_v1.SessionContext;


@Getter
@Setter
@Slf4j
@Service
@SuperBuilder
abstract class TravelPortSessionLessServiceManager {

	protected Order order;

	protected String toCurrency;

	protected List<SegmentInfo> segmentInfos;

	protected SupplierConfiguration configuration;

	protected AirSearchQuery searchQuery;

	protected String bookingId;

	protected AirSourceConfigurationOutput sourceConfiguration;

	protected Client client;

	protected TravelPortSessionLessBindingService bindingService;

	protected MoneyExchangeCommunicator moneyExchnageComm;

	protected String sessionkey;

	protected LocalDateTime ticketingTimeLimit;

	protected String providerCode;

	protected String universalPnr;

	protected List<String> criticalMessageLogger;

	protected Map<PaxType, String> bookingTravelerRefMap;

	protected String pnr;

	protected String reservationPNR;

	protected BigInteger version;

	protected String currencyCode;

	protected String traceId;

	protected DeliveryInfo deliveryInfo;

	protected User bookingUser;


	protected Map<PaxType, List<String>> airPricingInfoPaxMap;

	protected SoapRequestResponseListner listener;

	protected static final String UNAUTHORIZED_CRED = "Transport error: 401 Error: Unauthorized";

	// protected DateTimeFormatter dateFormat =
	// DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
	public void init() {
		currencyCode = getCurrencyCode();
		toCurrency = AirSourceConstants.getClientCurrencyCode();
		providerCode = TravelPortSessionLessUtils.getProviderCode(null, configuration);
	}

	public String getCurrencyCode() {
		if (StringUtils.isNotEmpty(configuration.getSupplierCredential().getCurrencyCode())) {
			return configuration.getSupplierCredential().getCurrencyCode();
		}
		return ServiceCommunicatorHelper.getClientInfo().getCurrencyCode();
	}

	public SupplierCredential getCredential() {
		return configuration.getSupplierCredential();
	}

	public double getAmountBasedOnCurrency(String price, String fromCurrency, double dividingFactor) {
		Double amount = Double.valueOf(StringUtils.replace(price, fromCurrency, ""));
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount.doubleValue() / dividingFactor;
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency).type(getType())
				.toCurrency(toCurrency).build();
		return moneyExchnageComm.getExchangeValue((amount.doubleValue() / dividingFactor), filter, true);
	}

	public String getType() {
		AirSourceType sourceType = AirSourceType.getAirSourceType(configuration.getSourceId());
		return StringUtils.join(sourceType.name(), "_", configuration.getSupplierCredential().getPcc()).toUpperCase();
	}

	public UniversalRecordRetrieveRsp getUnivRecordRetrieveRS() {
		UniversalRecordServiceStub universalRecordService = bindingService.getUniversalRecordService();
		UniversalRecordRetrieveReq universalRecordRetrieveReq = buildUniversalRequest();
		UniversalRecordRetrieveRsp universalRecordRetrieveRsp = null;
		listener.setType("RecordRetrieve");
		try {
			universalRecordService._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(universalRecordService, "UniversalRecordRetrieve");
			universalRecordRetrieveRsp = universalRecordService.service(universalRecordRetrieveReq,
					getSessionContext(false), getSupportedVersion());
			if (StringUtils.isNotBlank(universalRecordRetrieveRsp.getTraceId())) {
				traceId = universalRecordRetrieveRsp.getTraceId();
			} else {
				traceId = TravelPortSessionLessUtils.keyGenerator();
			}
			version = universalRecordRetrieveRsp.getUniversalRecord().getVersion().getTypeURVersion();
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} catch (UniversalRecordArchivedFaultMessage | UniversalRecordFaultMessage faultMessage) {
			throw new SupplierUnHandledFaultException(faultMessage.getMessage());
		} finally {
			universalRecordService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return universalRecordRetrieveRsp;
	}

	protected SupportedVersions getSupportedVersion() {
		SupportedVersions version = new SupportedVersions();
		return version;
	}


	private UniversalRecordRetrieveReq buildUniversalRequest() {
		UniversalRecordRetrieveReq universalRecordRetrieveReq = new UniversalRecordRetrieveReq();
		buildBaseCoreRequest(universalRecordRetrieveReq);
		universalRecordRetrieveReq.setUniversalRecordLocatorCode(getTypeLocatorCode(universalPnr));
		return universalRecordRetrieveReq;
	}

	protected BillingPointOfSaleInfo_type0 buildBillingPointOfSale() {
		BillingPointOfSaleInfo_type0 billingPointOfSaleInfo = new BillingPointOfSaleInfo_type0();
		billingPointOfSaleInfo.setOriginApplication(ORIGIN_APPLICATION);
		return billingPointOfSaleInfo;
	}

	protected TypeProviderCode getTypeProviderCode(String providerCode2) {
		TypeProviderCode code = new TypeProviderCode();
		code.setTypeProviderCode(providerCode2);
		return code;
	}

	protected void setAccountCodesInReq(List<String> accountCodeList, AirPricingModifiers_type0 airPricingModifiers) {
		if (CollectionUtils.isNotEmpty(accountCodeList)) {
			PromoCodes_type0 accountCodes = new PromoCodes_type0();
			airPricingModifiers.setPromoCodes(accountCodes);
			for (String accountCodeEntry : accountCodeList) {
				PromoCode_type0 accountCode = new PromoCode_type0();
				accountCode.setCode(getCode(accountCodeEntry));
				if (StringUtils.isNotBlank(configuration.getSupplierCredential().getProviderCode())) {
					accountCode
							.setProviderCode(getProviderCode(configuration.getSupplierCredential().getProviderCode()));
				}
				if (configuration.getSupplierCredential().getSupplierCode() != null) {
					accountCode.setSupplierCode(
							getTypeSupplierCode(configuration.getSupplierCredential().getSupplierCode()));
				}
				accountCodes.addPromoCode(accountCode);
			}
			airPricingModifiers.setPromoCodes(accountCodes);
		}
	}

	protected TypeProviderCode getProviderCode(String code) {
		TypeProviderCode providerCode = new TypeProviderCode();
		providerCode.setTypeProviderCode(code);
		return providerCode;
	}


	protected Code_type5 getCode(String accountCodeEntry) {
		Code_type5 code = new Code_type5();
		code.setCode_type5(accountCodeEntry);
		return code;
	}

	protected void buildBaseCoreRequest(BaseCoreReq baseCoreReq) {
		baseCoreReq.setAuthorizedBy(AGENT_USER_NAME);
		baseCoreReq.setTargetBranch(getTypeBranchCode(configuration.getSupplierCredential().getOrganisationCode()));
		baseCoreReq.setTraceId(traceId);
		baseCoreReq.setBillingPointOfSaleInfo(buildBillingPointOfSale());
	}

	protected void setAirlinePnrForSegment(String supplierCode, String supplierLocatorCode) {
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getAirlineCode(false).equals(supplierCode)) {
				segmentInfo.updateAirlinePnr(supplierLocatorCode);
			}
		});
	}

	protected void logCriticalMessage(String message) {
		if (criticalMessageLogger != null && StringUtils.isNotBlank(message)) {
			criticalMessageLogger.add(message);
		}
	}

	protected void buildAirSegmentList(List<TypeBaseAirSegment> typeBaseAirSegmentList, List<SegmentInfo> segmentInfos,
			boolean isBookingClassRequired, boolean isInfantRequired) {
		TypeBaseAirSegment typeBaseAirSegment = null;
		int groupId = 0;
		for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
			SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			Integer nextSegmentLegNum = (segmentIndex + 1 < segmentInfos.size())
					? segmentInfos.get(segmentIndex + 1).getPriceInfo(0).getMiscInfo().getLegNum()
					: 0;
			if (legNum == 0) {
				FareDetail fareDetail = TravelPortSessionLessUtils.getFareDetail(segmentInfo.getPriceInfo(0));
				PriceMiscInfo miscInfo = segmentInfo.getPriceInfo(0).getMiscInfo();
				typeBaseAirSegment = new TypeBaseAirSegment();
				typeBaseAirSegment.setKey(getTypeRef(miscInfo.getSegmentKey()));
				typeBaseAirSegment.setHostTokenRef(
						segmentInfo.getPriceInfoList().get(0).getMiscInfo().getTokenId().split(",")[0]);
				typeBaseAirSegment.setGroup(groupId);
				if (StringUtils.isNotBlank(miscInfo.getAvailablitySource())) {
					typeBaseAirSegment.setAvailabilitySource(getTypeAvailability(miscInfo.getAvailablitySource()));
				}
				if (StringUtils.isNotBlank(miscInfo.getAvailabilityDisplayType())) {
					typeBaseAirSegment.setAvailabilityDisplayType(miscInfo.getAvailabilityDisplayType());
				}
				if (StringUtils.isNotBlank(miscInfo.getPolledAvailabilityOption())) {
					typeBaseAirSegment.setPolledAvailabilityOption(miscInfo.getPolledAvailabilityOption());
				}
				if (StringUtils.isNotBlank(miscInfo.getParticipationLevel())) {
					typeBaseAirSegment.setParticipantLevel(miscInfo.getParticipationLevel());
				}
				if (Objects.nonNull(miscInfo.getLinkavailablity())) {
					typeBaseAirSegment.setLinkAvailability(miscInfo.getLinkavailablity());
				}
				typeBaseAirSegment.setCarrier(getTypeCarrierCode(segmentInfo.getAirlineCode(false)));
				typeBaseAirSegment.setFlightNumber(getTypeFlightNumber(segmentInfo.getFlightNumber()));
				if (isBookingClassRequired)
					typeBaseAirSegment.setClassOfService(getTypeClassOfService(fareDetail.getClassOfBooking()));
				typeBaseAirSegment.setOrigin(getIATACode(segmentInfo.getDepartureAirportCode()));

				// if (searchQuery != null) {
				// typeBaseAirSegment.setNumberInParty(getNumberInParty(
				// AirUtils.getPaxCountFromPaxInfo(searchQuery.getPaxInfo(), isInfantRequired)));
				// } else {
				// typeBaseAirSegment.setNumberInParty(getNumberInParty(
				// AirUtils.getPaxCountFromTravellerInfo(segmentInfo.getTravellerInfo(), isInfantRequired)));
				// }

				typeBaseAirSegment
						.setDepartureTime(TravelPortSessionLessUtils.getStringDateTime(segmentInfo.getDepartTime()));
				if (StringUtils.isNotBlank(providerCode)) {
					typeBaseAirSegment.setProviderCode(getProviderCode(providerCode));
				}
				if (segmentInfo.getOperatedByAirlineInfo() != null) {
					CodeshareInfo_type0 codeshareInfo = new CodeshareInfo_type0();
					codeshareInfo
							.setOperatingCarrier(getTypeCarrierCode(segmentInfo.getOperatedByAirlineInfo().getCode()));
					codeshareInfo.setString(segmentInfo.getOperatedByAirlineInfo().getCode());
					typeBaseAirSegment.setCodeshareInfo(codeshareInfo);
				}

				// typeBaseAirSegment
				// .setCabinClass(TravelPortUAPIUtils.getCabinClass(fareDetail.getCabinClass().getName()));

				typeBaseAirSegment.setSupplierCode(getTypeSupplierCode(segmentInfo.getAirlineCode(false)));
				if (StringUtils.isNotBlank(segmentInfo.getFlightDesignator().getEquipType()))
					typeBaseAirSegment.setEquipment(getEquipType(segmentInfo.getFlightDesignator().getEquipType()));
			}
			typeBaseAirSegment.setDestination(getIATACode(segmentInfo.getArrivalAirportCode()));
			typeBaseAirSegment
					.setArrivalTime(TravelPortSessionLessUtils.getStringDateTime(segmentInfo.getArrivalTime()));
			if (nextSegmentLegNum == 0) {
				if (segmentIndex + 1 < segmentInfos.size()) {
					SegmentInfo nextSegmentInfo = segmentInfos.get(segmentIndex + 1);
					if (nextSegmentInfo.getSegmentNum() != 0) {
						Connection_type0 connection = new Connection_type0();
						typeBaseAirSegment.setConnection(connection);
					} else {
						groupId++;
					}
				}
				typeBaseAirSegmentList.add(typeBaseAirSegment);
			}
		}
	}

	protected TypeEquipment getEquipType(String equipType) {
		TypeEquipment equipmentType = new TypeEquipment();
		equipmentType.setTypeEquipment(equipType);
		return equipmentType;
	}

	protected TypeSupplierCode getTypeSupplierCode(String airlineCode) {
		TypeSupplierCode supplierCode = new TypeSupplierCode();
		supplierCode.setTypeSupplierCode(airlineCode);
		return supplierCode;
	}

	private NumberInParty_type0 getNumberInParty(int paxCountFromPaxInfo) {
		NumberInParty_type0 numberInParty = new NumberInParty_type0();
		PositiveInteger positiveInteger = new PositiveInteger(String.valueOf(paxCountFromPaxInfo), 36);
		numberInParty.setNumberInParty_type0(positiveInteger);
		return numberInParty;
	}


	protected TypeAvailabilitySource getTypeAvailability(String availablitySource) {
		TypeAvailabilitySource avaSource = new TypeAvailabilitySource();
		avaSource.setTypeAvailabilitySource(availablitySource);
		return avaSource;
	}

	protected TypeClassOfService getTypeClassOfService(String classOfBooking) {
		TypeClassOfService service = new TypeClassOfService();
		service.setTypeClassOfService(classOfBooking);
		return service;
	}

	protected void buildAirPricingCommand(List<SegmentInfo> segmentInfoList, AirPriceReq airPriceReq,
			boolean isFareBasisCodeReq) {
		AirPricingCommand_type0 airPricingCommand = new AirPricingCommand_type0();
		for (int segmentIndex = 0; segmentIndex < segmentInfoList.size(); segmentIndex++) {
			SegmentInfo segmentInfo = segmentInfoList.get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			if (legNum == 0) {
				if (StringUtils.isNotEmpty(segmentInfo.getPriceInfoList().get(0).getMiscInfo().getSegmentKey())) {
					AirSegmentPricingModifiers_type0 airSegmentPricingModifiers =
							new AirSegmentPricingModifiers_type0();
					airSegmentPricingModifiers.setAirSegmentRef(
							getTypeRef(segmentInfo.getPriceInfoList().get(0).getMiscInfo().getSegmentKey()));

					/*
					 * Check if fareBasisCode is required in case of change class, or it can be compromise.
					 */
					if (isFareBasisCodeReq)
						airSegmentPricingModifiers
								.setFareBasisCode(segmentInfo.getPriceInfoList().get(0).getFareBasis(PaxType.ADULT));

					PermittedBookingCodes_type0 permittedBookingCodes = new PermittedBookingCodes_type0();

					// Considering the booking code of Adult as of now
					BookingCode_type0 bookingCode = new BookingCode_type0();
					bookingCode.setCode(getTypeClassOfService(
							segmentInfo.getPriceInfoList().get(0).getBookingClass(PaxType.ADULT)));
					permittedBookingCodes.addBookingCode(bookingCode);
					airSegmentPricingModifiers.setPermittedBookingCodes(permittedBookingCodes);
					airPricingCommand.addAirSegmentPricingModifiers(airSegmentPricingModifiers);
				}
			}
		}
		airPriceReq.addAirPricingCommand(airPricingCommand);
	}

	protected AirPricingModifiers_type0 getAirPricingModifiers(List<SegmentInfo> segmentInfos) {
		AirPricingModifiers_type0 airPricingModifiers = new AirPricingModifiers_type0();
		String accountCode = TravelPortSessionLessUtils.getAccountCodeFromTrip(segmentInfos);
		if (accountCode != null) {
			setAccountCodesInReq(Arrays.asList(accountCode), airPricingModifiers);
		}
		airPricingModifiers.setFaresIndicator(TypeFaresIndicator.PublicFaresOnly);
		/*
		 * PermittedCabins_type0 permittedCabins = new PermittedCabins_type0();
		 * travelport.www.schema.common_v47_0.CabinClass_type0 cabinClass = new
		 * travelport.www.schema.common_v47_0.CabinClass_type0();
		 * cabinClass.setType(TravelPortSessionLessUtils.getCabinClass(
		 * segmentInfos.get(0).getPriceInfo(0).getFareDetail(PaxType.ADULT).getCabinClass().getName()));
		 * //permittedCabins.addCabinClass(cabinClass); airPricingModifiers.setPermittedCabins(permittedCabins); //
		 * airPricingModifiers.setInventoryRequestType(value);
		 */
		return airPricingModifiers;
	}

	protected boolean checkAnyErrors(BookingStartRsp baseRsp) {
		AtomicBoolean isAnyError = new AtomicBoolean();
		StringJoiner message = new StringJoiner("");
		if (baseRsp.getBookingStartRsp().getResponseMessage() != null) {
			getList(baseRsp.getBookingStartRsp().getResponseMessage()).forEach(responseMessage -> {
				if (StringUtils.isNotBlank(responseMessage.getType().getValue()) && responseMessage.getType().getValue()
						.toLowerCase().equals(TravelPortSessionLessConstants.ERROR)) {
					message.add(responseMessage.getType().getValue());
					isAnyError.set(true);
				}
				if (StringUtils.isNotBlank(responseMessage.getType().getValue()) && responseMessage.getType().getValue()
						.toLowerCase().equals(TravelPortSessionLessConstants.WARNING)) {
					message.add(responseMessage.getType().getValue());
				}
			});
			if (StringUtils.isNotBlank(message.toString()))
				logCriticalMessage(message.toString());
		}
		if (StringUtils.isNotBlank(message.toString())) {
			logCriticalMessage(message.toString());
		}
		return isAnyError.get();
	}


	public Double getEquivalentTotalfare(AirPricingInfo_type0 airPricingInfo, double dividingFactor) {
		Double totalFare = new Double(0);
		if (StringUtils.isNotBlank(airPricingInfo.getApproximateTotalPrice().toString())) {
			totalFare = getAmountBasedOnCurrency(airPricingInfo.getApproximateTotalPrice().getTypeMoney(),
					airPricingInfo.getApproximateTotalPrice().getTypeMoney().substring(0, 3), dividingFactor);
		} else {
			totalFare = getAmountBasedOnCurrency(airPricingInfo.getTotalPrice().getTypeMoney(),
					airPricingInfo.getBasePrice().getTypeMoney().substring(0, 3), dividingFactor);
		}
		return totalFare;
	}

	public Double getEquivalentTotalfare(AirPricingSolution_type0 airPricingSolution, double dividingFactor) {
		Double totalFare = new Double(0);
		if (StringUtils.isNotBlank(airPricingSolution.getApproximateTotalPrice().getTypeMoney())) {
			totalFare = getAmountBasedOnCurrency(airPricingSolution.getApproximateTotalPrice().getTypeMoney(),
					airPricingSolution.getApproximateTotalPrice().getTypeMoney().substring(0, 3), dividingFactor);
		} else {
			totalFare = getAmountBasedOnCurrency(airPricingSolution.getTotalPrice().getTypeMoney(),
					airPricingSolution.getBasePrice().getTypeMoney().substring(0, 3), dividingFactor);
		}
		return totalFare;
	}

	public Double getEquivalentBaseFare(AirPricingInfo_type0 airPricingInfo, double dividingFactor) {
		Double baseFare = new Double(0);
		if (airPricingInfo.getEquivalentBasePrice() != null
				&& StringUtils.isNotBlank(airPricingInfo.getEquivalentBasePrice().getTypeMoney())) {
			baseFare = getAmountBasedOnCurrency(airPricingInfo.getEquivalentBasePrice().getTypeMoney(),
					airPricingInfo.getEquivalentBasePrice().getTypeMoney().substring(0, 3), dividingFactor);
		} else if (airPricingInfo.getApproximateBasePrice() != null
				&& StringUtils.isNotBlank(airPricingInfo.getApproximateBasePrice().getTypeMoney())) {
			baseFare = getAmountBasedOnCurrency(airPricingInfo.getApproximateBasePrice().getTypeMoney(),
					airPricingInfo.getApproximateBasePrice().getTypeMoney().substring(0, 3), dividingFactor);
		} else if (airPricingInfo.getBasePrice() != null) {
			baseFare = getAmountBasedOnCurrency(airPricingInfo.getBasePrice().getTypeMoney(),
					airPricingInfo.getBasePrice().getTypeMoney().substring(0, 3), dividingFactor);
		}
		return baseFare;
	}

	public Double getEquivalentFees(AirPricingInfo_type0 airPricingInfo, double dividingFactor) {
		Double fees = new Double(0);
		if (airPricingInfo.getApproximateFees() != null
				&& StringUtils.isNotBlank(airPricingInfo.getApproximateFees().getTypeMoney())) {
			fees = getAmountBasedOnCurrency(airPricingInfo.getApproximateFees().getTypeMoney(),
					airPricingInfo.getApproximateFees().getTypeMoney().substring(0, 3), dividingFactor);
		} else if (airPricingInfo.getFees() != null) {
			fees = getAmountBasedOnCurrency(airPricingInfo.getFees().getTypeMoney(),
					airPricingInfo.getFees().getTypeMoney().substring(0, 3), dividingFactor);
		}
		return fees;
	}

	public void setTaxDetails(FareDetail fareDetail, List<TypeTaxInfo> taxInfoList, double dividingFactor) {
		if (CollectionUtils.isNotEmpty(taxInfoList)) {
			Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
			Map<String, GDSFareComponentMapper> gdsFares = GDSFareComponentMapper.getGdsFareComponents();
			taxInfoList.forEach(typeTaxInfo -> {
				Double amount = Double.valueOf(getAmountBasedOnCurrency(typeTaxInfo.getAmount().toString(),
						typeTaxInfo.getAmount().getTypeMoney().substring(0, 3), dividingFactor));
				String gdsComponent = getMappedGdsComponent(gdsFares.keySet(), typeTaxInfo.getCarrierDefinedCategory());
				if (StringUtils.isNotBlank(gdsComponent)) {
					FareComponent fareComponent = gdsFares.get(gdsComponent).getFareComponent();
					Double previousAmount = fareComponents.getOrDefault(fareComponent, 0.0);
					fareComponents.put(fareComponent, previousAmount + amount);
				} else {
					FareComponent component = FareComponent.OC;
					if (typeTaxInfo.getCarrierDefinedCategory() != null) {
						component = Enums.getIfPresent(FareComponent.class, typeTaxInfo.getCarrierDefinedCategory())
								.or(FareComponent.OC);
					}
					Double previousAmount = fareComponents.getOrDefault(component, 0.0);
					fareComponents.put(component, previousAmount + amount);
				}
			});
		}
	}

	private String getMappedGdsComponent(Set<String> set, String element) {
		if (StringUtils.isNotBlank(element) && CollectionUtils.isNotEmpty(set)) {
			return set.stream().filter(item -> element.contains(item)).findFirst().orElse(null);
		}
		return null;
	}

	public void setFareRuleKeys(PriceMiscInfo miscInfo, FareInfo_type0 fareInfo) {
		if (Objects.nonNull(fareInfo.getFareRuleKey())) {
			if (StringUtils.isNotBlank(fareInfo.getFareRuleKey().getFareInfoRef())) {
				miscInfo.setFareRuleInfoRef(fareInfo.getFareRuleKey().getFareInfoRef());
				miscInfo.setFareRuleInfoValue(fareInfo.getFareRuleKey().getTypeNonBlanks());
			}
		}
	}

	protected void setFareIdentifier(FareInfo_type0 fareInfo, PriceInfo priceInfo1, String bookingCode) {
		TravelPortSessionLessUtils.setFareType(priceInfo1, bookingCode, fareInfo);
		if (BooleanUtils.isTrue(fareInfo.getPromotionalFare())
				&& CollectionUtils.isNotEmpty(configuration.getSupplierAdditionalInfo().getAccountCodes())) {
			priceInfo1.getMiscInfo().setIsPromotionalFare(fareInfo.getPromotionalFare());
			priceInfo1.getMiscInfo().setAccountCode(configuration.getSupplierAdditionalInfo().getAccountCodes().get(0));
		}
	}

	protected static <T> List<T> getList(T[] array) {
		if (array != null && ArrayUtils.isNotEmpty(array)) {
			List<T> list = new ArrayList<T>(Arrays.asList(array));
			return list;
		}
		return new ArrayList<>();
	}

	protected TypePTC getTypePTC(String type) {
		TypePTC code = new TypePTC();
		code.setTypePTC(type);
		return code;
	}

	protected TypeIATACode getIATACode(String code) {
		TypeIATACode airportCode = new TypeIATACode();
		airportCode.setTypeIATACode(code);
		return airportCode;
	}

	protected TypeFlightNumber getTypeFlightNumber(String flightNumber) {
		TypeFlightNumber flight = new TypeFlightNumber();
		flight.setTypeFlightNumber(flightNumber);
		return flight;
	}

	protected TypeCarrier getTypeCarrierCode(String airlineCode) {
		TypeCarrier carrierCode = new TypeCarrier();
		carrierCode.setTypeCarrier(airlineCode);
		return carrierCode;
	}

	protected TypeRef getTypeRef(String value) {
		TypeRef ref = new TypeRef();
		ref.setTypeRef(value);
		return ref;
	}

	protected TypeBranchCode getTypeBranchCode(String pcc) {
		TypeBranchCode branch = new TypeBranchCode();
		branch.setTypeBranchCode(pcc);
		return branch;
	}

	protected TypeSessionKey getTypeSessionKey(String sessionId) {
		TypeSessionKey sessionKey = new TypeSessionKey();
		sessionKey.setTypeRef(sessionId);
		return sessionKey;
	}

	protected TypeProviderLocatorCode getTypeProviderLocatorCode(String pnr) {
		TypeProviderLocatorCode locatorCode = new TypeProviderLocatorCode();
		locatorCode.setTypeProviderLocatorCode(pnr);
		return locatorCode;
	}

	protected TypeLocatorCode getTypeLocatorCode(String pnr) {
		TypeLocatorCode locatorCode = new TypeLocatorCode();
		locatorCode.setTypeLocatorCode(pnr);
		return locatorCode;
	}

	protected City_type1 getCityType(String city) {
		City_type1 cityType = new City_type1();
		cityType.setCity_type0(city);
		return cityType;
	}

	protected Type_type6 getType_Type6(String creditCardMode) {
		Type_type6 type = new Type_type6();
		type.setType_type6(creditCardMode);
		return type;
	}

	protected PostalCode_type1 getPostalCode(String postalCode) {
		PostalCode_type1 postal = new PostalCode_type1();
		postal.setPostalCode_type0(postalCode);
		return postal;
	}

	protected AddressName_type1 getAddressName(String address1) {
		AddressName_type1 address = new AddressName_type1();
		address.setAddressName_type0(address1);
		return address;
	}

	protected CountryCode_type0 getCountryType(String nationality) {
		CountryCode_type0 country = new CountryCode_type0();
		country.setCountryCode_type0(nationality);
		return country;
	}

	protected TypeURVersion getTypeURversion(BigInteger version) {
		TypeURVersion ver = new TypeURVersion();
		ver.setTypeURVersion(version);
		return ver;
	}

	protected TypePercentageWithDecimal getTypePercWithDec(String iataCommission) {
		TypePercentageWithDecimal per = new TypePercentageWithDecimal();
		per.setTypePercentageWithDecimal(iataCommission);
		return per;
	}

	protected TypeCardMerchantType getTypeMerchant(String cardType) {
		TypeCardMerchantType card = new TypeCardMerchantType();
		card.setTypeCardMerchantType(cardType);
		return card;
	}

	protected Name_type0 getNameType(String cardHolderName) {
		Name_type0 name = new Name_type0();
		name.setName_type0(cardHolderName);
		return name;
	}

	protected TypeCreditCardNumber getTypeCreditCard(String cardNumber) {
		TypeCreditCardNumber card = new TypeCreditCardNumber();
		card.setTypeCreditCardNumber(cardNumber);
		return card;
	}

	protected TypeGdsRemark getTypeGDS(String remarks) {
		TypeGdsRemark remark = new TypeGdsRemark();
		remark.setTypeGdsRemark(remarks);
		return remark;
	}

	protected TypeCardNumber getTypeCardNumber(String card) {
		TypeCardNumber cardNumber = new TypeCardNumber();
		cardNumber.setTypeCardNumber(card);
		return cardNumber;
	}

	protected TypeMoney getTypeMoney(String totalAirlineFare) {
		TypeMoney money = new TypeMoney();
		money.setTypeMoney(totalAirlineFare);
		return money;
	}

	protected TypeSSRFreeText getTypeSSrFreeText(String text) {
		TypeSSRFreeText freeText = new TypeSSRFreeText();
		freeText.setTypeSSRFreeText(text);
		return freeText;
	}

	protected TypeSSRCode getTypeSSrCode(String code) {
		TypeSSRCode ssrCode = new TypeSSRCode();
		ssrCode.setTypeSSRCode(code);
		return ssrCode;
	}

	protected Prefix_type1 getPrefix(String paxPrefix) {
		Prefix_type1 prefix = new Prefix_type1();
		prefix.setPrefix_type1(paxPrefix);
		return prefix;
	}

	protected TypeTravelerLastName getTravellerLastname(String lastName) {
		TypeTravelerLastName name = new TypeTravelerLastName();
		name.setTypeTravelerLastName(lastName);
		return name;
	}

	protected First_type2 getFirst_type2(String firstName) {
		First_type2 first = new First_type2();
		first.setFirst_type2(firstName);
		return first;
	}

	protected Text_type1 getText_Type1(String osiText) {
		Text_type1 text = new Text_type1();
		text.setText_type1(osiText);
		return text;
	}

	protected boolean checkAnyErrors(BaseRsp baseRsp) {
		boolean isError = false;
		ResponseMessage_type0[] responseMessgaes = baseRsp.getResponseMessage();
		if (ArrayUtils.isNotEmpty(responseMessgaes)) {
			for (ResponseMessage_type0 message : responseMessgaes)
				if (isError(message, "ERROR")) {
					isError = true;
					logCriticalMessage(message.getString());
				} else if (isWarning(message, "WARNING")) {
					logCriticalMessage(message.getString());
				}
		}
		return isError;
	}


	protected boolean isError(ResponseMessage_type0 responseMessgae, String error) {
		return responseMessgae != null && error.equalsIgnoreCase(responseMessgae.getType().getValue().toUpperCase());
	}

	protected boolean isWarning(ResponseMessage_type0 responseMessgae, String warning) {
		return responseMessgae != null && warning.equalsIgnoreCase(responseMessgae.getType().getValue().toUpperCase());
	}

	protected SessionContext getSessionContext(boolean isRequired) {
		SessionContext session = new SessionContext();
		SessTok_type0 sessTok_type0 = new SessTok_type0();
		SessProp_type0[] props = new SessProp_type0[2];
		props[0] = new SessProp_type0();
		props[0].setNm("Username");
		props[0].setVal(configuration.getSupplierCredential().getUserName());
		props[1] = new SessProp_type0();
		props[1].setNm("Password");
		props[1].setVal(configuration.getSupplierCredential().getPassword());
		sessTok_type0.setId(sessionkey);
		if (isRequired) {
			session.setSessProp(props);
			session.setSessTok(sessTok_type0);
		} else {
			session.setSessProp(null);
			session.setSessTok(null);
		}
		return session;
	}

	protected com.travelport.www.soa.common.security.sessioncontext_v1.SessionContext getSessionContxt(
			boolean isRequired) {
		com.travelport.www.soa.common.security.sessioncontext_v1.SessionContext session =
				new com.travelport.www.soa.common.security.sessioncontext_v1.SessionContext();
		com.travelport.www.soa.common.security.sessioncontext_v1.SessTok_type0 sessTok_type0 =
				new com.travelport.www.soa.common.security.sessioncontext_v1.SessTok_type0();
		com.travelport.www.soa.common.security.sessioncontext_v1.SessProp_type0[] props =
				new com.travelport.www.soa.common.security.sessioncontext_v1.SessProp_type0[2];
		props[0] = new com.travelport.www.soa.common.security.sessioncontext_v1.SessProp_type0();
		props[0].setNm("Username");
		props[0].setVal(configuration.getSupplierCredential().getUserName());
		props[1] = new com.travelport.www.soa.common.security.sessioncontext_v1.SessProp_type0();
		props[1].setNm("Password");
		props[1].setVal(configuration.getSupplierCredential().getPassword());
		if (isRequired) {
			session.setSessProp(props);
			if (StringUtils.isNotBlank(sessionkey)) {
				sessTok_type0.setId(sessionkey);
				session.setSessTok(sessTok_type0);
			}
		} else {
			session.setSessProp(null);
			session.setSessTok(null);
		}
		return session;
	}

	protected LocalDate getDob(LocalDate dob, String age) {
		if (dob == null) {
			return LocalDateTime.now().minusYears(NumberUtils.toLong(age)).toLocalDate();
		}
		return dob;
	}

	protected AirPriceRsp getAirPriceResponse(AirPriceReq airPriceReq, String listenerKey) throws Exception {
		AirServiceStub airService = bindingService.getAirService();
		listener.setType(AirUtils.getLogType(listenerKey, configuration));
		airService._getServiceClient().getAxisService().addMessageContextListener(listener);
		AirPriceRsp airPriceRsp = null;
		try {
			bindingService.setProxyAndAuthentication(airService, "AirPrice");
			airPriceRsp = airService.service(airPriceReq, getSessionContext(false));
			if (checkAnyErrors(airPriceRsp)) {
				throw new NoSeatAvailableException(String.join(",", criticalMessageLogger));
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} catch (AirFaultMessage e) {
			throw e;
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return airPriceRsp;
	}

	public static void setBaggageAllowance(FareDetail fare, FareInfo_type0 fareInfo) {
		if (!Objects.isNull(fareInfo.getBaggageAllowance())) {
			BaggageInfo baggageInfo = new BaggageInfo();
			baggageInfo.setAllowance(getBaggageAllowance(fareInfo.getBaggageAllowance()));
			fare.setBaggageInfo(baggageInfo);
		}
	}

	public static String getBaggageAllowance(BaggageAllowance_type0 baggageAllowance) {
		if (baggageAllowance.getMaxWeight() != null && baggageAllowance.getMaxWeight().getUnit() != null) {
			return baggageAllowance.getMaxWeight().getValue() + " "
					+ baggageAllowance.getMaxWeight().getUnit().getValue();
		} else if (baggageAllowance.getNumberOfPieces() != null) {
			return baggageAllowance.getNumberOfPieces() + " " + " Piece";
		}
		return StringUtils.EMPTY;
	}

	public static AirlineInfo getOperatingCarrier(CodeshareInfo_type0 codeshareInfo) {
		if (codeshareInfo != null && codeshareInfo.getOperatingCarrier() != null
				&& StringUtils.isNotBlank(codeshareInfo.getOperatingCarrier().getTypeCarrier())) {
			return AirlineHelper.getAirlineInfo(codeshareInfo.getOperatingCarrier().getTypeCarrier());
		}
		return null;
	}

	protected boolean checkAnyErrors(com.travelport.www.schema.common_v50_0.BaseRsp baseRsp) {
		boolean isError = false;
		com.travelport.www.schema.common_v50_0.ResponseMessage_type0[] responseMessgaes = baseRsp.getResponseMessage();
		if (ArrayUtils.isNotEmpty(responseMessgaes)) {
			for (com.travelport.www.schema.common_v50_0.ResponseMessage_type0 message : responseMessgaes)
				if (isError(message, "ERROR")) {
					isError = true;
					logCriticalMessage(message.getString());
				} else if (isWarning(message, "WARNING")) {
					// logCriticalMessage(message.getString());
				}
		}
		return isError;
	}

	protected boolean isWarning(com.travelport.www.schema.common_v50_0.ResponseMessage_type0 message, String warning) {
		return message != null && warning.equalsIgnoreCase(message.getType().getValue().toUpperCase());
	}

	protected boolean isError(com.travelport.www.schema.common_v50_0.ResponseMessage_type0 message, String error) {
		return message != null && error.equalsIgnoreCase(message.getType().getValue().toUpperCase());
	}

	protected void buildBaseCoreRequest_V50(com.travelport.www.schema.common_v50_0.BaseCoreReq baseCoreReq) {
		baseCoreReq.setAuthorizedBy(AGENT_USER_NAME);
		baseCoreReq.setTargetBranch(getTypeBranchCode_V50(configuration.getSupplierCredential().getOrganisationCode()));
		baseCoreReq.setTraceId(traceId);
		baseCoreReq.setBillingPointOfSaleInfo(buildBillingPointOfSale_V50());
	}

	private com.travelport.www.schema.common_v50_0.BillingPointOfSaleInfo_type0 buildBillingPointOfSale_V50() {
		com.travelport.www.schema.common_v50_0.BillingPointOfSaleInfo_type0 billingPointOfSaleInfo =
				new com.travelport.www.schema.common_v50_0.BillingPointOfSaleInfo_type0();
		billingPointOfSaleInfo.setOriginApplication(ORIGIN_APPLICATION);
		return billingPointOfSaleInfo;
	}

	private com.travelport.www.schema.common_v50_0.TypeBranchCode getTypeBranchCode_V50(String organisationCode) {
		com.travelport.www.schema.common_v50_0.TypeBranchCode branch =
				new com.travelport.www.schema.common_v50_0.TypeBranchCode();
		branch.setTypeBranchCode(organisationCode);
		return branch;
	}

	protected com.travelport.www.schema.common_v50_0.TypeProviderCode getTypeProviderCode_V50(String providerCode2) {
		com.travelport.www.schema.common_v50_0.TypeProviderCode code =
				new com.travelport.www.schema.common_v50_0.TypeProviderCode();
		code.setTypeProviderCode(providerCode2);
		return code;
	}

	protected com.travelport.www.schema.common_v50_0.TypeIATACode getTypeIataCode(String iataCode) {
		com.travelport.www.schema.common_v50_0.TypeIATACode typeIataCode =
				new com.travelport.www.schema.common_v50_0.TypeIATACode();
		typeIataCode.setTypeIATACode(iataCode);
		return typeIataCode;
	}

	protected com.travelport.www.schema.common_v50_0.TypePTC getTypePTC_V50(String ptc) {
		com.travelport.www.schema.common_v50_0.TypePTC type = new com.travelport.www.schema.common_v50_0.TypePTC();
		type.setTypePTC(ptc);
		return type;
	}

	protected com.travelport.www.schema.common_v50_0.TypeCarrier getTypeCarrierCode_V50(String airlineCode) {
		com.travelport.www.schema.common_v50_0.TypeCarrier carrierCode =
				new com.travelport.www.schema.common_v50_0.TypeCarrier();
		carrierCode.setTypeCarrier(airlineCode);
		return carrierCode;
	}

	protected FareBasis_type0 getFareBasisCode_V50(String fareBasis) {
		FareBasis_type0 fareBasisType = new FareBasis_type0();
		fareBasisType.setCode(fareBasis);
		return fareBasisType;
	}

	public String getResponseByRequest(String requestXML, String methodName, String soapAction, String requestURL)
			throws Exception {
		String sbStr = null;
		int numTry = 0;
		URLConnection urlCon = null;
		boolean doRetry = false;
		if (StringUtils.isNotEmpty(requestXML)) {
			do {
				try {
					URL url = new URL(requestURL);
					urlCon = url.openConnection();
					urlCon.setDoInput(true);
					urlCon.setDoOutput(true);
					urlCon.setUseCaches(false);
					urlCon.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
					urlCon.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1");
					urlCon.setRequestProperty("Accept-Encoding", "gzip,deflate");
					urlCon.setRequestProperty("SOAPAction", soapAction);
					urlCon.setRequestProperty("Host", "minirules.itq.in");
					urlCon.setRequestProperty("Securitykey", configuration.getSupplierCredential().getPassword());
					urlCon.setRequestProperty("Username", configuration.getSupplierCredential().getUserName());
					urlCon.setRequestProperty("Branchcode",
							configuration.getSupplierCredential().getOrganisationCode());
					HttpURLConnection httpUrlCon = (HttpURLConnection) url.openConnection();
					httpUrlCon.setRequestMethod(HttpUtils.REQ_METHOD_POST);

					DataOutputStream printout = new DataOutputStream(urlCon.getOutputStream());
					printout.writeBytes(requestXML);
					printout.flush();
					printout.close();

					InputStream rawInStream = urlCon.getInputStream();
					InputStream dataInput = null;
					String encoding = urlCon.getContentEncoding();
					if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
						dataInput = new GZIPInputStream(new BufferedInputStream(rawInStream));
					} else {
						dataInput = new BufferedInputStream(rawInStream);
					}
					StringBuffer sb = new StringBuffer();
					int rc;
					while ((rc = dataInput.read()) != -1) {
						sb.append((char) rc);
					}
					dataInput.close();
					sbStr = sb.toString();
					sbStr = sbStr.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&#xD;", "");
				} catch (IOException e) {
					log.error("Not able to connect for methodName {}", methodName);
					doRetry = false;
				} catch (Exception e) {
					log.error("Exception Occured while trying  methodName {}", methodName, e);
					doRetry = false;
				} finally {

				}
			} while (numTry++ < 3 && doRetry);
		}
		return sbStr;
	}


	public void bindInterceptors(Object port, String type, String key, String endPointAction) {
		bindInterceptors(port, type, key, endPointAction, false);
	}

	public void bindInterceptors(Object port, String type, String key, String endPointAction, boolean logVisibility) {
		String endpointURL =
				StringUtils.isBlank(endPointAction) ? configuration.getSupplierCredential().getUrl() : endPointAction;
		BindingProvider provider = (BindingProvider) port;
		provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);
		provider.getRequestContext().put(Message.PROTOCOL_HEADERS, addUserHeaders());
		client = ClientProxy.getClient(port);

		HTTPConduit http = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		http.setClient(httpClientPolicy);

		List<String> visibilityGroups = logVisibility ? AirSupplierUtils.getLogVisibility() : new ArrayList<>();

		client.getInInterceptors().add(new CxfResponseLogInterceptor(type, key, true, visibilityGroups));
		client.getOutInterceptors().add(new CxfRequestLogInterceptor(type, key, true, visibilityGroups));
	}


	private Map<String, String> addUserHeaders() {
		SupplierCredential credential = configuration.getSupplierCredential();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Securitykey", credential.getPassword());
		headers.put("Username", credential.getUserName());
		headers.put("Branchcode", credential.getOrganisationCode());
		return headers;
	}

	protected List<HostToken_type0> getHostTokenList(List<SegmentInfo> segmentInfos) {
		List<HostToken_type0> hostList = new ArrayList<HostToken_type0>();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getPriceInfoList().get(0).getMiscInfo().getLegNum() == 0) {
				HostToken_type0 hostToken = new HostToken_type0();
				String[] token = segmentInfo.getPriceInfoList().get(0).getMiscInfo().getTokenId().split(",");
				hostToken.setKey(getTypeRef(token[0]));
				hostToken.setString(token[1]);
				hostList.add(hostToken);
			}
		});
		return hostList;
	}

	protected SearchTraveler_type0 getSearchPassenger(String key, PaxType paxType, String prefix, String firstName,
			String lastName) {
		SearchTraveler_type0 passenger = new SearchTraveler_type0();
		passenger.setCode(getTypePTC(TravelPortSessionLessUtils.getTypePTC(paxType)));
		passenger.setKey(getTypeRef(key));
		passenger.setBookingTravelerRef(key);
		passenger.setName(getNameType(prefix, firstName, lastName));
		return passenger;
	}

	protected Name_type4 getNameType(String prefix, String firstName, String lastName) {
		Name_type4 name = new Name_type4();
		name.setPrefix(getPrefixType0(prefix));
		name.setFirst(getFirst_type0(firstName));
		name.setLast(getLastName(lastName));
		return name;
	}

	protected Prefix_type0 getPrefixType0(String pref) {
		Prefix_type0 prefix = new Prefix_type0();
		prefix.setPrefix_type0(pref);
		return prefix;
	}

	protected Last_type0 getLastName(String lastName) {
		Last_type0 name = new Last_type0();
		name.setLast_type0(lastName);
		return name;
	}

	protected First_type0 getFirst_type0(String firstName) {
		First_type0 first = new First_type0();
		first.setFirst_type0(firstName);
		return first;
	}

	protected boolean checkCriticalWarnings(BaseRsp baseRsp) {
		ResponseMessage_type0[] responseMessgaes = baseRsp.getResponseMessage();
		if (ArrayUtils.isNotEmpty(responseMessgaes)) {
			for (ResponseMessage_type0 message : responseMessgaes)
				if (isWarning(message, "WARNING") && TravelPortSessionLessConstants.CRITICAL_WARNING_CODES
						.contains(message.getCode().toString())) {
					return true;
				}
		}
		return false;
	}

}
