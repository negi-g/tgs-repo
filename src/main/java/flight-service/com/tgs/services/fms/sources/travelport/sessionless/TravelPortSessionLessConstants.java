package com.tgs.services.fms.sources.travelport.sessionless;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tgs.services.base.enums.FareComponent;

final class TravelPortSessionLessConstants {

	public static final String SESSIONED = "Sessioned";

	public static final String TERMINAL_COMMAND = "RULA/JNTAX|R.P|ER";

	public static final String TICKET_DATE = "T*";

	public static final String TYPE = "ACTIVE";

	public static final String END_SESSION_COMMAND = "End";

	public static final String IGNORE_SESSION_COMMAND = "Ignore";

	public static final String ADULT = "ADT";

	public static final String CHILD = "CHD";

	public static final String INFANT = "INF";

	public static final String ORIGIN_APPLICATION = "UAPI";

	public static final String TRACE_ID = "trace";

	public static final String AIR_SERVICE = "AirService";

	public static final String UNIVERSAL_RECORD_SERVICE = "UniversalRecordService";

	public static final String SHARED_BOOKING_SERVICE = "SharedBookingService";

	public static final String PASSIVE_SERVICE = "PassiveService";

	public static final String UNIVERSAL_RECORD = "UniversalRecord";

	// Bank Settlement Process
	public static final String BSP = "Cash";

	public static final String CREDIT_CARD_MODE = "Credit";

	public static final String AGENCY_PAYMENT = "AgencyPayment";

	public static final String ECONOMY = "Economy";

	public static final String PREMIUM_ECONOMY = "PremiumEconomy";

	public static final String BUSINESS = "Business";

	public static final String FIRST = "First";

	public static final String ERROR = "error";

	public static final String WARNING = "warning";

	public static final String SEGMENT_TYPE = "Tour";

	public static final String PASSIVE_REMARK_TYPE = "Retention";

	public static final String AFTER_DEPARTURE = "After Departure";

	public static final String CHILD_AGE = "08";
	public static final String INFANT_AGE = "01";
	public static final String ADULT_AGE = "30";

	public static final int TKT_NUMBER_LENGTH = 13;

	public static final String DEFAULT = "DEFAULT";

	public static final String AGENCY = "AGENCY";

	public static final String CHLD = "CHLD";

	public static final String HK = "HK";

	// SSR key only for child
	public static final String CHD_SSR_KEY = "1B";

	public static final Map<String, String> SSRINFO = new HashMap<String, String>() {
		{
			put("SPML", "SPECIAL MEAL");
			put("AVIH", "ANIMAL IN HOLD");
			put("BULK", "BULKY BAGGAGE");
			put("CBBG", "CABIN BAGGAGE");
			put("DPNA", "DISABLE PAX NEED ASSIST INTELLECT/DEVELOP");
			put("EPAY", "GUARANTEED ELECTRONIC PAYMENT");
			put("EXST", "EXTRA SEAT");
			put("FRAG", "FRAGILE BAGGAGE");
			put("GPST", "GROUP SEAT");
			put("GRPF", "GROUP FARE");
			put("LANG", "LANGUAGE ASSISTANCE");
			put("MAAS", "MEET AND ASSIST");
			put("PETC", "PET IN CABIN");
			put("SEMN", "SHIPS CREW");
			put("SPEQ", "SPORTS EQUIPMENT");
			put("TKNM", "MANUALLY ENTER TICKET NUMBER");
			put("UMNR", "UNACCOMPANIED MINOR");
			put("WEAP", "WEAPON FIREARM OR AMMUNITION AS CHKD BAG");
			put("XBAG", "EXCESS BAGGAGE");
		}
	};


	public static HashMap<String, String> FARERULE_CATEGORY = new HashMap<String, String>() {
		{
			put("16", "CHANGES/CANCELLATIONS");
		}
	};

	public static final String AGENT_USER_NAME = "user";

	public static final String UAPI = "uAPI";

	public static final List<String> AIRLINES_REQUIRE_CHILD_SSR = Arrays.asList("LH");

	public static final String BAGGAGE = "Baggage";

	public static final String MEAL_OR_BEVERAGE = "MealOrBeverage";

	public static final String SEAT = "PreReservedSeatAssignment";

	public static final String SSR_STATUS = "Priced";

	public static final String TRAVEL_DOCUMENTS_INFORMATION = "DOCS";

	public static final String PASSPORT_ACTON_CODE = "HK";

	public static final String TICKETED = "Ticketed";

	public static final String PN = "PN";

	public static final String CHG = "CHG";

	public static final String CANCELLATION_CAT = "Cancellation";

	public static final String NOSHOW_CAT = "NoShowPenalty";

	public static final String CHANGEITIN_CAT = "ChangeItinPenalty";

	public static final String AMOUNT1_CAT = "Amt1";

	public static final String CURRENCY1_CAT = "Currency1";

	public static final String CANCELLATIONREEFUNDS_CAT = "CancellationsRefunds";

	public static final String APPLIES = "X";

	public static final String CAT_16 = "CAT 16";

	public static final String SPECIAL_RETURN_CLASS = "N";

	public static final List<FareComponent> ssrList =
			Arrays.asList(FareComponent.BP, FareComponent.SP, FareComponent.MP);

	// https://support.travelport.com/webhelp/uapi/uAPI.htm#Error_Codes/ACH_Error_Codes.htm
	public static final List<String> CRITICAL_WARNING_CODES = Arrays.asList("15050");
}
