package com.tgs.services.fms.restcontroller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.fms.datamodel.supplier.SupplierConfigurationFilter;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.datamodel.supplier.SupplierRuleFilter;
import com.tgs.services.fms.jparepository.SupplierInfoService;
import com.tgs.services.fms.restmodel.SupplierInfoResponse;
import com.tgs.services.fms.restmodel.SupplierRuleResponse;
import com.tgs.services.fms.servicehandler.SupplierCredentialHandler;
import com.tgs.services.fms.validator.SupplierRuleValidator;
import com.tgs.services.ums.datamodel.AreaRole;


@RequestMapping("/fms/v1/supplier")
@RestController
@CustomRequestProcessor(areaRole = AreaRole.AIR_SUPPLIER, isMidOfficeAllowed = true,
		emulatedAllowedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
public class AirSupplierController {

	@Autowired
	SupplierCredentialHandler supplierHandler;

	@Autowired
	SupplierInfoService supplierInfoService;

	@Autowired
	SupplierRuleValidator ruleValidator;

	@Autowired
	AuditsHandler auditHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(ruleValidator);
	}

	@RequestMapping(value = "/info/save/list", method = RequestMethod.POST)
	protected SupplierInfoResponse saveOrUpdateSupplierInfos(HttpServletRequest request, HttpServletResponse response,
			@RequestBody List<SupplierInfo> supplierInfos) throws Exception {
		SupplierInfoResponse supplierInfoResponse = new SupplierInfoResponse();
		for (SupplierInfo supplierInfo : supplierInfos) {
			supplierInfoResponse.getSupplierInfos()
					.addAll(saveOrUpdateSupplierInfo(request, response, supplierInfo).getSupplierInfos());
		}
		return supplierInfoResponse;
	}

	@RequestMapping(value = "/info/save", method = RequestMethod.POST)
	protected SupplierInfoResponse saveOrUpdateSupplierInfo(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody SupplierInfo supplierInfo) throws Exception {
		supplierHandler.initData(supplierInfo, new SupplierInfoResponse());
		return supplierHandler.getResponse();
	}

	@RequestMapping(value = "/info/list", method = RequestMethod.POST)
	protected SupplierInfoResponse listSupplierInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody SupplierConfigurationFilter configurationFilter) throws Exception {
		return supplierHandler.getSupplierInfo(configurationFilter);
	}

	/*
	 * @RequestMapping(value = "/info/delete/{id}", method = RequestMethod.DELETE) protected BaseResponse
	 * deleteSupplierInfo(HttpServletRequest request, HttpServletResponse response,
	 * 
	 * @PathVariable("id") Long id) throws Exception { return supplierHandler.deleteSupplierInfo(id); }
	 */

	@RequestMapping(value = "/info/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updateInfoStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return supplierHandler.updateStatusSupplierInfo(id, status);
	}

	@RequestMapping(value = "/rule/save", method = RequestMethod.POST)
	protected SupplierRuleResponse saveOrUpdateSupplierRule(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody SupplierRule supplierRule) throws Exception {
		SupplierRuleResponse supplierRuleResponse = new SupplierRuleResponse();
		supplierRuleResponse.getSupplierRules().add(supplierHandler.saveSupplierRule(supplierRule));
		return supplierRuleResponse;
	}

	@RequestMapping(value = "/rule/list", method = RequestMethod.POST)
	protected SupplierRuleResponse listSupplierRule(HttpServletRequest request, HttpServletResponse response,
			@RequestBody SupplierRuleFilter ruleFilter) throws Exception {
		SupplierRuleResponse supplierRuleResponse = new SupplierRuleResponse();
		supplierRuleResponse.getSupplierRules().addAll(supplierHandler.getSupplierRules(ruleFilter));
		return supplierRuleResponse;
	}

	@RequestMapping(value = "/rule/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteSupplierRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return supplierHandler.deleteSupplierRule(id);
	}

	@RequestMapping(value = "/rule/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updateRuleStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return supplierHandler.updateSupplierRule(id, status);
	}

	@RequestMapping(value = "/info/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchSupplierInfoAudits(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(
				auditHandler.fetchAudits(auditsRequestData, com.tgs.services.fms.dbmodel.DbSupplierInfo.class, ""));
		return auditResponse;
	}

	@RequestMapping(value = "/rule/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchSupplierRuleAudits(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(
				auditHandler.fetchAudits(auditsRequestData, com.tgs.services.fms.dbmodel.DbSupplierRule.class, ""));
		return auditResponse;
	}
}
