package com.tgs.services.fms.manager;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.AirRules;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirFareTypeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirFilterConfiguration;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.airconfigurator.ChangeFareTypeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.HoldFeeChargeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.RestrictiveFareTypeOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TripPriceEngine {

	private static final String DELIMILTER = "_";

	private static String RESTRICTIVE_FARE_TYPE_KEY = "RESTRICTIVE_FARE_TYPE_KEY";

	public static void filterAirlinesAndFareTypes(SupplierConfiguration supplierConfiguration,
			AirSearchResult searchResult, AirSearchQuery searchQuery) {
		filterAirlines(supplierConfiguration, searchResult, searchQuery);
	}


	public static void setSpecialReturnIdentifier(SegmentInfo onwardSegment, PriceInfo onwardPriceInfo,
			SegmentInfo returnSegment, PriceInfo returnPriceInfo, TripInfo onwardTrip, TripInfo returnTrip) {
		String onwardIdentifier;
		String returnIdentifier;
		if (StringUtils.isEmpty(onwardPriceInfo.getSpecialReturnIdentifier())) {
			onwardIdentifier = StringUtils.join(onwardSegment.getFlightNumber(), onwardSegment.getAirlineCode(false),
					TgsStringUtils.generateRandomNumber(4, ""));
		} else {
			onwardIdentifier = onwardPriceInfo.getSpecialReturnIdentifier();
		}
		if (StringUtils.isEmpty(returnPriceInfo.getSpecialReturnIdentifier())) {
			returnIdentifier = StringUtils.join(returnSegment.getFlightNumber(), returnSegment.getAirlineCode(false),
					TgsStringUtils.generateRandomNumber(4, ""));
		} else {
			returnIdentifier = returnPriceInfo.getSpecialReturnIdentifier();
		}
		onwardPriceInfo.setSpecialReturnIdentifier(onwardIdentifier);
		onwardPriceInfo.getMatchedSpecialReturnIdentifier().add(returnIdentifier);
		returnPriceInfo.setSpecialReturnIdentifier(returnIdentifier);
		returnPriceInfo.getMatchedSpecialReturnIdentifier().add(onwardIdentifier);
		/**
		 * before uncomment discuss with flight team
		 */
		setIdentifierToSegmentPrices(onwardTrip, onwardIdentifier, onwardPriceInfo.getMatchedSpecialReturnIdentifier());
		setIdentifierToSegmentPrices(returnTrip, returnIdentifier, returnPriceInfo.getMatchedSpecialReturnIdentifier());
	}

	private static void setIdentifierToSegmentPrices(TripInfo tripInfo, String identifier,
			List<String> splIdentifiers) {
		if (CollectionUtils.size(tripInfo.getSegmentInfos()) > 1) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				if (segmentInfo.getSegmentNum() >= 1) {
					segmentInfo.getPriceInfoList().forEach(priceInfo -> {
						if (FareType.SPECIAL_RETURN.getName().equals(priceInfo.getFareIdentifier())) {
							priceInfo.setSpecialReturnIdentifier(identifier);
							priceInfo.setMatchedSpecialReturnIdentifier(splIdentifiers);
						}
					});
				}
			}
		}
	}

	protected static void filterAndMergePriceInfo(TripInfo tripInfo, Map<String, TripInfo> keyMap, String key,
			boolean isMergeRequired) {
		try {
			int priceIndex = 0;
			List<Integer> listOfPriceIndex = new ArrayList<>();
			List<Integer> removePriceIndex = new ArrayList<>();

			/**
			 * Finding listofPriceIndex based on farebasis , if any of farebasis already exist in old segmentInfo then
			 * there is no need to add it again. This typically happen in case of multiple suppliers and it can happen
			 * in domestic return as well.
			 */
			for (PriceInfo priceInfo : tripInfo.getSegmentInfos().get(0).getPriceInfoList()) {
				boolean fareBasisAlreadyExist = false;
				for (PriceInfo oldPriceInfo : keyMap.get(key).getSegmentInfos().get(0).getPriceInfoList()) {
					boolean isSamePriceType = AirUtils.isSamePriceInfo(oldPriceInfo, priceInfo);
					boolean isSameBaseFare = AirUtils.isSameBaseFare(oldPriceInfo, priceInfo);
					boolean isSpecialFare = BooleanUtils.isTrue(oldPriceInfo.isPrivateFare())
							|| BooleanUtils.isTrue(priceInfo.isPrivateFare());
					if (isSpecialFare && priceInfo.getSourceId() == AirSourceType.SPICEJET.getSourceId()
							&& isSameBaseFare && isSamePriceType && !StringUtils
									.equalsIgnoreCase(priceInfo.getAccountCode(), oldPriceInfo.getAccountCode())) {
						/**
						 * @implNote :<br>
						 *           1. Even Base Fare of Current PriceInfo and Account CodePrice Info are same,<br>
						 *           it will get merged to single tripinfo <br>
						 *           2. Those special private fare will get removed or booked as special fare by using
						 *           ShowPublicBookPrivateEngine Engine logic <br>
						 * @see ShowPublicBookPrivateEngine#isUserShowPublicBookPrivate logic
						 */
						continue;
					} else if (isMergeRequired && isSamePriceType && isSameBaseFare) {
						AirSourceConfigurationOutput oldPriceSourceConfig =
								AirUtils.getAirSourceConfigurationFromContextData(oldPriceInfo.getSupplierBasicInfo(),
										SystemContextHolder.getContextData());
						AirSourceConfigurationOutput newPriceSourceConfig =
								AirUtils.getAirSourceConfigurationFromContextData(priceInfo.getSupplierBasicInfo(),
										SystemContextHolder.getContextData());
						/**
						 * In case older priceinfo has less priority then system will remove the oldpriceinfo and will
						 * keep the new priceinfo
						 */
						if (oldPriceSourceConfig.getPricePriority() < newPriceSourceConfig.getPricePriority()) {
							removePriceIndex.add(priceIndex);
						} else {
							// log.info("Merging Price info for type {}", priceInfo.getFareIdentifier());
							fareBasisAlreadyExist = true;
							break;
						}
					}
				}
				if (!fareBasisAlreadyExist) {
					listOfPriceIndex.add(priceIndex);
				}
				priceIndex++;
			}


			/**
			 * Remove oldpriceinfo which has lower priority
			 */
			if (CollectionUtils.isNotEmpty(removePriceIndex)) {
				log.debug("RemovePriceIndex size is {}", removePriceIndex.size());
				for (SegmentInfo segmentInfo : keyMap.get(key).getSegmentInfos()) {
					priceIndex = 0;
					for (Iterator<PriceInfo> iter = segmentInfo.getPriceInfoList().iterator(); iter.hasNext();) {
						iter.next();
						if (removePriceIndex.contains(priceIndex)) {
							iter.remove();
						}
						priceIndex++;
					}
				}
			}


			// Adding those priceIndexes which are new in TripInfo will be clubbed in existing segmentInfo
			int segmentIndex = 0;
			for (SegmentInfo segmentInfo : keyMap.get(key).getSegmentInfos()) {
				for (Integer prIndex : listOfPriceIndex) {
					SegmentInfo sourceSegment = tripInfo.getSegmentInfos().get(segmentIndex);
					if (sourceSegment.getPriceInfoList().size() > prIndex) {
						segmentInfo.getPriceInfoList().add(sourceSegment.getPriceInfo(prIndex));
					}
				}
				segmentIndex++;
			}
		} catch (Exception e) {
			log.error("Error Occured on filtering price for trip {}", tripInfo, e);
		}

	}

	public static void filterComboPriceInfos(TripInfo copyOnwardTrip, TripInfo copyReturnTrip) {
		Set<String> onwardFareTypes = getFareIdentifier(copyOnwardTrip);
		Set<String> returnFareTypes = getFareIdentifier(copyReturnTrip);
		Set<CabinClass> onwardCabinClass = getCabinClass(copyOnwardTrip);
		Set<CabinClass> returnCabinClass = getCabinClass(copyReturnTrip);
		TripPriceEngine.filterBasedOnCabinClass(copyOnwardTrip, returnCabinClass);
		TripPriceEngine.filterBasedOnCabinClass(copyReturnTrip, onwardCabinClass);
		TripPriceEngine.filterBasedOnFareIdentifier(copyOnwardTrip, returnFareTypes);
		TripPriceEngine.filterBasedOnFareIdentifier(copyReturnTrip, onwardFareTypes);
	}

	public static Set<String> getFareIdentifier(TripInfo tripInfo) {
		Set<String> fareIdentifier = new HashSet<>();
		if (tripInfo.isPriceInfosNotEmpty()) {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					fareIdentifier.add(priceInfo.getFareType());
				});
			});
		}
		return fareIdentifier;
	}

	public static void filterBasedOnFareIdentifier(TripInfo tripInfo, Set<String> fareTypes) {
		if (CollectionUtils.isNotEmpty(fareTypes)) {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				List<PriceInfo> filteredPriceInfoList = segmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
					return fareTypes.contains(priceInfo.getFareType());
				}).collect(Collectors.toList());
				segmentInfo.setPriceInfoList(filteredPriceInfoList);
			});
		}
	}

	public static void filterBasedOnCabinClass(TripInfo tripInfo, Set<CabinClass> cabinClasses) {
		if (CollectionUtils.isNotEmpty(cabinClasses)) {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				List<PriceInfo> filteredPriceInfoList = segmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
					return cabinClasses.contains(priceInfo.getCabinClass(PaxType.ADULT));
				}).collect(Collectors.toList());
				segmentInfo.setPriceInfoList(filteredPriceInfoList);
			});
		}
	}

	public static Set<CabinClass> getCabinClass(TripInfo tripInfo) {
		Set<CabinClass> cabinClasses = new HashSet<>();
		if (tripInfo.isPriceInfosNotEmpty()) {
			tripInfo.getSegmentInfos().get(0).getPriceInfoList().forEach(priceInfo -> {
				cabinClasses.add(priceInfo.getCabinClass(PaxType.ADULT));
			});
		}
		return cabinClasses;
	}


	public static void filterTripOnLayOverTime(AirSearchResult searchResult, AirSearchQuery searchQuery,
			AirSourceConfigurationOutput sourceConfigurationOutput) {
		try {
			if (searchQuery.isIntlReturn() && searchResult != null
					&& MapUtils.isNotEmpty(searchResult.getTripInfos())) {
				for (String key : searchResult.getTripInfos().keySet()) {
					List<TripInfo> tripInfos = searchResult.getTripInfos().get(key);
					if (CollectionUtils.isNotEmpty(tripInfos)) {
						for (Iterator<TripInfo> tripIter = tripInfos.iterator(); tripIter.hasNext();) {
							TripInfo tripInfo = tripIter.next();
							tripInfo.setConnectionTime();
							AtomicBoolean isValidTrip = new AtomicBoolean(true);
							tripInfo.getSegmentInfos().forEach(segmentInfo -> {
								// For Now Will fixed more than 6 hours filtered
								if (segmentInfo.getConnectingTime() != null
										&& segmentInfo.getConnectingTime().intValue() > 360) {
									isValidTrip.set(Boolean.FALSE);
								}
							});
							if (!isValidTrip.get()) {
								tripIter.remove();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to filter TripOnLayOverTime for searchQuery {]", searchQuery);
		}
	}

	public static void shiftFareDetailsToFlightTraveller(List<TripInfo> tripInfos,
			List<FlightTravellerInfo> flightTravellerInfos) {
		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				SegmentBookingRelatedInfo bookingRelatedInfo = SegmentBookingRelatedInfo.builder().build();
				segmentInfo.setBookingRelatedInfo(bookingRelatedInfo);
				segmentInfo.getBookingRelatedInfo().setTravellerInfo(new ArrayList<>());
				segmentInfo.getPriceInfo(0).getFareDetails().forEach((paxType, fareDetail) -> {
					List<FlightTravellerInfo> travellerInfos1 =
							GsonUtils.getGson()
									.fromJson(
											GsonUtils.getGson()
													.toJson(AirUtils.getParticularPaxTravellerInfo(flightTravellerInfos,
															paxType)),
											new TypeToken<List<FlightTravellerInfo>>() {}.getType());
					travellerInfos1.forEach(travellerInfo -> {
						travellerInfo.setFareDetail(
								new GsonMapper<>(fareDetail, travellerInfo.getFareDetail(), FareDetail.class)
										.convert());
						segmentInfo.getBookingRelatedInfo().getTravellerInfo().add(travellerInfo);
					});
				});
				// Finally Make Price Info Fare Detail to empty
				segmentInfo.getPriceInfo(0).setFareDetails(null);
			});
		});
	}

	/**
	 * This method will filter the searchResults airlines and FareTypes based on the Supplier rule configuration
	 *
	 * @param supplierConfiguration
	 * @param searchResult
	 * @param searchQuery
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static void filterAirlines(SupplierConfiguration supplierConfiguration, AirSearchResult searchResult,
			AirSearchQuery searchQuery) {
		if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			AirFilterConfiguration airFilterConfig = getAirFilterConfig(supplierConfiguration);
			List<String> allowedAirlines = getAllowedAirlines(searchQuery, supplierConfiguration);
			List<String> disAllowedAirlines = getExcludeAirlines(searchQuery, supplierConfiguration);
			List<String> fareTypes = getDisallowedFareTypes(searchQuery, supplierConfiguration);
			boolean isCustomFareTypeApplicable =
					AirConfiguratorHelper.isRulesPresent(AirConfiguratorRuleType.CHANGE_FARE_TYPE);
			searchResult.getTripInfos().forEach((key, trips) -> {
				for (Iterator<TripInfo> iter = trips.iterator(); iter.hasNext();) {
					TripInfo tripInfo = iter.next();
					boolean isRemoveTrip = false;

					if (isCustomFareTypeApplicable) {
						// Replacing the FareIdentifier with Client defined FareType
						updateCustomFareType(tripInfo, searchQuery);
					}

					if (CollectionUtils.isNotEmpty(allowedAirlines)
							&& !isValidTripInfo(tripInfo, allowedAirlines, false)) {
						// If Not Allowed Airline for Supplier or Source Config
						isRemoveTrip = true;
					}

					if (CollectionUtils.isNotEmpty(disAllowedAirlines) && !isRemoveTrip
							&& isValidTripInfo(tripInfo, disAllowedAirlines, false)) {
						// Excluded Airlines for Supplier or Source Config
						isRemoveTrip = true;
					}

					if (!isRemoveTrip) {
						filterDisallowedFareType(tripInfo, fareTypes);
						removeRestrictiveFareType(tripInfo);
					}

					if (!isRemoveTrip && airFilterConfig != null
							&& BooleanUtils.isFalse((airFilterConfig.isSpecialReturnAllowed()))) {
						filterDisallowedFareType(tripInfo, Arrays.asList(FareType.SPECIAL_RETURN.getName()));
					}

					/**
					 * @implSpec : <br>
					 *           After Filtering DisAllowed FareTypes , there is chance of Only Skeleton of Trip is
					 *           Present but no price Object.
					 * 
					 *           if(isRemoveTrip || !tripInfo.isPriceInfosNotEmpty()) remove trip;
					 * 
					 */
					if (isRemoveTrip || !tripInfo.isPriceInfosNotEmpty()) {
						iter.remove();
					}

				}
				HashSet<String> existingKeys = (HashSet<String>) SystemContextHolder.getContextData().getValueMap()
						.getOrDefault(RESTRICTIVE_FARE_TYPE_KEY, new HashSet<String>());
				for (String existingKey : existingKeys) {
					SystemContextHolder.getContextData().getValueMap().remove(existingKey);
				}
			});
		}
	}

	private static void removeRestrictiveFareType(TripInfo tripInfo) {

		RestrictiveFareTypeOutput fareTypeConfigOutput = getRestrictiveFareTypeRule(tripInfo);

		if (tripInfo.isPriceInfosNotEmpty() && fareTypeConfigOutput != null) {
			Set<Integer> indexToBeRemoved = new HashSet<>();
			tripInfo.getSegmentInfos().forEach(segment -> {
				for (int priceIndex = 0; priceIndex < segment.getPriceInfoList().size(); priceIndex++) {
					PriceInfo priceInfo = segment.getPriceInfoList().get(priceIndex);
					String fareType = priceInfo.getFareType();
					if (CollectionUtils.isNotEmpty(fareTypeConfigOutput.getExcludedFareTypes())
							&& fareTypeConfigOutput.getExcludedFareTypes().contains(fareType)) {
						indexToBeRemoved.add(priceIndex);
					}

					if (CollectionUtils.isNotEmpty(fareTypeConfigOutput.getIncludedFareTypes())
							&& !fareTypeConfigOutput.getIncludedFareTypes().contains(fareType)) {
						indexToBeRemoved.add(priceIndex);
					}
				}
			});
			removePriceInfoUsingIndex(tripInfo, indexToBeRemoved);
		}
	}

	@SuppressWarnings("unchecked")
	private static RestrictiveFareTypeOutput getRestrictiveFareTypeRule(TripInfo tripInfo) {
		String key = StringUtils.join(AirConfiguratorRuleType.RESTRICTIVE_FARE_TYPE.name(),
				tripInfo.getSupplierInfo().getSupplierId(), "_", tripInfo.getPlatingCarrier());
		RestrictiveFareTypeOutput fareTypeConfigOutput =
				(RestrictiveFareTypeOutput) SystemContextHolder.getContextData().getValueMap().get(key);
		if (fareTypeConfigOutput == null) {
			FlightBasicFact flightFact =
					FlightBasicFact.createFact().generateFact(tripInfo, AirUtils.getAirType(tripInfo));
			BaseUtils.createFactOnUser(flightFact, SystemContextHolder.getContextData().getUser());
			AirConfiguratorInfo fareTypeConfig = AirConfiguratorHelper.getAirConfigRuleInfo(flightFact,
					AirConfiguratorRuleType.RESTRICTIVE_FARE_TYPE);
			if (fareTypeConfig != null) {
				SystemContextHolder.getContextData().getValueMap().put(key, fareTypeConfig.getIRuleOutPut());
				HashSet<String> existingKeys = (HashSet<String>) SystemContextHolder.getContextData().getValueMap()
						.getOrDefault(RESTRICTIVE_FARE_TYPE_KEY, new HashSet<>());
				existingKeys.add(key);
				SystemContextHolder.getContextData().getValueMap().put(RESTRICTIVE_FARE_TYPE_KEY, existingKeys);
				return (RestrictiveFareTypeOutput) fareTypeConfig.getIRuleOutPut();
			}
		}
		return fareTypeConfigOutput;
	}

	public static void removePriceInfoUsingIndex(TripInfo tripInfo, Set<Integer> indexToBeRemoved) {
		if (CollectionUtils.isNotEmpty(indexToBeRemoved)) {
			log.debug("Removing price indices {} from trip {}", indexToBeRemoved, tripInfo.getTripKey());
			tripInfo.getSegmentInfos().forEach(segment -> {
				int priceIndex = 0;
				for (Iterator<PriceInfo> iter = segment.getPriceInfoList().iterator(); iter.hasNext();) {
					iter.next();
					if (indexToBeRemoved.contains(priceIndex)) {
						iter.remove();
					}
					priceIndex++;
				}
			});
		}
	}

	private static AirFilterConfiguration getAirFilterConfig(SupplierConfiguration supplierConfiguration) {
		User user = SystemContextHolder.getContextData().getUser();
		FlightBasicFact flightfact = FlightBasicFact.builder().build();

		SupplierBasicInfo basicInfo = supplierConfiguration.getBasicInfo();
		if (basicInfo != null) {
			flightfact.setSourceId(basicInfo.getSourceId());
			flightfact.setSupplierId(basicInfo.getSupplierId());
		}
		if (SystemContextHolder.getContextData().getHttpHeaders() != null) {
			flightfact.setChannelType(SystemContextHolder.getContextData().getHttpHeaders().getChannelType());
		}
		AirFilterConfiguration airFilterConfig = AirConfiguratorHelper
				.getAirConfigRule(BaseUtils.createFactOnUser(flightfact, user), AirConfiguratorRuleType.FILTER);
		return airFilterConfig;
	}


	private static List<String> getDisallowedFareTypes(AirSearchQuery searchQuery,
			SupplierConfiguration supplierConfiguration) {
		List<String> fareTypes = new ArrayList<>();
		if (MapUtils.isNotEmpty(SystemContextHolder.getContextData().getValueMap())) {
			AirSourceConfigurationOutput airSourceOutput = (AirSourceConfigurationOutput) SystemContextHolder
					.getContextData().getValueMap().get(supplierConfiguration.getSupplierId());
			if ((airSourceOutput != null) && CollectionUtils.isNotEmpty(airSourceOutput.getDisAllowedFareTypes())) {
				fareTypes.addAll(airSourceOutput.getDisAllowedFareTypes());
			}
		}
		return fareTypes;
	}

	private static List<String> getAllowedAirlines(AirSearchQuery searchQuery,
			SupplierConfiguration supplierConfiguration) {
		List<String> airlines = new ArrayList<>();
		ContextData contextData = SystemContextHolder.getContextData();
		if (supplierConfiguration != null && supplierConfiguration.getSupplierAdditionalInfo() != null
				&& CollectionUtils.isNotEmpty(supplierConfiguration.getSupplierAdditionalInfo().getAllowedAirlines())) {
			airlines.addAll(supplierConfiguration.getSupplierAdditionalInfo().getAllowedAirlines());
		} else {
			AirSourceConfigurationOutput airSourceOutput =
					(AirSourceConfigurationOutput) contextData.getValueMap().get(supplierConfiguration.getSupplierId());
			if ((airSourceOutput != null) && CollectionUtils.isNotEmpty(airSourceOutput.getAllowedAirlines())) {
				airlines.addAll(airSourceOutput.getAllowedAirlines());
			}

		}
		return airlines;
	}

	private static List<String> getExcludeAirlines(AirSearchQuery searchQuery,
			SupplierConfiguration supplierConfiguration) {
		List<String> airlines = new ArrayList<>();
		ContextData contextData = SystemContextHolder.getContextData();
		if (supplierConfiguration != null && supplierConfiguration.getSupplierAdditionalInfo() != null
				&& CollectionUtils
						.isNotEmpty(supplierConfiguration.getSupplierAdditionalInfo().getExcludedAirlines())) {
			airlines.addAll(supplierConfiguration.getSupplierAdditionalInfo().getExcludedAirlines());
		} else {
			AirSourceConfigurationOutput airSourceOutput =
					(AirSourceConfigurationOutput) contextData.getValueMap().get(supplierConfiguration.getSupplierId());
			if ((airSourceOutput != null) && CollectionUtils.isNotEmpty(airSourceOutput.getExcludedAirlines())) {
				airlines.addAll(airSourceOutput.getExcludedAirlines());
			}
		}
		return airlines;

	}

	/**
	 * This method will check whether the trip is valid, as per the allowed airline List
	 * 
	 * @param tripInfo
	 * @param allowedAirlines
	 * @param isOperatingAirline
	 * @return
	 */

	private static boolean isValidTripInfo(TripInfo tripInfo, List<String> allowedAirlines,
			boolean isOperatingAirline) {
		boolean isValid = true;
		if (CollectionUtils.isNotEmpty(allowedAirlines)) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				if (!allowedAirlines.contains(segmentInfo.getPlatingCarrier(null))) {
					isValid = false;
					break;
				}
			}
		}
		return isValid;
	}

	private static void filterDisallowedFareType(TripInfo tripInfo, List<String> fareTypes) {
		if (CollectionUtils.isNotEmpty(fareTypes)) {
			Set<Integer> indexToBeRemoved = new HashSet<Integer>();
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				for (int priceIndex = 0; priceIndex < segmentInfo.getPriceInfoList().size(); priceIndex++) {
					PriceInfo pInfo = segmentInfo.getPriceInfoList().get(priceIndex);
					if (fareTypes.contains(pInfo.getFareType().toUpperCase())) {
						indexToBeRemoved.add(priceIndex);
					}
				}
			}

			if (CollectionUtils.isNotEmpty(indexToBeRemoved)) {
				log.debug("Removing price indexes {} for trip {}", indexToBeRemoved, tripInfo.getTripKey());
				tripInfo.getSegmentInfos().forEach(segment -> {
					int priceIndex = 0;
					for (Iterator<PriceInfo> iter = segment.getPriceInfoList().iterator(); iter.hasNext();) {
						iter.next();
						if (indexToBeRemoved.contains(priceIndex)) {
							iter.remove();
						}
						priceIndex++;
					}
				});
			}
		}
	}

	public static boolean removeRedundantAndCheckMergeApplicable(TripInfo oldTripInfo, TripInfo newTripInfo) {
		boolean isMergeable = false;
		// key is fareIdentifier : PUBLISHED, SME,FLEXI,CORP
		Set<String> onwardFareTypes = TripPriceEngine.getFareIdentifier(oldTripInfo);
		Set<String> returnFareTypes = TripPriceEngine.getFareIdentifier(newTripInfo);
		TripPriceEngine.filterBasedOnFareIdentifier(oldTripInfo, returnFareTypes);
		TripPriceEngine.filterBasedOnFareIdentifier(newTripInfo, onwardFareTypes);
		if (CollectionUtils.isNotEmpty(oldTripInfo.getSegmentInfos().get(0).getPriceInfoList())
				&& CollectionUtils.isNotEmpty(newTripInfo.getSegmentInfos().get(0).getPriceInfoList())) {
			isMergeable = true;
		}
		return isMergeable;
	}

	public static void sortPriceOnFareTypeOrdinal(TripInfo tripInfo) {
		if (tripInfo != null && CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				List<PriceInfo> priceInfos = segmentInfo.getPriceInfoList();
				if (CollectionUtils.isNotEmpty(priceInfos)) {
					Collections.sort(priceInfos, new Comparator<PriceInfo>() {
						@Override
						public int compare(PriceInfo o1, PriceInfo o2) {
							if (o1.getFareIdentifier() != null && o2.getFareIdentifier() != null) {
								return StringUtils.compare(o1.getFareIdentifier(), o2.getFareIdentifier());
							}
							return 0;
						}
					});
				}
				segmentInfo.setPriceInfoList(priceInfos);
			});
		}
	}

	public static void filterInValidPriceInfoFromTrip(List<SegmentInfo> segmentInfos, AirSearchQuery searchQuery) {

		for (SegmentInfo segmentInfo : segmentInfos) {
			for (Iterator<PriceInfo> iter = segmentInfo.getPriceInfoList().iterator(); iter.hasNext();) {
				PriceInfo priceInfo = iter.next();
				if (priceInfo.getCabinClass(PaxType.ADULT) == null
						|| !searchQuery.getCabinClass().isAllowedCabinClass(priceInfo.getCabinClass(PaxType.ADULT))) {
					iter.remove();
				}
			}
		}
	}

	protected static void processSystemDefinedFareType(ContextData contextData, TripInfo tripInfo,
			AirSearchQuery searchQuery, User user) {
		FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(searchQuery).generateFact(tripInfo,
				AirUtils.getAirType(tripInfo));
		BaseUtils.createFactOnUser(flightFact, user);
		for (int priceIndex = 0; priceIndex < tripInfo.getTripPriceInfos().size(); priceIndex++) {
			if (tripInfo.getSegmentInfos().get(0).getPriceInfo(priceIndex).getFareDetail(PaxType.ADULT) != null
					&& !tripInfo.getSegmentInfos().get(0).getPriceInfo(priceIndex).getFareDetail(PaxType.ADULT)
							.getFareComponents().containsKey(FareComponent.XT)) {
				AirConfiguratorInfo fareTypeRule =
						getFareTypeConfigRule(contextData, tripInfo, user, priceIndex, searchQuery, flightFact);
				if (fareTypeRule != null) {
					AirFareTypeOutput output = (AirFareTypeOutput) fareTypeRule.getIRuleOutPut();
					for (SegmentInfo segment : tripInfo.getSegmentInfos()) {
						PriceInfo segmentPriceInfo = segment.getPriceInfo(priceIndex);
						segmentPriceInfo.getMiscInfo().setFareTypeRuleId(fareTypeRule.getId());
						segmentPriceInfo.getMiscInfo().getRuleIdMap().put(AirRules.FT, fareTypeRule.getId());
						segmentPriceInfo.setOriginalFareType(segmentPriceInfo.getFareIdentifier());
						segmentPriceInfo.setSystemDefinedType(output.getFareType());
						segmentPriceInfo.setFareIdentifier(output.getFareType());
						PriceInfo tripPriceInfo = tripInfo.getTripPriceInfos().get(priceIndex);
						tripPriceInfo.getMiscInfo().setFareTypeRuleId(fareTypeRule.getId());
						tripPriceInfo.getMiscInfo().getRuleIdMap().put(AirRules.FT, fareTypeRule.getId());
						tripPriceInfo.setOriginalFareType(tripPriceInfo.getFareIdentifier());
						tripPriceInfo.setFareIdentifier(output.getFareType());
						tripPriceInfo.setSystemDefinedType(output.getFareType());
					}
				}
			}
		}
	}

	public static AirConfiguratorInfo getFareTypeConfigRule(ContextData contextData, TripInfo tripInfo, User user,
			int priceIndex, AirSearchQuery searchQuery, FlightBasicFact flightFact) {
		PriceInfo priceInfo = tripInfo.getTripPriceInfos().get(priceIndex);
		flightFact.generateFact(tripInfo, priceIndex);
		flightFact.generateFact(tripInfo.getSegmentInfos().get(0).getPriceInfo(priceIndex));
		flightFact.setTripFareComponents(priceInfo.getFareDetail(PaxType.ADULT).getFareComponents());
		BaseUtils.createFactOnUser(flightFact, user);
		AirConfiguratorInfo configuratorInfo =
				AirConfiguratorHelper.getAirConfigRuleInfo(flightFact, AirConfiguratorRuleType.FARE_TYPE);
		return configuratorInfo;
	}

	public static void holdFeeCharges(TripInfo trip, String bookingId) {
		FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(trip, AirUtils.getAirType(trip));
		BaseUtils.createFactOnUser(flightFact, SystemContextHolder.getContextData().getUser());
		AirConfiguratorInfo holdFeeRule =
				AirConfiguratorHelper.getAirConfigRuleInfo(flightFact, AirConfiguratorRuleType.HOLD_FEE);

		if (holdFeeRule != null) {
			log.info("Applying hold fee charges {} rule {} on trip {}", bookingId, holdFeeRule, trip);
			HoldFeeChargeOutput holdFeeCharge = (HoldFeeChargeOutput) holdFeeRule.getOutput();
			Long ruleId = holdFeeRule.getId();
			trip.getSegmentInfos().forEach(segmentInfo -> {
				segmentInfo.getPriceInfo(0).getFareDetails().forEach(((paxType, fareDetail) -> {
					Double amount = BaseUtils.evaluateExpression(holdFeeCharge.getExpression(), fareDetail, null, true);
					fareDetail.getFareComponents().put(FareComponent.HC, amount);
				}));
				segmentInfo.getPriceInfo(0).getMiscInfo().setHoldFeeRuleId(ruleId);
				segmentInfo.getPriceInfo(0).getMiscInfo().getRuleIdMap().put(AirRules.HF, ruleId);
			});
		}
	}


	public static void updateCustomFareType(TripInfo trip, AirSearchQuery searchQuery) {
		FlightBasicFact changeFareTypeFact =
				FlightBasicFact.createFact().generateFact(searchQuery).generateFact(trip, AirUtils.getAirType(trip));
		BaseUtils.createFactOnUser(changeFareTypeFact, SystemContextHolder.getContextData().getUser());

		SegmentInfo segment = trip.getSegmentInfos().get(0);
		for (int priceIdx = 0; priceIdx < segment.getPriceInfoList().size(); priceIdx++) {
			PriceInfo priceInfo = segment.getPriceInfoList().get(priceIdx);
			changeFareTypeFact.generateFact(priceInfo).generateFact(trip, priceIdx);
			AirConfiguratorInfo configuratorInfo = AirConfiguratorHelper.getAirConfigRuleInfo(changeFareTypeFact,
					AirConfiguratorRuleType.CHANGE_FARE_TYPE);
			if (configuratorInfo != null) {
				ChangeFareTypeOutput fareTypeRule = (ChangeFareTypeOutput) configuratorInfo.getIRuleOutPut();
				log.debug("Rule fetched for changefareType is {} with fact: {} ", configuratorInfo.getId(),
						changeFareTypeFact);
				priceInfo.setFareIdentifier(fareTypeRule.getFareType());
				priceInfo.getMiscInfo().setChangeFareTypeId(configuratorInfo.getId());
				priceInfo.getMiscInfo().getRuleIdMap().put(AirRules.CFT, configuratorInfo.getId());
				if (CollectionUtils.size(trip.getSegmentInfos()) > 1) {
					for (int sIndex = 1; sIndex < trip.getSegmentInfos().size(); sIndex++) {
						trip.getSegmentInfos().get(sIndex).getPriceInfo(priceIdx)
								.setFareIdentifier(fareTypeRule.getFareType());
					}
				}
			}
		}

	}
}
