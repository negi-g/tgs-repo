package com.tgs.services.fms.sources.kafila;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.kafila.pnrCreationRequest.Other;
import com.tgs.services.kafila.pnrCreationRequest.Pax;
import com.tgs.services.kafila.pnrCreationRequest.PnrCreationRequest;
import com.tgs.services.kafila.pnrCreationResponse.PnrCreationResponse;
import com.tgs.services.kafila.pnrRetrieveRequest.Details;
import com.tgs.services.kafila.pnrRetrieveRequest.PnrRetrieveRequest;
import com.tgs.services.kafila.pnrRetrieveResponse.PnrRetrieveResponse;
import com.tgs.services.kafila.reviewRequest.ReviewRequest;
import com.tgs.services.kafila.reviewResponse.ReviewResponse;
import com.tgs.services.oms.datamodel.Order;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Setter
@Getter
@Slf4j
final class KafilaBookingManager extends KafilaServiceManager {

	private Order order;
	private List<SegmentInfo> segmentInfos;
	private String kafilaBookingId;
	public String UUID;

	protected DeliveryInfo deliveryInfo;


	public boolean createPNR() {
		PnrCreationRequest createPnrRequest = buildPnrCreateRequest();
		PnrCreationResponse createPnrResponse = bindingService.doBooking(createPnrRequest);

		if (createPnrResponse != null && CollectionUtils.isNotEmpty(createPnrResponse.getRESULT())
				&& createPnrResponse.getRESULT().get(0).getBOOKINGID() != null) {
			kafilaBookingId = createPnrResponse.getRESULT().get(0).getBOOKINGID();
			return true;
		}
		return false;
	}

	public PnrCreationRequest buildPnrCreateRequest() {
		PnrCreationRequest request = new PnrCreationRequest();
		String str = segmentInfos.get(0).getPriceInfoList().get(0).getMiscInfo().getAvailabilityDisplayType();

		request = GsonUtils.getGson().fromJson(str, PnrCreationRequest.class);

		List<Pax> passengers = new ArrayList<Pax>();
		for (FlightTravellerInfo travellerInfo : segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo()) {
			Pax passenger = Pax.builder().build();
			passenger.setTtl(
					KafilaPaxTitle.getEnumFromCode(travellerInfo.getTitle().toUpperCase()).toString().toUpperCase());
			passenger.setFn(travellerInfo.getFirstName());
			passenger.setLn(travellerInfo.getLastName());
			if (Objects.nonNull(travellerInfo.getDob())) {
				passenger.setDob(travellerInfo.getDob().toString());
			}
			if (StringUtils.isNotBlank(travellerInfo.getPassportNumber())) {
				passenger.setPn(travellerInfo.getPassportNumber());
			}
			if (Objects.nonNull(travellerInfo.getExpiryDate())) {
				passenger.setEd(travellerInfo.getExpiryDate().toString());
			}
			passenger.setType(KafilaUtils.getPaxType(travellerInfo.getPaxType()));
			passengers.add(passenger);
		}
		request.setPAX(passengers);
		List<Other> others = new ArrayList<Other>();
		Other other = new Other();
		other.setREMARK(supplierConfiguration.getSupplierCredential().getUserName());
		other.setCUSTOMER_EMAIL(AirSupplierUtils.getEmailId(deliveryInfo));
		other.setCUSTOMER_MOBILE(AirSupplierUtils.getContactNumber(deliveryInfo));
		others.add(other);
		request.setOthers(others);

		return request;
	}

	public PnrRetrieveRequest buildPnrRetrieveRequest(String bookingID) {
		PnrRetrieveRequest request = new PnrRetrieveRequest();
		Details details = new Details();
		List<Details> detailList = new ArrayList<Details>();
		details.setBOOKINGID(bookingID);
		details.setCLIENT_SESSIONID(getUUID());
		details.setHS(String.valueOf(getHitSrc()));
		detailList.add(details);
		request.setSTR(detailList);
		return request;
	}

	public boolean checkAndUpdateBookingInfo(List<TripInfo> tripInfos, AirSearchQuery searchQuery) {

		boolean isSuccess = false;
		PnrRetrieveRequest pnrRequest = buildPnrRetrieveRequest(kafilaBookingId);
		PnrRetrieveResponse pnrResponse = null;
		int i = 0;
		while ((pnrResponse == null || pnrResponse.getPAX() == null) && i < 10) {
			try {
				Thread.sleep(5 * 1000);
				pnrResponse = bindingService.getPnr(pnrRequest);
				i++;
			} catch (InterruptedException e) {
				log.error("Thread interuppted for booking {}", bookingId, e);
			}
		}

		TripInfo outBoundTrip = tripInfos.get(0);
		TripInfo inBoundTrip = null;
		if (searchQuery.isReturn()) {
			inBoundTrip = tripInfos.get(1);
		}

		if (CollectionUtils.isNotEmpty(pnrResponse.getPAX())) {
			List<com.tgs.services.kafila.pnrRetrieveResponse.Pax> passengers = pnrResponse.getPAX();
			updatePNRSonJounrey(outBoundTrip, passengers);
			isSuccess = true;
		}
		if (CollectionUtils.isNotEmpty(pnrResponse.getPAXOW())) {
			List<com.tgs.services.kafila.pnrRetrieveResponse.Pax> passengers = pnrResponse.getPAXOW();
			updatePNRSonJounrey(outBoundTrip, passengers);
			isSuccess = true;
		}
		if (CollectionUtils.isNotEmpty(pnrResponse.getPAXRT())) {
			List<com.tgs.services.kafila.pnrRetrieveResponse.Pax> passengers = pnrResponse.getPAXRT();
			updatePNRSonJounrey(inBoundTrip, passengers);
			isSuccess = true;
		}

		return isSuccess;
	}

	private void updatePNRSonJounrey(TripInfo tripInfo,
			List<com.tgs.services.kafila.pnrRetrieveResponse.Pax> passengers) {
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			List<FlightTravellerInfo> travellerInfos = segmentInfo.getTravellerInfo();

			for (FlightTravellerInfo travellerInfo : travellerInfos) {
				com.tgs.services.kafila.pnrRetrieveResponse.Pax matchedPax = passengers.stream().filter(passenger -> {
					return StringUtils.equalsIgnoreCase(travellerInfo.getFirstName(), passenger.getFn())
							&& StringUtils.equalsIgnoreCase(travellerInfo.getLastName(), passenger.getLn());
				}).findFirst().get();
				if (matchedPax != null) {
					String airlinePnr = matchedPax.getApnr();
					String ticketNumbner = matchedPax.getTktno();
					String supplierPNR = matchedPax.getGpnr();
					travellerInfo.setPnr(airlinePnr);
					travellerInfo.setTicketNumber(ticketNumbner);
					travellerInfo.setReservationPNR(supplierPNR);
				}
			}

		});
	}

	public double fareCheckBeforeConfirmation() {

		String str = segmentInfos.get(0).getPriceInfoList().get(0).getMiscInfo().getJourneyKey();
		ReviewRequest request = GsonUtils.getGson().fromJson(str, ReviewRequest.class);
		ReviewResponse response = bindingService.doReview(request);
		segmentInfos.get(0).getPriceInfoList().get(0).getMiscInfo()
				.setJourneyKey((GsonUtils.getGson().toJson(response)));

		if (CollectionUtils.isNotEmpty(response.getFARE())) {
			return response.getFARE().get(0).getGRAND_TOTAL();
		} else {
			return response.getFAREOW().get(0).getGRAND_TOTAL() + response.getFARERT().get(0).getGRAND_TOTAL();
		}

	}
}
