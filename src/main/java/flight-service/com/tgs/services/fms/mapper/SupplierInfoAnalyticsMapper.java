package com.tgs.services.fms.mapper;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class SupplierInfoAnalyticsMapper extends Mapper<SupplierAnalyticsQuery> {

	private AirSearchQuery searchQuery;

	private String bookingId;

	private SupplierBasicInfo basicInfo;

	@Override
	protected void execute() throws CustomGeneralException {

		if (output == null) {
			output = SupplierAnalyticsQuery.builder().build();
		}

		AnalyticsAirQueryMapper.builder().searchQuery(searchQuery).basicInfo(basicInfo).build().setOutput(output)
				.convert();
		output.setBookingid(bookingId);
	}

}
