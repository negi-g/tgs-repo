package com.tgs.services.fms.sources.travelport.sessionless;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleTimeWindow;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import lombok.extern.slf4j.Slf4j;
import travelport.www.schema.air_v47_0.AirPricingInfo_type0;
import travelport.www.schema.air_v47_0.BookingInfo_type0;
import travelport.www.schema.air_v47_0.FareInfo_type0;
import travelport.www.schema.air_v47_0.OptionalService_type0;
import travelport.www.schema.air_v47_0.TypeFarePenalty;
import travelport.www.schema.air_v47_0.TypePrivateFare;
import travelport.www.schema.common_v47_0.BookingTraveler_type0;
import travelport.www.schema.universal_v47_0.UniversalRecordRetrieveRsp;


@Slf4j
class TravelPortSessionLessUtils {

	public static LocalDateTime getIsoDateTime(String dateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		LocalDateTime formattedDateTime = LocalDateTime.parse(dateTime, formatter);
		return formattedDateTime;
	}

	public static String getStringDateTime(LocalDateTime dateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		String formattedDateTime = formatter.format(dateTime);
		return formattedDateTime;
	}

	public static String keyGenerator() {
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz" + "/=";
		StringBuilder sb = new StringBuilder(20);

		for (int i = 0; i < 20; i++) {
			int index = (int) (AlphaNumericString.length() * Math.random());
			sb.append(AlphaNumericString.charAt(index));
		}
		return sb.toString();
	}

	public static String getAccountCodeFromTrip(List<SegmentInfo> segmentInfos) {
		String accountCode = null;
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(segmentInfos)
				&& org.apache.commons.collections.CollectionUtils.isNotEmpty(segmentInfos.get(0).getPriceInfoList())) {
			PriceInfo priceInfo = segmentInfos.get(0).getPriceInfo(0);
			accountCode = priceInfo.getAccountCode();
		}
		return accountCode;
	}

	public static boolean isPrivateFare(List<SegmentInfo> segmentInfos) {
		boolean isPrivateFare = false;
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(segmentInfos)
				&& org.apache.commons.collections.CollectionUtils.isNotEmpty(segmentInfos.get(0).getPriceInfoList())) {
			PriceInfo priceInfo = segmentInfos.get(0).getPriceInfo(0);
			isPrivateFare = priceInfo.isPrivateFare();

		}
		return isPrivateFare;
	}

	// this have to careful when using , when only in case of first name is empty then already we passed first name as
	// title and title as empty to match retension and booking traveller
	public static String getPaxPrefix(FlightTravellerInfo travellerInfo) {
		if (StringUtils.isBlank(travellerInfo.getFirstName())) {
			return StringUtils.EMPTY;
		}
		// if (travellerInfo.getPaxType().equals(PaxType.CHILD) || travellerInfo.getPaxType().equals(PaxType.INFANT)) {
		// if (StringUtils.equalsIgnoreCase(travellerInfo.getTitle(), "master")
		// || StringUtils.equalsIgnoreCase(travellerInfo.getTitle(), "mstr")) {
		// return "MSTR";
		// } else if (StringUtils.equalsIgnoreCase(travellerInfo.getTitle(), "ms")) {
		// return "MISS";
		// }
		// }
		return getPaxTitle(travellerInfo);
	}


	public static String getPaxTitle(FlightTravellerInfo travellerInfo) {
		if (travellerInfo.getPaxType().equals(PaxType.CHILD) || travellerInfo.getPaxType().equals(PaxType.INFANT)) {
			if (travellerInfo.getTitle().equalsIgnoreCase("master")
					|| travellerInfo.getTitle().equalsIgnoreCase("mstr")) {
				return "MSTR";
			} else if (travellerInfo.getTitle().equalsIgnoreCase("ms")) {
				return "MISS";
			}
		}
		return travellerInfo.getTitle();
	}

	public static String createDate(LocalDate passportDate) {
		StringBuilder stringBuilder = new StringBuilder();
		if (passportDate.getDayOfMonth() < 10) {
			stringBuilder.append("0" + passportDate.getDayOfMonth());
		} else {
			stringBuilder.append(passportDate.getDayOfMonth());
		}
		stringBuilder.append(passportDate.getMonth().toString().substring(0, 3));
		stringBuilder.append(
				String.valueOf(passportDate.getYear()).substring(String.valueOf(passportDate.getYear()).length() - 2));
		return stringBuilder.toString();
	}

	public static LocalDate createDate(String passportDate) {
		String date = passportDate.substring(0, 2);
		String month = WordUtils.capitalizeFully(passportDate.substring(2, 5));
		String year = passportDate.substring(5);

		String dateString = date + "-" + month + "-" + year;
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MMM-yy");
		LocalDate localDate = LocalDate.parse(dateString, dateTimeFormatter);
		return localDate;
	}

	public static String getCabinCode(SupplierConfiguration configuration, AirSearchQuery searchQuery) {
		String classToSearch = TravelPortSessionLessConstants.ECONOMY;
		if (configuration != null && configuration.getSupplierAdditionalInfo() != null
				&& StringUtils.isNotBlank(configuration.getSupplierAdditionalInfo().getFareClass())) {
			String fareClass = configuration.getSupplierAdditionalInfo().getFareClass();
			if (TravelPortSessionLessConstants.DEFAULT.equalsIgnoreCase(fareClass)) {
				fareClass = TravelPortSessionLessConstants.ECONOMY;
			}
			CabinClass cabinClass = CabinClass.valueOf(fareClass.toUpperCase());
			classToSearch = getCabinClass(cabinClass.getName());
		} else {
			classToSearch = getCabinClass(searchQuery.getCabinClass().getName());
		}
		return classToSearch;
	}

	public static String getCabinClass(String cabinClass) {
		String cabinClassRs = TravelPortSessionLessConstants.ECONOMY;
		String classSearched = cabinClass.toUpperCase();
		if (StringUtils.isNotBlank(classSearched)) {
			if (CabinClass.PREMIUM_ECONOMY.getName().equalsIgnoreCase(classSearched)) {
				cabinClassRs = TravelPortSessionLessConstants.PREMIUM_ECONOMY;
			} else if (CabinClass.BUSINESS.getName().equalsIgnoreCase(classSearched)) {
				cabinClassRs = TravelPortSessionLessConstants.BUSINESS;
			} else if (CabinClass.FIRST.getName().equalsIgnoreCase(classSearched)) {
				cabinClassRs = TravelPortSessionLessConstants.FIRST;
			}
		}
		return cabinClassRs;
	}

	public static CabinClass getCabinClass(BookingInfo_type0 bookingInfo) {
		String classSearched = bookingInfo.getCabinClass().toUpperCase();
		CabinClass cabinClass = CabinClass.ECONOMY;
		if (StringUtils.isNotBlank(classSearched)) {
			if (TravelPortSessionLessConstants.PREMIUM_ECONOMY.equalsIgnoreCase(classSearched)) {
				cabinClass = CabinClass.PREMIUM_ECONOMY;
			} else if (TravelPortSessionLessConstants.BUSINESS.equalsIgnoreCase(classSearched)) {
				cabinClass = CabinClass.BUSINESS;
			} else if (TravelPortSessionLessConstants.FIRST.equalsIgnoreCase(classSearched)) {
				cabinClass = CabinClass.FIRST;
			}
		}
		return cabinClass;
	}

	public static String getProviderCode(TripInfo selectedTrip, SupplierConfiguration configuration) {
		String pCode = StringUtils.EMPTY;
		if (selectedTrip != null && CollectionUtils.isNotEmpty(selectedTrip.getSegmentInfos())
				&& StringUtils.isNotBlank(selectedTrip.getSegmentInfos().get(0).getPriceInfoList().get(0).getMiscInfo()
						.getProviderCode())) {
			pCode = selectedTrip.getSegmentInfos().get(0).getPriceInfoList().get(0).getMiscInfo().getProviderCode();
		} else if (StringUtils.isNotBlank(configuration.getSupplierCredential().getProviderCode())) {
			pCode = configuration.getSupplierCredential().getProviderCode();
		}
		return pCode;
	}

	public static int getPaxAirPricingInfoCount(List<FlightTravellerInfo> travellerInfoList, PaxType paxType) {
		Set<Integer> paxDifferentAgeCount = new HashSet<>();
		if (paxType.equals(PaxType.CHILD)) {
			for (FlightTravellerInfo travellerInfo : travellerInfoList) {
				paxDifferentAgeCount.add(Integer.valueOf(TravelPortSessionLessUtils.getAge(travellerInfo)));
			}
			return paxDifferentAgeCount.size();
		}
		return 1;
	}

	public static Date getCreditCardExpiryDate(CreditCardInfo creditCardInfo) {
		String[] yearDate = creditCardInfo.getExpiry().split("/");
		if (yearDate[1].length() == 2) {
			yearDate[1] = "20" + yearDate[1];
		}
		Integer year = Integer.valueOf(yearDate[1]);
		Integer month = Integer.valueOf(yearDate[0]);
		Date date = new GregorianCalendar(year, month - 1, 1).getTime();
		return date;
	}

	public static String getValidRegisteredName(String registeredName) {
		if (registeredName.length() > 35) {
			registeredName = registeredName.substring(0, 35);
		}
		return registeredName;
	}

	public static Integer getRefundableType(AirPricingInfo_type0 airPricingInfo) {
		RefundableType refundableType = RefundableType.NON_REFUNDABLE;
		try {
			if (airPricingInfo != null && BooleanUtils.isTrue(airPricingInfo.getRefundable())) {
				refundableType = RefundableType.REFUNDABLE;
			} else if (airPricingInfo != null && ArrayUtils.isNotEmpty(airPricingInfo.getCancelPenalty())) {
				for (TypeFarePenalty typeFarePenalty : airPricingInfo.getCancelPenalty()) {
					if (typeFarePenalty.getPenaltyApplies().getValue()
							.equalsIgnoreCase(TravelPortSessionLessConstants.AFTER_DEPARTURE)
							&& (typeFarePenalty.getPercentage().getTypePercentageWithDecimal().equals("100.00")
									|| Double
											.valueOf(typeFarePenalty.getAmount().getTypeMoney().substring(3)) == 0.0)) {
						refundableType = RefundableType.REFUNDABLE;
						break;
					}

				}
			}
		} catch (Exception e) {
			log.error("Exception in setting refundable ", e);
		}
		return refundableType.getRefundableType();
	}

	public static String getFirstName(FlightTravellerInfo travellerInfo) {
		if (StringUtils.isNotBlank(travellerInfo.getFirstName())) {
			return travellerInfo.getFirstName();
		}
		return getPaxTitle(travellerInfo);
	}

	public static String getLastName(FlightTravellerInfo travellerInfo) {
		if (StringUtils.isNotBlank(travellerInfo.getLastName())) {
			return travellerInfo.getLastName();
		}
		return StringUtils.EMPTY;
	}

	public static String buildPassportFreeText(FlightTravellerInfo travellerInfo) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("P/");
		stringBuilder.append(travellerInfo.getPassportNationality() + "/");
		stringBuilder.append(travellerInfo.getPassportNumber().replaceAll("[^\\w\\s]", "") + "/");
		stringBuilder.append(travellerInfo.getPassportNationality() + "/");
		stringBuilder.append(createDate(travellerInfo.getDob()) + "/");
		stringBuilder.append(getGender(travellerInfo) + "/");
		stringBuilder.append(createDate(travellerInfo.getExpiryDate()) + "/");
		stringBuilder.append(travellerInfo.getLastName() + "/");
		stringBuilder.append(getFirstName(travellerInfo) + "/");
		return stringBuilder.toString();
	}

	public static String getGender(FlightTravellerInfo travellerInfo) {
		PaxType paxType = travellerInfo.getPaxType();
		String paxTitle = travellerInfo.getTitle();
		if (paxType.equals(PaxType.ADULT) || paxType.equals(PaxType.CHILD)) {
			if (paxTitle.equalsIgnoreCase("mr") || paxTitle.equalsIgnoreCase("mstr")
					|| paxTitle.equalsIgnoreCase("mstr") || paxTitle.equalsIgnoreCase("master"))
				return "M";
			else
				return "F";
		} else if (paxType.equals(PaxType.INFANT)) {
			if (paxTitle.equalsIgnoreCase("miss") || paxTitle.equalsIgnoreCase("ms"))
				return "FI";
			else
				return "MI";

		}
		return null;
	}

	public static String getTotalAirlineFare(String currencyCode, FareDetail fareDetail) {
		Double totalFare = 0.0;
		if (fareDetail != null && MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			totalFare = fareDetail.getAirlineFare();
		}
		return StringUtils.join(currencyCode, Double.valueOf(totalFare));
	}

	public static String getBaseFare(String currencyCode, FareDetail fareDetail) {
		Double baseFare = 0.0;
		if (fareDetail != null && MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			baseFare = fareDetail.getFareComponents().getOrDefault(FareComponent.BF, 0.0);
		}
		return StringUtils.join(currencyCode, Double.valueOf(baseFare));
	}

	public static String getTaxFare(String currencyCode, FareDetail fareDetail) {
		return StringUtils.join(currencyCode, Double.valueOf(getTaxFare(fareDetail)));
	}

	public static double getTaxFare(FareDetail fareDetail) {
		double taxFare = 0.0;
		if (fareDetail != null && MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			for (Map.Entry<FareComponent, Double> entry : fareDetail.getFareComponents().entrySet()) {
				if (entry.getKey().airlineComponent() && !entry.getKey().equals(FareComponent.BF)
						&& !TravelPortSessionLessConstants.ssrList.contains(entry.getKey())) {
					taxFare += entry.getValue();
				}
			}
		}
		return taxFare;
	}

	public static String getServiceFare(String currencyCode, FareDetail fareDetail) {
		return StringUtils.join(currencyCode, Double.valueOf(getServiceFare(fareDetail)));
	}

	public static double getServiceFare(FareDetail fareDetail) {
		double serviceFare = 0.0;
		if (fareDetail != null && MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			for (Map.Entry<FareComponent, Double> entry : fareDetail.getFareComponents().entrySet()) {
				if (entry.getKey().airlineComponent()
						&& TravelPortSessionLessConstants.ssrList.contains(entry.getKey())) {
					serviceFare += entry.getValue();
				}
			}
		}
		return serviceFare;
	}

	public static Boolean isPrivateFare(FareInfo_type0 fareInfo) {
		boolean isPrivateFare = false;
		if (fareInfo.getPrivateFare() != null) {
			String fareCode = fareInfo.getPrivateFare().getValue();
			if (TypePrivateFare._AirlinePrivateFare.equals(fareCode)
					|| TypePrivateFare._AgencyPrivateFare.equals(fareCode)
					|| TypePrivateFare._PrivateFare.equals(fareCode)) {
				isPrivateFare = true;
			}
		}
		return isPrivateFare;
	}

	public static FareDetail getFareDetail(PriceInfo priceInfo) {
		FareDetail fareDetail = null;
		if (MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
			if (priceInfo.getFareDetail(PaxType.ADULT) != null) {
				fareDetail = priceInfo.getFareDetail(PaxType.ADULT);
			} else if (priceInfo.getFareDetail(PaxType.CHILD) != null) {
				fareDetail = priceInfo.getFareDetail(PaxType.CHILD);
			}
		}
		return fareDetail;
	}

	public static boolean isPassportAvailable(List<FlightTravellerInfo> travellerInfos) {
		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			if (StringUtils.isNotBlank(travellerInfo.getPassportNumber()) && travellerInfo.getExpiryDate() != null) {
				return true;
			}
		}
		return false;
	}

	public static boolean isFqFlierAvailable(List<FlightTravellerInfo> travellerInfos) {
		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			if (MapUtils.isNotEmpty(travellerInfo.getFrequentFlierMap())) {
				return true;
			}
		}
		return false;
	}

	public static String getReservationPNR(List<SegmentInfo> segmentInfos) {
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			return segmentInfos.get(0).getTravellerInfo().get(0).getReservationPNR();
		}
		return null;
	}

	public static String getAge(FlightTravellerInfo travellerInfo) {
		if (travellerInfo.getDob() != null) {
			String age = String.valueOf(TgsDateUtils.getAgeFromBirthDate(travellerInfo.getDob()));
			if (age.length() == 1) {
				age = StringUtils.join("0", age);
			}
			return age;
		}
		if (PaxType.CHILD.equals(travellerInfo.getPaxType())) {
			return (TravelPortSessionLessConstants.CHILD_AGE);
		} else if (PaxType.INFANT.equals(travellerInfo.getPaxType())) {
			return TravelPortSessionLessConstants.INFANT_AGE;
		}
		return TravelPortSessionLessConstants.ADULT_AGE;
	}

	public static FareRuleTimeWindow getTimeWindow(String departureType) {
		if (departureType.equalsIgnoreCase("AFTER DEPARTURE")) {
			return FareRuleTimeWindow.AFTER_DEPARTURE;
		} else if (departureType.equalsIgnoreCase("BEFORE DEPARTURE")) {
			return FareRuleTimeWindow.BEFORE_DEPARTURE;
		} else if (departureType.equalsIgnoreCase("NO-SHOW")) {
			return FareRuleTimeWindow.NOSHOW;
		}
		return null;
	}

	public static boolean isSeatAddedInTrip(List<SegmentInfo> segmentInfos) {
		boolean isSSRAdded = false;
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (Objects.nonNull(segmentInfo.getBookingRelatedInfo())
					&& CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
				for (FlightTravellerInfo travellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					if (Objects.nonNull(travellerInfo.getSsrSeatInfo())) {
						isSSRAdded = true;
						break;
					}
				}
			}
		}
		return isSSRAdded;
	}

	public static boolean isSameTravellerInfo(FlightTravellerInfo traveller, FlightTravellerInfo travellerInfoItem) {
		if (traveller.getPaxType().equals(travellerInfoItem.getPaxType())
				&& traveller.getLastName().equals(travellerInfoItem.getLastName())
				&& traveller.getFirstName().equals(travellerInfoItem.getFirstName())) {
			return true;
		}
		return false;
	}

	public static Integer getNextSegmentNumber(int currentSegmentIndex, List<SegmentInfo> segmentInfos) {
		return (currentSegmentIndex + 1 < segmentInfos.size())
				? segmentInfos.get(currentSegmentIndex + 1).getPriceInfo(0).getMiscInfo().getLegNum()
				: 0;
	}

	public static boolean checkIsCommissionAlreadyAdded(String bookingId,
			UniversalRecordRetrieveRsp univRecordRetrieveRS) {
		try {
			if (univRecordRetrieveRS.getUniversalRecord() != null
					&& univRecordRetrieveRS.getUniversalRecord().getUniversalRecordChoice_type7() != null
					&& ArrayUtils.isNotEmpty(univRecordRetrieveRS.getUniversalRecord().getUniversalRecordChoice_type7()
							.getAirReservation())) {
				for (AirPricingInfo_type0 pricingInfo_type0 : univRecordRetrieveRS.getUniversalRecord()
						.getUniversalRecordChoice_type7().getAirReservation()[0].getAirPricingInfo()) {
					for (FareInfo_type0 fareInfo : pricingInfo_type0.getFareInfo()) {
						if (fareInfo.getCommission() != null
								&& StringUtils.isNotBlank(
										fareInfo.getCommission().getPercentage().getTypePercentageWithDecimal())
								&& Double.valueOf(
										fareInfo.getCommission().getPercentage().getTypePercentageWithDecimal()) > 0) {
							return true;
						} else {
							return (Objects.nonNull(fareInfo.getCommission())
									&& StringUtils.isNotBlank(fareInfo.getCommission().getAmount().getTypeMoney())
									&& Double.valueOf(
											fareInfo.getCommission().getAmount().getTypeMoney().substring(3)) > 0);
						}

					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to verify commission already added or not for booking {}", bookingId, e);
		}
		return false;
	}

	public static Set<String> getUniqueAirlines(List<SegmentInfo> segmentInfos) {
		Set<String> airlines = new HashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			airlines.add(segmentInfo.getAirlineCode(false));
		});
		return airlines;
	}

	public static boolean hasInternationalSegment(List<SegmentInfo> bookingSegments, String clientCountry) {
		for (SegmentInfo segment : bookingSegments) {
			if (AirType.INTERNATIONAL.equals(segment.getAirType(clientCountry))) {
				return true;
			}
		}
		return false;
	}

	public static String getFareType(FareInfo_type0 fareInfo) {
		if (fareInfo != null && fareInfo.getFareFamily() != null
				&& fareInfo.getFareFamily().getTypeFareFamily() != null) {
			return fareInfo.getFareFamily().getTypeFareFamily();
		}
		return FareType.PUBLISHED.name();
	}

	public static String getTitle(BookingTraveler_type0 bookingTraveler) {
		if (bookingTraveler.getBaseBookingTravelerInfoA() != null
				&& bookingTraveler.getBaseBookingTravelerInfoA().getBookingTravelerName().getPrefix() != null) {
			return bookingTraveler.getBaseBookingTravelerInfoA().getBookingTravelerName().getPrefix().getPrefix_type1();
		}
		return "Mr";
	}

	public static PaxType getPaxType(BookingTraveler_type0 bookingTraveler) {
		if (bookingTraveler.getTravelerType() != null && bookingTraveler.getTravelerType().getTypePTC() != null) {
			return getPaxType(bookingTraveler.getTravelerType().getTypePTC());
		}
		return PaxType.ADULT;
	}

	public static String getPaxFirstName(BookingTraveler_type0 bookingTraveler) {
		if (bookingTraveler.getBaseBookingTravelerInfoA() != null
				&& bookingTraveler.getBaseBookingTravelerInfoA().getBookingTravelerName() != null
				&& bookingTraveler.getBaseBookingTravelerInfoA().getBookingTravelerName().getFirst() != null) {
			return bookingTraveler.getBaseBookingTravelerInfoA().getBookingTravelerName().getFirst().getFirst_type2();
		}
		return StringUtils.EMPTY;
	}

	public static String getPaxLastName(BookingTraveler_type0 bookingTraveler) {
		if (bookingTraveler.getBaseBookingTravelerInfoA() != null
				&& bookingTraveler.getBaseBookingTravelerInfoA().getBookingTravelerName() != null
				&& bookingTraveler.getBaseBookingTravelerInfoA().getBookingTravelerName().getLast() != null) {
			return bookingTraveler.getBaseBookingTravelerInfoA().getBookingTravelerName().getLast()
					.getTypeTravelerLastName();
		}
		return StringUtils.EMPTY;
	}

	public static boolean isChildPaxRequiresSSR(String platingCarrier) {
		return TravelPortSessionLessConstants.AIRLINES_REQUIRE_CHILD_SSR.contains(platingCarrier);
	}

	public static String getPlattingCarrier(List<AirPricingInfo_type0> airPricingInfoList) {
		if (CollectionUtils.isNotEmpty(airPricingInfoList) && airPricingInfoList.get(0).getPlatingCarrier() != null) {
			return airPricingInfoList.get(0).getPlatingCarrier().getTypeCarrier();
		}
		return null;
	}

	public static String getPriceProviderCode(List<AirPricingInfo_type0> airPricingInfoList) {
		if (!ObjectUtils.isEmpty(airPricingInfoList.get(0).getProviderCode())
				&& StringUtils.isNotBlank(airPricingInfoList.get(0).getProviderCode().getTypeProviderCode())) {
			return airPricingInfoList.get(0).getProviderCode().getTypeProviderCode();
		}
		return null;
	}

	public static void setFareType(PriceInfo pInfo, String bookingCode, FareInfo_type0 fareInfo) {
		// boolean isPromoFare = fareInfo.getPromotionalFare();

		String fareFamily = Objects.nonNull(fareInfo.getFareFamily()) ? fareInfo.getFareFamily().getTypeFareFamily()
				: StringUtils.EMPTY;

		FareType fareIdentifier = TravelPortSessionLessFareFamily.getFareFamily(fareFamily);
		// if (isPromoFare) {
		// fareIdentifier = FareType.PROMO;
		// }
		pInfo.setFareIdentifier(fareIdentifier);
	}

	protected static String getTypePTC(PaxType type) {
		if (PaxType.CHILD.equals(type)) {
			return TravelPortSessionLessConstants.CHILD;
		}
		return type.getType();
	}


	protected static String getTypePTC(String typePTC) {
		if (StringUtils.equalsIgnoreCase(typePTC, "CHD") || StringUtils.equalsIgnoreCase(typePTC, "CNN")) {
			return TravelPortSessionLessConstants.CHILD;
		}
		return typePTC;
	}

	protected static PaxType getPaxType(String typePTC) {
		if (StringUtils.equalsIgnoreCase("ADT", typePTC)) {
			return PaxType.ADULT;
		}
		if (StringUtils.equalsIgnoreCase(typePTC, "CHD") || StringUtils.equalsIgnoreCase(typePTC, "CNN")) {
			return PaxType.CHILD;
		}
		return PaxType.INFANT;
	}

	public static List<SegmentInfo> getSubLegSegments(List<SegmentInfo> segmentInfos, int segmentIndex) {
		List<SegmentInfo> subLegSegments = new ArrayList<SegmentInfo>();
		for (; segmentIndex < segmentInfos.size(); segmentIndex++) {
			Integer nextSegmentLegNum = TravelPortSessionLessUtils.getNextSegmentNumber(segmentIndex, segmentInfos);
			subLegSegments.add(segmentInfos.get(segmentIndex));
			if (nextSegmentLegNum == 0) {
				break;
			}
		}
		return subLegSegments;
	}

	public static String getSSRCode(OptionalService_type0 service) {
		return service.getProviderDefinedType().getProviderDefinedType_type1().replaceAll("[^a-zA-Z0-9]", "");
	}

	public static boolean isSeatSSrSelected(List<SegmentInfo> segmentInfos) {
		for (SegmentInfo segment : segmentInfos) {
			for (FlightTravellerInfo traveller : segment.getTravellerInfo()) {
				if (traveller.getSsrSeatInfo() != null) {
					return true;
				}
			}
		}
		return false;
	}

	public static String subStr(String address) {
		if (StringUtils.isNotBlank(address)) {
			address = StringUtils.substring(address, 0, 40);
		}
		return address;
	}
}
