package com.tgs.services.fms.validator;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.fms.datamodel.airconfigurator.RestrictiveFareTypeOutput;

import java.util.Objects;

@Service
public class RestrictiveFareTypeValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {
		RestrictiveFareTypeOutput output = (RestrictiveFareTypeOutput) iRuleOutput;

		if (output == null) {
			errors.rejectValue(fieldName, SystemError.NULL_VALUE.errorCode(), SystemError.NULL_VALUE.getMessage());
			return;
		}

		if (CollectionUtils.isNotEmpty(output.getExcludedFareTypes())
				&& CollectionUtils.isNotEmpty(output.getIncludedFareTypes())) {
			for (String excludedFareType : output.getExcludedFareTypes()) {
				if (output.getIncludedFareTypes().contains(excludedFareType)) {
					rejectValue(errors, fieldName + ".includedFareTypes", SystemError.REPEATED_FARE_TYPE);
					break;
				}
			}
		}

	}

}
