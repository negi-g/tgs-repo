package com.tgs.services.fms.validator;

import java.util.Objects;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.AirHoldConfigOutput;

@Service
public class AirHoldConfigValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T output) {

		AirHoldConfigOutput holdConfig = (AirHoldConfigOutput) output;

		if (output == null || Objects.isNull(holdConfig.getDisableHoldBeforeDeparture())
				|| holdConfig.getDisableHoldBeforeDeparture() < 0) {
			errors.rejectValue(fieldName, SystemError.NEGATIVE_VALUE.errorCode(),
					SystemError.NEGATIVE_VALUE.getMessage());
			return;
		}
	}
}
