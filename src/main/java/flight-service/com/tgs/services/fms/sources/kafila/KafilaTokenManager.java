package com.tgs.services.fms.sources.kafila;

import java.util.ArrayList;
import java.util.List;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.experimental.SuperBuilder;
import com.tgs.services.kafila.login.LoginDetails;
import com.tgs.services.kafila.login.LoginRequest;
import com.tgs.services.kafila.login.LoginResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;


@Slf4j
@SuperBuilder
final class KafilaTokenManager extends KafilaServiceManager {

	private static final String SUCCESS = "SUCCESS";

	public String login() {
		LoginResponse loginResponse = bindingService.authenticate(buildLoginRequest());
		if (loginResponse.isError()) {
			throw new SupplierSessionException(loginResponse.errorMessage());
		}
		if (StringUtils.equalsIgnoreCase(loginResponse.getSTATUS(), SUCCESS)) {
			token = loginResponse.getRESULT();
			return token;
		}
		return null;
	}

	private LoginRequest buildLoginRequest() {
		LoginRequest request = LoginRequest.builder().build();
		List<LoginDetails> detailsList = new ArrayList<>();
		LoginDetails loginDetails = LoginDetails.builder().build();
		loginDetails.setA_ID(supplierConfiguration.getSupplierCredential().getUserName());
		loginDetails.setPWD(supplierConfiguration.getSupplierCredential().getPassword());
		loginDetails.setU_ID(supplierConfiguration.getSupplierCredential().getAgentUserName());
		loginDetails.setHS(getHitSrc());
		loginDetails.setMODULE(getDomain());
		detailsList.add(loginDetails);
		request.setSTR(detailsList);
		return request;
	}

}
