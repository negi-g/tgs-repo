package com.tgs.services.fms.validator;

import java.util.Objects;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.UnmaskFareTypeOutput;

@Service
public class UnmaskFareRuleValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {
		if (iRuleOutput == null) {
			return;
		}

		if (iRuleOutput instanceof UnmaskFareTypeOutput) {
			UnmaskFareTypeOutput unmaskFareTypeOutput = (UnmaskFareTypeOutput) iRuleOutput;
			if (Objects.isNull(unmaskFareTypeOutput.getUnmaskFare())) {
				errors.rejectValue(fieldName, SystemError.NULL_VALUE.errorCode(), SystemError.NULL_VALUE.getMessage());
				return;
			}

		}
	}

}
