package com.tgs.services.fms.sources.dealinventory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.InventoryOrderFilter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.DealInventoryCommunicator;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ims.datamodel.InventoryOrder;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class InventoryAirCancellationFactory extends AbstractAirCancellationFactory {

	@Autowired
	private DealInventoryCommunicator inventoryCommunicator;

	public InventoryAirCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		super(bookingSegments, order, supplierConf);
	}

	@Override
	public boolean releaseHoldPNR() {
		boolean isSuccess = false;
		List<SegmentInfo> segmentInfos = bookingSegments.getSegmentInfos();
		List<FlightTravellerInfo> travellerInfos =
				bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo();
		String inventoryId = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getInventoryId();
		LocalDate departureDate = segmentInfos.get(0).getDepartTime().toLocalDate();

		try {
			log.info("Reverting Seats Sold for bookingId {} , InventoryId {}", bookingId, inventoryId);
			if (LocalDateTime.now().isBefore(segmentInfos.get(0).getDepartTime())) {
				inventoryCommunicator.revertSeatsSold(inventoryId, departureDate,
						AirUtils.getPaxCountFromTravellerInfo(travellerInfos, false));
			} else {
				log.info("Seats not reverted to Inventory Departure date already passed for {}", bookingId);
			}
			InventoryOrderFilter filter =
					InventoryOrderFilter.builder().referenceIdIn(Arrays.asList(bookingId)).build();
			List<InventoryOrder> inventoryOrders = inventoryCommunicator.fetchInventoryOrders(filter);
			for (InventoryOrder invOrder : inventoryOrders) {
				InventoryOrder order = new GsonMapper<>(invOrder, InventoryOrder.class).convert();
				order.getTravellerInfo().forEach(traveller -> {
					traveller.setStatus(TravellerStatus.UNCONFIRMED);
				});
				inventoryCommunicator.saveInventoryOrder(order);
			}
			isSuccess = true;
		} finally {

		}
		return isSuccess;
	}

}
