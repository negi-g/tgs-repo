package com.tgs.services.fms.sources.technogram;

import java.util.List;
import java.util.Map;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.ProcessedFlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AirOrderDetails;
import com.tgs.services.oms.restmodel.BookingDetailResponse;

public class TechnoGramAirUtils {

	public static MoneyExchangeCommunicator moneyExchnageComm;


	public static void init(MoneyExchangeCommunicator moneyExchnageComm) {
		TechnoGramAirUtils.moneyExchnageComm = moneyExchnageComm;

	}

	public static BookingSegments updatePNRandTicketNumber(BookingDetailResponse retrieveResponse,
			BookingSegments bookingSegments) {
		if (retrieveResponse != null && MapUtils.isNotEmpty(retrieveResponse.getItemInfos())
				&& retrieveResponse.getItemInfos().get(OrderType.AIR.name()) != null) {
			List<ProcessedFlightTravellerInfo> processedTravellerInfos =
					((AirOrderDetails) retrieveResponse.getItemInfos().get(OrderType.AIR.name())).getTravellerInfos();
			for (ProcessedFlightTravellerInfo processedTraveller : processedTravellerInfos) {
				Map<String, String> pnrDetails = processedTraveller.getPnrDetails();
				Map<String, String> ticketDetails = processedTraveller.getTicketNumberDetails();
				// Map<String, String> supplierBookingIds = processedTraveller.getSupplierBookingIds();
				for (SegmentInfo segment : bookingSegments.getSegmentInfos()) {
					FlightTravellerInfo traveller =
							getSameTraveller(processedTraveller, segment.getBookingRelatedInfo().getTravellerInfo());
					if (StringUtils.isNotEmpty(pnrDetails.get(segment.getDepartureAndArrivalAirport()))) {
						traveller.setPnr(pnrDetails.get(segment.getDepartureAndArrivalAirport()));
					}
					if (StringUtils.isNotEmpty(ticketDetails.get(segment.getDepartureAndArrivalAirport()))) {
						traveller.setTicketNumber(ticketDetails.get(segment.getDepartureAndArrivalAirport()));
					}
				}
			}
		}
		return bookingSegments;
	}

	public static FlightTravellerInfo getSameTraveller(ProcessedFlightTravellerInfo processedTraveller,
			List<FlightTravellerInfo> flightTravellerInfos) {
		for (FlightTravellerInfo flightTraveller : flightTravellerInfos) {
			if (flightTraveller.getPaxType().equals(processedTraveller.getPaxType())
					&& StringUtils.equalsIgnoreCase(flightTraveller.getFirstName(), processedTraveller.getFirstName())
					&& StringUtils.equalsIgnoreCase(flightTraveller.getLastName(), processedTraveller.getLastName())
					&& StringUtils.equalsIgnoreCase(flightTraveller.getTitle(), processedTraveller.getTitle())) {
				return flightTraveller;
			}
		}
		return null;
	}

	public static double bookingDetailTTO() {
		return 300;
	}

	public static double searchTTO() {
		return 60;
	}


	public static OrderStatus getBookingStatus(BookingDetailResponse retrieveResponse) {
		return retrieveResponse.getOrder().getStatus();
	}

	public static double getExchangeValue(Double amount, MoneyExchangeInfoFilter moneyExchangeInfoFilter) {
		return moneyExchnageComm.getExchangeValue(amount, moneyExchangeInfoFilter, true);
	}

}
