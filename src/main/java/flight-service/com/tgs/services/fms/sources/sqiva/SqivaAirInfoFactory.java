package com.tgs.services.fms.sources.sqiva;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;


@Service
public class SqivaAirInfoFactory extends AbstractAirInfoFactory {

	SqivaAirline airline = null;

	RestAPIListener listener;

	SqivaBindingService bindingService = null;

	protected void intialize(String key) {
		listener = new RestAPIListener(key);
		airline = SqivaAirline.getSqivaAirline(supplierConf.getSourceId());
		bindingService = SqivaBindingService.builder().credential(supplierConf.getSupplierCredential())
				.listener(listener).user(user).airline(airline).build();
	}

	public SqivaAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableSchedules() {
		intialize(searchQuery.getSearchId());
		SqivaSearchManger searchManager = SqivaSearchManger.builder().searchQuery(searchQuery).sqivaAirline(airline)
				.configuration(supplierConf).bindingService(bindingService).listener(listener)
				.criticalMessageLogger(criticalMessageLogger).user(user).build();
		searchResult = searchManager.doSearch();
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		TripInfo updateTrip = null;
		boolean isReviewSuccess = false;
		try {
			intialize(bookingId);
			SqivaSearchManger searchManager = SqivaSearchManger.builder().searchQuery(searchQuery).sqivaAirline(airline)
					.configuration(supplierConf).bindingService(bindingService).listener(listener)
					.criticalMessageLogger(criticalMessageLogger).user(user).build();
			searchResult = searchManager.doSearch();
			searchResult = combineResults(searchResult, AirUtils.splitTripInfo(selectedTrip, false).size() == 2);
			updateTrip = SqivaUtils.filterResults(searchResult, selectedTrip);
			/*
			 * SqivaReviewManager reviewManager = SqivaReviewManager.builder().searchQuery(searchQuery)
			 * .configuration(supplierConf).criticalMessageLogger(criticalMessageLogger).listener(listener)
			 * .user(user).bookignId(bookingId).sqivaAirline(airline).bindingService(bindingService).build(); updateTrip
			 * = reviewManager.doReview(selectedTrip);
			 */
			if (updateTrip != null) {
				SqivaSSRManager ssrManager =
						SqivaSSRManager.builder().searchQuery(searchQuery).configuration(supplierConf)
								.criticalMessageLogger(criticalMessageLogger).bindingService(bindingService)
								.listener(listener).user(user).bookignId(bookingId).sqivaAirline(airline).build();
				ssrManager.doBaggage(updateTrip);
				ssrManager.doAncillary(updateTrip);
				isReviewSuccess = true;
			}
		} finally {
			if (isReviewSuccess && updateTrip != null && CollectionUtils.isNotEmpty(updateTrip.getSegmentInfos())) {
				storeBookingSession(bookingId, null, null, updateTrip);
			}
		}
		return updateTrip;
	}

	public AirSearchResult combineResults(AirSearchResult searchResult, boolean isDomesticSingleSell) {
		if (isDomesticSingleSell && searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			List<TripInfo> onwardTrips = searchResult.getTripInfos().get(TripInfoType.ONWARD.name());
			List<TripInfo> returnTrips = searchResult.getTripInfos().get(TripInfoType.RETURN.name());
			if (CollectionUtils.isNotEmpty(onwardTrips) && CollectionUtils.isNotEmpty(returnTrips)) {
				List<TripInfo> combinedTrips =
						SqivaUtils.buildCombination(onwardTrips, returnTrips, supplierConf.getSourceId(), user);
				if (CollectionUtils.isNotEmpty(combinedTrips)) {
					searchResult.getTripInfos().put(TripInfoType.COMBO.name(), combinedTrips);
				}
			}
		}
		return searchResult;
	}

}
