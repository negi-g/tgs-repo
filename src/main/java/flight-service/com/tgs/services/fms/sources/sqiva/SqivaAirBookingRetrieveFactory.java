package com.tgs.services.fms.sources.sqiva;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;

@Service
public class SqivaAirBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	SqivaAirline airline = null;

	RestAPIListener listener = null;

	SqivaBindingService bindingService = null;

	public SqivaAirBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
		listener = new RestAPIListener(pnr);
		airline = SqivaAirline.getSqivaAirline(supplierConf.getSourceId());
		bindingService = SqivaBindingService.builder().credential(supplierConf.getSupplierCredential())
				.listener(listener).user(bookingUser).airline(airline).build();
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		SqivaBookingRetrieveManager retrieveManager = SqivaBookingRetrieveManager.builder().configuration(supplierConf)
				.listener(listener).bindingService(bindingService).user(bookingUser).build();
		JSONObject retrieveResponse = retrieveManager.getAllBookInfo_V2(pnr);
		return retrieveManager.parseRetrieveResponse(retrieveResponse, pnr);
	}

}
