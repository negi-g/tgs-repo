package com.tgs.services.fms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;

import com.tgs.services.base.ruleengine.IRuleOutPut;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.base.ruleengine.GeneralRuleField;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorFilter;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.dbmodel.DbAirConfiguratorRule;
import com.tgs.services.fms.jparepository.AirConfiguratorService;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@InitializerGroup(group = InitializerGroup.Group.FLIGHT)
public class AirConfiguratorHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap airConfigRules;

	private static Map<AirConfiguratorRuleType, List<AirConfiguratorInfo>> inMemoryAirConfigRules = new HashMap<>();

	private static final String FIELD = "air_config";

	private static Map<String, ? extends IRuleField> gnResolverMap = EnumUtils.getEnumMap(GeneralRuleField.class);
	private static Map<String, ? extends IRuleField> airResolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);

	private static Map<String, IRuleField> fieldResolverMap = new HashMap<>();;

	static {
		fieldResolverMap.putAll(gnResolverMap);
		fieldResolverMap.putAll(airResolverMap);
	}

	private static AirConfiguratorHelper SINGLETON;

	@Autowired
	AirConfiguratorService airConfiguratorService;

	public AirConfiguratorHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap airConfigRules) {
		super(configurationHashMap);
		AirConfiguratorHelper.airConfigRules = airConfigRules;
	}

	public static AirConfiguratorInfo getAirConfigRuleById(AirConfiguratorRuleType ruleType, Long fareTypeRuleId) {
		List<AirConfiguratorInfo> configuratorInfos = inMemoryAirConfigRules.get(ruleType);
		return configuratorInfos.stream().filter(info -> info.getId().equals(fareTypeRuleId)).findFirst().get();
	}

	public static boolean isRulesPresent(AirConfiguratorRuleType ruleType) {
		return CollectionUtils.isNotEmpty(inMemoryAirConfigRules.get(ruleType));
	}

	@PostConstruct
	void init() {
		SINGLETON = this;
	}

	@Override
	public void process() {
		processInMemory();

		if (MapUtils.isEmpty(inMemoryAirConfigRules)) {
			return;
		}

		inMemoryAirConfigRules.forEach((ruleType, rules) -> {
			airConfigRules.put(ruleType.getName(), FIELD, rules,
					CacheMetaInfo.builder().set(CacheSetName.AIR_CONFIGURATOR.getName())
							.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
		});
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	private void processInMemory() {
		List<DbAirConfiguratorRule> configRules =
				airConfiguratorService.findAll(AirConfiguratorFilter.builder().build());
		Map<AirConfiguratorRuleType, List<AirConfiguratorInfo>> airConfigRuleMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(configRules)) {
			List<AirConfiguratorInfo> configuratorInfos = DbAirConfiguratorRule.toDomainList(configRules);
			if (CollectionUtils.isNotEmpty(configuratorInfos)) {
				configuratorInfos = configuratorInfos.stream().filter(rule -> rule.getRuleType() != null)
						.collect(Collectors.toList());
			}

			configuratorInfos.forEach(configuratorInfo -> {
				configuratorInfo.setIRuleOutPut(configuratorInfo.getOutput());
			});
			airConfigRuleMap =
					configuratorInfos.stream().collect(Collectors.groupingBy(AirConfiguratorInfo::getRuleType));
		}
		inMemoryAirConfigRules = airConfigRuleMap;
	}


	@Override
	public void deleteExistingInitializer() {
		if (inMemoryAirConfigRules != null) {
			inMemoryAirConfigRules.clear();
		}
		airConfigRules.truncate(CacheSetName.AIR_CONFIGURATOR.getName());
	}

	@SuppressWarnings("unchecked")
	public static <T> T getAirConfigRule(IFact flightFact, AirConfiguratorRuleType ruleType) {
		List<AirConfiguratorInfo> matchingRules = getApplicableRules(ruleType, flightFact);
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return (T) matchingRules.get(0).getIRuleOutPut();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getAllAirConfigRuleOutputs(IFact flightFact, AirConfiguratorRuleType ruleType) {
		List<AirConfiguratorInfo> matchingRules = getApplicableRules(ruleType, flightFact);
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			List<IRuleOutPut> outPuts = new ArrayList<>();
			matchingRules.forEach(rule -> {
				outPuts.add(rule.getIRuleOutPut());
			});
			return (T) outPuts;
		}
		return null;
	}

	public static AirConfiguratorInfo getAirConfigRuleInfo(IFact flightFact, AirConfiguratorRuleType ruleType) {
		List<AirConfiguratorInfo> matchingRules = getApplicableRules(ruleType, flightFact);
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return matchingRules.get(0);
		}
		return null;
	}

	public static AirGeneralPurposeOutput getGeneralPurposeOutput(FlightBasicFact basicFact) {
		AirGeneralPurposeOutput output = null;
		List<AirConfiguratorInfo> configuratorInfos = getApplicableRules(AirConfiguratorRuleType.GNPUPROSE, basicFact);
		if (CollectionUtils.isNotEmpty(configuratorInfos)) {
			output = (AirGeneralPurposeOutput) configuratorInfos.get(0).getIRuleOutPut();
		}
		return output;
	}


	public static List<AirConfiguratorInfo> getAllAirConfigRule(AirConfiguratorRuleType ruleType, IFact flightFact) {
		List<AirConfiguratorInfo> matchingRules = getApplicableRules(ruleType, flightFact);
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return matchingRules;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static List<AirConfiguratorInfo> getApplicableRules(AirConfiguratorRuleType ruleType, IFact flightFact) {
		LogUtils.log("configurator#ruleexecute#start" + ruleType.toString(),
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		List<AirConfiguratorInfo> airconfigRules = getAirConfiguratorRules(ruleType);

		if (CollectionUtils.isEmpty(airconfigRules)) {
			return null;
		}

		if (CollectionUtils.isNotEmpty(airconfigRules) && AirConfiguratorRuleType.FARE_TYPE.equals(ruleType)) {
			FlightBasicFact fact = (FlightBasicFact) flightFact;
			airconfigRules = filterAirConfigRules(airconfigRules, fact);
		}


		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(airconfigRules, flightFact, fieldResolverMap);
		List<AirConfiguratorInfo> rules = (List<AirConfiguratorInfo>) ruleEngine.fireAllRules();


		LogUtils.log("configurator#ruleexecute#end" + ruleType.toString(),
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				"configurator#ruleexecute#start" + ruleType.toString(), null);

		return rules;
	}

	private static List<AirConfiguratorInfo> filterAirConfigRules(List<AirConfiguratorInfo> airconfigRules,
			FlightBasicFact fact) {
		airconfigRules.stream().filter(rule -> {
			FlightBasicRuleCriteria ruleCriteria = (FlightBasicRuleCriteria) rule.getInclusionCriteria();
			return (rule.getEnabled()
					&& (AirType.ALL.equals(ruleCriteria.getAirType())
							|| ruleCriteria.getAirType().equals(fact.getAirType()))
					&& (CollectionUtils.isEmpty(ruleCriteria.getSupplierIds())
							|| ruleCriteria.getSupplierIds().contains(fact.getSupplierId()))
					&& (CollectionUtils.isEmpty(ruleCriteria.getAirlineList())
							|| ruleCriteria.getAirlineList().contains(fact.getAirline())));
		}).collect(Collectors.toList());
		return airconfigRules;
	}

	private static List<AirConfiguratorInfo> getAirConfiguratorRules(AirConfiguratorRuleType ruleType) {
		if (inMemoryAirConfigRules == null)
			SINGLETON.processInMemory();
		// List<AirConfiguratorInfo> rules = new ArrayList<>();
		// List<AirConfiguratorInfo> configuratorInfos = inMemoryAirConfigRules.get(ruleType);

		return inMemoryAirConfigRules.get(ruleType);
		// Gson gson = GsonUtils.getGson();
		// String json = gson.toJson(inMemoryAirConfigRules.get(ruleType));
		// return gson.fromJson(json, new TypeToken<List<AirConfiguratorInfo>>() {}.getType());
		/*
		 * List<AirConfiguratorInfo> ruleList = new ArrayList<>(); return airConfigRules.get(ruleType.getName(), FIELD,
		 * ruleList.getClass(), CacheMetaInfo.builder().set(CacheSetName.AIR_CONFIGURATOR.getName()) .typeOfT(new
		 * TypeToken<List<AirConfiguratorInfo>>() {}.getType()).build());
		 */
	}
}
