package com.tgs.services.fms.sources.ifly;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import com.ibsplc.www.ires.simpletypes.NameTitle_Type;
import com.ibsplc.www.ires.simpletypes.PaxDetails_Type;
import com.ibsplc.www.ires.simpletypes.GenderDetails_Type;
import com.ibsplc.www.ires.simpletypes.PricingComponentInfoType;
import com.ibsplc.www.ires.simpletypes.SegmentAvailabilityType;
import com.ibsplc.www.ires.simpletypes.SegmentInfoType;
import com.ibsplc.www.ires.simpletypes.TripInfoType;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.utils.AirUtils;

class IflyUtils {

	protected static LocalDateTime getLocalDateTimeFromString(String time) {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		LocalDateTime formattedDateTime = LocalDateTime.parse(time, formatter);
		return formattedDateTime;
	}


	protected static LocalDate getLocalDateFromString(String time) {
		if (StringUtils.isEmpty(time)) {
			return null;
		}
		time = time.substring(0, time.indexOf("T"));
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;
		LocalDate formattedDate = LocalDate.parse(time, formatter);
		return formattedDate;
	}

	/**
	 * This method will convert journey Time which is in format (01:20 , hh:mm)
	 *
	 * @param journeyTime
	 * @return journeyMinutes
	 */
	protected static long getJourneyMinutesFromHoursFormat(String journeyTime) {
		int journeyHours = Integer.parseInt(journeyTime.split(":")[0]);
		int journeyMinutes = Integer.parseInt(journeyTime.split(":")[1]);
		return journeyMinutes + journeyHours * 60;
	}


	/**
	 * This method will return the Booking Class map under which seats are available
	 *
	 * @param tripInfoType
	 * @param searchQuery
	 * @return
	 */


	protected static Map<String, Integer> getAvailableBookingClass(TripInfoType tripInfoType,
			AirSearchQuery searchQuery) {
		Map<String, Integer> seatMap = new HashMap<>();
		int index = 0;
		for (SegmentInfoType segmentInfoType : tripInfoType.getSegmentInfo()) {
			for (SegmentAvailabilityType segmentAvailability : segmentInfoType.getSegmentAvailability()) {
				int paxCount = AirUtils.getPaxCount(searchQuery, false);
				// for first segment
				if (index == 0 && segmentAvailability.getInventoryStatus().equals("AV")
						&& segmentAvailability.getSeatAvailablity() >= paxCount) {
					seatMap.put(segmentAvailability.getBookingClass(), segmentAvailability.getSeatAvailablity());
				} else if (index > 0) {
					if (seatMap.containsKey(segmentAvailability.getBookingClass())) {
						if (segmentAvailability.getInventoryStatus().equals("AV")) {
							int prevValue = seatMap.getOrDefault(segmentAvailability.getBookingClass(), 500);
							int currValue = segmentAvailability.getSeatAvailablity();
							seatMap.put(segmentAvailability.getBookingClass(), Math.min(prevValue, currValue));
						} else {
							// seat not available and it's not first segment , beacuase we have to remove
							// the key from map
							seatMap.remove(segmentAvailability.getBookingClass());
						}
					}
				}
			}
			index++;
		}
		return seatMap;
	}


	protected static FareType getFareType(PricingComponentInfoType[] componentInfoTypes) {
		if (ArrayUtils.isNotEmpty(componentInfoTypes) && StringUtils.isNotBlank(componentInfoTypes[0].getFareType())
				&& componentInfoTypes[0].getFareType().equalsIgnoreCase(FareType.CORPORATE.getName())) {
			return FareType.CORPORATE;
		}
		return FareType.PUBLISHED;
	}

	protected static GenderDetails_Type getGenderFromTitle(String title) {
		if (title.compareToIgnoreCase("Mr") == 0 || title.compareToIgnoreCase("Mstr") == 0
				|| title.compareToIgnoreCase("Master") == 0) {
			return GenderDetails_Type.M;
		} else if (title.compareToIgnoreCase("Ms") == 0 || title.compareToIgnoreCase("Mrs") == 0
				|| title.compareToIgnoreCase("Miss") == 0) {
			return GenderDetails_Type.F;
		}
		return null;
	}

	protected static NameTitle_Type getPaxTitle(String title) {
		if (title.compareToIgnoreCase("Mr") == 0) {
			return NameTitle_Type.MR;
		} else if (title.compareToIgnoreCase("Mstr") == 0) {
			return NameTitle_Type.MSTR;
		} else if (title.compareToIgnoreCase("Master") == 0) {
			return NameTitle_Type.MSTR;
		} else if (title.compareToIgnoreCase("Ms") == 0) {
			return NameTitle_Type.MS;
		} else if (title.compareToIgnoreCase("Mrs") == 0) {
			return NameTitle_Type.MRS;
		} else if (title.compareToIgnoreCase("Miss") == 0) {
			return NameTitle_Type.MISS;
		} else if (title.compareToIgnoreCase("Dr") == 0) {
			return NameTitle_Type.DR;
		} else
			return null;
	}


	public static PaxDetails_Type getPaxTypeDetail(FlightTravellerInfo travellerInfo) {
		PaxType paxType = travellerInfo.getPaxType();
		if (PaxDetails_Type._ADULT.equals(paxType.name())) {
			return PaxDetails_Type.ADULT;
		} else if (PaxDetails_Type._CHILD.equals(paxType.name())) {
			return PaxDetails_Type.CHILD;
		} else if (PaxDetails_Type._INFANT.equals(paxType.name())) {
			return PaxDetails_Type.INFANT;
		}
		return PaxDetails_Type.ADULT;
	}

	public static Calendar getDOB(FlightTravellerInfo travellerInfo) {
		if (travellerInfo.getDob() == null) {
			return null;
		}
		return TgsDateUtils.getCalendar(travellerInfo.getDob());
	}
}
