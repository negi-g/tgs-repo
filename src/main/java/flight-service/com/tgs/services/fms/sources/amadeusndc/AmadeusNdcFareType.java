package com.tgs.services.fms.sources.amadeusndc;

import lombok.Getter;
import java.util.Arrays;
import java.util.List;
import com.tgs.services.base.enums.FareType;

@Getter
public enum AmadeusNdcFareType {

	FLEXI(FareType.FLEXI, Arrays.asList("Y", "B", "E", "S", "T", "Z", "C", "J", "F", "A")),
	STANDARD(FareType.STANDARD, Arrays.asList("M", "H", "W", "L", "P", "U")),
	VALUE(FareType.VALUE, Arrays.asList("Q", "N", "R", "D")),
	LITE(FareType.LITE, Arrays.asList("V", "K"));

	private FareType fareType;

	private List<String> supportedClasses;

	private AmadeusNdcFareType(FareType fareType, List<String> supportedClasses) {
		this.fareType = fareType;
		this.supportedClasses = supportedClasses;
	}

	public static FareType getFareTypeFromCode(String code) {
		for (AmadeusNdcFareType amadeusNdcFareType : AmadeusNdcFareType.values()) {
			if (amadeusNdcFareType.getSupportedClasses().contains(code)) {
				return amadeusNdcFareType.getFareType();
			}
		}
		return null;
	}

}
