package com.tgs.services.fms.sources.flightroutes24;

import java.util.List;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.utils.encryption.EncryptionUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FlightRoutes24Utils {
	public static String getGender(PaxType paxType, String paxTitle) {
		if (paxType.equals(PaxType.ADULT) || paxType.equals(PaxType.CHILD)) {
			if (paxTitle.equalsIgnoreCase("mr") || paxTitle.equalsIgnoreCase("mstr")
					|| paxTitle.equalsIgnoreCase("mstr") || paxTitle.equalsIgnoreCase("master"))
				return "M";
			else
				return "F";
		}
		return null;
	}

	public static boolean isReturnFlight(List<SegmentInfo> segments) {
		for (SegmentInfo segment : segments) {
			if (segment.getIsReturnSegment()) {
				return true;
			}
		}
		return false;
	}

	public static String getEncryptedKey(String secretKey, SegmentInfo segmentInfo) {
		try {
			return EncryptionUtils.encryptUsingCBC(secretKey,
					segmentInfo.getPriceInfoList().get(0).getMiscInfo().getFareKey());
		} catch (Exception e) {
			log.error("Encryption failed for fare key {} ", secretKey, e);
		}
		return null;
	}
}
