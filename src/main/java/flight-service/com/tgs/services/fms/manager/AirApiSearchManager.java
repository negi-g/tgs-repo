package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.restmodel.AirSearchRequest;
import com.tgs.services.fms.restmodel.AirSearchResponse;
import com.tgs.services.fms.servicehandler.AirSearchHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Service
public class AirApiSearchManager {

	@Autowired
	AirSearchHandler airSearchHandler;

	public AirSearchResponse doSearch(AirSearchRequest searchRequest) throws Exception {
		airSearchHandler.initData(searchRequest, new AirSearchResponse());
		return airSearchHandler.getResponse();
	}

	public AirSearchResponse fetchSearchResponse(AirSearchRequest searchRequest) {
		AirSearchResponse searchResponse = new AirSearchResponse();
		String searchId = searchRequest.getSearchId();
		try {
			boolean isSuccess = false;
			boolean isRetry = true;
			int attempt = 0;
			while (!isSuccess && isRetry) {
				attempt++;
				searchResponse = doSearch(searchRequest);
				if (searchResponse.getRetryInSecond() != null) {
					Thread.sleep(100);
				} else {
					log.debug("Search Result found in attempt No : {} for searchId {}", attempt, searchId);
					isSuccess = true;
					isRetry = false;
				}
			}
		} catch (Exception e) {
			log.error("Error Occured during search due to {}", searchId, e);
		}
		return searchResponse;
	}

	public AirSearchResult processSearchResponse(AirSearchQuery searchQuery, AirSearchResult searchResult,
			AirSearchResponse searchResponse) {

		if (searchResponse.getSearchResult() != null
				&& MapUtils.isNotEmpty(searchResponse.getSearchResult().getTripInfos())) {
			if (searchQuery.isDomesticMultiCity()) {
				// last char is index of routeinfo index
				String index = searchResponse.getSearchId().substring(searchResponse.getSearchId().length() - 1);
				searchResult.getTripInfos().put(index, searchResponse.getSearchResult().getTripInfos()
						.getOrDefault(TripInfoType.ONWARD.getName(), new ArrayList<>()));
			} else {
				Map<String, List<TripInfo>> tripTypeMap = searchResponse.getSearchResult().getTripInfos();
				for (Entry<String, List<TripInfo>> tripType : tripTypeMap.entrySet()) {
					if (CollectionUtils.isNotEmpty(tripType.getValue())) {
						for (TripInfo tripInfo : tripType.getValue()) {
							searchResult.addTripInfo(tripType.getKey(), tripInfo);
						}
					}
				}
			}
		}
		return searchResult;
	}
}
