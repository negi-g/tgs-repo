package com.tgs.services.fms.sources.amadeusndc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.utils.exception.air.SupplierSessionException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;

public class AmadeusNdcHeaderHandler implements SOAPHandler<SOAPMessageContext> {

	private String soapAction;
	private SupplierConfiguration supplierConf;

	private static Map<String, String> soapActions = new HashMap<>();

	public AmadeusNdcHeaderHandler(SupplierConfiguration supplierConf, String soapAction) {
		this.soapAction = soapAction;
		this.supplierConf = supplierConf;
	}

	// Soap Element Name
	private static final String _TO_ = "To";
	private static final String _NONCE_ = "Nonce";
	private static final String _ACTION_ = "Action";
	private static final String _CREATED_ = "Created";
	private static final String _USERNAME_ = "Username";
	private static final String _PASSWORD_ = "Password";
	private static final String _SECURITY_ = "Security";
	private static final String _MESSAGEID_ = "MessageID";
	private static final String _USERNAME_TOKEN_ = "UsernameToken";

	// Header PREFIX
	private static final String WSA_PREFIX = "wsa";
	private static final String WSU_PREFIX = "wsu";
	private static final String WSSE_PREFIX = "wsse";

	// Name spaces
	private static final String WS_ADD_NS = "http://www.w3.org/2005/08/addressing";
	private static final String WS_SEC_NU =
			"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
	private static final String ENCODING_TYPE =
			"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary";
	private static final String TYPE =
			"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest";
	private static final String WS_SEC_NS =
			"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";

	private static final String MESSAGE_DIGEST_ALGORITHM_NAME_SHA_1 = "SHA-1";

	// Soap Actions
	public static final String _AIR_SHOPPING_ = "AIR_SHOPPING";
	public static final String _OFFER_PRICE_ = "OFFER_PRICE";
	public static final String _SERVICE_LIST_ = "SERVICE_LIST";
	public static final String _SEAT_AVAILABILITY_ = "SEAT_AVAILABILITY";
	public static final String _ORDER_RETRIEVE_ = "ORDER_RETRIEVE";
	public static final String _ORDER_RESHOP_ = "ORDER_RESHOP";
	public static final String _ORDER_CANCEL_ = "ORDER_CANCEL";
	public static final String _ORDER_CREATE_ = "ORDER_CREATE";
	public static final String _ORDER_CHANGE_ = "ORDER_CHANGE";


	static {
		soapActions.put(_AIR_SHOPPING_, "http://webservices.amadeus.com/NDC_AirShopping_18.1");
		soapActions.put(_OFFER_PRICE_, "http://webservices.amadeus.com/NDC_OfferPrice_18.1");
		soapActions.put(_SERVICE_LIST_, "http://webservices.amadeus.com/NDC_ServiceList_18.1");
		soapActions.put(_SEAT_AVAILABILITY_, "http://webservices.amadeus.com/NDC_SeatAvailability_18.1");
		soapActions.put(_ORDER_RESHOP_, "http://webservices.amadeus.com/NDC_OrderReshop_18.1");
		soapActions.put(_ORDER_CANCEL_, "http://webservices.amadeus.com/NDC_OrderCancel_18.1");
		soapActions.put(_ORDER_CREATE_, "http://webservices.amadeus.com/NDC_OrderCreate_18.1");
		soapActions.put(_ORDER_CHANGE_, "http://webservices.amadeus.com/NDC_OrderChange_18.1");
		soapActions.put(_ORDER_RETRIEVE_, "http://webservices.amadeus.com/NDC_OrderRetrieve_18.1");

	}

	public boolean handleMessage(SOAPMessageContext context) {
		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (outboundProperty) {
			try {
				AmadeusNdcSessionInfo securityInfo = this.getSecurityInfo();
				SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
				SOAPHeader header = envelope.getHeader();

				if (header == null) {
					header = envelope.addHeader();
				}
				// to check if header elements are already added or not
				int counter = 0;
				Iterator it = header.getChildElements();
				while (it.hasNext()) {
					counter = counter + 1;
					it.next();
				}
				if (counter >= 5) {
					return true;
				}

				SOAPElement security = header.addChildElement(_SECURITY_, WSSE_PREFIX, WS_SEC_NS);

				SOAPElement usernameToken = security.addChildElement(_USERNAME_TOKEN_, WSSE_PREFIX);
				usernameToken.addAttribute(new QName("xmlns:wsu"), WS_SEC_NU);

				SOAPElement username = usernameToken.addChildElement(_USERNAME_, WSSE_PREFIX);
				username.addTextNode(supplierConf.getSupplierCredential().getUserName());

				SOAPElement password = usernameToken.addChildElement(_PASSWORD_, WSSE_PREFIX);
				password.setAttribute("Type", TYPE);
				password.addTextNode(securityInfo.getPassword());

				SOAPElement nonce = usernameToken.addChildElement(_NONCE_, WSSE_PREFIX);
				nonce.setAttribute("EncodingType", ENCODING_TYPE);
				nonce.addTextNode(securityInfo.getNonce());

				SOAPElement created = usernameToken.addChildElement(_CREATED_, WSU_PREFIX);
				created.addTextNode(securityInfo.getCreated());

				SOAPElement messageId = header.addChildElement(_MESSAGEID_, WSA_PREFIX, WS_ADD_NS);
				messageId.addTextNode("uuid:" + UUID.randomUUID());

				SOAPElement action = header.addChildElement(_ACTION_, WSA_PREFIX, WS_ADD_NS);
				action.addTextNode(soapActions.get(soapAction));

				SOAPElement to = header.addChildElement(_TO_, WSA_PREFIX, WS_ADD_NS);
				to.addTextNode(supplierConf.getSupplierCredential().getUrl());

			} catch (SOAPException e) {
				throw new SupplierUnHandledFaultException(e.getMessage());
			}
		}
		return true;
	}

	public boolean handleFault(SOAPMessageContext context) {
		return false;
	}

	public void close(MessageContext context) {}

	@Override
	public Set<QName> getHeaders() {
		return null;
	}

	public AmadeusNdcSessionInfo getSecurityInfo() {
		AmadeusNdcSessionInfo sessionInfo = AmadeusNdcSessionInfo.builder().build();
		try {

			SecureRandom rand = SecureRandom.getInstance("SHA1PRNG");
			rand.setSeed(System.currentTimeMillis());
			byte[] nonceBytes = new byte[16];
			rand.nextBytes(nonceBytes);

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			df.setTimeZone(TimeZone.getTimeZone("UTC"));
			// df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			String createdDate = df.format(Calendar.getInstance().getTime());
			byte[] createdDateBytes = createdDate.getBytes();
			// Make the password
			byte[] passwordBytes = supplierConf.getSupplierCredential().getPassword().getBytes();
			ByteArrayOutputStream pwdBaos = new ByteArrayOutputStream();
			pwdBaos.write(passwordBytes);
			MessageDigest clearPwd = MessageDigest.getInstance(MESSAGE_DIGEST_ALGORITHM_NAME_SHA_1);
			byte[] sha1Pwd = clearPwd.digest(pwdBaos.toByteArray());

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write(nonceBytes);
			baos.write(createdDateBytes);
			baos.write(sha1Pwd);

			MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST_ALGORITHM_NAME_SHA_1);
			byte[] digestedPassword = md.digest(baos.toByteArray());

			String passwordB64 = Base64.getEncoder().encodeToString(digestedPassword);
			String nonceB64 = Base64.getEncoder().encodeToString(nonceBytes);

			sessionInfo.setPassword(passwordB64);
			sessionInfo.setNonce(nonceB64);
			sessionInfo.setCreated(createdDate);
		} catch (NoSuchAlgorithmException | IOException nos) {
			throw new SupplierSessionException(nos.getMessage());
		}
		return sessionInfo;
	}

}
