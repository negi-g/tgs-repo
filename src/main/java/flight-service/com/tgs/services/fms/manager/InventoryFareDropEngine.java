package com.tgs.services.fms.manager;

import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class InventoryFareDropEngine {

	final private static List<String> INVENTORY_FARE_TYPES =
			Arrays.asList(FareType.OFFER_FARE_WITH_PNR.toString(), FareType.OFFER_FARE_WITHOUT_PNR.toString());

	public void applyInventoryFareDrop(TripInfo tripInfo, User user) {

		try {
			List<PriceInfo> inventoryFares = tripInfo.getTripPriceInfos().stream()
					.filter(pInfo -> AirSourceType.DEALINVENTORY.getSourceId() == pInfo.getSourceId())
					.collect(Collectors.toList());

			log.debug("Price infos from deal inventories for trip {} are : {}", tripInfo, inventoryFares);

			if (CollectionUtils.isNotEmpty(inventoryFares)) {

				List<PriceInfo> publishedFares = new ArrayList<>();

				// ComparitivePriceInfo is used to reapply fare diff at the time of review
				PriceInfo comparitivePriceInfo =
						tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getComparitivePriceInfo();

				publishedFares = (comparitivePriceInfo != null) ? Arrays.asList(comparitivePriceInfo)
						: tripInfo.getTripPriceInfos().stream()
								.filter(priceInfo -> (!INVENTORY_FARE_TYPES.contains(priceInfo.getFareType()))
										|| !(AirSourceType.DEALINVENTORY.getSourceId() == priceInfo.getSourceId()))
								.collect(Collectors.toList());

				log.debug("Price infos of published fare for trip {} are : {}", tripInfo, publishedFares);

				if (CollectionUtils.isNotEmpty(publishedFares)) {
					PriceInfo lowestPublishedFare = lowestPrice(publishedFares);
					inventoryFares.forEach(inventoryFare -> {
						if (inventoryFare.getMiscInfo().getInventoryDiscount() > 0D) {
							applyFareDiff(tripInfo, inventoryFare, lowestPublishedFare);
						}
					});
				}
			}
		} catch (Exception exp) {
			log.error("Error occured while applying fare drop for trip {}", tripInfo);
		} finally {
			tripInfo.setTripPriceInfos(AirUtils.getTripTotalPriceInfoList(tripInfo, user));
		}
	}

	private void applyFareDiff(TripInfo tripInfo, PriceInfo inventoryFare, PriceInfo publishedFare) {

		double dealInventoryFare = inventoryFare.getTotalAirlineFare();
		double lowestPublishedFare = publishedFare.getTotalAirlineFare();

		double fareDifference = dealInventoryFare - lowestPublishedFare;

		log.debug("Fare diff between inventory and published fare is {} for inventory price info {} and published {}",
				fareDifference, inventoryFare, publishedFare);
		if (fareDifference >= 0D) {

			Double discount = inventoryFare.getMiscInfo().getInventoryDiscount();
			Double doubleDiscount =
					ObjectUtils.firstNonNull(inventoryFare.getMiscInfo().getInventoryDoubleDiscount(), 0d);
			PriceInfo secondLeastPriceInfo = inventoryFare.getMiscInfo().getSecondLeastPriceInfo();
			AtomicDouble secondLeastNetFare = new AtomicDouble(Double.MAX_VALUE);
			if (Objects.nonNull(secondLeastPriceInfo)) {
				Double invDiscount = secondLeastPriceInfo.getMiscInfo().getInventoryDiscount();
				secondLeastNetFare
						.set(secondLeastPriceInfo.getFareDetail(PaxType.ADULT).getFareComponents().get(FareComponent.TF)
								- invDiscount);
				log.debug(
						"Second least price info is {}, second least inventory discount is {}, and second least net fare is {}",
						secondLeastPriceInfo, invDiscount, secondLeastNetFare.get());
			}
			AtomicDouble finalDiscount = new AtomicDouble(0d);

			log.debug("Discount for inventory is {} and double discount is {}", discount, doubleDiscount);

			List<PriceInfo> priceList = tripInfo.getSegmentInfos().get(0).getPriceInfoList();

			priceList.forEach(priceInfo -> {
				if (isSamePriceInfoFromSameSupplier(priceInfo, inventoryFare)) {

					FareDetail adultFareDetail = priceInfo.getFareDetail(PaxType.ADULT);

					Double publishedGrossFare = publishedFare.getFareDetail(PaxType.ADULT).getAirlineFare();
					Double inventoryGrossFare = adultFareDetail.getAirlineFare();
					Double differentialMilestone = Math.min(publishedGrossFare, secondLeastNetFare.get());
					Double fareDiff = inventoryGrossFare - differentialMilestone;
					log.debug("Differential milestone is {}, inventory gross fare is {}, published gross fare is {}",
							differentialMilestone, inventoryGrossFare, publishedGrossFare);

					// Applying Fare Diff logic for ADULT paxType only, because fare of adult and child will be same
					if (fareDiff <= discount) {
						finalDiscount.set(fareDiff + doubleDiscount);
					} else {
						finalDiscount.set(discount);
					}

					priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
						// Applying inventory discount only for ADULT and CHILD
						if (!PaxType.INFANT.equals(paxType)) {
							Double baseFare = fareDetail.getFareComponents().getOrDefault(FareComponent.BF, 0D);
							baseFare -= finalDiscount.get();
							fareDetail.getFareComponents().put(FareComponent.BF, baseFare);
							fareDetail.getFareComponents().put(FareComponent.IDS, finalDiscount.get());
							Double totalFare = fareDetail.getAirlineFare();
							fareDetail.getFareComponents().put(FareComponent.TF, totalFare);
						}
					});

					// Save published priceInfo in the trip Info for reference at the time of review
					priceInfo.getMiscInfo().setComparitivePriceInfo(publishedFare);
				}
			});
		}
	}

	private PriceInfo lowestPrice(List<PriceInfo> publishedFarePriceInfos) {
		publishedFarePriceInfos.sort(Comparator.comparingDouble(PriceInfo::getTotalAirlineFare));
		log.debug("Lowest comparative price info of all non deal inventory fares is {}",
				publishedFarePriceInfos.get(0));
		return publishedFarePriceInfos.get(0);
	}

	private boolean isSamePriceInfoFromSameSupplier(PriceInfo oldPriceInfo, PriceInfo newPriceInfo) {
		return AirUtils.isSamePriceInfo(oldPriceInfo, newPriceInfo)
				&& oldPriceInfo.getSourceId() == newPriceInfo.getSourceId();
	}
}
