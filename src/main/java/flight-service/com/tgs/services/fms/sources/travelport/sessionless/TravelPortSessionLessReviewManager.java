package com.tgs.services.fms.sources.travelport.sessionless;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRMiscInfo;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import travelport.www.schema.air_v47_0.ProviderDefinedType_type1;
import travelport.www.schema.air_v47_0.AirItinerary_type0;
import travelport.www.schema.air_v47_0.AirPriceReq;
import travelport.www.schema.air_v47_0.AirPriceResult_type0;
import travelport.www.schema.air_v47_0.AirPriceRsp;
import travelport.www.schema.air_v47_0.AirPricingCommand_type0;
import travelport.www.schema.air_v47_0.AirPricingInfo_type0;
import travelport.www.schema.air_v47_0.AirPricingModifiers_type0;
import travelport.www.schema.air_v47_0.AirPricingSolution_type0;
import travelport.www.schema.air_v47_0.BaggageAllowanceInfo_type0;
import travelport.www.schema.air_v47_0.BaggageAllowances_type0;
import travelport.www.schema.air_v47_0.BookingInfo_type0;
import travelport.www.schema.air_v47_0.FareInfo_type0;
import travelport.www.schema.air_v47_0.OptionalService_type0;
import travelport.www.schema.air_v47_0.OptionalServices_type0;
import travelport.www.schema.air_v47_0.PassengerType_type0;
import travelport.www.schema.air_v47_0.TypeBaseAirSegment;
import travelport.www.schema.common_v47_0.HostToken_type0;
import travelport.www.schema.common_v47_0.SearchPassenger_type0;
import travelport.www.schema.common_v47_0.ServiceData_type0;
import travelport.www.service.air_v47_0.AirFaultMessage;
import travelport.www.service.air_v47_0.AirServiceStub;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Getter
@Setter
@SuperBuilder
final class TravelPortSessionLessReviewManager extends TravelPortSessionLessServiceManager {

	private List<TripInfo> tripInfoList;
	private List<TypeBaseAirSegment> airSegment;
	private Map<String, String> hostTokenMap;

	public TripInfo reviewFlight(TripInfo selectedTrip, boolean isAirPricingCommandRequired) {
		bookingId = bookingId != null ? bookingId : searchQuery.getSearchId();
		TripInfo reviewedTrip = null;
		tripInfoList = selectedTrip.splitTripInfo(false);
		AirPriceReq airPriceReq = buildAirPriceRequest(selectedTrip, isAirPricingCommandRequired);
		AirPriceRsp airPriceRsp = null;
		try {
			airPriceRsp = getAirPriceResponse(airPriceReq, "Air Price");
			airSegment = Arrays.asList(airPriceRsp.getAirItinerary().getAirSegment());
			if (airPriceRsp != null) {
				reviewedTrip = parseAirPriceResponse(airPriceRsp, selectedTrip);
			}
		} catch (Exception e) {
			throw new NoSeatAvailableException(e.getMessage());
		}
		return reviewedTrip;
	}

	private TripInfo parseAirPriceResponse(AirPriceRsp airPriceRsp, TripInfo selectedTrip) {
		TripInfo reviewedTrip = null;
		reviewedTrip = new GsonMapper<>(selectedTrip, TripInfo.class).convert();
		AirPricingSolution_type0 airPricingSolution = getAppropriateAirPricingSolution(airPriceRsp, selectedTrip);
		buildHostTokenMap(airPriceRsp.getAirItinerary().getHostToken());
		updateTripInfo(airPricingSolution, reviewedTrip);
		return reviewedTrip;
	}

	private void buildHostTokenMap(HostToken_type0[] hostToken) {
		if (hostTokenMap == null) {
			hostTokenMap = new HashMap<>();
		}
		Arrays.asList(hostToken).forEach(token -> {
			hostTokenMap.put(token.getKey().getTypeRef(), token.getString());
		});
	}

	private void updateTripInfo(AirPricingSolution_type0 airPricingSolution, TripInfo reviewedTrip) {
		SegmentInfo firstSegment = new GsonMapper<>(reviewedTrip.getSegmentInfos().get(0), SegmentInfo.class).convert();
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		priceInfo.setMiscInfo(firstSegment.getPriceInfoList().get(0).getMiscInfo());
		resetSegmentPriceFares(reviewedTrip);
		String segmentRefKey = null;
		for (int segmentNumber = 0; segmentNumber < reviewedTrip.getSegmentInfos().size(); segmentNumber++) {
			SegmentInfo segment = reviewedTrip.getSegmentInfos().get(segmentNumber);
			PriceInfo copyPriceInfo = getPriceInfo(segment, priceInfo);
			if (segment.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
				segmentRefKey = getAndUpdateSegmentKey(segment, airSegment, copyPriceInfo);
			}
			Map<PaxType, FareDetail> fareDetails = new HashMap<>();

			/**
			 * Storing airPricing keys and Booking traveller keys for booking request.
			 **/
			Map<PaxType, List<String>> paxPricingInfoKeyMap = new HashMap<>();
			Map<PaxType, String> fareInfoKeyMap = new HashMap<>();
			Map<PaxType, List<String>> bookingTravellerInfoRefMap = new HashMap<>();
			for (AirPricingInfo_type0 airPricingInfo : airPricingSolution.getAirPricingInfo()) {
				String airPriceKey = airPricingInfo.getKey().getTypeRef();
				PassengerType_type0 passengerType = Arrays.asList(airPricingInfo.getPassengerType()).get(0);
				PaxType paxType = TravelPortSessionLessUtils.getPaxType(passengerType.getCode().getTypePTC());
				List<String> paxKeyList = null;
				if (paxPricingInfoKeyMap.containsKey(paxType)) {
					paxKeyList = paxPricingInfoKeyMap.get(paxType);
				} else {
					paxKeyList = new ArrayList<>();
				}
				paxKeyList.add(airPriceKey);
				paxPricingInfoKeyMap.put(paxType, paxKeyList);
				List<String> travellerKeyList = new ArrayList<>();
				for (PassengerType_type0 passenger : airPricingInfo.getPassengerType()) {
					travellerKeyList.add(passenger.getBookingTravelerRef());
				}
				bookingTravellerInfoRefMap.put(paxType, travellerKeyList);
				List<BookingInfo_type0> bookingInfoList = Arrays.asList(airPricingInfo.getBookingInfo());
				List<FareInfo_type0> fareInfoList = Arrays.asList(airPricingInfo.getFareInfo());
				BookingInfo_type0 bookingInfo = getBookingInfoFromSegment(segmentRefKey, bookingInfoList);
				FareInfo_type0 fareInfo =
						getFareInfoFromBookingInfo(fareInfoList, bookingInfo.getFareInfoRef().getTypeRef());
				if (bookingInfo.getBookingCode().equals("N")
						&& StringUtils.isNotBlank(firstSegment.getPriceInfoList().get(0).getSpecialReturnIdentifier())
						&& CollectionUtils.isNotEmpty(
								firstSegment.getPriceInfoList().get(0).getMatchedSpecialReturnIdentifier())) {
					copyPriceInfo.setFareIdentifier(FareType.SPECIAL_RETURN);
				} else {
					setFareIdentifier(fareInfo, copyPriceInfo, bookingInfo.getBookingCode());
				}
				// storing Host token keys to be passed in booking request.
				setHostTokenKeys(copyPriceInfo.getMiscInfo(), bookingInfo.getHostTokenRef().getTypeRef());
				if (Objects.nonNull(bookingInfo)) {
					FareDetail fareDetail =
							getFareDetail(airPricingInfo, bookingInfo, segment, segmentNumber, fareInfo, paxType);
					fareInfoKeyMap.put(paxType, fareInfo.getKey().getTypeRef());
					fareDetails.put(paxType, fareDetail);
				}
			}
			PriceMiscInfo miscInfo = copyPriceInfo.getMiscInfo();
			miscInfo.setPaxPricingInfo(paxPricingInfoKeyMap);
			miscInfo.setBookingTravellerInfoRef(bookingTravellerInfoRefMap);
			miscInfo.setAirPriceSolutionKey(airPricingSolution.getKey().getTypeRef());
			miscInfo.setFareInfoKeyMap(fareInfoKeyMap);
			copyPriceInfo.setFareDetails(fareDetails);
			setSSRInfo(segment, segmentRefKey, airPricingSolution.getOptionalServices(), bookingTravellerInfoRefMap);
			segment.setPriceInfoList(Arrays.asList(copyPriceInfo));
		}
	}

	private void setHostTokenKeys(PriceMiscInfo miscInfo, String hostKeyRef) {
		StringJoiner hostToken = new StringJoiner(",");
		hostToken.add(hostKeyRef);
		hostToken.add(hostTokenMap.get(hostKeyRef));
		miscInfo.setTokenId(hostToken.toString());
	}

	private void setSSRInfo(SegmentInfo segment, String segmentRefKey, OptionalServices_type0 optionalServices,
			Map<PaxType, List<String>> bookingTravellerInfoRefMap) {

		String adultPaxKey = bookingTravellerInfoRefMap.get(PaxType.ADULT).get(0);

		Map<SSRType, List<? extends SSRInformation>> ssrInfo = new HashMap<>();
		List<BaggageSSRInformation> baggageSSRInfos = new ArrayList<>();
		List<MealSSRInformation> mealSSRInformations = new ArrayList<>();

		Map<SSRType, List<? extends SSRInformation>> preSSR = AirUtils.getPreSSR(getSSRFlightFact(segment));

		for (OptionalService_type0 service : optionalServices.getOptionalService()) {

			if (isSegmentKeyPresent(segmentRefKey, service.getServiceData())
					&& isKeyPresent(adultPaxKey, service.getServiceData())) {

				if (preSSR != null && isBaggage(service)) {
					SSRInformation bagInfo = AirUtils.getSSRInfo(preSSR.get(SSRType.BAGGAGE),
							service.getProviderDefinedType().getProviderDefinedType_type1());
					if (bagInfo != null) {
						baggageSSRInfos.add(getBaggageInfo(service, segment.getSegmentNum()));
					}
				}

				if (isMainLeg(segment) && isMeal(service)) {
					mealSSRInformations.add(getMealInfos(service));
				}
			}
		}
		if (CollectionUtils.isNotEmpty(baggageSSRInfos))
			ssrInfo.put(SSRType.BAGGAGE, baggageSSRInfos);
		if (CollectionUtils.isNotEmpty(mealSSRInformations))
			ssrInfo.put(SSRType.MEAL, mealSSRInformations);
		segment.setSsrInfo(ssrInfo);
	}

	private boolean isMainLeg(SegmentInfo segment) {
		return Objects.equals(segment.getPriceInfo(0).getMiscInfo().getLegNum(), Integer.valueOf(0));
	}

	private boolean isBaggage(OptionalService_type0 service) {
		return StringUtils.equalsIgnoreCase(service.getType().getTypeMerchandisingService(),
				TravelPortSessionLessConstants.BAGGAGE);
	}

	private boolean isMeal(OptionalService_type0 service) {
		return StringUtils.equalsIgnoreCase(service.getType().getTypeMerchandisingService(),
				TravelPortSessionLessConstants.MEAL_OR_BEVERAGE);
	}

	private boolean isSegmentKeyPresent(String segmentRefKey, ServiceData_type0[] serviceDatas) {
		for (ServiceData_type0 serviceData : serviceDatas) {
			if (segmentRefKey.equals(serviceData.getAirSegmentRef().getTypeRef())) {
				return true;
			}
		}
		return false;
	}

	private boolean isKeyPresent(String adultPaxKey, ServiceData_type0[] serviceDatas) {
		for (ServiceData_type0 serviceData : serviceDatas) {
			if (adultPaxKey.equals(serviceData.getBookingTravelerRef().getTypeRef())) {
				return true;
			}
		}
		return false;
	}

	private MealSSRInformation getMealInfos(OptionalService_type0 service) {
		MealSSRInformation mealInfo = new MealSSRInformation();
		mealInfo.setAmount(getAmountBasedOnCurrency(service.getTotalPrice().getTypeMoney(),
				service.getTotalPrice().getTypeMoney().substring(0, 3), 1.0));
		mealInfo.setCode(TravelPortSessionLessUtils.getSSRCode(service));
		mealInfo.setDesc(service.getDisplayText());
		mealInfo.setMiscInfo(buildSSRMiscInfo(service));
		return mealInfo;
	}

	private BaggageSSRInformation getBaggageInfo(OptionalService_type0 service, int segNum) {
		BaggageSSRInformation baggageInfo = new BaggageSSRInformation();
		if (segNum == 0)
			baggageInfo.setAmount(getAmountBasedOnCurrency(service.getTotalPrice().getTypeMoney(),
					service.getTotalPrice().getTypeMoney().substring(0, 3), 1.0));
		baggageInfo.setCode(TravelPortSessionLessUtils.getSSRCode(service));
		baggageInfo.setDesc(service.getDisplayText());
		baggageInfo.setMiscInfo(buildSSRMiscInfo(service));
		return baggageInfo;
	}

	private SSRMiscInfo buildSSRMiscInfo(OptionalService_type0 service) {
		return SSRMiscInfo.builder().seatCodeType(service.getProviderDefinedType().getProviderDefinedType_type1())
				.pricingCurrency(service.getTotalPrice().getTypeMoney().substring(0, 3)).build();
	}

	public PriceInfo getPriceInfo(SegmentInfo segmentInfo, PriceInfo defaultPriceInfo) {
		if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
			return segmentInfo.getPriceInfo(0);
		}
		return new GsonMapper<>(defaultPriceInfo, PriceInfo.class).convert();
	}

	private FareDetail getFareDetail(AirPricingInfo_type0 airPricingInfo, BookingInfo_type0 bookingInfo,
			SegmentInfo segment, int segmentNumber, FareInfo_type0 fareInfo, PaxType paxType) {
		double dividingFactor = 1.0;
		String classOfBook = bookingInfo.getBookingCode();
		FareDetail fare = new FareDetail();
		fare.setCabinClass(TravelPortSessionLessUtils.getCabinClass(bookingInfo));
		fare.setClassOfBooking(classOfBook);
		fare.setFareBasis(fareInfo.getFareBasis());
		fare.setRefundableType(TravelPortSessionLessUtils.getRefundableType(airPricingInfo));
		if (segmentNumber == 0) {
			double baseFare = getEquivalentBaseFare(airPricingInfo, dividingFactor)
					+ getEquivalentFees(airPricingInfo, dividingFactor);
			double totalFare = getEquivalentTotalfare(airPricingInfo, dividingFactor);
			fare.getFareComponents().put(FareComponent.BF, baseFare);
			fare.getFareComponents().put(FareComponent.TF, totalFare);
			if (Objects.nonNull(airPricingInfo.getTaxInfo())) {
				setTaxDetails(fare, Arrays.asList(airPricingInfo.getTaxInfo()), dividingFactor);
			}
		}
		if (StringUtils.isNotBlank(bookingInfo.getBookingCount())) {
			fare.setSeatRemaining(Integer.valueOf(bookingInfo.getBookingCount()));
		}
		setBaggageAllowance(fare, airPricingInfo, segment);
		if (StringUtils.isNotBlank(bookingInfo.getBookingCount())) {
			fare.setSeatRemaining(Integer.valueOf(bookingInfo.getBookingCount()));
		}
		return fare;
	}

	/*
	 * This logic will fail for nearbyAirport as there general purpose output don't have all the nearby airport
	 * configuration.
	 */
	private void setBaggageAllowance(FareDetail fare, AirPricingInfo_type0 airPricingInfo, SegmentInfo segment) {
		try {
			Map<String, String> baggageMap = new LinkedHashMap<String, String>();
			if (!Objects.isNull(airPricingInfo.getBaggageAllowances())) {
				for (int index =
						0; index < airPricingInfo.getBaggageAllowances().getBaggageAllowanceInfo().length; index++) {
					BaggageAllowanceInfo_type0 baggageAllowanceInfo =
							Arrays.asList(airPricingInfo.getBaggageAllowances().getBaggageAllowanceInfo()).get(index);
					String baggageKey =
							baggageAllowanceInfo.getOrigin() + "_" + baggageAllowanceInfo.getDestination() + index;
					baggageMap.put(baggageKey, getBaggageAllowance(baggageAllowanceInfo));
				}

				String tripInfoKey = "";
				for (int index = 0; index < tripInfoList.size(); index++) {
					TripInfo tripInfo = tripInfoList.get(index);
					for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
						if (segmentInfo.getSegmentKey().equals(segment.getSegmentKey())
								&& segmentInfo.getDepartTime().equals(segment.getDepartTime())) {
							tripInfoKey =
									tripInfo.getDepartureAirportCode() + "_" + tripInfo.getArrivalAirportCode() + index;

						}
					}
					if (StringUtils.isNotEmpty(tripInfoKey)) {
						break;
					}
				}
				BaggageInfo baggageInfo = new BaggageInfo();
				baggageInfo.setAllowance(baggageMap.get(tripInfoKey));
				fare.setBaggageInfo(baggageInfo);
			}
		} catch (Exception e) {
			log.error("Baggage Allowance Failed for bookingId {}", bookingId);
			setDefaultBaggage(airPricingInfo, fare);
		} finally {
			if (fare.getBaggageInfo() == null || StringUtils.isBlank(fare.getBaggageInfo().getAllowance())) {
				setDefaultBaggage(airPricingInfo, fare);
			}
		}
	}

	private void setDefaultBaggage(AirPricingInfo_type0 airPricingInfo, FareDetail fare) {
		try {
			if (!Objects.isNull(airPricingInfo.getBaggageAllowances())) {
				BaggageInfo baggageInfo = new BaggageInfo();
				baggageInfo.setAllowance(getBaggageAllowance(airPricingInfo.getBaggageAllowances()));
				fare.setBaggageInfo(baggageInfo);
			}
		} catch (Exception e) {
			log.error("Baggage Allowance Failed for bookingId {}", bookingId);
		}
	}


	private String getBaggageAllowance(BaggageAllowances_type0 baggageAllowances) {
		if (getList(getList(getList(baggageAllowances.getBaggageAllowanceInfo()).get(0).getTextInfo()).get(0).getText())
				.get(0).getTypeGeneralText().contains("P")) {
			return getList(
					getList(getList(baggageAllowances.getBaggageAllowanceInfo()).get(0).getTextInfo()).get(0).getText())
							.get(0).getTypeGeneralText()
					+ "iece";
		}
		return getList(
				getList(getList(baggageAllowances.getBaggageAllowanceInfo()).get(0).getTextInfo()).get(0).getText())
						.get(0).getTypeGeneralText()
				+ "G";
	}

	private String getBaggageAllowance(BaggageAllowanceInfo_type0 baggageAllowanceInfo) {
		if (getList(getList(baggageAllowanceInfo.getTextInfo()).get(0).getText()).get(0).getTypeGeneralText()
				.contains("P")) {
			return getList(getList(baggageAllowanceInfo.getTextInfo()).get(0).getText()).get(0).getTypeGeneralText()
					+ "iece";
		}
		return getList(getList(baggageAllowanceInfo.getTextInfo()).get(0).getText()).get(0).getTypeGeneralText() + "G";
	}


	private void resetSegmentPriceFares(TripInfo selectedTrip) {
		selectedTrip.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.getPriceInfoList().get(0).setFareDetails(null);
		});
	}

	private AirPricingSolution_type0 getAppropriateAirPricingSolution(AirPriceRsp airPriceRsp, TripInfo selectedTrip) {
		AirPricingSolution_type0 appropriateAirPricingSolution = null;
		Double minimumPrice = Double.valueOf(Integer.MAX_VALUE);
		List<AirPricingSolution_type0> filteredGuratedSolutionList =
				getPricingSolutionList(getList(airPriceRsp.getAirPriceResult()));
		boolean isClassChanged = false;
		boolean isPresenceChecked = false;
		boolean isBookingClassPresent = false;
		// select lowest price RBD in case of chosen RBD is not available.
		for (AirPricingSolution_type0 filteredGuratedSolution : filteredGuratedSolutionList) {
			List<BookingInfo_type0> bookingInfoList =
					getList(getList(filteredGuratedSolution.getAirPricingInfo()).get(0).getBookingInfo());
			for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
				if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
					if (StringUtils.isNotEmpty(segmentInfo.getPriceInfoList().get(0).getMiscInfo().getSegmentKey())) {
						String segmentRefKey =
								getAndUpdateSegmentKey(segmentInfo, airSegment, segmentInfo.getPriceInfoList().get(0));
						BookingInfo_type0 bookingInfo = getBookingInfoFromSegment(segmentRefKey, bookingInfoList);
						// FareInfo fareInfo = getFareInfoFromBookingInfo(fareInfoList, bookingInfo.getFareInfoRef());
						FareDetail fareDetail = TravelPortSessionLessUtils.getFareDetail(segmentInfo.getPriceInfo(0));
						/**
						 * Using a flag to ensure that the presence of the booking class is checked only once to avoid
						 * unnecessary iterations.
						 **/
						if (!isPresenceChecked) {
							isPresenceChecked = true;
							isBookingClassPresent = isClassofBookingPresent(filteredGuratedSolutionList,
									fareDetail.getClassOfBooking(), segmentRefKey);
						}
						if (!isClassChanged && bookingInfo.getBookingCode().equals(fareDetail.getClassOfBooking())) {
							appropriateAirPricingSolution = filteredGuratedSolution;
						} else if (isBookingClassPresent) {
							break;
						} else {
							Double totalFare = getEquivalentTotalfare(filteredGuratedSolution, 1.0);
							isClassChanged = true;
							log.info("Class Changed from " + fareDetail.getClassOfBooking() + " to "
									+ bookingInfo.getBookingCode() + " bookingId " + bookingId);
							if (totalFare < minimumPrice) {
								appropriateAirPricingSolution = filteredGuratedSolution;
								minimumPrice = totalFare;
							}
							break;
						}
					}
				}
			}
			if (!isClassChanged && appropriateAirPricingSolution != null) {
				break;
			}
		}
		return appropriateAirPricingSolution;
	}

	private boolean isClassofBookingPresent(List<AirPricingSolution_type0> filteredGuratedSolutionList,
			String classOfBooking, String segmentRefKey) {
		for (AirPricingSolution_type0 filteredGuratedSolution : filteredGuratedSolutionList) {
			List<BookingInfo_type0> bookingInfoList =
					getList(getList(filteredGuratedSolution.getAirPricingInfo()).get(0).getBookingInfo());
			BookingInfo_type0 bookingInfo = getBookingInfoFromSegment(segmentRefKey, bookingInfoList);
			if (bookingInfo.getBookingCode().equals(classOfBooking)) {
				return true;
			}
		}
		return false;
	}

	private List<AirPricingSolution_type0> getPricingSolutionList(List<AirPriceResult_type0> airPriceResult) {
		List<AirPricingSolution_type0> pricingSolutionSelectedList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(airPriceResult)
				&& CollectionUtils.isNotEmpty(Arrays.asList(airPriceResult.get(0).getAirPricingSolution()))) {
			pricingSolutionSelectedList.addAll(getList(airPriceResult.get(0).getAirPricingSolution()));
		}
		return pricingSolutionSelectedList;

	}

	private FareInfo_type0 getFareInfoFromBookingInfo(List<FareInfo_type0> fareInfoList, String fareInfoRef) {
		FareInfo_type0 fareInfo = null;
		for (FareInfo_type0 fareInfoItem : fareInfoList) {
			if (fareInfoItem.getKey().getTypeRef().equals(fareInfoRef)) {
				fareInfo = fareInfoItem;
				break;
			}
		}
		return fareInfo;
	}

	private BookingInfo_type0 getBookingInfoFromSegment(String segmentRefKey, List<BookingInfo_type0> bookingInfoList) {
		BookingInfo_type0 bookingInfo = null;
		for (BookingInfo_type0 bookingInfoItem : bookingInfoList) {
			if (bookingInfoItem.getSegmentRef().getTypeRef().equals(segmentRefKey)) {
				bookingInfo = bookingInfoItem;
				break;
			}
		}
		return bookingInfo;
	}

	private String getAndUpdateSegmentKey(SegmentInfo segmentInfo, List<TypeBaseAirSegment> airSegmentList,
			PriceInfo priceInfo) {
		String segmentRefKey = "";
		for (TypeBaseAirSegment typeBaseAirSegment : airSegmentList) {
			if (segmentInfo.getDepartureAirportCode().equals(typeBaseAirSegment.getOrigin().getTypeIATACode())
					&& TravelPortSessionLessUtils.getStringDateTime(segmentInfo.getDepartTime())
							.equals(TravelPortSessionLessUtils.getStringDateTime(TravelPortSessionLessUtils
									.getIsoDateTime(typeBaseAirSegment.getDepartureTime())))) {
				segmentRefKey = typeBaseAirSegment.getKey().getTypeRef();
				priceInfo.getMiscInfo().setSegmentKey(segmentRefKey);
				break;
			}
		}
		return segmentRefKey;
	}


	private AirPriceReq buildAirPriceRequest(TripInfo selectedTrip, boolean isAirPricingCommandReq) {
		AirPriceReq airPriceReq = new AirPriceReq();
		buildBaseCoreRequest(airPriceReq);
		buildAirItinerary(airPriceReq, selectedTrip, isAirPricingCommandReq);
		return airPriceReq;
	}

	private void buildSearchPassengerValues(AirPriceReq airPriceReq) {
		for (Map.Entry<PaxType, Integer> paxInfo : searchQuery.getPaxInfo().entrySet()) {
			int paxCount = paxInfo.getValue();
			int tempChildAge = Integer.valueOf(TravelPortSessionLessConstants.CHILD_AGE);
			while (paxCount > 0) {
				SearchPassenger_type0 passenger = new SearchPassenger_type0();
				PaxType paxType = paxInfo.getKey();
				passenger.setCode(getTypePTC(TravelPortSessionLessUtils.getTypePTC(paxType)));
				passenger.setBookingTravelerRef(TravelPortSessionLessUtils.keyGenerator());
				if (PaxType.CHILD.getType().equals(paxType.getType())) {
					passenger.setAge(new BigInteger(String.valueOf(tempChildAge)));
					tempChildAge++;
				}
				if (PaxType.INFANT.getType().equals(paxType.getType())) {
					passenger.setAge(new BigInteger(TravelPortSessionLessConstants.INFANT_AGE));
					passenger.setPricePTCOnly(Boolean.FALSE);
				}
				airPriceReq.addSearchPassenger(passenger);
				paxCount--;
			}
		}
	}


	private void buildAirItinerary(AirPriceReq airPriceReq, TripInfo selectedTrip, boolean isAirPricingCommandReq) {
		AirItinerary_type0 airItinerary = new AirItinerary_type0();
		List<TypeBaseAirSegment> typeBaseAirSegmentList = new ArrayList<>();
		buildAirSegmentList(typeBaseAirSegmentList, selectedTrip.getSegmentInfos(), true, true);
		airItinerary.setAirSegment(typeBaseAirSegmentList.toArray(new TypeBaseAirSegment[0]));
		airItinerary.setHostToken(getHostTokenList(selectedTrip.getSegmentInfos()).toArray(new HostToken_type0[0]));
		airPriceReq.setAirItinerary(airItinerary);
		AirPricingModifiers_type0 airPricingModifiers = getAirPricingModifiers(selectedTrip.getSegmentInfos());
		buildSearchPassengerValues(airPriceReq);
		AirPricingCommand_type0 airPricingCommand = new AirPricingCommand_type0();
		if (isAirPricingCommandReq || (AirFlowType.ALTERNATE_CLASS).equals(searchQuery.getFlowType())) {
			buildAirPricingCommand(selectedTrip.getSegmentInfos(), airPriceReq, true);
		} else {
			airPriceReq.addAirPricingCommand(airPricingCommand);
		}
		airPriceReq.setAirPricingModifiers(airPricingModifiers);
	}


	public FlightBasicFact getSSRFlightFact(SegmentInfo segment) {
		FlightBasicFact flightFact = FlightBasicFact.builder().build();
		Integer sourceId = segment.getSupplierInfo().getSourceId();
		AirType airType = searchQuery.getIsDomestic() ? AirType.DOMESTIC : AirType.INTERNATIONAL;
		String airline = segment.getAirlineCode(false);
		flightFact.setAirline(airline);
		flightFact.setSourceId(sourceId);
		flightFact.setAirType(airType);
		return flightFact;
	}
}
