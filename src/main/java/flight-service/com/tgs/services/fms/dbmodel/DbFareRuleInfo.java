package com.tgs.services.fms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.fms.datamodel.farerule.FareRuleInfo;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.runtime.database.CustomTypes.FlightBasicRuleCriteriaType;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@Entity
@TypeDefs({@TypeDef(name = "FareRuleInfoType", typeClass = FareRuleInfoType.class),
		@TypeDef(name = "FlightBasicRuleCriteriaType", typeClass = FlightBasicRuleCriteriaType.class)})
@Table(name = "fareruleinfo")
@Audited
public class DbFareRuleInfo extends BaseModel<DbFareRuleInfo, FareRuleInfo> {

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private String airType;

	@Column
	private String airline;

	@Column
	private double priority;

	@Column
	private boolean enabled;

	@Column
	@Type(type = "FlightBasicRuleCriteriaType")
	private FlightBasicRuleCriteria inclusionCriteria;

	@Column
	@Type(type = "FlightBasicRuleCriteriaType")
	private FlightBasicRuleCriteria exclusionCriteria;

	@Column
	@Type(type = "FareRuleInfoType")
	private FareRuleInformation fareRuleInformation;

	@Column
	private boolean isDeleted;

	@Column
	private String description;

	@Override
	public FareRuleInfo toDomain() {
		return new GsonMapper<>(this, FareRuleInfo.class).convert();
	}

	@Override
	public DbFareRuleInfo from(FareRuleInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbFareRuleInfo.class).convert();
	}

	public void cleanData() {
		if (inclusionCriteria != null)
			inclusionCriteria.cleanData();
		if (exclusionCriteria != null)
			exclusionCriteria.cleanData();
		if (fareRuleInformation != null)
			fareRuleInformation.cleanData();

	}


}
