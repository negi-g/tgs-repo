package com.tgs.services.fms.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import com.tgs.services.fms.dbmodel.DbSupplierInfo;

public interface SupplierInfoRepository
		extends JpaRepository<DbSupplierInfo, Long>, JpaSpecificationExecutor<DbSupplierInfo> {

	@Query(value = "SELECT nextval('supplierinfo_id_seq')", nativeQuery = true)
	public Long getNextSeriesId();

	public List<DbSupplierInfo> findAllByOrderBySourceIdAscProcessedOnDesc();

}
