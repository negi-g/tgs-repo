package com.tgs.services.fms.sources.amadeusndc;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AmadeusNdcSessionInfo {

	private String password;

	private String nonce;

	private String created;

}
