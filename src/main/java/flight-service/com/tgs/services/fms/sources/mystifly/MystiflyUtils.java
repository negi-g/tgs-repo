package com.tgs.services.fms.sources.mystifly;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PassengerType;
import org.datacontract.schemas._2004._07.mystifly_onepoint.Tax;
import com.google.common.base.Enums;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.helper.GDSFareComponentMapper;

public class MystiflyUtils {

	public static PaxType getPaxType(PassengerType passengerType) {
		if (passengerType.getValue().equals("CHD")) {
			return PaxType.CHILD;
		}
		return PaxType.getPaxType(passengerType.getValue());
	}

	public static FareComponent getFareComponent(Tax tax) {
		FareComponent fareComponent = FareComponent.AT;
		if (tax != null && tax.getTaxCode() != null && StringUtils.isNotBlank(tax.getTaxCode())) {
			fareComponent = Enums.getIfPresent(GDSFareComponentMapper.class, tax.getTaxCode())
					.or(GDSFareComponentMapper.TX).getFareComponent();
		}
		return fareComponent;
	}


	/**
	 * time will be of the format yyyy-mm-ddThh:mm:ss
	 * 
	 * @param time (eg : 2019-05-12T19:15:00)
	 * @return
	 */
	public static LocalDateTime getLocalDateTimeFromString(String time) {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		LocalDateTime formattedDateTime = LocalDateTime.parse(time, formatter);
		return formattedDateTime;
	}

	/**
	 * time will be of the format yyyy-mm-ddT
	 * 
	 * @param time (eg: 2019-05-12T)
	 * @return
	 */

	public static LocalDate getLocalDateFromString(String time) {
		if (StringUtils.isEmpty(time)) {
			return null;
		}
		time = time.substring(0, time.indexOf("T"));
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;
		LocalDate formattedDate = LocalDate.parse(time, formatter);
		return formattedDate;
	}

}
