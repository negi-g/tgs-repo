package com.tgs.services.fms.servicehandler;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.supplier.SupplierAbandonedSessionInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.mapper.AirAbandonedSessionToAnalyticsAirAbandonedSessionMapper;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirAbandonedSessionHandler {

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	AirAnalyticsHelper analyticsHelper;

	final private static int MAX_MINUTES = 4;

	public void releaseAbandonedSessions() {

		LocalDateTime now = LocalDateTime.now();
		log.info("[AirAbandonedSessionHandler] running for {}", LocalDateTime.now().toString());
		for (int minutes = MAX_MINUTES; minutes >= 0; minutes--) {
			String localTimeNow = now.minusMinutes(minutes).truncatedTo(ChronoUnit.MINUTES).toString();
			ExecutorUtils.getGeneralPurposeThreadPool().submit(() -> {

				SessionMetaInfo metaInfo = SessionMetaInfo.builder().set(CacheSetName.ABANDONED_SESSIONS.getName())
						.namespace(CacheNameSpace.FLIGHT.getName()).key(localTimeNow).compress(false).build();

				Map<String, List<SupplierSession>> abandonedSessionList =
						cachingCommunicator.getList(metaInfo, SupplierSession.class, false, false, new String[0]);
				try {
					List<Future<?>> futureTaskList = new ArrayList<>();
					if (MapUtils.isNotEmpty(abandonedSessionList)) {
						log.info(
								"[AirAbandonedSessionHandler] abandoned sessions list of size {} fetched from cache for expiry time {}",
								CollectionUtils.size(abandonedSessionList), localTimeNow);
						for (String expiryDate : abandonedSessionList.keySet()) {
							List<SupplierSession> abandonedSessions = abandonedSessionList.get(expiryDate);
							log.info(
									"[AirAbandonedSessionHandler] abandoned sessions list fetched from cache is {} for expiry time {}",
									abandonedSessions, localTimeNow);
							for (SupplierSession abandonedSession : abandonedSessions) {

								Integer sourceId = abandonedSession.getSourceId();
								AirSourceType sourceType = AirSourceType.getAirSourceType(sourceId);
								SupplierRule supplierRule = SupplierConfigurationHelper.getSupplierRule(sourceId,
										abandonedSession.getSupplierId());
								SupplierConfiguration supplierConf = SupplierConfigurationHelper
										.getSupplierConfiguration(sourceId, supplierRule.getId());
								AbstractAirInfoFactory factory = sourceType.getFactoryInstance(null, supplierConf);

								log.debug("[AirAbandonedSessionHandler] supplier session to be realeased is {}",
										abandonedSession);
								futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool()
										.submit(() -> factory.closeAbandonedSession(abandonedSession)));
							}
							futureTaskList.forEach(task -> {
								try {
									task.get(120, TimeUnit.SECONDS);
								} catch (TimeoutException e) {
									log.error("Cancelling releasing abandoned sessions thread due to timeout", e);
									task.cancel(true);
								} catch (Exception e) {
									log.error("Close abandoned session execution failed", e);
									throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
								}
							});
						}
					}
				} catch (Exception e) {
					log.error("Error occured while releasing abandoned sessions");
					throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
				} finally {
					sendDataToAnalytics(abandonedSessionList);
				}
			});
		}

	}

	public void sendDataToAnalytics(Map<String, List<SupplierSession>> abandonedSessionList) {
		try {
			if (MapUtils.isNotEmpty(abandonedSessionList)) {
				for (String expiryDate : abandonedSessionList.keySet()) {
					List<SupplierSession> abandonedSessions = abandonedSessionList.get(expiryDate);
					Map<String, List<SupplierSession>> supplierWiseAbandonedSessions = abandonedSessions.stream()
							.collect(Collectors.groupingBy(abandonedSession -> abandonedSession.getSupplierId()));
					log.debug("Original abandoned sesssions are {}", GsonUtils.getGson().toJson(abandonedSessions));
					log.debug("Abandoned sessions grouped by supplierid are {}",
							GsonUtils.getGson().toJson(supplierWiseAbandonedSessions));
					for (List<SupplierSession> sessions : supplierWiseAbandonedSessions.values()) {
						SupplierAbandonedSessionInfo sessionData = SupplierAbandonedSessionInfo.builder()
								.sourceId(sessions.get(0).getSourceId().toString())
								.supplierId(sessions.get(0).getSupplierId())
								.expiryTime(sessions.get(0).getExpiryTime().toString())
								.jobTriggeredTime(LocalDateTime.now().toString()).countOfSessions(sessions.size())
								.build();
						AirAbandonedSessionToAnalyticsAirAbandonedSessionMapper queryMapper =
								AirAbandonedSessionToAnalyticsAirAbandonedSessionMapper.builder()
										.user(SystemContextHolder.getContextData().getUser())
										.contextData(SystemContextHolder.getContextData()).sessionData(sessionData)
										.build();
						analyticsHelper.pushToAnalytics(queryMapper, AirAnalyticsType.ABANDONED_SESSIONS);
					}
				}
			}
		} catch (Exception exp) {
			log.error("Error occured while pushing abandoned session data", exp);
		}
	}
}
