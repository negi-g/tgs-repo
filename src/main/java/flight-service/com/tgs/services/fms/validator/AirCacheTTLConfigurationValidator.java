package com.tgs.services.fms.validator;

import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.AirCacheTypeTTLConfiguration;
import com.tgs.services.fms.helper.CacheType;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import java.util.Map;


@Service
public class AirCacheTTLConfigurationValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T output) {

		AirCacheTypeTTLConfiguration ttlConfiguration = (AirCacheTypeTTLConfiguration) output;

		if (ttlConfiguration == null || MapUtils.isEmpty(ttlConfiguration.getTtlMap())) {
			errors.rejectValue(fieldName + ".ttlMap", SystemError.NULL_VALUE.errorCode(),
					SystemError.NULL_VALUE.getMessage());
			return;
		}

		Map<String, Integer> ttlMap = ttlConfiguration.getTtlMap();

		for (Map.Entry<String, Integer> entry : ttlMap.entrySet()) {
			if (entry.getKey() == null) {
				errors.rejectValue(fieldName + ".ttlMap", SystemError.KEY_EMPTY.errorCode(),
						SystemError.KEY_EMPTY.getMessage());
				return;
			}
			if (entry.getValue() == null) {
				errors.rejectValue(fieldName + ".ttlMap", SystemError.NULL_VALUE.errorCode(),
						SystemError.NULL_VALUE.getMessage());
				return;
			}
			if (entry.getValue() <= 0) {
				errors.rejectValue(fieldName + ".ttlMap", SystemError.NEGATIVE_VALUE.errorCode(),
						SystemError.NEGATIVE_VALUE.getMessage());
				return;
			}
		}

	}
}
