package com.tgs.services.fms.sources.navitaireV4_2;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import com.navitaire.schemas.webservices.ContentManagerStub;
import com.tgs.utils.exception.air.FareRuleUnHandledFaultException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import com.google.common.io.ByteStreams;
import com.navitaire.schemas.webservices.datacontracts.booking.FareRuleRequestData;
import com.navitaire.schemas.webservices.servicecontracts.contentservice.FareRuleRequest;
import com.navitaire.schemas.webservices.servicecontracts.contentservice.GetFareRuleInfoResponse;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.navitaire.schemas.webservices.IContentManager_GetFareRuleInfo_APIValidationFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IContentManager_GetFareRuleInfo_APIUnhandledServerFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IContentManager_GetFareRuleInfo_APICriticalFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IContentManager_GetFareRuleInfo_APIGeneralFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IContentManager_GetFareRuleInfo_APISecurityFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IContentManager_GetFareRuleInfo_APIWarningFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IContentManager_GetFareRuleInfo_APIFaultFault_FaultMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
final class NavitaireFareRuleManager extends NavitaireServiceManager {

	protected ContentManagerStub contentBinding;

	public String getFareRule(TripInfo tripInfo) {
		GetFareRuleInfoResponse response = null;
		try {
			FareRuleRequest request = new FareRuleRequest();
			request.setFareRuleReqData(getFareRuleRequest(tripInfo));
			listener.setType("FareRuleInfo");
			contentBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			response = contentBinding.getFareRuleInfo(request, getContractVersion(), getExceptionStackTrace(),
					getMessageContractversion(), getSignature(), getSourceId());
			if (Objects.nonNull(response) && response.isFareRuleInfoSpecified()) {
				InputStream stream = response.getFareRuleInfo().getData().getDataSource().getInputStream();
				String fareRule = new String(ByteStreams.toByteArray(stream));
				return fareRule.replaceAll("\\\\", "");
			}
		} catch (IOException re) {
			throw new SupplierRemoteException(re.getMessage());
		} catch (IContentManager_GetFareRuleInfo_APIValidationFaultFault_FaultMessage
				| IContentManager_GetFareRuleInfo_APIUnhandledServerFaultFault_FaultMessage
				| IContentManager_GetFareRuleInfo_APICriticalFaultFault_FaultMessage
				| IContentManager_GetFareRuleInfo_APIGeneralFaultFault_FaultMessage
				| IContentManager_GetFareRuleInfo_APISecurityFaultFault_FaultMessage
				| IContentManager_GetFareRuleInfo_APIWarningFaultFault_FaultMessage
				| IContentManager_GetFareRuleInfo_APIFaultFault_FaultMessage fe) {
			throw new FareRuleUnHandledFaultException(fe.getMessage());
		} finally {
			contentBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
		}
		return StringUtils.EMPTY;
	}

	private FareRuleRequestData getFareRuleRequest(TripInfo tripInfo) {
		PriceInfo priceInfo = tripInfo.getSegmentInfos().get(0).getPriceInfo(0);
		FareRuleRequestData requestData = new FareRuleRequestData();
		requestData.setCarrierCode(getAirline(tripInfo));
		requestData.setClassOfService(priceInfo.getFareDetail(PaxType.ADULT).getClassOfBooking());
		requestData.setFareBasisCode(priceInfo.getFareDetail(PaxType.ADULT).getFareBasis());
		if (StringUtils.isNotBlank(priceInfo.getMiscInfo().getClassOfService())) {
			requestData.setClassOfService(priceInfo.getMiscInfo().getClassOfService());
		}
		return requestData;
	}

}
