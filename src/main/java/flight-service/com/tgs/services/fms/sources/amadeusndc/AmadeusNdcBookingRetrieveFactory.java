package com.tgs.services.fms.sources.amadeusndc;

import org.iata.iata._2015._00._2018_1.orderviewrs.OrderViewRS;
import org.springframework.stereotype.Service;
import com.amadeus.wsdl.altea_ndc_18_1_v1_v4.AlteaNDC181Service;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import com.tgs.services.fms.sources.AirSourceConstants;

@Service
public class AmadeusNdcBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	private static AlteaNDC181Service service = null;

	private String toCurrency;

	AmadeusNdcBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
		if (service == null) {
			service = new AlteaNDC181Service();
		}
		toCurrency = AirSourceConstants.getClientCurrencyCode();
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		AmadeusNdcBookingRetrieveManager retrieveManager =
				AmadeusNdcBookingRetrieveManager.builder().supplierConf(supplierConf).service(service).bookingId(pnr)
						.toCurrency(toCurrency).moneyExchangeComm(moneyExchangeComm).supplierPNR(pnr).build();
		OrderViewRS orderViewRs = retrieveManager.orderRetrieve();
		pnrBooking = retrieveManager.parseTripInfos(orderViewRs);
		return pnrBooking;
	}

}
