package com.tgs.services.fms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.gson.RestExcludeStrategy;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.restmodel.AirReviewRequest;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.restmodel.AirReviewSeatResponse;
import com.tgs.services.fms.restmodel.CancellationProtectionRequest;
import com.tgs.services.fms.restmodel.CancellationProtectionResponse;
import com.tgs.services.fms.servicehandler.AirAbandonedSessionHandler;
import com.tgs.services.fms.servicehandler.AirReviewHandler;
import com.tgs.services.fms.servicehandler.AirReviewSessionHandler;
import com.tgs.services.fms.servicehandler.AirSeatMapHandler;
import com.tgs.services.fms.servicehandler.CancellationProtectionHandler;
import com.tgs.services.oms.restmodel.BookingRequest;


@RequestMapping("/fms/v1")
@RestController
public class AirReviewController {

	@Autowired
	AirReviewHandler reviewHandler;

	@Autowired
	AirSeatMapHandler seatMapHandler;

	@Autowired
	AirReviewSessionHandler sessionHandler;

	@Autowired
	CancellationProtectionHandler protectionHandler;

	@Autowired
	AirAbandonedSessionHandler abandonedSessionHandler;

	@Deprecated
	@RequestMapping(value = "/review/", method = RequestMethod.POST)
	protected AirReviewResponse oldReviewTrip(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirReviewRequest reviewRequest) throws Exception {
		SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
		reviewHandler.initData(reviewRequest, new AirReviewResponse());
		return reviewHandler.getResponse();
	}

	@RequestMapping(value = "/review", method = RequestMethod.POST)
	protected AirReviewResponse reviewTrip(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirReviewRequest reviewRequest) throws Exception {
		SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
		reviewHandler.initData(reviewRequest, new AirReviewResponse());
		return reviewHandler.getResponse();
	}


	@RequestMapping(value = "/seat", method = RequestMethod.POST)
	protected AirReviewSeatResponse getSeatMap(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody BookingRequest bookingRequest) throws Exception {
		SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
		SystemContextHolder.getContextData().getReqIds().add(bookingRequest.getBookingId());
		seatMapHandler.initData(bookingRequest, new AirReviewSeatResponse());
		return seatMapHandler.getResponse();
	}

	@RequestMapping(value = "/close-session", method = RequestMethod.POST)
	protected BaseResponse closeSession(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody BookingRequest bookingRequest) throws Exception {
		sessionHandler.initData(bookingRequest, new BaseResponse());
		return sessionHandler.getResponse();
	}

	@RequestMapping(value = "/job/close-abandoned-session", method = RequestMethod.POST)
	protected BaseResponse closeAbandonedSession(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		abandonedSessionHandler.releaseAbandonedSessions();
		BaseResponse baseResponse = new BaseResponse();
		return baseResponse;
	}

	@RequestMapping(value = "/protection-charges", method = RequestMethod.POST)
	protected CancellationProtectionResponse getCancellationProtectionFee(HttpServletRequest request,
			HttpServletResponse response,
			@Valid @RequestBody CancellationProtectionRequest cancellationProtectionRequest) throws Exception {
		protectionHandler.initData(cancellationProtectionRequest, new CancellationProtectionResponse());
		return protectionHandler.getResponse();
	}
}
