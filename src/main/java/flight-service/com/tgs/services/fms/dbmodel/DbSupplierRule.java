package com.tgs.services.fms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.runtime.database.CustomTypes.FlightBasicRuleCriteriaType;
import com.tgs.services.fms.datamodel.supplier.SupplierAdditionalInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@TypeDefs({@TypeDef(name = "FlightBasicRuleCriteriaType", typeClass = FlightBasicRuleCriteriaType.class),
		@TypeDef(name = "SupplierAdditionalInfoType", typeClass = SupplierAdditionalInfoType.class)})
@Entity
@Table(name = "supplierrule")
@Audited
public class DbSupplierRule extends BaseModel<DbSupplierRule, SupplierRule> {

	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private boolean enabled;

	@Column
	private Integer sourceId;

	@Column
	private String supplierId;

	@Type(type = "FlightBasicRuleCriteriaType")
	private FlightBasicRuleCriteria inclusionCriteria;

	@Type(type = "FlightBasicRuleCriteriaType")
	private FlightBasicRuleCriteria exclusionCriteria;

	@Type(type = "SupplierAdditionalInfoType")
	private SupplierAdditionalInfo supplierAdditionalInfo;

	@Column
	private double priority;

	@Column
	private Boolean exitOnMatch;

	@Column
	private boolean isDeleted;

	@Column
	private String description;

	@Column
	private String airType;

	@Column
	private String searchType;

	public void cleanData() {
		if (inclusionCriteria != null)
			inclusionCriteria.cleanData();
		if (exclusionCriteria != null)
			exclusionCriteria.cleanData();
		if (supplierAdditionalInfo != null)
			supplierAdditionalInfo.cleanData();
	}

	@Override
	public SupplierRule toDomain() {
		return new GsonMapper<>(this, SupplierRule.class).convert();
	}

	@Override
	public DbSupplierRule from(SupplierRule dataModel) {
		return new GsonMapper<>(dataModel, this, DbSupplierRule.class).convert();
	}
}
