package com.tgs.services.fms.sources.kafila;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.kafila.searchresponse.FlightDetails;

public class KafilaUtils {

	private static final String FLX = "FLX";
	private static final String FLEXI = "FLEXI";
	private static final String CPN = "CPN";
	private static final String COUPON = "COUPON";
	private static final String SME = "SME";
	private static final String CORP = "CORP";
	private static final String LITE = "LITE";
	private static final String LTE = "LTE";
	private static final String TACTICAL = "TACTICAL";
	private static final String FAMILY = "FAMILY";
	private static final String SALE = "SALE";


	public static LocalDateTime convertStringToDate(String date, String time) {
		String datetime = date + time;
		if (datetime.charAt(2) == '/') {
			return LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern("MM/dd/yyyyHH:mm"));
		} else {
			return LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern("yyyy-MM-ddHH:mm"));
		}
	}

	public static LocalDateTime toLocalDateTime(String date, String time) {
		String datetime = date + time;
		if (datetime.charAt(2) == '/') {
			return LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern("MM/dd/yyyyHH:mm"));
		} else {
			return LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern("yyyy-MM-ddHH:mm"));
		}
	}

	public static LocalDate stringToDob(String date) {
		return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

	public static Long toMinutes(String duration) {
		String durationText = StringUtils.join("P", duration.toUpperCase()).replaceAll("D", "DT").replaceAll(":", "");
		return Duration.parse(durationText).toMinutes();
	}

	public static String getPaxType(PaxType paxType) {
		return paxType.name().toLowerCase();
	}

	public static PaxType setPaxType(String paxType) {
		return PaxType.valueOf(paxType.toUpperCase());
	}

	public static FareType getFareTypeOnPCC(FlightDetails trip) {
		FareType fareType = FareType.PUBLISHED;
		String pccDetail = trip.getPCC();
		if (StringUtils.containsIgnoreCase(pccDetail, FLX) || StringUtils.containsIgnoreCase(pccDetail, FLEXI)) {
			fareType = FareType.FLEXI;
		} else if (StringUtils.containsIgnoreCase(pccDetail, CORP)) {
			fareType = FareType.CORPORATE;
		} else if (StringUtils.containsIgnoreCase(pccDetail, SME)) {
			fareType = FareType.SME;
		} else if (StringUtils.containsIgnoreCase(pccDetail, CPN)
				|| StringUtils.containsIgnoreCase(pccDetail, COUPON)) {
			fareType = FareType.COUPON;
		} else if (StringUtils.containsIgnoreCase(pccDetail, LTE) || StringUtils.equalsIgnoreCase(pccDetail, LITE)) {
			fareType = FareType.LITE;
		} else if (StringUtils.containsIgnoreCase(pccDetail, SALE)) {
			fareType = FareType.SALE;
		} else if (StringUtils.containsIgnoreCase(pccDetail, FAMILY)) {
			fareType = FareType.FAMILY;
		}
		return fareType;
	}

	public static TripInfo createTripFromSegmentList(List<SegmentInfo> segmentList) {
		TripInfo trip = new TripInfo();
		List<SegmentInfo> sortedList = segmentList.stream()
				.sorted(Comparator.comparing(SegmentInfo::getId, Comparator.nullsFirst(Comparator.naturalOrder())))
				.collect(Collectors.toList());

		trip.setSegmentInfos(sortedList);
		return trip;
	}
}
