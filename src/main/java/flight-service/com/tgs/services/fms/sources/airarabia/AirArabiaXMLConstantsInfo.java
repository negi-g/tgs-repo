package com.tgs.services.fms.sources.airarabia;

public class AirArabiaXMLConstantsInfo {

	// common
	public static final String PREREQUESTDATA =
			"<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soap:Header><wsse:Security soap:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"><wsse:UsernameToken wsu:Id=\"UsernameToken-{0}\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsse:Username>{1}</wsse:Username><wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">{2}</wsse:Password></wsse:UsernameToken></wsse:Security></soap:Header><soap:Body xmlns:ns1=\"http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05\" xmlns:ns2=\"http://www.opentravel.org/OTA/2003/05\">";

	public static final String POSTREQUESTDATA = "</soap:Body></soap:Envelope>";

	// PNR Retreive
	public static final String PNRRBOOKDATA =
			"<ns2:OTA_ReadRQ EchoToken=\"{0}\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" TimeStamp=\"{1}\" Version=\"{2}\"><ns2:POS><ns2:Source TerminalID=\"{3}\"><ns2:RequestorID ID=\"{4}\" Type=\"4\" /><ns2:BookingChannel Type=\"12\" /></ns2:Source></ns2:POS>";

	public static final String POSTPNRREQUEST =
			"<ns1:AAReadRQExt><ns1:AALoadDataOptions><ns1:LoadTravelerInfo>true</ns1:LoadTravelerInfo><ns1:LoadAirItinery>true</ns1:LoadAirItinery><ns1:LoadPriceInfoTotals>true</ns1:LoadPriceInfoTotals><ns1:LoadFullFilment>true</ns1:LoadFullFilment></ns1:AALoadDataOptions></ns1:AAReadRQExt>";

	public static final String PNRREQUEST =
			"<ns2:ReadRequests><ns2:ReadRequest><ns2:UniqueID ID=\"{0}\" Type=\"14\" /></ns2:ReadRequest></ns2:ReadRequests></ns2:OTA_ReadRQ>";

	// Book
	public static final String BOOK_REQUEST_PAX =
			"<ns1:AAAirBookRQExt><ns1:ContactInfo><ns1:PersonName><ns1:Title>{0}</ns1:Title><ns1:FirstName>{1}</ns1:FirstName><ns1:LastName>{2}</ns1:LastName></ns1:PersonName><ns1:Telephone><ns1:PhoneNumber>{3}</ns1:PhoneNumber><ns1:CountryCode>971</ns1:CountryCode><ns1:AreaCode>88</ns1:AreaCode></ns1:Telephone><ns1:Email>{4}</ns1:Email><ns1:Address><ns1:AddressLine1>{5}</ns1:AddressLine1><ns1:CityName>{6}</ns1:CityName><ns1:CountryName><ns1:CountryName>{7}</ns1:CountryName><ns1:CountryCode>{8}</ns1:CountryCode></ns1:CountryName><ns1:CityName>{9}</ns1:CityName><ns1:taxRegNo>{10}</ns1:taxRegNo></ns1:Address></ns1:ContactInfo></ns1:AAAirBookRQExt>";

	public static final String BOOK_PRE_REQUEST =
			"<ns2:OTA_AirBookRQ EchoToken=\"{0}\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" TimeStamp=\"{1}\" TransactionIdentifier=\"{2}\" Version=\"{3}\"><ns2:POS><ns2:Source TerminalID=\"{4}\"><ns2:RequestorID ID=\"{5}\" Type=\"4\" /><ns2:BookingChannel Type=\"12\" /></ns2:Source></ns2:POS>";

	public static final String BOOK_PAYMENT =
			"<ns2:Fulfillment><ns2:PaymentDetails><ns2:PaymentDetail><ns2:DirectBill><ns2:CompanyName Code=\"{0}\" /></ns2:DirectBill><ns2:PaymentAmount Amount=\"{1}\" CurrencyCode=\"{2}\" DecimalPlaces=\"2\" /></ns2:PaymentDetail></ns2:PaymentDetails></ns2:Fulfillment>";

	public static final String BOOK_POST_PAYMENT =
			"<ns2:Fulfillment><ns2:PaymentDetails><ns2:PaymentDetail><ns2:DirectBill><ns2:CompanyName Code=\"{0}\" /></ns2:DirectBill><ns2:PaymentAmount Amount=\"{1}\" CurrencyCode=\"{2}\" DecimalPlaces=\"2\" /></ns2:PaymentDetail></ns2:PaymentDetails></ns2:Fulfillment>";


	// Origin Destination
	public static final String OD_OPEN = "<ns2:AirItinerary><ns2:OriginDestinationOptions>";

	public static final String OD_CLOSE = "</ns2:OriginDestinationOptions></ns2:AirItinerary>";

	// Flight
	public static final String FLIGHT_SEGMENT =
			"<ns2:OriginDestinationOption><ns2:FlightSegment ArrivalDateTime=\"{0}\" DepartureDateTime=\"{1}\" FlightNumber=\"{2}\" RPH=\"{3}\"><ns2:DepartureAirport LocationCode=\"{4}\" Terminal=\"TerminalX\" /><ns2:ArrivalAirport LocationCode=\"{5}\" Terminal=\"TerminalX\" /></ns2:FlightSegment></ns2:OriginDestinationOption>";

	// Traveler
	public static final String TRAVELER_OPEN = "<ns2:TravelerInfo>";

	public static final String TRAVELER_CLOSE = "</ns2:TravelerInfo>";

	public static final String AIRTRAVELER =
			"<ns2:AirTraveler BirthDate=\"{0}\" PassengerTypeCode=\"{1}\"><ns2:PersonName><ns2:GivenName>{2}</ns2:GivenName><ns2:Surname>{3}</ns2:Surname><ns2:NameTitle>{4}</ns2:NameTitle></ns2:PersonName><ns2:Telephone AreaCityCode=\"88\" CountryAccessCode=\"971\" PhoneNumber=\"{5}\" /><ns2:Address><ns2:CountryName Code=\"{6}\" /></ns2:Address><ns2:Document DocID=\"{7}\" DocType=\"PSPT\" DocHolderNationality=\"{8}\" /><ns2:TravelerRefNumber RPH=\"{9}\" /></ns2:AirTraveler>";

	public static final String INFANTTRAVELER =
			"<ns2:AirTraveler BirthDate=\"{0}\" PassengerTypeCode=\"INF\"><ns2:PersonName><ns2:GivenName>{1}</ns2:GivenName><ns2:Surname>{2}</ns2:Surname><ns2:NameTitle>{3}</ns2:NameTitle></ns2:PersonName><ns2:TravelerRefNumber RPH=\"{4}\" /><ns2:Document  DocType=\"PSPT\" DocID=\"{5}\" DocHolderNationality=\"{6}\" /></ns2:AirTraveler>";

	// After HOLD Modify Book
	public static final String MODIFYREQUEST =
			"<AAAirBookModifyRQExt xmlns=\"http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05\"><AALoadDataOptions><LoadTravelerInfo>true</LoadTravelerInfo><LoadAirItinery>true</LoadAirItinery><LoadPriceInfoTotals>true</LoadPriceInfoTotals><LoadFullFilment>true</LoadFullFilment></AALoadDataOptions></AAAirBookModifyRQExt>";

	public static final String REFERENCE =
			"<ns2:BookingReferenceID ID=\"{0}\" Type=\"14\" /></ns2:AirBookModifyRQ></ns2:OTA_AirBookModifyRQ>";

	public static final String BOOK_MODIFY =
			"<ns2:OTA_AirBookModifyRQ EchoToken=\"{0}\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" TimeStamp=\"{1}\" TransactionIdentifier=\"{2}\" Version=\"{3}\"><ns2:POS><ns2:Source TerminalID=\"{4}\"><ns2:RequestorID ID=\"{5}\" Type=\"4\" /><ns2:BookingChannel Type=\"12\" /></ns2:Source></ns2:POS><ns2:AirBookModifyRQ ModificationType=\"9\">";

	public static final String NO_BAG = "No Bag";

}

