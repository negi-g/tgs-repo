package com.tgs.services.fms.helper;

import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.math.NumberUtils;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.airconfigurator.AirCacheTypeTTLConfiguration;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;

@Getter
public enum CacheType {
	SEARCHIDWAITTIME(300),
	RETRYSECOND(10),
	SEARCHIDEXPIRATION(600),
	FLIGHTSEARCHCACHING(3600),
	PRICEIDKEYATSEARCH(2700),
	LOWAIRFARE(25200),
	REVIEWKEYEXPIRATION(7200),
	FARECOMPONENT(604800),
	SUPPLIERSESSIONEXPIRATION(3600),
	NON_OPERATIONAL_SECTORS(259200);

	private int ttl;


	private CacheType(int ttl) {
		this.ttl = ttl;
	}

	// contains dead code
	public int getTtl(User user) {
		AirGeneralPurposeOutput output =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
		Map<String, String> ttlMap = null;
		if (output != null && output.getTtlMap() != null) {
			ttlMap = output.getTtlMap();
			return NumberUtils.toInt(ttlMap.get(this.name()), ttl);
		}
		return ttl;
	}

	public int getTtl() {
		return getTtl(null);
	}


	public int getTtl(User user, AirSearchQuery searchQuery) {
		FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(searchQuery);
		BaseUtils.createFactOnUser(flightFact, user);

		AirCacheTypeTTLConfiguration ttlConfiguration =
				AirConfiguratorHelper.getAirConfigRule(flightFact, AirConfiguratorRuleType.CACHE_TTL);
		if (Objects.nonNull(ttlConfiguration) && MapUtils.isNotEmpty(ttlConfiguration.getTtlMap())
				&& Objects.nonNull(ttlConfiguration.getTtlMap().get(this.name()))) {
			return ttlConfiguration.getTtl(this.name());
		}
		return this.getTtl();
	}
}
