package com.tgs.services.fms.sources.otaradixx;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.CreatePNRActionTypes;
import org.springframework.stereotype.Service;
import org.tempuri.RetrievePNRResponse;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RadixxSessionLessAirBookingFactory extends RadixxAirBookingFactory {

	public RadixxSessionLessAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments,
			Order order) throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;
		initialize();
		/**
		 * In case session is expired or traveller took more than 20 min to complete payment/review section.
		 */
		RadixxTokenManager loginManager = RadixxTokenManager.builder().configuration(supplierConf)
				.bindingService(bindingService).airline(airline).listener(listener).bookingUser(bookingUser).build();
		loginManager.init();
		if (Objects.isNull(supplierSession)) {
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().sessionToken(loginManager.generateToken()).build();
			supplierSession = SupplierSession.builder().supplierSessionInfo(sessionInfo).bookingId(order.getBookingId())
					.sourceId(supplierConf.getSourceId()).supplierId(supplierConf.getSupplierId()).build();
		}
		loginManager.setBinaryToken(supplierSession.getSupplierSessionInfo().getSessionToken());
		double agencyCommission = loginManager.getAgencyCommission();
		int totalCommissionalablePaxCount = loginManager.getFromSegmentsPaxCount(bookingSegments.getSegmentInfos());
		agencyCommission = agencyCommission * totalCommissionalablePaxCount;
		log.info("Total Supplier Commission for Booking {} and Commission is {}", bookingId, agencyCommission);
		List<SegmentInfo> segmentInfos = bookingSegments.getSegmentInfos();
		RadixxBookingManager bookingManager = RadixxBookingManager.builder().bindingService(bindingService)
				.configuration(supplierConf).airline(airline).bookingId(bookingId).deliveryInfo(deliveryInfo)
				.criticalMessageLogger(criticalMessageLogger)
				.binaryToken(supplierSession.getSupplierSessionInfo().getSessionToken()).listener(listener)
				.gstInfo(gstInfo).bookingUser(bookingUser).build();
		bookingManager.init();
		bookingManager.setGmsCommunicator(gmsCommunicator);
		double amountToBePaidtoAirline = bookingManager.summaryPNR(segmentInfos);
		double amountWithoutCommission = amountToBePaidtoAirline - agencyCommission;

		if (!isFareDiff(amountWithoutCommission)) {
			blockedPnr = bookingManager.createPNR(CreatePNRActionTypes.CommitSummary, isHoldBooking);
			if (StringUtils.isNotBlank(blockedPnr)) {
				// For IX assessAgencyTransactionFees will be called after Commit Summary
				if (airline.isAssessFeeAllowed()) {
					amountToBePaidtoAirline = bookingManager.assessAgencyTransactionFees();
					amountWithoutCommission = amountToBePaidtoAirline - agencyCommission;
				}
				if (!isHoldBooking) {
					bookingManager.setConfirmationNumber(blockedPnr);
					BigDecimal amountToPay =
							new BigDecimal(amountToBePaidtoAirline).setScale(2, BigDecimal.ROUND_HALF_UP);
					log.info("Processing PNR Payment {} for Booking ID {}", blockedPnr, bookingId);
					bookingManager.processPnrPayment(amountToPay);
					pnr = bookingManager.createPNR(CreatePNRActionTypes.SaveReservation, isHoldBooking);

					if (StringUtils.isBlank(pnr)) {
						log.info("Failed to Confirm PNR {} for Booking ID {}", blockedPnr, bookingId);
						isSuccess = false;
						pnr = blockedPnr;
					} else if (bookingManager.isRequiredToAddInfo(segmentInfos)) {
						RadixxRetrieveBookingManager retrieveManager =
								RadixxRetrieveBookingManager.builder().bindingService(bindingService)
										.configuration(supplierConf).airline(airline).bookingId(bookingId)
										.binaryToken(supplierSession.getSupplierSessionInfo().getSessionToken())
										.listener(listener).criticalMessageLogger(criticalMessageLogger).pnr(pnr)
										.bookingUser(bookingUser).build();
						retrieveManager.init();
						RetrievePNRResponse retrievePNRResponse = retrieveManager.retrieveBooking();
						if (bookingManager.addUpdateApiInfo(segmentInfos, retrievePNRResponse)) {
							String pnrAfterAddInfo =
									bookingManager.createPNR(CreatePNRActionTypes.SaveReservation, false);
							if (StringUtils.isNotBlank(pnrAfterAddInfo)) {
								log.info("AddUpdateApisInfo is successful for Booking ID : {} and Confirmed PNR : {}",
										bookingId, pnr);
								isSuccess = true;
							}
						}
					} else {
						log.info("Confirmed PNR {} for Booking ID {}", pnr, bookingId);
						isSuccess = true;
					}
				} else {
					log.info("Blocked PNR {} for Booking ID {}", blockedPnr, bookingId);
					updateTimeLimit(bookingManager.getHoldTimeLimit());
					pnr = blockedPnr;
					isSuccess = true;
				}
			}
		}
		return isSuccess;
	}


}
