package com.tgs.services.fms.sources.sabre;

import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AtlasSabreAirBookingFactory extends SabreAirBookingFactory {

	public AtlasSabreAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	@Override
	protected void sendCommandsPostProcessing() {
		bookingManager.sabreCommandWithRetryLogic(SabreUtils.commandCTCM(order.getDeliveryInfo().getContacts().get(0)));
		bookingManager.sabreCommandWithRetryLogic(SabreUtils.commandCTCE(order.getDeliveryInfo().getEmails().get(0)));
		bookingManager.sabreCommandWithRetryLogic(SabreUtils.pnrRetention(180, bookingSegments.getSegmentInfos()));
		bookingManager.endTransaction();
		return;
	}

}
