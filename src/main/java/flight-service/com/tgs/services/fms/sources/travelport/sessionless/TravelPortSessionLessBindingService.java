package com.tgs.services.fms.sources.travelport.sessionless;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.sources.AirSourceConstants;
import travelport.www.service.sharedbooking_v47_0.SharedBookingServiceStub;
import travelport.www.service.universal_v47_0.PassiveServiceStub;
import travelport.www.service.universal_v47_0.UniversalRecordCancelServiceStub;
import travelport.www.service.universal_v47_0.UniversalRecordImportServiceStub;
import travelport.www.service.universal_v47_0.UniversalRecordModifyServiceStub;
import travelport.www.service.universal_v47_0.UniversalRecordServiceStub;
import travelport.www.schema.universal_v47_0.UniversalRecord;
import travelport.www.service.air_v47_0.AirServiceStub;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import static com.tgs.services.fms.sources.travelport.sessionless.TravelPortSessionLessConstants.*;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.impl.httpclient4.HttpTransportPropertiesImpl;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@Setter
@Getter
@Builder
final class TravelPortSessionLessBindingService {

	private SupplierConfiguration configuration;

	protected List<SupplierAnalyticsQuery> supplierAnalyticsInfos;

	public AirServiceStub getAirService() {
		AirServiceStub airServiceStub = null;
		try {
			String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), AIR_SERVICE);
			long startTime = System.currentTimeMillis();
			airServiceStub = new AirServiceStub(url);
			long endTime = System.currentTimeMillis();
			log.debug("Creating AirServiceStub {}", TimeUnit.MILLISECONDS.toSeconds(endTime - startTime));
		} catch (AxisFault e) {
			throw new CustomGeneralException("Error creating AirService Stub " + e);
		}
		return airServiceStub;
	}


	public UniversalRecordServiceStub getUniversalRecordService() {
		UniversalRecordServiceStub universalRecordService = null;
		if (universalRecordService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), UNIVERSAL_RECORD_SERVICE);
				universalRecordService = new UniversalRecordServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating UniversalRecordServiceStub " + e);

			}
		}
		return universalRecordService;
	}

	public SharedBookingServiceStub getSharedBookingService() {
		SharedBookingServiceStub sharedBookingService = null;
		if (sharedBookingService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), SHARED_BOOKING_SERVICE);
				sharedBookingService = new SharedBookingServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating SharedBookingServiceStub " + e);

			}
		}
		return sharedBookingService;
	}

	public UniversalRecordImportServiceStub getUniversalRecordImportService() {
		UniversalRecordImportServiceStub universalRecordImportService = null;
		if (universalRecordImportService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), UNIVERSAL_RECORD_SERVICE);
				universalRecordImportService = new UniversalRecordImportServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating UniversalRecordImportServiceStub " + e);
			}
		}
		return universalRecordImportService;
	}

	public UniversalRecordModifyServiceStub getUniversalRecordModifyService() {
		UniversalRecordModifyServiceStub universalRecordModifyService = null;
		if (universalRecordModifyService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), UNIVERSAL_RECORD_SERVICE);
				universalRecordModifyService = new UniversalRecordModifyServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating UniversalRecordModifyServiceStub " + e);
			}
		}
		return universalRecordModifyService;
	}

	public UniversalRecordCancelServiceStub getUniversalRecordCancelService() {
		UniversalRecordCancelServiceStub universalRecordCancelService = null;
		if (universalRecordCancelService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), UNIVERSAL_RECORD_SERVICE);
				universalRecordCancelService = new UniversalRecordCancelServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating UniversalRecordCancelServiceStub " + e);
			}
		}
		return universalRecordCancelService;
	}

	public PassiveServiceStub getPassiveService() {
		PassiveServiceStub passiveService = null;
		if (passiveService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), PASSIVE_SERVICE);
				passiveService = new PassiveServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating PassiveServiceStub " + e);
			}
		}
		return passiveService;
	}

	public travelport.www.service.universal_v47_0.AirServiceStub getAirCreateReservationStub() {
		travelport.www.service.universal_v47_0.AirServiceStub airServiceStub = null;
		if (airServiceStub == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), AIR_SERVICE);
				airServiceStub = new travelport.www.service.universal_v47_0.AirServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating AirCreateReservationStub " + e);

			}

		}
		return airServiceStub;
	}

	public travelport.www.service.universal_v47_0.MerchandiseService.AirServiceStub getAirMerchandiseFulfillmentStub() {
		travelport.www.service.universal_v47_0.MerchandiseService.AirServiceStub airServiceStub = null;
		if (airServiceStub == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), AIR_SERVICE);
				airServiceStub = new travelport.www.service.universal_v47_0.MerchandiseService.AirServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating AirMerchandiseFulfillmentStub " + e);
			}

		}
		return airServiceStub;
	}

	public travelport.www.service.air_v47_0.MerchandiseOfferAvailability.AirServiceStub getAirMerchandiseOfferAvailabilityStub() {
		travelport.www.service.air_v47_0.MerchandiseOfferAvailability.AirServiceStub airServiceStub = null;
		if (airServiceStub == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), AIR_SERVICE);
				airServiceStub = new travelport.www.service.air_v47_0.MerchandiseOfferAvailability.AirServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating AirMerchandiseOfferAvailabilityStub " + e);
			}

		}
		return airServiceStub;
	}

	// fare rule
	public com.travelport.www.service.air_v50_0.AirServiceStub getAirService_V50() {
		com.travelport.www.service.air_v50_0.AirServiceStub airServiceStub = null;
		try {
			String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), AIR_SERVICE);
			long startTime = System.currentTimeMillis();
			airServiceStub = new com.travelport.www.service.air_v50_0.AirServiceStub(url);
			long endTime = System.currentTimeMillis();
			log.debug("Creating AirService_V50 {}", TimeUnit.MILLISECONDS.toSeconds(endTime - startTime));
		} catch (AxisFault e) {
			throw new CustomGeneralException("Error creating AirService_V50 Stub " + e);
		}
		return airServiceStub;

	}

	// fare rule

	public void setProxyAndAuthentication(Stub stub, String service) {
		if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
			ServiceClient serviceClient = stub._getServiceClient();
			serviceClient.getOptions().setProperty(HTTPConstants.SO_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			serviceClient.getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			serviceClient.getOptions().setTimeOutInMilliSeconds(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			serviceClient.getOptions().setUserName(configuration.getSupplierCredential().getUserName());
			serviceClient.getOptions().setPassword(configuration.getSupplierCredential().getPassword());
			HttpTransportPropertiesImpl.Authenticator auth = new HttpTransportPropertiesImpl.Authenticator();
			auth.setUsername(configuration.getSupplierCredential().getUserName());
			auth.setPassword(configuration.getSupplierCredential().getPassword());
			auth.setAuthSchemes(Arrays.asList(AuthPolicy.BASIC));
			auth.setPreemptiveAuthentication(true);
			serviceClient.getOptions().setManageSession(true);
			auth.setAllowedRetry(true);
			serviceClient.getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
			setConnectionManager(stub);
			log.debug("BasicAuth Added for Service {}", service);
		}
	}

	public void updateOptions(Stub stub) {
		if (stub != null) {
			ServiceClient client = stub._getServiceClient();
			Options options = client.getOptions();
			options.setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, Boolean.TRUE);
			client.setOptions(options);
		}
	}

	public void setConnectionManager(Stub stub) {
		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(30);
		conmgr.getParams().setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		conmgr.getParams().setSoTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		HttpClient client = new HttpClient(conmgr);
		stub._getServiceClient().getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
	}
}
