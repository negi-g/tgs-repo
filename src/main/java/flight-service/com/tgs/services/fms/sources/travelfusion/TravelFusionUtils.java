package com.tgs.services.fms.sources.travelfusion;

import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.MASTER;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.MISS;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.amazonaws.util.NumberUtils;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.CardType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleTimeWindow;
import com.tgs.services.fms.helper.AirportHelper;
import TravelFusionRequests.TfClassEnumerationType;
import TravelFusionResponses.RouterStopListType;
import TravelFusionResponses.RouterStopType;
import TravelFusionResponses.RouterSupplierFeatureConditionType;
import TravelFusionResponses.RouterSupplierFeatureListType;
import TravelFusionResponses.RouterSupplierFeatureOptionType;
import TravelFusionResponses.RouterSupplierFeatureType;
import travelfusion.datamodel.TravelFusionFeatureFact;

public class TravelFusionUtils {

	public static String getTitle(String value) {
		if (value.equalsIgnoreCase(MASTER)) {
			return ("Mr");
		} else if (value.equalsIgnoreCase(MISS)) {
			return ("Miss");
		}
		return value;
	}

	public static Period getAge(LocalDate dob, LocalDate travelDate) {
		Period age = Period.between(dob, travelDate);
		return age;
	}

	public static TfClassEnumerationType getCabinType(AirSearchQuery searchQuery) {
		return (TravelFusionCabinClass.valueOf(searchQuery.getCabinClass().getName().toUpperCase())).getCabin();
	}

	public static String getCardType(CardType type) {
		String cardType = null;
		if (CardType.VISA.equals(type)) {
			cardType = TravelFusionConstants.VISA;
		} else if (CardType.AMERICAN_EXPRESS.equals(type)) {
			cardType = TravelFusionConstants.AMERICAN_EXPRESS;
		} else if (CardType.DINERS_CLUB.equals(type)) {
			cardType = TravelFusionConstants.DINERS_CLUB;
		} else if (CardType.MASTERCARD.equals(type)) {
			cardType = TravelFusionConstants.MASTERCARD;
		}
		return cardType;
	}

	public static String getRequestDate(LocalDate date) {
		return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "-00:00";
	}

	public static String getDate(LocalDate date) {
		return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	public static LocalDate getTravellerDOB(FlightTravellerInfo travellerInfo) {
		if (travellerInfo.getDob() == null) {
			return TgsDateUtils.calenderToLocalDate(TgsDateUtils.getDefaultDOB());
		}
		return travellerInfo.getDob();
	}

	public static LocalDateTime getResponseDate(String date) {
		return LocalDateTime.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy-HH:mm"));
	}

	public static String formatRQRS(String message, String type) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(message)) {
			buffer.append(type + " : " + LocalDateTime.now().toString()).append("\n\n").append(message).append("\n\n");
		}
		return buffer.toString();
	}

	protected static List<AirportInfo> getStopOverAirports(RouterStopListType stopList) {
		List<AirportInfo> airportStops = new ArrayList<>();
		for (RouterStopType routerStop : stopList.getStop()) {
			if (StringUtils.isNotBlank(routerStop.getLocation().getCode())) {
				AirportInfo stopOver = AirportHelper.getAirport(routerStop.getLocation().getCode());
				stopOver.setTerminal(routerStop.getLocation().getTerminal());
				airportStops.add(stopOver);
			}
		}
		return airportStops;
	}

	protected static String getTripId(List<SegmentInfo> segmentInfos, boolean isReturn) {
		String tripId = null;
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (segmentInfo.getSegmentNum().intValue() == 0 && isReturn
					&& BooleanUtils.isTrue(segmentInfo.getIsReturnSegment())) {
				tripId = segmentInfo.getPriceInfo(0).getMiscInfo().getFareKey();
			} else {
				if (segmentInfo.getSegmentNum().intValue() == 0 && !isReturn
						&& !BooleanUtils.isTrue(segmentInfo.getIsReturnSegment())) {
					tripId = segmentInfo.getPriceInfo(0).getMiscInfo().getFareKey();
				}
			}
		}
		return tripId;
	}

	public static boolean isPresent(String values, String value) {
		if (StringUtils.isNotBlank(values)) {
			List<String> valueList = Arrays.asList(values.split(","));
			if (valueList.contains(value))
				return true;
		}
		return false;
	}

	public static RouterSupplierFeatureType getFeatureByType(RouterSupplierFeatureListType features, String type,
			TravelFusionFeatureFact featureFact) {
		RouterSupplierFeatureType featureByType = null;
		if (features != null && CollectionUtils.isNotEmpty(features.getFeature())) {
			List<RouterSupplierFeatureType> filteredFeature = features.getFeature().stream()
					.filter(feature -> (type.equals(feature.getType()))).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(filteredFeature)) {
				List<RouterSupplierFeatureOptionType> validOptions =
						TravelFusionUtils.filterOptionByConditions(filteredFeature.get(0).getOption(), featureFact);
				if (CollectionUtils.isNotEmpty(validOptions)) {
					featureByType = new GsonMapper<>(filteredFeature.get(0), RouterSupplierFeatureType.class).convert();
					featureByType.getOption().removeIf(op -> {
						return true;
					});
					featureByType.getOption().addAll(validOptions);
				}
			}
		}
		return featureByType;
	}

	private static List<RouterSupplierFeatureOptionType> filterOptionByConditions(
			List<RouterSupplierFeatureOptionType> options, TravelFusionFeatureFact featureFact) {
		List<RouterSupplierFeatureOptionType> validOptions = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(options)) {
			for (RouterSupplierFeatureOptionType option : options) {
				if (TravelFusionUtils.validateConditions(option.getCondition(), featureFact)) {
					validOptions.add(option);
				}
			}
		}
		return validOptions;
	}

	private static boolean validateConditions(List<RouterSupplierFeatureConditionType> conditions,
			TravelFusionFeatureFact fact) {
		boolean isValid = true;
		if (CollectionUtils.isNotEmpty(conditions)) {
			for (RouterSupplierFeatureConditionType condition : conditions) {
				String type = condition.getType();
				if (TravelFusionConstants.TRAVELLER_TYPE.equals(type) && fact.getTravellerType() != null) {
					isValid = isValid && TravelFusionUtils.isPresent(condition.getValue(), fact.getTravellerType());
				} else if (TravelFusionConstants.SUPPLIER_CLASS.equals(type) && fact.getSupplierClass() != null) {
					isValid = isValid && TravelFusionUtils.isPresent(condition.getValue(), fact.getSupplierClass());
				} else if (TravelFusionConstants.ROUTE_TYPE.equals(type) && fact.getRouteType() != null) {
					isValid = isValid && TravelFusionUtils.isPresent(condition.getValue(), fact.getRouteType());
				} else if (TravelFusionConstants.PHASE.equals(type) && fact.getPhase() != null) {
					isValid = isValid && TravelFusionUtils.isPresent(condition.getValue(), fact.getPhase());
				} else if (TravelFusionConstants.CHANNEL.equals(type) && fact.getChannel() != null) {
					isValid = isValid && TravelFusionUtils.isPresent(condition.getValue(), fact.getChannel());
				}
			}
		}
		return isValid;
	}

	public static String getFareRuleText(FareRuleTimeWindow timeWindow,
			List<RouterSupplierFeatureConditionType> conditions) {
		StringJoiner joiner = new StringJoiner(";");
		for (RouterSupplierFeatureConditionType condition : conditions) {
			Integer value = NumberUtils.tryParseInt(condition.getValue());

			// After departure
			if (FareRuleTimeWindow.AFTER_DEPARTURE.equals(timeWindow)) {

			}

			// Before departure
			if (FareRuleTimeWindow.BEFORE_DEPARTURE.equals(timeWindow)) {
				if (TravelFusionConstants.MIN_TIME_BEFORE_DEPARTURE.equals(condition.getType()) && value != null) {
					joiner.add(StringUtils.join("Changes permitted ", (value / 60), ":", (value % 60), "hrs",
							" before scheduled departure"));
				}
			}

			if (TravelFusionConstants.CHARGE_MODEL.equals(condition.getType())
					&& StringUtils.isNotBlank(condition.getValue())) {
				joiner.add(StringUtils.join("Charge Applicable as ", condition.getValue()));
			}

			if (TravelFusionConstants.MIN_TIME_AFTER_BOOKING.equals(condition.getType()) && value != null
					&& value > 0) {
				joiner.add(StringUtils.join("Changes permitted after ", (value / 60), ":", (value % 60), "hrs",
						" from booking "));
			}
		}
		return joiner.toString();
	}

}
