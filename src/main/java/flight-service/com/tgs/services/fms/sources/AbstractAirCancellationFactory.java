package com.tgs.services.fms.sources;

import java.time.LocalDateTime;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BookingUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.datamodel.MessageType;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceCancelConfiguration;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.datamodel.cancel.JourneyCancellationDetail;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.manager.FareRuleManager;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.utils.AirCancelUtils;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.air.CancellationException;
import com.tgs.utils.exception.air.NoPNRFoundException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.Setter;
import java.util.ArrayList;
import java.util.List;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import static com.tgs.services.base.enums.FareComponent.*;

@Slf4j
@Getter
@Setter
@Service
@NoArgsConstructor
public abstract class AbstractAirCancellationFactory {

	protected BookingSegments bookingSegments;

	protected SupplierConfiguration supplierConf;

	@Autowired
	protected AirOrderItemCommunicator itemComm;

	@Autowired
	protected GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	protected GeneralCachingCommunicator cachingComm;

	@Value("${env}")
	private String env;

	protected String pnr;

	protected Order order;

	protected String bookingId;

	@Value("${pbuid}")
	private String prodBookingUserId;

	protected List<String> criticalMessageLogger;

	protected AirSourceConfigurationOutput sourceConfiguration;

	protected AirSourceCancelConfiguration cancelConfiguration;

	protected boolean isHoldBooking;

	protected boolean isCancellationReview;

	protected User bookingUser;

	@Autowired
	private UserServiceCommunicator userServiceCommunicator;

	@Autowired
	private FareRuleManager fareRuleManager;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchangeComm;

	private AirCancellationResponse airCancellationReviewResponse;

	public AbstractAirCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(bookingSegments.getSegmentInfos());
		this.order = order;
		this.supplierConf = supplierConf;
		bookingId = order.getBookingId();
		pnr = bookingSegments.getAirlinePNR();
		this.bookingSegments = bookingSegments;
		userServiceCommunicator = SpringContext.getApplicationContext().getBean(UserServiceCommunicator.class);
		bookingUser = userServiceCommunicator.getUserFromCache(order.getBookingUserId());
		sourceConfiguration = AirUtils.getAirSourceConfigOnTripInfo(tripInfo, bookingUser);
		cancelConfiguration = AirCancelUtils.getAirSourceCancelConfiguration(supplierConf.getBasicInfo(),
				bookingSegments, bookingUser);
		criticalMessageLogger = new ArrayList<>();
	}

	public boolean releasePNR() {
		log.info("Inside AbstractAirCancellationFactory#releasePNR for bookingId {} Supplier {}", bookingId,
				supplierConf.getBasicInfo().getSupplierId());
		boolean isReleased = false;
		this.isHoldBooking = getIsHoldBook(order);
		try {
			if (isCancellationAllowed())
				isReleased = releaseHoldPNR();
			else if (isTestEnvironmentMode())
				isReleased = true;
		} catch (SupplierUnHandledFaultException | NoPNRFoundException | SupplierSessionException e) {
			criticalMessageLogger.add(e.getMessage());
			log.error(AirSourceConstants.CANCELLATION_FAILED, pnr, bookingId, e);
		} catch (SupplierRemoteException re) {
			criticalMessageLogger.add(re.getMessage());
			log.error(AirSourceConstants.LOG_SUPPLIER_REMOTE_FAILED, supplierConf.getSourceId(), pnr, re.getMessage());
		} catch (Exception e) {
			log.error(AirSourceConstants.LOG_CANCELLATION_FAILED, pnr, bookingId, e);
		} finally {
			LogUtils.clearLogList();
			updateOrderAndItemStatus(isReleased);
			addCancellationMessageToNotes(isReleased);
		}
		return isReleased;
	}

	public AirCancellationDetail reviewCancellation() {
		AirCancellationDetail cancellationDetail = AirCancellationDetail.builder().build();
		try {
			if (isCancellationAllowed()) {
				cancellationDetail = this.reviewBookingForCancellation(cancellationDetail);
			}
		} catch (CancellationException ce) {
			cancellationDetail.setAutoCancellationAllowed(Boolean.FALSE);
			cancellationDetail.buildCancellationDetail(bookingId, bookingSegments, ce.getMessage());
			criticalMessageLogger.add(ce.getMessage() + " for this Amendment");
		} catch (SupplierUnHandledFaultException | NoPNRFoundException | SupplierSessionException e) {
			cancellationDetail.setAutoCancellationAllowed(Boolean.FALSE);
			cancellationDetail.buildCancellationDetail(bookingId, bookingSegments, e.getMessage());
			addCancellationMessageToBookingSegments();
			criticalMessageLogger.add(e.getMessage());
			log.error(AirSourceConstants.CANCELLATION_FAILED, pnr, bookingId, e);
		} catch (SupplierRemoteException re) {
			cancellationDetail.buildCancellationDetail(bookingId, bookingSegments, re.getMessage());
			cancellationDetail.setAutoCancellationAllowed(Boolean.FALSE);
			cancellationDetail.buildCancellationDetail(bookingId, bookingSegments, re.getMessage());
			addCancellationMessageToBookingSegments();
			log.error(AirSourceConstants.LOG_SUPPLIER_REMOTE_FAILED, supplierConf.getSourceId(), pnr, re.getMessage());
		} catch (Exception e) {
			cancellationDetail.buildCancellationDetail(bookingId, bookingSegments, e.getMessage());
			bookingSegments.setSegmentInfos(null);
			cancellationDetail.setAutoCancellationAllowed(Boolean.FALSE);
			log.error(AirSourceConstants.LOG_CANCELLATION_FAILED, pnr, bookingId, e);
		} finally {
			if (!isTravellerCancellationFeeZero(cancellationDetail)) {
				cancellationDetail.buildCancellationDetail(bookingId, bookingSegments, null);
				setClientCancellationFee(bookingUser, cancellationDetail);
			}
			addCancellationMessageToNotes(true);
			LogUtils.clearLogList();
		}
		return cancellationDetail;
	}


	public boolean isTravellerCancellationFeeZero(AirCancellationDetail cancellationDetail) {
		for (SegmentInfo segmentInfo : bookingSegments.getCancelSegments()) {
			if (segmentInfo.getSegmentNum().equals(0)) {
				for (FlightTravellerInfo flightTravellerInfo : segmentInfo.getTravellerInfo()) {
					if (!flightTravellerInfo.getPaxType().equals(PaxType.INFANT) && (flightTravellerInfo.getFareDetail()
							.getFareComponents().get(FareComponent.ACF) == null)) {
						log.error("Air Cancellation Fees is zero for this bookingId {}  for this passenger {}",
								bookingId, flightTravellerInfo);
						cancellationDetail.buildMessageInfo(bookingId, bookingSegments,
								"AirCancellation Fee is zero for this Passenger " + flightTravellerInfo.getFullName());
						cancellationDetail.setAutoCancellationAllowed(Boolean.FALSE);
						return true;
					}
				}
			}
		}
		return false;
	}

	private void setClientCancellationFee(User user, AirCancellationDetail airCancellationDetail) {
		try {

			List<TripInfo> tripInfos = BookingUtils.createTripListFromSegmentList(bookingSegments.getSegmentInfos());
			for (TripInfo tInfo : tripInfos) {
				// journey wise fare merge
				tInfo.setTripPriceInfos(AirUtils.getTripTotalPriceInfoList(tInfo, user));
			}
			for (SegmentInfo segmentInfo : bookingSegments.getSegmentInfos()) {
				fareRuleManager.updateCancellationAndReschedulingFees(segmentInfo, user, AmendmentType.CANCELLATION);
				fareRuleManager.updateAmendmentMarkup(segmentInfo, user, AmendmentType.CANCELLATION);
				for (Long segmentId : airCancellationDetail.getTravellers().keySet()) {
					if (segmentId.equals(Long.valueOf(segmentInfo.getId()))) {
						List<FlightTravellerInfo> flightTravellerInfos =
								airCancellationDetail.getTravellers().get(segmentId);
						for (FlightTravellerInfo flightTravellerInfo : flightTravellerInfos) {
							for (FlightTravellerInfo cancelledTravellerInfo : segmentInfo.getTravellerInfo()) {
								if (cancelledTravellerInfo.getId().equals(flightTravellerInfo.getId())) {
									if (segmentInfo.getSegmentNum().equals(0)) {
										TripInfo tripInfo = getTripFromSegmentId(tripInfos, segmentInfo);
										if (tripInfo != null) {
											fareRuleManager.updateCancellationCCF(flightTravellerInfo, segmentInfo,
													user, tripInfo);

											/**
											 * @implSpec :
											 *           https://docs.google.com/document/d/11jN7r-OX2bWIID_3lrMZx7j03L6ikNO6BYJDYBOGUzU/edit
											 */

											PaxType paxType = flightTravellerInfo.getPaxType();
											FareDetail modifiedFareDetail = cancelledTravellerInfo.getFareDetail();
											FareDetail withoutModificationFD = flightTravellerInfo.getFareDetail();

											Double orgTotalAirlineCancellationFee = withoutModificationFD
													.getComponentsSum(new HashSet<>(Arrays.asList(ACF)));
											Double cancellationMarkup =
													modifiedFareDetail.getFareComponents().get(CAMU);

											if (cancellationMarkup != null) {
												Double totalCancellationCharges =
														orgTotalAirlineCancellationFee + cancellationMarkup;


												// total journey base fare

												Double baseFare =
														tripInfo.getTripPriceInfos().get(0).getFareDetail(paxType)
																.getFareComponents().getOrDefault(BF, 0d);
												Integer compareTo = Double.compare(baseFare, totalCancellationCharges);

												// base fare is lower than org airline cancellation charges (rule
												// violation)
												if (Double.compare(baseFare, orgTotalAirlineCancellationFee) == -1) {
													throw new CancellationException(
															"AirlineCancellationFee Higher than BaseFare,Cancellation Not Allowed");
												}

												if (compareTo == -1) {
													cancellationMarkup = baseFare - orgTotalAirlineCancellationFee;
												}

												withoutModificationFD.getFareComponents().put(CAMU, cancellationMarkup);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} finally {
			SystemContextHolder.getContextData().setGson(null);
		}
	}

	protected TripInfo getTripFromSegmentId(List<TripInfo> tripInfos, SegmentInfo segmentInfo) {
		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segmentInfo1 : tripInfo.getSegmentInfos()) {
				if (StringUtils.equals(segmentInfo1.getId(), segmentInfo.getId())) {
					return tripInfo;
				}
			}
		}
		return null;
	}

	public boolean isSupplierCancellationAllowed(Integer sourceId) {
		boolean isCancellationAllowed = false;
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(bookingSegments.getSegmentInfos());
		LocalDateTime departureTime = this.bookingSegments.getCancelSegments().get(0).getDepartTime();
		String fareType = this.bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getOriginalFareType();
		AirType airType = AirUtils.getAirType(tripInfo);
		if (cancelConfiguration != null) {
			isCancellationAllowed =
					AirCancelUtils.isBasedOnFareTypeAllowed(cancelConfiguration, fareType, criticalMessageLogger)
							&& AirCancelUtils.isDependsOnDepartureTime(cancelConfiguration, airType, departureTime,
									fareType, criticalMessageLogger);
		}
		// addCancellationMessageToNotes(true);
		boolean isPassengerWiseCancellation = false;
		for (int index = 0; index < bookingSegments.getCancelSegments().size(); index++) {
			SegmentInfo cancelledSegment = bookingSegments.getCancelSegments().get(index);
			SegmentInfo originalSegment = bookingSegments.getSegmentInfos().get(index);
			if (cancelledSegment.getTravellerInfo().size() != originalSegment.getTravellerInfo().size()) {
				isPassengerWiseCancellation = true;
				break;
			}
		}
		if (isPassengerWiseCancellation) {
			isCancellationAllowed = isPassengerWiseCancellationAllowed(sourceId);
			if (!isCancellationAllowed) {
				criticalMessageLogger.add("PassengerWise Cancellation not allowed");
			}
		}
		return isCancellationAllowed;
	}

	protected boolean isPassengerWiseCancellationAllowed(Integer sourceId) {
		return false;
	}

	protected void addCancellationMessageToBookingSegments() {
		MessageInfo messageInfo = MessageInfo.builder().type(MessageType.CANCELLATION)
				.message(SystemError.INVALID_CANCELLATION_PNR.getMessage()).build();
		this.bookingSegments.getSegmentInfos().forEach(segmentInfo -> {
			this.bookingSegments.getMessages().put(String.valueOf(segmentInfo.getId()), messageInfo);
		});
	}

	public AirCancellationDetail confirmCancellation() {
		AirCancellationDetail cancellationDetail = AirCancellationDetail.builder().build();
		boolean isCancelled = false;
		try {
			if (isCancellationAllowed() && isSamePaxAndSegmentCount()) {
				isCancelled = this.confirmBookingForCancellation();
				itemComm.updateOrderAndItem(bookingSegments.getSegmentInfos(), order, null);
			}
		} catch (SupplierUnHandledFaultException | NoPNRFoundException | SupplierSessionException e) {
			addCancellationMessageToBookingSegments();
			criticalMessageLogger.add(e.getMessage());
			log.error(AirSourceConstants.CANCELLATION_FAILED, pnr, bookingId, e);
		} catch (SupplierRemoteException re) {
			addCancellationMessageToBookingSegments();
			log.error(AirSourceConstants.LOG_SUPPLIER_REMOTE_FAILED, supplierConf.getSourceId(), pnr, re.getMessage());
		} catch (Exception e) {
			bookingSegments.setSegmentInfos(null);
			log.error(AirSourceConstants.LOG_CANCELLATION_FAILED, pnr, bookingId, e);
		} finally {
			cancellationDetail.setAutoCancellationAllowed(isCancelled);
			if (!criticalMessageLogger.isEmpty()) {
				cancellationDetail.buildCancellationDetail(bookingId, bookingSegments,
						String.join(",", criticalMessageLogger));
			} else {
				cancellationDetail.buildCancellationDetail(bookingId, bookingSegments, null);
			}
			if (BooleanUtils.isTrue(cancellationDetail.getAutoCancellationAllowed())) {
				setClientCancellationFee(bookingUser, cancellationDetail);
			}
			// updateTravellerStatus(isCancelled);
			addCancellationMessageToNotes(isCancelled);
			LogUtils.clearLogList();
			if (isCancelled) {
				log.info("Auto cancellation done for bookingid {} for supplier {} ", bookingId,
						supplierConf.getBasicInfo());
			}
		}
		return cancellationDetail;
	}

	private boolean isSamePaxAndSegmentCount() {
		if (airCancellationReviewResponse.getCancellationDetail() != null
				&& MapUtils.isNotEmpty(airCancellationReviewResponse.getCancellationDetail().getTravellers())) {
			Map<Long, List<FlightTravellerInfo>> cancellingTravellers =
					airCancellationReviewResponse.getCancellationDetail().getTravellers();
			for (Map.Entry<Long, List<FlightTravellerInfo>> cancellingSeg : cancellingTravellers.entrySet()) {
				SegmentInfo cancellingSegment = bookingSegments.getCancelSegments().stream()
						.filter(s -> s.getId().equals(cancellingSeg.getKey().toString())).findAny().orElse(null);
				if (cancellingSegment != null) {
					if (cancellingSegment.getTravellerInfo().size() != cancellingSeg.getValue().size()) {
						log.error(
								"The pax count in cancellation review response and cancel segments for seg Id {} dont match",
								cancellingSegment.getId());
						return false;
					}
					for (FlightTravellerInfo traveller : cancellingSeg.getValue()) {
						Optional<FlightTravellerInfo> infoOptional =
								AirUtils.findTraveller(cancellingSegment.getTravellerInfo(), traveller);
						if (!infoOptional.isPresent()) {
							log.info("Traveller {} not found in cancelling segment {}", traveller.getFullName(),
									cancellingSeg.getKey());
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	public boolean isFareDiff(double airlineCancellationFees) {

		boolean isFareDiffFromReviewCharges = isFareDiffFromReviewCharges();
		double reviewCancellationFare = getReviewedCancellationFare();

		if (isFareDiffFromReviewCharges) {
			log.info("Fare Difference on Booking {} for reviewCancellationFare{}  and airlineCancellationFees {}",
					bookingId, reviewCancellationFare, airlineCancellationFees);
			String noteMessage = Note.getNoteMessage(null,
					StringUtils.join("Total amount to refund from airline - ", airlineCancellationFees,
							". Total amount to refund from airline review - ", reviewCancellationFare));
			Note note =
					Note.builder().bookingId(bookingId).noteType(NoteType.FAREJUMP).noteMessage(noteMessage).build();
			gmsCommunicator.addNote(note);
		}

		return isFareDiffFromReviewCharges;
	}

	protected boolean isFareDiffFromReviewCharges() {
		if (airCancellationReviewResponse.getCancellationDetail() != null
				&& MapUtils.isNotEmpty(airCancellationReviewResponse.getCancellationDetail().getTravellers())) {
			Map<Long, List<FlightTravellerInfo>> cancellingTravellers =
					airCancellationReviewResponse.getCancellationDetail().getTravellers();
			for (Map.Entry<Long, List<FlightTravellerInfo>> cancellingSeg : cancellingTravellers.entrySet()) {
				SegmentInfo cancellingSegment = bookingSegments.getCancelSegments().stream()
						.filter(s -> s.getId().equals(cancellingSeg.getKey().toString())).findAny().orElse(null);

				if (cancellingSegment != null) {
					if (cancellingSegment.getTravellerInfo().size() != cancellingSeg.getValue().size()) {
						log.error(
								"For bookingId {} The pax count in cancellation review response and cancel segments for seg Id {} dont match",
								bookingId, cancellingSegment.getId());
						String noteMessage = Note.getNoteMessage(null, StringUtils.join(
								"The pax count in cancellation review response and cancel segments for segment id ",
								cancellingSegment.getId()));
						Note note = Note.builder().bookingId(bookingId).noteType(NoteType.GENERAL)
								.noteMessage(noteMessage).build();
						gmsCommunicator.addNote(note);
						return true;
					}
					for (FlightTravellerInfo cancellingTraveller : cancellingSeg.getValue()) {
						Optional<FlightTravellerInfo> infoOptional =
								AirUtils.findTraveller(cancellingSegment.getTravellerInfo(), cancellingTraveller);
						if (infoOptional.isPresent()) {
							// when fare mismatch
							Double airlineCancellationFee = infoOptional.get().getFareDetail().getFareComponents()
									.getOrDefault(FareComponent.ACF, 0d);
							Double reviewedCancellationFee = cancellingTraveller.getFareDetail().getFareComponents()
									.getOrDefault(FareComponent.ACF, 0d);
							if (Double.compare(airlineCancellationFee, reviewedCancellationFee) != 0) {
								log.error(
										"For bookingId {} Pax Cancellation charges dont match for pax id {} airline charges {} review charges {} in segment id {}",
										bookingId, infoOptional.get().getId(), airlineCancellationFee,
										reviewedCancellationFee, cancellingSegment.getId());
								String noteMessage = Note.getNoteMessage(null,
										StringUtils.join("amount to refund from airline - ", airlineCancellationFee,
												". amount to refund from airline review - ", reviewedCancellationFee,
												". for pax id ", infoOptional.get().getId(), "for segment id",
												cancellingSegment.getId()));
								Note note = Note.builder().bookingId(bookingId).noteType(NoteType.FAREJUMP)
										.noteMessage(noteMessage).build();
								gmsCommunicator.addNote(note);
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	public double getReviewedCancellationFare() {
		AtomicDouble reviewCancellationFare = new AtomicDouble(0);
		for (Entry<String, JourneyCancellationDetail> entry : getAirCancellationReviewResponse().getCancellationDetail()
				.getJourneyCancellationDetails().entrySet()) {
			for (Entry<PaxType, FareDetail> fareDetail : entry.getValue().getFareDetails().entrySet()) {
				if (entry.getValue().getPnr().equals(pnr)) {
					Integer paxTypeCount = entry.getValue().getPaxCount().get(fareDetail.getKey());
					reviewCancellationFare
							.getAndAdd(fareDetail.getValue().getFareComponents().get(FareComponent.ACF) * paxTypeCount);
				}
			}
		}
		return reviewCancellationFare.doubleValue();
	}


	private void updateTravellerStatus(boolean isCancelled) {
		if (isCancelled) {
			bookingSegments.getCancelSegments().forEach(segementInfo -> {
				segementInfo.getTravellerInfo().forEach(flightTravellerInfo -> {
					flightTravellerInfo.setStatus(TravellerStatus.CANCELLED);
				});
			});
		}
	}

	public boolean confirmBookingForCancellation() {
		return false;
	}

	public AirCancellationDetail reviewBookingForCancellation(AirCancellationDetail cancellationDetail) {
		return null;
	}

	private boolean isCancellationAllowed() {
		return BooleanUtils.isTrue(this.getSupplierConf().getSupplierCredential().getIsTestCredential())
				|| "prod".equalsIgnoreCase(env)
				|| (prodBookingUserId != null && prodBookingUserId.contains(order.getBookingUserId() + ","));
	}

	private boolean isTestEnvironmentMode() {
		return "test".equals(env) || BooleanUtils.isFalse(supplierConf.getSupplierCredential().getIsTestCredential());
	}

	private void updateOrderAndItemStatus(boolean isAbortSuccess) {
		if (isAbortSuccess) {
			itemComm.updateOrderAndItem(bookingSegments.getSegmentInfos(), order, AirItemStatus.UNCONFIRMED);
			Note holdAbortNote = Note.builder().bookingId(bookingId).noteType(NoteType.PNR_RELEASED)
					.noteMessage("PNR Released :SYSTEM GENERATED").build();
			gmsCommunicator.addNote(holdAbortNote);
		}
	}

	public boolean isSupplierAutoCancellationAllowed(Integer souceId) {
		return BooleanUtils.isTrue(sourceConfiguration.getIsAutoCancellationAllowed())
				&& isSupplierCancellationAllowed(souceId);
	}

	public boolean isSupplierCancellationAllowed() {
		return true;
	}

	public boolean releaseHoldPNR() {
		return true;
	}


	private boolean getIsHoldBook(Order order) {
		boolean isHoldBook = false;
		if (order.getAdditionalInfo() == null || order.getAdditionalInfo().getPaymentStatus() == null) {
			isHoldBook = true;
		} else if (order.getAdditionalInfo() != null && order.getAdditionalInfo().getPaymentStatus() != null
				&& !order.getAdditionalInfo().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
			throw new CustomGeneralException("Cancellation Not Allowed, Due To Payment is Done!");
		}
		return isHoldBook;
	}

	public void addCancellationMessageToNotes(boolean isReleased) {
		List<String> filteredCriticalMessages =
				AirSupplierUtils.filterCriticalMessage(criticalMessageLogger, isReleased, bookingUser);
		if (CollectionUtils.isNotEmpty(filteredCriticalMessages)) {
			filteredCriticalMessages = filteredCriticalMessages.stream().distinct().collect(Collectors.toList());
			String supplierMessage = String.join(",", filteredCriticalMessages);
			if (StringUtils.isNotBlank(supplierMessage)) {
				log.info("Note message {} for bookingid {} ", supplierMessage, bookingId);
				Note note = Note.builder().bookingId(bookingId).noteType(NoteType.SUPPLIER_MESSAGE)
						.noteMessage(supplierMessage).build();
				gmsCommunicator.addNote(note);
			}
		}
	}

	public FlightAPIURLRuleCriteria getEndPointURL() {
		FlightAPIURLRuleCriteria flightUrls = AirUtils.getAirEndPointURL(sourceConfiguration);
		return flightUrls;
	}


}
