package com.tgs.services.fms.sources.tbo;

import lombok.Getter;

@Getter
public enum TravelBoutiquePaxType {

	ADULT(1), CHILD(2), INFANT(3);

	private int value;

	private TravelBoutiquePaxType(int value) {
		this.value = value;
	}
}
