package com.tgs.services.fms.sources;

import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import lombok.Getter;

@Getter
public class AirSourceConstants {

	// 30 seconds
	public static Integer TIME_OUT_IN_SECONDS = 30;
	public static Integer TIME_OUT_IN_MILLI_SECONDS = TIME_OUT_IN_SECONDS * 1_000;

	// Exception Related to Supplier Session Open
	public static final String AUHTENTICATION_ERROR = "Authentication Failed  ";

	// Exception Related to Supplier Session
	public static final String NO_SEARCH_RESULT = "No Search Result Availability";
	public static final String LOG_NO_SEARCH_RESULT = "No Search Result for {} sq {} error {}";
	public static final String REMOTE_FAILED = "Supplier Network Failed ";
	public static final String SEARCH_SESSION_FAILURE = "Unable to store session in queue for {}";

	public static final String NO_SEAT_AVAILABLE = "Selected Seats Are No longer available error {} for {} TripInfo {}";
	public static final String NOT_OPERATING_SECTOR = "Not Operating Sector";
	public static final String NO_SCHEDULE_FOUND = "No Schedule Found";
	public static final String FARERULE_FAILED = "FareRule Failed";
	public static final String LOG_FARERULE_FAILED = "FareRule Failed for {} {} tripinfo {} error {} ";

	public static final String LOG_SUPPLIER_REMOTE_FAILED = "Supplier Network Failed for {} bookingid {} error {}";
	public static final String LOG_SUPPLIER_SOAP_FAILED = "Soap Failed for source {} searchid {} error {}";

	// Exception Related to Booking
	public static final String BOOK_FAILED = "Booking Failed for bookingId {} Supplier {}";
	public static final String BOOK_TICKETING_FAILED = "Ticketing Failed for bookingId {} Supplier {}";
	public static final String CANCELLATION_FAILED = "Cancellation failed for the PNR {} BookingId {} error {}";
	public static final String LOG_CANCELLATION_FAILED = "Cancellation failed for the PNR {} BookingId {}";
	public static final String FARE_CHANGE_FAILED = "Confirm fare before ticket failed {}";
	public static final String MANUAL_ORDER_FAILED = "Manual Order Booking Failed for bookingId {}";
	public static final String IMPORT_PNR_FAILED = "Import PNR Failed for PNR {} Supplier {} error {}";

	// Exception Related to Supplier Session Close
	public static final String SESSION_CLOSE = "Session Closing failed for {}";

	// Exception Related to Code level
	public static final String REVIEW_FAILED = "Trip Review Failed for bookingId {} tripinfo {}";
	public static final String REVIEW_PRICE_FAILED = "Trip Review Price Failed";
	public static final String AIR_SUPPLIER_SEARCH = "Error Occured On supplier Search {} sq {} ";
	public static final String SUPPLIER_SEAT_FAILED = "Seat Map Fetch Failed {}";

	// Exception related to particular Supplier
	public static final String AIR_SUPPLIER_SEARCH_PRICE_THREAD = "Error Occured On Thread Price Search {}";
	public static final String AIR_SUPPLIER_PNR_THREAD = "Error Occured On Thread PNR {}";

	// Time Limit
	public static final String NOT_PARSABLE_TIME_LIMIT = "Unable to parse time limit for booking {} timelimit {}";
	public static final String TIME_LIMIT_NOT_RECEIVED = "No Time limit Received for Booking id {} timelimit {} ";

	// Exception related to particular Supplier
	public static final String AIR_SUPPLIER_CHANGE_CLASS_THREAD = "Error Occured On Thread ChangeClass Search ";

	// Import PNR Exceptions
	public static final String AIR_PNR_JOURNEY_UNAVAILABLE = "No PNR/Journey Available for ";
	public static final String AIR_PNR_PRICING_UNAVAILABLE = "No Pricing available for ";

	public static final String SEGMENT_INFO_PARSING_ERROR = "Segment Info parsing error for {} cause ";

	public static final String HOLD_TIME_EXPIRED = "Expired Time Limit ";

	public static final String FARE_CHANGE = "Fare Change ";

	public static final String TIMEOUT = "timeout";


	// CANCELLATION
	public static final String CANCELLATION_NA = "Cancellation Not Allowed";


	public static String getClientCurrencyCode() {
		return ServiceCommunicatorHelper.getClientInfo().getCurrencyCode();
	}

	public static String getSupplierInfoCurrencyCode(SupplierConfiguration configuration) {
		return configuration.getSupplierCredential().getCurrencyCode();
	}
}
