package com.tgs.services.fms.dbmodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.RestExclude;
import com.tgs.services.base.datamodel.AirportInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Entity(name = "airportinfo")
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "airportinfo")
public class DbAirportInfo extends BaseModel<DbAirportInfo, AirportInfo> {

	@Column(unique = true)
	private String code;
	@Column
	private String name;
	@Column
	private String city;
	@Column
	private String country;
	@Column
	private String countryCode;
	@Column
	private String cityCode;

	@RestExclude
	@Column
	private Boolean enabled;

	@RestExclude
	@Column
	private Double priority;

	@Override

	public DbAirportInfo from(AirportInfo dataModel) {
		dataModel.setCode(StringUtils.upperCase(dataModel.getCode()));
		dataModel.setCountryCode(StringUtils.upperCase(dataModel.getCountryCode()));
		dataModel.setName(StringUtils.capitalize(StringUtils.lowerCase(dataModel.getName())));
		dataModel.setCity(StringUtils.capitalize(StringUtils.lowerCase(dataModel.getCity())));
		return new GsonMapper<>(dataModel, this, DbAirportInfo.class).convert();
	}

	public com.tgs.services.base.datamodel.AirportInfo toDomain() {
		return new GsonMapper<>(this, AirportInfo.class).convert();
	}

}
