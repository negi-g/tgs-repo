package com.tgs.services.fms.sources.amadeusndc;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.StringJoiner;
import javax.xml.ws.BindingProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.iata.iata._2015._00._2018_1.ordercancelrq.AgencyTypeCodeContentType;
import org.iata.iata._2015._00._2018_1.ordercancelrq.CarrierType;
import org.iata.iata._2015._00._2018_1.ordercancelrq.CountryType;
import org.iata.iata._2015._00._2018_1.ordercancelrq.IATAPayloadStandardAttributesType;
import org.iata.iata._2015._00._2018_1.ordercancelrq.OrderCancelRQ;
import org.iata.iata._2015._00._2018_1.ordercancelrq.OrderType;
import org.iata.iata._2015._00._2018_1.ordercancelrq.PartyType;
import org.iata.iata._2015._00._2018_1.ordercancelrq.PointofSaleType2;
import org.iata.iata._2015._00._2018_1.ordercancelrq.RecipientType;
import org.iata.iata._2015._00._2018_1.ordercancelrq.RequestType;
import org.iata.iata._2015._00._2018_1.ordercancelrq.SenderType;
import org.iata.iata._2015._00._2018_1.ordercancelrq.TravelAgencyType;
import org.iata.iata._2015._00._2018_1.ordercancelrs.OrderCancelRS;
import com.amadeus.wsdl.altea_ndc_18_1_v1.AlteaNDC181PT;
import com.amadeus.wsdl.altea_ndc_18_1_v1_v4.AlteaNDC181Service;
import com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser;
import com.tgs.utils.exception.air.CancellationException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class AmadeusNdcCancellationManager extends AmadeusNdcServiceManager {

	public boolean orderCancel() {
		log.debug("Starting ORDER CANCEL NDC... for GDS-PNR {}", getSupplierPNR());
		AMASecurityHostedUser securityHostedUser = getSecurityHostedUser();
		AlteaNDC181PT servicePort = service.getAlteaNDC181Port();
		if (Objects.isNull(servicePort)) {
			service = new AlteaNDC181Service();
			servicePort = service.getAlteaNDC181Port();
		}
		BindingProvider bp = (BindingProvider) servicePort;
		addJAXHandlers(bp, bookingId, AmadeusNdcHeaderHandler._ORDER_CANCEL_);

		OrderCancelRS orderCancelRs = servicePort.ndcOrderCancel(createOrderCancelRq(), null, null, securityHostedUser);

		if (CollectionUtils.isNotEmpty(orderCancelRs.getError())) {
			StringJoiner errorMessage = new StringJoiner("");
			orderCancelRs.getError().forEach(error -> {
				errorMessage.add(error.getCode() + ":" + error.getDescText());
			});
			log.error("Could not cancel orderId{} ,bookingId{}", getSupplierPNR(), bookingId);
			throw new CancellationException(errorMessage.toString());
		}

		return isOrderCancelled(orderCancelRs);
	}

	private boolean isOrderCancelled(OrderCancelRS orderCancelRs) {
		String cancelledOrderId = orderCancelRs.getResponse().getOrderRefID();
		if (StringUtils.isNotBlank(cancelledOrderId)) {
			return StringUtils.equals(getSupplierPNR(), cancelledOrderId);
		} else if (CollectionUtils.isNotEmpty(orderCancelRs.getError())) {
			for (org.iata.iata._2015._00._2018_1.ordercancelrs.ErrorType errorType : orderCancelRs.getError()) {
				if (StringUtils.equals(errorType.getCode(), AmadeusNDCConstants.PNR_CANCELLED_CODE)
						&& StringUtils.equals(errorType.getDescText(), AmadeusNDCConstants.PNR_CANCELLED_MESSAGE)) {
					return true;
				}
			}
		}
		log.debug("OrderCancellation failed for GDS-PNR {}", getSupplierPNR());
		return false;
	}

	private OrderCancelRQ createOrderCancelRq() {
		OrderCancelRQ orderCancelRQ = new OrderCancelRQ();
		RequestType requestType = new RequestType();

		popluateRequestType(requestType);
		orderCancelRQ.setPayloadAttributes(getPayloadAttributes());
		orderCancelRQ.setPointOfSale(getPointOfSale());
		setPartyType(orderCancelRQ);

		orderCancelRQ.setRequest(requestType);
		return orderCancelRQ;
	}

	private PointofSaleType2 getPointOfSale() {
		PointofSaleType2 pointOfSale = new PointofSaleType2();
		CountryType countyType = new CountryType();
		countyType.setCountryCode(AmadeusNDCConstants.COUNTRY_CODE);
		pointOfSale.setCountry(countyType);
		return pointOfSale;
	}

	private IATAPayloadStandardAttributesType getPayloadAttributes() {
		IATAPayloadStandardAttributesType payloadAttributes = new IATAPayloadStandardAttributesType();
		payloadAttributes.setVersion(AmadeusNDCConstants.SERVICE_VERSION);
		return payloadAttributes;
	}

	private void popluateRequestType(RequestType requestType) {
		OrderType orderType = new OrderType();
		orderType.setOrderID(getSupplierPNR());
		orderType.setOwnerCode(AmadeusNDCConstants.AIRLINE_DESIG_CODE);
		requestType.setOrder(orderType);
	}

	private void setPartyType(OrderCancelRQ orderCancelRQ) {
		PartyType partyType = new PartyType();
		SenderType senderType = new SenderType();
		TravelAgencyType travelAgencyType = new TravelAgencyType();
		travelAgencyType.setAgencyID(supplierConf.getSupplierCredential().getIataNumber());
		travelAgencyType.setIATANumber(new BigDecimal(supplierConf.getSupplierCredential().getIataNumber()));
		travelAgencyType.setName(supplierConf.getSupplierCredential().getAgentUserName());
		travelAgencyType.setTypeCode(AgencyTypeCodeContentType.ONLINE_TRAVEL_AGENCY);
		senderType.setTravelAgency(travelAgencyType);

		RecipientType recipientType = new RecipientType();
		CarrierType carrierType = new CarrierType();
		carrierType.setAirlineDesigCode(supplierConf.getSupplierCredential().getProviderCode());
		recipientType.setORA(carrierType);
		partyType.setRecipient(recipientType);
		partyType.setSender(senderType);
		orderCancelRQ.setParty(partyType);

	}

}
