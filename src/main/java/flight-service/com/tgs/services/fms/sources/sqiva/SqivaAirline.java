package com.tgs.services.fms.sources.sqiva;

import com.tgs.services.base.enums.FareType;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.AirSourceType;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
public enum SqivaAirline {


	FLYBIG(AirSourceType.FLYBIG.getSourceId(), "S9") {

		@Override
		public void setFareType(PriceInfo priceInfo, boolean isSpecialReturn, String faretype) {
			FareType fareType = FareType.PUBLISHED;
			if (isSpecialReturn) {
				fareType = FareType.SPECIAL_RETURN;
			} else if (StringUtils.equalsIgnoreCase(faretype, "biglite")) {
				fareType = FareType.BIGLITE;
			} else if (StringUtils.equalsIgnoreCase(faretype, "bigeasy")) {
				fareType = FareType.BIGEASY;
			}
			priceInfo.setFareIdentifier(fareType);
		}

		@Override
		public List<String> getAllowedFareTypes() {
			return Arrays.asList("bigeasy", "biglite");
		}

		@Override
		public void setCombinedTripsFareType(List<TripInfo> combinedTrips) {
			combinedTrips.forEach(tripInfo -> {
				tripInfo.getSegmentInfos().forEach(segmentInfo -> {
					segmentInfo.getPriceInfoList().forEach(priceInfo -> {
						priceInfo.setFareIdentifier(FareType.SPECIAL_RETURN);
					});
				});
			});
		}
	},
	ALLIANCEAIR(AirSourceType.ALLIANCEAIR.getSourceId(), "9I") {

		@Override
		public void setFareType(PriceInfo priceInfo, boolean isSpecialReturn, String faretype) {
			FareType fareType = FareType.PUBLISHED;
			if (StringUtils.equalsIgnoreCase(faretype, "VAL")) {
				fareType = FareType.VAL;
			} else if (StringUtils.equalsIgnoreCase(faretype, "FLX")) {
				fareType = FareType.FLX;
			} else if (StringUtils.equalsIgnoreCase(faretype, "SUPSAV")) {
				fareType = FareType.SUPSAV;
			}
			priceInfo.setFareIdentifier(fareType);
		}

		@Override
		public List<String> getAllowedFareTypes() {
			return Arrays.asList("SUPSAV", "FLX", "VAL");
		}

		@Override
		public void setCombinedTripsFareType(List<TripInfo> combinedTrips) {}
	};


	String code;
	Integer sourceId;

	SqivaAirline(Integer sourceId, String code) {
		this.sourceId = sourceId;
		this.code = code;
	}


	public static SqivaAirline getSqivaAirline(Integer sourceID) {
		for (SqivaAirline sqiva : SqivaAirline.values()) {
			if (sqiva.getSourceId() == sourceID) {
				return sqiva;
			}
		}
		return null;
	}

	public List<String> getAllowedFareTypes() {
		return new ArrayList<>();
	}

	public void setFareType(PriceInfo priceInfo, boolean isSpecialReturn, String faretype) {
		if (isSpecialReturn) {
			priceInfo.setFareIdentifier(FareType.SPECIAL_RETURN);
		} else {
			priceInfo.setFareIdentifier(FareType.PUBLISHED);
		}
	}

	public void setCombinedTripsFareType(List<TripInfo> combinedTrips) {
		combinedTrips.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					priceInfo.setFareIdentifier(FareType.SPECIAL_RETURN);
				});
			});
		});
	}
}
