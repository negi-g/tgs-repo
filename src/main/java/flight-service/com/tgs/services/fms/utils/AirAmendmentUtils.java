package com.tgs.services.fms.utils;

import java.util.Map;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirClientFeeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AirAmendmentUtils {

	public static void updateClientFee(SegmentInfo segmentInfo, User userFromCache, AmendmentType amendmentType,
			FareComponent fc) {
		try {
			AirType airType = segmentInfo.getMiscInfo().getAirType();
			FlightBasicFact flightBasicFact = FlightBasicFact.createFact().generateFact(segmentInfo);
			flightBasicFact.setAirType(airType);
			BaseUtils.createFactOnUser(flightBasicFact, userFromCache);
			AirClientFeeOutput clientFeeOutput =
					AirConfiguratorHelper.getAirConfigRule(flightBasicFact, AirConfiguratorRuleType.CLIENTFEE);
			if (clientFeeOutput != null && MapUtils.isNotEmpty(clientFeeOutput.getAmendmentFeeMap())
					&& MapUtils.getObject(clientFeeOutput.getAmendmentFeeMap(), amendmentType.name()) != null) {
				Map<AirType, Double> clientFeeMap =
						clientFeeOutput.getAmendmentFeeMap().get(amendmentType.name()).getAmendmentFee();
				Double clientFee =
						clientFeeMap.getOrDefault(airType, clientFeeMap.getOrDefault(AirType.ALL, new Double(0)));
				if (clientFee >= 0.0) {
					segmentInfo.getTravellerInfo().forEach(travellerInfo -> {
						BaseUtils.updateFareComponent(travellerInfo.getFareDetail().getFareComponents(), fc, clientFee,
								userFromCache);
					});
				}
			}
		} catch (Exception e) {
			log.error("Error occured in setting client fees {} ", amendmentType, e);
		}
	}
}
