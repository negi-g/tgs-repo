package com.tgs.services.fms.restcontroller;

import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.fms.dbmodel.DbAirportInfo;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.esstackInitializer.ESStackAirportInfoInitializer;
import com.tgs.services.fms.jparepository.AirportInfoService;
import com.tgs.services.fms.restmodel.AirportInfoResponse;
import com.tgs.services.fms.servicehandler.AirportInfoHandler;
import com.tgs.services.ums.datamodel.AreaRole;

@RestController
@RequestMapping("/fms/v1/airports")
public class AirPortController {

	@Autowired
	AirportInfoHandler airportHandler;

	@Autowired
	AirportInfoService airportService;

	@Autowired
	AirportHelper airportHelper;

	@Autowired
	ESStackAirportInfoInitializer elasticHelper;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected AirportInfoResponse saveAirport(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirportInfo airportInfo) throws Exception {

		airportHandler.initData(airportInfo, new AirportInfoResponse());
		return airportHandler.getResponse();

	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected AirportInfoResponse listAirports(HttpServletRequest request, HttpServletResponse response,
			@RequestBody com.tgs.filters.AirportInfoFilter airportFilter) throws Exception {
		List<AirportInfo> airportList = airportService.search(airportFilter);
		AirportInfoResponse airportresponseInfo = new AirportInfoResponse();
		if (CollectionUtils.isNotEmpty(airportList)) {
			airportresponseInfo.setAirportInfos(airportList);
			return airportresponseInfo;
		}
		return airportresponseInfo;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected BaseResponse deleteAirport(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return airportHandler.deleteAirport(id);
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected AirportInfoResponse updateAirportStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return airportHandler.updateStatus(id, status);
	}
}
