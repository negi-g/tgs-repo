package com.tgs.services.messagingService.runtime;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.SmsVendorConfiguration;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.messagingService.datamodel.sms.SmsMetaData;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class QuickSmartSmsService implements SmsService {

	@Autowired
	GeneralServiceCommunicator gnServiceComm;

	@Override
	public void sendSms(SmsMetaData metaData, SmsVendorConfiguration conf) {
		// http://quicksmart.in/api/pushsms?user=12345&authkey=sAts553vs88gsf32dsb&sender=WBSMS&mobile=9800000000&text=Test
		// sms from API&rpt=1
		List<String> recipientNumbers = metaData.getRecipientNumbers();
		recipientNumbers.forEach(number -> {
			int retryCount = 3;
			String responseString = "";
			try {
				while (retryCount > 0) {
					HttpUtils httpUtils = HttpUtils.builder().urlString(conf.getUrl()).timeout(20 * 1000)
							.queryParams(getQueryParams(conf, number, metaData.getBody())).build();

					try {
						//{"RESPONSE":{"CODE":"100","INFO":"SUBMITTED","UID":"7d684872-45948593-27a3c18e-1341798"},"STATUS":"OK"}
						responseString = (String) httpUtils.getResponse(null).orElse("");
						if (StringUtils.isNotBlank(responseString) && responseString.contains("UID")) {
							log.info("Msg successfully sent to recipient {}, request sent {}, response received {}",
									number, metaData.getBody(), responseString);
							break;
						} else {
							log.error("Unable to send msg {} to recipient {} due to {}", metaData.getBody(), number,
									responseString);
							break;
						}
					} catch (IOException e) {
						log.error(
								"Unable to send msg due to IOException, request sent {}, response received {}, recipient {}",
								metaData.getBody(), responseString, number, e);
						retryCount--;
					}
				}
			} finally {
				if (retryCount == 1) {
					String noteMessage = StringUtils.join("Unable to send sms to : ", number, " with content : ",
							metaData.getBody(), "Response received is :", responseString);
					Note note = Note.builder().noteType(NoteType.SMSFAILURE).noteMessage(noteMessage).build();
					if (SystemContextHolder.getContextData().getUser() != null) {
						note.setUserId(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
					}
					gnServiceComm.addNote(note);
				}
			}
		});
	}

	private Map<String, String> getQueryParams(SmsVendorConfiguration conf, String recipientMobile, String smsBody) {

		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("user", conf.getUserName());
		queryParams.put("password", conf.getPassword());
		queryParams.put("sender", conf.getSender());
		queryParams.put("authkey", conf.getAuthkey());
		queryParams.put("mobile", recipientMobile);
		queryParams.put("text", smsBody);
		queryParams.put("rpt", conf.getAdditionalParameters().get(0));
		return queryParams;
	}
}
