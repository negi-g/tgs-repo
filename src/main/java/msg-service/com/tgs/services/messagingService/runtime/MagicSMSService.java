package com.tgs.services.messagingService.runtime;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.SmsVendorConfiguration;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.messagingService.datamodel.sms.SmsMetaData;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MagicSMSService implements SmsService {

	private static String SMS_TEXT = "sms_text";

	private static String MOBILE_NUMBER = "mobile_number";

	private static String SENDER_ID = "sender_id";

	@Autowired
	GeneralServiceCommunicator gnServiceComm;

	@Override
	public void sendSms(SmsMetaData metaData, SmsVendorConfiguration config) {
		List<String> recepientNumbers = metaData.getRecipientNumbers();
		recepientNumbers.forEach(number -> {
			int attempt = 0;
			boolean isSuccess = false;
			String responseString = "";
			try {
				while (attempt < 3 && !isSuccess) {
					HttpUtils httpUtils = HttpUtils.builder().urlString(config.getUrl()).timeout(20 * 1000)
							.headerParams(buildHeaderParam(config)).postData(buildPostData(config, metaData, number))
							.build();
					try {
						responseString = (String) httpUtils.getResponse(null).orElse("");
						if (StringUtils.isNotEmpty(responseString)) {
							Map<String, String> response = ObjectUtils.firstNonNull(
									GsonUtils.getGson().fromJson(responseString, HashMap.class), new HashMap<>());
							if ("submitted".equals(response.get("status"))
									&& StringUtils.isNotBlank(response.get("id"))) {
								isSuccess = true;
								log.info("Msg successfully sent to recipient {}, request sent {}, response received {}",
										number, metaData.getBody(), responseString);
							} else {
								log.error("Unable to send msg {} to recipient {} due to {}", metaData.getBody(), number,
										responseString);
							}

						} else {
							log.error("Unable to send msg {} to recipient {} due to {}", metaData.getBody(), number,
									responseString);
						}

					} catch (IOException e) {
						log.error("[SMSMagic] Receipient Number {} , msg content {} and response received is {}",
								number, metaData.getBody(), responseString);
						isSuccess = false;
					}
					attempt++;
				}
			} finally {
				if (!isSuccess) {
					String noteMessage = StringUtils.join("[SMSMagic] Unable to send sms to : ", number,
							" with content : ", metaData.getBody(), "Response received is :", responseString);
					Note note = Note.builder().noteType(NoteType.SMSFAILURE).noteMessage(noteMessage).build();
					if (SystemContextHolder.getContextData().getUser() != null) {
						note.setUserId(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
					}
					gnServiceComm.addNote(note);
				}
			}
		});
	}

	private String buildPostData(SmsVendorConfiguration config, SmsMetaData metaData, String recepientNumber) {
		Map<String, String> postData = new HashMap<>();
		postData.put(SMS_TEXT, metaData.getBody());
		postData.put(MOBILE_NUMBER, recepientNumber);
		postData.put(SENDER_ID, config.getSender());
		return GsonUtils.getGson().toJson(postData);
	}

	private Map<String, String> buildHeaderParam(SmsVendorConfiguration conf) {

		Map<String, String> headerParam = new HashMap<String, String>();
		headerParam.put("apikey", conf.getAuthkey());
		headerParam.put("cache-control", "no-cache");
		headerParam.put("content-type", "application/json");
		return headerParam;
	}

}
