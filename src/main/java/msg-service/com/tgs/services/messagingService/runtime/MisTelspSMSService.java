package com.tgs.services.messagingService.runtime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.SmsVendorConfiguration;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.messagingService.datamodel.sms.SmsMetaData;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MisTelspSMSService implements SmsService {

	@Autowired
	GeneralServiceCommunicator gnServiceComm;

	@Override
	public void sendSms(SmsMetaData metaData, SmsVendorConfiguration conf) {

		// https://api.telsp.in/pushapi/sendbulkmsg?username=AtlasNU&dest=919711048134&apikey=dhYoPSRYob55DgHq9wzH7X329Jf6MjmI&signature=ATLBKG&msgtype=PM&msgtxt=text
		List<String> recepientNumbers = metaData.getRecipientNumbers();
		recepientNumbers.forEach(number -> {
			int retryCount = 3;
			List<Map<String, String>> responseString = new ArrayList<>();
			try {
				while (retryCount > 0) {
					HttpUtils httpUtils = HttpUtils.builder().urlString(conf.getUrl())
							.queryParams(getQueryParams(conf, number, metaData.getBody())).build();
					try {
						
						responseString = (List<Map<String, String>>) httpUtils.getResponse(List.class)
								.orElse(new ArrayList<>());
						log.debug("HttpUtils url string {}", httpUtils.getUrlString());
						if (CollectionUtils.isNotEmpty(responseString)
								&& responseString.get(0).get("code").equals("6001")) {
							log.info("[MisTELSP] Receipient Number {} , msg content {} and response received is {}",
									number, metaData.getBody(), responseString);
						} else {
							log.error("[MisTELSP] Receipient Number {} , msg content {} and response received is {}",
									number, metaData.getBody(), responseString);
						}
						retryCount = 0;
					} catch (IOException e) {
						log.error("[MisTELSP] Receipient Number {} , msg content {} and response received is {}",
								number, metaData.getBody(), responseString);
						retryCount--;
					}
				}
			} finally {
				if (retryCount == 1) {
					String noteMessage = StringUtils.join("[MisTELSP] Unable to send sms to : ", number,
							" with content : ", metaData.getBody(), "Response received is :", responseString);
					Note note = Note.builder().noteType(NoteType.SMSFAILURE).noteMessage(noteMessage).build();
					if (SystemContextHolder.getContextData().getUser() != null) {
						note.setUserId(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
					}
					gnServiceComm.addNote(note);
				}
			}
		});

	}

	public Map<String, String> getQueryParams(SmsVendorConfiguration conf, String recepientMobile, String smsBody) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("username", conf.getUserName());
		queryParams.put("apikey", conf.getAuthkey());
		queryParams.put("signature", conf.getSender());
		queryParams.put("msgtype", conf.getAdditionalParameters().get(0));
		queryParams.put("dest", recepientMobile);
		queryParams.put("msgtxt", smsBody);
		return queryParams;
	}

}
