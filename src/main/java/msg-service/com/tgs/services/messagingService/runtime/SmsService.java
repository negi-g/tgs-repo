package com.tgs.services.messagingService.runtime;

import com.tgs.services.base.configurationmodel.SmsVendorConfiguration;
import com.tgs.services.messagingService.datamodel.sms.SmsMetaData;

public interface SmsService {
	
	public void sendSms(SmsMetaData metaData, SmsVendorConfiguration config);
}
