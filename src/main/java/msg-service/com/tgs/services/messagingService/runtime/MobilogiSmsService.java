package com.tgs.services.messagingService.runtime;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.SmsVendorConfiguration;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.messagingService.datamodel.sms.SmsMetaData;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MobilogiSmsService implements SmsService {

	@Autowired
	GeneralServiceCommunicator gnServiceComm;

	@Override
	public void sendSms(SmsMetaData metaData, SmsVendorConfiguration conf) {

		// http://vas.mobilogi.com/api.php?username=demo&password=pass123&route=1&sender=Moblgi&mobile[]=9574911084&message[]=TEST
		List<String> recepientNumbers = metaData.getRecipientNumbers();
		recepientNumbers.forEach(number -> {
			int retryCount = 3;
			String responseString = "";
			try {
				while (retryCount > 0) {
					HttpUtils httpUtils = HttpUtils.builder().urlString(conf.getUrl())
							.queryParams(getQueryParams(conf, number, metaData.getBody())).build();
					try {
						// For example, msgid:2345167
						responseString = (String) httpUtils.getResponse(null).orElse("");
						if (StringUtils.isNotEmpty(responseString)) {
							String[] response = responseString.split(":");
							if (!"msgid".equals(response[0]) || StringUtils.isEmpty(response[1])) {
								log.info("[MobiLogi] Receipient Number {} , msg content {} and response received is {}",
										number, metaData.getBody(), responseString);
								break;
							}
						} else {
							log.error("[MobiLogi] Receipient Number {} , msg content {} and response received is {}",
									number, metaData.getBody(), responseString);
							break;
						}
						retryCount = 0;
					} catch (IOException e) {
						log.error("[MobiLogi] Receipient Number {} , msg content {} and response received is {}",
								number, metaData.getBody(), responseString);
						retryCount--;
					}
				}
			} finally {
				if (retryCount == 1) {
					String noteMessage = StringUtils.join("[MobiLogi] Unable to send sms to : ", number,
							" with content : ", metaData.getBody(), "Response received is :", responseString);
					Note note = Note.builder().noteType(NoteType.SMSFAILURE).noteMessage(noteMessage).build();
					if (SystemContextHolder.getContextData().getUser() != null) {
						note.setUserId(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
					}
					gnServiceComm.addNote(note);
				}
			}
		});

	}

	public Map<String, String> getQueryParams(SmsVendorConfiguration conf, String recepientMobile, String smsBody) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("username", conf.getUserName());
		queryParams.put("password", conf.getPassword());
		queryParams.put("route", conf.getRoute());
		queryParams.put("sender", conf.getSender());
		queryParams.put("mobile[]", recepientMobile);
		queryParams.put("message[]", smsBody);
		return queryParams;
	}

}
