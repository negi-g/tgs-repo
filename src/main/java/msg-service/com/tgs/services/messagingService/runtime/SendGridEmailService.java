package com.tgs.services.messagingService.runtime;

import java.io.IOException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.sendgrid.Attachments;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.tgs.services.base.configurationmodel.EmailVendorConfiguration;
import com.tgs.services.messagingService.datamodel.EmailMetaData;
import com.tgs.utils.file.TgsFileUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SendGridEmailService implements EmailService {

	@Override
	public void sendEmail(EmailMetaData metaData, EmailVendorConfiguration vendorConfig) {
		String[] toMails = metaData.getToEmail().split(",");
		Email from = new Email(metaData.getFromEmail());
		Email reply = new Email(metaData.getFromEmail());
		Content content = new Content(metaData.getType(), metaData.getBody());
		/*
		 * Don't use this constructor.
		 * SendGrid internally creates one Personalization object with toEmail if we use Mail constructor. And we can
		 * not create personalization object with only cc/bcc (without to). So ,if we are already adding toEmail in
		 * personalization a well as Mail(), it results in sendgrip sending 2 duplicate emails.
		 * To avoid this, we will add toEmail only in Personalization().
		 */
		//Mail mail = new Mail(from, metaData.getSubject(), to, content);
		Mail mail = new Mail();
		mail.setFrom(from);
		mail.setSubject(metaData.getSubject());
		mail.addContent(content);
		mail.setReplyTo(reply);

		Personalization personalization = new Personalization();
		for (int i = 0; i < toMails.length; i++) {
			personalization.addTo(new Email(toMails[i].trim()));
		}

		if (StringUtils.isNotBlank(metaData.getCcEmail())) {
			String[] ccMails = metaData.getCcEmail().split(",");
			for (int i = 0; i < ccMails.length; i++) {
				personalization.addCc(new Email(ccMails[i].trim()));
			}
		}

		if (StringUtils.isNotBlank(metaData.getBccEmail())) {
			String[] bccMails = metaData.getBccEmail().split(",");
			for (int i = 0; i < bccMails.length; i++) {
				personalization.addBcc(new Email(bccMails[i].trim()));
			}
		}
		mail.addPersonalization(personalization);

		if (metaData.getAttachmentData() != null && metaData.getAttachmentData().getFileData() != null) {
			Attachments attachments = new Attachments();
			Base64 x = new Base64();
			String encodedString = x.encodeAsString(metaData.getAttachmentData().getFileData());
			attachments.setContent(encodedString);
			attachments.setType(TgsFileUtils.resolveContentType(metaData.getAttachmentData().getFileName()));
			attachments.setFilename(metaData.getAttachmentData().getFileName());
			attachments.setDisposition("attachment");
			mail.addAttachments(attachments);
		}
		
		Request request = new Request();
		try {
			SendGrid sg = new SendGrid(vendorConfig.getKeyId());
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			log.info("Response status is {} , to {} , ccEMail {} , bccEmail {}, subject {}", response.getStatusCode(),
					metaData.getToEmail(), metaData.getCcEmail(), metaData.getBccEmail(), metaData.getSubject());
		} catch (IOException e) {
			log.error("Unable to send email to {} , ccEMail {} , bccEmail {}, subject {}", metaData.getToEmail(),
					metaData.getCcEmail(), metaData.getBccEmail(), metaData.getSubject(), e);
		}

	}
	
}
