package com.tgs.services.messagingService.runtime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.SmsVendorConfiguration;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.messagingService.datamodel.sms.SmsMetaData;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SmsCountrySMSService implements SmsService {

	@Autowired
	GeneralServiceCommunicator gnServiceComm;
	
	@Override
	public void sendSms(SmsMetaData metaData, SmsVendorConfiguration conf) {

		// http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=SKYSAFAR&passwd=ARSH@2830&mobilenumber=9999703863&message=HELLO%20TESTING%20SKYSAFAR&sid=SKYSFR&mtype=N&DR=Y
		List<String> recepientNumbers = metaData.getRecipientNumbers();
		recepientNumbers.forEach(number -> {
			int retryCount = 3;
			String responseString = "";
			try {
				while (retryCount > 0) {
					HttpUtils httpUtils = HttpUtils.builder().urlString(conf.getUrl())
							.queryParams(getQueryParams(conf, number, metaData.getBody())).build();
					try {
						responseString = (String) httpUtils.getResponse(null).orElse("");
						if (StringUtils.isNotBlank(responseString) && responseString.contains("sent")) {
							log.info("[SMSCountry] Msg successfully sent to recipient {}, request sent {}, response received {}",
									number, metaData.getBody(), responseString);
						} else {
							log.error("[SMSCountry] Unable to send msg {} to recipient {} due to {}", metaData.getBody(), number,
									responseString);
						}
						retryCount = 0;
					} catch (IOException e) {
						log.error("[SMSCountry] Receipient Number {} , msg content {} and response received is {}",
								number, metaData.getBody(), responseString);
						retryCount--;
					}
				}
			} finally {
				if (retryCount == 1) {
					String noteMessage = StringUtils.join("[SMSCountry] Unable to send sms to : ", number,
							" with content : ", metaData.getBody(), "Response received is :", responseString);
					Note note = Note.builder().noteType(NoteType.SMSFAILURE).noteMessage(noteMessage).build();
					if (SystemContextHolder.getContextData().getUser() != null) {
						note.setUserId(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
					}
					gnServiceComm.addNote(note);
				}
			}
		});

	}
	
	public Map<String, String> getQueryParams(SmsVendorConfiguration conf, String recepientMobile, String smsBody) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("User", conf.getUserName());
		queryParams.put("passwd", conf.getPassword());
		queryParams.put("sid", conf.getSender());
		queryParams.put("mtype", conf.getAdditionalParameters().get(0));
		queryParams.put("DR", conf.getAdditionalParameters().get(1));
		queryParams.put("mobilenumber", recepientMobile);
		queryParams.put("message", smsBody);
		return queryParams;
	}
	
}
