package com.tgs.services.messagingService.runtime;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.common.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.WhatsAppConfiguration;
import com.tgs.services.base.datamodel.AttachmentMetadata;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.google.common.collect.Lists;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.messagingService.datamodel.whatsApp.WhatsAppMetaData;
import com.tgs.services.base.datamodel.AttachmentMetadata;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class WhatsAppMsgService implements WhatsAppService {

	@Autowired
	GeneralServiceCommunicator gnServiceComm;

	@Override
	public void sendWhatsAppMessage(WhatsAppMetaData metaData, WhatsAppConfiguration conf) {
		List<String> recepientNumbers = metaData.getRecipientNumbers();
		recepientNumbers.forEach(number -> {
			int retryCount = 3;
			String responseString = "";
			try {
				while (retryCount > 0) {
					Map<String, String> headerParams = new HashMap<>();
					headerParams.put("Content-Type", "application/json");
					headerParams.put("Authorization", conf.getApiKey());
					try {
						HttpUtils httpUtils = HttpUtils.builder().urlString(conf.getUrl().concat("consent/"))
								.headerParams(headerParams).postData(getOptInDetails(number)).build();

						Map<String, Object> responseObj = (Map<String, Object>) httpUtils.getResponse(Map.class)
								.orElse(new HashMap<>());
						if (isNumberOptIn(responseObj)) {
							HttpUtils httpUtils2 = HttpUtils.builder().urlString(conf.getUrl().concat("message/"))
									.headerParams(headerParams)
									.postData(getPostData(conf, number, metaData.getTemplateName(),
											metaData.getAttributes(), metaData.getAttachmentData()))
									.build();
							Map<String, Object> response = (Map<String, Object>) httpUtils2.getResponse(Map.class)
									.orElse(new HashMap<>());
							if ("success".equals(response.get("status"))) {
								log.info("[WhatsApp] successfully send message on number {}", number);
							} else {
								log.info("[WhatsApp] unable to send message on number {} due to {} ", number,
										response.get("error").toString());
								throw new CustomGeneralException(SystemError.WHATS_APP_MESSAGE_FAILED);
							}
						}

						retryCount = 0;
					} catch (IOException e) {
						retryCount--;
					}
				}
			} finally {
				if (retryCount == 1) {
					String noteMessage = StringUtils.join("[WhatsApp] Unable to whatsapp message to : ", number,
							" with template name : ", metaData.getTemplateName(), " and template attributes : ",
							metaData.getAttributes().toString());
					Note note = Note.builder().noteType(NoteType.WHATSAPP).noteMessage(noteMessage).build();
					if (SystemContextHolder.getContextData().getUser() != null) {
						note.setUserId(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
					}
					gnServiceComm.addNote(note);
				}
			}
		});
	}

	private boolean isNumberOptIn(Map<String, Object> responseObj) {
		if ("success".equals(responseObj.get("status"))) {
			String responseData = GsonUtils.getGson().toJson(responseObj.get("data"));
			Map<String, Object> responseMap = GsonUtils.getGson().fromJson(responseData,
					new TypeToken<Map<String, Object>>() {
					}.getType());
			return (Boolean) responseMap.get("wa_optin");
		}
		return false;
	}

	private String getOptInDetails(String recepientMobile) {
		Map<String, Object> optInData = new HashMap<>();
		optInData.put("phone_number", recepientMobile);
		return GsonUtils.getGson().toJson(optInData);
	}

	private String getPostData(WhatsAppConfiguration conf, String recepientMobile, String templateName,
			List<String> templateAttributes, AttachmentMetadata attachmentData) {
		Map<String, Object> postData = new HashMap<>();
		Map<String, Object> mediaData = new HashMap<>();
		Map<String, Object> messageParams = new HashMap<>();
		messageParams.put("recipient_whatsapp", recepientMobile);
		messageParams.put("message_type", "media_template");
		messageParams.put("recipient_type", "individual");
		messageParams.put("source", conf.getSourceId());

		mediaData.put("type", "document");
		mediaData.put("url", attachmentData.getUrl());
		mediaData.put("filename", attachmentData.getFileName());
		Map<String, Object> msgContent = new HashMap<>();
		msgContent.put("name", templateName);
		msgContent.put("attributes", templateAttributes);

		Map<String, Object> langContent = new HashMap<>();
		langContent.put("locale", conf.getTemplateLocale());
		langContent.put("policy", "deterministic");
		msgContent.put("language", langContent);

		messageParams.put("type_template", Lists.newArrayList(msgContent));
		messageParams.put("type_media_template", mediaData);

		postData.put("message", Lists.newArrayList(messageParams));
		return GsonUtils.getGson().toJson(postData);
	}

}