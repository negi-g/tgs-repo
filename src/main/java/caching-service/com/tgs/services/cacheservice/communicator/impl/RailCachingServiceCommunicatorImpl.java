package com.tgs.services.cacheservice.communicator.impl;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.RailCachingServiceCommunicator;
import com.tgs.services.cacheservice.aerospike.AerospikeNamespace;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.runtime.impl.RailAerospikeClient;

@Service
public class RailCachingServiceCommunicatorImpl implements RailCachingServiceCommunicator {


	@Autowired
	RailAerospikeClient aeroClient;

	@Override
	public <V> boolean store(String key, String bin, V value, CacheMetaInfo metaInfo) {
		metaInfo.setNamespace(AerospikeNamespace.RAIL.name());
		Map<String, V> binMap = new HashMap<>();
		binMap.put(bin, value);
		aeroClient.store(key, binMap, metaInfo);
		return true;
	}

	@Override
	public <V> V fetchValue(String key, Class<V> classofV, String set, String binValue) {
		return aeroClient.get(key, classofV, set, binValue);
	}

}
