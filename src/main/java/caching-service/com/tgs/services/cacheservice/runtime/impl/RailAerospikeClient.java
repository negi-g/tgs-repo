package com.tgs.services.cacheservice.runtime.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.ScanCallback;
import com.aerospike.client.policy.Policy;
import com.aerospike.client.policy.WritePolicy;
import com.tgs.services.cacheservice.aerospike.AerospikeConfiguration;
import com.tgs.services.cacheservice.aerospike.AerospikeHelper;
import com.tgs.services.cacheservice.aerospike.AerospikeNamespace;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RailAerospikeClient implements ScanCallback {

	@Autowired
	AerospikeClient client;

	@Autowired
	AerospikeConfiguration conf;

	@Value("${setPrefix}")
	private String setPrefix;

	public <V> void store(String key, Map<String, V> binMap, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		try {
			String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
			Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
			client.put(AerospikeHelper.getWritePolicy(metaInfo.getExpiration()), aerospikekey,
					AerospikeHelper.generateBinList(binMap, BooleanUtils.isNotFalse(metaInfo.getCompress()),
							BooleanUtils.isFalse(metaInfo.getPlainData())).toArray(new Bin[0]));
		} catch (Exception e) {
			log.error("Failed to store key {} , bin {} in aerospike", key, binMap.keySet(), e);
			throw e;
		}
	}

	public <V> V get(String key, Class<V> classofV, String set, String bin) {
		return get(key, classofV, set, bin, "compression").get(bin);
	}

	public <V> Map<String, V> get(String key, Class<V> classofV, String set, String... bin) {
		verifySetInRequest(set);
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		Policy policy = new Policy();
		String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.RAIL.name().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(set), key);
		Record record = client.get(policy, aerospikekey, bin);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().compress(true).set(setPrefix.concat("_").concat(set)).build();
		return AerospikeHelper.convertingBinMapToUserDefinedMap(record, classofV, metaInfo);

	}

	public <V> List<V> get(String key, Class<V> classofV, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		// ScanKeys();
		Policy policy = new Policy();
		String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
		Record result = client.get(policy, aerospikekey);
		metaInfo.setCompress(true);
		List<V> binList =
				new ArrayList<V>(AerospikeHelper.convertingBinMapToUserDefinedMap(result, classofV, metaInfo).values());
		return binList;
	}

	public boolean delete(CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		WritePolicy wpolicy = new WritePolicy();
		String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey());
		return client.delete(wpolicy, aerospikekey);
	}

	public void truncate(CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
		client.truncate(null, mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), null);
	}

	@Override
	public void scanCallback(Key key, Record record) throws AerospikeException {

	}

	public void verifySetInRequest(String set) {
		try {
			if (StringUtils.isEmpty(set)) {
				throw new Exception();
			}
		} catch (Exception e) {
			log.info("Set is empty {}", e);
		}
	}


}
