package com.tgs.services.cacheservice.runtime.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.aerospike.client.AerospikeClient;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.Language;
import com.aerospike.client.Record;
import com.aerospike.client.policy.Policy;
import com.aerospike.client.policy.QueryPolicy;
import com.aerospike.client.policy.WritePolicy;
import com.aerospike.client.query.Filter;
import com.aerospike.client.query.RecordSet;
import com.aerospike.client.query.Statement;
import com.aerospike.client.task.ExecuteTask;
import com.aerospike.client.task.RegisterTask;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.aerospike.AerospikeConfiguration;
import com.tgs.services.cacheservice.aerospike.AerospikeHelper;
import com.tgs.services.cacheservice.aerospike.AerospikeNamespace;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.utils.encryption.KryoUtils;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AerospikeHashMap implements CustomInMemoryHashMap {

	@Autowired
	AerospikeClient client;

	@Autowired
	AerospikeConfiguration conf;

	@Value("${setPrefix}")
	private String setPrefix;

	@Override
	public <V> V put(String key, String field, V value, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
		Map<String, Object> binValues =
				MapUtils.isNotEmpty(metaInfo.getBinValues()) ? metaInfo.getBinValues() : new HashMap<>();
		binValues.put(field, value);
		List<Bin> bins = new ArrayList<>();
		if (metaInfo.getCompress()) {
			bins.add(new Bin("compression", metaInfo.getCompress()));
		}
		for (Entry<String, Object> entrySet : binValues.entrySet()) {
			if (metaInfo.getCompress()) {
				bins.add(new Bin(field, KryoUtils.serialize(value).get()));
			} else if(BooleanUtils.isTrue(metaInfo.getListData()) && entrySet.getValue() instanceof List) {
				bins.add(new Bin(entrySet.getKey(), entrySet.getValue()));
			} else {
				bins.add(new Bin(entrySet.getKey(), GsonUtils.getGson().toJson(entrySet.getValue())));
			}
		}
		client.put(AerospikeHelper.getWritePolicy(metaInfo.getExpiration()), aerospikekey, bins.toArray(new Bin[0]));
		return value;
	}

	public Record getResult(String key, CacheMetaInfo metaInfo) {
		Policy policy = new Policy();
		String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
		Record result = client.get(policy, aerospikekey);
		return result;
	}

	public String getNameSpace(CacheMetaInfo metaInfo) {
		return conf
				.getMappedNameSpace(ObjectUtils.firstNonNull(metaInfo.getNamespace(), CacheNameSpace.STATIC_MAP.getName()));
	}

	@Override
	public <V> Map<String, Map<String, V>> getResultSet(CacheMetaInfo metaInfo, Class<V> classofV, Filter filter) {
		if (metaInfo != null && BooleanUtils.isTrue(metaInfo.getCacheResult())) {
			verifySetInRequest(metaInfo.getSet());
			ContextData contextData = SystemContextHolder.getContextData();
			if (contextData != null) {
				Optional<V> optionValue =
						contextData.getValue(String.join("", Arrays.toString(metaInfo.getKeys()), metaInfo.getKey()));
				if (optionValue != null) {
					return (Map<String, Map<String, V>>) optionValue.orElse(null);
				}
			}
		}

		Map<String, Map<String, V>> resultSet = new HashMap<>();
		try {
			Statement statement = new Statement();
			statement.setNamespace(getNameSpace(metaInfo));
			statement.setSetName(setPrefix.concat("_").concat(metaInfo.getSet()));
			statement.setFilter(filter);
			QueryPolicy policy = new QueryPolicy();
			if (metaInfo.getBins() != null) {
				statement.setBinNames(metaInfo.getBins());
			}
			RecordSet rs = client.query(policy, statement);
			while (rs.next()) {
				Key key = rs.getKey();
				Record record = rs.getRecord();
				Map<String, V> innerMap = AerospikeHelper.convertingBinMapToUserDefinedMap(record, classofV, metaInfo);
				if (resultSet.get(key.userKey.toString()) == null) {
					resultSet.put(key.userKey.toString(), innerMap);
				}
			}
		} catch (Exception e) {
			log.error("Unable to generate Result Set for set {} , key {} and class {} ",
					setPrefix.concat("_").concat(metaInfo.getSet()), Arrays.toString(metaInfo.getKeys()),
					metaInfo.getKey(), classofV.getCanonicalName(), e);
		}
		return resultSet;

	}

	@Override
	public <V> V get(String key, String field, Class<V> classofV, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		ContextData contextData = SystemContextHolder.getContextData();
		if (contextData != null) {
			Optional<V> optionValue = SystemContextHolder.getContextData().getValue(String.join("", key, field));
			if (optionValue != null) {
				return optionValue.orElse(null);
			}
		}
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		V value = null;
		try {
			Record result = getResult(key, metaInfo);

			if (result == null || result.getValue(field) == null) {
				return value;
			} 
			try {
				if (result.getBoolean("compression")) {
					value = KryoUtils.deserialize((byte[]) result.getValue(field), classofV).get();
					return value;
				}
			} catch (Exception e) {
				log.error("Unable to fetch value of key {},field {}", field, key, e);
				return value;
			}
			if (metaInfo.getTypeOfT() != null) {
				value = GsonUtils.getGson().fromJson(result.getString(field), metaInfo.getTypeOfT());
			} else {
				value = GsonUtils.getGson().fromJson(result.getString(field), classofV);
			}
		} finally {
			if (contextData != null) {
				contextData.setValue(String.join("", key, field), value);
			}
		}
		return value;
	}

	@Override
	public void delete(String key, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		WritePolicy wpolicy = new WritePolicy();
		String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
		client.delete(wpolicy, aerospikekey);
	}

	@Override
	public void truncate(String set) {
		verifySetInRequest(set);
		String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
		truncate(mappedNameSpace, setPrefix.concat("_").concat(set));
	}

	public void truncate(String namespace, String set) {
		client.truncate(null, namespace, set, null);
	}

	public void verifySetInRequest(String set) {
		try {
			if (StringUtils.isEmpty(set)) {
				throw new Exception();
			}
		} catch (Exception e) {
			log.info("Set is empty {}", e);
		}
	}


	@Override
	public void modifyTtl(String setName, int ttl) {
		try {
			RegisterTask task = client.register(null, AerospikeHashMap.class.getClassLoader(), "udf/modify_ttl.lua",
					"modify_ttl.lua", Language.LUA);
			task.waitTillComplete(1000, 5000);
			WritePolicy queryPolicy = AerospikeHelper.getWritePolicy(ttl);
			queryPolicy.setTimeouts(30000, 30000);
			queryPolicy.maxRetries = 4;
			Statement stmt = new Statement();
			String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
			stmt.setNamespace(mappedNameSpace);
			com.aerospike.client.Value[] values = new com.aerospike.client.Value[1];
			values[0] = com.aerospike.client.Value.get(ttl);
			stmt.setSetName(setPrefix.concat("_").concat(setName));
			ExecuteTask executeTask =
					client.execute(queryPolicy, stmt, "modify_ttl", "modify_ttl", com.aerospike.client.Value.get(ttl));
			executeTask.waitTillComplete(1000, 5000);
		} catch (Exception e) {
			log.error("Unable to update ttl through udf", e);
			throw e;
		}

	}

}
