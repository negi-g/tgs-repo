package com.tgs.services.cacheservice.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.runtime.impl.FlightAerospikeClient;


@RequestMapping("/cache/v1")
@RestController
public class CacheController {

	@Autowired
	FlightAerospikeClient aeroClient;

	@RequestMapping(value = "/delete/{namespace}/{set}", method = RequestMethod.GET)
	protected BaseResponse deleteCache(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("namespace") String namespace, @PathVariable("set") String set) throws Exception {

		aeroClient.scanAndDelete(CacheMetaInfo.builder().set(set).namespace(namespace).build());
		return new BaseResponse();
	}
}
