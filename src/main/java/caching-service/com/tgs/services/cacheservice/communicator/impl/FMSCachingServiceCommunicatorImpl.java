package com.tgs.services.cacheservice.communicator.impl;

import java.util.*;

import org.apache.commons.collections4.CollectionUtils;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.cacheservice.aerospike.AerospikeNamespace;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cacheservice.runtime.impl.FlightAerospikeClient;
import com.tgs.services.fms.datamodel.AirCacheSearchResult;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FMSCachingServiceCommunicatorImpl implements FMSCachingServiceCommunicator {

	@Autowired
	FlightAerospikeClient aeroClient;

	private static final Integer MAX_FLIGHT_IN_PARTITION = 100;
	private static final Integer MAX_FLIGHT_IN_PARTITION_1 = 1000;

	@Override
	public <V> boolean store(String key, String bin, V value, CacheMetaInfo metaInfo) {
		metaInfo.setNamespace(AerospikeNamespace.FLIGHT.name());
		Map<String, V> binMap = new HashMap<>();
		binMap.put(bin, value);
		aeroClient.store(key, binMap, metaInfo);
		return true;
	}

	@Override
	public boolean cacheFlightListingOnSearchId(List<TripInfo> trips, String key, CacheMetaInfo metaInfo) {
		if (StringUtils.isBlank(metaInfo.getNamespace())) {
			metaInfo.setNamespace(AerospikeNamespace.FLIGHT_CACHE.name());
		}
		List<List<TripInfo>> tripInfoPartition = Lists.partition(trips, MAX_FLIGHT_IN_PARTITION);
		int index = 1;
		String tempKey = key;
		for (List<TripInfo> tripInfoList : tripInfoPartition) {
			int i = 0;
			Map<String, TripInfo> binMap = new LinkedHashMap<>();
			for (TripInfo trip : tripInfoList) {
				binMap.put(i + "", trip);
				i++;
			}
			aeroClient.store(tempKey, binMap, metaInfo);
			tempKey = key + "_" + index;
			index++;
		}
		LogUtils.log(LogTypes.AIRSEARCH_CACHEFLIGHTLISTING,
				SystemContextHolder.getContextData().getLogMetaInfo().setTrips(CollectionUtils.size(trips)),
				LogTypes.AIRSEARCH_STORETRIPKEY);
		return true;
	}

	@Override
	public boolean cacheSearchResult(List<TripInfo> trips, String key, CacheMetaInfo metaInfo) {
		List<List<TripInfo>> tripInfoPartition = Lists.partition(trips, MAX_FLIGHT_IN_PARTITION_1);
		String tempKey = key;
		int index = 1;
		String createdOn = java.time.LocalDateTime.now().toString();
		for (List<TripInfo> tripInfos : tripInfoPartition) {
			Map<String, Object> binMap = new LinkedHashMap<>();
			binMap.put(BinName.CREATIONTIME.getName(), createdOn);
			binMap.put(BinName.SEARCHLIST.getName(), tripInfos);
			aeroClient.store(tempKey, binMap, metaInfo);
			tempKey = key + "_" + index;
			index++;
		}
		return true;
	}

	@Override
	public <V> List<V> fetchValues(String key, Class<V> classofV, CacheMetaInfo metaInfo) {
		if (StringUtils.isBlank(metaInfo.getNamespace())) {
			metaInfo.setNamespace(AerospikeNamespace.FLIGHT_CACHE.name());
		}
		if (classofV.getTypeName().equals(TripInfo.class.getTypeName())) {
			return fetchPartionValues(key, classofV, metaInfo);
		}
		return aeroClient.get(key, classofV, metaInfo);
	}

	@Override
	public <V> List<V> fetchPartionValues(String key, Class<V> classofV, CacheMetaInfo metaInfo) {
		List<V> resultList = new ArrayList<V>();
		List<V> templist = aeroClient.get(key, classofV, metaInfo);
		if (CollectionUtils.isEmpty(templist))
			return null;
		resultList.addAll(templist);
		int index = 1;
		String tempKey = key;
		while (templist.size() >= MAX_FLIGHT_IN_PARTITION) {
			tempKey = key + "_" + index;
			templist = aeroClient.get(tempKey, classofV, metaInfo);
			if (!templist.isEmpty())
				resultList.addAll(templist);
			index++;
		}
		return resultList;
	}

	@Override
	public AirCacheSearchResult fetchAirSearchResult(String key, CacheMetaInfo metaInfo) {
		return fetchPartionAirSearchResult(key, metaInfo);
	}

	public AirCacheSearchResult fetchPartionAirSearchResult(String key, CacheMetaInfo metaInfo) {
		AirCacheSearchResult cacheSearchResult = AirCacheSearchResult.builder().build();
		AirCacheSearchResult tempList = aeroClient.get(key, metaInfo);
		if (Objects.isNull(tempList) || CollectionUtils.isEmpty(tempList.getTripInfos()))
			return null;
		cacheSearchResult.setCreationTime(tempList.getCreationTime());
		cacheSearchResult.getTripInfos().addAll(tempList.getTripInfos());
		int index = 1;
		String tempKey = key;
		while (tempList.getTripInfos().size() >= MAX_FLIGHT_IN_PARTITION_1) {
			tempKey = key + "_" + index;
			tempList = aeroClient.get(tempKey, metaInfo);
			if (Objects.nonNull(tempList)) {
				cacheSearchResult.getTripInfos().addAll(tempList.getTripInfos());
			} else
				break;
			index++;
		}
		return cacheSearchResult;
	}


	@Override
	public <V> V fetchValue(String key, Class<V> classofV, String set, String binValue) {
		return aeroClient.get(key, classofV, set, binValue);
	}


	@Override
	public <V> Map<String, V> fetchValue(String key, Class<V> classofV, String set, String... binValue) {
		return aeroClient.get(key, classofV, set, binValue);
	}

	@Override
	public boolean deleteRecord(CacheMetaInfo metaInfo) {
		return aeroClient.delete(metaInfo.getKey(), metaInfo);
	}

	@Override
	public TripInfo getTripInfo(String priceId) {
		List<TripInfo> tripInfos = fetchValues(priceId, TripInfo.class, CacheMetaInfo.builder()
				.namespace(CacheNameSpace.FLIGHT.getName()).set(CacheSetName.PRICE_INFO.name()).build());
		if (CollectionUtils.isEmpty(tripInfos)) {
			log.info("No tripInfo found for priceId {}", priceId);
			throw new CachingLayerException(CachingLayerError.READ_ERROR);
		}
		return tripInfos.get(0);
	}

	@Override
	public AirSearchQuery getSearchQuery(String searchId) {
		AirSearchQuery searchQuery = fetchValue(searchId, AirSearchQuery.class, CacheSetName.SEARCHQUERY.name(),
				BinName.SEARCHQUERYBIN.getName());
		if (searchQuery == null) {
			log.error("No AirSearchQuery found for searchId {}", searchId);
			throw new CachingLayerException(CachingLayerError.READ_ERROR);
		}
		return searchQuery;
	}

	@Override
	public AirReviewResponse getAirReviewResponse(String bookingId) {
		AirReviewResponse reviewResponse = fetchValue(bookingId, AirReviewResponse.class,
				CacheSetName.AIR_REVIEW.getName(), BinName.AIRREVIEW.getName());
		if (reviewResponse == null) {
			log.error("No AirSearchResponse found for bookingId {}", bookingId);
			throw new CachingLayerException(CachingLayerError.READ_ERROR);
		}
		return reviewResponse;
	}

	@Override
	public void scanAndDelete(CacheMetaInfo metaInfo) {
		aeroClient.scanAndDelete(metaInfo);
	}

	@Override
	public <V> V fetchValue(Class<V> classofV, CacheMetaInfo metaInfo) {
		return aeroClient.get(classofV, metaInfo);
	}

	@Override
	public boolean deleteCacheRecord(CacheMetaInfo metaInfo) {
		String key = metaInfo.getKey();
		AirCacheSearchResult cacheResult = aeroClient.get(key, metaInfo);
		if (Objects.isNull(cacheResult) || CollectionUtils.isEmpty(cacheResult.getTripInfos()))
			return false;

		if (cacheResult.getTripInfos().size() < MAX_FLIGHT_IN_PARTITION_1)
			return aeroClient.delete(key, metaInfo);

		boolean status = false;
		String tempKey = key;
		int index = 1;

		while (Objects.nonNull(cacheResult)) {
			status = aeroClient.delete(tempKey, metaInfo);
			tempKey = key + "_" + index;
			cacheResult = aeroClient.get(tempKey, metaInfo);
			index++;
		}
		return status;
	}
}
