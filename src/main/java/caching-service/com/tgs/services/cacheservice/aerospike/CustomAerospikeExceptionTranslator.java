package com.tgs.services.cacheservice.aerospike;

import com.aerospike.client.AerospikeException;
import org.springframework.dao.*;
import org.springframework.data.aerospike.core.AerospikeExceptionTranslator;

public class CustomAerospikeExceptionTranslator implements AerospikeExceptionTranslator {

	public CustomAerospikeExceptionTranslator() {}

	public DataAccessException translateExceptionIfPossible(RuntimeException cause) {
		if (cause instanceof AerospikeException) {
			int resultCode = ((AerospikeException) cause).getResultCode();
			String msg = cause.getMessage();
			switch (resultCode) {
				case -7:
				case 14:
				case 18:
					return new TransientDataAccessResourceException(msg, cause);
				case 2:
					return new DataRetrievalFailureException(msg, cause);
				case 5:
					return new DuplicateKeyException(msg, cause);
				case 9:
				case 212:
					return new QueryTimeoutException(msg, cause);
				case 200:
				case 201:
					return new InvalidDataAccessResourceUsageException(msg, cause);
				default:
					return new RecoverableDataAccessException(msg, cause);
			}
		}
		return null;
	}
}
