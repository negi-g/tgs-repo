package com.tgs.services.cacheservice.runtime.impl;

import java.time.LocalDateTime;
import java.util.*;

import com.tgs.services.fms.datamodel.AirCacheSearchResult;
import com.tgs.services.fms.datamodel.TripInfo;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.ScanCallback;
import com.aerospike.client.policy.Policy;
import com.aerospike.client.policy.Priority;
import com.aerospike.client.policy.ScanPolicy;
import com.aerospike.client.policy.WritePolicy;
import com.tgs.services.cacheservice.aerospike.AerospikeConfiguration;
import com.tgs.services.cacheservice.aerospike.AerospikeHelper;
import com.tgs.services.cacheservice.aerospike.AerospikeNamespace;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.utils.encryption.KryoUtils;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FlightAerospikeClient implements ScanCallback {

	@Autowired
	AerospikeClient client;

	@Autowired
	AerospikeConfiguration conf;

	@Value("${setPrefix}")
	private String setPrefix;

	public <V> void store(String key, Map<String, V> binMap, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		try {
			String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
			Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
			client.put(AerospikeHelper.getWritePolicy(metaInfo.getExpiration()), aerospikekey,
					AerospikeHelper.generateBinList(binMap, BooleanUtils.isNotFalse(metaInfo.getCompress()),
							BooleanUtils.isFalse(metaInfo.getPlainData()),
							BooleanUtils.isTrue(metaInfo.getKyroCompress())).toArray(new Bin[0]));
		} catch (Exception e) {
			log.error("Failed to store key {} , bin {} in aerospike", key, binMap.keySet(), e);
			throw e;
		}
	}

	public <V> V get(String key, Class<V> classofV, String set, String bin) {
		return get(key, classofV, set, bin, "compression").get(bin);
	}

	public <V> Map<String, V> get(String key, Class<V> classofV, String set, String... bin) {
		verifySetInRequest(set);
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		Policy policy = new Policy();
		String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.FLIGHT.name().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(set), key);
		Record record = client.get(policy, aerospikekey, bin);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().compress(true).set(setPrefix.concat("_").concat(set)).build();
		return AerospikeHelper.convertingBinMapToUserDefinedMap(record, classofV, metaInfo);
	}

	public <V> List<V> get(String key, Class<V> classofV, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		// ScanKeys();
		Policy policy = new Policy();
		String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
		Record result = client.get(policy, aerospikekey);
		metaInfo.setCompress(true);
		List<V> binList =
				new ArrayList<V>(AerospikeHelper.convertingBinMapToUserDefinedMap(result, classofV, metaInfo).values());
		return binList;
	}

	public boolean delete(String key, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		WritePolicy wpolicy = new WritePolicy();
		String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
		return client.delete(wpolicy, aerospikekey);
	}

	public void scanAndDelete(CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		ScanPolicy policy = new ScanPolicy();
		policy.includeBinData = false;
		policy.concurrentNodes = false;
		policy.priority = Priority.LOW;
		String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
		client.scanAll(policy, mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), this, null);
	}

	@Override
	public void scanCallback(Key key, Record record) throws AerospikeException {
		WritePolicy wpolicy = new WritePolicy();
		log.info("Scan Key is {} , expiration is {} ", key.userKey, record.expiration);
		List<Bin> binList = new ArrayList<>();
		binList.add(new Bin("expire", true));
		/**
		 * Instead of deleting set TTL as 1 sec
		 */
		client.put(AerospikeHelper.getWritePolicy(1), key, binList.toArray(new Bin[0]));
	}

	public void verifySetInRequest(String set) {
		try {
			if (StringUtils.isEmpty(set)) {
				throw new Exception();
			}
		} catch (Exception e) {
			log.info("Set is empty {}", e);
		}
	}


	public <V> V get(Class<V> classofV, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(metaInfo.getKey())) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		try {
			Policy policy = new Policy();
			String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
			Key aerospikekey =
					new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey());
			Record record = client.get(policy, aerospikekey, metaInfo.getBins()[0]);
			return AerospikeHelper.convertingBinMapToUserDefinedMap(record, classofV, metaInfo)
					.get(metaInfo.getBins()[0]);
		} catch (Exception e) {
			log.error("Failed to get metaInfo {} ", metaInfo, e);
		}
		return null;
	}


	public AirCacheSearchResult get(String sqKey, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(sqKey))
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		Policy policy = new Policy();
		String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
		Key key = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), sqKey);

		// returns multiple type object in record
		Record result = client.get(policy, key);
		metaInfo.setCompress(true);
		if (Objects.isNull(result))
			return null;

		LocalDateTime creationTime = null;
		if (result.bins.containsKey(BinName.CREATIONTIME.getName())) {
			creationTime =
					LocalDateTime.parse(KryoUtils.deserialize((byte[]) result.bins.get(BinName.CREATIONTIME.getName()),
							String.class, BooleanUtils.isTrue(metaInfo.getKyroCompress())).get());
		}
		List<TripInfo> trips = KryoUtils.deserialize((byte[]) result.getValue(BinName.SEARCHLIST.getName()),
				ArrayList.class, BooleanUtils.isTrue(metaInfo.getKyroCompress())).get();
		return AirCacheSearchResult.builder().creationTime(creationTime).tripInfos(trips).build();
	}
}
