package com.tgs.services.nss.sources.spicejet;

import com.tgs.services.nss.sources.AbstractNoShowRefundFactory;
import org.springframework.stereotype.Service;
import com.tgs.services.nss.restmodel.NoShowRefundRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SpiceJetNoShowRefundFactroy extends AbstractNoShowRefundFactory {

	public SpiceJetNoShowRefundFactroy(NoShowRefundRequest noShowRefundRequest) {
		super(noShowRefundRequest);

	}

	@Override
	protected boolean refundNoShow() {
		return false;
	}

}
