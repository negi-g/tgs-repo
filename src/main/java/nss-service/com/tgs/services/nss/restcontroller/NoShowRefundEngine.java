package com.tgs.services.nss.restcontroller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import org.springframework.stereotype.Service;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.nss.helper.NoShowRefundSourceType;
import com.tgs.services.nss.restmodel.NoShowRefundRequest;
import com.tgs.services.nss.sources.AbstractNoShowRefundFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NoShowRefundEngine {

	public void processNoShowRefund(NoShowRefundRequest noShowRefundRequest) {
		try {

			List<Future<?>> futures = new ArrayList<>();
			NoShowRefundSourceType refundSourceType =
					NoShowRefundSourceType.getNoShowRefundSourceType(noShowRefundRequest.getAirline());
			AbstractNoShowRefundFactory noShowRefundFactory = refundSourceType.getFactoryInstance(noShowRefundRequest);

			Future<?> future = ExecutorUtils.getNoShowThreadPool().submit(() -> {
				noShowRefundFactory.refundNoShowPnr();
			});

			future.get();
			/*
			 * futures.add(future); if (futures.size() == 10) { for (Future<?> f : futures) { try { f.get(40,
			 * TimeUnit.SECONDS); } catch (Exception e) { log.info("Unable to get get response in 40 seconds due to {}",
			 * e.getMessage()); } } futures.clear(); }
			 */

		} catch (Exception e) {
			log.error("Exception Occured during refund of NoShow Pnr due to {}", noShowRefundRequest, e);
		}

	}


}
