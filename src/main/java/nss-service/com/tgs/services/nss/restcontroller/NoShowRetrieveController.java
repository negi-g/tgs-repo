package com.tgs.services.nss.restcontroller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.nss.datamodel.NoShowInfo;
import com.tgs.services.nss.restmodel.NoShowBookingRetrieveRequest;
import com.tgs.services.nss.restmodel.NoShowRefundRequest;
import com.tgs.services.nss.restmodel.NoShowBookingRetrieveResponse;
import com.tgs.services.nss.restmodel.NoShowRetrieveRequest;
import com.tgs.services.nss.restmodel.NoShowRetrieveResponse;
import com.tgs.services.nss.servicehandler.NoShowRefundHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.nss.manager.NoShowRetrieveEngine;
import com.tgs.services.nss.servicehandler.NoShowRetrieveHandler;

@RestController
@RequestMapping("/nss/v1")
public class NoShowRetrieveController {

	@Autowired
	NoShowRetrieveHandler noShowHandler;

	@Autowired
	NoShowRefundHandler refundHandler;

	@Autowired
	NoShowRetrieveEngine noShowBookingEngine;

	@Autowired
	NoShowRefundEngine noShowRefundEngine;

	@RequestMapping(value = "/fetch/noshow", method = RequestMethod.POST)
	protected NoShowRetrieveResponse fetchBookings(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid NoShowBookingRetrieveRequest noShowBookingRequest) throws Exception {
		Map<String, NoShowInfo> noShowInfoMap = noShowBookingEngine.findUniquePnrsMap(noShowBookingRequest);
		noShowHandler.initData(NoShowRetrieveRequest.builder().noShowInfoMap(noShowInfoMap).build(), new NoShowRetrieveResponse());
		return noShowHandler.getResponse();
	}

	@RequestMapping(value = "/upload/pnrmap", method = RequestMethod.POST)
	protected NoShowRetrieveResponse fetchBookings(HttpServletRequest request, HttpServletResponse response,
			@RequestBody NoShowRetrieveRequest noShowRequest) throws Exception {
		noShowHandler.initData(noShowRequest, new NoShowRetrieveResponse());
		return noShowHandler.getResponse();
	}

	@RequestMapping(value = "/noshow/refund", method = RequestMethod.POST)
	protected NoShowBookingRetrieveResponse autoRefundNoShow(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid NoShowRefundRequest noShowrefundrequest) throws Exception {
		refundHandler.initData(noShowrefundrequest, NoShowBookingRetrieveResponse.builder().build());
		return refundHandler.getResponse();
	}


}
