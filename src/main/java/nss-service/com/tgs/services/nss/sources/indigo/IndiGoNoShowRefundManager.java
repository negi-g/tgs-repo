package com.tgs.services.nss.sources.indigo;

import com.tgs.services.nss.restmodel.NoShowRefundRequest;
import com.tgs.services.nss.utils.NoShowScrapperUtils;
import com.tgs.utils.exception.air.NoShowRefundException;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.util.LinkedHashMap;

@Slf4j
@Builder
final class IndiGoNoShowRefundManager {

	public final static String INDIGO_FETCH_PNR_URL = "https://6ereaccomodation.goindigo.in/INDINSTX/?";


	public Document fetchNoShowPNRDetails(NoShowRefundRequest noShowRefundRequest) {
		LinkedHashMap<String, String> parameters = new LinkedHashMap<>();
		parameters.put("PNR", noShowRefundRequest.getPnr());
		parameters.put("Email", noShowRefundRequest.getEmail());
		parameters.put("submit", "Retrieve Booking");
		String requestUrl =
				StringUtils.join(INDIGO_FETCH_PNR_URL, NoShowScrapperUtils.getRequestData(false, parameters));
		log.debug("Fetch PNR {} Details url {}", noShowRefundRequest.getPnr(), requestUrl);
		Connection connection = Jsoup.connect(requestUrl).timeout(60000).ignoreContentType(true).ignoreHttpErrors(true);
		NoShowScrapperUtils.setConnectionProperties(connection);
		Document pnrResponse = NoShowScrapperUtils.getResponse(connection, Connection.Method.GET, null);
		log.debug("Fetch PNR Response {}", pnrResponse.text());
		return pnrResponse;
	}

	public boolean isNoShowValidPNR(Document pnrResponse) {
		boolean isRefundAllowed = true;
		if (pnrResponse != null && pnrResponse.getElementsByClass("errorMessage") != null) {
			Elements errorElement = pnrResponse.getElementsByClass("errorMessage");
			if (errorElement != null && StringUtils.isNotBlank(errorElement.text())) {
				isRefundAllowed = false;
				throw new NoShowRefundException(errorElement.text());
			}
		}
		return isRefundAllowed;
	}

	
}
