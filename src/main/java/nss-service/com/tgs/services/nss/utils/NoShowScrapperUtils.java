package com.tgs.services.nss.utils;

import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Set;
import static com.tgs.services.nss.utils.NoShowConstants.*;
import org.jsoup.nodes.Document;

@Slf4j
public class NoShowScrapperUtils {

	// jsoup
	public static void setConnectionProperties(Connection connection) {
		connection.header(KEY_ACCEPT, VALUE_ACCEPT);
		connection.header(KEY_ACCEPT_ENCODING, VALUE_ACCEPT_ENCODING);
		connection.header(KEY_ACCEPT_LANGUAGE, VALUE_ACCEPT_LANGUAGE);
		connection.header(KEY_CACHE_CONTROL, VALUE_NO_CACHE);
		connection.header(KEY_CONNECTION, VALUE_KEEP_ALIVE);
		connection.header(KEY_USER_AGENT, VALUE_USER_AGENT);
	}

	public static String getRequestData(boolean isEncodedUTF, LinkedHashMap<String, String> requestData) {
		Set<String> keys = requestData.keySet();
		StringBuffer sbuf = new StringBuffer();
		if (isEncodedUTF) {
			for (String key : keys) {
				try {
					sbuf.append(key).append("=").append(URLEncoder.encode(requestData.get(key), "UTF-8")).append("&");
				} catch (UnsupportedEncodingException e) {
					sbuf.append(key).append("=").append(requestData.get(key)).append("&");
				}
			}

		} else {
			for (String key : keys) {
				sbuf.append(key).append("=").append(requestData.get(key)).append("&");
			}

		}
		return sbuf.toString();
	}

	public static Document getResponse(Connection connection, Connection.Method method,
			LinkedHashMap<String, String> postData) {
		try {
			connection.method(method);
			if (Connection.Method.POST.equals(method)) {
				connection.data(postData);
				return connection.post();
			} else {
				return connection.get();
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		}
	}
}
