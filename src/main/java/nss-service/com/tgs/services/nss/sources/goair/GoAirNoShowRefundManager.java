package com.tgs.services.nss.sources.goair;

import com.tgs.services.nss.restmodel.NoShowRefundRequest;
import com.tgs.services.nss.utils.NoShowScrapperUtils;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.util.LinkedHashMap;

@Slf4j
@Builder
public class GoAirNoShowRefundManager {

	public final static String GOAIR_FETCH_PNR_URL = "https://www.goinet.in/gonoshow/default.aspx";

	public Document fetchNoShowPNRDetails(NoShowRefundRequest noShowRefundRequest) {
		log.debug("Fetch PNR {} Details url {}", noShowRefundRequest.getPnr(), GOAIR_FETCH_PNR_URL);
		Connection connection =
				Jsoup.connect(GOAIR_FETCH_PNR_URL).timeout(60000).ignoreContentType(true).ignoreHttpErrors(true);
		NoShowScrapperUtils.setConnectionProperties(connection);
		setConnectionHeader(connection);
		Document pnrResponse = NoShowScrapperUtils.getResponse(connection, Connection.Method.POST,
				getFetchPNRPostData(noShowRefundRequest));
		log.debug("Fetch PNR Response {}", pnrResponse.text());
		return pnrResponse;
	}

	private void setConnectionHeader(Connection connection) {
		connection.header("X-Requested-With", "XMLHttpRequest");
		connection.header("X-MicrosoftAjax", "Delta=true");
		connection.header("Host", "www.goinet.in");
		connection.header("Origin", "https://www.goinet.in");
		connection.header("Referer", GOAIR_FETCH_PNR_URL);
		connection.header("Sec-Fetch-Site", "same-origin");
		connection.header("Sec-Fetch-Mode", "cors");
	}

	public LinkedHashMap<String, String> getFetchPNRPostData(NoShowRefundRequest pnrRequest) {
		LinkedHashMap<String, String> parameters = new LinkedHashMap<>();
		parameters.put("ctl00$ScriptManager1",
				"ctl00$ContentPlaceHolder1$up_Item|ctl00$ContentPlaceHolder1$btn_RetrieveBooking");
		parameters.put("__EVENTTARGET", "");
		parameters.put("__EVENTARGUMENT", "");
		parameters.put("__VIEWSTATE", "/wEPDwUJMjE3NDY1NTg0ZGTcpc7C1g5IZIY9AN/3d4lEYZeL2NV99z5QbNqbLS1laA==");
		parameters.put("__VIEWSTATEGENERATOR", "76315384");
		parameters.put("__EVENTVALIDATION",
				"/wEdAAQcQxCanBVpPKY7Q0gOsmtnzb/KkUdKBEBB8shNNY4KMWkeczAMwf0tdVErNU+mMgTvUvLroVlON9CPocpH0C/8uHJ9DGQXZd1ZP8M6Q1UlYdBfgkY3KGHrZGHl8benc9k=");
		parameters.put("ctl00$ContentPlaceHolder1$txt_PNR", pnrRequest.getPnr());
		parameters.put("ctl00$ContentPlaceHolder1$txt_LastName", pnrRequest.getEmail());
		parameters.put("__ASYNCPOST", "true");
		parameters.put("ctl00$ContentPlaceHolder1$btn_RetrieveBooking", "Retrieve Booking");
		return parameters;
	}

}
