package com.tgs.services.nss.sources.indigo;

import com.tgs.services.nss.restmodel.NoShowRefundRequest;
import com.tgs.services.nss.sources.AbstractNoShowRefundFactory;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IndiGoNoShowRefundFactory extends AbstractNoShowRefundFactory {


	public IndiGoNoShowRefundFactory(NoShowRefundRequest noShowRefundRequest) {
		super(noShowRefundRequest);
	}


	@Override
	protected boolean refundNoShow() {
		IndiGoNoShowRefundManager refundManager = IndiGoNoShowRefundManager.builder().build();
		Document pnrResponse = refundManager.fetchNoShowPNRDetails(noShowRefundRequest);
		if (!refundManager.isNoShowValidPNR(pnrResponse)) {
			// confirm refund
		}
		return false;
	}

}
