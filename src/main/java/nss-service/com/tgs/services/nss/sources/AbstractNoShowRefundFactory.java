package com.tgs.services.nss.sources;

import com.tgs.services.nss.restmodel.NoShowRefundRequest;
import com.tgs.utils.exception.air.NoShowRefundException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import java.util.StringJoiner;

@Getter
@Setter
@Slf4j
public abstract class AbstractNoShowRefundFactory {

	public NoShowRefundRequest noShowRefundRequest;

	public AbstractNoShowRefundFactory(NoShowRefundRequest noShowRefundRequest) {
		this.noShowRefundRequest = noShowRefundRequest;
	}

	public void refundNoShowPnr() {
		log.info("Inside AbstractNoShowRefundfactory#refundNoShowPnr");
		boolean isRefundSuccess = false;
		StringJoiner errorMsg = new StringJoiner("");
		try {
			isRefundSuccess = this.refundNoShow();
		} catch (SupplierSessionException se) {
			errorMsg.add(se.getMessage());
			log.error("Authentication Failed for NoShow Refund {} error ", noShowRefundRequest, se);
		} catch (NoShowRefundException nre) {
			errorMsg.add(nre.getMessage());
			log.error("Refund Failed for {} error ", noShowRefundRequest, nre);
		} catch (SupplierRemoteException re) {
			errorMsg.add(re.getMessage());
			log.error("Network Failed for NoShow Refund {} error ", noShowRefundRequest, re);
		} catch (Exception e) {
			log.error("Error Occured while doing NoShow Refund due to {}", e);
		}
	}

	protected abstract boolean refundNoShow();

}
