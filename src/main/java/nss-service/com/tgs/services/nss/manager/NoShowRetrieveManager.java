package com.tgs.services.nss.manager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tgs.services.fms.datamodel.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.nss.analytics.NoShowAnalyticsQuery;
import com.tgs.services.nss.datamodel.NoShowInfo;
import com.tgs.services.oms.restmodel.air.AirImportPnrBookingRequest;
import com.tgs.utils.exception.air.NoPNRFoundException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NoShowRetrieveManager {

	@Autowired
	OrderServiceCommunicator orderComm;

	@Autowired
	FMSCommunicator fmsComm;

	@Autowired
	KafkaServiceCommunicator kafkaService;

	static List<QueueDataType> queueTypes;

	static {
		queueTypes = Arrays.asList(QueueDataType.ELASTICSEARCH);
	}


	/**
	 * This method will communicate to FMS and will give the list of travellers, who were NO SHOW corresponding to a
	 * PNR,
	 *
	 * @param pnr
	 * @param supplierId
	 * @return List of FlightTravellerInfo
	 */
	public void checkNoShowTravellers(String pnr, NoShowInfo noshowInfo) {
		try {
			AirImportPnrBooking pnrBooking = fmsComm.retrieveBooking(
					AirImportPnrBookingRequest.builder().pnr(pnr).supplierId(noshowInfo.getSupplierId()).build());
			if (pnrBooking != null && CollectionUtils.isNotEmpty(pnrBooking.getTripInfos())) {
				String emailId = pnrBooking.getEmailId();
				String contact = pnrBooking.getContactNo();
				for (TripInfo tripInfo : pnrBooking.getTripInfos()) {
					for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
						List<FlightTravellerInfo> travellers = new ArrayList<>();
						for (FlightTravellerInfo traveller : segmentInfo.getTravellerInfo()) {
							travellers.add(traveller);
							NoShowAnalyticsQuery noShowData = getNoShowAnalyticsData(traveller, segmentInfo);
							noShowData.setPnr(pnr);
							noShowData.setEmailid(emailId);
							noShowData.setContactno(contact);
							noShowData.setSupplierid(noshowInfo.getSupplierId());
							noShowData.setUserid(noshowInfo.getUserId());
							noShowData.setBookingid(noshowInfo.getBookingId());
							noShowData.setCreatedon(noshowInfo.getCreatedOn());
							pushToAnalytics(noShowData, traveller);
						}
					}
				}
			}
		} catch (NoPNRFoundException e) {
			log.error("No Pnr found for PNR {}, due to {}", pnr, e.getMessage());
		} catch (Exception e) {
			log.error("Error Occured for PNR {} ,due to ", pnr, e);
		}
	}


	private void pushToAnalytics(NoShowAnalyticsQuery noShowData, FlightTravellerInfo traveller) {
		try {
			QueueData queueData = QueueData.builder().key(ESMetaInfo.NO_SHOW.getIndex())
					.value(GsonUtils.getGson().toJson(noShowData)).build();
			kafkaService.queue(queueTypes, queueData);
		} catch (Exception e) {
			log.error("Failed to Push Analytics type  {} for pnr {} due to ", "NO_SHOW", traveller.getPnr(), e);
		}
	}

	private NoShowAnalyticsQuery getNoShowAnalyticsData(FlightTravellerInfo traveller, SegmentInfo segmentInfo) {
		NoShowAnalyticsQuery noShowQuery = NoShowAnalyticsQuery.builder().build();
		noShowQuery.setFirstname(traveller.getFirstName());
		noShowQuery.setLastname(traveller.getLastName());
		noShowQuery.setTitle(traveller.getTitle());
		noShowQuery.setPaxtype(traveller.getPaxType().toString());
		noShowQuery.setAirline(segmentInfo.getAirlineCode(false));
		noShowQuery.setRouteinfo(
				StringUtils.join(segmentInfo.getDepartureAirportCode(), "-", segmentInfo.getArrivalAirportCode()));
		noShowQuery.setTraveldate(segmentInfo.getDepartTime().toString());
		if (traveller.getStatus() != null) {
			noShowQuery.setTravellerstatus(traveller.getStatus().toString());
		}
		return noShowQuery;
	}


}
