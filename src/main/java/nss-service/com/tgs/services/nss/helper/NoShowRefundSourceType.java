package com.tgs.services.nss.helper;

import com.tgs.services.nss.sources.indigo.IndiGoNoShowRefundFactory;
import com.tgs.services.oms.restmodel.AirCreditShellRequest;
import org.apache.commons.lang.WordUtils;
import com.tgs.services.base.SpringContext;
import com.tgs.services.nss.restmodel.NoShowRefundRequest;
import com.tgs.services.nss.sources.AbstractNoShowRefundFactory;
import com.tgs.services.nss.sources.goair.GoAirNoShowRefundFactory;
import com.tgs.services.nss.sources.spicejet.SpiceJetNoShowRefundFactroy;
import lombok.Getter;

@Getter
public enum NoShowRefundSourceType {


	GOAIR("G8") {

		@Override
		public AbstractNoShowRefundFactory getFactoryInstance(NoShowRefundRequest noShowRefundRequest) {
			return getNoShowRefundBean(noShowRefundRequest, GoAirNoShowRefundFactory.class);
		}

	},
	SPICEJET("SG") {

		@Override
		public AbstractNoShowRefundFactory getFactoryInstance(NoShowRefundRequest noShowRefundRequest) {
			return getNoShowRefundBean(noShowRefundRequest, SpiceJetNoShowRefundFactroy.class);
		}

	},
	INDIGO("6E") {

		@Override
		public AbstractNoShowRefundFactory getFactoryInstance(NoShowRefundRequest noShowRefundRequest) {
			return getNoShowRefundBean(noShowRefundRequest, IndiGoNoShowRefundFactory.class);
		}

	},
	FLYSCOOT("TR") {

		@Override
		public AbstractNoShowRefundFactory getFactoryInstance(NoShowRefundRequest noShowRefundRequest) {
			return null;
		}

	},
	JAZEERA("J9") {

		@Override
		public AbstractNoShowRefundFactory getFactoryInstance(NoShowRefundRequest noShowRefundRequest) {
			return null;
		}

	};

	private String airline;

	private NoShowRefundSourceType(String airline) {
		this.airline = airline;
	}


	public static NoShowRefundSourceType getNoShowRefundSourceType(String airline) {
		for (NoShowRefundSourceType refundSourceType : NoShowRefundSourceType.values()) {
			if (refundSourceType.getAirline().equalsIgnoreCase(airline)) {
				return refundSourceType;
			}
		}
		return null;
	}

	public abstract AbstractNoShowRefundFactory getFactoryInstance(NoShowRefundRequest noShowRefundRequest);

	public <T extends AbstractNoShowRefundFactory> AbstractNoShowRefundFactory getNoShowRefundBean(
			NoShowRefundRequest noShowRefundRequest, Class<T> defaultClass) {
		return (AbstractNoShowRefundFactory) SpringContext.getApplicationContext()
				.getBean(WordUtils.uncapitalize(defaultClass.getSimpleName()), noShowRefundRequest);
	}


}
