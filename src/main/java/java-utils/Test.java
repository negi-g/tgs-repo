import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.gson.JsonStringSerializer;
import com.tgs.utils.encryption.KryoUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

public class Test {

	@Getter
	@Setter
	public static class Policy {
		@JsonAdapter(JsonStringSerializer.class)
		private String data;
	}

	@ToString
	public static class InnerClass {
		public String name;

		public InnerClass(String name) {
			this.name = name;
		}

	}

	public static void main(String[] args) {
		String className = "com.tgs.services.fms.servicehandler.AirBookingHandler";

		boolean match = className.matches("com.tgs.services.*.(restcontroller|manager|servicehandler).*");

		System.out.println(match);
		System.out.println(System.currentTimeMillis());
		System.out.println(System.nanoTime());

		List<InnerClass> names = new ArrayList<>();
		names.add(new InnerClass("ashu"));
		names.add(new InnerClass("gupta"));
		KryoUtils.serialize(names);
		List<InnerClass> someClassRecords = new ArrayList<>();

		System.out.println(KryoUtils.deserialize(KryoUtils.serialize(names).get(), someClassRecords.getClass()).get());

		LocalDate date = LocalDate.now();
		System.out.println(date.format(DateTimeFormatter.ofPattern("yyyy-MM-ddHH:mm:ss")));

		Policy pl = new Policy();
		pl.setData(
				"{\"view\": \"accounts\", \"url\" :\"/accounts\", \"fields\":[ {\"name\":\"product_type\", \"value\" : \"AirOrder\"}]}");

		System.out.println(new Gson().toJson(pl));

		String str = "{\"data\":{\"view\":\"accounts\",\"url\":\"/accounts\",\"fields\":[{\"name\":\"product_type\",\"value\":\"AirOrder\"}]}}";
		Policy pl1 = new Gson().fromJson(str, Policy.class);
		System.out.println(pl1.getData());

		// GsonTest gt = new GsonTest();
		// gt.setEmail("ashu@gmail.com");
		// gt.setName("Ashu");
		// System.out.println(TgsGsonUtils.getGson().toJson(gt));
		//
		// GsonTest gt1 = new GsonTest();
		// gt1.setName("Gupta");
		// gt1 = TgsGsonUtils.getGsonBuilder().registerTypeAdapter(GsonTest.class, new
		// CustomInstanceCreator(gt1)).create()
		// .fromJson("{\"email\":\"ashu@gmail.com\"}", GsonTest.class);
		//
		// System.out.println(gt1.getEmail() + gt1.getName());
		SQSExample.main(args);
	}
}
