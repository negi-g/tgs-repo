import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.pms.datamodel.Cycle;

public class PolicyCycle {

	public static void main(String[] args) throws IOException {


		String file = "/Users/nehaarora/Downloads/Corporate_Fortnightly1.xlsx";
		FileInputStream fis = new FileInputStream(new File(file));
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet = wb.getSheetAt(1);
		List<Cycle> cycles = new ArrayList<>();
		int id = 1;
		for (Row row : sheet) {
			if (row == null || row.getCell(0) == null)
				break;
			// System.out.println(row.getCell(0).getCellType());
			if (CellType.FORMULA.equals(row.getCell(0).getCellType())) {
				System.out.println(row.getCell(0).getDateCellValue().getYear());
				if (row.getCell(0).getDateCellValue().getYear() == 124) {
					Cycle cycle = new Cycle();
					cycle.setId(id++);
					cycle.setDue(convert(row.getCell(3).getDateCellValue()));
					cycle.setEnd(convert(row.getCell(2).getDateCellValue()));
					cycle.setLock(convert(row.getCell(5).getDateCellValue()));
					cycle.setStart(convert(row.getCell(0).getDateCellValue()));
					cycles.add(cycle);
				}
			}
		}
		System.out.println(GsonUtils.getGson().toJson(cycles));
	}

	private static LocalDate convert(Date date) {
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
}
