package com.tgs.utils.exception.air;

public class FareRuleUnHandledFaultException extends RuntimeException {

	private static final long serialVersionUID = 11L;

	public FareRuleUnHandledFaultException(String message) {
		super(message);
	}
}
