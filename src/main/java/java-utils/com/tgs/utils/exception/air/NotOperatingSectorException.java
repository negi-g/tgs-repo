package com.tgs.utils.exception.air;

public class NotOperatingSectorException extends RuntimeException {

    private static final long serialVersionUID = 6L;

    public NotOperatingSectorException(String message) {
        super(message);
    }

    public NotOperatingSectorException() {
        super();
    }
}
