package com.tgs.utils.exception.air;

public class ImportPNRNotAllowedException extends RuntimeException {

    private static final long serialVersionUID = 4L;

    public ImportPNRNotAllowedException(String message) {
        super(message);
    }
}
