package com.tgs.utils.exception;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;

public class PaymentException extends CustomGeneralException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PaymentException(SystemError error) {
		super(error);
	}

}
