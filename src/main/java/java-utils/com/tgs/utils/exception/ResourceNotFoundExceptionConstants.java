package com.tgs.utils.exception;

public class ResourceNotFoundExceptionConstants {

	public static final String INVALID_USERID = "Invalid UserId";
	public static final String INVALID_LOGIN_DETAILS = "Invalid Mobile Or Email. There doesn't exist any user in our database with mentioned mobile or Email";
	public static final String EXPIRED_KEYS = "Keys passed in the request has been expired";
}
