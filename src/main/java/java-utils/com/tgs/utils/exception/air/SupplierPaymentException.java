package com.tgs.utils.exception.air;


public class SupplierPaymentException extends RuntimeException {

    private static final long serialVersionUID = 8L;

    public SupplierPaymentException(String message) {
        super(message);
    }
}
