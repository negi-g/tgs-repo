package com.tgs.utils.exception.air;

public class SupplierSeatMapFaultException extends RuntimeException {

	private static final long serialVersionUID = 10L;

	public SupplierSeatMapFaultException(String message) {
		super(message);
	}
}
