package com.tgs.utils.exception.air;

public class NoPNRFoundException extends RuntimeException {

    private static final long serialVersionUID = 4L;

    public NoPNRFoundException(String message) {
        super(message);
    }

}
