package com.tgs.utils.exception.air;

import com.tgs.services.base.helper.SystemError;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class HoldLimitException extends RuntimeException {


    private static final long serialVersionUID = 10L;

    private SystemError error;

    public HoldLimitException(SystemError error) {
        this.error = error;
    }

}
