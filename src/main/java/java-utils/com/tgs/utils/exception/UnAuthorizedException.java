package com.tgs.utils.exception;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;

public class UnAuthorizedException extends CustomGeneralException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnAuthorizedException(String message) {
		super(message);
	}

	public UnAuthorizedException(SystemError error) {
		super(error);
	}
}
