package com.tgs.utils.aws;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.tgs.utils.file.TgsFileUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AwsUtils {

	public static String uploadToAws(File file, String bucketName) {
		try (FileInputStream fis = new FileInputStream(file)) {
			return uploadToAws(fis, bucketName, file.getName());
		} catch (IOException e) {
			log.error("Unable to upload file to AWS server with filename {} into bucket {} due to", file.getName(),
					bucketName, e);
		}
		return null;
	}

	/**
	 * 
	 * @param is
	 * @param bucketName
	 * @param objectKey
	 * @return s3 object URL. Returns {@code null}, if failed to upload.
	 */
	public static String uploadToAws(InputStream is, String bucketName, String objectKey) {
		log.debug("Uploading object to AWS server with objectKey {} into bucket {}", objectKey, bucketName);
		try {
			AmazonS3 s3Client =
					AmazonS3ClientBuilder.standard().withCredentials(new ProfileCredentialsProvider()).build();
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType(TgsFileUtils.resolveContentType(objectKey));
			metadata.addUserMetadata("x-amz-meta-title", objectKey);
			PutObjectRequest request = new PutObjectRequest(bucketName, objectKey, is, metadata)
					.withCannedAcl(CannedAccessControlList.PublicRead);
			s3Client.putObject(request);
			return s3Client.getUrl(bucketName, objectKey).getPath();
		} catch (SdkClientException e) {
			log.error("Unable to upload object to AWS server with objectKey {} into bucket {} due to", objectKey,
					bucketName, e);
		}
		return null;

	}

	/**
	 * 
	 * @param os
	 * @param bucketName
	 * @param objectKey
	 * @return {@code true} if read was successful
	 */
	public static boolean readFromAws(OutputStream os, String bucketName, String objectKey) {
		if (os == null) {
			throw new IllegalArgumentException("OutputStream os can't be null");
		}
		log.debug("Downloading object from AWS server with objectKey {} into bucket {}", objectKey, bucketName);
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withCredentials(new ProfileCredentialsProvider()).build();
		try {
			S3Object o = s3Client.getObject(bucketName, objectKey);
			S3ObjectInputStream s3is = o.getObjectContent();
			byte[] read_buf = new byte[1024];
			int read_len = 0;
			while ((read_len = s3is.read(read_buf)) > 0) {
				os.write(read_buf, 0, read_len);
			}
			s3is.close();
			return true;
		} catch (SdkClientException | IOException e) {
			log.error("Unable to read object from AWS server with objectKey {} in bucket {} due to", objectKey,
					bucketName, e);
		}
		return false;
	}

	/**
	 * 
	 * @param bucketName
	 * @param objectKeyPrefix
	 * @return non-null list of S3 object keys
	 */
	public static List<String> listAwsObjects(String bucketName, String objectKeyPrefix) {
		List<String> keyList = new ArrayList<>();
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withCredentials(new ProfileCredentialsProvider()).build();
		try {
			ObjectListing objectListing = s3Client.listObjects(bucketName, objectKeyPrefix);
			while (true) {
				List<S3ObjectSummary> objectSummaries = objectListing.getObjectSummaries();
				if (isNotEmpty(objectSummaries)) {
					objectSummaries.forEach(summary -> keyList.add(summary.getKey()));
				}
				if (!objectListing.isTruncated()) {
					break;
				}
				objectListing = s3Client.listNextBatchOfObjects(objectListing);
			}
		} catch (SdkClientException e) {
			log.error("Unable to list objects with objectKeyPrefix {} in AWS S3 bucket {} due to", objectKeyPrefix,
					bucketName, e);
		}
		return keyList;
	}
}
