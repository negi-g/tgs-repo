package com.tgs.utils.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TgsPdfUtils {

	public static void writeToPdf(List<Object[]> data, File file, List<String> columnNames) {

		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(file));
			document.open();
			PdfPTable table = new PdfPTable(columnNames.size());
			addTableHeader(table, columnNames);
			data.forEach(row -> {
				try {
					for (Object obj : row) {
						table.addCell(String.valueOf(obj) != "null" ? String.valueOf(obj) : "");
					}
				} catch (Exception e) {
					log.error("Unable to write to pdf file with filename {} and data {} ", file.getName(),
							Arrays.toString(row), e);
				}
			});
			document.add(table);
			document.close();
		} catch (FileNotFoundException | DocumentException e) {
			log.error("Error occured while writing to pdf file with filename {} ", file.getName(), e);
		}

	}

	private static void addTableHeader(PdfPTable table, List<String> columnNames) {
		columnNames.forEach(columnTitle -> {
			PdfPCell header = new PdfPCell();
			header.setBackgroundColor(BaseColor.LIGHT_GRAY);
			header.setBorderWidth(2);
			header.setPhrase(new Phrase(columnTitle));
			table.addCell(header);
		});
	}

}
