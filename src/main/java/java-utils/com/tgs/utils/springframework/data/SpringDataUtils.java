package com.tgs.utils.springframework.data;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import com.tgs.services.base.datamodel.QueryFilter;

public class SpringDataUtils {

	public static PageRequest getPageRequestFromFilter(QueryFilter filter) {
		PageRequest request = new PageRequest(0, 15000);
		if (filter.getPageAttr() != null) {
			if (filter.getPageAttr().getSortByAttr() != null) {
				Direction direction = Direction.fromString(filter.getPageAttr().getSortByAttr().get(0).getOrderBy());
				List<String> properties = filter.getPageAttr().getSortByAttr().get(0).getParams();
				request = new PageRequest(filter.getPageAttr().getPageNumber(), filter.getPageAttr().getSize(),
						direction, properties.toArray(new String[0]));
			} else {
				request = new PageRequest(filter.getPageAttr().getPageNumber(), filter.getPageAttr().getSize());
			}
		}
		return request;
	}

	public static void setCreatedOnPredicateUsingQueryFilter(Root<?> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, QueryFilter filter, List<Predicate> predicates) {
		if (filter.getCreatedOnBeforeDateTime() != null) {
			predicates.add(criteriaBuilder.lessThan(root.get("createdOn"), filter.getCreatedOnBeforeDateTime()));
		} else if (filter.getCreatedOnBeforeDate() != null) {
			predicates.add(criteriaBuilder.lessThan(root.get("createdOn"),
					LocalDateTime.of(filter.getCreatedOnBeforeDate(), LocalTime.MAX)));
		}

		if (filter.getCreatedOnAfterDateTime() != null) {
			predicates.add(criteriaBuilder.greaterThan(root.get("createdOn"), filter.getCreatedOnAfterDateTime()));
		} else if (filter.getCreatedOnAfterDate() != null) {
			predicates.add(criteriaBuilder.greaterThan(root.get("createdOn"),
					LocalDateTime.of(filter.getCreatedOnAfterDate(), LocalTime.MIN)));
		}
	}
}
