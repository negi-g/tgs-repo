package com.tgs.utils.string;

import java.util.HashMap;
import java.util.Map;

import org.antlr.stringtemplate.StringTemplate;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.gson.GsonUtils;

public class StringTemplateUtils {

	public static String replaceAttributes(String text, Object attrObj) {
		StringTemplate template = new StringTemplate(text);
		Map<String, Object> keyValuePairMap = new Gson().fromJson(GsonUtils.getGson().toJson(attrObj),
				new TypeToken<HashMap<String, Object>>() {
				}.getType());
		for (String key : keyValuePairMap.keySet()) {
			template.setAttribute(key, keyValuePairMap.get(key));
		}

		return template.toString();
	}

}
