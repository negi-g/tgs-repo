package com.tgs.utils.common;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SpelExpressionUtils {

	/**
	 * Expression will be evaluated using attributes.<br>
	 * All the coefficients are assumed to be percentage.
	 */
	public static Double evaluateExpression(String expression, Map<String, Double> attributes) {
		expression = expression.replaceAll("\\s+", "");
		boolean isPercentageExpression = expression.indexOf('%') >= 0;
		expression = expression.replaceAll("%", "");
		// if expression is number only
		if (isPercentageExpression && NumberUtils.isCreatable(expression)) {
			expression = expression.concat("*amt");
		}
		ExpressionParser expressionParser = new SpelExpressionParser();
		String commercialComponent = getFareComponentExpression(expression, attributes);
		Expression exp = expressionParser.parseExpression(commercialComponent);
		return exp.getValue(Double.class);
	}

	public static String getFareComponentExpression(String expression, Map<String, Double> attributes) {
		for (Entry<String, Double> attr : attributes.entrySet()) {
			Double amount = attr.getValue();
			expression = expression.replaceAll("\\b" + attr.getKey() + "\\b", String.valueOf(amount / 100));
		}
		return expression;
	}
}
