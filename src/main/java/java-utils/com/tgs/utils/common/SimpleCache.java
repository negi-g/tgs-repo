package com.tgs.utils.common;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple Cache for caching any data (e.g., TGS data-models).
 * <p>
 * This facilitates creating Map between any {@code Class} and {@code List} of
 * another {@code Class}.
 * 
 * @author Abhineet Kumar, Technogram Solutions.
 *
 * @param <K> Type for Key
 * @param <D> Type for Data
 */
abstract public class SimpleCache<K, D> {

	private Map<K, D> cache = new HashMap<>();

	public D getCached(D newValue) {
		K key = getKey(newValue);
		D cached = cache.get(key);
		if (cached != null) {
			return cached;
		}
		cache.put(key, newValue);
		return newValue;
	}

	protected abstract K getKey(D value);
}
