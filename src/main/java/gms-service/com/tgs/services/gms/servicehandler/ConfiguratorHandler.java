package com.tgs.services.gms.servicehandler;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.BankAccountInfo;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.gms.datamodel.ClientBankConfiguration;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.dbmodel.DbConfiguratorRule;
import com.tgs.services.gms.helper.ConfiguratorHelper;
import com.tgs.services.gms.jparepository.configurator.ConfiguratorService;
import com.tgs.services.gms.restmodel.ConfigResponse;
import com.tgs.services.gms.restmodel.ConfiguratorRuleTypeResponse;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ConfiguratorHandler extends ServiceHandler<ConfiguratorInfo, ConfiguratorRuleTypeResponse> {

	@Autowired
	ConfiguratorService configuratorService;

	@Autowired
	SyncService syncService;

	@Override
	public void beforeProcess() throws Exception {
		DbConfiguratorRule rule = null;
		if (Objects.nonNull(request.getId())) {
			rule = configuratorService.findById(request.getId());
		}
		rule = Optional.ofNullable(rule).orElseGet(() -> new DbConfiguratorRule());
		if (rule.getRuleType() != null && request.getRuleType() != null
				&& !rule.getRuleType().equals(request.getRuleType().getName())) {
			throw new CustomGeneralException(SystemError.RULE_TYPE_MISMATCH);
		}
		try {
			rule = rule.from(request);
			rule = configuratorService.save(rule);
			request.setId(rule.getId());
			syncService.sync("gms", rule.toDomain());
		} catch (Exception e) {
			log.error("Unable to save configuration", e);
			throw new Exception("Save or Update failed", e);
		}
	}

	@Override
	public void process() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {
		response.getConfiguratorInfos().add(request);
	}

	public ConfigResponse getConfigurationByType(String type) {
		ConfigResponse ruleTypeResponse = new ConfigResponse();
		ConfiguratorRuleType ruleType = ConfiguratorRuleType.getRuleType(type);
		if (ruleType != null) {
			User loggedInUser = SystemContextHolder.getContextData().getUser();
			GeneralBasicFact fact = GeneralBasicFact.builder().role(loggedInUser.getRole())
					.userId(loggedInUser.getUserId()).partnerId(loggedInUser.getPartnerId()).build();
			IRuleOutPut ruleOutput = ConfiguratorHelper.getConfigRule(ruleType, fact);
			if (ConfiguratorRuleType.CLIENTBANK.equals(ruleType)
					&& !UserRole.getMidOfficeRoles().contains(loggedInUser.getRole())) {
				ruleOutput = getClientBankConfig(ruleType, ruleOutput);
			}
			ruleTypeResponse.setConfig(ruleOutput);
		}
		return ruleTypeResponse;
	}

	public IRuleOutPut getClientBankConfig(ConfiguratorRuleType ruleType, IRuleOutPut ruleOutput) {
		ClientBankConfiguration configuration = (ClientBankConfiguration) ruleOutput;
		List<BankAccountInfo> accountInfo = configuration.getBankList();
		if (CollectionUtils.isNotEmpty(accountInfo)) {
			accountInfo =
					accountInfo.stream().filter(x -> !StringUtils.equalsIgnoreCase(x.getBankName(), "DBS Bank")).collect(Collectors.toList());
		}
		configuration.setBankList(accountInfo);
		return configuration;
	}

}
