package com.tgs.services.gms.helper;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.FetchType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.HolidayConfig;
import com.tgs.services.gms.datamodel.WeekendOffConfig;

@Service
public class HolidayHelper {

    private static final String HOLIDAY_CONFIG = "HOLIDAY_CONFIG";

    public int countHolidays(LocalDateTime from, LocalDateTime to) {
        List<Document> documents = BackendCollectionHelper.getDocuments(CollectionServiceFilter.builder().fetchType(FetchType.CACHE)
                .key(HOLIDAY_CONFIG).build());
        HolidayConfig holidayConfig = GsonUtils.getGson().fromJson(documents.get(0).getData(), HolidayConfig.class);
        int holidayCount = 0;
        LocalDateTime date = from;
        while (!date.isEqual(to)) {
            if (isHoliday(date, holidayConfig)) {
                holidayCount++;
            }
            date = date.plusDays(1);
        }
        return holidayCount;
    }

    public LocalDateTime nextWorkingDay(LocalDateTime date) {
        List<Document> documents = BackendCollectionHelper.getDocuments(CollectionServiceFilter.builder().fetchType(FetchType.CACHE)
                .key(HOLIDAY_CONFIG).build());
        if (CollectionUtils.isEmpty(documents)) return date;
        HolidayConfig holidayConfig = GsonUtils.getGson().fromJson(documents.get(0).getData(), HolidayConfig.class);
        while (isHoliday(date, holidayConfig)) {
            date = date.plusDays(1);
        }
        return date;
    }

    private boolean isHoliday(LocalDateTime date, HolidayConfig holidayConfig) {
        if (holidayConfig.getHolidays().contains(date.toLocalDate())) return true;
        if (date.getDayOfWeek().equals(DayOfWeek.SUNDAY)) return true;
        if (date.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
            return isWeekendOff(date.toLocalDate(), holidayConfig.getSaturday());
        }
        return false;
    }

    private boolean isWeekendOff(LocalDate date, WeekendOffConfig weekendOffConfig) {
        if (weekendOffConfig.equals(WeekendOffConfig.ALL)) return true;
        if (weekendOffConfig.equals(WeekendOffConfig.NONE)) return false;
        LocalDate secondOccurrence = LocalDate.of(date.getYear(), date.getMonth(), 1).with(
                TemporalAdjusters.dayOfWeekInMonth(2, DayOfWeek.SATURDAY));
        LocalDate fourthOccurrence = LocalDate.of(date.getYear(), date.getMonth(), 1).with(
                TemporalAdjusters.dayOfWeekInMonth(4, DayOfWeek.SATURDAY));
        if (weekendOffConfig.equals(WeekendOffConfig.SECOND_OFF) && date.equals(secondOccurrence)) {
            return true;
        }
        if (weekendOffConfig.equals(WeekendOffConfig.ALTERNATE) && (date.equals(secondOccurrence) || date.equals(fourthOccurrence))) {
            return true;
        }
        return false;
    }
}
