package com.tgs.services.gms.restcontroller;

import java.util.Arrays;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.gms.datamodel.ScheduleJob;
import com.tgs.services.gms.jparepository.ScheduleJobService;
import com.tgs.services.gms.restmodel.ScheduleJobResponse;

@RestController
@RequestMapping("/gms/v1/scheduler")
public class SchedulerController {

	@Autowired
	private ScheduleJobService service;


	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected ScheduleJobResponse save(@RequestBody ScheduleJob job) throws Exception {
		job = service.save(job);
		return ScheduleJobResponse.builder().jobs(Arrays.asList(job)).build();
	}


	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected ScheduleJobResponse search(@Valid @RequestBody QueryFilter filter) throws Exception {
		return ScheduleJobResponse.builder().jobs(BaseModel.toDomainList(service.search(filter))).build();
	}

}
