package com.tgs.services.gms.jparepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tgs.services.gms.dbmodel.DbDocument;

public interface CollectionRepository extends JpaRepository<DbDocument, Long>, JpaSpecificationExecutor<DbDocument> {

	public List<DbDocument> findByTypeAndExpiryGreaterThan(String type, LocalDateTime expiry);

	public Optional<DbDocument> findByKey(String key);

	public Optional<DbDocument> findById(Long id);
}
