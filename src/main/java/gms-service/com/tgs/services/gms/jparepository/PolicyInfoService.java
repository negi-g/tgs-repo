package com.tgs.services.gms.jparepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.tgs.services.gms.datamodel.PolicyFilter;
import com.tgs.services.gms.dbmodel.PolicyInfo;
import com.tgs.utils.springframework.data.SpringDataUtils;

@Service
public class PolicyInfoService {

	@Autowired
	PolicyInfoRepository policyInfoRepo;

	public PolicyInfo save(PolicyInfo policyInfo) {
		return policyInfoRepo.save(policyInfo);
	}

	public Optional<PolicyInfo> findById(Integer id) {
		return policyInfoRepo.findById(id);
	}

	public List<PolicyInfo> findByNameContainingAndEnabled(String name, Boolean enabled) {
		return policyInfoRepo.findByNameContainingAndEnabled(name, enabled);
	}

	public List<PolicyInfo> findAll(PolicyFilter filter) {
		PageRequest request = SpringDataUtils.getPageRequestFromFilter(filter);
		List<PolicyInfo> policyList = new ArrayList<>();

		Specification<PolicyInfo> specfication = new Specification<PolicyInfo>() {

			@Override
			public Predicate toPredicate(Root<PolicyInfo> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (filter.getEnabled() != null) {
					predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));

				}
				if (filter.getType() != null) {
					predicates.add(criteriaBuilder.equal(root.get("type"), filter.getType()));

				}
				if (filter.getGroupName() != null) {
					predicates.add(criteriaBuilder.equal(root.get("groupname"), filter.getGroupName()));
				}
				if (filter.getName() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
							"%" + filter.getName().toLowerCase() + "%"));
				}
				SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};

		Page<PolicyInfo> pageList = policyInfoRepo.findAll(specfication, request);

		policyList = pageList.getContent();
		return policyList;
	}
}
