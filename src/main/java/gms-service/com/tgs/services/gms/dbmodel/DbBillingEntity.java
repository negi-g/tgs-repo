package com.tgs.services.gms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.tgs.services.base.datamodel.BillingEntityInfo;
import com.tgs.services.base.runtime.database.CustomTypes.BillingEntityInfoType;
import org.hibernate.annotations.CreationTimestamp;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.dbmodel.BaseModel;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

@Entity
@Table(name="billing_entity")
@Getter
@Setter
@TypeDefs({ @TypeDef(name = "BillingEntityInfoType", typeClass = BillingEntityInfoType.class) })
public class DbBillingEntity extends BaseModel<DbBillingEntity, GstInfo>{
	
	@CreationTimestamp
	private LocalDateTime createdon;
	@Column
	private String gstNumber;
	@Column
	private String email;
	@Column
	private Boolean enabled;
	@Column
	private String mobile;
	@Column
	private String address;	
	@Column
	private String registeredName;
	@Column
	private String userId;
	@Column
	private String pincode;
	@Column
	private String state;
	@Column
	private String cityName;
	@Column
	private String billingCompanyName;
	@Column
	private Boolean isDeleted;

	@Column
	@Type(type = "BillingEntityInfoType")
	private BillingEntityInfo info;

	public BillingEntityInfo getInfo() {
		if (info == null) info = BillingEntityInfo.builder().build();
		return info;
	}

	@Override
	public GstInfo toDomain() {
		GstInfo domain = new GsonMapper<>(this, GstInfo.class).convert();
		domain.setAccountCode(this.getInfo().getAccountCode());
		return domain;
	}
	
	@Override
	public DbBillingEntity from(GstInfo gstInfo) {
		DbBillingEntity db = new GsonMapper<>(gstInfo, this, DbBillingEntity.class).convert();
		db.getInfo().setAccountCode(gstInfo.getAccountCode());
		return db;
	}
	
}
