package com.tgs.services.gms.servicehandler;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.AmendmentCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.gms.jparepository.NoteService;
import com.tgs.services.gms.restmodel.NoteResponse;
import com.tgs.services.messagingService.datamodel.NoteMailAttribute;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;

@Service
public class NoteHandler extends ServiceHandler<NoteFilter, NoteResponse> {

	@Autowired
	NoteService noteService;

	@Autowired
	UserServiceCommunicator userComm;
	
	@Autowired
	protected MsgServiceCommunicator msgSrvCommunicator;
	
	@Autowired
	private AmendmentCommunicator amendmentComm;
	
	@Autowired
	private OrderServiceCommunicator orderComm;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		List<Note> notes = noteService.findAll(request);
		List<String> userIds = notes.stream().map(Note::getUserId).collect(Collectors.toList());
		Map<String, User> userMap =
				userComm.getUsersFromCache(UserCacheFilter.builder().userIds(Lists.newArrayList(userIds)).build());

		for (Note note : notes) {
			if (userMap.get(note.getUserId()) != null)
				note.setUserName(userMap.get(note.getUserId()).getName());
		}
		response.setNotes(notes);
	}
	
	@Override
	public void afterProcess() throws Exception {

	}
	
	
	public NoteResponse saveNotes(Note note) throws Exception {
		User user = SystemContextHolder.getContextData().getUser();
		note.setUserId(UserUtils.getUserId(user));
		if (UserRole.AGENT.equals(user.getRole())) {
			if(NoteType.AGENT_COMMENTS.equals(note.getNoteType())) {
				note.getAdditionalInfo().setVisibleToAgent(true);
			} else {
				throw new CustomGeneralException(SystemError.NOT_ALLOWED);
			}
		}
		NoteResponse noteResponse = new NoteResponse();
		Note savedNotes = noteService.save(note);
		if(!UserRole.AGENT.equals(user.getRole()) && note.getAdditionalInfo() != null && note.getAdditionalInfo().getVisibleToAgent() != null && note.getAdditionalInfo().getVisibleToAgent()) {
			String userId = null;
			if(note.getAmendmentId() == null) {
				Order order = orderComm.findByBookingId(note.getBookingId());
				userId = order.getBookingUserId();
			} else {
				List<Amendment> amd = amendmentComm.fetchAmendments(Arrays.asList(note.getAmendmentId()));
				userId = amd.get(0).getBookingUserId();
			}
			sendMail(note, EmailTemplateKey.BOOKING_NOTE_SHOW_TO_ALL_EMAIL, userId);	
		}
		noteResponse.getOrderNotes().add(savedNotes);
		return noteResponse;
	}
	
	public void sendMail(Note note, EmailTemplateKey templateKey, String userId) {
		AbstractMessageSupplier<NoteMailAttribute> msgAttributes =
				new AbstractMessageSupplier<NoteMailAttribute>() {
					@Override
					public NoteMailAttribute get() {
						NoteMailAttribute mailAttributes = NoteMailAttribute.builder().build();
						mailAttributes.setToEmailUserId(userId);
						mailAttributes.setAmendmentId(note.getAmendmentId());
						mailAttributes.setKey(templateKey.name());
						mailAttributes.setBookingId(note.getBookingId());
						return mailAttributes;
					}
				};
				
		msgSrvCommunicator.sendMail(msgAttributes.getAttributes());
		
	}

}
