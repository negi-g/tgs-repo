package com.tgs.services.gms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.gson.JsonStringSerializer;
import com.tgs.services.gms.datamodel.ScheduleJob;
import com.tgs.services.gms.datamodel.ScheduleJobStatus;
import com.tgs.services.gms.datamodel.ScheduleJobType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@Audited
@Table(name = "schedulejob")
@ToString
public class DBScheduleJob extends BaseModel<DBScheduleJob, ScheduleJob> {


	@Column
	private String name;

	@Column(length = 10000)
	@JsonAdapter(JsonStringSerializer.class)
	private String data;

	@Column
	@CreationTimestamp
	private LocalDateTime createdon;

	@Column
	private ScheduleJobStatus status;

	@Column
	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private ScheduleJobType type;

	@Column
	private String userId;

	@Column
	private String reportlink;

	@Column
	private String comments;

	public static DBScheduleJob create(ScheduleJob schedulejob) {
		return new DBScheduleJob().from(schedulejob);
	}

	@Override
	public ScheduleJob toDomain() {
		return new GsonMapper<>(this, ScheduleJob.class).convert();
	}

	public ScheduleJob toDomain(ScheduleJob schedulejob) {
		return new GsonMapper<>(this, schedulejob, ScheduleJob.class).convert();
	}

	@Override
	public DBScheduleJob from(ScheduleJob schedulejob) {
		return new GsonMapper<>(schedulejob, this, DBScheduleJob.class).convert();
	}
}
