package com.tgs.services.gms.servicehandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.configurationmodel.WhatsAppConfiguration;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.WhatsAppConsentInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.helper.ConfiguratorHelper;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class WhatsAppConsentHandler extends ServiceHandler<WhatsAppConsentInfo, BaseResponse> {

	@Override
	public void beforeProcess() throws Exception {
	}

	@Override
	public void process() throws Exception {
		try {
			List<String> recipientNumbers = new ArrayList<>();
			request.getRecipientNumbers().forEach(number -> {
				recipientNumbers.add(checkValidNumber(number));
			});
			WhatsAppConfiguration conf = (WhatsAppConfiguration) ConfiguratorHelper.getConfigRule(ConfiguratorRuleType.WHATSAPPCONFIG, null);
			if ("optin".equals(request.getType()) || "optout".equals(request.getType()))
				setUserWhatsAppConsent(recipientNumbers, conf, request.getType());
			else
				throw new CustomGeneralException(SystemError.INVALID_CONSENT_TYPE);
		} catch (Exception e) {
			log.error("unable to take consent of mobile number {} due to {}", request.getRecipientNumbers().toString(),
					e.getMessage());
			throw e;
		}
	}
	
	private String checkValidNumber(String number) {
		if (number.contains("-") || number.contains("+")) {
		    return number.replaceAll("[-+]", "");
		} else {
			return StringUtils.join("91", number);
		}
	}

	public void setUserWhatsAppConsent(List<String> recepientNumbers, WhatsAppConfiguration conf, String type) {
		Map<String, String> headerParams = setHeaders(conf);
		recepientNumbers.forEach(number -> {
			String errMsg = null;
			try {
				HttpUtils httpUtils = HttpUtils.builder().urlString(conf.getUrl().concat("consent/manage"))
						.headerParams(headerParams).postData(getConsentDetails(number, type, conf.getSourceName()))
						.build();

				Map<String, Object> responseObj = (Map<String, Object>) httpUtils.getResponse(Map.class)
						.orElse(new HashMap<>());
				if (!isNumberConsent(responseObj)) {
					Map<String, Object> failureReason = (Map<String, Object>) responseObj.get("error");
					errMsg = failureReason.get("message").toString();
				}
			} catch (IOException e) {
				errMsg = e.getMessage();
			}

			if (errMsg != null) {
				log.error("[WhatsApp] Unable to {} for number {} due to {}", type, number, errMsg);
				throw new CustomGeneralException(SystemError.WHATS_APP_CONSENT_FAILED);
			} else {
				log.info("[WhatsApp] successfully to {} for number: {} ", type, number);
			}
		});
	}

	private String getConsentDetails(String recepientMobile, String type, String sourceName) {
		Map<String, Object> params = new HashMap<>();
		Map<String, Object> recipientData = new HashMap<>();
		recipientData.put("recipient", recepientMobile);
		recipientData.put("source", sourceName);
		params.put("type", type);
		params.put("recipients", Lists.newArrayList(recipientData));
		return GsonUtils.getGson().toJson(params);
	}

	@SuppressWarnings("unchecked")
	private boolean isNumberConsent(Map<String, Object> responseObj) {
		if ("success".equals(responseObj.get("status"))) {
			return true;
		}
		return false;
	}

	@Override
	public void afterProcess() throws Exception {
	}

	public BaseResponse getNumberStatus(WhatsAppConsentInfo consentRequest) {
		BaseResponse response = new BaseResponse();
		WhatsAppConfiguration conf = (WhatsAppConfiguration) ConfiguratorHelper.getConfigRule(ConfiguratorRuleType.WHATSAPPCONFIG, null);
		try {
			List<Object> responseData = getConsentStatusOfNumbers(response, consentRequest.getRecipientNumbers(), conf);
			Map<String, Object> params = new HashMap<>();
			params.put("consentStatusDetails", responseData);
			response.setMetaInfo(params);
			return response;
		} catch (Exception e) {
			log.error("unable to get status of mobile number {} due to {}", consentRequest.getRecipientNumbers().toString(),
					e.getMessage());
			response.addError(new ErrorDetail(SystemError.UNABLE_TO_GET_STATUS));
			return response;
		}
	}

	private List<Object> getConsentStatusOfNumbers(BaseResponse response, List<String> recipientNumbers,
			WhatsAppConfiguration conf) {
		List<Object> responseList = new ArrayList<Object>();
		recipientNumbers.forEach(num -> {
			Map<String, String> headerParams = setHeaders(conf);
			Map<String, Boolean> params = new HashMap<>();
			String number = checkValidNumber(num);
			try {
				HttpUtils httpUtils = HttpUtils.builder().urlString(conf.getUrl().concat("consent/"))
						.headerParams(headerParams).postData(getConsent(number)).build();
				Map<String, Object> responseObj = (Map<String, Object>) httpUtils.getResponse(Map.class)
						.orElse(new HashMap<>());
				if ("success".equals(responseObj.get("status"))) {
					String responseData = GsonUtils.getGson().toJson(responseObj.get("data"));
					Map<String, Object> responseMap = GsonUtils.getGson().fromJson(responseData,
							new TypeToken<Map<String, Object>>() {
							}.getType());
					params.put(num, (Boolean) responseMap.get("wa_optin"));
				} else {
					Map<String, Object> failureReason = (Map<String, Object>) responseObj.get("error");
					params.put(num, false);
				}
			} catch (Exception e) {
				params.put(num, false);
			}
			responseList.add(params);
		});
		return responseList;
	}

	private Map<String, String> setHeaders(WhatsAppConfiguration conf) {
		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("Content-Type", "application/json");
		headerParams.put("Authorization", conf.getApiKey());
		return headerParams;
	}

	private String getConsent(String recepientMobile) {
		Map<String, Object> optInData = new HashMap<>();
		optInData.put("phone_number", recepientMobile);
		return GsonUtils.getGson().toJson(optInData);
	}

}
