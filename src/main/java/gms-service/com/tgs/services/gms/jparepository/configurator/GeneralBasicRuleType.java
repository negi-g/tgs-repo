package com.tgs.services.gms.jparepository.configurator;

import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;

public class GeneralBasicRuleType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return GeneralBasicRuleCriteria.class;
    }
}
