package com.tgs.services.gms.jparepository;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.ScheduleJobFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.gms.datamodel.ScheduleJob;
import com.tgs.services.gms.dbmodel.DBScheduleJob;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ScheduleJobService extends SearchService<DBScheduleJob> {

	@Autowired
	private ScheduleJobEntityRepository repo;


	public ScheduleJob save(ScheduleJob job) {
		DBScheduleJob existing = new DBScheduleJob();
		if (job.getId() != null) {
			List<DBScheduleJob> jobList = super.search(ScheduleJobFilter.builder().id(job.getId()).build(), repo);
			if (CollectionUtils.isEmpty(jobList))
				throw new CustomGeneralException(SystemError.INCORRECT_ID);
			existing = jobList.get(0);
		}
		existing = existing.from(job);
		log.error("Inserting job {}", existing);
		job = repo.saveAndFlush(existing).toDomain();
		log.error("After insertion job {}", existing);
		return job;
	}


	@Override
	public List<DBScheduleJob> search(QueryFilter queryFilter) {
		return super.search(queryFilter, repo);
	}
}
