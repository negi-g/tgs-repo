package com.tgs.services.gms.servicehandler;

import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.gms.base.ITokenStore;
import com.tgs.services.gms.dbmodel.OtpToken;
import com.tgs.services.gms.manager.OtpServiceManager;
import com.tgs.services.base.gms.OtpValidateRequest;

@Service
public class OtpValidateHandler extends ServiceHandler<OtpValidateRequest, BaseResponse> {

	@Autowired
	ITokenStore tokenstore;

	@Autowired
	OtpServiceManager otpManager;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		otpManager.validateOtp(request.getRequestId(), request.getOtp(), request.getMaxAttemptCount(),
				request.getMaxAttemptCount());
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public void updateConsumedOtp(String requestId) {
		OtpToken otpToken = tokenstore.findTokenByRequestId(requestId);
		otpToken.setConsumed(true);
		tokenstore.updateOtp(otpToken);
	}

}
