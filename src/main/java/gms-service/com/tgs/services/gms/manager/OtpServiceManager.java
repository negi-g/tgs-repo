package com.tgs.services.gms.manager;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.base.ITokenStore;
import com.tgs.services.gms.dbmodel.OtpToken;
import com.tgs.services.ums.datamodel.User;

@Service
public class OtpServiceManager {

	@Autowired
	ITokenStore tokenstore;

	@Autowired
	UserServiceCommunicator userService;

	public void validateOtp(String requestId, String otp, Integer maxAttemptCount, Integer expiryTimeInMinutes)
			throws CustomGeneralException {
		OtpToken otpToken = tokenstore.findTokenByRequestId(requestId);
		if (otpToken == null) {
			throw new CustomGeneralException(SystemError.INVALID_REQUEST_ID);
		}
		try {
			if (!otpToken.getOtp().equals(otp)) {
				throw new CustomGeneralException(SystemError.INVALID_OTP);
			}

			if (otpToken.isConsumed()) {
				throw new CustomGeneralException(SystemError.OTP_ALREADY_USED);
			}

			if (otpToken.getAttemptCount() >= ObjectUtils.firstNonNull(maxAttemptCount, 3)) {
				throw new CustomGeneralException(SystemError.OTP_LIMIT_EXCEEDED);
			}

			if (otpToken.getCreatedOn()
					.isBefore(LocalDateTime.now().minusMinutes(ObjectUtils.firstNonNull(expiryTimeInMinutes, 30)))) {
				throw new CustomGeneralException(SystemError.OTP_EXPIRED);
			}

			otpToken.setConsumed(true);
		} catch (CustomGeneralException e) {
			otpToken.setAttemptCount(otpToken.getAttemptCount() + 1);
			throw e;
		} finally {
			tokenstore.updateOtp(otpToken);
		}
	}

	public User fetchUserForSendingOtp(com.tgs.services.gms.datamodel.OtpToken request) {
		User user = null;
		String partnerId = SystemContextHolder.getContextData().getHttpHeaders().getPartnerId();
		boolean isB2C = request.getRole() != null && UserRole.CUSTOMER.equals(request.getRole());
		UserFilter emailFilter =
				UserFilter.builder().email(request.getKey()).partnerIdIn(Arrays.asList(partnerId)).build();
		UserFilter mobileFilter =
				UserFilter.builder().mobile(request.getKey()).partnerIdIn(Arrays.asList(partnerId)).build();
		List<UserRole> customerRoles = Arrays.asList(UserRole.CUSTOMER);
		if (isB2C) {
			emailFilter.setRoles(customerRoles);
			mobileFilter.setRoles(customerRoles);
		} else {
			emailFilter.setRoleNotIn(customerRoles);
			mobileFilter.setRoleNotIn(customerRoles);
		}
		List<User> users =
				ObjectUtils.firstNonNull(userService.getUsers(emailFilter), userService.getUsers(mobileFilter));
		if (CollectionUtils.isNotEmpty(users)) {
			if (users.size() > 1)
				throw new CustomGeneralException(SystemError.DUPLICATE_USER);
			user = users.get(0);
		}
		// Allow WL Partner to perform all operations on his own domain also.
		if (Objects.isNull(user) && !UserUtils.DEFAULT_PARTNERID.equals(partnerId)) {
			User partnerUser = userService.getUserFromCache(partnerId);
			if (partnerUser.getEmail().equals(request.getKey()) || partnerUser.getMobile().equals(request.getKey())) {
				user = partnerUser;
			}
		}
		return user;
	}
}
