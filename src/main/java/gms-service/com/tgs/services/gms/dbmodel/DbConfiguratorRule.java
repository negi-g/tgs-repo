package com.tgs.services.gms.dbmodel;

import java.time.LocalDateTime;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.envers.Audited;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;

import lombok.Getter;
import lombok.Setter;

@Audited
@Getter
@Setter
@Entity
@Table(name="configuratorrule")
public class DbConfiguratorRule extends BaseModel<DbConfiguratorRule, ConfiguratorInfo> {

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;
	
	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private boolean enabled;

	@Column
	@Nonnull
	private String ruleType;

	@Column
	@JsonAdapter(JsonStringSerializer.class)
	private String inclusionCriteria;

	@Column
	@JsonAdapter(JsonStringSerializer.class)
	private String exclusionCriteria;

	@Column
	@Nonnull
	@JsonAdapter(JsonStringSerializer.class)
	private String output;

	@Column
	private double priority;

	@Column
	private boolean exitOnMatch;

	@Column
	private boolean isDeleted;
	
	@Override
	public ConfiguratorInfo toDomain() {
		return new GsonMapper<>(this, ConfiguratorInfo.class).convert();
	}
	
	@Override
	public DbConfiguratorRule from(ConfiguratorInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbConfiguratorRule.class).convert();
	}
}
