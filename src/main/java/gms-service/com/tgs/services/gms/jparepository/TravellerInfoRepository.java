package com.tgs.services.gms.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tgs.services.gms.dbmodel.DbTravellerInfo;

@Repository
public interface TravellerInfoRepository
		extends JpaRepository<DbTravellerInfo, Long>, JpaSpecificationExecutor<DbTravellerInfo> {

	public List<DbTravellerInfo> findByUserIdAndEmail(String userId, String email);

	public DbTravellerInfo findByUserIdAndNameAndEmail(String userId, String travellerName, String email);

	@Query(value = "SELECT a.* FROM travellerinfo a WHERE a.name ilike %:name% and a.userid=:userid  and a.travellerdata->>'pt'=:paxtype", nativeQuery = true)
	public List<DbTravellerInfo> findByNameAndUserIdAndPaxType(@Param("name") String name, @Param("userid") String userid,
			@Param("paxtype") String paxType);

}
