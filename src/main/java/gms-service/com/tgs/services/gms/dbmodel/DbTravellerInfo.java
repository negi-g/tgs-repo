package com.tgs.services.gms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.gms.hibernate.TravellerDataType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Table(name="travellerinfo")
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TypeDef(name = "TravellerDataType", typeClass =TravellerDataType.class)
public class DbTravellerInfo extends BaseModel<DbTravellerInfo, TravellerInfo>{
	
	
	@CreationTimestamp
	private LocalDateTime createdon;
	@Column
	private String userId;
	@Column
	private Boolean enabled;
	@Column
	private String name;
	@Column
	private String email;
	
	@Column
	@Type(type = "TravellerDataType")
	private TravellerInfo travellerData;
}
