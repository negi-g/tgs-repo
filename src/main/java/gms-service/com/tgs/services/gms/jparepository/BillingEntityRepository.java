package com.tgs.services.gms.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tgs.services.gms.dbmodel.DbBillingEntity;

public interface BillingEntityRepository extends JpaRepository<DbBillingEntity, Long>, JpaSpecificationExecutor<DbBillingEntity> {
	
	public List<DbBillingEntity> findByUserId(String userId);

	public DbBillingEntity findByUserIdAndGstNumberAndBillingCompanyName(String userId, String gstNumber, String billingCompanyName);
}
