package com.tgs.services.gms.jparepository;

import java.util.Arrays;
import java.util.List;
import com.google.gson.Gson;
import com.tgs.filters.GstInfoFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.gms.dbmodel.DbBillingEntity;
import com.tgs.services.ums.datamodel.User;

@Slf4j
@Service
public class BillingEntityService extends SearchService<DbBillingEntity> {

	@Autowired
	private BillingEntityRepository repo;

	public List<GstInfo> findByUserId(String userId) {
		return BaseModel.toDomainList(search(GstInfoFilter.builder().userIdIn(Arrays.asList(userId)).build()));
	}

	public DbBillingEntity findByUserIdAndGstNumberAndBillingCompanyName(String userId, String gstNumber,
			String billingCompanyName) {
		return repo.findByUserIdAndGstNumberAndBillingCompanyName(userId, gstNumber, billingCompanyName);
	}

	public GstInfo save(GstInfo billingEntity) {
		DbBillingEntity db = new DbBillingEntity().from(billingEntity);
		if (StringUtils.isEmpty(db.getUserId())) {
			db.setUserId(SystemContextHolder.getContextData().getUser().getUserId());
		}
		if (db.getIsDeleted() == null) {
			db.setIsDeleted(false);

		}
		if (StringUtils.isBlank(db.getBillingCompanyName())) {
			db.setBillingCompanyName(StringUtils.EMPTY);
		}
		DbBillingEntity existing = findByUserIdAndGstNumberAndBillingCompanyName(db.getUserId(), db.getGstNumber(),
				db.getBillingCompanyName());
		if (existing != null && !existing.getId().equals(db.getId())) {
			throw new CustomGeneralException(SystemError.GST_NUMBER_EXISTS);
		}
		return repo.saveAndFlush(db).toDomain();
	}


	public List<DbBillingEntity> search(QueryFilter queryFilter) {

		if (BooleanUtils.isNotTrue(((GstInfoFilter) queryFilter).getIsDeleted())) {
			((GstInfoFilter) queryFilter).isDeleted = false;
		}
		List<String> userIds = ((GstInfoFilter) queryFilter).userIdIn;
		if (CollectionUtils.isEmpty(userIds))
			throw new CustomGeneralException(SystemError.BAD_REQUEST, "userIds may not be empty");
		return super.search(queryFilter, repo);
	}

	public List<GstInfo> searchGst(GstInfoFilter filter) {
		List<DbBillingEntity> result = search(filter);
		return BaseModel.toDomainList(result);
	}

	public void delete(String userId, Long id) {
		log.info("Billing Entity Delete. UserId {}, id {}", userId, id);
		DbBillingEntity db = repo.findOne(id);
		log.info("Billing Entity Delete. db => {}", new Gson().toJson(db));
		if (!db.getUserId().equals(userId)) {
			throw new CustomGeneralException(SystemError.BAD_REQUEST,
					"Billing entity id does not belong to passed userId");
		}
		if (db != null) {
			db.setIsDeleted(true);
			repo.save(db);
		}
	}

	private boolean isConstraintViolation(Throwable ex) {
		int depth = 3;
		while (depth > 0) {
			if (ex instanceof ConstraintViolationException) {
				return true;
			}
			if (ex.getCause() == null)
				return false;
			ex = ex.getCause();
			depth--;
		}
		return false;
	}

	public List<DbBillingEntity> listGSTInfo(GstInfoFilter filter) {
		User user = SystemContextHolder.getContextData().getUser();
		if (!UserUtils.isMidOfficeRole(user.getRole()) && CollectionUtils.isEmpty(filter.getUserIdIn())) {
			filter.setUserIdIn(Arrays.asList(user.getParentUserId()));
		}
		return super.search(filter, repo);
	}

}
