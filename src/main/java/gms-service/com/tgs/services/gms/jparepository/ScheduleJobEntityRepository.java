package com.tgs.services.gms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.tgs.services.gms.dbmodel.DBScheduleJob;

@Repository
public interface ScheduleJobEntityRepository
		extends JpaRepository<DBScheduleJob, Long>, JpaSpecificationExecutor<DBScheduleJob> {

}
