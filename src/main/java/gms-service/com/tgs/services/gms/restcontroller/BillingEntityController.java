package com.tgs.services.gms.restcontroller;

import com.tgs.filters.GstInfoFilter;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.jparepository.BillingEntityService;
import com.tgs.services.gms.restmodel.BillingEntityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/gms/v1/billing-entity")
public class BillingEntityController {

    @Autowired
    private BillingEntityService service;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    protected BillingEntityResponse add(@Valid @RequestBody GstInfo billingEntity) throws Exception {
        return new BillingEntityResponse(service.save(billingEntity));
    }

    @RequestMapping(value = "/get/{userId}", method = RequestMethod.GET)
    protected BillingEntityResponse get(@PathVariable("userId") String userId) throws Exception {
        return new BillingEntityResponse(service.findByUserId(userId));
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    protected BillingEntityResponse search(@Valid @RequestBody GstInfoFilter filter) throws Exception {
        return new BillingEntityResponse(BaseModel.toDomainList(service.search(filter)));
    }

    @RequestMapping(value = "/delete/{userId}/{id}", method = RequestMethod.DELETE)
    protected BaseResponse delete(@PathVariable Long id, @PathVariable String userId) throws Exception {
        service.delete(userId, id);
        return new BaseResponse();
    }
    
    @RequestMapping(value = "/list-all", method = RequestMethod.POST)
    protected BillingEntityResponse listAll(@Valid @RequestBody GstInfoFilter filter) throws Exception {
        return new BillingEntityResponse(BaseModel.toDomainList(service.listGSTInfo(filter)));
    }

}
