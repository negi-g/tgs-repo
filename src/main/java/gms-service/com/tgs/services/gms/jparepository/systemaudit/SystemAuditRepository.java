package com.tgs.services.gms.jparepository.systemaudit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.gms.dbmodel.DbSystemAudit;


@Repository
public interface SystemAuditRepository extends JpaRepository<DbSystemAudit, Long>, JpaSpecificationExecutor<DbSystemAudit> {
}