package com.tgs.services.gms.jparepository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.dbmodel.DbNote;

@Service
public class NoteService extends SearchService<DbNote> {

	@Autowired
	private NoteRepository noteRepository;

	public Note save(Note noteDataModel) {
		noteDataModel.setNoteMessage(noteDataModel.getNoteMessage().replace("\n", " "));
		DbNote noteDbModel = null;
		if (Objects.nonNull(noteDataModel.getId())) {
			noteDbModel = findById(noteDataModel.getId());
		}
		noteDbModel = Optional.ofNullable(noteDbModel).orElseGet(() -> new DbNote()).from(noteDataModel);
		return noteRepository.save(noteDbModel).toDomain();
	}

	public List<Note> findAll(NoteFilter noteFilter) {
		return BaseModel.toDomainList(super.search(noteFilter, noteRepository));
	}

	public DbNote findById(Long id) {
		return noteRepository.findById(id);
	}
}
