package com.tgs.services.gms.jparepository.configurator;

import com.tgs.services.gms.datamodel.configurator.ConfiguratorFilter;
import com.tgs.services.gms.dbmodel.DbConfiguratorRule;
import com.tgs.utils.springframework.data.SpringDataUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public enum ConfiguratorSearchPredicate {

    CREATED_ON {
        @Override
        public void addPredicate(Root<DbConfiguratorRule> root, CriteriaQuery<?> query,
                                 CriteriaBuilder criteriaBuilder, ConfiguratorFilter filter, List<Predicate> predicates) {
            SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
        }
    },
    ENABLED {
        @Override
        public void addPredicate(Root<DbConfiguratorRule> root, CriteriaQuery<?> query,
                                 CriteriaBuilder criteriaBuilder, ConfiguratorFilter filter, List<Predicate> predicates) {
            if (Objects.nonNull(filter.getEnabled()))
                predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));
        }
    },
    ISDELETED {
        @Override
        public void addPredicate(Root<DbConfiguratorRule> root, CriteriaQuery<?> query,
                                 CriteriaBuilder criteriaBuilder, ConfiguratorFilter filter, List<Predicate> predicates) {
			predicates.add(criteriaBuilder.equal(root.get("isDeleted"), filter.isDeleted()));
        }
    },
    RULETYPE {
        @Override
        public void addPredicate(Root<DbConfiguratorRule> root, CriteriaQuery<?> query,
                                 CriteriaBuilder criteriaBuilder, ConfiguratorFilter filter, List<Predicate> predicates) {
            if (Objects.nonNull(filter.getRuleType()) && StringUtils.isNotBlank(filter.getRuleType()))
                predicates.add(criteriaBuilder.equal(root.get("ruleType"), filter.getRuleType()));
        }
    };

    public abstract void addPredicate(Root<DbConfiguratorRule> root, CriteriaQuery<?> query,
                                      CriteriaBuilder criteriaBuilder, ConfiguratorFilter filter, List<Predicate> predicates);

    public static List<Predicate> getPredicateListBasedOnConfigFilter(Root<DbConfiguratorRule> root,
                                                                      CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, ConfiguratorFilter filter) {
        List<Predicate> predicates = new ArrayList<>();
        for (ConfiguratorSearchPredicate configuratorFilter : ConfiguratorSearchPredicate.values()) {
            configuratorFilter.addPredicate(root, query, criteriaBuilder, filter, predicates);
        }
        return predicates;
    }
}
