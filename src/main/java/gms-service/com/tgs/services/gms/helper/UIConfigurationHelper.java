package com.tgs.services.gms.helper;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.helper.InitializerGroup;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.UIConfiguration;

@Service
@InitializerGroup(group = InitializerGroup.Group.GENERAL)
public class UIConfigurationHelper extends InMemoryInitializer {
	Map<String, String> inMemoryMap = new HashMap<>();

	public static final String KEY = "ui_conf";

	@Autowired
	GeneralCachingCommunicator cachingComm;

	@Autowired
	SyncService syncService;

	public UIConfigurationHelper(CustomInMemoryHashMap configurationHashMap) {
		super(configurationHashMap);
	}


	@Override
	@Scheduled(fixedDelay = 60 * 1000, initialDelay = 5 * 1000)
	public void process() {
		CacheMetaInfo cacheMetaInfo = CacheMetaInfo.builder().set(CacheSetName.UICONF.name()).key(KEY).build();
		String value = cachingComm.getBinValue(cacheMetaInfo, String.class, false, true, BinName.UICONF.name());
		if (value == null)
			value = "EMPTY";
		inMemoryMap.put(KEY, value);
	}

	public void updateInfo(UIConfiguration uiConfiguration) {
		String value = new Gson().toJson(uiConfiguration);
		CacheMetaInfo cacheMetaInfo = CacheMetaInfo.builder().set(CacheSetName.UICONF.name()).key(KEY).build();
		Map<String, String> temp = new HashMap<>();
		temp.put(BinName.UICONF.name(), value);
		cachingComm.store(cacheMetaInfo, temp, false, true, -1);
		process();
		syncService.sync("gms", uiConfiguration);
	}


	public String getUIConf() {
		if (inMemoryMap.isEmpty())
			process();
		return inMemoryMap.get(KEY);
	}

	@Override
	public void deleteExistingInitializer() {}

}
