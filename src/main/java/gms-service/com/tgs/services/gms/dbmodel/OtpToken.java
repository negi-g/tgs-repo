package com.tgs.services.gms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "requestId" }) })
@Getter
@Setter
@Data
public class OtpToken {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;

	@Column
	private String requestId;

	@Column
	private String key;
	@Column
	private String type;

	@CreationTimestamp
	@Column(name = "created_on")
	private LocalDateTime createdOn;

	@Column
	private String mobile;

	@Column
	private String email;

	@Column
	private Boolean smsreq;

	@Column
	private Boolean emailreq;

	@Column
	private String otp;

	@Column
	private int attemptCount;
	
	@Column
	private boolean isConsumed;

}
