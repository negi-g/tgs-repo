package com.tgs.services.gms.jparepository.configurator;

import com.tgs.services.gms.dbmodel.DbConfiguratorRule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConfiguratorRepository extends JpaRepository<DbConfiguratorRule, Long>, JpaSpecificationExecutor<DbConfiguratorRule> {

    List<DbConfiguratorRule> findByRuleType(String ruleType);
}
