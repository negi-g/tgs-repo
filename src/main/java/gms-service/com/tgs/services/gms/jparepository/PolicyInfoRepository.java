package com.tgs.services.gms.jparepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tgs.services.gms.dbmodel.PolicyInfo;

public interface PolicyInfoRepository extends JpaRepository<PolicyInfo, Integer>, JpaSpecificationExecutor<PolicyInfo> {

	public List<PolicyInfo> findByNameContainingAndEnabled(String name, boolean enabled);

	public Optional<PolicyInfo> findById(Integer id);
}
