package com.tgs.services.gms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.gms.datamodel.NoteAdditionalInfo;

public class NoteAdditionalInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return NoteAdditionalInfo.class;
	}
}
