package com.tgs.services.gms.base;

import java.time.LocalDateTime;
import com.tgs.services.gms.dbmodel.OtpToken;

public interface ITokenStore {

	public OtpToken generateToken(OtpToken token);

	public OtpToken validateToken(String requestId, String otp, LocalDateTime time);
	
	public OtpToken updateOtp(OtpToken otpToken);

	public OtpToken findTokenByRequestId(String requestId);

	public OtpToken getUnUtilizedOtpForUser(String email, String mobile, LocalDateTime time);
}
