package com.tgs.services.voucher.jparepository;


import com.tgs.services.voucher.dbmodel.DbVoucherConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VoucherConfigurationRepository
		extends JpaRepository<DbVoucherConfiguration, Long>, JpaSpecificationExecutor<DbVoucherConfiguration> {

}
