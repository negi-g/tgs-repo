package com.tgs.services.ss.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.ss.restmodel.TgsInternalLoginRequest;
import com.tgs.services.ss.servicehandler.TgsInternalLoginHandler;
import com.tgs.services.ums.restmodel.SignInResponse;

@RestController
@RequestMapping("/internal/v1")
public class TgsInternalController {

	@Autowired
	private TgsInternalLoginHandler loginHandler;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	protected SignInResponse login(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody TgsInternalLoginRequest loginRequest) throws Exception {
		loginHandler.initData(loginRequest, new SignInResponse());
		return loginHandler.getResponse();
	}

}
