alter table inventoryorders drop column if exists additionalinfo, add column additionalinfo jsonb;
alter table inventoryorders drop column if exists product, add column product varchar(50);
alter table inventoryorders drop column if exists flightNumber, add column flightNumber varchar(50);

alter table inventoryorders_aud drop column if exists additionalinfo, add column additionalinfo jsonb;
alter table inventoryorders_aud drop column if exists product, add column product varchar(50);
alter table inventoryorders_aud drop column if exists flightNumber, add column flightNumber varchar(50);

update inventoryorders set product = 'A';
update inventoryorders_aud set product = 'A';

update inventoryorders as i set flightNumber = a.flightNumber from airorderitem a where i.referenceid = a.bookingid::varchar
and a.status = 'S' and a.additionalinfo->'scid' ::text = '11';
update inventoryorders_aud as i set flightNumber = a.flightNumber from airorderitem a where i.referenceid = a.bookingid::varchar
and a.status = 'S' and a.additionalinfo->'scid' ::text = '11';

update inventoryorders set additionalinfo = json_build_object('source', source, 'dest', dest,'airline', airline, 'pnr', pnr,'flightNumber',flightNumber);
update inventoryorders_aud set additionalinfo = json_build_object('source', source, 'dest', dest,'airline', airline, 'pnr', pnr,'flightNumber',flightNumber);


alter table inventoryorders drop column if exists flightNumber;
alter table inventoryorders drop column if exists source;
alter table inventoryorders drop column if exists dest;
alter table inventoryorders drop column if exists airline;
alter table inventoryorders drop column if exists pnr;

alter table inventoryorders_aud drop column if exists flightNumber;
alter table inventoryorders_aud drop column if exists source;
alter table inventoryorders_aud drop column if exists dest;
alter table inventoryorders_aud drop column if exists airline;
alter table inventoryorders_aud drop column if exists pnr;


