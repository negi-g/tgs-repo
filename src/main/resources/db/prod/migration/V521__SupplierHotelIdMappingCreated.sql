CREATE TABLE if not exists supplierhotelidmapping (
	id serial,
	hotelid VARCHAR ( 50 ),
	supplierhotelid VARCHAR ( 50 ),
	suppliername VARCHAR ( 50 ),
	additionalInfo jsonb NULL,
	PRIMARY KEY (id),
	UNIQUE(hotelid,suppliername)
);