alter table hotelinfo drop constraint if exists hotelinfo_name_key;

alter table hotelinfo add constraint hotelinfo_name_rating_key unique (name, rating);

CREATE INDEX hotelinfo_rating_idx ON hotelinfo(rating);