CREATE OR REPLACE FUNCTION create_partition_orders_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
      partition_date TEXT;
      partition TEXT;
    BEGIN
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := 'orders_aud' || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS (orders_aud);';
      
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || 'orders_aud' || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE TRIGGER orders_aud_insert_trigger
BEFORE INSERT ON orders_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_orders_aud_insert();



CREATE OR REPLACE FUNCTION create_partition_airorders_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
      partition_date TEXT;
      partition TEXT;
    BEGIN
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := 'airorderitem_aud' || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS (airorderitem_aud);';
      
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || 'airorderitem_aud' || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE TRIGGER airorders_aud_insert_trigger
BEFORE INSERT ON airorderitem_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_airorders_aud_insert();


CREATE OR REPLACE FUNCTION create_partition_payment_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
      partition_date TEXT;
      partition TEXT;
    BEGIN
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := 'payment_aud' || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS (payment_aud);';
      
      END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || 'payment_aud' || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE TRIGGER payment_aud_insert_trigger
BEFORE INSERT ON payment_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_payment_aud_insert();


