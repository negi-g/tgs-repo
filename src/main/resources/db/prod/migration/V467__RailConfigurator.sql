alter table railstationinfo drop column if exists country;
alter table railstationinfo drop column if exists priority;

drop table if exists railconfiguratorrule;
drop table if exists railconfiguratorrule_aud;

CREATE TABLE railconfiguratorrule (
id bigserial NOT NULL,
createdon timestamp DEFAULT now(),
enabled bool DEFAULT true,
ruletype varchar(255) NOT NULL,
"output" varchar(50000),
priority float8,
exitonmatch bool DEFAULT false,
inclusioncriteria jsonb,
exclusioncriteria jsonb,
isdeleted bool DEFAULT false,
description varchar(5000),
processedon timestamp,
CONSTRAINT railconfiguratorrule_pkey PRIMARY KEY (id)
);


CREATE TABLE railconfiguratorrule_aud (
id bigserial NOT NULL,
rev bigint,
revtype smallint,
createdon timestamp,
enabled bool,
ruletype varchar(255),
"output" varchar(50000),
priority float8,
exitonmatch bool,
inclusioncriteria jsonb ,
exclusioncriteria jsonb ,
isdeleted bool,
description varchar(5000),
processedon timestamp,
primary key (id, REV)
);