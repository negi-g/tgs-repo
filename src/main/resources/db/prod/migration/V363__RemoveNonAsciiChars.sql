UPDATE users set userid = regexp_replace(userid, '[^[:ascii:]]', '', 'g') WHERE userid SIMILAR TO '%[^[:ascii:]]%'; 

UPDATE users set additionalinfo = regexp_replace(additionalinfo::text, '[^[:ascii:]]', '', 'g')::jsonb  WHERE additionalinfo::text SIMILAR TO '%[^[:ascii:]]%' 