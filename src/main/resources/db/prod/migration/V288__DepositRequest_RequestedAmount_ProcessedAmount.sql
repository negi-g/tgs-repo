UPDATE users SET additionalinfo = additionalinfo - 'gp' WHERE additionalinfo#>'{gp}' IS NOT NULL;

ALTER TABLE depositrequest RENAME COLUMN amount TO requestedamount;
ALTER TABLE depositrequest_aud RENAME COLUMN amount TO requestedamount;