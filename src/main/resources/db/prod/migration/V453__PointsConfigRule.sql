DROP TABLE IF EXISTS pointsconfigrule;

DROP TABLE IF EXISTS pointsconfigrule_aud;

CREATE TABLE pointsconfigrule (
 	id bigserial not null,
 	product varchar(50),
 	type varchar(50),
 	operationType varchar(50),
 	inclusioncriteria varchar(500),
 	exclusioncriteria varchar(500),
 	outputCriteria varchar(500),
 	enabled boolean DEFAULT true,
 	isDeleted boolean DEFAULT false,
 	priority float8,
 	createdon timestamp DEFAULT now(),
 	processedon timestamp DEFAULT now(),
 	PRIMARY KEY (id)
);

CREATE TABLE pointsconfigrule_aud (
 	id int4 not null,
 	REV int8 not null,
 	REVTYPE int2,
 	product varchar(50),
 	type varchar(50),
 	operationType varchar(50),
 	inclusioncriteria varchar(500),
 	exclusioncriteria varchar(500),
 	outputCriteria varchar(500),
 	enabled boolean,
 	isDeleted boolean,
 	priority float8,
 	createdon timestamp,
 	processedon timestamp,
	primary key (id, REV)
);

alter table pointsconfigrule_aud add constraint pointsconfig_foreign_idx foreign key (REV) references customrev;

Drop table IF exists userpoint;

CREATE TABLE userpoint(
        id serial not null,
		userid varchar(50),
		type varchar(50),
		balance bigint default 0,
		processedOn timestamp DEFAULT now(),
        primary key (id)
);

CREATE INDEX on userpoint(userid);
CREATE INDEX on userpoint(type);


