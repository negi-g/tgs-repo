DO
$BODY$
DECLARE
    i record;j record;k record;
	name TEXT;userProfile TEXT;
	newProfileData TEXT;temp TEXT;
	script text;
	exists boolean;
	userData TEXT;
BEGIN 
	FOR i IN SELECT * FROM airitemdetail
	LOOP
	newProfileData:=i.info -> 'profileData';
	FOR j IN SELECT * FROM jsonb_each_text(i.info -> 'profileData')
	LOOP
	execute format('SELECT users.name,users.userprofile->%L from users where userid=%L', 'data',j.key) into name,userProfile;
	execute format('SELECT REPLACE(%L,%L,%L)',newProfileData,j.key,name) into newProfileData;
	FOR k IN SELECT * FROM jsonb_each_text(userProfile::jsonb)
	LOOP
	execute format('SELECT %L::jsonb->%L',newProfileData,name) into userData;
	execute format('SELECT %L::jsonb -> %L->%L ? %L',i.info,'profileData',j.key,k.key) into exists;
	IF (exists='f') THEN
	execute format('SELECT jsonb_set(%L::jsonb,%L,%L)',userData,CONCAT('{',k.key,'}'),CONCAT('"',k.value,'"')) into temp;
	execute format('SELECT jsonb_set(%L::jsonb-%L,%L,%L)', newProfileData,name,CONCAT('{',name,'}'),temp) into newProfileData;
	END IF;	
	END LOOP;
	END LOOP;
	RAISE NOTICE 'userdata is %',newProfileData;
	script:=format('update airitemdetail set info = jsonb_set(info-%L,%L,%L) where bookingid = %L;',
				   'profileData','{profileData}',newProfileData,i.bookingid);
	RAISE NOTICE 'script is %',script;
	EXECUTE script;
	END LOOP;
END
$BODY$;