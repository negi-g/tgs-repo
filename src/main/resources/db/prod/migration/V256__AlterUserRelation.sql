alter table userrelation drop column if exists priority, add column priority int4;
alter table userrelation_aud drop column if exists priority, add column priority int4;