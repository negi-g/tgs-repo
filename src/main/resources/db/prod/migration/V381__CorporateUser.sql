ALTER TABLE users DROP COLUMN IF EXISTS employeeid;
ALTER TABLE users DROP COLUMN IF EXISTS userprofile;

ALTER TABLE users ADD COLUMN employeeid character varying (50);
ALTER TABLE users ADD COLUMN userprofile jsonb;

ALTER TABLE users_aud DROP COLUMN IF EXISTS employeeid;
ALTER TABLE users_aud DROP COLUMN IF EXISTS userprofile;

ALTER TABLE users_aud ADD COLUMN employeeid character varying (50);
ALTER TABLE users_aud ADD COLUMN userprofile jsonb;