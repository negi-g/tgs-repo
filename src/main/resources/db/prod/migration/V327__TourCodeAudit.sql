 create table if not exists tourcode_aud (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        createdon timestamp,
        enabled boolean,
        airline varchar(255),
        sourceId varchar(255),
        tourcode varchar(255),
        supplierId varchar(255),
        priority double precision,
        inclusioncriteria jsonb,
        exclusioncriteria jsonb,
        tourcodecriteria jsonb,
        isdeleted boolean,
        airtype varchar(255),
        searchtype varchar(255),
        description varchar(5000),
        processedon timestamp,
        primary key (id, REV)
    );

alter table tourcode_aud add constraint tourcode_foreign_idx foreign key (REV) references customrev;

create table if not exists creditcardinfo_aud (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        createdon timestamp,
        enabled boolean,
		product varchar(255),
        subproduct varchar(255),
	    supplierId varchar(255),
        cardtype varchar(255),
		bankname  varchar(255),
		cardnumber varchar(255),
		cvv varchar(255),
		expiry varchar(255),
		holdername varchar(255),
		street varchar(255),
		city varchar(255),
		state varchar(255),
		country varchar(255),
		pincode varchar(255),
		accountcode varchar(255),
        priority double precision,
		minpassthru double precision,
        isdeleted boolean,
        primary key (id, REV)
    );
  
alter table creditcardinfo_aud add constraint creditcard_foreign_idx foreign key (REV) references customrev;

create table if not exists commissionplan_aud (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        createdon timestamp,
        enabled boolean,
		product varchar(255),
        description varchar(255),
        processedon timestamp,
	    name varchar(255),
        primary key (id, REV)
    );
  
alter table commissionplan_aud add constraint commissionplan_foreign_idx foreign key (REV) references customrev;

create table if not exists aircommissionrule_aud (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        createdon timestamp,
        enabled boolean,
		airline varchar(255),
		supplierid varchar(255),
		priority double precision,
		inclusioncriteria jsonb,
		exclusioncriteria jsonb,
		isdeleted boolean,
		commissioncriteria jsonb,
        description varchar(255),
		processedon timestamp,
        primary key (id, REV)
    );
  
alter table aircommissionrule_aud add constraint aircommrule_foreign_idx foreign key (REV) references customrev;