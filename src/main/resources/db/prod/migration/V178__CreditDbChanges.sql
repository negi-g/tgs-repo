delete from creditpolicy;
delete from creditline;
delete from creditline_aud;

alter table creditpolicy
drop column if EXISTS overdraftlimit,
drop column if EXISTS billcycleday,
drop column if EXISTS paymentduehours,
drop column if EXISTS additionalInfo,
add column additionalInfo jsonb DEFAULT '{}'::jsonb;

alter table creditline
drop column if EXISTS comments,
drop column if EXISTS paymentDueEnd,
drop column if EXISTS additionalInfo,
add column paymentDueEnd TIMESTAMP,
add column additionalInfo jsonb DEFAULT '{}'::jsonb;

alter table creditline_aud
drop column if EXISTS comments,
drop column if EXISTS paymentDueEnd,
drop column if EXISTS additionalInfo,
add column paymentDueEnd TIMESTAMP,
add column additionalInfo jsonb DEFAULT '{}'::jsonb;