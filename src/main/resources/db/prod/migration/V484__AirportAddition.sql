delete from airportinfo where code in('XEN','BZY','BJH','KLE','OLA','UPB','LNI','AHZ','RBB','GUD','BSN','OLY','KDX','KMM','GMQ','WUI','CMT','VBV','HOX','SUR','SMJ','TOM','KMT','ERB','VCD','TGK','LXG','LGX','MIX','JTI','BDC','TGS','BLN','CNO','MRP','MJI','BDX','MBK','SVY','BMD','LYE','BMF','FEB','KGA','PYN','MKB','BMO','PYR','DIM','KGG','KOO','RUR','ZDY','RMP','LAK','RUY','ATK','CHH','LRJ','LZY','YTG','XRH','ADV','MLY','SYC','SYB','GII','BXA','SYP','SYT','KAD','NGL','KZG','KAQ','UMR','KAS','LTI','MNA','KRJ','KZR','UND','HDH','INS','WRI','RHG','PTT','MFQ','BQJ','KBQ','MOA','FYT','MWJ','TUD','KSJ','OKB','LUO','SBX','VIA','BIU','LUW','BRB','NAE','KKZ','CTK','FAU','OTD','MPA');

insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('XEN','Xingcheng Airport','CN','XEN','Jinzhou Shi','China',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BZY','Balti International Airport','MD','BZY','Balti, Moldova','Moldova',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BJH','Bajhang Airport','NP','BJH','Jayaprithvi','Nepal',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KLE','Kaele Airport','CM','KLE','Kaele','Cameroon',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('OLA','Orland Airport','US','OLA','Orlando','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('UPB','Playa Baracoa Airport','CU','UPB','Havana','Cuba',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LNI','Point Lonely Short Range Radar Site','US','LNI','Lonely','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('AHZ','Alpe dHuez Airport','FR','AHZ','Alpe dHuez','France',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('RBB','Borba Airport','BR','RBB','Borba','Brazil',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('GUD','Goundam Airport','ML','GUD','Goundam','Mali',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BSN','Bossangoa Airport','CF','BSN','Bossangoa','Central African Republic',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('OLY','Olney-Noble Airport','US','OLY','Noble','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KDX','Kadugli Airport','SD','KDX','Kaduqli','Sudan',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KMM','Kimam Airport','ID','KMM','Kimam','Indonesia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('GMQ','Golog Maqin Airport','CN','GMQ','Golog','China',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('WUI','Murrin Murrin Airport','AU','WUI','Murrin Murrin','Australia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('CMT','Cameta Airport','BR','CMT','Cameta','Brazil',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('VBV','Vanuabalavu Airport','FJ','VBV','Vanua Balavu','Fiji',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('HOX','Homalin Airport','MM','HOX','Homalin','Myanmar',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('SUR','Summer Beaver Airport','CA','SUR','Summer Beaver','Canada',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('SMJ','Sim Airport','PG','SMJ','Sim','Papua New Guinea',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('TOM','Timbuktu Airport','ML','TOM','Timbuktu','Mali',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KMT','Kampot Airport','KH','KMT','Krong Preah Sihanouk','Cambodia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('ERB','Pukatja Airport (Ernabella Airport','AU','ERB','Pukatja','Australia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('VCD','Victoria River Downs Airport','AU','VCD','Victoria River','Australia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('TGK','Taganrog Airport','RU','TGK','Rostov','Russia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LXG','Louang Namtha Airport','LA','LXG','Luang Namtha','Laos',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LGX','Lugh Ganane Airport','SO','LGX','Lugh Ganane','Somalia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MIX','Miriti-Parana Airport','CO','MIX','Miriti-Parana','Colombia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('JTI','Jatai Airport','BR','JTI','Jatai','Brazil',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BDC','Barra do Corda Airport','BR','BDC','Barra do Corda','Brazil',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('TGS','Chokwe Airport','MZ','TGS','Chokwe','Mozambique',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BLN','Benalla Airport','AU','BLN','Benalla','Australia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('CNO','Chino Airport','US','CNO','Chino','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MRP','Marla Airport','AU','MRP','Marla','Australia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MJI','Mitiga International Airport','LY','MJI','Tripoli','Libya',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BDX','Broadus Airport','US','BDX','Broadus','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MBK','Orlando Villas-Boas Regional Airport','BR','MBK','Matupa','Brazil',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('SVY','Savo Airport','SB','SVY','Savo Island','Solomon Islands',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BMD','Belo sur Tsiribihina Airport','MG','BMD','Belo Tsiribihina','Madagascar',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LYE','RAF Lyneham','GB','LYE','Lyneham','United Kingdom',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BMF','Bakouma Airport','CF','BMF','Bakouma','Central African Republic',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('FEB','Sanfebagar Airport','NP','FEB','Sanfebagar','Nepal',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KGA','Kananga Airport','CD','KGA','Kananga','Democratic Republic of the Congo',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('PYN','Payan Airport','CO','PYN','Payan','Colombia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MKB','Mekambo Airport','GA','MKB','Mekambo','Gabon',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BMO','Bhamo Airport','MM','BMO','Bhamo','Myanmar',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('PYR','Andravida Air Base','GR','PYR','Andravida','Greece',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('DIM','Dimbokro Airport','CI','DIM','Dimbokro','Ivory Coast',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KGG','Kedougou Airport','SN','KGG','Kedougou, Senegal','Senegal',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KOO','Kongolo Airport','CG','KOO','Kongolo','Democratic Republic of the Congo',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('RUR','Rurutu Airport','PF','RUR','Rurutu, French Polynesia','French Polynesia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('ZDY','Dalma Airport','AE','ZDY','Dalma','United Arab Emirates',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('RMP','Rampart Airport','US','RMP','Rampart','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LAK','Aklavik/Freddie Carmichael Airport','CA','LAK','Aklavik','Canada',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('RUY','Copan Ruinas Airport','HN','RUY','Copan Ruinas','Honduras',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('ATK','Atqasuk Edward Burnell Sr. Memorial Airport','US','ATK','Barrow','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('CHH','Chachapoyas Airport','PE','CHH','Chachapoyas','Peru',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LRJ','Le Mars Municipal Airport','US','LRJ','Le Mars','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LZY','Nyingchi Mainling Airport','CN','LZY','Nyingchi','China',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('YTG','Sullivan Bay Water Aerodrome','CA','YTG','Sullivan Bay','Canada',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('XRH','RAAF Base Richmond','AU','XRH','Richmond','Australia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('ADV','Ed Daein Airport','SD','ADV','Nyala','Sudan',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MLY','Manley Hot Springs Airport','US','MLY','Manley Hot Springs','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('SYC','Shiringayoc Airport','PE','SYC','Shiringayoc','Peru',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('SYB','Seal Bay Seaplane Base','US','SYB','Seal Bay','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('GII','Siguiri Airport','GN','GII','Siguiri','Guinea',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BXA','George R. Carr Memorial Air Field','US','BXA','Bogalusa','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('SYP','Ruben Cantu Airport','PA','SYP','Santiago','Panama',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('SYT','Saint-Yan Airport (Charolais Bourgogne Sud Airport)','FR','SYT','Saint-Yan','France',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KAD','Kaduna Airport','NG','KAD','Kaduna','Nigeria',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('NGL','Ngala Airfield','ZA','NGL','Ngala','South Africa',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KZG','Kitzingen Airport','DE','KZG','Kitzingen','Germany',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KAQ','Kamulai Airport','PG','KAQ','Kamulai','Papua New Guinea',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('UMR','RAAF Woomera Airfield','AU','UMR','Woomera','Australia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KAS','Karasburg Airport','NA','KAS','Karasburg','Namibia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LTI','Altai Airport','MN','LTI','Altai','Mongolia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MNA','Melangguane Airport','ID','MNA','Melonguane, Indonesia','Indonesia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KRJ','Karawari Airport','PG','KRJ','Karawari','Papua New Guinea',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KZR','Zafer Airport','TR','KZR','Kütahya, Turkey','Turkey',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('UND','Kunduz Airport','AF','UND','Kunduz','Afghanistan',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('HDH','Dillingham Airfield','US','HDH','Waialua','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('INS','Creech Air Force Base','US','INS','Indian Springs','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('WRI','McGuire Air Force Base','US','WRI','McGuire Air Force Base','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('RHG','Ruhengeri Airport','RW','RHG','Ruhengeri','Rwanda',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('PTT','Pratt Regional Airport','US','PTT','Pratt','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MFQ','Maradi Airport','NE','MFQ','Maradi','Niger',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BQJ','Batagay Airport','RU','BQJ','Batagay','Russia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KBQ','Kasungu Airport','MW','KBQ','Kasungu','Malawi',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MOA','Orestes Acosta Airport','CU','MOA','Moa','Cuba',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('FYT','Faya-Largeau Airport','TD','FYT','Faya-Largeau','Chad',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MWJ','Matthews Ridge Airport','GY','MWJ','Matthews Ridge','Guyana',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('TUD','Tambacounda Airport','SN','TUD','Tambacounda, Senegal','Senegal',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KSJ','Kasos Island Public Airport','GR','KSJ','Kasos Island, Greece','Greece',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('OKB','Orchid Beach Airport','AU','OKB','Fraser Island','Australia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LUO','Luena Airport','AO','LUO','Luena','Angola',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('SBX','Shelby Airport','US','SBX','Shelby','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('VIA','Angelo Ponzoni Municipal Airport','BR','VIA','Videira','Brazil',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BIU','Bildudalur Airport','IS','BIU','Bildudalur','Iceland',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LUW','Syukuran Aminuddin Amir Airport','ID','LUW','Luwuk, Indonesia','Indonesia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BRB','Barreirinhas Airport','BR','BRB','Barreirinhas','Brazil',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('NAE','Boundetingou Airport','BJ','NAE','Natitingou','Benin',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KKZ','Koh Kong Airport','KH','KKZ','Krong Khemara Phoumin','Cambodia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('CTK','Canton Municipal Airport','US','CTK','Canton','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('FAU','Fahud Airport','OM','FAU','Fahud','Oman',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('OTD','Contadora Airport','PA','OTD','San Miguel','Panama',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MPA','Katima Mulilo Airport (Mpacha Airport)','NA','MPA','Katima Mulilo','Namibia',true,0);