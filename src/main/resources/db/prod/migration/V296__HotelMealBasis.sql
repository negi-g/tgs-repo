DROP TABLE IF EXISTS hotelmealbasis;

create table hotelmealbasis
(
 	id bigserial NOT null primary key,
	fmealbasis varchar(50),
	supplier varchar(50),
	smealbasis varchar(225),
	createdOn timestamp NOT NULL DEFAULT now()
)
