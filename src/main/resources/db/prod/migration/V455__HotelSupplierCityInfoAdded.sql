drop table if exists hotelsuppliercity;

CREATE TABLE hotelsuppliercity (
	id bigserial NOT NULL,
	cityid text NOT NULL,	
	cityname text NOT NULL,
	countryid text NULL,
	countryname text NOT NULL,
	createdon timestamp NULL,
	suppliername text NULL,
	CONSTRAINT supplier_unique_city UNIQUE (cityname, countryname, suppliername),
	CONSTRAINT hotelsuppliercity_pkey PRIMARY KEY (id)
);

drop index if exists hotelsuppliercity_cityname_supplier_idx;
drop index if exists hotelsuppliercity_cityid_supplier_idx;

CREATE INDEX hotelsuppliercity_cityname_supplier_idx ON hotelsuppliercity USING btree (cityname, suppliername);
CREATE INDEX hotelsuppliercity_cityid_supplier_idx ON hotelsuppliercity USING btree (cityid, suppliername);