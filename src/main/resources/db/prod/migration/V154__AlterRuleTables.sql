alter table aircommissionrule add column description character varying(5000);
alter table supplierrule add column description character varying(5000);
alter table paymentconfiguration add column description character varying(5000);
alter table tourcode add column description character varying(5000);

alter table commissionrule add column description character varying(5000);
alter table commissionrule_aud add column description character varying(5000);

alter table airconfiguratorrule add column description character varying(5000);
alter table airconfiguratorrule_aud add column description character varying(5000);
