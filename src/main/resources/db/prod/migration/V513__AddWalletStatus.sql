alter table userwallet drop column if exists status;
alter table userwallet add column status character varying (255) default 'A';
update userwallet set status = 'A';