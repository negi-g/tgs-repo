Alter table orders add column processedon timestamp without time zone;

Alter table orders_aud add column processedon timestamp without time zone;

Alter table airorderitem add column processedon timestamp without time zone;

Alter table airorderitem_aud add column processedon timestamp without time zone;

