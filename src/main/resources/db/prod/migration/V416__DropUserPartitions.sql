ALTER TABLE users DISABLE TRIGGER users_insert_trigger;

DO
$do$
DECLARE
 rec  RECORD;
 rel text;
BEGIN
FOR rec IN (SELECT  child.relname       AS child
FROM pg_inherits
    JOIN pg_class parent ON pg_inherits.inhparent = parent.oid
    JOIN pg_class child             ON pg_inherits.inhrelid   = child.oid
    JOIN pg_namespace nmsp_parent   ON nmsp_parent.oid  = parent.relnamespace
    JOIN pg_namespace nmsp_child    ON nmsp_child.oid   = child.relnamespace
WHERE parent.relname='users') LOOP
rel = regexp_replace(rec::text, '[()]', '', 'g');
EXECUTE 'with t as (delete from ' ||rel ||(' returning *) 
    insert into users select * from t;');
EXECUTE 'DROP TABLE ' || rel;
END LOOP;
END
$do$;