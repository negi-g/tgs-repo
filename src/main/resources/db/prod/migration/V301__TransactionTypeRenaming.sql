update payment set type = 'TLC' where type = 'TLE';
update payment set type = 'CR_ISS' where type = 'CR_LC';
update payment set type = 'CR_RED' where type = 'CR_ISS' and (additionalinfo->>'tExt')::double precision < 0;