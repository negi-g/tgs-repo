DROP INDEX IF EXISTS users_parentuserid_idx;
CREATE INDEX ON users(parentuserid);

DROP TABLE IF EXISTS airitemdetail;
CREATE TABLE airitemdetail(
id BIGSERIAL PRIMARY KEY,
bookingid VARCHAR(100) NOT NULL,
info jsonb,
createdon timestamp
);

DROP INDEX IF EXISTS airitemdetail_bookingid_idx;
CREATE INDEX ON airitemdetail(bookingid);

DROP INDEX IF EXISTS airitemdetail_createdon_idx;
CREATE INDEX ON airitemdetail(createdon);