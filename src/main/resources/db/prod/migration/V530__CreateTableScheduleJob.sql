CREATE TABLE IF NOT EXISTS schedulejob (
id serial NOT NULL ,
name varchar(500) NULL,
createdon timestamp NOT NULL,
status varchar(500) NOT NULL,
processedon timestamp NOT NULL,
type varchar(500) NOT NULL,
userid varchar(500) NOT NULL,
reportlink varchar(500) NULL,
comments varchar(500)  NULL,
data varchar(5000)  NULL
);

CREATE TABLE IF NOT EXISTS schedulejob_aud (
id serial NOT NULL ,
 REV int8 not null,
 REVTYPE int2,
name varchar(500) NULL,
createdon timestamp NOT NULL,
status varchar(500) NOT NULL,
processedon timestamp NOT NULL,
type varchar(500) NOT NULL,
userid varchar(500) NOT NULL,
reportlink varchar(500) NULL,
comments varchar(500)  NULL,
data varchar(5000)  NULL
);
