update supplierinfo set credentialinfo = jsonb_set(credentialinfo,'{cabinClasses}', '["ECONOMY"]', true);
update supplierinfo set credentialinfo = jsonb_set(credentialinfo,'{cabinClasses}', '["ECONOMY","PREMIUM_ECONOMY","BUSINESS"]', true) where sourceid in (2,9);

update supplierinfo set credentialinfo = jsonb_set(credentialinfo,'{isTestCredential}', 'true', true);