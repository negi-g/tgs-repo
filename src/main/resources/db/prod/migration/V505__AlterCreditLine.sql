alter table creditline drop column if exists processedon;
alter table creditline add column processedon timestamp without time zone;
update creditline set processedon = createdon;
alter table creditline_aud drop column if exists processedon;
alter table creditline_aud add column processedon timestamp without time zone;



