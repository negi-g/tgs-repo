delete from airlineinfo where code='HC';
delete from airlineinfo where code='KP';
delete from airlineinfo where code='JY';
delete from airlineinfo where code='5Z';
delete from airlineinfo where code='L6';
delete from airlineinfo where code='PB';

SELECT setval('airline_info_id_seq', (SELECT MAX(id) FROM airlineinfo));
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('HC','Air Senegal',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('KP','Asky Airlines',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('JY','InterCaribbean Airways',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('5Z','CemAir',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('L6','Mauritania Airlines',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('PB','PAL Airlines',false,true,'');

update airlineinfo set name='Go First' where code='G8'  RETURNING *;