drop table IF EXISTS billing_entity ;
drop index IF EXISTS billing_entity_idx;

CREATE TABLE billing_entity (
 id bigserial not null,
 userId varchar(50),
 name varchar(100),
 info jsonb,
 createdon timestamp,
 processedon timestamp,
 PRIMARY KEY (id)
);

CREATE UNIQUE INDEX billing_entity_idx ON billing_entity (userId, name);