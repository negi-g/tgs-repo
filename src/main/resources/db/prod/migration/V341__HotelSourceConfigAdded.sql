delete from hotelconfiguratorrule where ruletype = 'SOURCECONFIG';

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, "output", priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true,'SOURCECONFIG', '{"sket": 20, "iGzip": true, "rtll": 20, "iao": false, "isda": true, "imgls": 20, "srto": 30,
"furls": {"SEARCH": "http://hotel.atlastravelsonline.com/ws/index.php", "DETAIL": "http://hotel.atlastravelsonline.com/ws/index.php",
"CANCELLATION": "http://hotel.atlastravelsonline.com/ws/index.php", "BOOK": "http://hotel.atlastravelsonline.com/ws/index.php"}}'
, 1, false, '{"srcids": [1]}', NULL, false, 'Promotion rule', current_timestamp);