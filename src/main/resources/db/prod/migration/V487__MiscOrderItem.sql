drop table if exists miscorderitem;


create table miscorderitem (
       id bigserial not null,
        additionalInfo jsonb,
        priceInfo jsonb,
        bookingId varchar(255),
        createdOn timestamp,
        processedOn timestamp,
        status varchar(255),
        primary key (id)
    );
    
create sequence if not exists misc_invoice_id_seq increment by 1 minvalue 1 maxvalue 99999999999999 start with 1;