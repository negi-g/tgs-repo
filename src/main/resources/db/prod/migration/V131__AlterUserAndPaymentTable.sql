alter table users alter column balance type bigint;
alter table users_aud alter column balance type bigint;
alter table payment alter column currentbalance type bigint;
alter table payment_aud alter column currentbalance type bigint;

