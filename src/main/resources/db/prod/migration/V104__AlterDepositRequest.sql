DROP TABLE IF EXISTS DepositRequest;
DROP TABLE IF EXISTS DepositRequest_aud;

CREATE TABLE DepositRequest(
id bigserial not null,
userid  varchar(255),
updateuserid varchar(255),
createdon timestamp,
processedon timestamp,
status varchar(255),
type varchar(255),
mobile varchar(255),
amount double precision,
transactionid varchar(255),
depositbranch varchar(255),
bank varchar(255),
accountnumber varchar(255),
chequedrawonbank varchar(255),
chequeissuedate date,
chequenumber varchar(255),
comments  varchar(255),
primary key (id)
);


CREATE TABLE  DepositRequest_aud(
id bigserial not null,
 rev bigint,
 revtype smallint,
userid  varchar(255),
updateuserid varchar(255),
createdon timestamp,
processedon timestamp,
status varchar(255),
type varchar(255),
mobile varchar(255),
amount double precision,
transactionid varchar(255),
depositbranch varchar(255),
bank varchar(255),
accountnumber varchar(255),
chequedrawonbank varchar(255),
chequeissuedate date,
chequenumber varchar(255),
comments  varchar(255),
primary key (id)
);