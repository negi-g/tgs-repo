alter table aircommissionrule drop column product;
alter table tourcode drop column product;
alter table aircommissionrule alter column priority set default 0;
alter table tourcode alter column priority set default 0;
ALTER TABLE tourcode RENAME COLUMN commercialcriteria TO tourCodeCriteria;
ALTER TABLE aircommissionrule  RENAME COLUMN commercialcriteria TO commissionCriteria;