ALTER TABLE hotelinfo
ADD COLUMN supplierhotelid varchar(255) NULL;

ALTER TABLE hotelinfo
ADD COLUMN suppliername varchar(255) NULL;

ALTER TABLE hotelinfo
ADD COLUMN unicaid varchar(255) NULL;

ALTER TABLE hotelinfo
ADD COLUMN additionalinfo jsonb NULL;