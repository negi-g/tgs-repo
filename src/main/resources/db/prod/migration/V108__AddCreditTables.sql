drop table IF EXISTS creditline;
drop table IF EXISTS creditline_aud;
drop table IF EXISTS creditbills;

CREATE TABLE creditline (
	id bigserial NOT NULL,
	userId character varying(255) NOT NULL,
	creditLimit BIGINT NOT NULL,
	products character varying(10)[],
	lastBilled timestamp without time zone,
	billCycleDays SMALLINT default 30,
	paymentDueDays SMALLINT default 20,
	outstandingBalance double precision default 0,
	isDisabled BOOLEAN Default false,
	disableReason character varying(10),
	createdon timestamp without time zone,
	primary key (id)
);

CREATE TABLE creditbills (
	id bigserial NOT NULL,
	billNumber character varying(30) NOT NULL,
	creditId BIGINT NOT NULL,
	userId character varying(255) NOT NULL,
	chargedPaymentIds BIGINT[],
	settlementPaymentIds BIGINT[],
	billAmount double precision NOT NULL,
	settledAmount double precision default 0,
	isSettled BOOLEAN Default false,
	createdon timestamp without time zone,
	paymentduedate timestamp without time zone,
	primary key (id)
);

CREATE TABLE creditline_aud (
	id bigserial NOT NULL,
	rev bigint NOT NULL,
	revtype smallint,
	userId character varying(255) NOT NULL,
	creditLimit BIGINT NOT NULL,
	products character varying(10)[],
	lastBilled timestamp without time zone,
	billCycleDays SMALLINT default 30,
	paymentDueDays SMALLINT default 20,
	outstandingBalance double precision default 0,
	isDisabled BOOLEAN Default false,
	disableReason character varying(10),
	createdon timestamp without time zone,
	primary key (id, rev)
);

Alter Table payment add COLUMN walletId BIGINT;

