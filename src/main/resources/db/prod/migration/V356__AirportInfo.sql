DELETE from airportinfo where code in ('QRZ','ZIA','ULH','HLG', 'AHH', 'UET', 'BZX', 'YCU', 'DAX', 'YKH', 'CXB', 'MAW', 'FFT', 'SER', 'LWV', 'XAI', 'LUM', 'SQD', 'LFQ','IKA');

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Resende', 'QRZ', 'Resende', 'QRZ', 'Brazil', 'BR', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Zhukovsky International Airport', 'ZIA', 'Zhukovsky', 'ZIA', 'Russia', 'RU', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Majeed Bin Abdulaziz', 'ULH', 'Al Ula', 'ULH', 'Saudi Arabia', 'SA', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Ohio County', 'HLG', 'Wheeling', 'HLG', 'United States', 'US', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Municipal', 'AHH', 'Amery', 'AHH', 'United States', 'US', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Quetta International Airport', 'UET', 'Quetta', 'UET', 'Pakistan', 'PK', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Enyang', 'BZX', 'Bazhong', 'BZX', 'China', 'CN', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Guangong', 'YCU', 'Yuncheng', 'YCU', 'China', 'CN', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Heshi', 'DAX', 'Dazhou', 'DAX', 'China', 'CN', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Yingkou Lanqi', 'YKH', 'Yingkou', 'YKH', 'China', 'CN', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Coxs Bazar', 'CXB', 'Coxs Bazar', 'CXB', 'Bangladesh', 'BD', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Malden Regional Airport', 'MAW', 'Malden', 'MAW', 'United States', 'US', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Capital City', 'FFT', 'Frankfor', 'FFT', 'United States', 'US', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Freeman Municipal', 'SER', 'Seymour', 'SER', 'United States', 'US', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Vincennes Intl', 'LWV', 'Lawrenceville', 'LWV', 'United States', 'US', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Minggang', 'XAI', 'Xinyang', 'XAI', 'China', 'CN', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Dehong Mangshi', 'LUM', 'Mangshi', 'LUM', 'China', 'CN', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Sanqingshan', 'SQD', 'Shangrao', 'SQD', 'China', 'CN', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Imam Khomeini Intl', 'IKA', 'Tehran', 'THR', 'Iran', 'IR', true, 0);

INSERT INTO airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) VALUES ('Qiaoli', 'LFQ', 'Linfen', 'LFQ', 'China', 'CN', true, 0);

