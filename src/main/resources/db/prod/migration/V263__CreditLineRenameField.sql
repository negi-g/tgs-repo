UPDATE creditline SET additionalinfo = replace(additionalinfo::text, 'overDraftLimit', 'maxTemporaryExt')::jsonb;
UPDATE creditline SET additionalinfo = replace(additionalinfo::text, 'approvedOdAmount', 'curTemporaryExt')::jsonb;
