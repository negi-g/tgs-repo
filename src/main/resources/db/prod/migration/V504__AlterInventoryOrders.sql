ALTER TABLE inventoryorders ADD COLUMN IF NOT EXISTS processedon TIMESTAMP;
ALTER TABLE inventoryorders_aud ADD COLUMN IF NOT EXISTS processedon TIMESTAMP;
