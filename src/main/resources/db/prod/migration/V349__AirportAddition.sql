delete from airportinfo where code in ('NAO','ACX','YIC','JGS','KAW','SOJ','BVG','AQG','ZAT','NTG','KOS','STO');


insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Nanchong Gaoping Airport', 'NAO', 'Nanchong', 'NAO', 'China', 'CN', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Xingyi', 'ACX', 'Xingyi', 'ACX', 'China', 'CN', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Mingyueshan', 'YIC', 'Yichun (Jiangxi)', 'YIC', 'China', 'CN', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Jinggangshan', 'JGS', 'Ji an', 'JGS', 'China', 'CN', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Kawthaung', 'KAW', 'Kawthaung', 'KAW', 'Myanmar', 'MM', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Sorkjosen', 'SOJ', 'Sorkjosen', 'SOJ', 'Norway', 'NO', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Berlevag', 'BVG', 'Berlevag', 'BVG', 'Norway', 'NO', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Tianzhushan', 'AQG', 'v', 'AQG', 'China', 'CN', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Zhaotong', 'ZAT', 'Zhaotong', 'ZAT', 'China', 'CN', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Xingdong', 'NTG', 'Nantong', 'NTG', 'China', 'CN', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Sihanoukville International', 'KOS', 'Sihanoukville', 'KOS', 'Cambodia', 'KH', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Metropolitan Area', 'STO', 'Stockholm', 'STO', 'Sweden', 'SE', true, 0);