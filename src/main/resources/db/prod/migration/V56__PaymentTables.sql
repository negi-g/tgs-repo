	drop table IF EXISTS payment CASCADE;
	drop table IF EXISTS payment_aud;
	drop table IF EXISTS  paymentdetail; 
	drop table IF EXISTS  paymentdetail_aud;
	drop table if exists paymentledger;
	drop table if exists paymentledger_AUD;



	CREATE TABLE payment (
	    id bigserial NOT NULL,
	    createdon timestamp without time zone,
	    paymentmedium character varying(255),
	    status character varying(255),
	    type character varying(255),
	    optype character varying(255),
	    bookingid character varying(255),
	    payuserid character varying(255),
	    loggedinuserid character varying(255),
	    amount double precision,
	    markup bigint,
	    tds double precision,
	    paymentfee double precision,
	    reason character varying(255),
	    primary key (id)
	);

	CREATE TABLE payment_aud (
	    id bigserial NOT NULL,
	    rev bigint NOT NULL,
	    revtype smallint,
	    createdon timestamp without time zone,
	    paymentmedium character varying(255),
	    status character varying(255),
	    type character varying(255),
	    optype character varying(255),
	    bookingid character varying(255),
	    payuserid character varying(255),
	    loggedinuserid character varying(255),
	    amount double precision,
	    markup bigint,
	    tds double precision,
	    paymentfee double precision,
	    reason character varying(255),
	    primary key (id,REV)
	);


	create table paymentledger (
	        id  bigserial not null,
	        createdOn timestamp,
	        paymentId int8 REFERENCES payment(id),
	        status varchar(255),
	        type varchar(255),
	        amount double precision,
	        payUserId varchar(255),
	        bookingId varchar(255),
	        additionalInfo varchar(255),
	        currentBalance double precision,
	        primary key (id)
	);

	create table paymentledger_AUD (
	        id int8 not null,
	        REV int8 not null,
	        REVTYPE int2,
	        createdOn timestamp,
	        paymentId int8 REFERENCES payment(id),
	        status varchar(255),
	        type varchar(255),
	        amount double precision,
	        payUserId varchar(255),
	        bookingId varchar(255),
	        additionalInfo varchar(255),
	        currentBalance double precision,
	        primary key (id, REV)
	);

