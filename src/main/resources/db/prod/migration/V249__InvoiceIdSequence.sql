Drop sequence if exists atlas_invoice_id_seq;
create sequence atlas_invoice_id_seq increment by 1 minvalue 1 maxvalue 9999999 start with 1;