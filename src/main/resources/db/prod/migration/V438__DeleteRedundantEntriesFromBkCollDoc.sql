delete from backend_collection_documents where type in ('nameFormats', 'corporate', 'acl','dashboardForms','Offers','sources','roles','amendStatus','groups','staticHtml','grades','themeColor','amendTypes','Notification','airport-list','hostname-themeConfig','clientConfig');

delete from backend_collection_documents where type like '%_customise_report';

delete from backend_collection_documents where type like '%_customise_screen';

delete from backend_collection_documents where type like 'reports_%';

alter table backend_collection_documents drop column owner;