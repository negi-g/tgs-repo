alter table amendment_aud
drop COLUMN if exists assignedon,
add column assignedon TIMESTAMP;