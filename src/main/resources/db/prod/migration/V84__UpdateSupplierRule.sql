update supplierrule set inclusioncriteria = jsonb_set(inclusioncriteria,'{cabinClasses}', '["ECONOMY"]', true);
update supplierrule set inclusioncriteria = jsonb_set(inclusioncriteria,'{cabinClasses}', '["ECONOMY","PREMIUM_ECONOMY","BUSINESS"]', true) where sourceid in (2,9);

update supplierrule set supplieradditionalinfo = supplieradditionalinfo - 'cabinClasses';

update supplierinfo set credentialinfo = credentialinfo - 'cabinClasses';
