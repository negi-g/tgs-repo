

drop table IF EXISTS supplier_session ;
drop table IF EXISTS SupplierSessionManagement;

CREATE TABLE SupplierSessionManagement (
 id  bigserial not null,
 bookingId varchar(255),
 supplierSessionInfo jsonb ,
 sourceId int4,
 generationTime timestamp,
 expiryTime timestamp,
 PRIMARY KEY (bookingId,sourceId)
);

