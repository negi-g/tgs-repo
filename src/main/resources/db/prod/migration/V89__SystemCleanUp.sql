alter table airorderitem drop column if exists ruleid;
alter table airorderitem_aud drop column if exists ruleid;

alter table airorderitem rename column additionalSegmentInfo to additionalinfo;
alter table airorderitem_aud rename column additionalSegmentInfo to additionalinfo;

alter table users drop column if exists commissionplan;
alter table users_aud drop column if exists commissionplan;

alter table users add column userconf jsonb;
alter table users_aud add column userconf jsonb;
