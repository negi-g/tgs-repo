
alter table aircommissionrule add column processedon timestamp;
alter table airconfiguratorrule add column processedon timestamp;
alter table airconfiguratorrule_aud add column processedon timestamp;
alter table commissionplan add column processedon timestamp;
alter table commissionrule add column processedon timestamp;
alter table commissionrule_aud add column processedon timestamp;
alter table commissionplanmapper add column processedon timestamp;
alter table configuratorrule add column processedon timestamp;
alter table configuratorrule_aud add column processedon timestamp;
alter table supplierinfo add column processedon timestamp;
alter table supplierinfo_aud add column processedon timestamp;
alter table supplierrule add column processedon timestamp;
alter table paymentconfiguration add column processedon timestamp;
alter table payment add column processedon timestamp;
alter table payment_aud add column processedon timestamp;


update   aircommissionrule set processedon = createdon;
update   airconfiguratorrule set processedon = createdon ;
update   airconfiguratorrule_aud set  processedon = createdon ;
update   commissionplan set processedon= createdon  ;
update   commissionrule set processedon = createdon ;
update   commissionrule_aud set processedon = createdon ;
update   commissionplanmapper set processedon = createdon ;
update   configuratorrule set processedon = createdon ;
update   configuratorrule_aud set processedon = createdon ;
update   supplierinfo set processedon  = createdon;
update   supplierinfo_aud set processedon = createdon ;
update   supplierrule set processedon = createdon ;
update   paymentconfiguration set processedon = createdon ;
update   payment set processedon = createdon;
update   payment_aud set processedon = createdon ;






alter table users rename column created_on to createdon;
alter table travellerinfo rename column created_on to createdon;