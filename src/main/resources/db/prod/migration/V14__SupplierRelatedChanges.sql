drop table IF EXISTS supplierconfigurationinfo_aud ;
drop table IF EXISTS supplierconfigurationinfo ;

 create table SupplierCredentialInfo (
        id  bigserial not null,
        configurationdata varchar(5000),
        created_on timestamp,
        enabled boolean,
        name varchar(255),
        sourceId int4,
        supplierId varchar(255),
        primary key (id)
    );


   create table SupplierCredentialInfo_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        configurationdata varchar(5000),
        created_on timestamp,
        enabled boolean,
        name varchar(255),
        sourceId int4,
        supplierId varchar(255),
        primary key (id, REV)
    );

   alter table SupplierCredentialInfo_AUD
        add constraint FK1js0akdfws6h01feq8ubp2nxu
        foreign key (REV)
        references customrev;

   alter table supplier_rules
        add column exclusionCriteria jsonb;
   alter table supplier_rules
        add column exitOnMatch boolean;

   alter table supplier_rules
        add column inclusionCriteria jsonb;

   alter table supplier_rules
        add column priority float8;

    alter table supplier_rules
        add column supplierInfo jsonb;

   alter table supplier_rules drop column if exists ruleinfo ;    

   
          