CREATE TABLE userrelation (
        id serial not null,
		userid1 varchar(255),
		userid2 varchar(255),
		depth int4,
        primary key (id)
);

CREATE INDEX on userrelation(userid1);