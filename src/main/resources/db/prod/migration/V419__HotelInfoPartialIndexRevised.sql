
alter table hotelinfo add column cityname varchar(255) NULL;
alter table hotelinfo add column countryname varchar(255) NULL;

drop INDEX if exists hotel_info_unique_name_rating_index_notnull;
drop INDEX if exists hotel_info_unique_name_rating_index_null;

CREATE UNIQUE INDEX hotel_info_unique_name_rating_city_country_index_notnull ON public.hotelinfo USING btree (name, rating, cityname, countryname) WHERE (rating IS NOT NULL);
CREATE UNIQUE INDEX hotel_info_unique_name_rating_index_null ON public.hotelinfo USING btree (name, cityname, countryname) WHERE (rating IS NULL);

