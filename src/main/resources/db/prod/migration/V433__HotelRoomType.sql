Drop table IF exists roomType;

CREATE TABLE roomType(
		id serial not null PRIMARY KEY,
		roomTypeName varchar(80),
		maxOccupancy INT,
		UNIQUE(roomTypeName)
);

insert into roomtype(roomtypename,maxoccupancy) VALUES('SINGLE',1);
insert into roomtype(roomtypename,maxoccupancy) VALUES('DOUBLE',2);
insert into roomtype(roomtypename,maxoccupancy) VALUES('TRIPLE',3);
insert into roomtype(roomtypename,maxoccupancy) VALUES('QUADRUPLE',4);
insert into roomtype(roomtypename,maxoccupancy) VALUES('FAMILY',5);
