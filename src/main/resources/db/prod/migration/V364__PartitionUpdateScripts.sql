CREATE OR REPLACE FUNCTION create_partition_users_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'users';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS users_insert_trigger ON users;
CREATE TRIGGER users_insert_trigger BEFORE INSERT ON users FOR EACH ROW EXECUTE PROCEDURE create_partition_users_insert();


CREATE OR REPLACE FUNCTION create_partition_orders_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'orders';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS orders_insert_trigger ON orders;
CREATE TRIGGER orders_insert_trigger BEFORE INSERT ON orders FOR EACH ROW EXECUTE PROCEDURE create_partition_orders_insert();


CREATE OR REPLACE FUNCTION create_partition_airorderitem_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'airorderitem';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS airorderitem_insert_trigger ON airorderitem;
CREATE TRIGGER airorderitem_insert_trigger BEFORE INSERT ON airorderitem FOR EACH ROW EXECUTE PROCEDURE create_partition_airorderitem_insert();


CREATE OR REPLACE FUNCTION create_partition_payment_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'payment';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS payment_insert_trigger ON payment;
CREATE TRIGGER payment_insert_trigger BEFORE INSERT ON payment FOR EACH ROW EXECUTE PROCEDURE create_partition_payment_insert();


CREATE OR REPLACE FUNCTION create_partition_amendment_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'amendment';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS amendment_insert_trigger ON amendment;
CREATE TRIGGER amendment_insert_trigger BEFORE INSERT ON amendment FOR EACH ROW EXECUTE PROCEDURE create_partition_amendment_insert();

CREATE OR REPLACE FUNCTION create_partition_systemaudit_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'systemaudit';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS systemaudit_insert_trigger ON systemaudit;
CREATE TRIGGER systemaudit_insert_trigger BEFORE INSERT ON systemaudit FOR EACH ROW EXECUTE PROCEDURE create_partition_systemaudit_insert();

CREATE OR REPLACE FUNCTION create_partition_orders_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'orders_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION create_partition_airorders_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'airorderitem_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION create_partition_payment_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'payment_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE OR REPLACE FUNCTION create_partition_users_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'users_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE OR REPLACE FUNCTION create_partition_amendment_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'amendment_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE OR REPLACE FUNCTION create_partition_seatallocation_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'seatallocation_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION create_partition_inventoryorders_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'inventoryorders_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION create_partition_depositrequest_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'depositrequest_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION create_partition_creditbill_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'creditbill_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION create_partition_creditline_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'creditline_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION create_partition_note_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'note';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;



CREATE OR REPLACE FUNCTION create_partition_hotelorderitem_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'hotelorderitem_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;



CREATE OR REPLACE FUNCTION create_partition_collection_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'collection_documents_aud';
      partition_date := to_char(NEW.created_on,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (created_on >= ''' || 
		DATE_TRUNC('month',NEW.created_on) ||'''AND created_on< ''' || DATE_TRUNC('month',NEW.created_on)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;



CREATE OR REPLACE FUNCTION moneyexchange_aud_insert_trigger() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'moneyexchange_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
