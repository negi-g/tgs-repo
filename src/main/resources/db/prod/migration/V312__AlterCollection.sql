alter table collection_documents drop column if exists owner;
alter table collection_documents add column owner character varying (255);

alter table collection_documents_aud drop column if exists owner;
alter table collection_documents_aud add column owner character varying (255);

