DROP TABLE IF EXISTS voucherconfig;

DROP TABLE IF EXISTS voucherconfig_aud;

CREATE TABLE voucherconfig (
 	id bigserial not null,
 	voucherCode varchar(50),
 	description varchar(255),
 	voucherrules jsonb,
 	enabled boolean DEFAULT true,
 	isDeleted boolean DEFAULT false,
 	priority float8,
 	quantityConsumed int4,
 	createdon timestamp DEFAULT now(),
 	processedon timestamp DEFAULT now(),
 	expiry timestamp not null,
 	PRIMARY KEY (id)
);


CREATE TABLE voucherconfig_aud (
 	id int4 not null,
 	REV int8 not null,
 	REVTYPE int2,
 	voucherCode varchar(50),
 	description varchar(255),
 	voucherrules jsonb,
 	enabled boolean DEFAULT true,
 	isDeleted boolean DEFAULT false,
 	priority float8,
 	quantityConsumed int4,
 	createdon timestamp DEFAULT now(),
 	processedon timestamp DEFAULT now(),
 	expiry timestamp not null,
	primary key (id, REV)
);
