alter table payment drop constraint if exists merchanttxnid_uniq ;
alter table payment add constraint merchanttxnid_uniq unique (merchanttxnid,status);
alter table creditline rename column isdisabled to enabled;
alter table creditline_aud rename  column isdisabled to enabled;
