delete from countryinfo where code in ('AX','BV','IO','TF','GG','HM','XK','GS');
insert into countryinfo(name,code,isocode,mobilecode) values('Aland Islands','AX','ALA','358');
insert into countryinfo(name,code,isocode,mobilecode) values('Bouvet Island','BV','BVT','47');
insert into countryinfo(name,code,isocode,mobilecode) values('British Indian Ocean Territory','IO','IOT','246');
insert into countryinfo(name,code,isocode,mobilecode) values('French Southern Territories','TF','ATF','262');
insert into countryinfo(name,code,isocode,mobilecode) values('Guernsey','GG','GGY','44');
insert into countryinfo(name,code,isocode,mobilecode) values('Heard Island and Mcdonald Islands','HM','HMD','0');
insert into countryinfo(name,code,isocode,mobilecode) values('Kosovo','XK','XKX','383');
insert into countryinfo(name,code,isocode,mobilecode) values('South Georgia and the South Sandwich Islands','GS','SGS','500');
