
CREATE TABLE TourCode (
 id  bigserial not null,
 createdon timestamp DEFAULT now(),
 enabled boolean DEFAULT false,
 supplierId varchar(255),
 sourceId varchar(255),
 airline  varchar(255),
 tourCode varchar(255),
 priority float8,
 inclusionCriteria jsonb ,
 exclusionCriteria jsonb,
 commercialCriteria jsonb,
 PRIMARY KEY (id)
);
