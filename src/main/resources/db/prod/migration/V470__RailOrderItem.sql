drop table if exists railorderitem;
drop table if exists railorderitem_aud;
create table railorderitem (
       id bigserial not null,
        additionalInfo jsonb,
        bookingId varchar(255),
        fromStn varchar(255),
        toStn varchar(255),
        createdOn timestamp,
        processedOn timestamp,
        departureTime timestamp,
        arrivalTime timestamp,
        markup float8,
        amount float8,
        status varchar(255),
        travellerInfo jsonb,
        primary key (id)
    );

create table railorderitem_aud (
       id bigserial not null,
        REV int8 not null,
        REVTYPE int2,
        additionalInfo jsonb,
        bookingId varchar(255),
        fromStn varchar(255),
        toStn varchar(255),
        createdOn timestamp,
        processedOn timestamp,
        departureTime timestamp,
        arrivalTime timestamp,
        markup float8,
        amount float8,
        status varchar(255),
        travellerInfo jsonb,
        primary key (id, REV)
    );   
