alter table userrelation drop column if exists username1, add column username1 character varying (255);
alter table userrelation drop column if exists username2, add column username2 character varying (255);
alter table userrelation drop column if exists createdon, add column createdon timestamp DEFAULT now();
alter table userrelation drop column if exists processedon, add column processedon timestamp;


create table userrelation_aud (
        id int4 not null,
        REV int8 not null,
        REVTYPE int2,
        userid1 varchar(255),
		userid2 varchar(255),
		username1 varchar(255),
		username2 varchar(255),
		depth int4,
		createdon timestamp,
		processedon timestamp,
		primary key (id,REV)
);

alter table userrelation_aud add constraint userrelation_foreign_idx foreign key (REV) references customrev;

 