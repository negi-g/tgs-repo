delete from airlineinfo where code='3M';
delete from airlineinfo where code='V7';
delete from airlineinfo where code='VG';
delete from airlineinfo where code='GP';

SELECT setval('airline_info_id_seq', (SELECT MAX(id) FROM airlineinfo));
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('3M','Silver Airways',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('V7','Volotea',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('VG','VG Airlines',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('GP','APG AIRLINES',false,true,'');