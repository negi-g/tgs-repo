alter table inventoryorders drop column if exists source, add column source varchar(50);

alter table inventoryorders_aud drop column if exists source, add column source varchar(50);

alter table inventoryorders drop column if exists dest, add column dest varchar(50);

alter table inventoryorders_aud drop column if exists dest, add column dest varchar(50);