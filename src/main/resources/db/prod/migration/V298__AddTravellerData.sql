delete from travellerinfo;
alter table travellerinfo drop column  if exists createdon;
alter table travellerinfo drop column if exists bookinguserid;
alter table travellerinfo drop column if exists email;
alter table travellerinfo drop column if exists name;
alter table travellerinfo drop column if exists userid;
alter table travellerinfo drop column if exists travellerdata;

alter table travellerinfo add column userid character varying(255) not null;
alter table travellerinfo add column email character varying(255) not null;
alter table travellerinfo add column name character varying(255) not null;
alter table travellerinfo add column travellerdata jsonb;
alter table travellerinfo drop constraint if exists travellerinfo_pkey;
alter table travellerinfo add constraint travellerinfo_pkey primary key (userid,name,email);

