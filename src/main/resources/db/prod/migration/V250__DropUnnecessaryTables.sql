drop table if exists airbookingdetails;
drop table if exists airbookingdetails_aud;
drop table if exists clientconfiguration;
drop table if exists policyinfo;
drop table if exists policyinfo_aud;
drop table if exists usersublogin;
drop table if exists paymentledger_aud;
drop table if exists paymentledger;