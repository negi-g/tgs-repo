alter table userpolicy drop column if exists createdon , add column createdon timestamp default now();
alter table userpolicy  drop column if exists processedon , add column processedon timestamp;
alter table policygroup drop column if exists groupid;