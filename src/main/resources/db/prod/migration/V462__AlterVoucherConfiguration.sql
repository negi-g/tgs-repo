ALTER TABLE voucherconfig DROP COLUMN  IF EXISTS totalQuantity;
ALTER TABLE voucherconfig_aud DROP COLUMN IF EXISTS totalQuantity;
ALTER TABLE voucherconfig DROP COLUMN  IF EXISTS quantityConsumed;
ALTER TABLE voucherconfig_aud DROP COLUMN IF EXISTS quantityConsumed;

ALTER TABLE voucherconfig ADD COLUMN  totalQuantity int4;
ALTER TABLE voucherconfig_aud ADD COLUMN totalQuantity int4;
ALTER TABLE voucherconfig ADD COLUMN  quantityConsumed int4;
ALTER TABLE voucherconfig_aud ADD COLUMN quantityConsumed int4;

update voucherconfig set totalquantity = ((voucherrules->0)::json->>'qty')::integer, quantityconsumed = (COALESCE(((voucherrules->0)::json->>'quantityConsumed')::integer,0));
