DELETE FROM collection_documents where key = 'USER_FILTER';

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}]},{"combination":[{"field":"roles"}]},{"combination":[{"field":"name"}]},{"combination":[{"field":"userId"}]},{"combination":[{"field":"userIds"}]}]}', true, 'USER_FILTER', current_timestamp, 'UserFilter');

CREATE OR REPLACE FUNCTION create_partition_users_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'users';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
		EXECUTE 'CREATE INDEX ON ' || partition || '(role);';
		EXECUTE 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_uniq_idx UNIQUE (email,mobile,role);';
		EXECUTE 'ALTER TABLE ' || partition || ' ADD PRIMARY KEY (id);';
      
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS users_insert_trigger ON users;

CREATE TRIGGER users_insert_trigger
BEFORE INSERT ON users
FOR EACH ROW EXECUTE PROCEDURE create_partition_users_insert();


