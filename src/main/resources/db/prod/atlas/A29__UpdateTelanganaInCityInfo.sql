update cityinfo set state = 'Telangana' where name IN ('Hyderabad','Medak','Nirmal','Peddapalli','Bhongir','Warangal','Vikarabad','Wanaparthy','Siddipet','Sangareddy','Nagarkurnool','Narayanpet','Mancherial','Sircilla','Kothagudem','Kamareddy','Jangaon','Gadwal','Warangal','Nizamabad','Khammam','Karimnagar','Ramagundam','Mahabubnagar','Adilabad','Suryapet','Miryalaguda','Jagtial');
 
INSERT INTO cityinfo (name, state,country,enabled) VALUES
('Nalgonda', 'Telangana','India',true),
('Komaram Bheem','Telangana','India',true),
('Mahabubabad','Telangana','India',true),
('Bhupalpally','Telangana','India',true),
('Medchal Malkajgiri','Telangana','India',true),
('Ranga Reddy','Telangana','India',true),
('Mulugu','Telangana','India',true);
