alter table payment drop constraint if exists merchanttxnid_uniq ;
alter table payment add constraint merchanttxnid_uniq unique (merchanttxnid,status,paymentmedium,amount);
