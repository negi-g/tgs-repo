
alter table configuratorrule alter column inclusioncriteria type jsonb using to_json(inclusioncriteria);
alter table configuratorrule alter column exclusioncriteria type jsonb using to_json(exclusioncriteria);

alter table configuratorrule alter column enabled set default false;

alter table configuratorrule alter column createdon set default now();

alter table configuratorrule alter column output type character varying(50000);