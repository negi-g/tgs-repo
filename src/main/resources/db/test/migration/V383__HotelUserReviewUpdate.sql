DROP TABLE IF EXISTS userreviewidinfo;

CREATE TABLE userreviewidinfo(id SERIAL PRIMARY KEY,key VARCHAR (100) UNIQUE NOT NULL,reviewId VARCHAR (50) ,hotelId VARCHAR (50),supplierId VARCHAR (50) NOT NULL,rating Integer, city VARCHAR (50), reviewDataList jsonb, staticData jsonb,UNIQUE(key,supplierId));