alter table users drop column if exists applstatus;
alter table users_aud drop column if exists applstatus;

alter table users drop column if exists railadditionalinfo, add column railadditionalinfo jsonb;
alter table users_aud drop column if exists railadditionalinfo, add column railadditionalinfo jsonb;



update users as a set railadditionalinfo =(select additionalinfo#>'{railInfo}' from users where id=a.id);
update users set railadditionalinfo  = railadditionalinfo - 'irctcid' || jsonb_build_object('iid', railadditionalinfo->'irctcid') where railadditionalinfo ? 'irctcid';


update users set railadditionalinfo = railadditionalinfo || jsonb_build_object('ai', '{}'::jsonb) where not railadditionalinfo is null;
update users as a set railadditionalinfo = jsonb_set(railadditionalinfo , '{ai,email}', (select railadditionalinfo#>'{email}' from users where id=a.id)) where railadditionalinfo ? 'email';
update users as a set railadditionalinfo = jsonb_set(railadditionalinfo , '{ai,mn}', (select railadditionalinfo#>'{mno}' from users where id=a.id)) where railadditionalinfo ? 'mno';
