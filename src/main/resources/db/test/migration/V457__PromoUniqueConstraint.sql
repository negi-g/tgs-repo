ALTER TABLE voucherconfig DROP CONSTRAINT IF EXISTS voucherconfig_uniq_idx;

ALTER TABLE voucherconfig add constraint voucherconfig_uniq_idx unique (vouchercode);