alter table rateplan add constraint rateplan_uniq_idx unique (name);

alter table airinventory add constraint airinv_uniq_idx unique (name);

alter table seatallocation drop column if exists isdeleted, add column isdeleted boolean default false;

alter table seatallocation_aud drop column if exists isdeleted, add column isdeleted boolean default false;

alter table seatallocation drop column if exists fareruleid, add column fareruleid varchar(50);

alter table seatallocation_aud drop column if exists fareruleid, add column fareruleid varchar(50);
