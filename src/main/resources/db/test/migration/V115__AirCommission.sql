CREATE TABLE aircommissionrule (
 id  bigserial not null,
 createdon timestamp DEFAULT now(),
 enabled boolean DEFAULT true,
 product  varchar(255),
 airline  varchar(255),
 supplierid  varchar(255),
 priority float8,
 inclusionCriteria jsonb,
 exclusionCriteria jsonb,
 commercialCriteria varchar(50000),
 PRIMARY KEY (id)
);
