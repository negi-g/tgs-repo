CREATE TABLE IF NOT EXISTS hotelregioninfo (
	id bigserial NOT NULL,
	regionname text NOT NULL,
	countryname text NOT NULL,
	createdon timestamp NULL,
	iatacode text NULL,
	priority int4 NULL,
	processedon timestamp NULL,
	regiontype text NULL,
	fullregionname text NULL,
	enabled bool NULL,
	CONSTRAINT hotelregioninfo_pkey PRIMARY KEY (id),
	CONSTRAINT hotelregioninfo_uniq_idx UNIQUE (regionname, countryname, regiontype, fullregionname)
);
CREATE INDEX IF NOT EXISTS hotelregioninfo_processedon_idx ON hotelregioninfo USING btree (processedon);

CREATE TABLE IF NOT EXISTS hotelsupplierregioninfo (
	id bigserial NOT NULL,
	regionid text NOT NULL,
	regionname text NOT NULL,
	countryid text NULL,
	countryname text NOT NULL,
	createdon timestamp NULL,
	suppliername text NULL,
	statename text NULL,
	stateid text NULL,
	regiontype text NULL,
	supplierregionname text NULL,
	processedon timestamp NULL,
	additionalinfo jsonb NULL,
	fullregionname text NULL,
	CONSTRAINT hotelsupplierregioninfo_pkey PRIMARY KEY (id),
	CONSTRAINT hotelsupplierregioninfo_uniq_idx UNIQUE (regionname, statename, countryname, suppliername, regiontype, fullregionname)
);
CREATE INDEX IF NOT EXISTS hotelsupplierregioninfo_countryid_idx ON hotelsupplierregioninfo USING btree (countryid);
CREATE INDEX IF NOT EXISTS hotelsupplierregioninfo_countryname_idx ON hotelsupplierregioninfo USING btree (countryname);
CREATE INDEX IF NOT EXISTS hotelsupplierregioninfo_regionid_idx ON hotelsupplierregioninfo USING btree (regionid);
CREATE INDEX IF NOT EXISTS hotelsupplierregioninfo_regionname_idx ON hotelsupplierregioninfo USING btree (regionname);
CREATE INDEX IF NOT EXISTS hotelsupplierregioninfo_stateid_idx ON hotelsupplierregioninfo USING btree (stateid);
CREATE INDEX IF NOT EXISTS hotelsupplierregioninfo_statename_idx ON hotelsupplierregioninfo USING btree (statename);
CREATE INDEX IF NOT EXISTS hotelsupplierregioninfo_supplier_idx ON hotelsupplierregioninfo USING btree (suppliername);

CREATE TABLE IF NOT EXISTS hotelregioninfomapping (
	id bigserial NOT NULL,
	regionid bigserial NOT NULL,
	supplierregionid bigserial NOT NULL,
	suppliername text NOT NULL,
	createdon timestamp NULL,
	CONSTRAINT hotelregioninfomapping_pkey PRIMARY KEY (id),
	CONSTRAINT hotelregioninfomapping_uniq_idx UNIQUE (regionid, suppliername)
);
