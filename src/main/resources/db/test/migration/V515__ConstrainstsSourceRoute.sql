delete from sourcerouteinfo where src is null or dest is null or sourceId is null;
delete from airportinfo where name is null or code is null or country is null or countrycode is null;

alter table sourcerouteinfo alter column src set not null;
alter table sourcerouteinfo alter column dest set not null;
alter table sourcerouteinfo alter column sourceId set not null;

alter table airportinfo alter column name set not null;
alter table airportinfo alter column code set not null;
alter table airportinfo alter column country set not null;
