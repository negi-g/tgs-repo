alter table railorderitem drop column IF EXISTS pnr;
alter table railorderitem add column pnr varchar(20);

alter table railorderitem_aud drop column IF EXISTS pnr;
alter table railorderitem_aud add column pnr varchar(20);

