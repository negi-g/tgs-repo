alter table users_aud rename column created_on to createdon;
alter table users_aud drop column if exists processedon, add column processedon timestamp;
update  users_aud set processedon = createdon;
