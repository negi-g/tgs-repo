
ALTER TABLE hotelsuppliercity DROP COLUMN if exists statename;
ALTER TABLE hotelsuppliercity DROP COLUMN if exists stateid;

ALTER TABLE  hotelsuppliercity ADD statename text;

ALTER TABLE  hotelsuppliercity ADD stateid text;

drop index if exists hotelsuppliercity_stateid_supplier_idx;
drop index if exists hotelsuppliercity_statename_supplier_idx;
drop index if exists hotelsuppliercity_countryid_supplier_idx;
drop index if exists hotelsuppliercity_countryname_supplier_idx;
CREATE INDEX hotelsuppliercity_stateid_supplier_idx ON hotelsuppliercity USING btree (stateid, suppliername);
CREATE INDEX hotelsuppliercity_statename_supplier_idx ON hotelsuppliercity USING btree (statename, suppliername);
CREATE INDEX hotelsuppliercity_countryid_supplier_idx ON hotelsuppliercity USING btree (countryid, suppliername);
CREATE INDEX hotelsuppliercity_countryname_supplier_idx ON hotelsuppliercity USING btree (countryname, suppliername);


alter table hotelsuppliercity drop constraint if exists supplier_unique_city;
alter table hotelsuppliercity add constraint  supplier_unique_city UNIQUE (cityname, countryname, statename,suppliername)
