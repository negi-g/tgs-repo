alter table airinventory drop column  IF EXISTS additionalInfo;
alter table airinventory_aud drop column IF EXISTS additionalInfo;

alter table airinventory add column IF NOT EXISTS additionalInfo  jsonb;
alter table airinventory_aud add column IF NOT EXISTS additionalInfo jsonb;
