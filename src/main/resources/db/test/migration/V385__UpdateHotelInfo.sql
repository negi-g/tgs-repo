ALTER TABLE hotelinfo 
DROP COLUMN IF EXISTS notes;

DO $$ 
    BEGIN
        BEGIN
            ALTER TABLE hotelinfo ADD COLUMN instructions json;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column instructions already exists in hotelinfo.';
        END;
    END;
$$