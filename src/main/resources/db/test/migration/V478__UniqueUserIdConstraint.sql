alter table users drop constraint if exists users_uniq_userid;
alter table users add constraint users_uniq_userid unique (userId);