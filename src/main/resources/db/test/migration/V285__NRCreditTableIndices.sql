drop INDEX if exists idx_nrcredit_creditid;
drop INDEX if exists idx_nrcredit_userid;
drop INDEX if exists idx_nrcredit_expiry;

create unique index idx_nrcredit_creditid on nonrevolvingcredit(creditid);
create index idx_nrcredit_userid on nonrevolvingcredit(userid);
create index idx_nrcredit_expiry on nonrevolvingcredit(creditexpiry);