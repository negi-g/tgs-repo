alter table hotelcityinfo 
add column if not exists priority bigint;

alter table hotelcityinfo 
add column if not exists processedon timestamp;

CREATE INDEX IF NOT EXISTS hotelinfo_processedon_idx ON hotelcityinfo(processedon);