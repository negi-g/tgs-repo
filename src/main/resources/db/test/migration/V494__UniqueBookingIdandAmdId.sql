alter table orders drop constraint if exists orders_uniq_bookingId;
alter table orders add constraint orders_uniq_bookingId unique (bookingid);

alter table amendment drop constraint if exists amendment_uniq_amendmentId;
alter table amendment add constraint amendment_uniq_amendmentId unique (amendmentId);