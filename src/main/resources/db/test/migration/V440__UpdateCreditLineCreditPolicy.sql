update creditline set additionalinfo = jsonb_set(additionalinfo, '{associatedCalendar}', '"bimonthly_calendar"', true) where billcycletype='BMC';
update creditline set additionalinfo = jsonb_set(additionalinfo, '{associatedCalendar}', '"bsp_calendar"', true) where billcycletype='BSP';
update creditline set billcycletype = 'CL' where billcycletype='BMC' or billcycletype='BSP';

update creditpolicy set additionalinfo = jsonb_set(additionalinfo, '{associatedCalendar}', '"bimonthly_calendar"', true) where billcycletype='6';
update creditpolicy set additionalinfo = jsonb_set(additionalinfo, '{associatedCalendar}', '"bsp_calendar"', true) where billcycletype='5';
update creditpolicy set billCycleType = '5' where billcycletype='6' or billcycletype='7';
