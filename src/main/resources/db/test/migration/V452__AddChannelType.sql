alter table amendment drop column if exists channelType;
alter table amendment add column channelType varchar(255) default 'D';

alter table amendment_aud drop column if exists channelType;
alter table amendment_aud add column channelType varchar(255) default 'D';