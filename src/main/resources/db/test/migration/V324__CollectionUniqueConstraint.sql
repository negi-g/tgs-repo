ALTER TABLE collection_documents DROP CONSTRAINT IF EXISTS collkey_uniq_idx;

ALTER TABLE collection_documents add constraint collkey_uniq_idx unique (key);