CREATE TABLE IF NOT EXISTS hotelbookingreconciliation (
	
	id bigserial NOT NULL,
	bookingid varchar(255) NOT NULL,
	createdon timestamp NOT NULL,
	processedon timestamp NOT NULL,
	bookingstatus varchar(255) NULL,
	bookingdate date NOT NULL,
	hotelname varchar(255) NOT NULL,
	checkindate date NOT NULL,
	checkoutdate date NOT NULL,
	amount float8 NULL,
	reconcileruserid varchar(255) NULL,
	cancellationdeadline timestamp NULL,
	additionalinfo jsonb NULL, 
	roominfos jsonb NOT NULL,
	CONSTRAINT hotelbookingreconciliation_pkey PRIMARY KEY (id),
	CONSTRAINT hotelbookingreconciliation_uniq_idx UNIQUE (bookingid)
);

DROP INDEX IF EXISTS hotelbookingreconciliation_booking_idx;
DROP INDEX IF EXISTS hotelbookingreconciliation_createdon_idx;

CREATE INDEX hotelbookingreconciliation_booking_idx ON hotelbookingreconciliation USING btree (bookingid);
CREATE INDEX hotelbookingreconciliation_createdon_idx ON hotelbookingreconciliation USING btree (createdon);

CREATE TABLE IF NOT EXISTS hotelbookingreconciliation_aud (
	
	id bigserial NOT NULL,
	rev int8 NOT NULL,
	revtype int2 NULL,
	bookingid varchar(255) NOT NULL,
	createdon timestamp NOT NULL,
	processedon timestamp NOT NULL,
	bookingstatus varchar(255) NULL,
	bookingdate date NOT NULL,
	hotelname varchar(255) NOT NULL,
	checkindate date NOT NULL,
	checkoutdate date NOT NULL,
	amount float8 NULL,
	reconcileruserid varchar(255) NULL,
	cancellationdeadline timestamp NULL,
	additionalinfo jsonb NULL, 
	roominfos jsonb NOT NULL,
	CONSTRAINT hotelbookingreconciliation_aud_pkey PRIMARY KEY (id, rev),
	CONSTRAINT hotelbookingreconciliation_foreign_idx FOREIGN KEY (rev) REFERENCES customrev(id)
);
