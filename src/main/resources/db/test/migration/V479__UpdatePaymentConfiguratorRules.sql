update paymentconfigurationrule set ruleType = 'EXTERNAL_PAYMENT_MEDIUM' where ruletype = 'PAYMENT_MEDIUM' and (medium = 'NB' or medium = 'CC' or medium = 'DC');

drop table IF EXISTS paymentgatewayconfiginfo ;
drop table IF EXISTS paymentgatewayconfiginfo_aud ;

create table paymentgatewayconfiginfo(
id bigserial not null,
name  varchar(255) not null,
createdon timestamp DEFAULT now(),
processedon timestamp DEFAULT now(),
gatewayType varchar(255),
gateWayInfo TEXT,
displayName varchar(255),
enabled boolean DEFAULT true,
isDeleted boolean DEFAULT false,
termsAndConditions varchar(255),
primary key (name)
);

 create table paymentgatewayconfiginfo_aud(
 id bigserial not null,
 rev bigint,
 revtype smallint,
 name  varchar(255) not null,
 createdon timestamp DEFAULT now(),
 processedon timestamp DEFAULT now(),
 gatewayType varchar(255),
 displayName varchar(255),
 gateWayInfo TEXT,
 enabled boolean DEFAULT true,
 isDeleted boolean DEFAULT false,
 termsAndConditions varchar(255),
 primary key (name, rev)
 );

alter table paymentconfigurationrule drop column if exists externalPgInfoId;   

alter table paymentconfigurationrule_aud drop column if exists externalPgInfoId;
 
Alter table paymentconfigurationrule add column externalPgInfoId varchar(255);

Alter table paymentconfigurationrule_aud add column externalPgInfoId varchar(255);

Insert Into paymentgatewayconfiginfo(name, createdon, processedon, gatewaytype, gatewayinfo, displayname)
Select CONCAT(medium,'_',output::jsonb->>'gt','_', id), createdon, processedon, output::jsonb->>'gt' as gatewaytype, output::jsonb->>'ginfo' as gatewayInfo, output::jsonb->>'dn'  from paymentconfigurationrule where output::jsonb->'ginfo' is not null or output::jsonb->>'gt' is not null;
	
update paymentconfigurationrule set externalPgInfoId = CONCAT(medium,'_',output::jsonb->>'gt','_', id)
where (output::jsonb->'ginfo' is not null or output::jsonb->>'gt' is not null) and externalPgInfoId is null;