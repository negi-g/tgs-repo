ALTER TABLE airorderitem DROP COLUMN IF EXISTS bookinguserid, ADD COLUMN bookinguserid VARCHAR(50);
ALTER TABLE airorderitem_aud DROP COLUMN IF EXISTS bookinguserid, ADD COLUMN bookinguserid VARCHAR(50);

UPDATE airorderitem a SET bookinguserid = o.bookinguserid FROM orders o WHERE a.bookingid = o.bookingid and a.bookinguserid is NULL ;
CREATE index on airorderitem(bookinguserid) ;