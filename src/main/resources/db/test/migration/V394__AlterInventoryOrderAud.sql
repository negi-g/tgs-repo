DO
$$
DECLARE
	row record;
	table_name TEXT;
BEGIN
	table_name := 'inventoryorders_aud';
	FOR row IN select relname from pg_inherits i join pg_class c on c.oid = inhrelid where inhparent = table_name::regclass
	LOOP
	EXECUTE 'alter table ' || row.relname || ' drop constraint if exists '|| row.relname || '_pkey;';
	EXECUTE 'alter table ' || row.relname || ' add primary key (id,rev);';
	END LOOP;
END;
$$;
