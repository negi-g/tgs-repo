alter table tourcode drop column if exists processedon, add column processedon timestamp;
alter table fareruleinfo drop column if exists processedon, add column processedon timestamp;
update tourcode set processedon = createdon;
update fareruleinfo set processedon = createdon;