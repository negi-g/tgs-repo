alter table creditline
drop column if EXISTS creditlimit, add COLUMN creditlimit BIGINT NOT NULL,
drop column if EXISTS products, add COLUMN products character varying(10)[],
drop column if EXISTS billCycleType, add COLUMN billCycleType character varying(10),
drop column if EXISTS paymentdueend;

alter table creditline_aud
drop column if EXISTS creditlimit, add COLUMN creditlimit BIGINT NOT NULL,
drop column if EXISTS products, add COLUMN products character varying(10)[],
drop column if EXISTS billCycleType, add COLUMN billCycleType character varying(10),
drop column if EXISTS paymentdueend;