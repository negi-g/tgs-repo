alter table orders drop column if exists sublogin;
alter table orders_aud drop column if exists sublogin;
alter table orders drop column if exists paymentmedium;
alter table orders_aud drop column if exists paymentmedium;

