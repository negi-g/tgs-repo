alter table ConfiguratorRule add column type character varying(255);

alter table ConfiguratorRule drop column inclusionCriteria;
alter table ConfiguratorRule drop column exclusionCriteria;


alter table ConfiguratorRule add column inclusionCriteria character varying(5000);

alter table ConfiguratorRule add column exclusionCriteria character varying(5000);