delete from airlineinfo where code='TW';
delete from airlineinfo where code='8Y';
delete from airlineinfo where code='7M';
delete from airlineinfo where code='JA';
delete from airlineinfo where code='QD';
delete from airlineinfo where code='BF';
delete from airlineinfo where code='VP';
delete from airlineinfo where code='XZ';
delete from airlineinfo where code='8Q';
delete from airlineinfo where code='0B';
delete from airlineinfo where code='GS';
delete from airlineinfo where code='H9';
delete from airlineinfo where code='4Z';


insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('TW','Tway Air',true,false,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('8Y','Pan Pacific Airlines',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('7M','MAY Air',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('JA','JetSmart',true,false,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('QD','JC International Airlines',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('BF','French Bee',true,false,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('VP','FlyMe',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('XZ','SA Express',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('8Q','Onur Air',true,false,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('0B','Blue Air',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('GS','Tianjin Airlines',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('H9','Himalaya Airlines',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('4Z','AirLink',true,false,'');




