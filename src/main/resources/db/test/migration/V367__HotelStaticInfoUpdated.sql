DO $$
BEGIN
  IF EXISTS(SELECT *
    FROM information_schema.columns
    WHERE table_name='hotelinfo' and column_name='thumbnails')
  THEN
      ALTER TABLE "public"."hotelinfo" RENAME COLUMN "thumbnails" TO "images";
  END IF;
END $$;

ALTER TABLE hotelinfo ADD COLUMN propertytype text;

ALTER TABLE hotelinfo ALTER COLUMN images type jsonb using to_json(images);

ALTER TABLE hotelsuppliermapping DROP CONSTRAINT IF EXISTS hotelsuppliermapping_uniq_idx;

ALTER TABLE hotelsuppliermapping ADD CONSTRAINT hotelsuppliermapping_uniq_idx UNIQUE (hotelid,suppliername);