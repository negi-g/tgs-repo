ALTER TABLE seatallocation DROP CONSTRAINT IF EXISTS seatallocation_uniq;

ALTER TABLE seatallocation ADD CONSTRAINT seatallocation_uniq UNIQUE (inventoryid,validon); 
