CREATE TABLE IF NOT EXISTS hotelsupplierinfo_aud(
	id bigserial NOT NULL,
 	rev bigint,
 	revtype smallint,
	credentialinfo jsonb NULL,
	createdon timestamp NULL DEFAULT now(),
	enabled bool NULL DEFAULT true,
	name varchar(255) NOT NULL,
	sourceid int4 NOT NULL,
	isdeleted bool NULL DEFAULT false,
	processedon timestamp NULL,
	PRIMARY KEY (id,rev)
)