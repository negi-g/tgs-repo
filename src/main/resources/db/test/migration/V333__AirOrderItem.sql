update airorderitem set additionalinfo = jsonb_set(additionalinfo-'fareIdentifier', '{fi}', to_jsonb(additionalinfo#>>'{fareIdentifier}')) 
where additionalinfo#>'{fareIdentifier}' is not null;

update airorderitem set additionalinfo = jsonb_set(additionalinfo-'searchType', '{st}', '"O"'::jsonb) 
where additionalInfo@>'{"searchType" : "ONEWAY"}';

update airorderitem set additionalinfo = jsonb_set(additionalinfo-'searchType', '{st}', '"R"'::jsonb) 
where additionalInfo@>'{"searchType" : "RETURN"}';

update airorderitem set additionalinfo = jsonb_set(additionalinfo-'searchType', '{st}', '"M"'::jsonb) 
where additionalInfo@>'{"searchType" : "MULTICITY"}';

update airorderitem set additionalinfo = jsonb_set(additionalinfo-'searchType', '{st}', '"A"'::jsonb) 
where additionalInfo@>'{"searchType" : "ALL"}';
