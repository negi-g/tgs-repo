alter table moneyexchange drop column if exists isenabled;
alter table moneyexchange add column isenabled boolean;

alter table moneyexchange_aud drop column if exists isenabled;
alter table moneyexchange_aud add column isenabled boolean;