create table configuratorrule_aud(
 id bigserial not null,
 rev bigserial not null,
 revtype smallint,
 createdon timestamp DEFAULT now(),
 enabled boolean DEFAULT true,
 ruleType character varying(255) not null,
 type character varying(255) not null,
 inclusionCriteria character varying(50000)  ,
 exclusionCriteria character varying(50000)  ,
 output character varying(50000),
 priority float8,
 exitOnMatch boolean,
 PRIMARY KEY (id)
);

alter table configuratorrule alter column enabled set default true;

alter table airconfiguratorrule alter column enabled set default true;

create table airconfiguratorrule_aud(
id bigserial not null,
 rev bigserial not null,
 revtype smallint,
createdon timestamp DEFAULT now(),
enabled boolean DEFAULT true,
ruletype character varying(255) not null,
output character varying(50000),
priority float8,
exitonmatch boolean,
inclusioncriteria jsonb,
exclusioncriteria jsonb,
 PRIMARY KEY (id)
);