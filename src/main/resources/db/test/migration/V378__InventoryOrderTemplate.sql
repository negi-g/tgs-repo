alter table inventoryorders drop column if exists additionalinfo, add column additionalinfo jsonb;
alter table inventoryorders drop column if exists product, add column product varchar(50);
alter table inventoryorders drop column if exists flightNumber, add column flightNumber varchar(50);

alter table inventoryorders_aud drop column if exists additionalinfo, add column additionalinfo jsonb;
alter table inventoryorders_aud drop column if exists product, add column product varchar(50);
alter table inventoryorders_aud drop column if exists flightNumber, add column flightNumber varchar(50);

update inventoryorders set product = 'A';
update inventoryorders_aud set product = 'A';

update inventoryorders as i set flightNumber = a.flightNumber from airorderitem a where i.referenceid = a.bookingid::varchar
and a.status = 'S' and a.additionalinfo->'scid' ::text = '11';
update inventoryorders_aud as i set flightNumber = a.flightNumber from airorderitem a where i.referenceid = a.bookingid::varchar
and a.status = 'S' and a.additionalinfo->'scid' ::text = '11';

update inventoryorders set additionalinfo = json_build_object('source', source, 'dest', dest,'airline', airline, 'pnr', pnr,'flightNumber',flightNumber);
update inventoryorders_aud set additionalinfo = json_build_object('source', source, 'dest', dest,'airline', airline, 'pnr', pnr,'flightNumber',flightNumber);

UPDATE inventoryorders SET travellerinfo = to_json(t.newValue)
FROM (WITH temp AS (SELECT i.referenceid, jsonb_array_elements(i.travellerinfo) as it,jsonb_array_elements(a.travellerinfo) as at FROM airorderitem a join inventoryorders i on a.bookingid = i.referenceid
				   where a.additionalinfo->'scid' ::text = '11')
    SELECT referenceid, array_agg(it|| jsonb_build_object('cp', at->'fd'->'fC'->'BF')) AS newValue
    FROM temp GROUP BY referenceid) AS t
WHERE inventoryorders.referenceid = t.referenceid;

UPDATE inventoryorders_aud SET travellerinfo = to_json(t.newValue)
FROM (WITH temp AS (SELECT i.referenceid, jsonb_array_elements(i.travellerinfo) as it,jsonb_array_elements(a.travellerinfo) as at FROM airorderitem a join inventoryorders i on a.bookingid = i.referenceid
				   where a.additionalinfo->'scid' ::text = '11')
    SELECT referenceid, array_agg(it|| jsonb_build_object('cp', at->'fd'->'fC'->'BF')) AS newValue
    FROM temp GROUP BY referenceid) AS t
WHERE inventoryorders_aud.referenceid = t.referenceid;

alter table inventoryorders drop column if exists flightNumber;
alter table inventoryorders drop column if exists source;
alter table inventoryorders drop column if exists dest;
alter table inventoryorders drop column if exists airline;
alter table inventoryorders drop column if exists pnr;

alter table inventoryorders_aud drop column if exists flightNumber;
alter table inventoryorders_aud drop column if exists source;
alter table inventoryorders_aud drop column if exists dest;
alter table inventoryorders_aud drop column if exists airline;
alter table inventoryorders_aud drop column if exists pnr;


