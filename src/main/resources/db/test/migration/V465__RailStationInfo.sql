drop table IF EXISTS railstationinfo ;

CREATE TABLE railstationinfo (
 id  bigserial not null,
 code varchar(255),
 name varchar(255),
 enabled boolean default true,
 priority float8 default 0,
 PRIMARY KEY (id)
);