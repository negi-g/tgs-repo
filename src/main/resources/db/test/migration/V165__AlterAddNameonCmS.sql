alter table commissionplan
drop column if exists name, add column name character varying(255);

update commissionplan set name = description;

alter table commissionplan alter column name SET NOT NULL;