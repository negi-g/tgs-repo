CREATE TABLE FareRuleInfo (
 id bigserial not null,
 createdon timestamp DEFAULT now(),
 airType CHARACTER VARYING(255),
 airline CHARACTER VARYING(255),
 priority float8,
 enabled boolean DEFAULT false,
 inclusionCriteria jsonb,
 exclusionCriteria jsonb,
 fareRuleInformation jsonb,
 PRIMARY KEY (id)
 );