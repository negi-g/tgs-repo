drop table if EXISTS arearolemap;

create table arearolemap(
id bigserial,
arearole varchar(100),
mappinginfo jsonb,
primary key(id)
);