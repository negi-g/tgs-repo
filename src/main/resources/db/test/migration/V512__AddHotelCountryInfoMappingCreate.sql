CREATE TABLE IF NOT EXISTS hotelcountryinfomapping  (
id bigserial PRIMARY KEY,
countryId text NOT NULL, 
supplierName text NOT NULL, 
supplierCountryId text NOT NULL,
supplierCountryName text,
createdOn timestamp,
UNIQUE(countryId , supplierName));