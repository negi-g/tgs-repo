Drop table IF exists userwallet;

CREATE TABLE userwallet(
        id serial not null,
		userid varchar(255),
		balance bigint default 0,
		processedOn timestamp DEFAULT now(),
        primary key (id)
);

CREATE INDEX on userwallet(userid);

ALTER TABLE userwallet add constraint user_uniq_idx unique (userid);

INSERT INTO userwallet(userid, balance) select userid,balance from users;

/*ALTER TABLE users drop column if exists balance;

ALTER TABLE users_aud drop column if exists balance;*/


