delete from airportinfo where code in('LBD','PBG','CIH','BGG','GRE','KET','BLM','EJH','WNH','KRY','NYM','PMA','XIC');
delete from airportinfo where code in ('KLL','KDH','DGR','CNI','SVT','NDL','DQM','USA','KOH','FEG','KOL','OXC','OGU','MLP','PRR','KHS','SIF','INF','AFN','DDN','MFU','BAN','CTO');

insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('LBD','Khujand Airport','TJ','LBD','Khujand, Tajikistan','Tajikistan',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('PBG','Plattsburgh International Airport','US','PBG','Plattsburgh','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('CIH','Changzhi Wangcun Airport','CN','CIH','Changzhi Shi','China',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BGG','Bingol Airport','TR','BGG','Bingol, Turkey','Turkey',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('GRE','Greenville Airport','US','GRE','Greer','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KET','Kengtung Airport','MM','KET','Keng Tung','Myanmar',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BLM','Monmouth Executive Airport','US','BLM','Wall Township','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('EJH','Al Wajh Domestic Airport','SA','EJH','Al Wajh','Saudi Arabia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('WNH','Wenshan Puzhehei Airport','CN','WNH','Wenshan','China',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KRY','Karamay Airport','CN','KRY','Kelamayi Shi','China',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('NYM','Nadym Airport','RU','NYM','Nadym','Russia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('PMA','Pemba Airport','MZ','PMA','Pemba','Mozambique',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('XIC','Xichang Qingshan Airport','CN','XIC','Liangshan','China',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KLL','Levelock Airport','US','KLL','Levelock','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KDH','Kandahar International Airport','AF','KDH','Kandahar, Afghanistan','Afghanistan',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('DGR','Dargaville Aerodrome','NZ','DGR','Turiwiri','New Zealand',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('CNI','Changhai Airport','CN','CNI','Dalian','China',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('SVT','Savuti Airport','BW','SVT','Savuti, Botswana','Botswana',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('NDL','NDele Airport','CF','NDL','NDele, Central African Republic','Central African Republic',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('DQM','Duqm International Airport','OM','DQM','Duqm, Oman','Oman',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('USA','Concord Regional Airport (FAA: JQF)','US','USA','Concord','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KOH','Koolatah Airport','AU','KOH','Maramie','Australia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('FEG','Fergana International Airport','UZ','FEG','Oepraha','Uzbekistan',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KOL','Koumala Airport','AU','KOL','East Mackay','Australia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('OXC','Waterbury Oxford Airport','US','OXC','Oxford','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('OGU','OrduGiresun Airport','TR','OGU','OrduGiresun, Turkey','Turkey',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MLP','Malabang Airport','PH','MLP','Malabang','Philippines',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('PRR','Paruima Airport','GY','PRR','Paruima, Guyana','Guyana',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('KHS','Khasab Airport','OM','KHS','Al Khasab','Oman',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('SIF','Simara Airport','NP','SIF','Gadhimai','Nepal',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('INF','In Guezzam Airport','DZ','INF','In Guezzam','Algeria',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('AFN','Jaffrey AirportSilver Ranch','US','AFN','Jaffrey','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('DDN','Delta Downs Airport','US','DDN','Orange','United States',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('MFU','Mfuwe Airport','ZM','MFU','Mfuwe, Zambia','Zambia',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('BAN','Basongo Airport','CG','BAN','Basongo, Democratic Republic of the Congo','',true,0);
insert into airportinfo(code,name,countrycode,citycode,city,country,enabled,priority) values('CTO','Calverton Executive Airpark (FAA: 3C8)','US','CTO','Calverton','United States',true,0);
