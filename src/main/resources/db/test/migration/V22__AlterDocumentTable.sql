alter table collection_documents add column expiry timestamp without time zone;
alter table collection_documents drop constraint collections_key_uniq;
alter table orders drop column sourcetype;
