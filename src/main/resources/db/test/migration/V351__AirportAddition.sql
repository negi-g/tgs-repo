delete from airportinfo where code in ('RIZ','PKX','LPF','DSS');

insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Shanzihe', 'RIZ', 'Rizhao', 'RIZ', 'China', 'CN', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Daxing Intl', 'PKX', 'Beijing', 'BJS', 'China', 'CN', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Yue Zhao', 'LPF', 'Liupanshui', 'LPF', 'China', 'CN', true, 0);
insert into airportinfo (name, code, city, citycode, country, countrycode, enabled, priority) values ('Blaise Diagne', 'DSS', 'Senegal', 'SEN', 'United Kingdom', 'GB', true, 0);