drop table if EXISTS backend_collection_documents;

create table backend_collection_documents (
        id  bigserial not null,
        created_on timestamp,
        data text,
        enabled boolean,
        key varchar(255),
        expiry timestamp without time zone,
        owner varchar(255),
        type  varchar(500),
        primary key (id)
    );
    
alter table backend_collection_documents alter COLUMN created_on SET default now();

ALTER TABLE backend_collection_documents add constraint backcendcollkey_uniq_idx unique (key);

INSERT INTO backend_collection_documents(id,data,enabled,key,expiry,type) SELECT id,data,enabled,key,expiry,type from collection_documents ;
SELECT setval('backend_collection_documents_id_seq', (SELECT MAX(id) FROM backend_collection_documents));


alter table backend_collection_documents add column processed_on timestamp without time zone;


alter table backend_collection_documents alter COLUMN processed_on SET default now();
