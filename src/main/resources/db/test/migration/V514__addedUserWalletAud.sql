alter table userwallet drop column if exists status;
alter table userwallet add column status character varying (255) default 'A';
update userwallet set status = 'A';

alter table userwallet drop column if exists comments;
alter table userwallet add column comments character varying (255);

drop table IF EXISTS userwallet_aud;
CREATE TABLE userwallet_aud(
		id int8 not null,
        REV int8 not null,
        REVTYPE int2,
		userid varchar(255),
		balance bigint default 0,
		status varchar(255),
		comments varchar(255),
		processedOn timestamp DEFAULT now(),
        primary key (id, REV)
);