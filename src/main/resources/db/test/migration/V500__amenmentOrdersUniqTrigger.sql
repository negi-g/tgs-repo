CREATE OR REPLACE FUNCTION public.amendment_unq_trigger()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN

  IF count(1) > 1 FROM amendment WHERE amendmentId = NEW.amendmentid THEN
    RAISE EXCEPTION 'duplicate key value violates unique constraint "%" ON "%"', 
      TG_NAME, TG_TABLE_NAME
      USING DETAIL = format('Key (amendmentid)=(%L) already exists.', NEW.amendmentid);
  END IF;

  RETURN NULL;
END
$function$;

CREATE OR REPLACE FUNCTION create_partition_amendment_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'amendment';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		updateScript := 'CREATE CONSTRAINT TRIGGER amendment_unq_trigger AFTER INSERT ON ' || partition ||
  ' DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE public.amendment_unq_trigger();';
  		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS amendment_insert_trigger ON amendment;
CREATE TRIGGER amendment_insert_trigger BEFORE INSERT ON amendment FOR EACH ROW EXECUTE PROCEDURE create_partition_amendment_insert();

DROP TRIGGER IF EXISTS amendment_unq_trigger ON amendment;
CREATE CONSTRAINT TRIGGER amendment_unq_trigger AFTER INSERT ON amendment
  DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE public.amendment_unq_trigger();
  
CREATE OR REPLACE FUNCTION public.orders_unq_trigger()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN

  IF count(1) > 1 FROM orders WHERE bookingid = NEW.bookingid THEN
    RAISE EXCEPTION 'duplicate key value violates unique constraint "%" ON "%"', 
      TG_NAME, TG_TABLE_NAME
      USING DETAIL = format('Key (bookingid)=(%L) already exists.', NEW.bookingid);
  END IF;

  RETURN NULL;
END
$function$;

CREATE OR REPLACE FUNCTION create_partition_orders_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'orders';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''');';
		EXECUTE updateScript;
		updateScript := 'CREATE CONSTRAINT TRIGGER orders_unq_trigger AFTER INSERT ON ' || partition ||
  ' DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE public.orders_unq_trigger();';
  		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS orders_insert_trigger ON orders;
CREATE TRIGGER orders_insert_trigger BEFORE INSERT ON orders FOR EACH ROW EXECUTE PROCEDURE create_partition_orders_insert();

DROP TRIGGER IF EXISTS orders_unq_trigger ON orders;
CREATE CONSTRAINT TRIGGER orders_unq_trigger AFTER INSERT ON orders
  DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE public.orders_unq_trigger();
  

  
