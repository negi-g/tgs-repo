alter table userfee drop column if exists priority;

alter table userfee add column priority double precision default 0;