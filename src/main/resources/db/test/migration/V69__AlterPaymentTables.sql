alter table payment add column currentbalance float8;
alter table payment_aud add column currentbalance float8;
alter table paymentledger alter column currentbalance type float8;
alter table paymentledger_aud  alter column currentbalance type float8;		
