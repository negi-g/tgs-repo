alter table paymentgatewayconfiginfo drop column if exists isHidden;   

alter table paymentgatewayconfiginfo_aud drop column if exists isHidden;
 
alter table paymentgatewayconfiginfo add column isHidden boolean DEFAULT false;

alter table paymentgatewayconfiginfo_aud add column isHidden boolean DEFAULT false;

update paymentgatewayconfiginfo set isHidden = true;