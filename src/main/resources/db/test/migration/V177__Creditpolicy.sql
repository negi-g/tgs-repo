Alter table creditline 
	drop column if exists createdbyuserid,
	drop column if exists issuedbyuserid,
	drop column if exists comments,
	drop COLUMN if EXISTS creditlimit,
	drop COLUMN if EXISTS products,
	drop COLUMN if EXISTS billcycledays,
	drop COLUMN if EXISTS paymentduedays,
	drop COLUMN if EXISTS disablereason,
	drop COLUMN if EXISTS billcycleend,
	drop COLUMN if EXISTS billcyclestart,
	DROP COLUMN if EXISTS policyid,
	drop COLUMN if EXISTS lastbilled;


Alter table creditline_aud
	drop column if exists createdbyuserid,
	drop column if exists issuedbyuserid,
	drop column if exists comments,
	drop COLUMN if EXISTS creditlimit,
	drop COLUMN if EXISTS products,
	drop COLUMN if EXISTS billcycledays,
	drop COLUMN if EXISTS paymentduedays,
	drop COLUMN if EXISTS disablereason,
	drop COLUMN if EXISTS billcycleend,
	drop COLUMN if EXISTS billcyclestart,
	DROP COLUMN if EXISTS policyid,
	drop COLUMN if EXISTS lastbilled;


Alter table creditline 
	add column issuedbyuserid VARCHAR(30), 
	add COLUMN policyid VARCHAR(30),
	add COLUMN billcycleend TIMESTAMP,
	add COLUMN billcyclestart TIMESTAMP,
	add column comments VARCHAR(255);


Alter table creditline_aud
	add column issuedbyuserid VARCHAR(30), 
	add COLUMN policyid VARCHAR(30),
	add COLUMN billcycleend TIMESTAMP,
	add COLUMN billcyclestart TIMESTAMP,
	add column comments VARCHAR(255);


delete from creditline;
delete from creditline_aud;


drop table if EXISTS creditpolicy;


Create Table creditpolicy(
id bigserial NOT NULL,
policyid VARCHAR(30),
creditlimit BIGINT,
overdraftlimit BIGINT,
products _varchar(10),
billcycletype VARCHAR(10),
billcycleday SMALLINT,
paymentduehours SMALLINT,
createdon TIMESTAMP,
PRIMARY KEY (id)
);

