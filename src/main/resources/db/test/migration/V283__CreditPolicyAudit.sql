drop table if exists creditpolicy_aud;

create table creditpolicy_aud(
rev bigint NOT NULL,
revtype smallint not null,
id bigserial not null,
policyid varchar(50),
creditlimit bigint not null,
products _varchar,
billcycletype varchar(10),
createdon timestamp,
additionalinfo jsonb DEFAULT '{}'::jsonb,
documents _varchar,
PRIMARY KEY (id, rev)
);