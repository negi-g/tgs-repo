ALTER TABLE corptrip drop COLUMN if EXISTS additionalInfo, add COLUMN additionalInfo jsonb default '{}'::jsonb;

