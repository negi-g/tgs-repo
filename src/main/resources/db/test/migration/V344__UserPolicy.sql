Drop table IF exists userpolicy;

CREATE TABLE userpolicy(
        id serial not null,
        userid varchar(500),
		policyInfo jsonb,
        primary key (id)
);

CREATE INDEX on userpolicy(userid);

ALTER TABLE userpolicy add constraint userid_uniq_idx unique (userid);

Drop table IF exists policygroup;

CREATE TABLE policygroup (
id bigserial NOT NULL,
createdon timestamp DEFAULT now(),
processedon timestamp DEFAULT now(),
enabled boolean DEFAULT false,
isdeleted boolean DEFAULT false,
groupid varchar(255) NOT NULL UNIQUE,
name varchar(255),
description varchar(255),
policies jsonb,
PRIMARY KEY (id)
);

CREATE INDEX on policygroup(groupid);

ALTER TABLE policygroup add constraint groupid_uniq_idx unique (groupid);