DROP TABLE IF EXISTS backend_collection_documents_aud;

CREATE TABLE  backend_collection_documents_aud(
id int8 not null,
REV int8 not null,
REVTYPE int2,created_on timestamp,
expiry timestamp,
data text,
enabled boolean,
key varchar(255),
type varchar(500),
owner varchar(255),
primary key (id, REV)
);

alter table backend_collection_documents_aud add column processed_on timestamp without time zone;

alter table backend_collection_documents_aud alter COLUMN processed_on SET default now();

alter table backend_collection_documents_aud
      add constraint backend_collection_documents_aud_foreign_idx
      foreign key (REV)
      references customrev ;
      
      
CREATE OR REPLACE FUNCTION create_partition_backend_collection_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'backend_collection_documents_aud';
      partition_date := to_char(NEW.created_on,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (created_on >= ''' || 
		DATE_TRUNC('month',NEW.created_on) ||'''AND created_on< ''' || DATE_TRUNC('month',NEW.created_on)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
      	EXECUTE 'CREATE INDEX ON ' || partition || '(created_on);';

	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS backend_collection_aud_insert_trigger ON backend_collection_documents_aud;

CREATE TRIGGER backend_collection_aud_insert_trigger
BEFORE INSERT ON backend_collection_documents_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_backend_collection_aud_insert();
