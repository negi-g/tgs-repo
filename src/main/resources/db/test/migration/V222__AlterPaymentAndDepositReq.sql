alter table payment rename bookingid to refid;
alter table payment_aud rename bookingid to refid;
alter table depositrequest add column reqid character varying (20);
alter table depositrequest_aud add column reqid character varying (20);
