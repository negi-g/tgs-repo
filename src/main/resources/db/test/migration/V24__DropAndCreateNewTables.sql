drop table IF EXISTS AirBookingDetails;
drop table IF EXISTS AirBookingDetails_AUD;
drop table IF EXISTS AirOrderItem;
drop table IF EXISTS AirOrderItem_AUD;
drop table IF EXISTS orders;
drop table IF EXISTS orders_AUD;
drop table IF EXISTS GstInfo;

update client_configurations set configuration = '{"searchTypeGroupSourceIds":{"ONEWAY_DOM":[[1,2,3]],"RETURN_DOM":[[1,2,3]],"MULTICITY_DOM":[[1,2,3]],"ONEWAY_INTL":[[1,2,3]],"RETURN_INTL":[[1,2,3]],"MULTICITY_INTL":[[1,2,3]]},"pathMap":{"BLRSXR":[{"edges":[{"src":"BLR","dest":"DEL","sourceId":[1]},{"src":"DEL","dest":"SXR","sourceId":[2]}]}]}}' where type = 'AIRCONFIG';

create table AirBookingDetails (
        id  bigserial not null,
        bookingId varchar(255),
        paxNo int4,
        segmentNos varchar(255),
        status varchar(255),
        type varchar(255),
        primary key (id)
    );

create table AirBookingDetails_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        bookingId varchar(255),
        paxNo int4,
        segmentNos varchar(255),
        status varchar(255),
        type varchar(255),
        primary key (id, REV)
    );


     create table AirOrderItem (
        id  bigserial not null,
        additionalSegmentInfo jsonb,
        airlinecode varchar(255),
        amount float8,
        arrivalTime timestamp,
        bookingId varchar(255),
        departureTime timestamp,
        dest varchar(255),
        flightNumber varchar(255),
        markup float8,
        ruleId int8,
        source varchar(255),
        status varchar(255),
        supplierId varchar(255),
        travellerInfo jsonb,
        primary key (id)
    );


     create table AirOrderItem_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        additionalSegmentInfo jsonb,
        airlinecode varchar(255),
        amount float8,
        arrivalTime timestamp,
        bookingId varchar(255),
        departureTime timestamp,
        dest varchar(255),
        flightNumber varchar(255),
        markup float8,
        ruleId int8,
        source varchar(255),
        status varchar(255),
        supplierId varchar(255),
        travellerInfo jsonb,
        primary key (id, REV)
    );

    create table GstInfo (
        id  bigserial not null,
        address varchar(255),
        bookingId varchar(255),
        cityName varchar(255),
        email varchar(255),
        gstNumber varchar(255),
        mobile varchar(255),
        pincode varchar(255),
        registeredName varchar(255),
        state varchar(255),
        primary key (id)
    );

     create table orders (
        id  bigserial not null,
        amount float8,
        bookingId varchar(255),
        bookingUserId varchar(255),
        channelType varchar(255),
        created_on timestamp,
        deliveryInfo jsonb,
        loggedInUserId varchar(255),
        orderType varchar(255),
        staffName varchar(255),
        primary key (id)
    );

    create table orders_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        amount float8,
        bookingId varchar(255),
        bookingUserId varchar(255),
        channelType varchar(255),
        created_on timestamp,
        deliveryInfo jsonb,
        loggedInUserId varchar(255),
        orderType varchar(255),
        staffName varchar(255),
        primary key (id, REV)
    );

     alter table AirBookingDetails_AUD
        add constraint FK66i6pnq50j7npa7j3dd00xg50
        foreign key (REV)
        references customrev;

       alter table AirOrderItem_AUD
        add constraint FKtdlpnxf9ge00qv437ficlwk8i
        foreign key (REV)
        references customrev;


       alter table orders_AUD
        add constraint FKfj97jp5uhwhi2qfica6pmsivf
        foreign key (REV)
        references customrev;

ALTER TABLE airorderitem DROP CONSTRAINT IF EXISTS airorderitem_uniq;
ALTER TABLE airorderitem ADD CONSTRAINT airorderitem_uniq UNIQUE (source,dest,airlinecode,flightnumber,bookingid);


