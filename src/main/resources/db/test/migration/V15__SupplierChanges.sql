drop table IF EXISTS suppliercredentialinfo_aud ;
drop table IF EXISTS suppliercredentialinfo ;

 create table SupplierInfo (
        id  bigserial not null,
        credentialInfo jsonb,
        created_on timestamp,
        enabled boolean,
        name varchar(255),
        sourceId int4,
        supplierId varchar(255),
        primary key (id)
    );


   create table SupplierInfo_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        credentialInfo jsonb,
        created_on timestamp,
        enabled boolean,
        name varchar(255),
        sourceId int4,
        supplierId varchar(255),
        primary key (id, REV)
    );

   alter table SupplierInfo_AUD
        add constraint FK1js0akdfws6h01feq8ubp2nxu
        foreign key (REV)
        references customrev;

    alter table supplier_rules
        rename column supplierInfo to supplierAdditionalInfo;