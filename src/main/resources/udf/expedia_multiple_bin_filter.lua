local function rec_to_map(rec)
   local xrec = map()
   for i, bin_name in ipairs(record.bin_names(rec)) do
       xrec[bin_name] = rec[bin_name]
       xrec["PK"] = record.key(rec)
   end
   return xrec
end

function str_between(stream, city, ratings)
   info("city %s, rating %s", tostring(city),tostring(ratings))
   local function range_filter(rec)
      local cVal = rec["CITY"]
      local rVal = rec["RATING"]
      if cVal == city then
         info("rating list %s", tostring(string.gmatch(ratings, '([^,]+)')))
         for value in string.gmatch(ratings, '([^,]+)') do
             if value == rVal then
                 return true
             end
         end
         return false
      else
         return false
      end
   end
   return stream:filter(range_filter):map(rec_to_map)
end