function modify_ttl(rec, to_ttl)
    record.set_ttl(rec, to_ttl)
    aerospike:update(rec)
end