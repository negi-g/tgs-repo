FROM openjdk:8-jdk-alpine

MAINTAINER tgs

ENV TZ="Asia/Kolkata"

WORKDIR /app

RUN mkdir appl
RUN mkdir -p src/main/webapp

COPY build/libs/tgs-java-based-services.jar tgs-java-based-services.jar
COPY src/main/webapp src/main/webapp
COPY java-services.sh java-services.sh
COPY src/main/resources src/main/resources

RUN chmod +x /app/tgs-java-based-services.jar

CMD ["java","-jar" ,"/app/tgs-java-based-services.jar"]
