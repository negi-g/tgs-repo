#!/bin/bash

# Move to master branch
git checkout master

# Collect branches
branches=()
eval "$(git for-each-ref --shell --format='branches+=(%(refname))' refs/heads/)"

# Chip it up
for branch in "${branches[@]}"; do
  old="refs/heads/"
  branchName=${branch/$old/}
  if [[ "$branchName" != "master" && "$branchName" != "staging" ]]; then
    #git branch -D $branchName
    echo "Branch Name " $branchName
    git show --summary $branchName
    read -p 'Is Delete [type y else skipped.] : ' isDelete
    if [[ "y" = $isDelete ]]; then
    	git push origin --delete $branchName
    	git branch -d $branchName
    fi
  fi
done